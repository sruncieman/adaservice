﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.CleansingService.Interface;
using System.Text.RegularExpressions;
using System.Net.Mail;
using MDA.Common.Server;
using MDA.Common;
using MDA.DAL;

namespace MDA.CleansingService.Mock
{
    public class CleansingService : ICleansingService
    {
        //protected List<RiskWordFilter> _riskWords;
        //protected bool _trace = false;
        protected CurrentContext _ctx;
        //protected MdaDbContext _db;

        public CleansingService(CurrentContext ctx)
        {
            //_ctx = ctx;
            //_db = ctx.db as MdaDbContext;
            //_trace = ctx.Trace;

            //_riskWords = new MDA.RiskService.RiskServices(ctx).GetRiskWordsForClient(ctx.riskClientId);
        }

        public ProcessingResults CleanseTheClaim(MDA.Pipeline.Model.IPipelineClaim claim)
        {
            return new ProcessingResults("Cleanse");
        }

        public CurrentContext CurrentContext
        {
            set { _ctx = value; }
        }

        //public MdaDbContext SetDbContext
        //{
        //    set
        //    {
        //        //_db = value;
        //        //_ctx.db = value;
        //    }
        //}

        public int CleansePolicy(MDA.DAL.Policy policy, MessageNode errorNode)
        { return 0; }
        public int CleanseAddress(MDA.DAL.Address address, MessageNode errorNode)
        { return 0; }
        public int CleanseBankAccount(MDA.DAL.BankAccount bankAccount, MessageNode errorNode)
        { return 0; }
        public int CleanseOrganisation(MDA.DAL.Organisation organisation, MessageNode errorNode)
        { return 0; }
        public int CleansePaymentCard(MDA.DAL.PaymentCard paymentCard, MessageNode errorNode)
        { return 0; }
        public int CleansePerson(MDA.DAL.Person person, MessageNode errorNode)
        { return 0; }
        public int CleanseTelephone(MDA.DAL.Telephone telephone, MessageNode errorNode)
        { return 0; }
        public int CleanseVehicle(MDA.DAL.Vehicle vehicle, MessageNode errorNode)
        { return 0; }
        public int CleanseClaimInfo(Incident2Person i2p, MessageNode errorNode)
        { return 0; }
    }
}
