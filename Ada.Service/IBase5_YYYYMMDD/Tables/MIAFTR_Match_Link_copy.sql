﻿CREATE TABLE [dbo].[MIAFTR_Match_Link_copy] (
    [Unique_ID]           VARCHAR (50)  NOT NULL,
    [AltEntity]           VARCHAR (50)  NULL,
    [Create_Date]         DATETIME      NOT NULL,
    [Create_User]         VARCHAR (10)  NOT NULL,
    [DB_Source_387004609] VARCHAR (50)  NULL,
    [Description_]        VARCHAR (50)  NULL,
    [Last_Upd_Date]       DATETIME      NULL,
    [Last_Upd_User]       VARCHAR (10)  NULL,
    [Record_Status]       TINYINT       NULL,
    [SCC]                 VARCHAR (255) NULL,
    [Status_Binding]      VARCHAR (50)  NULL
);

