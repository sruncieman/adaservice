﻿CREATE TABLE [dbo].[_FormLinkField] (
    [Form_ID]       INT      NOT NULL,
    [Entity_ID]     INT      NOT NULL,
    [Link_ID]       INT      NOT NULL,
    [LinkDirection] TINYINT  NOT NULL,
    [Field_ID]      INT      NOT NULL,
    [Column_No]     TINYINT  NULL,
    [FieldIndex]    SMALLINT NOT NULL,
    [Page_No]       TINYINT  NULL,
    CONSTRAINT [PK__FormLinkField] PRIMARY KEY NONCLUSTERED ([Form_ID] ASC, [Entity_ID] ASC, [Link_ID] ASC, [LinkDirection] ASC, [Field_ID] ASC) WITH (FILLFACTOR = 90)
);

