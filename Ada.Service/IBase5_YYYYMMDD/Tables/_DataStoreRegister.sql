﻿CREATE TABLE [dbo].[_DataStoreRegister] (
    [RecordType]  INT           NOT NULL,
    [Icon]        VARCHAR (255) NULL,
    [Name]        VARCHAR (255) NOT NULL,
    [VisibleType] TINYINT       NULL,
    CONSTRAINT [PK__DataStoreRegister] PRIMARY KEY NONCLUSTERED ([RecordType] ASC) WITH (FILLFACTOR = 90)
);

