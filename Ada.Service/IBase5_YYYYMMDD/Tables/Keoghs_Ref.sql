﻿CREATE TABLE [dbo].[Keoghs_Ref] (
    [Unique_ID]               VARCHAR (50)  NOT NULL,
    [AltEntity]               VARCHAR (50)  NULL,
    [Create_Date]             DATETIME      NOT NULL,
    [Create_User]             VARCHAR (10)  NOT NULL,
    [DB_Source_387004609]     VARCHAR (50)  NULL,
    [Keoghs_AXIA_Ref]         VARCHAR (50)  NULL,
    [Keoghs_ELITE_Ref]        VARCHAR (50)  NULL,
    [Last_Upd_Date]           DATETIME      NULL,
    [Last_Upd_User]           VARCHAR (10)  NULL,
    [Record_Status]           TINYINT       DEFAULT (0) NULL,
    [SCC]                     VARCHAR (255) DEFAULT ('') NULL,
    [Status_Binding]          VARCHAR (50)  DEFAULT ('') NULL,
    [IconColour]              INT           NULL,
    [Report_Reference]        VARCHAR (255) NULL,
    [Forwarded_Record_Column] INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Keoghs_Ref] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Keoghs_AXIA_Ref]
    ON [dbo].[Keoghs_Ref]([Keoghs_AXIA_Ref] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Keoghs_ELITE_Ref]
    ON [dbo].[Keoghs_Ref]([Keoghs_ELITE_Ref] ASC) WITH (FILLFACTOR = 90);

