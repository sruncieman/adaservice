﻿CREATE TABLE [dbo].[Account_] (
    [Unique_ID]               VARCHAR (50)  NOT NULL,
    [Account_Number]          VARCHAR (50)  NULL,
    [AltEntity]               VARCHAR (50)  NULL,
    [Card_Number]             VARCHAR (50)  NULL,
    [Create_Date]             DATETIME      NOT NULL,
    [Create_User]             VARCHAR (10)  NOT NULL,
    [DB_Source_387004609]     VARCHAR (50)  NULL,
    [Last_Upd_Date]           DATETIME      NULL,
    [Last_Upd_User]           VARCHAR (10)  NULL,
    [Record_Status]           TINYINT       DEFAULT (0) NULL,
    [SCC]                     VARCHAR (255) DEFAULT ('') NULL,
    [Sort_Code]               VARCHAR (8)   NULL,
    [Status_Binding]          VARCHAR (50)  DEFAULT ('') NULL,
    [Bank_]                   VARCHAR (255) NULL,
    [IconColour]              INT           NULL,
    [Forwarded_Record_Column] INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Account_] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Account_Number]
    ON [dbo].[Account_]([Account_Number] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Card_Number]
    ON [dbo].[Account_]([Card_Number] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Sort_Code]
    ON [dbo].[Account_]([Sort_Code] ASC) WITH (FILLFACTOR = 90);

