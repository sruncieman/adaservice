﻿CREATE TABLE [dbo].[_wsMatches] (
    [W_Id] INT NULL,
    [R_Id] INT NULL
);


GO
CREATE NONCLUSTERED INDEX [W_IdIndex]
    ON [dbo].[_wsMatches]([W_Id] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [R_IdIndex]
    ON [dbo].[_wsMatches]([R_Id] ASC) WITH (FILLFACTOR = 90);

