﻿CREATE TABLE [dbo].[_LinkEnd_copy] (
    [Link_ID]        VARCHAR (50)  NOT NULL,
    [Entity_ID1]     VARCHAR (50)  NOT NULL,
    [Confidence]     TINYINT       NOT NULL,
    [Direction]      TINYINT       NOT NULL,
    [Entity_ID2]     VARCHAR (50)  NOT NULL,
    [EntityType_ID1] INT           NOT NULL,
    [EntityType_ID2] INT           NOT NULL,
    [LinkType_ID]    INT           NOT NULL,
    [Record_Status]  TINYINT       NULL,
    [Record_Type]    TINYINT       NOT NULL,
    [SCC]            VARCHAR (255) NULL
);

