﻿CREATE TABLE [dbo].[_wsWords] (
    [Id]     INT          NULL,
    [Word]   VARCHAR (20) NULL,
    [Count_] INT          NULL,
    [Phone]  VARCHAR (4)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IdIndex]
    ON [dbo].[_wsWords]([Id] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [WordIndex]
    ON [dbo].[_wsWords]([Word] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [PhoneIndex]
    ON [dbo].[_wsWords]([Phone] ASC) WITH (FILLFACTOR = 90);

