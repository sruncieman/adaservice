﻿CREATE TABLE [dbo].[_Codes] (
    [Unique_ID]    VARCHAR (50)  NOT NULL,
    [Code]         VARCHAR (255) NULL,
    [CodeGroup_ID] INT           NOT NULL,
    [Expansion]    VARCHAR (255) NULL,
    [Description]  VARCHAR (255) NULL,
    [Parent_ID]    VARCHAR (50)  NULL,
    [SortIndex]    INT           NULL,
    CONSTRAINT [PK__Codes] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [Codegroup_ID]
    ON [dbo].[_Codes]([CodeGroup_ID] ASC) WITH (FILLFACTOR = 90);

