﻿CREATE TABLE [dbo].[_CalculatedField] (
    [Field_ID]    INT        NOT NULL,
    [Sequence_No] INT        NOT NULL,
    [Op_DatePart] TINYINT    NULL,
    [Op_Field_ID] INT        NULL,
    [Op_Type]     TINYINT    NULL,
    [Op_Value]    FLOAT (53) NULL,
    CONSTRAINT [PK__CalculatedField] PRIMARY KEY NONCLUSTERED ([Field_ID] ASC, [Sequence_No] ASC) WITH (FILLFACTOR = 90)
);

