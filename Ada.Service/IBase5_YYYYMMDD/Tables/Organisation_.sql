﻿CREATE TABLE [dbo].[Organisation_] (
    [Unique_ID]               VARCHAR (50)  NOT NULL,
    [AltEntity]               VARCHAR (50)  NULL,
    [Create_Date]             DATETIME      NOT NULL,
    [Create_User]             VARCHAR (10)  NOT NULL,
    [DB_Source_387004609]     VARCHAR (50)  NULL,
    [Document_]               IMAGE         NULL,
    [Document__Binding]       VARCHAR (50)  NULL,
    [Last_Upd_Date]           DATETIME      NULL,
    [Last_Upd_User]           VARCHAR (10)  NULL,
    [Name_]                   VARCHAR (255) NULL,
    [Picture_]                IMAGE         NULL,
    [Picture__Binding]        VARCHAR (50)  NULL,
    [Record_Status]           TINYINT       DEFAULT (0) NULL,
    [SCC]                     VARCHAR (255) DEFAULT ('') NULL,
    [Status_Binding]          VARCHAR (50)  DEFAULT ('') NULL,
    [VAT_No]                  VARCHAR (50)  NULL,
    [Registered_No]           VARCHAR (50)  NULL,
    [IconColour]              INT           NULL,
    [Forwarded_Record_Column] INT           IDENTITY (1, 1) NOT NULL,
    [Incorporated_Date]       DATETIME      NULL,
    [Dissolved_Date]          DATETIME      NULL,
    [Organisation_Type]       VARCHAR (100) NULL,
    CONSTRAINT [PK_Organisation_] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Name_]
    ON [dbo].[Organisation_]([Name_] ASC) WITH (FILLFACTOR = 90);

