﻿CREATE TABLE [dbo].[_wsRecords] (
    [Id]        INT          NULL,
    [Record_Id] VARCHAR (18) NULL
);


GO
CREATE NONCLUSTERED INDEX [IdIndex]
    ON [dbo].[_wsRecords]([Id] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [Record_IdIndex]
    ON [dbo].[_wsRecords]([Record_Id] ASC) WITH (FILLFACTOR = 90);

