﻿CREATE TABLE [dbo].[_Configuration] (
    [Item] VARCHAR (255) NOT NULL,
    [Data] TEXT          NOT NULL,
    CONSTRAINT [PK__Configuration] PRIMARY KEY NONCLUSTERED ([Item] ASC) WITH (FILLFACTOR = 90)
);

