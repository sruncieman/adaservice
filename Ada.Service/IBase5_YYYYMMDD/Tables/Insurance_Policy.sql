﻿CREATE TABLE [dbo].[Insurance_Policy] (
    [Unique_ID]               VARCHAR (50)  NOT NULL,
    [AltEntity]               VARCHAR (50)  NULL,
    [Cancellation_Date]       DATETIME      NULL,
    [Create_Date]             DATETIME      NOT NULL,
    [Create_User]             VARCHAR (10)  NOT NULL,
    [DB_Source_387004609]     VARCHAR (50)  NULL,
    [Inception_Date]          DATETIME      NULL,
    [Insurer_]                VARCHAR (255) NULL,
    [Last_Upd_Date]           DATETIME      NULL,
    [Last_Upd_User]           VARCHAR (10)  NULL,
    [Policy_Number]           VARCHAR (50)  NULL,
    [Record_Status]           TINYINT       DEFAULT (0) NULL,
    [SCC]                     VARCHAR (255) DEFAULT ('') NULL,
    [Status_Binding]          VARCHAR (50)  DEFAULT ('') NULL,
    [Type_]                   VARCHAR (255) NULL,
    [IconColour]              INT           NULL,
    [Forwarded_Record_Column] INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Insurance_Policy] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Policy_Number]
    ON [dbo].[Insurance_Policy]([Policy_Number] ASC) WITH (FILLFACTOR = 90);

