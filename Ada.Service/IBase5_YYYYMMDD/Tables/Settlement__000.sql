﻿CREATE TABLE [dbo].[Settlement__000] (
    [Unique_ID]             VARCHAR (50)  NOT NULL,
    [AltEntity]             VARCHAR (50)  NULL,
    [Create_Date]           DATETIME      NOT NULL,
    [Create_User]           VARCHAR (10)  NOT NULL,
    [DB_Source_387004609]   VARCHAR (50)  NULL,
    [Last_Upd_Date]         DATETIME      NULL,
    [Last_Upd_User]         VARCHAR (10)  NULL,
    [Record_Status]         TINYINT       DEFAULT (0) NULL,
    [SCC]                   VARCHAR (255) DEFAULT ('') NULL,
    [Settlement_Date]       DATETIME      NULL,
    [Status_Binding]        VARCHAR (50)  DEFAULT ('') NULL,
    [Total_Special_Damages] MONEY         NULL,
    [Total_TP_Costs]        MONEY         NULL,
    [Value_]                MONEY         NULL,
    [Settlement_Type]       VARCHAR (255) NULL,
    [IconColour]            INT           NULL,
    [Keoghs_Reference]      VARCHAR (25)  NULL,
    CONSTRAINT [PK_Settlement__000] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);

