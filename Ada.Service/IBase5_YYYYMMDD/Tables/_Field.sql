﻿CREATE TABLE [dbo].[_Field] (
    [Field_ID]         INT           NOT NULL,
    [Characteristic]   SMALLINT      NOT NULL,
    [ChartAttributeID] INT           NULL,
    [CodeGroup_ID]     INT           NULL,
    [DefaultValue]     VARCHAR (255) NULL,
    [Description]      VARCHAR (255) NULL,
    [Discriminator]    SMALLINT      NOT NULL,
    [FieldIndex]       SMALLINT      NULL,
    [FieldSize]        TINYINT       NULL,
    [Fixed]            SMALLINT      NOT NULL,
    [Format]           VARCHAR (255) NULL,
    [LogicalName]      VARCHAR (255) NOT NULL,
    [LogicalType]      TINYINT       NOT NULL,
    [Mandatory]        SMALLINT      NOT NULL,
    [PhysicalName]     VARCHAR (255) NOT NULL,
    [Search]           SMALLINT      NOT NULL,
    [Table_ID]         INT           NOT NULL,
    CONSTRAINT [PK__Field] PRIMARY KEY NONCLUSTERED ([Field_ID] ASC) WITH (FILLFACTOR = 90)
);

