﻿CREATE TABLE [dbo].[Fraud_Ring] (
    [Unique_ID]                  VARCHAR (50)  NOT NULL,
    [AltEntity]                  VARCHAR (50)  NULL,
    [Create_Date]                DATETIME      NOT NULL,
    [Create_User]                VARCHAR (10)  NOT NULL,
    [DB_Source_387004609]        VARCHAR (50)  NULL,
    [Last_Upd_Date]              DATETIME      NULL,
    [Last_Upd_User]              VARCHAR (10)  NULL,
    [Name_]                      VARCHAR (255) NULL,
    [Record_Status]              TINYINT       DEFAULT (0) NULL,
    [SCC]                        VARCHAR (255) DEFAULT ('') NULL,
    [Status_Binding]             VARCHAR (50)  DEFAULT ('') NULL,
    [IconColour]                 INT           NULL,
    [Key_Attractor]              VARCHAR (200) NULL,
    [MO_]                        VARCHAR (200) NULL,
    [Industry_Alternative_Names] VARCHAR (100) NULL,
    [Owning_Insurer]             VARCHAR (100) NULL,
    [Closed_Date]                DATETIME      NULL,
    CONSTRAINT [PK_Fraud_Ring] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);

