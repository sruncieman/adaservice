﻿CREATE TABLE [dbo].[Report_] (
    [Unique_ID]           VARCHAR (50)  NOT NULL,
    [AltEntity]           VARCHAR (50)  NULL,
    [Create_Date]         DATETIME      NOT NULL,
    [Create_User]         VARCHAR (10)  NOT NULL,
    [DB_Source_387004609] VARCHAR (100) NULL,
    [Last_Upd_Date]       DATETIME      NULL,
    [Last_Upd_User]       VARCHAR (10)  NULL,
    [Record_Status]       TINYINT       DEFAULT (0) NULL,
    [SCC]                 VARCHAR (255) DEFAULT ('') NULL,
    [Status_Binding]      VARCHAR (50)  DEFAULT ('') NULL,
    [IconColour]          INT           NULL,
    [Description_]        VARCHAR (50)  NULL,
    CONSTRAINT [PK_Report_] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);

