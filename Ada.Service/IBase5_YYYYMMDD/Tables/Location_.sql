﻿CREATE TABLE [dbo].[Location_] (
    [Unique_ID]               VARCHAR (50)  NOT NULL,
    [AltEntity]               VARCHAR (50)  NULL,
    [County_]                 VARCHAR (255) NULL,
    [Create_Date]             DATETIME      NOT NULL,
    [Create_User]             VARCHAR (10)  NOT NULL,
    [DB_Source_387004609]     VARCHAR (50)  NULL,
    [Document_]               IMAGE         NULL,
    [Document__Binding]       VARCHAR (50)  NULL,
    [DX_Exchange]             VARCHAR (150) NULL,
    [DX_Number]               VARCHAR (50)  NULL,
    [Easting_]                FLOAT (53)    NULL,
    [Flat_No]                 VARCHAR (20)  NULL,
    [Last_Upd_Date]           DATETIME      NULL,
    [Last_Upd_User]           VARCHAR (10)  NULL,
    [Locality_]               VARCHAR (50)  NULL,
    [Northing_]               FLOAT (53)    NULL,
    [Picture_]                IMAGE         NULL,
    [Picture__Binding]        VARCHAR (50)  NULL,
    [Post_Code]               VARCHAR (50)  NULL,
    [Property_Name]           VARCHAR (50)  NULL,
    [Property_No]             VARCHAR (50)  NULL,
    [QAS_Result_Code]         VARCHAR (50)  NULL,
    [Record_Status]           TINYINT       DEFAULT (0) NULL,
    [SCC]                     VARCHAR (255) DEFAULT ('') NULL,
    [Status_Binding]          VARCHAR (50)  DEFAULT ('') NULL,
    [Street_]                 VARCHAR (50)  NULL,
    [Town_]                   VARCHAR (50)  NULL,
    [Type_]                   VARCHAR (255) NULL,
    [IconColour]              INT           NULL,
    [Forwarded_Record_Column] INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Location_] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Post_Code]
    ON [dbo].[Location_]([Post_Code] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Property_Name]
    ON [dbo].[Location_]([Property_Name] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Property_No]
    ON [dbo].[Location_]([Property_No] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Street_]
    ON [dbo].[Location_]([Street_] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Town_]
    ON [dbo].[Location_]([Town_] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Flat_No]
    ON [dbo].[Location_]([Flat_No] ASC) WITH (FILLFACTOR = 90);

