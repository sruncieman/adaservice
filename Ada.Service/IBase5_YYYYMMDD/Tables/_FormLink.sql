﻿CREATE TABLE [dbo].[_FormLink] (
    [Form_ID]         INT          NOT NULL,
    [Entity_ID]       INT          NOT NULL,
    [Link_ID]         INT          NOT NULL,
    [LinkDirection]   TINYINT      NOT NULL,
    [EntityLinkIndex] SMALLINT     NOT NULL,
    [IgnoreDirection] SMALLINT     NOT NULL,
    [LinkDescription] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK__FormLink] PRIMARY KEY NONCLUSTERED ([Form_ID] ASC, [Entity_ID] ASC, [Link_ID] ASC, [LinkDirection] ASC) WITH (FILLFACTOR = 90)
);

