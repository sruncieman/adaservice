﻿CREATE TABLE [dbo].[Finance_Link] (
    [Unique_ID]               VARCHAR (50)  NOT NULL,
    [AltEntity]               VARCHAR (50)  NULL,
    [Create_Date]             DATETIME      NOT NULL,
    [Create_User]             VARCHAR (10)  NOT NULL,
    [DB_Source_387004609]     VARCHAR (100) NULL,
    [Description_]            VARCHAR (255) NULL,
    [Last_Upd_Date]           DATETIME      NULL,
    [Last_Upd_User]           VARCHAR (10)  NULL,
    [Record_Status]           TINYINT       DEFAULT (0) NULL,
    [SCC]                     VARCHAR (255) DEFAULT ('') NULL,
    [Status_Binding]          VARCHAR (50)  DEFAULT ('') NULL,
    [IconColour]              INT           NULL,
    [Forwarded_Record_Column] INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Finance_Link] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);

