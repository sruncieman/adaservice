﻿CREATE TABLE [dbo].[Vehicle_] (
    [Unique_ID]               VARCHAR (50)  NOT NULL,
    [AltEntity]               VARCHAR (50)  NULL,
    [Colour_]                 VARCHAR (255) NULL,
    [Create_Date]             DATETIME      NOT NULL,
    [Create_User]             VARCHAR (10)  NOT NULL,
    [DB_Source_387004609]     VARCHAR (50)  NULL,
    [Document_]               IMAGE         NULL,
    [Document__Binding]       VARCHAR (50)  NULL,
    [Last_Upd_Date]           DATETIME      NULL,
    [Last_Upd_User]           VARCHAR (10)  NULL,
    [Make_]                   VARCHAR (255) NULL,
    [Model_]                  VARCHAR (50)  NULL,
    [Picture_]                IMAGE         NULL,
    [Picture__Binding]        VARCHAR (50)  NULL,
    [Record_Status]           TINYINT       NULL,
    [Registration_No]         VARCHAR (20)  NULL,
    [SCC]                     VARCHAR (255) NULL,
    [Status_Binding]          VARCHAR (50)  NULL,
    [IconColour]              INT           NULL,
    [Forwarded_Record_Column] INT           IDENTITY (1, 1) NOT NULL,
    [VIN_Number]              VARCHAR (20)  NULL,
    CONSTRAINT [pk_vehicle_] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Registration_No]
    ON [dbo].[Vehicle_]([Registration_No] ASC) WITH (FILLFACTOR = 90);

