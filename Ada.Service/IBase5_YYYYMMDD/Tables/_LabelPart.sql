﻿CREATE TABLE [dbo].[_LabelPart] (
    [Part_ID]   INT           NOT NULL,
    [Field_ID]  INT           NULL,
    [PartIndex] SMALLINT      NOT NULL,
    [Scheme_ID] INT           NOT NULL,
    [Screen]    SMALLINT      NOT NULL,
    [Table_ID]  INT           NOT NULL,
    [Type]      TINYINT       NOT NULL,
    [UserText]  VARCHAR (255) NULL,
    CONSTRAINT [PK__LabelPart] PRIMARY KEY NONCLUSTERED ([Part_ID] ASC) WITH (FILLFACTOR = 90)
);

