﻿CREATE TABLE [dbo].[_DataTable] (
    [Table_ID]     INT           NOT NULL,
    [Fixed]        SMALLINT      NOT NULL,
    [InExpandList] SMALLINT      NOT NULL,
    [LogicalName]  VARCHAR (255) NOT NULL,
    [PhysicalName] VARCHAR (255) NOT NULL,
    [TableCode]    VARCHAR (3)   NOT NULL,
    [Type]         TINYINT       NOT NULL,
    [Description]  VARCHAR (255) NULL,
    CONSTRAINT [PK__DataTable] PRIMARY KEY NONCLUSTERED ([Table_ID] ASC) WITH (FILLFACTOR = 90)
);

