﻿CREATE TABLE [dbo].[ClaimFile_] (
    [Unique_ID]                  VARCHAR (50)  NOT NULL,
    [AltEntity]                  VARCHAR (50)  NULL,
    [Claim_Type]                 VARCHAR (255) NULL,
    [Closure_Date]               DATETIME      NULL,
    [Comments_]                  TEXT          NULL,
    [Create_Date]                DATETIME      NOT NULL,
    [Create_User]                VARCHAR (10)  NOT NULL,
    [DB_Source_387004609]        VARCHAR (50)  NULL,
    [Fraud_Ring_Name]            VARCHAR (255) NULL,
    [IFB_Ref]                    VARCHAR (50)  NULL,
    [Incident_Date]              DATETIME      NULL,
    [Incident_Time]              DATETIME      NULL,
    [Insurer_]                   VARCHAR (255) NULL,
    [Insurer_Instructing_Office] VARCHAR (50)  NULL,
    [Insurer_Reference]          VARCHAR (50)  NULL,
    [Keoghs_Fee_Earner]          VARCHAR (255) NULL,
    [Keoghs_Office]              VARCHAR (255) NULL,
    [Keoghs_Reference_1]         VARCHAR (50)  NULL,
    [Keoghs_Reference_2]         VARCHAR (50)  NOT NULL,
    [Last_Upd_Date]              DATETIME      NULL,
    [Last_Upd_User]              VARCHAR (10)  NULL,
    [LSI_]                       SMALLINT      NOT NULL,
    [Record_Status]              TINYINT       DEFAULT (0) NULL,
    [SCC]                        VARCHAR (255) DEFAULT ('') NULL,
    [Status_]                    VARCHAR (255) NULL,
    [Status_Binding]             VARCHAR (50)  DEFAULT ('') NULL,
    [Insurer_Claims_Handler]     VARCHAR (50)  NULL,
    [Current_Estimate]           MONEY         NULL,
    [IconColour]                 INT           NULL,
    [Forwarded_Record_Column]    INT           IDENTITY (1, 1) NOT NULL,
    [Data_Type]                  VARCHAR (255) NULL,
    [Primary_Weed_Date]          DATETIME      NULL,
    [Primary_Weed_Details]       VARCHAR (100) NULL,
    [Secondary_Weed_Date]        DATETIME      NULL,
    [Secondary_Weed_Details]     VARCHAR (100) NULL,
    CONSTRAINT [PK_ClaimFile_] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Keoghs_Reference_1]
    ON [dbo].[ClaimFile_]([Keoghs_Reference_1] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Keoghs_Reference_2]
    ON [dbo].[ClaimFile_]([Keoghs_Reference_2] ASC) WITH (FILLFACTOR = 90);

