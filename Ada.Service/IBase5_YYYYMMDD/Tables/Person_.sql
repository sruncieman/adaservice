﻿CREATE TABLE [dbo].[Person_] (
    [Unique_ID]                  VARCHAR (50)  NOT NULL,
    [Alias_]                     VARCHAR (100) NULL,
    [AltEntity]                  VARCHAR (50)  NULL,
    [Comments_]                  TEXT          NULL,
    [Create_Date]                DATETIME      NOT NULL,
    [Create_User]                VARCHAR (10)  NOT NULL,
    [Date_Of_Birth]              DATETIME      NULL,
    [DB_Source_387004609]        VARCHAR (50)  NULL,
    [Document_]                  VARCHAR (255) NULL,
    [Driving_Licence_Number]     VARCHAR (50)  NULL,
    [Forename_1]                 VARCHAR (50)  NULL,
    [Hobbies_]                   VARCHAR (50)  NULL,
    [Last_Upd_Date]              DATETIME      NULL,
    [Last_Upd_User]              VARCHAR (10)  NULL,
    [Middlename_]                VARCHAR (50)  NULL,
    [Nationality_]               VARCHAR (255) NULL,
    [NI_Number]                  VARCHAR (10)  NULL,
    [Occupation_]                VARCHAR (100) NULL,
    [Passport_Number]            VARCHAR (75)  NULL,
    [Picture_]                   IMAGE         NULL,
    [Picture__Binding]           VARCHAR (50)  NULL,
    [Record_Status]              TINYINT       DEFAULT (0) NULL,
    [SCC]                        VARCHAR (255) DEFAULT ('') NULL,
    [SchoolsUniversityColleges_] VARCHAR (150) NULL,
    [Status_Binding]             VARCHAR (50)  DEFAULT ('') NULL,
    [Surname_]                   VARCHAR (50)  NULL,
    [Title_]                     VARCHAR (255) NULL,
    [Kref_]                      VARCHAR (20)  NULL,
    [IconColour]                 INT           NULL,
    [Forwarded_Record_Column]    INT           IDENTITY (1, 1) NOT NULL,
    [NHS_Number]                 VARCHAR (50)  NULL,
    CONSTRAINT [PK_Person_] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Alias_]
    ON [dbo].[Person_]([Alias_] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Date_Of_Birth]
    ON [dbo].[Person_]([Date_Of_Birth] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Forename_1]
    ON [dbo].[Person_]([Forename_1] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Middlename_]
    ON [dbo].[Person_]([Middlename_] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_NI_Number]
    ON [dbo].[Person_]([NI_Number] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Surname_]
    ON [dbo].[Person_]([Surname_] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Kref_]
    ON [dbo].[Person_]([Kref_] ASC) WITH (FILLFACTOR = 90);

