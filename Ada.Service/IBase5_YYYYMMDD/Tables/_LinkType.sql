﻿CREATE TABLE [dbo].[_LinkType] (
    [LinkType_ID] INT          NOT NULL,
    [Colour]      VARCHAR (50) NOT NULL,
    [End1Types]   TEXT         NULL,
    [End2Types]   TEXT         NULL,
    CONSTRAINT [PK__LinkType] PRIMARY KEY NONCLUSTERED ([LinkType_ID] ASC) WITH (FILLFACTOR = 90)
);

