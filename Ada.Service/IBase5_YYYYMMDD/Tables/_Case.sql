﻿CREATE TABLE [dbo].[_Case] (
    [CaseID]      NVARCHAR (50)  NOT NULL,
    [Case_Status] INT            NOT NULL,
    [Name]        NVARCHAR (255) NOT NULL,
    [Date_Open]   DATETIME       NULL,
    [Date_Closed] DATETIME       NULL,
    [Description] NTEXT          NULL,
    [Notes]       NTEXT          NULL,
    CONSTRAINT [PK__Case] PRIMARY KEY NONCLUSTERED ([CaseID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [Name]
    ON [dbo].[_Case]([Name] ASC) WITH (FILLFACTOR = 90);

