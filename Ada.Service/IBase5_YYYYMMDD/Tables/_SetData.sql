﻿CREATE TABLE [dbo].[_SetData] (
    [Set_ID]       VARCHAR (50) NOT NULL,
    [Record_ID]    VARCHAR (50) NOT NULL,
    [Record_Index] INT          NOT NULL,
    CONSTRAINT [PK__SetData] PRIMARY KEY NONCLUSTERED ([Set_ID] ASC, [Record_ID] ASC, [Record_Index] ASC) WITH (FILLFACTOR = 90)
);

