﻿CREATE TABLE [dbo].[_AccessDenied] (
    [GID]            VARCHAR (50)  NOT NULL,
    [ItemType]       INT           NOT NULL,
    [ItemIdentifier] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK__AccessDenied] PRIMARY KEY NONCLUSTERED ([GID] ASC, [ItemType] ASC, [ItemIdentifier] ASC) WITH (FILLFACTOR = 90)
);

