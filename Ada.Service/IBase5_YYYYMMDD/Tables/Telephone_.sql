﻿CREATE TABLE [dbo].[Telephone_] (
    [Unique_ID]               VARCHAR (50)  NOT NULL,
    [AltEntity]               VARCHAR (50)  NULL,
    [Create_Date]             DATETIME      NOT NULL,
    [Create_User]             VARCHAR (10)  NOT NULL,
    [DB_Source_387004609]     VARCHAR (50)  NULL,
    [Detail_]                 VARCHAR (255) NULL,
    [Last_Upd_Date]           DATETIME      NULL,
    [Last_Upd_User]           VARCHAR (10)  NULL,
    [Number_]                 VARCHAR (50)  NULL,
    [Record_Status]           TINYINT       DEFAULT (0) NULL,
    [SCC]                     VARCHAR (255) DEFAULT ('') NULL,
    [Status_Binding]          VARCHAR (50)  DEFAULT ('') NULL,
    [EMail_Address]           VARCHAR (255) NULL,
    [Web_Site]                VARCHAR (255) NULL,
    [IP_Address]              VARCHAR (20)  NULL,
    [IconColour]              INT           NULL,
    [Forwarded_Record_Column] INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Telephone_] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_Number_]
    ON [dbo].[Telephone_]([Number_] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_EMail_Address]
    ON [dbo].[Telephone_]([EMail_Address] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Web_Site]
    ON [dbo].[Telephone_]([Web_Site] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_IP_Address]
    ON [dbo].[Telephone_]([IP_Address] ASC) WITH (FILLFACTOR = 90);

