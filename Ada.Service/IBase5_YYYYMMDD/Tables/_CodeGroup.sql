﻿CREATE TABLE [dbo].[_CodeGroup] (
    [CodeGroup_ID] INT           NOT NULL,
    [Description]  VARCHAR (255) NOT NULL,
    [Notes]        TEXT          NULL,
    [Type]         TINYINT       NULL,
    [Parent_ID]    INT           NULL,
    [SortOrder]    TINYINT       NULL,
    CONSTRAINT [PK__CodeGroup] PRIMARY KEY NONCLUSTERED ([CodeGroup_ID] ASC) WITH (FILLFACTOR = 90)
);

