﻿CREATE TABLE [dbo].[_DataStore] (
    [Record_ID]               VARCHAR (50)  NOT NULL,
    [Category]                VARCHAR (255) NOT NULL,
    [CreateDate]              DATETIME      NOT NULL,
    [CreateUser]              VARCHAR (10)  NOT NULL,
    [Data]                    TEXT          NULL,
    [GIDs]                    VARCHAR (255) NULL,
    [Name]                    VARCHAR (255) NOT NULL,
    [RecordType]              INT           NOT NULL,
    [SCC]                     VARCHAR (50)  NULL,
    [UpdateDate]              DATETIME      NULL,
    [UpdateUser]              VARCHAR (10)  NULL,
    [Description]             VARCHAR (255) NULL,
    [Unique_ID]               VARCHAR (38)  NULL,
    [Forwarded_Record_Column] INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK__DataStore] PRIMARY KEY NONCLUSTERED ([Record_ID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [Category]
    ON [dbo].[_DataStore]([Category] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [Name]
    ON [dbo].[_DataStore]([Name] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [RecordType]
    ON [dbo].[_DataStore]([RecordType] ASC) WITH (FILLFACTOR = 90);

