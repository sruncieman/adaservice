﻿CREATE TABLE [dbo].[_LinkEnd] (
    [Link_ID]        VARCHAR (50)  NOT NULL,
    [Entity_ID1]     VARCHAR (50)  NOT NULL,
    [Confidence]     TINYINT       NOT NULL,
    [Direction]      TINYINT       NOT NULL,
    [Entity_ID2]     VARCHAR (50)  NOT NULL,
    [EntityType_ID1] INT           NOT NULL,
    [EntityType_ID2] INT           NOT NULL,
    [LinkType_ID]    INT           NOT NULL,
    [Record_Status]  TINYINT       DEFAULT (0) NULL,
    [Record_Type]    TINYINT       NOT NULL,
    [SCC]            VARCHAR (255) DEFAULT ('') NULL,
    CONSTRAINT [PK__LinkEnd] PRIMARY KEY NONCLUSTERED ([Link_ID] ASC, [Entity_ID1] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE CLUSTERED INDEX [Entity_ID1]
    ON [dbo].[_LinkEnd]([Entity_ID1] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Dumbbell_1]
    ON [dbo].[_LinkEnd]([Entity_ID2] ASC, [Entity_ID1] ASC, [Link_ID] ASC, [EntityType_ID1] ASC, [LinkType_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Dumbbell_1R]
    ON [dbo].[_LinkEnd]([Entity_ID1] ASC, [Entity_ID2] ASC, [Link_ID] ASC, [EntityType_ID2] ASC, [LinkType_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Dumbbell_2]
    ON [dbo].[_LinkEnd]([Entity_ID1] ASC, [Entity_ID2] ASC, [EntityType_ID2] ASC, [LinkType_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_SCC]
    ON [dbo].[_LinkEnd]([SCC] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX__LinkEnd_Record_Status_20130823]
    ON [dbo].[_LinkEnd]([Record_Status] ASC)
    INCLUDE([Entity_ID2], [Entity_ID1], [Link_ID]);

