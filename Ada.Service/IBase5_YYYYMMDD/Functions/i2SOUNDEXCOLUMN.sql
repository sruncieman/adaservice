﻿CREATE FUNCTION [dbo].[i2SOUNDEXCOLUMN] 
(@coltempvalue text,
@word varchar(255))
RETURNS smallint AS
BEGIN
DECLARE
@colvalue varchar(8000),
@found smallint,
@n int,
@len int,
@subword varchar(255)
	SET @colvalue = CAST(@coltempvalue AS varchar(8000))
	SET @found = 0
	IF (@colvalue IS NOT NULL)
	BEGIN
		SET @len = 1
		WHILE @len <= LEN(@colvalue)
		BEGIN
			SET @subword = ''
			WHILE SUBSTRING(@colvalue, @len, 1) <> ' ' AND @len <=LEN(@colvalue)
			BEGIN
				SET @subword = @subword + SUBSTRING(@colvalue, @len, 1)
				SET @len = @len + 1
				CONTINUE
			END
			SET @len = @len + 1
			SET @found = (SELECT 1 WHERE dbo.i2SOUNDEX(@subword) = dbo.i2SOUNDEX(@word))
			IF ISNULL(@found, 0) = 1
				BREAK
			ELSE
				CONTINUE
		END
	END
	RETURN ISNULL(@found, 0)
END