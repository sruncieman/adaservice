﻿CREATE FUNCTION [dbo].[i2SOUNDEX] (@word varchar(255))
RETURNS char(4) AS
BEGIN
DECLARE
@phone varchar(4),
@keepchars varchar(255),
@position smallint,
@char char(1),
@check char(1),
@prevchar char(1)
SET @phone = ''
SET @keepchars = ''
-- Remove repeating 'sound-a-like' characters and any 'vowels', AEIOUHWY.
IF @word <> ''
BEGIN
	SET @check = '*'
	SET @position = 1
	WHILE @position < LEN(@word)
	BEGIN
		SET @char = UPPER(SUBSTRING(@word, @position + 1, 1))
		SET @prevchar = UPPER(SUBSTRING(@word, @position, 1))
		IF @position > 1
			SET @check = UPPER(SUBSTRING(@word, @position - 1, 1))
			IF dbo.i2SOUNDCODE(@prevchar) <> dbo.i2SOUNDCODE(@char) AND PATINDEX('%' + @char + '%', 'AEIOUHWY') = 0
			BEGIN
			-- Ignore the consonant, if it comes after an H or W and the letter before that
			-- has the same soundcode as the current one.
			-- NOTE: SQL Server 2000 does not implement this check in its Soundex function,
			-- which is the reason for this user-defined function.
			IF NOT (PATINDEX('%' + @prevchar + '%', 'HW') > 0 AND dbo.i2SOUNDCODE(@check) = dbo.i2SOUNDCODE(@char))
				SET @keepchars = @keepchars + @char
			END
			SET @position = @position + 1
			CONTINUE
		END
		SET @keepchars = LEFT(@word, 1) + @keepchars + '***'
		-- Use first character.
		SET @phone = UPPER(LEFT(@keepchars, 1))
		If PATINDEX('%' + UPPER(@phone) + '%', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') > 0 --character
		BEGIN
			SET @position = 2
			WHILE @position < 5
			BEGIN
				SET @phone = @phone + dbo.i2SOUNDCODE(SUBSTRING(@keepchars, @position, 1))
				SET @position = @position + 1
				CONTINUE
			END
		END
		ELSE -- Non-character.
			SET @phone = @phone + '000'
	END
	RETURN @phone
END