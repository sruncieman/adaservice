﻿CREATE FUNCTION [dbo].[i2SOUNDCODE] (@letter char(1))
RETURNS char(1) AS
BEGIN
DECLARE @code char(1)
	SET @letter = UPPER(@letter)
	SELECT @code =
	CASE
		WHEN PATINDEX('%' + @letter + '%', 'BPFV') > 0 THEN '1'
		WHEN PATINDEX('%' + @letter + '%', 'CSGJKQXZ') > 0 THEN '2'
		WHEN PATINDEX('%' + @letter + '%', 'DT') > 0 THEN '3'
		WHEN PATINDEX('%' + @letter + '%', 'L') > 0 THEN '4'
		WHEN PATINDEX('%' + @letter + '%', 'MN') > 0 THEN '5'
		WHEN PATINDEX('%' + @letter + '%', 'R') > 0 THEN '6'
		ELSE '0'
	END
	RETURN @code
END