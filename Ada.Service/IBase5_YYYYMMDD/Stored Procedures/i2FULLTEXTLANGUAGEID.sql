﻿CREATE PROCEDURE [dbo].[i2FULLTEXTLANGUAGEID] @dbname nvarchar(128)
AS
DECLARE
	@dbcollation nvarchar(128),
	@lcid sql_variant,
	@checksum tinyint
SELECT @checksum = 1
SELECT @dbcollation = CAST(DATABASEPROPERTYEX(@dbname, 'collation') AS nvarchar(128))
SELECT @lcid = COLLATIONPROPERTY(@dbcollation, 'LCID')
SELECT @checksum,
	CASE @lcid
	WHEN 2052 THEN -- 0x804 Chinese_Simplified
		@lcid
	WHEN 1028 THEN -- 0x0404 Chinese_Traditional
		@lcid
	WHEN 1043 THEN -- 0x0413 Dutch
		@lcid
	WHEN 2057 THEN -- 0x0809 English_UK
		@lcid
	WHEN 1033 THEN -- 0x0409 English_US
		@lcid
	WHEN 1036 THEN -- 0x040c French
		@lcid
	WHEN 1031 THEN -- 0x0407 German
		@lcid
	WHEN 1040 THEN -- 0x0410 Italian
		@lcid
	WHEN 1041 THEN -- 0x0411 Japanese
		@lcid
	WHEN 1042 THEN -- 0x0412 Korean
		@lcid
	WHEN 3082 THEN -- 0x0c0a Spanish_Modern
		@lcid
	WHEN 1053 THEN -- 0x041d Swedish_Default
		@lcid
	ELSE
		0 -- Language Neutral
	END