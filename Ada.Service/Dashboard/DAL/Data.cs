﻿using Dashboard.Model;
using MDA.Common.Server;
using MDA.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.DAL
{
    class Data
    {
        private bool _trace = false;
        private CurrentContext _ctx;
        private MdaDbContext _db;

        public Data(CurrentContext ctx)
        {
            _ctx = ctx;
            _db = ctx.db as MdaDbContext;
            _trace = ctx.TraceLoad;

            //_db.Configuration.ProxyCreationEnabled = false;
            //_db.Configuration.LazyLoadingEnabled = false;
        }


        internal IEnumerable<RiskBatch> GetRiskBatches(int clientSelected)
        {


            
            var riskBatches =  from rb in _db.RiskBatches
                               where(rb.RiskClient_Id == clientSelected)
                               select rb;

            return riskBatches;
        }

        internal int CountClaimsByStatus(int batchId, int claimStatus)
        {
            var loadedCount = _db.RiskClaims.Where(x => x.RiskBatch_Id == batchId && x.ClaimStatus == claimStatus).Count();

            return loadedCount;
        }

        internal IQueryable<int> GetRiskClaimLoadingDuration(int batchId)
        {
            var riskClaimLoadingDuration = _db.RiskClaims.Where(x => x.RiskBatch_Id == batchId).Select(x => x.LoadingDurationInMs);

            return riskClaimLoadingDuration;
        }

        internal IEnumerable<int> GetRiskClaimRunScoringDuration(int batchId)
        {
            var claimsQuery = (from rc in _db.RiskClaims
                               join rcr in _db.RiskClaimRuns on rc.Id equals rcr.RiskClaim_Id
                               where rc.RiskBatch_Id == batchId
                               select rcr.ScoringDurationInMs).ToList();


            return claimsQuery;
        }

        internal List<ScoringDetails> GetBatchScoringDetails(int clientSelected)
        {
            var scoringList = new List<ScoringDetails>();
            var riskBatches = _db.RiskBatches.Where(x => x.RiskClient_Id == clientSelected && x.BatchStatus == 5).ToList();

            foreach(var batch in riskBatches){

                 var queryReds = (from rb in _db.RiskBatches
                             join rc in _db.RiskClaims on rb.Id equals rc.RiskBatch_Id
                             join rcr in _db.RiskClaimRuns on rc.Id equals rcr.RiskClaim_Id
                             where rb.RiskClient_Id == clientSelected && rcr.TotalScore >= 250 && rc.RiskBatch_Id == batch.Id
                             select rb.Id).ToList();

                 var queryAmbers = (from rb in _db.RiskBatches
                                  join rc in _db.RiskClaims on rb.Id equals rc.RiskBatch_Id
                                  join rcr in _db.RiskClaimRuns on rc.Id equals rcr.RiskClaim_Id
                                  where rb.RiskClient_Id == clientSelected && rcr.TotalScore >=51 && rcr.TotalScore < 250 && rc.RiskBatch_Id == batch.Id
                                  select rb.Id).ToList();

                 var queryGreens = (from rb in _db.RiskBatches
                                    join rc in _db.RiskClaims on rb.Id equals rc.RiskBatch_Id
                                    join rcr in _db.RiskClaimRuns on rc.Id equals rcr.RiskClaim_Id
                                    where rb.RiskClient_Id == clientSelected && rcr.TotalScore >= 0 && rcr.TotalScore < 51 && rc.RiskBatch_Id == batch.Id
                                    select rb.Id).ToList();

                 int reds = queryReds.Count();
                 int ambers = queryAmbers.Count();
                 int greens = queryGreens.Count();

                 var scoringD = new ScoringDetails{Id = batch.Id, Reference = batch.ClientBatchReference, Reds = reds, Ambers= ambers, Greens= greens };
                 scoringList.Add(scoringD);
            }

            return scoringList;
        }
    }   
}
