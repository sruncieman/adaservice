﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Model
{
    public class BatchDetails 
    {
        public int Id { get; set; }
        public string Reference { get; set; }
        public string Status { get; set; }
        public string CreatedDate { get; set; }

        public int Received { get; set; }

        public int New { get; set; }

        public int Modified { get; set; }

        public int Load { get; set; }

        public int Errors { get; set; }

        public int Skipped { get; set; }

        public string LoadTime { get; set; }

        public string ProcessingTime { get; set; }

        public string ScoringTime { get; set; }

        public string TotalTime { get; set; }

        //public event PropertyChangedEventHandler PropertyChanged;


        //private void OnPropertyChanged([CallerMemberName] string caller = "")
        //{
        //    if (PropertyChanged != null)
        //    {
        //        PropertyChanged(this, new PropertyChangedEventArgs(caller));
        //    }
        //}
            
    }
}
