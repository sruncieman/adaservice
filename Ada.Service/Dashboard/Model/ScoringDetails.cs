﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Model
{
    public class ScoringDetails 
    {
        public int Id { get; set; }
        public string Reference { get; set; }
        

        public int Reds { get; set; }

        public int Ambers { get; set; }

        public int Greens { get; set; }

    }
}
