﻿using Dashboard.DAL;
using Dashboard.Model;
using MDA.Common.Server;
using MDA.DAL;
using MDA.DataService;
using MDA.Pipeline.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dashboard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static int _currentClientContext = 0;

        public MainWindow()
        {
            InitializeComponent();
            

            tabCtrlSummary.Visibility = System.Windows.Visibility.Hidden;
            cboClients.Visibility = System.Windows.Visibility.Hidden;
            btnRefresh.Visibility = System.Windows.Visibility.Hidden;
            lblClients.Visibility = System.Windows.Visibility.Hidden;
            lblBatchDetails.Visibility = System.Windows.Visibility.Hidden;
        }

        private void cboClients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tabCtrlSummary.Visibility = System.Windows.Visibility.Visible;

            lblBatchDetails.Visibility = System.Windows.Visibility.Visible;

            btnRefresh.Visibility = System.Windows.Visibility.Visible;

            int clientSelected = cboClients.SelectedIndex;

            DisplayBatchSummary(clientSelected);
        }

         

        private void DisplayBatchSummary(int clientSelected)
        {
            using (CurrentContext ctx = new CurrentContext(_currentClientContext, 0, "Dashboard", null))
            {
                Data dataService = new Data(ctx);
                var lstScoringDetails = new List<ScoringDetails>();
                var riskBatches = dataService.GetRiskBatches(clientSelected);
                
                lstScoringDetails = dataService.GetBatchScoringDetails(clientSelected);

                var lstBatchDetails = new List<BatchDetails>();
                

                foreach (var batch in riskBatches)
                {
                    var bDetails = new BatchDetails { 
                        Id = batch.Id,
                        Status = GetStatus(batch.BatchStatus, dataService, batch.Id), 
                        Reference = batch.ClientBatchReference, 
                        CreatedDate = batch.CreatedDate.ToShortDateString() + string.Format(" {0}", batch.CreatedDate.ToShortTimeString()), 
                        Received = batch.TotalClaimsReceived, 
                        New = batch.TotalNewClaims, 
                        Modified = batch.TotalModifiedClaims, 
                        Skipped = batch.TotalClaimsSkipped, 
                        Load = batch.TotalClaimsLoaded, 
                        Errors = batch.TotalClaimsInError,
                        LoadTime = GetLoadingTime(batch.LoadingDurationInMs),
                        ProcessingTime = GetProcessingTime(dataService, batch.Id),
                        ScoringTime = GetScoringTime(dataService, batch.Id),
                        TotalTime = GetTotalTime(dataService, batch.Id, batch.LoadingDurationInMs)
                    };

                    lstBatchDetails.Add(bDetails);
                }

                //DataContext = lstBatchDetails;
                dg1.ItemsSource = lstBatchDetails;
                dg2.ItemsSource = lstScoringDetails;
            }
        }

        private string GetTotalTime(Data dataService, int batchId, int loadingTimeInMs)
        {
            TimeSpan loadingSpan = TimeSpan.FromMilliseconds(loadingTimeInMs);
            TimeSpan processingSpan;
            TimeSpan scoringSpan;

            var claimsQuery = dataService.GetRiskClaimLoadingDuration(batchId);
            var claimsScoringQuery =  dataService.GetRiskClaimRunScoringDuration(batchId);

            if (claimsQuery.Any())
            {
                var sumProcessing = claimsQuery.Select(x => x).Sum();
                processingSpan = TimeSpan.FromMilliseconds(sumProcessing);
            }
            else
            {
                processingSpan = TimeSpan.Zero;
            }

            if(claimsScoringQuery.Any()){

                var sumScoring = claimsScoringQuery.Select(x => x).Sum();
                scoringSpan = TimeSpan.FromMilliseconds(sumScoring);
            }
            else
            {
                scoringSpan = TimeSpan.Zero;
            }

            var totalSpan = loadingSpan + processingSpan + scoringSpan;

            return String.Format("{0} days, {1} hrs, {2} mins, {3} secs", totalSpan.Days, totalSpan.Hours, totalSpan.Minutes, totalSpan.Seconds);
        }

        private string GetScoringTime(Data dataService, int batchId)
        {
            var claimsQuery = dataService.GetRiskClaimRunScoringDuration(batchId);

            if (claimsQuery.Any())
            {
                var sumScoring = claimsQuery.Select(x => x).Sum();
                var span = TimeSpan.FromMilliseconds(sumScoring);
                return String.Format("{0} days, {1} hrs, {2} mins, {3} secs", span.Days, span.Hours, span.Minutes, span.Seconds);
            }
            else
            {
                return "0";
            }
        }

        private string GetProcessingTime(Data dataService, int batchId)
        {

            var claimsQuery = dataService.GetRiskClaimLoadingDuration(batchId);

            if (claimsQuery.Any())
            {
                var sumProcessing = claimsQuery.Select(x => x).Sum();

                var span = TimeSpan.FromMilliseconds(sumProcessing);
                return String.Format("{0} days, {1} hrs, {2} mins, {3} secs", span.Days, span.Hours, span.Minutes, span.Seconds);
            }
            else
            {
                return "0";
            }
        }

        private string GetLoadingTime(int loadingTimeInMs)
        {
            var span = TimeSpan.FromMilliseconds(loadingTimeInMs);

            return String.Format("{0} mins, {1} secs",
                span.Minutes, span.Seconds);
            
        }

        private string GetStatus(int statusId, Data dataService, int batchId)
        {
            string strStatus;

            switch (statusId)
            {

                case -1:
                    strStatus = "Bad";
                    break;

                case 0:
                    strStatus = "Initialising";
                    break;
                case 1:
                    strStatus = "QueuedForCreating";
                    break;
                case 2:
                    strStatus = "CreatingClaims";
                    break;
                case 3:
                    strStatus = "QueuedForLoading";
                    break;
                case 4:
                    strStatus = "Loading";
                    break;
                case 5:
                    strStatus = "Complete";
                    break;
                default:
                    strStatus = "None";
                    break;
            }

            int loadedCount = dataService.CountClaimsByStatus(batchId, 3);
            int scoringCount = dataService.CountClaimsByStatus(batchId, 7);
           
            if(loadedCount>0)
                strStatus = string.Format("Loaded ({0})", loadedCount);

            if(scoringCount>0)
                strStatus = string.Format("Scoring ({0} left)",scoringCount);

            return strStatus;
        }

        private void cboEnvironments_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cboClients.SelectedIndex = -1;
            tabCtrlSummary.Visibility = System.Windows.Visibility.Hidden;
            lblBatchDetails.Visibility = System.Windows.Visibility.Hidden;
            btnRefresh.Visibility = System.Windows.Visibility.Hidden;
            lblClients.Visibility = System.Windows.Visibility.Visible;
            cboClients.Visibility = System.Windows.Visibility.Visible;
           

            int environmentSelected = cboEnvironments.SelectedIndex;

            SetEnvironment(environmentSelected);
        }

        private static void SetEnvironment(int environmentSelected)
        {
            if (environmentSelected > 0)
            {
                switch (environmentSelected)
                {
                    case 1:
                        _currentClientContext = 1;
                        break;
                    case 2:
                        _currentClientContext = 3;
                        break;
                    case 3:
                        _currentClientContext = 12;
                        break;
                    case 4:
                        _currentClientContext = 4;
                        break;
                    case 5:
                        _currentClientContext = 8;
                        break;
                    case 6:
                        _currentClientContext = 9;
                        break;
                    case 7:
                        _currentClientContext = 10;
                        break;
                    case 8:
                        _currentClientContext = 99;
                        break;
                }
            }
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            int environmentSelected = cboEnvironments.SelectedIndex;

            SetEnvironment(environmentSelected);

            tabCtrlSummary.Visibility = System.Windows.Visibility.Visible;

            int clientSelected = cboClients.SelectedIndex;

            DisplayBatchSummary(clientSelected);
        }

        
    }
}
