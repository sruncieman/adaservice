﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common.FileModel;
using System.Xml;
using System.Runtime.Serialization;
using MDA.VerificationService.Interface;
using System.Net.Mail;
using System.Text.RegularExpressions;
using MDA.Common.Enum;
using MDA.Common;

namespace MDA.VerificationService.Mobile
{
    public class DetailedValidation : IValidationService
    {

        public ProcessingResults ValidateClaim(MDA.Pipeline.Model.IPipelineClaim newClaim)
        {
            MDA.Pipeline.Model.PipelineMobileClaim claim = newClaim as MDA.Pipeline.Model.PipelineMobileClaim;

            ProcessingResults vr = new ProcessingResults("Validation");

            try
            {
                string claimNum = (claim.ClaimNumber == null) ? "#NoClaimNum#" : claim.ClaimNumber;

                MessageNode claimErrorNode = new MessageNode(claimNum);

                ValidateClaim(claim, claimErrorNode);

                if (claim.Policy == null)
                    claimErrorNode.AddWarning(claimNum + " : Policy section missing");
                else
                {
                    MessageNode policyErrorNode = new MessageNode("Policy");

                    ValidatePolicy(claim.Policy, policyErrorNode);

                    if (policyErrorNode.Nodes.Count > 0)
                        claimErrorNode.AddNode(policyErrorNode);
                }

                if (claim.Organisations != null)
                {
                    MessageNode orgsErrorNode = new MessageNode("Organisations");

                    foreach (var organisation in claim.Organisations)
                    {
                        MessageNode orgErrorNode = new MessageNode("Organisation:" + organisation.DisplayText);

                        ValidateOrganisation(organisation, orgErrorNode);

                        if (organisation.Addresses != null)
                        {
                            MessageNode orgAddressesErrorNode = new MessageNode("Addresses");

                            foreach (var address in organisation.Addresses)
                            {
                                MessageNode orgAddrErrorNode = new MessageNode(address.DisplayText);

                                ValidateAddress(address, orgAddrErrorNode);

                                if (orgAddrErrorNode.Nodes.Count > 0)
                                    orgAddressesErrorNode.AddNode(orgAddrErrorNode);
                            }

                            if (orgAddressesErrorNode.Nodes.Count > 0)
                                orgErrorNode.AddNode(orgAddressesErrorNode);
                        }
                        else
                        {
                            orgErrorNode.AddWarning(claimNum + " : Organisation Address not supplied");
                        }

                        if (organisation.OrganisationType_Id == (int)OrganisationType.PolicyHolder)
                        {
                            if ((organisation.BankAccounts == null || organisation.BankAccounts.Count == 0) &&
                                 (organisation.PaymentCards == null || organisation.PaymentCards.Count == 0))
                            {
                                orgsErrorNode.AddWarning(claimNum + " : Organisation Policyholder has no Bank or Payment card information");
                            }
                            else
                            {
                                if (organisation.BankAccounts != null && organisation.BankAccounts.Count > 0)
                                {
                                    foreach (var ba in organisation.BankAccounts)
                                    {
                                        MessageNode orgBankAccountErrorNode = new MessageNode("Bank Account");

                                        ValidateBankAccount(ba, orgBankAccountErrorNode);

                                        if (orgBankAccountErrorNode.Nodes.Count > 0)
                                            orgErrorNode.AddNode(orgBankAccountErrorNode);
                                    }
                                }

                                if (organisation.PaymentCards != null && organisation.PaymentCards.Count > 0)
                                {
                                    foreach (var pc in organisation.PaymentCards)
                                    {
                                        MessageNode orgPaymentCardErrorNode = new MessageNode("Payment Card");

                                        ValidatePaymentCard(pc, orgPaymentCardErrorNode);

                                        if (orgPaymentCardErrorNode.Nodes.Count > 0)
                                            orgErrorNode.AddNode(orgPaymentCardErrorNode);
                                    }
                                }
                            }
                        }

                        if (orgErrorNode.Nodes.Count > 0)
                            orgsErrorNode.AddNode(orgErrorNode);
                    }

                    if (orgsErrorNode.Nodes.Count > 0)
                        claimErrorNode.AddNode(orgsErrorNode);
                }

                int countInsuredHandsets = 0;

                if (claim.Handsets != null)
                {
                    MessageNode vehiclesErrorNode = new MessageNode("Vehicles");

                    foreach (var handset in claim.Handsets)
                    {
                        MessageNode vehicleErrorNode = new MessageNode("Vehicle:" + handset.DisplayText);

                        //ValidateVehicle(handset, vehicleErrorNode);

                        //if (handset.I2H_LinkData.Incident2HandsetLinkType_Id == (int)Incident2HandsetLinkType.InsuredVehicle)
                        //    countInsuredHandsets++;

                        if (handset.People != null)
                        {
                            MessageNode peopleErrorNode = new MessageNode("People");

                            foreach (var person in handset.People)
                            {
                                MessageNode personErrorNode = new MessageNode("Person:" + person.DisplayText);

                                ValidatePerson(person, personErrorNode);

                                if (person.Addresses != null)
                                {
                                    MessageNode perAddressesErrorNode = new MessageNode("Addresses");

                                    foreach (var address in person.Addresses)
                                    {
                                        MessageNode perAddrErrorNode = new MessageNode(address.DisplayText);

                                        ValidateAddress(address, perAddrErrorNode);

                                        if (perAddrErrorNode.Nodes.Count > 0)
                                            perAddressesErrorNode.AddNode(perAddrErrorNode);
                                    }

                                    if (perAddressesErrorNode.Nodes.Count > 0)
                                        personErrorNode.AddNode(perAddressesErrorNode);
                                }

                                if (person.BankAccounts != null && person.BankAccounts.Count > 0)
                                {
                                    foreach (var ba in person.BankAccounts)
                                    {
                                        MessageNode perBankAccountErrorNode = new MessageNode("Bank Account");

                                        ValidateBankAccount(ba, perBankAccountErrorNode);

                                        if (perBankAccountErrorNode.Nodes.Count > 0)
                                            personErrorNode.AddNode(perBankAccountErrorNode);
                                    }
                                }

                                if (person.PaymentCards != null && person.PaymentCards.Count > 0)
                                {
                                    foreach (var pc in person.PaymentCards)
                                    {
                                        MessageNode perPaymentCardErrorNode = new MessageNode("Payment Card");

                                        ValidatePaymentCard(pc, perPaymentCardErrorNode);

                                        if (perPaymentCardErrorNode.Nodes.Count > 0)
                                            personErrorNode.AddNode(perPaymentCardErrorNode);
                                    }
                                }

                                if (person.Organisations != null)
                                {
                                    MessageNode perOrgsErrorNode = new MessageNode("Organisations");

                                    foreach (var organisation in person.Organisations)
                                    {
                                        MessageNode orgErrorNode = new MessageNode("Organisation:" + organisation.DisplayText);

                                        ValidateOrganisation(organisation, orgErrorNode);

                                        if (organisation.Addresses != null)
                                        {
                                            MessageNode orgAddressesErrorNode = new MessageNode("Addresses");

                                            foreach (var address in organisation.Addresses)
                                            {
                                                MessageNode orgAddrErrorNode = new MessageNode(address.DisplayText);

                                                ValidateAddress(address, orgAddrErrorNode);

                                                if (orgAddrErrorNode.Nodes.Count > 0)
                                                    orgAddressesErrorNode.AddNode(orgAddrErrorNode);
                                            }

                                            if (orgAddressesErrorNode.Nodes.Count > 0)
                                                orgErrorNode.AddNode(orgAddressesErrorNode);
                                        }
                                        else
                                            orgErrorNode.AddNode(new MessageNode(claimNum + " : Organisation Address not supplied", MessageType.Warning));
                                    }
                                }

                                if (personErrorNode.Nodes.Count > 0)
                                    peopleErrorNode.AddNode(personErrorNode);
                            }

                            if (peopleErrorNode.Nodes.Count > 0)
                                vehicleErrorNode.AddNode(peopleErrorNode);
                        }

                        if (vehicleErrorNode.Nodes.Count > 0)
                            vehiclesErrorNode.AddNode(vehicleErrorNode);
                    }

                    if (vehiclesErrorNode.Nodes.Count > 0)
                        claimErrorNode.AddNode(vehiclesErrorNode);
                }
                else
                    claimErrorNode.AddError(claimNum + " : No vehicles specified");

                if (countInsuredHandsets > 1)
                    claimErrorNode.AddError(claimNum + " : More than 1 Insured Vehicles specified");

                if (claimErrorNode.Nodes.Count > 0)
                    vr.Message.AddNode(claimErrorNode);
            }
            catch (Exception ex)
            {
                vr.Message.AddError("Unexpected Error : " + ex.Message);
            }

            return vr;
        }

        private void ValidateClaim(MDA.Pipeline.Model.PipelineMobileClaim claim, MessageNode errorNode)
        {
            var date = CheckDate("Incident", "IncidentDate", claim.IncidentDate, "SmallDateTime", true);

            if (date == null || date.Equals(DateTime.MinValue))
            {
                errorNode.AddError("The Incident date is out of range: " + claim.IncidentDate.ToString());
            }
            else
                claim.IncidentDate = (DateTime)date;
        }

        private void ValidatePolicy(MDA.Pipeline.Model.PipelinePolicy policy, MessageNode errorNode)
        {
            //policy.PolicyNumber = CheckString("Policy", "PolicyNumber", policy.PolicyNumber, 50, false, errorNode);
            //policy.Broker = CheckString("Policy", "Broker", policy.Broker, 50, true, errorNode);
            //policy.TradingName = CheckString("Policy", "TradingName", policy.TradingName, 50, true, errorNode);
            //policy.PolicyStartDate = CheckDate("Policy", "PolicyStartDate", policy.PolicyStartDate, "SmallDateTime", true);
            //policy.PolicyEndDate = CheckDate("Policy", "PolicyEndDate", policy.PolicyEndDate, "SmallDateTime", true);
            //if (policy.PolicyStartDate > policy.PolicyEndDate)
            //{
            //    errorNode.AddWarning(string.Format("Policy start date {0} greater than end date {1} ", policy.PolicyStartDate, policy.PolicyEndDate));
            //}
        }

        private void ValidateVehicle(MDA.Pipeline.Model.PipelineVehicle vehicle, MessageNode errorNode)
        {
            //vehicle.Make = CheckString("Vehicle", "Make", vehicle.Make, 50, true, errorNode);
            //vehicle.Model = CheckString("Vehicle", "Model", vehicle.Model, 20, true, errorNode);
            //vehicle.EngineCapacity = CheckString("Vehicle", "EngineCapacity", vehicle.EngineCapacity, 50, true, errorNode);
            //vehicle.VIN = CheckString("Vehicle", "VIN", vehicle.VIN, 20, true, errorNode);

            //if (vehicle.VehicleRegistration != null)
            //    vehicle.VehicleRegistration = CheckString("Vehicle", "VehicleRegistration", GetAlphaNumericOnly(vehicle.VehicleRegistration.Trim()).ToUpper(), 20, true, errorNode);

        }

        private void ValidateBankAccount(MDA.Pipeline.Model.PipelineBankAccount bankAccount, MessageNode errorNode)
        {

        }

        private void ValidatePaymentCard(MDA.Pipeline.Model.PipelinePaymentCard paymentCard, MessageNode errorNode)
        {

        }

        private void ValidatePerson(MDA.Pipeline.Model.PipelinePerson person, MessageNode errorNode)
        {
            //person.FirstName = CheckString("Person", "FirstName", person.FirstName, 100, true, errorNode);
            //person.MiddleName = CheckString("Person", "MiddleName", person.MiddleName, 100, true, errorNode);
            //person.LastName = CheckString("Person", "LastName", person.LastName, 100, true, errorNode);
            //person.DateOfBirth = CheckDate("Person", "DateOfBirth", person.DateOfBirth, "SmallDate", true);
            //person.Nationality = CheckString("Person", "Nationality", person.Nationality, 50, true, errorNode);

            //if (person.NINumber != null)
            //    person.NINumber = CheckString("Person", "NINumber", GetAlphaNumericOnly(person.NINumber.Trim()).ToUpper(), 10, true, errorNode);

            //if (person.DrivingLicenseNumber != null)
            //    person.DrivingLicenseNumber = CheckString("Person", "DrivingLicenseNumber", GetAlphaNumericOnly(person.DrivingLicenseNumber.Trim()).ToUpper(), 50, true, errorNode);

            //if (person.PassportNumber != null)
            //    person.PassportNumber = CheckString("Person", "PassportNumber", GetAlphaNumericOnly(person.PassportNumber.Trim()).ToUpper(), 50, true, errorNode);

            //person.Occupation = CheckString("Person", "Occupation", person.Occupation, 225, true, errorNode);
            //person.EmailAddress = CheckString("Person", "EmailAddress", person.EmailAddress, 225, true, errorNode);

            ////if (person.EmailAddress != null)
            ////    person.EmailAddress = IsValidEmail(person.EmailAddress.Trim(), errorNode);

            //person.LandlineTelephone = CheckPhoneNumberValid("Person", "LandlineTelephone", person.LandlineTelephone, 50, errorNode);
            //person.WorkTelephone = CheckPhoneNumberValid("Person", "WorkTelephone", person.WorkTelephone, 50, errorNode);
            //person.MobileTelephone = CheckPhoneNumberValid("Person", "MobileTelephone", person.MobileTelephone, 50, errorNode);
            //person.OtherTelephone = CheckPhoneNumberValid("Person", "OtherTelephone", person.OtherTelephone, 50, errorNode);
        }

        private void ValidateOrganisation(MDA.Pipeline.Model.PipelineOrganisation organisation, MessageNode errorNode)
        {
            //organisation.OrganisationName = CheckString("Organisation", "OrganisationName", organisation.OrganisationName, 50, true, errorNode);
            //organisation.RegisteredNumber = CheckString("Organisation", "RegisteredNumber", organisation.RegisteredNumber, 50, true, errorNode);
            //organisation.VatNumber = CheckString("Organisation", "VatNumber", organisation.VatNumber, 50, true, errorNode);
            //organisation.MojCrmNumber = CheckString("Organisation", "MojCrmNumber", organisation.MojCrmNumber, 50, true, errorNode);
            //organisation.Email = CheckString("Organisation", "Email", organisation.Email, 50, true, errorNode);
            ////organisation.Email = IsValidEmail(organisation.Email, errorNode);
            //organisation.WebSite = CheckString("Organisation", "WebSite", organisation.WebSite, 50, true, errorNode);
            //organisation.WebSite = IsValidWebsite(organisation.WebSite, errorNode);
            //organisation.Telephone1 = CheckPhoneNumberValid("Organisation", "Telephone1", organisation.Telephone1, 50, errorNode);
            //organisation.Telephone2 = CheckPhoneNumberValid("Organisation", "Telephone2", organisation.Telephone2, 50, errorNode);
            //organisation.Telephone3 = CheckPhoneNumberValid("Organisation", "Telephone3", organisation.Telephone3, 50, errorNode);
        }

        private void ValidateAddress(MDA.Pipeline.Model.PipelineAddress address, MessageNode errorNode)
        {
            //address.StartOfResidency = CheckDate("Address", "StartOfResidency", address.StartOfResidency, "SmallDate", true);
            //address.EndOfResidency = CheckDate("Address", "EndOfResidency", address.EndOfResidency, "SmallDate", true);

            //if (address.StartOfResidency > address.EndOfResidency)
            //{
            //    errorNode.AddWarning(string.Format("Residency start date {0} greater than end date {1} ", address.StartOfResidency, address.EndOfResidency));
            //}

            //address.SubBuilding = CheckString("Address", "SubBuilding", address.SubBuilding, 50, true, errorNode);
            //address.Building = CheckString("Address", "Building", address.Building, 50, true, errorNode);
            //address.BuildingNumber = CheckString("Address", "BuildingNumber", address.BuildingNumber, 50, true, errorNode);
            //address.Street = CheckString("Address", "Street", address.Street, 50, true, errorNode);
            //address.Locality = CheckString("Address", "Locality", address.Locality, 50, true, errorNode);
            //address.Town = CheckString("Address", "Town", address.Town, 50, true, errorNode);
            //address.County = CheckString("Address", "County", address.County, 50, true, errorNode);
            //address.PostCode = CheckString("Address", "PostCode", address.PostCode, 50, true, errorNode);
        }

        private string GetAlphaNumericOnly(string s)
        {
            StringBuilder ret = new StringBuilder();

            foreach (char c in s)
            {
                if (Char.IsLetterOrDigit(c))
                    ret.Append(c);
            }
            return ret.ToString();
        }

        private string CheckString(string entityName, string fieldName, string s, int maxLength, bool canBeNull, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(s) || string.IsNullOrWhiteSpace(s) && canBeNull) return null;

            s = s.Replace("&", "and");

            if (s.Length > maxLength)
            {
                errorNode.AddWarning(string.Format("{0}.{1}[{2}] : Exceeds maximum field length of {3}. Field Truncated", entityName, fieldName, s, maxLength.ToString()));
                return s.Substring(0, maxLength);
            }

            return s;
        }

        private DateTime? CheckDate(string entityName, string fieldName, DateTime? t, string dateTimeFormat, bool canBeNull)
        {
            if (t == null && canBeNull) return null;

            if (dateTimeFormat == "SmallDateTime")
            {
                t = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(t);
                return t;
            }

            if (dateTimeFormat == "SmallDate")
            {
                t = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(t);
                return t;
            }

            return t;
        }

        //public string IsValidEmail(string emailaddress, MessageNode errorNode)
        //{
        //    try
        //    {
        //        if (emailaddress != null)
        //        {
        //            MailAddress m = new MailAddress(emailaddress);
        //        }
        //        return emailaddress;
        //    }
        //    catch (FormatException)
        //    {
        //        errorNode.AddWarning(string.Format("Invalid email address {0}.", emailaddress));
        //        return "";
        //    }
        //}

        //public string IsValidWebsite(string website, MessageNode errorNode)
        //{
        //    try
        //    {
        //        if (website != null)
        //        {
        //            Uri m = new Uri(website);
        //        }
        //        return website;
        //    }
        //    catch (FormatException)
        //    {
        //        errorNode.AddWarning(string.Format("Invalid website {0}.", website));
        //        return "";
        //    }
        //}

        private string CheckPhoneNumberValid(string entityName, string fieldName, string phoneNumber, int maxLength, MessageNode errorNode)
        {
            if (phoneNumber == null) return null;

            string cleanPhoneNumber = CleanNumber(phoneNumber); // remove all non-numeric characters

            if (cleanPhoneNumber.Length > maxLength)
            {
                errorNode.AddWarning(string.Format("{0}.{1}[{2}] : Exceeds maximum field length of {3}. Field Truncated", entityName, fieldName, phoneNumber, maxLength.ToString()));
                return cleanPhoneNumber.Substring(0, maxLength);
            }

            return cleanPhoneNumber;
        }

        private string CleanNumber(string number)
        {
            if (number == null) return null;

            Regex digitsOnly = new Regex(@"[^\d]");
            return digitsOnly.Replace(number, "");
        }

    }
}
