﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using MDA.Common;
using MDA.VerificationService.Interface;

namespace MDA.VerificationService.Eldon.Motor
{
    public class FileVerification : IVerificationService
    {
        public Common.ProcessingResults QuickVerification(System.IO.Stream file, Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            var vr = new ProcessingResults("Quick Verify");

            if (!CheckFileExtension(file, expectedFileExtns))
            {
                vr.Message.AddError("Reason: The file type is invalid for this client");
            }
            else if (!CheckFileValidity(file))
            {
                vr.Message.AddError("Reason: The file supplied is invalid, wrong number of delimiters");
            }
            else
            {
                file.Position = 0;

                switch (CheckMandatoryFields(file))
                {
                    case 1:
                        vr.Message.AddError("Reason: Mandatory field Claim Reference missing");
                        break;
                    case 2:
                        vr.Message.AddError("Reason: Mandatory field Incident Date missing");
                        break;
                }
            }
            return vr;
        }

        /// <summary>
        /// Verify we have the mandatory fields
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private int CheckMandatoryFields(System.IO.Stream file)
        {
            // Incident Date and Claim Reference are mandatory fields
            int mandatoryFieldCode = 0;

            // 0 - No issues
            // 1 - Claim Ref Null or Empty
            // 2 - Incident Date Null or Empty

            var eldonFile = new System.IO.StreamReader(file);
            string line;

            while ((line = eldonFile.ReadLine()) != null)
            {
                string[] lineItems = line.Split('|');

                if (string.IsNullOrEmpty(lineItems[2]))
                {
                    // Check for Claim Ref
                    mandatoryFieldCode = 1;
                }

                if (lineItems[1] == "CLAIM_MOTOR")
                {
                    // Check for Incident Date
                    if (string.IsNullOrEmpty(lineItems[12]))
                    {
                        mandatoryFieldCode = 2;
                    }
                }
            }
            return mandatoryFieldCode;
        }

        /// <summary>
        ///  // Verify file has correct number of pipe delimiters
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool CheckFileValidity(System.IO.Stream file)
        {
            bool correctNumberOfDelimeters = true;
            try
            {
                int countDelimiters;
                string line;
                int count = 0;

                var eldonFile = new System.IO.StreamReader(file);

                while ((line = eldonFile.ReadLine()) != null)
                {
                    if (count == Convert.ToInt32(ConfigurationManager.AppSettings["EldonDelimiterLimit"]))
                    {
                        break;
                    }

                    countDelimiters = line.Split('|').Length - 1;
                    if (countDelimiters != Convert.ToInt32(ConfigurationManager.AppSettings["EldonDelimiterAmount"]))
                    {
                        correctNumberOfDelimeters = false;
                        break;
                    }

                    count++;
                }


            }

            catch (Exception ex)
            {
                return false;
            }

            return correctNumberOfDelimeters;

        }

        /// <summary>
        /// Verify File has expected file extension
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool CheckFileExtension(System.IO.Stream file, string expectedFileExtns)
        {
            bool correctExtension = false;

            try
            {
                var fileStream = file as FileStream;

                if (fileStream != null)
                {
                    var clientExtensions = expectedFileExtns;

                    string extension = Path.GetExtension(fileStream.Name);
                    if (extension != null) extension = extension.Replace(".", "").ToLower();

                    string[] clientExts = new string[] { };
                    if (clientExtensions != null) clientExts = clientExtensions.Split(',');

                    if (clientExts.Contains(extension))
                    {
                        correctExtension = true;
                    }
                }
            }

            catch (Exception ex)
            {
                return false;
            }

            return correctExtension;
        }
    }
}
