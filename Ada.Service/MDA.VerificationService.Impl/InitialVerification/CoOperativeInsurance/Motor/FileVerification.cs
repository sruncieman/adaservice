﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using MDA.Common;
using MDA.VerificationService.Interface;

namespace MDA.VerificationService.CoOperativeInsurance.Motor
{
    public class FileVerification : IVerificationService
    {
        public Common.ProcessingResults QuickVerification(System.IO.Stream file, Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            var vr = new ProcessingResults("Quick Verify");

            if (!CheckFileValidity(file))
            {
                vr.Message.AddError("Reason: The file supplied is invalid, wrong number of delimiters");
            }

            return vr;
        }


        /// <summary>
        ///  // Verify file has correct number of delimiters
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool CheckFileValidity(System.IO.Stream file)
        {
            bool correctNumberOfDelimeters = true;
            try
            {
                int countDelimiters;
                string line;
                int count = 0;

                var coOpFile = new System.IO.StreamReader(file);

                while ((line = coOpFile.ReadLine()) != null)
                {
                    if (count == Convert.ToInt32(ConfigurationManager.AppSettings["CoOpDelimiterLimit"]))
                    {
                        break;
                    }

                    countDelimiters = line.Split(',').Length - 1;
                    if (countDelimiters != Convert.ToInt32(ConfigurationManager.AppSettings["CoOpDelimiterAmount"]))
                    {
                        correctNumberOfDelimeters = false;
                        break;
                    }

                    count++;
                }


            }

            catch (Exception ex)
            {
                return false;
            }

            return correctNumberOfDelimeters;

        }

    }
}
