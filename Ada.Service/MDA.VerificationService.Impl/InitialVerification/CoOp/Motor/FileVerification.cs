﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using FileHelpers;
using MDA.Common;
//using MDA.MappingService.CoOp.Motor.Model;
using MDA.VerificationService.Interface;


namespace MDA.VerificationService.CoOp.Motor
{
    public class FileVerification : IVerificationService
    {
        public Common.ProcessingResults QuickVerification(System.IO.Stream file, Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            var vr = new ProcessingResults("Quick Verify");

            // 1. Check file has the correct extension
            // 2. Check correct number of header columns exist
            // 3. Check mandatory fields exist

            if (!CheckFileExtension(file, expectedFileExtns))
            {
                vr.Message.AddError("Reason: The file type is invalid for this client");
            }
            else if (!CheckFileValidity(file))
            {
                vr.Message.AddError("Reason: The file supplied is invalid, wrong number of delimiters");
            }
            //else
            //{
            //    file.Position = 0;

            //    switch (CheckMandatoryFields(file))
            //    {
            //        case 1:
            //            vr.Message.AddError("Reason: Mandatory field Claim Reference missing");
            //            break;
            //        case 2:
            //            vr.Message.AddError("Reason: Mandatory field Incident Date missing");
            //            break;
            //    }
            //}
            return vr;
        }

        /// <summary>
        /// Check CoOp file has the correct extension
        /// </summary>
        /// <param name="file"></param>
        /// <param name="ctx"></param>
        /// <returns></returns>
        private bool CheckFileExtension(System.IO.Stream file, string expectedFileExtns)
        {
            bool correctExtension = false;

            try
            {
                var fileStream = file as FileStream;

                if (fileStream != null)
                {
                    var clientExtensions = expectedFileExtns;

                    string extension = Path.GetExtension(fileStream.Name);
                    if (extension != null) extension = extension.Replace(".", "").ToLower();

                    string[] clientExts = new string[] { };
                    if (clientExtensions != null) clientExts = clientExtensions.Split(',');

                    if (clientExts.Contains(extension))
                    {
                        correctExtension = true;
                    }
                }
            }

            catch (Exception ex)
            {
                return false;
            }

            return correctExtension;
        }


        /// <summary>
        /// Verify we have the mandatory fields
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        //private int CheckMandatoryFields(System.IO.Stream file)
        //{
        //    // Incident Date and Claim Reference are mandatory fields
        //    int mandatoryFieldCode = 0;

        //    // 0 - No issues
        //    // 1 - Claim Ref Null or Empty
        //    // 2 - Incident Date Null or Empty

        //    StreamReader CoOpFile = new StreamReader(file);
        //    FileHelperEngine  _claimDataEngine = new FileHelperEngine(typeof(ClaimData));
            
        //    var _claimData = _claimDataEngine.ReadStream(CoOpFile) as ClaimData[];

        //    foreach (var claimData in _claimData)
        //    {
        //        if (string.IsNullOrEmpty(claimData.InsurerClientRef))
        //        {
        //            mandatoryFieldCode = 1;
        //            break;
        //        }
                   

        //        if (claimData.AccDate == null)
        //        {
        //            mandatoryFieldCode = 2;
        //            break;
        //        }
                    
        //    }
            
        //    return mandatoryFieldCode;
        //}

        /// <summary>
        ///  // Verify file header has correct number of delimiters
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool CheckFileValidity(System.IO.Stream file)
        {
            bool correctNumberOfDelimeters = true;
            try
            {
                int countDelimiters;
                string line;
                int count = 0;

                var CoOpFile = new System.IO.StreamReader(file);

                while ((line = CoOpFile.ReadLine()) != null)
                {
                    if (count == 1)
                    {
                        break;
                    }

                    countDelimiters = line.Split(',').Length - 1;
                    if (countDelimiters != Convert.ToInt32(ConfigurationManager.AppSettings["CoOpDelimiterAmount"]))
                    {
                        correctNumberOfDelimeters = false;
                        break;
                    }

                    count++;
                }
            }

            catch (Exception ex)
            {
                return false;
            }

            return correctNumberOfDelimeters;
        }
    }
}
