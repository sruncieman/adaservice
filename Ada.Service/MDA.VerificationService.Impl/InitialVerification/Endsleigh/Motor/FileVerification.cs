﻿using MDA.VerificationService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common;
using System.Configuration;
using System.IO;
using Ionic.Zip;

namespace MDA.VerificationService.Endsleigh.Motor
{
    public class FileVerification : IVerificationService
    {
        public Common.ProcessingResults QuickVerification(System.IO.Stream file, Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            var vr = new ProcessingResults("Quick Verify");

            // 1. Check file has the correct extension
            // 2. Check correct number of header columns exist
            // 3. Check mandatory fields exist

            if (!CheckFileExtension(file, expectedFileExtns))
            {
                vr.Message.AddError("Reason: The file type is invalid for this client");
            }
            else if (!CheckFileValidity(file))
            {
                vr.Message.AddError("Reason: The file supplied is invalid, wrong number of delimiters");
            }

            return vr;
        }

        /// <summary>
        /// Check Endsleigh file has the correct extension
        /// </summary>
        /// <param name="file"></param>
        /// <param name="ctx"></param>
        /// <returns></returns>
        private bool CheckFileExtension(System.IO.Stream file, string expectedFileExtns)
        {
            bool correctExtension = false;

            try
            {
                var fileStream = file as FileStream;

                if (fileStream != null)
                {
                    var clientExtensions = expectedFileExtns;

                    string extension = Path.GetExtension(fileStream.Name);
                    if (extension != null) extension = extension.Replace(".", "").ToLower();

                    string[] clientExts = new string[] { };
                    if (clientExtensions != null) clientExts = clientExtensions.Split(',');

                    if (clientExts.Contains(extension))
                    {
                        correctExtension = true;
                    }
                }
            }

            catch (Exception ex)
            {
                return false;
            }

            return correctExtension;
        }

        /// <summary>
        ///  // Verify file header has correct number of delimiters
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool CheckFileValidity(System.IO.Stream file)
        {
            bool correctNumberOfDelimeters = true;
            try
            {
                int countDelimiters;
                string line;
                int count = 0;

                var EndsleighFile = new System.IO.StreamReader(file);

                while ((line = EndsleighFile.ReadLine()) != null)
                {
                    if (count == 1)
                    {
                        break;
                    }

                    countDelimiters = line.Split(',').Length - 1;
                    if (countDelimiters != Convert.ToInt32(ConfigurationManager.AppSettings["EndsleighDelimiterAmount"]))
                    {
                        correctNumberOfDelimeters = false;
                        break;
                    }

                    count++;
                }
            }

            catch (Exception ex)
            {
                return false;
            }

            return correctNumberOfDelimeters;
        }
    }
}
