﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using MDA.Common;
using MDA.VerificationService.Interface;
          
namespace MDA.VerificationService.Haven.Motor
{
    public class FileVerification : IVerificationService
    {
        public Common.ProcessingResults QuickVerification(System.IO.Stream file, Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            var vr = new ProcessingResults("Quick Verify");

            if (!CheckFileExtension(file, expectedFileExtns))
            {
                vr.Message.AddError("Reason: The file type is invalid for this client");
            }
            else if (!CheckFileValidity(file))
            {
                vr.Message.AddError("Reason: The file supplied is invalid, wrong number of delimiters");
            }

            return vr;
        }

        /// <summary>
        ///  // Verify file has correct number of pipe delimiters
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool CheckFileValidity(System.IO.Stream file)
        {
            bool correctNumberOfDelimeters = true;
            try
            {
                int countDelimiters;
                string line;
                int count = 0;

                var havenFile = new System.IO.StreamReader(file);

                while ((line = havenFile.ReadLine()) != null)
                {
                    if (count == Convert.ToInt32(ConfigurationManager.AppSettings["HavenDelimiterLimit"]))
                    {
                        break;
                    }

                    countDelimiters = line.Split('|').Length - 1;
                    if (countDelimiters != Convert.ToInt32(ConfigurationManager.AppSettings["HavenDelimiterAmount"]))
                    {
                        correctNumberOfDelimeters = false;
                        break;
                    }

                    count++;
                }


            }

            catch (Exception ex)
            {
                return false;
            }

            return correctNumberOfDelimeters;

        }

        /// <summary>
        /// Verify File has expected file extension
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool CheckFileExtension(System.IO.Stream file, string expectedFileExtns)
        {
            bool correctExtension = false;

            try
            {
                var fileStream = file as FileStream;

                if (fileStream != null)
                {
                    var clientExtensions = expectedFileExtns;

                    string extension = Path.GetExtension(fileStream.Name);
                    if (extension != null) extension = extension.Replace(".", "").ToLower();

                    string[] clientExts = new string[] { };
                    if (clientExtensions != null) clientExts = clientExtensions.Split(',');

                    if (clientExts.Contains(extension))
                    {
                        correctExtension = true;
                    }
                }
            }

            catch (Exception ex)
            {
                return false;
            }

            return correctExtension;
        }
    }
}
