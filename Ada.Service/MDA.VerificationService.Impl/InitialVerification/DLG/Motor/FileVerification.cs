﻿using MDA.VerificationService.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common;
using System.Configuration;
using System.IO;
using Ionic.Zip;

namespace MDA.VerificationService.DLG.Motor
{
    public class FileVerification : IVerificationService
    {
        private string _tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];
        private string _filePath;
        private string _dlgFileNumber = ConfigurationManager.AppSettings["DLGFileNum"];
        private bool _deleteUploadedFiles = Convert.ToBoolean(ConfigurationManager.AppSettings["DeleteUploadedFiles"]);
        
        public ProcessingResults QuickVerification(System.IO.Stream file, Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            var vr = new ProcessingResults("Quick Verify");

            // 1. Check file is a zip
            // 2. Check zip file has correct number of files
            // 3. Check all files have the correct name and extension

            if (!CheckFileExtension(file, expectedFileExtns))
            {
                vr.Message.AddError("Reason: The file type is invalid for this client");
            }
            else
            {
                switch (CheckExtractedFiles(file, ctx))
                {
                    case 1:
                        vr.Message.AddError("Reason: Wrong number of files included in zip file for this client");
                        break;
                    case 2:
                        vr.Message.AddError("Reason: Error in expected list of files for this client");
                        break;
                }
            }

            return vr;
        }

        private int CheckExtractedFiles(System.IO.Stream file, Common.Server.CurrentContext ctx)
        {
            int checkExtractedFilesCheck = 0;
            int countFiles = 0;
            bool correctFileName = true;
            FileStream targetStream = null;
            string[] expectedFileNames = ConfigurationManager.AppSettings["DLGFiles"].Split(',').Select(s => s.Trim()).ToArray();

            string clientFolder = _tempFolder + "\\" + ctx.RiskClientId;
            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);

            _filePath = string.Format(@"{0}\{1}-{2}{3}", clientFolder, "DLG",
                                             DateTime.Now.ToString("ddMMyyyyHHmmss"), ".zip");

            try
            {
                using (targetStream = new FileStream(_filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    //read from the input stream in 65000 byte chunks
                    const int bufferLen = 65000;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;
                    while ((count = file.Read(buffer, 0, bufferLen)) > 0)
                    {
                        // save to output stream
                        targetStream.Write(buffer, 0, count);
                    }
                    targetStream.Close();
                    file.Close();
                }

                using (ZipFile zip1 = ZipFile.Read(_filePath))
                {
                    foreach (ZipEntry e in zip1)
                    {
                        string unpackDirectory = clientFolder;
                        e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
                        countFiles++;

                        if (!expectedFileNames.Contains(e.FileName))
                        {
                            correctFileName = false;
                            break;
                        }
                    }
                }


                if (!correctFileName)
                {
                    checkExtractedFilesCheck = 2;
                    return checkExtractedFilesCheck;
                }

                if (countFiles != Convert.ToInt32(_dlgFileNumber))
                {
                    checkExtractedFilesCheck = 1;
                    return checkExtractedFilesCheck;
                }

                //File.Delete(filePath);
                DeleteExtractedFiles(clientFolder, _deleteUploadedFiles);
            }

            catch (Exception ex)
            {
                return 0;
            }

            return checkExtractedFilesCheck;
        }

        private void DeleteExtractedFiles(string clientFolder, bool deleteExtractedFiles)
        {
            if (deleteExtractedFiles)
            {
                string[] filePaths = Directory.GetFiles(clientFolder);
                foreach (string file in filePaths)
                {
                    File.Delete(file);
                }
            }
        }

        private bool CheckFileExtension(System.IO.Stream file, string expectedFileExtns)
        {
            bool correctExtension = false;

            try
            {
                var fileStream = file as FileStream;

                if (fileStream != null)
                {
                    var clientExtensions = expectedFileExtns;

                    string extension = Path.GetExtension(fileStream.Name);
                    if (extension != null) extension = extension.Replace(".", "").ToLower();

                    string[] clientExts = new string[] { };
                    if (clientExtensions != null) clientExts = clientExtensions.Split(',');

                    if (clientExts.Contains(extension))
                    {
                        correctExtension = true;
                    }
                }
            }

            catch (Exception ex)
            {
                return false;
            }

            return correctExtension;
        }
    }
}
