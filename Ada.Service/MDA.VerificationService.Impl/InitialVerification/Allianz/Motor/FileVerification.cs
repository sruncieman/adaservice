﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using FileHelpers;
using MDA.Common;
using MDA.VerificationService.Interface;

namespace MDA.VerificationService.Allianz.Motor
{
    public class FileVerification : IVerificationService
    {
        public Common.ProcessingResults QuickVerification(System.IO.Stream file, Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            var vr = new ProcessingResults("Quick Verify");

            // 1. Check file has the correct extension
            // 2. Check correct number of header columns exist

            if (ctx.RiskClientId == 54)
                expectedFileExtns = "csv";

            if (!CheckFileExtension(file, expectedFileExtns))
            {
                vr.Message.AddError("Reason: The file type is invalid for this client");
            }

            if (ctx.RiskClientId != 54)
            {
                if (!CheckFileValidity(file))
                {
                    vr.Message.AddError("Reason: The file supplied is invalid, wrong number of delimiters");
                }
            }

            return vr;
        }

        private bool CheckFileExtension(System.IO.Stream file, string expectedFileExtns)
        {
            bool correctExtension = false;

            try
            {
                var fileStream = file as FileStream;

                if (fileStream != null)
                {
                    var clientExtensions = expectedFileExtns;

                    string extension = Path.GetExtension(fileStream.Name);
                    if (extension != null) extension = extension.Replace(".", "").ToLower();

                    string[] clientExts = new string[] { };
                    if (clientExtensions != null) clientExts = clientExtensions.Split(',');

                    if (clientExts.Contains(extension))
                    {
                        correctExtension = true;
                    }
                }
            }

            catch (Exception ex)
            {
                return false;
            }

            return correctExtension;
        }

        private bool CheckFileValidity(System.IO.Stream file)
        {
            bool correctNumberOfDelimeters = true;
            try
            {
                int countDelimiters;
                string line;
                int count = 0;

                var AllianzFile = new System.IO.StreamReader(file);

                while ((line = AllianzFile.ReadLine()) != null)
                {
                    if (count == 1)
                    {
                        break;
                    }

                    countDelimiters = line.Split(',').Length - 1;
                    if (countDelimiters != Convert.ToInt32(ConfigurationManager.AppSettings["AllianzDelimiterAmount"]))
                    {
                        correctNumberOfDelimeters = false;
                        break;
                    }

                    count++;
                }
            }

            catch (Exception ex)
            {
                return false;
            }

            return correctNumberOfDelimeters;
        }
    }
}
