﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using FileHelpers;
using MDA.Common;
using MDA.VerificationService.Interface;

namespace MDA.VerificationService.Impl.EsureNew.Motor
{
    public class FileVerification : IVerificationService
    {
        public ProcessingResults QuickVerification(Stream file, Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            var vr = new ProcessingResults("Quick Verify");

            // 1. Check file has the correct extension
            // 2. Check correct number of header columns exist

            if (ctx.RiskClientId == 67)
                expectedFileExtns = "csv";

            if (!CheckFileExtension(file, expectedFileExtns))
            {
                vr.Message.AddError("Reason: The file type is invalid for this client");
            }

            return vr;
        }


        private static bool CheckFileExtension(Stream file, string expectedFileExtns)
        {
            var correctExtension = false;

            try
            {
                var fileStream = file as FileStream;

                if (fileStream != null)
                {
                    var clientExtensions = expectedFileExtns;

                    var extension = Path.GetExtension(fileStream.Name);
                    if (extension != null) extension = extension.Replace(".", "").ToLower();

                    var clientExts = new string[] { };
                    if (clientExtensions != null) clientExts = clientExtensions.Split(',');

                    if (clientExts.Contains(extension))
                    {
                        correctExtension = true;
                    }
                }
            }

            catch (Exception)
            {
                return false;
            }

            return correctExtension;
        }

    }
}
