﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Ionic.Zip;
using MDA.Common;
using MDA.VerificationService.Interface;

namespace MDA.VerificationService.CH1.Motor
{
    public class FileVerification : IVerificationService
    {
        private string _tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];
        private string _ch1FileNumber = ConfigurationManager.AppSettings["CH1FileNum"];
        private bool _deleteUploadedFiles = Convert.ToBoolean(ConfigurationManager.AppSettings["DeleteUploadedFiles"]);
        private int _checkFileCount = 0;

        public Common.ProcessingResults QuickVerification(System.IO.Stream file, Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            var vr = new ProcessingResults("Quick Verify");

            // 1. Check file is a zip
            // 2. Check zip file has correct number of core files (2)
           

            if (!CheckFileExtension(file, expectedFileExtns))
            {
                vr.Message.AddError("Reason: The file type is invalid for this client");
            }
            else 
            {
                switch (CheckExtractedFiles(file, ctx))
                {
                    case 1:
                        vr.Message.AddError("Reason: Error in expected list of files for this client");
                        break;
                }
            }

            return vr;
        }

        /// <summary>
        /// Verify correct number of files within zip file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private int CheckExtractedFiles(Stream file, Common.Server.CurrentContext ctx)
        {
            int checkExtractedFilesCheck = 0;
           
            bool correctFileName = true;
            FileStream targetStream = null;
            string[] expectedFileNames = ConfigurationManager.AppSettings["CH1Files"].Split(',').Select(s => s.Trim()).ToArray();

            string clientFolder = _tempFolder + "\\" + ctx.RiskClientId;
            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
            try
            {
                using (ZipFile zip1 = ZipFile.Read(file)) //_filePath))
                {
                    foreach (ZipEntry e in zip1)
                    {
                        var extractTmpPath = string.Concat(clientFolder + @"\" + e.FileName, ".tmp");

                        //check if the temp file already exists 
                        if (File.Exists(extractTmpPath))
                            File.Delete(extractTmpPath);

                        string unpackDirectory = clientFolder;
                        e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
                        
                        string fileName = e.FileName;
                        
                        if (expectedFileNames.Contains(fileName))
                        {
                            _checkFileCount++;
                        }
                    }
                }

                if (_checkFileCount!= Convert.ToInt32(_ch1FileNumber))
                {
                    checkExtractedFilesCheck = 1;
                    return checkExtractedFilesCheck;
                }


                //File.Delete(filePath);
                DeleteExtractedFiles(clientFolder, _deleteUploadedFiles);
            }

            catch (Exception ex)
            {
                return 0;
            }

            return checkExtractedFilesCheck;
        }

        /// <summary>
        /// Verify File has expected file extension
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool CheckFileExtension(System.IO.Stream file, string expectedFileExtns)
        {
            bool correctExtension = false;

            try
            {
                var fileStream = file as FileStream;

                if (fileStream != null)
                {
                    var clientExtensions = expectedFileExtns;

                    string extension = Path.GetExtension(fileStream.Name);
                    if (extension != null) extension = extension.Replace(".", "").ToLower();

                    string[] clientExts = new string[] { };
                    if (clientExtensions != null) clientExts = clientExtensions.Split(',');

                    if (clientExts.Contains(extension))
                    {
                        correctExtension = true;
                    }
                }
            }

            catch (Exception ex)
            {
                return false;
            }

            return correctExtension;
        }

        private static void DeleteExtractedFiles(string clientFolder, bool deleteExtractedFiles)
        {
            if (deleteExtractedFiles)
            {
                    string[] filePaths = Directory.GetFiles(clientFolder);
                    foreach (string file in filePaths)
                    {
                        File.Delete(file);
                    }
            }
        }
    }
}
