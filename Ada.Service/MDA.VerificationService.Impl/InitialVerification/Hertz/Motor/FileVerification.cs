﻿using MDA.Common;
using MDA.VerificationService.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.VerificationService.Hertz.Motor
{
    public class FileVerification : IVerificationService
    {
        public Common.ProcessingResults QuickVerification(System.IO.Stream file, Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            var vr = new ProcessingResults("Quick Verify");

            // 1. Check file is a zip
            // 2. Check zip file has correct number of core files (2)


            if (!CheckFileExtension(file, expectedFileExtns))
            {
                vr.Message.AddError("Reason: The file type is invalid for this client");
            }

            return vr;
        }

        private bool CheckFileExtension(System.IO.Stream file, string expectedFileExtns)
        {
            bool correctExtension = false;

            try
            {
                var fileStream = file as FileStream;

                if (fileStream != null)
                {
                    var clientExtensions = expectedFileExtns;

                    string extension = Path.GetExtension(fileStream.Name);
                    if (extension != null) extension = extension.Replace(".", "").ToLower();

                    string[] clientExts = new string[] { };
                    if (clientExtensions != null) clientExts = clientExtensions.Split(',');

                    if (clientExts.Contains(extension))
                    {
                        correctExtension = true;
                    }
                }
            }

            catch (Exception ex)
            {
                return false;
            }

            return correctExtension;
        }
    }


}
