﻿using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.FileModel;
using MDA.VerificationService.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MDA.VerificationService.Standard.Mobile
{
    /// <summary>
    /// Perform quick validation.  Should be quick enough to allow it to return before web service
    /// times out
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    public class FileVerification : IVerificationService
    {
        public Common.ProcessingResults QuickVerification(System.IO.Stream file, Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            try
            {
                file.Seek(0, SeekOrigin.Begin);

                // Deserialise the file, assuming it is "our" ClaimBatch structure
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(file, new XmlDictionaryReaderQuotas());
                DataContractSerializer ser = new DataContractSerializer(typeof(MobileClaimBatch));

                MobileClaimBatch c = (MDA.Common.FileModel.MobileClaimBatch)ser.ReadObject(reader, true);

                return VerifyClaimBatch(c);
            }
            catch (Exception ex)
            {
                ProcessingResults vr = new ProcessingResults("Quick Verify");

                vr.Message.AddError("Failed to recognise XML file. " + ex.Message);

                return vr;
            }

        }

        private string claimNum = "#NoClaimNum#";

        private ProcessingResults VerifyClaimBatch(MDA.Common.FileModel.MobileClaimBatch c)
        {
            ProcessingResults vr = new ProcessingResults("Quick Verify");

            foreach (MDA.Common.FileModel.MobileClaim claim in c.Claims)
            {
                claimNum = (claim.ClaimNumber == null) ? "#NoClaimNum#" : claim.ClaimNumber;

                MessageNode claimErrorNode = new MessageNode(claimNum);

                ValidateClaim(claim, claimErrorNode);

                if (claim.Policy == null)
                    claimErrorNode.AddWarning(claimNum + " : Policy section missing");
                else
                {
                    MessageNode policyErrorNode = new MessageNode("Policy");

                    ValidatePolicy(claim.Policy, policyErrorNode);

                    if (policyErrorNode.Nodes.Count > 0)
                        claimErrorNode.AddNode(policyErrorNode);
                }

                if (claim.Organisations != null)
                {
                    MessageNode orgsErrorNode = new MessageNode("Organisations");

                    foreach (var organisation in claim.Organisations)
                    {
                        MessageNode orgErrorNode = new MessageNode("Organisation:" + organisation.DisplayText);

                        ValidateClaimOrganisation(organisation, orgErrorNode);

                        if (organisation.Addresses != null)
                        {
                            MessageNode orgAddressesErrorNode = new MessageNode("Addresses");

                            foreach (var address in organisation.Addresses)
                            {
                                MessageNode orgAddrErrorNode = new MessageNode(address.DisplayText);

                                ValidateAddress(address, orgAddrErrorNode);

                                if (orgAddrErrorNode.Nodes.Count > 0)
                                    orgAddressesErrorNode.AddNode(orgAddrErrorNode);
                            }

                            if (orgAddressesErrorNode.Nodes.Count > 0)
                                orgErrorNode.AddNode(orgAddressesErrorNode);
                        }
                        else
                        {
                            orgErrorNode.AddWarning(claimNum + " : Organisation Address not supplied");
                        }

                        if (organisation.OrganisationType == OrganisationType.PolicyHolder)
                        {
                            if (organisation.BankAccount == null && organisation.PaymentCard == null)
                                orgsErrorNode.AddWarning(claimNum + " : Organisation Policyholder has no Bank or Payment card information");
                            else
                            {
                                if (organisation.BankAccount != null)
                                {
                                    MessageNode orgBankAccountErrorNode = new MessageNode("Bank Account");

                                    ValidateBankAccount(organisation.BankAccount, orgBankAccountErrorNode);

                                    if (orgBankAccountErrorNode.Nodes.Count > 0)
                                        orgErrorNode.AddNode(orgBankAccountErrorNode);
                                }

                                if (organisation.PaymentCard != null)
                                {
                                    MessageNode orgPaymentCardErrorNode = new MessageNode("Payment Card");

                                    ValidatePaymentCard(organisation.PaymentCard, orgPaymentCardErrorNode);

                                    if (orgPaymentCardErrorNode.Nodes.Count > 0)
                                        orgErrorNode.AddNode(orgPaymentCardErrorNode);
                                }
                            }
                        }

                        if (orgErrorNode.Nodes.Count > 0)
                            orgsErrorNode.AddNode(orgErrorNode);
                    }

                    if (orgsErrorNode.Nodes.Count > 0)
                        claimErrorNode.AddNode(orgsErrorNode);
                }


                if (claimErrorNode.Nodes.Count > 0)
                    vr.Message.AddNode(claimErrorNode);
            }
            return vr;
        }

        private void ValidateClaim(MDA.Common.FileModel.MobileClaim claim, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(claim.ClaimNumber))
                errorNode.AddError(claimNum + " : Claim Number must be supplied");

            if (claim.ClaimType != ClaimType.MobilePhone)
                errorNode.AddError(claimNum + " : Claim Type must be MobilePhone");

            if (claim.IncidentDate == null)
                errorNode.AddError(claimNum + " : Incident Date must be provided");

            if (claim.MobileClaimInfo == null)
                errorNode.AddWarning(claimNum + " : ClaimInfo not supplied");
        }

        private void ValidatePersonOrganisation(MDA.Common.FileModel.Organisation organisation, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(organisation.OrganisationName))
                errorNode.AddWarning(claimNum + " : Organisation Name not supplied");
        }

        private void ValidateClaimOrganisation(MDA.Common.FileModel.Organisation organisation, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(organisation.OrganisationName))
                errorNode.AddWarning(claimNum + " : Organisation Name not supplied");
        }

        private void ValidateHandset(MDA.Common.FileModel.Handset handset, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(handset.IMEI))
                errorNode.AddWarning(claimNum + " : Handset IMEI  should be supplied");
        }

        private void ValidateBankAccount(MDA.Common.FileModel.BankAccount bankAccount, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(bankAccount.AccountNumber))
                errorNode.AddWarning(claimNum + " : Bank Account Number not supplied");
        }

        private void ValidatePaymentCard(MDA.Common.FileModel.PaymentCard paymentCard, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(paymentCard.PaymentCardNumber))
                errorNode.AddWarning(claimNum + " : Payment Number not supplied");
        }

        private void ValidatePolicy(MDA.Common.FileModel.Policy policy, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(policy.PolicyNumber))
                errorNode.AddWarning(claimNum + " : Policy Number not supplied");
        }

        private void ValidatePerson(MDA.Common.FileModel.Person person, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(person.LastName))
                errorNode.AddWarning(claimNum + " : Person Last Name not supplied");

            if (person.DateOfBirth == null)
                errorNode.AddWarning(claimNum + " : Person Date of Birth not supplied");
        }

        private void ValidateAddress(MDA.Common.FileModel.Address address, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(address.Building) && string.IsNullOrEmpty(address.BuildingNumber))
                errorNode.AddWarning(claimNum + " : Building or Building Number not supplied");

            if (string.IsNullOrEmpty(address.PostCode))
                errorNode.AddWarning(claimNum + " : Postcode not supplied");
        }


        
    }
}
