﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common.FileModel;
using System.Xml;
using System.Runtime.Serialization;
using MDA.VerificationService.Interface;
using System.Net.Mail;
using System.Text.RegularExpressions;
using MDA.Common.Enum;
using MDA.Common;

namespace MDA.VerificationService.Standard.Motor
{
    public class FileVerification : IVerificationService
    {
        /// <summary>
        /// Perform quick validation.  Should be quick enough to allow it to return before web service
        /// times out
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public ProcessingResults QuickVerification(Stream file, MDA.Common.Server.CurrentContext ctx, string expectedFileExtns)
        {
            try
            {
                file.Seek(0, SeekOrigin.Begin);

                // Deserialise the file, assuming it is "our" ClaimBatch structure
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(file, new XmlDictionaryReaderQuotas());
                DataContractSerializer ser = new DataContractSerializer(typeof(MotorClaimBatch));

                MotorClaimBatch c = (MDA.Common.FileModel.MotorClaimBatch)ser.ReadObject(reader, true);

                return VerifyClaimBatch(c);
            }
            catch (Exception ex)
            {
                ProcessingResults vr = new ProcessingResults("Quick Verify");

                vr.Message.AddError("Failed to recognise XML file. " + ex.Message);

                return vr;
            }
        }

        private string claimNum = "#NoClaimNum#";

        private ProcessingResults VerifyClaimBatch(MDA.Common.FileModel.MotorClaimBatch c)
        {
            ProcessingResults vr = new ProcessingResults("Quick Verify");

            foreach (MDA.Common.FileModel.MotorClaim claim in c.Claims)
            {
                claimNum = (claim.ClaimNumber == null) ? "#NoClaimNum#" : claim.ClaimNumber;

                MessageNode claimErrorNode = new MessageNode(claimNum);

                ValidateClaim(claim, claimErrorNode);

                if (claim.Policy == null)
                    claimErrorNode.AddWarning(claimNum + " : Policy section missing");
                else
                {
                    MessageNode policyErrorNode = new MessageNode("Policy");
 
                    ValidatePolicy(claim.Policy, policyErrorNode);

                    if (policyErrorNode.Nodes.Count > 0)
                        claimErrorNode.AddNode(policyErrorNode);
                }

                if (claim.Organisations != null)
                {
                    MessageNode orgsErrorNode = new MessageNode("Organisations");
 
                    foreach (var organisation in claim.Organisations)
                    {
                        MessageNode orgErrorNode = new MessageNode("Organisation:" + organisation.DisplayText);

                        ValidateClaimOrganisation(organisation, orgErrorNode);

                        if (organisation.Addresses != null)
                        {
                            MessageNode orgAddressesErrorNode = new MessageNode("Addresses");

                            foreach (var address in organisation.Addresses)
                            {
                                MessageNode orgAddrErrorNode = new MessageNode(address.DisplayText);

                                ValidateAddress(address, orgAddrErrorNode);

                                if (orgAddrErrorNode.Nodes.Count > 0)
                                    orgAddressesErrorNode.AddNode(orgAddrErrorNode);
                            }

                            if (orgAddressesErrorNode.Nodes.Count > 0)
                                orgErrorNode.AddNode(orgAddressesErrorNode);
                        }
                        else
                        {
                            orgErrorNode.AddWarning(claimNum + " : Organisation Address not supplied");
                        }

                        if (organisation.OrganisationType == OrganisationType.PolicyHolder)
                        {
                            if (organisation.BankAccount == null && organisation.PaymentCard == null)
                                orgsErrorNode.AddWarning(claimNum + " : Organisation Policyholder has no Bank or Payment card information");
                            else
                            {
                                if (organisation.BankAccount != null)
                                {
                                    MessageNode orgBankAccountErrorNode = new MessageNode("Bank Account");

                                    ValidateBankAccount(organisation.BankAccount, orgBankAccountErrorNode);

                                    if (orgBankAccountErrorNode.Nodes.Count > 0)
                                        orgErrorNode.AddNode(orgBankAccountErrorNode);
                                }

                                if (organisation.PaymentCard != null)
                                {
                                    MessageNode orgPaymentCardErrorNode = new MessageNode("Payment Card");

                                    ValidatePaymentCard(organisation.PaymentCard, orgPaymentCardErrorNode);

                                    if (orgPaymentCardErrorNode.Nodes.Count > 0)
                                        orgErrorNode.AddNode(orgPaymentCardErrorNode);
                                }
                            }
                        }

                        if (orgErrorNode.Nodes.Count > 0)
                            orgsErrorNode.AddNode(orgErrorNode);
                    }

                    if (orgsErrorNode.Nodes.Count > 0)
                        claimErrorNode.AddNode(orgsErrorNode);
                }

                int countInsuredVehicles = 0;

                if (claim.Vehicles != null)
                {
                    MessageNode vehiclesErrorNode = new MessageNode("Vehicles");

                    foreach (var vehicle in claim.Vehicles)
                    {
                        MessageNode vehicleErrorNode = new MessageNode("Vehicle:" + vehicle.DisplayText);
 
                        ValidateVehicle(vehicle, vehicleErrorNode);

                        if (vehicle.Incident2VehicleLinkType == Incident2VehicleLinkType.InsuredVehicle)
                            countInsuredVehicles++;

                        if (vehicle.People != null)
                        {
                            MessageNode peopleErrorNode = new MessageNode("People");

                            foreach (var person in vehicle.People)
                            {
                                MessageNode personErrorNode = new MessageNode("Person:" + person.DisplayText);

                                ValidatePerson(person, personErrorNode);

                                if (person.Addresses != null)
                                {
                                    MessageNode perAddressesErrorNode = new MessageNode("Addresses");

                                    foreach (var address in person.Addresses)
                                    {
                                        MessageNode perAddrErrorNode = new MessageNode(address.DisplayText);

                                        ValidateAddress(address, perAddrErrorNode);

                                        if (perAddrErrorNode.Nodes.Count > 0)
                                            perAddressesErrorNode.AddNode(perAddrErrorNode);
                                    }

                                    if (perAddressesErrorNode.Nodes.Count > 0)
                                        personErrorNode.AddNode(perAddressesErrorNode);
                                }

                                if (person.BankAccount != null)
                                {
                                    MessageNode perBankAccountErrorNode = new MessageNode("Bank Account");

                                    ValidateBankAccount(person.BankAccount, perBankAccountErrorNode);

                                    if (perBankAccountErrorNode.Nodes.Count > 0)
                                        personErrorNode.AddNode(perBankAccountErrorNode);
                                }

                                if (person.PaymentCard != null)
                                {
                                    MessageNode perPaymentCardErrorNode = new MessageNode("Payment Card");

                                    ValidatePaymentCard(person.PaymentCard, perPaymentCardErrorNode);

                                    if (perPaymentCardErrorNode.Nodes.Count > 0)
                                        personErrorNode.AddNode(perPaymentCardErrorNode);
                                }

                                if (person.Organisations != null)
                                {
                                    MessageNode perOrgsErrorNode = new MessageNode("Organisations");

                                    foreach (var organisation in person.Organisations)
                                    {
                                        MessageNode orgErrorNode = new MessageNode("Organisation:" + organisation.DisplayText);

                                        ValidatePersonOrganisation(organisation, orgErrorNode);

                                        if (organisation.Addresses != null)
                                        {
                                            MessageNode orgAddressesErrorNode = new MessageNode("Addresses");
                                            
                                            foreach (var address in organisation.Addresses)
                                            {
                                                MessageNode orgAddrErrorNode = new MessageNode(address.DisplayText);

                                                ValidateAddress(address, orgAddrErrorNode);

                                                if (orgAddrErrorNode.Nodes.Count > 0)
                                                    orgAddressesErrorNode.AddNode(orgAddrErrorNode);
                                            }

                                            if (orgAddressesErrorNode.Nodes.Count > 0)
                                                orgErrorNode.AddNode(orgAddressesErrorNode);
                                        }
                                        else
                                            orgErrorNode.AddWarning(claimNum + " : Organisation Address not supplied");
                                    }
                                }

                                if (personErrorNode.Nodes.Count > 0)
                                    peopleErrorNode.AddNode(personErrorNode);
                            }

                            if (peopleErrorNode.Nodes.Count > 0)
                                vehicleErrorNode.AddNode(peopleErrorNode);
                        }

                        if (vehicleErrorNode.Nodes.Count > 0)
                            vehiclesErrorNode.AddNode(vehicleErrorNode);
                    }

                    if (vehiclesErrorNode.Nodes.Count > 0)
                        claimErrorNode.AddNode(vehiclesErrorNode);
                }
                else
                    claimErrorNode.AddError(claimNum + " : No vehicles specified");

                if (countInsuredVehicles > 1)
                    claimErrorNode.AddError(claimNum + " : More than 1 Insured Vehicles specified");

                if ( claimErrorNode.Nodes.Count > 0 )
                    vr.Message.AddNode(claimErrorNode);
            }
            return vr;
        }

        private void ValidateClaim(MDA.Common.FileModel.MotorClaim claim, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(claim.ClaimNumber))
                errorNode.AddError(claimNum + " : Claim Number must be supplied");

            if (claim.ClaimType != ClaimType.Motor)
                errorNode.AddError(claimNum + " : Claim Type must be MOTOR");

            if (claim.IncidentDate == null)
                errorNode.AddError(claimNum + " : Incident Date must be provided");

            if (claim.MotorClaimInfo == null)
                errorNode.AddWarning(claimNum + " : MotorClaimInfo not supplied");
        }

        private void ValidatePersonOrganisation(MDA.Common.FileModel.Organisation organisation, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(organisation.OrganisationName))
                errorNode.AddWarning(claimNum + " : Organisation Name not supplied");

            //??if (organisation.ClaimInfo == null)
            //??    errorNode.AddWarning(claimNum + " : Organisation ClaimInfo not supplied");
        }

        private void ValidateClaimOrganisation(MDA.Common.FileModel.Organisation organisation, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(organisation.OrganisationName))
                errorNode.AddWarning(claimNum + " : Organisation Name not supplied");

            //??if (organisation.ClaimInfo == null)
            //??    errorNode.AddWarning(claimNum + " : Organisation ClaimInfo not supplied");
        }

        private void ValidateVehicle(MDA.Common.FileModel.Vehicle vehicle, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(vehicle.VIN) && string.IsNullOrEmpty(vehicle.VehicleRegistration))
                errorNode.AddWarning(claimNum + " : Either Vehicle VIN or Registration Number should be supplied");
        }

        private void ValidateBankAccount(MDA.Common.FileModel.BankAccount bankAccount, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(bankAccount.AccountNumber))
                errorNode.AddWarning(claimNum + " : Bank Account Number not supplied");
        }

        private void ValidatePaymentCard(MDA.Common.FileModel.PaymentCard paymentCard, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(paymentCard.PaymentCardNumber))
                errorNode.AddWarning(claimNum + " : Payment Number not supplied");
        }

        private void ValidatePolicy(MDA.Common.FileModel.Policy policy, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(policy.PolicyNumber))
                errorNode.AddWarning(claimNum + " : Policy Number not supplied");
        }

        private void ValidatePerson(MDA.Common.FileModel.Person person, MessageNode errorNode)
        {
            //??if (person.ClaimInfo == null)
            //??    errorNode.AddWarning(claimNum + " : Person Claim Info not supplied");

            // if (string.IsNullOrEmpty(person.FirstName))
            //     warnings.Add("Person First Name v supplied");

            if (string.IsNullOrEmpty(person.LastName))
                errorNode.AddWarning(claimNum + " : Person Last Name not supplied");

            if (person.DateOfBirth == null)
                errorNode.AddWarning(claimNum + " : Person Date of Birth not supplied");
        }

        private void ValidateAddress(MDA.Common.FileModel.Address address, MessageNode errorNode)
        {
            if (string.IsNullOrEmpty(address.Building) && string.IsNullOrEmpty(address.BuildingNumber))
                errorNode.AddWarning(claimNum + " : Building or Building Number not supplied");

            if (string.IsNullOrEmpty(address.PostCode))
                errorNode.AddWarning(claimNum + " : Postcode not supplied");
        }
    }
   
}
