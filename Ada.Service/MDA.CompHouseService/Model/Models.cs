﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.CompHouseService.Model
{
    /// <summary>
    ///  define any classes used to pass data into or out of the service
    /// </summary>
    public class Appointment
    {
        public string CompanyNumber { get; set; }
        public string AppintmentType { get; set; }
        public string PersonNumber { get; set; }
        public DateTime AppointmentDate { get; set; }
        public DateTime ResignationDate { get; set; }
        public string Postcode { get; set; }
        public DateTime DOB { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string SubBuilding { get; set; }
        public string Building { get; set; }
        public string PropertyNum { get; set; }
        public string Street { get; set; }
        public string Locality { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Occupation { get; set; }
        public string Nationality { get; set; }
    }

    public class Appointments
    {
        public List<Appointment> ListAppointments { get; set; } 
    }
}
