﻿using System;
using System.Threading;
using MDA.Common.Server;
using System.Data.Entity.Validation;
using System.Reflection;

namespace MDA.Pipeline.ConsoleService
{
    class Program
    {
        private static bool needDate = false;

        private static ServiceConfigSettings configSettings = new ServiceConfigSettings();

        private static int ShowProgress(ProgressSeverity severity, string message, bool eol = true)
        {
            Console.Write("T[" + Thread.CurrentThread.ManagedThreadId.ToString("D2") + "] ");

            if (needDate)
                Console.Write(DateTime.Now.ToString("dd/MM HH:mm:ss.fff") + ": ");

            if (eol)
                Console.WriteLine(message);
            else
                Console.Write(message);

            if(severity == ProgressSeverity.Error || severity == ProgressSeverity.Fatal)
                MDA.Pipeline.Service.PipelineEngine.SendEmail(severity, message);

            needDate = eol;

            return 0;
        }

        private static void TraceException(Exception ex)
        {
            if (ex is DbEntityValidationException)
            {
                var dbEx = ex as DbEntityValidationException;

                Console.WriteLine("EXCEPTION " + dbEx.Message);

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Console.WriteLine(string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }

                Exception e = ex.InnerException;

                while (e != null)
                {
                    Console.WriteLine("INNER EXCEPTION " + e.Message);
                    e = e.InnerException;
                }
            }
            else
            {
                Console.WriteLine("EXCEPTION " + ex.Message);

                Exception e = ex.InnerException;

                while (e != null)
                {
                    Console.WriteLine("INNER EXCEPTION " + e.Message);
                    e = e.InnerException;
                }
            }
        }

        private static void MonitorFolder(object data)
        {
            int disableMonitoringCount = 0;
            while (true)
            {
                if (configSettings.EnableMonitoring)
                {
                    while(configSettings.EnableMonitoring == true) 
                    {
                        if (!MDA.Pipeline.Service.PipelineEngine.PerformFolderMonitoringCheck(configSettings, ShowProgress))
                        {
                            disableMonitoringCount++;
                            ShowProgress(ProgressSeverity.Error, "Checking Monitored Folders FAILED attempt " + disableMonitoringCount , true);

                            Thread.Sleep(configSettings.MonitorErrorRetrySleepTime);
                        }
                        else
                        {
                            // Reset count
                            disableMonitoringCount = 0;
                        }

                        if (disableMonitoringCount == 10)
                        {
                            ShowProgress(ProgressSeverity.Error, "Checking Monitored Folders FAILED after 10 retries. Disabling service thread", true);
                            configSettings.EnableMonitoring = false;
                            return;
                        }

                        Thread.Sleep(configSettings.MonitorSleepTime);
                    }
                }                
            }
        }

        private static void ProcessBatchQueue(object data)
        {
            while (true)
            {
                try
                {
                    if (configSettings.ProcessBatches)
                    {
                        using (var ctx = new CurrentContext(0, 0, "System", ShowProgress))
                        {
                            Pipeline.Stage2_ProcessBatchQueue(ctx, configSettings, null);

                            Thread.Sleep(configSettings.ServiceSleepTime);
                        }
                    }
                    else
                    {
                        ShowProgress(ProgressSeverity.Info, "Batch loading disabled", true);
                        break;
                    }
                }
                catch (Exception ex)
                {
                    // 
                }
            }
        }

        static void Main(string[] args)
        {

            

            ShowProgress(ProgressSeverity.Info, "ADA CONSOLE v" + Assembly.GetExecutingAssembly().GetName().Version.ToString() + ": Claim Processing Starting...");

            // MDA.Pipeline.Service.PipelineEngine.RecoverPipeline(configSettings, ShowProgress);

            Thread t2 = new Thread(new ParameterizedThreadStart(MonitorFolder));
            t2.Start(null);

            Thread t3 = new Thread(new ParameterizedThreadStart(ProcessBatchQueue));
            t3.Start(null);

            while (true)
            {
                try
                {
                    MDA.Pipeline.Service.PipelineEngine.RunPipeline(configSettings, ShowProgress);

                    bool justADot = false;

                    while (MDA.Pipeline.Service.PipelineEngine.NothingToDo(configSettings, ShowProgress))
                    {
                        int timeInSec = configSettings.ServiceSleepTime / 1000;

                        ShowProgress(ProgressSeverity.JustConsole, (justADot) ? "." : "Thread Sleeping (" + timeInSec + " second intervals)", !justADot);

                        justADot = true;

                        Thread.Sleep(configSettings.ServiceSleepTime);
                    }
                }
                catch (Exception ex)
                {
                    TraceException(ex);

                    ShowProgress(ProgressSeverity.Error, "Exception: Pause, Sleeping 60 seconds", true);

                    Thread.Sleep(60 * 1000);

                    MDA.Pipeline.Service.PipelineEngine.RecoverPipeline(configSettings, ShowProgress);
                }
            }
#if false
            //bool ProcessBatches  = Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessBatches"]);
            //bool ProcessLoading  = Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessLoading"]);
            //bool Process3rdParty = Convert.ToBoolean(ConfigurationManager.AppSettings["Process3rdParty"]);
            //bool ProcessScoring  = Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessScoring"]);
            //bool EnableSync      = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSync"]);
            //bool EnableDeDupe    = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableDeDupe"]);
            //bool ReqBatchReports = Convert.ToBoolean(ConfigurationManager.AppSettings["RequestBatchReports"]);
 
            //int numberThreads    = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfScoringThreads"]);
            //int ServiceSleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["ServiceSleepTimeInMs"]);
            //int ThreadSleepTime  = Convert.ToInt32(ConfigurationManager.AppSettings["ThreadSleepTimeInMs"]);

            try
            {
                using (CurrentContext ctx = new CurrentContext(0, 0, "System"))
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    //int maxBatchesToProcess = 1;
                    //bool scoreWithLiveRules = false;

                    //MDA.Pipeline.Pipeline.Deduplicate(ctx, ShowProgress);

                    //int? batchId = null;

                    ShowProgress("----Recovery Stage.");

                    ShowProgress("Recovery: Check for scoring (1)");
                    if (config.ProcessScoring)
                        MDA.Pipeline.Pipeline.ScoreClaimQueue(ctx, config, null, ShowProgress);
                    else
                        ShowProgress("Recovery: Scoring disabled");

                    ShowProgress("Recovery: Check for 3rd party calls");
                    if (config.Process3rdParty)
                        MDA.Pipeline.Pipeline.ProcessClaimQueueThirdParties(ctx, config, null, ShowProgress);
                    else
                        ShowProgress("Recovery: 3rd party disabled");

                    ShowProgress("Recovery: Check for scoring (2)");
                    if (config.ProcessScoring)
                        MDA.Pipeline.Pipeline.ScoreClaimQueue(ctx, config, null, ShowProgress);
                    else
                        ShowProgress("Recovery: Scoring disabled");

                    MDA.Pipeline.Pipeline.CleanUpPartiallyCreatedBatchClaims(ctx);

                    MDA.Pipeline.Pipeline.RecoverBatchesWithClaimsBeingLoaded(ctx, ShowProgress);

                    List<RiskBatch> batches = riskServices.GetListOfBatchesWithClaimsBeingLoaded();

                    foreach (var riskBatch in batches)
                    {
                        ShowProgress("Recovery: Check for Batches to load");
                        if (config.ProcessLoading)
                            MDA.Pipeline.Pipeline.LoadBatchClaimsIntoDatabase(ctx, config, riskBatch.Id, ShowProgress);
                        else
                            ShowProgress("Recovery: Claims loading disabled");

                        ShowProgress("Recovery: Check for 3rd party calls");
                        if (config.Process3rdParty)
                            MDA.Pipeline.Pipeline.ProcessClaimQueueThirdParties(ctx, config, riskBatch.Id, ShowProgress);
                        else
                            ShowProgress("Recovery: 3rd party disabled");

                        ShowProgress("Recovery: Check for scoring");
                        if (config.ProcessScoring)
                            MDA.Pipeline.Pipeline.ScoreClaimQueue(ctx, config, riskBatch.Id, ShowProgress);
                        else
                            ShowProgress("Recovery: Scoring disabled");
                    }

                    #region SYNC and DEDUPE
                    if (config.EnableSync)
                    {
                        ShowProgress("Sync with IBASE : Starting");

                        try
                        {
                            MDA.Pipeline.Pipeline.Synchronise(ctx, config, true, ShowProgress);

                            ShowProgress("Sync with IBASE : Complete");
                        }
                        catch(Exception ex)
                        {
                            TraceException(ex);
                            ShowProgress("Sync with IBASE : Exception : " + ex.Message);
                        }

                        ShowProgress("Sync with IBASE : Complete");
                    }
                    else
                        ShowProgress("Sync with IBASE : Disabled");


                    if (config.EnableDedupe)
                    {
                        ShowProgress("De-Duplicate ADA : Starting");

                        MDA.Pipeline.Pipeline.Deduplicate(ctx, config, ShowProgress);

                        ShowProgress("De-Duplicate ADA : Complete");
                    }
                    else
                        ShowProgress("De-Duplicate ADA : Disabled");

                    if (config.EnableSync)
                    {
                        ShowProgress("Sync with IBASE : Starting");

                        try
                        {
                            MDA.Pipeline.Pipeline.Synchronise(ctx, config, false, ShowProgress);

                            ShowProgress("Sync with IBASE : Complete");
                        }
                        catch(Exception ex)
                        {
                            TraceException(ex);
                            ShowProgress("Sync with IBASE : Exception : " + ex.Message);
                        }

                        ShowProgress("Sync with IBASE : Complete");
                    }
                    else
                        ShowProgress("Sync with IBASE : Disabled");
                    #endregion

                    //MDA.Pipeline.Pipeline.RecoverBatchesWithClaimsBeingLoaded(ShowProgress, _trace);

                    ShowProgress("Initial Full Backup (if required)");
                    if(config.EnableBackups)
                        MDA.Pipeline.Pipeline.PerformInitialFullBackup(config);
                    ShowProgress("Initial Full Backup Complete");

                    lastOvernightSync = DateTime.Now;
                }
            }
            catch(Exception ex)
            {
                while (ex != null)
                {
                    ShowProgress("Exception: " + ex.Message);
                    ex = ex.InnerException;
                }
                Console.WriteLine("Hit a key....");
                Console.ReadKey();
            }

            while (true)
            {
                using (CurrentContext ctx = new CurrentContext(0, 0, "System"))
                {
                    IRiskServices riskServices = new RiskServices(ctx);

                    try
                    {
                        int? batchId = null;

                        ShowProgress("----Normal processing");

                        #region Batches

                        ShowProgress("Check for new batches");
                        if (config.ProcessBatches)
                            batchId = MDA.Pipeline.Pipeline.ProcessBatchQueue(ctx, config, null, ShowProgress);
                        else
                            ShowProgress("Batch loading disabled");

                        #endregion

                        #region Loading

                        ShowProgress("Check Claims to processs");
                        if (config.ProcessLoading)
                            MDA.Pipeline.Pipeline.LoadBatchClaimsIntoDatabase(ctx, config, batchId, ShowProgress);
                        else
                            ShowProgress("Claims loading disabled");

                        #endregion

                        #region 3rd party

                        ShowProgress("Check for 3rd party calls");
                        if (config.Process3rdParty)
                            MDA.Pipeline.Pipeline.ProcessClaimQueueThirdParties(ctx, config, batchId, ShowProgress);
                        else
                            ShowProgress("3rd party disabled");

                        #endregion

                        #region Scoring

                        ShowProgress("Check for scoring");
                        if (config.ProcessScoring)
                            MDA.Pipeline.Pipeline.ScoreClaimQueue(ctx, config, batchId, ShowProgress);
                        else
                            ShowProgress("Scoring disabled");

                        #endregion

                        #region Redo external calls when one comes online then rescore

                        ShowProgress("Check for 3rd party retry calls");
                        if (config.Process3rdParty)
                            MDA.Pipeline.Pipeline.ReProcessClaimQueueThirdParties(ctx, config, ShowProgress);
                        else
                            ShowProgress("3rd party disabled");

                        ShowProgress("Check for re-scoring");
                        if (config.ProcessScoring)
                            MDA.Pipeline.Pipeline.ScoreClaimQueue(ctx, config, batchId, ShowProgress);
                        else
                            ShowProgress("Scoring disabled");

                        #endregion

                        if (config.ReqBatchReports)
                        {
                            ShowProgress("Requesting Batch Report");
                            if (batchId != null)
                                RequestbatchReport((int) batchId);
                            ShowProgress("Batch Report Complete");
                        }
                        else
                        {
                            ShowProgress("Req Batch Report Disabled (RequestBatchReports=false)");
                        }

                        #region SYNC and DEDUPE
                        int lastSync;

                        if (config.EnableSync)
                        {
                            ShowProgress("Sync with IBASE : Starting");

                            try
                            {
                                lastSync = MDA.Pipeline.Pipeline.Synchronise(ctx, config, true, ShowProgress);

                                ShowProgress("Sync with IBASE : Complete");
                            }
                            catch(Exception ex)
                            {
                                TraceException(ex);
                                ShowProgress("Sync with IBASE : Exception : " + ex.Message);
                            }

                            ShowProgress("Sync with IBASE : Complete");
                        }
                        else
                            ShowProgress("Sync with IBASE : Disabled");


                        if (config.EnableDedupe)
                        {
                            ShowProgress("De-Duplicate ADA : Starting");

                            MDA.Pipeline.Pipeline.Deduplicate(ctx, config, ShowProgress);

                            ShowProgress("De-Duplicate ADA : Complete");
                        }
                        else
                            ShowProgress("De-Duplicate ADA : Disabled");

                        if (config.EnableSync)
                        {
                            ShowProgress("Sync with IBASE : Starting");

                            try
                            {
                                lastSync = MDA.Pipeline.Pipeline.Synchronise(ctx, config, false, ShowProgress);

                                ShowProgress("Sync with IBASE : Complete");
                            }
                            catch(Exception ex)
                            {
                                TraceException(ex);
                                ShowProgress("Sync with IBASE : Exception : " + ex.Message);
                            }

                            ShowProgress("Sync with IBASE : Complete");
                        }
                        else
                            ShowProgress("Sync with IBASE : Disabled");
                        #endregion

                        ShowProgress(": ----Completed");

                        bool justADot = false;

                        while ((!config.ProcessLoading || !riskServices.IsAClaimToProcess()) &&
                               (!config.ProcessScoring || !riskServices.IsAClaimToScore()) &&
                               (!config.Process3rdParty || !riskServices.IsAClaimToProcessThirdParties()) &&
                               (!config.ProcessBatches || !riskServices.IsABatchToProcess()))
                        {
                            int timeInSec = config.ServiceSleepTime/1000;

                            if (DateTime.Now.Hour > 3 && DateTime.Today > lastOvernightSync.Date)
                            {
                                #region SYNC and DEDUPE
                                ShowProgress("");
                                ShowProgress("Detected overnight processing (sync, dedupe, backup)");

                                if (config.EnableSync)
                                {
                                    ShowProgress("Sync with IBASE : Starting");

                                    try
                                    {
                                        lastSync = MDA.Pipeline.Pipeline.Synchronise(ctx, config, true, ShowProgress);

                                        ShowProgress("Sync with IBASE : Complete");
                                    }
                                    catch(Exception ex)
                                    {
                                        TraceException(ex);
                                        ShowProgress("Sync with IBASE : Exception : " + ex.Message);
                                    }
                                    
                                }
                                else
                                    ShowProgress("Sync with IBASE : Disabled");


                                if (config.EnableDedupe)
                                {
                                    ShowProgress("De-Duplicate ADA : Starting");

                                    MDA.Pipeline.Pipeline.Deduplicate(ctx, config, ShowProgress);

                                    ShowProgress("De-Duplicate ADA : Complete");
                                }
                                else
                                    ShowProgress("De-Duplicate ADA : Disabled");

                                if (config.EnableSync)
                                {
                                    ShowProgress("Sync with IBASE : Starting");

                                    try
                                    {
                                        lastSync = MDA.Pipeline.Pipeline.Synchronise(ctx, config, false, ShowProgress);

                                        ShowProgress("Sync with IBASE : Complete");
                                    }
                                    catch(Exception ex)
                                    {
                                        TraceException(ex);
                                        ShowProgress("Sync with IBASE : Exception : " + ex.Message);
                                    }

                                    ShowProgress("Sync with IBASE : Complete");
                                }
                                else
                                    ShowProgress("Sync with IBASE : Disabled");
                                #endregion

                                lastOvernightSync = DateTime.Today;
                            }

                            ShowProgress((justADot) ? "." : "Sleeping (" + timeInSec + " second intervals)", !justADot);

                            justADot = true;

                            Thread.Sleep(config.ServiceSleepTime);
                        }

                        ShowProgress("Detected work to do...");
                    }
                    catch (Exception ex)
                    {
                        TraceException(ex);

                        ShowProgress("----Sleeping 60 seconds)");

                        Thread.Sleep(60);
                    }
                } // context
            } // forever
#endif
        }

       

    }
}
