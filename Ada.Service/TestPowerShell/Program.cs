﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Collections.ObjectModel;
using System.Management.Automation.Runspaces;
using System.Security;

namespace TestPowerShell
{
    class Program
    {
        static void Main(string[] args)
        {
            string pwd = "M3rid1an13";
            //string pwd = "M3rid1an18";
            SecureString password = new SecureString();
            foreach(char c in pwd.ToCharArray()){
                password.AppendChar(c);
            }


            PSCredential psc = new PSCredential(@"foxtest.prog\jonathanw-admin",password);
            //PSCredential psc = new PSCredential(@"knet\jonathanwalker", password);
            WSManConnectionInfo connectionInfo = new WSManConnectionInfo(null,null, psc);
            //connectionInfo.AuthenticationMechanism = AuthenticationMechanism.Kerberos;

            connectionInfo.OperationTimeout = 4 * 60 * 1000; // 4 minutes.
            connectionInfo.OpenTimeout = 1 * 60 * 1000; // 1 minute.

            //connectionInfo.ComputerName = "INTELPROD-DB1";
            connectionInfo.ComputerName = "INTELTST-POC3.FOXTEST.PROG";
            



            // Create a remote runspace using the connection information.
            //using (Runspace remoteRunspace = RunspaceFactory.CreateRunspace())
            using (Runspace remoteRunspace = RunspaceFactory.CreateRunspace(connectionInfo))
            {
                // Establish the connection by calling the Open() method to open the runspace. 
                // The OpenTimeout value set previously will be applied while establishing 
                // the connection. Establishing a remote connection involves sending and 
                // receiving some data, so the OperationTimeout will also play a role in this process.
                remoteRunspace.Open();


                using (PowerShell ps = PowerShell.Create())
                {
                    ps.Runspace = remoteRunspace;

                    ps.AddCommand("Get-Process");

                    ps.Invoke();

                    Collection<PSObject> results = ps.Invoke();

                    Console.WriteLine("Process              PID");
                    Console.WriteLine("--------------------------------");

                    // Display the results.
                    foreach (PSObject result in results)
                    {
                        Console.WriteLine(
                                          "{0,-20} {1}",
                                          result.Members["ProcessName"].Value,
                                          result.Members["Id"].Value);
                    }
                }
                // Close the connection. Call the Close() method to close the remote 
                // runspace. The Dispose() method (called by using primitive) will call 
                // the Close() method if it is not already called.
                remoteRunspace.Close();

            }
        }
    }
}
