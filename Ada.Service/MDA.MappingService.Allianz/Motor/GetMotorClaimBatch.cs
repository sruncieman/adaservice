﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FileHelpers;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.Common.Helpers;
using MDA.MappingService.Allianz.Model;
using MDA.Common.Server;
using MDA.Common;
using System.Globalization;

namespace MDA.MappingService.Allianz.Motor
{
    public class GetMotorClaimBatch
    {

        private string _claimDataFile;
        private FileHelperEngine _claimDataEngine;
        private ClaimData[] _claimData;
        private StreamReader _claimDataFileStreamReader;

        public void RetrieveMotorClaimBatch(CurrentContext ctx, Stream filem, string clientFolder, object statusTracking, 
            Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn, 
            ProcessingResults processingResults)
        {
            #region Files

            if (filem == null)
            {
                _claimDataFile = @"D:\Dev\Projects\MDASolution\MDA.Mapping.Allianz\Resource\COA Output - 23-6-14.csv";
            }
            else
            {
                try
                {
                    _claimDataFileStreamReader = new StreamReader(filem);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in creating csv file: " + ex);
                }

            }
            #endregion

            #region Initialise FileHelper Engine
            try
            {
                _claimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
            #endregion

            #region Populate FileHelper Engine with data from file

            try
            {
                if (filem == null)
                {
                    _claimData = _claimDataEngine.ReadFile(_claimDataFile) as ClaimData[];
                }
                else
                {
                    _claimData = _claimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in reading csv file via filehelper engine: " + ex);
            }

            #endregion

            TranslateToXml(ctx, _claimData, statusTracking, ProcessClaimFn, processingResults);

        }

        private void TranslateToXml(CurrentContext ctx, ClaimData[] claimData, object statusTracking, 
            Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn, ProcessingResults processingResults)
        {
            List<String> processedClaims = new List<String>(); //Create list of proccessed claims to ensure same claim not translated twice

            foreach (var claim in claimData)
            {

                if (!processedClaims.Contains(claim.ClaimNumber.ToUpper()))
                {
                    ClaimData[] uniqueCliams = (from x in claimData     // Find all rows relating to one claim, claim number may appear on several rows linking parties to claim
                                                where x.ClaimNumber.ToUpper() == claim.ClaimNumber.ToUpper()
                                                select x).ToArray();

                    PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                    #region Claim
                    motorClaim.ClaimNumber = claim.ClaimNumber.ToUpper();
                    motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                    if (!string.IsNullOrWhiteSpace(claim.IncidentDate))
                    {
                        DateTime incidentDate = DateTime.ParseExact(claim.IncidentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                        motorClaim.IncidentDate = incidentDate;
                    }
                    #endregion

                    #region Claim Info
                    motorClaim.ExtraClaimInfo.ClaimCode = claim.Branch;
                    DateTime claimNotificationDate = DateTime.ParseExact(claim.AcceptedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                    motorClaim.ExtraClaimInfo.ClaimNotificationDate = claimNotificationDate;
                    #endregion

                    #region Policy
                    motorClaim.Policy.Insurer = "Allianz";
                    motorClaim.Policy.PolicyNumber = claim.PolicyNumber;
                    #endregion

                    #region Organisation - Policyholder

                    foreach (var o in uniqueCliams)
                    {
                        if (!string.IsNullOrWhiteSpace(o.PolicyHolder))
                        {
                            PipelineOrganisation organisation = new PipelineOrganisation();
                            organisation.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                            organisation.OrganisationType_Id = (int)OrganisationType.PolicyHolder;

                            organisation.OrganisationName = o.PolicyHolder;

                            motorClaim.Organisations.Add(organisation);
                        }
                    }

                    #endregion

                    #region Insured Vehicle

                    var uniqueInsuredVehicle = (from x in uniqueCliams  // Insured Vehicle reg can appear against multiple rows, get distinct values
                                                where !string.IsNullOrWhiteSpace(x.InsuredVehicleRegistration)
                                                select x.InsuredVehicleRegistration).Distinct().FirstOrDefault();

                    if (uniqueInsuredVehicle != null)
                    {
                        PipelineVehicle insuredVehicle = new PipelineVehicle();
                        insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                        insuredVehicle.VehicleRegistration = uniqueInsuredVehicle;
                        motorClaim.Vehicles.Add(insuredVehicle);

                        insuredVehicle.VehicleType_Id = (int)VehicleType.Car;

                        #region Insured Driver

                        var uniqueInsuredDriver = (from x in uniqueCliams  // Insured Driver can appear against multiple rows, get distinct values
                                                   where x.InsuredDriver != x.PolicyHolder
                                                   && !string.IsNullOrWhiteSpace(x.InsuredDriver)
                                                   select x.InsuredDriver).Distinct().FirstOrDefault();

                        PipelinePerson insuredDriver = new PipelinePerson();
                        insuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        insuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        if (uniqueInsuredDriver != null)
                        {

                            var insuredDriverName = uniqueInsuredDriver.Split();

                            if (insuredDriverName.Count() == 3)
                            {
                                insuredDriver.FirstName = insuredDriverName[0];
                                insuredDriver.MiddleName = insuredDriverName[1];
                                insuredDriver.LastName = insuredDriverName[2];
                            }
                            else if (insuredDriverName.Count() == 2)
                            {
                                insuredDriver.FirstName = insuredDriverName[0];
                                insuredDriver.LastName = insuredDriverName[1];
                            }
                            else if (insuredDriverName.Count() == 1)
                            {
                                insuredDriver.LastName = insuredDriverName[0];
                            }
                            else
                            {
                                insuredDriver.LastName = uniqueInsuredDriver;
                            }
                        }

                        insuredVehicle.People.Add(insuredDriver);

                        #endregion

                        #region Insured Passenger

                        var insuredPassengers = (from x in uniqueCliams
                                                where x.ClaimantInvolvement == " A Passenger in or on a vehicle owned by someone else"
                                                && x.ClaimantVehicleRegistion == uniqueInsuredVehicle
                                                //&& !string.IsNullOrEmpty(x.ClaimantFirstName) || !string.IsNullOrEmpty(x.ClaimantLastName)
                                                select x).Distinct();

                        foreach (var iP in insuredPassengers)
                        {
                            PipelinePerson insuredPassenger = new PipelinePerson();
                            insuredPassenger.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            insuredPassenger.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;

                            insuredPassenger.FirstName = iP.ClaimantFirstName;
                            insuredPassenger.LastName = iP.ClaimantLastName;

                            if (!string.IsNullOrWhiteSpace(iP.ClaimantDoB))
                            {
                                DateTime insuredPassengerDoB = DateTime.ParseExact(iP.ClaimantDoB, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                                insuredPassenger.DateOfBirth = insuredPassengerDoB;
                            }

                            if(!string.IsNullOrWhiteSpace(iP.NINumber))
                            {
                                PipelineNINumber NINumber = new PipelineNINumber();
                                NINumber.NINumber1 = iP.NINumber;
                                insuredPassenger.NINumbers.Add(NINumber);
                            }

                            if (!string.IsNullOrWhiteSpace(iP.ClaimantPhoneNumber))
                            {
                                PipelineTelephone telephoneNumber = new PipelineTelephone();
                                telephoneNumber.ClientSuppliedNumber = iP.ClaimantPhoneNumber;
                                insuredPassenger.Telephones.Add(telephoneNumber);
                            }

                            if (!string.IsNullOrWhiteSpace(iP.ClaimantEmail))
                            {
                                PipelineEmail emailAddress = new PipelineEmail();
                                emailAddress.EmailAddress = iP.ClaimantEmail;
                                insuredPassenger.EmailAddresses.Add(emailAddress);
                            }

                            if (!string.IsNullOrWhiteSpace(iP.ClaimantAddress))
                            {
                                PipelineAddress insuredPassengerAddress = new PipelineAddress();
                                
                                var address = iP.ClaimantAddress.Split(',');


                                if (address.Count() == 1) 
                                {
                                    insuredPassengerAddress.BuildingNumber = address[0];
                                }
                                else if (address.Count() == 2)
                                {
                                    insuredPassengerAddress.BuildingNumber = address[0];
                                    insuredPassengerAddress.Street = address[1];
                                }
                                else if (address.Count() == 3)
                                {
                                    insuredPassengerAddress.BuildingNumber = address[0];
                                    insuredPassengerAddress.Street = address[1];
                                    insuredPassengerAddress.Town = address[2];
                                }
                                else if (address.Count() == 4)
                                {
                                    insuredPassengerAddress.BuildingNumber = address[0];
                                    insuredPassengerAddress.Street = address[1];
                                    insuredPassengerAddress.Town = address[2];
                                    insuredPassengerAddress.Locality = address[3];
                                }
                                else if (address.Count() == 5)
                                {
                                    insuredPassengerAddress.BuildingNumber = address[0];
                                    insuredPassengerAddress.Street = address[1];
                                    insuredPassengerAddress.Town = address[2];
                                    insuredPassengerAddress.Locality = address[3];
                                    insuredPassengerAddress.County = address[4];
                                }

                                if (!string.IsNullOrWhiteSpace(iP.ClaimantPostCode))
                                {
                                    insuredPassengerAddress.PostCode = iP.ClaimantPostCode;
                                }

                                insuredPassenger.Addresses.Add(insuredPassengerAddress);
                                
                            }

                            if (!string.IsNullOrWhiteSpace(iP.ClaimantSolicitor))
                            {
                                PipelineOrganisation solicitor = new PipelineOrganisation();

                                solicitor.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                solicitor.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                solicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;

                                solicitor.OrganisationName = iP.ClaimantSolicitor;

                                insuredPassenger.Organisations.Add(solicitor);
                            }

                            insuredVehicle.People.Add(insuredPassenger);
                        }

                        #endregion

                        #region Vehicle Owner

                        var vehicleOwners = (from x in uniqueCliams
                                             where x.ClaimantInvolvement == "The owner of the vehicle but not driving"
                                             && x.ClaimantVehicleRegistion == uniqueInsuredVehicle
                                             //&& !string.IsNullOrEmpty(x.ClaimantFirstName) || !string.IsNullOrEmpty(x.ClaimantLastName)
                                             select x).Distinct();

                        foreach (var vO in vehicleOwners)
                        {
                            PipelinePerson vehicleOwner = new PipelinePerson();
                            vehicleOwner.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;

                            vehicleOwner.FirstName = vO.ClaimantFirstName;
                            vehicleOwner.LastName = vO.ClaimantLastName;

                            if (!string.IsNullOrWhiteSpace(vO.ClaimantDoB))
                            {
                                DateTime insuredPassengerDoB = DateTime.ParseExact(vO.ClaimantDoB, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                                vehicleOwner.DateOfBirth = insuredPassengerDoB;
                            }

                            if (!string.IsNullOrWhiteSpace(vO.NINumber))
                            {
                                PipelineNINumber NINumber = new PipelineNINumber();
                                NINumber.NINumber1 = vO.NINumber;
                                vehicleOwner.NINumbers.Add(NINumber);
                            }

                            if (!string.IsNullOrWhiteSpace(vO.ClaimantPhoneNumber))
                            {
                                PipelineTelephone telephoneNumber = new PipelineTelephone();
                                telephoneNumber.ClientSuppliedNumber = vO.ClaimantPhoneNumber;
                                vehicleOwner.Telephones.Add(telephoneNumber);
                            }

                            if (!string.IsNullOrWhiteSpace(vO.ClaimantEmail))
                            {
                                PipelineEmail emailAddress = new PipelineEmail();
                                emailAddress.EmailAddress = vO.ClaimantEmail;
                                vehicleOwner.EmailAddresses.Add(emailAddress);
                            }

                            if (!string.IsNullOrWhiteSpace(vO.ClaimantAddress))
                            {
                                PipelineAddress vehicleOwnerAddress = new PipelineAddress();

                                var address = vO.ClaimantAddress.Split(',');

                                if (address.Count() == 1)
                                {
                                    vehicleOwnerAddress.BuildingNumber = address[0];
                                }
                                else if (address.Count() == 2)
                                {
                                    vehicleOwnerAddress.BuildingNumber = address[0];
                                    vehicleOwnerAddress.Street = address[1];
                                }
                                else if (address.Count() == 3)
                                {
                                    vehicleOwnerAddress.BuildingNumber = address[0];
                                    vehicleOwnerAddress.Street = address[1];
                                    vehicleOwnerAddress.Town = address[2];
                                }
                                else if (address.Count() == 4)
                                {
                                    vehicleOwnerAddress.BuildingNumber = address[0];
                                    vehicleOwnerAddress.Street = address[1];
                                    vehicleOwnerAddress.Town = address[2];
                                    vehicleOwnerAddress.Locality = address[3];
                                }
                                else if (address.Count() == 5)
                                {
                                    vehicleOwnerAddress.BuildingNumber = address[0];
                                    vehicleOwnerAddress.Street = address[1];
                                    vehicleOwnerAddress.Town = address[2];
                                    vehicleOwnerAddress.Locality = address[3];
                                    vehicleOwnerAddress.County = address[4];
                                }

                                if (!string.IsNullOrWhiteSpace(vO.ClaimantPostCode))
                                {
                                    vehicleOwnerAddress.PostCode = vO.ClaimantPostCode;
                                }

                                vehicleOwner.Addresses.Add(vehicleOwnerAddress);
                            }

                            if (!string.IsNullOrWhiteSpace(vO.ClaimantSolicitor))
                            {
                                PipelineOrganisation solicitor = new PipelineOrganisation();

                                solicitor.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                solicitor.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                solicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;

                                solicitor.OrganisationName = vO.ClaimantSolicitor;

                                vehicleOwner.Organisations.Add(solicitor);
                            }

                            insuredVehicle.People.Add(vehicleOwner);
                        }

                        #endregion

                    }

                    #endregion

                    #region Third Party Vehicle

                    var uniqueThirdPartyVehicle = (from x in uniqueCliams 
                                                   where x.ClaimantVehicleRegistion != x.InsuredVehicleRegistration
                                                   //&& !string.IsNullOrWhiteSpace(x.ClaimantVehicleRegistion)
                                                   select x).Distinct();
                    
                    List<string> vehReg = new List<string>();

                    foreach (var thpv in uniqueThirdPartyVehicle)
                    {

                        PipelineVehicle thirdPartyVehicle = new PipelineVehicle();
                        thirdPartyVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                        thirdPartyVehicle.VehicleRegistration = thpv.ClaimantVehicleRegistion;

                        #region Vehicle Type

                        if (thpv.ClaimantInvolvement == "Cyclist")
                        {
                            thirdPartyVehicle.VehicleType_Id = (int)VehicleType.Bicycle;
                        }
                        else if (thpv.ClaimantInvolvement == "Motorcyclist")
                        {
                            thirdPartyVehicle.VehicleType_Id = (int)VehicleType.Motorcycle;
                        }
                        else
                        {
                            thirdPartyVehicle.VehicleType_Id = (int)VehicleType.Car;
                        }


                        #endregion

                        if (!vehReg.Contains(thpv.ClaimantVehicleRegistion))
                        {
                            motorClaim.Vehicles.Add(thirdPartyVehicle);

                            vehReg.Add(thpv.ClaimantVehicleRegistion);
                        }

                        #region Third Party Driver

                        var thirdPartyDrivers = (from x in uniqueThirdPartyVehicle
                                                 where (x.ClaimantInvolvement == "Driver" || x.ClaimantInvolvement == "Motorcyclist" || x.ClaimantInvolvement == "Cyclist")
                                                 && x.ClaimantVehicleRegistion == thpv.ClaimantVehicleRegistion
                                                 //&& !string.IsNullOrEmpty(x.ClaimantFirstName) || !string.IsNullOrEmpty(x.ClaimantLastName)
                                                 select x).Distinct();

                        foreach (var tpd in thirdPartyDrivers)
                        {
                            PipelinePerson thirdPartyDriver = new PipelinePerson();
                            thirdPartyDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                            thirdPartyDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            thirdPartyDriver.FirstName = tpd.ClaimantFirstName;
                            thirdPartyDriver.LastName = tpd.ClaimantLastName;

                            if (!string.IsNullOrWhiteSpace(tpd.ClaimantDoB))
                            {
                                DateTime insuredPassengerDoB = DateTime.ParseExact(tpd.ClaimantDoB, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                                thirdPartyDriver.DateOfBirth = insuredPassengerDoB;
                            }
                            if (!string.IsNullOrWhiteSpace(tpd.NINumber))
                            {
                                PipelineNINumber NINumber = new PipelineNINumber();
                                NINumber.NINumber1 = tpd.NINumber;
                                thirdPartyDriver.NINumbers.Add(NINumber);
                            }

                            if (!string.IsNullOrWhiteSpace(tpd.ClaimantPhoneNumber))
                            {
                                PipelineTelephone telephoneNumber = new PipelineTelephone();
                                telephoneNumber.ClientSuppliedNumber = tpd.ClaimantPhoneNumber;
                                thirdPartyDriver.Telephones.Add(telephoneNumber);
                            }

                            if (!string.IsNullOrWhiteSpace(tpd.ClaimantEmail))
                            {
                                PipelineEmail emailAddress = new PipelineEmail();
                                emailAddress.EmailAddress = tpd.ClaimantEmail;
                                thirdPartyDriver.EmailAddresses.Add(emailAddress);
                            }

                            if (!string.IsNullOrWhiteSpace(tpd.ClaimantAddress))
                            {
                                PipelineAddress thirdPartyPassengerAddress = new PipelineAddress();

                                var address = tpd.ClaimantAddress.Split(',');

                                if (address.Count() == 1)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                }
                                else if (address.Count() == 2)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                }
                                else if (address.Count() == 3)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                    thirdPartyPassengerAddress.Town = address[2];
                                }
                                else if (address.Count() == 4)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                    thirdPartyPassengerAddress.Town = address[2];
                                    thirdPartyPassengerAddress.Locality = address[3];
                                }
                                else if (address.Count() == 5)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                    thirdPartyPassengerAddress.Town = address[2];
                                    thirdPartyPassengerAddress.Locality = address[3];
                                    thirdPartyPassengerAddress.County = address[4];
                                }

                                if (!string.IsNullOrWhiteSpace(tpd.ClaimantPostCode))
                                {
                                    thirdPartyPassengerAddress.PostCode = tpd.ClaimantPostCode;
                                }

                                thirdPartyDriver.Addresses.Add(thirdPartyPassengerAddress);

                            }

                            if (!string.IsNullOrWhiteSpace(tpd.ClaimantSolicitor))
                            {
                                PipelineOrganisation solicitor = new PipelineOrganisation();

                                solicitor.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                solicitor.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                solicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;

                                solicitor.OrganisationName = tpd.ClaimantSolicitor;

                                thirdPartyDriver.Organisations.Add(solicitor);
                            }

                            thirdPartyVehicle.People.Add(thirdPartyDriver);
                        }

                        #endregion

                        #region Third Party Passenger

                        var thirdPartyPassengers = (from x in uniqueCliams
                                                    where x.ClaimantInvolvement == " A Passenger in or on a vehicle owned by someone else"
                                                    && x.ClaimantVehicleRegistion != uniqueInsuredVehicle
                                                    && x.ClaimantVehicleRegistion == thpv.ClaimantVehicleRegistion
                                                    select x).Distinct();


                        foreach (var iP in thirdPartyPassengers)
                        {
                            PipelinePerson thirdPartyPassenger = new PipelinePerson();
                            thirdPartyPassenger.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                            thirdPartyPassenger.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;

                            thirdPartyPassenger.FirstName = iP.ClaimantFirstName;
                            thirdPartyPassenger.LastName = iP.ClaimantLastName;

                            if (!string.IsNullOrWhiteSpace(iP.ClaimantDoB))
                            {
                                DateTime insuredPassengerDoB = DateTime.ParseExact(iP.ClaimantDoB, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                                thirdPartyPassenger.DateOfBirth = insuredPassengerDoB;
                            }
                            if (!string.IsNullOrWhiteSpace(iP.NINumber))
                            {
                                PipelineNINumber NINumber = new PipelineNINumber();
                                NINumber.NINumber1 = iP.NINumber;
                                thirdPartyPassenger.NINumbers.Add(NINumber);
                            }

                            if (!string.IsNullOrWhiteSpace(iP.ClaimantPhoneNumber))
                            {
                                PipelineTelephone telephoneNumber = new PipelineTelephone();
                                telephoneNumber.ClientSuppliedNumber = iP.ClaimantPhoneNumber;
                                thirdPartyPassenger.Telephones.Add(telephoneNumber);
                            }

                            if (!string.IsNullOrWhiteSpace(iP.ClaimantEmail))
                            {
                                PipelineEmail emailAddress = new PipelineEmail();
                                emailAddress.EmailAddress = iP.ClaimantEmail;
                                thirdPartyPassenger.EmailAddresses.Add(emailAddress);
                            }

                            if (!string.IsNullOrWhiteSpace(iP.ClaimantAddress))
                            {
                                PipelineAddress thirdPartyPassengerAddress = new PipelineAddress();

                                var address = iP.ClaimantAddress.Split(',');

                                if (address.Count() == 1)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                }
                                else if (address.Count() == 2)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                }
                                else if (address.Count() == 3)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                    thirdPartyPassengerAddress.Town = address[2];
                                }
                                else if (address.Count() == 4)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                    thirdPartyPassengerAddress.Town = address[2];
                                    thirdPartyPassengerAddress.Locality = address[3];
                                }
                                else if (address.Count() == 5)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                    thirdPartyPassengerAddress.Town = address[2];
                                    thirdPartyPassengerAddress.Locality = address[3];
                                    thirdPartyPassengerAddress.County = address[4];
                                }

                                if (!string.IsNullOrWhiteSpace(iP.ClaimantPostCode))
                                {
                                    thirdPartyPassengerAddress.PostCode = iP.ClaimantPostCode;
                                }

                                thirdPartyPassenger.Addresses.Add(thirdPartyPassengerAddress);

                            }

                            if (!string.IsNullOrWhiteSpace(iP.ClaimantSolicitor))
                            {
                                PipelineOrganisation solicitor = new PipelineOrganisation();

                                solicitor.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                solicitor.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                solicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;

                                solicitor.OrganisationName = iP.ClaimantSolicitor;

                                thirdPartyPassenger.Organisations.Add(solicitor);
                            }

                            thirdPartyVehicle.People.Add(thirdPartyPassenger);
                        }

                        #endregion

                        #region Third Party Vehicle Owner

                        var vehicleOwners = (from x in uniqueCliams
                                             where x.ClaimantInvolvement == "The owner of the vehicle but not driving"
                                             && x.ClaimantVehicleRegistion != uniqueInsuredVehicle
                                             && x.ClaimantVehicleRegistion == thpv.ClaimantVehicleRegistion
                                             select x).Distinct();

                        foreach (var vO in vehicleOwners)
                        {
                            PipelinePerson vehicleOwner = new PipelinePerson();
                            vehicleOwner.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;

                            vehicleOwner.FirstName = vO.ClaimantFirstName;
                            vehicleOwner.LastName = vO.ClaimantLastName;

                            if (!string.IsNullOrWhiteSpace(vO.ClaimantDoB))
                            {
                                DateTime insuredPassengerDoB = DateTime.ParseExact(vO.ClaimantDoB, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                                vehicleOwner.DateOfBirth = insuredPassengerDoB;
                            }

                            if (!string.IsNullOrWhiteSpace(vO.NINumber))
                            {
                                PipelineNINumber NINumber = new PipelineNINumber();
                                NINumber.NINumber1 = vO.NINumber;
                                vehicleOwner.NINumbers.Add(NINumber);
                            }

                            if (!string.IsNullOrWhiteSpace(vO.ClaimantPhoneNumber))
                            {
                                PipelineTelephone telephoneNumber = new PipelineTelephone();
                                telephoneNumber.ClientSuppliedNumber = vO.ClaimantPhoneNumber;
                                vehicleOwner.Telephones.Add(telephoneNumber);
                            }

                            if (!string.IsNullOrWhiteSpace(vO.ClaimantEmail))
                            {
                                PipelineEmail emailAddress = new PipelineEmail();
                                emailAddress.EmailAddress = vO.ClaimantEmail;
                                vehicleOwner.EmailAddresses.Add(emailAddress);
                            }

                            if (!string.IsNullOrWhiteSpace(vO.ClaimantAddress))
                            {
                                PipelineAddress vehicleOwnerAddress = new PipelineAddress();

                                var address = vO.ClaimantAddress.Split(',');

                                vehicleOwnerAddress.BuildingNumber = address[0];

                                if (address.Count() == 1)
                                {
                                    vehicleOwnerAddress.BuildingNumber = address[0];
                                }
                                else if (address.Count() == 2)
                                {
                                    vehicleOwnerAddress.BuildingNumber = address[0];
                                    vehicleOwnerAddress.Street = address[1];
                                }
                                else if (address.Count() == 3)
                                {
                                    vehicleOwnerAddress.BuildingNumber = address[0];
                                    vehicleOwnerAddress.Street = address[1];
                                    vehicleOwnerAddress.Town = address[2];
                                }
                                else if (address.Count() == 4)
                                {
                                    vehicleOwnerAddress.BuildingNumber = address[0];
                                    vehicleOwnerAddress.Street = address[1];
                                    vehicleOwnerAddress.Town = address[2];
                                    vehicleOwnerAddress.Locality = address[3];
                                }
                                else if (address.Count() == 5)
                                {
                                    vehicleOwnerAddress.BuildingNumber = address[0];
                                    vehicleOwnerAddress.Street = address[1];
                                    vehicleOwnerAddress.Town = address[2];
                                    vehicleOwnerAddress.Locality = address[3];
                                    vehicleOwnerAddress.County = address[4];
                                }

                                if (string.IsNullOrWhiteSpace(vO.ClaimantPostCode))
                                {
                                    vehicleOwnerAddress.PostCode = vO.ClaimantPostCode;
                                }

                                vehicleOwner.Addresses.Add(vehicleOwnerAddress);
                            }

                            if (!string.IsNullOrWhiteSpace(vO.ClaimantSolicitor))
                            {
                                PipelineOrganisation solicitor = new PipelineOrganisation();

                                solicitor.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                solicitor.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                solicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;

                                solicitor.OrganisationName = vO.ClaimantSolicitor;

                                vehicleOwner.Organisations.Add(solicitor);
                            }

                            thirdPartyVehicle.People.Add(vehicleOwner);
                        }

                        #endregion

                        #region Unknown Third Party

                        var thirdParties = (from x in uniqueCliams
                                                 where x.ClaimantInvolvement == "Pedestrian" || string.IsNullOrWhiteSpace(x.ClaimantInvolvement)
                                                 && x.ClaimantVehicleRegistion != uniqueInsuredVehicle
                                                 && x.ClaimantVehicleRegistion == thpv.ClaimantVehicleRegistion
                                                 //&& !string.IsNullOrEmpty(x.ClaimantFirstName) || !string.IsNullOrEmpty(x.ClaimantLastName)
                                                 select x).Distinct();

                        foreach (var tp in thirdParties)
                        {
                            PipelinePerson thirdParty = new PipelinePerson();
                            thirdParty.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                            thirdParty.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                            thirdParty.FirstName = tp.ClaimantFirstName;
                            thirdParty.LastName = tp.ClaimantLastName;

                            if (!string.IsNullOrWhiteSpace(tp.ClaimantDoB))
                            {
                                DateTime insuredPassengerDoB = DateTime.ParseExact(tp.ClaimantDoB, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                                thirdParty.DateOfBirth = insuredPassengerDoB;
                            }
                            if (!string.IsNullOrWhiteSpace(tp.NINumber))
                            {
                                PipelineNINumber NINumber = new PipelineNINumber();
                                NINumber.NINumber1 = tp.NINumber;
                                thirdParty.NINumbers.Add(NINumber);
                            }

                            if (!string.IsNullOrWhiteSpace(tp.ClaimantPhoneNumber))
                            {
                                PipelineTelephone telephoneNumber = new PipelineTelephone();
                                telephoneNumber.ClientSuppliedNumber = tp.ClaimantPhoneNumber;
                                thirdParty.Telephones.Add(telephoneNumber);
                            }

                            if (!string.IsNullOrWhiteSpace(tp.ClaimantEmail))
                            {
                                PipelineEmail emailAddress = new PipelineEmail();
                                emailAddress.EmailAddress = tp.ClaimantEmail;
                                thirdParty.EmailAddresses.Add(emailAddress);
                            }

                            if (!string.IsNullOrWhiteSpace(tp.ClaimantAddress))
                            {
                                PipelineAddress thirdPartyPassengerAddress = new PipelineAddress();

                                var address = tp.ClaimantAddress.Split(',');

                                if (address.Count() == 1)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                }
                                else if (address.Count() == 2)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                }
                                else if (address.Count() == 3)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                    thirdPartyPassengerAddress.Town = address[2];
                                }
                                else if (address.Count() == 4)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                    thirdPartyPassengerAddress.Town = address[2];
                                    thirdPartyPassengerAddress.Locality = address[3];
                                }
                                else if (address.Count() == 5)
                                {
                                    thirdPartyPassengerAddress.BuildingNumber = address[0];
                                    thirdPartyPassengerAddress.Street = address[1];
                                    thirdPartyPassengerAddress.Town = address[2];
                                    thirdPartyPassengerAddress.Locality = address[3];
                                    thirdPartyPassengerAddress.County = address[4];
                                }

                                if (!string.IsNullOrWhiteSpace(tp.ClaimantPostCode))
                                {
                                    thirdPartyPassengerAddress.PostCode = tp.ClaimantPostCode;
                                }

                                thirdParty.Addresses.Add(thirdPartyPassengerAddress);

                            }

                            if (!string.IsNullOrWhiteSpace(tp.ClaimantSolicitor))
                            {
                                PipelineOrganisation solicitor = new PipelineOrganisation();

                                solicitor.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                solicitor.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                solicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;

                                solicitor.OrganisationName = tp.ClaimantSolicitor;

                                thirdParty.Organisations.Add(solicitor);
                            }

                            thirdPartyVehicle.People.Add(thirdParty);
                        }

                        #endregion

                    }

                    #endregion

                    #region No Vehicle Involved

                    var unknownVehicle = (from x in uniqueCliams
                                          where string.IsNullOrWhiteSpace(x.ClaimantVehicleRegistion)
                                          && string.IsNullOrWhiteSpace(x.InsuredVehicleRegistration)
                                          select x).Distinct();

                    PipelineVehicle noVehicleInvolved = new PipelineVehicle();
                    noVehicleInvolved.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;

                    if (unknownVehicle.Count() > 0)
                    {
                        motorClaim.Vehicles.Add(noVehicleInvolved);
                    }

                    foreach (var v in unknownVehicle)
                    {

                        var people = (from x in unknownVehicle
                                      where !string.IsNullOrWhiteSpace(x.ClaimantLastName) 
                                      //&& !string.IsNullOrEmpty(x.ClaimantFirstName) || !string.IsNullOrEmpty(x.ClaimantLastName)
                                      select x).Distinct();

                        foreach (var p in people)
                        {
                            PipelinePerson person = new PipelinePerson();
                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                            person.FirstName = p.ClaimantFirstName;
                            person.LastName = p.ClaimantLastName;

                            if (!string.IsNullOrWhiteSpace(p.ClaimantDoB))
                            {
                                DateTime insuredPassengerDoB = DateTime.ParseExact(p.ClaimantDoB, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                                person.DateOfBirth = insuredPassengerDoB;
                            }
                            if (!string.IsNullOrWhiteSpace(p.NINumber))
                            {
                                PipelineNINumber NINumber = new PipelineNINumber();
                                NINumber.NINumber1 = p.NINumber;
                                person.NINumbers.Add(NINumber);
                            }

                            if (!string.IsNullOrWhiteSpace(p.ClaimantPhoneNumber))
                            {
                                PipelineTelephone telephoneNumber = new PipelineTelephone();
                                telephoneNumber.ClientSuppliedNumber = p.ClaimantPhoneNumber;
                                person.Telephones.Add(telephoneNumber);
                            }

                            if (!string.IsNullOrWhiteSpace(p.ClaimantEmail))
                            {
                                PipelineEmail emailAddress = new PipelineEmail();
                                emailAddress.EmailAddress = p.ClaimantEmail;
                                person.EmailAddresses.Add(emailAddress);
                            }

                            if (!string.IsNullOrWhiteSpace(p.ClaimantAddress))
                            {
                                PipelineAddress personAddress = new PipelineAddress();

                                var address = p.ClaimantAddress.Split(',');

                                if (address.Count() == 1)
                                {
                                    personAddress.BuildingNumber = address[0];
                                }
                                else if (address.Count() == 2)
                                {
                                    personAddress.BuildingNumber = address[0];
                                    personAddress.Street = address[1];
                                }
                                else if (address.Count() == 3)
                                {
                                    personAddress.BuildingNumber = address[0];
                                    personAddress.Street = address[1];
                                    personAddress.Town = address[2];
                                }
                                else if (address.Count() == 4)
                                {
                                    personAddress.BuildingNumber = address[0];
                                    personAddress.Street = address[1];
                                    personAddress.Town = address[2];
                                    personAddress.Locality = address[3];
                                }
                                else if (address.Count() == 5)
                                {
                                    personAddress.BuildingNumber = address[0];
                                    personAddress.Street = address[1];
                                    personAddress.Town = address[2];
                                    personAddress.Locality = address[3];
                                    personAddress.County = address[4];
                                }

                                if (!string.IsNullOrWhiteSpace(p.ClaimantPostCode))
                                {
                                    personAddress.PostCode = p.ClaimantPostCode;
                                }

                                person.Addresses.Add(personAddress);

                            }

                            if (!string.IsNullOrWhiteSpace(p.ClaimantSolicitor))
                            {
                                PipelineOrganisation solicitor = new PipelineOrganisation();

                                solicitor.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                solicitor.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                solicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;

                                solicitor.OrganisationName = p.ClaimantSolicitor;

                                person.Organisations.Add(solicitor);
                            }

                            noVehicleInvolved.People.Add(person);
                        }

                        #endregion

                    #region Insured Vehicle and Driver Check

                    bool insuredVehicle = false;
                    bool insuredDriver = false;

                    foreach (var vehicle in motorClaim.Vehicles)
                    {
                        if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                        {
                            insuredVehicle = true;

                            foreach (var person in vehicle.People)
                            {
                                if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                                {
                                    insuredDriver = true;
                                }
                            }
                        }
                    }

                    if (insuredVehicle == false)
                    {
                        PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                        defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                        }

                        motorClaim.Vehicles.Add(defaultInsuredVehicle);
                    }
                    else
                    {
                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            foreach (var vehicle in motorClaim.Vehicles)
                            {
                                if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                {

                                    vehicle.People.Add(defaultInsuredDriver);

                                }

                            }

                        }
                    }

                    #endregion

                    }

                    processedClaims.Add(claim.ClaimNumber.ToUpper()); // Add claim number to list so that the claim is not processed again

                    if (ProcessClaimFn(ctx, motorClaim, statusTracking) == -1) return;

                }
            }
        }
    }
}
