﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.Allianz.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PortalApplicationId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AcceptedDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String SeqNbr;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String LineOfBusiness;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Branch;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Team;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String IncidentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredVehicleRegistration;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolder;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriver;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantInvolvement;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantVehicleRegistion;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantFirstName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantLastName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantAddress;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantPostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantPhoneNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantEmail;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantDoB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NINumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantSolicitor;

    }
}
