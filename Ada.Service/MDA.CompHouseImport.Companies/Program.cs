﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;

namespace MDA.CompHouseImport.Companies
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string path = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\CompaniesHouse\Resource\";
            Int32 ImportId = 0;

            foreach (var file in Directory.GetFiles(path, "Basic*", SearchOption.AllDirectories))
            {
                Stopwatch stopwatch = new Stopwatch();
                //Stream reader will read test.csv file in current folder
                StreamReader sr = new StreamReader(file);
                //Csv reader reads the stream 
                CsvReader csvread = new CsvReader(sr);
                csvread.Configuration.RegisterClassMap<MyClassMap>();
                //csvread will fetch all record in one go to the IEnumerable object record
                IEnumerable<Company> record = csvread.GetRecords<Company>();
                DateTime? dateTime;

                string connectionString = GetNewConnection();
                var conn = new SqlConnection(connectionString);

                stopwatch.Start();


                string sqlQuery = "Insert INTO CH_ImportLog (FileUploaded, CH_Type, UploadDate)";
                sqlQuery += "OUTPUT INSERTED.ID VALUES (@Filename, @CH_Type, @UploadDate)";

                using (SqlConnection dataConnection = new SqlConnection(connectionString))
                {
                    using (var dataCommand = new SqlCommand(sqlQuery, conn))
                    {
                        dataConnection.Open();
                        dataCommand.CommandType = CommandType.Text;
                        dataCommand.CommandText = sqlQuery;
                        dataCommand.Parameters.AddWithValue("@Filename", Path.GetFileName(file));
                        dataCommand.Parameters.AddWithValue("@CH_Type", "Company");
                        dataCommand.Parameters.AddWithValue("@UploadDate", DateTime.Now);
                        conn.Open();
                        ImportId = (Int32)dataCommand.ExecuteScalar();
                        dataConnection.Close();
                        conn.Close();
                    }
                }

                try
                {
                    conn.Open();
                    using (conn)
                    {
                        foreach (var rec in record) // Each record will be fetched and inserted
                        {
                            var sqlCmd = new SqlCommand();
                            sqlCmd.CommandText = "CH_InsertCompanies";
                            sqlCmd.CommandType = CommandType.StoredProcedure;
                            sqlCmd.Parameters.AddWithValue("@ImportId", ImportId);
                            sqlCmd.Parameters.AddWithValue("@CompanyName", rec.CompanyName);
                            sqlCmd.Parameters.AddWithValue("@CompanyNumber", rec.CompanyNumber);
                            sqlCmd.Parameters.AddWithValue("@RegAddressPOBox", rec.RegAddressPOBox);
                            sqlCmd.Parameters.AddWithValue("@RegAddressAddressLine1", rec.RegAddressAddressLine1);
                            sqlCmd.Parameters.AddWithValue("@RegAddressAddressLine2", rec.RegAddressAddressLine2);
                            sqlCmd.Parameters.AddWithValue("@RegAddressPostTown", rec.RegAddressPostTown);
                            sqlCmd.Parameters.AddWithValue("@RegAddressCounty", rec.RegAddressCounty);
                            sqlCmd.Parameters.AddWithValue("@RegAddressPostCode", rec.RegAddressPostCode);
                            sqlCmd.Parameters.AddWithValue("@CompanyStatus", rec.CompanyStatus);


                            sqlCmd.Parameters.AddWithValue("@IncorporationDate", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.IncorporationDate, out dateTime) ? (object) dateTime : DBNull.Value;
                            sqlCmd.Parameters.AddWithValue("@DissolutionDate", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.DissolutionDate, out dateTime) ? (object) dateTime : DBNull.Value;
                            sqlCmd.Parameters.AddWithValue("@PreviousName1CONDATE", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.PreviousName1CONDATE, out dateTime)
                                    ? (object) dateTime
                                    : DBNull.Value;
                            sqlCmd.Parameters.AddWithValue("@PreviousName2CONDATE", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.PreviousName2CONDATE, out dateTime)
                                    ? (object) dateTime
                                    : DBNull.Value;
                            sqlCmd.Parameters.AddWithValue("@PreviousName3CONDATE", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.PreviousName3CONDATE, out dateTime)
                                    ? (object) dateTime
                                    : DBNull.Value;
                            sqlCmd.Parameters.AddWithValue("@PreviousName4CONDATE", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.PreviousName4CONDATE, out dateTime)
                                    ? (object) dateTime
                                    : DBNull.Value;
                            sqlCmd.Parameters.AddWithValue("@PreviousName5CONDATE", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.PreviousName5CONDATE, out dateTime)
                                    ? (object) dateTime
                                    : DBNull.Value;
                            sqlCmd.Parameters.AddWithValue("@PreviousName6CONDATE", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.PreviousName6CONDATE, out dateTime)
                                    ? (object) dateTime
                                    : DBNull.Value;
                            sqlCmd.Parameters.AddWithValue("@PreviousName7CONDATE", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.PreviousName7CONDATE, out dateTime)
                                    ? (object) dateTime
                                    : DBNull.Value;
                            sqlCmd.Parameters.AddWithValue("@PreviousName8CONDATE", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.PreviousName8CONDATE, out dateTime)
                                    ? (object) dateTime
                                    : DBNull.Value;
                            sqlCmd.Parameters.AddWithValue("@PreviousName9CONDATE", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.PreviousName9CONDATE, out dateTime)
                                    ? (object) dateTime
                                    : DBNull.Value;
                            sqlCmd.Parameters.AddWithValue("@PreviousName10CONDATE", SqlDbType.DateTime).Value =
                                DateTimeTryParse(rec.PreviousName10CONDATE, out dateTime)
                                    ? (object) dateTime
                                    : DBNull.Value;


                            sqlCmd.Parameters.AddWithValue("@SICCodeSicText_1", rec.SICCodeSicText_1);
                            sqlCmd.Parameters.AddWithValue("@PreviousName1CompanyName", rec.PreviousName1CompanyName);
                            sqlCmd.Parameters.AddWithValue("@PreviousName2CompanyName", rec.PreviousName2CompanyName);
                            sqlCmd.Parameters.AddWithValue("@PreviousName3CompanyName", rec.PreviousName3CompanyName);
                            sqlCmd.Parameters.AddWithValue("@PreviousName4CompanyName", rec.PreviousName4CompanyName);
                            sqlCmd.Parameters.AddWithValue("@PreviousName5CompanyName", rec.PreviousName5CompanyName);
                            sqlCmd.Parameters.AddWithValue("@PreviousName6CompanyName", rec.PreviousName6CompanyName);
                            sqlCmd.Parameters.AddWithValue("@PreviousName7CompanyName", rec.PreviousName7CompanyName);
                            sqlCmd.Parameters.AddWithValue("@PreviousName8CompanyName", rec.PreviousName8CompanyName);
                            sqlCmd.Parameters.AddWithValue("@PreviousName9CompanyName", rec.PreviousName9CompanyName);
                            sqlCmd.Parameters.AddWithValue("@PreviousName10CompanyName", rec.PreviousName10CompanyName);

                            sqlCmd.Connection = conn;
                            sqlCmd.ExecuteNonQuery();
                        }

                        stopwatch.Stop();

                        // Get the elapsed time as a TimeSpan value.
                        TimeSpan ts = stopwatch.Elapsed;

                        // Format and display the TimeSpan value. 
                        string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                                           ts.Hours, ts.Minutes, ts.Seconds,
                                                           ts.Milliseconds/10);
                        Console.WriteLine("RunTime " + elapsedTime);

                        SendEmail(true, null, file, elapsedTime);
                    }
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    SendEmail(false, ex, file, null);
                }

                finally
                {
                    conn.Close();
                    sr.Close();
                }
            }
        }


        private static void SendEmail(bool result, Exception ex, string fileName, string elapsedTime)
        {
            MailMessage mail = new MailMessage("admin@CSV.co.uk", "jonathanwalker@keoghs.co.uk");
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Host = ConfigurationManager.AppSettings["Smtp"];

            if (result)
            {
                mail.Subject = "Companies House Import Success.";
                mail.Body = "Companies House Import was successful. " + Environment.NewLine + Environment.NewLine + "File: " + fileName + Environment.NewLine;
                mail.Body += "Runtime: " + elapsedTime;
            }
            else
            {
                mail.Subject = "Companies House Import Failure.";
                mail.Body = "Companies House Import was unsucessful. File: " + fileName + Environment.NewLine;
                mail.Body += ex;
            }

            client.Send(mail);
        }

        public static bool DateTimeTryParse(string text, out DateTime? result)
        {
            result = null;
            // We allow an empty string for null (could also use IsNullOrWhitespace)
            if (String.IsNullOrEmpty(text)) return true;

            DateTime d;
            if (!DateTime.TryParse(text, out d)) return false;
            result = d;

            if (d.Year < 1753) return false;

            return true;
        }

        public static string GetNewConnection()
        {
            return ConfigurationManager.ConnectionStrings["CompaniesHouseDbContext"].ConnectionString;
        }
    }
}
