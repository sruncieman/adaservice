﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration;

namespace MDA.CompHouseImport.Companies
{
    public class Company
    {

       // "RIDINGS IT LIMITED","06520921","","","RACS GROUP HOUSE","THREE HORSESHOES WALK","WARMINSTER","WILTSHIRE","ENGLAND","BA12 9BT","Private Limited Company","Active","United Kingdom","","03/03/2008","30","11","31/08/2013","30/11/2011","TOTAL EXEMPTION SMALL","31/03/2014","03/03/2013","0","0","0","0","62020 - Information technology consultancy activities","","","","0","0","http://business.data.gov.uk/id/company/06520921","26/01/2010","PROFESSIONAL FREELANCER 931 LIMITED","","","","","","","","","","","","","","","","","",""

        public string CompanyName { get; set; }     //RIDINGS IT LIMITED
        public string CompanyNumber { get; set; }   //06520921
        public string RegAddressPOBox { get; set; } //
        public string RegAddressAddressLine1 { get; set; } //
        public string RegAddressAddressLine2 { get; set; }
        public string RegAddressPostTown { get; set; }
        public string RegAddressCounty { get; set; }
        public string RegAddressPostCode { get; set; }
        public string CompanyStatus { get; set; }
        public string DissolutionDate { get; set; }
        public string IncorporationDate { get; set; }
        public string SICCodeSicText_1 { get; set; }
        public string PreviousName1CONDATE { get; set; }
        public string PreviousName1CompanyName { get; set; }
        public string PreviousName2CONDATE { get; set; }
        public string PreviousName2CompanyName { get; set; }
        public string PreviousName3CONDATE { get; set; }
        public string PreviousName3CompanyName { get; set; }
        public string PreviousName4CONDATE { get; set; }
        public string PreviousName4CompanyName { get; set; }
        public string PreviousName5CONDATE { get; set; }
        public string PreviousName5CompanyName { get; set; }
        public string PreviousName6CONDATE { get; set; }
        public string PreviousName6CompanyName { get; set; }
        public string PreviousName7CONDATE { get; set; }
        public string PreviousName7CompanyName { get; set; }
        public string PreviousName8CONDATE { get; set; }
        public string PreviousName8CompanyName { get; set; }
        public string PreviousName9CONDATE { get; set; }
        public string PreviousName9CompanyName { get; set; }
        public string PreviousName10CONDATE { get; set; }
        public string PreviousName10CompanyName { get; set; }
    }

    public class MyClassMap : CsvClassMap<Company>
    {
        public override void CreateMap()
        {
            Map(m => m.CompanyName).Index(0);
            Map(m => m.CompanyNumber).Index(1);
            Map(m => m.RegAddressPOBox).Index(3);
            Map(m => m.RegAddressAddressLine1).Index(4);
            Map(m => m.RegAddressAddressLine2).Index(5);
            Map(m => m.RegAddressPostTown).Index(6);
            Map(m => m.RegAddressCounty).Index(7);
            Map(m => m.RegAddressPostCode).Index(9);
            Map(m => m.CompanyStatus).Index(11);
            Map(m => m.DissolutionDate).Index(13);
            Map(m => m.IncorporationDate).Index(14);
            Map(m => m.SICCodeSicText_1).Index(26);
            Map(m => m.PreviousName1CONDATE).Index(33);
            Map(m => m.PreviousName1CompanyName).Index(34);
            Map(m => m.PreviousName2CONDATE).Index(35);
            Map(m => m.PreviousName2CompanyName).Index(36);
            Map(m => m.PreviousName3CONDATE).Index(37);
            Map(m => m.PreviousName3CompanyName).Index(38);
            Map(m => m.PreviousName4CONDATE).Index(39);
            Map(m => m.PreviousName4CompanyName).Index(40);
            Map(m => m.PreviousName5CONDATE).Index(41);
            Map(m => m.PreviousName5CompanyName).Index(42);
            Map(m => m.PreviousName6CONDATE).Index(43);
            Map(m => m.PreviousName6CompanyName).Index(44);
            Map(m => m.PreviousName7CONDATE).Index(45);
            Map(m => m.PreviousName7CompanyName).Index(46);
            Map(m => m.PreviousName8CONDATE).Index(47);
            Map(m => m.PreviousName8CompanyName).Index(48);
            Map(m => m.PreviousName9CONDATE).Index(49);
            Map(m => m.PreviousName9CompanyName).Index(50);
            Map(m => m.PreviousName10CONDATE).Index(51);
            Map(m => m.PreviousName10CompanyName).Index(52);
        }
    }
}
