﻿CREATE PROCEDURE ETL.PopulateETLSanctionListData
AS
SET NOCOUNT ON

---------------------------------------------------
--Change the directory if required
--
--The UK List is available @ https://www.gov.uk/government/publications/financial-sanctions-consolidated-list-of-targets/consolidated-list-of-targets
--The plain text file should be downloaded.
--
--The US SDN data is available @ http://www.treasury.gov/resource-center/sanctions/SDN-List/Pages/default.aspx
--The PIP Files should be downloaded and saved as txt documents. These files will need to be open, the last row which is a symbol -> (arrow)deleted and saved.
----------------------------------------------------

----------------------------------
--UKConsolidatedList
----------------------------------
--ImportUKConsolidatedList
TRUNCATE TABLE [ETL].[UK_ConsolidatedTargetList]
BULK INSERT [ETL].[UK_ConsolidatedTargetList] FROM 'D:\SSIS\SSISImport\InternationalSanctions\sanctionsconlist.txt'
WITH (
	 FIRSTROW = 3,
	 DATAFILETYPE = 'char',
	 FIELDTERMINATOR = ';',
	 ROWTERMINATOR = '\n'
	)

----------------------------------
--USSpeciallyDesignatedNationalsList(SDN)
----------------------------------
--ImportSDNNames
TRUNCATE TABLE [ETL].[US_SDN_Names]
BULK INSERT [ETL].[US_SDN_Names] FROM 'D:\SSIS\SSISImport\InternationalSanctions\SDN.txt'
WITH (
	 DATAFILETYPE = 'char',
	 FIELDTERMINATOR ='|',
     ROWTERMINATOR ='\n'
	)

--ImportSDNAddresses
TRUNCATE TABLE [ETL].[US_SDN_Address]
BULK INSERT [ETL].[US_SDN_Address] FROM 'D:\SSIS\SSISImport\InternationalSanctions\add.txt'
WITH (
	 DATAFILETYPE = 'char',
	 FIELDTERMINATOR ='|',
     ROWTERMINATOR ='\n'
	)

--ImportSDNAlternativeNames
TRUNCATE TABLE [ETL].[US_SDN_AlternativeNames]
BULK INSERT [ETL].[US_SDN_AlternativeNames] FROM 'D:\SSIS\SSISImport\InternationalSanctions\alt.txt'
WITH (
	 DATAFILETYPE = 'char',
	 FIELDTERMINATOR ='|',
     ROWTERMINATOR ='\n'
	)

--ImportSDNComments
TRUNCATE TABLE [ETL].[US_SDN_Comments]
BULK INSERT [ETL].[US_SDN_Comments] FROM 'D:\SSIS\SSISImport\InternationalSanctions\sdn_comments.txt'
WITH (
	 DATAFILETYPE = 'char',
	 FIELDTERMINATOR ='|',
     ROWTERMINATOR ='\n'
	)


