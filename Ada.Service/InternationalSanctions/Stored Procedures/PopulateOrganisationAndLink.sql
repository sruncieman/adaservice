﻿CREATE PROCEDURE [dbo].[PopulateOrganisationAndLink]
AS
SET NOCOUNT ON

------------------------------------------------
--CreateObjectsDeclareVariablesTruncateTables
------------------------------------------------
DELETE [dbo].[Organisation2Email]
DELETE [dbo].[Organisation2Telephone]
DELETE [dbo].[Organisation2Website]
DELETE [dbo].[Organisation]

DBCC CHECKIDENT ('dbo.Organisation2Email', RESEED, 0) WITH NO_INFOMSGS;
DBCC CHECKIDENT ('dbo.Organisation2Telephone', RESEED, 0) WITH NO_INFOMSGS;
DBCC CHECKIDENT ('dbo.Organisation2Website', RESEED, 0) WITH NO_INFOMSGS;
DBCC CHECKIDENT ('dbo.Organisation', RESEED, 0) WITH NO_INFOMSGS;

IF OBJECT_ID('tempdb..#Organisations') IS NOT NULL
	DROP TABLE #Organisations
CREATE TABLE #Organisations (RowID INT IDENTITY(1,1), UniqueID INT, OrganisationName VARCHAR(255), OrganisationAlternativeName VARCHAR(255), FullAddress VARCHAR(500), Country VARCHAR(50), Remarks VARCHAR(MAX))

IF OBJECT_ID('tempdb..#FullAddressSpliter') IS NOT NULL
	DROP TABLE #FullAddressSpliter
CREATE TABLE #FullAddressSpliter (RowID INT IDENTITY(1,1), UniqueID INT, FullAddress VARCHAR(500))

IF OBJECT_ID('tempdb..#AddressSplit') IS NOT NULL
	DROP TABLE #AddressSplit
CREATE TABLE #AddressSplit (RowID INT IDENTITY(1,1), UniqueID INT, AddressLineNo INT ,AddressLine VARCHAR(500))

IF OBJECT_ID('tempdb..#RemarksSpliter') IS NOT NULL
	DROP TABLE #RemarksSpliter
CREATE TABLE #RemarksSpliter (RowID INT IDENTITY(1,1), UniqueID INT, Remarks VARCHAR(MAX))

IF OBJECT_ID('tempdb..#RemarksSplit') IS NOT NULL
	DROP TABLE #RemarksSplit
CREATE TABLE #RemarksSplit (RowID INT IDENTITY(1,1), UniqueID INT, Type VARCHAR(100), Remarks VARCHAR(MAX))

IF OBJECT_ID('tempdb..#OrganisationsInsert') IS NOT NULL
	DROP TABLE #OrganisationsInsert
CREATE TABLE #OrganisationsInsert (RowID INT IDENTITY(1,1), SanctionListId INT, UniqueID INT, OrganisationName VARCHAR(255), OrganisationAlternativeName VARCHAR(255), AddressLine1 VARCHAR(200), AddressLine2 VARCHAR(200), AddressLine3 VARCHAR(200), AddressLine4 VARCHAR(200), AddressLine5 VARCHAR(500), Country VARCHAR(50))

IF OBJECT_ID('tempdb..#RemarksInsert') IS NOT NULL
	DROP TABLE #RemarksInsert
CREATE TABLE #RemarksInsert (RowID INT IDENTITY(1,1), UniqueID INT, Type VARCHAR(100), Value VARCHAR(200))

DECLARE  @InsertCount		INT
		,@Counter			INT = 1
		,@StartPosition		INT = 0
		,@EndPosition		INT
		,@WorkingString		VARCHAR(MAX) 
		,@Delimiter			VARCHAR = ','
		,@ExtractionString	VARCHAR(MAX)
		,@InsertString		VARCHAR(MAX) 
		,@UniqueID			INT
		,@AddressLineNo		INT = 0

------------------------------------------------
--UK Sanctions
------------------------------------------------
INSERT INTO #Organisations (UniqueID, OrganisationName, FullAddress, Country, Remarks)
SELECT	 CAST(UniqueID AS INT) UniqueID
		,CAST(LEFT(OrganisationName,255) AS VARCHAR(255)) OrganisationName
		,CASE WHEN FullAddress = '' THEN NULL ELSE CAST(LEFT(SUBSTRING(FullAddress,1,LEN(FullAddress)-1),500) AS VARCHAR(500)) END FullAddress
		,CAST(LEFT(Country,50) AS VARCHAR(50)) Country
		,CAST(Remarks AS VARCHAR(MAX)) Remarks
FROM	(
		SELECT	 GroupID UniqueID
				,Name6 OrganisationName
				,ISNULL(Address1 + ', ','') + ISNULL(Address2 + ', ','') + ISNULL(Address3 + ', ','') + ISNULL(Address4 + ', ','') + ISNULL(Address5 + ', ','') + ISNULL(Address6 + ', ','') + ISNULL(PostZipCode + ', ','') FullAddress
				,Country
				,OtherInformation Remarks
		FROM [ETL].[UK_ConsolidatedTargetList]
		WHERE GroupType = 'Entity'
		) DATA
GROUP BY CAST(UniqueID AS INT)
		,CAST(LEFT(OrganisationName,255) AS VARCHAR(255))
		,CASE WHEN FullAddress = '' THEN NULL ELSE CAST(LEFT(SUBSTRING(FullAddress,1,LEN(FullAddress)-1),500) AS VARCHAR(500)) END
		,CAST(LEFT(Country,50) AS VARCHAR(50))
		,CAST(Remarks AS VARCHAR(MAX))
ORDER BY CAST(UniqueID AS INT)

----------------------
--Address Split
----------------------
INSERT INTO #FullAddressSpliter (UniqueID, FullAddress)
SELECT RowID, RTRIM(LTRIM(FullAddress + ',')) FullAddress
FROM #Organisations
WHERE FullAddress IS NOT NULL
GROUP BY RowID, FullAddress + ','
ORDER BY RowID
SELECT @InsertCount = @@ROWCOUNT

WHILE @Counter <= @InsertCount
BEGIN
	SELECT	 @WorkingString = FullAddress
			,@UniqueID		= UniqueID FROM #FullAddressSpliter WHERE RowID = @Counter

	WHILE LEN(@WorkingString) >0 
	BEGIN
		IF @AddressLineNo <4
		BEGIN
			SELECT @EndPosition			= REPLACE(CHARINDEX(@Delimiter,@WorkingString),0,LEN(@WorkingString))
			SELECT @ExtractionString	= SUBSTRING(@WorkingString,@StartPosition,@EndPosition)
			SELECT @WorkingString		= SUBSTRING(REPLACE(@WorkingString,@ExtractionString,''),3,LEN(@WorkingString))
			SELECT @InsertString		= REPLACE(LTRIM(RTRIM(@ExtractionString)),@Delimiter,'')
			SELECT @AddressLineNo		+= 1
		END
		ELSE
		BEGIN
			SELECT @InsertString	= SUBSTRING(@WorkingString,1,LEN(@WorkingString) -1)
			SELECT @AddressLineNo	+= 1
			SELECT @WorkingString	= NULL

		END

		INSERT INTO #AddressSplit (UniqueID, AddressLineNo, AddressLine)
		SELECT @UniqueID, @AddressLineNo, LTRIM(RTRIM(@InsertString))
		WHERE LEN(@InsertString) > 0

	END
	SELECT	 @Counter		+=1
			,@AddressLineNo =0
END

INSERT INTO #OrganisationsInsert (SanctionListId, UniqueID, OrganisationName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country)
SELECT	 1 --UK
		,ORG.UniqueID
		,ORG.OrganisationName
		,AS1.AddressLine AddressLine1
		,AS2.AddressLine AddressLine2
		,AS3.AddressLine AddressLine3
		,AS4.AddressLine AddressLine4
		,AS5.AddressLine AddressLine5
		,ORG.Country
FROM #Organisations ORG
LEFT JOIN #AddressSplit AS1 ON AS1.UniqueID = ORG.RowID AND AS1.AddressLineNo = 1
LEFT JOIN #AddressSplit AS2 ON AS2.UniqueID = ORG.RowID AND AS2.AddressLineNo = 2
LEFT JOIN #AddressSplit AS3 ON AS3.UniqueID = ORG.RowID AND AS3.AddressLineNo = 3
LEFT JOIN #AddressSplit AS4 ON AS4.UniqueID = ORG.RowID AND AS4.AddressLineNo = 4
LEFT JOIN #AddressSplit AS5 ON AS5.UniqueID = ORG.RowID AND AS5.AddressLineNo = 5
ORDER BY ORG.UniqueID

----------------------------------------------
--US Sanctions
------------------------------------------------
TRUNCATE TABLE #Organisations
INSERT INTO #Organisations (UniqueID, OrganisationName, OrganisationAlternativeName, FullAddress, Country, Remarks)
SELECT   CAST(UniqueID AS INT) UniqueID
		,CAST(LEFT(OrganisationName,255) AS VARCHAR(255)) OrganisationName
		,CAST(LEFT(OrganisationAlternativeName,255) AS VARCHAR(255)) OrganisationAlternativeName
		,CAST(LEFT(NULLIF(Address + CASE WHEN Address != '' AND CityStateProvincePostalCode != '' THEN ', ' ELSE '' END + CityStateProvincePostalCode,''),500) AS VARCHAR(500)) FullAddress
		,CAST(LEFT(Country,50) AS VARCHAR(50)) Country
		,CAST(Remarks AS VARCHAR(MAX)) Remarks
FROM	(
		SELECT   N.ent_num UniqueID
				,CASE N.SDN_Name WHEN '-0-' THEN NULL ELSE SUBSTRING(N.SDN_Name,2,LEN(N.SDN_Name) -2) END OrganisationName
				,CASE AN.alt_name WHEN '-0-' THEN NULL ELSE SUBSTRING(AN.alt_name,2,LEN(AN.alt_name) -2) END OrganisationAlternativeName
				,CASE A.Address WHEN '-0-' THEN '' ELSE SUBSTRING(A.Address,2,LEN(A.Address) -2) END Address
				,CASE A.CityStateProvincePostalCode WHEN '-0-' THEN '' ELSE SUBSTRING(A.CityStateProvincePostalCode ,2,LEN(A.CityStateProvincePostalCode) -2) END CityStateProvincePostalCode
				,CASE A.Country WHEN '-0-' THEN NULL ELSE SUBSTRING(A.Country,2,LEN(A.Country) -2) END Country
				,CASE N.remarks WHEN '-0-' THEN NULL ELSE SUBSTRING(N.remarks,2,LEN(N.remarks) -2) END Remarks	
		FROM [ETL].[US_SDN_Names] N
		LEFT JOIN [ETL].[US_SDN_Address] A ON A.[Ent_num] = N.[Ent_num]
		LEFT JOIN [ETL].[US_SDN_AlternativeNames] AN ON AN.[Ent_num] = N.[Ent_num]
		WHERE [SDN_Type] = '-0-' --Organisation
		) DATA
GROUP BY CAST(UniqueID AS INT)
		,CAST(LEFT(OrganisationName,255) AS VARCHAR(255))
		,CAST(LEFT(OrganisationAlternativeName,255) AS VARCHAR(255))
		,CAST(LEFT(NULLIF(Address + CASE WHEN Address != '' AND CityStateProvincePostalCode != '' THEN ', ' ELSE '' END + CityStateProvincePostalCode,''),500) AS VARCHAR(500))
		,CAST(LEFT(Country,50) AS VARCHAR(50))
		,CAST(Remarks AS VARCHAR(MAX))
ORDER BY CAST(UniqueID AS INT)

----------------------
--Address Split
----------------------
TRUNCATE TABLE #FullAddressSpliter
TRUNCATE TABLE #AddressSplit
INSERT INTO #FullAddressSpliter (UniqueID, FullAddress)
SELECT RowID, RTRIM(LTRIM(FullAddress + ',')) FullAddress
FROM #Organisations
WHERE FullAddress IS NOT NULL
GROUP BY RowID, FullAddress + ','
ORDER BY RowID
SELECT @InsertCount = @@ROWCOUNT

WHILE @Counter <= @InsertCount
BEGIN
	SELECT	 @WorkingString = FullAddress
			,@UniqueID		= UniqueID FROM #FullAddressSpliter WHERE RowID = @Counter

	WHILE LEN(@WorkingString) >0 
	BEGIN
		IF @AddressLineNo <4
		BEGIN
			SELECT @EndPosition			= REPLACE(CHARINDEX(@Delimiter,@WorkingString),0,LEN(@WorkingString))
			SELECT @ExtractionString	= SUBSTRING(@WorkingString,@StartPosition,@EndPosition)
			SELECT @WorkingString		= SUBSTRING(REPLACE(@WorkingString,@ExtractionString,''),3,LEN(@WorkingString))
			SELECT @InsertString		= REPLACE(LTRIM(RTRIM(@ExtractionString)),@Delimiter,'')
			SELECT @AddressLineNo		+= 1
		END
		ELSE
		BEGIN
			SELECT @InsertString	= SUBSTRING(@WorkingString,1,LEN(@WorkingString) -1)
			SELECT @AddressLineNo	+= 1
			SELECT @WorkingString	= NULL

		END

		INSERT INTO #AddressSplit (UniqueID, AddressLineNo, AddressLine)
		SELECT @UniqueID, @AddressLineNo, LTRIM(RTRIM(@InsertString))
		WHERE LEN(@InsertString) > 0

	END
	SELECT	 @Counter		+=1
			,@AddressLineNo =0
END

INSERT INTO #OrganisationsInsert (SanctionListId, UniqueID, OrganisationName, OrganisationAlternativeName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country)
SELECT	 2 --US
		,ORG.UniqueID
		,ORG.OrganisationName
		,ORG.OrganisationAlternativeName
		,AS1.AddressLine AddressLine1
		,AS2.AddressLine AddressLine2
		,AS3.AddressLine AddressLine3
		,AS4.AddressLine AddressLine4
		,AS5.AddressLine AddressLine5
		,ORG.Country
FROM #Organisations ORG
LEFT JOIN #AddressSplit AS1 ON AS1.UniqueID = ORG.RowID AND AS1.AddressLineNo = 1
LEFT JOIN #AddressSplit AS2 ON AS2.UniqueID = ORG.RowID AND AS2.AddressLineNo = 2
LEFT JOIN #AddressSplit AS3 ON AS3.UniqueID = ORG.RowID AND AS3.AddressLineNo = 3
LEFT JOIN #AddressSplit AS4 ON AS4.UniqueID = ORG.RowID AND AS4.AddressLineNo = 4
LEFT JOIN #AddressSplit AS5 ON AS5.UniqueID = ORG.RowID AND AS5.AddressLineNo = 5
ORDER BY ORG.UniqueID

----------------------
--Remarks Split
----------------------
INSERT INTO #RemarksSpliter (UniqueID, Remarks)
SELECT ORG.UniqueID, ORG.Remarks
FROM #Organisations ORG 
WHERE ORG.Remarks IS NOT NULL
GROUP BY ORG.UniqueID, ORG.Remarks
SELECT	 @InsertCount	= @@ROWCOUNT
		,@Counter		= 1
		,@Delimiter		= ';'

WHILE @Counter <= @InsertCount
BEGIN
	SELECT	 @WorkingString = Remarks
			,@UniqueID		= UniqueID FROM #RemarksSpliter WHERE RowID = @Counter

	WHILE LEN(@WorkingString) >0
	BEGIN
		SELECT @EndPosition			= REPLACE(CHARINDEX(@Delimiter,@WorkingString),0,LEN(@WorkingString))
		SELECT @ExtractionString	= SUBSTRING(@WorkingString,@StartPosition,@EndPosition)
		SELECT @WorkingString		= SUBSTRING(REPLACE(@WorkingString,@ExtractionString,''),3,LEN(@WorkingString))
		SELECT @InsertString		= REPLACE(LTRIM(RTRIM(@ExtractionString)),@Delimiter,'')

		INSERT INTO #RemarksSplit (UniqueID, Type, Remarks)
		SELECT @UniqueID, NULLIF(LTRIM(RTRIM(SUBSTRING(LTRIM(RTRIM(@InsertString)),1,CHARINDEX(' ',LTRIM(RTRIM(@InsertString)))))),''), LTRIM(RTRIM(@InsertString))
		WHERE LEN(@InsertString) > 0

	END
	SELECT @Counter +=1
END

UPDATE #RemarksSplit 
SET Type = CASE WHEN Type IN ('Telephone:','Phone') THEN 'Telephone' END
WHERE Type IN ('Telephone:','Phone')

DELETE #RemarksSplit
WHERE ISNULL(Type,'') NOT IN ('Telephone','Email','Website')

INSERT INTO #RemarksInsert (UniqueID, Type, Value)
SELECT UniqueID, Type, CASE WHEN Type IN ('Email','Website') THEN SUBSTRING(Value,1,ISNULL(NULLIF(CHARINDEX(' ',Value),0),LEN(Value))) ELSE Value END Value 
FROM	(
		SELECT ROW_NUMBER() OVER (PARTITION BY S.RowID ORDER BY LEN(FilterWord) DESC) RowNo, *, RTRIM(LTRIM(SUBSTRING(S.Remarks,LEN(FilterWord) + 2, LEN(S.Remarks) - LEN(FilterWord)))) Value
		FROM #RemarksSplit S
		LEFT JOIN	(
					SELECT 'Email Address' FilterWord UNION ALL
					SELECT 'Website' FilterWord UNION ALL
					SELECT 'Phone No.' FilterWord UNION ALL
					SELECT 'Phone' FilterWord UNION ALL
					SELECT 'Telephone' FilterWord UNION ALL
					SELECT 'Telephone No.' FilterWord UNION ALL
					SELECT 'Telephone Number:' FilterWord UNION ALL
					SELECT 'Email' FilterWord 
					) Filter ON Filter.FilterWord = LEFT(S.Remarks,LEN(Filter.FilterWord))
		) Clean
WHERE RowNo = 1
ORDER BY Type

-------------------------
--Insert Data
-------------------------
INSERT INTO [dbo].[Organisation] (SanctionListId, UniqueID, OrganisationName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country)
SELECT SanctionListId, UniqueID, OrganisationName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country
FROM #OrganisationsInsert
GROUP BY SanctionListId, UniqueID, OrganisationName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country
UNION ALL 
SELECT SanctionListId, UniqueID, OrganisationAlternativeName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country
FROM #OrganisationsInsert
WHERE OrganisationAlternativeName IS NOT NULL
GROUP BY SanctionListId, UniqueID, OrganisationAlternativeName, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country
ORDER BY SanctionListId, UniqueID

INSERT INTO [dbo].[Organisation2Email] (OrganisationId, EmailAddress)
SELECT O.Id, Value
FROM #RemarksInsert RI
INNER JOIN dbo.Organisation O ON O.UniqueID = RI.UniqueID AND O.SanctionListId = 2 --US
WHERE Type = 'Email'
GROUP BY O.Id, Value
ORDER BY O.Id

INSERT INTO [dbo].[Organisation2Telephone] (OrganisationId, Telephone)
SELECT O.Id, Value
FROM #RemarksInsert RI
INNER JOIN dbo.Organisation O ON O.UniqueID = RI.UniqueID AND O.SanctionListId = 2 --US
WHERE Type = 'Telephone'
GROUP BY O.Id, Value
ORDER BY O.Id

INSERT INTO [dbo].[Organisation2Website] (OrganisationId, Website)
SELECT O.Id, Value
FROM #RemarksInsert RI
INNER JOIN dbo.Organisation O ON O.UniqueID = RI.UniqueID AND O.SanctionListId = 2 --US
WHERE Type = 'Website'
GROUP BY O.Id, Value
ORDER BY O.Id

