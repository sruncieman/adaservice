﻿CREATE PROCEDURE [dbo].[PopulateIndividualAndLink]
AS
SET NOCOUNT ON

------------------------------------------------
--CreateObjectsDeclareVariablesTruncateTables
------------------------------------------------
DELETE [dbo].[Individual2NINumber]
DELETE [dbo].[Individual2Passport]
DELETE [dbo].[Individual]

DBCC CHECKIDENT ('dbo.Individual2NINumber', RESEED, 0) WITH NO_INFOMSGS;
DBCC CHECKIDENT ('dbo.Individual2Passport', RESEED, 0) WITH NO_INFOMSGS;
DBCC CHECKIDENT ('dbo.Individual', RESEED, 0) WITH NO_INFOMSGS;

IF OBJECT_ID('tempdb..#Individual') IS NOT NULL
	DROP TABLE #Individual
CREATE TABLE #Individual (RowID INT IDENTITY(1,1), UniqueID INT, LastName VARCHAR(255), FirstNames VARCHAR(500), AlternativeLastName VARCHAR(255), AlternativeFistNames VARCHAR(500), DateOfBirth DATE, FullAddress VARCHAR(500), Country VARCHAR(50), NINumber VARCHAR(1000), PassportDetails VARCHAR(1000), Remarks VARCHAR(MAX))

IF OBJECT_ID('tempdb..#FullAddressSpliter') IS NOT NULL
	DROP TABLE #FullAddressSpliter
CREATE TABLE #FullAddressSpliter (RowID INT IDENTITY(1,1), UniqueID INT, FullAddress VARCHAR(500))

IF OBJECT_ID('tempdb..#AddressSplit') IS NOT NULL
	DROP TABLE #AddressSplit
CREATE TABLE #AddressSplit (RowID INT IDENTITY(1,1), UniqueID INT, AddressLineNo INT ,AddressLine VARCHAR(500))

IF OBJECT_ID('tempdb..#RemarksSpliter') IS NOT NULL
	DROP TABLE #RemarksSpliter
CREATE TABLE #RemarksSpliter (RowID INT IDENTITY(1,1), SanctionListId INT, UniqueID INT, Remarks VARCHAR(MAX))

IF OBJECT_ID('tempdb..#RemarksSplit') IS NOT NULL
	DROP TABLE #RemarksSplit
CREATE TABLE #RemarksSplit (RowID INT IDENTITY(1,1), SanctionListId INT, UniqueID INT, Type VARCHAR(100), Remarks VARCHAR(MAX))

IF OBJECT_ID('tempdb..#IndividualInsert') IS NOT NULL
	DROP TABLE #IndividualInsert
CREATE TABLE #IndividualInsert(RowID INT IDENTITY(1,1), SanctionListId INT, UniqueID INT, LastName VARCHAR(255), FirstNames VARCHAR(500), AlternativeLastName VARCHAR(255), AlternativeFistNames VARCHAR(500), DateOfBirth DATE,  AddressLine1 VARCHAR(200), AddressLine2 VARCHAR(200), AddressLine3 VARCHAR(200), AddressLine4 VARCHAR(200), AddressLine5 VARCHAR(500), Country VARCHAR(50))

IF OBJECT_ID('tempdb..#RemarksInsert') IS NOT NULL
	DROP TABLE #RemarksInsert
CREATE TABLE #RemarksInsert (RowID INT IDENTITY(1,1), SanctionListId INT, UniqueID INT, Type VARCHAR(100), Value VARCHAR(200))

DECLARE  @InsertCount		INT
		,@Counter			INT = 1
		,@StartPosition		INT = 0
		,@EndPosition		INT
		,@WorkingString		VARCHAR(MAX) 
		,@Delimiter			VARCHAR = ','
		,@ExtractionString	VARCHAR(MAX)
		,@InsertString		VARCHAR(MAX) 
		,@UniqueID			INT
		,@AddressLineNo		INT = 0
		,@Number			VARCHAR(3) = 1

------------------------------------------------
--UK Sanctions
------------------------------------------------
SET DATEFORMAT DMY
INSERT INTO #Individual (UniqueID, LastName, FirstNames, DateOfBirth, FullAddress, Country, NINumber, PassportDetails, Remarks)
SELECT	 CAST(UniqueID AS INT) UniqueID
		,CAST(LEFT(LastName,255) AS VARCHAR(255)) LastName
		,CAST(LEFT(FirstNames,500) AS VARCHAR(500)) FirstNames
		,CASE WHEN ISDATE(DateOfBirth) = 1 THEN CAST(CONVERT(VARCHAR, DateOfBirth, 103) AS DATE) ELSE NULL END DateOfBirth
		,LTRIM(RTRIM(CASE WHEN FullAddress = '' THEN NULL ELSE CAST(LEFT(SUBSTRING(FullAddress,1,LEN(FullAddress)-1),500) AS VARCHAR(500)) END)) FullAddress
		,CAST(LEFT(Country,50) AS VARCHAR(50)) Country
		,CAST(NINumber AS VARCHAR(1000)) NINumber
		,CAST(PassportDetails AS VARCHAR(1000)) PassportDetails
		,CAST(Remarks AS VARCHAR(MAX)) Remarks
FROM	(
		SELECT	 GroupID UniqueID
				,Name6 LastName
				,DOB DateOfBirth
				,ISNULL(Name1 + ' ','') + ISNULL(Name2 + ' ','') + ISNULL(Name3 + ' ','') + ISNULL(Name4 + ' ','') + ISNULL(Name5 + ' ','') FirstNames
				,ISNULL(Address1 + ', ','') + ISNULL(Address2 + ', ','') + ISNULL(Address3 + ', ','') + ISNULL(Address4 + ', ','') + ISNULL(Address5 + ', ','') + ISNULL(Address6 + ', ','') + ISNULL(PostZipCode + ', ','') FullAddress
				,Country
				,NINumber
				,PassportDetails
				,OtherInformation Remarks
		FROM [ETL].[UK_ConsolidatedTargetList]
		WHERE GroupType = 'Individual'
		) DATA
GROUP BY CAST(UniqueID AS INT)
		,CAST(LEFT(LastName,255) AS VARCHAR(255))
		,CAST(LEFT(FirstNames,500) AS VARCHAR(500))
		,CASE WHEN ISDATE(DateOfBirth) = 1 THEN CAST(CONVERT(VARCHAR, DateOfBirth, 103) AS DATE) ELSE NULL END
		,CASE WHEN FullAddress = '' THEN NULL ELSE CAST(LEFT(SUBSTRING(FullAddress,1,LEN(FullAddress)-1),500) AS VARCHAR(500)) END
		,CAST(LEFT(Country,50) AS VARCHAR(50))
		,CAST(NINumber AS VARCHAR(1000)) 
		,CAST(PassportDetails AS VARCHAR(1000)) 
		,CAST(Remarks AS VARCHAR(MAX))
ORDER BY CAST(UniqueID AS INT)

----------------------
--Address Split
----------------------
INSERT INTO #FullAddressSpliter (UniqueID, FullAddress)
SELECT RowID, RTRIM(LTRIM(FullAddress + ',')) FullAddress
FROM #Individual
WHERE FullAddress IS NOT NULL
GROUP BY RowID, FullAddress + ','
ORDER BY RowID
SELECT @InsertCount = @@ROWCOUNT

WHILE @Counter <= @InsertCount
BEGIN
	SELECT	 @WorkingString = FullAddress
			,@UniqueID		= UniqueID FROM #FullAddressSpliter WHERE RowID = @Counter

	WHILE LEN(@WorkingString) >0 
	BEGIN
		IF @AddressLineNo <4
		BEGIN
			SELECT @EndPosition			= REPLACE(CHARINDEX(@Delimiter,@WorkingString),0,LEN(@WorkingString))
			SELECT @ExtractionString	= SUBSTRING(@WorkingString,@StartPosition,@EndPosition)
			SELECT @WorkingString		= SUBSTRING(REPLACE(@WorkingString,@ExtractionString,''),3,LEN(@WorkingString))
			SELECT @InsertString		= REPLACE(LTRIM(RTRIM(@ExtractionString)),@Delimiter,'')
			SELECT @AddressLineNo		+= 1
		END
		ELSE
		BEGIN
			SELECT @InsertString	= SUBSTRING(@WorkingString,1,LEN(@WorkingString) -1)
			SELECT @AddressLineNo	+= 1
			SELECT @WorkingString	= NULL

		END

		INSERT INTO #AddressSplit (UniqueID, AddressLineNo, AddressLine)
		SELECT @UniqueID, @AddressLineNo, LTRIM(RTRIM(@InsertString))
		WHERE LEN(@InsertString) > 0

	END
	SELECT	 @Counter		+=1
			,@AddressLineNo =0
END

INSERT INTO #IndividualInsert(SanctionListId, UniqueID, LastName, FirstNames, DateOfBirth,  AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country)
SELECT	 1 --UK
		,INV.UniqueID
		,INV.LastName
		,INV.FirstNames
		,INV.DateOfBirth
		,AS1.AddressLine AddressLine1
		,AS2.AddressLine AddressLine2
		,AS3.AddressLine AddressLine3
		,AS4.AddressLine AddressLine4
		,AS5.AddressLine AddressLine5
		,INV.Country
FROM #Individual INV
LEFT JOIN #AddressSplit AS1 ON AS1.UniqueID = INV.RowID AND AS1.AddressLineNo = 1
LEFT JOIN #AddressSplit AS2 ON AS2.UniqueID = INV.RowID AND AS2.AddressLineNo = 2
LEFT JOIN #AddressSplit AS3 ON AS3.UniqueID = INV.RowID AND AS3.AddressLineNo = 3
LEFT JOIN #AddressSplit AS4 ON AS4.UniqueID = INV.RowID AND AS4.AddressLineNo = 4
LEFT JOIN #AddressSplit AS5 ON AS5.UniqueID = INV.RowID AND AS5.AddressLineNo = 5
ORDER BY INV.UniqueID

--GetThePassportDetails
SELECT   @InsertCount		= NULL
		,@Counter			= 1
		,@StartPosition		= 0
		,@EndPosition		= NULL
		,@WorkingString		= NULL
		,@Number			= 1
		,@ExtractionString	= NULL
		,@InsertString		= NULL
		,@UniqueID			= NULL

--InsertTheDataWhereThereIsOnlyOneValue
INSERT INTO #RemarksSplit (SanctionListId, UniqueID, Type, Remarks)
SELECT 1, UniqueID, 'Passport', PassportDetails
FROM #Individual
WHERE PassportDetails NOT LIKE '([0-9])%'
GROUP BY UniqueID, PassportDetails

--WeNeedToLoopThroughTheMultipleDataList
INSERT INTO #RemarksSpliter (SanctionListId, UniqueID, Remarks)
SELECT 1, UniqueID, REPLACE(PassportDetails,'-','')
FROM #Individual
WHERE PassportDetails LIKE '([0-9])%'
GROUP BY UniqueID, REPLACE(PassportDetails,'-','')
SELECT @InsertCount = @@ROWCOUNT

WHILE @Counter <= @InsertCount
BEGIN
	SELECT	 @WorkingString = Remarks
			,@UniqueID		= UniqueID 
			,@Number		= 1	FROM #RemarksSpliter WHERE RowID = @Counter

	WHILE LEN(@WorkingString) >0
	BEGIN
		SELECT	 @StartPosition		= CHARINDEX('('+@Number+')',@WorkingString)
				,@Number +=1

		SELECT @EndPosition			= REPLACE(CHARINDEX('('+@Number+')',@WorkingString) -1,-1,LEN(@WorkingString))
		SELECT @ExtractionString	= SUBSTRING(@WorkingString,@StartPosition,@EndPosition)
		SELECT @WorkingString		= REPLACE(@WorkingString,@ExtractionString,'')
		SELECT @InsertString		= REPLACE(LTRIM(RTRIM(@ExtractionString)),'('+CAST(CAST(@Number AS INT) - 1 AS VARCHAR)+')','')

		INSERT INTO #RemarksSplit (SanctionListId, UniqueID, Type, Remarks)
		SELECT 1, @UniqueID, 'Passport', LTRIM(RTRIM(@InsertString))
		WHERE NOT EXISTS (SELECT TOP 1 1 FROM #RemarksSplit WHERE Remarks = @InsertString AND UniqueID = @UniqueID)
		AND LEN(@InsertString) > 0
		AND @InsertString NOT LIKE '([0-9])%'
	END
	SELECT @Counter +=1
END

--GetTheNIDetails
TRUNCATE TABLE #RemarksSpliter
SELECT   @InsertCount		= NULL
		,@Counter			= 1
		,@StartPosition		= 0
		,@EndPosition		= NULL
		,@WorkingString		= NULL
		,@Number			= 1
		,@ExtractionString	= NULL
		,@InsertString		= NULL
		,@UniqueID			= NULL

--InsertTheDataWhereThereIsOnlyOneValue
INSERT INTO #RemarksSplit (SanctionListId, UniqueID, Type, Remarks)
SELECT 1, UniqueID, 'NINumber', NINumber
FROM #Individual
WHERE NINumber NOT LIKE '([0-9])%'
GROUP BY UniqueID, NINumber

--WeNeedToLoopThroughTheMultipleDataList
INSERT INTO #RemarksSpliter (SanctionListId, UniqueID, Remarks)
SELECT 1, UniqueID, REPLACE(NINumber,'-','')
FROM #Individual
WHERE NINumber LIKE '([0-9])%'
GROUP BY UniqueID, REPLACE(NINumber,'-','')
SELECT @InsertCount = @@ROWCOUNT

WHILE @Counter <= @InsertCount
BEGIN
	SELECT	 @WorkingString = Remarks
			,@UniqueID		= UniqueID 
			,@Number		= 1	FROM #RemarksSpliter WHERE RowID = @Counter

	WHILE LEN(@WorkingString) >0
	BEGIN
		SELECT	 @StartPosition		= CHARINDEX('('+@Number+')',@WorkingString)
				,@Number +=1

		SELECT @EndPosition			= REPLACE(CHARINDEX('('+@Number+')',@WorkingString) -1,-1,LEN(@WorkingString))
		SELECT @ExtractionString	= SUBSTRING(@WorkingString,@StartPosition,@EndPosition)
		SELECT @WorkingString		= REPLACE(@WorkingString,@ExtractionString,'')
		SELECT @InsertString		= REPLACE(LTRIM(RTRIM(@ExtractionString)),'('+CAST(CAST(@Number AS INT) - 1 AS VARCHAR)+')','')

		INSERT INTO #RemarksSplit (SanctionListId, UniqueID, Type, Remarks)
		SELECT 1, @UniqueID, 'NINumber', LTRIM(RTRIM(@InsertString))
		WHERE NOT EXISTS (SELECT TOP 1 1 FROM #RemarksSplit WHERE Remarks = @InsertString AND UniqueID = @UniqueID)
		AND LEN(@InsertString) > 0
		AND @InsertString NOT LIKE '([0-9])%'
	END
	SELECT @Counter +=1
END

----------------------------------------------
--US Sanctions
------------------------------------------------
TRUNCATE TABLE #Individual
INSERT INTO #Individual (UniqueID, LastName, FirstNames, AlternativeLastName, AlternativeFistNames, FullAddress, Country, Remarks)
SELECT   CAST(UniqueID AS INT) UniqueID
		,CAST(LEFT(SUBSTRING(Name,1,CASE WHEN CHARINDEX(',',Name) = 0 THEN LEN(Name) ELSE CHARINDEX(',',Name) -1 END),255) AS VARCHAR(255)) LastName
		,CAST(LEFT(SUBSTRING(Name,CASE WHEN CHARINDEX(',',Name) = 0 THEN 1 ELSE CHARINDEX(',',Name) + 2 END, LEN(Name)),500) AS VARCHAR(500)) FirstNames
		,CAST(LEFT(SUBSTRING(AlternativeName,1,CASE WHEN CHARINDEX(',',AlternativeName) = 0 THEN LEN(AlternativeName) ELSE CHARINDEX(',',AlternativeName) -1 END),255) AS VARCHAR(255)) AlternativeLastName
		,CAST(LEFT(SUBSTRING(AlternativeName,CASE WHEN CHARINDEX(',',AlternativeName) = 0 THEN 1 ELSE CHARINDEX(',',AlternativeName) + 2 END, LEN(AlternativeName)),500) AS VARCHAR(500)) AlternativeFirstNames
		,CAST(LEFT(NULLIF(Address + CASE WHEN Address != '' AND CityStateProvincePostalCode != '' THEN ', ' ELSE '' END + CityStateProvincePostalCode,''),500) AS VARCHAR(500)) FullAddress
		,CAST(LEFT(Country,50) AS VARCHAR(50)) Country
		,CAST(Remarks AS VARCHAR(MAX)) Remarks
FROM	(
		SELECT   N.ent_num UniqueID
				,CASE N.SDN_Name WHEN '-0-' THEN NULL ELSE SUBSTRING(N.SDN_Name,2,LEN(N.SDN_Name) -2) END Name
				,CASE AN.alt_name WHEN '-0-' THEN NULL ELSE SUBSTRING(AN.alt_name,2,LEN(AN.alt_name) -2) END AlternativeName
				,CASE A.Address WHEN '-0-' THEN '' ELSE SUBSTRING(A.Address,2,LEN(A.Address) -2) END Address
				,CASE A.CityStateProvincePostalCode WHEN '-0-' THEN '' ELSE SUBSTRING(A.CityStateProvincePostalCode ,2,LEN(A.CityStateProvincePostalCode) -2) END CityStateProvincePostalCode
				,CASE A.Country WHEN '-0-' THEN NULL ELSE SUBSTRING(A.Country,2,LEN(A.Country) -2) END Country
				,CASE N.remarks WHEN '-0-' THEN NULL ELSE SUBSTRING(N.remarks,2,LEN(N.remarks) -2) END Remarks	
		FROM [ETL].[US_SDN_Names] N
		LEFT JOIN [ETL].[US_SDN_Address] A ON A.[Ent_num] = N.[Ent_num]
		LEFT JOIN [ETL].[US_SDN_AlternativeNames] AN ON AN.[Ent_num] = N.[Ent_num]
		WHERE [SDN_Type] = '"individual"'
		) DATA
GROUP BY CAST(UniqueID AS INT)
		,CAST(LEFT(Name,255) AS VARCHAR(255))
		,CAST(LEFT(SUBSTRING(Name,1,CASE WHEN CHARINDEX(',',Name) = 0 THEN LEN(Name) ELSE CHARINDEX(',',Name) -1 END),255) AS VARCHAR(255))
		,CAST(LEFT(SUBSTRING(Name,CASE WHEN CHARINDEX(',',Name) = 0 THEN 1 ELSE CHARINDEX(',',Name) + 2 END, LEN(Name)),500) AS VARCHAR(500))
		,CAST(LEFT(SUBSTRING(AlternativeName,1,CASE WHEN CHARINDEX(',',AlternativeName) = 0 THEN LEN(AlternativeName) ELSE CHARINDEX(',',AlternativeName) -1 END),255) AS VARCHAR(255))
		,CAST(LEFT(SUBSTRING(AlternativeName,CASE WHEN CHARINDEX(',',AlternativeName) = 0 THEN 1 ELSE CHARINDEX(',',AlternativeName) + 2 END, LEN(AlternativeName)),500) AS VARCHAR(500))
		,CAST(LEFT(NULLIF(Address + CASE WHEN Address != '' AND CityStateProvincePostalCode != '' THEN ', ' ELSE '' END + CityStateProvincePostalCode,''),500) AS VARCHAR(500))
		,CAST(LEFT(Country,50) AS VARCHAR(50))
		,CAST(Remarks AS VARCHAR(MAX)) 
ORDER BY CAST(UniqueID AS INT)

----------------------
--Address Split
----------------------
TRUNCATE TABLE #FullAddressSpliter
TRUNCATE TABLE #AddressSplit
INSERT INTO #FullAddressSpliter (UniqueID, FullAddress)
SELECT RowID, RTRIM(LTRIM(FullAddress + ',')) FullAddress
FROM #Individual
WHERE FullAddress IS NOT NULL
GROUP BY RowID, FullAddress + ','
ORDER BY RowID
SELECT @InsertCount = @@ROWCOUNT

SELECT   @Counter		= 0
		,@StartPosition	= 0

WHILE @Counter <= @InsertCount
BEGIN
	SELECT	 @WorkingString = FullAddress
			,@UniqueID		= UniqueID FROM #FullAddressSpliter WHERE RowID = @Counter

	WHILE LEN(@WorkingString) >0 
	BEGIN
		IF @AddressLineNo <4
		BEGIN
			SELECT @EndPosition			= REPLACE(CHARINDEX(@Delimiter,@WorkingString),0,LEN(@WorkingString))
			SELECT @ExtractionString	= SUBSTRING(@WorkingString,@StartPosition,@EndPosition)
			SELECT @WorkingString		= SUBSTRING(REPLACE(@WorkingString,@ExtractionString,''),3,LEN(@WorkingString))
			SELECT @InsertString		= REPLACE(LTRIM(RTRIM(@ExtractionString)),@Delimiter,'')
			SELECT @AddressLineNo		+= 1
		END
		ELSE
		BEGIN
			SELECT @InsertString	= SUBSTRING(@WorkingString,1,LEN(@WorkingString) -1)
			SELECT @AddressLineNo	+= 1
			SELECT @WorkingString	= NULL

		END

		INSERT INTO #AddressSplit (UniqueID, AddressLineNo, AddressLine)
		SELECT @UniqueID, @AddressLineNo, LTRIM(RTRIM(@InsertString))
		WHERE LEN(@InsertString) > 0

	END
	SELECT	 @Counter		+=1
			,@AddressLineNo =0
END

INSERT INTO #IndividualInsert(SanctionListId, UniqueID, LastName, FirstNames, AlternativeLastName, AlternativeFistNames, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country)
SELECT	 2 --UK
		,INV.UniqueID
		,INV.LastName
		,INV.FirstNames
		,INV.AlternativeLastName
		,INV.AlternativeFistNames
		,AS1.AddressLine AddressLine1
		,AS2.AddressLine AddressLine2
		,AS3.AddressLine AddressLine3
		,AS4.AddressLine AddressLine4
		,AS5.AddressLine AddressLine5
		,INV.Country
FROM #Individual INV
LEFT JOIN #AddressSplit AS1 ON AS1.UniqueID = INV.RowID AND AS1.AddressLineNo = 1
LEFT JOIN #AddressSplit AS2 ON AS2.UniqueID = INV.RowID AND AS2.AddressLineNo = 2
LEFT JOIN #AddressSplit AS3 ON AS3.UniqueID = INV.RowID AND AS3.AddressLineNo = 3
LEFT JOIN #AddressSplit AS4 ON AS4.UniqueID = INV.RowID AND AS4.AddressLineNo = 4
LEFT JOIN #AddressSplit AS5 ON AS5.UniqueID = INV.RowID AND AS5.AddressLineNo = 5
ORDER BY INV.UniqueID

----------------------
--Remarks Split
----------------------
TRUNCATE TABLE #RemarksSpliter
INSERT INTO #RemarksSpliter (SanctionListId, UniqueID, Remarks)
SELECT 2, INV.UniqueID, INV.Remarks
FROM #Individual INV 
WHERE INV.Remarks IS NOT NULL
GROUP BY INV.UniqueID, INV.Remarks

SELECT	 @InsertCount	= @@ROWCOUNT
		,@Counter		= 1
		,@Delimiter		= ';'
		,@StartPosition		= 0
		

WHILE @Counter <= @InsertCount
BEGIN
	SELECT	 @WorkingString = Remarks
			,@UniqueID		= UniqueID FROM #RemarksSpliter WHERE RowID = @Counter

	WHILE LEN(@WorkingString) >0
	BEGIN
		SELECT @EndPosition			= REPLACE(CHARINDEX(@Delimiter,@WorkingString),0,LEN(@WorkingString))
		SELECT @ExtractionString	= SUBSTRING(@WorkingString,@StartPosition,@EndPosition)
		SELECT @WorkingString		= SUBSTRING(REPLACE(@WorkingString,@ExtractionString,''),3,LEN(@WorkingString))
		SELECT @InsertString		= REPLACE(LTRIM(RTRIM(@ExtractionString)),@Delimiter,'')

		INSERT INTO #RemarksSplit (SanctionListId, UniqueID, Type, Remarks)
		SELECT 2, @UniqueID, NULLIF(LTRIM(RTRIM(SUBSTRING(LTRIM(RTRIM(@InsertString)),1,CHARINDEX(' ',LTRIM(RTRIM(@InsertString)))))),''), LTRIM(RTRIM(@InsertString))
		WHERE LEN(@InsertString) > 0
		AND NULLIF(LTRIM(RTRIM(SUBSTRING(LTRIM(RTRIM(@InsertString)),1,CHARINDEX(' ',LTRIM(RTRIM(@InsertString)))))),'') IN ('DOB','Passport')

	END
	SELECT @Counter +=1
END

INSERT INTO #RemarksInsert (SanctionListId, UniqueID, Type, Value)
SELECT  SanctionListId, UniqueID, Type, REPLACE(REPLACE(Value,'.',''),',','')
FROM	(
		SELECT *, SUBSTRING(REPLACE(Remarks,'Passport ',''),1,CASE WHEN CHARINDEX(' ',REPLACE(Remarks,'Passport ','')) = 0 THEN LEN(REPLACE(Remarks,'Passport ','')) ELSE CHARINDEX(' ',REPLACE(Remarks,'Passport ','')) END) Value
		FROM #RemarksSplit
		WHERE Type IN ('Passport','NINumber')
		) Data
WHERE LEN(Value) >=4
AND Value LIKE '%[0-9]%'

INSERT INTO #RemarksInsert (SanctionListId, UniqueID, Type, Value)
SELECT SanctionListId, UniqueID, Type, CASE WHEN ISDATE(REPLACE(Remarks,'DOB ','')) = 1 THEN CAST(CONVERT(VARCHAR(20),REPLACE(Remarks,'DOB ',''),103) AS DATE) ELSE NULL END Value
FROM #RemarksSplit
WHERE Type IN ('DOB')

-------------------------
--Insert Data
-------------------------
INSERT INTO [dbo].[Individual] (SanctionListId, UniqueID, LastName, FirstNames, DateOfBirth, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country)
SELECT SanctionListId, UniqueID, LastName, FirstNames, DateOfBirth, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country
FROM #IndividualInsert
GROUP BY SanctionListId, UniqueID, LastName, FirstNames, DateOfBirth, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country
UNION ALL
SELECT SanctionListId, UniqueID, AlternativeLastName, AlternativeFistNames, DateOfBirth, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country
FROM #IndividualInsert
WHERE AlternativeLastName IS NOT NULL OR AlternativeFistNames IS NOT NULL
GROUP BY SanctionListId, UniqueID, AlternativeLastName, AlternativeFistNames, DateOfBirth, AddressLine1, AddressLine2, AddressLine3, AddressLine4, AddressLine5, Country

--UpdateTheDOBFortheUS
UPDATE I
SET DateOfBirth = Value
FROM Individual I
INNER JOIN #RemarksInsert RS ON RS.UniqueID = I.UniqueID
WHERE I.SanctionListId = 2 --US
AND RS.Value = 'DOB'

INSERT INTO [dbo].[Individual2NINumber] (IndividualId, NINumber)
SELECT I.ID, Value
FROM #RemarksInsert RI
INNER JOIN  Individual I ON I.UniqueID = RI.UniqueID AND I.SanctionListId = RI.SanctionListId
WHERE Type = 'NINumber'
GROUP BY I.ID, Value
ORDER BY I.ID

INSERT INTO [dbo].[Individual2Passport] (IndividualId, Passport)
SELECT I.ID, Value
FROM #RemarksInsert RI
INNER JOIN  Individual I ON I.UniqueID = RI.UniqueID AND I.SanctionListId = RI.SanctionListId
WHERE Type = 'Passport'
GROUP BY I.ID, Value
ORDER BY I.ID


