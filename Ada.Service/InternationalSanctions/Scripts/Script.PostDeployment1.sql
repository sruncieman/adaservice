﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

SET IDENTITY_INSERT [dbo].[SanctionList] ON 

GO
INSERT [dbo].[SanctionList] ([Id], [Country], [Name], [URL]) VALUES (1, N'United Kingdom', N'Consolidated list of targets', N'https://www.gov.uk/government/publications/financial-sanctions-consolidated-list-of-targets/consolidated-list-of-targets')
GO
INSERT [dbo].[SanctionList] ([Id], [Country], [Name], [URL]) VALUES (2, N'United States of America', N'Specially Designated Nationals List (SDN) ', N'http://www.treasury.gov/resource-center/sanctions/SDN-List/Pages/default.aspx')
GO
SET IDENTITY_INSERT [dbo].[SanctionList] OFF
GO