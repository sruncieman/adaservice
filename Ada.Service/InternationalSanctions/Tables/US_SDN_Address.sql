﻿CREATE TABLE [ETL].[US_SDN_Address] (
    [Ent_num]                     NVARCHAR (MAX) NULL,
    [Add_num]                     NVARCHAR (MAX) NULL,
    [Address]                     NVARCHAR (MAX) NULL,
    [CityStateProvincePostalCode] NVARCHAR (MAX) NULL,
    [Country]                     NVARCHAR (MAX) NULL,
    [Add_remarks]                 NVARCHAR (MAX) NULL
);

