﻿CREATE TABLE [dbo].[Organisation2Website] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [OrganisationId] INT           NULL,
    [Website]        VARCHAR (200) NULL,
    CONSTRAINT [PK_Organisation2Website_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Organisation2Website_Organisation_Id] FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id])
);

