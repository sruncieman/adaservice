﻿CREATE TABLE [dbo].[SanctionList] (
    [Id]      INT           IDENTITY (1, 1) NOT NULL,
    [Country] VARCHAR (100) NULL,
    [Name]    VARCHAR (100) NULL,
    [URL]     VARCHAR (200) NULL,
    CONSTRAINT [PK_SanctionList_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

