﻿CREATE TABLE [ETL].[US_SDN_Names] (
    [ent_num]    NVARCHAR (MAX) NULL,
    [SDN_Name]   NVARCHAR (MAX) NULL,
    [SDN_Type]   NVARCHAR (MAX) NULL,
    [Program]    NVARCHAR (MAX) NULL,
    [Title]      NVARCHAR (MAX) NULL,
    [Call_Sign]  NVARCHAR (MAX) NULL,
    [Vess_type]  NVARCHAR (MAX) NULL,
    [Tonnage]    NVARCHAR (MAX) NULL,
    [GRT]        NVARCHAR (MAX) NULL,
    [Vess_flag]  NVARCHAR (MAX) NULL,
    [Vess_owner] NVARCHAR (MAX) NULL,
    [remarks]    NVARCHAR (MAX) NULL
);

