﻿CREATE TABLE [dbo].[Organisation2Email] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [OrganisationId] INT           NULL,
    [EmailAddress]   VARCHAR (200) NULL,
    CONSTRAINT [PK_Organisation2Email_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Organisation2Email_Organisation_Id] FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id])
);

