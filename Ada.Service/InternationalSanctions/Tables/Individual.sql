﻿CREATE TABLE [dbo].[Individual] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [SanctionListId] INT           NULL,
    [UniqueID]       INT           NULL,
    [LastName]       VARCHAR (255) NULL,
    [FirstNames]     VARCHAR (500) NULL,
    [DateOfBirth]    DATE          NULL,
    [AddressLine1]   VARCHAR (200) NULL,
    [AddressLine2]   VARCHAR (200) NULL,
    [AddressLine3]   VARCHAR (200) NULL,
    [AddressLine4]   VARCHAR (200) NULL,
    [AddressLine5]   VARCHAR (500) NULL,
    [Country]        VARCHAR (50)  NULL,
    CONSTRAINT [PK_Individual_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Individual_SanctionList_Id] FOREIGN KEY ([SanctionListId]) REFERENCES [dbo].[SanctionList] ([Id])
);

