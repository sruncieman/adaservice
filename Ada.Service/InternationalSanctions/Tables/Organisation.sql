﻿CREATE TABLE [dbo].[Organisation] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [SanctionListId]   INT           NULL,
    [UniqueID]         INT           NULL,
    [OrganisationName] VARCHAR (255) NULL,
    [AddressLine1]     VARCHAR (200) NULL,
    [AddressLine2]     VARCHAR (200) NULL,
    [AddressLine3]     VARCHAR (200) NULL,
    [AddressLine4]     VARCHAR (200) NULL,
    [AddressLine5]     VARCHAR (500) NULL,
    [Country]          VARCHAR (50)  NULL,
    CONSTRAINT [PK_Organisation_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Organisation_SanctionList_Id] FOREIGN KEY ([SanctionListId]) REFERENCES [dbo].[SanctionList] ([Id])
);

