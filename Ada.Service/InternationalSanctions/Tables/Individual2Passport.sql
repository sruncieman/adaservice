﻿CREATE TABLE [dbo].[Individual2Passport] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [IndividualId] INT           NULL,
    [Passport]     VARCHAR (200) NULL,
    CONSTRAINT [PK_Individual2Passport_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Individual2Passport_Individual_Id] FOREIGN KEY ([IndividualId]) REFERENCES [dbo].[Individual] ([Id])
);

