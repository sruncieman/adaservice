﻿CREATE TABLE [ETL].[US_SDN_AlternativeNames] (
    [ent_num]     NVARCHAR (MAX) NULL,
    [alt_num]     NVARCHAR (MAX) NULL,
    [alt_type]    NVARCHAR (MAX) NULL,
    [alt_name]    NVARCHAR (MAX) NULL,
    [alt_remarks] NVARCHAR (MAX) NULL
);

