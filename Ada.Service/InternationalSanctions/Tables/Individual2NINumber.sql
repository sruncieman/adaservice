﻿CREATE TABLE [dbo].[Individual2NINumber] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [IndividualId] INT           NULL,
    [NINumber]     VARCHAR (200) NULL,
    CONSTRAINT [PK_Individual2NINumber_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Individual2NINumber_Individual_Id] FOREIGN KEY ([IndividualId]) REFERENCES [dbo].[Individual] ([Id])
);

