﻿CREATE TABLE [dbo].[Organisation2Telephone] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [OrganisationId] INT           NULL,
    [Telephone]      VARCHAR (200) NULL,
    CONSTRAINT [PK_Organisation2Telephone_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Organisation2Telephone_Organisation_Id] FOREIGN KEY ([OrganisationId]) REFERENCES [dbo].[Organisation] ([Id])
);

