﻿using MDA.Pipeline.Model;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using MDA.MappingService.Interface;
using System.Collections.Generic;
using System;
using MDA.Common;
using MDA.Common.Server;
using MDA.MappingService;
using System.Runtime.Serialization.Formatters.Binary;

namespace MDA.MappingService.Cue.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        /// <summary>
        /// Convert Cue format to ClaimBatch.  Simply deserialise the file. It if fails it's broken. 
        /// </summary>
        /// <param name="ctx">Current Context containing DB connection to use</param>
        /// <param name="fs">FileStream containing the file to be loaded</param>
        /// <param name="statusTracking"></param>
        /// <param name="processClaim">CallBack function to call for each PipelineClaim created</param>
        /// <param name="ShowProgress">Show Progress callback</param>
        /// <param name="processingResults">Structure to hold ALL processing results</param>
        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking,
                                                Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn,
                                                out ProcessingResults processingResults)
        {
            fs.Seek(0, SeekOrigin.Begin);

            PipelineClaimBatch ret = new PipelineClaimBatch();

            processingResults = new ProcessingResults("Mapping");

            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                fs.Position = 0;
                object o = bf.Deserialize(fs);

                // This OBJECT is exactly what was returned from CUE. So MAP it as normal and call ProcessClaimFn() for every claim found!!

                //XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                //DataContractSerializer ser = new DataContractSerializer(typeof(MDA.Common.FileModel.ClaimBatch));

                //MDA.Common.FileModel.ClaimBatch cb = (MDA.Common.FileModel.ClaimBatch)ser.ReadObject(reader, true);

                //MDA.Pipeline.Model.Translator tr = new MDA.Pipeline.Model.Translator();

                //foreach (var c in cb.Claims)
                //    if (ProcessClaimFn(ctx, tr.Translate(c), statusTracking) == -1) return;
            }
            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Failed to recognise XML file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

    }
}

