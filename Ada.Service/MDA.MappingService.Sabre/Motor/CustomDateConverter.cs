﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.Sabre.Motor
{
    public class CustomDateConverter : ConverterBase
    {
        public override object StringToField(string value)
        {
            DateTime dt;

            if (DateTime.TryParseExact(value.Replace("\"",""), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
                return dt;

            return null;
        }
    }
}
