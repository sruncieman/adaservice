﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.Sabre.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord("|")]
    public class VehicleData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Client_Reference_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Party_Type;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Relationship_Type;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Vehicle_Identifier;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Vehicle_Coverage;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Vehicle_Added_Date;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Vehicle_Class;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Vehicle_Storage;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)] 
        public String Foreign_Registered;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Green_Card_Reference;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Vehicle_Type;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Vehicle_Registration_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Vehicle_Make;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)] 
        public String Vehicle_Model;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Vehicle_Date_Of_Theft;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Vehicle_Date_Scrapped;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)] 
        public String Value_of_Insured_Vehicle;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Vehicle_Use;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String DVLA_Reg_Owner;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String DVLA_Reg_Address;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Blame_code;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)] [FieldOptional] [FieldTrim(TrimMode.Both)]
        public String Feed_type;
    }
}
