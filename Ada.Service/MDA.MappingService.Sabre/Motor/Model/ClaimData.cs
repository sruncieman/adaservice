﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.Sabre.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord("|")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Feed_Type;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Policy_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Claim_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Closed_Claim_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Info_Only_Flag;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime? Start_Date;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Start_Time;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime? Claim_Incident_Date;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Claim_Incident_Time;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Policy_Change_Date;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)] 
        public String Status;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)] 
        public String Status_Change_Date;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)] 
        public String Product_Code;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Product_Description;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Policy_Group;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Policy_Type;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Brand;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Broker;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Agent;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Handler;
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Premium;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Premium_Payment_Method;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Policy_Value;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Source_Of_Business;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime? Policy_End_Date;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Term;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Multi_Policy_Holders;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String IP_Address;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Previous_Insurer;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Claim_Type_Description;

        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Claim_Type_Code;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Claim_Current_Estimate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Claim_Total_Paid;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Claim_Recovery_Estimate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Claim_Total_Recovered;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Date_Claim_Settled;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Cancellation_Reason;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Incident_Address_1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Incident_Address_2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Incident_Address_3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Incident_Address_4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Incident_Address_5;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Incident_Address_6;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Incident_Address_7;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Incident_Address_8;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Incident_Address_9;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Incident_Address_10;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Accident_Circumstance;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Accident_Grade;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Party_Type;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Contact_Reference;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Title;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Forename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Initials;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Surname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Alias_Forename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Alias_Surname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime? Date_Of_Birth;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Gender;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Nationality;
       
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Country_Of_Birth;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Deceased_Indicator;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Date_of_Death;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Company_Name;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Driving_License_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String License_Type;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String NI_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Passport_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Marital_Status;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Previous_Surname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Mothers_Maiden_Name;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Income;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Employment_Type_Description;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Employment_Description;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String No_Claims_Discount;
       
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String No_Claims_Years;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)] 
        public String Number_Of_Motoring_Convictions;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Number_Of_Non_Motoring_Convictions;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String PI_Flag;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String PI_Code;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String PI_Realisation_Date;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Bank_Sort_Code;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Bank_Account_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Address_Type;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Address_1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Address_2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Address_3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Address_4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Address_5;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Address_6;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Address_7;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Address_8;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Address_9;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Address_10;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String From_Date;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String To_Date;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Residential_Term;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Residential_Status;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Fax_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Home_Telephone;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Work_Telephone;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Mobile_Telephone;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Home_Email_Address;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Work_Email_Address;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Police_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Police_Crime_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Policy_Branch;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Voluntary_Excess;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Compulsory_Excess;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Relationship;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Relationship_Type;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Large_Loss_Indicator;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)][FieldOptional][FieldTrim(TrimMode.Both)]
        public String Driving_Experience;
    }
}
