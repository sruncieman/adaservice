﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common;
using MDA.Pipeline.Model;
using MDA.MappingService.Interface;
using MDA.Common.Server;
using MDA.MappingService;
using System.Xml.Serialization;

namespace MDA.MappingService.Sabre.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        private string _tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx">Current Context containing DB connection to use</param>
        /// <param name="fs">FileStream containing the file to be loaded</param>
        /// <param name="statusTracking"></param>
        /// <param name="processClaim">CallBack function to call for each PipelineClaim created</param>
        /// <param name="ShowProgress">Show Progress callback</param>
        /// <param name="processingResults">Structure to hold ALL processing results</param>
        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking,
                                        Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn,
                                        out ProcessingResults processingResults)
        {
            string clientFolder = null;

            GetFilePath(out clientFolder, ctx.RiskClientId);
            
            processingResults = new ProcessingResults("Mapping");

            try
            {
                GetMotorClaimBatch claimBatch = new GetMotorClaimBatch();

                bool DeleteUploadedFiles = Convert.ToBoolean(ConfigurationManager.AppSettings["DeleteUploadedFiles"]);

                claimBatch.RetrieveMotorClaimBatch(ctx, fs, clientFolder, DeleteUploadedFiles, statusTracking, ProcessClaimFn, processingResults);
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping Sabre file. " + ex.Message);
                    ex = ex.InnerException;
                } 

                return;
            }
        }

        private void GetFilePath(out string clientFolder, int clientId)
        {
            clientFolder = _tempFolder + "\\" + clientId;

            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
        }

        public override void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard, int settleTime, int batchSize, string postOp, string archiveFolder, Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {

            // Create structure to hold our new batch of claims
            List<object> batch = new List<object>();

            int batchCount = 0;
            string firstFileName = null;

            // Get a list of the files in the folderPath filtered by the wildCard. Search the top level only (don't go into child directories)
            string[] files = Directory.GetFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

            // If we find none, return
            if (files.Count() == 0) return;

            // If ANY files in that list have been modified in the last "settleTime" minutes return.  The directory is not stable, files may still being
            // written.  Return now and wait for the next check
            if (files.Where(x => File.GetCreationTime(x).AddMinutes(settleTime) > DateTime.Now).Any())
                return;

            // If we ge here we have files to process
            foreach (var file in files)
            {
                // Create a filename to use for this batch.  We will just use the first file name in this case
                if (firstFileName == null)
                    firstFileName = Path.GetFileName(file);

                // Add this new claim to the batch
                batch.Add(file);

                #region Process all the options we invented for our postOp functionality

                // We are passed the string we entered into config.  Now we have to code it!
                // In this case we have implemented RENAME, DELETE, ARCHIVE
                // ARCHIVE checks and (if needed) creates the destination folder. It also checks if the file already exists
                // in the destination folder (which would break the MOVE) and (if needed) adds a random string to make it unique
                switch (postOp.ToUpper())
                {
                    case "RENAME":
                        File.Move(file, file + ".old");
                        break;

                    case "DELETE":
                        File.Delete(file);
                        break;

                    case "ARCHIVE":
                        if (!Directory.Exists(archiveFolder))
                            Directory.CreateDirectory(archiveFolder);   // Create destination if needed

                        string fname = archiveFolder + "\\" + Path.GetFileName(file);  // build dest path

                        if (File.Exists(fname))  // Check if it already exists
                        {
                            fname += "." + Path.GetRandomFileName();  // Adds a random string to end of filename
                        }

                        File.Move(file, fname);
                        break;
                }
                #endregion

                #region If Batch full process it

                if (batch.Count >= batchSize)
                {
                    batchCount++;

                    #region Serialise our batch into a stream
                    MemoryStream stream = new MemoryStream();

                    var ser = new XmlSerializer(typeof(List<object>));

                    ser.Serialize(stream, batch);

                    stream.Position = 0;
                    #endregion

                    #region Process the batch (call the callback function)
                    string fileName = firstFileName;
                    string mimeType = "zip";
                    string batchRef = "Batch" + batchCount;

                    processClientBatch(ctx, stream, fileName, mimeType, "Motor", batchRef);
                    #endregion

                    // Setup for a new batch and go round the loop again
                    batch.Clear();
                    firstFileName = null;
                }
                #endregion

            }

            #region Process the part full batch left at the end. Lazy cut & paste!!
            if (batch.Count > 0)
            {
                batchCount++;

                MemoryStream stream = new MemoryStream();

                var ser = new XmlSerializer(typeof(List<object>));

                ser.Serialize(stream, batch);

                stream.Position = 0;

                string fileName = firstFileName;
                string mimeType = "zip";
                string batchRef = "Batch" + batchCount;

                processClientBatch(ctx, stream, fileName, mimeType, "Motor", batchRef);

                batch.Clear();
            }
            #endregion



        }
    }

    //public class SabreBigFileMapping : ITableMappingService
    //{
    //    private string _tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];

    //    public PipelineClaimBatch ConvertToClaimBatch(CurrentContext ctx, Stream fs, out ProcessingResults processingResults, Func<CurrentContext, PipelineClaim, int> ProcessClaim)
    //    {
    //        string clientFolder = null;

    //        GetFilePath(out clientFolder, ctx.riskClientId);

    //        processingResults = new ProcessingResults("Mapping");

    //        try
    //        {
    //            GetMotorClaimBatch claimBatch = new GetMotorClaimBatch();

    //            bool DeleteUploadedFiles = Convert.ToBoolean(ConfigurationManager.AppSettings["DeleteUploadedFiles"]);

    //            return claimBatch.RetrieveMotorClaimBatch(ctx, fs, clientFolder, DeleteUploadedFiles, ProcessClaim);
    //        }

    //        catch (Exception ex)
    //        {
    //            processingResults.Message.AddError("Error occurred mapping Sabre file. " + ex.Message);

    //            return null;
    //        }
    //    }

    //    private void GetFilePath(out string clientFolder, int clientId)
    //    {
    //        clientFolder = _tempFolder + "\\" + clientId;

    //        if (!Directory.Exists(clientFolder))
    //            Directory.CreateDirectory(clientFolder);
    //    }

        
    //}
}
