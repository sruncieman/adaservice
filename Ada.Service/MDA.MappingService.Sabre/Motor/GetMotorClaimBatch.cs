﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using FileHelpers;
using Ionic.Zip;
using MDA.Pipeline.Model;
using MDA.MappingService.Sabre.Motor.Model;
using MDA.Common.Enum;
using System.Linq;
using MDA.Common.Server;
using MDA.Common;

namespace MDA.MappingService.Sabre.Motor
{
    public class GetMotorClaimBatch
    {
        #region Initialise Fields
        private string _coreClaimDataFile;
        private string _vehicleClaimDataFile;
        private Stopwatch stopwatch;
        private bool _debug;
        private FileHelperEngine _coreClaimDataEngine;
        private FileHelperEngine _vehicleClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private VehicleData[] _vehicleData;
        private List<string> _uniqueClaimNumberList;
        private IEnumerable<VehicleData> vehicleRows;
        private string _claimCode;
        private VehicleData _topVehicleRow;
        private IEnumerable<ClaimData> _policyItemRows;
        private IEnumerable<ClaimData> _claimItemRows;
        private string _filePath = null;
        private bool _deleteExtractedFiles;
        private int _claimCount = 0;
        private string _clientFolder;

        #endregion

        public GetMotorClaimBatch()
        {
            
            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _vehicleClaimDataEngine = new FileHelperEngine(typeof(VehicleData));
            _uniqueClaimNumberList = new List<string>();
        }

        public void RetrieveMotorClaimBatch(CurrentContext ctx, Stream filem, string clientFolder, bool deleteExtractedFiles,
                                                        object statusTracking,
                                                        Func<CurrentContext, MDA.Pipeline.Model.PipelineMotorClaim, object, int> ProcessClaimFn,
                                                        ProcessingResults processingResults)
        {
            _clientFolder = clientFolder;

            #region Extract Files from Zip

            if (ctx.ShowProgress != null)
                ctx.ShowProgress(ProgressSeverity.Info, "Before Unzip", true);

            //Stream coreStream = new MemoryStream();
            //Stream vehicleStream = new MemoryStream();



            GetFilesFromZip(ctx, filem, clientFolder);

            if (ctx.ShowProgress != null)
                ctx.ShowProgress(ProgressSeverity.Info, "After Unzip", true);

            #endregion

            //PipelineClaimBatch cb = new PipelineClaimBatch();

            if (ctx.ShowProgress != null)
                ctx.ShowProgress(ProgressSeverity.Info, "Initialise Files : ", false);

            InitialiseFiles(ctx, filem);

            if (ctx.ShowProgress != null)
            {
                ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                ctx.ShowProgress(ProgressSeverity.Info, "Populate File Helpers : ", false);
            }

            PopulateFileHelpersEngines(ctx, clientFolder);

            if (ctx.ShowProgress != null)
            {
                ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                ctx.ShowProgress(ProgressSeverity.Info, "Converting Claims Starting", true);
            }

            ConvertToClaimBatch(ctx, statusTracking, ProcessClaimFn, processingResults);

            if (ctx.ShowProgress != null)
            {
                ctx.ShowProgress(ProgressSeverity.Info, "Converting Claims Complete : ", true);
            }

            if (_debug)
            {
                stopwatch.Stop();
                GetTimeSpan(stopwatch);
            }

            //return cb;
        }

        private void GetFilesFromZip(CurrentContext ctx, Stream filem, string clientFolder)
        {
            if (filem != null)
            {
                try
                {
                    _debug = false;

                    //Stream targetStream = null;

                    //_filePath = string.Format(@"{0}\{1}-{2}{3}", clientFolder, "Sabre", DateTime.Now.ToString("ddMMyyyyHHmmss"), ".zip");

                    //if (ctx.ShowProgress != null)
                    //    ctx.ShowProgress(ProgressSeverity.Info, "Reading from FilePath:" + _filePath + ":", false);

                    //using (targetStream = new FileStream(_filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                    //{
                    //    //read from the input stream in 65000 byte chunks
                    //    const int bufferLen = 65000;
                    //    byte[] buffer = new byte[bufferLen];
                    //    int count = 0;
                    //    while ((count = filem.Read(buffer, 0, bufferLen)) > 0)
                    //    {
                    //        // save to output stream
                    //        targetStream.Write(buffer, 0, count);
                    //    }
                    //    targetStream.Close();
                    //    filem.Close();
                    //}

                    //if (ctx.ShowProgress != null)
                    //    ctx.ShowProgress(ProgressSeverity.Info, "Done", true);

                    using (ZipFile zip1 = ZipFile.Read(filem)) //_filePath))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            var extractTmpPath = string.Concat(clientFolder+@"\"+e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);
                            
                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);


                            string unpackDirectory = clientFolder;
                            e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                    }
                }

                catch (Exception ex)
                {
                    if (ctx.ShowProgress != null)
                        ctx.ShowProgress(ProgressSeverity.Info, ": Error in extraction of files : " + ex, true);

                    throw new CustomException("Error in extraction of files: " + ex);
                }
            }

            DeleteExtractedFiles(clientFolder, _deleteExtractedFiles);
        }

        private void InitialiseFiles(CurrentContext ctx, Stream filem)
        {
            if (filem == null)
            {
                _coreClaimDataFile = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Sabre\Resource\SABR_Core.txt";
                _vehicleClaimDataFile = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Sabre\Resource\SABR_Vehi.txt";

                //_coreClaimDataFile = @"C:\Dev\Projects\MDA\MDASolution\MDA.Mapping.Sabre\Resource\Claim Test File.txt";
                //_vehicleClaimDataFile = @"C:\Dev\Projects\MDA\MDASolution\MDA.Mapping.Sabre\Resource\Vehicle Test File.txt";

                _debug = true;
            }
            else
            {
                try
                {
                    _coreClaimDataFile = _clientFolder + @"\SABR_Core.txt";
                    _vehicleClaimDataFile = _clientFolder + @"\SABR_Vehi.txt";
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in assigning extracted files: " + ex);
                }
            }

        }

        private void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            try
            {
                if (_debug)
                {
                    stopwatch = new Stopwatch();
                    Console.WriteLine("Start");
                    stopwatch.Start();
                }

                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Populate File Helper Engine (core) : ", false);
                }

                _coreClaimData = _coreClaimDataEngine.ReadFile(_coreClaimDataFile) as ClaimData[];
                
                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                    ctx.ShowProgress(ProgressSeverity.Info, "Populate File Helper Engine (vehicles) : ", false);
                }

                _vehicleData = _vehicleClaimDataEngine.ReadFile(_vehicleClaimDataFile) as VehicleData[];

                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                }

                
                DeleteExtractedFiles(clientFolder, _deleteExtractedFiles);
            }

            catch (Exception ex)
            {
                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "PopulateFileHelpersEngines Error : " + ex.Message, true);
                }

                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
        }

        private void ConvertToClaimBatch(CurrentContext ctx, object statusTracking,
                                                        Func<CurrentContext, MDA.Pipeline.Model.PipelineMotorClaim, object, int> ProcessClaimFn,
                                                        ProcessingResults processingResults)
        {

            try
            {
                //_uniqueClaimNumberList.Add("4440011");
                //_uniqueClaimNumberList.Add("584900");
                //_uniqueClaimNumberList.Add("653139");
                //_uniqueClaimNumberList.Add("652998");
                //_uniqueClaimNumberList.Add("549964");
                //_uniqueClaimNumberList.Add("587948");
                _uniqueClaimNumberList = _coreClaimData.Where(x => x.Claim_Number != "").Select(x => x.Claim_Number).ToList();

                // Get Distinct claim numbers
                if (_uniqueClaimNumberList != null)
                    _uniqueClaimNumberList = _uniqueClaimNumberList
                        .GroupBy(i => i)
                        .Select(g => g.First())
                        .ToList();

                _claimCount = _uniqueClaimNumberList.Count();

            }

            catch (Exception ex)
            {
                throw new CustomException("Error in filtering records: " + ex);
            }

            int count = 0;
            foreach (var _uniqueClaimRowItem in _uniqueClaimNumberList)
            {
                bool PI_Flag = false;

                if (_debug)
                    ShowPercentProgress("Processing...", count, _claimCount);
            
                PipelineMotorClaim claim = new PipelineMotorClaim();

                // claim and policy records
                _claimItemRows = _coreClaimData.Where(x => x.Claim_Number == _uniqueClaimRowItem);
                var topItemRow = _coreClaimData.Where(x => x.Claim_Number == _uniqueClaimRowItem).First();

                var distinctPolicyNumber = _claimItemRows.Select(x => x.Policy_Number).FirstOrDefault();

                _policyItemRows =
                    _coreClaimData.Where(x => x.Feed_Type == "POLICY" && x.Policy_Number == distinctPolicyNumber);

                vehicleRows =
                    _vehicleData.Where(
                        x => x.Client_Reference_Number == distinctPolicyNumber && x.Party_Type == "PLH0001");

                if (!vehicleRows.Any())
                {
                    vehicleRows =
                        _vehicleData.Where(
                            x => x.Client_Reference_Number == distinctPolicyNumber && x.Party_Type == "CLM0001");
                }

                if (vehicleRows.Any())
                    _topVehicleRow = vehicleRows.First();

                claim.ClaimNumber = _uniqueClaimRowItem;
                claim.IncidentDate = (DateTime)topItemRow.Start_Date; //Claim_Incident_Date;

                #region Claim Info

                if (!string.IsNullOrEmpty(topItemRow.Status))
                {
                    switch (topItemRow.Status.ToUpper())
                    {
                        case "OPENED":
                            claim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                            break;
                        case "FINALISED":
                            claim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                            break;
                        case "RE-OPENED":
                            claim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                            break;
                        default:
                            claim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                            break;
                    }
                }

                #region ClaimCode

                if (!string.IsNullOrEmpty(topItemRow.Claim_Type_Description))
                {
                    _claimCode = topItemRow.Claim_Type_Description;
                }

                foreach (var claimItem in _claimItemRows.Where(x => x.Forename != null && x.Surname != null))
                {
                    if (claimItem.PI_Flag != "N" && !string.IsNullOrEmpty(claimItem.PI_Flag))
                    {
                        PI_Flag = true;
                        break;
                    }
                           
                }

                if (PI_Flag)
                {
                    _claimCode += "; PI;";

                    foreach (var claimItem in _claimItemRows.Where(x => x.Forename != null && x.Surname != null))
                    {
                        if (claimItem.PI_Flag == "Y")
                        {
                            if(!_claimCode.Contains(claimItem.PI_Code))
                                _claimCode += " " + claimItem.PI_Code + ";";
                        }
                    }

                    int length = _claimCode.Length;

                    _claimCode = _claimCode.Substring(0, length - 1);
                }

                if (!string.IsNullOrEmpty(_claimCode))
                    claim.ExtraClaimInfo.ClaimCode = _claimCode;

                #endregion


                if (!string.IsNullOrEmpty(topItemRow.Accident_Circumstance))
                    claim.ExtraClaimInfo.IncidentCircumstances = topItemRow.Accident_Circumstance;

                if (!string.IsNullOrEmpty(topItemRow.Claim_Current_Estimate))
                {
                    decimal number;
                    if (Decimal.TryParse(topItemRow.Claim_Current_Estimate, out number))
                    {
                        claim.ExtraClaimInfo.Reserve = number;
                    }
                }

                if (!string.IsNullOrEmpty(topItemRow.Claim_Total_Paid))
                {
                    decimal number;
                    if (Decimal.TryParse(topItemRow.Claim_Total_Paid, out number))
                    {
                        claim.ExtraClaimInfo.PaymentsToDate = number;
                    }
                }

                #endregion

                #region Policy

                if (!string.IsNullOrEmpty(topItemRow.Policy_Type))
                {
                    switch (topItemRow.Policy_Type.ToUpper())
                    {
                        case "PRIVATE CAR":
                            claim.Policy.PolicyType_Id = (int)PolicyType.PersonalMotor;
                            break;
                        case "COMMERCIAL VEHICLE":
                        case "TAXI":
                            claim.Policy.PolicyType_Id = (int)PolicyType.CommercialMotor;
                            break;
                        default:
                            claim.Policy.PolicyType_Id = (int)PolicyType.Unknown;
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(topItemRow.Policy_Number))
                    claim.Policy.PolicyNumber = topItemRow.Policy_Number;

                if (!string.IsNullOrEmpty(topItemRow.Brand))
                    claim.Policy.Insurer = topItemRow.Brand;

                if (!string.IsNullOrEmpty(topItemRow.Broker))
                    claim.Policy.Broker = topItemRow.Broker;

                if (_topVehicleRow != null)
                {
                    if (!string.IsNullOrEmpty(_topVehicleRow.Vehicle_Coverage))
                    {
                        switch (_topVehicleRow.Vehicle_Coverage.ToUpper())
                        {
                            case "COMP":
                                claim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.Comprehensive;
                                break;
                            case "TPFT":
                                claim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.ThirdPartyFireAndTheft;
                                break;
                            case "TPO":
                                claim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.ThirdPartyOnly;
                                break;
                            default:
                                claim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.Unknown;
                                break;
                        }
                    }
                }

                claim.Policy.PolicyStartDate = topItemRow.Start_Date;
                claim.Policy.PolicyEndDate = topItemRow.Policy_End_Date;


                #endregion

                #region Insured Vehicle

                PipelineVehicle insuredVehicle = new PipelineVehicle();

                if (_topVehicleRow != null)
                {
                    if (!string.IsNullOrEmpty(_topVehicleRow.Vehicle_Class))
                    {
                        switch (_topVehicleRow.Vehicle_Class.ToUpper())
                        {
                            case "PC":
                                insuredVehicle.VehicleType_Id = (int)VehicleType.Car;
                                break;
                            case "PU":
                                insuredVehicle.VehicleType_Id = (int)VehicleType.Pickup;
                                break;
                            case "CV":
                            case "VN":
                                insuredVehicle.VehicleType_Id = (int)VehicleType.Van;
                                break;
                            default:
                                insuredVehicle.VehicleType_Id = (int)VehicleType.Unknown;
                                break;
                        }
                    }

                    if (!string.IsNullOrEmpty(_topVehicleRow.Vehicle_Registration_Number))
                        insuredVehicle.VehicleRegistration = _topVehicleRow.Vehicle_Registration_Number;

                    if (!string.IsNullOrEmpty(_topVehicleRow.Vehicle_Make))
                        insuredVehicle.VehicleMake = _topVehicleRow.Vehicle_Make;

                    if (!string.IsNullOrEmpty(_topVehicleRow.Vehicle_Model))
                        insuredVehicle.VehicleModel = _topVehicleRow.Vehicle_Model;
                }
                claim.Vehicles.Add(insuredVehicle);

                #endregion

                #region Add Vehicle Person (Policyholder AND/OR insured driver)

                // find PolicyHolder and/insured driver
                var policyHolderRow =  _policyItemRows.FirstOrDefault(x=>x.Party_Type.ToUpper() == "PLH0001");
                var insuredDriverRow = _claimItemRows.FirstOrDefault(x => x.Party_Type.ToUpper() == "CLM0001");

                #region Missing policyHolder Items 

                var missingPolicyHolderClaimItems = _policyItemRows.FirstOrDefault(x => policyHolderRow != null && (x.Policy_Number == policyHolderRow.Policy_Number && x.Forename == policyHolderRow.Forename && x.Surname == policyHolderRow.Surname && x.Date_Of_Birth == policyHolderRow.Date_Of_Birth));

                // check if items are missing
                if (missingPolicyHolderClaimItems != null)
                {
                    if (string.IsNullOrEmpty(policyHolderRow.Employment_Type_Description) &&
                        !string.IsNullOrEmpty(missingPolicyHolderClaimItems.Employment_Type_Description))
                        policyHolderRow.Employment_Type_Description =
                            missingPolicyHolderClaimItems.Employment_Type_Description;

                    if (string.IsNullOrEmpty(policyHolderRow.Employment_Description) &&
                        !string.IsNullOrEmpty(missingPolicyHolderClaimItems.Employment_Description))
                        policyHolderRow.Employment_Description = missingPolicyHolderClaimItems.Employment_Description;

                    if (string.IsNullOrEmpty(policyHolderRow.Home_Telephone) &&
                        !string.IsNullOrEmpty(missingPolicyHolderClaimItems.Home_Telephone))
                        policyHolderRow.Home_Telephone = missingPolicyHolderClaimItems.Home_Telephone;

                    if (string.IsNullOrEmpty(policyHolderRow.Address_Type) &&
                        !string.IsNullOrEmpty(missingPolicyHolderClaimItems.Address_Type))
                        policyHolderRow.Address_Type = missingPolicyHolderClaimItems.Address_Type;
                }

                #endregion

                #region Missing insured driver Items

                var missingInsuredDriverClaimItems =
                 _policyItemRows.FirstOrDefault(x => insuredDriverRow != null && (x.Policy_Number == insuredDriverRow.Policy_Number && x.Forename == insuredDriverRow.Forename
                                                                                  && x.Surname == insuredDriverRow.Surname && x.Date_Of_Birth == insuredDriverRow.Date_Of_Birth));


                // check if items are missing
                if (missingInsuredDriverClaimItems != null)
                {
                    if (string.IsNullOrEmpty(insuredDriverRow.Employment_Type_Description) &&
                        !string.IsNullOrEmpty(missingInsuredDriverClaimItems.Employment_Type_Description))
                        insuredDriverRow.Employment_Type_Description =
                            missingInsuredDriverClaimItems.Employment_Type_Description;

                    if (string.IsNullOrEmpty(insuredDriverRow.Employment_Description) &&
                        !string.IsNullOrEmpty(missingInsuredDriverClaimItems.Employment_Description))
                        insuredDriverRow.Employment_Description = missingInsuredDriverClaimItems.Employment_Description;

                    if (string.IsNullOrEmpty(insuredDriverRow.Home_Telephone) &&
                        !string.IsNullOrEmpty(missingInsuredDriverClaimItems.Home_Telephone))
                        insuredDriverRow.Home_Telephone = missingInsuredDriverClaimItems.Home_Telephone;

                    if (string.IsNullOrEmpty(insuredDriverRow.Address_Type) &&
                        !string.IsNullOrEmpty(missingInsuredDriverClaimItems.Address_Type))
                        insuredDriverRow.Address_Type = missingInsuredDriverClaimItems.Address_Type;
                }

                #endregion



                ////bool policyholderExists = false;
                ////bool insuredDriverExists = false;

                //if (policyHolderRow != null)
                //{
                //    if (!string.IsNullOrEmpty(policyHolderRow.Forename) &&
                //        !string.IsNullOrEmpty(policyHolderRow.Surname))
                //        policyholderExists = true;
                //}

                ////if (insuredDriverExists != null)
                ////{
                ////    if (!string.IsNullOrEmpty(insuredDriverRow.Forename) &&
                ////        !string.IsNullOrEmpty(insuredDriverRow.Surname))
                ////    {
                ////        insuredDriverExists = true;
                ////    }

                ////}

                //if (policyholderExists && insuredDriverExists)
                //if (policyholderExists)
                //{
                    
                if(policyHolderRow!=null && insuredDriverRow!=null)
                {
                    if (policyHolderRow.Forename == insuredDriverRow.Forename &&
                        policyHolderRow.Surname == insuredDriverRow.Surname &&
                        policyHolderRow.Date_Of_Birth == insuredDriverRow.Date_Of_Birth)
                    {
                        // policyholder is the same as insured driver so only insured driver should be added
                        claim = AddDriver(claim, insuredDriverRow, PartyType.Insured);
                    }
                    else
                    {
                        // policyholder is not the same as insured driver so both should be added
                        claim = AddDriver(claim, insuredDriverRow, PartyType.Insured);
                        claim = AddDriver(claim, policyHolderRow, PartyType.Policyholder);
                    }
                }

                if (policyHolderRow != null && insuredDriverRow == null)
                {
                    claim = AddDriver(claim, policyHolderRow, PartyType.Policyholder);
                }


                if (policyHolderRow == null && insuredDriverRow == null)
                {
                    claim = AddDriver(claim, insuredDriverRow, PartyType.Insured);
                }


                if (policyHolderRow == null && insuredDriverRow != null)
                {
                    claim = AddDriver(claim, insuredDriverRow, PartyType.Insured);
                }

                #endregion

               

                #region ThirdParty

                    PipelineVehicle _noVehicleInvolved = new PipelineVehicle();
                foreach (var claimItem in _claimItemRows)
                {
                    _noVehicleInvolved.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;
                    PipelinePerson thirdPartyPerson = new PipelinePerson();
                    PipelineOrganisation claimOrganisation = new PipelineOrganisation();
                    PipelineAddress organisationAddress = new PipelineAddress();
                    organisationAddress.AddressType_Id = (int)AddressLinkType.TradingAddress;
                    string partyTypeAbbrev = null;

                    if(!string.IsNullOrEmpty(claimItem.Party_Type))
                        partyTypeAbbrev = claimItem.Party_Type.Substring(0, 3);

                    if (string.IsNullOrEmpty(claimItem.Company_Name) && (partyTypeAbbrev == "THP" || partyTypeAbbrev == "CLR"))
                    {
                        #region Third Party Person

                        switch (partyTypeAbbrev)
                        {
                            case "THP":
                                thirdPartyPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                break;
                            case "CLR":
                                thirdPartyPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                                break;
                        }

                        if (!string.IsNullOrEmpty(claimItem.Forename))
                            thirdPartyPerson.FirstName = claimItem.Forename;

                        if (!string.IsNullOrEmpty(claimItem.Surname))
                            thirdPartyPerson.LastName = claimItem.Surname;

                        if (!string.IsNullOrEmpty(claimItem.Gender))
                        {
                            switch (claimItem.Gender.ToUpper())
                            {
                                case "M":
                                    thirdPartyPerson.Gender_Id = (int)Gender.Male;
                                    break;
                                case "F":
                                    thirdPartyPerson.Gender_Id = (int)Gender.Female;
                                    break;
                                default:
                                    thirdPartyPerson.Gender_Id = (int)Gender.Unknown;
                                    break;
                            }
                        }

                        thirdPartyPerson.DateOfBirth = claimItem.Date_Of_Birth;

                        if (!string.IsNullOrEmpty(claimItem.Employment_Type_Description))
                            thirdPartyPerson.Occupation = claimItem.Employment_Type_Description;

                        if (!string.IsNullOrEmpty(claimItem.Employment_Description))
                            thirdPartyPerson.Occupation += " - " + claimItem.Employment_Description;

                        if (!string.IsNullOrEmpty(claimItem.Home_Telephone))
                            thirdPartyPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = claimItem.Home_Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                            //thirdPartyPerson.LandlineTelephone = claimItem.Home_Telephone;

                        if (!string.IsNullOrEmpty(claimItem.Work_Telephone))
                            thirdPartyPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = claimItem.Work_Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                            //thirdPartyPerson.WorkTelephone = claimItem.Work_Telephone;

                        if (!string.IsNullOrEmpty(claimItem.Mobile_Telephone))
                            thirdPartyPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = claimItem.Mobile_Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                            //thirdPartyPerson.MobileTelephone = claimItem.Mobile_Telephone;

                        if (!string.IsNullOrEmpty(claimItem.Home_Email_Address))
                            thirdPartyPerson.EmailAddresses.Add(new PipelineEmail() { EmailAddress = claimItem.Home_Email_Address });
                            //thirdPartyPerson.EmailAddress = claimItem.Home_Email_Address;

                        #endregion

                        #region Third Party Address
                        PipelineAddress thirdPartyAddress = new PipelineAddress();

                        if (claimItem.Address_Type.ToUpper() == "CURRNT")
                            thirdPartyAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                        if (!string.IsNullOrEmpty(claimItem.Address_1))
                            thirdPartyAddress.SubBuilding = claimItem.Address_1;

                        if (!string.IsNullOrEmpty(claimItem.Address_2))
                            thirdPartyAddress.SubBuilding += " " + claimItem.Address_2;

                        if (!string.IsNullOrEmpty(claimItem.Address_3))
                            thirdPartyAddress.SubBuilding += " " + claimItem.Address_3;

                        if (!string.IsNullOrEmpty(claimItem.Address_4))
                            thirdPartyAddress.Street = claimItem.Address_4;

                        if (!string.IsNullOrEmpty(claimItem.Address_5))
                            thirdPartyAddress.Locality = claimItem.Address_5;

                        if (!string.IsNullOrEmpty(claimItem.Address_6))
                            thirdPartyAddress.Town = claimItem.Address_6;

                        if (!string.IsNullOrEmpty(claimItem.Address_7))
                            thirdPartyAddress.County = claimItem.Address_7;

                        if (!string.IsNullOrEmpty(claimItem.Address_8))
                            thirdPartyAddress.PostCode = claimItem.Address_8;

                        if (!string.IsNullOrEmpty(claimItem.Address_9))
                            thirdPartyAddress.Building = claimItem.Address_9;

                        if (!string.IsNullOrEmpty(claimItem.Address_10))
                            thirdPartyAddress.Building += " " + claimItem.Address_10;
                        
                        #endregion

                        //if (!string.IsNullOrEmpty(thirdPartyPerson.FirstName) &&
                        //    !string.IsNullOrEmpty(thirdPartyPerson.LastName))
                        //{
                        if (thirdPartyAddress.Street != null || thirdPartyAddress.SubBuilding != null || thirdPartyAddress.Building != null || thirdPartyAddress.BuildingNumber != null || thirdPartyAddress.Locality != null || thirdPartyAddress.PostCode != null || thirdPartyAddress.County != null || thirdPartyAddress.Town != null)
                             thirdPartyPerson.Addresses.Add(thirdPartyAddress);

                            if (partyTypeAbbrev == "CLR"
                                && string.IsNullOrEmpty(claimItem.Company_Name)
                                && string.IsNullOrEmpty(thirdPartyPerson.FirstName)
                                && string.IsNullOrEmpty(thirdPartyPerson.LastName))
                            {
                                // don't add
                            }
                            else
                            {
                                _noVehicleInvolved.People.Add(thirdPartyPerson);
                            }

                        //}
                    }

                    if (!string.IsNullOrEmpty(claimItem.Company_Name))
                    {
                      #region Organisation

                        claimOrganisation.OrganisationName = claimItem.Company_Name;
                        if (!string.IsNullOrEmpty(claimItem.Home_Telephone))
                            claimOrganisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = claimItem.Home_Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                            //claimOrganisation.Telephone1 = claimItem.Home_Telephone;

                      #endregion

                      #region Organisation Address

                      if (claimItem.Address_Type.ToUpper() == "CURRNT")
                          organisationAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                      if (!string.IsNullOrEmpty(claimItem.Address_1))
                          organisationAddress.SubBuilding = claimItem.Address_1;

                      if (!string.IsNullOrEmpty(claimItem.Address_2))
                          organisationAddress.SubBuilding += " " + claimItem.Address_2;

                      if (!string.IsNullOrEmpty(claimItem.Address_3))
                          organisationAddress.SubBuilding += " " + claimItem.Address_3;

                      if (!string.IsNullOrEmpty(claimItem.Address_4))
                          organisationAddress.Street = claimItem.Address_4;

                      if (!string.IsNullOrEmpty(claimItem.Address_5))
                          organisationAddress.Locality = claimItem.Address_5;

                      if (!string.IsNullOrEmpty(claimItem.Address_6))
                          organisationAddress.Town = claimItem.Address_6;

                      if (!string.IsNullOrEmpty(claimItem.Address_7))
                          organisationAddress.County = claimItem.Address_7;

                      if (!string.IsNullOrEmpty(claimItem.Address_8))
                          organisationAddress.PostCode = claimItem.Address_8;

                      if (!string.IsNullOrEmpty(claimItem.Address_9))
                          organisationAddress.Building = claimItem.Address_9;

                      if (!string.IsNullOrEmpty(claimItem.Address_10))
                          organisationAddress.Building += " " + claimItem.Address_10;

                      #endregion

                      if (!string.IsNullOrEmpty(claimOrganisation.OrganisationName))
                      {
                          if (organisationAddress.Street != null || organisationAddress.SubBuilding != null || organisationAddress.Building != null || organisationAddress.BuildingNumber != null || organisationAddress.Locality != null || organisationAddress.PostCode != null || organisationAddress.County != null || organisationAddress.Town != null)
                              claimOrganisation.Addresses.Add(organisationAddress);

                          claim.Organisations.Add(claimOrganisation);
                      }
                    }
                }

                if (_noVehicleInvolved.People.Any())
                    claim.Vehicles.Add(_noVehicleInvolved);

                #endregion

                #region Insured Vehicle and Driver Check

                bool insuredVehicleCheck = false;
                bool insuredDriverCheck = false;

                foreach (var vehicle in claim.Vehicles)
                {
                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                    {
                        insuredVehicleCheck = true;

                        foreach (var person in vehicle.People)
                        {
                            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                            {
                                insuredDriverCheck = true;
                            }
                        }
                    }
                }

                if (insuredVehicleCheck == false)
                {
                    PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                    defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                    }

                    claim.Vehicles.Add(defaultInsuredVehicle);
                }
                else
                {
                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        foreach (var vehicle in claim.Vehicles)
                        {
                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                            {

                                vehicle.People.Add(defaultInsuredDriver);

                            }

                        }

                    }
                }

                #endregion


                //Console.WriteLine(claim.ClaimNumber);

                if (ProcessClaimFn(ctx, claim, statusTracking) == -1) return;

                count++;
            }

            //return cb;
        }
       
        private PipelineMotorClaim AddDriver(PipelineMotorClaim claim, ClaimData claimData, PartyType driverType)
        {

            #region Driver

            PipelinePerson driver = new PipelinePerson();

            switch (driverType)
            {
                case PartyType.Policyholder:
                    driver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Policyholder;
                    driver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    break;
                case PartyType.Insured:
                    driver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    driver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    break;
            }
            
            if (claimData != null)
            {

            if (!string.IsNullOrEmpty(claimData.Forename))
                driver.FirstName = claimData.Forename;

            if (!string.IsNullOrEmpty(claimData.Surname))
                driver.LastName = claimData.Surname;

            if (!string.IsNullOrEmpty(claimData.Gender))
            {
                switch (claimData.Gender.ToUpper())
                {
                    case "M":
                        driver.Gender_Id = (int)Gender.Male;
                        break;
                    case "F":
                        driver.Gender_Id = (int)Gender.Female;
                        break;
                    default:
                        driver.Gender_Id = (int)Gender.Unknown;
                        break;
                }
            }

            driver.DateOfBirth = claimData.Date_Of_Birth;
            if (!string.IsNullOrEmpty(claimData.Employment_Type_Description))
                driver.Occupation = claimData.Employment_Type_Description;

            if (!string.IsNullOrEmpty(claimData.Employment_Description))
                driver.Occupation += " - " + claimData.Employment_Description;

            if (!string.IsNullOrEmpty(claimData.Home_Telephone))
                driver.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = claimData.Home_Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                //driver.LandlineTelephone = claimData.Home_Telephone;

            if (!string.IsNullOrEmpty(claimData.Work_Telephone))
                driver.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = claimData.Work_Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                //driver.WorkTelephone = claimData.Work_Telephone;

            if (!string.IsNullOrEmpty(claimData.Mobile_Telephone))
                driver.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = claimData.Mobile_Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                //driver.MobileTelephone = claimData.Mobile_Telephone;

            if (!string.IsNullOrEmpty(claimData.Home_Email_Address))
                driver.EmailAddresses.Add(new PipelineEmail() { EmailAddress = claimData.Home_Email_Address });
                //driver.EmailAddress = claimData.Home_Email_Address;

            #endregion

            #region Address

            PipelineAddress address = new PipelineAddress();

            if (claimData.Address_Type.ToUpper() == "CURRNT")
                address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

            if (!string.IsNullOrEmpty(claimData.Address_1))
                address.SubBuilding = claimData.Address_1;

            if (!string.IsNullOrEmpty(claimData.Address_2))
                address.SubBuilding += " " + claimData.Address_2;

            if (!string.IsNullOrEmpty(claimData.Address_3))
                address.SubBuilding += " " + claimData.Address_3;

            if (!string.IsNullOrEmpty(claimData.Address_4))
                address.Street = claimData.Address_4;

            if (!string.IsNullOrEmpty(claimData.Address_5))
                address.Locality = claimData.Address_5;

            if (!string.IsNullOrEmpty(claimData.Address_6))
                address.Town = claimData.Address_6;

            if (!string.IsNullOrEmpty(claimData.Address_7))
                address.County = claimData.Address_7;

            if (!string.IsNullOrEmpty(claimData.Address_8))
                address.PostCode = claimData.Address_8;

            if (!string.IsNullOrEmpty(claimData.Address_9))
                address.Building = claimData.Address_9;

            if (!string.IsNullOrEmpty(claimData.Address_10))
                address.Building += " " + claimData.Address_10;

            #endregion

                if (!string.IsNullOrEmpty(claimData.Address_1) || !string.IsNullOrEmpty(claimData.Address_2) || !string.IsNullOrEmpty(claimData.Address_3) || !string.IsNullOrEmpty(claimData.Address_4) || !string.IsNullOrEmpty(claimData.Address_5) || !string.IsNullOrEmpty(claimData.Address_6) || !string.IsNullOrEmpty(claimData.Address_7) || !string.IsNullOrEmpty(claimData.Address_8)  || !string.IsNullOrEmpty(claimData.Address_9) || !string.IsNullOrEmpty(claimData.Address_10))
            driver.Addresses.Add(address);

            claim.Vehicles[0].People.Add(driver);
            }
            
            return claim;
        }

        private void DeleteExtractedFiles(string clientFolder, bool _deleteExtractedFiles)
        {
            if (_deleteExtractedFiles)
            {
                try
                {
                    string[] filePaths = Directory.GetFiles(clientFolder);
                    foreach (string file in filePaths)
                    {
                        File.Delete(file);
                    }
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in deleting extracted files: " + ex);
                }
            }
        }

        private void ShowPercentProgress(string message, int currentCount, int totalCount)
        {
            int percent = (100 * (currentCount + 1)) / totalCount;
            Console.Write("\r{0}{1}% complete ", message, percent);
        }

        private void GetTimeSpan(Stopwatch stopwatch)
        {
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopwatch.Elapsed;

            // Format and display the TimeSpan value. 
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                               ts.Hours, ts.Minutes, ts.Seconds,
                                               ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
            Console.WriteLine("Finish");

            if (_debug)
                Console.ReadLine();
        }
    }
}
