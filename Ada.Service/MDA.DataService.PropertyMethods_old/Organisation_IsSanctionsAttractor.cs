﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public List<Organisation_IsSanctionsAttractor_Result> Organisation_IsSanctionsAttractor(EntityAliases organisationAliases, int organisationId, string cacheKey, MessageCache messageCache, bool _trace)
        {
            List<int> organisationsId = _dataServices.FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o in _db.Organisations.AsNoTracking()
                    where organisationsId.Contains(o.Id)
                    && o.SanctionList.Length > 0
                    && o.RecordStatus == (byte)ADARecordStatus.Current
                    select new Organisation_IsSanctionsAttractor_Result()
                    {
                        OrganisationName = o.OrganisationName
                    }).ToList();
        }

    }
}