﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using MDA.Common.Helpers;
using LinqKit;
using MDA.Common.Debug;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {

        public int Person_NumberOfIncidents(int excludeIncidentId, int riskClaimId, int personId, DateTime incidentDate, int partyType, int subPartyType, EntityAliases personAliases, string incidentSource, string incidentType, string periodSkip, string periodCount, bool? isPotentialClaimant, /*string searchDate,*/ bool _trace)
        {

            PropertyDateRange dateRange = new PropertyDateRange(incidentDate, periodSkip, periodCount);

            var count = _db.uspPersonClaimExperienceCount(excludeIncidentId, riskClaimId, personId, partyType, subPartyType, personAliases.IncludeThis,
                        personAliases.IncludeConfirmed, personAliases.IncludeUnconfirmed, personAliases.IncludeTentative, incidentDate,
                        dateRange.StartDate, dateRange.EndDate, isPotentialClaimant).FirstOrDefault();

            return (count == null) ? 0 : (int)count;

        }

    }

    /// <summary>
    /// A class to allow us to weed out duplicate INCIDENTS in this records set.  Called in a DISTINCT()
    /// </summary>
    class ComparePersonNumberOfIncidents_Query : IEqualityComparer<Person_NumberOfIncidents_Query>
    {
        public bool Equals(Person_NumberOfIncidents_Query x, Person_NumberOfIncidents_Query y)
        {
            return x.Incident.Id == y.Incident.Id;
        }
        public int GetHashCode(Person_NumberOfIncidents_Query codeh)
        {
            return codeh.Incident.Id.GetHashCode();
        }
    }
}
