﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
	public partial class PropertyDataServices
	{
		/// <summary>
		/// Returns a list of Telephone numbers that are linked to PersonId's aliases but not including numbers on this claim
		/// </summary>
		/// <param name="db"></param>
		/// <param name="personAliases"></param>
		/// <param name="personId"></param>
		/// <param name="riskClaimId"></param>
		/// <param name="_trace"></param>
		/// <returns></returns>
		public List<Person_LinkedTelephones> Person_LinkedTelephones(EntityAliases personAliases, int personId, int riskClaimId, bool _trace)
		{

			List<int> telephones = (from t in _db.Person2Telephone
									where t.Person_Id == personId
									&& t.RiskClaim_Id == riskClaimId
									select t.Telephone_Id).ToList();

			List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

			return (from p2t in _db.Person2Telephone
					where peopleId.Contains(p2t.Person_Id)
					&& !telephones.Contains(p2t.Telephone.Id)
					&& (p2t.RiskClaim_Id != riskClaimId || p2t.RiskClaim_Id == null)
					&& (p2t.LinkConfidence == (int)LinkConfidence.Confirmed || p2t.LinkConfidence == (int)LinkConfidence.Unconfirmed)
					&& p2t.ADARecordStatus == (byte)ADARecordStatus.Current
					select new Person_LinkedTelephones()
					{
						Confirmed = p2t.LinkConfidence == (int)LinkConfidence.Confirmed,
						Telephone = p2t.Telephone.TelephoneNumber,
					}).ToList();
		}
	}
}
