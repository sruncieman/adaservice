﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.Common.Helpers;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;


namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public List<Address_NumberOfCUEIncidents_Result> Address_NumberOfCUEIncidents(int addressId, int riskClaimId, DateTime incidentDate,  string periodSkip, string periodCount, MessageCache messageCache, bool _trace)
        {

            #region Set start and end dates

            DateTime enddate;
            DateTime startdate;

            PropertyDateRange dateRange = new PropertyDateRange(incidentDate, periodSkip, periodCount);

            enddate = dateRange.EndDate;
            startdate = dateRange.StartDate;

            #endregion

            var cases = (from i2a in _db.Incident2Address
                         where i2a.Address_Id == addressId
                         && i2a.RiskClaim_Id != riskClaimId
                         && i2a.ADARecordStatus == (byte)ADARecordStatus.Current
                         && i2a.Incident.IncidentDate >= startdate && i2a.Incident.IncidentDate <= enddate
                         select new Address_NumberOfCUEIncidents_Result
                         {
                             Id = i2a.Incident.Id,
                             //DateOfBirth = ,
                             //FirstName = ,
                             IncidentDate = i2a.Incident.IncidentDate,
                             //LastName = ,
                             //PartyTypeText = ,
                             //SubPartyText = ,
                             LinkedClaimType = i2a.Incident.ClaimType.ClaimType1,
                             //Insurer = ,
                             //InsurerClaimRef = ,
                             RawAddress = i2a.RawAddress,
                         }).Distinct().ToList();

            return cases;
        }
    }
}
