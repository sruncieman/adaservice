﻿using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using MDA.Common.Helpers;
using MDA.Common.Debug;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {

        public List<uspPersonNumberOpenCasesMessages_Result> Person_NumberOfCases(int personId, EntityAliases personAliases, string linkType, bool? isPotentialClaimant, string periodSkip, string periodCount, bool _trace)
        {
            //bool includeConfirmed;
            //bool includeUnconfirmed;
            //bool includeTentative;
            //bool includeThis;

            //ProcessMatchType(matchType, out includeThis, out includeConfirmed, out includeUnconfirmed, out includeTentative, _trace);

            int keoghsCase2IncidentLinkType = 1;

            if (linkType == "Info Only")
            {
                keoghsCase2IncidentLinkType = 2;
            }

            PropertyDateRange dateRange = new PropertyDateRange(null, periodSkip, periodCount);

            if (_trace)
                ADATrace.WriteLine(string.Format("Date Range: StartDate[{0}] : EndDate[{1}]", dateRange.StartDate.ToString(), dateRange.EndDate.ToString()));

            //DateTime enddate = dateRange.EndDate; //DateTime.Now.AddMonths(-monthSkip).Date;
            //DateTime startdate = dateRange.StartDate; //enddate.AddMonths(-monthCount).Date;

            return _db.uspPersonNumberOpenCasesMessages(personId, personAliases.IncludeThis, personAliases.IncludeConfirmed, personAliases.IncludeUnconfirmed,
                                                                 personAliases.IncludeTentative, dateRange.StartDate, dateRange.EndDate, keoghsCase2IncidentLinkType, isPotentialClaimant).ToList();

            //if ( recs != null )
            //{
            //    int count = recs.Count();

            //    if (_trace)
            //        ADATrace.WriteLine("NumberOpenCases: " + count.ToString() );

            //    return (int)count;
            //}

            //return 0;
            // //int count = 0;

            // List<int> peopleId = FindAllPersonAliases(db, personId, matchType, _trace);



            // //foreach( int pId in peopleId )
            // //{
            //     //string eDate = enddate.ToString("yyyy-MMM-dd");
            //     //string sDate = startdate.ToString("yyyy-MMM-dd");

            //     //StringBuilder ss = new StringBuilder();
            //     //string comma = "";
            //     //foreach (var pid in peopleId)
            //     //{
            //     //    ss.Append(comma + pid.ToString());
            //     //    comma = ",";
            //     //}

            //     //string sql = "SELECT count(distinct(I2C.Id)) FROM Incident2Person AS I2P " +
            //     //             "INNER JOIN KeoghsCase2Incident AS I2C ON I2P.Incident_Id = I2C.Incident_Id " +
            //     //             "LEFT OUTER JOIN Incident2PersonOutcome AS I2P_1 ON I2P.Incident_Id = I2P_1.Incident_Id AND I2P.Person_Id = I2P_1.Person_Id " +
            //     //             "WHERE (I2P_1.Id IS NULL AND CaseIncidentLinkType_Id = 2 " +
            //     //             "and I.IncidentDate >= '" + sDate + "' " +
            //     //             "and I.IncidentDate <= '" + eDate + "' " +
            //     //             "and I2P.ADArecordStatus = 0 " +
            //     //             "and I.ADArecordStatus = 0 " +
            //     //             //"and I2P_1.ADArecordStatus = 0 " +
            //     //             "I2P.ID in (" + ss.ToString() + ") )";


            //     //var x = db.Database.SqlQuery<int>(sql);

            //     //return x.FirstOrDefault();

            //     //count = x;

            //     //var outcomes = (from i2po in _db.Incident2PersonOutcome
            //     //                where i2po.Person_Id == pId 
            //     //                && i2po.Incident_Id == iId
            //     //                && i2po.ADARecordStatus == (byte)ADARecordStatus.Current
            //     //                select i2po.);

            //     //var cases = (from i2p in _db.Incident2Person
            //     //             join c2i in _db.KeoghsCase2Incident on i2p.Incident_Id equals c2i.Incident_Id
            //     //             where i2p.Person_Id == pId
            //     //             && i2p.Incident.IncidentDate >= startdate 
            //     //             && i2p.Incident.IncidentDate <= enddate
            //     //             //&& c2i.CaseIncidentLinkType_Id != (int)MDA.Common.Enum.CaseIncidentLinkType.InfoOnly

            //     //             && i2p.ADARecordStatus            == (byte)ADARecordStatus.Current
            //     //             && i2p.Incident.ADARecordStatus   == (byte)ADARecordStatus.Current
            //     //             && c2i.ADARecordStatus            == (byte)ADARecordStatus.Current
            //     //             && c2i.KeoghsCase.ADARecordStatus == (byte)ADARecordStatus.Current
            //     //             select c2i.KeoghsCase);

            //     //count += cases.Distinct().Count();
            //// }

            //// return count;
        }

    }
}
