﻿using System.Linq;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        /// <summary>
        /// Does the person supplied have ANY links to Bank Accounts with a key attractor 
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public bool Person_IsAnyBankAccountKeyAttractor(int personId)
        {
            return (from p2a in _db.Person2BankAccount.AsNoTracking()
                    where p2a.Person_Id == personId
                    && p2a.RecordStatus == (byte)ADARecordStatus.Current
                    && p2a.BankAccount.KeyAttractor != null && p2a.BankAccount.KeyAttractor.Length > 0
                    && (p2a.BankAccount.FraudRingClosed_ == null || p2a.BankAccount.FraudRingClosed_ == "No")
                    select p2a).Count() > 0;

        }

	}
}
