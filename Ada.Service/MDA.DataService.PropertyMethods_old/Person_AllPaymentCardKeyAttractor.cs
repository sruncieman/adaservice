﻿using System.Linq;
using System.Text;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        /// <summary>
        /// Build a CSV string of all Key Attractor values for any current Payment Card connected with this person
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public string Person_AllPaymentCardKeyAttractor(int personId)
        {
            var keyStrings = from p2c in _db.Person2PaymentCard.AsNoTracking()
                             where p2c.Person_Id == personId
                             && p2c.RecordStatus == (byte)ADARecordStatus.Current
                             select new
                             {
                                 KeyString = p2c.PaymentCard.KeyAttractor,
                                 AccNumber = p2c.PaymentCard.PaymentCardNumber
                             };

            StringBuilder s = new StringBuilder();

            string comma = "";

            foreach (var k in keyStrings)
            {
                s.Append(string.Format("{0} [{1}] {2}", comma, k.AccNumber, k.KeyString));
                comma = ", ";
            }

            return s.ToString();
        }

	}
}
