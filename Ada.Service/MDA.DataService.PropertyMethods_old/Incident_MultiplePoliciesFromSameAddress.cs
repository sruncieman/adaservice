﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {

        public bool Incident_MultiplePoliciesFromSameAddress(int incidentId, int limit, string cacheKey, MessageCache messageCache, bool _trace)
        {
            bool result = false;
            
            int numberOfPolicies = 0;

            List<int> policyHolderTypes = MDA.Common.Helpers.LinqHelper.GetPolicyHolderPartyTypes(); 

            var queryResult = from i in _db.Incidents
                              join i2p in _db.Incident2Person on i.Id equals i2p.Incident_Id
                              join p2p in _db.Person2Policy on i2p.Person_Id equals p2p.Person_Id
                              join p2a in _db.Person2Address on p2p.Person_Id equals p2a.Person_Id
                              where policyHolderTypes.Contains(i2p.PartyType_Id)
                                    && i.Id == incidentId
                              group p2a by p2a.Address_Id into g
                              
                              select new
                                  {
                                      AddressID = g.Key,
                                      AddressCount = g.Count()
                                  };

            if (queryResult == null) return false;

            foreach (var item in queryResult)
            {
                numberOfPolicies = item.AddressCount;
            }


            if (numberOfPolicies >= limit)
                result = true;

            return result;
        }


    }

}
