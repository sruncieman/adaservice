﻿using System.Collections.Generic;
using System.Linq;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        public int Incident_VehiclesOccupancyCount(int incidentId, int riskClaimId)
        {

            int result = 0;

            List<int> claimantTypes = MDA.Common.Helpers.LinqHelper.GetVehicleOccupancySubPartyTypes();   // driver, passenger, unknown ONLY

            // Get all the vehicles on this incident
            var vehicles = (from x in _db.Incident2Vehicle
                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                            && x.RiskClaim_Id == riskClaimId
                            && x.Incident_Id == incidentId
                            && x.Incident2VehicleLinkType_Id != (int)MDA.Common.Enum.Incident2VehicleLinkType.NoVehicleInvolved
                            select x);

            if (vehicles == null) return 0;

            foreach (var vehicle in vehicles)
            {
                result += (from x in _db.Vehicle2Person
                          join y in _db.Incident2Person on x.Person_Id equals y.Person_Id
                          where claimantTypes.Contains(y.SubPartyType_Id)
                          && x.ADARecordStatus == (byte)ADARecordStatus.Current
                          && x.RiskClaim_Id == riskClaimId
                          && x.Vehicle_Id == vehicle.Vehicle_Id
                          && y.Incident_Id == incidentId
                          select x.Id).Distinct().Count();
            }

            return result;

        }


    }

}
