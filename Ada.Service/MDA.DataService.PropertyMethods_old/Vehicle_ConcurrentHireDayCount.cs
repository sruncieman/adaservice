﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public int Vehicle_ConcurrentHireDayCount(int riskClaimId, int vehicleId, EntityAliases vehicleAliases, DateTime? hireStartDate, DateTime? hireEndDate)
        {

            var count = _db.uspConcurrentHireVehicleDayCount(vehicleId, riskClaimId, vehicleAliases.IncludeThis, vehicleAliases.IncludeConfirmed, vehicleAliases.IncludeUnconfirmed, vehicleAliases.IncludeTentative, hireStartDate, hireEndDate).FirstOrDefault();

            return (count == null) ? 0 : (int)count;

            //List<int> vehicleAliasIds = _dataServices.FindAllVehicleAliases(vehicleId, vehicleAliases, _trace);

            //var hirePeriod = (from x in _db.Vehicle2Person
            //                  where vehicleAliasIds.Contains(x.Vehicle_Id)
            //                  && x.RiskClaim_Id != riskClaimId
            //                  && x.HireStartDate <= hireEndDate && x.HireEndDate >= hireStartDate
            //                  && x.VehicleLinkType_Id == (int)MDA.Common.Enum.VehicleLinkType.Hirer
            //                  && x.ADARecordStatus == (byte)ADARecordStatus.Current
            //                  select x).ToList();

            //int dayCount = 0;

            //if (hirePeriod.Count() > 0)
            //{
            //    foreach (var h in hirePeriod)
            //    {
            //        dayCount += GetOverlappingDays(hireStartDate ?? DateTime.Now, hireEndDate ?? DateTime.Now, h.HireStartDate ?? DateTime.Now, h.HireEndDate ?? DateTime.Now);
            //    }
            //}

            //return dayCount;

        }

        //private static int GetOverlappingDays(DateTime s1, DateTime e1, DateTime s2, DateTime e2)
        //{
        //    // If they don't intersect return 0.
        //    if (!(s1 <= e2 && e1 >= s2))
        //    {
        //        return 0;
        //    }

        //    // Take the highest start date and the lowest end date.
        //    DateTime start = s1 > s2 ? s1 : s2;
        //    DateTime end = e1 > e2 ? e2 : e1;

        //    // Add one to the time range since its inclusive.
        //    return (int)(end - start).TotalDays + 1;
        //}

    }

}
