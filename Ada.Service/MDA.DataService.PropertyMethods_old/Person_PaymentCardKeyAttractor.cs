﻿using System.Linq;
using System.Text;

namespace MDA.DataService.PropertyMethods
{
	public partial class PropertyDataServices
	{
 
		/// <summary>
		/// Build a CSV string of all key attractor values for any payment cards connected to this person on this claim
		/// </summary>
		/// <param name="db"></param>
		/// <param name="personId"></param>
		/// <param name="riskClaimId"></param>
		/// <returns></returns>
		public string Person_PaymentCardKeyAttractor(int personId, int riskClaimId)
		{
			var keyStrings = from p2c in _db.Person2PaymentCard.AsNoTracking()
							 where p2c.Person_Id == personId
							 && p2c.RiskClaim_Id == riskClaimId
							 && p2c.RecordStatus == (byte)ADARecordStatus.Current
							 && p2c.PaymentCard.KeyAttractor != null && p2c.PaymentCard.KeyAttractor.Length > 0
							 select new
							 {
								 KeyString = p2c.PaymentCard.KeyAttractor,
								 AccNumber = p2c.PaymentCard.PaymentCardNumber
							 };

			StringBuilder s = new StringBuilder();

			string comma = "";

			foreach (var k in keyStrings)
			{
				s.Append(string.Format("{0} [{1}] {2}", comma, k.AccNumber, k.KeyString));
				comma = ", ";
			}

			return s.ToString();
		}

	}
}
