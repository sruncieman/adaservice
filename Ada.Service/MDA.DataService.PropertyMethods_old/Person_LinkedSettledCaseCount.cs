﻿using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using MDA.Common.Helpers;
using MDA.Common.Debug;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
	public partial class PropertyDataServices
	{

        public List<uspPersonLinkedSettledCaseCountMessages_Result> Person_LinkedSettledCaseCount(int incidentId, int personId,
								int category, int? claimTypeId, bool? isPotentialClaimant, string periodSkip, string periodCount,
								EntityAliases personAliases, bool _trace)
		{

			PropertyDateRange dateRange = new PropertyDateRange(null, periodSkip, periodCount);

			if (_trace)
				ADATrace.WriteLine(string.Format("Date Range: StartDate[{0}] : EndDate[{1}]", dateRange.StartDate.ToString(), dateRange.EndDate.ToString()));

			//enddate = dateRange.EndDate; // incidentDate.AddMonths(+monthSkip).Date;
			//startdate = dateRange.StartDate; // incidentDate.AddMonths(-monthCount).Date;

            return _db.uspPersonLinkedSettledCaseCountMessages(personId, incidentId, personAliases.IncludeThis,
                        personAliases.IncludeConfirmed, personAliases.IncludeUnconfirmed, personAliases.IncludeTentative,
                        dateRange.StartDate, dateRange.EndDate, category, claimTypeId, isPotentialClaimant).ToList();


		}

	}
}
