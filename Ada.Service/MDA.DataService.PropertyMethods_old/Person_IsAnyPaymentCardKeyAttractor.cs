﻿using System.Linq;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        /// <summary>
        /// Does this person have any connections with any Cards with a key attractor
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public bool Person_IsAnyPaymentCardKeyAttractor(int personId)
        {
            return (from p2c in _db.Person2PaymentCard.AsNoTracking()
                    where p2c.Person_Id == personId
                    && p2c.RecordStatus == (byte)ADARecordStatus.Current
                    && p2c.PaymentCard.KeyAttractor != null && p2c.PaymentCard.KeyAttractor.Length > 0
                    select p2c).Count() > 0;

        }

	}
}
