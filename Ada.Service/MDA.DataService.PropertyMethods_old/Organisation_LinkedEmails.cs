﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
  
        public List<Organisation_LinkedEmails> Organisation_LinkedEmails(EntityAliases organisationAliases, int organisationId, int riskClaimId, bool _trace)
        {
            //List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            //return (from o2e in _db.Organisation2Email.AsNoTracking()
            //        where organisationsId.Contains(o2e.Organisation_Id)
            //        && (o2e.RiskClaim_Id != riskClaimId || o2e.RiskClaim_Id == null)
            //        && (o2e.LinkConfidence == (int)LinkConfidence.Confirmed || o2e.LinkConfidence == (int)LinkConfidence.Unconfirmed)
            //        && o2e.ADARecordStatus == (byte)ADARecordStatus.Current
            //        select new Organisation_LinkedEmails()
            //        {
            //            Confirmed = o2e.LinkConfidence == (int)LinkConfidence.Confirmed,
            //            EmailAddress = o2e.Email.EmailAddress
            //        }).ToList();

            List<int> emails = (from e in _db.Organisation2Email.AsNoTracking()
                                where e.Organisation_Id == organisationId
                                && e.RiskClaim_Id == riskClaimId
                                select e.Email.Id).ToList();

            List<int> organisationsId = _dataServices.FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o2e in _db.Organisation2Email.AsNoTracking()
                    where organisationsId.Contains(o2e.Organisation_Id)
                    && !emails.Contains(o2e.Email.Id)
                    && (o2e.RiskClaim_Id != riskClaimId || o2e.RiskClaim_Id == null)
                    && (o2e.LinkConfidence == (int)LinkConfidence.Confirmed || o2e.LinkConfidence == (int)LinkConfidence.Unconfirmed)
                    && o2e.ADARecordStatus == (byte)ADARecordStatus.Current
                    select new Organisation_LinkedEmails()
                    {
                        Confirmed = o2e.LinkConfidence == (int)LinkConfidence.Confirmed,
                        EmailAddress = o2e.Email.EmailAddress
                    }).ToList();
        }

    }
}
