﻿using System.Linq;
using MDA.DAL;
using LinqKit;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        /// <summary>
        /// Returns true if any of the telephone types provided have a key attractor for this person in ANY claim
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId">Person ID</param>
        /// <param name="telephoneType">'|' separated list of telephone types UKNOWN, LANDLINE, MOBILE, FAX.</param>
        /// <param name="riskClaimId">The riskclaim ID</param>
        /// <returns></returns>
        public bool Person_IsAnyTelephoneKeyAttractor(int personId, string telephoneType, string cacheKey, MessageCache messageCache, bool _trace)
        {
            var phones = (from p2t in _db.Person2Telephone.AsNoTracking()
                          where p2t.Person_Id == personId
                          && p2t.RecordStatus == (byte)ADARecordStatus.Current
                          && p2t.Telephone.KeyAttractor != null
                          && p2t.Telephone.KeyAttractor.Length > 0
                          && (p2t.Telephone.FraudRingClosed_ == null || p2t.Telephone.FraudRingClosed_ == "No")
                          select p2t);

            string[] typesSource = telephoneType.Split('|');

            var predicate = PredicateBuilder.False<Person2Telephone>();
            bool predUsed = false;

            foreach (string s in typesSource)
            {
                if (string.Compare(s, "Unknown", true) == 0)
                {
                    predicate = predicate.Or(p => p.Telephone.TelephoneType_Id == (int)MDA.Common.Enum.TelephoneType.Unknown);
                    predUsed = true;
                }
                else if (string.Compare(s, "Landline", true) == 0)
                {
                    predicate = predicate.Or(p => p.Telephone.TelephoneType_Id == (int)MDA.Common.Enum.TelephoneType.Landline);
                    predUsed = true;
                }
                else if (string.Compare(s, "Mobile", true) == 0)
                {
                    predicate = predicate.Or(p => p.Telephone.TelephoneType_Id == (int)MDA.Common.Enum.TelephoneType.Mobile);
                    predUsed = true;
                }
                else if (string.Compare(s, "FAX", true) == 0)
                {
                    predicate = predicate.Or(p => p.Telephone.TelephoneType_Id == (int)MDA.Common.Enum.TelephoneType.FAX);
                    predUsed = true;
                }
            }

            if (predUsed)
                phones = phones.AsExpandable().Where(predicate);

            return phones.Count() > 0;

        }

	}
}
