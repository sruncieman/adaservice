﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_IsBankAccountKeyAttractor
    {
        [DataMember]
        public string SortCode { get; set; }
        [DataMember]
        public string AccountNumber { get; set; }
        [DataMember]
        public string KeyAttractor { get; set; }

        public Person_IsBankAccountKeyAttractor Clone()
        {
            return new Person_IsBankAccountKeyAttractor()
            {
                SortCode = this.SortCode,
                AccountNumber = this.AccountNumber,
                KeyAttractor = this.KeyAttractor
            };
        }
    }
}
