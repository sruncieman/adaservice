﻿using MDA.DAL;
using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Address_IsKeyAttractor_Result
    {
        [DataMember]
        public Address Address { get; set; }
        [DataMember]
        public string KeyAttractor { get; set; }

        public Address_IsKeyAttractor_Result Clone()
        {
            return new Address_IsKeyAttractor_Result()
            {
                Address = this.Address,
                KeyAttractor = this.KeyAttractor
            };
        }
    }

}
