﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Organisation_LinkedEmails
    {
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Organisation_LinkedEmails Clone()
        {
            return new Organisation_LinkedEmails()
            {
                EmailAddress = this.EmailAddress,
                Confirmed = this.Confirmed
            };
        }
    }
}
