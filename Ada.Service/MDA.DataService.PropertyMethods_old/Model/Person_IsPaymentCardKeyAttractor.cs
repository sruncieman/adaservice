﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_IsPaymentCardKeyAttractor
    {
        [DataMember]
        public string PaymentCard { get; set; }
        [DataMember]
        public string KeyAttractor { get; set; }

        public Person_IsPaymentCardKeyAttractor Clone()
        {
            return new Person_IsPaymentCardKeyAttractor()
            {
                PaymentCard = this.PaymentCard,
                KeyAttractor = this.KeyAttractor
            };
        }
    }
}
