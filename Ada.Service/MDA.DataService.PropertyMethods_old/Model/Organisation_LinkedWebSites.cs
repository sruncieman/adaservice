﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Organisation_LinkedWebSites
    {
        [DataMember]
        public string WebSiteURL { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Organisation_LinkedWebSites Clone()
        {
            return new Organisation_LinkedWebSites()
            {
                WebSiteURL = this.WebSiteURL,
                Confirmed = this.Confirmed
            };
        }
    }
}
