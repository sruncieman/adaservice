﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_IsTelephoneKeyAttractor
    {
        [DataMember]
        public int TelephoneType { get; set; } // TelephoneType
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public string KeyAttractor { get; set; }
        [DataMember]
        public int TelephoneId { get; set; }

        public Person_IsTelephoneKeyAttractor Clone()
        {
            return new Person_IsTelephoneKeyAttractor()
            {
                Telephone = this.Telephone,
                KeyAttractor = this.KeyAttractor,
                TelephoneId = this.TelephoneId
            };
        }
    }
}
