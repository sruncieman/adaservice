﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Organisation_IsTOGAttractor_Result
    {
        [DataMember]
        public string OrganisationName { get; set; }
        [DataMember]
        public string KeyAttractor { get; set; }

        public Organisation_IsTOGAttractor_Result Clone()
        {
            return new Organisation_IsTOGAttractor_Result()
            {
                OrganisationName = this.OrganisationName,
                KeyAttractor = this.KeyAttractor
            };
        }
    }
}
