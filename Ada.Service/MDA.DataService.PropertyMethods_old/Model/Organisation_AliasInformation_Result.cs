﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Organisation_AliasInformation_Result
    {
        [DataMember]
        public int OrganisationId { get; set; }
        [DataMember]
        public string OrganisationName { get; set; }

        public Organisation_AliasInformation_Result Clone()
        {
            return new Organisation_AliasInformation_Result()
            {
                OrganisationId = this.OrganisationId,
                OrganisationName = this.OrganisationName
            };
        }
    }
}
