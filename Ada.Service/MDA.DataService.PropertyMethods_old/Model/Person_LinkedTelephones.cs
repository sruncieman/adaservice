﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_LinkedTelephones
    {
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Person_LinkedTelephones Clone()
        {
            return new Person_LinkedTelephones()
            {
                Telephone = this.Telephone,
                Confirmed = this.Confirmed
            };
        }
    }


}
