﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_LinkedNINumbers
    {
        [DataMember]
        public string NINumber { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Person_LinkedNINumbers Clone()
        {
            return new Person_LinkedNINumbers()
            {
                NINumber = this.NINumber,
                Confirmed = this.Confirmed
            };
        }
    }
}
