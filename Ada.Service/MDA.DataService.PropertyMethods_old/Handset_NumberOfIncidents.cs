﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.Common.Helpers;
using LinqKit;
using RiskEngine.Model;
using MDA.Common.Debug;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public List<Handset_NumberOfIncidents_Result> Handset_NumberOfIncidents(int excludeIncidentId, DateTime incidentDate, int baseRiskClaimId, int handsetId, EntityAliases handsetAliases, string incidentSource, string incidentType, string periodSkip, string periodCount, bool _trace)
        {

            List<int> handsetAliasIds = _dataServices.FindAllHandsetAliases(handsetId, handsetAliases, _trace);

            DateTime enddate; // = DateTime.Now.AddMonths(-monthSkip).Date;
            DateTime startdate; // = enddate.AddMonths(-monthCount).Date;

            PropertyDateRange dateRange = new PropertyDateRange(incidentDate, periodSkip, periodCount);

            enddate = dateRange.EndDate; // incidentDate.AddMonths(+monthSkip).Date;
            startdate = dateRange.StartDate; // incidentDate.AddMonths(-monthCount).Date;

            if (_trace)
                ADATrace.WriteLine(string.Format("Date Range: StartDate[{0}] : EndDate[{1}] : IncidentDate[{2}]", startdate.ToString(), enddate.ToString(), incidentDate.ToString()));

            string[] incidentSources = incidentSource.Split('|');
            string[] incidentTypes = incidentType.Split('|');

            var incidents = (from i2h in _db.Incident2Handset
                                join i in _db.Incidents on i2h.Incident_Id equals i.Id
                                join h in _db.Handsets on i2h.Handset_Id equals h.Id
                                where handsetAliasIds.Contains(i2h.Handset_Id)
                                && i2h.ADARecordStatus == (byte)ADARecordStatus.Current
                                && i.ADARecordStatus == (byte)ADARecordStatus.Current
                                && h.ADARecordStatus == (byte)ADARecordStatus.Current
                                && (i2h.BaseRiskClaim_Id != baseRiskClaimId)
                                && i.IncidentDate >= startdate && i.IncidentDate <= enddate
                                && i2h.Incident_Id != excludeIncidentId
                                select new Handset_NumberOfIncidents_Query()
                                {
                                    Handset = h,
                                    Incident = i,
                                    //Incident2Person = i2p,
                                    Incident2Handset = i2h,
                                    RiskClaim = i2h.RiskClaim,
                                    //Person = v2p.Person,
                                    //Involvement = i2h.Incident2VehicleLinkType.Text,
                                });

            var predicate1 = PredicateBuilder.False<Handset_NumberOfIncidents_Query>();

            bool pred1used = false;

            foreach (string s in incidentSources)
            {
                if (string.Compare(s, "CUE", true) == 0)
                {
                    predicate1 = predicate1.Or(i => i.Incident.Source == "CUE");
                    pred1used = true;
                }
                else if (string.Compare(s, "ADA", true) == 0)
                {
                    predicate1 = predicate1.Or(i => i.Incident.Source == "ADA");
                    pred1used = true;
                }
                else if (string.Compare(s, "ACTIVE", true) == 0)
                {
                    predicate1 = predicate1.Or(i => i.Incident.Source != "Keoghs CMS");
                    pred1used = true;
                }
                else if (string.Compare(s, "INFO", true) == 0)
                {
                    predicate1 = predicate1.Or(i => i.Incident.Source == "Keoghs InfoOnly");
                    pred1used = true;
                }
            }

            var predicate2 = PredicateBuilder.False<Handset_NumberOfIncidents_Query>();

            bool pred2used = false;

            foreach (string s in incidentTypes)
            {
                if (string.Compare(s, "RTC", true) == 0)
                {
                    predicate2 = predicate2.Or(i => i.Incident.IncidentType_Id == (int)MDA.Common.Enum.ClaimType.Motor);
                    pred2used = true;
                }
            }

            if (pred1used)
                incidents = incidents.AsExpandable().Where(predicate1);

            if (pred2used)
                incidents = incidents.AsExpandable().Where(predicate2);

            var incidentList = incidents.ToList();

            var incidentList2 = incidentList.Distinct(new CompareHandsetNumberOfIncidents_Query()).ToList();

            List<Handset_NumberOfIncidents_Result> res = (from x in incidentList2
                                                            select new Handset_NumberOfIncidents_Result()
                                                            {
                                                                IncidentDate = x.Incident.IncidentDate,
                                                                //Insurer = x.Incident2Person.Insurer,
                                                                ClaimRef = x.RiskClaim.ClientClaimRefNumber,
                                                                //DriverDetails = x.Person.FirstName + " " + x.Person.LastName + " " + x.Person.DateOfBirth.ToString(),
                                                                Involvement = x.Involvement,
                                                                //VehicleDetails = x.Vehicle.VehicleRegistration + " " + x.Vehicle.VehicleMake + " " + x.Vehicle.Model
                                                            }).ToList();
            return res;


        }

        class CompareHandsetNumberOfIncidents_Query : IEqualityComparer<Handset_NumberOfIncidents_Query>
        {
            public bool Equals(Handset_NumberOfIncidents_Query x, Handset_NumberOfIncidents_Query y)
            {
                return x.Incident.Id == y.Incident.Id;
            }
            public int GetHashCode(Handset_NumberOfIncidents_Query codeh)
            {
                return codeh.Incident.Id.GetHashCode();
            }
        }

    }
}
