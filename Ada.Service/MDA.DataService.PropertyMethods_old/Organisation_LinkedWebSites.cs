﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {

        public List<Organisation_LinkedWebSites> Organisation_LinkedWebSites(EntityAliases organisationAliases, int organisationId, int riskClaimId, bool _trace)
        {
            //List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            //return (from o2w in _db.Organisation2WebSite.AsNoTracking()
            //        where organisationsId.Contains(o2w.Organisation_Id)
            //        && (o2w.RiskClaim_Id != riskClaimId || o2w.RiskClaim_Id == null)
            //        && (o2w.LinkConfidence == (int)LinkConfidence.Confirmed || o2w.LinkConfidence == (int)LinkConfidence.Unconfirmed)
            //        && o2w.ADARecordStatus == (byte)ADARecordStatus.Current
            //        select new Organisation_LinkedWebSites()
            //        {
            //            Confirmed = o2w.LinkConfidence == (int)LinkConfidence.Confirmed,
            //            WebSiteURL = o2w.WebSite.URL,
            //        }).ToList();

            List<int> webSites = (from w in _db.Organisation2WebSite.AsNoTracking()
                                      where w.Organisation_Id == organisationId
                                      && w.RiskClaim_Id == riskClaimId
                                      select w.WebSite.Id).ToList();

            List<int> organisationsId = _dataServices.FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o2w in _db.Organisation2WebSite.AsNoTracking()
                    where organisationsId.Contains(o2w.Organisation_Id)
                    && !webSites.Contains(o2w.WebSite.Id)
                    && (o2w.RiskClaim_Id != riskClaimId || o2w.RiskClaim_Id == null)
                    && (o2w.LinkConfidence == (int)LinkConfidence.Confirmed || o2w.LinkConfidence == (int)LinkConfidence.Unconfirmed)
                    && o2w.ADARecordStatus == (byte)ADARecordStatus.Current
                    select new Organisation_LinkedWebSites()
                    {
                        Confirmed = o2w.LinkConfidence == (int)LinkConfidence.Confirmed,
                        WebSiteURL = o2w.WebSite.URL,
                    }).ToList();
        }

    }
}
