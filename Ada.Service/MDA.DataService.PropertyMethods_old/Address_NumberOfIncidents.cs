﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.Common.Helpers;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;


namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public List<Address_NumberOfIncidents_Result> Address_NumberOfIncidents(int addressId, int riskClaimId, string linkType, DateTime incidentDate, ListOfInts peopleAliasIds, string incidentType, string claimType, string periodSkip, string periodCount, string cacheKey, MessageCache messageCache, bool _trace)
        {

            #region Build list of acceptable p2a address links
            List<int> addressLinkType = new List<int>();

            linkType = linkType.ToLower();

            if (linkType == "current")
            {
                addressLinkType.Add((int)MDA.Common.Enum.AddressLinkType.CurrentAddress); 
                addressLinkType.Add((int)MDA.Common.Enum.AddressLinkType.LivesAt);
                addressLinkType.Add((int)MDA.Common.Enum.AddressLinkType.Owner);
            }
            else if (linkType == "notcurrent")
            {

                addressLinkType = (from alt in _db.AddressLinkTypes
                                   where alt.Id != (int)MDA.Common.Enum.AddressLinkType.CurrentAddress
                                   && alt.Id != (int)MDA.Common.Enum.AddressLinkType.LivesAt
                                   && alt.Id != (int)MDA.Common.Enum.AddressLinkType.Owner
                                   select alt.Id).ToList();
            }
            else
            {
                addressLinkType = (from alt in _db.AddressLinkTypes
                                   select alt.Id).ToList();
            }
            #endregion

            // Get people attached to THIS address who are not on this claim/incident or an alias of a person on this claim/incident
            List<int> peopleNotOnClaim = (from p2a in _db.Person2Address
                                          where p2a.Address_Id == addressId
                                          && addressLinkType.Contains(p2a.AddressLinkType_Id)  // Check the linktype between the person and the address
                                          && p2a.RiskClaim_Id != riskClaimId
                                          && !peopleAliasIds.Contains(p2a.Person_Id)
                                          && p2a.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select p2a.Person_Id).ToList();

            #region Set start and end dates

            DateTime enddate;
            DateTime startdate;

            PropertyDateRange dateRange = new PropertyDateRange(incidentDate, periodSkip, periodCount);

            enddate = dateRange.EndDate;
            startdate = dateRange.StartDate;

            #endregion

            #region Build list of incident types

            int it = 99;
            List<int> incidentTypes = new List<int>();

            if (string.IsNullOrEmpty(incidentType))
            {
                incidentTypes = (from x in _db.IncidentTypes select x.Id).ToList();
            }
            else if (incidentType.StartsWith("-"))
            {
                //incidentType = incidentType.Replace("-","");

                it = Math.Abs(Convert.ToInt32(incidentType));

                incidentTypes = (from x in _db.IncidentTypes
                                 where x.Id != it
                                 select x.Id).ToList();

            }
            else
            {
                it = Convert.ToInt32(incidentType);
                incidentTypes.Add(it);
            }

            #endregion

            #region Build list of claim types

            int ct = 99;
            List<int> claimTypes = new List<int>();

            if (string.IsNullOrEmpty(claimType))
            {
                claimTypes = (from x in _db.ClaimTypes select x.Id).ToList();
            }
            else if (claimType.StartsWith("-"))
            {
                //claimType = claimType.Replace("-", "");
                ct = Math.Abs(Convert.ToInt32(claimType));

                claimTypes = (from x in _db.ClaimTypes
                              where x.Id != ct
                              select x.Id).ToList();

            }
            else
            {
                ct = Convert.ToInt32(claimType);
                claimTypes.Add(ct);
            }

            #endregion

            if (peopleNotOnClaim.Count() > 0)
            {

                // Get cases for all people not on this claim
                var cases = (from i2p in _db.Incident2Person
                             join p2a in _db.Person2Address on i2p.Person_Id equals p2a.Person_Id 
                             where peopleNotOnClaim.Contains(i2p.Person_Id)
                             && p2a.Address_Id == addressId
                             && p2a.RiskClaim_Id == i2p.RiskClaim_Id
                             && i2p.Incident.IncidentDate >= startdate && i2p.Incident.IncidentDate <= enddate
                             && claimTypes.Contains(i2p.Incident.ClaimType_Id)
                             && incidentTypes.Contains(i2p.Incident.IncidentType_Id)
                             && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                             && i2p.Incident.ADARecordStatus == (byte)ADARecordStatus.Current
                             select new Address_NumberOfIncidents_Result
                             {
                                 Id = i2p.Incident.Id,
                                 DateOfBirth = i2p.Person.DateOfBirth,
                                 FirstName = i2p.Person.FirstName,
                                 IncidentDate = i2p.Incident.IncidentDate,
                                 LastName = i2p.Person.LastName,
                                 PartyTypeText = i2p.PartyType.PartyTypeText,
                                 SubPartyText = i2p.SubPartyType.SubPartyText,
                                 LinkedClaimType = i2p.Incident.ClaimType.ClaimType1,
                                 Insurer = i2p.Insurer,
                                 InsurerClaimRef = i2p.ClaimNumber,
                                 RawAddress = p2a.RawAddress,
                             }).Distinct().ToList();


                return cases;

            }

            return new List<Address_NumberOfIncidents_Result >();

        }

  
    }
}
