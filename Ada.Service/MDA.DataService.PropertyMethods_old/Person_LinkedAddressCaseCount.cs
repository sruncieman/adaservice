﻿using System;
using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
	public partial class PropertyDataServices
	{
 
		public List<Address_NumberOfCases_Result> Person_LinkedAddressCaseCount(int personId, int riskClaimId, string linkType, bool? isPotentialClaimant, DateTime incidentDate, ListOfInts peopleAliasIds, EntityAliases personAliases, string incidentType, string claimType, string periodSkip, string periodCount, string cacheKey, MessageCache messageCache, bool _trace)
		{

			List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

			var addressesOnClaim = (from p2a in _db.Person2Address.AsNoTracking()
									where p2a.RiskClaim_Id == riskClaimId
									&& p2a.LinkConfidence == (int)LinkConfidence.Confirmed
									&& p2a.ADARecordStatus == (byte)ADARecordStatus.Current
									select p2a.Address_Id).ToList();

			var otherAddresses = (from p2a in _db.Person2Address.AsNoTracking()
								  where peopleId.Contains(p2a.Person_Id)
									   && p2a.RiskClaim_Id != riskClaimId
									   && !addressesOnClaim.Contains(p2a.Address_Id)
									   && p2a.LinkConfidence == (int)LinkConfidence.Confirmed
									   && p2a.ADARecordStatus == (byte)ADARecordStatus.Current
								  select p2a.Address_Id).ToList();

			foreach (var a in otherAddresses)
			{
				var ret = Address_NumberOfCases(a, riskClaimId, linkType, isPotentialClaimant, incidentDate, peopleAliasIds, incidentType, claimType, periodSkip, periodCount, cacheKey, messageCache, _trace);

				if (ret.Count() > 0)
				{
					return ret;
				}

			}

			return new List<Address_NumberOfCases_Result>();
		}


	}
}
