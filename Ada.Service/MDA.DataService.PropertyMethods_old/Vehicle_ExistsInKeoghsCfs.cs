﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.Common.Helpers;
using LinqKit;
using RiskEngine.Model;
using MDA.Common.Debug;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public int Vehicle_IsInKeoghsCfs(int excludeIncidentId, DateTime incidentDate, int baseRiskClaimId, int vehicleId, string indexVehicleLinkType, bool _trace)
        {
            var thisCar = _db.Incident2Vehicle.FirstOrDefault(i => i.Vehicle_Id == vehicleId);

            PropertyDateRange dateRange = new PropertyDateRange(incidentDate, "Y", "-1");

            var matchingCfsVehicles = (from iv in _db.Incident2Vehicle
                                       join i in _db.Incidents
                                       on iv.Incident_Id equals i.Id
                                       where iv.LinkConfidence == (byte)LinkConfidence.Confirmed
                                       && i.IncidentDate >= dateRange.StartDate
                     select iv.Vehicle_Id).Distinct();

            if (matchingCfsVehicles.Any())
                return 1;

            matchingCfsVehicles = (from iv in _db.Incident2Vehicle
                                       join i in _db.Incidents
                                       on iv.Incident_Id equals i.Id
                                       where iv.LinkConfidence == (byte)LinkConfidence.Confirmed
                                       select iv.Vehicle_Id).Distinct();

            if (matchingCfsVehicles.Any())
                return 2;

            return 0;
        }
    }
}
