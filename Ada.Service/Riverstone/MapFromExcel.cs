﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;
using MDA.Common.Enum;
using MDA.Pipeline.Model;

namespace MDA.Mapping.Riverstone
{
    
    public class RiverstonePerson
    {

        public DateTime? DOB { get; set; }
        public string Salutation { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string ClaimantType { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string TelephoneNum { get; set; }
        public string MobileNum { get; set; }
        public string EmailAddress { get; set; }
        public string NI { get; set; }
    }


    public class RiverstoneVehiclePassenger
    {

        public DateTime? DOB { get; set; }
        public string Salutation { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string ClaimantType { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string TelephoneNum { get; set; }
        public string MobileNum { get; set; }
       
    }
    
    public static class MapFromExcel
    {
        public static string ClaimNumber { get; set; }
     

 
        public static PipelineClaimBatch ReadExcelFile(String strFile, string strSheetName)
        {
            PipelineClaimBatch batch = new PipelineClaimBatch();

            //String strFile2 = @"C:\Dev\Projects\MDA\MDASolution\Riverstone\Excel\RIL4.xlsx";

            
            
            //string strXMLFile = @"C:\Dev\Projects\MDA\MDASolution\Riverstone\Excel\RIL.xlsx";
            string strExtensionName = "";
            string strFileName = strFile;
            DataSet ds = new DataSet();
            DataTable dtt = new DataTable();

            dtt.Columns.Add(new DataColumn("Claim_ID"));

            if (!string.IsNullOrEmpty(strFileName))
            {
                //get the extension name, check if it's a spreadsheet                
                strExtensionName = strFileName.Substring(strFileName.IndexOf(".") + 1);
                if (strExtensionName.Equals("xls") || strExtensionName.Equals("xlsx"))
                {
                    //open connection out to read excel 
                    string strConnectionString = string.Empty;
                    if (strExtensionName == "xls")
                        strConnectionString = "Provider=Microsoft.ACE.OLEDB.4.0;Data Source=" + strFile + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    else if (strExtensionName == "xlsx")
                        strConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strFile + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1\"";

                    if (!string.IsNullOrEmpty(strConnectionString))
                    {
                        OleDbConnection objConnection = new OleDbConnection(strConnectionString);
                        objConnection.Open();
                        DataTable oleDbSchemaTable = objConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                        OleDbCommand objCmd = new OleDbCommand(string.Format("Select * from [{0}$]", strSheetName), objConnection);
                        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                        objAdapter1.SelectCommand = objCmd;
                        DataSet objDataSet = new DataSet();
                        objAdapter1.Fill(objDataSet);
                        objConnection.Close();
                        dtt = objDataSet.Tables[0];

                        CreateXmlDocument(dtt, batch);

                    }
                }
            }
            return batch;
        }

        private static PipelineClaimBatch CreateXmlDocument(DataTable dtt, PipelineClaimBatch batch)
        {
            DateTime incidentDate = new DateTime();
            TimeSpan incidentTime;
            
            bool typeOfOtherPolicyIsPolicyHolder = false;

            var distinctRows = (from DataRow dr in dtt.Rows
                                where dr.Field<string>("Claim Number") != null && dr.Field<string>("Claim Number") == "BS115098P"
                                select (string) dr["Claim Number"]).Distinct();

            foreach (var distinctRow in distinctRows)
            {
                MDA.Pipeline.Model.PipelineMotorClaim claim = new MDA.Pipeline.Model.PipelineMotorClaim();
                var dictVehicles = new Dictionary<String, PipelineVehicle>();
                var dictPeople = new Dictionary<String, PipelinePerson>();
                // Defaults
                claim.ClaimType_Id = (int)ClaimType.Motor;
                claim.Operation = (int)Operation.Normal;
                claim.Policy.PolicyType_Id = (int)PolicyType.Unknown;
                claim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.Unknown;


                 DataTable claimRows = (from DataRow dr in dtt.AsEnumerable()
                                       where dr.Field<string>("Claim Number") == distinctRow
                                       select dr).CopyToDataTable();

                // returns unique vehicle registraions
                var distinctVehicles = (from DataRow dr in dtt.AsEnumerable()
                                        where dr.Field<string>("Vehicle Registration Number") != null
                                        && dr.Field<string>("Claim Number") == distinctRow
                                        select (string)dr["Vehicle Registration Number"]).Distinct().ToList();

                foreach (var distinctVehicleReg in distinctVehicles)
                {
                    PipelinePerson person = new PipelinePerson();
                    var otherPeopleInVehicle = (from DataRow dr in claimRows.AsEnumerable()
                                                where
                                                    dr.Field<string>("Other Party Vehicle Registration") == distinctVehicleReg
                                                     && dr.Field<string>("Vehicle Registration Number") == dr.Field<string>("Other Party Vehicle Registration")
                                                select dr).Distinct();
                    
                    
                    var riverstonePersonDataRow = (from DataRow dr in claimRows.AsEnumerable()
                                           where dr.Field<string>("Vehicle Registration Number") == distinctVehicleReg
                                           select dr).FirstOrDefault();

                    RiverstonePerson riverstonePerson = ConvertRowToRiverstonePerson(riverstonePersonDataRow);

                    var incident2VehicleLinkType = new Incident2VehicleLinkType();

                    switch (riverstonePerson.ClaimantType)
                    {
                        case "Own Driver":
                            incident2VehicleLinkType = Incident2VehicleLinkType.InsuredVehicle;
                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            break;
                        case "TP Driver":
                            incident2VehicleLinkType = Incident2VehicleLinkType.ThirdPartyVehicle;
                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                            break;
                    }


                    PipelineAddress address = new PipelineAddress();
                    address.Street = riverstonePerson.Address;
                    address.PostCode = riverstonePerson.Postcode;
                    person.Addresses.Add(address);

                    person.DateOfBirth = riverstonePerson.DOB;
                    person.EmailAddresses.Add(new PipelineEmail() { EmailAddress = riverstonePerson.EmailAddress });

                    MDA.Common.Enum.Salutation salutation = new Salutation();

                    switch (riverstonePerson.Salutation)
                    {
                        case "Mr":
                            salutation = MDA.Common.Enum.Salutation.Mr;
                            break;
                        case "Miss":
                            salutation = MDA.Common.Enum.Salutation.Miss;
                            break;
                        case "Mrs":
                            salutation = MDA.Common.Enum.Salutation.Mrs;
                            break;

                    }

                    person.Salutation_Id = (int)salutation;
                    person.FirstName = riverstonePerson.Firstname;
                    person.LastName = riverstonePerson.Lastname;
                    person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = riverstonePerson.TelephoneNum, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                    person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = riverstonePerson.MobileNum, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                    person.NINumbers.Add(new PipelineNINumber() { NINumber1 = riverstonePerson.NI });
                    
    
                    PipelineVehicle vehicle = new PipelineVehicle();
                    vehicle.People                   = null;
                    vehicle.VehicleRegistration      = distinctVehicleReg;
                    vehicle.EngineCapacity           = null;
                    vehicle.VehicleMake              = string.Empty;
                    vehicle.VehicleModel             = string.Empty;
                    vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)incident2VehicleLinkType;
                    vehicle.VIN                      = string.Empty;

                    //person.Organisations.Add(new Organisation());

                    vehicle.People = new List<PipelinePerson>();
                    vehicle.People.Add(person);

                    foreach (var dataRow in otherPeopleInVehicle)
                    {
                        PipelinePerson otherPerson = new PipelinePerson();
                        
                        if (dataRow.Field<string>("Other Party Vehicle Registration") == distinctVehicleReg)
                        {
                            RiverstoneVehiclePassenger otherPersonInVehicle =
                                ConvertRowToRiverstoneVehiclePassenger(dataRow);
                               

                            switch (otherPersonInVehicle.ClaimantType)
                            {
                                case "Own Driver":
                                    otherPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    break;
                                case "TP Driver":
                                    otherPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                                    break;
                                case "Own Passenger":
                                    otherPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                                    break;
                            }

                            PipelineAddress addressOther = new PipelineAddress();
                            addressOther.Street = otherPersonInVehicle.Address;
                            addressOther.PostCode = otherPersonInVehicle.Postcode;
                            otherPerson.Addresses.Add(addressOther);

                            MDA.Common.Enum.Salutation salutationOther = new Salutation();

                            switch (otherPersonInVehicle.Salutation)
                            {
                                case "Mr":
                                    salutationOther = MDA.Common.Enum.Salutation.Mr;
                                    break;
                                case "Miss":
                                    salutationOther = MDA.Common.Enum.Salutation.Miss;
                                    break;
                                case "Mrs":
                                    salutationOther = MDA.Common.Enum.Salutation.Mrs;
                                    break;

                            }

                            otherPerson.Salutation_Id = (int)salutationOther;
                            otherPerson.FirstName = otherPersonInVehicle.Firstname;
                            otherPerson.LastName = otherPersonInVehicle.Lastname;
                            otherPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = otherPersonInVehicle.TelephoneNum, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                            otherPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = otherPersonInVehicle.MobileNum, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });

                            vehicle.People.Add(otherPerson);
                        }
                    }

                    //dictVehicles.Add(distinctVehicleReg, vehicle);

                    claim.Vehicles.Add(vehicle);
                }
                

                // Add claim organisations
                var organisations = (from DataRow dr in claimRows.AsEnumerable()
                                            where
                                 dr.Field<string>("Type of Other Party") == "Claimant Insurer" || dr.Field<string>("Type of Other Party") == "Police" || dr.Field<string>("Type of Other Party") == "Policy Holder" || dr.Field<string>("Type of Other Party") == "Other" || dr.Field<string>("Type of Other Party") == "Claimant Solicitor"
                                     select new {
                                          OrgType =  dr.Field<string>("Type of Other Party"),
                                          OrgName =  dr.Field<string>("Other Party Name Organisation"),
                                          OrgAddress = dr.Field<string>("Other Party Address"),
                                          OrgPostcode = dr.Field<string>("Other Party Post Code"),
                                          OrgTelephone = dr.Field<string>("Other Party Telephone Number"),
                                          OrgMobile = dr.Field<string>("Other Party Mobile Telephone Number")
                                     } ).Distinct();


                foreach (var organisation in organisations)
                {
                    PipelineOrganisation claimOrganisation = new PipelineOrganisation();
                    switch (organisation.OrgType)
                    {
                        case "Other":
                            claimOrganisation.OrganisationType_Id = (int)OrganisationType.UndefinedSupplier;
                            break;
                        case "Claimant Solicitor":
                            claimOrganisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                            break;
                    }

                    claimOrganisation.OrganisationName = organisation.OrgName;
                    claimOrganisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = organisation.OrgTelephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                    claimOrganisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = organisation.OrgMobile, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });

                    //claimOrganisation.Telephone1 = organisation.OrgTelephone;
                    //claimOrganisation.Telephone2 = organisation.OrgMobile;
                    
                    PipelineAddress organisationAddress = new PipelineAddress();

                    string address = organisation.OrgAddress;
                    if (address != null)
                    {
                        string[] addressParts = address.Split('\n');
                        organisationAddress.Street = addressParts[0];
                    }
                    
                    organisationAddress.PostCode = organisation.OrgPostcode;

                    claimOrganisation.Addresses.Add(organisationAddress);

                    claim.Organisations.Add(claimOrganisation);

                }


                DataTable claimRowsFirst = (from DataRow dr in dtt.AsEnumerable()
                                       where dr.Field<string>("Claim Number") == distinctRow
                                       select dr).Take(1).CopyToDataTable();
                
                foreach (DataRow row in claimRowsFirst.Rows)
                {
                    foreach (DataColumn column in claimRowsFirst.Columns)
                    {
                        switch (column.ColumnName)
                        {
                            #region Claim
                            case "Claim Number":
                                claim.ClaimNumber = row[column].ToString();
                                break;
                            case "Incident Date":
                                string tempDate = row[column].ToString();
                                incidentDate = Convert.ToDateTime(tempDate);
                                break;
                            case "Incident Time":
                                if (TimeSpan.TryParse(row[column].ToString(), out incidentTime))
                                    incidentDate = incidentDate.Add(incidentTime);
                                claim.IncidentDate = incidentDate;
                                break;
                            case "Outstanding":
                                decimal tempReserve = Convert.ToDecimal(row[column].ToString());
                                if (claim.ExtraClaimInfo != null) 
                                    claim.ExtraClaimInfo.Reserve = tempReserve;
                                break;
                            #endregion


                            #region Policy
                            case "Type of Other Party":
                                if (row[column].ToString() == "Policy Holder")
                                    typeOfOtherPolicyIsPolicyHolder = true;
                                break;
                            case "Other Party Policy Number":
                                if (typeOfOtherPolicyIsPolicyHolder)
                                {
                                    claim.Policy.PolicyNumber = row[column].ToString();
                                }
                                break;
                            case "Other Party Policy Start Date":
                                if (typeOfOtherPolicyIsPolicyHolder && row[column].ToString() != string.Empty)
                                {
                                    string tempPolicyDate = row[column].ToString();

                                    try
                                    {
                                        claim.Policy.PolicyStartDate = Convert.ToDateTime(tempPolicyDate);
                                    }
                                    catch (Exception)
                                    {
                                        
                                    }
                                }
                                break;
                            #endregion

                            #region Vehicles

                            #endregion
                        }
                    }
                }
                batch.Claims.Add(claim);
            }

            FileStream writer = null;
            string strXmlFileSave = @"C:\Dev\Projects\MDA\MDASolution\Riverstone\XML\RIL.xml";

            writer = new FileStream(strXmlFileSave, FileMode.Create);
            DataContractSerializer ser = new DataContractSerializer(typeof(PipelineClaimBatch));

            ser.WriteObject(writer, batch);



            return batch;

        }

        private static RiverstonePerson ConvertRowToRiverstonePerson(DataRow row)
        {
            var riverstonePerson = new RiverstonePerson();

            string address = row.Field<string>("Address");
            if (address != null)
            {
                string[] addressParts = address.Split('\n');
                riverstonePerson.Address = addressParts[0];
            }
           
            
            riverstonePerson.ClaimantType = row.Field<string>("Claimant Type");
            if (row.Field<DateTime?>("DOB") != null)
            {
                riverstonePerson.DOB = row.Field<DateTime>("DOB");
            }

            string name = row.Field<string>("Claimant Driver Name");
            string[] nameParts = name.Split(' ');
            riverstonePerson.Salutation = nameParts[0];
            riverstonePerson.EmailAddress = row.Field<string>("Email Address");
            riverstonePerson.Firstname = row.Field<string>("First Name");
            riverstonePerson.Lastname = row.Field<string>("Last Name Organisation");
            riverstonePerson.MobileNum = row.Field<string>("Mobile Telephone Number");
            riverstonePerson.NI = row.Field<string>("National Insurance Number");
            riverstonePerson.Postcode = row.Field<string>("Post Code");
            riverstonePerson.TelephoneNum = row.Field<string>("Telephone Number");

            return riverstonePerson;
        }

        private static RiverstoneVehiclePassenger ConvertRowToRiverstoneVehiclePassenger(DataRow row)
        {
            var riverstoneVehiclePassenger = new RiverstoneVehiclePassenger();

            
            riverstoneVehiclePassenger.ClaimantType = row.Field<string>("Type Of Other Party");

            string address = row.Field<string>("Other Party Address");
            if (address != null)
            {
                string[] addressParts = address.Split('\n');
                riverstoneVehiclePassenger.Address = addressParts[0];
            }
            riverstoneVehiclePassenger.Postcode = row.Field<string>("Other Party Post Code");
            riverstoneVehiclePassenger.TelephoneNum = row.Field<string>("Other Party Telephone Number");
            riverstoneVehiclePassenger.MobileNum = row.Field<string>("Other Party Mobile Telephone Number");

            string name = row.Field<string>("Other Party Name Organisation");
            string[] nameParts = name.Split(' ');
            int namePartsCount = nameParts.Count();

            switch (namePartsCount)
            {
                case 1:
                    riverstoneVehiclePassenger.Salutation = nameParts[0];
                    break;
                case 2:
                     riverstoneVehiclePassenger.Salutation = nameParts[0];
                     riverstoneVehiclePassenger.Firstname = nameParts[1];
                    break;
                case 3:
                     riverstoneVehiclePassenger.Salutation = nameParts[0];
                     riverstoneVehiclePassenger.Firstname = nameParts[1];
                     riverstoneVehiclePassenger.Lastname = nameParts[2];
                     break;
            }

           

            return riverstoneVehiclePassenger;
        }
    }
}
