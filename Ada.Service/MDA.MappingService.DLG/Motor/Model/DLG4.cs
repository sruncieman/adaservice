﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.DLG.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class DLG4
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? Claim_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? Claim_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? Policy_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Policy_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? DM_Claim_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? CC_Vehicle_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? CC_Incident_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DM_Driver_SK;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CC;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Colour;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Make;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Marque;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Model;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RegNo;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? Year;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? Seat_Num;
        
    }
}
