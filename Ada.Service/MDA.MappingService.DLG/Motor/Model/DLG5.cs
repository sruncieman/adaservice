﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.DLG.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class DLG5
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DM_CLAIM_ID;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DM_CONTACT_ID;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EMAIL_ADDRESS_LN_1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EMAIL_ADDRESS_LN_2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String OCCUPATION_TXT;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String UK_COMPANY_REG_NUM;
    }
}
