﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.DLG.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class DLG1
    {
        public DLG1()
        {
            Vehicles = new List<DLG4>();
            ExtraInfo = new List<DLG2>();
          
        }
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? Claim_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Policy_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Business;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Product;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Brand;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimStateDesc;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String LossTypeDesc;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String LossCauseDesc;
        [FieldConverter(typeof(CustomDateConverter2))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? LossDate2;
        [FieldConverter(typeof(CustomDateConverter2))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? ReportedDate2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MoCaseOpName;
        [FieldConverter(typeof(CustomDateConverter2))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PolicyEffectiveDt;
        [FieldConverter(typeof(CustomDateConverter2))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PolicyExpirationDt;
        [FieldConverter(typeof(CustomDateConverter2))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String LineOfBusiness;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CoverTypeDesc;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String BrokerName;
        [FieldIgnored]
        public List<DLG4> Vehicles;
        [FieldIgnored]
        public List<DLG2> ExtraInfo;
       
    }
}
