﻿using RiskEngine.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace RiskEngine.Interfaces
{
    public interface IRiskEngine
    {
        CalculateRiskResponse CalculateRisk(CalculateRiskRequest request);

       RuleGroupData GetWorkingRuleGroupByPath(string entityPath, int orgId);
       RuleGroupData GetLiveRuleGroupByPath(string entityPath, int orgId);
    }

}
