﻿using ADAFtpPullFromClient.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADAFtpPullFromClient
{
    public static class Factory
    {
        public static BaseClientFTP Get(string client)
        {
            string clientName;

            clientName = client.Replace("-", "");

            switch (clientName.ToUpper())
            {
                case "ACE":
                    return new AceFTP(clientName);
                case "ADMIRAL":
                    return new AdmiralFTP(clientName);
                default:
                    return null;
            }
        }
    }
}
