﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinSCP;

namespace ADAFtpPullFromClient.Clients
{
    public class AceFTP : BaseClientFTP
    {
        public AceFTP(string client)
            : base(client)
        {
            this.NetworkPath = ConfigurationManager.AppSettings["AceNetworkAddress"];
            this.DestinationPath = NetworkPath;
            this.StrRemoteDirectory = ConfigurationManager.AppSettings["AceRemoteDirectoryTxt"];
            this.LogFileLocation = ConfigurationManager.AppSettings["AceLogFileLocation"];
            this.FTPHostName = ConfigurationManager.AppSettings["AceHostName"];
            this.FTPUsername = ConfigurationManager.AppSettings["AceUserName"];
            this.SshHostKeyFingerprint = ConfigurationManager.AppSettings["AceSshHostKeyFingerprint"];
            this.Password = ConfigurationManager.AppSettings["AcePassword"];
        }
        public override void DownloadNewFiles(Session session)
        {
            try
            {
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary;
                transferOptions.FileMask = "|*/";
                session.FileTransferred += FilesTransferred;

                int fileCount = 0;
                RemoteDirectoryInfo directory = session.ListDirectory(this.StrRemoteDirectory);

                foreach (RemoteFileInfo fileInfo in directory.Files)
                {
                    string extension = Path.GetExtension(fileInfo.Name);
                    if (string.Compare(extension, ".csv", true) == 0)
                    {
                        if (fileInfo.Name.Contains("SPL_O2_KEO_Claims") || fileInfo.Name.Contains("SPL_O2_KEO_Policies"))
                            fileCount++;
                    }
                }

                if (fileCount == 2)
                {
                    SynchronizationResult synchronizationResult;
                    synchronizationResult = session.SynchronizeDirectories(SynchronizationMode.Local, this.DestinationPath, this.StrRemoteDirectory, false, false, SynchronizationCriteria.Time, transferOptions);

                    foreach (var download in synchronizationResult.Downloads.AsQueryable())
                    {
                        if (download.Error == null)
                        {
                            Console.WriteLine("Download of {0} succeeded, removing from source", download.FileName);
                            var removalResult = session.RemoveFiles(session.EscapeFileMask(download.FileName));

                            if (removalResult.IsSuccess)
                            {
                                Console.WriteLine("Removing of file {0} succeeded", download.FileName);
                            }
                            else
                            {
                                Console.WriteLine("Removing of file {0} failed: {1}", download.FileName, removalResult.Failures.First());
                            }
                        }

                        else
                        {
                            Console.WriteLine("Download of {0} failed: {1}", download.FileName, download.Error.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex);
            }
        }
    }
}