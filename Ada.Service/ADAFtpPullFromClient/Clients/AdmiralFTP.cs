﻿using System;
using System.Configuration;
using WinSCP;

namespace ADAFtpPullFromClient.Clients
{
    public class AdmiralFTP : BaseClientFTP
    {
        public AdmiralFTP(string client)
            : base(client)
        {
            this.NetworkPath = ConfigurationManager.AppSettings["AdmiralNetworkAddress"];
            this.DestinationPath = NetworkPath;
            this.StrRemoteDirectory = ConfigurationManager.AppSettings["AdmiralRemoteDirectoryTxt"];
            this.LogFileLocation = ConfigurationManager.AppSettings["AdmiralLogFileLocation"];
            this.FTPHostName = ConfigurationManager.AppSettings["AdmiralHostName"];
            this.FTPUsername = ConfigurationManager.AppSettings["AdmiralUserName"];
            this.SshHostKeyFingerprint = ConfigurationManager.AppSettings["AdmiralSshHostKeyFingerprint"];
            this.Password = ConfigurationManager.AppSettings["AdmiralPassword"];
        }
    }
}