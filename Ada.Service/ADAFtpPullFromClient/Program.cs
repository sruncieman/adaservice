﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using WinSCP;

namespace ADAFtpPullFromClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var ftpClient = Factory.Get(args[0]);
                var protocolConfigValue = ConfigurationManager.AppSettings["Protocol"];
               
                var sessionOptions = new SessionOptions
                {
                    Protocol = (Protocol)Enum.Parse(typeof(Protocol), protocolConfigValue),
                    HostName = ftpClient.FTPHostName,
                    UserName = ftpClient.FTPUsername,
                    PortNumber = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    SshHostKeyFingerprint = ftpClient.SshHostKeyFingerprint,
                    SshPrivateKeyPath = ftpClient.SshPrivateKeyPath,
                    SshPrivateKeyPassphrase = ftpClient.SshPrivateKeyPassphrase,
                    Password = ftpClient.Password
                };

                using (Session session = new Session())
                {
                    session.SessionLogPath = ftpClient.LogFileLocation + "Log.txt";
                    session.Open(sessionOptions);
                    ftpClient.DownloadNewFiles(session);
                }
            }

            catch (Exception ex)
            {
                BaseClientFTP.SendEmail(false, ex, args[0].Replace("-", ""));
            }
        }
    }
}
