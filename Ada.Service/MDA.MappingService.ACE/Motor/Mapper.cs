﻿using FileHelpers;
using Ionic.Zip;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Common.Server;
using MDA.MappingService.ACE.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.ACE.Motor
{
    public class Mapper
    {
        private Stream _fs;
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        private List<Claim> _lstClaim;
        private object statusTracking;
        private static bool _boolDebug = true;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        public bool BoolDebug { get; set; }
        public string FilePath { get; set; }
        public string FolderPath { get; set; }
        public string ClaimFile { get; set; }
        public string PolicyFile { get; set; }
        public Claim[] ACE_Claim { get; set; }
        public Policy[] ACE_Policy { get; set; }
        public FileHelperEngine ClaimEngine { get; set; }
        public FileHelperEngine PolicyEngine { get; set; }
        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking)
        {
            this._fs = fs;
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;
            _lstClaim = new List<Claim>();
        }
        public enum InsuredPersonHandsetType
        {
            Insured,
            Updated,
            Replacement
        }
        public void AttemptToExtractFilesFromZip()
        {
            try
            {
                if (_fs != null)
                {
                    _boolDebug = false;

                    using (ZipFile zip1 = ZipFile.Read(_fs)) //_filePath))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            var extractTmpPath = string.Concat(ClientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);


                            string unpackDirectory = ClientFolder;
                            e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in extraction of files: " + ex);
            }
        }
        public void AssignFiles()
        {
            try
            {
                if (_boolDebug)
                {
                    FolderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\ACE\Resource\";
                    ClaimFile = FolderPath + @"\ACE_PilotClaimExtract.csv";
                    PolicyFile = FolderPath + @"\ACE_PilotPolicyExtract.csv";
                }
                else
                {
                    ClaimFile = ClientFolder + @"\ACE_PilotClaimExtract.csv";
                    PolicyFile = ClientFolder + @"\ACE_PilotPolicyExtract.csv";
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in assigning extracted files: " + ex);
            }
        }
        public void InitialiseFileHelperEngines()
        {
            try
            {
                ClaimEngine = new FileHelperEngine(typeof(Claim));
                PolicyEngine = new FileHelperEngine(typeof(Policy));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }
        public void PopulateFileHelperEngines()
        {
            try
            {
                ACE_Claim = ClaimEngine.ReadFile(ClaimFile) as Claim[];
                ACE_Policy = PolicyEngine.ReadFile(PolicyFile) as Policy[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }
        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers 
                if (ACE_Claim != null)
                    _lstClaim = ACE_Claim
                        .GroupBy(i => i.ClaimNumber)
                        .Select(g => g.First())
                        .ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }

        }
        public void Translate()
        {
            int _skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["AceSkipNum"]);
            int _takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["AceTakeNum"]);
            
            foreach (var claim in _lstClaim.Skip(_skipValue).Take(_takeValue))
            {

                // Is this person an organisation? - Skip claim if organisation
                if ((claim.Title.Trim() == string.Empty && claim.CustomerForname.Trim() == string.Empty) || (claim.Title.Trim() == string.Empty && claim.CustomerForname == "."))
                {
                    string strSalutation = claim.CustomerSurname.Split(' ')[0].Replace(".","");

                    if(SalutationHelper.GetSalutation(strSalutation)==Salutation.Unknown)
                        continue;
                }


                var policyRecord = ACE_Policy.Where(x => x.POLICY == claim.PolicyNumber).FirstOrDefault();
                
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                #region GeneralClaimData
                try
                {
                    motorClaim.ClaimNumber = claim.ClaimNumber.Replace("CLAIM_WORK_CLM_", "");
                    motorClaim.IncidentDate = Convert.ToDateTime(claim.IncidentDate);
                    motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general claim data: " + ex);
                }
                #endregion


                #region ClaimInfo
                try
                {
                    if(!string.IsNullOrEmpty(claim.ClaimStatus)){
                        switch (claim.ClaimStatus.ToUpper())
                        {
                            case "PENDING - 2020 RETRY":
                            case "PENDING - AWAITING FURTHER INFO":
                            case "PENDING - AWAITING POLICY":
                            case "PENDING - AWAITING RESTOCK":
                            case "PENDING - AWAITING RGS CONF":
                            case "PENDING - CLAIM FORM":
                            case "PENDING - CLAIM INVESTIGATION":
                            case "PENDING - COLLECT DEDUCTIBLE":
                            case "PENDING - CUSTOMER REPLY":
                            case "PENDING - ESCALATION RGS":
                            case "PENDING - FULFILLMENT":
                            case "PENDING - IN PROGRESS":
                            case "PENDING - NEW FULFILLMENT":
                            case "PENDING - POLICY NOT FOUND":
                            case "PENDING - PROBLEM CLAIM":
                            case "RESOLVED - APPEALING":
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                break;
                            case "RESOLVED - COMPLETED":
                            case "RESOLVED - CUSTOMERREPLY":
                            case "RESOLVED - DENIED":
                            case "RESOLVED - OVERTURNED":
                            case "RESOLVED - RECEIVED":
                            case "RESOLVED - SHIPPED":
                            case "RESOLVED - TECH HELP":
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                break;
                            case "RESOLVED - ABANDONED":
                            case "RESOLVED - AUTO ABANDONED":
                            case "RESOLVED - CANCELLED":
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Withdrawn;
                                break;
                        }
                    }

                    motorClaim.ExtraClaimInfo.ClaimNotificationDate = claim.ClaimNotificationDate;
                    
                    if (!string.IsNullOrEmpty(claim.Incident_Type))
                        motorClaim.ExtraClaimInfo.ClaimCode = claim.Incident_Type;

                    if (!string.IsNullOrEmpty(claim.IncidentLocation))
                        motorClaim.ExtraClaimInfo.IncidentLocation = claim.IncidentLocation;

                    if (!string.IsNullOrEmpty(claim.CauseOfLoss))
                        motorClaim.ExtraClaimInfo.IncidentCircumstances = claim.CauseOfLoss;

                    motorClaim.ExtraClaimInfo.Reserve = claim.TotalLessExcessCost;

                    if (claim.PoliceNotifiedDate != null)
                    {
                        motorClaim.ExtraClaimInfo.PoliceAttended = true;
                        if (!string.IsNullOrEmpty(claim.PoliceStation))
                            motorClaim.ExtraClaimInfo.PoliceForce = claim.PoliceStation;
                        
                        if (!string.IsNullOrEmpty(claim.CrimeRefNumber))
                            motorClaim.ExtraClaimInfo.PoliceReference = claim.CrimeRefNumber;
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating claim info data: " + ex);
                }
                #endregion


                #region Policy
                try
                {
                    if (policyRecord != null)
                    {
                        motorClaim.Policy.Insurer = "ACE";
                        if (!string.IsNullOrEmpty(policyRecord.POLICY))
                            motorClaim.Policy.PolicyNumber = policyRecord.POLICY;

                        motorClaim.Policy.PolicyStartDate = Convert.ToDateTime(policyRecord.START_DATE);
                        motorClaim.Policy.PolicyEndDate = Convert.ToDateTime(policyRecord.CANCEL_DATE);

                        if (claim.ClaimHistoryCounter != null)
                            motorClaim.Policy.PreviousFaultClaimsCount = claim.ClaimHistoryCounter;
                      
                        if (policyRecord.GROSS_PREMIUM != null)
                            motorClaim.Policy.Premium = policyRecord.GROSS_PREMIUM;
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating claim policy data: " + ex);
                }
                #endregion

                #region Insured Person Handsets
                try
                {
                    PipelineVehicle insuredHandset = AddHandsetToInsuredPerson(claim, InsuredPersonHandsetType.Insured);
                    PipelineVehicle updatedHandset = AddHandsetToInsuredPerson(claim, InsuredPersonHandsetType.Updated);
                    PipelineVehicle replacementHandset = AddHandsetToInsuredPerson(claim, InsuredPersonHandsetType.Replacement);
                    motorClaim.Vehicles.Add(insuredHandset);
                    motorClaim.Vehicles.Add(updatedHandset);
                    motorClaim.Vehicles.Add(replacementHandset);

                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured person handset data: " + ex);
                }

                #endregion


                #region Policyholder Handset
                try
                {
                    if (policyRecord != null)
                    {
                        PipelineVehicle policyHandset = AddHandsetToPolicyHolder(claim, policyRecord);
                        motorClaim.Vehicles.Add(policyHandset);
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating policy holder handset data: " + ex);
                }

                #endregion


                if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;
            }
        }
        private PipelineVehicle AddHandsetToPolicyHolder(Claim claim, Policy policy)
        {
            PipelineVehicle handset = new PipelineVehicle();
            PipelinePerson policyHolder = new PipelinePerson();
            PipelineAddress policyHolderAddress = new PipelineAddress();
            
            #region Handset

            handset.VehicleType_Id = (int)VehicleType.Bicycle;
            handset.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.WitnessVehicle;

            if (!string.IsNullOrEmpty(claim.InsuredHandsetIMEI))
                handset.VIN = claim.InsuredHandsetIMEI;

            if (!string.IsNullOrEmpty(claim.InsuredHandsetMake))
                handset.VehicleMake = claim.InsuredHandsetMake;

            if (!string.IsNullOrEmpty(claim.InsuredHandsetModel))
                handset.VehicleModel = claim.InsuredHandsetModel;
            #endregion

            #region PolicyHolder
            policyHolder.I2Pe_LinkData.PartyType_Id = (int)PartyType.Policyholder;

            // Split name
            if ((policy.FIRST_NAME.Trim() == string.Empty) || (policy.FIRST_NAME.Trim() == "."))
            {
                int nameSectionCount = policy.SURNAME.Split(' ').Count();
                if (nameSectionCount > 1 && nameSectionCount < 5)
                {
                    switch (nameSectionCount)
                    {
                        case 2:
                            policyHolder.Salutation_Id = (int)SalutationHelper.GetSalutation(policy.SURNAME.Split(' ')[0].Replace(".", ""));
                            policyHolder.LastName = policy.SURNAME.Split(' ')[1];
                            break;
                        case 3:
                            policyHolder.Salutation_Id = (int)SalutationHelper.GetSalutation(policy.SURNAME.Split(' ')[0].Replace(".", ""));
                            policyHolder.FirstName = policy.SURNAME.Split(' ')[1];
                            policyHolder.LastName = policy.SURNAME.Split(' ')[2];
                            break;
                        case 4:
                            policyHolder.Salutation_Id = (int)SalutationHelper.GetSalutation(policy.SURNAME.Split(' ')[0].Replace(".", ""));
                            policyHolder.FirstName = policy.SURNAME.Split(' ')[1];
                            policyHolder.MiddleName = policy.SURNAME.Split(' ')[2];
                            policyHolder.LastName = policy.SURNAME.Split(' ')[3];
                            break;
                    }
                }
                if (nameSectionCount > 4)
                {
                    string[] nameSections = policy.SURNAME.Split(' ');
                    policyHolder.Salutation_Id = (int)SalutationHelper.GetSalutation(policy.SURNAME.Split(' ')[0].Replace(".", ""));
                    policyHolder.FirstName = policy.SURNAME.Split(' ')[1];
                    policyHolder.MiddleName = policy.SURNAME.Split(' ')[2];
                    policyHolder.LastName = policy.SURNAME.Split(' ')[3];

                    foreach (var nameSection in nameSections.Skip(4))
                    {
                        policyHolder.LastName += " " + nameSection;
                    }
                }
            }
            else
            {


                if (!string.IsNullOrEmpty(policy.FIRST_NAME))
                    policyHolder.FirstName = policy.FIRST_NAME;

                if (!string.IsNullOrEmpty(policy.SURNAME))
                    policyHolder.LastName = policy.SURNAME;
            }

            policyHolder.DateOfBirth = policy.DATE_OF_BIRTH;

            if (!string.IsNullOrEmpty(policy.MOBILE_NUMBER))
                policyHolder.Telephones.Add(new PipelineTelephone()
                {
                    ClientSuppliedNumber = policy.MOBILE_NUMBER,
                    TelephoneType_Id =
                        (int)MDA.Common.Enum.TelephoneType.Mobile
                });


            #endregion

            #region Address
            if (!string.IsNullOrEmpty(policy.ADDRESS_LINE_1))
                policyHolderAddress.Street = policy.ADDRESS_LINE_1;

            if (!string.IsNullOrEmpty(policy.ADDRESS_LINE_2))
                policyHolderAddress.Locality = policy.ADDRESS_LINE_2;

            if (!string.IsNullOrEmpty(policy.ADDRESS_LINE_3))
                policyHolderAddress.Town = policy.ADDRESS_LINE_3;

            if (!string.IsNullOrEmpty(policy.ADDRESS_LINE_4))
                policyHolderAddress.County = policy.ADDRESS_LINE_4;

            if (!string.IsNullOrEmpty(policy.POST_CODE))
                policyHolderAddress.PostCode = policy.POST_CODE;

            policyHolderAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.LinkedAddress;
            #endregion

            CheckEmptyAddress(policyHolder, policyHolderAddress);

            handset.People.Add(policyHolder);

            return handset;
        }
        private static PipelineVehicle AddHandsetToInsuredPerson(Claim claim, InsuredPersonHandsetType insuredPersonHandsetType)
        {
            PipelineVehicle handset = new PipelineVehicle();
            PipelinePerson insuredPerson = new PipelinePerson();
            PipelineAddress insuredAddress = new PipelineAddress();
            PipelineAddress deliveryAddress = new PipelineAddress();
            PipelineAddress pickUpAddress = new PipelineAddress();
            PipelineAddress jiffyAddress = new PipelineAddress();


            #region Handset

            handset.VehicleType_Id = (int)VehicleType.Bicycle;

            switch (insuredPersonHandsetType)
            {
                case InsuredPersonHandsetType.Insured:
                    handset.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                    if (!string.IsNullOrEmpty(claim.InsuredHandsetIMEI))
                        handset.VIN = claim.InsuredHandsetIMEI;

                    if (!string.IsNullOrEmpty(claim.InsuredHandsetMake))
                        handset.VehicleMake = claim.InsuredHandsetMake;

                    if (!string.IsNullOrEmpty(claim.InsuredHandsetModel))
                        handset.VehicleModel = claim.InsuredHandsetModel;

                    if (!string.IsNullOrEmpty(claim.DeviceFault))
                        handset.DamageDescription = claim.DeviceFault;
                            
                        break;
                case InsuredPersonHandsetType.Updated:
                    handset.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredHireVehicle;
                    if (!string.IsNullOrEmpty(claim.UpdatedProductIMEI))
                        handset.VIN = claim.UpdatedProductIMEI;

                    if (!string.IsNullOrEmpty(claim.UpdatedProductMake))
                        handset.VehicleMake = claim.UpdatedProductMake;

                    if (!string.IsNullOrEmpty(claim.UpdatedProductModel))
                        handset.VehicleModel = claim.UpdatedProductModel;

                    if (!string.IsNullOrEmpty(claim.DeviceFault))
                        handset.DamageDescription = claim.DeviceFault;
                    
                        break;
                case InsuredPersonHandsetType.Replacement:
                    handset.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                    if (!string.IsNullOrEmpty(claim.ReplacementPhoneIMEI))
                        handset.VIN = claim.ReplacementPhoneIMEI;

                    if (!string.IsNullOrEmpty(claim.ReplacementPhoneMake))
                        handset.VehicleMake = claim.ReplacementPhoneMake;

                    if (!string.IsNullOrEmpty(claim.ReplacementPhoneModel))
                        handset.VehicleModel = claim.ReplacementPhoneModel;
                    
                        break;
            }
            #endregion

            #region Insured Person

            insuredPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;

            // Split name
            if ((claim.Title.Trim() == string.Empty && claim.CustomerForname.Trim() == string.Empty) || (claim.Title.Trim() == string.Empty && claim.CustomerForname == "."))
            {
                int nameSectionCount = claim.CustomerSurname.Split(' ').Count();
                if (nameSectionCount > 1 && nameSectionCount < 5)
                {
                    switch (nameSectionCount)
                    {
                        case 2:
                            insuredPerson.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.CustomerSurname.Split(' ')[0].Replace(".",""));
                            insuredPerson.LastName = claim.CustomerSurname.Split(' ')[1];
                            break;
                        case 3:
                            insuredPerson.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.CustomerSurname.Split(' ')[0].Replace(".", ""));
                            insuredPerson.FirstName = claim.CustomerSurname.Split(' ')[1];
                            insuredPerson.LastName = claim.CustomerSurname.Split(' ')[2];
                            break;
                        case 4:
                            insuredPerson.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.CustomerSurname.Split(' ')[0].Replace(".", ""));
                            insuredPerson.FirstName = claim.CustomerSurname.Split(' ')[1];
                            insuredPerson.MiddleName = claim.CustomerSurname.Split(' ')[2];
                            insuredPerson.LastName = claim.CustomerSurname.Split(' ')[3];
                            break;
                    }
                }
                if (nameSectionCount > 4)
                {
                    string[] nameSections = claim.CustomerSurname.Split(' ');
                    insuredPerson.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.CustomerSurname.Split(' ')[0].Replace(".", ""));
                    insuredPerson.FirstName = claim.CustomerSurname.Split(' ')[1];
                    insuredPerson.MiddleName = claim.CustomerSurname.Split(' ')[2];
                    insuredPerson.LastName = claim.CustomerSurname.Split(' ')[3];

                    foreach (var nameSection in nameSections.Skip(4))
                    {
                        insuredPerson.LastName += " " + nameSection;
                    }
                }
            }
            else
            {

                if (!string.IsNullOrEmpty(claim.Title))
                    insuredPerson.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.Title);

                if (!string.IsNullOrEmpty(claim.CustomerForname))
                    insuredPerson.FirstName = claim.CustomerForname;

                if (!string.IsNullOrEmpty(claim.CustomerSurname))
                    insuredPerson.LastName = claim.CustomerSurname;
            }

            if (!string.IsNullOrEmpty(claim.CustomerEmail))
            {
                insuredPerson.EmailAddresses.Add(new PipelineEmail()
                {
                    EmailAddress = claim.CustomerEmail
                });
            }

            if (!string.IsNullOrEmpty(claim.MobileNumber))
                insuredPerson.Telephones.Add(new PipelineTelephone()
                {
                    ClientSuppliedNumber = claim.MobileNumber,
                    TelephoneType_Id =
                        (int)MDA.Common.Enum.TelephoneType.Mobile
                });

            #endregion

            #region Insured Address

            if (!string.IsNullOrEmpty(claim.CustomerAddressLine1))
                insuredAddress.Street = claim.CustomerAddressLine1;

            if (!string.IsNullOrEmpty(claim.CustomerAddressLine2))
                insuredAddress.Locality = claim.CustomerAddressLine2;

            if (!string.IsNullOrEmpty(claim.CustomerAddressLine3))
                insuredAddress.Town = claim.CustomerAddressLine3;

            if (!string.IsNullOrEmpty(claim.CustomerAddressLine4))
                insuredAddress.County = claim.CustomerAddressLine4;

            if (!string.IsNullOrEmpty(claim.S6PostCode))
                insuredAddress.PostCode = claim.S6PostCode;

            insuredAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

            #endregion

            #region Delivery Address

            if (!string.IsNullOrEmpty(claim.DeliveryAddressLine1))
                deliveryAddress.Street = claim.DeliveryAddressLine1;

            if (!string.IsNullOrEmpty(claim.DeliveryAddressLine2))
                deliveryAddress.Locality = claim.DeliveryAddressLine2;

            if (!string.IsNullOrEmpty(claim.DeliveryAddressLine3))
                deliveryAddress.Town = claim.DeliveryAddressLine3;

            if (!string.IsNullOrEmpty(claim.DeliveryAddressLine4))
                deliveryAddress.County = claim.DeliveryAddressLine4;

            if (!string.IsNullOrEmpty(claim.DeliveryPostCode))
                deliveryAddress.PostCode = claim.DeliveryPostCode;

            deliveryAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.RegisteredOffice;
            #endregion

            #region PickUp Address

            if (!string.IsNullOrEmpty(claim.PickUpAddress1))
                pickUpAddress.Street = claim.PickUpAddress1;

            if (!string.IsNullOrEmpty(claim.PickUpADDRESS2))
                pickUpAddress.Locality = claim.PickUpADDRESS2;

            if (!string.IsNullOrEmpty(claim.pICKuPAddress3))
                pickUpAddress.Town = claim.pICKuPAddress3;

            if (!string.IsNullOrEmpty(claim.PickUpAddress4))
                pickUpAddress.County = claim.PickUpAddress4;

            if (!string.IsNullOrEmpty(claim.PickUpPostCode))
                pickUpAddress.PostCode = claim.PickUpPostCode;

            pickUpAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.PreviousAddress;

            #endregion

            #region Jiffy Address

            if (!string.IsNullOrEmpty(claim.JiffyBagAddress1))
                jiffyAddress.Street = claim.JiffyBagAddress1;

            if (!string.IsNullOrEmpty(claim.JiffyBagAddress2))
                jiffyAddress.Locality = claim.JiffyBagAddress2;

            if (!string.IsNullOrEmpty(claim.JiffyBagAddress3))
                jiffyAddress.Town = claim.JiffyBagAddress3;

            if (!string.IsNullOrEmpty(claim.JiffyBagAddress4))
                jiffyAddress.County = claim.JiffyBagAddress4;

            if (!string.IsNullOrEmpty(claim.JiffyBagPostCode))
                jiffyAddress.PostCode = claim.JiffyBagPostCode;


            jiffyAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.StorageAddress;
            #endregion
            
            CheckEmptyAddress(insuredPerson, insuredAddress);
            CheckEmptyAddress(insuredPerson, deliveryAddress);
            CheckEmptyAddress(insuredPerson, pickUpAddress);
            CheckEmptyAddress(insuredPerson, jiffyAddress);
            
            handset.People.Add(insuredPerson);
            return handset;
        }
        private static void CheckEmptyAddress(PipelinePerson person, PipelineAddress addressType)
        {
            if (addressType.Street == null && addressType.Locality == null && addressType.Town == null && addressType.County == null && addressType.PostCode == null)
            { } 
            else
            {
                person.Addresses.Add(addressType);
            }
        }
        public void DeleteExtractedFiles(string clientFolder, bool deleteExtractedFiles)
        {
            if (deleteExtractedFiles)
            {
                try
                {
                    string[] filePaths = Directory.GetFiles(clientFolder);
                    foreach (string file in filePaths)
                    {
                        File.Delete(file);
                    }
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in deleting extracted files: " + ex);
                }
            }
        }
    }
}
