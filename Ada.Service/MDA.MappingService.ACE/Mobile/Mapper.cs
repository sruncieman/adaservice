﻿using FileHelpers;
using Ionic.Zip;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Common.Server;
using MDA.MappingService.ACE.Mobile.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MDA.MappingService.ACE.Mobile
{
    public class Mapper
    {
        private Stream _fs;
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        private List<Claim> _lstClaim;
        private object statusTracking;
        private static bool _boolDebug = true;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        public bool BoolDebug { get; set; }
        public string FilePath { get; set; }
        public string FolderPath { get; set; }
        public string ClaimFile { get; set; }
        public string PolicyFile { get; set; }
        public Claim[] ACE_Claim { get; set; }
        public Policy[] ACE_Policy { get; set; }
        public FileHelperEngine ClaimEngine { get; set; }
        public FileHelperEngine PolicyEngine { get; set; }

        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking)
        {
            this._fs = fs;
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;
            _lstClaim = new List<Claim>();
        }

        public Regex reg = new Regex(@"([^\^,\r\n])""+(?=[^$,\r\n])",RegexOptions.Multiline);

        public void AttemptToExtractFilesFromZip()
        {
            try
            {
                if (_fs != null)
                {
                    _boolDebug = false;

                    using (ZipFile zip1 = ZipFile.Read(_fs)) //_filePath))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            var extractTmpPath = string.Concat(ClientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            //GC.Collect();

                            //GC.WaitForPendingFinalizers();

                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);

                            

                            string unpackDirectory = ClientFolder;
                            e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in extraction of files: " + ex);
            }
        }

        public void AssignFiles()
        {
            try
            {
                if (_boolDebug)
                {
                    FolderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\ACE\Data\PilotDataExtract_November2014";
                    ClaimFile = FolderPath + @"\ACE_PilotClaimExtract.csv";
                    PolicyFile = FolderPath + @"\ACE_PilotPolicyExtract.csv";
                }
                else
                {
                    ClaimFile = ClientFolder + @"\ACE_PilotClaimExtract.csv";
                    PolicyFile = ClientFolder + @"\ACE_PilotPolicyExtract.csv";
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in assigning extracted files: " + ex);
            }
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                ClaimEngine = new FileHelperEngine(typeof(Claim));
                ClaimEngine.BeforeReadRecord += ClaimEngine_BeforeReadRecord;
                PolicyEngine = new FileHelperEngine(typeof(Policy));
                PolicyEngine.BeforeReadRecord += PolicyEngine_BeforeReadRecord;
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        void PolicyEngine_BeforeReadRecord(EngineBase engine, FileHelpers.Events.BeforeReadEventArgs<object> e)
        {
            try
            {
                e.RecordLine = reg.Replace(e.RecordLine, @"$1");
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in initialising policy file helper engine: (Reg Exp)" + ex);
            }
        }

        void ClaimEngine_BeforeReadRecord(EngineBase engine, FileHelpers.Events.BeforeReadEventArgs<object> e)
        {
            try
            {
                    e.RecordLine = reg.Replace(e.RecordLine, @"$1");
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in initialising claim file helper engine: (Reg Exp)" + ex);
            }
        }
        

        public void PopulateFileHelperEngines()
        {
            try
            {
               
                ACE_Claim = ClaimEngine.ReadFile(ClaimFile) as Claim[];
                ACE_Policy = PolicyEngine.ReadFile(PolicyFile) as Policy[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers 
                if (ACE_Claim != null)
                    _lstClaim = ACE_Claim
                        .GroupBy(i => i.ClaimNumber)
                        .Select(g => g.First())
                        .ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }

        }

        public void Translate()
        {
            int _skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["AceSkipNum"]);
            int _takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["AceTakeNum"]);

            foreach (var claim in _lstClaim.Skip(_skipValue).Take(_takeValue))
            {

                PipelineMobileClaim mobileClaim = new PipelineMobileClaim();

                var policyRecord = ACE_Policy.Where(x => x.POLICY == claim.PolicyNumber).FirstOrDefault();

                #region Organisation

                bool policyHolder = false;

                // Is this person an organisation? 
                if ((claim.Title.Trim() == string.Empty && claim.CustomerForname.Trim() == string.Empty) || (claim.Title.Trim() == string.Empty && claim.CustomerForname == "."))
                {
                    string strSalutation = claim.CustomerSurname.Split(' ')[0].Replace(".","");

                    if (SalutationHelper.GetSalutation(strSalutation) == Salutation.Unknown)
                    {
                        PipelineOrganisation organisation = new PipelineOrganisation();

                        if (policyRecord != null)
                        {

                            if (claim.CustomerSurname.ToUpper() == policyRecord.COMPANY_NAME.ToUpper())
                            {
                                organisation.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                policyHolder = true;
                            }
                            else
                            {
                                organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                            }
                        }
                        else
                        {
                            organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                        }

                        organisation.OrganisationName = claim.CustomerSurname;

                        if (!string.IsNullOrEmpty(claim.MobileNumber))
                        {
                            PipelineTelephone mobile = new PipelineTelephone();
                            mobile.TelephoneType_Id = (int)TelephoneType.Mobile;
                            mobile.ClientSuppliedNumber = claim.MobileNumber;
                            organisation.Telephones.Add(mobile);
                        }

                        if (!string.IsNullOrWhiteSpace(claim.CustomerEmail))
                        {
                            PipelineEmail customerEmail = new PipelineEmail();
                            customerEmail.EmailAddress = claim.CustomerEmail;
                            organisation.EmailAddresses.Add(customerEmail);
                        }

                        if (!string.IsNullOrWhiteSpace(claim.AlternateEmail))
                        {
                            PipelineEmail alternativeEmail = new PipelineEmail();
                            alternativeEmail.EmailAddress = claim.AlternateEmail;
                            organisation.EmailAddresses.Add(alternativeEmail);
                        }

                        PipelineAddress deliveryAddress = new PipelineAddress();
                        PipelineAddress pickUpAddress = new PipelineAddress();
                        PipelineAddress jiffyAddress = new PipelineAddress();
                        PipelineAddress insuredAddress = new PipelineAddress();

                        #region Insured Address

                        if (!string.IsNullOrEmpty(claim.CustomerAddressLine1))
                            insuredAddress.Street = claim.CustomerAddressLine1;

                        if (!string.IsNullOrEmpty(claim.CustomerAddressLine2))
                            insuredAddress.Locality = claim.CustomerAddressLine2;

                        if (!string.IsNullOrEmpty(claim.CustomerAddressLine3))
                            insuredAddress.Town = claim.CustomerAddressLine3;

                        if (!string.IsNullOrEmpty(claim.CustomerAddressLine4))
                            insuredAddress.County = claim.CustomerAddressLine4;

                        if (!string.IsNullOrEmpty(claim.S6PostCode))
                            insuredAddress.PostCode = claim.S6PostCode;

                        insuredAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;

                        #endregion

                        #region Delivery Address

                        if (!string.IsNullOrEmpty(claim.DeliveryAddressLine1))
                            deliveryAddress.Street = claim.DeliveryAddressLine1;

                        if (!string.IsNullOrEmpty(claim.DeliveryAddressLine2))
                            deliveryAddress.Locality = claim.DeliveryAddressLine2;

                        if (!string.IsNullOrEmpty(claim.DeliveryAddressLine3))
                            deliveryAddress.Town = claim.DeliveryAddressLine3;

                        if (!string.IsNullOrEmpty(claim.DeliveryAddressLine4))
                            deliveryAddress.County = claim.DeliveryAddressLine4;

                        if (!string.IsNullOrEmpty(claim.DeliveryPostCode))
                            deliveryAddress.PostCode = claim.DeliveryPostCode;

                        deliveryAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.DeliveryAddress;
                        #endregion

                        #region PickUp Address

                        if (!string.IsNullOrEmpty(claim.PickUpAddress1))
                            pickUpAddress.Street = claim.PickUpAddress1;

                        if (!string.IsNullOrEmpty(claim.PickUpADDRESS2))
                            pickUpAddress.Locality = claim.PickUpADDRESS2;

                        if (!string.IsNullOrEmpty(claim.pICKuPAddress3))
                            pickUpAddress.Town = claim.pICKuPAddress3;

                        if (!string.IsNullOrEmpty(claim.PickUpAddress4))
                            pickUpAddress.County = claim.PickUpAddress4;

                        if (!string.IsNullOrEmpty(claim.PickUpPostCode))
                            pickUpAddress.PostCode = claim.PickUpPostCode;

                        pickUpAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.PickupAddress;

                        #endregion

                        #region Jiffy Address

                        if (!string.IsNullOrEmpty(claim.JiffyBagAddress1))
                            jiffyAddress.Street = claim.JiffyBagAddress1;

                        if (!string.IsNullOrEmpty(claim.JiffyBagAddress2))
                            jiffyAddress.Locality = claim.JiffyBagAddress2;

                        if (!string.IsNullOrEmpty(claim.JiffyBagAddress3))
                            jiffyAddress.Town = claim.JiffyBagAddress3;

                        if (!string.IsNullOrEmpty(claim.JiffyBagAddress4))
                            jiffyAddress.County = claim.JiffyBagAddress4;

                        if (!string.IsNullOrEmpty(claim.JiffyBagPostCode))
                            jiffyAddress.PostCode = claim.JiffyBagPostCode;


                        jiffyAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.JiffyBagAddress;
                        #endregion

                        CheckEmptyAddress(organisation, deliveryAddress);
                        CheckEmptyAddress(organisation, pickUpAddress);
                        CheckEmptyAddress(organisation, jiffyAddress);
                        CheckEmptyAddress(organisation, insuredAddress);
                        mobileClaim.Organisations.Add(organisation);

                    }
                }

                if (policyRecord != null)
                {

                    if (policyRecord.CUSTOMER_TYPE.ToUpper() == "C" && policyHolder == false)
                    {
                        PipelineOrganisation organisation = new PipelineOrganisation();
                        organisation.OrganisationType_Id = (int)OrganisationType.PolicyHolder;

                        organisation.OrganisationName = policyRecord.COMPANY_NAME;

                        if (!string.IsNullOrEmpty(policyRecord.MOBILE_NUMBER))
                        {
                            PipelineTelephone mobile = new PipelineTelephone();
                            mobile.TelephoneType_Id = (int)TelephoneType.Mobile;
                            mobile.ClientSuppliedNumber = policyRecord.MOBILE_NUMBER;
                            organisation.Telephones.Add(mobile);
                        }

                        PipelineAddress policyHolderAddress = new PipelineAddress();

                        #region Policy Holder Address

                        if (!string.IsNullOrEmpty(policyRecord.ADDRESS_LINE_1))
                            policyHolderAddress.Street = policyRecord.ADDRESS_LINE_1;

                        if (!string.IsNullOrEmpty(policyRecord.ADDRESS_LINE_2))
                            policyHolderAddress.Locality = policyRecord.ADDRESS_LINE_2;

                        if (!string.IsNullOrEmpty(policyRecord.ADDRESS_LINE_3))
                            policyHolderAddress.Town = policyRecord.ADDRESS_LINE_3;

                        if (!string.IsNullOrEmpty(policyRecord.ADDRESS_LINE_4))
                            policyHolderAddress.County = policyRecord.ADDRESS_LINE_4;

                        if (!string.IsNullOrEmpty(policyRecord.POST_CODE))
                            policyHolderAddress.PostCode = policyRecord.POST_CODE;

                        policyHolderAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;

                        #endregion

                        CheckEmptyAddress(organisation, policyHolderAddress);

                        mobileClaim.Organisations.Add(organisation);

                    }
                }

                #endregion

                #region GeneralClaimData
                try
                {
                    mobileClaim.ClaimNumber = claim.ClaimNumber.Replace("CLAIM_WORK_CLM_", "");
                    mobileClaim.IncidentDate = Convert.ToDateTime(claim.IncidentDate);


                    if (!string.IsNullOrEmpty(claim.Incident_Type))
                    {
                        switch (claim.Incident_Type.ToUpper())
                        {
                            case "(24 HOUR) ACCIDENTAL DAMAGE - CELL PHONE":
                                mobileClaim.ClaimType_Id = (int)ClaimType.MobilePhoneAccidentalDamage;
                                break;
                            case "(24 HOUR) LOSS - CELL PHONE":
                                mobileClaim.ClaimType_Id = (int)ClaimType.MobilePhoneLoss;
                                break;
                            case "(24 HOUR) THEFT - CELL PHONE":
                                mobileClaim.ClaimType_Id = (int)ClaimType.MobilePhoneTheft;
                                break;
                            default:
                                mobileClaim.ClaimType_Id = (int)ClaimType.MobilePhone;
                                break;
                        }
                    }

                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general claim data: " + ex);
                }
                #endregion
                
                #region ClaimInfo
                try
                {
                    if(!string.IsNullOrEmpty(claim.ClaimStatus)){
                        switch (claim.ClaimStatus.ToUpper())
                        {
                            case "PENDING - 2020 RETRY":
                            case "PENDING - AWAITING FURTHER INFO":
                            case "PENDING - AWAITING POLICY":
                            case "PENDING - AWAITING RESTOCK":
                            case "PENDING - AWAITING RGS CONF":
                            case "PENDING - CLAIM FORM":
                            case "PENDING - CLAIM INVESTIGATION":
                            case "PENDING - COLLECT DEDUCTIBLE":
                            case "PENDING - CUSTOMER REPLY":
                            case "PENDING - ESCALATION RGS":
                            case "PENDING - FULFILLMENT":
                            case "PENDING - IN PROGRESS":
                            case "PENDING - NEW FULFILLMENT":
                            case "PENDING - POLICY NOT FOUND":
                            case "PENDING - PROBLEM CLAIM":
                            case "RESOLVED - APPEALING":
                                mobileClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                break;
                            case "RESOLVED - COMPLETED":
                            case "RESOLVED - CUSTOMERREPLY":
                            case "RESOLVED - OVERTURNED":
                            case "RESOLVED - RECEIVED":
                            case "RESOLVED - SHIPPED":
                            case "RESOLVED - TECH HELP":
                                mobileClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                break;
                            case "RESOLVED - ABANDONED":
                            case "RESOLVED - AUTO ABANDONED":
                            case "RESOLVED - CANCELLED":
                            case "RESOLVED - DENIED":
                                mobileClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Withdrawn;
                                break;
                        }

                    }

                    

                    mobileClaim.ExtraClaimInfo.ClaimNotificationDate = claim.ClaimNotificationDate;
                    
                    if (!string.IsNullOrEmpty(claim.Incident_Type))
                        mobileClaim.ExtraClaimInfo.ClaimCode = claim.Incident_Type;

                    if (!string.IsNullOrEmpty(claim.ClaimStatus))
                        mobileClaim.ExtraClaimInfo.SourceClaimStatus = claim.ClaimStatus;

                    if (!string.IsNullOrEmpty(claim.IncidentLocation))
                        mobileClaim.ExtraClaimInfo.IncidentLocation = claim.IncidentLocation;

                    if (!string.IsNullOrEmpty(claim.CauseOfLoss))
                        mobileClaim.ExtraClaimInfo.IncidentCircumstances = claim.CauseOfLoss;

                    if (!string.IsNullOrEmpty(claim.TotalClaimCost))
                        mobileClaim.ExtraClaimInfo.TotalClaimCost = Convert.ToDecimal(claim.TotalClaimCost);
                    
                    mobileClaim.ExtraClaimInfo.TotalClaimCostLessExcess = claim.TotalLessExcessCost;

                    if (!string.IsNullOrEmpty(claim.BypassFraud))
                    {
                        switch (claim.BypassFraud.ToUpper())
                        {
                            case "TRUE":
                                mobileClaim.ExtraClaimInfo.BypassFraud = true;
                                break;
                            case "FALSE":
                                mobileClaim.ExtraClaimInfo.BypassFraud = false;
                                break;
                        }
                        
                    }

                    if (claim.PoliceNotifiedDate != null)
                    {
                        mobileClaim.ExtraClaimInfo.PoliceAttended = true;
                        if (!string.IsNullOrEmpty(claim.PoliceStation))
                            mobileClaim.ExtraClaimInfo.PoliceForce = claim.PoliceStation;
                        
                        if (!string.IsNullOrEmpty(claim.CrimeRefNumber))
                            mobileClaim.ExtraClaimInfo.PoliceReference = claim.CrimeRefNumber;
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating claim info data: " + ex);
                }
                #endregion
                
                #region Policy
                try
                {
                    if (policyRecord != null)
                    {
                        mobileClaim.Policy.Insurer = "ACE";
                        if (!string.IsNullOrEmpty(policyRecord.POLICY))
                            mobileClaim.Policy.PolicyNumber = policyRecord.POLICY;

                        mobileClaim.Policy.PolicyStartDate = Convert.ToDateTime(policyRecord.START_DATE);
                        mobileClaim.Policy.PolicyEndDate = Convert.ToDateTime(policyRecord.CANCEL_DATE);

                        if (claim.ClaimHistoryCounter != null)
                            mobileClaim.Policy.PreviousFaultClaimsCount = claim.ClaimHistoryCounter;
                      
                        if (policyRecord.GROSS_PREMIUM != null)
                            mobileClaim.Policy.Premium = policyRecord.GROSS_PREMIUM;

                        mobileClaim.Policy.PolicyType_Id = (int)MDA.Common.Enum.PolicyType.Mobile;
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating claim policy data: " + ex);
                }
                #endregion

                #region Policyholder Handset
                try
                {
                    if (policyRecord != null)
                    {
                        if (policyRecord.IMEI.Trim().ToUpper() == claim.InsuredHandsetIMEI.Trim().ToUpper() || policyRecord.IMEI.Trim().ToUpper() == claim.UpdatedProductIMEI.Trim().ToUpper())
                        {

                        }
                        else
                        {
                            PipelineHandset policyHandset = AddHandsetToPolicyHolder(claim, policyRecord);
                            mobileClaim.Handsets.Add(policyHandset);
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating policy holder handset data: " + ex);
                }

                #endregion
                
                #region Insured Person Handsets
                try
                {
                    if (claim.InsuredHandsetIMEI.Trim().ToUpper() == claim.UpdatedProductIMEI.Trim().ToUpper())
                    {
                        PipelineHandset insuredHandset = AddHandsetToInsuredPerson(claim, HandsetType.Insured);
                        mobileClaim.Handsets.Add(insuredHandset);
                    }
                    else
                    {
                        PipelineHandset insuredHandset = AddHandsetToInsuredPerson(claim, HandsetType.Updated); 
                        mobileClaim.Handsets.Add(insuredHandset);
                        PipelineHandset previouslyInsuredHandset = AddHandsetToInsuredPerson(claim, HandsetType.PreviouslyInsuredHandset);
                        mobileClaim.Handsets.Add(previouslyInsuredHandset);
                    }

                    if (!string.IsNullOrEmpty(claim.ReplacementPhoneIMEI) || !string.IsNullOrEmpty(claim.ReplacementPhoneMake) || !string.IsNullOrEmpty(claim.ReplacementPhoneModel))
                    {
                        PipelineHandset replacementHandset = AddHandsetToInsuredPerson(claim, HandsetType.Replacement);
                        mobileClaim.Handsets.Add(replacementHandset);
                    }                                       

                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured person handset data: " + ex);
                }

                #endregion
               
                
                if (ProcessClaimFn(Ctx, mobileClaim, statusTracking) == -1) return;
            }
        }

        private PipelineHandset AddHandsetToPolicyHolder(Claim claim, Policy policy)
        {
            PipelineHandset handset = new PipelineHandset();
            
            #region Handset

            handset.I2H_LinkData.Incident2HandsetLinkType_Id = (int)Incident2HandsetLinkType.PolicyHandset;                     

            if (!string.IsNullOrEmpty(policy.IMEI))
                handset.HandsetIMEI = policy.IMEI;

            if (!string.IsNullOrEmpty(policy.PRODUCT_INSURED))
                handset.HandsetMake = policy.PRODUCT_INSURED;

            #endregion

            #region PolicyHolder
            
            if ((policy.FIRST_NAME.Trim() == string.Empty) || (policy.FIRST_NAME.Trim() == "."))
            {

            }
            else
            {

                PipelinePerson policyHolder = new PipelinePerson();
                policyHolder.I2Pe_LinkData.PartyType_Id = (int)PartyType.Policyholder;
                PipelineAddress policyHolderAddress = new PipelineAddress();

                if (!string.IsNullOrEmpty(policy.FIRST_NAME))
                    policyHolder.FirstName = policy.FIRST_NAME;

                if (!string.IsNullOrEmpty(policy.SURNAME))
                    policyHolder.LastName = policy.SURNAME;

                policyHolder.DateOfBirth = policy.DATE_OF_BIRTH;

                if (!string.IsNullOrEmpty(policy.MOBILE_NUMBER))
                    policyHolder.Telephones.Add(new PipelineTelephone()
                    {
                        ClientSuppliedNumber = policy.MOBILE_NUMBER,
                        TelephoneType_Id =
                            (int)MDA.Common.Enum.TelephoneType.Mobile
                    });


                #region Address
                if (!string.IsNullOrEmpty(policy.ADDRESS_LINE_1))
                    policyHolderAddress.Street = policy.ADDRESS_LINE_1;

                if (!string.IsNullOrEmpty(policy.ADDRESS_LINE_2))
                    policyHolderAddress.Locality = policy.ADDRESS_LINE_2;

                if (!string.IsNullOrEmpty(policy.ADDRESS_LINE_3))
                    policyHolderAddress.Town = policy.ADDRESS_LINE_3;

                if (!string.IsNullOrEmpty(policy.ADDRESS_LINE_4))
                    policyHolderAddress.County = policy.ADDRESS_LINE_4;

                if (!string.IsNullOrEmpty(policy.POST_CODE))
                    policyHolderAddress.PostCode = policy.POST_CODE;

                policyHolderAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                #endregion

                CheckEmptyAddress(policyHolder, policyHolderAddress);

                handset.People.Add(policyHolder);
            }
            
            #endregion
            
            return handset;
        }

        private static PipelineHandset AddHandsetToInsuredPerson(Claim claim, HandsetType insuredPersonHandsetType)
        {
            PipelineHandset handset = new PipelineHandset();            
            PipelineAddress deliveryAddress = new PipelineAddress();
            PipelineAddress pickUpAddress = new PipelineAddress();
            PipelineAddress jiffyAddress = new PipelineAddress();
            PipelineAddress insuredAddress = new PipelineAddress();


            #region Handset

            switch(insuredPersonHandsetType){
                case HandsetType.Insured:
                    handset.I2H_LinkData.Incident2HandsetLinkType_Id = (int)Incident2HandsetLinkType.InsuredHandset;
                    break;
                case HandsetType.Replacement:
                    handset.I2H_LinkData.Incident2HandsetLinkType_Id = (int)Incident2HandsetLinkType.ReplacementHandset;
                    break;
                case HandsetType.Updated:
                    handset.I2H_LinkData.Incident2HandsetLinkType_Id = (int)Incident2HandsetLinkType.InsuredHandset;
                    break;
                case HandsetType.PreviouslyInsuredHandset:
                    handset.I2H_LinkData.Incident2HandsetLinkType_Id = (int)Incident2HandsetLinkType.PreviouslyInsuredHandset;
                    break;
                default:
                    handset.I2H_LinkData.Incident2HandsetLinkType_Id = (int)Incident2HandsetLinkType.Unknown;
                    break;
            }
           

            switch (insuredPersonHandsetType)
            {
                case HandsetType.Insured:
                    if (!string.IsNullOrEmpty(claim.InsuredHandsetIMEI))
                        handset.HandsetIMEI = claim.InsuredHandsetIMEI;

                    if (!string.IsNullOrEmpty(claim.InsuredHandsetMake))
                        handset.HandsetMake = claim.InsuredHandsetMake;

                    if (!string.IsNullOrEmpty(claim.InsuredHandsetModel))
                        handset.HandsetModel = claim.InsuredHandsetModel;

                        handset.HandsetValue = claim.HandsetPrice;

                    if (!string.IsNullOrEmpty(claim.DeviceFault))
                        handset.I2H_LinkData.DeviceFault = claim.DeviceFault;
                        
                        break;
                case HandsetType.Updated:
                    if (!string.IsNullOrEmpty(claim.UpdatedProductIMEI))
                        handset.HandsetIMEI = claim.UpdatedProductIMEI;

                    if (!string.IsNullOrEmpty(claim.UpdatedProductMake))
                        handset.HandsetMake = claim.UpdatedProductMake;

                    if (!string.IsNullOrEmpty(claim.UpdatedProductModel))
                        handset.HandsetModel = claim.UpdatedProductModel;

                    if (!string.IsNullOrEmpty(claim.DeviceFault))
                        handset.I2H_LinkData.DeviceFault = claim.DeviceFault;

                        break;
                case HandsetType.PreviouslyInsuredHandset:
                    if (!string.IsNullOrEmpty(claim.InsuredHandsetIMEI))
                        handset.HandsetIMEI = claim.InsuredHandsetIMEI;

                    if (!string.IsNullOrEmpty(claim.InsuredHandsetMake))
                        handset.HandsetMake = claim.InsuredHandsetMake;

                    if (!string.IsNullOrEmpty(claim.InsuredHandsetModel))
                        handset.HandsetModel = claim.InsuredHandsetModel;

                        handset.HandsetValue = claim.HandsetPrice;

                    if (!string.IsNullOrEmpty(claim.DeviceFault))
                        handset.I2H_LinkData.DeviceFault = claim.DeviceFault;
                  
                        break;
                case HandsetType.Replacement:
                    if (!string.IsNullOrEmpty(claim.ReplacementPhoneIMEI))
                        handset.HandsetIMEI = claim.ReplacementPhoneIMEI;

                    if (!string.IsNullOrEmpty(claim.ReplacementPhoneMake))
                        handset.HandsetMake = claim.ReplacementPhoneMake;

                    if (!string.IsNullOrEmpty(claim.ReplacementPhoneModel))
                        handset.HandsetModel = claim.ReplacementPhoneModel;

                    if (!string.IsNullOrEmpty(claim.ReplacementPhoneGrade))
                        handset.I2H_LinkData.HandsetValueCategory = claim.ReplacementPhoneGrade;


                        break;
            }
            #endregion

            #region Insured Person
                  
            #region Insured Address

            if (!string.IsNullOrEmpty(claim.CustomerAddressLine1))
                insuredAddress.Street = claim.CustomerAddressLine1;

            if (!string.IsNullOrEmpty(claim.CustomerAddressLine2))
                insuredAddress.Locality = claim.CustomerAddressLine2;

            if (!string.IsNullOrEmpty(claim.CustomerAddressLine3))
                insuredAddress.Town = claim.CustomerAddressLine3;

            if (!string.IsNullOrEmpty(claim.CustomerAddressLine4))
                insuredAddress.County = claim.CustomerAddressLine4;

            if (!string.IsNullOrEmpty(claim.S6PostCode))
                insuredAddress.PostCode = claim.S6PostCode;

            insuredAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

            #endregion

            #region Delivery Address

            if (!string.IsNullOrEmpty(claim.DeliveryAddressLine1))
                deliveryAddress.Street = claim.DeliveryAddressLine1;

            if (!string.IsNullOrEmpty(claim.DeliveryAddressLine2))
                deliveryAddress.Locality = claim.DeliveryAddressLine2;

            if (!string.IsNullOrEmpty(claim.DeliveryAddressLine3))
                deliveryAddress.Town = claim.DeliveryAddressLine3;

            if (!string.IsNullOrEmpty(claim.DeliveryAddressLine4))
                deliveryAddress.County = claim.DeliveryAddressLine4;

            if (!string.IsNullOrEmpty(claim.DeliveryPostCode))
                deliveryAddress.PostCode = claim.DeliveryPostCode;

            deliveryAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.DeliveryAddress;
            #endregion

            #region PickUp Address

            if (!string.IsNullOrEmpty(claim.PickUpAddress1))
                pickUpAddress.Street = claim.PickUpAddress1;

            if (!string.IsNullOrEmpty(claim.PickUpADDRESS2))
                pickUpAddress.Locality = claim.PickUpADDRESS2;

            if (!string.IsNullOrEmpty(claim.pICKuPAddress3))
                pickUpAddress.Town = claim.pICKuPAddress3;

            if (!string.IsNullOrEmpty(claim.PickUpAddress4))
                pickUpAddress.County = claim.PickUpAddress4;

            if (!string.IsNullOrEmpty(claim.PickUpPostCode))
                pickUpAddress.PostCode = claim.PickUpPostCode;

            pickUpAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.PickupAddress;

            #endregion

            #region Jiffy Address

            if (!string.IsNullOrEmpty(claim.JiffyBagAddress1))
                jiffyAddress.Street = claim.JiffyBagAddress1;

            if (!string.IsNullOrEmpty(claim.JiffyBagAddress2))
                jiffyAddress.Locality = claim.JiffyBagAddress2;

            if (!string.IsNullOrEmpty(claim.JiffyBagAddress3))
                jiffyAddress.Town = claim.JiffyBagAddress3;

            if (!string.IsNullOrEmpty(claim.JiffyBagAddress4))
                jiffyAddress.County = claim.JiffyBagAddress4;

            if (!string.IsNullOrEmpty(claim.JiffyBagPostCode))
                jiffyAddress.PostCode = claim.JiffyBagPostCode;


            jiffyAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.JiffyBagAddress;
            #endregion

            if ((claim.Title.Trim() == string.Empty && claim.CustomerForname.Trim() == string.Empty) || (claim.Title.Trim() == string.Empty && claim.CustomerForname == "."))
            {

            }
            else
            {

                PipelinePerson insuredPerson = new PipelinePerson();

                insuredPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;

                if (!string.IsNullOrEmpty(claim.Title))
                    insuredPerson.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.Title);

                if (!string.IsNullOrEmpty(claim.CustomerForname))
                    insuredPerson.FirstName = claim.CustomerForname;

                if (!string.IsNullOrEmpty(claim.CustomerSurname))
                    insuredPerson.LastName = claim.CustomerSurname;

                if (!string.IsNullOrWhiteSpace(claim.CustomerEmail))
                {
                    insuredPerson.EmailAddresses.Add(new PipelineEmail()
                    {
                        EmailAddress = claim.CustomerEmail
                    });
                }

                if (!string.IsNullOrWhiteSpace(claim.AlternateEmail))
                {
                    insuredPerson.EmailAddresses.Add(new PipelineEmail()
                    {
                        EmailAddress = claim.AlternateEmail,
                    });
                }

                if (!string.IsNullOrEmpty(claim.MobileNumber))
                    insuredPerson.Telephones.Add(new PipelineTelephone()
                    {
                        ClientSuppliedNumber = claim.MobileNumber,
                        TelephoneType_Id =
                            (int)MDA.Common.Enum.TelephoneType.Mobile
                    });

                CheckEmptyAddress(insuredPerson, deliveryAddress);
                CheckEmptyAddress(insuredPerson, pickUpAddress);
                CheckEmptyAddress(insuredPerson, jiffyAddress);
                CheckEmptyAddress(insuredPerson, insuredAddress);

                handset.People.Add(insuredPerson);
            }            

            #endregion
                               
            return handset;
        }

        private static void CheckEmptyAddress(PipelinePerson person, PipelineAddress addressType)
        {
            if (addressType.Street == null && addressType.Locality == null && addressType.Town == null && addressType.County == null && addressType.PostCode == null)
            { } 
            else
            {
                person.Addresses.Add(addressType);
            }
        }

        private static void CheckEmptyAddress(PipelineOrganisation organisation, PipelineAddress addressType)
        {
            if (addressType.Street == null && addressType.Locality == null && addressType.Town == null && addressType.County == null && addressType.PostCode == null)
            { }
            else
            {
                organisation.Addresses.Add(addressType);
            }
        }

        public void DeleteExtractedFiles(string clientFolder, bool deleteExtractedFiles)
        {
            if (deleteExtractedFiles)
            {
                try
                {
                    string[] filePaths = Directory.GetFiles(clientFolder);
                    foreach (string file in filePaths)
                    {
                        File.Delete(file);
                    }
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in deleting extracted files: " + ex);
                }
            }
        }
              
    }
}
