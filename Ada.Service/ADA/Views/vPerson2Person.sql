﻿CREATE VIEW vPerson2Person

AS 

	SELECT 
			CASE WHEN Person1_Id > Person2_Id THEN Person1_Id ELSE Person2_Id END ParentId,
			CASE WHEN Person1_Id > Person2_Id THEN Person2_Id ELSE Person1_Id END ChildID,
			LinkConfidence
	FROM dbo.Person2Person
	  WHERE Person2PersonLinkType_Id = 1 AND ADARecordStatus = 0
