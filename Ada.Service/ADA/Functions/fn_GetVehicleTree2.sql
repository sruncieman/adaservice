﻿

CREATE FUNCTION [dbo].[fn_GetVehicleTree2]
(
	 @Id INT,
	 @LinkConfidence INT
)
RETURNS 
@T TABLE 
(
	Id INT
)
AS
BEGIN
	
	--DECLARE @LinkConfidence INT = 0

	DECLARE @ObjectLevel INT = 1

	DECLARE @Tree TABLE(ObjectLevel INT, ObjectId INT, LinkConfidence INT)

	INSERT INTO @Tree
	SELECT @ObjectLevel, Vehicle1_Id, LinkConfidence FROM dbo.Vehicle2Vehicle WHERE Vehicle1_Id = @Id AND LinkConfidence <= @LinkConfidence 
	UNION ALL
	SELECT @ObjectLevel, Vehicle2_Id, LinkConfidence FROM dbo.Vehicle2Vehicle WHERE Vehicle2_Id = @Id AND LinkConfidence <= @LinkConfidence 

	WHILE(@@Rowcount > 0)
	BEGIN
		SET @ObjectLevel += 1
		INSERT INTO @Tree
			SELECT @ObjectLevel ObjectLevel, Vehicle1_Id, LinkConfidence FROM dbo.Vehicle2Vehicle 
			WHERE Vehicle2_Id IN(SELECT ObjectId FROM @Tree WHERE ObjectLevel = @ObjectLevel -1) AND Vehicle1_Id NOT IN(SELECT ObjectId FROM @Tree) AND LinkConfidence <= @LinkConfidence 
			UNION ALL
			SELECT @ObjectLevel ObjectLevel, Vehicle2_Id, LinkConfidence FROM dbo.Vehicle2Vehicle 
			WHERE Vehicle1_Id IN(SELECT ObjectId FROM @Tree WHERE ObjectLevel = @ObjectLevel -1) AND Vehicle2_Id NOT IN(SELECT ObjectId FROM @Tree) AND LinkConfidence <= @LinkConfidence 
	END
	
	INSERT INTO @T(Id)
	SELECT DISTINCT ObjectId FROM @Tree WHERE ISNULL(LinkConfidence, @LinkConfidence) = @LinkConfidence 

	RETURN 
END
