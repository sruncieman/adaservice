﻿
CREATE FUNCTION [dbo].[fnGetMatchingPersonTSql] 
(
	 @Id INT
	,@UseRiskClaimId BIT
	,@RiskClaim_Id VARCHAR(MAX)
	,@FirstName VARCHAR(MAX)
	,@LastName VARCHAR(MAX)
	,@DateOfBirth VARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
DECLARE @SQL NVARCHAR(MAX)

DECLARE @From NVARCHAR(1000) ='
FROM Person PER'

DECLARE @JoinRiskClaim NVARCHAR(1000) ='
LEFT OUTER JOIN Person2Policy AS P2PER ON PER.Id = P2PER.Person_Id AND P2PER.AdaRecordStatus = 0
LEFT OUTER JOIN Incident2Person AS I2PER ON PER.Id = I2PER.Person_Id AND I2PER.AdaRecordStatus = 0
LEFT OUTER JOIN Vehicle2Person AS V2PER ON PER.Id = V2PER.Person_Id AND V2PER.AdaRecordStatus = 0'

DECLARE @JoinNINumber NVARCHAR(1000) =  '
INNER JOIN Person2NINumber AS P2NI WITH(NOLOCK) ON PER.Id = P2NI.Person_Id AND P2NI.AdaRecordStatus = 0
INNER JOIN NINumber AS NI WITH(NOLOCK) ON P2NI.NINumber_Id = NI.Id  AND NI.AdaRecordStatus = 0'

DECLARE @JoinDriver NVARCHAR(1000) =  '
INNER JOIN Person2DrivingLicense AS P2DL WITH(NOLOCK) ON PER.Id = P2DL.Person_Id AND P2DL.AdaRecordStatus = 0 
INNER JOIN DrivingLicense AS DL WITH(NOLOCK) ON P2DL.DrivingLicense_Id = DL.Id AND DL.AdaRecordStatus = 0 '

DECLARE @JoinPassport NVARCHAR(1000) =  '
INNER JOIN Person2Passport AS P2Pas WITH(NOLOCK) ON PER.Id = P2Pas.Person_Id AND P2Pas.AdaRecordStatus = 0 
INNER JOIN Passport AS Pas WITH(NOLOCK) ON P2Pas.Passport_Id = Pas.Id  AND Pas.AdaRecordStatus = 0'

DECLARE @JoinPaymentCard NVARCHAR(1000) =  '
INNER JOIN Person2PaymentCard AS P2Pay WITH(NOLOCK) ON PER.Id = P2Pay.Person_Id AND P2Pay.AdaRecordStatus = 0 
INNER JOIN PaymentCard AS Pay WITH(NOLOCK) ON P2Pay.PaymentCard_Id = Pay.Id AND Pay.AdaRecordStatus = 0'

DECLARE @JoinAccount NVARCHAR(1000) =  '
INNER JOIN Person2BankAccount AS P2BA WITH(NOLOCK) ON PER.Id = P2BA.Person_Id AND P2BA.AdaRecordStatus = 0 
INNER JOIN BankAccount AS Acc WITH(NOLOCK) ON P2BA.BankAccount_Id = Acc.Id AND Acc.AdaRecordStatus = 0'

DECLARE @JoinPolicy NVARCHAR(1000) =  '
INNER JOIN Person2Policy AS P2POL ON PER.Id = P2POL.Person_Id  AND P2POL.AdaRecordStatus = 0
INNER JOIN Policy AS POL ON P2POL.Policy_Id = POL.Id AND POL.AdaRecordStatus = 0'

DECLARE @JoinAddress NVARCHAR(1000) =  '
INNER JOIN Person2Address P2A WITH(NOLOCK) ON PER.Id = P2A.Person_Id  AND P2A.AdaRecordStatus = 0
INNER JOIN Address AS _ADD WITH(NOLOCK) ON P2A.Address_ID = _ADD.Id  AND _ADD.AdaRecordStatus = 0
INNER JOIN #Addresses _ADD_ '

DECLARE @JoinTelephone NVARCHAR(1000) =  '
INNER JOIN Person2Telephone AS P2Tel WITH(NOLOCK) ON PER.Id = P2Tel.Person_Id  AND P2Tel.AdaRecordStatus = 0
INNER JOIN Telephone AS Tel WITH(NOLOCK) ON P2Tel.Telephone_Id = Tel.Id  AND Tel.AdaRecordStatus = 0'

DECLARE @JoinVehicle NVARCHAR(1000) =  '
INNER JOIN Vehicle2Person AS V2Per2 WITH(NOLOCK) ON PER.Id = V2Per2.Person_Id  AND V2Per2.AdaRecordStatus = 0
INNER JOIN Vehicle AS Veh WITH(NOLOCK) ON V2Per2.Vehicle_Id = Veh.Id  AND Veh.AdaRecordStatus = 0
INNER JOIN #Vehicles _VEH_ '

DECLARE @JoinOrganisation NVARCHAR(1000) =  '
INNER JOIN Person2Organisation AS P2ORG WITH(NOLOCK) ON PER.Id = P2ORG.Person_Id  AND P2ORG.AdaRecordStatus = 0 AND P2ORG.Person2OrganisationLinkType_Id IN (1,2,3,4,5,6,7,8,9,21,23,25,26,27)
INNER JOIN Organisation AS ORG WITH(NOLOCK) ON P2ORG.Organisation_Id = ORG.Id AND ORG.AdaRecordStatus = 0
INNER JOIN #Organisations _ORG_ '

DECLARE @JoinEmail NVARCHAR(1000) =  '
INNER JOIN Person2Email AS P2E WITH(NOLOCK) ON PER.Id = P2E.Person_Id AND P2E.AdaRecordStatus = 0
INNER JOIN dbo.Email AS EMI WITH(NOLOCK) ON  P2E.Email_Id = EMI.ID AND EMI.AdaRecordStatus = 0'

DECLARE @JoinHand NVARCHAR(1000) =  '
INNER JOIN Handset2Person AS Hand2Per2 WITH(NOLOCK) ON PER.Id = Hand2Per2.Person_Id  AND Hand2Per2.AdaRecordStatus = 0
INNER JOIN Handset AS Hand WITH(NOLOCK) ON Hand2Per2.Handset_Id = Hand.Id  AND Hand.AdaRecordStatus = 0'

SELECT
	@SQL =
	--SELECT
	'SELECT PER.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), CASE WHEN @UseRiskClaimId = 1 THEN 0 ELSE MatchType END) + ' AS MatchType'
	--FROM
	+ @From 
	+ CASE WHEN @UseRiskClaimId = 1 THEN @JoinRiskClaim ELSE '' END 
	+ CASE WHEN NINumber = 1 THEN @JoinNINumber ELSE '' END 
	+ CASE WHEN DriverNumber  = 1 THEN @JoinDriver ELSE '' END 
	+ CASE WHEN PassportNumber  = 1 THEN @JoinPassport ELSE '' END 
  	+ CASE WHEN PaymentCardNumber = 1 THEN @JoinPaymentCard ELSE '' END 
	+ CASE WHEN AccountNumber = 1 THEN @JoinAccount ELSE '' END 
	+ CASE WHEN PolicyNumber = 1 THEN @JoinPolicy ELSE '' END 
	+ CASE WHEN ISNULL(CONVERT(INT, SubBuilding), 0) + ISNULL(CONVERT(INT, Building), 0) +  ISNULL(CONVERT(INT, BuildingNumber), 0) +  ISNULL(CONVERT(INT, Street), 0) +  ISNULL(CONVERT(INT, Locality), 0) +  ISNULL(CONVERT(INT, Town), 0) +  ISNULL(CONVERT(INT, PostCode), 0) + ISNULL(CONVERT(INT, PafUPRN), 0)> 0 THEN @JoinAddress
	+ dbo.fnFormatOnClause(
		  CASE WHEN SubBuilding = 1 THEN 'AND _ADD.SubBuilding = _ADD_.SubBuilding  ' ELSE '' END
		+ CASE WHEN Building = 1 THEN 'AND _ADD.Building = _ADD_.Building ' ELSE '' END
		+ CASE WHEN BuildingNumber = 1 THEN 'AND _ADD.BuildingNumber = _ADD_.BuildingNumber ' ELSE '' END
		+ CASE WHEN Street = 1 THEN 'AND _ADD.Street = _ADD_.Street ' ELSE '' END
		+ CASE WHEN Locality = 1 THEN 'AND _ADD.Locality = _ADD_.Locality ' ELSE '' END
		+ CASE WHEN Town = 1 THEN 'AND _ADD.Town = _ADD_.Town ' ELSE '' END
		+ CASE WHEN PostCode = 1 THEN 'AND _ADD.PostCode = _ADD_.PostCode ' ELSE '' END
		+ CASE WHEN PafUPRN = 1 THEN 'AND NULLIF(_ADD.PafUPRN,'''') = NULLIF(_ADD_.PafUPRN,'''') ' ELSE '' END
	) ELSE '' END 
	+ CASE WHEN TelephoneNumber = 1 THEN @JoinTelephone ELSE '' END 
	+ CASE WHEN ISNULL(CONVERT(INT, VehicleRegistration), 0) +  ISNULL(CONVERT(INT, VehicleVIN), 0) > 0 THEN @JoinVehicle 
	+ dbo.fnFormatOnClause(
			  CASE WHEN VehicleRegistration = 1 THEN 'AND VEH.VehicleRegistration = _VEH_.VehicleRegistration ' ELSE '' END
			+ CASE WHEN VehicleVIN = 1 THEN 'AND VEH.VIN = _VEH_.VehicleVIN ' ELSE '' END
	) ELSE '' END 
	+ CASE WHEN ISNULL(CONVERT(INT, OrganisationName), 0) + ISNULL(CONVERT(INT, RegisteredNumber), 0) +  ISNULL(CONVERT(INT, VATNumber), 0) > 0 THEN @JoinOrganisation
	+ dbo.fnFormatOnClause(
			CASE WHEN OrganisationName = 1 THEN 'AND ORG.OrganisationName = _ORG_.OrganisationName ' ELSE '' END
			+ CASE WHEN RegisteredNumber = 1 THEN 'AND ORG.RegisteredNumber = _ORG_.RegisteredNumber ' ELSE '' END
			+ CASE WHEN VATNumber = 1 THEN 'AND ORG.VATNumber = _ORG_.VATNumber ' ELSE '' END
	) ELSE '' END 
	+ CASE WHEN EmailAddress = 1 THEN @JoinEmail ELSE '' END 
	+ CASE WHEN HandestImei = 1 THEN @JoinHand ELSE '' END 
	--WHERE
	+ dbo.fnFormatAddCarriageReturn('WHERE PER.ADARecordStatus = 0 ') 
	+ CASE WHEN @UseRiskClaimId = 1 THEN 'AND COALESCE(I2PER.BaseRiskClaim_Id, V2PER.BaseRiskClaim_Id, P2PER.BaseRiskClaim_Id) = ''' + @RiskClaim_ID  + '''  ' + CHAR(13) ELSE '' END  
	+ CASE WHEN FirstName = 1 THEN 'AND PER.FirstName = ''' + @FirstName + ''' ' + CHAR(13)  ELSE '' END
	+ CASE WHEN LastName = 1 THEN 'AND PER.LastName = ''' + @LastName + ''' ' + CHAR(13)  ELSE '' END
	+ CASE WHEN DateOfBirth = 1 THEN 'AND CONVERT(DATE, PER.DateOfBirth, 103) = ''' + @DateOfBirth + ''' ' + CHAR(13)  ELSE '' END
	+ CASE WHEN NINumber = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #NINumber _NI_ WHERE _NI_.NINumber = NI.NINumber)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN DriverNumber = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #DrivingLicenses _DL_ WHERE _DL_.DriverNumber = DL.DriverNumber)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN PassportNumber = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #Passports _PN_ WHERE _PN_.PassportNumber = Pas.PassportNumber)' + CHAR(13) ELSE '' END 
	+ CASE WHEN PaymentCardNumber = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #PaymentCards _PC_ WHERE _PC_.PaymentCardNumber = Pay.PaymentCardNumber)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN AccountNumber = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #BankAccounts _BA_ WHERE _BA_.AccountNumber = Acc.AccountNumber)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN PolicyNumber = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #Policies _POL_ WHERE _POL_.PolicyNumber = POL.PolicyNumber)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN EmailAddress = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #EmailAddresses _EA_ WHERE _EA_.EmailAddress = EMI.EmailAddress)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN HandestImei = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #Handsets _HS_ WHERE _HS_.HandsetImei = Hand.HandsetIMEI)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN TelephoneNumber = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #Telephones _TEL_ WHERE _TEL_.TelephoneNumber = Tel.TelephoneNumber)' + CHAR(13)  ELSE '' END 
	--GROUP BY
	+ 'GROUP BY PER.Id'
	FROM MatchingRulesPerson
	WHERE RuleNo = @Id

RETURN @SQL

END
