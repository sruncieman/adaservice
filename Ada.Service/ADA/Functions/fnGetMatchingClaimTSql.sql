﻿

CREATE FUNCTION [dbo].[fnGetMatchingClaimTSql] 
(
	 @Id INT
	,@UseRiskClaimId BIT
	,@RiskClientTableType AS dbo.RiskClientTableType READONLY
	,@RiskClaim_ID VARCHAR(MAX)
	,@ClientClaimReference VARCHAR(MAX)
	,@ClientID VARCHAR(MAX)
	,@IncidentDate VARCHAR(MAX)
	,@IncidentTime VARCHAR(MAX)
	,@IncidentLocation VARCHAR(MAX)
	,@IncidentCircumstances VARCHAR(MAX)
	)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @SQL NVARCHAR(MAX)

DECLARE @From NVARCHAR(1000) ='
FROM dbo.Incident AS INC WITH(NOLOCK)'

DECLARE @JoinRiskClaim NVARCHAR(1000) ='
INNER JOIN #RiskClaim RC ON RC.Incident_ID = INC.Id AND RC.ADARecordStatus = 0 AND RC.BaseRiskClaim_Id = '+ @RiskClaim_ID + ' '

DECLARE @JoinClaimRef NVARCHAR(1000) ='
INNER JOIN (
			SELECT SQ2.Incident_Id, SQ2.[ClaimNumber], SQ2.[ClaimCode], SQ2.[Insurer], SQ2.[ClaimNumberCalcuated], SQ2.[ClaimCodeCalcuated]
			FROM	(
					SELECT I2P2.Incident_Id, I2P2.[ClaimNumber], I2P2.[ClaimCode], I2P2.[Insurer], I2P2.[ClaimNumberCalcuated], I2P2.[ClaimCodeCalcuated] FROM dbo.Incident2Person I2P2 WHERE EXISTS (SELECT TOP 1 1 FROM dbo.Incident INC WHERE INC.Id = I2P2.Incident_Id) AND I2P2.ADARecordStatus = 0
					UNION ALL 
					SELECT I2O.Incident_Id, I2O.[ClaimNumber], I2O.[ClaimCode], I2O.[Insurer], I2O.[ClaimNumberCalcuated], I2O.[ClaimCodeCalcuated]  FROM dbo.Incident2Organisation I2O WHERE EXISTS (SELECT TOP 1 1 FROM dbo.Incident INC WHERE INC.Id = I2O.Incident_Id) AND I2O.ADARecordStatus = 0
					) SQ2
			GROUP BY SQ2.Incident_Id, SQ2.[ClaimNumber], SQ2.[ClaimCode], SQ2.[Insurer], SQ2.[ClaimNumberCalcuated], SQ2.[ClaimCodeCalcuated]
			) JCF ON JCF.Incident_Id = INC.Id
INNER JOIN dbo.InsurersClients IC ON IC.ClientName = JCF.Insurer'

DECLARE @JoinPerson	NVARCHAR(1000) ='
INNER JOIN dbo.Incident2Person I2P WITH(NOLOCK) ON I2P.Incident_Id = INC.ID AND I2P.ADARecordStatus IN (0,10) '

DECLARE @JoinInvolvement	NVARCHAR(1000) ='
INNER JOIN [dbo].[Incident2Vehicle] I2V ON I2V.Incident_Id = INC.Id AND I2V.ADARecordStatus IN (0,10)
INNER JOIN [dbo].[Vehicle2Person] V2P ON V2P.Vehicle_Id = I2V.Vehicle_Id AND V2P.ADARecordStatus IN (0,10)
INNER JOIN [dbo].[Incident2Person] I2P ON I2P.Incident_Id = INC.ID AND I2P.Incident_Id = I2V.Incident_Id AND I2P.Person_Id = V2P.Person_Id AND I2P.ADARecordStatus IN (0,10)'

DECLARE @JoinVehicle NVARCHAR(1000) ='
INNER JOIN Incident2Vehicle I2V WITH(NOLOCK) ON I2V.Incident_Id = INC.Id AND I2V.ADARecordStatus = 0 '

DECLARE @JoinPeopleIn NVARCHAR(1000) ='
INNER JOIN #PerAll PA ON PA.PerID = I2P.Person_Id'

DECLARE @JoinVehicleIn NVARCHAR(1000) ='
INNER JOIN #VecAll VA ON VA.VecID = I2V.Vehicle_Id'

DECLARE @InvolvementPersonExists NVARCHAR(1000) ='
AND EXISTS (SELECT TOP 1 1 FROM #PerAll Per WHERE Per.PerID = I2P.Person_Id)'

DECLARE @InvolvementVehicleExists NVARCHAR(1000) ='
AND EXISTS (SELECT TOP 1 1 FROM #VecAll Veh WHERE Veh.VecID = I2V.Vehicle_Id)'

DECLARE @InvolvementPersonVehicleExists NVARCHAR(1000) ='
AND (EXISTS (SELECT TOP 1 1 FROM #PerAll Per WHERE Per.PerID = I2P.Person_Id) OR EXISTS (SELECT TOP 1 1 FROM #VecAll Veh WHERE Veh.VecID = I2V.Vehicle_Id))'

DECLARE @HavingPeopleCount NVARCHAR(1000) ='
AND COUNT(DISTINCT PA.Id) >='

DECLARE @HavingVehicleCount NVARCHAR(1000) ='
AND COUNT(DISTINCT VA.Id) >='

DECLARE @JoinHand NVARCHAR(1000) ='
INNER JOIN Incident2Handset I2Hand WITH(NOLOCK) ON I2Hand.Incident_Id = INC.Id AND I2Hand.ADARecordStatus = 0 AND I2Hand.[Incident2HandsetLinkType_Id] = 2'

DECLARE @JoinHandIn NVARCHAR(1000) ='
INNER JOIN #HandAll Hand ON Hand.HandID = I2Hand.Handset_Id '

DECLARE @HavingHandCount NVARCHAR(1000) ='
AND COUNT(DISTINCT Hand.Id) >='

DECLARE  @ClientClaimReferenceWhere NVARCHAR(1000) 
		,@ClientClaimCodeWhere NVARCHAR(1000)

IF  EXISTS (SELECT TOP 1 1 FROM @RiskClientTableType WHERE RiskClient_ID IN (19))
BEGIN
	SELECT @ClientClaimReferenceWhere = ' JCF.ClaimNumberCalcuated = '''+ @ClientClaimReference + ''' '
	SELECT @ClientClaimCodeWhere = ' JCF.ClaimCodeCalcuated = '''+ @ClientClaimReference + ''' '
END
ELSE
BEGIN
	SELECT @ClientClaimReferenceWhere = 'JCF.ClaimNumber = '''+ @ClientClaimReference + ''' '
	SELECT @ClientClaimCodeWhere = ' JCF.ClaimCode = ''' + @ClientClaimReference + ''' '
END

IF (SELECT TOP 1 1 FROM dbo.MatchingRulesClaim MRC WHERE RuleNo = @Id AND [Involvements] IS NULL) IS NOT NULL --NoInvolvements
BEGIN
	SELECT @SQL = 
			--SELECT
			'SELECT INC.Id, '
			+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
			+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
			+ CONVERT(VARCHAR(20), CASE WHEN @UseRiskClaimId = 1 THEN 0 ELSE MatchType END) + ' AS MatchType'
			--FROM
			+ @From 
			+ CASE WHEN @UseRiskClaimId = 1 THEN @JoinRiskClaim ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.ClientClaimReference ), 0) + ISNULL(CONVERT(INT,MRC.ClientID ), 0) > 0 THEN + @JoinClaimRef ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.IncidentTime), 0) + ISNULL(CONVERT(INT,MRC.IncidentLocation), 0) + ISNULL(CONVERT(INT,MRC.IncidentCircumstances), 0) + ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) > 0 THEN + @JoinPerson ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN + @JoinVehicle ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.HandsetIMEIMatch), 0) > 0 THEN + @JoinHand ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) > 0 THEN + @JoinPeopleIn ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN + @JoinVehicleIn ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.HandsetIMEIMatch), 0) > 0 THEN + @JoinHandIn ELSE '' END
			--WHERE
			+ dbo.fnFormatAddCarriageReturn('WHERE INC.ADARecordStatus = 0 ') 
			+ CASE WHEN ClientClaimReference = 1 THEN 'AND '+ @ClientClaimReferenceWhere + ' ' ELSE '' END
			+ CASE WHEN ClientClaimCode = 1 THEN 'AND '+ @ClientClaimCodeWhere + ' ' ELSE '' END
			+ CASE WHEN ClientID = 1 THEN 'AND ISNULL(IC.InsurerClientParent_Id,IC.Id) = ''' + @ClientID + ''' ' ELSE '' END
			+ CASE WHEN IncidentTime = 1 THEN 'AND I2P.IncidentTime = ''' + @IncidentTime + ''' ' ELSE '' END
			+ CASE WHEN IncidentLocation = 1 THEN 'AND I2P.IncidentLocation = ''' + @IncidentLocation + ''' ' ELSE '' END
			+ CASE WHEN IncidentCircumstances = 1 THEN 'AND I2P.IncidentCircs = ''' + @IncidentCircumstances + ''' ' ELSE '' END
			+ CASE WHEN IncidentDate = 1 THEN 'AND INC.IncidentDate = ''' + @IncidentDate + ''' ' ELSE '' END
			--GROUP BY
			+ dbo.fnFormatAddCarriageReturn('GROUP BY INC.Id')
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) + ISNULL(CONVERT(INT,MRC.VehicleMatch), 0 + ISNULL(CONVERT(INT,MRC.HandsetIMEIMatch), 0)) > 0 THEN
			--HAVING
			+ dbo.fnFormatAddCarriageReturn('HAVING') 
			+ dbo.fnFormatWhereClause(
				  CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) > 0 THEN @HavingPeopleCount + CAST(MRC.PeopleMatch AS VARCHAR(5)) + ' ' ELSE '' END
				+ CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN @HavingVehicleCount + CAST(MRC.VehicleMatch AS VARCHAR(5)) + ' ' ELSE '' END
				+ CASE WHEN ISNULL(CONVERT(INT,MRC.HandsetIMEIMatch), 0) > 0 THEN @HavingHandCount+ CAST(MRC.HandsetIMEIMatch AS VARCHAR(5)) + ' ' ELSE '' END
				)
			ELSE '' END
			FROM dbo.MatchingRulesClaim MRC WITH(NOLOCK)   
			WHERE RuleNo = @Id
END

IF (SELECT TOP 1 1 FROM dbo.MatchingRulesClaim MRC WHERE RuleNo = @Id AND [Involvements] IS NOT NULL) IS NOT NULL --Involvement
BEGIN
	SELECT @SQL = 
			--SELECT
			'SELECT IncidentID, '
			+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
			+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
			+ CONVERT(VARCHAR(20), CASE WHEN @UseRiskClaimId = 1 THEN 0 ELSE MatchType END) + ' AS MatchType ' + CHAR(13)
			--FROM (SUBQUERY)
			+ 'FROM' + CHAR(13)
			+ '(' + CHAR(13)
			--SELECT
			+ 'SELECT INC.ID IncidentID, I2V.Vehicle_Id VehicleID, I2P.Person_Id PersonID'
			--FROM
			+ @From
			+ CASE WHEN @UseRiskClaimId = 1 THEN @JoinRiskClaim ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.ClientClaimReference ), 0) + ISNULL(CONVERT(INT,MRC.ClientID ), 0) > 0 THEN + @JoinClaimRef ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.IncidentTime), 0) + ISNULL(CONVERT(INT,MRC.IncidentLocation), 0) + ISNULL(CONVERT(INT,MRC.IncidentCircumstances), 0) + ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) + ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN + @JoinInvolvement ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.HandsetIMEIMatch), 0) > 0 THEN + @JoinHand ELSE '' END
			+ CASE WHEN ISNULL(CONVERT(INT,MRC.HandsetIMEIMatch), 0) > 0 THEN + @JoinHandIn ELSE '' END
			--WHERE
			+ dbo.fnFormatAddCarriageReturn('WHERE INC.ADARecordStatus = 0 ')
			+ CASE WHEN ClientClaimReference = 1 THEN 'AND '+ @ClientClaimReferenceWhere + ' ' ELSE '' END
			+ CASE WHEN ClientClaimCode = 1 THEN 'AND '+ @ClientClaimCodeWhere + ' ' ELSE '' END
			+ CASE WHEN ClientID = 1 THEN 'AND ISNULL(IC.InsurerClientParent_Id,IC.Id) = ''' + @ClientID + ''' ' ELSE '' END
			+ CASE WHEN IncidentTime = 1 THEN 'AND I2P.IncidentTime = ''' + @IncidentTime + ''' ' ELSE '' END
			+ CASE WHEN IncidentLocation = 1 THEN 'AND I2P.IncidentLocation = ''' + @IncidentLocation + ''' ' ELSE '' END
			+ CASE WHEN IncidentCircumstances = 1 THEN 'AND I2P.IncidentCircs = ''' + @IncidentCircumstances + ''' ' ELSE '' END
			+ CASE WHEN IncidentDate = 1 THEN 'AND INC.IncidentDate = ''' + @IncidentDate + ''' ' ELSE '' END
			+ CASE  WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) > 0 AND ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN @InvolvementPersonVehicleExists
					WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) > 0 THEN @InvolvementPersonExists 
					WHEN ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN @InvolvementVehicleExists 
					ELSE '' END + CHAR(13)	
			--GROUP BY
			+ 'GROUP BY INC.ID, INC.IncidentDate, I2V.Vehicle_Id, I2P.Person_Id'
			+ CHAR(13)
			+ ') SubQ '
			--GROUP BY
			+ dbo.fnFormatAddCarriageReturn('GROUP BY IncidentID')
			--HAVING
			+ dbo.fnFormatAddCarriageReturn('HAVING COUNT(DISTINCT VehicleID) >= ' + CAST(MRC.Involvements AS VARCHAR(5)))
			+ dbo.fnFormatAddCarriageReturn(CASE WHEN ISNULL(CONVERT(INT,MRC.PeopleMatch), 0) > 0 THEN 'AND COUNT(DISTINCT PersonID) >= ' + CAST(MRC.PeopleMatch AS VARCHAR(5)) ELSE '' END)
			+ dbo.fnFormatAddCarriageReturn( CASE WHEN ISNULL(CONVERT(INT,MRC.VehicleMatch), 0) > 0 THEN 'AND COUNT(DISTINCT VehicleID) >= '  + CAST(MRC.VehicleMatch AS VARCHAR(5)) ELSE '' END)
			FROM dbo.MatchingRulesClaim MRC WITH(NOLOCK)   
			WHERE RuleNo = @Id
END
		
RETURN @SQL

END
