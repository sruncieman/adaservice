﻿
CREATE FUNCTION [dbo].[fnGetXmlMediumDataAliasInfo]
(
	@RiskClaim_Id INT,
	@Person_Id INT
)
RETURNS  VARCHAR(500)
AS
BEGIN
	--DataMed1	DataMed2	DataMed3	DataMed4	DataMed5	DataMed6
--2002-09-05T00:00:00	Jebairia	Nettleford	83423	Miss	

	DECLARE @Val VARCHAR(500) = ''
	DECLARE @Sep CHAR(2) = '; '
	DECLARE @Len INT = 0
	
	SELECT 	@Val += CASE WHEN MD.DataMed2 IS NULL THEN '' ELSE MD.DataMed2 + ' ' END +
			CASE WHEN MD.DataMed3 IS NULL THEN '' ELSE UPPER(MD.DataMed3)  END +
			CASE WHEN MD.DataMed1 IS NULL THEN '' ELSE ' DOB ' +  CONVERT(VARCHAR, CONVERT(DATE, MD.DataMed1, 103), 103) END + @Sep --+ CASE WHEN MD.DataMed6 = 'True' THEN ' [C]' ELSE ' [U]' END 
	FROM dbo.rpt_Level_One_Report_DataMed AS MD
	WHERE md.RiskClaim_Id = @RiskClaim_Id
    AND MD.Per_DbId = @Person_Id
	AND MD.LocalName = 'PersonRisk' 
	AND MD.Rule_Key IN ('Person_AliasInformation_Result')
	ORDER BY MD.Rule_Key 	

	IF LEN(@Val) > 0 SET @Len = 1
	
	IF @Val IS NOT NULL
		SET @Val = NULLIF(LEFT(@Val, LEN(@Val) - @Len), '')


	RETURN @Val

END

