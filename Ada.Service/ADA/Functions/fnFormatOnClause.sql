﻿
CREATE FUNCTION [dbo].[fnFormatOnClause] 
(
	@OnClause  NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @Result NVARCHAR(MAX)
	
	IF (LEN(@OnClause) > 3)
		SET @Result = ' ON ' + RIGHT(@OnClause, LEN(@OnClause) - 3)
	ELSE
		SET @Result = ''
	
	
	RETURN @Result

END


