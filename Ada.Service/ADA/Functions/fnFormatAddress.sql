﻿CREATE FUNCTION [dbo].[fnFormatAddress]
(	
	@SubBuilding VARCHAR(50),
	@Building VARCHAR(50),
	@BuildingNumber VARCHAR(4),
	@Street VARCHAR(50),
	@Locality VARCHAR(50),
	@Town VARCHAR(50),
	@County VARCHAR(50),
	@Postcode VARCHAR(8)
)
RETURNS VARCHAR(312)
AS
BEGIN
	DECLARE @Result VARCHAR(312)

	SET @Result = 
				LTRIM(RTRIM(
				CASE WHEN ISNULL(@SubBuilding, '') = '' THEN '' ELSE @SubBuilding END +
				CASE WHEN ISNULL(@Building, '') = '' THEN '' ELSE  ' ' + @Building END +
				CASE WHEN ISNULL(@BuildingNumber, '') = '' THEN '' ELSE  ' ' + @BuildingNumber END +
				CASE WHEN ISNULL(@Street, '') = '' THEN '' ELSE  ' ' + @Street END +
				CASE WHEN ISNULL(@Locality, '') = '' THEN '' ELSE  ', ' +  @Locality  END +
				CASE WHEN ISNULL(@Town, '') = '' THEN '' ELSE  ', ' +   @Town END +
				CASE WHEN ISNULL(@County, '') = '' THEN '' ELSE  ', ' +   @County END +
				CASE WHEN ISNULL(@Postcode, '') = '' THEN '' ELSE  ' ' +   @Postcode END))

	
	
	
	
	
	
	
	



	


	RETURN @Result
END
