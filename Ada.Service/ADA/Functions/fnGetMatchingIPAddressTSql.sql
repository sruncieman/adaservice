﻿


CREATE FUNCTION [dbo].[fnGetMatchingIPAddressTSql] 
(
	@Id INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @SQL NVARCHAR(MAX)
DECLARE @RiskClaim_Id BIT = (SELECT RiskClaim_Id FROM dbo.MatchingRulesIPAddress WHERE RuleNo = @Id)

IF (@RiskClaim_Id IS NULL)
	SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  dbo.IPAddress AS A 
	WHERE (IPAddress = @IPAddress)'
FROM dbo.MatchingRulesIPAddress WHERE RuleNo = @Id
ELSE
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	  
	FROM IPAddress AS A 
	INNER JOIN Person2IPAddress AS P2IP ON A.Id = P2IP.IPAddress_Id AND P2IP.AdaRecordStatus = 0
	WHERE  ' 
	+
	dbo.fnFormatWhereClause(
	CASE WHEN IPAddress = 1 THEN ' AND IPAddress = @IPAddress' ELSE '' END + 
	CASE WHEN RiskClaim_Id = 1 THEN ' AND BaseRiskClaim_Id = @RiskClaim_Id' ELSE '' END )

FROM dbo.MatchingRulesIPAddress  WHERE RuleNo = @Id

RETURN @SQL

END
