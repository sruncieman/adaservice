﻿

CREATE FUNCTION [dbo].[fn_GetOrganisationTree2]
(
	 @Id INT,
	 @LinkConfidence INT
)
RETURNS 
@T TABLE 
(
	Id INT
)
AS
BEGIN
	
	--DECLARE @LinkConfidence INT = 0

	DECLARE @ObjectLevel INT = 1

	DECLARE @Tree TABLE(ObjectLevel INT, ObjectId INT, LinkConfidence INT)

	INSERT INTO @Tree
	SELECT @ObjectLevel, Organisation1_Id, LinkConfidence FROM dbo.Organisation2Organisation WHERE Organisation1_Id = @Id AND LinkConfidence <= @LinkConfidence AND OrganisationLinkType_Id in (5,6,7)
	UNION ALL
	SELECT @ObjectLevel, Organisation2_Id, LinkConfidence FROM dbo.Organisation2Organisation WHERE Organisation2_Id = @Id AND LinkConfidence <= @LinkConfidence AND OrganisationLinkType_Id in (5,6,7)
	OPTION(RECOMPILE)

	WHILE(@@Rowcount > 0)
	BEGIN
		SET @ObjectLevel += 1
		INSERT INTO @Tree
		SELECT @ObjectLevel ObjectLevel, Organisation1_Id, LinkConfidence FROM dbo.Organisation2Organisation 
		WHERE Organisation2_Id IN(SELECT ObjectId FROM @Tree WHERE ObjectLevel = @ObjectLevel -1) AND Organisation1_Id NOT IN(SELECT ObjectId FROM @Tree) AND LinkConfidence <= @LinkConfidence AND OrganisationLinkType_Id  in (5,6,7)
		UNION ALL
		SELECT @ObjectLevel ObjectLevel, Organisation2_Id, LinkConfidence FROM dbo.Organisation2Organisation 
		WHERE Organisation1_Id IN(SELECT ObjectId FROM @Tree WHERE ObjectLevel = @ObjectLevel -1) AND Organisation2_Id NOT IN(SELECT ObjectId FROM @Tree) AND LinkConfidence <= @LinkConfidence AND OrganisationLinkType_Id  in (5,6,7)
		OPTION(RECOMPILE)
	END
	
	INSERT INTO @T(Id)
	SELECT DISTINCT ObjectId FROM @Tree WHERE ISNULL(LinkConfidence, @LinkConfidence) = @LinkConfidence 

	RETURN 
END
