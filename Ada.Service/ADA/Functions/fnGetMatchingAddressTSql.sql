﻿

CREATE FUNCTION [dbo].[fnGetMatchingAddressTSql] 
(
	@Id INT,
	@UseRiskClaimId BIT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN



DECLARE @SQL NVARCHAR(MAX)

DECLARE @OrgJoin NVARCHAR(1000) = '
INNER JOIN dbo.Organisation2Address O2A WITH(NOLOCK) ON O2A.Address_Id = A.Id
INNER JOIN dbo.Organisation ORG WITH(NOLOCK) ON ORG.Id = O2A.Organisation_Id'

DECLARE @OrgWhere NVARCHAR(1000) = '
AND ORG.OrganisationNameCalcuated = @OrganisationName
AND O2A.ADARecordStatus = 0
AND ORG.ADARecordStatus = 0
'

IF (@UseRiskClaimId = 0)
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  Address AS A  WITH(NOLOCK) '
	+ CASE WHEN OrganisationName = 1 THEN @OrgJoin ELSE '' END +'
	WHERE '
		+ dbo.fnFormatWhereClause(
		CASE WHEN SubBuilding = 1 THEN ' AND SubBuilding = @SubBuilding' ELSE '' END 
		+ CASE WHEN Building = 1 THEN ' AND Building = @Building' ELSE '' END 
		+ CASE WHEN BuildingNumber = 1 THEN ' AND BuildingNumber = @BuildingNumber' ELSE '' END 
		+ CASE WHEN Street = 1 THEN ' AND Street = @Street' ELSE '' END 
		+ CASE WHEN Locality = 1 THEN ' AND Locality = @Locality' ELSE '' END 
		+ CASE WHEN Town = 1 THEN ' AND Town = @Town' ELSE '' END 
		+ CASE WHEN PostCode = 1 THEN ' AND PostCode = REPLACE(@PostCode, '' '', '''')' ELSE '' END 
		+ CASE WHEN OrganisationName = 1 THEN @OrgWhere ELSE '' END 
		+ ' AND A.ADARecordStatus = 0 '
		+ ' GROUP BY A.Id' 
		+ ' OPTION (RECOMPILE)' 
	)
FROM dbo.MatchingRulesAddress WHERE RuleNo = @Id
ELSE
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  Address AS A  WITH(NOLOCK) '
	+ CASE WHEN OrganisationName = 1 THEN @OrgJoin ELSE '' END +'
	INNER JOIN
	(
	SELECT BaseRiskClaim_Id, Address_Id  FROM Person2Address WHERE BaseRiskClaim_Id = @RiskClaim_Id AND [LinkConfidence] = 0
	UNION
	SELECT BaseRiskClaim_Id, Address_Id  FROM dbo.Organisation2Address WHERE BaseRiskClaim_Id = @RiskClaim_Id AND [LinkConfidence] = 0
	) AS U
	 ON U.Address_Id = A.Id
	
	WHERE  '
	+ dbo.fnFormatWhereClause(
		CASE WHEN SubBuilding = 1 THEN ' AND SubBuilding = @SubBuilding' ELSE '' END 
		+ CASE WHEN Building = 1 THEN ' AND Building = @Building' ELSE '' END 
		+ CASE WHEN BuildingNumber = 1 THEN ' AND BuildingNumber = @BuildingNumber' ELSE '' END 
		+ CASE WHEN Street = 1 THEN ' AND Street = @Street' ELSE '' END 
		+ CASE WHEN Locality = 1 THEN ' AND Locality = @Locality' ELSE '' END 
		+ CASE WHEN Town = 1 THEN ' AND Town = @Town' ELSE '' END 
		+ CASE WHEN PostCode = 1 THEN ' AND PostCode = REPLACE(@PostCode, '' '', '''')' ELSE '' END 
		+ CASE WHEN OrganisationName = 1 THEN @OrgWhere ELSE '' END 
		+ ' AND A.ADARecordStatus = 0 '	
		+ ' GROUP BY A.Id' 	 
		+ ' OPTION (RECOMPILE)' 
	)

FROM dbo.MatchingRulesAddress WHERE RuleNo = @Id

RETURN @SQL

END
