﻿CREATE FUNCTION [dbo].[fn_GetSummaryMessagesInpBatchID] (@BatchID INT =0)
RETURNS @XMLOut TABLE (RiskClaimRunID INT, RPTHeader VARCHAR(1000), RPTMessage VARCHAR(1000))
AS
BEGIN

	DECLARE  @RowCount				INT
			,@SummaryMessagesXml	XML
			,@RiskClaimRunID		INT

	DECLARE @T TABLE (TID INT IDENTITY(1,1), RiskClaimRunID INT)

	INSERT INTO @T (RiskClaimRunID)
	SELECT   RCR.Id RiskClaimRunID 
	FROM dbo.RiskClaimRun RCR
	INNER JOIN dbo.RiskClaim RC ON RC.Id = RCR.RiskClaim_Id
	INNER JOIN dbo.RiskBatch RB ON RB.Id = RC.RiskBatch_Id
	WHERE RB.Id = @BatchID

	SELECT @RowCount = @@ROWCOUNT

	WHILE @RowCount > 0
	BEGIN

		SELECT @RiskClaimRunID = RiskClaimRunID FROM @T WHERE TID = @RowCount
		SELECT @SummaryMessagesXml = SummaryMessagesXml FROM dbo.RiskClaimRun WHERE Id = @RiskClaimRunID
		
		;WITH XMLNAMESPACES (DEFAULT 'http://keo/MsgsV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
		INSERT INTO @XMLOut (RiskClaimRunID, RPTHeader, RPTMessage)
		SELECT   @RiskClaimRunID RiskClaimRunID
				,NULLIF(CONVERT(varchar(1000),TBL.COL.query('data(../../H)')),'') AS RPTHeader
				,NULLIF(CONVERT(varchar(1000),TBL.COL.query('data(.)')),'') AS RPTMessage
		FROM @SummaryMessagesXml.nodes('//ArrayOfML/ML/M/I') AS TBL(COL)
		
		SELECT @RowCount -= 1

	END

	RETURN

END

