﻿


CREATE FUNCTION  [dbo].[fnGetXmlMediumData]
(
	 @RiskClaim_Id INT = 2
)
RETURNS 
@Table TABLE 
(
	RuleSetKey varchar(MAX),
	Key_ varchar(MAX),
	DataMed1 varchar(MAX),
	DataMed2 varchar(MAX),
	DataMed3 varchar(MAX),
	DataMed4 varchar(MAX),
	DataMed5 varchar(MAX),
	DataMed6 varchar(MAX),
	DataMed7 varchar(MAX),
	DataMed8 varchar(MAX),
	DataMed9 varchar(MAX),
	DataMed10 varchar(MAX),
	DataMed11 varchar(MAX),
	DataMed12 varchar(MAX),
	DataMed13 varchar(MAX),
	DataMed14 varchar(MAX),
	DataMed15 varchar(MAX),
	DataMed16 varchar(MAX),
	DataMed17 varchar(MAX),
	DataMed18 varchar(MAX),
	DataMed19 varchar(MAX),
	DataMed20 varchar(MAX),
	
	
	
	Inc_DbId VARCHAR(10),
	Pol_DbId VARCHAR(10),
	Veh_Id VARCHAR(10),
	Per_Id VARCHAR(10),
	Add_Id VARCHAR(10),
	localName  VARCHAR(50)
	
	,Org_DbId  VARCHAR(10)

	
)
AS
BEGIN

 --DECLARE @RiskClaim_Id INT = 99
	
	DECLARE @XML XML = (SELECT ScoreEntityXml FROM dbo.RiskClaimRun WHERE RiskClaim_Id =  @RiskClaim_Id AND ScoreEntityEncodeFormat = 'xml') 

	;WITH XMLNAMESPACES (DEFAULT 'http://keo/ScoresV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)		INSERT INTO @Table		SELECT 
			CONVERT(varchar(MAX), TBL.COL.query('data(../../../../../Key)')) AS RuleSetKey,
			CONVERT(varchar(MAX), TBL.COL.query('data(../../../Key)')) AS Key_,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[1]')), '') AS DataMed1,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[2]')), '') AS DataMed2,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[3]')), '') AS DataMed3,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[4]')), '') AS DataMed4,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[5]')), '') AS DataMed5,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[6]')), '') AS DataMed6,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[7]')), '') AS DataMed7,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[8]')), '') AS DataMed8,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[9]')), '') AS DataMed9,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[10]')), '') AS DataMed10,
			
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[11]')), '') AS DataMed11,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[12]')), '') AS DataMed12,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[13]')), '') AS DataMed13,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[14]')), '') AS DataMed14,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[15]')), '') AS DataMed15,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[16]')), '') AS DataMed16,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[17]')), '') AS DataMed17,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[18]')), '') AS DataMed18,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[19]')), '') AS DataMed19,
			NULLIF(CONVERT(varchar(MAX), TBL.COL.query('data(C)[20]')), '') AS DataMed20,
			
			
			CONVERT(VARCHAR(10), TBL.COL.query('data(//ClaimScoreResults/Incident/Inc_DbId[1])')) AS Inc_DbId,
			CONVERT(VARCHAR(10), TBL.COL.query('data(//ClaimScoreResults/Incident/Policy/Pol_DbId[1])')) AS Pol_DbId,
			
			
			
			CASE TBL.COL.value('fn:local-name(../../../../../.[1])', 'nvarchar(50)') 
				WHEN 'VehicleRisk' THEN CONVERT(VARCHAR(10), TBL.COL.query('data(../../../../../../../../Veh_DbId)'))	
				WHEN 'PersonRisk' THEN CONVERT(VARCHAR(10), TBL.COL.query('data(../../../../../../../../../../Veh_DbId)'))	
			END AS Veh_Id,	
			
			
			
			CASE TBL.COL.value('fn:local-name(../../../../../../../../.[1])', 'nvarchar(50)') 
				WHEN 'PersonRisk' THEN CONVERT(VARCHAR(10), TBL.COL.query('data(../../../../../../../../Per_DbId)'))	
				WHEN 'AddressRisk' THEN CONVERT(VARCHAR(10), TBL.COL.query('data(../../../../../../../../../../Per_DbId)'))
			END AS Per_Id,
			
			
			CASE TBL.COL.value('fn:local-name(../../../../../../../../.[1])', 'nvarchar(50)') 
				WHEN 'AddressRisk' THEN CONVERT(VARCHAR(10), TBL.COL.query('data(../../../../../Add_DbId)'))	
			END AS Add_Id,
			
			
			TBL.COL.value('fn:local-name(../../../../../../../../.[1])', 'nvarchar(50)') AS localName,
			
			
			CONVERT(VARCHAR(10), TBL.COL.query('data(../../../../../../Org_DbId)')) AS Org_DbId
			

			
		FROM @Xml.nodes('//RGResults/Value/DM/R') AS TBL(COL)	
		WHERE 
			LTRIM(RTRIM(
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[1]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[2]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[3]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[4]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[5]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[6]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[7]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[8]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[9]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[10]')) +
			
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[11]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[12]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[13]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[14]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[15]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[16]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[17]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[18]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[19]')) +
			CONVERT(varchar(MAX), TBL.COL.query('data(C)[20]'))
			
			
			
		)) != ''
	RETURN 
END


