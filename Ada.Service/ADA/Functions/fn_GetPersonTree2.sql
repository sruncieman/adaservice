﻿

CREATE FUNCTION [dbo].[fn_GetPersonTree2]
(
	 @Id INT,
	 @LinkConfidence INT

)
RETURNS 
@T TABLE 
(
	Id INT
)
AS
BEGIN
	
	--DECLARE @LinkConfidence INT = 0

	DECLARE @ObjectLevel INT = 1

	DECLARE @Tree TABLE(ObjectLevel INT, ObjectId INT, LinkConfidence INT)

	INSERT INTO @Tree
	SELECT @ObjectLevel, Person1_Id, LinkConfidence FROM dbo.Person2Person WHERE Person1_Id = @Id AND LinkConfidence <= @LinkConfidence AND Person2PersonLinkType_Id = 1
	UNION ALL
	SELECT @ObjectLevel, Person2_Id, LinkConfidence FROM dbo.Person2Person WHERE Person2_Id = @Id AND LinkConfidence <= @LinkConfidence AND Person2PersonLinkType_Id = 1

	WHILE(@@Rowcount > 0)
	BEGIN
		SET @ObjectLevel += 1
		INSERT INTO @Tree
			SELECT @ObjectLevel ObjectLevel, Person1_Id, LinkConfidence FROM dbo.Person2Person 
			WHERE Person2_Id IN(SELECT ObjectId FROM @Tree WHERE ObjectLevel = @ObjectLevel -1) AND Person1_Id NOT IN(SELECT ObjectId FROM @Tree) AND LinkConfidence <= @LinkConfidence AND Person2PersonLinkType_Id = 1
			UNION ALL
			SELECT @ObjectLevel ObjectLevel, Person2_Id, LinkConfidence FROM dbo.Person2Person 
			WHERE Person1_Id IN(SELECT ObjectId FROM @Tree WHERE ObjectLevel = @ObjectLevel -1) AND Person2_Id NOT IN(SELECT ObjectId FROM @Tree) AND LinkConfidence <= @LinkConfidence AND Person2PersonLinkType_Id = 1
	END
	
	INSERT INTO @T(Id)
	SELECT DISTINCT ObjectId FROM @Tree WHERE ISNULL(LinkConfidence, @LinkConfidence) = @LinkConfidence 

	RETURN 
END
