﻿

CREATE FUNCTION [dbo].[fn_GetPersonTree]
(
	 @Id INT,
	 @includeConfirmed BIT,
	 @includeUnconfirmed BIT,
	 @includeTentative BIT,
	 @includeThis BIT
)
RETURNS 
@T TABLE 
(
	Id INT
)
AS
BEGIN
	
	DECLARE @LinkConfidence INT

	IF @includeTentative = 1  
		SET	@LinkConfidence = 2
	ELSE IF @includeUnconfirmed = 1  
		SET	@LinkConfidence = 1
	ELSE IF @includeConfirmed = 1  
		SET	@LinkConfidence = 0
		

	DECLARE @ObjectLevel INT = 1

	DECLARE @Tree TABLE(ObjectLevel INT, ObjectId INT, LinkConfidence INT)

	INSERT INTO @Tree
	SELECT @ObjectLevel, Person1_Id, LinkConfidence FROM dbo.Person2Person WHERE Person1_Id = @Id AND LinkConfidence <= @LinkConfidence AND Person2PersonLinkType_Id = 1
	UNION ALL
	SELECT @ObjectLevel, Person2_Id, LinkConfidence FROM dbo.Person2Person WHERE Person2_Id = @Id AND LinkConfidence <= @LinkConfidence AND Person2PersonLinkType_Id = 1


	WHILE(@@Rowcount > 0)
	BEGIN
		SET @ObjectLevel += 1
		INSERT INTO @Tree
			SELECT @ObjectLevel ObjectLevel, Person1_Id, LinkConfidence FROM dbo.Person2Person 
			WHERE Person2_Id IN(SELECT ObjectId FROM @Tree WHERE ObjectLevel = @ObjectLevel -1) AND Person1_Id NOT IN(SELECT ObjectId FROM @Tree) AND LinkConfidence <= @LinkConfidence AND Person2PersonLinkType_Id = 1
			UNION ALL
			SELECT @ObjectLevel ObjectLevel, Person2_Id, LinkConfidence FROM dbo.Person2Person 
			WHERE Person1_Id IN(SELECT ObjectId FROM @Tree WHERE ObjectLevel = @ObjectLevel -1) AND Person2_Id NOT IN(SELECT ObjectId FROM @Tree) AND LinkConfidence <= @LinkConfidence AND Person2PersonLinkType_Id = 1
	END
	
	IF @includeThis = 1
		INSERT INTO @T(Id)
		SELECT @Id
	
	INSERT INTO @T(Id)
	SELECT DISTINCT ObjectId FROM @Tree WHERE ISNULL(LinkConfidence, @LinkConfidence) = @LinkConfidence AND ObjectId != @Id
	
	 
	RETURN 
END



