﻿
CREATE FUNCTION [dbo].[fnGetMatchingVehicleTSql] 
(
	 @Id INT
	,@UseRiskClaimId BIT
	,@RiskClaim_Id VARCHAR(MAX)
	,@VehicleMake VARCHAR(MAX)
	,@Model VARCHAR(MAX)
	,@VehicleRegistration VARCHAR(MAX)
	,@Colour VARCHAR(MAX)
	,@VIN VARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @SQL NVARCHAR(MAX)

DECLARE @From NVARCHAR(1000) ='
FROM  Vehicle AS VEH WITH(NOLOCK)'

DECLARE @JoinRiskClaim NVARCHAR(1000) ='
LEFT JOIN Incident2Vehicle AS I2V WITH (NOLOCK) ON VEH.Id = I2V.Vehicle_Id AND I2V.AdaRecordStatus = 0
LEFT JOIN dbo.Vehicle2Organisation AS V2O WITH (NOLOCK) ON VEH.Id = V2O.Vehicle_Id AND V2O.AdaRecordStatus = 0'

DECLARE @JoinAddress NVARCHAR(1000) ='
INNER JOIN Vehicle2Person AS V2P WITH (NOLOCK) ON VEH.Id = V2P.Vehicle_Id AND V2P.AdaRecordStatus = 0
INNER JOIN Person2Address AS P2A WITH (NOLOCK) ON V2P.Person_Id = P2A.Person_Id AND P2A.AdaRecordStatus = 0
INNER JOIN Address AS AD WITH (NOLOCK) ON P2A.Address_Id = AD.Id AND AD.AdaRecordStatus = 0
INNER JOIN #VehicleAddressDetails VAD '
	
SELECT @SQL =
	--SELECT
	'SELECT VEH.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), CASE WHEN @UseRiskClaimId = 1 THEN 0 ELSE MatchType END) + ' AS MatchType'
	--FROM
	+ @From 
	+ CASE WHEN @UseRiskClaimId = 1 THEN @JoinRiskClaim ELSE '' END 
	+ CASE WHEN ISNULL(CONVERT(INT, BuildingNumber), 0) + ISNULL(CONVERT(INT, PostCode), 0) + ISNULL(CONVERT(INT, PafUPRN), 0) > 0 THEN  @JoinAddress + dbo.fnFormatOnClause(CASE WHEN BuildingNumber = 1 THEN 'AND VAD.BuildingNumber = AD.BuildingNumber ' ELSE '' END + CASE WHEN PostCode = 1 THEN 'AND VAD.PostCode = AD.PostCode ' ELSE '' END + CASE WHEN PafUPRN = 1 THEN 'AND NULLIF(VAD.PafUPRN,'''') = NULLIF(AD.PafUPRN,'''') ' ELSE '' END) ELSE '' END 
	--WHERE
	+ dbo.fnFormatAddCarriageReturn('WHERE VEH.ADARecordStatus = 0 ') + CHAR(13)
	+ CASE WHEN @UseRiskClaimId = 1 THEN 'AND ISNULL(I2V.BaseRiskClaim_Id, V2O.BaseRiskClaim_Id) = ''' + @RiskClaim_ID  + ''' ' + CHAR(13) ELSE '' END
	+ CASE WHEN VehicleRegistration = 1 THEN 'AND VEH.VehicleRegistration = ''' + @VehicleRegistration + ''' ' + CHAR(13) ELSE '' END
	+ CASE WHEN VIN = 1 THEN 'AND VEH.VIN = ''' + @VIN + ''' ' + CHAR(13) ELSE '' END
	+ CASE WHEN VehicleMake = 1 THEN 'AND VEH.VehicleMake = ''' + @VehicleMake + ''' ' + CHAR(13) ELSE '' END
	+ CASE WHEN Model = 1 THEN 'AND VEH.Model = ''' + @Model + ''' ' + CHAR(13) ELSE '' END
	+ CASE WHEN [Colour] = 1 THEN 'AND VEH.[VehicleColour_Id] = ''' + @Colour + ''' ' + CHAR(13) ELSE '' END
	--GROUP BY
	+ dbo.fnFormatAddCarriageReturn('GROUP BY VEH.Id')
	FROM  MatchingRulesVehicles MRV WITH(NOLOCK)   
	WHERE RuleNo = @Id
	
RETURN @SQL
END
