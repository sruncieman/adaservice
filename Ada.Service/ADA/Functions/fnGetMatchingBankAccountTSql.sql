﻿
CREATE FUNCTION [dbo].[fnGetMatchingBankAccountTSql] 
(
	@Id INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN


DECLARE @SQL NVARCHAR(MAX)
DECLARE @RiskClaim_Id BIT = (SELECT RiskClaim_Id FROM dbo.MatchingRulesAccount WHERE RuleNo = @Id)

IF (@RiskClaim_Id IS NULL)
	SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  BankAccount AS A WITH(NOLOCK)
	WHERE '
		+ dbo.fnFormatWhereClause(
		CASE WHEN AccountNumber = 1 THEN ' AND AccountNumber = @AccountNumber' ELSE '' END 
		+ CASE WHEN SortCode = 1 THEN ' AND SortCode = @SortCode' ELSE '' END 
		+ CASE WHEN BankName = 1 THEN ' AND BankName = @BankName' ELSE '' END 
		)
	+ ' AND (A.ADARecordStatus = 0) '
	+ ' GROUP BY A.Id'
FROM dbo.MatchingRulesAccount WHERE RuleNo = @Id
ELSE
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  BankAccount AS A WITH(NOLOCK)
	LEFT OUTER JOIN Policy2BankAccount AS POL2B  WITH(NOLOCK) ON A.Id = POL2B.BankAccount_Id AND POL2B.ADARecordStatus = 0
	LEFT OUTER JOIN Organisation2BankAccount AS O2B  WITH(NOLOCK) ON A.Id = O2B.BankAccount_Id AND O2B.ADARecordStatus = 0
	LEFT OUTER JOIN Person2BankAccount AS PER2B  WITH(NOLOCK) ON A.Id = PER2B.BankAccount_Id AND PER2B.ADARecordStatus = 0
	
	WHERE '
		+ dbo.fnFormatWhereClause(
		CASE WHEN AccountNumber = 1 THEN ' AND AccountNumber = @AccountNumber' ELSE '' END 
		+ CASE WHEN SortCode = 1 THEN ' AND SortCode = @SortCode' ELSE '' END 
		+ CASE WHEN BankName = 1 THEN ' AND BankName = @BankName' ELSE '' END 
		+ ' AND COALESCE(POL2B.BaseRiskClaim_Id,  O2B.BaseRiskClaim_Id, PER2B.BaseRiskClaim_Id) = @RiskClaim_Id'
		)
	+ ' AND (A.ADARecordStatus = 0) '
	+ ' GROUP BY A.Id'
	
FROM dbo.MatchingRulesAccount WHERE RuleNo = @Id

RETURN @SQL

END
