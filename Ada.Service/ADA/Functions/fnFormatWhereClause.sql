﻿
CREATE FUNCTION [dbo].[fnFormatWhereClause] 
(
	@OnClause  NVARCHAR(1000)
)
RETURNS NVARCHAR(1000)
AS
BEGIN
	DECLARE @Result NVARCHAR(1000)
	
	IF (LEN(@OnClause) > 4)
		SET @Result =  RIGHT(@OnClause, LEN(@OnClause) - 4)
	ELSE
		SET @Result = ''
	
	
	RETURN @Result

END


