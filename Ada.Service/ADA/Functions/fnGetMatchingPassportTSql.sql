﻿


CREATE FUNCTION [dbo].[fnGetMatchingPassportTSql] 
(
	@Id INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @SQL NVARCHAR(MAX)
DECLARE @RiskClaim_Id BIT = (SELECT RiskClaim_Id FROM dbo.MatchingRulesPassport WHERE RuleNo = @Id)

IF (@RiskClaim_Id IS NULL)
	SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  dbo.Passport AS A 
	WHERE (PassportNumber = @PassportNumber)'
FROM dbo.MatchingRulesPassport WHERE RuleNo = @Id
ELSE
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	  
	FROM  Passport AS A 
	INNER JOIN Person2Passport AS P2P ON A.Id = P2P.Passport_Id AND P2P.AdaRecordStatus = 0
	WHERE  ' 
	+
	dbo.fnFormatWhereClause(
	CASE WHEN PassportNumber = 1 THEN ' AND PassportNumber = @PassportNumber' ELSE '' END + 
	CASE WHEN RiskClaim_Id = 1 THEN ' AND BaseRiskClaim_Id = @RiskClaim_Id' ELSE '' END )

FROM dbo.MatchingRulesPassport  WHERE RuleNo = @Id

RETURN @SQL

END
