﻿


CREATE FUNCTION [dbo].[fnGetMatchingSanctionsPersonToProcess]
(	
 @XML XML
)
RETURNS TABLE 
AS
RETURN 
(
SELECT 
	MRP.*
	FROM  MatchingRulesPerson MRP WITH(NOLOCK) 
	INNER JOIN(
	SELECT  
			MAX(LEN(XP.Data.value('@FirstName', 'VARCHAR(50)'))) FirstName,
			MAX(LEN(XP.Data.value('@LastName', 'VARCHAR(50)'))) LastName,
			MAX(LEN(XP.Data.value('@DateOfBirth', 'VARCHAR(50)'))) DateOfBirth,
			MAX(LEN(XA.Data.value('@SubBuilding', 'VARCHAR(50)'))) SubBuilding,
			MAX(LEN(XA.Data.value('@Building', 'VARCHAR(50)'))) Building,
			MAX(LEN(XA.Data.value('@BuildingNumber', 'VARCHAR(50)'))) BuildingNumber,
			MAX(LEN(XA.Data.value('@Street', 'VARCHAR(50)'))) Street,
			MAX(LEN(XA.Data.value('@Locality', 'VARCHAR(50)'))) Locality,
			MAX(LEN(XA.Data.value('@Town', 'VARCHAR(50)'))) Town,
			MAX(LEN(XA.Data.value('@PostCode', 'VARCHAR(50)'))) PostCode,
			MAX(LEN(XNI.Data.value('@NINumber', 'VARCHAR(50)'))) NINumber,
			MAX(LEN(XPAS.Data.value('@PassportNumber', 'VARCHAR(50)'))) PassportNumber
		FROM
			@XML.nodes('//Data/Person') XP(Data) 	
		INNER JOIN
			@XML.nodes('//Data/Person/Addresses/Address') XA(Data)
			ON 1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/NINumbers/NINumber') XNI(Data)
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/Passports/Passport') XPAS(Data)
			ON  1 = 1
		) _XML
	ON	ISNULL(MRP.FirstName,  0) <= _XML.FirstName
		AND ISNULL(MRP.LastName,  0) <= _XML.LastName
		AND ISNULL(MRP.DateOfBirth, 0) <= _XML.DateOfBirth
		AND ISNULL(MRP.SubBuilding, 0) <= _XML.SubBuilding
		AND ISNULL(MRP.Building, 0) <= _XML.Building
		AND ISNULL(MRP.BuildingNumber, 0) <= _XML.BuildingNumber
		AND ISNULL(MRP.Street, 0) <= _XML.Street
		AND ISNULL(MRP.Locality, 0) <= _XML.Locality
		AND ISNULL(MRP.Town, 0)	<= _XML.Town
		AND ISNULL(MRP.PostCode, 0)	<= _XML.PostCode
		AND MRP.AccountNumber IS NULL
		AND MRP.DriverNumber IS NULL
		AND ISNULL(MRP.NINumber, 0) <= _XML.NINumber
		AND ISNULL(MRP.PassportNumber, 0) <= _XML.PassportNumber
		AND MRP.EmailAddress IS NULL
		AND MRP.PaymentCardNumber IS NULL
		AND MRP.TelephoneNumber IS NULL
		AND MRP.VehicleRegistration IS NULL
		AND MRP.VehicleVIN IS NULL
		AND MRP.OrganisationName IS NULL
		AND MRP.RegisteredNumber IS NULL
		AND MRP.VATNumber IS NULL
		AND MRP.PolicyNumber IS NULL
		AND MRP.PafUPRN IS NULL
		WHERE MRP.MatchType = 0 --ConfirmedOnly
		AND RiskClaimFlag != 1
)