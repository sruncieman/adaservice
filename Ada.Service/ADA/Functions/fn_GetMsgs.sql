﻿



CREATE FUNCTION [dbo].[fn_GetMsgs]
(
      @RiskBatch_Id INT
)
RETURNS 
@T TABLE 
(
		RiskClaim_Id int,   
		RptHeader varchar(1000), 
		RptMsg varchar(1000)
)


AS
BEGIN

	
	  --DECLARE @Tmp TABLE( 
			--			  element_id int,
			--			  parent_id int, 
			--			  object_id int,
			--			  name nvarchar(2000),
			--			  stringvalue nvarchar(4000) NOT NULL,
			--			  valuetype nvarchar(100) NOT null )

	
      DECLARE @RiskClaim_Id INT
      DECLARE @XML NVARCHAR(MAX)

      DECLARE _cursor CURSOR FOR 
      SELECT   RiskClaim_Id, CONVERT(NVARCHAR(MAX), ScoreEntityXml) AS ScoreEntityXml
      FROM    RiskClaimRun AS A INNER JOIN
                  RiskClaim AS B ON A.RiskClaim_Id = B.Id
      WHERE   (B.RiskBatch_Id = @RiskBatch_Id) 

      OPEN _cursor

      FETCH NEXT FROM _cursor 
      INTO @RiskClaim_Id, @XML

      WHILE @@FETCH_STATUS = 0
      BEGIN
			--INSERT INTO @Tmp
			
			
			--SELECT * FROM dbo.fn_parseJSON(@XML)
			
			INSERT INTO @T
			SELECT  @RiskClaim_Id,   RptHeader, RptMsg
			FROM         dbo.Fn_GetMsg(@XML)
			
			--SELECT
			--	@RiskClaim_Id,
			--	A.name Name1,
			--	A.stringvalue Value1,
			--	B.name Name2 ,
			--	B.stringvalue Value2,
			--	C.name Name3,
			--	C.stringvalue Value3
			--FROM @Tmp A 
			--INNER JOIN  @Tmp B
			--ON A.parent_id = B.element_id
			--INNER JOIN  @Tmp C
			--ON B.parent_id = C.parent_id
			--WHERE A.NAME LIKE 'Message%' AND LEN(A.stringvalue) > 1 AND C.NAME = 'ReportHeading'
          
			--DELETE FROM @Tmp
		
            FETCH NEXT FROM _cursor 
            INTO @RiskClaim_Id, @XML
      END 
      CLOSE _cursor;
      DEALLOCATE _cursor;
      
      
      RETURN 
END
