﻿


CREATE FUNCTION [dbo].[fnRiskClaimXml]
(
	 @RiskClaim_Id INT
)
RETURNS XML
AS
BEGIN


	DECLARE @XML XML = (SELECT ScoreEntityXml FROM dbo.RiskClaimRun WHERE RiskClaim_Id =  @RiskClaim_Id AND ScoreEntityEncodeFormat = 'xml') 

	RETURN @XML

END


