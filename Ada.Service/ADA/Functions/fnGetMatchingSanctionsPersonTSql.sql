﻿

CREATE FUNCTION [dbo].[fnGetMatchingSanctionsPersonTSql] 
(
 @Id INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX)
	DECLARE @From NVARCHAR(1000) ='
	FROM InternationalSanctions.dbo.Individual PER'

	DECLARE @JoinPerson NVARCHAR(1000) ='
	INNER JOIN @XML.nodes(''//Person'') XPER(Data)'

	DECLARE @JoinNINumber NVARCHAR(1000) =  '
	INNER JOIN InternationalSanctions.dbo.Individual2NINumber AS P2NI WITH(NOLOCK) ON PER.Id = P2NI.IndividualId
	INNER JOIN @XML.nodes(''//Person/NINumbers/NINumber'') XNI(Data) ON P2NI.NINumber = XNI.Data.value(''@NINumber'', ''VARCHAR(50)'')'

	DECLARE @JoinPassport NVARCHAR(1000) =  '
	INNER JOIN InternationalSanctions.dbo.Individual2Passport AS P2Pas WITH(NOLOCK) ON PER.Id = P2Pas.IndividualId
	INNER JOIN @XML.nodes(''//Person/Passports/Passport'') XPAS(Data) ON P2Pas.Passport = XPAS.Data.value(''@PassportNumber'', ''VARCHAR(50)'')'

	DECLARE @JoinAddress NVARCHAR(1000) =  '
	INNER JOIN @XML.nodes(''//Person/Addresses/Address'') XADD(Data)'

	SELECT
		@SQL =
		'SELECT DISTINCT CASE PER.SanctionListId WHEN 1 THEN ''UKConsolidatedListOfTargets:'' WHEN 2 THEN ''USSpeciallyDesignatedNationalsList:'' END + CAST(PER.UniqueID AS VARCHAR(10)), '
		+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
		+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
		+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType '
		+ @From 
		+ CASE WHEN ISNULL(CONVERT(INT, FirstName), 0) + ISNULL(CONVERT(INT, LastName), 0) +  ISNULL(CONVERT(INT, DateOfBirth), 0)  > 0 THEN  
			@JoinPerson 
			+ dbo.fnFormatOnClause(CASE WHEN FirstName = 1 THEN 'AND PER.FirstNames = XPER.Data.value(''@FirstName'', ''VARCHAR(50)'') ' ELSE '' END
			+	CASE WHEN LastName = 1 THEN 'AND PER.LastName = XPER.Data.value(''@LastName'', ''VARCHAR(50)'') ' ELSE '' END
			+	CASE WHEN  DateOfBirth  = 1 THEN 'AND CONVERT(DATE, PER.DateOfBirth, 103)  =  CONVERT(DATE, XPER.Data.value(''@DateOfBirth'', ''VARCHAR(50)''), 103) ' ELSE '' END)
		ELSE '' END  
		+ CASE WHEN NINumber = 1 THEN @JoinNINumber ELSE '' END 
		+ CASE WHEN PassportNumber  = 1 THEN @JoinPassport ELSE '' END 
  		+ CASE WHEN ISNULL(CONVERT(INT, SubBuilding), 0) + ISNULL(CONVERT(INT, Building), 0) +  ISNULL(CONVERT(INT, BuildingNumber), 0) +  ISNULL(CONVERT(INT, Street), 0) +  ISNULL(CONVERT(INT, Locality), 0) +  ISNULL(CONVERT(INT, Town), 0) + ISNULL(CONVERT(INT, PostCode), 0) > 0 
			THEN @JoinAddress
			+ dbo.fnFormatOnClause(
				  CASE WHEN PostCode = 1 THEN 'AND (PER.AddressLine1 LIKE ''%'' + XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine2 LIKE ''%'' + XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine3 LIKE ''%'' + XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine4 LIKE ''%'' + XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine5 LIKE ''%'' + XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN Building = 1 THEN 'AND (PER.AddressLine1 LIKE ''%'' + XADD.Data.value(''@Building'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine2 LIKE ''%'' + XADD.Data.value(''@Building'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine3 LIKE ''%'' + XADD.Data.value(''@Building'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine4 LIKE ''%'' + XADD.Data.value(''@Building'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine5 LIKE ''%'' + XADD.Data.value(''@Building'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN SubBuilding = 1 THEN 'AND (PER.AddressLine1 LIKE ''%'' + XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine2 LIKE ''%'' + XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine3 LIKE ''%'' + XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine4 LIKE ''%'' + XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine5 LIKE ''%'' + XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN Street = 1 THEN 'AND (PER.AddressLine1 LIKE ''%'' + XADD.Data.value(''@Street'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine2 LIKE ''%'' + XADD.Data.value(''@Street'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine3 LIKE ''%'' + XADD.Data.value(''@Street'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine4 LIKE ''%'' + XADD.Data.value(''@Street'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine5 LIKE ''%'' + XADD.Data.value(''@Street'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN BuildingNumber = 1 THEN 'AND (PER.AddressLine1 LIKE ''%'' + XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine2 LIKE ''%'' + XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine3 LIKE ''%'' + XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine4 LIKE ''%'' + XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine5 LIKE ''%'' + XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN Town = 1 THEN 'AND (PER.AddressLine1 LIKE ''%'' + XADD.Data.value(''@Town'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine2 LIKE ''%'' + XADD.Data.value(''@Town'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine3 LIKE ''%'' + XADD.Data.value(''@Town'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine4 LIKE ''%'' + XADD.Data.value(''@Town'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine5 LIKE ''%'' + XADD.Data.value(''@Town'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN Locality = 1 THEN 'AND (PER.AddressLine1 LIKE ''%'' + XADD.Data.value(''@Locality'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine2 LIKE ''%'' + XADD.Data.value(''@Locality'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine3 LIKE ''%'' + XADD.Data.value(''@Locality'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine4 LIKE ''%'' + XADD.Data.value(''@Locality'', ''VARCHAR(50)'') + ''%'' OR PER.AddressLine5 LIKE ''%'' + XADD.Data.value(''@Locality'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END)
			ELSE '' END
	FROM MatchingRulesPerson
	WHERE RuleNo = @Id

	RETURN @SQL

END