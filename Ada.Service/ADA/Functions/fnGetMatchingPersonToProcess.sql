﻿


CREATE FUNCTION [dbo].[fnGetMatchingPersonToProcess]
(	
	@XML XML
)
RETURNS TABLE 
AS
RETURN 
(
SELECT 
	MRP.*, HasRiskClaim_Id
	FROM  MatchingRulesPerson MRP WITH(NOLOCK) 
	INNER JOIN(
	SELECT  

			CONVERT(BIT, MAX(LEN(XP.Data.value('@RiskClaim_Id', 'VARCHAR(50)')))) HasRiskClaim_Id,
			MAX(LEN(XP.Data.value('@FirstName', 'VARCHAR(50)'))) FirstName,
			MAX(LEN(XP.Data.value('@LastName', 'VARCHAR(50)'))) LastName,
			MAX(LEN(XP.Data.value('@DateOfBirth', 'VARCHAR(50)'))) DateOfBirth,
			MAX(LEN(XA.Data.value('@SubBuilding', 'VARCHAR(50)'))) SubBuilding,
			MAX(LEN(XA.Data.value('@Building', 'VARCHAR(50)'))) Building,
			MAX(LEN(XA.Data.value('@BuildingNumber', 'VARCHAR(50)'))) BuildingNumber,
			MAX(LEN(XA.Data.value('@Street', 'VARCHAR(50)'))) Street,
			MAX(LEN(XA.Data.value('@Locality', 'VARCHAR(50)'))) Locality,
			MAX(LEN(XA.Data.value('@Town', 'VARCHAR(50)'))) Town,
			MAX(LEN(XA.Data.value('@PostCode', 'VARCHAR(50)'))) PostCode,
			MAX(LEN(XAN.Data.value('@AccountNumber', 'VARCHAR(50)'))) AccountNumber,
			MAX(LEN(XDLA.Data.value('@DriverNumber', 'VARCHAR(50)'))) DriverNumber,
			MAX(LEN(XNI.Data.value('@NINumber', 'VARCHAR(50)'))) NINumber,
			MAX(LEN(XPAS.Data.value('@PassportNumber', 'VARCHAR(50)'))) PassportNumber,
			MAX(LEN(XEMI.Data.value('@EmailAddress', 'VARCHAR(50)'))) EmailAddress,
			MAX(LEN(XPC.Data.value('@PaymentCardNumber', 'VARCHAR(50)'))) PaymentCardNumber,
			MAX(LEN(XTN.Data.value('@TelephoneNumber', 'VARCHAR(50)'))) TelephoneNumber,
			MAX(LEN(XV.Data.value('@VehicleRegistration', 'VARCHAR(50)'))) VehicleRegistration,
			MAX(LEN(XV.Data.value('@VehicleVIN', 'VARCHAR(50)'))) VehicleVIN,
			MAX(LEN(XORG.Data.value('@OrganisationName', 'VARCHAR(50)'))) OrganisationName,
			MAX(LEN(XORG.Data.value('@RegisteredNumber', 'VARCHAR(50)'))) RegisteredNumber,
			MAX(LEN(XORG.Data.value('@VATNumber', 'VARCHAR(50)')) ) VATNumber,
			MAX(LEN(XPOL.Data.value('@PolicyNumber', 'VARCHAR(50)'))) PolicyNumber,
			MAX(LEN(XA.Data.value('@PafUPRN', 'VARCHAR(50)'))) PafUPRN,
			MAX(LEN(XHAND.Data.value('@HandsetImei', 'VARCHAR(50)'))) HandsetIMEI
		FROM
			@XML.nodes('//Data/Person') XP(Data) 	
		INNER JOIN
			@XML.nodes('//Data/Person/Addresses/Address') XA(Data)
			ON 1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/BankAccounts/BankAccount') XAN(Data)
			ON 1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/DrivingLicenses/DrivingLicense') XDLA(Data)	
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/NINumbers/NINumber') XNI(Data)
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/Passports/Passport') XPAS(Data)
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/EmailAddresses/EmailAddress') XEMI(Data)
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/PaymentCards/PaymentCard') XPC(Data)
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/Telephones/Telephone') XTN(Data)
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/Vehicles/Vehicle') XV(Data)
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/Organisations/Organisation') XORG(Data)
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/Policies/Policy') XPOL(Data)
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Person/Handsets/Handset') XHAND(Data)
			ON  1 = 1
		) _XML

	ON

		ISNULL(MRP.FirstName,  0) <= _XML.FirstName
		AND ISNULL(MRP.LastName,  0) <= _XML.LastName
		AND ISNULL(MRP.DateOfBirth, 0) <= _XML.DateOfBirth
		AND ISNULL(MRP.SubBuilding, 0) <= _XML.SubBuilding
		AND ISNULL(MRP.Building, 0) <= _XML.Building
		AND ISNULL(MRP.BuildingNumber, 0) <= _XML.BuildingNumber
		AND ISNULL(MRP.Street, 0) <= _XML.Street
		AND ISNULL(MRP.Locality, 0) <= _XML.Locality
		AND ISNULL(MRP.Town, 0)	<= _XML.Town
		AND ISNULL(MRP.PostCode, 0)	<= _XML.PostCode
		AND ISNULL(MRP.AccountNumber, 0) <= _XML.AccountNumber
		AND ISNULL(MRP.DriverNumber, 0) <= _XML.DriverNumber
		AND ISNULL(MRP.NINumber, 0) <= _XML.NINumber
		AND ISNULL(MRP.PassportNumber, 0) <= _XML.PassportNumber
		AND ISNULL(MRP.EmailAddress, 0) <= _XML.EmailAddress
		AND ISNULL(MRP.PaymentCardNumber, 0) <=  _XML.PaymentCardNumber
		AND ISNULL(MRP.TelephoneNumber, 0) <= _XML.TelephoneNumber
		AND ISNULL(MRP.VehicleRegistration, 0) <= _XML.VehicleRegistration
		AND ISNULL(MRP.VehicleVIN, 0) <= _XML.VehicleVIN
		AND ISNULL(MRP.OrganisationName, 0) <= _XML.OrganisationName
		AND ISNULL(MRP.RegisteredNumber, 0) <= _XML.RegisteredNumber
		AND ISNULL(MRP.VATNumber, 0) <= _XML.VATNumber
		AND ISNULL(MRP.PolicyNumber, 0) <= _XML.PolicyNumber
		AND ISNULL(MRP.PafUPRN, 0) <= _XML.PafUPRN
		AND ISNULL(MRP.HandestImei, 0) <= _XML.HandsetImei
)







