﻿


CREATE FUNCTION [dbo].[fnGetMatchingNINumberTSql] 
(
	@Id INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @SQL NVARCHAR(MAX)
DECLARE @RiskClaim_Id BIT = (SELECT RiskClaim_Id FROM dbo.MatchingRulesNINumber WHERE RuleNo = @Id)

IF (@RiskClaim_Id IS NULL)
	SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  dbo.NINumber AS A 
	WHERE (NINumber = @NINumber)'
FROM dbo.MatchingRulesNINumber WHERE RuleNo = @Id
ELSE
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	  
	FROM  dbo.NINumber AS A 
	INNER JOIN Person2NINumber AS P2IP ON A.Id = P2IP.NINumber_Id AND P2IP.AdaRecordStatus = 0
	WHERE ' 
	+
	dbo.fnFormatWhereClause(
	CASE WHEN NINumber = 1 THEN ' AND NINumber = @NINumber' ELSE '' END + 
	CASE WHEN RiskClaim_Id = 1 THEN ' AND BaseRiskClaim_Id = @RiskClaim_Id' ELSE '' END )

FROM dbo.MatchingRulesNINumber  WHERE RuleNo = @Id

RETURN @SQL

END
