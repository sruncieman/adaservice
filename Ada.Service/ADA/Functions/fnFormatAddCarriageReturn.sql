﻿
CREATE FUNCTION [dbo].[fnFormatAddCarriageReturn]
(
	@Clause  NVARCHAR(1000)
)
RETURNS NVARCHAR(1000)
AS
BEGIN
	DECLARE @Result NVARCHAR(1000)
	
	SELECT @Result = CHAR(13) + CHAR(10) + @Clause
	
	RETURN @Result

END

