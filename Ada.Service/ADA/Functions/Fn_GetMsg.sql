﻿
CREATE FUNCTION [dbo].[Fn_GetMsg]
(
	@JS VARCHAR(MAX)
)
RETURNS 
@Table TABLE 
(
	 RptHeader VARCHAR(1000),
	 RptMsg VARCHAR(1000)
)
AS
BEGIN
	DECLARE @Tmp TABLE(parent_id INT, object_id INT, name VARCHAR(1000), stringvalue VARCHAR(1000))
	INSERT INTO @Tmp( object_id, parent_id, name, stringvalue)
	SELECT object_id, parent_id, name, stringvalue FROM 
	[dbo].[fn_ParseJSON](@JS) 
	
	INSERT INTO @Table
	SELECT RH.stringvalue AS RptHeader, A.stringvalue  AS RptMsg FROM @Tmp A
		INNER JOIN @Tmp B
		ON
		A.parent_id = B.object_id
		INNER JOIN @Tmp C
		ON
		B.parent_id = C.object_id
		INNER JOIN @Tmp D
		ON
		C.parent_id = D.object_id
		INNER JOIN @Tmp E
		ON
		D.parent_id = E.object_id
		INNER JOIN 
		(SELECT * FROM @Tmp A WHERE (A.name  = 'ReportHeading')) RH
		ON E.parent_id = RH.parent_id
		WHERE (A.name = 'MessageLow' AND LEN(A.stringvalue) > 1)
	
	
	
	RETURN 
END
