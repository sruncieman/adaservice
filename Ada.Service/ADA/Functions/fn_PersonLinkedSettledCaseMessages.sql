﻿
CREATE FUNCTION [dbo].[fn_PersonLinkedSettledCaseMessages]
(	
	
	 @Person_Id INT,
	 @Incident_Id INT,
	 @Flag INT = NULL

)
RETURNS TABLE 
AS
RETURN 
(
	SELECT 
	    S.Text AS Salutation,
		P.FirstName, 
		P.LastName, 
		P.DateOfBirth, 
		PT.PartyTypeText,  
		SPT.SubPartyText, 
		I2P.FiveGrading AS Incident2Person_FiveGrading,
		i2po.FiveGrading AS Incident2PersonOutcome_FiveGrading, 
		I2PO.MannerOfResolution,
		i.IncidentDate,  
		CT.ClaimType, 
		KC.KeoghsEliteReference,
		@Flag AS Flag
	FROM 
		dbo.Person P
	INNER JOIN 
		dbo.Incident2Person I2P
		 ON P.Id = I2p.Person_Id
	INNER JOIN 
		Incident I 
		 ON I2P.Incident_Id = I.Id
	LEFT JOIN 
		Incident2PersonOutcome I2PO
		 ON I2p.Person_Id = I2PO.Person_Id
	LEFT JOIN 
		KeoghsCase2Incident KC2I
		 ON KC2I.Incident_Id = I.Id
		 AND I2P.Incident_Id = I2P.Incident_Id
	LEFT JOIN  
		KeoghsCase KC
		 ON KC2I.KeoghsCase_Id = KC.Id
	INNER JOIN 
		PartyType PT
		 ON i2p.PartyType_Id = PT.Id
	INNER JOIN 
		SubPartyType SPT
		 ON i2p.SubPartyType_Id = SPT.Id
	INNER JOIN 
		ClaimType CT
		 ON i.ClaimType_Id = CT.Id
	INNER JOIN 
		Salutation S 
		 ON P.Salutation_Id = S.Id
	WHERE 
		P.Id = @Person_Id AND  I.Id = @Incident_Id
)

