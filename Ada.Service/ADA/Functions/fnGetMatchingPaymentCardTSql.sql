﻿
CREATE FUNCTION [dbo].[fnGetMatchingPaymentCardTSql] 
(
	@Id INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN


DECLARE @SQL NVARCHAR(MAX)
DECLARE @_Id BIT = (SELECT RiskClaim_Id FROM dbo.MatchingRulesPaymentCard WHERE RuleNo = @Id)

IF (@_Id IS NULL)
	SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  dbo.PaymentCard AS A WITH(NOLOCK)
	WHERE  '
				+ 
		dbo.fnFormatWhereClause(
		CASE WHEN PaymentCardNumber = 1 THEN ' AND PaymentCardNumber = @PaymentCardNumber' ELSE '' END 
		+ CASE WHEN SortCode = 1 THEN ' AND SortCode = @SortCode' ELSE '' END 
		+ CASE WHEN BankName = 1 THEN ' AND BankName = @BankName' ELSE '' END
		+ CASE WHEN ExpiryDate = 1 THEN ' AND ExpiryDate = @ExpiryDate' ELSE '' END

		)
		+ ' GROUP BY A.Id'
FROM dbo.MatchingRulesPaymentCard WHERE RuleNo = @Id
ELSE
SELECT @SQL = 	
	'SELECT A.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType	
	FROM  PaymentCard AS A WITH (NOLOCK) 
	--INNER JOIN Person2PaymentCard AS P2PC  WITH (NOLOCK)  ON A.Id = P2PC.PaymentCard_Id
	
	
	INNER JOIN
	(
	SELECT BaseRiskClaim_Id FROM Policy2PaymentCard WHERE BaseRiskClaim_Id = @RiskClaim_Id
	UNION
	SELECT BaseRiskClaim_Id FROM dbo.Organisation2PaymentCard WHERE BaseRiskClaim_Id = @RiskClaim_Id
	UNION
	SELECT BaseRiskClaim_Id FROM Person2PaymentCard WHERE BaseRiskClaim_Id = @RiskClaim_Id
	) AS U
	 ON U.BaseRiskClaim_Id = @RiskClaim_Id
	
	
	
	WHERE '
		+ 
		dbo.fnFormatWhereClause(
		 CASE WHEN PaymentCardNumber = 1 THEN ' AND PaymentCardNumber = @PaymentCardNumber' ELSE '' END 
		+ CASE WHEN SortCode = 1 THEN ' AND SortCode = @SortCode' ELSE '' END 
		+ CASE WHEN BankName = 1 THEN ' AND BankName = @BankName' ELSE '' END
		+ CASE WHEN ExpiryDate = 1 THEN ' AND ExpiryDate = @ExpiryDate' ELSE '' END
		)
		+ ' GROUP BY A.Id'

FROM dbo.MatchingRulesPaymentCard WHERE RuleNo = @Id





RETURN @SQL

END
