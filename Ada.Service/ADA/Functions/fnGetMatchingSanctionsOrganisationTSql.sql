﻿CREATE FUNCTION [dbo].fnGetMatchingSanctionsOrganisationTSql 
(
 @Id INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX)
	DECLARE @From NVARCHAR(1000) ='
		FROM InternationalSanctions.dbo.Organisation ORG'
	
	DECLARE @JoinOrganisation NVARCHAR(1000) ='
		INNER JOIN @XML.nodes(''//Organisation'') XO(Data)'
	
	DECLARE @JoinEmail NVARCHAR(1000) ='
		INNER JOIN InternationalSanctions.dbo.Organisation2Email O2EMA ON ORG.Id = O2EMA.OrganisationId
		INNER JOIN @XML.nodes(''//Organisation/EmailAddresses/EmailAddress'') XEMA(Data) ON O2EMA.EmailAddress = XEMA.Data.value(''@EmailAddress'', ''VARCHAR(100)'')'                    

	DECLARE @JoinWebSite NVARCHAR(1000) ='         
		INNER JOIN InternationalSanctions.dbo.Organisation2Website AS O2WEB ON ORG.Id = O2WEB.OrganisationId
		INNER JOIN @XML.nodes(''//Organisation/Websites/Website'') XWEB(Data) ON O2WEB.Website = XWEB.Data.value(''@URL'', ''VARCHAR(100)'')'  
		 
	DECLARE @JoinTelephone NVARCHAR(1000) ='
		INNER JOIN InternationalSanctions.dbo.Organisation2Telephone AS O2TEL ON ORG.Id = O2TEL.OrganisationId           
		INNER JOIN @XML.nodes(''//Organisation/TelephoneNumbers/TelephoneNumber'') XTEL(Data) ON O2TEL.Telephone = XTEL.Data.value(''@TelephoneNumber'', ''VARCHAR(50)'')'             
           
	DECLARE @JoinAddress NVARCHAR(1000) = '                   
		INNER JOIN @XML.nodes(''//Organisation/Addresses/Address'') XADD(Data)'
	
	SELECT @SQL =
		'SELECT DISTINCT CASE ORG.SanctionListId WHEN 1 THEN ''UKConsolidatedListOfTargets:'' WHEN 2 THEN ''USSpeciallyDesignatedNationalsList:'' END + CAST(ORG.UniqueID AS VARCHAR(10)), '
		+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
		+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
		+ CONVERT(VARCHAR(20), MatchType) + ' AS MatchType '
		+ @From 
			+ CASE WHEN ISNULL(CONVERT(INT, OrganisationName), 0) > 0 
			THEN @JoinOrganisation 
			+ dbo.fnFormatOnClause(CASE WHEN OrganisationName = 1 THEN 'AND ORG.OrganisationName = XO.Data.value(''@OrganisationName'', ''VARCHAR(50)'') ' ELSE '' END)
		 ELSE '' END
		+ CASE WHEN EmailAddress = 1 THEN @JoinEmail ELSE '' END 
		+ CASE WHEN Website = 1 THEN @JoinWebSite ELSE '' END 
		+ CASE WHEN TelephoneNumber = 1 THEN @JoinTelephone ELSE '' END 
		+ CASE WHEN ISNULL(CONVERT(INT, SubBuilding), 0) + ISNULL(CONVERT(INT, Building), 0) +  ISNULL(CONVERT(INT, BuildingNumber), 0) +  ISNULL(CONVERT(INT, Street), 0) +  ISNULL(CONVERT(INT, Locality), 0) +  ISNULL(CONVERT(INT, Town), 0) + ISNULL(CONVERT(INT, PostCode), 0) > 0 
			THEN @JoinAddress
			+ dbo.fnFormatOnClause(
				  CASE WHEN PostCode = 1 THEN 'AND (ORG.AddressLine1 LIKE ''%'' + XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine2 LIKE ''%'' + XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine3 LIKE ''%'' + XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine4 LIKE ''%'' + XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine5 LIKE ''%'' + XADD.Data.value(''@PostCode'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN Building = 1 THEN 'AND (ORG.AddressLine1 LIKE ''%'' + XADD.Data.value(''@Building'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine2 LIKE ''%'' + XADD.Data.value(''@Building'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine3 LIKE ''%'' + XADD.Data.value(''@Building'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine4 LIKE ''%'' + XADD.Data.value(''@Building'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine5 LIKE ''%'' + XADD.Data.value(''@Building'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN SubBuilding = 1 THEN 'AND (ORG.AddressLine1 LIKE ''%'' + XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine2 LIKE ''%'' + XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine3 LIKE ''%'' + XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine4 LIKE ''%'' + XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine5 LIKE ''%'' + XADD.Data.value(''@SubBuilding'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN Street = 1 THEN 'AND (ORG.AddressLine1 LIKE ''%'' + XADD.Data.value(''@Street'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine2 LIKE ''%'' + XADD.Data.value(''@Street'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine3 LIKE ''%'' + XADD.Data.value(''@Street'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine4 LIKE ''%'' + XADD.Data.value(''@Street'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine5 LIKE ''%'' + XADD.Data.value(''@Street'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN BuildingNumber = 1 THEN 'AND (ORG.AddressLine1 LIKE ''%'' + XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine2 LIKE ''%'' + XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine3 LIKE ''%'' + XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine4 LIKE ''%'' + XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine5 LIKE ''%'' + XADD.Data.value(''@BuildingNumber'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN Town = 1 THEN 'AND (ORG.AddressLine1 LIKE ''%'' + XADD.Data.value(''@Town'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine2 LIKE ''%'' + XADD.Data.value(''@Town'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine3 LIKE ''%'' + XADD.Data.value(''@Town'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine4 LIKE ''%'' + XADD.Data.value(''@Town'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine5 LIKE ''%'' + XADD.Data.value(''@Town'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END
				+ CASE WHEN Locality = 1 THEN 'AND (ORG.AddressLine1 LIKE ''%'' + XADD.Data.value(''@Locality'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine2 LIKE ''%'' + XADD.Data.value(''@Locality'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine3 LIKE ''%'' + XADD.Data.value(''@Locality'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine4 LIKE ''%'' + XADD.Data.value(''@Locality'', ''VARCHAR(50)'') + ''%'' OR ORG.AddressLine5 LIKE ''%'' + XADD.Data.value(''@Locality'', ''VARCHAR(50)'') + ''%'') ' ELSE '' END)
			ELSE '' END
	FROM  dbo.MatchingRulesOrganisation
	WHERE RuleNo = @Id

	RETURN @SQL

END