﻿



CREATE FUNCTION [dbo].[fnGetMatchingSanctionsOrganisationToProcess]
(	
	@XML XML
)
RETURNS TABLE 
AS 
RETURN (

	SELECT 
		MAT.*
	FROM  dbo.MatchingRulesOrganisation MAT WITH(NOLOCK) 
	INNER JOIN(
		SELECT  
			MAX(LEN(XORG.Data.value('@OrganisationName', 'VARCHAR(50)'))) OrganisationName,
			MAX(LEN(XEML.Data.value('@EmailAddress', 'VARCHAR(50)'))) EmailAddress,
			MAX(LEN(XWEB.Data.value('@URL', 'VARCHAR(50)'))) Website,
			MAX(LEN(XTEL.Data.value('@TelephoneNumber', 'VARCHAR(50)'))) TelephoneNumber,
			MAX(LEN(XADD.Data.value('@SubBuilding', 'VARCHAR(50)'))) SubBuilding,
			MAX(LEN(XADD.Data.value('@Building', 'VARCHAR(50)'))) Building,
			MAX(LEN(XADD.Data.value('@BuildingNumber', 'VARCHAR(50)'))) BuildingNumber,
			MAX(LEN(XADD.Data.value('@Street', 'VARCHAR(50)'))) Street,
			MAX(LEN(XADD.Data.value('@Locality', 'VARCHAR(50)'))) Locality,
			MAX(LEN(XADD.Data.value('@Town', 'VARCHAR(50)'))) Town,
			MAX(LEN(XADD.Data.value('@PostCode', 'VARCHAR(50)'))) PostCode
		FROM
			@XML.nodes('//Data/Organisation') XORG(Data) 	
		INNER JOIN
			@XML.nodes('//Data/Organisation/EmailAddresses/EmailAddress') XEML(Data) 	
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Organisation/Websites/Website') XWEB(Data) 	
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Organisation/TelephoneNumbers/TelephoneNumber') XTEL(Data) 	
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Organisation/Addresses/Address') XADD(Data) 	
			ON  1 = 1

		) _XML
		ON ISNULL(MAT.OrganisationName,  0) <= _XML.OrganisationName
		AND MAT.RegisteredNumber IS NULL
		AND MAT.VATNumber IS NULL
		AND ISNULL(MAT.EmailAddress,  0) <= _XML.EmailAddress
		AND ISNULL(MAT.Website,  0) <= _XML.Website
		AND MAT.IPAddress IS NULL
		AND ISNULL(MAT.TelephoneNumber,  0) <= _XML.TelephoneNumber
		AND MAT.AccountNumber IS NULL
		AND MAT.PaymentCardNumber IS NULL
		AND ISNULL(MAT.SubBuilding,  0) <= _XML.SubBuilding
		AND ISNULL(MAT.Building,  0) <= _XML.Building
		AND ISNULL(MAT.BuildingNumber,  0) <= _XML.BuildingNumber
		AND ISNULL(MAT.Street,  0) <= _XML.Street
		AND ISNULL(MAT.Locality,  0) <= _XML.Locality
		AND ISNULL(MAT.Town,  0) <= _XML.Town
		AND ISNULL(MAT.PostCode,  0) <= _XML.PostCode
		AND MAT.PafUPRN IS NULL
		WHERE MAT.MatchType = 0 --ConfirmedOnly
		AND RiskClaimFlag != 1
)