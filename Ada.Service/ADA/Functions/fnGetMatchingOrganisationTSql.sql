﻿
CREATE FUNCTION [dbo].[fnGetMatchingOrganisationTSql] 
(
	 @Id INT
	,@UseRiskClaimId BIT
	,@RCid VARCHAR(10)
	,@RiskClaim_Id VARCHAR(MAX)
	,@OrganisationName VARCHAR(MAX)
	,@RegisteredNumber VARCHAR(MAX)
	,@VATNumber VARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE @SQL NVARCHAR(MAX)

DECLARE @From NVARCHAR(1000) ='
FROM dbo.Organisation ORG'
	
DECLARE @JoinRiskClaim NVARCHAR(1000) ='
INNER JOIN Incident2Organisation AS INC2O ON ORG.Id = INC2O.Organisation_Id AND INC2O.AdaRecordStatus = 0'

DECLARE @JoinEmail NVARCHAR(1000) ='
INNER JOIN Organisation2Email AS O2EMA ON ORG.Id = O2EMA.Organisation_Id AND O2EMA.AdaRecordStatus = 0
INNER JOIN Email AS EMA ON O2EMA.Email_Id = EMA.Id AND EMA.AdaRecordStatus = 0'                    
	                               
DECLARE @JoinWebSite NVARCHAR(1000) ='           
INNER JOIN Organisation2WebSite AS O2WEB ON ORG.Id = O2WEB.Organisation_Id AND O2WEB.AdaRecordStatus = 0 
INNER JOIN WebSite AS WEB ON O2WEB.WebSite_Id = WEB.Id AND WEB.AdaRecordStatus = 0 '             
          
DECLARE @JoinIPAddress NVARCHAR(1000) ='    
INNER JOIN Organisation2IPAddress AS O2ADD ON ORG.Id = O2ADD.Organisation_Id  AND O2ADD.AdaRecordStatus = 0
INNER JOIN IPAddress AS IPA ON O2ADD.IPAddress_Id = IPA.Id AND IPA.AdaRecordStatus = 0'             

DECLARE @JoinTelephone NVARCHAR(1000) ='
INNER JOIN Organisation2Telephone AS O2TEL ON ORG.Id = O2TEL.Organisation_Id  AND O2TEL.AdaRecordStatus = 0
INNER JOIN Telephone AS TEL ON O2TEL.Telephone_Id = TEL.Id  AND TEL.AdaRecordStatus = 0'             

DECLARE @JoinBankAccount NVARCHAR(1000) ='
INNER JOIN Organisation2BankAccount AS O2BAN ON ORG.Id = O2BAN.Organisation_Id  AND O2BAN.AdaRecordStatus = 0
INNER JOIN BankAccount AS BAN ON O2BAN.BankAccount_Id = BAN.Id AND BAN.AdaRecordStatus = 0 '             
 
DECLARE @JoinPaymentCard NVARCHAR(1000) ='                      
INNER JOIN  Organisation2PaymentCard AS O2PER ON ORG.Id = O2PER.Organisation_Id  AND O2PER.AdaRecordStatus = 0
INNER JOIN PaymentCard AS PAY ON O2PER.PaymentCard_Id = PAY.Id AND PAY.AdaRecordStatus = 0 '             
	                
DECLARE @JoinAddress NVARCHAR(1000) ='                    
INNER JOIN Organisation2Address AS O2ADD ON ORG.Id = O2ADD.Organisation_Id AND O2ADD.AdaRecordStatus = 0 AND (O2ADD.BaseRiskClaim_Id = ' + ISNULL(@RCid,'''''') + ' OR ' + ISNULL(@RCid,'''''') + ' ='''')
INNER JOIN Address AS _ADD ON O2ADD.Address_Id = _ADD.Id AND _ADD.AdaRecordStatus = 0
INNER JOIN #Addresses _ADD_ '           
          
SELECT
	@SQL =
	--SELECT
	'SELECT DISTINCT ORG.Id, '
	+ CONVERT(VARCHAR(20), RuleNo) + ' AS RuleNo, '
	+ CONVERT(VARCHAR(20), PriorityGroup) + ' AS PriorityGroup, '
	+ CONVERT(VARCHAR(20), CASE WHEN @UseRiskClaimId = 1 THEN 0 ELSE MatchType END) + ' AS MatchType'
	--FROM
	+ @From 
	+ CASE WHEN @UseRiskClaimId = 1 THEN @JoinRiskClaim ELSE '' END 
	+ CASE WHEN EmailAddress = 1 THEN @JoinEmail ELSE '' END 
	+ CASE WHEN Website  = 1 THEN @JoinWebSite ELSE '' END 
	+ CASE WHEN IPAddress = 1 THEN @JoinIPAddress ELSE '' END 
	+ CASE WHEN TelephoneNumber = 1 THEN @JoinTelephone ELSE '' END 
	+ CASE WHEN AccountNumber = 1 THEN @JoinBankAccount ELSE '' END 
	+ CASE WHEN PaymentCardNumber = 1 THEN @JoinPaymentCard ELSE '' END 
	+ CASE WHEN ISNULL(CONVERT(INT, SubBuilding), 0) + ISNULL(CONVERT(INT, Building), 0) +  ISNULL(CONVERT(INT, BuildingNumber), 0) +  ISNULL(CONVERT(INT, Street), 0) +  ISNULL(CONVERT(INT, Locality), 0) +  ISNULL(CONVERT(INT, Town), 0) +  ISNULL(CONVERT(INT, PostCode), 0) + ISNULL(CONVERT(INT, PafUPRN), 0)> 0 THEN @JoinAddress
	+ dbo.fnFormatOnClause(
		  CASE WHEN SubBuilding = 1 THEN 'AND _ADD.SubBuilding = _ADD_.SubBuilding  ' ELSE '' END
		+ CASE WHEN Building = 1 THEN 'AND _ADD.Building = _ADD_.Building ' ELSE '' END
		+ CASE WHEN BuildingNumber = 1 THEN 'AND _ADD.BuildingNumber = _ADD_.BuildingNumber ' ELSE '' END
		+ CASE WHEN Street = 1 THEN 'AND _ADD.Street = _ADD_.Street ' ELSE '' END
		+ CASE WHEN Locality = 1 THEN 'AND _ADD.Locality = _ADD_.Locality ' ELSE '' END
		+ CASE WHEN Town = 1 THEN 'AND _ADD.Town = _ADD_.Town ' ELSE '' END
		+ CASE WHEN PostCode = 1 THEN 'AND _ADD.PostCode = _ADD_.PostCode ' ELSE '' END
		+ CASE WHEN PafUPRN = 1 THEN 'AND NULLIF(_ADD.PafUPRN,'''') = NULLIF(_ADD_.PafUPRN,'''') ' ELSE '' END
	) ELSE '' END 
	--WHERE
	+ dbo.fnFormatAddCarriageReturn('WHERE ORG.ADARecordStatus = 0 ') + CHAR(13)
	+ CASE WHEN @UseRiskClaimId = 1 THEN 'AND INC2O.BaseRiskClaim_Id  = ''' + @RiskClaim_ID  + '''  ' + CHAR(13) ELSE '' END
	+ CASE WHEN OrganisationName = 1 THEN 'AND ORG.OrganisationNameCalcuated = ''' + @OrganisationName + ''' ' + CHAR(13)  ELSE '' END
	+ CASE WHEN RegisteredNumber = 1 THEN 'AND ORG.RegisteredNumber = ''' + @RegisteredNumber + ''' ' + CHAR(13)  ELSE '' END  
	+ CASE WHEN VATNumber = 1 THEN 'AND ORG.VATNumber = ''' + @VATNumber + ''' ' + CHAR(13)  ELSE '' END  
	+ CASE WHEN EmailAddress = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #EmailAddresses _EMA_ WHERE _EMA_.EmailAddress = EMA.EmailAddress)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN Website = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #Websites _WEBS_ WHERE _WEBS_.URL = WEB.URL)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN IPAddress = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #IPAddresses _IPA_ WHERE _IPA_.IPAddress = IPA.IPAddress)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN TelephoneNumber = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #TelephoneNumbers _TELN_ WHERE _TELN_.TelephoneNumber = TEL.TelephoneNumber)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN PaymentCardNumber = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #PaymentCards _PC_ WHERE _PC_.PaymentCardNumber = Pay.PaymentCardNumber)' + CHAR(13)  ELSE '' END 
	+ CASE WHEN AccountNumber = 1 THEN 'AND EXISTS (SELECT TOP 1 1 FROM #BankAccounts _BA_ WHERE _BA_.AccountNumber = BAN.AccountNumber)' + CHAR(13)  ELSE '' END 
	--GROUP BY	
	+ 'GROUP BY ORG.Id'
	+ ' OPTION (RECOMPILE)'
	FROM     dbo.MatchingRulesOrganisation
	WHERE RuleNo = @Id

RETURN @SQL
END
