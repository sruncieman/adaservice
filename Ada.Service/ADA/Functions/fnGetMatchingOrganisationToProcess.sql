﻿


CREATE FUNCTION [dbo].[fnGetMatchingOrganisationToProcess]
(	
	@XML XML
)
RETURNS TABLE 
AS 
RETURN (

	SELECT 
		MAT.*, HasRiskClaim_Id
	FROM  dbo.MatchingRulesOrganisation MAT WITH(NOLOCK) 
	INNER JOIN(
		SELECT  
			CONVERT(BIT, MAX(LEN(XORG.Data.value('@RiskClaim_Id', 'VARCHAR(50)')))) HasRiskClaim_Id,
			MAX(LEN(XORG.Data.value('@OrganisationName', 'VARCHAR(50)'))) OrganisationName,
			MAX(LEN(XORG.Data.value('@RegisteredNumber', 'VARCHAR(50)'))) RegisteredNumber,
			MAX(LEN(XORG.Data.value('@VATNumber', 'VARCHAR(50)'))) VATNumber,
			MAX(LEN(XEML.Data.value('@EmailAddress', 'VARCHAR(50)'))) EmailAddress,
			MAX(LEN(XWEB.Data.value('@URL', 'VARCHAR(50)'))) Website,
			MAX(LEN(XIPA.Data.value('@IPAddress', 'VARCHAR(50)'))) IPAddress,
			MAX(LEN(XTEL.Data.value('@TelephoneNumber', 'VARCHAR(50)'))) TelephoneNumber,
			MAX(LEN(XACC.Data.value('@AccountNumber', 'VARCHAR(50)'))) AccountNumber,
			MAX(LEN(XPAY.Data.value('@PaymentCardNumber', 'VARCHAR(50)'))) PaymentCardNumber,
			MAX(LEN(XADD.Data.value('@SubBuilding', 'VARCHAR(50)'))) SubBuilding,
			MAX(LEN(XADD.Data.value('@Building', 'VARCHAR(50)'))) Building,
			MAX(LEN(XADD.Data.value('@BuildingNumber', 'VARCHAR(50)'))) BuildingNumber,
			MAX(LEN(XADD.Data.value('@Street', 'VARCHAR(50)'))) Street,
			MAX(LEN(XADD.Data.value('@Locality', 'VARCHAR(50)'))) Locality,
			MAX(LEN(XADD.Data.value('@Town', 'VARCHAR(50)'))) Town,
			MAX(LEN(XADD.Data.value('@PostCode', 'VARCHAR(50)'))) PostCode,
			MAX(LEN(XADD.Data.value('@PafUPRN', 'VARCHAR(50)'))) PafUPRN
		FROM
			@XML.nodes('//Data/Organisation') XORG(Data) 	
		INNER JOIN
			@XML.nodes('//Data/Organisation/EmailAddresses/EmailAddress') XEML(Data) 	
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Organisation/Websites/Website') XWEB(Data) 	
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Organisation/IPAddresses/IPAddress') XIPA(Data) 	
			ON  1 = 1

		INNER JOIN
			@XML.nodes('//Data/Organisation/TelephoneNumbers/TelephoneNumber') XTEL(Data) 	
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Organisation/BankAccounts/BankAccount') XACC(Data) 	
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Organisation/PaymentCards/PaymentCard') XPAY(Data) 	
			ON  1 = 1
		INNER JOIN
			@XML.nodes('//Data/Organisation/Addresses/Address') XADD(Data) 	
			ON  1 = 1

		) _XML
		ON
		--ISNULL(MAT.RiskClaim_Id,  0) <= _XML.RiskClaim_Id
		ISNULL(MAT.OrganisationName,  0) <= _XML.OrganisationName
		AND ISNULL(MAT.RegisteredNumber,  0) <= _XML.RegisteredNumber
		AND ISNULL(MAT.VATNumber,  0) <= _XML.VATNumber
		AND ISNULL(MAT.EmailAddress,  0) <= _XML.EmailAddress
		AND ISNULL(MAT.Website,  0) <= _XML.Website
		AND ISNULL(MAT.IPAddress,  0) <= _XML.IPAddress
		AND ISNULL(MAT.TelephoneNumber,  0) <= _XML.TelephoneNumber
		AND ISNULL(MAT.AccountNumber,  0) <= _XML.AccountNumber
		AND ISNULL(MAT.PaymentCardNumber,  0) <= _XML.PaymentCardNumber
		AND ISNULL(MAT.SubBuilding,  0) <= _XML.SubBuilding
		AND ISNULL(MAT.Building,  0) <= _XML.Building
		AND ISNULL(MAT.BuildingNumber,  0) <= _XML.BuildingNumber
		AND ISNULL(MAT.Street,  0) <= _XML.Street
		AND ISNULL(MAT.Locality,  0) <= _XML.Locality
		AND ISNULL(MAT.Town,  0) <= _XML.Town
		AND ISNULL(MAT.PostCode,  0) <= _XML.PostCode
		AND ISNULL(MAT.PafUPRN,  0) <= _XML.PafUPRN
)




