﻿CREATE FULLTEXT INDEX ON [dbo].[Address]
    ([AddressSearch] LANGUAGE 1033)
    KEY INDEX [PK_Address]
    ON [FullTextCatalog];

