﻿CREATE TABLE [dbo].[VehicleLinkType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [LinkText]        NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_VehicleLinkType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_VehicleLinkType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_VehicleLinkType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

