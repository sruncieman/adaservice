﻿CREATE TABLE [dbo].[RiskBatch] (
    [Id]                      INT           IDENTITY (1, 1) NOT NULL,
    [RiskClient_Id]           INT           NOT NULL,
    [BatchReference]          VARCHAR (50)  NOT NULL,
    [BatchEntityType]         VARCHAR (100) NOT NULL,
    [ClientBatchReference]    VARCHAR (50)  NULL,
    [BatchStatus]             INT           CONSTRAINT [DF__tmp_ms_xx__Batch__7A12F3AE] DEFAULT ((0)) NOT NULL,
    [BatchStatusLastModified] DATETIME      CONSTRAINT [DF_RiskBatch_BatchStatusLastModified] DEFAULT (getdate()) NOT NULL,
    [RiskOriginalFile_Id]     INT           NULL,
    [CreatedDate]             DATETIME      NOT NULL,
    [CreatedBy]               VARCHAR (50)  NOT NULL,
    [VerificationResults]     VARCHAR (MAX) NULL,
    [MappingResults]          VARCHAR (MAX) NULL,
    [TotalClaimsReceived]     INT           CONSTRAINT [DF__tmp_ms_xx__Total__7B0717E7] DEFAULT ((0)) NOT NULL,
    [TotalNewClaims]          INT           CONSTRAINT [DF__tmp_ms_xx__Total__7BFB3C20] DEFAULT ((0)) NOT NULL,
    [TotalModifiedClaims]     INT           CONSTRAINT [DF__tmp_ms_xx__Total__7CEF6059] DEFAULT ((0)) NOT NULL,
    [TotalClaimsLoaded]       INT           CONSTRAINT [DF__tmp_ms_xx__Total__7DE38492] DEFAULT ((0)) NOT NULL,
    [TotalClaimsInError]      INT           CONSTRAINT [DF__tmp_ms_xx__Total__7ED7A8CB] DEFAULT ((0)) NOT NULL,
    [TotalClaimsSkipped]      INT           CONSTRAINT [DF__tmp_ms_xx__Total__7FCBCD04] DEFAULT ((0)) NOT NULL,
    [LoadingDurationInMs]     INT           CONSTRAINT [DF__tmp_ms_xx__Loadi__00BFF13D] DEFAULT ((0)) NOT NULL,
    [VisibilityStatus]        INT           CONSTRAINT [DF_RiskBatch_Visible] DEFAULT ((0)) NOT NULL,
    [RiskBatchReport_Id]      INT           NULL,
    [RiskUser_Id]             INT           CONSTRAINT [DF_RiskBatch_RiskUser_Id] DEFAULT ((0)) NULL,
    [SubmitDirect]            BIT           CONSTRAINT [DF_RiskBatch_SubmitDirect] DEFAULT ((0)) NOT NULL,
    [ScoreDirect]             BIT           CONSTRAINT [DF_RiskBatch_ScoreDirect] DEFAULT ((0)) NOT NULL,
    [BatchPriorityOverride]   INT           NULL,
    CONSTRAINT [PK_RiskBatch] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [FK_RiskBatch_RiskClient] FOREIGN KEY ([RiskClient_Id]) REFERENCES [dbo].[RiskClient] ([Id]),
    CONSTRAINT [FK_RiskBatch_RiskOriginalFile] FOREIGN KEY ([RiskOriginalFile_Id]) REFERENCES [dbo].[RiskOriginalFile] ([PkId]),
    CONSTRAINT [FK_RiskBatch_RiskReports] FOREIGN KEY ([RiskBatchReport_Id]) REFERENCES [dbo].[RiskReports] ([PkId]),
    CONSTRAINT [FK_RiskBatch_RiskUser] FOREIGN KEY ([RiskUser_Id]) REFERENCES [dbo].[RiskUser] ([Id])
);








GO
ALTER TABLE [dbo].[RiskBatch] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




GO
CREATE NONCLUSTERED INDEX [IX_RiskBatch_RiskClient_Id]
    ON [dbo].[RiskBatch]([RiskClient_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RiskBatch_BatchStatus_SubmitDirect]
    ON [dbo].[RiskBatch]([BatchStatus] ASC, [SubmitDirect] ASC);

