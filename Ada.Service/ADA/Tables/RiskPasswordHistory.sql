﻿CREATE TABLE [dbo].[RiskPasswordHistory] (
    [Id]          INT            NOT NULL IDENTITY,
    [RiskUser_Id] INT            NOT NULL,
    [Password]    NVARCHAR (128) NULL,
    [DateChanged] SMALLDATETIME  NULL,
    CONSTRAINT [FK_RiskPasswordHistory_RiskUser] FOREIGN KEY ([RiskUser_Id]) REFERENCES [dbo].[RiskUser] ([Id]), 
    CONSTRAINT [PK_RiskPasswordHistory] PRIMARY KEY ([Id])
);

