﻿CREATE TABLE [dbo].[KeoghsCase] (
    [Id]                   INT             IDENTITY (1, 1) NOT NULL,
    [KeoghsCaseId]         NVARCHAR (50)   NULL,
    [KeoghsEliteReference] NVARCHAR (50)   NULL,
    [KeoghsOffice_Id]      INT             NOT NULL,
    [FeeEarner]            NVARCHAR (50)   NULL,
    [Client_Id]            INT             NOT NULL,
    [ClientReference]      NVARCHAR (50)   NULL,
    [ClientClaimsHandler]  NVARCHAR (50)   NULL,
    [CaseStatus_Id]        INT             NOT NULL,
    [CurrentReserve]       NVARCHAR (59)   NULL,
    [ClosureDate]          SMALLDATETIME   NULL,
    [WeedDate]             SMALLDATETIME   NULL,
    [KeyAttractor]         NVARCHAR (100)  NULL,
    [Source]               NVARCHAR (50)   NULL,
    [SourceReference]      NVARCHAR (50)   NULL,
    [SourceDescription]    NVARCHAR (1024) NULL,
    [CreatedBy]            NVARCHAR (50)   NOT NULL,
    [CreatedDate]          SMALLDATETIME   NOT NULL,
    [ModifiedBy]           NVARCHAR (50)   NULL,
    [ModifiedDate]         SMALLDATETIME   NULL,
    [IBaseId]              NVARCHAR (50)   NULL,
    [RecordStatus]         TINYINT         CONSTRAINT [DF_KeoghsCase_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]      TINYINT         CONSTRAINT [DF_KeoghsCase_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]           ROWVERSION      NOT NULL,
    CONSTRAINT [PK_KeoghsCase] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_KeoghsCase_InsurersClients] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[InsurersClients] ([Id]),
    CONSTRAINT [FK_KeoghsCase_KeoghsCaseStatus] FOREIGN KEY ([CaseStatus_Id]) REFERENCES [dbo].[KeoghsCaseStatus] ([Id]),
    CONSTRAINT [FK_KeoghsCase_KeoghsOffice] FOREIGN KEY ([KeoghsOffice_Id]) REFERENCES [dbo].[KeoghsOffice] ([Id])
);


GO
ALTER TABLE [dbo].[KeoghsCase] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

