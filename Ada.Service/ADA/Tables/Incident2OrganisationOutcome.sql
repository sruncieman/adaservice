﻿CREATE TABLE [dbo].[Incident2OrganisationOutcome] (
    [Id]                       INT             IDENTITY (1, 1) NOT NULL,
    [Incident_Id]              INT             NOT NULL,
    [Organisation_Id]          INT             NOT NULL,
    [OutcomeLinkId]            NVARCHAR (50)   NULL,
    [SettlementDate]           SMALLDATETIME   NULL,
    [MannerOfResolution]       NVARCHAR (100)  NULL,
    [EnforcementRole]          NVARCHAR (100)  NULL,
    [PropertyOwned]            NVARCHAR (20)   NULL,
    [MethodOfEnforcement]      NVARCHAR (100)  NULL,
    [DateOfSettlementAgreed]   SMALLDATETIME   NULL,
    [SettlementAmountAgreed]   NVARCHAR (20)   NULL,
    [AmountRecoveredToDate]    NVARCHAR (20)   NULL,
    [AmountStillOutstanding]   NVARCHAR (20)   NULL,
    [WasEnforcementSuccessful] NVARCHAR (20)   NULL,
    [TypeOfSuccess]            NVARCHAR (100)  NULL,
    [FiveGrading]              NVARCHAR (10)   NULL,
    [LinkConfidence]           INT             DEFAULT ((0)) NOT NULL,
    [Source]                   NVARCHAR (50)   NULL,
    [SourceReference]          NVARCHAR (50)   NULL,
    [SourceDescription]        NVARCHAR (1024) NULL,
    [CreatedBy]                NVARCHAR (50)   NOT NULL,
    [CreatedDate]              SMALLDATETIME   NOT NULL,
    [ModifiedBy]               NVARCHAR (50)   NULL,
    [ModifiedDate]             SMALLDATETIME   NULL,
    [RiskClaim_Id]             INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]         INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]                  NVARCHAR (50)   NULL,
    [RecordStatus]             TINYINT         CONSTRAINT [DF_Incident2OrganisationOutcome_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]          TINYINT         CONSTRAINT [DF_Incident2OrganisationOutcome_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]               ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Incident2OrgOutcome] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Incident2OrganisationOutcome_Incident] FOREIGN KEY ([Incident_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_Incident2OrganisationOutcome_Organisation] FOREIGN KEY ([Organisation_Id]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Incident2OrganisationOutcome_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Incident2OrganisationOutcome] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2OrganisationOutcome_Incident_Id]
    ON [dbo].[Incident2OrganisationOutcome]([Incident_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2OrganisationOutcome_Organisation_Id]
    ON [dbo].[Incident2OrganisationOutcome]([Organisation_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2OrganisationOutcome_RiskClaim_Id]
    ON [dbo].[Incident2OrganisationOutcome]([RiskClaim_Id] ASC);

