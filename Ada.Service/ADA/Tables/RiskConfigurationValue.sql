﻿CREATE TABLE [dbo].[RiskConfigurationValue] (
    [Id]                              INT           IDENTITY (1, 1) NOT NULL,
    [RiskConfigurationDescription_Id] INT           NOT NULL,
    [RiskClient_Id]                   INT           CONSTRAINT [DF_RiskConfigurationValue_RiskClient_Id] DEFAULT ((0)) NOT NULL,
    [ConfigurationValue]              VARCHAR (500) NOT NULL,
    [IsActive]                        BIT           CONSTRAINT [DF_RiskConfigurationValue_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_RiskConfigurationValue_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskConfigurationValue_RiskConfigurationDescription_RiskClient_Id] FOREIGN KEY ([RiskClient_Id]) REFERENCES [dbo].[RiskClient] ([Id]),
    CONSTRAINT [FK_RiskConfigurationValue_RiskConfigurationDescription_RiskConfigurationDescription_Id] FOREIGN KEY ([RiskConfigurationDescription_Id]) REFERENCES [dbo].[RiskConfigurationDescription] ([Id])
);

