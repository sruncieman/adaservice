﻿CREATE TABLE [dbo].[ClaimType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [ClaimType]       NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_ClaimType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_ClaimType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ClaimType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

