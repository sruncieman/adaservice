﻿CREATE TABLE [dbo].[VehicleFuel] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [FuelText]        NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_VehicleFuel_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_VehicleFuel_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_VehicleFuel] PRIMARY KEY CLUSTERED ([Id] ASC)
);

