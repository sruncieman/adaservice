﻿CREATE TABLE [dbo].[RiskSendMeAReportExecution] (
    [Id]                   INT            IDENTITY (1, 1) NOT NULL,
    [RiskSendMeAReport_Id] SMALLINT       NOT NULL,
    [RiskBatch_Id]         INT            NOT NULL,
    [ExecutionDateTime]    DATETIME       CONSTRAINT [DF_RiskSendMeAReportExecution_ExecutionDateTime] DEFAULT (getdate()) NOT NULL,
    [ExecutionDetails]     VARCHAR (5000) NOT NULL,
    [RegenerateReport]     BIT            CONSTRAINT [DF_RiskSendMeAReportExecution_RegenerateReport] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_RiskSendMeAReportExecution_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskSendMeAReportExecution_RiskBatch] FOREIGN KEY ([RiskBatch_Id]) REFERENCES [dbo].[RiskBatch] ([Id]),
    CONSTRAINT [FK_RiskSendMeAReportExecution_RiskSendMeAReport_Id] FOREIGN KEY ([RiskSendMeAReport_Id]) REFERENCES [dbo].[RiskSendMeAReport] ([Id])
);

