﻿CREATE TABLE [dbo].[MatchingRulesPaymentCard] (
    [RuleNo]            INT NOT NULL,
    [RiskClaim_Id]      BIT NULL,
    [PaymentCardNumber] BIT NULL,
    [SortCode]          BIT NULL,
    [BankName]          BIT NULL,
    [ExpiryDate]        BIT NULL,
    [PriorityGroup]     INT NULL,
    [MatchType]         INT NULL,
    CONSTRAINT [PK_MatchingRulesPaymentCard] PRIMARY KEY CLUSTERED ([RuleNo] ASC)
);

