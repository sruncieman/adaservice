﻿CREATE TABLE [dbo].[RiskTemplateStructure] (
    [Id]                   INT           IDENTITY (1, 1) NOT NULL,
    [TemplateStructure_Id] INT           NULL,
    [Name]                 VARCHAR (50)  NULL,
    [Template_Structure]   VARCHAR (MAX) NULL,
    CONSTRAINT [PK_RiskTemplate] PRIMARY KEY CLUSTERED ([Id] ASC)
);

