﻿CREATE TABLE [dbo].[Person2BankAccount] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Person_Id]         INT             NOT NULL,
    [BankAccount_Id]    INT             NOT NULL,
    [AccountLinkId]     NVARCHAR (50)   NULL,
    [Validated]         BIT             NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Person2BankAccount_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Person2BankAccount_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Person2BankAccount] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person2BankAccount_BankAccount] FOREIGN KEY ([BankAccount_Id]) REFERENCES [dbo].[BankAccount] ([Id]),
    CONSTRAINT [FK_Person2BankAccount_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Person2BankAccount_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Person2BankAccount] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Person2BankAccount_Bank_Id]
    ON [dbo].[Person2BankAccount]([BankAccount_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2BankAccount_Person_Id]
    ON [dbo].[Person2BankAccount]([Person_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2BankAccount_RiskClaimId]
    ON [dbo].[Person2BankAccount]([RiskClaim_Id] ASC);

