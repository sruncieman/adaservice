﻿CREATE TABLE [dbo].[Timings] (
    [Id]                    INT      IDENTITY (1, 1) NOT NULL,
    [DeduplicationTimeInMs] INT      NOT NULL,
    [DeduplicationDateTime] DATETIME NULL,
    [DedupeType]            INT      NOT NULL,
    [SyncTimeInMs]          INT      NOT NULL,
    [SyncDateTime]          DATETIME NULL,
    [SyncType]              INT      NOT NULL,
    [BackupTimeInMs]        INT      NOT NULL,
    [BackupDateTime]        DATETIME NULL,
    [BackupType]            INT      NOT NULL,
    CONSTRAINT [PK_Timings] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
ALTER TABLE [dbo].[Timings] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



GO
