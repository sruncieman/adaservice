﻿CREATE TABLE [dbo].[RiskReports] (
    [PkId]            INT                        IDENTITY (1, 1) NOT NULL,
    [Id]              UNIQUEIDENTIFIER           CONSTRAINT [DF_RiskReports_Id] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [Description]     NVARCHAR (64)              NOT NULL,
    [FileData]        VARBINARY (MAX) FILESTREAM NULL,
    [ReturnedData]    VARCHAR (MAX)              NULL,
    [ParentEntity_Id] INT                        NULL,
    CONSTRAINT [PK_RiskReports_PkId] PRIMARY KEY CLUSTERED ([PkId] ASC),
    CONSTRAINT [UQ_RiskReports_ID] UNIQUE NONCLUSTERED ([Id] ASC)
) FILESTREAM_ON [Filestream];



