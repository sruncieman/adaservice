﻿CREATE TABLE [dbo].[SubPartyType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [SubPartyText]    NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_SubPartyType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_SubPartyType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SubPartyType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

