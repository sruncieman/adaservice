﻿CREATE TABLE [dbo].[TimingMetric] (
    [Id]               INT          IDENTITY (1, 1) NOT NULL,
    [EntityType]       VARCHAR (50) NOT NULL,
    [RuleNumber]       INT          NOT NULL,
    [TimingInMs]       INT          NOT NULL,
    [Created]          DATETIME     NOT NULL,
    [CreatedBy]        VARCHAR (50) NOT NULL,
    [RiskClaim_Id]     INT          NOT NULL,
    [BaseRiskClaim_Id] INT          NOT NULL,
    [TimingType]       INT          NOT NULL,
    CONSTRAINT [PK_TimingsMatching] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TimingMetric_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);

