﻿CREATE TABLE [dbo].[PaymentCard] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [PaymentCardId]      NVARCHAR (50)   NULL,
    [PaymentCardNumber]  NVARCHAR (50)   NOT NULL,
    [HashedCardNumber]   VARCHAR (64)    NULL,
    [SortCode]           NVARCHAR (8)    NULL,
    [ExpiryDate]         NVARCHAR (4)    NULL,
    [BankName]           NVARCHAR (50)   NULL,
    [PaymentCardType_Id] INT             NOT NULL,
    [KeyAttractor]       NVARCHAR (100)  NULL,
    [Source]             NVARCHAR (50)   NULL,
    [SourceReference]    NVARCHAR (50)   NULL,
    [SourceDescription]  NVARCHAR (1024) NULL,
    [CreatedBy]          NVARCHAR (50)   NOT NULL,
    [CreatedDate]        SMALLDATETIME   NOT NULL,
    [ModifiedBy]         NVARCHAR (50)   NULL,
    [ModifiedDate]       SMALLDATETIME   NULL,
    [IBaseId]            NVARCHAR (50)   NULL,
    [RecordStatus]       TINYINT         CONSTRAINT [DF_PaymentCard_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]    TINYINT         CONSTRAINT [DF_PaymentCard_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]         ROWVERSION      NOT NULL,
    CONSTRAINT [PK_PaymentCard] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PaymentCard_PaymentCardType] FOREIGN KEY ([PaymentCardType_Id]) REFERENCES [dbo].[PaymentCardType] ([Id])
);


GO
ALTER TABLE [dbo].[PaymentCard] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_PaymentCard_BankName]
    ON [dbo].[PaymentCard]([BankName] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PaymentCard_CardNum]
    ON [dbo].[PaymentCard]([PaymentCardNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PaymentCard_SortCode]
    ON [dbo].[PaymentCard]([SortCode] ASC);

