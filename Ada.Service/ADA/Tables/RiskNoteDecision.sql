﻿CREATE TABLE [dbo].[RiskNoteDecision] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [Decision] VARCHAR (50) NOT NULL,
    [RiskClient_Id] INT NOT NULL, 
    [ADARecordStatus] TINYINT NOT NULL, 
    CONSTRAINT [PK_RiskDecision] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_RiskNoteDecision_RiskClient] FOREIGN KEY ([RiskClient_Id]) REFERENCES [dbo].[RiskClient] ([Id])
);

