﻿CREATE TABLE [dbo].[KeoghsCaseStatus] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [StatusText]      NVARCHAR (20) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_KeoghsCaseStatus_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_KeoghsCaseStatus_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_KeoghsCaseStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

