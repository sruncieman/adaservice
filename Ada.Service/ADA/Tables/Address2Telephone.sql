﻿CREATE TABLE [dbo].[Address2Telephone] (
    [Id]                   INT             IDENTITY (1, 1) NOT NULL,
    [Address_Id]           INT             NOT NULL,
    [Telephone_Id]         INT             NOT NULL,
    [TelephoneLinkId]      NVARCHAR (50)   NULL,
    [TelephoneLinkType_Id] INT             CONSTRAINT [DF_Address2Telephone_TelephoneLinkType_Id] DEFAULT ((0)) NOT NULL,
    [RawAddress]           NVARCHAR (512)  NULL,
    [FiveGrading]          NVARCHAR (10)   NULL,
    [LinkConfidence]       INT             CONSTRAINT [DF__Address2T__LinkC__662B2B3B] DEFAULT ((0)) NOT NULL,
    [Source]               NVARCHAR (50)   NULL,
    [SourceReference]      NVARCHAR (50)   NULL,
    [SourceDescription]    NVARCHAR (1024) NULL,
    [CreatedBy]            NVARCHAR (50)   NOT NULL,
    [CreatedDate]          SMALLDATETIME   NOT NULL,
    [ModifiedBy]           NVARCHAR (50)   NULL,
    [ModifiedDate]         SMALLDATETIME   NULL,
    [RiskClaim_Id]         INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]     INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]              NVARCHAR (50)   NULL,
    [RecordStatus]         TINYINT         CONSTRAINT [DF_Address2Telephone_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]      TINYINT         CONSTRAINT [DF_Address2Telephone_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]           ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Address2Telephone] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Address2Telephone_Address] FOREIGN KEY ([Address_Id]) REFERENCES [dbo].[Address] ([Id]),
    CONSTRAINT [FK_Address2Telephone_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_Address2Telephone_Telephone] FOREIGN KEY ([Telephone_Id]) REFERENCES [dbo].[Telephone] ([Id]),
    CONSTRAINT [FK_Address2Telephone_TelephoneLinkType] FOREIGN KEY ([TelephoneLinkType_Id]) REFERENCES [dbo].[TelephoneLinkType] ([Id])
);


GO
ALTER TABLE [dbo].[Address2Telephone] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Address2Telephone_Address_Id]
    ON [dbo].[Address2Telephone]([Address_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Address2Telephone_RiskClaimId]
    ON [dbo].[Address2Telephone]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Address2Telephone_Telephone_Id]
    ON [dbo].[Address2Telephone]([Telephone_Id] ASC);

