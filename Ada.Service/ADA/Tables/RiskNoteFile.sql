﻿CREATE TABLE [dbo].[RiskNoteFile] (
    [PkId]        INT                        IDENTITY (1, 1) NOT NULL,
    [Id]          UNIQUEIDENTIFIER           DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [RiskNote_ID] INT                        NOT NULL,
    [Description] NVARCHAR (64)              NOT NULL,
    [FileType]    VARCHAR (100)               NOT NULL,
    [FileData]    VARBINARY (MAX) FILESTREAM NULL,
    PRIMARY KEY CLUSTERED ([PkId] ASC),
    CONSTRAINT [FK_RiskNoteFile_RiskNote_Id] FOREIGN KEY ([RiskNote_ID]) REFERENCES [dbo].[RiskNote] ([Id]),
    UNIQUE NONCLUSTERED ([Id] ASC)
) FILESTREAM_ON [Filestream];



