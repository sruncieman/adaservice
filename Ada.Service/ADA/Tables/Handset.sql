﻿CREATE TABLE [dbo].[Handset] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [HandsetIMEI]       NVARCHAR (255)  NULL,
    [HandsetMake]       NVARCHAR (50)   NULL,
    [HandsetModel]      NVARCHAR (50)   NULL,
    [HandsetColour_Id]  INT             NOT NULL,
    [HandsetValue]      MONEY           NULL,
    [KeyAttractor]      NVARCHAR (100)  NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Handset_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Handset_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Handset] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Handset_HandsetColour] FOREIGN KEY ([HandsetColour_Id]) REFERENCES [dbo].[HandsetColour] ([Id])
);


GO
ALTER TABLE [dbo].[Handset] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



