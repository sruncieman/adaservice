﻿CREATE TABLE [dbo].[Person] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [PersonId]          NVARCHAR (50)   NULL,
    [Gender_Id]         INT             NOT NULL,
    [Salutation_Id]     INT             NOT NULL,
    [FirstName]         NVARCHAR (100)  NULL,
    [MiddleName]        NVARCHAR (100)  NULL,
    [LastName]          NVARCHAR (100)  NULL,
    [DateOfBirth]       SMALLDATETIME   NULL,
    [Nationality]       NVARCHAR (50)   NULL,
    [Occupation]        NVARCHAR (255)  NULL,
    [TaxiDriverLicense] NVARCHAR (20)   NULL,
    [Schools]           NVARCHAR (150)  NULL,
    [Hobbies]           NVARCHAR (150)  NULL,
    [Notes]             NVARCHAR (1024) NULL,
    [Documents]         NVARCHAR (1024) NULL,
    [KeyAttractor]      NVARCHAR (100)  NULL,
    [SanctionDate]      SMALLDATETIME   NULL,
    [SanctionSource]    NVARCHAR (50)   NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Person_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Person_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    [SanctionList]      NVARCHAR (500)  NULL,
    CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [FK_Person2Gender] FOREIGN KEY ([Gender_Id]) REFERENCES [dbo].[Gender] ([Id]),
    CONSTRAINT [FK_Person2Salutation] FOREIGN KEY ([Salutation_Id]) REFERENCES [dbo].[Salutation] ([Id])
);





GO
ALTER TABLE [dbo].[Person] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Person2Gender]
    ON [dbo].[Person]([Gender_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Person2Salutation]
    ON [dbo].[Person]([Salutation_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person_201306121503]
    ON [dbo].[Person]([FirstName] ASC, [LastName] ASC, [DateOfBirth] ASC)
    INCLUDE([Id]);


GO
CREATE NONCLUSTERED INDEX [IX_Person_LastName]
    ON [dbo].[Person]([LastName] ASC);

