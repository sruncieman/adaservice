﻿CREATE TABLE [dbo].[HandsetColour] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [Colour]          NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_HandsetColour_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_HandsetColour_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_HandsetColour] PRIMARY KEY CLUSTERED ([Id] ASC)
);

