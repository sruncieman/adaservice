﻿CREATE TABLE [dbo].[OrganisationLinkType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [LinkType]        NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_OrganisationLinkType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_OrganisationLinkType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_OrganisationLinkType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

