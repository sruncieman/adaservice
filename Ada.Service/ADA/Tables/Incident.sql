﻿CREATE TABLE [dbo].[Incident] (
    [Id]                   INT             IDENTITY (1, 1) NOT NULL,
    [IncidentId]           NVARCHAR (50)   NULL,
    [KeoghsEliteReference] NVARCHAR (50)   NULL,
    [IncidentType_Id]      INT             NOT NULL,
    [ClaimType_Id]         INT             NOT NULL,
    [IfbReference]         NVARCHAR (50)   NULL,
    [IncidentDate]         SMALLDATETIME   NOT NULL,
    [FraudRingName]        NVARCHAR (50)   NULL,
    [PaymentsToDate]       MONEY           NULL,
    [Reserve]              MONEY           NULL,
    [KeyAttractor]         NVARCHAR (100)  NULL,
    [Source]               NVARCHAR (50)   NULL,
    [SourceReference]      NVARCHAR (50)   NULL,
    [SourceDescription]    NVARCHAR (1024) NULL,
    [CreatedBy]            NVARCHAR (50)   NOT NULL,
    [CreatedDate]          SMALLDATETIME   NOT NULL,
    [ModifiedBy]           NVARCHAR (50)   NULL,
    [ModifiedDate]         SMALLDATETIME   NULL,
    [IBaseId]              NVARCHAR (50)   NULL,
    [RecordStatus]         TINYINT         CONSTRAINT [DF_Incident_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]      TINYINT         CONSTRAINT [DF_Incident_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]           ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Incident] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Incident_ClaimType] FOREIGN KEY ([ClaimType_Id]) REFERENCES [dbo].[ClaimType] ([Id]),
    CONSTRAINT [FK_Incident_IncidentType] FOREIGN KEY ([IncidentType_Id]) REFERENCES [dbo].[IncidentType] ([Id])
);


GO
ALTER TABLE [dbo].[Incident] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

