﻿CREATE TABLE [dbo].[RiskGroup] (
    [Id]         INT          IDENTITY (1, 1) NOT NULL,
    [Group_Name] VARCHAR (50) NOT NULL,
    [Parent_Id]  INT          NOT NULL,
    [Child_Id]   INT          NOT NULL,
    CONSTRAINT [PK_RiskUserGroup] PRIMARY KEY CLUSTERED ([Id] ASC)
);

