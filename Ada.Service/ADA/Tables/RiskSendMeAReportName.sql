﻿CREATE TABLE [dbo].[RiskSendMeAReportName] (
    [Id]                SMALLINT      IDENTITY (1, 1) NOT NULL,
    [SendMeAReportName] VARCHAR (100) NOT NULL,
    [ADAVisable]        BIT           CONSTRAINT [DF_RiskSendMeAReportName_ADAVisable] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_RiskSendMeAReportName_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

