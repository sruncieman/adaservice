﻿CREATE TABLE [dbo].[TOG] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [TogId]             NVARCHAR (50)   NULL,
    [TogReference]      NVARCHAR (50)   NULL,
    [ReferredBy]        NVARCHAR (50)   NULL,
    [DateOfReview]      SMALLDATETIME   NULL,
    [Decision]          NVARCHAR (20)   NULL,
    [AllocatedTo]       NVARCHAR (50)   NULL,
    [DateAllocated]     SMALLDATETIME   NULL,
    [DateCompleted]     SMALLDATETIME   NULL,
    [Report]            NVARCHAR (250)  NULL,
    [KeyAttractor]      NVARCHAR (100)  NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_TOG_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_TOG_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_TOG] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
ALTER TABLE [dbo].[TOG] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

