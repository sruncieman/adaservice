﻿CREATE TABLE [dbo].[MatchingRulesClaim] (
    [RuleNo]                INT NOT NULL,
    [ClientClaimReference]  BIT NULL,
    [ClientClaimCode]       BIT NULL,
    [ClientID]              BIT NULL,
    [IncidentDate]          BIT NULL,
    [IncidentTime]          BIT NULL,
    [IncidentLocation]      BIT NULL,
    [PeopleMatch]           INT NULL,
    [VehicleMatch]          INT NULL,
    [Involvements]          INT NULL,
    [IncidentCircumstances] BIT NULL,
    [HandsetIMEIMatch]      INT NULL,
    [RiskClient_Id]         INT NULL,
    [PriorityGroup]         INT NULL,
    [MatchType]             INT NULL,
    [RiskClaimFlag]         INT NOT NULL,
    CONSTRAINT [PK_MatchingRulesClaim] PRIMARY KEY CLUSTERED ([RuleNo] ASC),
    CONSTRAINT [FK_MatchingRulesClaim_RiskClient_Id] FOREIGN KEY ([RiskClient_Id]) REFERENCES [dbo].[RiskClient] ([Id])
);









