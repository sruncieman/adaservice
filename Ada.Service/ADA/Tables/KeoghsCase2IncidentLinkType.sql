﻿CREATE TABLE [dbo].[KeoghsCase2IncidentLinkType] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [LinkType]        NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_KeoghsCase2IncidentLinkType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_KeoghsCase2IncidentLinkType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_CaseIncidentLinkType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

