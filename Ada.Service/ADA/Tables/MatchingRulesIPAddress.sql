﻿CREATE TABLE [dbo].[MatchingRulesIPAddress] (
    [RuleNo]        INT NOT NULL,
    [IPAddress]     BIT NULL,
    [PriorityGroup] INT NULL,
    [MatchType]     INT NULL,
    [RiskClaim_Id]  BIT NULL,
    CONSTRAINT [PK_MatchingRulesIPAddress] PRIMARY KEY CLUSTERED ([RuleNo] ASC)
);

