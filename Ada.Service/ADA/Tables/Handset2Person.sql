﻿CREATE TABLE [dbo].[Handset2Person] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [Handset_Id]         INT             NOT NULL,
    [Person_Id]          INT             NOT NULL,
    [HandsetLinkType_Id] INT             NOT NULL,
    [FiveGrading]        NVARCHAR (10)   NULL,
    [LinkConfidence]     INT             CONSTRAINT [DF__Handset2P__LinkC__44160A59] DEFAULT ((0)) NOT NULL,
    [Source]             NVARCHAR (50)   NULL,
    [SourceReference]    NVARCHAR (50)   NULL,
    [SourceDescription]  NVARCHAR (1024) NULL,
    [CreatedBy]          NVARCHAR (50)   NOT NULL,
    [CreatedDate]        SMALLDATETIME   NOT NULL,
    [ModifiedBy]         NVARCHAR (50)   NULL,
    [ModifiedDate]       SMALLDATETIME   NULL,
    [RiskClaim_Id]       INT             CONSTRAINT [DF__Handset2P__RiskC__450A2E92] DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]   INT             CONSTRAINT [DF__Handset2P__BaseR__45FE52CB] DEFAULT ((0)) NOT NULL,
    [IBaseId]            NVARCHAR (50)   NULL,
    [RecordStatus]       TINYINT         CONSTRAINT [DF_Handset2Person_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]    TINYINT         CONSTRAINT [DF_Handset2Person_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]         ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Handset2Person] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Handset2Person_Handset] FOREIGN KEY ([Handset_Id]) REFERENCES [dbo].[Handset] ([Id]),
    CONSTRAINT [FK_Handset2Person_HandsetLinkType] FOREIGN KEY ([HandsetLinkType_Id]) REFERENCES [dbo].[HandsetLinkType] ([Id]),
    CONSTRAINT [FK_Handset2Person_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Handset2Person_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Handset2Person] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



