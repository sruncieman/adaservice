﻿CREATE TABLE [dbo].[InsurersClients] (
    [Id]                     INT           IDENTITY (-1, 1) NOT NULL,
    [ClientName]             NVARCHAR (50) NOT NULL,
    [InsurerClientParent_Id] INT           NULL,
    [RecordStatus]           TINYINT       CONSTRAINT [DF_InsurersClients_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]        TINYINT       CONSTRAINT [DF_InsurersClients_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_InsurersClients] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
ALTER TABLE [dbo].[InsurersClients] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



