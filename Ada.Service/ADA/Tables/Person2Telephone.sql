﻿CREATE TABLE [dbo].[Person2Telephone] (
    [Id]                   INT             IDENTITY (1, 1) NOT NULL,
    [Person_Id]            INT             NOT NULL,
    [Telephone_Id]         INT             NOT NULL,
    [TelephoneLinkId]      NVARCHAR (50)   NULL,
    [TelephoneLinkType_Id] INT             CONSTRAINT [DF_Person2Telephone_TelephoneLinkType_id] DEFAULT ((0)) NOT NULL,
    [Validated]            BIT             NULL,
    [Status]               NVARCHAR (20)   NULL,
    [CurrentLocation]      NVARCHAR (30)   NULL,
    [FiveGrading]          NVARCHAR (10)   NULL,
    [LinkConfidence]       INT             CONSTRAINT [DF__Person2Te__LinkC__0A688BB1] DEFAULT ((0)) NOT NULL,
    [Source]               NVARCHAR (50)   NULL,
    [SourceReference]      NVARCHAR (50)   NULL,
    [SourceDescription]    NVARCHAR (1024) NULL,
    [CreatedBy]            NVARCHAR (50)   NOT NULL,
    [CreatedDate]          SMALLDATETIME   NOT NULL,
    [ModifiedBy]           NVARCHAR (50)   NULL,
    [ModifiedDate]         SMALLDATETIME   NULL,
    [RiskClaim_Id]         INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]     INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]              NVARCHAR (50)   NULL,
    [RecordStatus]         TINYINT         CONSTRAINT [DF_Person2Telephone_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]      TINYINT         CONSTRAINT [DF_Person2Telephone_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]           ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Person2Telephone] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person2Telephone_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Person2Telephone_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_Person2Telephone_Telephone] FOREIGN KEY ([Telephone_Id]) REFERENCES [dbo].[Telephone] ([Id]),
    CONSTRAINT [FK_Person2Telephone_TelephoneLinkType] FOREIGN KEY ([TelephoneLinkType_Id]) REFERENCES [dbo].[TelephoneLinkType] ([Id])
);


GO
ALTER TABLE [dbo].[Person2Telephone] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Telephone_Person_Id]
    ON [dbo].[Person2Telephone]([Person_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Telephone_RiskClaimId]
    ON [dbo].[Person2Telephone]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Telephone_Telephone_Id]
    ON [dbo].[Person2Telephone]([Telephone_Id] ASC);

