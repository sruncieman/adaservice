﻿CREATE TABLE [dbo].[RiskUserADASearchAudit] (
    [Id]               INT             IDENTITY (1, 1) NOT NULL,
    [UserId]           INT             NOT NULL,
    [CreatedDate]      SMALLDATETIME   CONSTRAINT [DF_RiskUserADASearchAudit_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ReportParameters] NVARCHAR (1000) NOT NULL,
    [TotalRowCount]    INT             NOT NULL,
    [CurrentPageNo]    INT             NOT NULL,
    [PageSize]         INT             NOT NULL,
    CONSTRAINT [PK_RiskUserADASearchAudit_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskUserADASearchAudit_RiskUser_ID] FOREIGN KEY ([UserId]) REFERENCES [dbo].[RiskUser] ([Id])
);

