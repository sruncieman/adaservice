﻿CREATE TABLE [dbo].[RiskClient] (
    [Id]                       INT            IDENTITY (1, 1) NOT NULL,
    [ClientName]               NVARCHAR (128) NOT NULL,
    [InsurersClients_Id]       INT            CONSTRAINT [DF_RiskClient_InsurersClients_Id] DEFAULT ((1)) NOT NULL,
    [AmberThreshold]           INT            CONSTRAINT [DF__RiskClien__Amber__408F9238] DEFAULT ((51)) NOT NULL,
    [RedThreshold]             INT            CONSTRAINT [DF__RiskClien__RedTh__4183B671] DEFAULT ((250)) NOT NULL,
    [EmailDomain]              NVARCHAR (50)  NULL,
    [ExpectedFileExtension]    VARCHAR (50)   NULL,
    [LevelOneReportDelay]      INT            CONSTRAINT [DF__RiskClien__Level__4F72AE6C] DEFAULT ((1440)) NOT NULL,
    [LevelTwoReportDelay]      INT            CONSTRAINT [DF__RiskClien__Level__5066D2A5] DEFAULT ((1440)) NOT NULL,
    [ClaimsActivePeriodInDays] INT            CONSTRAINT [DF__RiskClien__Claim__6A3191A0] DEFAULT ((0)) NOT NULL,
    [ScoreClosedClaims]        BIT            CONSTRAINT [DF__RiskClien__Score__77368703] DEFAULT ((0)) NOT NULL,
    [Status]                   INT            NULL,
    [RecordReserveChanges]     BIT            NULL,
    [InternalClient]           INT            NULL,
    [BatchPriority]            INT            NULL,
    CONSTRAINT [PK_RiskClient] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskClient_InsurersClients] FOREIGN KEY ([InsurersClients_Id]) REFERENCES [dbo].[InsurersClients] ([Id])
);








GO
ALTER TABLE [dbo].[RiskClient] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);





