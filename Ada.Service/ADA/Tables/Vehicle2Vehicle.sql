﻿CREATE TABLE [dbo].[Vehicle2Vehicle] (
    [Id]                    INT             IDENTITY (1, 1) NOT NULL,
    [Vehicle1_Id]           INT             NOT NULL,
    [Vehicle2_Id]           INT             NOT NULL,
    [Vehicle2VehicleLinkId] NVARCHAR (50)   NULL,
    [DateOfRegChange]       SMALLDATETIME   NULL,
    [FiveGrading]           NVARCHAR (10)   NULL,
    [LinkConfidence]        INT             DEFAULT ((0)) NOT NULL,
    [Source]                NVARCHAR (50)   NULL,
    [SourceReference]       NVARCHAR (50)   NULL,
    [SourceDescription]     NVARCHAR (1024) NULL,
    [CreatedBy]             NVARCHAR (50)   NOT NULL,
    [CreatedDate]           SMALLDATETIME   NOT NULL,
    [ModifiedBy]            NVARCHAR (50)   NULL,
    [ModifiedDate]          SMALLDATETIME   NULL,
    [IBaseId]               NVARCHAR (50)   NULL,
    [RecordStatus]          TINYINT         CONSTRAINT [DF_Vehicle2Vehicle_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]       TINYINT         CONSTRAINT [DF_Vehicle2Vehicle_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]            ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Vehicle2Vehicle] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Vehicle2Vehicle_Vehicle1] FOREIGN KEY ([Vehicle1_Id]) REFERENCES [dbo].[Vehicle] ([Id]),
    CONSTRAINT [FK_Vehicle2Vehicle_Vehicle2] FOREIGN KEY ([Vehicle2_Id]) REFERENCES [dbo].[Vehicle] ([Id])
);


GO
ALTER TABLE [dbo].[Vehicle2Vehicle] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Vehicle2Vehicle_Vehicle1_Id]
    ON [dbo].[Vehicle2Vehicle]([Vehicle1_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Vehicle2Vehicle_Vehicle2_Id]
    ON [dbo].[Vehicle2Vehicle]([Vehicle2_Id] ASC);

