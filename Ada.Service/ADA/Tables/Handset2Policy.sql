﻿CREATE TABLE [dbo].[Handset2Policy] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Handset_Id]        INT             NOT NULL,
    [Policy_Id]         INT             NOT NULL,
    [PolicyLinkId]      NVARCHAR (50)   NULL,
    [PolicyLinkType_Id] INT             NOT NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Handset2Policy_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Handset2Policy_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Handset2Policy] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Handset2Policy_Handset] FOREIGN KEY ([Handset_Id]) REFERENCES [dbo].[Handset] ([Id]),
    CONSTRAINT [FK_Handset2Policy_Policy] FOREIGN KEY ([Policy_Id]) REFERENCES [dbo].[Policy] ([Id]),
    CONSTRAINT [FK_Handset2Policy_PolicyLinkType] FOREIGN KEY ([PolicyLinkType_Id]) REFERENCES [dbo].[PolicyLinkType] ([Id]),
    CONSTRAINT [FK_Handset2Policy_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Handset2Policy] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



