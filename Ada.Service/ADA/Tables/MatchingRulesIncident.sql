﻿CREATE TABLE [dbo].[MatchingRulesIncident] (
    [RuleNo]              INT NOT NULL,
    [RiskClaim_Id]        BIT NULL,
    [IncidentDate]        BIT NULL,
    [IncidentTime]        BIT NULL,
    [FirstName]           BIT NULL,
    [LastName]            BIT NULL,
    [DateOfBirth]         BIT NULL,
    [VehicleRegistration] BIT NULL,
    [OrganisationName]    BIT NULL,
    [RegisteredNumber]    BIT NULL,
    [VATNumber]           BIT NULL,
    [PriorityGroup]       INT NULL,
    [MatchType]           INT NULL,
    CONSTRAINT [PK_MatchingRulesIncident] PRIMARY KEY CLUSTERED ([RuleNo] ASC)
);

