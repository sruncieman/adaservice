﻿CREATE TABLE [dbo].[KeoghsCase2Incident] (
    [Id]                      INT             IDENTITY (1, 1) NOT NULL,
    [KeoghsCase_Id]           INT             NOT NULL,
    [Incident_Id]             INT             NOT NULL,
    [CaseIncidentLinkId]      NVARCHAR (50)   NULL,
    [CaseIncidentLinkType_Id] INT             NOT NULL,
    [FiveGrading]             NVARCHAR (10)   NULL,
    [LinkConfidence]          INT             CONSTRAINT [DF__KeoghsCas__LinkC__756D6ECB] DEFAULT ((0)) NOT NULL,
    [Source]                  NVARCHAR (50)   NULL,
    [SourceReference]         NVARCHAR (50)   NULL,
    [SourceDescription]       NVARCHAR (1024) NULL,
    [CreatedBy]               NVARCHAR (50)   NOT NULL,
    [CreatedDate]             SMALLDATETIME   NOT NULL,
    [ModifiedBy]              NVARCHAR (50)   NULL,
    [ModifiedDate]            SMALLDATETIME   NULL,
    [RiskClaim_Id]            INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]        INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]                 NVARCHAR (50)   NULL,
    [RecordStatus]            TINYINT         CONSTRAINT [DF_KeoghsCase2Incident_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]         TINYINT         CONSTRAINT [DF_KeoghsCase2Incident_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]              ROWVERSION      NOT NULL,
    CONSTRAINT [PK_KeoghsCase2Incident] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_KeoghsCase2Incident_CaseIncidentLinkType] FOREIGN KEY ([CaseIncidentLinkType_Id]) REFERENCES [dbo].[KeoghsCase2IncidentLinkType] ([Id]),
    CONSTRAINT [FK_KeoghsCase2Incident_Incident] FOREIGN KEY ([Incident_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_KeoghsCase2Incident_KeoghsCase] FOREIGN KEY ([KeoghsCase_Id]) REFERENCES [dbo].[KeoghsCase] ([Id]),
    CONSTRAINT [FK_KeoghsCase2Incident_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[KeoghsCase2Incident] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_KeoghsCase2Incident_Incident_Id]
    ON [dbo].[KeoghsCase2Incident]([Incident_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_KeoghsCase2Incident_KeoghsCase_Id]
    ON [dbo].[KeoghsCase2Incident]([KeoghsCase_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_KeoghsCase2Incident_RiskClaimId]
    ON [dbo].[KeoghsCase2Incident]([RiskClaim_Id] ASC);

