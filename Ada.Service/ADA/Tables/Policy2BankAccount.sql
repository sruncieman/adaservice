﻿CREATE TABLE [dbo].[Policy2BankAccount] (
    [Id]                      INT             IDENTITY (1, 1) NOT NULL,
    [Policy_Id]               INT             NOT NULL,
    [BankAccount_Id]          INT             NOT NULL,
    [PolicyPaymentLinkId]     NVARCHAR (50)   NULL,
    [PolicyPaymentType_Id]    INT             CONSTRAINT [DF_Policy2BankAccount_PremiumPaymentType_Id] DEFAULT ((0)) NOT NULL,
    [DatePaymentDetailsTaken] SMALLDATETIME   NULL,
    [FiveGrading]             NVARCHAR (10)   NULL,
    [LinkConfidence]          INT             CONSTRAINT [DF__Policy2Ba__LinkC__0E391C95] DEFAULT ((0)) NOT NULL,
    [Source]                  NVARCHAR (50)   NULL,
    [SourceReference]         NVARCHAR (50)   NULL,
    [SourceDescription]       NVARCHAR (1024) NULL,
    [CreatedBy]               NVARCHAR (50)   NOT NULL,
    [CreatedDate]             SMALLDATETIME   NOT NULL,
    [ModifiedBy]              NVARCHAR (50)   NULL,
    [ModifiedDate]            SMALLDATETIME   NULL,
    [RiskClaim_Id]            INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]        INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]                 NVARCHAR (50)   NULL,
    [RecordStatus]            TINYINT         CONSTRAINT [DF_Policy2BankAccount_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]         TINYINT         CONSTRAINT [DF_Policy2BankAccount_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]              ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Policy2BankAccount] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Policy2BankAccount_BankAccount] FOREIGN KEY ([BankAccount_Id]) REFERENCES [dbo].[BankAccount] ([Id]),
    CONSTRAINT [FK_Policy2BankAccount_Policy] FOREIGN KEY ([Policy_Id]) REFERENCES [dbo].[Policy] ([Id]),
    CONSTRAINT [FK_Policy2BankAccount_PolicyPaymentType] FOREIGN KEY ([PolicyPaymentType_Id]) REFERENCES [dbo].[PolicyPaymentType] ([Id]),
    CONSTRAINT [FK_Policy2BankAccount_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Policy2BankAccount] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Policy2BankAccount_Bank_Id]
    ON [dbo].[Policy2BankAccount]([BankAccount_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Policy2BankAccount_Policy_Id]
    ON [dbo].[Policy2BankAccount]([Policy_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Policy2BankAccount_RiskClaimId]
    ON [dbo].[Policy2BankAccount]([RiskClaim_Id] ASC);

