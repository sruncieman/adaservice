﻿CREATE TABLE [dbo].[Incident2HandsetLinkType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [Text]            NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_Incident2HandsetLinkType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_Incident2HandsetLinkType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Incident2HandsetLinkType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

