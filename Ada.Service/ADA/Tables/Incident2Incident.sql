﻿CREATE TABLE [dbo].[Incident2Incident] (
    [Id]                  INT             IDENTITY (1, 1) NOT NULL,
    [Incident1_Id]        INT             NOT NULL,
    [Incident2_Id]        INT             NOT NULL,
    [LinkType]            INT             DEFAULT ((0)) NOT NULL,
    [IncidentMatchLinkId] NVARCHAR (50)   NULL,
    [FiveGrading]         NVARCHAR (10)   NULL,
    [LinkConfidence]      INT             DEFAULT ((0)) NOT NULL,
    [Source]              NVARCHAR (50)   NULL,
    [SourceReference]     NVARCHAR (50)   NULL,
    [SourceDescription]   NVARCHAR (1024) NULL,
    [CreatedBy]           NVARCHAR (50)   NOT NULL,
    [CreatedDate]         SMALLDATETIME   NOT NULL,
    [ModifiedBy]          NVARCHAR (50)   NULL,
    [ModifiedDate]        SMALLDATETIME   NULL,
    [IBaseId]             NVARCHAR (50)   NULL,
    [RecordStatus]        TINYINT         CONSTRAINT [DF_Incident2Incident_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]     TINYINT         CONSTRAINT [DF_Incident2Incident_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]          ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Incident2Incident] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Incident2Incident_Incident1] FOREIGN KEY ([Incident1_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_Incident2Incident_Incident2] FOREIGN KEY ([Incident2_Id]) REFERENCES [dbo].[Incident] ([Id])
);


GO
ALTER TABLE [dbo].[Incident2Incident] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Incident_Incident1_Id]
    ON [dbo].[Incident2Incident]([Incident1_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Incident_Incident2_Id]
    ON [dbo].[Incident2Incident]([Incident2_Id] ASC);

