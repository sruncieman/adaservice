﻿CREATE TABLE [dbo].[PolicyPaymentType] (
    [Id]              INT          IDENTITY (1, 1) NOT NULL,
    [Text]            VARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT      CONSTRAINT [DF_PolicyPaymentType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT      CONSTRAINT [DF_PolicyPaymentType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PremiumPaymentType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

