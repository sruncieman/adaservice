﻿CREATE TABLE [dbo].[RiskConfigurationDescription] (
    [Id]                       INT           IDENTITY (1, 1) NOT NULL,
    [ConfigurationDescription] VARCHAR (200) NOT NULL,
    CONSTRAINT [PK_RiskConfigurationDescription_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

