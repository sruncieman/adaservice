﻿CREATE TABLE [dbo].[Organisation2Email] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Organisation_Id]   INT             NOT NULL,
    [Email_Id]          INT             NOT NULL,
    [EmailLinkId]       NVARCHAR (50)   NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Organisation2Email_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Organisation2Email_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Organisation2Email] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Organisation2Email_Email] FOREIGN KEY ([Email_Id]) REFERENCES [dbo].[Email] ([Id]),
    CONSTRAINT [FK_Organisation2Email_Organisation] FOREIGN KEY ([Organisation_Id]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Organisation2Email_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Organisation2Email] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Email_Email_Id]
    ON [dbo].[Organisation2Email]([Email_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Email_Organisation_Id]
    ON [dbo].[Organisation2Email]([Organisation_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation2Email_RiskClaimId]
    ON [dbo].[Organisation2Email]([RiskClaim_Id] ASC);

