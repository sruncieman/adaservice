﻿CREATE TABLE [dbo].[VehicleType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [Text]            NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_VehicleType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_VehicleType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_VehicleType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

