﻿CREATE TABLE [dbo].[Address] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [AddressId]         NVARCHAR (50)   NULL,
    [SubBuilding]       NVARCHAR (50)   NULL,
    [Building]          NVARCHAR (50)   NULL,
    [BuildingNumber]    NVARCHAR (50)   NULL,
    [Street]            NVARCHAR (255)  NULL,
    [Locality]          NVARCHAR (50)   NULL,
    [Town]              NVARCHAR (50)   NULL,
    [County]            NVARCHAR (50)   NULL,
    [PostCode]          NVARCHAR (20)   NULL,
    [DxNumber]          NVARCHAR (50)   NULL,
    [DxExchange]        NVARCHAR (150)  NULL,
    [GridX]             NVARCHAR (50)   NULL,
    [GridY]             NVARCHAR (50)   NULL,
    [PafValidation]     INT             CONSTRAINT [DF_Address_PafValidation] DEFAULT ((-1)) NOT NULL,
    [PafUPRN]           NVARCHAR (20)   NULL,
    [DocumentLink]      NVARCHAR (256)  NULL,
    [KeyAttractor]      NVARCHAR (100)  NULL,
    [PropertyType]      NVARCHAR (20)   NULL,
    [MosaicCode]        NVARCHAR (20)   NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [AddressType_Id]    INT             NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Address_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Address_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    [AddressSearch]     AS              (CONVERT([nvarchar](500),upper(replace(replace(replace(((((isnull(nullif([SubBuilding],'')+' ','')+isnull(nullif([Building],'')+' ',''))+isnull(nullif([BuildingNumber],'')+' ',''))+isnull(nullif([Street],'')+' ',''))+isnull(nullif([Locality],'')+' ',''))+isnull(nullif([Town],'')+' ',''),'-',''),',',''),'.','')),(0))) PERSISTED,
    CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AddressTypeAddress] FOREIGN KEY ([AddressType_Id]) REFERENCES [dbo].[AddressType] ([Id])
);








GO
ALTER TABLE [dbo].[Address] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Address_Locality]
    ON [dbo].[Address]([Locality] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Address_PafUPRN]
    ON [dbo].[Address]([PafUPRN] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Address_PostCodeBldNum]
    ON [dbo].[Address]([PostCode] ASC, [BuildingNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Address_PostCodeBldSubBld]
    ON [dbo].[Address]([PostCode] ASC, [Building] ASC, [SubBuilding] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Address_PostCodeSt]
    ON [dbo].[Address]([PostCode] ASC, [Street] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Address_Street]
    ON [dbo].[Address]([Street] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Address_Town]
    ON [dbo].[Address]([Town] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_AddressADARecordStatus_NC]
    ON [dbo].[Address]([ADARecordStatus] ASC);
