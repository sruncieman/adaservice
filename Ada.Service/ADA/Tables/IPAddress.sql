﻿CREATE TABLE [dbo].[IPAddress] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [IPAddressId]       NVARCHAR (50)   NULL,
    [IPAddress]         NVARCHAR (20)   NULL,
    [KeyAttractor]      NVARCHAR (100)  NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_IPAddress_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_IPAddress_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_IPAddress] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
ALTER TABLE [dbo].[IPAddress] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_IPAddress_Addr]
    ON [dbo].[IPAddress]([IPAddress] ASC);

