﻿CREATE TABLE [dbo].[OrganisationType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [TypeText]        NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_OrganisationType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_OrganisationType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_OrganisationType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

