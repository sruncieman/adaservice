﻿CREATE TABLE [dbo].[PolicyCoverType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [CoverText]       NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_PolicyCoverType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_PolicyCoverType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PolicyCoverType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

