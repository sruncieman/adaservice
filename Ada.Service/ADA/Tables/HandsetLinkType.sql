﻿CREATE TABLE [dbo].[HandsetLinkType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [LinkText]        NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_HandsetLinkType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_HandsetLinkType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_HandsetLinkType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

