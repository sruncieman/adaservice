﻿CREATE TABLE [dbo].[OrganisationStatus] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [StatusText]      NVARCHAR (50) NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_OrganisationStatus_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_OrganisationStatus_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_OrganisationStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

