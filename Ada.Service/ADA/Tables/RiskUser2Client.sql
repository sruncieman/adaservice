﻿CREATE TABLE [dbo].[RiskUser2Client] (
    [Id]            INT IDENTITY (1, 1) NOT NULL,
    [RiskUser_Id]   INT NOT NULL,
    [RiskClient_Id] INT NOT NULL,
    CONSTRAINT [PK_RiskUser2Client] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskUser2Client_RiskClient] FOREIGN KEY ([RiskClient_Id]) REFERENCES [dbo].[RiskClient] ([Id]),
    CONSTRAINT [FK_RiskUser2Client_RiskUser] FOREIGN KEY ([RiskUser_Id]) REFERENCES [dbo].[RiskUser] ([Id])
);

