﻿CREATE TABLE [dbo].[Person2DrivingLicense] (
    [Id]                   INT             IDENTITY (1, 1) NOT NULL,
    [Person_Id]            INT             NOT NULL,
    [DrivingLicense_Id]    INT             NOT NULL,
    [DrivingLicenseLinkId] NVARCHAR (50)   NULL,
    [Validated]            BIT             NULL,
    [FiveGrading]          NVARCHAR (10)   NULL,
    [LinkConfidence]       INT             DEFAULT ((0)) NOT NULL,
    [Source]               NVARCHAR (50)   NULL,
    [SourceReference]      NVARCHAR (50)   NULL,
    [SourceDescription]    NVARCHAR (1024) NULL,
    [CreatedBy]            NVARCHAR (50)   NOT NULL,
    [CreatedDate]          SMALLDATETIME   NOT NULL,
    [ModifiedBy]           NVARCHAR (50)   NULL,
    [ModifiedDate]         SMALLDATETIME   NULL,
    [RiskClaim_Id]         INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]     INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]              NVARCHAR (50)   NULL,
    [RecordStatus]         TINYINT         CONSTRAINT [DF_Person2DrivingLicense_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]      TINYINT         CONSTRAINT [DF_Person2DrivingLicense_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]           ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Person2DrivingLicense] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person2DrivingLicense_DrivingLicense] FOREIGN KEY ([DrivingLicense_Id]) REFERENCES [dbo].[DrivingLicense] ([Id]),
    CONSTRAINT [FK_Person2DrivingLicense_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Person2DrivingLicense_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Person2DrivingLicense] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Person2DrivingLicense_License_Id]
    ON [dbo].[Person2DrivingLicense]([DrivingLicense_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2DrivingLicense_Person_Id]
    ON [dbo].[Person2DrivingLicense]([Person_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2DrivingLicense_RiskClaimId]
    ON [dbo].[Person2DrivingLicense]([RiskClaim_Id] ASC);

