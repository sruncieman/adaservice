﻿CREATE TABLE [dbo].[Person2Organisation] (
    [Id]                             INT             IDENTITY (1, 1) NOT NULL,
    [Person_Id]                      INT             NOT NULL,
    [Organisation_Id]                INT             NOT NULL,
    [Person2OrganisationLinkId]      NVARCHAR (50)   NULL,
    [Person2OrganisationLinkType_Id] INT             NOT NULL,
    [FiveGrading]                    NVARCHAR (10)   NULL,
    [AppointmentDate]                SMALLDATETIME   NULL,
    [ResignedDate]                   SMALLDATETIME   NULL,
    [LinkConfidence]                 INT             DEFAULT ((0)) NOT NULL,
    [Source]                         NVARCHAR (50)   NULL,
    [SourceReference]                NVARCHAR (50)   NULL,
    [SourceDescription]              NVARCHAR (1024) NULL,
    [CreatedBy]                      NVARCHAR (50)   NOT NULL,
    [CreatedDate]                    SMALLDATETIME   NOT NULL,
    [ModifiedBy]                     NVARCHAR (50)   NULL,
    [ModifiedDate]                   SMALLDATETIME   NULL,
    [RiskClaim_Id]                   INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]               INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]                        NVARCHAR (50)   NULL,
    [RecordStatus]                   TINYINT         CONSTRAINT [DF_Person2Organisation_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]                TINYINT         CONSTRAINT [DF_Person2Organisation_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]                     ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Person2Organisation] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Person2Organisation_Organisation] FOREIGN KEY ([Organisation_Id]) REFERENCES [dbo].[Organisation] ([Id]),
    CONSTRAINT [FK_Person2Organisation_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Person2Organisation_PersonOrganisationLinkType] FOREIGN KEY ([Person2OrganisationLinkType_Id]) REFERENCES [dbo].[Person2OrganisationLinkType] ([Id]),
    CONSTRAINT [FK_Person2Organisation_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Person2Organisation] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Organisation_Organisation_Id]
    ON [dbo].[Person2Organisation]([Organisation_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Organisation_Person_Id]
    ON [dbo].[Person2Organisation]([Person_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Person2Organisation_RiskClaimId]
    ON [dbo].[Person2Organisation]([RiskClaim_Id] ASC);

