﻿CREATE TABLE [dbo].[RiskTeam] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [TeamName]      NVARCHAR (250) NOT NULL,
    [RiskClient_Id] INT            NOT NULL,
    CONSTRAINT [PK_RiskTeam] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskTeam_RiskClient] FOREIGN KEY ([RiskClient_Id]) REFERENCES [dbo].[RiskClient] ([Id])
);

