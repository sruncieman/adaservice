﻿CREATE TABLE [dbo].[VehicleCategoryOfLoss] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [CatText]         NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_VehicleCategoryOfLoss_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_VehicleCategoryOfLoss_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_VehicleCategoryOfLoss] PRIMARY KEY CLUSTERED ([Id] ASC)
);

