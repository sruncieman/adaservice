﻿CREATE TABLE [dbo].[MatchingRulesNINumber] (
    [RuleNo]        INT NOT NULL,
    [RiskClaim_Id]  BIT NULL,
    [NINumber]      BIT NULL,
    [PriorityGroup] INT NULL,
    [MatchType]     INT NULL,
    CONSTRAINT [PK_MatchingRulesNINumber] PRIMARY KEY CLUSTERED ([RuleNo] ASC)
);

