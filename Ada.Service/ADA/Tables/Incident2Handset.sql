﻿CREATE TABLE [dbo].[Incident2Handset] (
    [Id]                          INT             IDENTITY (1, 1) NOT NULL,
    [Incident_Id]                 INT             NOT NULL,
    [Handset_Id]                  INT             NOT NULL,
    [Incident2HandsetLinkType_Id] INT             NOT NULL,
    [HandsetValueCategory]        NVARCHAR (50)   NULL,
    [DeviceFault]                 NVARCHAR (250)  NULL,
    [FiveGrading]                 NVARCHAR (10)   NULL,
    [LinkConfidence]              INT             CONSTRAINT [DF__Incident2__LinkC__74794A93] DEFAULT ((0)) NOT NULL,
    [Source]                      NVARCHAR (50)   NULL,
    [SourceReference]             NVARCHAR (50)   NULL,
    [SourceDescription]           NVARCHAR (1024) NULL,
    [CreatedBy]                   NVARCHAR (50)   NOT NULL,
    [CreatedDate]                 SMALLDATETIME   NOT NULL,
    [ModifiedBy]                  NVARCHAR (50)   NULL,
    [ModifiedDate]                SMALLDATETIME   NULL,
    [RiskClaim_Id]                INT             CONSTRAINT [DF__Incident2__RiskC__5A054B78] DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]            INT             CONSTRAINT [DF__Incident2__BaseR__5AF96FB1] DEFAULT ((0)) NOT NULL,
    [IBaseId]                     NVARCHAR (50)   NULL,
    [RecordStatus]                TINYINT         CONSTRAINT [DF_Incident2Handset_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]             TINYINT         CONSTRAINT [DF_Incident2Handset_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]                  ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Incident2Handset] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Incident2Handset_Handset] FOREIGN KEY ([Handset_Id]) REFERENCES [dbo].[Handset] ([Id]),
    CONSTRAINT [FK_Incident2Handset_Incident] FOREIGN KEY ([Incident_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_Incident2Handset_Incident2HandsetLinkType] FOREIGN KEY ([Incident2HandsetLinkType_Id]) REFERENCES [dbo].[Incident2HandsetLinkType] ([Id]),
    CONSTRAINT [FK_Incident2Handset_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Incident2Handset] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



