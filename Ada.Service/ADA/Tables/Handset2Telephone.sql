﻿CREATE TABLE [dbo].[Handset2Telephone] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Handset_Id]        INT             NOT NULL,
    [Telephone_Id]      INT             NOT NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             CONSTRAINT [DF__Handset2Te__LinkC__0A688BB1] DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [RiskClaim_Id]      INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]  INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Handset2Telephone_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Handset2Telephone_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Handset2Telephone] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Handset2Telephone_Handset] FOREIGN KEY ([Handset_Id]) REFERENCES [dbo].[Handset] ([Id]),
    CONSTRAINT [FK_Handset2Telephone_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_Handset2Telephone_Telephone] FOREIGN KEY ([Telephone_Id]) REFERENCES [dbo].[Telephone] ([Id])
);


GO
ALTER TABLE [dbo].[Handset2Telephone] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



