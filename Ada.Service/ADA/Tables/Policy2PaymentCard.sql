﻿CREATE TABLE [dbo].[Policy2PaymentCard] (
    [Id]                      INT             IDENTITY (1, 1) NOT NULL,
    [Policy_Id]               INT             NOT NULL,
    [PaymentCard_Id]          INT             NOT NULL,
    [PolicyPaymentLinkId]     NVARCHAR (50)   NULL,
    [PolicyPaymentType_Id]    INT             CONSTRAINT [DF_Policy2PaymentCard_PremiumPaymentType_Id] DEFAULT ((0)) NOT NULL,
    [DatePaymentDetailsTaken] SMALLDATETIME   NULL,
    [FiveGrading]             NVARCHAR (10)   NULL,
    [LinkConfidence]          INT             CONSTRAINT [DF__Policy2Pa__LinkC__0F2D40CE] DEFAULT ((0)) NOT NULL,
    [Source]                  NVARCHAR (50)   NULL,
    [SourceReference]         NVARCHAR (50)   NULL,
    [SourceDescription]       NVARCHAR (1024) NULL,
    [CreatedBy]               NVARCHAR (50)   NOT NULL,
    [CreatedDate]             SMALLDATETIME   NOT NULL,
    [ModifiedBy]              NVARCHAR (50)   NULL,
    [ModifiedDate]            SMALLDATETIME   NULL,
    [RiskClaim_Id]            INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]        INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]                 NVARCHAR (50)   NULL,
    [RecordStatus]            TINYINT         CONSTRAINT [DF_Policy2PaymentCard_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]         TINYINT         CONSTRAINT [DF_Policy2PaymentCard_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]              ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Policy2PaymentCard] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Policy2PaymentCard_PaymentCard] FOREIGN KEY ([PaymentCard_Id]) REFERENCES [dbo].[PaymentCard] ([Id]),
    CONSTRAINT [FK_Policy2PaymentCard_Policy] FOREIGN KEY ([Policy_Id]) REFERENCES [dbo].[Policy] ([Id]),
    CONSTRAINT [FK_Policy2PaymentCard_PolicyPaymentType] FOREIGN KEY ([PolicyPaymentType_Id]) REFERENCES [dbo].[PolicyPaymentType] ([Id]),
    CONSTRAINT [FK_Policy2PaymentCard_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Policy2PaymentCard] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Policy2PaymentCard_Card_Id]
    ON [dbo].[Policy2PaymentCard]([PaymentCard_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Policy2PaymentCard_Policy_Id]
    ON [dbo].[Policy2PaymentCard]([Policy_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Policy2PaymentCard_RiskClaimId]
    ON [dbo].[Policy2PaymentCard]([RiskClaim_Id] ASC);

