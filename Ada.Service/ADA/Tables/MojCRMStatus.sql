﻿CREATE TABLE [dbo].[MojCRMStatus] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [Status]          NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_MojCRMStatus_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_MojCRMStatus_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MojCRMStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

