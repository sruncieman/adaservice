﻿CREATE TABLE [dbo].[Handset2Handset] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Handset1_Id]       INT             NOT NULL,
    [Handset2_Id]       INT             NOT NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_Handset2Handset_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_Handset2Handset_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Handset2Handset] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Handset2Handset_Handset1] FOREIGN KEY ([Handset1_Id]) REFERENCES [dbo].[Handset] ([Id]),
    CONSTRAINT [FK_Handset2Handset_Handset2] FOREIGN KEY ([Handset2_Id]) REFERENCES [dbo].[Handset] ([Id])
);


GO
ALTER TABLE [dbo].[Handset2Handset] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



