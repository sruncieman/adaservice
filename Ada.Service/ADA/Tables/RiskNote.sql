﻿CREATE TABLE [dbo].[RiskNote] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [BaseRiskClaim_Id] INT            NOT NULL,
    [RiskClaim_Id]     INT            NOT NULL,
    [Note]             VARCHAR (2000) NULL,
    [CreatedDate]      DATETIME       CONSTRAINT [DF_RiskNote_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        VARCHAR (50)   NOT NULL,
    [DecisionId]       INT            NULL,
    [Visibility]       INT            NULL,
    [Deleted]          BIT            CONSTRAINT [DF_RiskNote_Deleted] DEFAULT ((0)) NOT NULL,
    [UserType]         INT            NULL,
    [UpdatedDate]      DATETIME       NULL,
    [UpdatedBy]        VARCHAR (50)   NULL,
    CONSTRAINT [PK_RiskNote] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskNote_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_RiskNote_RiskNoteDecision] FOREIGN KEY ([DecisionId]) REFERENCES [dbo].[RiskNoteDecision] ([Id])
);







