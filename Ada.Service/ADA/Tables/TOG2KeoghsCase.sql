﻿CREATE TABLE [dbo].[TOG2KeoghsCase] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [TOG_Id]            INT             NOT NULL,
    [KeoghsCase_Id]     INT             NOT NULL,
    [TogLinkId]         NVARCHAR (50)   NULL,
    [Notes]             NVARCHAR (500)  NULL,
    [FiveGrading]       NVARCHAR (10)   NULL,
    [LinkConfidence]    INT             DEFAULT ((0)) NOT NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_TOG2KeoghsCase_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_TOG2KeoghsCase_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_TOG2KeoghsCase_1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TOG2KeoghsCase_KeoghsCase] FOREIGN KEY ([KeoghsCase_Id]) REFERENCES [dbo].[KeoghsCase] ([Id]),
    CONSTRAINT [FK_TOG2KeoghsCase_TOG] FOREIGN KEY ([TOG_Id]) REFERENCES [dbo].[TOG] ([Id])
);


GO
ALTER TABLE [dbo].[TOG2KeoghsCase] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

