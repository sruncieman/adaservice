﻿CREATE TABLE [dbo].[RiskUser] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [RiskRole_Id]       INT            CONSTRAINT [DF__RiskUser__RiskRo__5A856B86] DEFAULT ((0)) NOT NULL,
    [RiskGroup_Id]      INT            NULL,
    [UserName]          VARCHAR (50)   NULL,
    [Password]          NVARCHAR (128) NULL,
    [RiskTeam_Id]       INT            CONSTRAINT [DF__RiskUser__RiskTe__5B798FBF] DEFAULT ((0)) NOT NULL,
    [RiskClient_Id]     INT            CONSTRAINT [DF__RiskUser__RiskCl__5C6DB3F8] DEFAULT ((0)) NOT NULL,
    [FirstName]         VARCHAR (50)   NULL,
    [LastName]          VARCHAR (50)   NULL,
    [TelephoneNumber]   VARCHAR (16)   NULL,
    [LastGoodLoginDate] SMALLDATETIME  NULL,
    [BadLoginCount]     INT            CONSTRAINT [DF__RiskUser__BadLog__5D61D831] DEFAULT ((0)) NOT NULL,
    [Status]            INT            NULL,
    [AutoLoader]        BIT            NULL,
    [UserType]          INT            NULL,
    [Token]             VARCHAR (50)   NULL,
    [TokenExpiry]       SMALLDATETIME  NULL,
    [PasswordResetDate] SMALLDATETIME  NULL,
    CONSTRAINT [PK__RiskUser__3214EC272FCF1A8A] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskUser_RiskGroup] FOREIGN KEY ([RiskGroup_Id]) REFERENCES [dbo].[RiskGroup] ([Id]),
    CONSTRAINT [FK_RiskUser_RiskTeam] FOREIGN KEY ([RiskTeam_Id]) REFERENCES [dbo].[RiskTeam] ([Id]),
    CONSTRAINT [FK_RiskUser_ToRiskClient] FOREIGN KEY ([RiskClient_Id]) REFERENCES [dbo].[RiskClient] ([Id]),
    CONSTRAINT [FK_RiskUser_ToRiskRole] FOREIGN KEY ([RiskRole_Id]) REFERENCES [dbo].[RiskRole] ([Id])
);





