﻿CREATE TABLE [dbo].[PolicyLinkType] (
    [Id]              INT           IDENTITY (-1, 1) NOT NULL,
    [LinkText]        NVARCHAR (50) NOT NULL,
    [RecordStatus]    TINYINT       CONSTRAINT [DF_PolicyLinkType_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus] TINYINT       CONSTRAINT [DF_PolicyLinkType_DeletedReason] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PolicyLinkType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

