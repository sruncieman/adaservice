﻿CREATE TABLE [dbo].[Handset2Address] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [Handset_Id]         INT             NOT NULL,
    [Address_Id]         INT             NOT NULL,
    [AddressLinkId]      NVARCHAR (50)   NULL,
    [AddressLinkType_Id] INT             NOT NULL,
    [StartOfResidency]   SMALLDATETIME   NULL,
    [EndOfResidency]     SMALLDATETIME   NULL,
    [RawAddress]         NVARCHAR (512)  NULL,
    [Notes]              NVARCHAR (1024) NULL,
    [FiveGrading]        NVARCHAR (10)   NULL,
    [LinkConfidence]     INT             DEFAULT ((0)) NOT NULL,
    [Source]             NVARCHAR (50)   NULL,
    [SourceReference]    NVARCHAR (50)   NULL,
    [SourceDescription]  NVARCHAR (1024) NULL,
    [CreatedBy]          NVARCHAR (50)   NOT NULL,
    [CreatedDate]        SMALLDATETIME   NOT NULL,
    [ModifiedBy]         NVARCHAR (50)   NULL,
    [ModifiedDate]       SMALLDATETIME   NULL,
    [RiskClaim_Id]       INT             DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]   INT             DEFAULT ((0)) NOT NULL,
    [IBaseId]            NVARCHAR (50)   NULL,
    [RecordStatus]       TINYINT         CONSTRAINT [DF_Handset2Address_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]    TINYINT         CONSTRAINT [DF_Handset2Address_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]         ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Handset2Address] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Handset2Address_Address] FOREIGN KEY ([Address_Id]) REFERENCES [dbo].[Address] ([Id]),
    CONSTRAINT [FK_Handset2Address_AddressLinkType] FOREIGN KEY ([AddressLinkType_Id]) REFERENCES [dbo].[AddressLinkType] ([Id]),
    CONSTRAINT [FK_Handset2Address_Handset] FOREIGN KEY ([Handset_Id]) REFERENCES [dbo].[Handset] ([Id]),
    CONSTRAINT [FK_Handset2Address_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id])
);


GO
ALTER TABLE [dbo].[Handset2Address] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



