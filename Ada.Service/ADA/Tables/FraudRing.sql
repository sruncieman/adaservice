﻿CREATE TABLE [dbo].[FraudRing] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [FraudRingId]       NVARCHAR (50)   NULL,
    [FraudRingName]     NVARCHAR (50)   NULL,
    [Status]            NVARCHAR (10)   NULL,
    [LeadAnalyst]       NVARCHAR (50)   NULL,
    [Champion]          NVARCHAR (50)   NULL,
    [BriefingDocument]  NVARCHAR (255)  NULL,
    [KeyAttractor]      NVARCHAR (100)  NULL,
    [Source]            NVARCHAR (50)   NULL,
    [SourceReference]   NVARCHAR (50)   NULL,
    [SourceDescription] NVARCHAR (1024) NULL,
    [CreatedBy]         NVARCHAR (50)   NOT NULL,
    [CreatedDate]       SMALLDATETIME   NOT NULL,
    [ModifiedBy]        NVARCHAR (50)   NULL,
    [ModifiedDate]      SMALLDATETIME   NULL,
    [IBaseId]           NVARCHAR (50)   NULL,
    [RecordStatus]      TINYINT         CONSTRAINT [DF_FraudRing_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]   TINYINT         CONSTRAINT [DF_FraudRing_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    CONSTRAINT [PK_FraudRing] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
ALTER TABLE [dbo].[FraudRing] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

