﻿CREATE TABLE [dbo].[RiskSendMeAReport] (
    [Id]                       SMALLINT       IDENTITY (1, 1) NOT NULL,
    [RiskSendMeAReportName_Id] SMALLINT       NOT NULL,
    [RiskClient_Id]            INT            NOT NULL,
    [IsActive]                 BIT            NOT NULL,
    [RiskRunExceedScore]       INT            NULL,
    [DestinationFileLocation]  VARCHAR (200)  NULL,
    [DestinationFileName]      VARCHAR (200)  NULL,
    [DestinationFileFormat]    VARCHAR (10)   NULL,
    [EmailSubject]             VARCHAR (500)  NULL,
    [EmailBody_WithData]       VARCHAR (5000) NULL,
    [EmailBody_WithoutData]    VARCHAR (5000) NULL,
    [DatabaseMailProfile]      VARCHAR (50)   NULL,
    [EmailTo]                  VARCHAR (200)  NULL,
    [EmailCC]                  VARCHAR (200)  NULL,
    [EMailBCC]                 VARCHAR (200)  NULL,
    [IgnoreBatchIdsTo]         INT            NULL,
    CONSTRAINT [PK_RiskSendMeAReport_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RiskSendMeAReport_RiskClient_Id] FOREIGN KEY ([RiskClient_Id]) REFERENCES [dbo].[RiskClient] ([Id]),
    CONSTRAINT [FK_RiskSendMeAReport_RiskSendMeAReportName_Id] FOREIGN KEY ([RiskSendMeAReportName_Id]) REFERENCES [dbo].[RiskSendMeAReportName] ([Id])
);

