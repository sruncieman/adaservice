﻿CREATE TABLE [dbo].[Address2Address] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [Address1_Id]        INT             NOT NULL,
    [Address2_Id]        INT             NOT NULL,
    [AddressLinkId]      NVARCHAR (50)   NULL,
    [AddressLinkType_Id] INT             NOT NULL,
    [StartOfResidency]   SMALLDATETIME   NULL,
    [EndOfResidency]     SMALLDATETIME   NULL,
    [Notes]              NVARCHAR (1024) NULL,
    [FiveGrading]        NVARCHAR (10)   NULL,
    [LinkConfidence]     INT             DEFAULT ((0)) NOT NULL,
    [Source]             NVARCHAR (50)   NULL,
    [SourceReference]    NVARCHAR (50)   NULL,
    [SourceDescription]  NVARCHAR (1024) NULL,
    [CreatedBy]          NVARCHAR (50)   NOT NULL,
    [CreatedDate]        SMALLDATETIME   NOT NULL,
    [ModifiedBy]         NVARCHAR (50)   NULL,
    [ModifiedDate]       SMALLDATETIME   NULL,
    [IBaseId]            NVARCHAR (50)   NULL,
    [RecordStatus]       TINYINT         CONSTRAINT [DF_Address2Address_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]    TINYINT         CONSTRAINT [DF_Address2Address_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]         ROWVERSION      NOT NULL,
    CONSTRAINT [PK_Address2Address] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Address2Address_Address] FOREIGN KEY ([Address1_Id]) REFERENCES [dbo].[Address] ([Id]),
    CONSTRAINT [FK_Address2Address_Address1] FOREIGN KEY ([Address2_Id]) REFERENCES [dbo].[Address] ([Id]),
    CONSTRAINT [FK_Address2Address_AddressLinkType] FOREIGN KEY ([AddressLinkType_Id]) REFERENCES [dbo].[AddressLinkType] ([Id])
);


GO
ALTER TABLE [dbo].[Address2Address] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Address2Address_Address1_Id]
    ON [dbo].[Address2Address]([Address1_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Address2Address_Address2_Id]
    ON [dbo].[Address2Address]([Address2_Id] ASC);

