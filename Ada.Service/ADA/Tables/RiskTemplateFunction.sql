﻿CREATE TABLE [dbo].[RiskTemplateFunction] (
    [Id]                    INT           IDENTITY (1, 1) NOT NULL,
    [RiskRole_Id]           INT           NOT NULL,
    [TemplateStructure_Id]  INT           NOT NULL,
    [TemplateFunctionsJson] VARCHAR (MAX) NOT NULL,
    [Name]                  VARCHAR (50)  NOT NULL,
    [Value]                 VARCHAR (50)  NULL,
    [CreatedBy]             VARCHAR (50)  NOT NULL,
    [ModifiedDate]          DATETIME      NOT NULL,
    CONSTRAINT [PK_RiskTemplateFunction] PRIMARY KEY CLUSTERED ([Id] ASC)
);









