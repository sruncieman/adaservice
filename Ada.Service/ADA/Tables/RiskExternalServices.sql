﻿CREATE TABLE [dbo].[RiskExternalServices] (
    [Id]                INT           IDENTITY (1, 1) NOT NULL,
    [ServiceName]       VARCHAR (30)  NOT NULL,
    [MaskValue]         INT           NOT NULL,
    [CurrentStatus]     INT           DEFAULT ((0)) NOT NULL,
    [StatusMessage]     VARCHAR (50)  NULL,
    [ServiceEnabled]    BIT           DEFAULT ((0)) NOT NULL,
    [StatusLastChecked] SMALLDATETIME NULL,
    [StatusTimeout]     INT           DEFAULT ((10)) NOT NULL,
    CONSTRAINT [PK_RiskExternalServices] PRIMARY KEY CLUSTERED ([Id] ASC)
);

