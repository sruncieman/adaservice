﻿CREATE TABLE [dbo].[Incident2Person] (
    [Id]                       INT             IDENTITY (1, 1) NOT NULL,
    [Incident_Id]              INT             NOT NULL,
    [Person_Id]                INT             NOT NULL,
    [IncidentLinkId]           NVARCHAR (50)   NULL,
    [PartyType_Id]             INT             NOT NULL,
    [SubPartyType_Id]          INT             NOT NULL,
    [MojStatus_Id]             INT             CONSTRAINT [DF_Incident2Person_MojStatus_Id] DEFAULT ((0)) NOT NULL,
    [IncidentTime]             TIME (7)        NULL,
    [IncidentCircs]            NVARCHAR (1024) NULL,
    [IncidentLocation]         NVARCHAR (255)  NULL,
    [Insurer]                  NVARCHAR (255)  NULL,
    [ClaimNumber]              NVARCHAR (50)   NULL,
    [ClaimStatus_Id]           INT             CONSTRAINT [DF_Incident2Person_ClaimStatus] DEFAULT ((0)) NOT NULL,
    [ClaimType]                NVARCHAR (50)   NULL,
    [ClaimCode]                NVARCHAR (80)   NULL,
    [ClaimNotificationDate]    SMALLDATETIME   NULL,
    [SourceClaimStatus]        NVARCHAR (50)   NULL,
    [PaymentsToDate]           MONEY           NULL,
    [Reserve]                  MONEY           NULL,
    [TotalClaimCost]           MONEY           NULL,
    [TotalClaimCostLessExcess] MONEY           NULL,
    [BypassFraud]              BIT             NULL,
    [Broker]                   NVARCHAR (255)  NULL,
    [ReferralSource]           NVARCHAR (255)  NULL,
    [Solicitors]               NVARCHAR (255)  NULL,
    [Engineer]                 NVARCHAR (255)  NULL,
    [Recovery]                 NVARCHAR (255)  NULL,
    [RecoveryAddress]          NVARCHAR (255)  NULL,
    [Storage]                  NVARCHAR (255)  NULL,
    [StorageAddress]           NVARCHAR (255)  NULL,
    [Repairer]                 NVARCHAR (255)  NULL,
    [RepairerAddress]          NVARCHAR (225)  NULL,
    [Hire]                     NVARCHAR (255)  NULL,
    [AccidentManagement]       NVARCHAR (255)  NULL,
    [MedicalExaminer]          NVARCHAR (255)  NULL,
    [MedicalLegal]             NVARCHAR (255)  NULL,
    [InspectionAddress]        NVARCHAR (255)  NULL,
    [UndefinedSupplier]        NVARCHAR (255)  NULL,
    [PoliceAttended]           BIT             NULL,
    [PoliceForce]              NVARCHAR (50)   NULL,
    [PoliceReference]          NVARCHAR (50)   NULL,
    [AmbulanceAttended]        BIT             NULL,
    [AttendedHospital]         BIT             NULL,
    [FiveGrading]              NVARCHAR (10)   NULL,
    [LinkConfidence]           INT             CONSTRAINT [DF__tmp_ms_xx__LinkC__01F34141] DEFAULT ((0)) NOT NULL,
    [Source]                   NVARCHAR (50)   NULL,
    [SourceReference]          NVARCHAR (50)   NULL,
    [SourceDescription]        NVARCHAR (1024) NULL,
    [CreatedBy]                NVARCHAR (50)   NOT NULL,
    [CreatedDate]              SMALLDATETIME   NOT NULL,
    [ModifiedBy]               NVARCHAR (50)   NULL,
    [ModifiedDate]             SMALLDATETIME   NULL,
    [RiskClaim_Id]             INT             CONSTRAINT [DF__Incident2__RiskC__6399A2AA] DEFAULT ((0)) NOT NULL,
    [BaseRiskClaim_Id]         INT             CONSTRAINT [DF__Incident2__BaseR__648DC6E3] DEFAULT ((0)) NOT NULL,
    [IBaseId]                  NVARCHAR (50)   NULL,
    [RecordStatus]             TINYINT         CONSTRAINT [DF_Incident2Person_RecordStatus] DEFAULT ((0)) NOT NULL,
    [ADARecordStatus]          TINYINT         CONSTRAINT [DF_Incident2Person_DeletedReason] DEFAULT ((0)) NOT NULL,
    [RowVersion]               ROWVERSION      NOT NULL,
    [ClaimNumberCalcuated]     AS              (replace([ClaimNumber],'GIOS','')) PERSISTED,
    [ClaimCodeCalcuated]       AS              (replace([ClaimCode],'GIOS','')) PERSISTED,
    CONSTRAINT [PK_Incident2Person] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Incident2Person_ClaimStatus] FOREIGN KEY ([ClaimStatus_Id]) REFERENCES [dbo].[ClaimStatus] ([Id]),
    CONSTRAINT [FK_Incident2Person_Incident] FOREIGN KEY ([Incident_Id]) REFERENCES [dbo].[Incident] ([Id]),
    CONSTRAINT [FK_Incident2Person_MojStatus] FOREIGN KEY ([MojStatus_Id]) REFERENCES [dbo].[MojStatus] ([Id]),
    CONSTRAINT [FK_Incident2Person_PartyType] FOREIGN KEY ([PartyType_Id]) REFERENCES [dbo].[PartyType] ([Id]),
    CONSTRAINT [FK_Incident2Person_Person] FOREIGN KEY ([Person_Id]) REFERENCES [dbo].[Person] ([Id]),
    CONSTRAINT [FK_Incident2Person_RiskClaim] FOREIGN KEY ([RiskClaim_Id]) REFERENCES [dbo].[RiskClaim] ([Id]),
    CONSTRAINT [FK_Incident2Person_SubPartyType] FOREIGN KEY ([SubPartyType_Id]) REFERENCES [dbo].[SubPartyType] ([Id])
);








GO
ALTER TABLE [dbo].[Incident2Person] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






GO



GO
CREATE NONCLUSTERED INDEX [IX_Incident2Person_Incident_Id]
    ON [dbo].[Incident2Person]([Incident_Id] ASC, [ADARecordStatus] ASC) WITH (FILLFACTOR = 80);




GO
CREATE NONCLUSTERED INDEX [IX_Incident2Person_Person_Id]
    ON [dbo].[Incident2Person]([Person_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Person_RiskClaimId]
    ON [dbo].[Incident2Person]([RiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_Incident2Person_insurer]
    ON [dbo].[Incident2Person]([Insurer] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_Incident2Person_ClaimNumber]
    ON [dbo].[Incident2Person]([ClaimNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_Incident2Person_ClaimCode]
    ON [dbo].[Incident2Person]([ClaimCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Person_Id_ADARecordStatus_BaseRiskClaimID_NC]
    ON [dbo].[Incident2Person]([Id] ASC, [ADARecordStatus] ASC, [BaseRiskClaim_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Person_ClaimNumberCalcuated_NC]
    ON [dbo].[Incident2Person]([ClaimNumberCalcuated] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Incident2Person_ClaimCodeCalcuated_NC]
    ON [dbo].[Incident2Person]([ClaimCodeCalcuated] ASC);

