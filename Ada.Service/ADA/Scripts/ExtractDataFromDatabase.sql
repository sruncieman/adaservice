﻿--RunThisScriptToScriptOutAllData.
--There must be a clustered index on the table
--Note compound primary key are ignored


DECLARE  @QualifiedTableName	 VARCHAR(220)
		,@TableObjectID			 INT
		,@PrimaryKeyColumn		 VARCHAR(200)
		,@MergeTable			 NVARCHAR(MAX)
		,@DataSource			 NVARCHAR(MAX)
		,@MergeJoin				 NVARCHAR(MAX)
		,@UpdateStatement		 NVARCHAR(MAX)
		,@InsertStatement		 NVARCHAR(MAX)
		,@InsertValueStatement	 NVARCHAR(MAX)
		,@FullSQLStatement		 NVARCHAR(MAX)
		,@InsertColumnList		 NVARCHAR(MAX)
		,@TableLoopCount		 INT
		,@RowLoopCount			 INT
		,@TableLoopCounter		 INT = 1
		,@RowLoopCounter		 INT = 1
		,@DynamicRowLoopCountSQL NVARCHAR(MAX)
		,@DynamicRowQuery		 NVARCHAR(MAX)
		,@UniqueKey				 VARCHAR(200)

IF OBJECT_ID('tempdb..#TableListLoop') IS NOT NULL
	DROP TABLE #TableListLoop    
CREATE TABLE #TableListLoop (RowID INT IDENTITY(1,1) PRIMARY KEY, SchemaName VARCHAR(20) NOT NULL, TableName VARCHAR(200) NOT NULL, TableObjectID INT NOT NULL, QualifiedTableName VARCHAR(220) NOT NULL, PrimaryKeyColumn VARCHAR(200) NOT NULL)

IF OBJECT_ID('tempdb..#RowListLoop') IS NOT NULL
	DROP TABLE #RowListLoop   
CREATE TABLE #RowListLoop (RowID INT IDENTITY(1,1) PRIMARY KEY, UniqueKey VARCHAR(50) NOT NULL)

IF OBJECT_ID('tempdb..#SQLExecute') IS NOT NULL
	DROP TABLE #SQLExecute 
CREATE TABLE #SQLExecute (RowID INT IDENTITY(1,1) PRIMARY KEY, SQLStatement NVARCHAR(MAX))

---------------------------------------
--Populate#TableListLoopForAlltablesWhichWillHaveDataFor
---------------------------------------
;WITH CTE_TableList AS
	(
	SELECT OBJECT_SCHEMA_NAME(p.object_id) SchemaName, OBJECT_NAME(i.object_id) TableName, i.object_id TableObjectID, QUOTENAME(OBJECT_SCHEMA_NAME(p.object_id)) + '.' + QUOTENAME(OBJECT_NAME(i.object_id)) QualifiedTableName, COL_NAME(ic.OBJECT_ID,ic.column_id) AS PrimaryKeyColumn
	FROM sys.partitions p 
	INNER JOIN sys.indexes i ON p.object_id = i.object_id AND p.index_id = i.index_id
	INNER JOIN sys.index_columns AS ic ON  i.OBJECT_ID = ic.OBJECT_ID AND i.index_id = ic.index_id
	WHERE OBJECT_SCHEMA_NAME(p.object_id) = 'dbo'
	AND i.is_primary_key = 1
	AND rows > 0 
	AND OBJECT_NAME(i.object_id) NOT IN ('__RefactorLog','sysdiagrams','MatchingRulesPerson_TestData')
	)

INSERT INTO #TableListLoop (SchemaName, TableName, TableObjectID, QualifiedTableName, PrimaryKeyColumn)
SELECT SchemaName, TableName, TableObjectID, QualifiedTableName, PrimaryKeyColumn
FROM CTE_TableList A
WHERE EXISTS (SELECT TOP 1 1 FROM CTE_TableList B WHERE A.TableName = B.TableName GROUP BY TableName HAVING COUNT(1) = 1)
ORDER BY TableName;
SELECT @TableLoopCount = @@ROWCOUNT

INSERT INTO #SQLExecute (SQLStatement)
SELECT 'USE MDA' UNION ALL
SELECT 'SET NOCOUNT ON' UNION ALL
SELECT ''

WHILE @TableLoopCount >= @TableLoopCounter
BEGIN
	SELECT   @QualifiedTableName = QualifiedTableName
			,@TableObjectID		 = TableObjectID
			,@PrimaryKeyColumn	 = PrimaryKeyColumn	
	FROM #TableListLoop
	WHERE RowID = @TableLoopCounter

	INSERT INTO #SQLExecute (SQLStatement)
	SELECT 'PRINT ''UPDATING TABLE ' + @QualifiedTableName + '...'''
	INSERT INTO #SQLExecute (SQLStatement)
	SELECT 'SET IDENTITY_INSERT ' + @QualifiedTableName + ' ON' WHERE EXISTS (SELECT TOP 1 1 FROM sys.columns C WHERE C.object_id = @TableObjectID AND Is_identity = 1)

	SELECT @MergeTable			 = 'MERGE ' + @QualifiedTableName + ' AS T USING ('
	SELECT @MergeJoin			 = ') AS S ON S.' + C.name + ' = T.' + C.name + ' WHEN MATCHED THEN UPDATE SET ' FROM sys.columns C WHERE C.object_id = @TableObjectID AND name = @PrimaryKeyColumn
	SELECT @UpdateStatement		 = ISNULL(@UpdateStatement,'') + 'T.' + QUOTENAME(Name) + ' = S.' + + QUOTENAME(Name) +', ' FROM Sys.columns WHERE object_id = @TableObjectID AND name != @PrimaryKeyColumn AND system_type_id NOT IN (189)
	SELECT @UpdateStatement		 = LEFT(@UpdateStatement,LEN(@UpdateStatement) -1) + ' WHEN NOT MATCHED BY TARGET THEN INSERT ('
	SELECT @InsertStatement		 = ISNULL(@InsertStatement,'') + QUOTENAME(Name) + ', ' FROM Sys.columns WHERE object_id = @TableObjectID AND system_type_id NOT IN (189)
	SELECT @InsertStatement		 = LEFT(@InsertStatement,LEN(@InsertStatement) -1) + ') VALUES ('
	SELECT @InsertValueStatement = ISNULL(@InsertValueStatement,'') + 'S.'+ QUOTENAME(Name) + ', ' FROM Sys.columns WHERE object_id = @TableObjectID AND system_type_id NOT IN (189)
	SELECT @InsertValueStatement = LEFT(@InsertValueStatement,LEN(@InsertValueStatement) -1) + ');'

	SELECT @DynamicRowLoopCountSQL = 'SELECT ' + c.name + ' FROM ' + @QualifiedTableName FROM sys.columns C WHERE C.object_id = @TableObjectID AND name = @PrimaryKeyColumn
	INSERT INTO #RowListLoop (UniqueKey)
	EXECUTE SP_EXECUTESQL @DynamicRowLoopCountSQL
	SELECT @RowLoopCount = @@ROWCOUNT

	WHILE @RowLoopCount >= @RowLoopCounter
	BEGIN
		SELECT @UniqueKey = UniqueKey FROM #RowListLoop WHERE RowID = @RowLoopCounter

		SELECT  @DynamicRowQuery = ISNULL(@DynamicRowQuery,'') + ' ISNULL(CAST(REPLACE('+QUOTENAME(Name)+','''''''',''<~#~>'') AS VARCHAR(MAX)),'''') + '''''' AS ' + QUOTENAME(Name) + ', '''''' + '  FROM Sys.columns WHERE object_id = @TableObjectID AND system_type_id NOT IN (189)
		SELECT  @DynamicRowQuery = 'SELECT @DynamicRowQueryOutput = ' + LEFT(@DynamicRowQuery,LEN(@DynamicRowQuery) -7) + '''' + ' FROM ' + @QualifiedTableName + ' WHERE ' + QUOTENAME(Name) + ' = ' + @UniqueKey FROM sys.columns C WHERE C.object_id = @TableObjectID AND name = @PrimaryKeyColumn

		EXECUTE SP_EXECUTESQL @DynamicRowQuery, N'@DynamicRowQueryOutput VARCHAR(MAX) OUTPUT', @DataSource OUTPUT
		SELECT @DataSource = 'SELECT ''' + (@DataSource)

		INSERT INTO #SQLExecute (SQLStatement)
		SELECT @MergeTable + @DataSource + @MergeJoin + @UpdateStatement + @InsertStatement + @InsertValueStatement

		SELECT   @RowLoopCounter += 1
				,@DynamicRowQuery = ''
				,@DataSource = ''
	END

	INSERT INTO #SQLExecute (SQLStatement)
	SELECT 'SET IDENTITY_INSERT ' + @QualifiedTableName + ' OFF' WHERE EXISTS (SELECT TOP 1 1 FROM sys.columns C WHERE C.object_id = @TableObjectID AND Is_identity = 1)
	INSERT INTO #SQLExecute (SQLStatement)
	SELECT ''

	SELECT   @TableLoopCounter		+= 1
			,@RowLoopCounter		= 1
			,@QualifiedTableName	= ''
			,@TableObjectID			= ''
			,@MergeTable			= ''
			,@MergeJoin				= ''
			,@UpdateStatement		= ''
			,@UpdateStatement		= ''
			,@InsertStatement		= ''
			,@InsertStatement		= ''
			,@InsertValueStatement	= ''
			,@InsertValueStatement	= ''

	TRUNCATE TABLE #RowListLoop
	
END

UPDATE #SQLExecute
SET SQLStatement = REPLACE(SQLStatement,'''''','NULL')

UPDATE #SQLExecute
SET SQLStatement = REPLACE(SQLStatement,'<~#~>','''''')

SELECT SQLStatement FROM #SQLExecute ORDER BY RowID