﻿CREATE TABLE [dbo].[RiskUserAudit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[IPAddress] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[SuccessfulLogin] [smallint] NOT NULL,
 CONSTRAINT [PK_RiskUserAudit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO