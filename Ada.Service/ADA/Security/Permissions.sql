﻿GRANT UPDATE
    ON SCHEMA::[dbo] TO [ADAServiceRole];


GO
GRANT SELECT
    ON SCHEMA::[dbo] TO [ADAServiceRole];


GO
GRANT INSERT
    ON SCHEMA::[dbo] TO [ADAServiceRole];


GO
GRANT EXECUTE
    ON SCHEMA::[dbo] TO [ADAServiceRole];


GO
GRANT DELETE
    ON SCHEMA::[dbo] TO [ADAServiceRole];

