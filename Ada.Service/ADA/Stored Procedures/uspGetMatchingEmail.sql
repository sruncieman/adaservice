﻿
CREATE PROCEDURE [dbo].[uspGetMatchingEmail]
	
	@RiskClaim_Id INT  = NULL,
	@EmailAddress VARCHAR(50)  = NULL,
	@FieldsFlag BIT OUTPUT 

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @FieldsFlag = 1
SELECT Id, 0 RuleNo, 1 PriorityGroup, 0 MatchType 
FROM dbo.Email 
WHERE ADARecordStatus = 0 AND EmailAddress = @EmailAddress
GROUP BY Id