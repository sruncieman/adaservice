﻿
CREATE PROCEDURE [dbo].[uspAddressLinkedSettledCaseCountMessages]
 /**************************************************************************************************/
-- ObjectName:		MDA.dbo.uspAddressLinkedSettledCaseCountMessages
--
-- Description:		Fraud Risk Address Rules, modified from uspPersonLinkedSettledCaseCountMessages 
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-10-03			Paul Allen		Created, TFS Item 8303
--		2016-03-01			Paul Allen		Updated to use Date Variables
/**************************************************************************************************/
(
 @PersonIds					VARCHAR(1000)		= '5'
,@includeThisPersonId		BIT					= 1
,@IncludeConfirmed			BIT					= 1
,@IncludeUnconfirmed		BIT					= 0
,@IncludeTentative			BIT					= 0
,@StartDate					SMALLDATETIME		= '2000-01-01'
,@EndDate					SMALLDATETIME		= '2020-01-01'
,@OutcomeCategoryId			INT					= 2
,@ClaimTypeId				INT					= 19
,@IsPotentialClaimant		BIT					= NULL
 )
	 
AS
SET NOCOUNT ON

--------------------------------------------------
--DeclareVariablesETC
--------------------------------------------------
DECLARE  @PersonTree				NVARCHAR(1000)
		,@IsPotentialClaimantWhere	NVARCHAR(100)
		,@Where						NVARCHAR(500)
		,@Sql						NVARCHAR(3000)
		,@PersonSplit				XML				=	(SELECT CAST('<A>' + REPLACE(@PersonIds,',','</A><A>') + '</A>' AS XML))
		,@PersonId					INT
		,@Counter					INT				= 1
		,@RowToProcess				INT
		
DECLARE @PersonTable	TABLE (ID INT, PersonID INT)
DECLARE @ResultsTable	TABLE (Incident_ID INT, Person_ID INT,ThisPartyType NVARCHAR(50),ThisSubPartyType NVARCHAR(50),LinkedSalutation NVARCHAR(20),LinkedFirstName NVARCHAR(100),LinkedLastName NVARCHAR(1000),LinkedDateOfBirth SMALLDATETIME,LinkedPartyType NVARCHAR(50),LinkedSubPartyType NVARCHAR(50),LinkedIncidentDate SMALLDATETIME, LinkedClaimType NVARCHAR(50),LinkedEliteReference NVARCHAR(50),LinkedFiveGrading NVARCHAR(10),OtherPartyType NVARCHAR(50),OtherSubPartyType NVARCHAR(50),OtherSalutation NVARCHAR(20),OtherFirstname NVARCHAR(100),OtherLastName NVARCHAR(100),OtherDoB SMALLDATETIME,OtherMoR NVARCHAR(100),OtherFiveGrading NVARCHAR(10))
DECLARE @Tbl TmpTbl

--SplitPersonIDsIntoTable
INSERT INTO @PersonTable (PersonID, ID)
SELECT	 PersonID = t.value('.', 'NVARCHAR(50)')
		,ID = ROW_NUMBER() OVER (ORDER BY t.value('count(.)', 'bigint'))
FROM @PersonSplit.nodes('/A') AS x (t)
SELECT @RowToProcess = @@ROWCOUNT

WHILE @RowToProcess >= @Counter
BEGIN
	SELECT @PersonId = (SELECT PersonID FROM @PersonTable WHERE ID = @Counter)
	SET @PersonTree					= 'SELECT Id FROM dbo.fn_GetPersonTree(' + CONVERT(NVARCHAR(50) ,@personId) + ', ' + CONVERT(NVARCHAR(50), @includeConfirmed) + ', ' + CONVERT(NVARCHAR(50), @includeUnconfirmed) + ', ' + CONVERT(NVARCHAR(50), @includeTentative) + ', ' + CONVERT(NVARCHAR(50), @includeThisPersonId) + ') '
	SET	@IsPotentialClaimantWhere	= ''

	IF @IsPotentialClaimant IS NOT NULL
	SET @IsPotentialClaimantWhere = ' AND PC.Flag = ' + CONVERT(CHAR(1), @IsPotentialClaimant)

	SET @Where	=  CASE WHEN @ClaimTypeId < 0 THEN  'WHERE ClaimType_Id !=  ' + CONVERT(NVARCHAR(50) ,ABS(@ClaimTypeId)) ELSE CASE WHEN @ClaimTypeId >= 0 THEN  'WHERE ClaimType_Id = ' + CONVERT(NVARCHAR(50) ,@ClaimTypeId) ELSE '' END END
	SET	@Sql	= ' SELECT Incident_ID 
										FROM Incident2Person MOR
										INNER JOIN PotentialClaimant PC ON MOR.SubPartyType_Id = PC.SubPartyType AND MOR.PartyType_Id = PC.PartyType 
										WHERE Person_Id IN	(
															SELECT Person_Id 
															FROM Incident2Person MOR 
															WHERE (Person_Id IN(' + @PersonTree + ')) 
															)	
										' + @IsPotentialClaimantWhere
	DELETE @Tbl
	INSERT INTO @Tbl (Id)
	EXECUTE sp_executesql @Sql
		
	IF (@ClaimTypeId >= 0)
	INSERT INTO @ResultsTable (Incident_ID,Person_ID,LinkedSalutation,LinkedFirstName,LinkedLastName,LinkedDateOfBirth,LinkedPartyType,LinkedSubPartyType,LinkedIncidentDate,LinkedClaimType,LinkedEliteReference,LinkedFiveGrading,OtherPartyType,OtherSubPartyType,OtherSalutation,OtherFirstname,OtherLastName,OtherDoB,OtherMoR,OtherFiveGrading)
	SELECT	 A.Incident_ID
			,A.Person_Id
			,LinkedSalutation
			,LinkedFirstName
			,LinkedLastName
			,LinkedDateOfBirth
			,LinkedPartyType
			,LinkedSubPartyType
			,LinkedIncidentDate
			,LinkedClaimType
			,LinkedEliteReference
			,LinkedFiveGrading
			,OtherPartyType
			,OtherSubPartyType
			,OtherSalutation
			,OtherFirstname
			,OtherLastName 
			,OtherDoB
			,OtherMoR
			,OtherFiveGrading
	FROM		(
				SELECT	 DISTINCT Salutation LinkedSalutation
						,FirstName LinkedFirstName
						,LastName LinkedLastName
						,DateOfBirth LinkedDateOfBirth
						,PartyTypeText LinkedPartyType 
						,SubPartyText LinkedSubPartyType
						,IncidentDate LinkedIncidentDate 
						,ClaimType  LinkedClaimType
						,KeoghsEliteReference  LinkedEliteReference
						,Incident2PersonOutcome_FiveGrading  LinkedFiveGrading
						,Incident_ID
						,Person_Id
				FROM dbo.vPersonLinkedSettledCaseMessages A
				LEFT JOIN dbo.fn_GetPersonTree(@personId, @includeConfirmed, @includeUnconfirmed, @includeTentative, @includeThisPersonId) PT ON A.Person_Id = PT.Id
				WHERE Incident_ID IN (SELECT id from @Tbl)  
				AND Person_Id = PT.Id
				AND ISNULL(CategoryTypeId,1) != 0 --NotFraud
				AND A.IncidentDate >= @StartDate
				AND A.IncidentDate <= @EndDate

				) A
	INNER JOIN	(
				SELECT	DISTINCT PartyTypeText OtherPartyType
						,SubPartyText OtherSubPartyType
						,Salutation OtherSalutation
						,FirstName OtherFirstname
						,LastName OtherLastName
						,DateOfBirth OtherDoB
						,IncidentDate OtherIncidentDate
						,ClaimType  OtherClaimType
						,KeoghsEliteReference  OtherEliteReference
						,Incident2PersonOutcome_FiveGrading  OtherFiveGrading
						,MannerOfResolution  OtherMoR
						,Incident_ID
				FROM  dbo.vPersonLinkedSettledCaseMessages A
				LEFT JOIN dbo.fn_GetPersonTree(@personId, @includeConfirmed, @includeUnconfirmed, @includeTentative, @includeThisPersonId) PT ON A.Person_Id = PT.Id
				WHERE Incident_ID IN (SELECT id from @Tbl)  
				AND Person_Id != ISNULL(PT.Id, 0)
 				AND CategoryTypeId = @OutcomeCategoryId
				AND ClaimType_Id = @ClaimTypeId
				) B ON A.Incident_ID = B.Incident_ID
	ELSE 
	INSERT INTO @ResultsTable (Incident_ID,Person_ID,LinkedSalutation,LinkedFirstName,LinkedLastName,LinkedDateOfBirth,LinkedPartyType,LinkedSubPartyType,LinkedIncidentDate,LinkedClaimType,LinkedEliteReference,LinkedFiveGrading,OtherPartyType,OtherSubPartyType,OtherSalutation,OtherFirstname,OtherLastName,OtherDoB,OtherMoR,OtherFiveGrading)
	SELECT   A.Incident_ID
			,A.Person_Id 
			,LinkedSalutation
			,LinkedFirstName 
			,LinkedLastName 
			,LinkedDateOfBirth
			,LinkedPartyType 
			,LinkedSubPartyType
			,LinkedIncidentDate
			,LinkedClaimType
			,LinkedEliteReference
			,LinkedFiveGrading
			,OtherPartyType
			,OtherSubPartyType
			,OtherSalutation
			,OtherFirstname
			,OtherLastName
			,OtherDoB
			,OtherMoR
			,OtherFiveGrading
	FROM		(
				SELECT DISTINCT Salutation LinkedSalutation
					  ,FirstName LinkedFirstName
					  ,LastName LinkedLastName
					  ,DateOfBirth LinkedDateOfBirth
					  ,PartyTypeText LinkedPartyType
					  ,SubPartyText LinkedSubPartyType 
					  ,IncidentDate LinkedIncidentDate 
					  ,ClaimType  LinkedClaimType
					  ,KeoghsEliteReference  LinkedEliteReference
					  ,Incident2PersonOutcome_FiveGrading  LinkedFiveGrading
					  ,Incident_ID
					  ,Person_Id 
				FROM dbo.vPersonLinkedSettledCaseMessages A
				LEFT JOIN dbo.fn_GetPersonTree(@personId, @includeConfirmed, @includeUnconfirmed, @includeTentative, @includeThisPersonId) PT ON A.Person_Id = PT.Id
				WHERE Incident_ID IN (SELECT id from @Tbl)  
				AND Person_Id = PT.Id
				AND ISNULL(CategoryTypeId,1) != 0 --NotFraud
				AND A.IncidentDate >= @StartDate
				AND A.IncidentDate <= @EndDate
				) A
	INNER JOIN	(
				SELECT	 DISTINCT PartyTypeText OtherPartyType
						,SubPartyText OtherSubPartyType
						,Salutation OtherSalutation
						,FirstName OtherFirstname 
						,LastName OtherLastName
						,DateOfBirth OtherDoB
						,IncidentDate OtherIncidentDate 
						,ClaimType  OtherClaimType
						,KeoghsEliteReference  OtherEliteReference
						,Incident2PersonOutcome_FiveGrading  OtherFiveGrading
						,MannerOfResolution  OtherMoR
						,Incident_ID
				FROM dbo.vPersonLinkedSettledCaseMessages A
				LEFT JOIN dbo.fn_GetPersonTree(@personId, @includeConfirmed, @includeUnconfirmed, @includeTentative, @includeThisPersonId) PT ON A.Person_Id = PT.Id
				WHERE Incident_ID IN (SELECT id from @Tbl)  
				AND Person_Id != ISNULL(PT.Id, 0)
 				AND CategoryTypeId = @OutcomeCategoryId
				AND ClaimType_Id != ABS(@ClaimTypeId)
				) B ON A.Incident_ID = B.Incident_ID
	
	SELECT @Counter += 1	
END

SELECT 	 SPT.ThisPartyType 
		,SPT.ThisSubPartyType
		,LinkedSalutation
		,LinkedFirstName 
		,LinkedLastName 
		,LinkedDateOfBirth
		,LinkedPartyType 
		,LinkedSubPartyType
		,LinkedIncidentDate
		,LinkedClaimType
		,LinkedEliteReference
		,LinkedFiveGrading
		,OtherPartyType
		,OtherSubPartyType
		,OtherSalutation
		,OtherFirstname
		,OtherLastName
		,OtherDoB
		,OtherMoR
		,OtherFiveGrading 
FROM @ResultsTable RT
INNER JOIN (
			SELECT	 PT.PartyTypeText AS ThisPartyType
					,SPT.SubPartyText AS ThisSubPartyType
					,I2P.Incident_Id
					,I2P.Person_Id
			FROM dbo.Incident2Person I2P
			INNER JOIN PartyType PT ON i2p.PartyType_Id = PT.Id
			INNER JOIN SubPartyType SPT ON i2p.SubPartyType_Id = SPT.Id
		   ) SPT ON SPT.Incident_Id = RT.Incident_Id AND SPT.Person_Id = RT.Person_Id