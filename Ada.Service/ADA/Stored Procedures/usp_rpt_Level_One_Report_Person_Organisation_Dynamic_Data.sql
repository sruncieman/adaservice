﻿/****** Object:  StoredProcedure [dbo].[usp_rpt_Level_One_Report_Person_Organisation_Dynamic_Data]    Script Date: 02/11/2014 16:33:44 ******/	
CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Person_Organisation_Dynamic_Data]
(@RiskClaim_Id INT
,@Person_ID INT = NULL
,@Organisation_ID INT = NULL)

AS

DECLARE  @ParentName VARCHAR(20) = CASE WHEN @Person_ID IS NOT NULL THEN 'PersonRisk' ELSE 'Incident' END

SELECT B.ColumnName, B.Value
FROM	(
		SELECT   CAST(RegisteredNumber AS VARCHAR(50)) [Registered Number]
				,CAST(VatNumber AS VARCHAR(50)) [VAT Number]
				,CAST(Email AS VARCHAR(50)) [Email]
				,CAST(Website AS VARCHAR(50)) [Website]
		FROM dbo.rpt_Level_One_Report_Organisation
		WHERE RiskClaim_Id = @RiskClaim_Id 
		AND (Per_DbId = @Person_ID OR @Person_ID IS NULL)
		AND (Org_DbId = @Organisation_ID OR @Organisation_ID IS NULL)
		AND ParentName = @ParentName
		) A
UNPIVOT (Value FOR ColumnName IN (
								 [Registered Number]
								,[VAT Number]
								,[Email]
								,[Website]
								)
		) AS B


