﻿ 
 CREATE PROCEDURE [dbo].[uspPersonLinkedSettledCaseCount]
 
	 @PersonId INT = NULL,
	 @IncidentId INT  = 1,
	 @IncludeThisPersonId BIT = 1,
	 @IncludeConfirmed BIT = 1,
	 @IncludeUnconfirmed BIT = 0,
	 @IncludeTentative BIT = 0,
	 @StartDate SMALLDATETIME = '2000-01-01',
	 @EndDate SMALLDATETIME = '2020-01-01', 
	 @OutcomeCategoryId INT = 1,
	 @ClaimTypeId INT = 1,
 	 @IsPotentialClaimant BIT  = NULL
 AS
 
IF (@PersonId IS NULL)
BEGIN
 SELECT 1000 AS OUT_Count
 return 0
END


DECLARE @PersonTree NVARCHAR(1000) = 
'SELECT Id FROM dbo.fn_GetPersonTree(' + CONVERT(NVARCHAR(50) ,@PersonId) + ', ' +   
CONVERT(NVARCHAR(50), @IncludeConfirmed)  + ', ' +   
CONVERT(NVARCHAR(50), @IncludeUnconfirmed) + ', ' +    
CONVERT(NVARCHAR(50), @IncludeTentative)  + ', ' +  
CONVERT(NVARCHAR(50), @IncludeThisPersonId) + ') '

DECLARE @NotPersonTree NVARCHAR(1000) = 
'SELECT Id FROM dbo.fn_GetPersonTree(' + CONVERT(NVARCHAR(50) ,@PersonId) + ', 0, 1, 0, 1)'

DECLARE @IsPotentialClaimantWhere NVARCHAR(100) = ''
IF @IsPotentialClaimant IS NOT NULL
	SET @IsPotentialClaimantWhere = ' AND PC.Flag = ' + CONVERT(CHAR(1), @IsPotentialClaimant)



DECLARE @Where NVARCHAR(500) =
	CASE  WHEN @ClaimTypeId < 0 THEN  'WHERE ClaimType_Id !=  ' + CONVERT(NVARCHAR(50) ,ABS(@ClaimTypeId))
	ELSE CASE  WHEN @ClaimTypeId >= 0 THEN  'WHERE ClaimType_Id = ' + CONVERT(NVARCHAR(50) ,@ClaimTypeId) ELSE  ''  END  
END

	DECLARE @Sql NVARCHAR(3000) = '	
		SELECT Incident_ID 
		FROM Incident2Person MOR
		INNER JOIN PotentialClaimant PC
		ON MOR.SubPartyType_Id = PC.SubPartyType
		AND MOR.PartyType_Id = PC.PartyType 
		WHERE Person_Id IN(
		SELECT Person_Id 
		FROM Incident2Person MOR 
		WHERE(Person_Id IN(' + @PersonTree + ') 
		AND (Incident_ID  != ' + CONVERT(NVARCHAR(50) ,@IncidentId) + ')))	
		' + @IsPotentialClaimantWhere
		
		
	PRINT @Sql
	DECLARE @Tbl TmpTbl
		INSERT INTO @Tbl
		EXECUTE sp_executesql @Sql
	
	
	
DECLARE @ParmDefinition NVARCHAR(3000) = '@Tbl TmpTbl READONLY'
	
SET @Sql  = 			
	'SELECT 
		COUNT(*) OUT_Coun 
	FROM (		
		SELECT MOR.Incident_Id
		FROM 
			Incident2PersonOutcome MOR 
		INNER JOIN 
			OutcomeCategories OC 
		ON MOR.MannerOfResolution = OC.CategoryText
		WHERE Incident_ID IN (SELECT id from @Tbl) AND Person_Id NOT IN (' + @PersonTree + ')  AND OC.CategoryTypeId = ' + CONVERT(Varchar(50), @OutcomeCategoryId)	+
		
		') AS A
		INNER JOIN 
		Incident  B ON A.Incident_Id = B.Id ' + @Where
		
	
PRINT @SQL
DECLARE @TblOUT TmpTbl
INSERT INTO @TblOUT
EXECUTE sp_executesql @Sql, @ParmDefinition,  @Tbl=@Tbl
SELECT Id AS OUT_Count FROM @TblOUT
