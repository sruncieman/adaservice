﻿
CREATE PROCEDURE [dbo].[uspGetMatchingPerson]
	@XML XML,
	@FieldsFlag BIT OUTPUT 

AS
SET NOCOUNT ON
SET DATEFORMAT YMD
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--XML DEFINITION NEEDED TO ACCOUNT FOR MISSING TAGS
DECLARE @XML_DEF VARCHAR(MAX) = '<Person RiskClaim_Id="" FirstName="" LastName="" DateOfBirth=""><NINumbers><NINumber NINumber="" /></NINumbers><DrivingLicenses><DrivingLicense DriverNumber="" /></DrivingLicenses><Passports><Passport PassportNumber="" /></Passports><EmailAddresses><EmailAddress EmailAddress="" /></EmailAddresses><PaymentCards><PaymentCard PaymentCardNumber="" /></PaymentCards><BankAccounts><BankAccount AccountNumber="" /></BankAccounts><Policies><Policy PolicyNumber="" /></Policies><Addresses><Address PafUPRN="" SubBuilding="" Building="" BuildingNumber="" Street="" Locality="" Town="" PostCode="" /></Addresses><Telephones><Telephone TelephoneNumber="" /></Telephones><Vehicles><Vehicle VehicleRegistration=""  VehicleVIN="" /></Vehicles><Organisations><Organisation OrganisationName="" RegisteredNumber="" VATNumber="" /></Organisations><Handsets><Handset HandsetImei="" /></Handsets></Person>'
DECLARE @XML_Check XML = '<Data>' +  REPLACE(CONVERT(VARCHAR(MAX), @XML), 'UNKNOWN', '') +  @XML_DEF + '</Data>'

SET @FieldsFlag = 0 --RETURN VALUE 0 IF NOT ENOUGHT DATA SUPPLIED
DECLARE @T AS TABLE(TID INT NOT NULL IDENTITY(1,1), RuleNo INT, PriorityGroup INT, RiskClaimFlag INT, HasRiskClaim_Id BIT)
DECLARE @Ret TABLE(ID INT, RuleNo INT, PriorityGroup INT, MatchType INT)

--CREATING TABLE OF POSSABLE RULES TO MATCH
INSERT INTO @T(RuleNo, PriorityGroup, RiskClaimFlag, HasRiskClaim_Id)
SELECT RuleNo, PriorityGroup, RiskClaimFlag, HasRiskClaim_Id FROM fnGetMatchingPersonToProcess (@XML_Check) ORDER BY PriorityGroup ,RuleNo
DECLARE @ROWCOUNT INT = @@ROWCOUNT	
IF @ROWCOUNT = 0  --NOT ENOUGHT DATA SUPPLIED EXIT PROCEDURE
	GOTO MatchEnd
ELSE
	SET @FieldsFlag = 1
		
-------------------------------------------------------------
--IfWeHaveRulesWeCanMatchLetsAssignXMLValuesToParameters
-------------------------------------------------------------
--Person
DECLARE  @RiskClaim_Id VARCHAR(MAX)
		,@FirstName VARCHAR(MAX)
		,@LastName VARCHAR(MAX)
		,@DateOfBirth VARCHAR(MAX)

SELECT   @RiskClaim_Id = XKC.Data.value('@RiskClaim_Id', 'VARCHAR(MAX)')
		,@FirstName = XKC.Data.value('@FirstName', 'VARCHAR(MAX)')
		,@LastName = XKC.Data.value('@LastName', 'VARCHAR(MAX)')
		,@DateOfBirth = XKC.Data.value('@DateOfBirth', 'VARCHAR(100)')
FROM @XML.nodes('//Person') XKC(Data) 

SELECT   @DateOfBirth = CONVERT(DATE, @DateOfBirth, 103)
		,@LastName = REPLACE(@LastName,'''','''''')

--NINumbers
IF OBJECT_ID('tempdb..#NINumber') IS NOT NULL
	DROP TABLE #NINumber
CREATE TABLE #NINumber (NINumber VARCHAR(MAX))

INSERT INTO #NINumber (NINumber)
SELECT   XKC.Data.value('@NINumber', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/NINumbers/NINumber') XKC(Data) 

--DrivingLicenses
IF OBJECT_ID('tempdb..#DrivingLicenses') IS NOT NULL
	DROP TABLE #DrivingLicenses
CREATE TABLE #DrivingLicenses (DriverNumber VARCHAR(MAX))

INSERT INTO #DrivingLicenses (DriverNumber)
SELECT  XKC.Data.value('@DriverNumber', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/DrivingLicenses/DrivingLicense') XKC(Data) 

--Passports
IF OBJECT_ID('tempdb..#Passports') IS NOT NULL
	DROP TABLE #Passports
CREATE TABLE #Passports (PassportNumber VARCHAR(MAX))

INSERT INTO #Passports (PassportNumber)
SELECT  XKC.Data.value('@PassportNumber', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/Passports/Passport') XKC(Data) 

--EmailAddresses
IF OBJECT_ID('tempdb..#EmailAddresses') IS NOT NULL
	DROP TABLE #EmailAddresses
CREATE TABLE #EmailAddresses (EmailAddress VARCHAR(MAX))

INSERT INTO #EmailAddresses (EmailAddress)
SELECT  XKC.Data.value('@EmailAddress', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/EmailAddresses/EmailAddress') XKC(Data) 

--PaymentCards
IF OBJECT_ID('tempdb..#PaymentCards') IS NOT NULL
	DROP TABLE #PaymentCards
CREATE TABLE #PaymentCards(PaymentCardNumber VARCHAR(MAX))

INSERT INTO #PaymentCards (PaymentCardNumber)
SELECT  XKC.Data.value('@PaymentCardNumber', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/PaymentCards/PaymentCard') XKC(Data) 

--BankAccounts
IF OBJECT_ID('tempdb..#BankAccounts') IS NOT NULL
	DROP TABLE #BankAccounts
CREATE TABLE #BankAccounts(AccountNumber VARCHAR(MAX))

INSERT INTO #BankAccounts (AccountNumber)
SELECT  XKC.Data.value('@AccountNumber', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/BankAccounts/BankAccount') XKC(Data) 

--Policies
IF OBJECT_ID('tempdb..#Policies') IS NOT NULL
	DROP TABLE #Policies
CREATE TABLE #Policies(PolicyNumber VARCHAR(MAX))

INSERT INTO #Policies (PolicyNumber)
SELECT  XKC.Data.value('@PolicyNumber', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/Policies/Policy') XKC(Data) 

--Addresses
IF OBJECT_ID('tempdb..#Addresses') IS NOT NULL
	DROP TABLE #Addresses
CREATE TABLE #Addresses(PafUPRN VARCHAR(MAX), SubBuilding VARCHAR(MAX), Building VARCHAR(MAX), BuildingNumber VARCHAR(MAX), Street VARCHAR(MAX), Locality VARCHAR(MAX), Town VARCHAR(MAX), PostCode VARCHAR(MAX))

INSERT INTO #Addresses (PafUPRN, SubBuilding, Building, BuildingNumber, Street, Locality, Town, PostCode)
SELECT   XKC.Data.value('@PafUPRN', 'VARCHAR(MAX)')
		,XKC.Data.value('@SubBuilding', 'VARCHAR(MAX)')
		,XKC.Data.value('@Building', 'VARCHAR(MAX)')
		,XKC.Data.value('@BuildingNumber', 'VARCHAR(MAX)')
		,XKC.Data.value('@Street', 'VARCHAR(MAX)')
		,XKC.Data.value('@Locality', 'VARCHAR(MAX)')
		,XKC.Data.value('@Town', 'VARCHAR(MAX)')
		,XKC.Data.value('@PostCode', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/Addresses/Address') XKC(Data) 

--Telephones
IF OBJECT_ID('tempdb..#Telephones') IS NOT NULL
	DROP TABLE #Telephones
CREATE TABLE #Telephones(TelephoneNumber VARCHAR(MAX))

INSERT INTO #Telephones (TelephoneNumber)
SELECT  XKC.Data.value('@TelephoneNumber', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/Telephones/Telephone') XKC(Data) 

--Vehicles
IF OBJECT_ID('tempdb..#Vehicles') IS NOT NULL
	DROP TABLE #Vehicles
CREATE TABLE #Vehicles(VehicleRegistration VARCHAR(MAX), VehicleVIN VARCHAR(MAX))

INSERT INTO #Vehicles (VehicleRegistration, VehicleVIN)
SELECT   XKC.Data.value('@VehicleRegistration', 'VARCHAR(MAX)')
		,XKC.Data.value('@VehicleVIN', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/Vehicles/Vehicle') XKC(Data) 

--Organisations
IF OBJECT_ID('tempdb..#Organisations') IS NOT NULL
	DROP TABLE #Organisations
CREATE TABLE #Organisations (OrganisationName VARCHAR(MAX), RegisteredNumber VARCHAR(MAX), VATNumber VARCHAR(MAX))

INSERT INTO #Organisations (OrganisationName, RegisteredNumber, VATNumber)
SELECT   XKC.Data.value('@OrganisationName', 'VARCHAR(MAX)')
		,XKC.Data.value('@RegisteredNumber', 'VARCHAR(MAX)')
		,XKC.Data.value('@VATNumber', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/Organisations/Organisation') XKC(Data) 

--Handsets
IF OBJECT_ID('tempdb..#Handsets') IS NOT NULL
	DROP TABLE #Handsets
CREATE TABLE #Handsets(HandsetImei VARCHAR(MAX))

INSERT INTO #Handsets (HandsetImei)
SELECT  XKC.Data.value('@HandsetImei', 'VARCHAR(MAX)')
FROM @XML.nodes('//Person/Handsets/Handset') XKC(Data) 
	
DECLARE @RuleNo INT
DECLARE @PriorityGroup INT
DECLARE @RiskClaimFlag TINYINT
DECLARE @ExecWithRcId BIT
DECLARE @ExecWithOutRcId BIT
DECLARE @Count INT = 1
DECLARE @SQL NVARCHAR(MAX)

--LOOP EACH RULE, EXIT WHEN A MATCH IS FOUND
WHILE @Count <= @ROWCOUNT
BEGIN

	SELECT
		@RuleNo = RuleNo,	
		@PriorityGroup = PriorityGroup,	
		@RiskClaimFlag = RiskClaimFlag,
		@ExecWithRcId = CASE WHEN HasRiskClaim_Id = 1 AND RiskClaimFlag > 0 THEN 1 ELSE 0 END,
		@ExecWithOutRcId = CASE WHEN RiskClaimFlag = 0 OR RiskClaimFlag = 2 THEN 1 ELSE 0 END
	FROM @T WHERE TID = @Count
		
	--PROCESS ALL TENTATIVE (Risk Claim Flag) UNION SAME RULE GROUPS 
	IF @RiskClaimFlag = 0
	BEGIN
		
		DECLARE @GrpCount INT = @Count
		DECLARE @GrpCountEnd INT = (SELECT MAX(TID) FROM @T WHERE PriorityGroup = (SELECT PriorityGroup FROM @T WHERE TID = @Count) AND RiskClaimFlag = 0 AND TID >= @Count)
		SET @SQL = dbo.fnGetMatchingPersonTSql(@RuleNo, 0, @RiskClaim_Id, @FirstName, @LastName, @DateOfBirth)
		
		WHILE @GrpCount < @GrpCountEnd
		BEGIN
			SET @GrpCount += 1
			SELECT @RuleNo = RuleNo FROM @T WHERE TID = @GrpCount
			SET @SQL +=   CHAR(10) + 'UNION ALL' + CHAR(10) + dbo.fnGetMatchingPersonTSql(@RuleNo, 0, @RiskClaim_Id, @FirstName, @LastName, @DateOfBirth)

		END
			PRINT '################################# RiskClaimFlag = 0 START #######################################'
			PRINT ''
			PRINT @SQL
			PRINT ''
			PRINT '################################# RiskClaimFlag = 0 END #########################################'
		INSERT INTO @Ret
		EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
		IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
		
		SET @Count = @GrpCountEnd
		
	END
	ELSE
	BEGIN
		--PROCESS ALL CONF (Risk Claim Flag) If we have a RC_ID 
		IF @ExecWithRcId = 1
		BEGIN
			SET @SQL = dbo.fnGetMatchingPersonTSql(@RuleNo, 1, @RiskClaim_Id, @FirstName, @LastName, @DateOfBirth)
			PRINT '------------------------------------WithRcId-------------------------------------------'
			PRINT @SQL
			PRINT ''
			INSERT INTO @Ret
			EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
				
		END
		
		--PROCESS ALL UN-CONF (Risk Claim Flag) 
		IF @ExecWithOutRcId = 1
		BEGIN
			PRINT '----------------------------------WithOutRcId------------------------------------------'
			SET @SQL = dbo.fnGetMatchingPersonTSql(@RuleNo, 0, @RiskClaim_Id, @FirstName, @LastName, @DateOfBirth) 
			PRINT @SQL
			PRINT ''
			INSERT INTO @Ret
			EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
			
		END

	END
	
SET @Count += 1

END 

MatchEnd:
	
IF (SELECT COUNT(1) FROM @Ret) > 1
BEGIN
	--RemoveAnyDuplicationsIfTheresultSetReturnsMultipleMatches
	DECLARE @UniqueList TABLE (ID INT)
	DECLARE @IDString VARCHAR(MAX) = (SELECT (SELECT CAST(R.ID AS VARCHAR(MAX)) + ',' FROM @Ret R INNER JOIN [dbo].[Person] P ON P.Id = R.ID ORDER BY P.PersonId, P.Id FOR XML PATH (''))  IDString)
	DECLARE @LinkConfidence INT = (SELECT TOP 1 MatchType FROM @Ret)

	SELECT @IDString = LEFT(@IDString, LEN(@IDString) -1)

	INSERT INTO @UniqueList
	EXECUTE [dbo].[uspUniquePerson] @IDString, ',', @LinkConfidence

	--ReturnTheListUnFiltered
	SELECT R.ID, MIN(R.RuleNo) AS RuleNo, R.PriorityGroup, R.MatchType 
	FROM @Ret R
	INNER JOIN @UniqueList UL ON UL.ID = R.ID
	INNER JOIN [dbo].[Person] P ON P.Id = R.ID 
	GROUP BY R.ID, R.PriorityGroup, R.MatchType,P.PersonId, P.Id 
	ORDER BY P.PersonId, P.Id 

END
ELSE
BEGIN
	--ReturnTheListUnFiltered
	SELECT ID, MIN(RuleNo) AS RuleNo, PriorityGroup, MatchType FROM @Ret GROUP BY ID, PriorityGroup, MatchType 
END

-----------------------
--Tidy
-----------------------
IF OBJECT_ID('tempdb..#NINumber') IS NOT NULL
	DROP TABLE #NINumber
IF OBJECT_ID('tempdb..#DrivingLicenses') IS NOT NULL
	DROP TABLE #DrivingLicenses
IF OBJECT_ID('tempdb..#Passports') IS NOT NULL
	DROP TABLE #Passports
IF OBJECT_ID('tempdb..#EmailAddresses') IS NOT NULL
	DROP TABLE #EmailAddresses
IF OBJECT_ID('tempdb..#PaymentCards') IS NOT NULL
	DROP TABLE #PaymentCards
IF OBJECT_ID('tempdb..#BankAccounts') IS NOT NULL
	DROP TABLE #BankAccounts
IF OBJECT_ID('tempdb..#Policies') IS NOT NULL
	DROP TABLE #Policies
IF OBJECT_ID('tempdb..#Addresses') IS NOT NULL
	DROP TABLE #Addresses
IF OBJECT_ID('tempdb..#Telephones') IS NOT NULL
	DROP TABLE #Telephones
IF OBJECT_ID('tempdb..#Vehicles') IS NOT NULL
	DROP TABLE #Vehicles
IF OBJECT_ID('tempdb..#Organisations') IS NOT NULL
	DROP TABLE #Organisations
IF OBJECT_ID('tempdb..#Handsets') IS NOT NULL
	DROP TABLE #Handsets



