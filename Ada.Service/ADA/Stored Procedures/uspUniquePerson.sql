﻿

CREATE PROCEDURE [dbo].[uspUniquePerson]

	@RowData nvarchar(MAX),
	@Delimiter nvarchar(5),
	@LinkConfidence int

AS

DECLARE @IN TABLE (Id INT, Data INT)

INSERT INTO @IN(Id, Data)
SELECT  Id, Data FROM  dbo.fn_Split(@RowData, @Delimiter)
DECLARE @Rows INT = @@ROWCOUNT

DECLARE @Count INT = 1
DECLARE @Data INT = 1

WHILE (@Count <= @Rows AND (SELECT COUNT(1) FROM @IN) > 1)
BEGIN
	SELECT @Data = Data FROM @IN WHERE  Id = @Count
	IF( @Data IS NOT NULL)
		DELETE FROM @IN WHERE Data IN(SELECT * FROM dbo.fn_GetPersonTree2(@Data, @LinkConfidence)) AND Data != @Data
	SET @Count += 1
END

SELECT DATA AS UniquePeopleIds FROM @IN
