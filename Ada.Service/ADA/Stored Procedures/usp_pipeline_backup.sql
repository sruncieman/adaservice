﻿CREATE PROCEDURE [dbo].[usp_pipeline_backup]
	@FilePath varchar(256),
	@FolderName varchar(256),
	@SaveSetName varchar(50),
	@BackupType int
--WITH EXECUTE AS 'SVC-ADA-PowerUser'
AS
BEGIN

DECLARE @DirTree TABLE (subdirectory nvarchar(255), depth INT)
DECLARE @FullFileName varchar(256)
DECLARE @TypeExtn varchar(256)
DECLARE @DatabaseName varchar(60)
DECLARE @FileExists bit
DECLARE @ExistsOutput int
DECLARE @rc int, @dir nvarchar(4000)

SET @DatabaseName = convert(varchar, DB_NAME())

--IF no filepath was supplied (empty or null) read from registry. See comment above about permissions if this call fails
IF NULLIF(@FilePath, '') IS NULL
BEGIN
exec @rc = master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory', @dir output, 'no_output'
Select @dir
END
ELSE
BEGIN
Set @dir = @FilePath
END

--if we are given a folder name (not empty, not null), check IF it exists and IF not create it
IF NOT NULLIF(@FolderName, '') IS NULL
BEGIN

--if the folder name is a # replace with database name
IF @FolderName = '#'
BEGIN
SET @FolderName = @DatabaseName
END

INSERT INTO @DirTree(subdirectory, depth)
EXEC master.sys.xp_dirtree @dir

SET @dir = @dir + '\' + @FolderName

--Create the folder and force a full backup
IF NOT EXISTS (SELECT 1 FROM @DirTree WHERE subdirectory = @FolderName)
BEGIN
EXEC master.dbo.xp_create_subdir @dir
SET @BackupType = 1
END
END

--Create a subfolder with todays date. IF it doesn't exist create it
SET @FolderName = CONVERT(varchar(10),GETDATE(),120)

/*
INSERT INTO @DirTree(subdirectory, depth)
EXEC master.sys.xp_dirtree @dir
*/

SET @dir = @dir + '\' + @FolderName

--BelowSectionAddesPA:20140917
DECLARE @xp_fileexist_output TABLE (FILE_EXISTS INT NOT NULL, FILE_IS_DIRECTORY INT NOT NULL, PARENT_DIRECTORY_EXISTS INT NOT NULL)

INSERT INTO @xp_fileexist_output
EXECUTE Master.dbo.xp_fileexist @dir

--Create the folder and force a full backup
IF NOT EXISTS (SELECT TOP 1 1 FROM @xp_fileexist_output WHERE FILE_IS_DIRECTORY = 1 )
BEGIN
	EXECUTE Master.dbo.xp_create_subdir @dir
	SET @BackupType = 0 --IfWeNeedToCreateTheFolderWeNeedToForceAFullFormattedBackup
END
	
/*	
--Create the folder and force a full backup
IF NOT EXISTS (SELECT 1 FROM @DirTree WHERE subdirectory = @FolderName)
BEGIN
EXEC master.dbo.xp_create_subdir @dir
SET @BackupType = 1
END
*/

SET @TypeExtn = ''

--IF @BackupType = 1  --Full initialised and formatted backup in a new save set
--BEGIN
--	SET @TypeExtn = '_Full'
--END
--ELSE IF @BackupType = 2  --Full backup into existing save set
--BEGIN
--	SET @TypeExtn = '_Full'
--END
--ELSE IF @BackupType = 3 --Differantial backup into existing save set
--BEGIN
--	SET @TypeExtn = '_Diff'
--END
--ELSE IF @BackupType = 4 --Log backup into existing save set
--BEGIN
--	SET @TypeExtn = '_Log'
--END

--Now the full paths of folders exists so append the database name
SET @FullFileName = @dir + '\' + @DatabaseName + @TypeExtn + '.bak'

--Check IF the backup file exists
exec master.dbo.xp_fileexist @FullFileName, @ExistsOutput OUTPUT

--IF the file does not exist force a full backup
IF @ExistsOutput = 0 
BEGIN
	SET @BackupType = 1
	--SET @TypeExtn = '_Full'
	SET @FullFileName = @dir + '\' + @DatabaseName + @TypeExtn + '.bak'
END

--Now perform backup required

IF @BackupType = 1  --Full initialised and formatted backup in a new save set
BEGIN
	BACKUP DATABASE @DatabaseName TO  DISK = @FullFileName WITH FORMAT, INIT,  
	NAME = @SaveSetName, SKIP, NOREWIND, NOUNLOAD,  STATS = 10
END
ELSE IF @BackupType = 2  --Full backup into existing save set
BEGIN
	BACKUP DATABASE @DatabaseName TO  DISK = @FullFileName WITH NOFORMAT, INIT,  
	NAME = @SaveSetName, SKIP, NOREWIND, NOUNLOAD,  STATS = 10
END
ELSE IF @BackupType = 3 --DIFferantial backup into existing save set
BEGIN
	BACKUP DATABASE @DatabaseName TO  DISK = @FullFileName WITH NOFORMAT, NOINIT, DIFFERENTIAL,
	NAME = @SaveSetName, SKIP, NOREWIND, NOUNLOAD,  STATS = 10
END
ELSE IF @BackupType = 4 --Log backup into existing save set
BEGIN
	BACKUP LOG @DatabaseName TO  DISK = @FullFileName WITH NOFORMAT, NOINIT, 
	NAME = @SaveSetName, SKIP, NOREWIND, NOUNLOAD,  STATS = 10
END
END