﻿ 
 CREATE PROCEDURE [dbo].[uspPersonSettledCaseCount]
 
	 @PersonId INT,
	 @IncidentID INT,
	 @IncludeThisPersonId BIT,
	 @IncludeConfirmed BIT ,
	 @IncludeUnconfirmed BIT,
	 @IncludeTentative BIT,
	 @StartDate SMALLDATETIME,
	 @EndDate SMALLDATETIME,
	 @OutcomeCategoryId INT,
  	 @IsPotentialClaimant BIT  = NULL
 
 AS

IF @IsPotentialClaimant = 0
		SELECT COUNT(*) OUT_Count FROM (
			SELECT MOR.Id, MOR.Person_Id, MOR.Incident_Id FROM Incident2PersonOutcome MOR 
			INNER JOIN OutcomeCategories OC 
			ON MOR.MannerOfResolution = OC.CategoryText
			WHERE Person_Id IN(SELECT Id FROM dbo.fn_GetPersonTree (@PersonId, @IncludeConfirmed ,@IncludeUnconfirmed, @IncludeTentative, @IncludeThisPersonId))
									
		) A
		INNER JOIN
		Incident2Person B
		ON A.Incident_Id = B.Incident_Id 
		AND A.Person_Id = B.Person_Id
		WHERE 
			(PartyType_Id = 3 AND SubPartyType_Id IN (0, 1, 3))
			OR (PartyType_Id IN (5 ,9))

ELSE IF @IsPotentialClaimant = 1
		SELECT COUNT(*) OUT_Count FROM (
			SELECT MOR.Id, MOR.Person_Id, MOR.Incident_Id FROM Incident2PersonOutcome MOR 
			INNER JOIN OutcomeCategories OC 
			ON MOR.MannerOfResolution = OC.CategoryText
			WHERE Person_Id IN(SELECT Id FROM dbo.fn_GetPersonTree (@PersonId, @IncludeConfirmed ,@IncludeUnconfirmed, @IncludeTentative, @IncludeThisPersonId))
									
		) A
		INNER JOIN
		Incident2Person B
		ON A.Incident_Id = B.Incident_Id 
		AND A.Person_Id = B.Person_Id
		WHERE 
			(PartyType_Id != 3 AND SubPartyType_Id NOT IN (0, 1, 3))
			OR (PartyType_Id NOT IN (5 ,9))
			
ELSE


	SELECT COUNT(*) OUT_Count FROM Incident2PersonOutcome MOR 
	INNER JOIN OutcomeCategories OC 
	ON MOR.MannerOfResolution = OC.CategoryText
	WHERE Person_Id IN(SELECT Id FROM dbo.fn_GetPersonTree(@PersonId, @IncludeConfirmed ,@IncludeUnconfirmed, @IncludeTentative, @IncludeThisPersonId)
	) AND Incident_Id  != @IncidentID
	AND
	OC.CategoryTypeId = @OutcomeCategoryId



