﻿CREATE PROCEDURE [dbo].[uspRPTGetClaimSummaryMsgsInpBatchIDThresholds] 
(
 @BatchId		INT	
,@RedClaim		INT	
,@AmberClaim	INT	
)
AS
SET NOCOUNT ON

SELECT	  ClaimId
		, BaseRiskClaim_Id
		, [Claim Number]
		, CreatedDate
		, [Client Batch Reference]
		, [Incident Date]
		, TotalKeyAttractorCount
		, Reserve
		, Payment
		, [Upload Date/Time]
		, Received
		, Processed
		, RiskRating
		, LatestVersion
		, RiskClaim_Id
		, RPTHeader
		, RptMsg
		, (CASE WHEN a.RiskRating = 'Red' THEN a.Reserve END) AS Red_Reserve
		, (CASE WHEN a.RiskRating = 'Red' THEN a.Payment END) AS Red_Payments
		, (CASE WHEN a.RiskRating = 'Peru' THEN a.Reserve ELSE 0 END) AS Amber_Reserve
		, (CASE WHEN a.RiskRating = 'Peru' THEN a.Payment ELSE 0 END) AS Amber_Payments
		, (CASE WHEN a.RiskRating = 'Green' THEN a.Reserve ELSE 0 END) AS Green_Reserve
		, (CASE WHEN a.RiskRating = 'Green' THEN a.Payment ELSE 0 END) AS Green_Payments
		, TotalScore
FROM	(
		SELECT	rc.Id AS ClaimId
				, rc.BaseRiskClaim_Id
				, rc.ClientClaimRefNumber AS [Claim Number]
				, rc.CreatedDate
				, ISNULL(rb.ClientBatchReference, 'N/A')  AS [Client Batch Reference]
				, i.IncidentDate AS [Incident Date]
				, rcr.TotalKeyAttractorCount
				, ISNULL(i.Reserve, 0) AS Reserve
				, ISNULL(i.PaymentsToDate, 0) AS Payment
				, rc.CreatedDate AS [Upload Date/Time]
				, (SELECT 1 AS Expr1) AS Received
				, (CASE WHEN rc.ClaimStatus = 3 THEN 1 ELSE NULL END) AS Processed
				, (CASE WHEN rcr.TotalScore >= @RedClaim THEN 'Red' WHEN rcr.TotalScore >= @AmberClaim THEN 'Peru' ELSE 'Green' END) AS RiskRating
				, rc.LatestVersion
				, ff.RiskClaimRunID RiskClaim_Id
				, ff.RPTHeader
				, ff.RPTMessage RptMsg
				, rcr.TotalScore
       FROM RiskBatch AS rb 
       INNER JOIN RiskClaim AS rc ON rb.Id = rc.RiskBatch_Id 
       INNER JOIN Incident AS i ON rc.Incident_Id = i.Id 
       INNER JOIN RiskClaimRun AS rcr ON rcr.RiskClaim_Id = rc.Id 
       LEFT OUTER JOIN dbo.fn_GetSummaryMessagesInpBatchID(@BatchId) AS ff ON ff.RiskClaimRunID = rcr.Id
       WHERE (rb.Id = @BatchId) 
       AND (rcr.LatestVersion = 1)) AS a

