﻿

CREATE PROCEDURE [dbo].[uspGetMatchingHandsetIMEI]
	
	@RiskClaim_Id INT = NULL,
	@HandsetIMEI NVARCHAR(255) = NULL,
	@FieldsFlag BIT OUTPUT 

AS


SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @FieldsFlag = 1

SELECT Id, 0 RuleNo, 1 PriorityGroup, 0 MatchType 
FROM [dbo].[Handset]
WHERE ADARecordStatus = 0 AND [HandsetIMEI] = @HandsetIMEI
GROUP BY Id