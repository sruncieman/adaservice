﻿
CREATE PROCEDURE [dbo].[uspGetMatchingDrivingLicense]
	
	@RiskClaim_Id INT = NULL,
	@DriverNumber VARCHAR(50) = NULL,
	@FieldsFlag BIT OUTPUT 

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @FieldsFlag = 1
SELECT Id, 0 RuleNo, 1 PriorityGroup, 0 MatchType 
FROM dbo.DrivingLicense 
WHERE ADARecordStatus = 0 AND DriverNumber = @DriverNumber
GROUP BY Id
