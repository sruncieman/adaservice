﻿CREATE PROCEDURE [dbo].[uspRPTGetBatchInpBatchID] @BatchId INT = 0
AS

SET NOCOUNT ON

SELECT   RC.ClientName
		,ISNULL(RB.ClientBatchReference, 'N/A') AS ClientBatchReference
		,RB.BatchReference
		,ISNULL(FirstName + ' ','') + LastName CreatedBy
		,RB.CreatedDate
FROM dbo.RiskBatch RB
INNER JOIN dbo.RiskClient RC ON RC.Id = RB.RiskClient_Id
INNER JOIN dbo.RiskUser RT ON RT.UserName = RB.CreatedBy
WHERE RB.Id = @BatchId

