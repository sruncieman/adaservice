﻿
CREATE PROCEDURE [dbo].[uspGetMatchingBBPin]
	
	@RiskClaim_Id INT = NULL,
	@BBPin VARCHAR(50) = NULL,
	@DisplayName VARCHAR(50) = NULL,
	@FieldsFlag BIT OUTPUT 

AS


SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @FieldsFlag = 1

SELECT Id, 0 RuleNo, 1 PriorityGroup, 0 MatchType 
FROM dbo.BBPin 
WHERE ADARecordStatus = 0 AND BBPin = @BBPin
GROUP BY Id
