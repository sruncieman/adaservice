﻿

CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Message_Data_Person]

	@RiskClaim_Id INT,
	@Person_Id INT

AS

SELECT	 RiskClaim_Id
		,Per_DbId
		,Veh_DbId
		,Add_DbId
		,Org_DbId
		,LocalName
		--,Rule_Key
		,Rule_Set_Key
		,DataMed1
		,DataMed2
		--,CONVERT(DATE,DataMed2) AS DataMed2
		,DataMed3
		,DataMed4
		,DataMed5
		,DataMed6
		,DataMed7
		,DataMed8
		,DataMed9
		,DataMed10
		,DataMed11
		,DataMed12
		,DataMed13
		,DataMed14
		,DataMed15
		,DataMed16
		,DataMed17
		,DataMed18
		,DataMed19
		,DataMed20
		,MessageHeader
		,ReportHeader
FROM dbo.rpt_Level_One_Report_DataMed 
WHERE RiskClaim_Id = @RiskClaim_Id 
AND Per_DbId = @Person_Id 
AND LocalName = 'PersonRisk' 
AND Rule_Set_Key = 'PersonAllClaimsExperience'
GROUP BY RiskClaim_Id
		,Per_DbId
		,Veh_DbId
		,Add_DbId
		,Org_DbId
		,LocalName
		--,Rule_Key
		,Rule_Set_Key
		,DataMed1
		,DataMed2
		--,CONVERT(DATE,DataMed2) AS DataMed2
		,DataMed3
		,DataMed4
		,DataMed5
		,DataMed6
		,DataMed7
		,DataMed8
		,DataMed9
		,DataMed10
		,DataMed11
		,DataMed12
		,DataMed13
		,DataMed14
		,DataMed15
		,DataMed16
		,DataMed17
		,DataMed18
		,DataMed19
		,DataMed20
		,MessageHeader
		,ReportHeader
		

