﻿
/****** Object:  StoredProcedure [dbo].[usp_rpt_Level_One_Report_Person_Organisation]    Script Date: 02/11/2014 16:14:54 ******/
CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Person_Organisation]

	@RiskClaim_Id INT,
	@Person_ID INT = NULL

AS

DECLARE  @ParentName VARCHAR(20) = CASE WHEN @Person_ID IS NOT NULL THEN 'PersonRisk' ELSE 'Incident' END
		,@Msg VARCHAR(50) = 'Not Provided'
		,@NKD VARCHAR(50) = 'No Keoghs Data Match'
		,@ClaimRef VARCHAR(50)

SELECT @ClaimRef = ClaimNumber FROM dbo.rpt_Level_One_Report_Incident_And_Policy WHERE RiskClaim_Id = @RiskClaim_Id

SELECT	DISTINCT O.RiskClaim_Id
		,Inc_DbId
		,Per_DbId
		,O.ParentName
		,O.LocalName
		,O.IsKeyAttractor
		,Org_DbId
		,UPPER(ISNULL(OrganisationName,@Msg)) OrganisationName
		,OrganisationPersonLinkType
		,OrganisationPersonLinkType_Text
		,ISNULL(dbo.fnFormatAddress (a.SubBuilding, A.BuildingName, a.BuildingNumber, a.Street, a.Locality, a.Town, a.County, a.Postcode),@Msg) AS OrganisationsAddresses
		,'Need To Fix This !' AS OrganisationsVehicles
		,OrganisationType
		,OrganisationType_Text
		,PaymentsToDate
		,RegisteredNumber
		,Reserve
		,VatNumber
		,ISNULL(NULLIF(Telephone,''),@Msg) Telephone
		,A.Org_ID
	    ,CASE
			WHEN @Person_ID IS NULL THEN O.OrganisationPersonLinkType_Text + ' - ' + O.OrganisationName
			ELSE O.OrganisationPersonLinkType_Text + ' for ' + P.FullName + ' - ' + O.OrganisationName
		 END ReportHeader
		,O.MessageHeader
		,LinkedName
		,ISNULL(LinkedAddress,@NKD) LinkedAddress
		,ISNULL(LinkedTelephones,@NKD) LinkedTelephones
		,ISNULL(LinkedWebsites,@NKD) LinkedWebsites
		,Email
		,Website
FROM  dbo.rpt_Level_One_Report_Organisation O
LEFT JOIN dbo.rpt_Level_One_Report_Addresses A ON O.Org_DbId = A.Org_ID
LEFT JOIN	(
			SELECT RiskClaim_Id, Vehicle_Id, Person_Id, FullName
			FROM dbo.rpt_Level_One_Report_Person
			WHERE RiskClaim_Id = @RiskClaim_Id
			AND Person_Id = @Person_ID
			GROUP BY RiskClaim_Id, Vehicle_Id, Person_Id, FullName
			) P ON P.RiskClaim_Id = @RiskClaim_Id AND P.Person_Id = @Person_ID
WHERE O.RiskClaim_Id = @RiskClaim_Id 
AND (Per_DbId = @Person_ID OR @Person_ID IS NULL)
AND O.ParentName = @ParentName


