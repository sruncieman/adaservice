﻿CREATE PROCEDURE [dbo].[usp_rpt_Level_One_ClaimDetails] 
@RiskClaim_Id INT
AS

DECLARE @Msg VARCHAR(50) = 'Not Provided' 

SELECT   RiskClaim_Id
		,LatestVersion
		,CreateDate
		,LastRunDate
		,ISNULL(IncidentType, @Msg) AS IncidentType
		,ISNULL(ClaimNumber, @Msg) AS ClaimNumber
		,ISNULL(ClaimStatus, @Msg) AS ClaimStatus
		,IncidentDate
		,IncidentDateTime
		,ISNULL(ClaimCode, @Msg) AS ClaimCode
		,CASE 
			WHEN Reserve IS NOT NULL AND Reserve >=0 THEN '£' + CONVERT(VARCHAR,Reserve,1) 
			WHEN Reserve IS NOT NULL AND Reserve <0 THEN REPLACE('(£' + CONVERT(VARCHAR,Reserve,1) +')','-','')
			ELSE @Msg
		 END Reserve
		 ,CASE 
			WHEN PaymentsToDate IS NOT NULL AND PaymentsToDate >=0 THEN '£' + CONVERT(VARCHAR,PaymentsToDate,1) 
			WHEN PaymentsToDate IS NOT NULL AND PaymentsToDate <0 THEN REPLACE('(£' + CONVERT(VARCHAR,PaymentsToDate,1) +')','-','')
			ELSE @Msg
		 END PaymentsToDate
		,ISNULL(Location, @Msg) AS Location
		,ISNULL(Circumstances, @Msg) AS Circumstances
		,ISNULL(PolicyNumber, @Msg) AS PolicyNumber
		,PolicyStartDate
		,PolicyEndDate
		,ISNULL(Broker, @Msg) AS [Broker]
		,ISNULL(NULLIF(PolicyType,'Unknown'),@Msg) AS PolicyType
		,ISNULL(NULLIF(CoverType,'Unknown'),@Msg) AS CoverType
FROM dbo.rpt_Level_One_Report_Incident_And_Policy 
WHERE RiskClaim_Id = @RiskClaim_Id


