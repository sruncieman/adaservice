﻿
/****** Object:  StoredProcedure [dbo].[usp_rpt_Level_One_Report_Person]    Script Date: 02/11/2014 12:59:16 ******/
CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Person]

	@RiskClaim_Id INT,
	@Vehicle_Id INT

AS

DECLARE  @Msg VARCHAR(50) = 'Not Provided'
		,@NKD VARCHAR(50) = 'No Keoghs Data Match'

SELECT   P.Person_Id
		,P.RiskClaim_Id
		,P.Vehicle_Id
		,P.ConfirmedAliases
		,ISNULL(NULLIF(P.Gender,'Unknown'),@Msg) Gender
		,ISNULL(NULLIF(P.Salutation,'Unknown'),@Msg) Salutation
		,ISNULL(NULLIF(UPPER(P.FullName),'Unknown'),@Msg) FullName
		,P.DateOfBirth
		,ISNULL(NULLIF(P.MobileNumber,'Unknown'),@Msg) MobileNumber
		,ISNULL(NULLIF(P.NINumber,'Unknown'),@Msg) NINumber
		,P.PartyType
		,P.SubPartyType
		,ISNULL(P.PossibleAlias,@NKD) PossibleAlias
		,ISNULL(P.Occupation,@NKD) Occupation
		,ISNULL(P.OtherLinkedAddresses,@NKD) OtherLinkedAddresses
		,ISNULL(P.LinkedTelephone,@NKD) LinkedTelephone
		,P.SortOrder
		,ISNULL(NULLIF(dbo.fnFormatAddress (A.SubBuilding, A.BuildingName, A.BuildingNumber, A.Street, A.Locality, A.Town, A.County, A.Postcode),''),@Msg) AS Address_
		--,REPLACE(P.ReportHeader,'Involvement Unknown - ', 'Involvement Unknown - Details Unknown') + ', ' + Veh.ReportHeader ReportHeader
		,P.PartyType + ' in ' + Veh.VehicleIncidentLink_Text + ' - ' + P.FullName  ReportHeader
		,ISNULL(NULLIF(P.Telephone,'Unknown'),@Msg) Telephone
		,P.Nationality
		,P.DrivingLicenceNumber
		,P.PassportNumber
		,P.OccupationClaim
		,P.EmailAddress
		,P.BankAccount
FROM dbo.rpt_Level_One_Report_Person P
LEFT JOIN dbo.rpt_Level_One_Report_Addresses A 
	ON A.RiskClaim_Id = P.RiskClaim_Id
	AND A.Person_ID = P.Person_Id
	AND A.Vehicle_ID = P.Vehicle_Id
LEFT JOIN	(
			SELECT RiskClaim_Id, Vehicle_Id, ReportHeader, VehicleIncidentLink_Text
			FROM dbo.rpt_Level_One_Report_Vehicles
			WHERE RiskClaim_Id = @RiskClaim_Id 
			AND Vehicle_Id = @Vehicle_Id
			GROUP BY RiskClaim_Id, Vehicle_Id, ReportHeader, VehicleIncidentLink_Text
			) Veh ON Veh.RiskClaim_Id = P.RiskClaim_Id AND Veh.Vehicle_Id = P.Vehicle_Id
WHERE P.RiskClaim_Id = @RiskClaim_Id 
AND P.Vehicle_Id = @Vehicle_Id



