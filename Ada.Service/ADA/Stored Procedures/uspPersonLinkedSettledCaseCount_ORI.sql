﻿ 
 CREATE PROCEDURE [dbo].[uspPersonLinkedSettledCaseCount_ORI]
 
	 @PersonId INT,
	 @IncidentId INT,
	 @IncludeConfirmed BIT ,
	 @IncludeUnconfirmed BIT,
	 @IncludeTentative BIT,
	 @StartDate SMALLDATETIME,
	 @EndDate SMALLDATETIME,
	 @OutcomeCategoryId INT,
	 @ClaimTypeId INT
 
 AS
 
/*
IF Parameter @ClaimTypeId equals a positive value then the where becomes equal to @ClaimTypeId 
IF Parameter @ClaimTypeId equals a negative value then the where becomes not equal to (@ClaimTypeId changed to a positive value)
IF Parameter @ClaimTypeId is null then no where is not applied
*/

 
DECLARE @WHERE NVARCHAR(500) =
	CASE  WHEN @ClaimTypeId < 0 THEN  'WHERE I.ClaimType_Id !=  ' + CONVERT(NVARCHAR(50) ,ABS(@ClaimTypeId))
	ELSE CASE  WHEN @ClaimTypeId >= 0 THEN  'WHERE I.ClaimType_Id = ' + CONVERT(NVARCHAR(50) ,@ClaimTypeId) ELSE ''  END
END

DECLARE @FUNCTION_PARAMS NVARCHAR(1000) = '(' + CONVERT(NVARCHAR(50) ,@PersonId) + ', ' +   CONVERT(NVARCHAR(50), @IncludeConfirmed)  + ', ' +   CONVERT(NVARCHAR(50), @IncludeUnconfirmed) + ', ' +    
CONVERT(NVARCHAR(50), @IncludeTentative) +') UNION SELECT ' +  CONVERT(NVARCHAR(50), dbo.fn_CheckLinkConfidence(@PersonId, @IncludeConfirmed, @IncludeUnconfirmed, @IncludeUnconfirmed)) + ')'

DECLARE @SQL NVARCHAR(2500) = '
SELECT
	COUNT(*) OUT_Count 
FROM (
	SELECT MOR.Id, Incident_Id
	 
	FROM 
	Incident2PersonOutcome MOR
	INNER JOIN 
	OutcomeCategories OC 
	ON MOR.MannerOfResolution = OC.CategoryText
	AND OC.CategoryTypeId = ' + CONVERT(NVARCHAR(50), @OutcomeCategoryId) + '


	WHERE  Incident_ID IN(
						SELECT Incident_ID FROM Incident2PersonOutcome MOR 
						INNER JOIN OutcomeCategories OC 
						ON MOR.MannerOfResolution = OC.CategoryText
						WHERE
							(Person_Id IN(SELECT Id FROM dbo.fn_GetPersonTree' + @FUNCTION_PARAMS + ') 
							AND (Incident_ID  != ' + CONVERT(NVARCHAR(50) ,@IncidentId) + '))
							AND  (Person_Id NOT IN (SELECT Id FROM dbo.fn_GetPersonTree' + @FUNCTION_PARAMS + ')
							--AND (LinkConfidence = ' + CASE WHEN @IncludeConfirmed = 1 THEN '0' ELSE CASE WHEN @IncludeUnconfirmed = 1 THEN '1' ELSE CASE WHEN @IncludeTentative = 1 THEN '2' END END END + ')
							
) AS A						

INNER JOIN Incident I  


ON A.Incident_Id = I.Id '

SET @SQL += @WHERE

PRINT @SQL

EXECUTE sp_executesql @SQL
