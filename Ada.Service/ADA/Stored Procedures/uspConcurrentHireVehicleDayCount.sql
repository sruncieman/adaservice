﻿CREATE PROCEDURE [dbo].[uspConcurrentHireVehicleDayCount]
(
 @VehicleID INT
,@RiskClaimID  INT
,@IncludeThisVehicleId BIT
,@IncludeConfirmed BIT
,@IncludeUnconfirmed BIT
,@IncludeTentative BIT
,@HireStartDate SMALLDATETIME
,@HireEndDate SMALLDATETIME
)
AS
SET NOCOUNT ON

;WITH CTE_Results AS
	(
	SELECT  V2P.[HireStartDate], V2P.[HireEndDate]
	FROM [dbo].[Vehicle2Person] V2P
	WHERE EXISTS	(
					SELECT _V2P.[Id]
					FROM [dbo].[Vehicle2Person] _V2P
					INNER JOIN dbo.RiskClaim RC ON RC.Id = V2P.RiskClaim_Id
					WHERE _V2P.Id = V2P.Id
					AND _V2P.[Vehicle_Id] IN (SELECT Id FROM [dbo].[fn_GetVehicleTree](@VehicleID, @IncludeConfirmed ,@IncludeUnconfirmed, @IncludeTentative, @IncludeThisVehicleId))
					AND RC.Incident_Id != (SELECT Incident_Id FROM dbo.RiskClaim WHERE Id = @RiskClaimID)
					AND _V2P.[HireStartDate] <= @HireEndDate
					AND _V2P.[HireEndDate] >= @HireStartDate
					AND _V2P.[ADARecordStatus] = 0
					AND _V2P.[VehicleLinkType_Id] = 4 --Hirer
					GROUP BY _V2P.[Id]
					)
	UNION ALL
	SELECT @HireStartDate, @HireEndDate
	)

SELECT DATEDIFF(DAY,(SELECT MAX([HireStartDate]) FROM CTE_Results),(SELECT MIN([HireEndDate]) FROM CTE_Results)) + 1 ConcurrentHireVehicleDayCount