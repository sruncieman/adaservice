﻿
/*
CREATE PROCEDURE [dbo].[uspInitialiseDatabaseLookups]


AS

DELETE FROM [dbo].[RiskUser]
DELETE FROM [dbo].[RiskTeam]
DELETE FROM [dbo].[RiskRole]
DELETE FROM dbo.RiskClaim
DELETE FROM dbo.RiskBatch
DELETE FROM dbo.RiskOriginalFile
DELETE FROM dbo.RiskClient2RiskExternalServices
DELETE FROM [dbo].[RiskExternalServices]
DELETE FROM [dbo].[RiskExternalServices]
DELETE FROM dbo.RiskWordFilter
DELETE FROM [dbo].[RiskClient]

DELETE FROM   [dbo].[InsurersClients]
SET IDENTITY_INSERT [dbo].[InsurersClients] ON
INSERT [dbo].[InsurersClients] ([Id], [ClientName], [RecordStatus], [ADARecordStatus]) 
SELECT [Id], [ClientName], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[InsurersClients] 
SET IDENTITY_INSERT [dbo].[InsurersClients] OFF

SET IDENTITY_INSERT [dbo].[RiskClient] ON
INSERT INTO [dbo].[RiskClient] (Id, ClientName, AmberThreshold, RedThreshold, EmailDomain, ExpectedFileExtension, LevelOneReportDelay, LevelTwoReportDelay, ClaimsActivePeriodInDays, ScoreClosedClaims,InsurersClients_ID)
SELECT Id, ClientName, AmberThreshold, RedThreshold, EmailDomain, ExpectedFileExtension, LevelOneReportDelay, LevelTwoReportDelay, ClaimsActivePeriodInDays, ScoreClosedClaims,InsurersClients_ID
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[RiskClient]
SET IDENTITY_INSERT [dbo].[RiskClient] OFF
 
SET IDENTITY_INSERT [dbo].[RiskTeam] ON
INSERT INTO [dbo].[RiskTeam](Id, [TeamName], [RiskClient_Id])
SELECT Id, [TeamName], [RiskClient_Id]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[RiskTeam] 
SET IDENTITY_INSERT [dbo].[RiskTeam] OFF
 
SET IDENTITY_INSERT [dbo].[RiskRole] ON
INSERT [dbo].[RiskRole] ([Id], [Name])
SELECT [Id], [Name]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[RiskRole]
SET IDENTITY_INSERT [dbo].[RiskRole] OFF
 
SET IDENTITY_INSERT [dbo].[RiskUser] ON
INSERT INTO [dbo].[RiskUser]([Id], [RiskRole_Id], [UserName], [Password], [RiskTeam_Id], [RiskClient_Id], [FirstName], [LastName], [TelephoneNumber], [LastGoodLoginDate], [BadLoginCount])
SELECT [Id], [RiskRole_Id], [UserName], [Password], [RiskTeam_Id], [RiskClient_Id], [FirstName], [LastName], [TelephoneNumber], [LastGoodLoginDate], [BadLoginCount]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[RiskUser]
SET IDENTITY_INSERT [dbo].[RiskUser] OFF

DELETE FROM [dbo].[VehicleType]
SET IDENTITY_INSERT [dbo].[VehicleType] ON
INSERT [dbo].[VehicleType] ([Id], [Text], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Text], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[VehicleType]
SET IDENTITY_INSERT [dbo].[VehicleType] OFF

DELETE FROM  [dbo].[VehicleTransmission]
SET IDENTITY_INSERT [dbo].[VehicleTransmission] ON
INSERT INTO [dbo].[VehicleTransmission] ([Id], [Text], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Text], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[VehicleTransmission]
SET IDENTITY_INSERT [dbo].[VehicleTransmission] OFF

DELETE FROM  [dbo].[VehicleMake]
SET IDENTITY_INSERT [dbo].[VehicleMake] ON
INSERT INTO [dbo].[VehicleMake] ([Id], [Make], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Make], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[VehicleMake]
SET IDENTITY_INSERT [dbo].[VehicleMake] OFF

DELETE FROM   [dbo].[VehicleLinkType]
SET IDENTITY_INSERT [dbo].[VehicleLinkType] ON
INSERT [dbo].[VehicleLinkType] ([Id], [LinkText], [RecordStatus], [ADARecordStatus])
SELECT [Id], [LinkText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[VehicleLinkType]
SET IDENTITY_INSERT [dbo].[VehicleLinkType] OFF

DELETE FROM   [dbo].[VehicleFuel]
SET IDENTITY_INSERT [dbo].[VehicleFuel] ON
INSERT [dbo].[VehicleFuel] ([Id], [FuelText], [RecordStatus], [ADARecordStatus])
SELECT [Id], [FuelText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[VehicleFuel]
SET IDENTITY_INSERT [dbo].[VehicleFuel] OFF

DELETE FROM   [dbo].[VehicleColour] 
SET IDENTITY_INSERT [dbo].[VehicleColour] ON
INSERT [dbo].[VehicleColour] ([Id], [Colour], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Colour], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[VehicleColour]
SET IDENTITY_INSERT [dbo].[VehicleColour] OFF

DELETE FROM  [dbo].[VehicleCategoryOfLoss]
SET IDENTITY_INSERT [dbo].[VehicleCategoryOfLoss] ON
INSERT [dbo].[VehicleCategoryOfLoss] ([Id], [CatText], [RecordStatus], [ADARecordStatus])
SELECT [Id], [CatText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[VehicleCategoryOfLoss]
SET IDENTITY_INSERT [dbo].[VehicleCategoryOfLoss] OFF

DELETE FROM   [dbo].[TelephoneType] 
SET IDENTITY_INSERT [dbo].[TelephoneType] ON
INSERT [dbo].[TelephoneType] ([Id], [PhoneText], [RecordStatus], [ADARecordStatus])
SELECT [Id], [PhoneText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[TelephoneType]
SET IDENTITY_INSERT [dbo].[TelephoneType] OFF

DELETE FROM  [dbo].[TelephoneLinkType]
SET IDENTITY_INSERT [dbo].[TelephoneLinkType] ON
INSERT [dbo].[TelephoneLinkType] ([Id], [Text], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Text], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[TelephoneLinkType]
SET IDENTITY_INSERT [dbo].[TelephoneLinkType] OFF

DELETE FROM   [dbo].[SubPartyType]
SET IDENTITY_INSERT [dbo].[SubPartyType] ON
INSERT [dbo].[SubPartyType] ([Id], [SubPartyText], [RecordStatus], [ADARecordStatus]) 
SELECT [Id], [SubPartyText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[SubPartyType]
SET IDENTITY_INSERT [dbo].[SubPartyType] OFF

DELETE FROM   [dbo].PolicyPaymentType 
SET IDENTITY_INSERT [dbo].PolicyPaymentType ON
INSERT [dbo].PolicyPaymentType ([Id], [Text], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Text], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].PolicyPaymentType
SET IDENTITY_INSERT [dbo].PolicyPaymentType OFF

DELETE FROM   [dbo].[Salutation]
SET IDENTITY_INSERT [dbo].[Salutation] ON
INSERT [dbo].[Salutation] ([Id], [Text], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Text], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[Salutation]
SET IDENTITY_INSERT [dbo].[Salutation] OFF

DELETE FROM   [dbo].[PolicyType] 
SET IDENTITY_INSERT [dbo].[PolicyType] ON
INSERT [dbo].[PolicyType] ([Id], [Text], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Text], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[PolicyType]
SET IDENTITY_INSERT [dbo].[PolicyType] OFF

DELETE FROM   [dbo].[PolicyLinkType] 
SET IDENTITY_INSERT [dbo].[PolicyLinkType] ON
INSERT [dbo].[PolicyLinkType] ([Id], [LinkText], [RecordStatus], [ADARecordStatus])
SELECT [Id], [LinkText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[PolicyLinkType]
SET IDENTITY_INSERT [dbo].[PolicyLinkType] OFF

DELETE FROM   [dbo].[PolicyCoverType]
SET IDENTITY_INSERT [dbo].[PolicyCoverType] ON
INSERT [dbo].[PolicyCoverType] ([Id], [CoverText], [RecordStatus], [ADARecordStatus])
SELECT [Id], [CoverText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[PolicyCoverType]
SET IDENTITY_INSERT [dbo].[PolicyCoverType] OFF

DELETE FROM   [dbo].[Person2PersonLinkType] 
SET IDENTITY_INSERT [dbo].[Person2PersonLinkType] ON
INSERT [dbo].[Person2PersonLinkType] ([Id], [LinkText], [RecordStatus], [ADARecordStatus])
SELECT [Id], [LinkText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[Person2PersonLinkType] 
SET IDENTITY_INSERT [dbo].[Person2PersonLinkType] OFF

DELETE FROM  [dbo].[Person2OrganisationLinkType]
SET IDENTITY_INSERT [dbo].[Person2OrganisationLinkType] ON
INSERT [dbo].[Person2OrganisationLinkType] ([Id], [LinkTypeText], [RecordStatus], [ADARecordStatus]) 
SELECT [Id], [LinkTypeText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[Person2OrganisationLinkType] 
SET IDENTITY_INSERT [dbo].[Person2OrganisationLinkType] OFF

DELETE FROM  [dbo].[PaymentCardType]
SET IDENTITY_INSERT [dbo].[PaymentCardType] ON
INSERT [dbo].[PaymentCardType] ([Id], [CardTypeText], [RecordStatus], [ADARecordStatus])
SELECT [Id], [CardTypeText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[PaymentCardType] 
SET IDENTITY_INSERT [dbo].[PaymentCardType] OFF

DELETE FROM  [dbo].[PartyType]
SET IDENTITY_INSERT [dbo].[PartyType] ON
INSERT [dbo].[PartyType] ([Id], [PartyTypeText], [RecordStatus], [ADARecordStatus]) 
SELECT [Id], [PartyTypeText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[PartyType] 
SET IDENTITY_INSERT [dbo].[PartyType] OFF

DELETE FROM   [dbo].[OrganisationType]
SET IDENTITY_INSERT [dbo].[OrganisationType] ON
INSERT [dbo].[OrganisationType] ([Id], [TypeText], [RecordStatus], [ADARecordStatus]) 
SELECT [Id], [TypeText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[OrganisationType] 
SET IDENTITY_INSERT [dbo].[OrganisationType] OFF

DELETE FROM  [dbo].[OrganisationStatus] 
SET IDENTITY_INSERT [dbo].[OrganisationStatus] ON
INSERT [dbo].[OrganisationStatus] ([Id], [StatusText], [RecordStatus], [ADARecordStatus]) 
SELECT [Id], [StatusText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[OrganisationStatus] 
SET IDENTITY_INSERT [dbo].[OrganisationStatus] OFF

DELETE FROM  [dbo].[OrganisationLinkType]
SET IDENTITY_INSERT [dbo].[OrganisationLinkType] ON
INSERT [dbo].[OrganisationLinkType] ([Id], [LinkType], [RecordStatus], [ADARecordStatus])
SELECT [Id], [LinkType], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[OrganisationLinkType] 
SET IDENTITY_INSERT [dbo].[OrganisationLinkType] OFF

DELETE FROM  [dbo].[MojCRMStatus]
SET IDENTITY_INSERT [dbo].[MojCRMStatus] ON
INSERT [dbo].[MojCRMStatus] ([Id], [Status], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Status], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[MojCRMStatus] 
SET IDENTITY_INSERT [dbo].[MojCRMStatus] OFF

DELETE FROM   [dbo].MojStatus 
SET IDENTITY_INSERT [dbo].MojStatus ON
INSERT [dbo].[MojStatus] ([Id], [Text], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Text], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].MojStatus 
SET IDENTITY_INSERT [dbo].MojStatus OFF

DELETE FROM   [dbo].[KeoghsOffice] 
SET IDENTITY_INSERT [dbo].[KeoghsOffice] ON
INSERT [dbo].[KeoghsOffice] ([Id], [OfficeName], [RecordStatus], [ADARecordStatus])
SELECT [Id], [OfficeName], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[KeoghsOffice] 
SET IDENTITY_INSERT [dbo].[KeoghsOffice] OFF

DELETE FROM   [dbo].[KeoghsCaseStatus]
SET IDENTITY_INSERT [dbo].[KeoghsCaseStatus] ON
INSERT [dbo].[KeoghsCaseStatus] ([Id], [StatusText], [RecordStatus], [ADARecordStatus])
SELECT [Id], [StatusText], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[KeoghsCaseStatus] 
SET IDENTITY_INSERT [dbo].[KeoghsCaseStatus] OFF

DELETE FROM   [dbo].[KeoghsCase2IncidentLinkType]
SET IDENTITY_INSERT [dbo].[KeoghsCase2IncidentLinkType] ON
INSERT [dbo].[KeoghsCase2IncidentLinkType] ([Id], [LinkType], [RecordStatus], [ADARecordStatus]) 
SELECT [Id], [LinkType], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[KeoghsCase2IncidentLinkType] 
SET IDENTITY_INSERT [dbo].[KeoghsCase2IncidentLinkType] OFF

DELETE FROM  [dbo].[Incident2VehicleLinkType]
DELETE FROM  [dbo].[Incident] 
DELETE FROM  [dbo].[IncidentType] 

SET IDENTITY_INSERT [dbo].[IncidentType] ON
INSERT [dbo].[IncidentType] ([Id], [IncidentType], [RecordStatus], [ADARecordStatus])
SELECT [Id], [IncidentType], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[IncidentType] 
SET IDENTITY_INSERT [dbo].[IncidentType] OFF

SET IDENTITY_INSERT [dbo].[Incident2VehicleLinkType] ON
INSERT [dbo].[Incident2VehicleLinkType] ([Id], [Text], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Text], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[Incident2VehicleLinkType] 
SET IDENTITY_INSERT [dbo].[Incident2VehicleLinkType] OFF

DELETE FROM   [dbo].[Gender] 
SET IDENTITY_INSERT [dbo].[Gender] ON
INSERT [dbo].[Gender] ([Id], [Text], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Text], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[Gender] 
SET IDENTITY_INSERT [dbo].[Gender] OFF

DELETE FROM   [dbo].[County]
SET IDENTITY_INSERT [dbo].[County] ON
INSERT [dbo].[County] ([Id], [County], [RecordStatus], [ADARecordStatus])
SELECT [Id], [County], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[County] 
SET IDENTITY_INSERT [dbo].[County] OFF

DELETE FROM   [dbo].[ClaimType] 
SET IDENTITY_INSERT [dbo].[ClaimType] ON
INSERT [dbo].[ClaimType] ([Id], [ClaimType], [RecordStatus], [ADARecordStatus])
SELECT [Id], [ClaimType], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[ClaimType] 
SET IDENTITY_INSERT [dbo].[ClaimType] OFF

DELETE FROM   [dbo].[ClaimStatus] 
SET IDENTITY_INSERT [dbo].[ClaimStatus] ON
INSERT [dbo].[ClaimStatus] ([Id], [Text], [RecordStatus], [ADARecordStatus]) 
SELECT [Id], [Text], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[ClaimStatus] 
SET IDENTITY_INSERT [dbo].[ClaimStatus] OFF

DELETE FROM  [dbo].[AddressType]
SET IDENTITY_INSERT [dbo].[AddressType] ON
INSERT [dbo].[AddressType] ([Id], [Text], [RecordStatus], [ADARecordStatus])
SELECT [Id], [Text], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[AddressType] 
SET IDENTITY_INSERT [dbo].[AddressType] OFF

DELETE FROM  [dbo].[AddressLinkType]
SET IDENTITY_INSERT [dbo].[AddressLinkType] ON
INSERT [dbo].[AddressLinkType] ([Id], [LinkType], [RecordStatus], [ADARecordStatus])
SELECT [Id], [LinkType], [RecordStatus], [ADARecordStatus]
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[AddressLinkType] 
SET IDENTITY_INSERT [dbo].[AddressLinkType] OFF
 
SET IDENTITY_INSERT [dbo].[Incident] ON
INSERT [dbo].[Incident] (Id, IncidentId, KeoghsEliteReference, IncidentType_Id, ClaimType_Id, IfbReference, IncidentDate, FraudRingName, PaymentsToDate, Reserve, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
SELECT Id, IncidentId, KeoghsEliteReference, IncidentType_Id, ClaimType_Id, IfbReference, IncidentDate, FraudRingName, PaymentsToDate, Reserve, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[Incident]  
SET IDENTITY_INSERT [dbo].[Incident] OFF
 
DELETE FROM   [dbo].[RiskOriginalFile]
SET IDENTITY_INSERT [dbo].[RiskOriginalFile] ON
INSERT [dbo].[RiskOriginalFile] (PkId, Id, Description, FileData)
SELECT PkId, Id, Description, FileData
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[RiskOriginalFile]  
SET IDENTITY_INSERT [dbo].[RiskOriginalFile] OFF
 
DBCC CHECKIDENT ('RISKBATCH', RESEED, 0)
SET IDENTITY_INSERT [dbo].[RiskBatch] ON
INSERT [dbo].[RiskBatch] (Id, RiskClient_Id, RiskUser_Id, BatchReference, BatchEntityType, ClientBatchReference, BatchStatus, BatchStatusLastModified, RiskOriginalFile_Id, CreatedDate, CreatedBy, VerificationResults, MappingResults, TotalClaimsReceived, TotalNewClaims, TotalModifiedClaims, TotalClaimsLoaded, TotalClaimsInError, TotalClaimsSkipped, LoadingDurationInMs)
SELECT Id, RiskClient_Id, RiskUser_Id, BatchReference, BatchEntityType, ClientBatchReference, BatchStatus, BatchStatusLastModified, RiskOriginalFile_Id, CreatedDate, CreatedBy, VerificationResults, MappingResults, TotalClaimsReceived, TotalNewClaims, TotalModifiedClaims, TotalClaimsLoaded, TotalClaimsInError, TotalClaimsSkipped, LoadingDurationInMs
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[RiskBatch]  
SET IDENTITY_INSERT [dbo].[RiskBatch] OFF

DBCC CHECKIDENT ('RISKCLAIM', RESEED, 0)
SET IDENTITY_INSERT [dbo].[RiskClaim] ON
INSERT [dbo].[RiskClaim] (Id, RiskBatch_Id, Incident_Id, MdaClaimRef, BaseRiskClaim_Id, SuppliedClaimStatus, ClaimStatus, ClaimStatusLastModified, ClientClaimRefNumber, LevelOneRequestedCount, LevelTwoRequestedCount, LevelOneRequestedWhen, LevelTwoRequestedWhen, LevelOneRequestedWho, LevelTwoRequestedWho, LevelOneUnavailable, LevelTwoUnavailable, SourceReference, SourceEntityEncodeFormat, SourceEntityXML, EntityClassType, CreatedDate, CreatedBy, LatestVersion, CallbackRequested, CallbackRequestedWhen, CallbackRequestedWho, ExternalServicesRequired, ExternalServicesRequestsRequired, ClaimReadStatus, LoadingDurationInMs, ValidationResults, CleansingResults)
SELECT Id, RiskBatch_Id, Incident_Id, MdaClaimRef, BaseRiskClaim_Id, SuppliedClaimStatus, ClaimStatus, ClaimStatusLastModified, ClientClaimRefNumber, LevelOneRequestedCount, LevelTwoRequestedCount, LevelOneRequestedWhen, LevelTwoRequestedWhen, LevelOneRequestedWho, LevelTwoRequestedWho, LevelOneUnavailable, LevelTwoUnavailable, SourceReference, SourceEntityEncodeFormat, SourceEntityXML, EntityClassType, CreatedDate, CreatedBy, LatestVersion, CallbackRequested, CallbackRequestedWhen, CallbackRequestedWho, ExternalServicesRequired, ExternalServicesRequestsRequired, ClaimReadStatus, LoadingDurationInMs, ValidationResults, CleansingResults
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[RiskClaim]  
SET IDENTITY_INSERT [dbo].[RiskClaim] OFF
 
DELETE FROM [dbo].[RiskExternalServices]
DBCC CHECKIDENT ('RiskExternalServices', RESEED, 1)

TRUNCATE TABLE [dbo].[RiskClient2RiskExternalServices]

SET IDENTITY_INSERT [dbo].[RiskExternalServices] ON
INSERT [RiskExternalServices] (Id, ServiceName, MaskValue, CurrentStatus, StatusMessage, ServiceEnabled, StatusLastChecked, StatusTimeout)
SELECT Id, ServiceName, MaskValue, CurrentStatus, StatusMessage, ServiceEnabled, StatusLastChecked, StatusTimeout
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[RiskExternalServices]  
SET IDENTITY_INSERT [dbo].[RiskExternalServices] OFF

INSERT [dbo].[RiskClient2RiskExternalServices] (RiskClient_Id, RiskExternalServices_Id, ServiceUsage, ServicePassword, ServiceUserName, Tracesmart_Use_Passport, Tracesmart_Use_Telephone, Tracesmart_Use_Driving, Tracesmart_Use_Birth, Tracesmart_Use_Smartlink, Tracesmart_Use_NI, Tracesmart_Use_CardNumber, Tracesmart_Use_BankAccount, Tracesmart_Use_Mobile, Tracesmart_Use_Crediva, Tracesmart_Use_CreditActive, Tracesmart_Use_NHS, Tracesmart_Use_Cardavs, Tracesmart_Use_MPan, Tracesmart_Use_Address, Tracesmart_Use_DeathScreen, Tracesmart_Use_DoB, Tracesmart_Use_Sanction, Tracesmart_Use_Insolvency, Tracesmart_Use_CCJ)
SELECT RiskClient_Id, RiskExternalServices_Id, ServiceUsage, ServicePassword, ServiceUserName, Tracesmart_Use_Passport, Tracesmart_Use_Telephone, Tracesmart_Use_Driving, Tracesmart_Use_Birth, Tracesmart_Use_Smartlink, Tracesmart_Use_NI, Tracesmart_Use_CardNumber, Tracesmart_Use_BankAccount, Tracesmart_Use_Mobile, Tracesmart_Use_Crediva, Tracesmart_Use_CreditActive, Tracesmart_Use_NHS, Tracesmart_Use_Cardavs, Tracesmart_Use_MPan, Tracesmart_Use_Address, Tracesmart_Use_DeathScreen, Tracesmart_Use_DoB, Tracesmart_Use_Sanction, Tracesmart_Use_Insolvency, Tracesmart_Use_CCJ
FROM [UKBOLINTEL-TST3].[[$(MDA)]-MasterGen].[dbo].[RiskClient2RiskExternalServices]
*/