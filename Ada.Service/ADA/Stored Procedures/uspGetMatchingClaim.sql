﻿

CREATE PROCEDURE [dbo].[uspGetMatchingClaim]
	@XML XML,
	@FieldsFlag BIT OUTPUT,
	@SupressOutput BIT = 1
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
 
IF @XML IS NULL
BEGIN
	SELECT CAST(NULL AS INT) ID, CAST(NULL AS INT)  RuleNo, CAST(NULL AS INT)  PriorityGroup, CAST(NULL AS INT)  MatchType 
	RETURN
END
 
--DECLARE @XML XML = '<Claim><RiskClaim_ID></RiskClaim_ID><ClientClaimReference></ClientClaimReference><ClientID></ClientID><IncidentDate></IncidentDate><IncidentTime></IncidentTime><IncidentLocation></IncidentLocation><Vehicles><Vehicle></Vehicle></Vehicles><People><Person></Person></People><IncidentCircumstances></IncidentCircumstances><Handsets><Handset HandsetImei="" /></Handsets></Claim>'
--DECLARE	@FieldsFlag BIT 

--XML DEFINITION NEEDED TO ACCOUNT FOR MISSING TAGS
DECLARE @XML_DEF VARCHAR(MAX) ='<Claim><RiskClaim_ID></RiskClaim_ID><ClientClaimReference></ClientClaimReference><ClientID></ClientID><IncidentDate></IncidentDate><IncidentTime></IncidentTime><IncidentLocation></IncidentLocation><Vehicles><Vehicle></Vehicle></Vehicles><People><Person></Person></People><IncidentCircumstances></IncidentCircumstances><Handsets><Handset HandsetImei="" /></Handsets></Claim>'
DECLARE @XML_Check XML = '<Data>' +  REPLACE(CONVERT(VARCHAR(MAX), @XML), 'UNKNOWN', '') +  @XML_DEF + '</Data>'
SET @FieldsFlag = 0 --RETURN VALUE 0 IF NOT ENOUGHT DATA SUPPLIED
DECLARE @T AS TABLE(TID INT NOT NULL IDENTITY(1,1), RuleNo INT, PriorityGroup INT, RiskClaimFlag INT, HasRiskClaim_Id BIT)
DECLARE @Ret TABLE(ID INT, RuleNo INT, PriorityGroup INT, MatchType INT)

--WeAreGoingToAssignAllSingularXMLValuesToParameters
DECLARE  @RiskClaim_ID VARCHAR(MAX)
		,@ClientClaimReference VARCHAR(MAX)
		,@ClientID VARCHAR(MAX)
		,@IncidentDate VARCHAR(MAX)
		,@IncidentTime VARCHAR(MAX)
		,@IncidentLocation VARCHAR(MAX)
		,@IncidentCircumstances VARCHAR(MAX)

SELECT   @RiskClaim_ID = CONVERT(VARCHAR(MAX), XKC.Data.query('data(RiskClaim_ID)'))
		,@ClientClaimReference = REPLACE(CONVERT(VARCHAR(MAX), XKC.Data.query('data(ClientClaimReference)')),'GIOS','')
		,@ClientID = CONVERT(VARCHAR(MAX), XKC.Data.query('data(ClientID)'))
		,@IncidentDate = CONVERT(VARCHAR(MAX), XKC.Data.query('data(IncidentDate)'))
		,@IncidentTime = CONVERT(VARCHAR(MAX), XKC.Data.query('data(IncidentTime)'))
		,@IncidentLocation = CONVERT(VARCHAR(MAX), XKC.Data.query('data(IncidentLocation)'))
		,@IncidentCircumstances = CONVERT(VARCHAR(MAX), XKC.Data.query('data(IncidentCircumstances)'))
FROM @XML.nodes('//Claim') XKC(Data) 

--WeNeedToExtractTheClientIDFromTheXMLAndTranslateToTheRiskClientID
DECLARE @RiskClientTableType  AS dbo.RiskClientTableType
INSERT INTO @RiskClientTableType (RiskClient_ID)
SELECT [Id]
FROM [dbo].[RiskClient] RC
WHERE RC.[InsurersClients_Id] = @ClientID

--CREATING TABLE OF POSSABLE RULES TO MATCH
INSERT INTO @T(RuleNo, PriorityGroup, RiskClaimFlag, HasRiskClaim_Id)
SELECT RuleNo, PriorityGroup, RiskClaimFlag, HasRiskClaim_ID FROM dbo.fnGetMatchingClaimToProcess(@XML_Check, @RiskClientTableType) ORDER BY PriorityGroup ,RuleNo
DECLARE @ROWCOUNT INT = @@ROWCOUNT	
IF @ROWCOUNT = 0  --NOT ENOUGHT DATA SUPPLIED EXIT PROCEDURE
	GOTO MatchEnd
ELSE
	SET @FieldsFlag = 1

----------------------------------------------
--GetThePeopleAndVehicleIDsAndConfirmedLinks
----------------------------------------------
--GetAllThePeopleIDsFromTheXMLAndConfirmedLinks
DECLARE @Per TABLE (ID INT IDENTITY(1,1), PerID INT)

IF (SELECT OBJECT_ID('tempdb..#PerAll')) IS NOT NULL
	DROP TABLE #PerAll
CREATE TABLE #PerAll  (ID INT IDENTITY(1,1), PerID INT)

INSERT INTO @Per (PerID)
SELECT CONVERT(VARCHAR(50), PEO.Data.query('data(.)')) PersonID
FROM @XML.nodes('//Claim/People/Person') PEO(Data)
DECLARE @PerCount INT = @@ROWCOUNT
DECLARE @C INT = 1

IF @PerCount > 0
BEGIN
	DECLARE @PerID INT
	WHILE @C <= @PerCount
		BEGIN
			SELECT @PerID = PerID FROM @Per WHERE ID = @C
			INSERT INTO #PerAll (PerID)
			SELECT Id
			FROM dbo.fn_GetPersonTree(@PerID,0,1,0,1) --Include Unconfirmed and This

			SELECT @C +=1
		END
END

--GetAllTheVecIDsFromTheXMLAndConfirmedLinks
DECLARE @Vec TABLE (ID INT IDENTITY(1,1), VecID INT)

IF (SELECT OBJECT_ID('tempdb..#VecAll')) IS NOT NULL
	DROP TABLE #VecAll
CREATE TABLE #VecAll (ID INT IDENTITY(1,1), VecID INT)

INSERT INTO @Vec (VecID)
SELECT CONVERT(varchar(50), VEH.Data.query('data(.)')) VehicleID
FROM @XML.nodes('//Claim/Vehicles/Vehicle') VEH(Data)
DECLARE @VecCount INT = @@ROWCOUNT
SELECT @C = 1

IF @VecCount > 0
BEGIN
	DECLARE  @VecID INT
	WHILE @C <= @VecCount
		BEGIN
			SELECT @VecID = VecID FROM @Vec WHERE ID = @C
			INSERT INTO #VecAll (VecID)
			SELECT Id
			FROM [dbo].[fn_GetVehicleTree] (@VecID,0,1,0,1) --Include Unconfirmed and This

			SELECT @C +=1
		END
END

--GetAllTheHandsetIEMIs
IF (SELECT OBJECT_ID('tempdb..#HandAll')) IS NOT NULL
	DROP TABLE #HandAll
CREATE TABLE #HandAll (ID INT IDENTITY(1,1), HandID INT, LinkConfidence TINYINT)

INSERT INTO #HandAll (HandID, LinkConfidence)
SELECT HAND.Id, 0
FROM @XML.nodes('//Claim/Handsets/Handset') XHAND(Data)
INNER JOIN [dbo].[Handset] HAND ON HAND.HandsetIMEI = CONVERT(varchar(50), XHAND.Data.query('data(@HandsetImei)'))
DECLARE @HandCount INT = @@ROWCOUNT

--IfWeHaveARiskClaimIDBuildTheTable
IF NULLIF(@RiskClaim_ID,'') IS NOT NULL
BEGIN
	IF (SELECT OBJECT_ID('tempdb..#RiskClaim')) IS NOT NULL
		DROP TABLE #RiskClaim
	CREATE TABLE #RiskClaim (Incident_ID INT, ADARecordStatus TINYINT, BaseRiskClaim_Id INT)
	
	INSERT INTO #RiskClaim (Incident_ID, ADARecordStatus, BaseRiskClaim_Id)
	SELECT SQ1.Incident_ID, SQ1.ADARecordStatus, SQ1.BaseRiskClaim_Id
	FROM	(
			SELECT _KC21.Incident_ID Incident_ID, _KC21.ADARecordStatus ADARecordStatus, _KC21.BaseRiskClaim_Id BaseRiskClaim_Id FROM dbo.KeoghsCase2Incident _KC21 WHERE _KC21.BaseRiskClaim_Id = @RiskClaim_ID AND _KC21.ADARecordStatus = 0
			UNION ALL
			SELECT _I2P.Incident_ID Incident_ID, _I2P.ADARecordStatus ADARecordStatus, _I2P.BaseRiskClaim_Id BaseRiskClaim_Id FROM dbo.Incident2Person _I2P WHERE _I2P.BaseRiskClaim_Id = @RiskClaim_ID AND _I2P.ADARecordStatus = 0
			UNION ALL
			SELECT _I2V.Incident_ID Incident_ID, _I2V.ADARecordStatus ADARecordStatus, _I2V.BaseRiskClaim_Id BaseRiskClaim_Id FROM dbo.Incident2Vehicle _I2V WHERE _I2V.BaseRiskClaim_Id = @RiskClaim_ID AND _I2V.ADARecordStatus = 0
			) SQ1
	GROUP BY SQ1.Incident_ID, SQ1.ADARecordStatus, SQ1.BaseRiskClaim_Id

	CREATE INDEX IX_#RiskClaim ON #RiskClaim (Incident_ID, ADARecordStatus, BaseRiskClaim_Id)
END

--LOOP EACH RULE, EXIT WHEN A MATCH IS FOUND
DECLARE @RuleNo INT
DECLARE @PriorityGroup INT
DECLARE @RiskClaimFlag TINYINT
DECLARE @ExecWithRcId BIT
DECLARE @ExecWithOutRcId BIT
DECLARE @Count INT = 1
DECLARE @SQL NVARCHAR(MAX)

WHILE @Count <= @ROWCOUNT
BEGIN

	SELECT	@RuleNo			= RuleNo,	
			@PriorityGroup	= PriorityGroup,	
			@RiskClaimFlag	= RiskClaimFlag,
			@ExecWithRcId	= CASE WHEN HasRiskClaim_Id = 1 AND RiskClaimFlag > 0 THEN 1 ELSE 0 END,
			@ExecWithOutRcId= CASE WHEN RiskClaimFlag = 0 OR RiskClaimFlag = 2 THEN 1 ELSE 0 END
	FROM @T 
	WHERE TID = @Count

	--SELECT @RuleNo, GETDATE()
	
	--PROCESS ALL TENTATIVE (Risk Claim Flag) UNION SAME RULE GROUPS 
	IF @RiskClaimFlag = 0
	BEGIN
		
		DECLARE @GrpCount INT = @Count
		DECLARE @GrpCountEnd INT = (SELECT MAX(TID) FROM @T WHERE PriorityGroup = (SELECT PriorityGroup FROM @T WHERE TID = @Count) AND RiskClaimFlag = 0 AND TID >= @Count)
		SET @SQL = dbo.fnGetMatchingClaimTSql (@RuleNo, 0, @RiskClientTableType, @RiskClaim_ID,@ClientClaimReference ,@ClientID ,@IncidentDate ,@IncidentTime ,@IncidentLocation ,@IncidentCircumstances ) 
		
		WHILE @GrpCount < @GrpCountEnd
		BEGIN
			SET @GrpCount += 1
			SELECT @RuleNo = RuleNo FROM @T WHERE TID = @GrpCount
			SET @SQL +=   CHAR(10) + 'UNION ALL' + CHAR(10) + dbo.fnGetMatchingClaimTSql (@RuleNo, 0, @RiskClientTableType, @RiskClaim_ID,@ClientClaimReference ,@ClientID ,@IncidentDate ,@IncidentTime ,@IncidentLocation ,@IncidentCircumstances ) 

		END
		IF @SupressOutput = 0
		BEGIN
			PRINT '################################# RiskClaimFlag = 0 START #######################################'
			PRINT ''
			PRINT @SQL
			PRINT ''
			PRINT '################################# RiskClaimFlag = 0 END #########################################'
		END
		INSERT INTO @Ret
		EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
		IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
		
		SET @Count = @GrpCountEnd
		
	END
	ELSE
	BEGIN
		--PROCESS ALL CONF (Risk Claim Flag) If we have a RC_ID 
		IF @ExecWithRcId = 1
		BEGIN
			SET @SQL = dbo.fnGetMatchingClaimTSql (@RuleNo, 1, @RiskClientTableType, @RiskClaim_ID,@ClientClaimReference ,@ClientID ,@IncidentDate ,@IncidentTime ,@IncidentLocation ,@IncidentCircumstances ) 
			IF @SupressOutput = 0
			BEGIN
				PRINT '------------------------------------WithRcId-------------------------------------------'
				PRINT @SQL
				PRINT ''
			END
			INSERT INTO @Ret
			EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
				
		END
	
		--PROCESS ALL UN-CONF (Risk Claim Flag) 
		IF @ExecWithOutRcId = 1
		BEGIN
			SET @SQL = dbo.fnGetMatchingClaimTSql (@RuleNo, 0, @RiskClientTableType, @RiskClaim_ID,@ClientClaimReference ,@ClientID ,@IncidentDate ,@IncidentTime ,@IncidentLocation ,@IncidentCircumstances ) 
			IF @SupressOutput = 0
			BEGIN
				PRINT '----------------------------------WithOutRcId------------------------------------------'
				PRINT @SQL
				PRINT ''
			END
			INSERT INTO @Ret
			EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
		END
	END
	
SET @Count += 1

END 

MatchEnd:
	
SELECT ID, MIN(RuleNo) AS RuleNo, PriorityGroup, MatchType FROM @Ret 
GROUP BY ID, PriorityGroup, MatchType 
ORDER BY RuleNo DESC

-----------------------
--Tidy
-----------------------
IF (SELECT OBJECT_ID('tempdb..#PerAll')) IS NOT NULL
	DROP TABLE #PerAll
IF (SELECT OBJECT_ID('tempdb..#VecAll')) IS NOT NULL
	DROP TABLE #VecAll
IF (SELECT OBJECT_ID('tempdb..#HandAll')) IS NOT NULL
	DROP TABLE #HandAll
