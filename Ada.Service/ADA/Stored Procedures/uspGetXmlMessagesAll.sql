﻿
CREATE PROCEDURE uspGetXmlMessagesAll

	
	@XML XML


AS


DECLARE @TblReportHeading TABLE(
	Id VARCHAR(10),
	BaseRiskClaimId VARCHAR(10),
	RiskClaimId VARCHAR(10),
	ReportHeading VARCHAR(500),
	GroupID INT)

DECLARE  @TblMessage TABLE(
	Id VARCHAR(10),
	MessageHigh VARCHAR(100),
	MessageMedium VARCHAR(100),
	MessageLow VARCHAR(100),
	RuleSetId VARCHAR(10),
	Score VARCHAR(10),
	GroupID INT)

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:PassportRisk', 0, 1
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:PassportRisk', 1, 1

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:IncidentPolicy', 0, 2
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:IncidentPolicy', 1, 2

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:IncidentVehicles', 0, 3
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:IncidentVehicles', 1, 3

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:AddressRisk', 0,  4
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:AddressRisk', 1, 4

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:BBPinRisk', 0, 5
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:BBPinRisk', 1, 5

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:DrivingLicenseRisk', 0, 6
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:DrivingLicenseRisk', 1, 6

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:EmailRisk', 0, 7
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:EmailRisk', 1, 7

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:FraudRingRisk', 0, 8
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:FraudRingRisk', 1, 8

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:IncidentRisk', 0, 9
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:IncidentRisk', 1, 9

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:IPAddressRisk', 0, 10
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:IPAddressRisk', 1, 10

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:KeoghsCaseRisk', 0, 11
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:KeoghsCaseRisk', 1, 11

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:NINumberRisk', 0,  12
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:NINumberRisk', 1, 12

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:OrganisationRisk', 0, 13
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:OrganisationRisk', 1, 13

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:PaymentCardRisk', 0, 14
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:PaymentCardRisk', 1, 14

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:PersonRisk', 0, 15
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:PersonRisk', 1, 15

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:PolicyRisk', 0, 16
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:PolicyRisk', 1, 16

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:VehicleRisk', 0, 17
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:VehicleRisk', 1, 17

INSERT INTO @TblMessage
EXEC uspGetXmlMessages @XML, '//a:WebSiteRisk', 0, 18
IF (@@ROWCOUNT > 0)
INSERT INTO @TblReportHeading
EXEC uspGetXmlMessages @XML, '//a:WebSiteRisk', 1, 18


SELECT T.Id,	BaseRiskClaimId,	RiskClaimId,	ReportHeading,	MessageHigh,	MessageMedium,	MessageLow,	RuleSetId,	Score
 FROM @TblReportHeading H
INNER JOIN @TblMessage T
ON H.Id = T.Id
AND H.GroupID = T.GroupID



