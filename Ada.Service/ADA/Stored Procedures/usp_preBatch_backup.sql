﻿CREATE PROCEDURE [dbo].[usp_preBatch_backup]
	@batch_no INT
--WITH EXECUTE AS 'SVC-ADA-PowerUser'
AS
BEGIN
	
	DECLARE @Date VARCHAR(12)
	DECLARE @FileName NVARCHAR(112)
	
	SET @Date = CONVERT(varchar,GETDATE(),112)
	
	SET @FileName =N'D:\SQLServerR2\Backup\MDA_'+CONVERT(varchar,GETDATE(),112)+ '_BATCH_' +CAST(@batch_no As Varchar)
	

	BACKUP DATABASE [MDA] TO  DISK = @Filename WITH NOFORMAT, NOINIT,  
	NAME = N'MDA-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10


END