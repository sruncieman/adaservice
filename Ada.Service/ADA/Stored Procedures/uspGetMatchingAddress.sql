﻿

CREATE PROCEDURE [dbo].[uspGetMatchingAddress]

	@RiskClaim_Id INT = NULL,
	@SubBuilding VARCHAR(50) =  NULL,
	@Building VARCHAR(50) =  NULL,
	@BuildingNumber VARCHAR(50) =  NULL,
	@Street VARCHAR(50) =  NULL,
	@Locality VARCHAR(50) =  NULL,
	@Town VARCHAR(50) =  NULL,
	@PostCode VARCHAR(50) =  NULL,
	@OrganisationName VARCHAR(255) = NULL,
	@FieldsFlag BIT OUTPUT 

AS


SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET @FieldsFlag = 0

--SomeLogicAroundTheNameForMoreMatches
SELECT @OrganisationName = REPLACE(REPLACE(REPLACE(@OrganisationName,'Limited',''),'Ltd',''),'PLC','')

DECLARE @T AS TABLE(TID INT NOT NULL IDENTITY(1,1), RuleNo INT, PriorityGroup INT, RiskClaimFlag INT, HasRiskClaim_Id BIT)
DECLARE @Ret TABLE(ID INT, RuleNo INT, PriorityGroup INT, MatchType INT)

INSERT INTO @T(RuleNo, PriorityGroup, RiskClaimFlag, HasRiskClaim_Id)
SELECT 	
	RuleNo, 
	PriorityGroup,
	RiskClaimFlag,
	CONVERT(BIT, ISNULL(@RiskClaim_Id, 0)) HasRiskClaim_Id
FROM dbo.MatchingRulesAddress
WHERE
		ISNULL(SubBuilding, 0 ) <= ISNULL(LEN(@SubBuilding), 0)
		AND	ISNULL(Building, 0 ) <= ISNULL(LEN(@Building), 0)
		AND	ISNULL(BuildingNumber, 0 ) <= ISNULL(LEN(@BuildingNumber), 0)
		AND	ISNULL(Street, 0 ) <= ISNULL(LEN(@Street), 0)
		AND	ISNULL(Locality, 0 ) <= ISNULL(LEN(@Locality), 0)
		AND	ISNULL(Town, 0 ) <= ISNULL(LEN(@Town), 0)
		AND	ISNULL(PostCode, 0 ) <= ISNULL(LEN(@PostCode), 0)
		AND	ISNULL(OrganisationName, 0 ) <= ISNULL(LEN(@OrganisationName), 0)
ORDER BY PriorityGroup

DECLARE @ROWCOUNT INT = @@ROWCOUNT	
IF @ROWCOUNT = 0  --NOT ENOUGHT DATA SUPPLIED EXIT PROCEDURE
	GOTO MatchEnd
ELSE
	SET @FieldsFlag = 1
	
DECLARE @RuleNo INT
DECLARE @PriorityGroup INT
DECLARE @RiskClaimFlag TINYINT
DECLARE @ExecWithRcId BIT
DECLARE @ExecWithOutRcId BIT
DECLARE @Count INT = 1
DECLARE @SQL NVARCHAR(MAX)



--LOOP EACH RULE, EXIT WHEN A MATCH IS FOUND

WHILE @Count <= @ROWCOUNT
BEGIN

	SELECT
		@RuleNo = RuleNo,	
		@PriorityGroup = PriorityGroup,	
		@RiskClaimFlag = RiskClaimFlag,
		@ExecWithRcId = CASE WHEN HasRiskClaim_Id = 1 AND RiskClaimFlag > 0 THEN 1 ELSE 0 END,
		@ExecWithOutRcId = CASE WHEN RiskClaimFlag = 0 OR RiskClaimFlag = 2 THEN 1 ELSE 0 END
	FROM @T WHERE TID = @Count
		
	
		
	--PROCESS ALL TENTATIVE (Risk Claim Flag) UNION SAME RULE GROUPS 
	IF @RiskClaimFlag = 0
	BEGIN
		DECLARE @GrpCount INT = @Count
		DECLARE @GrpCountEnd INT = (SELECT MAX(TID) FROM @T WHERE PriorityGroup = (SELECT PriorityGroup FROM @T WHERE TID = @Count) AND RiskClaimFlag = 0 AND TID >= @Count)
		SET @SQL = dbo.fnGetMatchingAddressTSql(@RuleNo, 0)  
		
		WHILE @GrpCount < @GrpCountEnd
		BEGIN
			SET @GrpCount += 1
			SELECT @RuleNo = RuleNo FROM @T WHERE TID = @GrpCount
			SET @SQL +=   CHAR(10) + 'UNION ALL' + CHAR(10) + dbo.fnGetMatchingAddressTSql(@RuleNo, 0) 

		END
			PRINT '################################# RiskClaimFlag = 0 START #######################################'
			PRINT ''
			PRINT @SQL
			PRINT ''
			PRINT '################################# RiskClaimFlag = 0 END #########################################'
		INSERT INTO @Ret
		EXEC sp_executesql @SQL ,
			N'@SubBuilding VARCHAR(50), @Building VARCHAR(50), @BuildingNumber VARCHAR(50), @Street VARCHAR(50), @Locality VARCHAR(50), @Town VARCHAR(50), @PostCode VARCHAR(50), @OrganisationName VARCHAR(255)',
			@SubBuilding = @SubBuilding, @Building = @Building, @BuildingNumber = @BuildingNumber, @Street = @Street, @Locality = @Locality, @Town = @Town, @PostCode = @PostCode, @OrganisationName = @OrganisationName

		IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
		
		SET @Count = @GrpCountEnd
		
	END
	ELSE
	BEGIN
		--PROCESS ALL CONF (Risk Claim Flag) If we have a RC_ID 
		IF @ExecWithRcId = 1
		BEGIN
			SET @SQL = dbo.fnGetMatchingAddressTSql(@RuleNo, 1) 
			PRINT ''
			PRINT '------------------------------------WithRcId-------------------------------------------'
			--PRINT 'Count: ' + CONVERT(VARCHAR(10), @Count)
			--PRINT 'ExecWithRcId: '	+	CONVERT(VARCHAR(10),@ExecWithRcId)
			--PRINT 'ExecWithOutRcId: ' +   CONVERT(VARCHAR(10),@ExecWithOutRcId)
			SET @SQL = REPLACE(@SQL, '1 AS MatchType', '0 AS MatchType')
			SET @SQL = REPLACE(@SQL, '2 AS MatchType', '0 AS MatchType')
			PRINT @SQL
			PRINT ''
			PRINT ''
			INSERT INTO @Ret
			EXEC sp_executesql @SQL ,
			N'@RiskClaim_Id INT, @SubBuilding VARCHAR(50), @Building VARCHAR(50), @BuildingNumber VARCHAR(50), @Street VARCHAR(50), @Locality VARCHAR(50), @Town VARCHAR(50), @PostCode VARCHAR(50), @OrganisationName VARCHAR(255)',
			@RiskClaim_Id = @RiskClaim_Id, @SubBuilding = @SubBuilding, @Building = @Building, @BuildingNumber = @BuildingNumber, @Street = @Street, @Locality = @Locality, @Town = @Town, @PostCode = @PostCode, @OrganisationName = @OrganisationName

			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
				
		END
		
		--PROCESS ALL UN-CONF (Risk Claim Flag) 
		IF @ExecWithOutRcId = 1
		BEGIN
			PRINT ''
			PRINT '----------------------------------WithOutRcId------------------------------------------'
			--PRINT 'Count: ' + CONVERT(VARCHAR(10), @Count)
			--PRINT 'ExecWithRcId: '	+	CONVERT(VARCHAR(10),@ExecWithRcId)
			--PRINT 'ExecWithOutRcId: ' +   CONVERT(VARCHAR(10),@ExecWithOutRcId)
		
			
			SET @SQL = dbo.fnGetMatchingAddressTSql(@RuleNo, 0) 
			PRINT @SQL
			PRINT ''
			PRINT ''
			INSERT INTO @Ret
			EXEC sp_executesql @SQL ,
			N'@SubBuilding VARCHAR(50), @Building VARCHAR(50), @BuildingNumber VARCHAR(50), @Street VARCHAR(50), @Locality VARCHAR(50), @Town VARCHAR(50), @PostCode VARCHAR(50), @OrganisationName VARCHAR(255)',
			@SubBuilding = @SubBuilding, @Building = @Building, @BuildingNumber = @BuildingNumber, @Street = @Street, @Locality = @Locality, @Town = @Town, @PostCode = @PostCode, @OrganisationName = @OrganisationName

			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
			
		END

	END
	
SET @Count += 1

END 


MatchEnd:
	

SELECT ID, MIN(RuleNo) AS RuleNo, PriorityGroup, MatchType FROM @Ret 
GROUP BY ID, PriorityGroup, MatchType 
ORDER BY RuleNo DESC
