﻿
CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Person_Dynamic_Data]
(@RiskClaim_Id INT
,@Vehicle_Id INT)

AS

SELECT B.ColumnName, B.Value
FROM	(
		SELECT   CAST(Nationality AS VARCHAR(100)) [Nationality]
				,CAST(DrivingLicenceNumber AS VARCHAR(100)) [Driving Licence Number]
				,CAST(PassportNumber AS VARCHAR(100)) [Passport Number]
				,CAST(OccupationClaim AS VARCHAR(100)) [Occupation]
				,CAST(EmailAddress AS VARCHAR(100)) [Email Address]
				,CAST(BankAccount AS VARCHAR(100)) [Bank Account]
		FROM dbo.rpt_Level_One_Report_Person
		WHERE RiskClaim_Id = @RiskClaim_Id
		AND Vehicle_Id = @Vehicle_Id
		) A
UNPIVOT (Value FOR ColumnName IN (
								 Nationality
								,[Driving Licence Number]
								,[Passport Number]
								,[Occupation]
								,[Email Address]
								,[Bank Account]
								)
		) AS B

