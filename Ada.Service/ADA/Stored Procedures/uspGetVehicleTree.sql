﻿
CREATE PROCEDURE [dbo].[uspGetVehicleTree]

	 @Id INT,
	 @includeConfirmed BIT,
	 @includeUnconfirmed BIT,
	 @includeTentative BIT,
	 @includeThis BIT

AS

SELECT Id FROM dbo.fn_GetVehicleTree(@Id, @includeConfirmed, @includeUnconfirmed, @includeTentative, @includeThis)




