﻿CREATE PROCEDURE [dbo].[uspGetMatchingVehicle]
	@XML XML,
	@FieldsFlag BIT OUTPUT
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--XML DEFINITION NEEDED TO ACCOUNT FOR MISSING TAGS
DECLARE @XML_DEF VARCHAR(MAX) = '<Vehicle RiskClaim_Id="" VehicleMake="" Model="" VehicleRegistration="" Colour="" VIN=""><Addresses><Address PafUPRN="" BuildingNumber="" PostCode="" /></Addresses></Vehicle>'
DECLARE @XML_Check XML = '<Data>' +  REPLACE(CONVERT(VARCHAR(MAX), @XML), 'UNKNOWN', '') +  @XML_DEF + '</Data>'

SET @FieldsFlag = 0 --RETURN VALUE 0 IF NOT ENOUGHT DATA SUPPLIED
DECLARE @T AS TABLE(TID INT NOT NULL IDENTITY(1,1), RuleNo INT, PriorityGroup INT, RiskClaimFlag INT, HasRiskClaim_Id BIT)
DECLARE @Ret TABLE(ID INT, RuleNo INT, PriorityGroup INT, MatchType INT)

--CREATING TABLE OF POSSABLE RULES TO MATCH
INSERT INTO @T(RuleNo, PriorityGroup, RiskClaimFlag, HasRiskClaim_Id)
SELECT RuleNo, PriorityGroup, RiskClaimFlag, HasRiskClaim_Id FROM fnGetMatchingVehicleToProcess (@XML_Check) ORDER BY PriorityGroup ,RuleNo
DECLARE @ROWCOUNT INT = @@ROWCOUNT	
IF @ROWCOUNT = 0  --NOT ENOUGHT DATA SUPPLIED EXIT PROCEDURE
	GOTO MatchEnd
ELSE
	SET @FieldsFlag = 1

--IfWeHaveRulesWeCanMatchLetsAssignXMLValuesToParameters
DECLARE  @RiskClaim_Id VARCHAR(MAX)
		,@VehicleMake VARCHAR(MAX)
		,@Model VARCHAR(MAX)
		,@VehicleRegistration VARCHAR(MAX)
		,@Colour VARCHAR(MAX)
		,@VIN VARCHAR(MAX)

SELECT   @RiskClaim_Id = XKC.Data.value('@RiskClaim_Id', 'VARCHAR(MAX)')
		,@VehicleMake = XKC.Data.value('@VehicleMake', 'VARCHAR(MAX)')
		,@Model = XKC.Data.value('@Model', 'VARCHAR(MAX)')
		,@VehicleRegistration = XKC.Data.value('@VehicleRegistration', 'VARCHAR(MAX)')
		,@Colour = XKC.Data.value('@Colour', 'VARCHAR(MAX)')
		,@VIN = XKC.Data.value('@VIN', 'VARCHAR(MAX)')
FROM @XML.nodes('//Vehicle') XKC(Data) 

IF OBJECT_ID('tempdb..#VehicleAddressDetails') IS NOT NULL
	DROP TABLE #VehicleAddressDetails
CREATE TABLE #VehicleAddressDetails (PafUPRN VARCHAR(MAX), BuildingNumber VARCHAR(MAX), PostCode VARCHAR(MAX) )

INSERT INTO #VehicleAddressDetails (PafUPRN, BuildingNumber, PostCode)
SELECT   XKC.Data.value('@PafUPRN', 'VARCHAR(MAX)') PafUPRN
		,XKC.Data.value('@BuildingNumber', 'VARCHAR(MAX)') BuildingNumber
		,XKC.Data.value('@PostCode', 'VARCHAR(Max)') PostCode
FROM @XML.nodes('//Vehicle/Addresses/Address') XKC(Data) 
	
DECLARE @RuleNo INT
DECLARE @PriorityGroup INT
DECLARE @RiskClaimFlag TINYINT
DECLARE @ExecWithRcId BIT
DECLARE @ExecWithOutRcId BIT
DECLARE @Count INT = 1
DECLARE @SQL NVARCHAR(MAX)

--LOOP EACH RULE, EXIT WHEN A MATCH IS FOUND
WHILE @Count <= @ROWCOUNT
BEGIN

	SELECT
		@RuleNo = RuleNo,	
		@PriorityGroup = PriorityGroup,	
		@RiskClaimFlag = RiskClaimFlag,
		@ExecWithRcId = CASE WHEN HasRiskClaim_Id = 1 AND RiskClaimFlag > 0 THEN 1 ELSE 0 END,
		@ExecWithOutRcId = CASE WHEN RiskClaimFlag = 0 OR RiskClaimFlag = 2 THEN 1 ELSE 0 END
	FROM @T WHERE TID = @Count
		
	--PROCESS ALL TENTATIVE (Risk Claim Flag) UNION SAME RULE GROUPS 
	IF @RiskClaimFlag = 0
	BEGIN
		
		DECLARE @GrpCount INT = @Count
		DECLARE @GrpCountEnd INT = (SELECT MAX(TID) FROM @T WHERE PriorityGroup = (SELECT PriorityGroup FROM @T WHERE TID = @Count) AND RiskClaimFlag = 0 AND TID >= @Count)
		SET @SQL = dbo.fnGetMatchingVehicleTSql(@RuleNo, 0, @RiskClaim_Id, @VehicleMake, @Model, @VehicleRegistration, @Colour, @VIN)
		
		WHILE @GrpCount < @GrpCountEnd
		BEGIN
			SET @GrpCount += 1
			SELECT @RuleNo = RuleNo FROM @T WHERE TID = @GrpCount
			SET @SQL +=   CHAR(10) + 'UNION ALL' + CHAR(10) + dbo.fnGetMatchingVehicleTSql(@RuleNo, 0, @RiskClaim_Id ,@VehicleMake , @Model, @VehicleRegistration ,@Colour, @VIN)

		END
			PRINT '################################# RiskClaimFlag = 0 START #######################################'
			PRINT ''
			PRINT @SQL
			PRINT '################################# RiskClaimFlag = 0 END #########################################'
		INSERT INTO @Ret
		EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
		IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
		
		SET @Count = @GrpCountEnd
		
	END
	ELSE
	BEGIN
		--PROCESS ALL CONF (Risk Claim Flag) If we have a RC_ID 
		IF @ExecWithRcId = 1
		BEGIN
			SET @SQL = dbo.fnGetMatchingVehicleTSql(@RuleNo, 1, @RiskClaim_Id, @VehicleMake, @Model, @VehicleRegistration, @Colour, @VIN)
			PRINT ''
			PRINT '------------------------------------WithRcId-------------------------------------------'

			PRINT @SQL
			INSERT INTO @Ret
			EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
				
		END
		
		--PROCESS ALL UN-CONF (Risk Claim Flag) 
		IF @ExecWithOutRcId = 1
		BEGIN
			PRINT ''
			PRINT '----------------------------------WithOutRcId------------------------------------------'

			SET @SQL = dbo.fnGetMatchingVehicleTSql(@RuleNo, 0, @RiskClaim_Id, @VehicleMake, @Model, @VehicleRegistration, @Colour, @VIN)
			PRINT @SQL
			INSERT INTO @Ret
			EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
			IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd
			
		END

	END
	
SET @Count += 1

END 

MatchEnd:

IF (SELECT COUNT(1) FROM @Ret) > 1
BEGIN
	--RemoveAnyDuplicationsIfTheresultSetReturnsMultipleMatches
	DECLARE @UniqueList TABLE (ID INT)
	DECLARE @IDString VARCHAR(MAX) = (SELECT (SELECT CAST(R.ID AS VARCHAR(MAX)) + ',' FROM @Ret R INNER JOIN [dbo].[Vehicle] V ON V.Id = R.ID ORDER BY V.VehicleId, V.Id FOR XML PATH (''))  IDString)
	DECLARE @LinkConfidence INT = (SELECT TOP 1 MatchType FROM @Ret)

	SELECT @IDString = LEFT(@IDString, LEN(@IDString) -1)

	INSERT INTO @UniqueList
	EXECUTE [dbo].[uspUniqueVehicle] @IDString, ',', @LinkConfidence

	--ReturnTheListUnFiltered
	SELECT R.ID, MIN(R.RuleNo) AS RuleNo, R.PriorityGroup, R.MatchType 
	FROM @Ret R
	INNER JOIN @UniqueList UL ON UL.ID = R.ID
	INNER JOIN [dbo].[Vehicle] V ON V.Id = R.ID 
	GROUP BY R.ID, R.PriorityGroup, R.MatchType, V.VehicleId, V.Id 
	ORDER BY V.VehicleId, V.Id 

END
ELSE
BEGIN
	--ReturnTheListUnFiltered
	SELECT ID, MIN(RuleNo) AS RuleNo, PriorityGroup, MatchType FROM @Ret GROUP BY ID, PriorityGroup, MatchType 
END

-----------------------
--Tidy
-----------------------
IF OBJECT_ID('tempdb..#VehicleAddressDetails') IS NOT NULL
	DROP TABLE #VehicleAddressDetails
