﻿
CREATE PROCEDURE [dbo].[uspADASearch]
/**************************************************************************************************/
-- ObjectName:		[dbo].[uspADASearch]
--
-- Description:		ADA Search					
--
-- RevisionHistory:
--		Date				Author			Modification
--		2016-03-09			Paul Allen		Created
--		2016-03-10			Paul Allen		Added Auditing
--											Added Address Search
--		2016-03-13			Paul Allen		Output Indviduial Matched On Fields
--											Added Vehicle Registration				
/**************************************************************************************************/
(
 @UserID				INT			 = NULL
,@FirstName				NVARCHAR(100)
,@LastName				NVARCHAR(100)
,@DateOfBirth			NVARCHAR(12)
,@AddressDetails		NVARCHAR(500) = NULL
,@PostCode				NVARCHAR(12)  = NULL
,@VehicleRegistration	NVARCHAR(20)  = NULL
,@CurrentPageNo			SMALLINT = 1
,@PageSize				SMALLINT = 50
,@OutputRowCount		INT	= 0 OUTPUT 
)
AS

SET NOCOUNT ON
SET FMTONLY OFF
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY
	------------------------------------
	--Create Audit Record
	-------------------------------------
	IF @UserID IS NOT NULL
	BEGIN
		DECLARE  @ReportParameters			NVARCHAR(1000) = 'FirstName:['+ISNULL(@FirstName,'')+'];LastName:['+ISNULL(@LastName,'')+'];DOB:['+ISNULL(@DateOfBirth,'')+'];AddressDetails:['+ISNULL(@AddressDetails,'')+'];PostCode:['+ISNULL(@PostCode,'')+'];VehicleRegistration:['+ISNULL(@VehicleRegistration,'')+']'
				,@RiskUserADASearchAudit_Id INT

		INSERT INTO [dbo].[RiskUserADASearchAudit] (UserId, ReportParameters, TotalRowCount, CurrentPageNo, PageSize)
		SELECT @UserID, @ReportParameters, ISNULL(@OutputRowCount,0), ISNULL(@CurrentPageNo,1), ISNULL(@PageSize,50)
		SELECT @RiskUserADASearchAudit_Id = SCOPE_IDENTITY()
	END

	------------------------------------
	--Declare any variables and Objects
	-------------------------------------
	DECLARE	 @SQL		NVARCHAR(1000)	= 'SELECT PER.Id, SAL.[Text],PER.[FirstName],PER.[MiddleName],PER.[LastName],PER.[DateOfBirth] FROM dbo.Person PER INNER JOIN [dbo].[Salutation] SAL ON SAL.Id = PER.Salutation_Id WHERE PER.ADARecordStatus = 0'
			,@SQLWhere	NVARCHAR(500)	= ''
			,@RowIDFrom INT = CASE WHEN @CurrentPageNo = 1 THEN 0 ELSE (@CurrentPageNo * @PageSize) - @PageSize END
			,@RowIDTo	INT = @CurrentPageNo * @PageSize -1
			
	SELECT @OutputRowCount = 0

	IF OBJECT_ID('tempdb..#PersonIds') IS NOT NULL
		DROP TABLE #PersonIds
	CREATE TABLE #PersonIds (Id INT NOT NULL, Title NVARCHAR(20) NULL, FirstName NVARCHAR(100) NULL, MiddleName NVARCHAR(100) NULL, LastName NVARCHAR(100) NULL, DateofBirth SMALLDATETIME NULL, FullName NVARCHAR(500) NULL, NameAndDOB NVARCHAR(500) NULL)
	ALTER TABLE #PersonIds ADD CONSTRAINT PK_#PersonIds_Id PRIMARY KEY CLUSTERED (Id)

	IF OBJECT_ID('tempdb..#AddressIds') IS NOT NULL
		DROP TABLE #AddressIds
	CREATE TABLE #AddressIds (Id INT NOT NULL)
	ALTER TABLE #AddressIds ADD CONSTRAINT PK_#AddressIds_Id PRIMARY KEY CLUSTERED (Id)

	IF OBJECT_ID('tempdb..#VehicleIds') IS NOT NULL
		DROP TABLE #VehicleIds
	CREATE TABLE #VehicleIds (Id INT NOT NULL)
	ALTER TABLE #VehicleIds ADD CONSTRAINT PK_#VehicleIds_Id PRIMARY KEY CLUSTERED (Id)

	IF OBJECT_ID('tempdb..#ResultsStage') IS NOT NULL
		DROP TABLE #ResultsStage
	CREATE TABLE #ResultsStage (RowID INT IDENTITY(1,1),Incident_Id INT NOT NULL, RiskClaim_Id INT NOT NULL, Title NVARCHAR(20) NULL, FirstName NVARCHAR(100) NULL, MiddleName NVARCHAR(100) NULL, LastName NVARCHAR(100) NULL, DateOfBirth SMALLDATETIME NULL, NameAndDOB NVARCHAR(500) NOT NULL, SubBuilding NVARCHAR(50) NULL, Building NVARCHAR(50) NULL, BuildingNumber NVARCHAR(50) NULL, Street NVARCHAR(255) NULL, Locality NVARCHAR(50) NULL, Town NVARCHAR(50) NULL, PostCode NVARCHAR(20) NULL, VehicleMake NVARCHAR(50) NULL, VehicleModel NVARCHAR(50) NULL, VehicleRegistration NVARCHAR(50) NULL ,Involvement NVARCHAR(50) NOT NULL, ClaimType NVARCHAR(50) NOT NULL, ClaimStatus NVARCHAR(50) NOT NULL, IncidentDate SMALLDATETIME NULL, SourceReference NVARCHAR(50), TypeOfClaim NVARCHAR(50), Address NVARCHAR(500) NULL, Vehicle NVARCHAR(150) NULL, MatchedOn NVARCHAR(2000) NULL, ClientName NVARCHAR(128) NULL, ClientClaimRef NVARCHAR(50) NULL, MatterRef NVARCHAR(50) NULL, IsKeoghsCase BIT NULL, Reference NVARCHAR(50) NULL, KeoghsCaseStatus NVARCHAR(50) NULL)

	IF OBJECT_ID('tempdb..#Results') IS NOT NULL
		DROP TABLE #Results
	CREATE TABLE #Results (RowID INT IDENTITY(0,1), MatchOn NVARCHAR(2000) NOT NULL, Title NVARCHAR(20) NULL, FirstName NVARCHAR(100) NULL, MiddleName NVARCHAR(100) NULL, LastName NVARCHAR(100) NULL, DateOfBirth SMALLDATETIME NULL, Address NVARCHAR(500) NULL, PostCode NVARCHAR(20) NULL, VehicleRegistration NVARCHAR(50) NULL, VehicleMake NVARCHAR(50) NULL, VehicleModel NVARCHAR(50) NULL, Involvement NVARCHAR(50) NOT NULL, Claim NVARCHAR(100) NOT NULL, Status NVARCHAR(50) NOT NULL, Date SMALLDATETIME NULL, Source NVARCHAR(50) NULL, Reference NVARCHAR(50) NULL, IsKeoghsCase BIT NOT NULL)
	ALTER TABLE #Results ADD CONSTRAINT PK_#Results_RowID PRIMARY KEY CLUSTERED (RowID)

	SELECT   @FirstName				= NULLIF(LTRIM(RTRIM(@FirstName)),'')
			,@LastName				= NULLIF(LTRIM(RTRIM(@LastName)),'')
			,@DateOfBirth			= NULLIF(LTRIM(RTRIM(@DateOfBirth)),'')
			,@AddressDetails		= NULLIF(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@AddressDetails,'.',''),',',''),'-',''))),'')
			,@PostCode				= NULLIF(LTRIM(RTRIM(REPLACE(@PostCode,' ',''))),'')
			,@VehicleRegistration	= NULLIF(LTRIM(RTRIM(REPLACE(@VehicleRegistration,' ',''))),'')

	--IfAllMyInputVariablesAreNullReturnResultSet
	IF @FirstName IS NULL AND @LastName IS NULL AND @DateOfBirth IS NULL AND @AddressDetails IS NULL AND @PostCode IS NULL AND @VehicleRegistration IS NULL
	BEGIN
		SELECT @OutputRowCount = -1
		GOTO ResultSet
	END

	--IfTheSearchIsLessThen3Characters
	IF LEN(ISNULL(@FirstName,'') + ISNULL(@LastName,'') + ISNULL(@AddressDetails,'') + ISNULL(@PostCode,'') + ISNULL(@VehicleRegistration,'')) <0
	BEGIN
		SELECT @OutputRowCount = -2
		GOTO ResultSet
	END

	------------------------------------
	--GetAListOfPeopleIDs
	-----------------------------------
	IF @FirstName IS NOT NULL OR @LastName IS NOT NULL OR @DateOfBirth IS NOT  NULL
	BEGIN
		SELECT @SQLWhere =  CASE WHEN @FirstName IS NOT NULL THEN ' AND [FirstName] LIKE '''+@FirstName+'%''' ELSE '' END + CASE WHEN @LastName IS NOT NULL THEN ' AND [LastName] LIKE '''+@LastName+'%''' ELSE '' END +  CASE WHEN @DateOfBirth IS NOT NULL THEN ' AND [DateOfBirth] = '''+@DateOfBirth+'''' ELSE '' END
		SELECT @SQL = @SQL + @SQLWhere
	
		INSERT INTO #PersonIds (Id,Title,FirstName,MiddleName,LastName,DateofBirth)
		EXECUTE SP_EXECUTESQL @SQL
	END

	------------------------------------
	--GetAListOfAddressIDs
	-----------------------------------
	IF @AddressDetails IS NOT NULL OR @PostCode IS NOT NULL 
	BEGIN
		SELECT  @SQL = 'SELECT AD.Id FROM dbo.Address AD WHERE AD.ADARecordStatus = 0'
			   ,@SQLWhere = CASE WHEN @AddressDetails IS NOT NULL THEN ' AND CONTAINS(AD.AddressSearch,'''+REPLACE(@AddressDetails,' ',' AND ')+''')' ELSE '' END  + CASE WHEN @PostCode IS NOT NULL THEN ' AND AD.PostCode LIKE'''+@PostCode+'%''' ELSE '' END

		SELECT @SQL = @SQL + @SQLWhere

		INSERT INTO #AddressIds (Id)
		EXECUTE SP_EXECUTESQL @SQL

		INSERT INTO #AddressIds (Id) VALUES (-99)
	END

	------------------------------------
	--GetAListOfVehicleIDs
	-----------------------------------
	IF @VehicleRegistration IS NOT NULL
	BEGIN
		SELECT  @SQL = 'SELECT VEH.Id FROM [dbo].[Vehicle] VEH WHERE VEH.ADARecordStatus = 0'
			   ,@SQLWhere = CASE WHEN @VehicleRegistration IS NOT NULL THEN ' AND VEH.[VehicleRegistration] LIKE '''+@VehicleRegistration+'%''' ELSE '' END

		SELECT @SQL = @SQL + @SQLWhere

		INSERT INTO #VehicleIds (Id)
		EXECUTE SP_EXECUTESQL @SQL

		INSERT INTO #VehicleIds (Id) VALUES (-99)
	END

	------------------------------------
	--GetAListOfPeopleIDsIfNoPeopleDetailsSupplied
	-----------------------------------
	IF (SELECT COUNT(1) FROM #PersonIds) = 0
	BEGIN
		INSERT INTO #PersonIds (Id,Title,FirstName,MiddleName,LastName,DateofBirth)
		SELECT DATA.Id,DATA.[Text],DATA.[FirstName],DATA.[MiddleName],DATA.[LastName],DATA.[DateOfBirth]
		FROM	(
				SELECT PER.Id,SAL.[Text],PER.[FirstName],PER.[MiddleName],PER.[LastName],PER.[DateOfBirth]
				FROM #AddressIds AD
				INNER JOIN [dbo].[Person2Address] P2A ON P2A.Address_Id = AD.Id AND P2A.ADARecordStatus = 0
				INNER JOIN [dbo].[Person] PER ON PER.Id = P2A.Person_Id AND PER.ADARecordStatus = 0
				INNER JOIN [dbo].[Salutation] SAL ON SAL.Id = PER.Salutation_Id
				UNION ALL
				SELECT PER.Id,SAL.[Text],PER.[FirstName],PER.[MiddleName],PER.[LastName],PER.[DateOfBirth]
				FROM #VehicleIds AD
				INNER JOIN [dbo].[Vehicle2Person] V2P ON V2P.Vehicle_Id = AD.Id AND V2P.ADARecordStatus = 0
				INNER JOIN [dbo].[Person] PER ON PER.Id = V2P.Person_Id AND PER.ADARecordStatus = 0
				INNER JOIN [dbo].[Salutation] SAL ON SAL.Id = PER.Salutation_Id
				) DATA
		GROUP BY DATA.Id,DATA.[Text],DATA.[FirstName],DATA.[MiddleName],DATA.[LastName],DATA.[DateOfBirth]
	END

	------------------------------------
	--GetTheResultsDataIfIHaveFoundAMatch
	-----------------------------------
	IF (SELECT COUNT(1) FROM #PersonIds) > 0
	BEGIN
		UPDATE #PersonIds
		SET  Title			= UPPER(LTRIM(RTRIM(NULLIF(NULLIF(Title,'Unknown'),''))))
			,FirstName		= UPPER(LTRIM(RTRIM(NULLIF(FirstName,''))))
			,MiddleName		= UPPER(LTRIM(RTRIM(NULLIF(MiddleName,''))))
			,LastName		= UPPER(LTRIM(RTRIM(NULLIF(LastName,''))))
			,DateofBirth	= UPPER(LTRIM(RTRIM(NULLIF(DateofBirth,''))))

		UPDATE #PersonIds
		SET  NameAndDOB =  STUFF(UPPER(ISNULL(' '+Title,'')+ISNULL(' '+FirstName,'')+ISNULL(' '+MiddleName,'')+ISNULL(' '+LastName,'')+ISNULL(' '+CONVERT(VARCHAR(20), [DateOfBirth], 103),'')),1,1,'')

		INSERT INTO #ResultsStage (Incident_Id,RiskClaim_Id,Title,FirstName,MiddleName,LastName,DateOfBirth,NameAndDOB,SubBuilding,Building,BuildingNumber,Street,Locality,Town,PostCode,VehicleMake,VehicleModel,VehicleRegistration,Involvement,ClaimType,ClaimStatus,IncidentDate,SourceReference,TypeOfClaim)
		SELECT   INC.Id Incident_Id
				,I2P.RiskClaim_Id
				,PER.Title
				,PER.FirstName
				,PER.MiddleName
				,PER.LastName
				,PER.DateofBirth
				,PER.NameAndDOB
				,AD.SubBuilding
				,AD.Building
				,AD.BuildingNumber
				,AD.Street
				,AD.Locality
				,AD.Town
				,AD.PostCode
				,VEH.[VehicleMake]
				,VEH.[Model]
				,VEH.[VehicleRegistration]
				,PT.PartyTypeText + ISNULL(' ' + NULLIF(SPT.SubPartyText,'Unknown'),'') Involvement
				,INCT.IncidentType ClaimType
				,CS.Text ClaimStatus
				,INC.IncidentDate IncidentDate
				,INC.SourceReference
				,CT.[ClaimType]
		FROM #PersonIds PER
		INNER JOIN [dbo].[Incident2Person] I2P ON I2P.[Person_Id] = PER.Id AND I2P.ADARecordStatus = 0
		INNER JOIN [dbo].[Incident] INC ON INC.Id = I2P.Incident_Id
		INNER JOIN	(
					SELECT	 _I2P.Person_Id
							,ISNULL(_P2A.RiskClaim_Id,_I2P.RiskClaim_Id) RiskClaim_Id
							,_AD.Id
							,_AD.SubBuilding
							,_AD.Building
							,_AD.BuildingNumber
							,_AD.Street
							,_AD.Locality
							,_AD.Town
							,_AD.PostCode
					FROM [dbo].[Incident2Person] _I2P
					LEFT JOIN [dbo].[Person2Address] _P2A ON _P2A.Person_Id = _I2P.Person_Id AND _P2A.RiskClaim_Id = _I2P.RiskClaim_Id AND _P2A.ADARecordStatus = 0 AND [AddressLinkType_Id] = 3 --CurrentAddress
					LEFT JOIN [dbo].[Address] _AD ON _AD.Id = _P2A.Address_Id AND _AD.ADARecordStatus = 0
					WHERE EXISTS (SELECT TOP 1 1 FROM #PersonIds _PER WHERE _PER.Id = _I2P.Person_Id)
					) AD ON AD.Person_Id = PER.Id AND AD.RiskClaim_Id = I2P.RiskClaim_Id
		LEFT JOIN	( 
					SELECT _V2P.Person_Id, _I2V.Incident_Id, _I2V.RiskClaim_Id, _VEH.Id, _VEH.[VehicleMake], _VEH.[Model], _VEH.[VehicleRegistration]
					FROM [dbo].[Vehicle2Person] _V2P
					INNER JOIN [dbo].[Incident2Vehicle] _I2V ON _I2V.Vehicle_Id = _V2P.Vehicle_Id AND _I2V.RiskClaim_Id = _V2P.RiskClaim_Id AND _I2V.ADARecordStatus = 0
					INNER JOIN [dbo].[Vehicle] _VEH ON _VEH.Id = _I2V.Vehicle_Id AND _VEH.Id = _V2P.Vehicle_Id AND _VEH.ADARecordStatus = 0
					WHERE EXISTS (SELECT TOP 1 1 FROM #PersonIds PER WHERE _V2P.Person_Id = PER.Id)
					AND _V2P.ADARecordStatus = 0
					GROUP BY _V2P.Person_Id, _I2V.Incident_Id, _I2V.RiskClaim_Id, _VEH.Id, _VEH.[VehicleMake], _VEH.[Model], _VEH.[VehicleRegistration]
					) VEH ON VEH.Person_Id = PER.Id AND VEH.Incident_Id = INC.Id AND VEH.RiskClaim_Id = I2P.RiskClaim_Id
		INNER JOIN [dbo].[ClaimStatus] CS ON CS.Id = I2P.[ClaimStatus_Id]
		INNER JOIN [dbo].[PartyType] PT ON PT.Id = I2P.PartyType_Id
		INNER JOIN [dbo].[SubPartyType] SPT ON SPT.Id = I2P.SubPartyType_Id
		INNER JOIN [dbo].[IncidentType] INCT ON INCT.Id = INC.[IncidentType_Id]
		INNER JOIN [dbo].[ClaimType] CT ON CT.Id = INC.[ClaimType_Id]
		WHERE (EXISTS (SELECT TOP 1 1 FROM #AddressIds AD_ WHERE AD_.Id = AD.Id) OR (SELECT COUNT(1) FROM #AddressIds) = 0)
		AND (EXISTS (SELECT TOP 1 1 FROM #VehicleIds VEH_ WHERE VEH_.Id = VEH.Id) OR (SELECT COUNT(1) FROM #VehicleIds) = 0)
		GROUP BY INC.Id
				,I2P.RiskClaim_Id
				,PER.Title
				,PER.FirstName
				,PER.MiddleName
				,PER.LastName
				,PER.DateofBirth
				,PER.NameAndDOB
				,AD.SubBuilding
				,AD.Building
				,AD.BuildingNumber
				,AD.Street
				,AD.Locality
				,AD.Town
				,AD.PostCode
				,VEH.[VehicleMake]
				,VEH.[Model]
				,VEH.[VehicleRegistration]
				,PT.PartyTypeText + ISNULL(' ' + NULLIF(SPT.SubPartyText,'Unknown'),'')
				,INCT.IncidentType 
				,CS.Text 
				,INC.IncidentDate
				,INC.SourceReference
				,CT.[ClaimType]

		UPDATE RS
		SET  ClientClaimRef = RC.ClientClaimRefNumber
			,ClientName		= CASE WHEN RCL.InternalClient = 1 THEN 'Keoghs' ELSE RCL.[ClientName] END
		FROM #ResultsStage RS
		INNER JOIN [dbo].[RiskClaim] RC ON RC.ID = RS.RiskClaim_Id
		INNER JOIN [dbo].[RiskBatch] RB ON RB.ID = RC.RiskBatch_Id
		INNER JOIN [dbo].[RiskClient] RCL ON RCL.Id = RB.RiskClient_Id

		UPDATE RS
		SET  MatterRef = KC.KeoghsEliteReference
			,IsKeoghsCase = 1
			,KeoghsCaseStatus = KCS.StatusText
		FROM #ResultsStage RS
		INNER JOIN [dbo].[KeoghsCase2Incident] KC2I ON KC2I.Incident_Id = RS.Incident_Id AND ADARecordStatus = 0
		INNER JOIN [dbo].[KeoghsCase] KC ON KC.Id = KC2I.KeoghsCase_Id
		INNER JOIN [dbo].[KeoghsCaseStatus] KCS ON KCS.Id = KC.CaseStatus_Id

		UPDATE #ResultsStage 
		SET ClaimType = ClaimType + ' <br/> ' + ISNULL(TypeOfClaim,'')
		WHERE IsKeoghsCase = 1

		UPDATE #ResultsStage
		SET  SubBuilding			= UPPER(LTRIM(RTRIM(NULLIF(SubBuilding,''))))
			,Building				= UPPER(LTRIM(RTRIM(NULLIF(Building,''))))
			,BuildingNumber			= UPPER(LTRIM(RTRIM(NULLIF(BuildingNumber,''))))
			,Street					= UPPER(LTRIM(RTRIM(NULLIF(Street,''))))
			,Locality				= UPPER(LTRIM(RTRIM(NULLIF(Locality,''))))
			,Town					= UPPER(LTRIM(RTRIM(NULLIF(Town,''))))
			,PostCode				= UPPER(LTRIM(RTRIM(NULLIF(PostCode,''))))
			,IncidentDate			= UPPER(LTRIM(RTRIM(NULLIF(IncidentDate,''))))
			,ClaimType				= REPLACE(ClaimType,'RTC','Motor')
			,ClientClaimRef			= UPPER(CASE WHEN ClientClaimRef = 'Dummy' THEN NULL ELSE LTRIM(RTRIM(NULLIF(ClientClaimRef,''))) END)
			,MatterRef				= UPPER(LTRIM(RTRIM(NULLIF(MatterRef,''))))
			,IsKeoghsCase			= ISNULL(IsKeoghsCase,0)
			,VehicleMake			= UPPER(LTRIM(RTRIM(NULLIF(VehicleMake,''))))
			,VehicleModel			= UPPER(LTRIM(RTRIM(NULLIF(VehicleModel,''))))
			,VehicleRegistration	= UPPER(LTRIM(RTRIM(NULLIF(VehicleRegistration,''))))
			,ClaimStatus			= ISNULL(ISNULL(NULLIF(ClaimStatus,'Unknown'),NULLIF(KeoghsCaseStatus,'Unknown')),'Unknown')

		UPDATE #ResultsStage
		SET  Address	= STUFF(ISNULL(' '+SubBuilding,'')+ISNULL(' '+Building,'')+ISNULL(' '+BuildingNumber,'')+ISNULL(' '+Street,'')+ISNULL(' '+Locality,'')+ISNULL(' '+Town,'')+ISNULL(' '+PostCode,''),1,1,'')
			,Reference	= ISNULL(COALESCE(MatterRef,ClientClaimRef,SourceReference),'-')
			,Vehicle	= STUFF(ISNULL(' | '+VehicleRegistration,'')+ISNULL(' | '+VehicleMake,'')+ISNULL(' | '+VehicleModel,''),1,3,'')
			
		UPDATE #ResultsStage
		SET MatchedOn =  STUFF(ISNULL('<br/>'+NameAndDOB,'') + ISNULL('<br/>'+Address,'') + ISNULL('<br/>'+Vehicle,''),1,5,'')

		INSERT INTO  #Results (MatchOn, Title, FirstName, MiddleName, LastName, DateOfBirth, Address, PostCode, VehicleRegistration, VehicleMake, VehicleModel, Involvement, Claim, Status, Date, Source, Reference, IsKeoghsCase)
		SELECT DISTINCT MatchedOn, Title, FirstName, MiddleName, LastName, DateofBirth,REPLACE(Address,' '+ PostCode,''), PostCode, VehicleRegistration, VehicleMake, VehicleModel, Involvement, ClaimType, ClaimStatus, IncidentDate , ClientName, Reference, IsKeoghsCase
		FROM #ResultsStage
		ORDER BY MatchedOn, IncidentDate DESC, Reference
		SELECT @OutputRowCount = @@ROWCOUNT
	END

	-----------------------------------------
	--ReturnTheResultSet
	-----------------------------------------
	ResultSet:
	IF @UserID IS NOT NULL
	BEGIN
		UPDATE [dbo].[RiskUserADASearchAudit]
		SET [TotalRowCount] = @OutputRowCount
		WHERE [Id] = @RiskUserADASearchAudit_Id
	END

	SELECT MatchOn, Title, FirstName, MiddleName, LastName, DateOfBirth, Address, PostCode, VehicleRegistration, VehicleMake, VehicleModel, Involvement, Claim, Status, Date, Source, Reference, IsKeoghsCase
	FROM #Results
	WHERE RowID >= @RowIDFrom
	AND RowID <= @RowIDTo

	----------------------------------------
	--Tidy
	-----------------------------------------
	IF OBJECT_ID('tempdb..#PersonIds') IS NOT NULL
		DROP TABLE #PersonIds
	IF OBJECT_ID('tempdb..#AddressIds') IS NOT NULL
		DROP TABLE #AddressIds
	IF OBJECT_ID('tempdb..#VehicleIds') IS NOT NULL
		DROP TABLE #VehicleIds
	IF OBJECT_ID('tempdb..#ResultsStage') IS NOT NULL
		DROP TABLE #ResultsStage
	IF OBJECT_ID('tempdb..#Results') IS NOT NULL
		DROP TABLE #Results

END TRY
BEGIN CATCH
	IF @UserID IS NOT NULL
	BEGIN
		UPDATE [dbo].[RiskUserADASearchAudit]
		SET [TotalRowCount] = @OutputRowCount
		WHERE [Id] = @RiskUserADASearchAudit_Id
	END

	SELECT NULL MatchOn, NULL Title, NULL FirstName, NULL MiddleName, NULL LastName, NULL DateOfBirth, NULL Address, NULL PostCode, NULL VehicleRegistration, NULL VehicleMake, NULL VehicleModel, NULL Involvement, NULL Claim, NULL Status, NULL Date, NULL Source, NULL Reference, NULL IsKeoghsCase
	SELECT @OutputRowCount = -99

	IF OBJECT_ID('tempdb..#PersonIds') IS NOT NULL
		DROP TABLE #PersonIds
	IF OBJECT_ID('tempdb..#AddressIds') IS NOT NULL
		DROP TABLE #AddressIds
	IF OBJECT_ID('tempdb..#VehicleIds') IS NOT NULL
		DROP TABLE #VehicleIds
	IF OBJECT_ID('tempdb..#ResultsStage') IS NOT NULL
		DROP TABLE #ResultsStage
	IF OBJECT_ID('tempdb..#Results') IS NOT NULL
		DROP TABLE #Results
END CATCH

