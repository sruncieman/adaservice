﻿
CREATE PROCEDURE [dbo].[uspGetMatchingTelephone]

 @TelephoneNumber VARCHAR(50)

AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  
IF LEN(@TelephoneNumber) < 6 
BEGIN
	
	SELECT NULL AS ID, NULL AS IntCode,  NULL AS AreaCode, NULL AS Number
	RETURN
END

DECLARE @ID INT
DECLARE @RetIntCode VARCHAR(3)
DECLARE @RetAreaCode VARCHAR(6)
DECLARE @RetNumber VARCHAR(50)

WHILE PATINDEX('%[^0-9]%', @TelephoneNumber) > 0
   BEGIN
        SET @TelephoneNumber = STUFF(@TelephoneNumber, PATINDEX('%[^0-9]%', @TelephoneNumber), 1, '')
   END

IF LEFT(@TelephoneNumber, 4) = '0044' 
	SET @RetNumber = RIGHT(@TelephoneNumber, LEN(@TelephoneNumber) - 4) 
ELSE IF LEFT(@TelephoneNumber, 2) = '44'
	SET @RetNumber = RIGHT(@TelephoneNumber, LEN(@TelephoneNumber) - 2) 
ELSE IF LEFT(@TelephoneNumber, 1) = '0'
	SET @RetNumber = RIGHT(@TelephoneNumber, LEN(@TelephoneNumber) - 1) 

SELECT @ID = Id, @RetIntCode = CountryCode,  @RetAreaCode = STDCode   FROM dbo.Telephone WHERE ADARecordStatus = 0 AND TelephoneNumber = '0' + @RetNumber

IF (@ID IS NULL)
BEGIN
	IF (LEFT(@RetNumber, 1) != '7')	
	BEGIN
		SELECT @RetAreaCode = AreaCode FROM UKAreaCodes WHERE AreaCode = LEFT(@RetNumber, LEN(AreaCode))
		IF (@RetAreaCode IS NOT NULL)
			SELECT  @RetIntCode = '+44', @RetAreaCode = '0' + @RetAreaCode
	END
END

	
SELECT @ID AS Id, @RetIntCode AS IntCode,  @RetAreaCode AS AreaCode, '0' + @RetNumber AS Number
