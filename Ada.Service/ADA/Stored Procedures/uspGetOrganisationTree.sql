﻿CREATE PROCEDURE [dbo].[uspGetOrganisationTree]

	 @Id INT,
	 @includeConfirmed BIT,
	 @includeUnconfirmed BIT,
	 @includeTentative BIT,
	 @includeThis BIT
WITH RECOMPILE
AS

SELECT ID 
FROM dbo.fn_GetOrganisationTree(@Id, @includeConfirmed, @includeUnconfirmed, @includeTentative, @includeThis)
	
	
	

