﻿
/****** Object:  StoredProcedure [dbo].[usp_rpt_Level_One_Report_Messages_Vehicle]    Script Date: 02/11/2014 12:59:16 ******/
CREATE PROCEDURE [dbo].[usp_rpt_Level_One_Report_Messages_Vehicle]

	@RiskClaim_Id INT,
	@Vehicle_Id INT

AS

SELECT   RiskClaim_Id
		,Per_DbId
		,Veh_DbId
		,Add_DbId
		,Org_DbId
		,LocalName
		,Rule_Key
		,Rule_Set_Key
		,ClaimNumber
		,Header
		,MessageType
		,REPLACE(Message,'~n',CHAR(13) + CHAR(10)) [Message]
		,MessageHeader
		,ReportHeader 
FROM rpt_Level_One_Report_Messages 
WHERE RiskClaim_Id = @RiskClaim_Id 
AND Veh_DbId = @Vehicle_Id
AND Rule_Set_Key IN ('VehicleAllClaimsExperience','VehicleOccupancy')



