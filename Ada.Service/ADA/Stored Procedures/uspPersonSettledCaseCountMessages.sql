﻿
 CREATE PROCEDURE [dbo].[uspPersonSettledCaseCountMessages]
 (
 @PersonId INT
,@IncidentID INT
,@IncludeThisPersonId BIT
,@IncludeConfirmed BIT 
,@IncludeUnconfirmed BIT
,@IncludeTentative BIT
,@StartDate SMALLDATETIME
,@EndDate SMALLDATETIME
,@OutcomeCategoryId INT
,@IsPotentialClaimant BIT = NULL
 )
AS

SELECT	 PT.PartyTypeText
		,SPT.SubPartyText
		,I.IncidentDate
		,REPLACE(CT.ClaimType, 'RTA - ', '') ClaimType
		,KC.KeoghsEliteReference
		,I2P.FiveGrading AS  I2P_FiveGrading
		,MOR.MannerOfResolution
		,MOR.FiveGrading AS  MOR_FiveGrading
		,CASE WHEN @IncludeUnconfirmed = 1 THEN  S.Text ELSE NULL END AS Salutation
		,CASE WHEN @IncludeUnconfirmed = 1 THEN  P.FirstName ELSE NULL END AS FirstName       
		,CASE WHEN @IncludeUnconfirmed = 1 THEN  P.LastName ELSE NULL END AS LastName
		,CASE WHEN @IncludeUnconfirmed = 1 THEN  P.DateOfBirth ELSE NULL END AS DateOfBirth
FROM dbo.Incident2PersonOutcome MOR 
INNER JOIN dbo.OutcomeCategories OC ON MOR.MannerOfResolution = OC.CategoryText
INNER JOIN dbo.Incident2Person AS I2P ON MOR.Person_Id = I2p.Person_Id AND MOR.Incident_Id = I2p.Incident_Id
INNER JOIN dbo.Person P ON MOR.Person_Id = P.Id
INNER JOIN dbo.PartyType PT ON I2P.PartyType_Id = PT.Id
INNER JOIN dbo.SubPartyType SPT ON I2P.SubPartyType_Id = SPT.Id
INNER JOIN dbo.Incident I ON I2p.Incident_Id = I.Id AND MOR.Incident_Id = I.Id
INNER JOIN dbo.ClaimType CT ON I.ClaimType_Id = CT.Id
INNER JOIN dbo.Salutation S ON P.Salutation_Id = S.Id
INNER JOIN dbo.KeoghsCase2Incident AS I2C ON I2P.Incident_Id = I2C.Incident_Id AND MOR.Incident_Id = I2C.Incident_Id
INNER JOIN dbo.KeoghsCase KC ON KC.Id = I2c.KeoghsCase_Id
WHERE MOR.Person_Id IN (SELECT Id FROM dbo.fn_GetPersonTree(@PersonId, @IncludeConfirmed ,@IncludeUnconfirmed, @IncludeTentative, @IncludeThisPersonId)) 
AND MOR.Incident_Id  != @IncidentID 
AND OC.CategoryTypeId = @OutcomeCategoryId
AND I.IncidentDate >= @StartDate
AND I.IncidentDate <= @EndDate






