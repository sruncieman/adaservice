﻿CREATE PROCEDURE dbo.uspPersonClaimExperienceCount
(
 @IncidentId			INT 
,@RiskClaimId			INT
,@PersonId				INT
,@PartyType				INT
,@SubPartyType			INT
,@IncludeThisPersonId	BIT
,@IncludeConfirmed		BIT
,@IncludeUnconfirmed	BIT
,@IncludeTentative		BIT
,@IncidentDate			SMALLDATETIME
,@StartDate				SMALLDATETIME
,@EndDate				SMALLDATETIME
,@IsPotentialClaimant	BIT
)
AS 
SET NOCOUNT ON

SELECT COUNT(DISTINCT I2P.[Incident_Id]) IncidentCount
FROM [dbo].[Incident2Person] I2P
INNER JOIN [dbo].[PotentialClaimant] PC ON PC.[PartyType] = @PartyType AND PC.[SubPartyType] = @SubPartyType
INNER JOIN [dbo].[Incident] INC ON INC.[Id] = I2P.[Incident_Id]
WHERE I2P.Person_Id IN (SELECT Id FROM dbo.fn_GetPersonTree(@PersonId, @IncludeConfirmed ,@IncludeUnconfirmed, @IncludeTentative, @IncludeThisPersonId))
AND I2P.[Incident_Id] != @IncidentId
AND INC.IncidentDate != @IncidentDate
AND INC.IncidentDate >= @StartDate
AND INC.IncidentDate <= @EndDate
AND PC.[Flag] = @IsPotentialClaimant
AND I2P.RiskClaim_Id != @RiskClaimId
AND INC.[ADARecordStatus] = 0
