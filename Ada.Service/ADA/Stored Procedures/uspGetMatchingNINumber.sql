﻿
CREATE PROCEDURE [dbo].[uspGetMatchingNINumber]
	
	@RiskClaim_Id INT = NULL,
	@NINumber VARCHAR(50) = NULL,
	@FieldsFlag BIT OUTPUT 

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


SET @FieldsFlag = 1
SELECT Id, 0 RuleNo, 1 PriorityGroup, 0 MatchType 
FROM dbo.NINumber 
WHERE ADARecordStatus = 0 AND NINumber = @NINumber
GROUP BY Id