﻿CREATE PROCEDURE [dbo].[uspGetMatchingSanctionsPerson]
(
  @XML XML
 ,@FieldsFlag BIT OUTPUT 
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--XML DEFINITION NEEDED TO ACCOUNT FOR MISSING TAGS
DECLARE @XML_DEF VARCHAR(MAX) = '<Person FirstName="" LastName="" DateOfBirth=""><NINumbers><NINumber NINumber="" /></NINumbers><Passports><Passport PassportNumber="" /></Passports><Addresses><Address SubBuilding="" Building="" BuildingNumber="" Street="" Locality="" Town="" PostCode="" /></Addresses></Person>'
DECLARE @XML_Check XML = '<Data>' +  REPLACE(CONVERT(VARCHAR(MAX), @XML), 'UNKNOWN', '') +  @XML_DEF + '</Data>'

SET @FieldsFlag = 0 --RETURN VALUE 0 IF NOT ENOUGHT DATA SUPPLIED
DECLARE @T AS TABLE(TID INT NOT NULL IDENTITY(1,1), RuleNo INT, PriorityGroup INT)
DECLARE @Ret TABLE(ID VARCHAR(500), RuleNo INT, PriorityGroup INT, MatchType INT)

--CREATING TABLE OF POSSABLE RULES TO MATCH
INSERT INTO @T(RuleNo, PriorityGroup)
SELECT RuleNo, PriorityGroup FROM dbo.fnGetMatchingSanctionsPersonToProcess (@XML_Check) ORDER BY PriorityGroup, RuleNo
DECLARE @ROWCOUNT INT = @@ROWCOUNT	
IF @ROWCOUNT = 0  --NOT ENOUGHT DATA SUPPLIED EXIT PROCEDURE
	GOTO MatchEnd
ELSE
	SET @FieldsFlag = 1

DECLARE  @RuleNo		INT
		,@PriorityGroup INT
		,@Count			INT = 1
		,@SQL			NVARCHAR(MAX)
		
--LOOP EACH RULE, EXIT WHEN A MATCH IS FOUND
WHILE @Count <= @ROWCOUNT
BEGIN		
	SELECT	 @RuleNo = RuleNo
			,@PriorityGroup = PriorityGroup
	FROM @T 
	WHERE TID = @Count

	DECLARE @GrpCount INT = @Count
	DECLARE @GrpCountEnd INT = (SELECT MAX(TID) FROM @T WHERE PriorityGroup = (SELECT PriorityGroup FROM @T WHERE TID = @Count) AND TID >= @Count)
	SET @SQL = dbo.fnGetMatchingSanctionsPersonTSql(@RuleNo)
		
	WHILE @GrpCount < @GrpCountEnd
	BEGIN
		SET @GrpCount += 1
		SELECT @RuleNo = RuleNo FROM @T WHERE TID = @GrpCount
		SET @SQL +=   CHAR(10) + 'UNION ALL' + CHAR(10) + dbo.fnGetMatchingSanctionsPersonTSql(@RuleNo)
	END
	
	PRINT '################################# RiskClaimFlag = 0 START #######################################'
	PRINT ''
	PRINT @SQL
	PRINT ''
	PRINT '################################# RiskClaimFlag = 0 END #########################################'
		
	INSERT INTO @Ret
	EXEC sp_executesql @SQL, N'@XML XML', @XML=@XML
	IF EXISTS (SELECT * FROM @Ret) GOTO MatchEnd

	SET @Count += 1
END

MatchEnd:

IF (SELECT COUNT(1) FROM @Ret) > 1
BEGIN
	DECLARE @ValueString VARCHAR(MAX)
	SELECT @ValueString = COALESCE(@ValueString+'; ' ,'') + ID
	FROM @Ret
	
	SELECT @ValueString ID, RuleNo, PriorityGroup, MatchType 
	FROM @Ret
	WHERE RuleNo = (SELECT MIN(RuleNo) FROM @Ret)
END
ELSE
BEGIN
	SELECT ID, MIN(RuleNo) AS RuleNo, PriorityGroup, MatchType 
	FROM @Ret 
	GROUP BY ID, PriorityGroup, MatchType 
	ORDER BY RuleNo DESC, ID ASC
END