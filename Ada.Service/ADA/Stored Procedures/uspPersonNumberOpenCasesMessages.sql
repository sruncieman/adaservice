﻿
CREATE PROCEDURE [dbo].[uspPersonNumberOpenCasesMessages]
                @personId int,
                @includeThisPersonId bit,
                @includeConfirmed bit,
                @includeUnconfirmed bit,
                @includeTentative bit,
                @startDate smallDateTime,
                @endDate smallDateTime,
                @caseIncidentLinkType int,
                @IsPotentialClaimant BIT  = NULL
AS


	 SELECT
          PT.PartyTypeText, SPT.SubPartyText, I.IncidentDate, REPLACE(CT.ClaimType, 'RTA - ', '') ClaimType, KC.KeoghsEliteReference, I2P.FiveGrading,
          CASE WHEN @includeUnconfirmed = 1 THEN  S.Text ELSE NULL END AS Salutation,
          CASE WHEN @includeUnconfirmed = 1 THEN  P.FirstName ELSE NULL END AS FirstName,
          CASE WHEN @includeUnconfirmed = 1 THEN  P.LastName ELSE NULL END AS LastName,
          CASE WHEN @includeUnconfirmed = 1 THEN  P.DateOfBirth ELSE NULL END AS DateOfBirth

          FROM Incident2Person AS I2P
          INNER JOIN KeoghsCase2Incident AS I2C 
			ON I2P.Incident_Id = I2C.Incident_Id
          INNER JOIN Incident AS I 
			ON I2P.Incident_Id = I.Id
          INNER JOIN KeoghsCase KC 
			ON KC.Id = I2c.KeoghsCase_Id
          LEFT OUTER JOIN Incident2PersonOutcome AS I2P_1 
			ON I2P.Incident_Id = I2P_1.Incident_Id AND I2P.Person_Id = I2P_1.Person_Id
		INNER JOIN PartyType PT
		ON I2P.PartyType_Id = PT.Id
		INNER JOIN SubPartyType SPT
		ON I2P.SubPartyType_Id = SPT.Id
		INNER JOIN ClaimType CT
		ON I.ClaimType_Id = CT.Id
		INNER JOIN Person P
		ON I2P.Person_Id = P.Id
		INNER JOIN Salutation S 
		ON P.Salutation_Id = S.Id
			
          WHERE (I2P_1.Id IS NULL AND CaseIncidentLinkType_Id = @caseIncidentLinkType
          and I.IncidentDate >= @startDate
          and I.IncidentDate <= @endDate
          and I2P.ADARecordStatus = 0
          and I.ADARecordStatus = 0 
          and I.IncidentType_Id = 8
          and kc.CaseStatus_Id = 2
		  and I2P.Person_Id in (SELECT Id FROM dbo.fn_GetPersonTree(@personId, @includeConfirmed ,@includeUnconfirmed, @includeTentative, @includeThisPersonId)))

