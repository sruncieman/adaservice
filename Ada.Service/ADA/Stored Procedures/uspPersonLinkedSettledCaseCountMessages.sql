﻿
CREATE  PROCEDURE [dbo].[uspPersonLinkedSettledCaseCountMessages]
(
 @PersonId INT = 5
,@IncidentId INT  = 66
,@IncludeThisPersonId BIT = 1
,@IncludeConfirmed BIT = 1
,@IncludeUnconfirmed BIT = 0
,@IncludeTentative BIT = 0
,@StartDate SMALLDATETIME = '2000-01-01'
,@EndDate SMALLDATETIME = '2020-01-01'
,@OutcomeCategoryId INT = 2
,@ClaimTypeId INT = 19
,@IsPotentialClaimant BIT  = NULL
)	 
AS
 
DECLARE @PersonTree NVARCHAR(1000) = 'SELECT Id FROM dbo.fn_GetPersonTree(' + CONVERT(NVARCHAR(50) ,@PersonId) + ', ' +  CONVERT(NVARCHAR(50), @IncludeConfirmed)  + ', ' +   CONVERT(NVARCHAR(50), @IncludeUnconfirmed) + ', ' +  CONVERT(NVARCHAR(50), @IncludeTentative)  + ', ' +  CONVERT(NVARCHAR(50), @IncludeThisPersonId) + ') '
DECLARE @IsPotentialClaimantWhere NVARCHAR(100) = ''

IF @IsPotentialClaimant IS NOT NULL
	SET @IsPotentialClaimantWhere = ' AND PC.Flag = ' + CONVERT(CHAR(1), @IsPotentialClaimant)

DECLARE @where NVARCHAR(500) = CASE WHEN @ClaimTypeId < 0 THEN  'WHERE ClaimType_Id !=  ' + CONVERT(NVARCHAR(50) ,ABS(@ClaimTypeId)) ELSE CASE  WHEN @ClaimTypeId >= 0 THEN  'WHERE ClaimType_Id = ' + CONVERT(NVARCHAR(50) ,@ClaimTypeId) ELSE  ''  END  END
DECLARE @Sql NVARCHAR(3000) = '	SELECT Incident_Id
								FROM Incident2Person MOR
								INNER JOIN PotentialClaimant PC ON MOR.SubPartyType_Id = PC.SubPartyType AND MOR.PartyType_Id = PC.PartyType 
								WHERE Person_Id IN (
													SELECT Person_Id 
													FROM Incident2Person MOR 
													WHERE (Person_Id IN(' + @PersonTree + ') 
													AND (Incident_Id  != ' + CONVERT(NVARCHAR(50) ,@IncidentId) + '))
													)	
								' + @IsPotentialClaimantWhere
			
DECLARE @Tbl TmpTbl
INSERT INTO @Tbl
EXECUTE sp_executesql @Sql
	
IF (@ClaimTypeId >= 0)
	SELECT  ThisPartyType,  
			ThisSubPartyType, 
			LinkedSalutation,
			LinkedFirstName, 
			LinkedLastName, 
			LinkedDateOfBirth,
			LinkedPartyType, 
			LinkedSubPartyType, 
			LinkedIncidentDate, 
			LinkedClaimType,
			LinkedEliteReference,
			LinkedFiveGrading,
			OtherPartyType, 
			OtherSubPartyType, 
			OtherSalutation,
			OtherFirstname, 
			OtherLastName, 
			OtherDoB, 
			OtherMoR,
			OtherFiveGrading
	FROM	(
			SELECT	  DISTINCT Salutation LinkedSalutation,
					  FirstName LinkedFirstName, 
					  LastName LinkedLastName, 
					  DateOfBirth LinkedDateOfBirth,
					  PartyTypeText LinkedPartyType, 
					  SubPartyText LinkedSubPartyType, 
					  IncidentDate LinkedIncidentDate, 
					  ClaimType  LinkedClaimType,
					  KeoghsEliteReference  LinkedEliteReference,
					  Incident2PersonOutcome_FiveGrading  LinkedFiveGrading,
					  Incident_Id
			FROM  dbo.vPersonLinkedSettledCaseMessages A
			LEFT JOIN dbo.fn_GetPersonTree(@PersonId, @IncludeConfirmed, @IncludeUnconfirmed, @IncludeTentative, @IncludeThisPersonId) PT ON A.Person_Id = PT.Id
			WHERE Incident_Id IN (SELECT Id from @Tbl)  AND Person_Id = PT.Id
			AND A.IncidentDate >= @StartDate
			AND A.IncidentDate <= @EndDate
			) A
	INNER JOIN	(
				SELECT  DISTINCT PartyTypeText OtherPartyType, 
						SubPartyText OtherSubPartyType, 
						Salutation OtherSalutation,
						FirstName OtherFirstname, 
						LastName OtherLastName, 
						DateOfBirth OtherDoB, 
						IncidentDate OtherIncidentDate, 
						ClaimType  OtherClaimType,
						KeoghsEliteReference  OtherEliteReference,
						Incident2PersonOutcome_FiveGrading  OtherFiveGrading,
						MannerOfResolution  OtherMoR,
						Incident_Id
				FROM  vPersonLinkedSettledCaseMessages A
				LEFT JOIN dbo.fn_GetPersonTree(@PersonId, @IncludeConfirmed, @IncludeUnconfirmed, @IncludeTentative, @IncludeThisPersonId) PT ON A.Person_Id = PT.Id
				WHERE Incident_Id IN (SELECT Id from @Tbl)  
				AND Person_Id != ISNULL(PT.Id, 0)
 				AND CategoryTypeId = @OutcomeCategoryId
				AND ClaimType_Id = @ClaimTypeId
				) B ON A.Incident_Id = B.Incident_Id
	CROSS JOIN 	(
				SELECT   PT.PartyTypeText AS ThisPartyType
						,SPT.SubPartyText AS ThisSubPartyType
				FROM dbo.Incident2Person I2P
				INNER JOIN PartyType PT ON i2p.PartyType_Id = PT.Id
				INNER JOIN SubPartyType SPT ON i2p.SubPartyType_Id = SPT.Id
				WHERE Incident_Id = @IncidentId AND Person_Id = @PersonId
				) X
ELSE 
	SELECT  ThisPartyType,  
			ThisSubPartyType, 
			LinkedSalutation,
			LinkedFirstName, 
			LinkedLastName, 
			LinkedDateOfBirth,
			LinkedPartyType, 
			LinkedSubPartyType, 
			LinkedIncidentDate, 
			LinkedClaimType,
			LinkedEliteReference,
			LinkedFiveGrading,
			OtherPartyType, 
			OtherSubPartyType, 
			OtherSalutation,
			OtherFirstname, 
			OtherLastName, 
			OtherDoB, 
			OtherMoR,
			OtherFiveGrading
	FROM	(
			SELECT DISTINCT Salutation LinkedSalutation,
				  FirstName LinkedFirstName, 
				  LastName LinkedLastName, 
				  DateOfBirth LinkedDateOfBirth,
				  PartyTypeText LinkedPartyType, 
				  SubPartyText LinkedSubPartyType, 
				  IncidentDate LinkedIncidentDate, 
				  ClaimType  LinkedClaimType,
				  KeoghsEliteReference  LinkedEliteReference,
				  Incident2PersonOutcome_FiveGrading  LinkedFiveGrading,
				  Incident_Id
			FROM  vPersonLinkedSettledCaseMessages A
			LEFT JOIN dbo.fn_GetPersonTree(@PersonId, @IncludeConfirmed, @IncludeUnconfirmed, @IncludeTentative, @IncludeThisPersonId) PT ON A.Person_Id = PT.Id
			WHERE Incident_Id IN (SELECT Id from @Tbl) AND Person_Id = PT.Id
			AND A.IncidentDate >= @StartDate
			AND A.IncidentDate <= @EndDate
			) A
	INNER JOIN	(
				SELECT  DISTINCT PartyTypeText OtherPartyType, 
						SubPartyText OtherSubPartyType, 
						Salutation OtherSalutation,
						FirstName OtherFirstname, 
						LastName OtherLastName, 
						DateOfBirth OtherDoB, 
						IncidentDate OtherIncidentDate, 
						ClaimType  OtherClaimType,
						KeoghsEliteReference  OtherEliteReference,
						Incident2PersonOutcome_FiveGrading  OtherFiveGrading,
						MannerOfResolution  OtherMoR,
						Incident_Id
				FROM  vPersonLinkedSettledCaseMessages A
				LEFT JOIN dbo.fn_GetPersonTree(@PersonId, @IncludeConfirmed, @IncludeUnconfirmed, @IncludeTentative, @IncludeThisPersonId) PT ON A.Person_Id = PT.Id
				WHERE Incident_Id IN (SELECT Id from @Tbl)  
				AND Person_Id != ISNULL(PT.Id, 0)
 				AND CategoryTypeId = @OutcomeCategoryId
				AND ClaimType_Id != ABS(@ClaimTypeId)
				) B ON A.Incident_Id = B.Incident_Id
	CROSS JOIN	(
				SELECT   PT.PartyTypeText AS ThisPartyType
						,SPT.SubPartyText AS ThisSubPartyType
				FROM dbo.Incident2Person I2P
				INNER JOIN PartyType PT ON i2p.PartyType_Id = PT.Id
				INNER JOIN SubPartyType SPT ON i2p.SubPartyType_Id = SPT.Id
				WHERE Incident_Id = @IncidentId AND Person_Id = @PersonId
				) X

