﻿CREATE PROCEDURE dbo.uspConcurrentHireVehiclePeriodCount
(
 @VehicleID INT
,@RiskClaimID  INT
,@IncludeThisVehicleId BIT
,@IncludeConfirmed BIT
,@IncludeUnconfirmed BIT
,@IncludeTentative BIT
,@HireStartDate SMALLDATETIME
,@HireEndDate SMALLDATETIME
)
AS
SET NOCOUNT ON

SELECT COUNT(HirePeriodCount) HirePeriodCount
FROM	(
		SELECT RC.Incident_Id HirePeriodCount
		FROM [dbo].[Vehicle2Person] V2P
		INNER JOIN dbo.RiskClaim RC ON RC.Id = V2P.RiskClaim_Id
		WHERE V2P.[Vehicle_Id] IN (SELECT Id FROM [dbo].[fn_GetVehicleTree](@VehicleID, @IncludeConfirmed ,@IncludeUnconfirmed, @IncludeTentative, @IncludeThisVehicleId))
		AND RC.Incident_Id != (SELECT Incident_Id FROM dbo.RiskClaim WHERE Id = @RiskClaimID)
		AND V2P.[HireStartDate] <= @HireEndDate
		AND V2P.[HireEndDate] >= @HireStartDate
		AND V2P.[ADARecordStatus] = 0
		AND V2P.[VehicleLinkType_Id] = 4 --Hirer
		GROUP BY RC.Incident_Id 
		) DATA