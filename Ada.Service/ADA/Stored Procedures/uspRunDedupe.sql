﻿CREATE PROCEDURE [dbo].[uspRunDedupe]
@RunDedupe SMALLINT OUTPUT
--WITH EXECUTE AS 'SVC-ADA-PowerUser'
AS
SET DATEFORMAT YMD

DECLARE  @Today				SMALLDATETIME = CAST(CONVERT(VARCHAR(20),GETDATE(),111) AS DATETIME)
		,@RunNameID_Dedupe	SMALLINT = 145
		,@RunNameID_Sync	SMALLINT = 139

--DedupeHasntRunTodayAndLastSyncWasASuccess....
IF (SELECT TOP 1 1 FROM [MDAControl].[dbo].[DBControlRun] WHERE CAST(CONVERT(VARCHAR(20),[RunStartTime],111) AS DATETIME) = @Today AND [RunNameID] = @RunNameID_Dedupe) IS NULL
AND (SELECT TOP 1 1 FROM [MDAControl].[dbo].[DBControlRun] WHERE CAST(CONVERT(VARCHAR(20),[RunStartTime],111) AS DATETIME) = @Today AND [RunNameID] = @RunNameID_Sync AND RunEndTime IS NOT NULL AND Error IS NULL) IS NOT NULL
BEGIN
	SELECT @RunDedupe = 1
	INSERT INTO [MDAControl].[dbo].[DBControlRun] ([RunNameID],[RunStartTime],[RunEndTime],[Details])
	SELECT @RunNameID_Dedupe, GETDATE(), GETDATE(),'Run the dedeuplication process for today, its not been run and Sync as been successful for today...'
END

SELECT @RunDedupe = ISNULL(@RunDedupe,0)