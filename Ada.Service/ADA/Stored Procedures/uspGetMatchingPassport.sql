﻿
CREATE PROCEDURE [dbo].[uspGetMatchingPassport]
	
	@RiskClaim_Id INT  = NULL,
	@PassportNumber VARCHAR(50)  = NULL,
	@FieldsFlag BIT OUTPUT 

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SET @FieldsFlag = 1

SELECT Id, 0 RuleNo, 1 PriorityGroup, 0 MatchType 
FROM dbo.Passport 
WHERE ADARecordStatus = 0 AND PassportNumber = @PassportNumber
GROUP BY Id
