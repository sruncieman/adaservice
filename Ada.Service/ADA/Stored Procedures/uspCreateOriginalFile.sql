﻿CREATE PROCEDURE [dbo].[uspCreateOriginalFile] 
	-- Add the parameters for the stored procedure here
	@File_Id uniqueidentifier,
	@File_Description varchar(50),
	@BinaryFile varbinary(max),
	@RiskOriginalFile_Id int OUTPUT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.RiskOriginalFile(Id, [Description], FileData) values(@File_Id, @File_Description, @BinaryFile)
    
    SET @RiskOriginalFile_Id = SCOPE_IDENTITY()
END
