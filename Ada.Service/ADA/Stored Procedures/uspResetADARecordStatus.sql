﻿CREATE PROCEDURE [dbo].[uspResetADARecordStatus]
/**************************************************************************************************/
-- ObjectName:		[dbo].[uspResetADARecordStatus]	
--
-- Steps:			1)DeclareVariablesTempObjects
--					2)GetTheTableListToUpdate
--					3)UpdateTheADAecordStatus
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-05-29			Paul Allen		Created
/**************************************************************************************************/
AS
SET NOCOUNT ON

----------------------------------------------------
--1)DeclareVariablesTempObjects
----------------------------------------------------
DECLARE  @Sys_Change_Context	VARBINARY(128) = CONVERT(VARBINARY(128),'ResetADARecordStatus')
		,@SQL					NVARCHAR(MAX)
		,@TableName				VARCHAR(200)
		,@TableProcessCount		INT
		,@Counter				INT				= 1
		,@ParamDef				NVARCHAR(200)	= '@Sys_Change_Context VARBINARY(128)'

DECLARE @UpdateTable TABLE (ID INT IDENTITY(1,1), TableName VARCHAR(200))

----------------------------------------------------
--2)GetTheTableListToUpdate
----------------------------------------------------
INSERT INTO @UpdateTable (TableName)
SELECT S.name + '.' + T.name TableName
FROM sys.columns C
INNER JOIN sys.tables T ON T.object_id = C.object_id
INNER JOIN sys.schemas S ON S.schema_id = T.schema_id
WHERE C.name = 'ADARecordStatus'
SELECT @TableProcessCount = @@ROWCOUNT

----------------------------------------------------
--3)UpdateTheADAecordStatus
----------------------------------------------------
WHILE @Counter <= @TableProcessCount
BEGIN
	SELECT @TableName = (SELECT TableName FROM @UpdateTable WHERE ID = @Counter)
	SELECT @SQL =
				'
				SET NOCOUNT ON;
				UPDATE ' + @TableName + ' SET ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10;
				'
	EXECUTE sp_executesql @SQL, @ParamDef, @Sys_Change_Context
	SELECT @Counter +=1
END
