﻿using MDA.Pipeline.Model;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using MDA.MappingService.Interface;
using System.Collections.Generic;
using System;
using MDA.Common;
using MDA.Common.Server;
using MDA.MappingService;

namespace MDA.MappingService.Pipeline.PI
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        /// <summary>
        /// Process a file ALREADY in PIPELINE format. Was going to be used for IBASE inports but was dropped. May 
        /// prove to be useful in te future however
        /// </summary>
        /// <param name="ctx">Current Context containing DB connection to use</param>
        /// <param name="fs">FileStream containing the file to be loaded</param>
        /// <param name="statusTracking"></param>
        /// <param name="processClaim">CallBack function to call for each PipelineClaim created</param>
        /// <param name="ShowProgress">Show Progress callback</param>
        /// <param name="processingResults">Structure to hold ALL processing results</param>
        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking,
                                        Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn,
                                        out ProcessingResults processingResults)
        {
            fs.Seek(0, SeekOrigin.Begin);

            PipelineClaimBatch ret = new PipelineClaimBatch();

            processingResults = new ProcessingResults("Mapping");

            try
            {
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                DataContractSerializer ser = new DataContractSerializer(typeof(MDA.Pipeline.Model.PipelineClaimBatch));

                MDA.Pipeline.Model.PipelineClaimBatch cb = (MDA.Pipeline.Model.PipelineClaimBatch)ser.ReadObject(reader, true);

                foreach (var c in cb.Claims)
                    if (ProcessClaimFn(ctx, c, statusTracking) == -1) return;

                //return cb;
            }
            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Failed to recognise XML file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }
    }
}
