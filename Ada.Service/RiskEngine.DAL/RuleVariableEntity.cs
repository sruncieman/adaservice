//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RiskEngine.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class RuleVariableEntity
    {
        public int RuleVariableId { get; set; }
        public int RuleGroupId { get; set; }
        public string VariableName { get; set; }
        public string VariableValue { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    
        public virtual RuleGroupEntity RuleGroup { get; set; }
    }
}
