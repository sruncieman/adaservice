﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Service.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class Claim
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string YOA;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNo;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Status;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? NotifiedDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Paid;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Recovered;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Estimate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyNo;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimType;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime IncidentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PartyType;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Title;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Forename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Surname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? DateOfBirth;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Gender;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Town;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string County;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Registration;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PhoneHome;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PhoneDay;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Mobile;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Email;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FraudSuspicion1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FraudSuspicion2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FraudSuspicion3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FraudSuspicion4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FraudSuspicion5;

    }
}
