﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Service.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class Policy
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string YOA;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyNo;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? EffDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyStatus;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ProductCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Premium;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Title;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NameFore;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NameMiddle;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NameSur;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? BirthDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Gender;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PartyType;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Town;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string County;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehReg;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PhoneDay;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PhoneHome;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Mobile;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Email;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Barred;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string BarredReason;
    }
}
