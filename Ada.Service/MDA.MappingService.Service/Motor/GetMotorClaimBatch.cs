﻿using FileHelpers;
using Ionic.Zip;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Common.Server;
using MDA.MappingService.Service.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MDA.MappingService.Service.Motor
{
    public class GetMotorClaimBatch
    {
        private static bool boolDebug = false;
        private static string filePath;
        private static string folderPath;
        internal void RetrieveMotorClaimBatch(Common.Server.CurrentContext ctx, System.IO.Stream filem, string clientFolder, bool DeleteUploadedFiles, object statusTracking, Func<Common.Server.CurrentContext, Pipeline.Model.IPipelineClaim, object, int> processClaim, Common.ProcessingResults processingResults)
        {
            #region Initialise variables
            string ClaimFile = string.Empty;
            string PolicyFile = string.Empty;
            string fileDateString = string.Empty;

            Claim[] Service_Claim;
            Policy[] Service_Policy;
           

            #endregion

            #region Extract Files from Zip
            if (filem != null)
            {
                try
                {
                    boolDebug = false;

                    using (ZipFile zip1 = ZipFile.Read(filem)) //_filePath))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            var extractTmpPath = string.Concat(clientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);

                            string unpackDirectory = clientFolder;
                            e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

                            fileDateString = e.FileName;

                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                        ClaimFile = clientFolder + @"\Claims.csv";
                        PolicyFile = clientFolder + @"\Policies.csv";
                       
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in extraction of files: " + ex);
                }
            }
            else
            {
                boolDebug = true;
                folderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Service\Resource\";
                ClaimFile = folderPath + @"\Keoghs Claims 121214.csv";
                PolicyFile = folderPath + @"\Policies.csv";
                //ClaimFile = @"d:\temp\ServiceClaimsE1.csv";
                //PolicyFile = @"d:\temp\ServicePolicyE1.csv";
            }
            #endregion

            DeleteExtractedFiles(clientFolder, DeleteUploadedFiles);

            #region FileHelper Engines
            try
            {
                FileHelperEngine ServiceClaimEngine = new FileHelperEngine(typeof(Claim));
                FileHelperEngine ServicePolicyEngine = new FileHelperEngine(typeof(Policy));
               

                #region Populate FileHelper Engines with data from files
                Service_Claim = ServiceClaimEngine.ReadFile(ClaimFile) as Claim[];
                Service_Policy = ServicePolicyEngine.ReadFile(PolicyFile) as Policy[];
                #endregion

            }

            catch (Exception ex)
            {
                throw new CustomException("Error in initialising FileHelper Engines: " + ex);
            }

            #endregion

            #region Get Distinct Claims List
            List<Claim> lstClaims = new List<Claim>();

            if (Service_Claim != null)
                lstClaims = Service_Claim
                    .GroupBy(i => i.ClaimNo)
                    .Select(g => g.First())
                    .ToList();
            #endregion

            #region Translate to XML

            foreach (var claim in lstClaims)
            {
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                var claimLines = Service_Claim.Where(x => x.ClaimNo == claim.ClaimNo && !CheckNullOrEmpty(x)).ToList();

                var partyTypesToIgnore = "POLICE, INSURER, INSURED VEHICLE PASSENGER, THIRD PARTY VEHICLE PASSENGER, WITNESS, DRIVER";

                var organisationClaimLines = claimLines.Where(x => x.Title == "NULL" && x.Forename == "NULL" && (x.Surname != "NULL" && x.Surname != "") && !partyTypesToIgnore.Contains(x.PartyType.ToUpper())).ToList();

                var policyLines = Service_Policy.Where(x => x.PolicyNo == claim.PolicyNo).ToList();

                var policyInfo = Service_Policy.Where(x => x.PolicyNo == claim.PolicyNo).FirstOrDefault();

                var vehicleRegCount = claimLines.Where(x => x.ClaimNo == claim.ClaimNo && x.Registration!="NULL").Count();
                //var peopleWithNoVehicle = claimLines.Where(x => x.ClaimNo == claim.ClaimNo && x.Registration == "NULL" && (x.PartyType.ToUpper() != "POLICE" && x.PartyType.ToUpper() != "INSURER")).ToList();
                var insuredPassengers = claimLines.Where(x => x.ClaimNo == claim.ClaimNo &&  x.PartyType.ToUpper() == "INSURED VEHICLE PASSENGER").ToList();
                var thirdPartyPassengers = claimLines.Where(x => x.ClaimNo == claim.ClaimNo && x.PartyType.ToUpper() == "THIRD PARTY VEHICLE PASSENGER").ToList();
                var witnesses = claimLines.Where(x => x.ClaimNo == claim.ClaimNo && x.PartyType.ToUpper() == "WITNESS").ToList();

                #region CLAIM
                motorClaim.ClaimNumber = claim.ClaimNo;
                motorClaim.IncidentDate = claim.IncidentDate;
                motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                #endregion

                #region CLAIM INFO
                switch (claim.Status.ToUpper())
                {
                    case "OPEN":
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                        break;
                    case "SETTLED":
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                        break;
                    case "REOPENED":
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                        break;
                    default:
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                        break;
                }

                motorClaim.ExtraClaimInfo.ClaimNotificationDate = claim.NotifiedDate;
                if(!string.IsNullOrEmpty(claim.ClaimType))
                    motorClaim.ExtraClaimInfo.ClaimCode = claim.ClaimType;


                decimal totalEst = 0;
                decimal totalPymts = 0;

                var estimates = claimLines.Where(x => x.ClaimNo == claim.ClaimNo && claim.ClaimNo != null).Select(x => x.Estimate).ToList();
                var payments = claimLines.Where(x => x.ClaimNo == claim.ClaimNo && claim.ClaimNo != null).Select(x => x.Paid).ToList();

                foreach (var est in estimates)
                {
                    decimal result;

                    if (decimal.TryParse(est, out result))
                        totalEst += result;
                }


                foreach (var val in payments)
                {
                    decimal result;

                    if (decimal.TryParse(val, out result))
                        totalPymts += result;
                }

                if (totalEst > 0)
                    motorClaim.ExtraClaimInfo.Reserve = totalEst;

                if (totalPymts > 0)
                    motorClaim.ExtraClaimInfo.PaymentsToDate = totalPymts;

                #endregion

                #region Policy

                motorClaim.Policy.Insurer = "Service Insurance Group";
                if (!string.IsNullOrEmpty(claim.PolicyNo))
                    motorClaim.Policy.PolicyNumber = claim.PolicyNo;


                if (policyInfo != null)
                {

                    if (policyInfo.EffDate != null)
                    {
                        motorClaim.Policy.PolicyStartDate = policyInfo.EffDate;
                        DateTime polStartDate = Convert.ToDateTime(policyInfo.EffDate);
                        motorClaim.Policy.PolicyEndDate = polStartDate.AddMonths(12);
                    }

                    if (policyInfo.Premium != null)
                    {
                        decimal result;

                        if (decimal.TryParse(policyInfo.Premium, out result))
                            motorClaim.Policy.Premium = result;
                    }
                }
                #endregion

                #region CLAIM LEVEL ORGANISATION

                foreach (var organisationDetails in organisationClaimLines)
                {
                    //var organisationDetails = claimLines.Where(x => x.Title == "NULL" && x.Forename == "NULL").FirstOrDefault();

                    PipelineOrganisation claimOrg = new PipelineOrganisation();

                    #region OrgInvolvement
                    switch (organisationDetails.PartyType.ToUpper())
                    {
                        case "ENGINEER":
                            claimOrg.OrganisationType_Id = (int)OrganisationType.VehicleEngineer;
                            claimOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Engineer;
                            break;
                        case "BROKER'S REPRESENTATIVE":
                        case "BROKER":
                            claimOrg.OrganisationType_Id = (int)OrganisationType.Broker;
                            claimOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Broker;
                            break;
                        case "REPAIRER":
                        case "WINDSCREEN":
                            claimOrg.OrganisationType_Id = (int)OrganisationType.Repairer;
                            claimOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Repairer;
                            break;
                        case "VEHICLE STORAGE":
                            claimOrg.OrganisationType_Id = (int)OrganisationType.Storage;
                            claimOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Storage;
                            break;
                        case "CLAIMANT SOLICITOR":
                        case "CLAIMANT REPRESENTATIVE":
                        case "SOLICITOR":
                        case "THIRD PARTY SOLICITOR":
                        case "INSURED NOMINATED REPRESENTATIVE":
                        case "CUSTOMER SOLICITOR":
                        case "OWN SOLICITOR":
                        case "MOTOR THIRD PARTY SOLICITOR":
                        case "PANEL SOLICITOR":
                        case "MOTOR PANEL SOLICITOR":
                            claimOrg.OrganisationType_Id = (int)OrganisationType.Solicitor;
                            claimOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                            break;
                        case "CREDIT HIRE COMPANY":
                            claimOrg.OrganisationType_Id = (int)OrganisationType.CreditHire;
                            claimOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire;
                            break;
                        case "ACCIDENT MANAGEMENT CO":
                            claimOrg.OrganisationType_Id = (int)OrganisationType.AccidentManagement;
                            claimOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.AccidentManagement;
                            break;
                        case "TOW":
                            claimOrg.OrganisationType_Id = (int)OrganisationType.Recovery;
                            claimOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Recovery;
                            break;
                        case "MEDICAL AGENCY":
                            claimOrg.OrganisationType_Id = (int)OrganisationType.MedicalExaminer;
                            claimOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.MedicalExaminer;
                            break;
                        default:
                            claimOrg.OrganisationType_Id = (int)OrganisationType.Unknown;
                            claimOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                            break;
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(organisationDetails.Surname))
                        claimOrg.OrganisationName = organisationDetails.Surname;

                    #region Email


                    if (!string.IsNullOrEmpty(organisationDetails.Email))
                    {
                        PipelineEmail email = new PipelineEmail();

                        email.EmailAddress = organisationDetails.Email;

                        claimOrg.EmailAddresses.Add(email);
                    }

                    #endregion

                    #region TELEPHONES

                    if (!string.IsNullOrEmpty(organisationDetails.PhoneHome))
                    {
                        PipelineTelephone tel1 = new PipelineTelephone();

                        tel1.ClientSuppliedNumber = organisationDetails.PhoneHome;

                        claimOrg.Telephones.Add(tel1);
                    }

                    if (!string.IsNullOrEmpty(organisationDetails.Mobile))
                    {
                        PipelineTelephone tel2 = new PipelineTelephone();

                        tel2.ClientSuppliedNumber = claim.Mobile;

                        claimOrg.Telephones.Add(tel2);
                    }

                    if (!string.IsNullOrEmpty(organisationDetails.PhoneDay))
                    {
                        PipelineTelephone tel3 = new PipelineTelephone();

                        tel3.ClientSuppliedNumber = organisationDetails.PhoneDay;

                        claimOrg.Telephones.Add(tel3);
                    }

                    #endregion



                    PipelineAddress addressOrg = new PipelineAddress();

                    if (!string.IsNullOrEmpty(organisationDetails.Address1))
                    {
                        addressOrg.Street = organisationDetails.Address1;
                    }

                    if (!string.IsNullOrEmpty(organisationDetails.Address2))
                    {
                        addressOrg.Locality = organisationDetails.Address2;
                    }

                    if (!string.IsNullOrEmpty(organisationDetails.Town))
                        addressOrg.Town = organisationDetails.Town;

                    if (!string.IsNullOrEmpty(organisationDetails.County))
                        addressOrg.County = organisationDetails.County;

                    if (!string.IsNullOrEmpty(organisationDetails.Postcode))
                        addressOrg.PostCode = organisationDetails.Postcode;

                    addressOrg.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;

                    if (addressOrg.PostCode != null && addressOrg.Street != null)
                        claimOrg.Addresses.Add(addressOrg);

                    motorClaim.Organisations.Add(claimOrg);
                }
                #endregion

                #region Vehicles
                if (vehicleRegCount > 1)
                {
                    var partyClaimLines = claimLines.Where(x => x.ClaimNo == claim.ClaimNo && x.Registration != "NULL").ToList();

                   
                    var blankPartyTypes = partyClaimLines.Where(x => x.PartyType.ToUpper() == "NULL").ToList();
                    

                    var matchedInsuredDriverQuery = (from b in blankPartyTypes
                                                     join p in policyLines
                                                     on new
                                                     {
                                                         PolicyNo = b.PolicyNo,
                                                         //VehReg = b.Registration,
                                                         Forename = b.Forename.ToUpper(),
                                                         Surname = b.Surname.ToUpper(),
                                                         YOA = b.YOA
                                                     }
                                                     equals new
                                                     {
                                                         PolicyNo = p.PolicyNo,
                                                         //VehReg = p.VehReg,
                                                         Forename = p.NameFore.ToUpper(),
                                                         Surname = p.NameSur.ToUpper(),
                                                         YOA = p.YOA
                                                     }
                                                     
                                                     select new
                                                     {
                                                         claimInfo = b,
                                                         policyInfo = p
                                                     }).ToList();

                    


                    var insuredDriver = matchedInsuredDriverQuery.Where(x => x.policyInfo.PartyType.ToUpper() == "PROPOSER").FirstOrDefault();
                    Policy policyHolder = null;
                    if (insuredDriver == null && matchedInsuredDriverQuery !=null)
                    {
                        insuredDriver = matchedInsuredDriverQuery.FirstOrDefault();

                        policyHolder = policyLines.Where(x => x.PartyType.ToUpper() == "PROPOSER").FirstOrDefault();
                    }

                    if (insuredDriver != null)
                    {
                        var excludedVehicleRegistartions = insuredDriver.claimInfo.Registration;

                        var otherDrivers = blankPartyTypes.Where(x => x.Registration != "" && !excludedVehicleRegistartions.Contains(x.Registration)).ToList();

                        // Add TP Driver and Vehicle
                        foreach (var driver in otherDrivers)
                        {
                            PipelineVehicle vehicleTP = new PipelineVehicle();
                            PipelinePerson personTP = new PipelinePerson();

                            AddDriverAndVehicle(driver, out vehicleTP, out personTP, PartyType.ThirdParty, SubPartyType.Driver, Incident2VehicleLinkType.ThirdPartyVehicle);

                            vehicleTP.People.Add(personTP);
                            motorClaim.Vehicles.Add(vehicleTP);
                        }

                    }

                    var otherPeople = blankPartyTypes.Where(x => x.Registration == "").ToList();

                    foreach (var driver in otherPeople)
                    {
                        PipelineVehicle vehicleTP = new PipelineVehicle();
                        PipelinePerson personTP = new PipelinePerson();

                        AddDriverAndVehicle(driver, out vehicleTP, out personTP, PartyType.Unknown, SubPartyType.Unknown, Incident2VehicleLinkType.NoVehicleInvolved);

                        vehicleTP.People.Add(personTP);
                        motorClaim.Vehicles.Add(vehicleTP);
                    }

                    var excludedPartyTypes = "INSURED VEHICLE PASSENGER, THIRD PARTY VEHICLE PASSENGER";
                    
                    var otherPartyTypes = partyClaimLines.Where(x => x.PartyType.ToUpper() != "NULL" && !excludedPartyTypes.Contains(x.PartyType.ToUpper())).ToList();
                    
                    var countTPDrivers = partyClaimLines.Where(x => x.PartyType.ToUpper() == "DRIVER").Count();

                    // Insured Driver and Vehicle
                    PipelineVehicle vehicle = new PipelineVehicle();
                    PipelinePerson person = new PipelinePerson();

                    // Add Insured Driver and Vehicle
                    if(insuredDriver!=null)
                    { 
                        AddDriverAndVehicle(insuredDriver.claimInfo, out vehicle, out person, PartyType.Insured, SubPartyType.Driver, Incident2VehicleLinkType.InsuredVehicle);

                        vehicle.People.Add(person);
                    }

                    // Add policyholder if it exists
                    if (policyHolder != null)
                    {
                        PipelinePerson policyHolderPerson = new PipelinePerson();
                        PipelineVehicle policyHolderVehicle = new PipelineVehicle();
                        AddDriverAndVehicle(policyHolder, out policyHolderVehicle, out policyHolderPerson, PartyType.Policyholder, SubPartyType.Unknown, Incident2VehicleLinkType.InsuredVehicle);
                        vehicle.People.Add(policyHolderPerson);
                    }

                    // Any Insured Passengers?
                    if (insuredPassengers.Any())
                    {
                        foreach (var insuredPassenger in insuredPassengers)
                        {
                            PipelinePerson personPassenger = new PipelinePerson();
                            PipelineVehicle vehiclePassenger = new PipelineVehicle();
                            if (insuredPassenger.Forename.ToUpper() != "NULL" && insuredPassenger.Surname.ToUpper() != "NULL")
                            {
                                AddDriverAndVehicle(insuredPassenger, out vehiclePassenger, out personPassenger, PartyType.Insured, SubPartyType.Passenger,Incident2VehicleLinkType.InsuredVehicle);
                                vehicle.People.Add(personPassenger);
                            }
                        }
                    }

                    motorClaim.Vehicles.Add(vehicle);

                    // Add Others
                    foreach (var otherDriver in otherPartyTypes)
                    {
                        PipelineVehicle vehicleOther = new PipelineVehicle();
                        PipelinePerson personOther = new PipelinePerson();

                        switch (otherDriver.PartyType)
                        {
                            case "Driver":
                                AddDriverAndVehicle(otherDriver, out vehicleOther, out personOther, PartyType.ThirdParty, SubPartyType.Driver, Incident2VehicleLinkType.ThirdPartyVehicle);
                                
                                break;
                            case "Property Owner":
                                AddDriverAndVehicle(otherDriver, out vehicleOther, out personOther, PartyType.Unknown, SubPartyType.Unknown, Incident2VehicleLinkType.Unknown);
                                break;
                            case "Pedestrian":
                                AddDriverAndVehicle(otherDriver, out vehicleOther, out personOther, PartyType.Claimant, SubPartyType.Unknown,Incident2VehicleLinkType.Unknown);
                                break;
                            case "Vehicle Owner":
                                AddDriverAndVehicle(otherDriver, out vehicleOther, out personOther, PartyType.VehicleOwner, SubPartyType.Unknown,Incident2VehicleLinkType.Unknown);
                                break;
                            case "Cyclist":
                                AddDriverAndVehicle(otherDriver, out vehicleOther, out personOther, PartyType.Claimant, SubPartyType.Unknown,  Incident2VehicleLinkType.Unknown);
                                break;
                            case "MotorCyclist":
                                AddDriverAndVehicle(otherDriver, out vehicleOther, out personOther, PartyType.Claimant, SubPartyType.Unknown, Incident2VehicleLinkType.Unknown);
                                break;
                            default:
                                AddDriverAndVehicle(otherDriver, out vehicleOther, out personOther, PartyType.Unknown, SubPartyType.Unknown , Incident2VehicleLinkType.Unknown);
                                break;
                        }

                        vehicleOther.People.Add(personOther);

                        // Any TP Passengers?
                        if (thirdPartyPassengers.Any())
                        {
                            
                            foreach (var TPPassenger in thirdPartyPassengers)
                            {

                                if(TPPassenger.Registration == vehicleOther.VehicleRegistration)
                                {
                                PipelinePerson TPpersonPassenger = new PipelinePerson();
                                PipelineVehicle TPvehiclePassenger = new PipelineVehicle();
                                if (TPPassenger.Forename.ToUpper() != "NULL" && TPPassenger.Surname.ToUpper() != "NULL")
                                {
                                    if ((TPPassenger.Registration == "NULL" || TPPassenger.Registration == "" ) && (countTPDrivers==1))
                                    {
                                        AddDriverAndVehicle(TPPassenger, out TPvehiclePassenger, out TPpersonPassenger, PartyType.ThirdParty, SubPartyType.Passenger, Incident2VehicleLinkType.ThirdPartyVehicle);
                                        vehicleOther.People.Add(TPpersonPassenger);
                                    }
                                    else if ((TPPassenger.Registration == "NULL" || TPPassenger.Registration == "") && (countTPDrivers > 1))
                                    {
                                        PipelineVehicle vehicleBlank = new PipelineVehicle();
                                        AddDriverAndVehicle(TPPassenger, out TPvehiclePassenger, out TPpersonPassenger, PartyType.ThirdParty, SubPartyType.Passenger, Incident2VehicleLinkType.ThirdPartyVehicle);
                                        vehicleBlank.People.Add(TPpersonPassenger);
                                        motorClaim.Vehicles.Add(vehicleBlank);
                                    }
                                    else
                                    {
                                        AddDriverAndVehicle(TPPassenger, out TPvehiclePassenger, out TPpersonPassenger, PartyType.ThirdParty, SubPartyType.Passenger, Incident2VehicleLinkType.ThirdPartyVehicle);
                                        vehicleOther.People.Add(TPpersonPassenger);
                                    }
                                }

                                }
                            }
                        }

                        motorClaim.Vehicles.Add(vehicleOther);
                    }

                }

                if (vehicleRegCount == 1)
                {

                    var insuredClaimLine = claimLines.Where(x => x.ClaimNo == claim.ClaimNo && x.Registration != "NULL").FirstOrDefault();

                    PipelineVehicle vehicle;
                    PipelinePerson person;
                    
                    AddDriverAndVehicle(insuredClaimLine, out vehicle, out person, PartyType.Insured, SubPartyType.Driver, Incident2VehicleLinkType.InsuredVehicle);

                    vehicle.People.Add(person);

                    motorClaim.Vehicles.Add(vehicle);

                }
                #endregion

                #region Witness

                if (witnesses.Any())
                {

                    foreach (var witness in witnesses)
                    {
                            PipelineVehicle noVehicleInvolved = new PipelineVehicle();
                            PipelinePerson witnessPerson = new PipelinePerson();

                            AddDriverAndVehicle(witness, out noVehicleInvolved, out witnessPerson, PartyType.Witness, SubPartyType.Witness, Incident2VehicleLinkType.NoVehicleInvolved);
 
                            noVehicleInvolved.People.Add(witnessPerson);

                            if (noVehicleInvolved.People.Count > 0)
                            {

                                motorClaim.Vehicles.Add(noVehicleInvolved);
                            }
                        }
                }

                #endregion

                #region Insured Vehicle and Driver Check

                bool insuredVehicleCheck = false;
                bool insuredDriverCheck = false;

                foreach (var vehicle in motorClaim.Vehicles)
                {
                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                    {
                        insuredVehicleCheck = true;

                        foreach (var person in vehicle.People)
                        {
                            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                            {
                                insuredDriverCheck = true;
                            }
                        }
                    }
                }

                if (insuredVehicleCheck == false)
                {
                    PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                    defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                    }

                    motorClaim.Vehicles.Add(defaultInsuredVehicle);
                }
                else
                {
                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        foreach (var vehicle in motorClaim.Vehicles)
                        {
                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                            {

                                vehicle.People.Add(defaultInsuredDriver);

                            }

                        }

                    }
                }

                #endregion
                
                if (processClaim(ctx, motorClaim, statusTracking) == -1) return;
            }

            #endregion
        }

        private void AddDriverAndVehicle(Policy policyLine, out PipelineVehicle vehicle, out PipelinePerson policyHolderPerson, PartyType partyType, SubPartyType subPartyType, Incident2VehicleLinkType incident2VehicleLinkType)
        {
            // Insured Driver and Vehicle
            vehicle = new PipelineVehicle();
            policyHolderPerson = new PipelinePerson();

            if (!string.IsNullOrEmpty(policyLine.VehReg))
                vehicle.VehicleRegistration = policyLine.VehReg;

            vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)incident2VehicleLinkType;

            policyHolderPerson.I2Pe_LinkData.PartyType_Id = (int)partyType;
            policyHolderPerson.I2Pe_LinkData.SubPartyType_Id = (int)subPartyType;

            if (!string.IsNullOrEmpty(policyLine.Title))
                policyHolderPerson.Salutation_Id = (int)SalutationHelper.GetSalutation(policyLine.Title);

            if (!string.IsNullOrEmpty(policyLine.NameFore))
                policyHolderPerson.FirstName = policyLine.NameFore;

            if (!string.IsNullOrEmpty(policyLine.NameSur))
                policyHolderPerson.LastName = policyLine.NameSur;

            if (!string.IsNullOrEmpty(policyLine.Gender))
            {
                switch (policyLine.Gender)
                {
                    case "M":
                        policyHolderPerson.Gender_Id = (int)Gender.Male;
                        break;
                    case "F":
                        policyHolderPerson.Gender_Id = (int)Gender.Female;
                        break;
                    default:
                        policyHolderPerson.Gender_Id = (int)Gender.Unknown;
                        break;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(policyLine.Title))
                    policyHolderPerson.Gender_Id = (int)GenderHelper.Salutation2Gender(policyLine.Title.ToLower());
            }

            if (policyLine.Gender == "NULL")
            {
                if (!string.IsNullOrEmpty(policyLine.Title))
                    policyHolderPerson.Gender_Id = (int)GenderHelper.Salutation2Gender(policyLine.Title.ToLower());
            }

            #region Email

            if (!string.IsNullOrEmpty(policyLine.Email))
            {
                PipelineEmail email = new PipelineEmail();

                email.EmailAddress = policyLine.Email;

                policyHolderPerson.EmailAddresses.Add(email);
            }

            #endregion

            #region TELEPHONES

            if (!string.IsNullOrEmpty(policyLine.PhoneHome))
            {
                PipelineTelephone landLine = new PipelineTelephone();

                landLine.ClientSuppliedNumber = policyLine.PhoneHome;
                landLine.TelephoneType_Id = (int)TelephoneType.Landline;

                policyHolderPerson.Telephones.Add(landLine);
            }

            if (!string.IsNullOrEmpty(policyLine.Mobile))
            {
                PipelineTelephone mobile = new PipelineTelephone();

                mobile.ClientSuppliedNumber = policyLine.Mobile;
                mobile.TelephoneType_Id = (int)TelephoneType.Mobile;

                policyHolderPerson.Telephones.Add(mobile);
            }

            if (!string.IsNullOrEmpty(policyLine.PhoneDay))
            {
                PipelineTelephone mobile = new PipelineTelephone();

                mobile.ClientSuppliedNumber = policyLine.PhoneDay;
                mobile.TelephoneType_Id = (int)TelephoneType.Unknown;

                policyHolderPerson.Telephones.Add(mobile);
            }

            #endregion

            #region ADDRESS

            //if (!string.IsNullOrEmpty(insuredClaimLine.Address1) || !string.IsNullOrEmpty(pItem.Postcode))
            //{
            PipelineAddress address = new PipelineAddress();
            if (!string.IsNullOrEmpty(policyLine.Address1))
            {
                address.Street = policyLine.Address1;
            }

            if (!string.IsNullOrEmpty(policyLine.Address2))
            {
                address.Locality = policyLine.Address2;
            }

            if (!string.IsNullOrEmpty(policyLine.Town))
                address.Town = policyLine.Town;

            if (!string.IsNullOrEmpty(policyLine.County))
                address.County = policyLine.County;

            if (!string.IsNullOrEmpty(policyLine.Postcode))
                address.PostCode = policyLine.Postcode;

            address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

            if (address.PostCode != null && address.Street != null)
                policyHolderPerson.Addresses.Add(address);

            //}

            #endregion


        }

        

        private static void AddDriverAndVehicle(Claim claimLine, out PipelineVehicle vehicle, out PipelinePerson person, PartyType partyType, SubPartyType subPartyType, Incident2VehicleLinkType incident2VehicleLinkType)
        {
            // Insured Driver and Vehicle
            vehicle = new PipelineVehicle();
            person = new PipelinePerson();

            if (!string.IsNullOrEmpty(claimLine.Registration))
                vehicle.VehicleRegistration = claimLine.Registration;

            vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)incident2VehicleLinkType;

            person.I2Pe_LinkData.PartyType_Id = (int)partyType;
            person.I2Pe_LinkData.SubPartyType_Id = (int)subPartyType;

            if (!string.IsNullOrEmpty(claimLine.Title))
                person.Salutation_Id = (int)SalutationHelper.GetSalutation(claimLine.Title);

            if (!string.IsNullOrEmpty(claimLine.Forename))
                person.FirstName = claimLine.Forename;

            if (!string.IsNullOrEmpty(claimLine.Surname))
                person.LastName = claimLine.Surname;


            if (claimLine.DateOfBirth != null)
                person.DateOfBirth = claimLine.DateOfBirth;

            if (!string.IsNullOrEmpty(claimLine.Gender))
            {
                switch (claimLine.Gender)
                {
                    case "M":
                        person.Gender_Id = (int)Gender.Male;
                        break;
                    case "F":
                        person.Gender_Id = (int)Gender.Female;
                        break;
                    default:
                        person.Gender_Id = (int)Gender.Unknown;
                        break;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(claimLine.Title))
                    person.Gender_Id = (int)GenderHelper.Salutation2Gender(claimLine.Title.ToLower());
            }
            
            if(claimLine.Gender == "NULL")
            {
                if (!string.IsNullOrEmpty(claimLine.Title))
                    person.Gender_Id = (int)GenderHelper.Salutation2Gender(claimLine.Title.ToLower());
            }

            #region Email

            if (!string.IsNullOrEmpty(claimLine.Email))
            {
                PipelineEmail email = new PipelineEmail();

                email.EmailAddress = claimLine.Email;

                person.EmailAddresses.Add(email);
            }

            #endregion

            #region TELEPHONES

            if (!string.IsNullOrEmpty(claimLine.PhoneHome))
            {
                PipelineTelephone landLine = new PipelineTelephone();

                landLine.ClientSuppliedNumber = claimLine.PhoneHome;
                landLine.TelephoneType_Id = (int)TelephoneType.Landline;

                person.Telephones.Add(landLine);
            }

            if (!string.IsNullOrEmpty(claimLine.Mobile))
            {
                PipelineTelephone mobile = new PipelineTelephone();

                mobile.ClientSuppliedNumber = claimLine.Mobile;
                mobile.TelephoneType_Id = (int)TelephoneType.Mobile;

                person.Telephones.Add(mobile);
            }

            if (!string.IsNullOrEmpty(claimLine.PhoneDay))
            {
                PipelineTelephone mobile = new PipelineTelephone();

                mobile.ClientSuppliedNumber = claimLine.PhoneDay;
                mobile.TelephoneType_Id = (int)TelephoneType.Unknown;

                person.Telephones.Add(mobile);
            }

            #endregion

            #region ADDRESS

            //if (!string.IsNullOrEmpty(insuredClaimLine.Address1) || !string.IsNullOrEmpty(pItem.Postcode))
            //{
            PipelineAddress address = new PipelineAddress();
            if (!string.IsNullOrEmpty(claimLine.Address1))
            {
                address.Street = claimLine.Address1;
            }

            if (!string.IsNullOrEmpty(claimLine.Address2))
            {
                address.Locality = claimLine.Address2;
            }



            if (!string.IsNullOrEmpty(claimLine.Town))
                address.Town = claimLine.Town;

            if (!string.IsNullOrEmpty(claimLine.County))
                address.County = claimLine.County;

            if (!string.IsNullOrEmpty(claimLine.Postcode))
                address.PostCode = claimLine.Postcode;

            address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

            if (address.PostCode != null && address.Street != null)
                person.Addresses.Add(address);

            //}

            #endregion
        }

  


        private bool CheckNullOrEmpty(Claim x)
        {
            if (
                (x.PartyType == "NULL" || x.PartyType == "") && (x.Title == "NULL" || x.Title == "") &&
                (x.Forename == "NULL" || x.Forename == "") &&
                (x.Surname == "NULL" || x.Surname == "") &&
                (x.DateOfBirth == null) &&
                (x.Gender == "NULL" || x.Gender == "") &&
                (x.Address1 == "NULL" || x.Address1 == "") &&
                (x.Address2 == "NULL" || x.Address2 == "") &&
                (x.Town == "NULL" || x.Town == "") &&
                (x.County == "NULL" || x.County == "") &&
                (x.Postcode == "NULL" || x.Postcode == "") &&
                (x.Registration == "NULL" || x.Registration == "") &&
                (x.PhoneHome == "NULL" || x.PhoneHome == "") &&
                (x.PhoneDay == "NULL" || x.PhoneDay == "") &&
                (x.Mobile == "NULL" || x.Mobile == "") &&
                (x.Email == "NULL" || x.Email == "") &&
                (x.FraudSuspicion1 == "NULL" || x.FraudSuspicion1 == "") &&
                (x.FraudSuspicion2 == "NULL" || x.FraudSuspicion2 == "") &&
                (x.FraudSuspicion3 == "NULL" || x.FraudSuspicion3 == "") &&
                (x.FraudSuspicion4 == "NULL" || x.FraudSuspicion4 == "") &&
                (x.FraudSuspicion5 == "NULL" || x.FraudSuspicion5 == "")
                )
                return true;
            else
                return false;
        }

       

        private static void DeleteExtractedFiles(string clientFolder, bool deleteExtractedFiles)
        {
            if (deleteExtractedFiles)
            {
                try
                {
                    string[] filePaths = Directory.GetFiles(clientFolder);
                    foreach (string file in filePaths)
                    {
                        File.Delete(file);
                    }
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in deleting extracted files: " + ex);
                }
            }
        }
    }
}
