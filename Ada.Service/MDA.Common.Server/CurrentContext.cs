﻿using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using MDA.Pipeline.Config;
using System;
using System.Data.Entity;
using MDA.DAL;

namespace MDA.Common.Server
{
    /// <summary>
    /// A class to carry the current context through the layers.  In any layer you can get hold of the properties
    /// in this class.  It is populated at the web service level.
    /// </summary>
    public class CurrentContext : IDisposable 
    {
        private void SetupConfig(int riskClientId)
        {
            //DefaultClientSettings = PipelineSettings.DefaultClientModules;

            //if (riskClientId == -1) riskClientId = 0;

            //CustomClientSettings = PipelineSettings.ClientModules.GetItemByKey(riskClientId.ToString());

            //if (CustomClientSettings == null)
            //    throw new System.NotImplementedException("Client " + riskClientId.ToString() + " not defined in XML config");

            PipelineConfiguration pc = MDA.Pipeline.Config.PipelineConfig.GetConfig;

            DatabaseContext dbc = pc.DbConnections.GetItemByKey(riskClientId);

            if ( dbc == null )
                dbc = pc.DbConnections.GetItemByKey(0);

            //dbConnectionString = ((!string.IsNullOrEmpty(CustomClientSettings.AdaDbContext.ConnectionString))
            //                                              ? CustomClientSettings.AdaDbContext.ConnectionString 
            //                                              : DefaultClientSettings.AdaDbContext.ConnectionString);

            dbConnectionString = dbc.ConnectionString;

            dbConn = new EntityConnection(new EntityConnectionStringBuilder(dbConnectionString).ToString());

            this.db = new MdaDbContext(dbConn.ConnectionString);
        }

        public CurrentContext(int riskClientId, int riskUserId, string who, Func<ProgressSeverity, string, bool, int> showProgress = null)
        {
            SourceDescription = "ADA";

            PipelineSettings = MDA.Pipeline.Config.PipelineConfig.GetConfig;

            //PolicyPipelineSettings = PipelineSettings.PolicyPipelines[policyType];

            //PipelineSettings = pType; // PipelineConfig.GetConfig.PolicyPipelines[policyType];

            this.RiskClientId = riskClientId;
            this.RiskUserId = riskUserId;

            this.Who = who;

            this.ShowProgress = showProgress;

            this.TraceLoad = Convert.ToBoolean(PipelineSettings.TraceLoading.Value);
            this.TraceScore = Convert.ToBoolean(PipelineSettings.TraceScore.Value);
            this.TraceScoring = Convert.ToBoolean(PipelineSettings.TraceScoring.Value);
            this.TraceDeDupe = Convert.ToBoolean(PipelineSettings.TraceDeDupe.Value);

            this.SubmitDirect = Convert.ToBoolean(PipelineSettings.SubmitDirectFromPipeline.Value);
            this.ScoreDirect = Convert.ToBoolean(PipelineSettings.ScoreDirectFromPipeline.Value);
            this.ScoreWithLiveRules = Convert.ToBoolean(PipelineSettings.ScoreClaimsWithLiveRules.Value);

            SetupConfig(riskClientId);

            this.guid = Guid.NewGuid();
        }

        public CurrentContext(CurrentContext ctx)
        {
            SourceDescription = ctx.SourceDescription;

            PipelineSettings = ctx.PipelineSettings;
            //PolicyPipelineSettings = ctx.PolicyPipelineSettings;

            this.dbConnectionString = ctx.dbConnectionString;

            _riskClientId = ctx.RiskClientId;  // don't use property, use _riskClientId to stop SetConfi being called

            this.RiskUserId = ctx.RiskUserId;

            this.Who = ctx.Who;

            this.ShowProgress = ctx.ShowProgress;

            this.TraceLoad = ctx.TraceLoad;
            this.TraceScore = ctx.TraceScore;
            this.TraceScoring = ctx.TraceScoring;

            this.ScoreWithLiveRules = ctx.ScoreWithLiveRules;

            this.SourceRef = ctx.SourceRef;
            this.ClientName = ctx.ClientName;
            //this.CustomClientSettings = ctx.CustomClientSettings;
            //this.DefaultClientSettings = ctx.DefaultClientSettings;
            this.InsurersClientId = ctx.InsurersClientId;

            this.SubmitDirect = ctx.SubmitDirect;
            this.ScoreDirect = ctx.ScoreDirect;

            this.guid = Guid.NewGuid();

            this.dbConn = new EntityConnection(new EntityConnectionStringBuilder(this.dbConnectionString).ToString());

            this.db = new MdaDbContext(dbConn.ConnectionString);
        }

        public void Dispose()
        {
            this.db.Dispose();
        }

        public Guid guid { get; set; }

        /// <summary>
        /// The current database context if there is one
        /// </summary>
        public MdaDbContext db { get; set; }

        private EntityConnection dbConn { get; set; }
 
        public string dbConnectionString { get; set; }

        public PipelineConfiguration PipelineSettings { get; set; }
        //public PolicyPipeline PolicyPipelineSettings { get; set; }
        

        //public DefaultClientModule DefaultClientSettings { get; set; }

        //public ClientModule CustomClientSettings { get; set; }

        private int _riskClientId = 0;

        public int RiskClientId
        {
            get { return _riskClientId; }
            set { _riskClientId = value; SetupConfig(value); }
        }
        public int RiskUserId { get; set; }
        public int InsurersClientId { get; set; }
        public string ClientName { get; set; }

        /// <summary>
        /// The database Client record for the current executing client.
        /// </summary>
        //public RiskClient RiskClient { get; set; }

        /// <summary>
        /// A string containing the username of the person making the web service call
        /// </summary>
        public string Who { get; set; }

        /// <summary>
        /// The current SourceRef value to stamp on all new records created in the database
        /// </summary>
        public string SourceRef { get; set; }

        public bool TraceLoad { get; set; }
        public bool TraceScore { get; set; }
        public bool TraceScoring { get; set; }
        public bool TraceDeDupe { get; set; }

        public bool SubmitDirect { get; set; }
        public bool ScoreDirect { get; set; }
        public bool ScoreWithLiveRules { get; set; }

        public string SourceDescription { get; set; }

        public Func<ProgressSeverity, string, bool, int> ShowProgress { get; set; }
    }

    public enum ProgressSeverity
    {
        /// <summary>
        /// Just send the text to the console
        /// </summary>
        JustConsole = -1,

        /// <summary>
        /// Information
        /// </summary>
        Info = 0,

        /// <summary>
        /// Warning
        /// </summary>
        Warning = 1,

        /// <summary>
        /// Error/Exception
        /// </summary>
        Error = 2,

        /// <summary>
        /// Fatal error
        /// </summary>
        Fatal = 3
    }
}
