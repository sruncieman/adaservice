﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
--------------------------
--DisableForeignKeyConstraints
--------------------------
SET NOCOUNT ON
DECLARE @sql NVARCHAR(MAX) = N'';

;WITH x AS 
(
  SELECT DISTINCT obj = 
      QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id)) + '.' 
    + QUOTENAME(OBJECT_NAME(parent_object_id)) 
  FROM sys.foreign_keys
)
SELECT @sql += N'ALTER TABLE ' + obj + ' NOCHECK CONSTRAINT ALL;
' FROM x;

EXEC sp_executesql @sql;

GO

--------------------------
--TruncateTheData
--------------------------
DELETE [dbo].RuleGroup2RuleSet;
DELETE [dbo].RuleScore;
DELETE [dbo].RuleSet;
DELETE [dbo].RuleVariable;
DELETE [dbo].RuleClient;
DELETE [dbo].RuleGroup;

--------------------------
--LoadTheData
--------------------------
:r .\RulesData.sql

--------------------------
--EnableForeignKeyConstraints
--------------------------
SET NOCOUNT ON
DECLARE @sql NVARCHAR(MAX) = N'';


;WITH x AS 
(
  SELECT DISTINCT obj = 
      QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id)) + '.' 
    + QUOTENAME(OBJECT_NAME(parent_object_id)) 
  FROM sys.foreign_keys
)
SELECT @sql += N'ALTER TABLE ' + obj + ' WITH CHECK CHECK CONSTRAINT ALL;
' FROM x;
