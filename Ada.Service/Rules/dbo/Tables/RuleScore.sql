﻿CREATE TABLE [dbo].[RuleScore] (
    [RuleScoreId]       INT            IDENTITY (1, 1) NOT NULL,
    [RuleGroupId]       INT            CONSTRAINT [DF_RuleScore_RuleGroupID] DEFAULT ((1)) NOT NULL,
    [RootRuleSetId]     INT            NOT NULL,
    [RuleName]          NVARCHAR (128) NOT NULL,
    [ThenScore]         INT            CONSTRAINT [DF_RuleScore_ThenScore] DEFAULT ((0)) NOT NULL,
    [ElseScore]         INT            CONSTRAINT [DF_RuleScore_ElseScore] DEFAULT ((0)) NOT NULL,
    [ThenMessageLow]    NVARCHAR (512) NULL,
    [ElseMessageLow]    NVARCHAR (512) NULL,
    [ThenMessageMedium] NVARCHAR (512) NULL,
    [ElseMessageMedium] NVARCHAR (512) NULL,
    [ThenMessageHigh]   NVARCHAR (512) NULL,
    [ElseMessageHigh]   NVARCHAR (512) NULL,
    [ThenError]         NVARCHAR (512) NULL,
    [ElseError]         NVARCHAR (512) NULL,
    [CreatedDate]       SMALLDATETIME  NOT NULL,
    [ModifiedDate]      SMALLDATETIME  NULL,
    [CreatedBy]         NVARCHAR (50)  NOT NULL,
    [ModifiedBy]        NVARCHAR (50)  NULL,
    CONSTRAINT [PK_RuleScore] PRIMARY KEY CLUSTERED ([RuleScoreId] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [FK_RuleScore_RuleGroup] FOREIGN KEY ([RuleGroupId]) REFERENCES [dbo].[RuleGroup] ([RuleGroupId])
);


GO
ALTER TABLE [dbo].[RuleScore] NOCHECK CONSTRAINT [FK_RuleScore_RuleGroup];




GO
CREATE NONCLUSTERED INDEX [IX_RuleScore_RuleGroupId]
    ON [dbo].[RuleScore]([RuleGroupId] ASC);

