﻿CREATE TABLE [dbo].[RuleVariable] (
    [RuleVariableId] INT            IDENTITY (1, 1) NOT NULL,
    [RuleGroupId]    INT            CONSTRAINT [DF_RuleVariable_RuleGroupId] DEFAULT ((1)) NOT NULL,
    [VariableName]   NVARCHAR (128) NOT NULL,
    [VariableValue]  NVARCHAR (128) CONSTRAINT [DF_UserVariable_VariableValue] DEFAULT ('') NOT NULL,
    [CreatedDate]    SMALLDATETIME  NOT NULL,
    [ModifiedDate]   SMALLDATETIME  NULL,
    [CreatedBy]      NVARCHAR (50)  NOT NULL,
    [ModifiedBy]     NVARCHAR (50)  NULL,
    CONSTRAINT [PK_UserVariable] PRIMARY KEY CLUSTERED ([RuleVariableId] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [FK_RuleVariable_RuleGroup] FOREIGN KEY ([RuleGroupId]) REFERENCES [dbo].[RuleGroup] ([RuleGroupId])
);


GO
ALTER TABLE [dbo].[RuleVariable] NOCHECK CONSTRAINT [FK_RuleVariable_RuleGroup];



