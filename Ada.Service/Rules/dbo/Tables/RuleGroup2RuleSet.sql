﻿CREATE TABLE [dbo].[RuleGroup2RuleSet] (
    [RuleGroupId] INT NOT NULL,
    [RuleSetId]   INT NOT NULL,
    CONSTRAINT [PK_RuleGroup2RuleSet] PRIMARY KEY CLUSTERED ([RuleGroupId] ASC, [RuleSetId] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [FK_RuleGroup2RuleSet_RuleGroup] FOREIGN KEY ([RuleGroupId]) REFERENCES [dbo].[RuleGroup] ([RuleGroupId]),
    CONSTRAINT [FK_RuleGroup2RuleSet_RuleSet] FOREIGN KEY ([RuleSetId]) REFERENCES [dbo].[RuleSet] ([RuleSetId])
);


GO
ALTER TABLE [dbo].[RuleGroup2RuleSet] NOCHECK CONSTRAINT [FK_RuleGroup2RuleSet_RuleGroup];


GO
ALTER TABLE [dbo].[RuleGroup2RuleSet] NOCHECK CONSTRAINT [FK_RuleGroup2RuleSet_RuleSet];



