﻿CREATE TABLE [dbo].[RuleSet] (
    [RuleSetId]       INT            IDENTITY (1, 1) NOT NULL,
    [RootRuleSetId]   INT            NOT NULL,
    [Name]            NVARCHAR (128) NOT NULL,
    [AssemblyPath]    NVARCHAR (256) NULL,
    [EntityClassType] NVARCHAR (128) NULL,
    [Version]         INT            NOT NULL,
    [RuleSetXML]      NTEXT          NULL,
    [Status]          INT            NULL,
    [CreatedDate]     SMALLDATETIME  NOT NULL,
    [CreatedBy]       NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_RuleSet] PRIMARY KEY CLUSTERED ([RuleSetId] ASC)
);

