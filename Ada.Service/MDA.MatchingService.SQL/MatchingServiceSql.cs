﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MDA.MatchingService.Model;
using MDA.MatchingService.Interface;
using MDA.Common.Server;
using MDA.Common.Debug;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.DAL;
using System.Data.Entity;

namespace MDA.MatchingService.SQL
{
    /// <summary>
    /// An implementation of ImatchingService that performs matching using the SQL database matching SP's
    /// </summary>
    public class MatchingService : IMatchingService
    {
        private bool _trace = false;
        private CurrentContext _ctx;
        private MdaDbContext _db;


        public MatchingService(CurrentContext ctx)
        {
            _ctx = ctx;
            _db = ctx.db as MdaDbContext;
            _trace = _ctx.TraceLoad;
        }

        private MatchResults CreateInitialMatchingResults(object o)
        {
            MatchResults _DummyResults = new MatchResults(o)
            {
                MatchIdList = new List<int>(),
            };

            return _DummyResults;
        }


        private MatchResults CreateInitialSanctionsMatchingResults(object o)
        {
            MatchResults _DummyResults = new MatchResults(o)
            {
                MatchSanctionsList = new List<string>(),
            };

            return _DummyResults;
        }

        //public MdaDbContext SetDbContext
        //{
        //    set
        //    {
        //        _db = value;
        //        _ctx.db = value;
        //    }
        //}

        public CurrentContext CurrentContext
        {
            set
            {
                _ctx = value;
                _db = _ctx.db as MdaDbContext;
            }
        }

        private string CleanseXml(string xml)
        {
            string s = xml.Replace("&", "&amp;");

            return s;
        }

        private string FormatAddrInXml(PipelineAddress a)
        {
            StringBuilder newXml = new StringBuilder();

            if (a.PafValidation != 0 && !string.IsNullOrEmpty(a.PafUPRN))
            {
                if ((from x in _db.Addresses where x.PafUPRN == a.PafUPRN select x).Any())
                {
                    newXml.Append("PafUPRN=\"" + a.PafUPRN + "\" ");
                }
            }

            if (!string.IsNullOrEmpty(a.SubBuilding))
                newXml.Append("SubBuilding=\"" + a.SubBuilding + "\" ");

            if (!string.IsNullOrEmpty(a.Building))
                newXml.Append("Building=\"" + a.Building + "\" ");

            if (!string.IsNullOrEmpty(a.BuildingNumber))
                newXml.Append("BuildingNumber=\"" + a.BuildingNumber + "\" ");

            if (!string.IsNullOrEmpty(a.Street))
                newXml.Append("Street=\"" + a.Street + "\" ");

            if (!string.IsNullOrEmpty(a.Locality))
                newXml.Append("Locality=\"" + a.Locality + "\" ");

            if (!string.IsNullOrEmpty(a.Town))
                newXml.Append("Town=\"" + a.Town + "\" ");

            if (!string.IsNullOrEmpty(a.PostCode))
                newXml.Append("PostCode=\"" + a.PostCode + "\" ");

            if (newXml.Length > 0)
                return "<Address " + newXml.ToString() + "/>";

            return string.Empty;
        }

        private string FormatAddrInXml(Address a)
        {
            StringBuilder newXml = new StringBuilder();

            if (a.PafValidation != 0 && !string.IsNullOrEmpty(a.PafUPRN))
            {
                if ((from x in _db.Addresses where x.PafUPRN == a.PafUPRN select x).Any())
                {
                    newXml.Append("PafUPRN=\"" + a.PafUPRN + "\" ");
                }
            }

            if (!string.IsNullOrEmpty(a.SubBuilding))
                newXml.Append("SubBuilding=\"" + a.SubBuilding + "\" ");

            if (!string.IsNullOrEmpty(a.Building))
                newXml.Append("Building=\"" + a.Building + "\" ");

            if (!string.IsNullOrEmpty(a.BuildingNumber))
                newXml.Append("BuildingNumber=\"" + a.BuildingNumber + "\" ");

            if (!string.IsNullOrEmpty(a.Street))
                newXml.Append("Street=\"" + a.Street + "\" ");

            if (!string.IsNullOrEmpty(a.Locality))
                newXml.Append("Locality=\"" + a.Locality + "\" ");

            if (!string.IsNullOrEmpty(a.Town))
                newXml.Append("Town=\"" + a.Town + "\" ");

            if (!string.IsNullOrEmpty(a.PostCode))
                newXml.Append("PostCode=\"" + a.PostCode + "\" ");

            if (newXml.Length > 0)
                return "<Address " + newXml.ToString() + "/>";

            return string.Empty;
        }

        private int InsurerClientId(string insurerClient)
        {
            int ret = (from x in this._db.InsurersClients
                       where x.ClientName == insurerClient
                       select x.Id).FirstOrDefault();

            return ret;     
            
        }

        /// <summary>
        /// Given the incidentId of a record in the database will attempt to find matches using normal matching rules. Works
        /// by extracting the incident and associated records to rebuild the XML needed for the matching SP.
        /// </summary>
        /// <param name="incidentId"></param>
        /// <param name="existingRiskClaimId"></param>
        /// <returns></returns>
        public MatchResults MatchExistingClaim(int incidentId, int? existingRiskClaimId)
        {
            var incident = (from i in _db.Incidents where i.Id == incidentId select i).FirstOrDefault();

            if (incident == null) return null;

            MatchResults matchResults = CreateInitialMatchingResults(incident);

            StringBuilder xml = new StringBuilder();
            xml.Append("<Claim>");

            //if ( claim != null && claim.InsurersClientId != null )
            //    xml.Append("<ClientID>" + claim.InsurersClientId.ToString() + "</ClientID>");

            xml.Append("<IncidentDate>" + incident.IncidentDate.ToString("dd-MMM-yyyy") + "</IncidentDate>");

            #region Process PEOPLE (and ORGS) linked to Incident to find the POLICYHOLDER

            _db.Entry(incident).Collection(z => z.Incident2Person).Query()
                .Where(p => p.RiskClaim_Id == existingRiskClaimId && p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            bool foundPolicyHolder = false;

            if (incident.Incident2Person.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var i2p in incident.Incident2Person)
                {
                    if (i2p.PartyType_Id == (int)MDA.Common.Enum.PartyType.Policyholder) // && i2p.IncidentTime != null)
                    {
                        foundPolicyHolder = true;

                        if (i2p.IncidentTime != null)
                            newXml.Append("<IncidentTime>" + i2p.IncidentTime + "</IncidentTime>");
                                        

                        if (i2p.IncidentLocation != null)
                            newXml.Append("<IncidentLocation>" + i2p.IncidentLocation + "</IncidentLocation>");

                        if (i2p.IncidentCircs != null)
                            newXml.Append("<IncidentCircumstances>" + i2p.IncidentCircs + "</IncidentCircumstances>");



                        newXml.Append("<ClientClaimReference>" + i2p.ClaimNumber + "</ClientClaimReference>");

                        if (i2p.Insurer != null)
                        {
                            newXml.Append("<ClientID>" + InsurerClientId(i2p.Insurer) + "</ClientID>");
                        }

                        if (newXml.Length > 0)
                            xml.Append(newXml.ToString());
                    }

                }
            }

            #region Try to find ORG that is POLICYHOLDER
            if (!foundPolicyHolder)
            {
                _db.Entry(incident).Collection(z => z.Incident2Organisation).Query()
                    .Where(p => p.RiskClaim_Id == existingRiskClaimId && p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

                if (incident.Incident2Organisation.Any())
                {
                    StringBuilder newXml = new StringBuilder();

                    foreach (var i2o in incident.Incident2Organisation)
                    {
                        if (i2o.PartyType_Id == (int)MDA.Common.Enum.PartyType.Policyholder)
                        {
                            foundPolicyHolder = true;

                            if (i2o.IncidentTime != null)
                                newXml.Append("<IncidentTime>" + i2o.IncidentTime + "</IncidentTime>");
                            
                            if (i2o.IncidentLocation != null)
                                newXml.Append("<IncidentLocation>" + i2o.IncidentLocation + "</IncidentLocation>");

                            if (i2o.IncidentCircs != null)
                                newXml.Append("<IncidentCircumstances>" + i2o.IncidentCircs + "</IncidentCircumstances>");

                            newXml.Append("<ClientClaimReference>" + i2o.ClaimNumber + "</ClientClaimReference>");

                            if (i2o.Insurer != null)
                            {
                                newXml.Append("<ClientID>" + InsurerClientId(i2o.Insurer) + "</ClientID>");
                            }

                            break;
                        }
                    }

                    if (newXml.Length > 0)
                        xml.Append(newXml.ToString());
                }
            }
            #endregion

            if (!foundPolicyHolder)
            {
                if (incident.Incident2Person.Any())
                {
                    StringBuilder newXml = new StringBuilder();

                    int numberOfInsuredDrivers = 0;

                    foreach (var i2p in incident.Incident2Person)
                    {
                        if (i2p.PartyType_Id == (int)MDA.Common.Enum.PartyType.Insured && i2p.SubPartyType_Id ==  (int)MDA.Common.Enum.SubPartyType.Driver && numberOfInsuredDrivers == 0)// && i2p.IncidentTime != null)
                        {
                            numberOfInsuredDrivers++;

                            if (i2p.IncidentTime != null)
                                newXml.Append("<IncidentTime>" + i2p.IncidentTime + "</IncidentTime>");

                            if (i2p.IncidentLocation != null)
                                newXml.Append("<IncidentLocation>" + i2p.IncidentLocation + "</IncidentLocation>");

                            if (i2p.IncidentCircs != null)
                                newXml.Append("<IncidentCircumstances>" + i2p.IncidentCircs + "</IncidentCircumstances>");

                            newXml.Append("<ClientClaimReference>" + i2p.ClaimNumber + "</ClientClaimReference>");

                            if (i2p.Insurer != null)
                            {
                                newXml.Append("<ClientID>" + InsurerClientId(i2p.Insurer) + "</ClientID>");
                            }

                            if (newXml.Length > 0)
                                xml.Append(newXml.ToString());
                        }
                    }
                }
            }
            #endregion

            List<int> vehicleIds = new List<int>();
            List<int> peopleIds = new List<int>();
            List<int> uVehicleIds = new List<int>();
            List<int> uPeopleIds = new List<int>();

            #region Process any CURRENT and CONFIRMED I2V records to get list of Vehicle ID's and Person ID's
            _db.Entry(incident).Collection(z => z.Incident2Vehicle).Query()
                    .Where(p => p.RiskClaim_Id == existingRiskClaimId && p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (incident.Incident2Vehicle.Any())
            {
                foreach (var i2v in incident.Incident2Vehicle)
                {
                    Vehicle vehicle = i2v.Vehicle;

                    _db.Entry(vehicle).Collection(z => z.Vehicle2Person).Query()
                            .Where(p => p.RiskClaim_Id == existingRiskClaimId && p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

                    if (vehicle.Vehicle2Person.Any())
                    {
                        foreach (var v2p in vehicle.Vehicle2Person)
                        {
                            var MatchPersonRes = MatchExistingPerson(v2p.Person_Id, existingRiskClaimId);

                            if (MatchPersonRes.MatchTypeFound == MatchType.Confirmed)
                            {
                                peopleIds.AddRange(MatchPersonRes.MatchIdList);
                            }
                            else if (MatchPersonRes.MatchTypeFound == MatchType.Unconfirmed)
                            {
                                uPeopleIds.AddRange(MatchPersonRes.MatchIdList);
                            }

                        }
                    }

                    var matchRes = MatchExistingVehicle(vehicle.Id, existingRiskClaimId);

                    if (matchRes.MatchTypeFound == MatchType.Confirmed)
                    {
                        vehicleIds.AddRange(matchRes.MatchIdList);
                        }
                    else if (matchRes.MatchTypeFound == MatchType.Unconfirmed)
                    {
                        uVehicleIds.AddRange(matchRes.MatchIdList);
                    }                           
                }
            }

            #endregion

            // Remove Duplicates
            vehicleIds = vehicleIds.Distinct().ToList();
            uVehicleIds = uVehicleIds.Distinct().ToList();
            peopleIds = peopleIds.Distinct().ToList();
            uPeopleIds = uPeopleIds.Distinct().ToList();

            // Remove confirmed ID's from the unconfirmed lists
            uVehicleIds = uVehicleIds.Except(vehicleIds).ToList();
            uPeopleIds = uPeopleIds.Except(peopleIds).ToList();

            if (vehicleIds.Count > 0)
            {
                xml.Append("<Vehicles>");
                foreach (int vid in vehicleIds)
                    xml.Append("<Vehicle>" + vid.ToString() + "</Vehicle>");
                xml.Append("</Vehicles>");
            }

            //if (uVehicleIds.Count > 0)
            //{
            //    xml.Append("<uVehicles>");
            //    foreach (int vid in uVehicleIds)
            //        xml.Append("<Vehicle>" + vid.ToString() + "</Vehicle>");
            //    xml.Append("</uVehicles>");
            //}

            if (peopleIds.Count > 0)
            {
                xml.Append("<People>");
                foreach (int pid in peopleIds)
                    xml.Append("<Person>" + pid.ToString() + "</Person>");
                xml.Append("</People>");
            }

            //if (uPeopleIds.Count > 0)
            //{
            //    xml.Append("<uPeople>");
            //    foreach (int pid in uPeopleIds)
            //        xml.Append("<Person>" + pid.ToString() + "</Person>");
            //    xml.Append("</uPeople>");
            //}

            xml.Append("</Claim>");

            string xmls = CleanseXml(xml.ToString());   // Remove any &

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("MatchExistingClaim XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter pp = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var matchedIncidents = _db.uspGetMatchingClaim(xmls, pp, true).ToList();

            if (matchedIncidents != null && matchedIncidents.Count > 0)
            {
                if (_trace)
                {
                    ADATrace.WriteLine(string.Format("Matched Incidents: Count {0}, Type {1}, Rule {2}, Using first value ID {3} ", matchedIncidents.Count, matchedIncidents[0].MatchType, matchedIncidents[0].RuleNo, (int)matchedIncidents[0].ID));
                }

                matchResults.MatchIdList.Add((int)matchedIncidents[0].ID);
                matchResults.MatchTypeFound = (MatchType)matchedIncidents[0].MatchType;
                matchResults.MatchRuleNumber = matchedIncidents[0].RuleNo;
            }
            else
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matched Incidents NULL or EMPTY");
                }
            }
            return matchResults;
        }

        public MatchResults MatchClaim(PipelineMobileClaim pipeClaim, int? existingIncidentId, int? existingRiskClaimId)
        {
            MatchResults matchResults = CreateInitialMatchingResults(pipeClaim);

            StringBuilder xml = new StringBuilder();
            xml.Append("<Claim>");

            //if (existingRiskClaimId != null)
            //    xml.Append("<RiskClaim_ID>" + existingRiskClaimId.ToString() + "</RiskClaims_ID>");

            xml.Append("<ClientClaimReference>" + pipeClaim.ClaimNumber + "</ClientClaimReference>");
            xml.Append("<ClientID>" + _ctx.InsurersClientId.ToString() + "</ClientID>");

            if (pipeClaim.IncidentDate != null)
            {
                xml.Append("<IncidentDate>" + pipeClaim.IncidentDate.ToString("dd-MMM-yyyy") + "</IncidentDate>");

                if (pipeClaim.OriginalIncidentDateTime != null)
                {
                    if (pipeClaim.OriginalIncidentDateTime.Hour != 0 || pipeClaim.OriginalIncidentDateTime.Minute != 0 || pipeClaim.OriginalIncidentDateTime.Second != 0)
                        xml.Append("<IncidentTime>" + pipeClaim.OriginalIncidentDateTime.ToShortTimeString() + "</IncidentTime>");
                }
            }

            if (pipeClaim.ExtraClaimInfo != null && !string.IsNullOrEmpty(pipeClaim.ExtraClaimInfo.IncidentLocation))
                xml.Append("<IncidentLocation>" + pipeClaim.ExtraClaimInfo.IncidentLocation + "</IncidentLocation>");

            if (pipeClaim.ExtraClaimInfo != null && !string.IsNullOrEmpty(pipeClaim.ExtraClaimInfo.IncidentCircumstances))
                xml.Append("<IncidentCircumstances>" + pipeClaim.ExtraClaimInfo.IncidentCircumstances + "</IncidentCircumstances>");

            //List<int> handsetIds = new List<int>();
            List<int> peopleIds = new List<int>();
            //List<int> uHandsetIds = new List<int>();
            List<int> uPeopleIds = new List<int>();

            foreach (var handset in pipeClaim.Handsets)
            {
                List<MDA.Pipeline.Model.PipelineAddress> addresses = new List<MDA.Pipeline.Model.PipelineAddress>();

                foreach (var person in handset.People)
                {
                    foreach (var address in person.Addresses)
                    {
                        addresses.Add(address);
                    }

                    var MatchPersonRes = MatchPerson(person, handset, pipeClaim, existingRiskClaimId);

                    if (MatchPersonRes.MatchTypeFound == MatchType.Confirmed)
                    {
                        peopleIds.AddRange(MatchPersonRes.MatchIdList);
                    }
                    else if (MatchPersonRes.MatchTypeFound == MatchType.Unconfirmed)
                    {
                        uPeopleIds.AddRange(MatchPersonRes.MatchIdList);
                    }
                }

                //var matchRes = MatchHandsetIMEI(handset, existingRiskClaimId);

                //if (matchRes.MatchTypeFound == MatchType.Confirmed)
                //{
                //    handsetIds.AddRange(matchRes.MatchIdList);
                //}
                //else if (matchRes.MatchTypeFound == MatchType.Unconfirmed)
                //{
                //    uHandsetIds.AddRange(matchRes.MatchIdList);
                //}
            }

            // Remove Duplicates
            //handsetIds = handsetIds.Distinct().ToList();
            //uHandsetIds = uHandsetIds.Distinct().ToList();
            peopleIds = peopleIds.Distinct().ToList();
            uPeopleIds = uPeopleIds.Distinct().ToList();

            // Remove confirmed ID's from the unconfirmed lists
            //uHandsetIds = uHandsetIds.Except(handsetIds).ToList();
            uPeopleIds = uPeopleIds.Except(peopleIds).ToList();

            //if (handsetIds.Count > 0)
            //{
            //    xml.Append("<Handsets>");
            //    foreach (int hid in handsetIds)
            //        xml.Append("<Handset>" + hid.ToString() + "</Handset>");
            //    xml.Append("</Handsets>");
            //}

            if (pipeClaim.Handsets.Where(x => x.I2H_LinkData.Incident2HandsetLinkType_Id == (int)MDA.Common.Enum.Incident2HandsetLinkType.InsuredHandset).ToList().Count > 0)
            {

                xml.Append("<Handsets>");
                foreach (PipelineHandset h in pipeClaim.Handsets)
                {
                    if (h.I2H_LinkData.Incident2HandsetLinkType_Id == (int)MDA.Common.Enum.Incident2HandsetLinkType.InsuredHandset && !string.IsNullOrWhiteSpace(h.HandsetIMEI))
                    {
                        xml.Append("<Handset HandsetImei=" + "\"" + h.HandsetIMEI.ToString() + "\"/>");
                    }
                }
                xml.Append("</Handsets>");
            }

            //if (uVehicleIds.Count > 0)
            //{
            //    xml.Append("<uVehicles>");
            //    foreach (int vid in uVehicleIds)
            //        xml.Append("<Vehicle>" + vid.ToString() + "</Vehicle>");
            //    xml.Append("</uVehicles>");
            //}

            if (peopleIds.Count > 0)
            {
                xml.Append("<People>");
                foreach (int pid in peopleIds)
                    xml.Append("<Person>" + pid.ToString() + "</Person>");
                xml.Append("</People>");
            }

            //if (uPeopleIds.Count > 0)
            //{
            //    xml.Append("<uPeople>");
            //    foreach (int pid in uPeopleIds)
            //        xml.Append("<Person>" + pid.ToString() + "</Person>");
            //    xml.Append("</uPeople>");
            //}

            xml.Append("</Claim>");

            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("MatchClaim XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var matchedIncidents = _db.uspGetMatchingClaim(xmls, p, true).ToList();

            _db.SaveChanges();

            if (matchedIncidents != null && matchedIncidents.Count > 0)
            {
                matchResults.MatchIdList.Add((int)matchedIncidents[0].ID);
                matchResults.MatchTypeFound = (MatchType)matchedIncidents[0].MatchType;
                matchResults.MatchRuleNumber = matchedIncidents[0].RuleNo;
            }

            if (matchResults.MatchRuleNumber == -1 && existingIncidentId != null)
            {
                matchResults.MatchIdList.Clear();

                matchResults.MatchIdList.Add((int)existingIncidentId);
                //matchResults.CompleteMatchId = existingIncidentId;
                matchResults.MatchTypeFound = MatchType.Confirmed;
                matchResults.MatchRuleNumber = 999;
                return matchResults;
            }

            return matchResults;


        }

        public MatchResults MatchClaim(PipelinePIClaim pipeClaim, int? existingIncidentId, int? existingRiskClaimId)
        {
            MatchResults matchResults = CreateInitialMatchingResults(pipeClaim);

            StringBuilder xml = new StringBuilder();
            xml.Append("<Claim>");

            //if (existingRiskClaimId != null)
            //    xml.Append("<RiskClaim_ID>" + existingRiskClaimId.ToString() + "</RiskClaims_ID>");

            xml.Append("<ClientClaimReference>" + pipeClaim.ClaimNumber + "</ClientClaimReference>");
            xml.Append("<ClientID>" + _ctx.InsurersClientId.ToString() + "</ClientID>");

            if (pipeClaim.IncidentDate != null)
            {
                xml.Append("<IncidentDate>" + pipeClaim.IncidentDate.ToString("dd-MMM-yyyy") + "</IncidentDate>");

                if (pipeClaim.OriginalIncidentDateTime != null)
                {
                    if (pipeClaim.OriginalIncidentDateTime.Hour != 0 || pipeClaim.OriginalIncidentDateTime.Minute != 0 || pipeClaim.OriginalIncidentDateTime.Second != 0)
                        xml.Append("<IncidentTime>" + pipeClaim.OriginalIncidentDateTime.ToShortTimeString() + "</IncidentTime>");
                }
            }

            if (pipeClaim.ExtraClaimInfo != null && !string.IsNullOrEmpty(pipeClaim.ExtraClaimInfo.IncidentLocation))
                xml.Append("<IncidentLocation>" + pipeClaim.ExtraClaimInfo.IncidentLocation + "</IncidentLocation>");

            if (pipeClaim.ExtraClaimInfo != null && !string.IsNullOrEmpty(pipeClaim.ExtraClaimInfo.IncidentCircumstances))
                xml.Append("<IncidentCircumstances>" + pipeClaim.ExtraClaimInfo.IncidentCircumstances + "</IncidentCircumstances>");

            List<int> peopleIds = new List<int>();
            List<int> uPeopleIds = new List<int>();

            List<MDA.Pipeline.Model.PipelineAddress> addresses = new List<MDA.Pipeline.Model.PipelineAddress>();

            var person = pipeClaim.Person;

            foreach (var address in person.Addresses)
            {
                addresses.Add(address);
            }

            var nullVehicle = new PipelineVehicle();
            nullVehicle = null;

            var MatchPersonRes = MatchPerson(person, nullVehicle, pipeClaim, existingRiskClaimId);

            if (MatchPersonRes.MatchTypeFound == MatchType.Confirmed)
            {
                peopleIds.AddRange(MatchPersonRes.MatchIdList);
            }
            else if (MatchPersonRes.MatchTypeFound == MatchType.Unconfirmed)
            {
                uPeopleIds.AddRange(MatchPersonRes.MatchIdList);
            }

            // Remove Duplicates
            peopleIds = peopleIds.Distinct().ToList();
            uPeopleIds = uPeopleIds.Distinct().ToList();

            // Remove confirmed ID's from the unconfirmed lists
            uPeopleIds = uPeopleIds.Except(peopleIds).ToList();


            if (peopleIds.Count > 0)
            {
                xml.Append("<People>");
                foreach (int pid in peopleIds)
                    xml.Append("<Person>" + pid.ToString() + "</Person>");
                xml.Append("</People>");
            }

            //if (uPeopleIds.Count > 0)
            //{
            //    xml.Append("<uPeople>");
            //    foreach (int pid in uPeopleIds)
            //        xml.Append("<Person>" + pid.ToString() + "</Person>");
            //    xml.Append("</uPeople>");
            //}

            xml.Append("</Claim>");

            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("MatchClaim XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var matchedIncidents = _db.uspGetMatchingClaim(xmls, p, true).ToList();

            _db.SaveChanges();

            if (matchedIncidents != null && matchedIncidents.Count > 0)
            {
                matchResults.MatchIdList.Add((int)matchedIncidents[0].ID);
                matchResults.MatchTypeFound = (MatchType)matchedIncidents[0].MatchType;
                matchResults.MatchRuleNumber = matchedIncidents[0].RuleNo;
            }

            if (matchResults.MatchRuleNumber == -1 && existingIncidentId != null)
            {
                matchResults.MatchIdList.Clear();

                matchResults.MatchIdList.Add((int)existingIncidentId);
                //matchResults.CompleteMatchId = existingIncidentId;
                matchResults.MatchTypeFound = MatchType.Confirmed;
                matchResults.MatchRuleNumber = 999;
                return matchResults;
            }

            return matchResults;


        }

        public MatchResults MatchClaim(PipelineMotorClaim pipeClaim, int? existingIncidentId, int? existingRiskClaimId)
        {
            MatchResults matchResults = CreateInitialMatchingResults(pipeClaim);

            StringBuilder xml = new StringBuilder();
            xml.Append("<Claim>");

            //if (existingRiskClaimId != null)
            //    xml.Append("<RiskClaim_ID>" + existingRiskClaimId.ToString() + "</RiskClaims_ID>");

            xml.Append("<ClientClaimReference>" + pipeClaim.ClaimNumber + "</ClientClaimReference>");
            xml.Append("<ClientID>" + _ctx.InsurersClientId.ToString() + "</ClientID>");

            if (pipeClaim.IncidentDate != null)
            {
                xml.Append("<IncidentDate>" + pipeClaim.IncidentDate.ToString("dd-MMM-yyyy") + "</IncidentDate>");

                if (pipeClaim.OriginalIncidentDateTime != null)
                {
                    if (pipeClaim.OriginalIncidentDateTime.Hour != 0 || pipeClaim.OriginalIncidentDateTime.Minute != 0 || pipeClaim.OriginalIncidentDateTime.Second != 0)
                        xml.Append("<IncidentTime>" + pipeClaim.OriginalIncidentDateTime.ToShortTimeString() + "</IncidentTime>");
                }
            }



            if ( pipeClaim.ExtraClaimInfo != null && !string.IsNullOrEmpty(pipeClaim.ExtraClaimInfo.IncidentLocation) )
                xml.Append("<IncidentLocation>" + pipeClaim.ExtraClaimInfo.IncidentLocation + "</IncidentLocation>");

            if (pipeClaim.ExtraClaimInfo != null && !string.IsNullOrEmpty(pipeClaim.ExtraClaimInfo.IncidentCircumstances))
                xml.Append("<IncidentCircumstances>" + pipeClaim.ExtraClaimInfo.IncidentCircumstances + "</IncidentCircumstances>");

            List<int> vehicleIds = new List<int>();
            List<int> peopleIds = new List<int>();
            List<int> uVehicleIds = new List<int>();
            List<int> uPeopleIds = new List<int>();

            foreach (var vehicle in pipeClaim.Vehicles)
            {
                List<MDA.Pipeline.Model.PipelineAddress> addresses = new List<MDA.Pipeline.Model.PipelineAddress>();

                foreach (var person in vehicle.People)
                {
                    foreach (var address in person.Addresses)
                    {
                        addresses.Add(address);
                    }

                    var MatchPersonRes = MatchPerson(person, vehicle, pipeClaim, existingRiskClaimId);

                    if (MatchPersonRes.MatchTypeFound == MatchType.Confirmed ) 
                    {
                        peopleIds.AddRange(MatchPersonRes.MatchIdList);
                    }
                    else if (MatchPersonRes.MatchTypeFound == MatchType.Unconfirmed )
                    {
                        uPeopleIds.AddRange(MatchPersonRes.MatchIdList);
                    }
                }
                
                var matchRes = MatchVehicle(vehicle, addresses, existingRiskClaimId);

                if (matchRes.MatchTypeFound == MatchType.Confirmed ) 
                {
                    vehicleIds.AddRange(matchRes.MatchIdList);
                }
                else if (matchRes.MatchTypeFound == MatchType.Unconfirmed)
                {
                    uVehicleIds.AddRange(matchRes.MatchIdList);
                }
            }

            // Remove Duplicates
            vehicleIds = vehicleIds.Distinct().ToList();
            uVehicleIds = uVehicleIds.Distinct().ToList();
            peopleIds = peopleIds.Distinct().ToList();
            uPeopleIds = uPeopleIds.Distinct().ToList();

            // Remove confirmed ID's from the unconfirmed lists
            uVehicleIds = uVehicleIds.Except(vehicleIds).ToList();
            uPeopleIds  = uPeopleIds.Except(peopleIds).ToList();

            //Vehicle List

            if (vehicleIds.Count > 0 || uVehicleIds.Count > 0)
            {
                xml.Append("<Vehicles>");
            }

            if (vehicleIds.Count > 0 )
            {
                foreach(int vid in vehicleIds)
                    xml.Append("<Vehicle>" + vid.ToString() + "</Vehicle>");                
            }

            if (uVehicleIds.Count > 0)
            {
                foreach (int vid in uVehicleIds)
                    xml.Append("<Vehicle>" + vid.ToString() + "</Vehicle>");
            }

            if (vehicleIds.Count > 0 || uVehicleIds.Count > 0)
            {
                xml.Append("</Vehicles>");
            }

            //People List

            if (peopleIds.Count > 0 || uPeopleIds.Count > 0)
            {
                xml.Append("<People>");              
            }

            if (peopleIds.Count > 0)
            {
                foreach(int pid in peopleIds)
                    xml.Append("<Person>" + pid.ToString() + "</Person>");  
            }

            if (uPeopleIds.Count > 0)
            {
                foreach (int pid in uPeopleIds)
                    xml.Append("<Person>" + pid.ToString() + "</Person>");
                
            }

            if (peopleIds.Count > 0 || uPeopleIds.Count > 0)
            {
                xml.Append("</People>");
            }


            xml.Append("</Claim>");

            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("MatchClaim XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var matchedIncidents = _db.uspGetMatchingClaim(xmls, p, true).ToList();

            _db.SaveChanges();

            if (matchedIncidents != null && matchedIncidents.Count > 0)
            {
                matchResults.MatchIdList.Add((int)matchedIncidents[0].ID);
                matchResults.MatchTypeFound = (MatchType)matchedIncidents[0].MatchType;
                matchResults.MatchRuleNumber = matchedIncidents[0].RuleNo;
            }

            if (matchResults.MatchRuleNumber == -1 && existingIncidentId != null)
            {
                matchResults.MatchIdList.Clear();

                matchResults.MatchIdList.Add((int)existingIncidentId);
                //matchResults.CompleteMatchId = existingIncidentId;
                matchResults.MatchTypeFound = MatchType.Confirmed;
                matchResults.MatchRuleNumber = 999;
                return matchResults;
            }

            return matchResults;
        }

        public MatchResults MatchEmailAddress(MDA.Pipeline.Model.PipelineEmail pipeEmail, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(pipeEmail);

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingEmail(existingRiskClaimId, pipeEmail.EmailAddress, p).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchExistingPolicy(int policyId, int? existingRiskClaimId)
        {
            var policy = (from p in _db.Policies where p.Id == policyId select p).FirstOrDefault();

            if (policy == null) return null;

            MatchResults mr = CreateInitialMatchingResults(policy);

            System.Data.Entity.Core.Objects.ObjectParameter pp = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingPolicy(existingRiskClaimId, policy.PolicyNumber, pp).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchPolicy(PipelinePolicy policy, int? IncidentId, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(policy);

            if (existingRiskClaimId != null && IncidentId != null)
            {
                var i2p = (from xx in _db.Incident2Policy where xx.Incident_Id == IncidentId select xx).FirstOrDefault();

                if (i2p != null)
                {
                    mr.MatchIdList.Add(i2p.Policy_Id);
                    mr.MatchTypeFound = MatchType.Confirmed;
                    mr.MatchRuleNumber = 999;
                    return mr;
                }
            }

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingPolicy(existingRiskClaimId, policy.PolicyNumber, p).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchPassportNumber(MDA.Pipeline.Model.PipelinePassport pipePassport, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(pipePassport);

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingPassport(existingRiskClaimId, pipePassport.PassportNumber, p).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchNINumber(MDA.Pipeline.Model.PipelineNINumber pipeNiNumber, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(pipeNiNumber);

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingNINumber(existingRiskClaimId, pipeNiNumber.NINumber1, p).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchLicenseNumber(MDA.Pipeline.Model.PipelineDrivingLicense pipeDrivingLicense, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(pipeDrivingLicense);

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingDrivingLicense(existingRiskClaimId, pipeDrivingLicense.DriverNumber, p).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }
   
        public MatchResults MatchWebSite(MDA.Pipeline.Model.PipelineWebSite pipeWebsite, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(pipeWebsite);

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingWebSite(existingRiskClaimId, pipeWebsite.URL, p).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        //public void ProcessTelephoneNumber(PipelineTelephone telephone)
        //{
        //    //using (MdaDbContext db = new MdaDbContext(ctx.dbConnection.ConnectionString))
        //    //{
        //        var x = _db.uspGetMatchingTelephone(telephone.ClientSuppliedNumber).ToList();
        //        _db.SaveChanges();

        //        // one record.  With ID it was found. even if not found reformatted telephone returned
        //        if (x.Count() == 1)
        //        {
        //            telephone.AreaCode = x[0].AreaCode;
        //            telephone.FullNumber = x[0].Number;
        //            telephone.InternationalCode = x[0].IntCode;
        //        }

        //        if (telephone.FullNumber != null)
        //        {
        //            if (telephone.FullNumber.StartsWith("07"))
        //                telephone.TelephoneType_Id = (int)Common.Enum.TelephoneType.Mobile;
        //            else if (telephone.TelephoneType_Id == (int)Common.Enum.TelephoneType.Mobile)
        //                telephone.TelephoneType_Id = (int)Common.Enum.TelephoneType.Unknown;
        //        }
        //    //}
        //}

        public MatchResults MatchTelephoneNumber(PipelineTelephone telephone, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(telephone);

            var x = _db.uspGetMatchingTelephone(telephone.ClientSuppliedNumber).ToList();
            _db.SaveChanges();

            // one record.  With ID it was found. even if not found reformatted telephone returned
            if (x.Count() == 1)
            {
                if (x[0].ID != null)
                {
                    mr.MatchTypeFound = MatchType.Confirmed;
                    mr.MatchIdList.Add((int)x[0].ID);
                    mr.MatchRuleNumber = 998;

                }

                telephone.AreaCode = x[0].AreaCode;
                telephone.FullNumber = x[0].Number;
                telephone.InternationalCode = x[0].IntCode;
            }
           
            return mr;
        }

        public MatchResults MatchExistingBankAccount(int accountId, int? existingRiskClaimId)
        {
            var bankAccount = (from p in _db.BankAccounts where p.Id == accountId select p).FirstOrDefault();

            if (bankAccount == null) return null;

            MatchResults mr = CreateInitialMatchingResults(bankAccount);

            System.Data.Entity.Core.Objects.ObjectParameter pp = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingBankAccount(existingRiskClaimId, bankAccount.AccountNumber, bankAccount.SortCode,
                                                            bankAccount.BankName, pp).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchBankAccount(PipelineBankAccount bankAccount, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(bankAccount);

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingBankAccount(existingRiskClaimId, bankAccount.AccountNumber, bankAccount.SortCode, 
                                                            bankAccount.BankName, p).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchExistingPaymentCard(int cardId, int? existingRiskClaimId)
        {
            var paymentCard = (from p in _db.PaymentCards where p.Id == cardId select p).FirstOrDefault();

            if (paymentCard == null) return null;

            MatchResults mr = CreateInitialMatchingResults(paymentCard);

            System.Data.Entity.Core.Objects.ObjectParameter pp = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingPaymentCard(existingRiskClaimId, paymentCard.PaymentCardNumber, paymentCard.SortCode,
                                                            paymentCard.BankName, paymentCard.ExpiryDate, pp).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchPaymentCard(PipelinePaymentCard paymentCard, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(paymentCard);

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingPaymentCard(existingRiskClaimId, paymentCard.PaymentCardNumber, paymentCard.SortCode,
                                                            paymentCard.BankName, paymentCard.ExpiryDate, p).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchExistingPerson(int personId, int? existingRiskClaimId)
        {
            var person = (from p in _db.People where p.Id == personId select p).FirstOrDefault();

            if (person == null) return null;

            MatchResults mr = CreateInitialMatchingResults(person);

            string DoB = (person.DateOfBirth == null) ? "" : ((DateTime)person.DateOfBirth).ToString("dd-MMM-yyyy");

            StringBuilder xml = new StringBuilder(string.Format("<Person RiskClaim_Id=\"{0}\" FirstName=\"{1}\" LastName=\"{2}\" DateOfBirth=\"{3}\" >",
                 existingRiskClaimId, person.FirstName, person.LastName, DoB));

            _db.Entry(person).Collection(z => z.Person2NINumber).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (person.Person2NINumber.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var p2ni in person.Person2NINumber)
                {
                    if (!string.IsNullOrEmpty(p2ni.NINumber.NINumber1))
                    {
                        newXml.Append(string.Format("<NINumber NINumber=\"{0}\"/>", p2ni.NINumber.NINumber1));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<NINumbers>" + newXml + "</NINumbers>");
            }

            _db.Entry(person).Collection(z => z.Person2DrivingLicense).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (person.Person2DrivingLicense.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var p2n in person.Person2DrivingLicense)
                {
                    if (!string.IsNullOrEmpty(p2n.DrivingLicense.DriverNumber))
                    {
                        newXml.Append(string.Format("<DrivingLicense DriverNumber=\"{0}\"/>", p2n.DrivingLicense.DriverNumber));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<DrivingLicenses>" + newXml + "</DrivingLicenses>");
            }

            _db.Entry(person).Collection(z => z.Person2Passport).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (person.Person2Passport.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var p2n in person.Person2Passport)
                {
                    if (!string.IsNullOrEmpty(p2n.Passport.PassportNumber))
                    {
                        newXml.Append(string.Format("<Passport PassportNumber=\"{0}\"/>", p2n.Passport.PassportNumber));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Passports>" + newXml + "</Passports>");
            }

            _db.Entry(person).Collection(z => z.Person2Email).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (person.Person2Email.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var p2e in person.Person2Email)
                {
                    if (!string.IsNullOrEmpty(p2e.Email.EmailAddress))
                    {
                        newXml.Append(string.Format("<EmailAddress EmailAddress=\"{0}\" />", p2e.Email.EmailAddress));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<EmailAddresses>" + newXml + "</EmailAddresses>");
            }

            _db.Entry(person).Collection(z => z.Person2PaymentCard).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (person.Person2PaymentCard.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var p2c in person.Person2PaymentCard)
                {
                    if (!string.IsNullOrEmpty(p2c.PaymentCard.PaymentCardNumber))
                    {
                        newXml.Append(string.Format("<PaymentCard PaymentCardNumber=\"{0}\"/>", p2c.PaymentCard.PaymentCardNumber));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<PaymentCards>" + newXml + "</PaymentCards>");
            }

            _db.Entry(person).Collection(z => z.Person2BankAccount).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (person.Person2BankAccount.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var b2a in person.Person2BankAccount)
                {
                    if (!string.IsNullOrEmpty(b2a.BankAccount.AccountNumber))
                    {
                        newXml.Append(string.Format("<BankAccount AccountNumber=\"{0}\"/>", b2a.BankAccount.AccountNumber));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<BankAccounts>" + newXml + "</BankAccounts>");
            }

            _db.Entry(person).Collection(z => z.Person2Policy).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (person.Person2Policy.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var p2p in person.Person2Policy)
                {
                    if (!string.IsNullOrEmpty(p2p.Policy.PolicyNumber))
                    {
                        newXml.Append(string.Format("<Policy PolicyNumber=\"{0}\"/>", p2p.Policy.PolicyNumber));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Policies>" + newXml + "</Policies>"); 
            }

            _db.Entry(person).Collection(z => z.Person2Address).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (person.Person2Address.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var p2a in person.Person2Address)
                {
                    newXml.Append(FormatAddrInXml(p2a.Address));
                }
                if (newXml.Length > 0)
                    xml.Append("<Addresses>" + newXml + "</Addresses>");
            }

            _db.Entry(person).Collection(z => z.Person2Telephone).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (person.Person2Telephone.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var p2t in person.Person2Telephone)
                {
                    if (!string.IsNullOrEmpty(p2t.Telephone.TelephoneNumber))
                    {
                        newXml.Append(string.Format("<Telephone TelephoneNumber=\"{0}\"/>", p2t.Telephone.TelephoneNumber));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Telephones>" + newXml + "</Telephones>");
            }

            _db.Entry(person).Collection(z => z.Vehicle2Person).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (person.Vehicle2Person.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach( var v2p in person.Vehicle2Person )
                {
                    var vehicle = v2p.Vehicle;

                    bool regExists = !string.IsNullOrEmpty(vehicle.VehicleRegistration) && 
                        (from vv in _db.Vehicles
                         where vv.VehicleRegistration == vehicle.VehicleRegistration
                         select vv).Any();

                    bool vinExists = !string.IsNullOrEmpty(vehicle.VIN) && (from vv in _db.Vehicles
                                                                        where vv.VIN == vehicle.VIN
                                                                        select vv).Any();

                    if (regExists || vinExists)
                    {
                        newXml.Append("<Vehicle ");

                        if (regExists)
                            newXml.Append(string.Format("VehicleRegistration=\"{0}\" ", vehicle.VehicleRegistration));

                        if (vinExists)
                            newXml.Append(string.Format("VehicleVIN=\"{0}\" ", vehicle.VIN));

                        newXml.Append(" />");
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Vehicles>" + newXml + "</Vehicles>");
            }

            _db.Entry(person).Collection(z => z.Person2Organisation).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (person.Person2Organisation.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var p2o in person.Person2Organisation)
                {
                    var o = p2o.Organisation;

                    bool nameExists = !string.IsNullOrEmpty(o.OrganisationName) && (from oo in _db.Organisations
                                                                                    where oo.OrganisationName == o.OrganisationName
                                                                                    select oo).Any();

                    bool regNumExists = !string.IsNullOrEmpty(o.RegisteredNumber) && (from oo in _db.Organisations
                                                                                      where oo.RegisteredNumber == o.RegisteredNumber
                                                                                      select oo).Any();

                    bool vatNumExists = !string.IsNullOrEmpty(o.VATNumber) && (from oo in _db.Organisations
                                                                               where oo.VATNumber == o.VATNumber
                                                                               select oo).Any();
                    if (nameExists || regNumExists || vatNumExists)
                    {
                        newXml.Append("<Organisation ");
                        if (nameExists)
                            newXml.Append(string.Format("OrganisationName=\"{0}\" ", o.OrganisationName));

                        if (regNumExists)
                            newXml.Append(string.Format("RegisteredNumber=\"{0}\" ", o.RegisteredNumber));

                        if (vatNumExists)
                            newXml.Append(string.Format("VATNumber=\"{0}\" ", o.VATNumber));

                        newXml.Append("/>");
                    }
                }
                if (newXml.Length > 0)
                    xml.Append("<Organisations>" + newXml + "</Organisations>");
            }

            xml.Append("</Person>");

            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("MatchPerson XML: " + xmls);
            }
            System.Data.Entity.Core.Objects.ObjectParameter pp = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var xx = _db.uspGetMatchingPerson(xmls, pp).ToList();
            _db.SaveChanges();

            if (xx.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)xx[0].MatchType;
                mr.MatchRuleNumber = xx[0].RuleNo;

                foreach (var row in xx)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }
        
        public MatchResults MatchSanctionsPerson(PipelinePerson person, int? existingBaseRiskClaimId)
        {
            MatchResults mr = CreateInitialSanctionsMatchingResults(person);

            string DoB = (person.DateOfBirth == null) ? "" : ((DateTime)person.DateOfBirth).ToString("dd-MMM-yyyy");

            StringBuilder xml = new StringBuilder(string.Format("<Person FirstName=\"{0}\" LastName=\"{1}\" DateOfBirth=\"{2}\" >",
                 person.FirstName, person.LastName, DoB));

            if (person.NINumbers.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var nii in person.NINumbers)
                {
                    if (!string.IsNullOrEmpty(nii.NINumber1))
                    {
                         newXml.Append(string.Format("<NINumber NINumber=\"{0}\"/>", nii.NINumber1));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<NINumbers>" + newXml + "</NINumbers>");
            }

            if (person.PassportNumbers.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var pn in person.PassportNumbers)
                {
                    if (!string.IsNullOrEmpty(pn.PassportNumber))
                    {
                        {
                            newXml.Append(string.Format("<Passport PassportNumber=\"{0}\"/>", pn.PassportNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Passports>" + newXml + "</Passports>");
            }


            if (person.Addresses.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var a in person.Addresses)
                {
                    newXml.Append(FormatAddrInXml(a));
                }

                if (newXml.Length > 0)
                    xml.Append("<Addresses>" + newXml + "</Addresses>");
            }

            xml.Append("</Person>");

            string xmls = CleanseXml(xml.ToString());

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var xx = _db.uspGetMatchingSanctionsPerson(xmls, p).ToList();
            _db.SaveChanges();

            if (xx.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)xx[0].MatchType;
                mr.MatchRuleNumber = xx[0].RuleNo;

                foreach (var row in xx)
                    mr.MatchSanctionsList.Add(row.ID);
            }

            return mr;
        }
        
        public MatchResults MatchPerson(PipelinePerson person, PipelineVehicle vehicle, IPipelineClaim claim, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(person);

            string DoB = (person.DateOfBirth == null) ? "" : ((DateTime)person.DateOfBirth).ToString("dd-MMM-yyyy");

            StringBuilder xml = new StringBuilder(string.Format("<Person RiskClaim_Id=\"{0}\" FirstName=\"{1}\" LastName=\"{2}\" DateOfBirth=\"{3}\" >",
                 existingRiskClaimId, person.FirstName, person.LastName, DoB));

            if (person.NINumbers.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var nii in person.NINumbers)
                {
                    if (!string.IsNullOrEmpty(nii.NINumber1))
                    {
                        if ((from ni in _db.NINumbers
                             where ni.NINumber1 == nii.NINumber1
                             select ni).Any())
                        {
                            newXml.Append(string.Format("<NINumber NINumber=\"{0}\"/>", nii.NINumber1));
                        }
                    }
                }

                if ( newXml.Length > 0 )
                    xml.Append("<NINumbers>" + newXml + "</NINumbers>");
            }

            if (person.DrivingLicenseNumbers.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var pn in person.DrivingLicenseNumbers)
                {
                    if (!string.IsNullOrEmpty(pn.DriverNumber))
                    {
                        if ((from x in _db.DrivingLicenses
                             where x.DriverNumber == pn.DriverNumber
                             select x).Any())
                        {
                            newXml.Append(string.Format("<DrivingLicense DriverNumber=\"{0}\"/>", pn.DriverNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<DrivingLicenses>" + newXml + "</DrivingLicenses>");
            }

            if (person.PassportNumbers.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var pn in person.PassportNumbers)
                {
                    if (!string.IsNullOrEmpty(pn.PassportNumber))
                    {
                        if ((from x in _db.Passports
                             where x.PassportNumber == pn.PassportNumber
                             select x).Any())
                        {
                            newXml.Append(string.Format("<Passport PassportNumber=\"{0}\"/>", pn.PassportNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Passports>" + newXml + "</Passports>");
            }

            if (person.EmailAddresses.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var e in person.EmailAddresses)
                {
                    if (!string.IsNullOrEmpty(e.EmailAddress))
                    {
                        if ((from x in _db.Emails
                             where x.EmailAddress == e.EmailAddress
                             select x).Any())
                        {
                            newXml.Append(string.Format("<EmailAddress EmailAddress=\"{0}\" />", e.EmailAddress));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<EmailAddresses>" + newXml + "</EmailAddresses>");
            }

            if (person.PaymentCards.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var pc in person.PaymentCards)
                {
                    if (!string.IsNullOrEmpty(pc.PaymentCardNumber))
                    {
                        if ((from x in _db.PaymentCards
                             where x.PaymentCardNumber == pc.PaymentCardNumber
                             select x).Any())
                        {
                            newXml.Append(string.Format("<PaymentCard PaymentCardNumber=\"{0}\"/>", pc.PaymentCardNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<PaymentCards>" + newXml + "</PaymentCards>");
            }

            if (person.BankAccounts.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var ba in person.BankAccounts)
                {
                    if (!string.IsNullOrEmpty(ba.AccountNumber))
                    {
                        if ((from x in _db.BankAccounts
                             where x.AccountNumber == ba.AccountNumber
                             select x).Any())
                        {
                            newXml.Append(string.Format("<BankAccount AccountNumber=\"{0}\"/>", ba.AccountNumber));
                        }
                    }
                }
                if (newXml.Length > 0)
                    xml.Append("<BankAccounts>" + newXml + "</BankAccounts>");
            }


            if (claim.Policy != null && !string.IsNullOrEmpty(claim.Policy.PolicyNumber))
            {
                if ((from x in _db.Policies
                        where x.PolicyNumber == claim.Policy.PolicyNumber
                        select x).Any())
                {
                    xml.Append(string.Format("<Policies><Policy PolicyNumber=\"{0}\"/></Policies>", claim.Policy.PolicyNumber));
                }
            }

            if (person.Addresses.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var a in person.Addresses)
                {
                    newXml.Append(FormatAddrInXml(a));
                }

                if (newXml.Length > 0)
                    xml.Append("<Addresses>" + newXml + "</Addresses>");
            }

            if (person.Telephones.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var tn in person.Telephones)
                {
                    if (!string.IsNullOrEmpty(tn.FullNumber))
                    {
                        if ((from tt in _db.Telephones
                             where tt.TelephoneNumber == tn.FullNumber
                             select tt).Any())
                        {
                            newXml.Append(string.Format("<Telephone TelephoneNumber=\"{0}\"/>", tn.FullNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Telephones>" + newXml + "</Telephones>");
            }

            if (vehicle != null)
            {
                StringBuilder newXml = new StringBuilder();

                bool regExists = !string.IsNullOrEmpty(vehicle.VehicleRegistration) && (from vv in _db.Vehicles
                                                                                        where vv.VehicleRegistration == vehicle.VehicleRegistration
                                                                                        select vv).Any();

                bool vinExists = !string.IsNullOrEmpty(vehicle.VIN) && (from vv in _db.Vehicles
                                                                        where vv.VIN == vehicle.VIN
                                                                        select vv).Any();

                if (regExists || vinExists)
                {
                    newXml.Append("<Vehicle ");
                    
                    if (regExists)
                        newXml.Append(string.Format("VehicleRegistration=\"{0}\" ", vehicle.VehicleRegistration));

                    if (vinExists)
                        newXml.Append(string.Format("VehicleVIN=\"{0}\" ", vehicle.VIN));

                    newXml.Append("/>");
                }

                if (newXml.Length > 0)
                    xml.Append("<Vehicles>" + newXml + "</Vehicles>");
            }


            if (person.Organisations.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var o in person.Organisations)
                {
                    if (MDA.Common.Helpers.MatchingHelper.IncludeOrganisationInPersonMatch((MDA.Common.Enum.Person2OrganisationLinkType)o.P2O_LinkData.Person2OrganisationLinkType_Id))
                    {
                        bool nameExists = !string.IsNullOrEmpty(o.OrganisationName) && (from oo in _db.Organisations
                                                                                        where oo.OrganisationName == o.OrganisationName
                                                                                        select oo).Any();

                        bool regNumExists = !string.IsNullOrEmpty(o.RegisteredNumber) && (from oo in _db.Organisations
                                                                                          where oo.RegisteredNumber == o.RegisteredNumber
                                                                                          select oo).Any();

                        bool vatNumExists = !string.IsNullOrEmpty(o.VatNumber) && (from oo in _db.Organisations
                                                                                   where oo.VATNumber == o.VatNumber
                                                                                   select oo).Any();
                        if (nameExists || regNumExists || vatNumExists)
                        {
                            newXml.Append("<Organisation ");
                            if (nameExists)
                                newXml.Append(string.Format("OrganisationName=\"{0}\" ", o.OrganisationName));

                            if (regNumExists)
                                newXml.Append(string.Format("RegisteredNumber=\"{0}\" ", o.RegisteredNumber));

                            if (vatNumExists)
                                newXml.Append(string.Format("VATNumber=\"{0}\" ", o.VatNumber));

                            newXml.Append("/>");
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Organisations>" + newXml + "</Organisations>");
            }
            xml.Append("</Person>");

            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("MatchPerson XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var xx = _db.uspGetMatchingPerson(xmls, p).ToList();
            _db.SaveChanges();

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("uspGetMatchingPerson returned : " + xx.Count() + " results.");
            }

            if (xx.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)xx[0].MatchType;
                mr.MatchRuleNumber = xx[0].RuleNo;

                foreach (var row in xx)
                {
                    mr.MatchIdList.Add((int)row.ID);

                    if (_trace)
                    {
                        ADATrace.WriteLine("");
                        ADATrace.WriteLine("Adding row ID " + row.ID + " to MatchIdList");
                    }
                }
            }

            return mr;
        }

        public MatchResults MatchPerson(PipelinePerson person, PipelineHandset handset, IPipelineClaim claim, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(person);

            string DoB = (person.DateOfBirth == null) ? "" : ((DateTime)person.DateOfBirth).ToString("dd-MMM-yyyy");

            StringBuilder xml = new StringBuilder(string.Format("<Person RiskClaim_Id=\"{0}\" FirstName=\"{1}\" LastName=\"{2}\" DateOfBirth=\"{3}\" >",
                 existingRiskClaimId, person.FirstName, person.LastName, DoB));

            if (person.NINumbers.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var nii in person.NINumbers)
                {
                    if (!string.IsNullOrEmpty(nii.NINumber1))
                    {
                        if ((from ni in _db.NINumbers
                             where ni.NINumber1 == nii.NINumber1
                             select ni).Any())
                        {
                            newXml.Append(string.Format("<NINumber NINumber=\"{0}\"/>", nii.NINumber1));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<NINumbers>" + newXml + "</NINumbers>");
            }

            if (person.DrivingLicenseNumbers.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var pn in person.DrivingLicenseNumbers)
                {
                    if (!string.IsNullOrEmpty(pn.DriverNumber))
                    {
                        if ((from x in _db.DrivingLicenses
                             where x.DriverNumber == pn.DriverNumber
                             select x).Any())
                        {
                            newXml.Append(string.Format("<DrivingLicense DriverNumber=\"{0}\"/>", pn.DriverNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<DrivingLicenses>" + newXml + "</DrivingLicenses>");
            }

            if (person.PassportNumbers.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var pn in person.PassportNumbers)
                {
                    if (!string.IsNullOrEmpty(pn.PassportNumber))
                    {
                        if ((from x in _db.Passports
                             where x.PassportNumber == pn.PassportNumber
                             select x).Any())
                        {
                            newXml.Append(string.Format("<Passport PassportNumber=\"{0}\"/>", pn.PassportNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Passports>" + newXml + "</Passports>");
            }

            if (person.EmailAddresses.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var e in person.EmailAddresses)
                {
                    if (!string.IsNullOrEmpty(e.EmailAddress))
                    {
                        if ((from x in _db.Emails
                             where x.EmailAddress == e.EmailAddress
                             select x).Any())
                        {
                            newXml.Append(string.Format("<EmailAddress EmailAddress=\"{0}\" />", e.EmailAddress));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<EmailAddresses>" + newXml + "</EmailAddresses>");
            }

            if (person.PaymentCards.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var pc in person.PaymentCards)
                {
                    if (!string.IsNullOrEmpty(pc.PaymentCardNumber))
                    {
                        if ((from x in _db.PaymentCards
                             where x.PaymentCardNumber == pc.PaymentCardNumber
                             select x).Any())
                        {
                            newXml.Append(string.Format("<PaymentCard PaymentCardNumber=\"{0}\"/>", pc.PaymentCardNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<PaymentCards>" + newXml + "</PaymentCards>");
            }

            if (person.BankAccounts.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var ba in person.BankAccounts)
                {
                    if (!string.IsNullOrEmpty(ba.AccountNumber))
                    {
                        if ((from x in _db.BankAccounts
                             where x.AccountNumber == ba.AccountNumber
                             select x).Any())
                        {
                            newXml.Append(string.Format("<BankAccount AccountNumber=\"{0}\"/>", ba.AccountNumber));
                        }
                    }
                }
                if (newXml.Length > 0)
                    xml.Append("<BankAccounts>" + newXml + "</BankAccounts>");
            }


            if (claim.Policy != null && !string.IsNullOrEmpty(claim.Policy.PolicyNumber))
            {
                if ((from x in _db.Policies
                     where x.PolicyNumber == claim.Policy.PolicyNumber
                     select x).Any())
                {
                    xml.Append(string.Format("<Policies><Policy PolicyNumber=\"{0}\"/></Policies>", claim.Policy.PolicyNumber));
                }
            }

            if (person.Addresses.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var a in person.Addresses)
                {
                    newXml.Append(FormatAddrInXml(a));
                }

                if (newXml.Length > 0)
                    xml.Append("<Addresses>" + newXml + "</Addresses>");
            }

            if (person.Telephones.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var tn in person.Telephones)
                {
                    if (!string.IsNullOrEmpty(tn.FullNumber))
                    {
                        if ((from tt in _db.Telephones
                             where tt.TelephoneNumber == tn.FullNumber
                             select tt).Any())
                        {
                            newXml.Append(string.Format("<Telephone TelephoneNumber=\"{0}\"/>", tn.FullNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Telephones>" + newXml + "</Telephones>");
            }

            if (handset != null)
            {
                StringBuilder newXml = new StringBuilder();

                bool imeiExists = !string.IsNullOrEmpty(handset.HandsetIMEI) && (from hh in _db.Handsets
                                                                                        where hh.HandsetIMEI == handset.HandsetIMEI
                                                                                        select hh).Any();

                //bool vinExists = !string.IsNullOrEmpty(vehicle.VIN) && (from vv in _db.Vehicles
                //                                                        where vv.VIN == vehicle.VIN
                //                                                        select vv).Any();

                if (imeiExists)
                {
                    newXml.Append("<Handset ");

                    if (imeiExists)
                        newXml.Append(string.Format("HandsetImei=\"{0}\" ", handset.HandsetIMEI));

                    //if (vinExists)
                    //    newXml.Append(string.Format("VehicleVIN=\"{0}\" ", vehicle.VIN));

                    newXml.Append("/>");
                }

                if (newXml.Length > 0)
                    xml.Append("<Handsets>" + newXml + "</Handsets>");
            }


            if (person.Organisations.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var o in person.Organisations)
                {
                    if (MDA.Common.Helpers.MatchingHelper.IncludeOrganisationInPersonMatch((MDA.Common.Enum.Person2OrganisationLinkType)o.P2O_LinkData.Person2OrganisationLinkType_Id))
                    {
                        bool nameExists = !string.IsNullOrEmpty(o.OrganisationName) && (from oo in _db.Organisations
                                                                                        where oo.OrganisationName == o.OrganisationName
                                                                                        select oo).Any();

                        bool regNumExists = !string.IsNullOrEmpty(o.RegisteredNumber) && (from oo in _db.Organisations
                                                                                          where oo.RegisteredNumber == o.RegisteredNumber
                                                                                          select oo).Any();

                        bool vatNumExists = !string.IsNullOrEmpty(o.VatNumber) && (from oo in _db.Organisations
                                                                                   where oo.VATNumber == o.VatNumber
                                                                                   select oo).Any();
                        if (nameExists || regNumExists || vatNumExists)
                        {
                            newXml.Append("<Organisation ");
                            if (nameExists)
                                newXml.Append(string.Format("OrganisationName=\"{0}\" ", o.OrganisationName));

                            if (regNumExists)
                                newXml.Append(string.Format("RegisteredNumber=\"{0}\" ", o.RegisteredNumber));

                            if (vatNumExists)
                                newXml.Append(string.Format("VATNumber=\"{0}\" ", o.VatNumber));

                            newXml.Append("/>");
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Organisations>" + newXml + "</Organisations>");
            }
            xml.Append("</Person>");

            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("MatchPerson XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var xx = _db.uspGetMatchingPerson(xmls, p).ToList();
            _db.SaveChanges();

            if (xx.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)xx[0].MatchType;
                mr.MatchRuleNumber = xx[0].RuleNo;

                foreach (var row in xx)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchExistingVehicle(int vehicleId, int? existingRiskClaimId)
        {
            var vehicle = (from x in _db.Vehicles.Include("Vehicle2Person") where x.Id == vehicleId select x).FirstOrDefault();

            if (vehicle == null) return null;

            MatchResults mr = CreateInitialMatchingResults(vehicle);

            //string vColour = (vehicle.VehicleColour_Id == (int)Common.Enum.VehicleColour.Unknown) ? "" : vehicle.VehicleColour_Id.ToString();

            //StringBuilder xml = new StringBuilder(string.Format("<Vehicle RiskClaim_Id=\"{0}\" VehicleMake=\"{1}\" Model=\"{2}\" VehicleRegistration=\"{3}\" Colour=\"{4}\" VIN=\"{5}\">",
            //       existingRiskClaimId, vehicle.VehicleMake, vehicle.Model, vehicle.VehicleRegistration, vColour, vehicle.VIN));

            StringBuilder xml = new StringBuilder();
            StringBuilder newXml = new StringBuilder();

            if (existingRiskClaimId != null)
                newXml.AppendFormat("RiskClaim_Id=\"{0}\" ", existingRiskClaimId);

            if (!string.IsNullOrEmpty(vehicle.VehicleMake))
                newXml.AppendFormat("VehicleMake=\"{0}\" ", vehicle.VehicleMake);

            if (!string.IsNullOrEmpty(vehicle.Model))
                newXml.AppendFormat("Model=\"{0}\" ", vehicle.Model);

            if (!string.IsNullOrEmpty(vehicle.VehicleRegistration))
                newXml.AppendFormat("VehicleRegistration=\"{0}\" ", vehicle.VehicleRegistration);

            if (vehicle.VehicleColour_Id != (int)Common.Enum.VehicleColour.Unknown)
                newXml.AppendFormat("Colour=\"{0}\" ", vehicle.VehicleColour_Id.ToString());

            if (!string.IsNullOrEmpty(vehicle.VIN))
                newXml.AppendFormat("VIN=\"{0}\" ", vehicle.VIN);

            if (newXml.Length == 0)
                return mr;

            xml.Append("<Vehicle " + newXml + ">");

            if (vehicle.Vehicle2Person != null)
            {
                newXml = new StringBuilder();

                foreach (var v2p in vehicle.Vehicle2Person)
                {
                    foreach (var p in v2p.Person.Person2Address)
                    {
                        //found = true;
                        newXml.Append("<Address ");

                        if (p.Address.PafValidation != 0 && !string.IsNullOrEmpty(p.Address.PafUPRN))
                            newXml.Append("PafUPRN=\"" + p.Address.PafUPRN + "\" ");

                        newXml.Append(string.Format("BuildingNumber=\"{0}\" PostCode=\"{1}\" ", p.Address.BuildingNumber, p.Address.PostCode));

                        newXml.Append("/>");
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Addresses>" + newXml + "</Addresses>");

            }

            xml.Append("</Vehicle>");

            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("Match Vehicle XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter pp = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var m = _db.uspGetMatchingVehicle(xmls, pp).ToList();
            _db.SaveChanges();

            if (m.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)m[0].MatchType;
                mr.MatchRuleNumber = m[0].RuleNo;

                foreach (var row in m)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchHandset(PipelineHandset handset, List<PipelineAddress> addresses, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(handset);

            if (existingRiskClaimId != null && handset.I2H_LinkData.Incident2HandsetLinkType_Id == (int)Common.Enum.Incident2HandsetLinkType.NoHandsetInvolved)
            {
                var i2hRec = (from i2h in _db.Incident2Handset
                              where i2h.RiskClaim_Id == existingRiskClaimId
                                  && i2h.Incident2HandsetLinkType_Id == (int)Common.Enum.Incident2HandsetLinkType.NoHandsetInvolved
                              select i2h).FirstOrDefault();
                _db.SaveChanges();

                if (i2hRec != null)
                {
                    mr.MatchTypeFound = MatchType.Confirmed;

                    mr.MatchIdList.Add(i2hRec.Handset_Id);

                    return mr;
                }
            }

            StringBuilder xml = new StringBuilder();
            StringBuilder newXml = new StringBuilder();

            if (existingRiskClaimId != null)
                newXml.AppendFormat("RiskClaim_Id=\"{0}\" ", existingRiskClaimId);

            if (!string.IsNullOrEmpty(handset.HandsetMake))
                newXml.AppendFormat("HandsetMake=\"{0}\" ", handset.HandsetMake);

            if (!string.IsNullOrEmpty(handset.HandsetModel))
                newXml.AppendFormat("Model=\"{0}\" ", handset.HandsetModel);

            if (!string.IsNullOrEmpty(handset.HandsetIMEI))
                newXml.AppendFormat("HandsetIMEI=\"{0}\" ", handset.HandsetIMEI);

            if (handset.HandsetColour_Id != (int)Common.Enum.HandsetColour.Unknown)
                newXml.AppendFormat("Colour=\"{0}\" ", handset.HandsetColour_Id.ToString());

            if (newXml.Length == 0)
                return mr;

            xml.Append("<Handset " + newXml + ">");

            if (addresses.Any())
            {
                newXml = new StringBuilder();

                foreach (var a in addresses)
                {
                    newXml.Append("<Address ");

                    if (a.PafValidation != 0 && !string.IsNullOrEmpty(a.PafUPRN))
                        newXml.Append("PafUPRN=\"" + a.PafUPRN + "\" ");

                    newXml.Append(string.Format("BuildingNumber=\"{0}\" PostCode=\"{1}\" ", a.BuildingNumber, a.PostCode));

                    newXml.Append("/>");
                }

                if (newXml.Length > 0)
                    xml.Append("<Addresses>" + newXml + "</Addresses>");
            }

            xml.Append("</Handset>");

            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("Match Handset XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            // Need to change for Handset
            //var x = _db.uspGetMatchingVehicle(xmls, p).ToList();
            //_db.SaveChanges();

            //if (x.Count() > 0)
            //{
            //    mr.MatchTypeFound = (MatchType)x[0].MatchType;
            //    mr.MatchRuleNumber = x[0].RuleNo;

            //    foreach (var row in x)
            //        mr.MatchIdList.Add((int)row.ID);
            //}

            return mr;
        }

        public MatchResults MatchHandsetIMEI(PipelineHandset handset, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(handset);

            if (!string.IsNullOrWhiteSpace(handset.HandsetIMEI))
            {

                if (_trace)
                {
                    ADATrace.WriteLine("");
                    ADATrace.WriteLine("Match Handset XML: " + handset.HandsetIMEI);
                }

                System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

                var x = _db.uspGetMatchingHandsetIMEI(existingRiskClaimId, handset.HandsetIMEI, p).ToList();

                if (x.Count() > 0)
                {
                    mr.MatchTypeFound = (MatchType)x[0].MatchType;
                    mr.MatchRuleNumber = x[0].RuleNo;

                    foreach (var row in x)
                        mr.MatchIdList.Add((int)row.Id);
                }
            }

            return mr;
        }

        public MatchResults MatchVehicle(PipelineVehicle vehicle, List<PipelineAddress> addresses, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(vehicle);

            if (existingRiskClaimId != null && vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Common.Enum.Incident2VehicleLinkType.NoVehicleInvolved)
            {
                var i2vRec = (from i2v in _db.Incident2Vehicle
                            where i2v.RiskClaim_Id == existingRiskClaimId 
                                && i2v.Incident2VehicleLinkType_Id == (int)Common.Enum.Incident2VehicleLinkType.NoVehicleInvolved
                            select i2v).FirstOrDefault();
                _db.SaveChanges();

                if (i2vRec != null)
                {
                    mr.MatchTypeFound = MatchType.Confirmed;

                    mr.MatchIdList.Add(i2vRec.Vehicle_Id);

                    return mr;
                }
            }

            StringBuilder xml = new StringBuilder();
            StringBuilder newXml = new StringBuilder();

            if (existingRiskClaimId != null)
                newXml.AppendFormat("RiskClaim_Id=\"{0}\" ", existingRiskClaimId);

            if (!string.IsNullOrEmpty(vehicle.VehicleMake))
                newXml.AppendFormat("VehicleMake=\"{0}\" ", vehicle.VehicleMake);

            if (!string.IsNullOrEmpty(vehicle.VehicleModel))
                newXml.AppendFormat("Model=\"{0}\" ", vehicle.VehicleModel);

            if (!string.IsNullOrEmpty(vehicle.VehicleRegistration))
                newXml.AppendFormat("VehicleRegistration=\"{0}\" ", vehicle.VehicleRegistration);

            if (vehicle.VehicleColour_Id != (int)Common.Enum.VehicleColour.Unknown)
                newXml.AppendFormat("Colour=\"{0}\" ", vehicle.VehicleColour_Id.ToString());

            if (!string.IsNullOrEmpty(vehicle.VIN))
                newXml.AppendFormat("VIN=\"{0}\" ", vehicle.VIN);

            if (newXml.Length == 0)
                return mr;

            xml.Append("<Vehicle " + newXml + ">");

            if (addresses.Any())
            {
                newXml = new StringBuilder();

                foreach (var a in addresses)
                {
                    newXml.Append("<Address ");

                    if (a.PafValidation != 0 && !string.IsNullOrEmpty(a.PafUPRN))
                        newXml.Append("PafUPRN=\"" + a.PafUPRN + "\" ");

                    newXml.Append(string.Format("BuildingNumber=\"{0}\" PostCode=\"{1}\" ", a.BuildingNumber, a.PostCode));

                    newXml.Append("/>");
                }

                if (newXml.Length > 0)
                    xml.Append("<Addresses>" + newXml + "</Addresses>");
            }

            xml.Append("</Vehicle>");

            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("Match Vehicle XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingVehicle(xmls, p).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="addressId">Address ID. Assumed UPRN is null or blank and ADARecordStatus will be 10</param>
        /// <param name="existingRiskClaimId"></param>
        /// <returns></returns>
        public MatchResults MatchExistingAddress(int addressId, int? existingRiskClaimId)
        {
            var address = (from i in _db.Addresses where i.Id == addressId select i).FirstOrDefault();

            if (address == null) return null;

            MatchResults mr = CreateInitialMatchingResults(address);

            //if (address.PafValidation != 0 && !string.IsNullOrEmpty(address.PafUPRN))
            //{
            //    var addr = (from xx in _db.Addresses where xx.PafUPRN == address.PafUPRN select xx).FirstOrDefault();

            //    if (addr != null)
            //    {
            //        mr.MatchTypeFound = MatchType.Confirmed;
            //        mr.MatchRuleNumber = 999;
            //        mr.MatchIdList.Add(addr.Id);

            //        return mr;
            //    }
            //}

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingAddress(existingRiskClaimId, address.SubBuilding, address.Building, address.BuildingNumber,
                                                 address.Street, address.Locality, address.Town, address.PostCode, "", p).ToList();
            _db.SaveChanges();

            if (x.Count() > 0)
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchAddress(PipelineAddress address, string orgName, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(address);

            if (address.PafValidation != 0 && !string.IsNullOrEmpty(address.PafUPRN))
            {
                var addr = (from xx in _db.Addresses where xx.PafUPRN == address.PafUPRN && xx.ADARecordStatus == 0 select xx).FirstOrDefault();

                if ( addr != null )
                {
                    mr.MatchTypeFound = MatchType.Confirmed;
                    mr.MatchRuleNumber = 999;
                    mr.MatchIdList.Add(addr.Id);

                    return mr;
                }
            }

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var x = _db.uspGetMatchingAddress(existingRiskClaimId, address.SubBuilding, address.Building, address.BuildingNumber,
                                                 address.Street, address.Locality, address.Town, address.PostCode, orgName, p).ToList();
            _db.SaveChanges();

            if (x.Any())
            {
                mr.MatchTypeFound = (MatchType)x[0].MatchType;
                mr.MatchRuleNumber = x[0].RuleNo;

                foreach (var row in x)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchExistingOrganisation(int orgId, int? existingRiskClaimId)
        {
            var organisation = (from p in _db.Organisations where p.Id == orgId select p).FirstOrDefault();

            if (organisation == null) return null;

            MatchResults mr = CreateInitialMatchingResults(organisation);

            StringBuilder xml = new StringBuilder();
            StringBuilder nwXml = new StringBuilder();

            if (existingRiskClaimId != null)
                nwXml.AppendFormat("RiskClaim_Id=\"{0}\" ", existingRiskClaimId);

            if (!string.IsNullOrEmpty(organisation.OrganisationName))
                nwXml.AppendFormat("OrganisationName=\"{0}\" ", organisation.OrganisationName);

            if (!string.IsNullOrEmpty(organisation.RegisteredNumber))
                nwXml.AppendFormat("RegisteredNumber=\"{0}\" ", organisation.RegisteredNumber);

            if (!string.IsNullOrEmpty(organisation.VATNumber))
                nwXml.AppendFormat("VATNumber=\"{0}\" ", organisation.VATNumber);

            if (nwXml.Length == 0)
                return mr;

            xml.Append("<Organisation " + nwXml + ">");

            #region Process EMAILS
            _db.Entry(organisation).Collection(z => z.Organisation2Email).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (organisation.Organisation2Email.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var ea in organisation.Organisation2Email)
                {
                    if (!string.IsNullOrEmpty(ea.Email.EmailAddress))
                    {
                        newXml.Append(string.Format("<EmailAddress EmailAddress=\"{0}\" />", ea.Email.EmailAddress));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<EmailAddresses>" + newXml + "</EmailAddresses>");
            }
            #endregion

            #region Process WEBSITE
            _db.Entry(organisation).Collection(z => z.Organisation2WebSite).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (organisation.Organisation2WebSite.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var t in organisation.Organisation2WebSite)
                {
                    if (!string.IsNullOrEmpty(t.WebSite.URL))
                    {
                        newXml.Append(string.Format("<Website URL=\"{0}\" />", t.WebSite.URL));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Websites>" + newXml + "</Websites>");
            }
            #endregion

            #region Process TELEPHONE
            _db.Entry(organisation).Collection(z => z.Organisation2Telephone).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (organisation.Organisation2Telephone.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var t in organisation.Organisation2Telephone)
                {
                    if (!string.IsNullOrEmpty(t.Telephone.TelephoneNumber))
                    {
                        newXml.Append(string.Format("<TelephoneNumber TelephoneNumber=\"{0}\" />", t.Telephone.TelephoneNumber));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<TelephoneNumbers>" + newXml + "</TelephoneNumbers>");
            }
            #endregion

            #region Process BANK ACCOUNTS
            _db.Entry(organisation).Collection(z => z.Organisation2BankAccount).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (organisation.Organisation2BankAccount.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var ba in organisation.Organisation2BankAccount)
                {
                    if (!string.IsNullOrEmpty(ba.BankAccount.AccountNumber))
                    {
                        newXml.Append(string.Format("<BankAccount AccountNumber=\"{0}\" />", ba.BankAccount.AccountNumber));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<BankAccounts>" + newXml + "</BankAccounts>");
            }
            #endregion

            #region PAYMENT CARDS
            _db.Entry(organisation).Collection(z => z.Organisation2PaymentCard).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (organisation.Organisation2PaymentCard.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var pc in organisation.Organisation2PaymentCard)
                {
                    if (!string.IsNullOrEmpty(pc.PaymentCard.PaymentCardNumber))
                    {
                        newXml.Append(string.Format("<PaymentCard PaymentCardNumber=\"{0}\" />", pc.PaymentCard.PaymentCardNumber));
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<PaymentCards>" + newXml + "</PaymentCards>");
            }
            #endregion

            #region ADDRESSES
            _db.Entry(organisation).Collection(z => z.Organisation2Address).Query().Where(p => p.LinkConfidence == 0 && (p.ADARecordStatus == 0 || p.ADARecordStatus == 10)).Load();

            if (organisation.Organisation2Address.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var o2a in organisation.Organisation2Address)
                {
                    newXml.Append(FormatAddrInXml(o2a.Address));
                }

                if (newXml.Length > 0)
                    xml.Append("<Addresses>" + newXml + "</Addresses>");
            }
            #endregion

            xml.Append("</Organisation>");

            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("Match Org XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter pp = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var xx = _db.uspGetMatchingOrganisation(xmls, pp, true).ToList();
            _db.SaveChanges();

            if (xx.Any())
            {
                mr.MatchTypeFound = (MatchType)xx[0].MatchType;
                mr.MatchRuleNumber = xx[0].RuleNo;

                foreach (var row in xx)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }

        public MatchResults MatchSanctionsOrganisation(PipelineOrganisation organisation, int? existingBaseRiskClaimId)
        {

            MatchResults mr = CreateInitialSanctionsMatchingResults(organisation);

            StringBuilder xml = new StringBuilder();
            StringBuilder nwXml = new StringBuilder();

            if (!string.IsNullOrEmpty(organisation.OrganisationName))
            {
                {
                    nwXml.AppendFormat("OrganisationName=\"{0}\" ", organisation.OrganisationName);
                }
            }

            if (nwXml.Length == 0)
                return mr;

            xml.Append("<Organisation " + nwXml + ">");

            #region Process EMAIL ADDRESS
            if (organisation.EmailAddresses.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var ea in organisation.EmailAddresses)
                {
                    if (!string.IsNullOrEmpty(ea.EmailAddress))
                    {
                        {
                            newXml.Append(string.Format("<EmailAddress EmailAddress=\"{0}\" />", ea.EmailAddress));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<EmailAddresses>" + newXml + "</EmailAddresses>");
            }
            #endregion

            #region Process WEBSITES
            if (organisation.WebSites.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var t in organisation.WebSites)
                {
                    if (!string.IsNullOrEmpty(t.URL))
                    {
                        {
                            newXml.Append(string.Format("<Website URL=\"{0}\" />", t.URL));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Websites>" + newXml + "</Websites>");
            }
            #endregion

            #region Process TELEPHONES
            if (organisation.Telephones.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var t in organisation.Telephones)
                {
                    if (!string.IsNullOrEmpty(t.FullNumber))
                    {
                        {
                            newXml.Append(string.Format("<TelephoneNumber TelephoneNumber=\"{0}\" />", t.FullNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<TelephoneNumbers>" + newXml + "</TelephoneNumbers>");
            }
            #endregion

            #region Process ADDRESSES
            if (organisation.Addresses.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var a in organisation.Addresses)
                {
                    newXml.Append(FormatAddrInXml(a));
                }

                if (newXml.Length > 0)
                    xml.Append("<Addresses>" + newXml + "</Addresses>");
            }
            #endregion

            xml.Append("</Organisation>");


            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("Match Sanctions Org XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var xx = _db.uspGetMatchingSanctionsOrganisation(xmls, p).ToList();
            _db.SaveChanges();

            if (xx.Any())
            {
                mr.MatchTypeFound = (MatchType)xx[0].MatchType;
                mr.MatchRuleNumber = xx[0].RuleNo;

                foreach (var row in xx)
                    
                    mr.MatchSanctionsList.Add(row.ID);
            }

            return mr;
        }


        public MatchResults MatchOrganisation(PipelineOrganisation organisation, int? existingRiskClaimId)
        {
            MatchResults mr = CreateInitialMatchingResults(organisation);

            //List<string> warnings = new List<string>();

            //string orgNameWithoutExceptions = string.Empty;
            //orgNameWithoutExceptions = organisation.OrganisationName;
            //if (orgNameWithoutExceptions != null)
            //{
            //    orgNameWithoutExceptions = orgNameWithoutExceptions.ToUpper().Replace("LTD", "");
            //    orgNameWithoutExceptions = orgNameWithoutExceptions.ToUpper().Replace("PLC", "");
            //    orgNameWithoutExceptions = orgNameWithoutExceptions.ToUpper().Replace("LIMITED", "");

            //    orgNameWithoutExceptions = orgNameWithoutExceptions.TrimEnd();
            //}

            StringBuilder xml = new StringBuilder();
            StringBuilder nwXml = new StringBuilder();

            if (existingRiskClaimId != null)
                nwXml.AppendFormat("RiskClaim_Id=\"{0}\" ", existingRiskClaimId);

            if (!string.IsNullOrEmpty(organisation.OrganisationName))
            {
                //if ((from x in _db.Organisations
                //     where x.OrganisationName.ToUpper().Replace("LTD", "").Replace("PLC", "").Replace("LIMITED", "").TrimEnd() == orgNameWithoutExceptions
                //     select x).Any())
                //{
                    nwXml.AppendFormat("OrganisationName=\"{0}\" ", organisation.OrganisationName);
                //}
            }

            if (!string.IsNullOrEmpty(organisation.RegisteredNumber))
            {
                if ((from x in _db.Organisations
                     where x.RegisteredNumber == organisation.RegisteredNumber
                     select x).Any())
                {
                    nwXml.AppendFormat("RegisteredNumber=\"{0}\" ", organisation.RegisteredNumber);
                }
            }

            if (!string.IsNullOrEmpty(organisation.VatNumber))
            {
                if ((from x in _db.Organisations
                     where x.VATNumber == organisation.VatNumber
                     select x).Any())
                {
                    nwXml.AppendFormat("VATNumber=\"{0}\" ", organisation.VatNumber);
                }
            }

            if (nwXml.Length == 0)
                return mr;

            xml.Append("<Organisation " + nwXml + ">");

            #region Process EMAIL ADDRESS
            if (organisation.EmailAddresses.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var ea in organisation.EmailAddresses)
                {
                    if (!string.IsNullOrEmpty(ea.EmailAddress))
                    {
                        if ((from x in _db.Emails
                             where x.EmailAddress == ea.EmailAddress
                             select x).Any())
                        {
                            newXml.Append(string.Format("<EmailAddress EmailAddress=\"{0}\" />", ea.EmailAddress));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<EmailAddresses>" + newXml + "</EmailAddresses>");
            }
            #endregion

            #region Process WEBSITES
            if (organisation.WebSites.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var t in organisation.WebSites)
                {
                    if (!string.IsNullOrEmpty(t.URL))
                    {
                        if ((from x in _db.WebSites
                             where x.URL == t.URL
                             select x).Any())
                        {
                            newXml.Append(string.Format("<Website URL=\"{0}\" />", t.URL));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<Websites>" + newXml + "</Websites>");
            }
            #endregion

            #region Process TELEPHONES
            if (organisation.Telephones.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var t in organisation.Telephones)
                {
                    if (!string.IsNullOrEmpty(t.FullNumber))
                    {
                        if ((from tt in _db.Telephones
                             where tt.TelephoneNumber == t.FullNumber
                             select tt).Any())
                        {
                            newXml.Append(string.Format("<TelephoneNumber TelephoneNumber=\"{0}\" />", t.FullNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<TelephoneNumbers>" + newXml + "</TelephoneNumbers>");
            }
            #endregion

            #region Process BANK ACCOUNT
            if (organisation.BankAccounts.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var ba in organisation.BankAccounts)
                {
                    if (!string.IsNullOrEmpty(ba.AccountNumber))
                    {
                        if ((from x in _db.BankAccounts
                             where x.AccountNumber == ba.AccountNumber
                             select x).Any())
                        {
                            newXml.Append(string.Format("<BankAccount AccountNumber=\"{0}\" />", ba.AccountNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<BankAccounts>" + newXml + "</BankAccounts>");
            }
            #endregion

            #region Process PAYMENT CARDS
            if (organisation.PaymentCards.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var pc in organisation.PaymentCards)
                {
                    if (!string.IsNullOrEmpty(pc.PaymentCardNumber))
                    {
                        if ((from x in _db.PaymentCards
                             where x.PaymentCardNumber == pc.PaymentCardNumber
                             select x).Any())
                        {
                            newXml.Append(string.Format("<PaymentCard PaymentCardNumber=\"{0}\" />", pc.PaymentCardNumber));
                        }
                    }
                }

                if (newXml.Length > 0)
                    xml.Append("<PaymentCards>" + newXml + "</PaymentCards>");
            }
            #endregion

            #region Process ADDRESSES
            if (organisation.Addresses.Any())
            {
                StringBuilder newXml = new StringBuilder();

                foreach (var a in organisation.Addresses)
                {
                    newXml.Append(FormatAddrInXml(a));
                }

                if (newXml.Length > 0)
                    xml.Append("<Addresses>" + newXml + "</Addresses>");
            }
            #endregion

            xml.Append("</Organisation>");

            string xmls = CleanseXml(xml.ToString());

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("Match Org XML: " + xmls);
            }

            System.Data.Entity.Core.Objects.ObjectParameter p = new System.Data.Entity.Core.Objects.ObjectParameter("fieldsFlag", typeof(bool));

            var xx = _db.uspGetMatchingOrganisation(xmls, p, true).ToList();
            _db.SaveChanges();

            if (xx.Any())
            {
                mr.MatchTypeFound = (MatchType)xx[0].MatchType;
                mr.MatchRuleNumber = xx[0].RuleNo;

                foreach (var row in xx)
                    mr.MatchIdList.Add((int)row.ID);
            }

            return mr;
        }








        

     
    }
}
