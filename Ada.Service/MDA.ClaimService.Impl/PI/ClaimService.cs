﻿using MDA.Common.Server;
using MDA.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using MDA.ClaimService.Interface;
using MDA.Common.Enum;
using MDA.Common.Debug;
using System.Diagnostics;
using MDA.MatchingService.Interface;
using MDA.CleansingService.Interface;
using MDA.DataService;
using MDA.DataService.PropertyMethods;
using MDA.Common.Helpers;
using MDA.Common;
using MDA.VerificationService.Interface;
using System.Configuration;
using RiskEngine.Scoring.Entities;
using RiskEngine.Model;
using RiskEngine.Interfaces;
using RiskEngine.Scoring.Model;
using MDA.RiskService.Interface;
using MDA.Pipeline.Model;

namespace MDA.ClaimService.PI
{
    public partial class ClaimService : ClaimServiceBase, IClaimService
    {
        /// <summary>
        /// Constructor for all pipelines except SCORING.  Needs to have interfaces to all supporting services
        /// </summary>
        /// <param name="ctx">Current Context</param>
        /// <param name="cleansingService">Ref to Cleansing Service</param>
        /// <param name="matchingService">Ref to Matching Service</param>
        /// <param name="validationService">Ref to Validation Service</param>
        /// <param name="riskService">Ref to Risk Service</param>
        /// <param name="riskEngineService">Ref to Risk Engine Service</param>
        public ClaimService(CurrentContext ctx, ICleansingService cleansingService, IMatchingService matchingService, 
                        IValidationService validationService, IRiskServices riskService, IRiskEngine riskEngineService) :
            base(ctx, cleansingService, matchingService, validationService, riskService, riskEngineService)
        {
        }

        /// <summary>
        /// Constructor that can be used for SCORING
        /// </summary>
        /// <param name="ctx">Current Context</param>
        public ClaimService(CurrentContext ctx) : base(ctx)
        {
        }

        #region Process Claim for S3-LOADING

        /// <summary>
        /// This method is called from S3-LOADING and loads (Matches and inserts entities and links) the claim provided
        /// </summary>
        /// <param name="pipeClaim">The PipelienClaim to be loaded</param>
        /// <param name="newRiskClaim">The RiskClaim record that belongs to this new claim</param>
        /// <returns>The DAL Incident record inserted</returns>
        public Incident LoadClaimIntoDatabase(MDA.Pipeline.Model.IPipelineClaim pipeClaim, RiskClaim newRiskClaim)
        {
            return _LoadClaimIntoDatabase((MDA.Pipeline.Model.PipelinePIClaim)pipeClaim, newRiskClaim);
        }
        
        private Incident _LoadClaimIntoDatabase(MDA.Pipeline.Model.PipelinePIClaim pipeClaim, RiskClaim newRiskClaim)
        {
            #region Assert
            DebugCheck.NotNull<CurrentContext>(_ctx);
            DebugCheck.NotNull<MDA.Pipeline.Model.PipelinePIClaim>(pipeClaim);
            DebugCheck.NotNull<RiskClaim>(newRiskClaim);
            #endregion

            int? existingBaseRiskClaimId = newRiskClaim.BaseRiskClaim_Id; // (newRiskClaim.BaseRiskClaim_Id != newRiskClaim.Id) ? newRiskClaim.BaseRiskClaim_Id : (int?)null;
            int? existingIncidentId = (newRiskClaim.Incident_Id == 0) ? (int?)null : newRiskClaim.Incident_Id;

            #region Assert
            Debug.Assert(existingBaseRiskClaimId == null || existingBaseRiskClaimId > 0);
            Debug.Assert(existingIncidentId == null || existingIncidentId > 0);
            #endregion

            #region Trace
            if (_trace)
                ADATrace.WriteLine(string.Format("ProcessClaim. NewRiskClaimId[{0}]. ExistingBaseRiskClaimId[{1}]. ExistingIncidentId[{2}]", newRiskClaim.Id,
                        (existingBaseRiskClaimId == null) ? "null" : existingBaseRiskClaimId.Value.ToString(),
                        (existingIncidentId == null) ? "null" : existingIncidentId.Value.ToString()));
            #endregion

            #region Create INCIDENT record
            Incident incident = ProcessClaimMatch(_ctx, (Operation)pipeClaim.Operation, pipeClaim, newRiskClaim, existingIncidentId, existingBaseRiskClaimId);
            #endregion

            #region Trace
            if (_trace)
                ADATrace.WriteLine(string.Format("ProcessClaim. This Incident Id[{0}]", incident.Id.ToString()));
            #endregion

            int incidentId = incident.Id;

            MDA.Pipeline.Model.PipelinePolicy pipePolicy = pipeClaim.Policy;


            #region Create POLICY record
            Policy policy = ProcessPolicyMatch(_ctx, (Operation)pipeClaim.Policy.Operation, pipePolicy, incidentId, newRiskClaim, existingBaseRiskClaimId);
            #endregion

            #region Add Incident2Policy Links
            if (pipePolicy.I2Po_LinkData.LinkConfidence == -1)
                pipePolicy.I2Po_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

            ProcessIncident2PolicyLink(_ctx, incident.Id, policy.Id, pipePolicy.I2Po_LinkData, (Operation)pipePolicy.Operation, newRiskClaim);
            #endregion

            #region Process Person
            var p = pipeClaim.Person;

            var nullVehicle = new PipelineVehicle();
            nullVehicle = null;

            // we don't pass OP into this one as it only link people to people, not other entities
            Person person = ProcessPersonMatch(_ctx, (Operation)p.Operation, p, nullVehicle, pipeClaim, incidentId, newRiskClaim, existingBaseRiskClaimId);

            if (person == null) return incident;

            #region Add Incident2Person link (EVEN if PaidPolicy)
            if (GetOperation(null, (Operation)p.Operation) != Operation.Normal || LinkHelper.CreateIncident2PersonLink(pipeClaim, p))
            {
                MDA.Common.Enum.Operation op = GetOperation(null, (Operation)p.Operation);

                if (p.I2Pe_LinkData.LinkConfidence == -1)
                    p.I2Pe_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                var l = ProcessIncident2PersonLink(_ctx, incident.Id, person.Id, p.I2Pe_LinkData, /*xmlClaim, p, xmlClaim.MotorClaimInfo,*/ op, newRiskClaim);
            }
            #endregion

            #region Add Person2Policy link (only if polholder, paidpol, insured driver)
            if (policy != null && (GetOperation((Operation)p.Operation, (Operation)pipeClaim.Policy.Operation) != Operation.Normal || LinkHelper.CreatePerson2PolicyLink(pipeClaim.Policy, p)))
            {
                MDA.Common.Enum.PolicyLinkType linkType = LinkHelper.GetPerson2PolicyLinkType((MDA.Common.Enum.PartyType)p.I2Pe_LinkData.PartyType_Id, (MDA.Common.Enum.SubPartyType)p.I2Pe_LinkData.SubPartyType_Id, (MDA.Common.Enum.PolicyLinkType)p.Pe2Po_LinkData.PolicyLinkType_Id);

                MDA.Common.Enum.Operation op = GetOperation((Operation)p.Operation, (Operation)pipeClaim.Policy.Operation);

                if (p.Pe2Po_LinkData.LinkConfidence == -1)
                {
                    p.Pe2Po_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;
                    p.Pe2Po_LinkData.PolicyLinkType_Id = (int)linkType;
                }
                var l = ProcessPerson2PolicyLink(_ctx, person.Id, policy.Id, p.Pe2Po_LinkData, op, newRiskClaim);
            }
            #endregion

            #region Match each Address record - Create Person2Address links for all addresses

            foreach (MDA.Pipeline.Model.PipelineAddress a in p.Addresses)
            {
                a.AddressType_Id = (int)MDA.Common.Enum.AddressType.Residential;
                var addr = ProcessPersonAddressMatch(_ctx, (Operation)a.Operation, person.Id, a, incidentId, newRiskClaim, existingBaseRiskClaimId);

                // Process the link between this person and this address
                if (addr != null)
                {
                    if (a.PO2A_LinkData.LinkConfidence == -1)
                        a.PO2A_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                    //var l = ProcessPerson2AddressLink(ctx, person.Id, (int)addr.Id, a.PO2A_LinkData, (MDA.Common.Enum.AddressLinkType)a.AddressLinkType_Id, a.StartOfResidency, a.EndOfResidency, (Operation)a.Operation, newRiskClaim);
                    ProcessPerson2AddressLink(_ctx, person.Id, (int)addr.Id, a.PO2A_LinkData, (Operation)a.Operation, newRiskClaim);
                }
            }
            #endregion

            #region Match EMAIL address. Create Person2Email link
            if (p.EmailAddresses != null && p.EmailAddresses.Count > 0)
            {
                foreach (var ea in p.EmailAddresses)
                {
                    var e = ProcessPersonEmailAddressMatch(_ctx, (Operation)p.Operation, person.Id, ea, incidentId, newRiskClaim, existingBaseRiskClaimId);

                    if (e != null)
                    {
                        if (ea.PO2E_LinkData.LinkConfidence == -1)
                            ea.PO2E_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                        var link = ProcessPerson2EmailLink(_ctx, person.Id, e.Id, ea.PO2E_LinkData, (Operation)p.Operation, newRiskClaim);
                    }
                }
            }
            #endregion

            #region Match BANK ACCOUNT. Create Person2BankAccount link
            if (p.BankAccounts != null && p.BankAccounts.Count > 0)
            {
                foreach (var ba in p.BankAccounts)
                {
                    if (!string.IsNullOrWhiteSpace(ba.AccountNumber) && !string.IsNullOrWhiteSpace(ba.SortCode))
                    {
                        DAL.BankAccount bankAccount = ProcessPersonBankAccountMatch(_ctx, (Operation)ba.Operation, person.Id, ba, pipeClaim, incidentId, newRiskClaim, existingBaseRiskClaimId);

                        if (bankAccount != null)
                        {
                            if (ba.PO2Ba_LinkData.LinkConfidence == -1)
                                ba.PO2Ba_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            // Link this Person with Bank Account - confirmed
                            var link = ProcessPerson2BankAccountLink(_ctx, person.Id, bankAccount.Id, ba.PO2Ba_LinkData, (Operation)ba.Operation, newRiskClaim);

                            // link with policy if paidpol or polholder
                            if (GetOperation((Operation)pipeClaim.Policy.Operation, (Operation)ba.Operation) != Operation.Normal || LinkHelper.CreatePolicy2BankAccountLink(pipeClaim.Policy, p, ba))
                            {
                                MDA.Common.Enum.Operation op = GetOperation((Operation)pipeClaim.Policy.Operation, (Operation)ba.Operation);

                                if (ba.Po2Ba_LinkData.LinkConfidence == -1)
                                    ba.Po2Ba_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                var l = ProcessPolicy2BankAccountLink(_ctx, policy.Id, bankAccount.Id, ba.Po2Ba_LinkData, op, newRiskClaim);
                            }
                        }
                    }
                }
            }
            #endregion

            #region Match PAYMENT CARD. Create Person2PaymentCard link
            if (p.PaymentCards != null && p.PaymentCards.Count > 0)
            {
                foreach (var pc in p.PaymentCards)
                {
                    if (!string.IsNullOrWhiteSpace(pc.PaymentCardNumber))
                    {
                        PaymentCard paymentCard = ProcessPersonPaymentCardMatch(_ctx, (Operation)pc.Operation, person.Id, pc, incidentId, newRiskClaim, existingBaseRiskClaimId);

                        if (paymentCard != null)
                        {
                            if (pc.PO2Pc_LinkData.LinkConfidence == -1)
                                pc.PO2Pc_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            // Link this Person with Payment Card - confirmed
                            var link = ProcessPerson2PaymentCardLink(_ctx, person.Id, paymentCard.Id, pc.PO2Pc_LinkData, (Operation)pc.Operation, newRiskClaim);

                            // link with policy if paidpol or polholder
                            if (GetOperation((Operation)pipeClaim.Policy.Operation, (Operation)pc.Operation) != Operation.Normal || LinkHelper.CreatePolicy2PaymentCardLink(pipeClaim.Policy, p, pc))
                            {
                                MDA.Common.Enum.Operation op = GetOperation((Operation)pipeClaim.Policy.Operation, (Operation)pc.Operation);

                                if (pc.Po2Pc_LinkData.LinkConfidence == -1)
                                    pc.Po2Pc_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                var l = ProcessPolicy2PaymentCardLink(_ctx, policy.Id, paymentCard.Id, pc.Po2Pc_LinkData, /*pc,*/ op, newRiskClaim);
                            }
                        }
                    }
                }
            }

            #endregion

            #region Match PASSPORT. Create Person2Passport link
            if (p.PassportNumbers != null && p.PassportNumbers.Count > 0)
            {
                foreach (var pn in p.PassportNumbers)
                {
                    if (!string.IsNullOrWhiteSpace(pn.PassportNumber))
                    {
                        var pa = ProcessPassportMatch(_ctx, (Operation)p.Operation, person.Id, pn, incidentId, newRiskClaim, existingBaseRiskClaimId);

                        if (pa != null)
                        {
                            if (pn.Pe2Pa_LinkData.LinkConfidence == -1)
                                pn.Pe2Pa_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            // Link this Person with any partial Passport matches we got returned
                            var link = ProcessPerson2PassportLink(_ctx, person.Id, pa.Id, pn.Pe2Pa_LinkData, (Operation)p.Operation, newRiskClaim);
                        }
                    }
                }
            }

            #endregion

            #region Match NINumber. Create Person2NINumber link
            if (p.NINumbers != null && p.NINumbers.Count > 0)
            {
                foreach (var nin in p.NINumbers)
                {
                    if (!string.IsNullOrWhiteSpace(nin.NINumber1))
                    {
                        var ni = ProcessNINumberMatch(_ctx, (Operation)p.Operation, person.Id, nin, incidentId, newRiskClaim, existingBaseRiskClaimId);

                        if (ni != null)
                        {
                            if (nin.Pe2Ni_LinkData.LinkConfidence == -1)
                                nin.Pe2Ni_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            var link = ProcessPerson2NINumberLink(_ctx, person.Id, (int)ni.Id, nin.Pe2Ni_LinkData, (Operation)p.Operation, newRiskClaim);
                        }
                    }
                }
            }
            #endregion

            #region Match Driving License. Create License link
            if (p.DrivingLicenseNumbers != null && p.DrivingLicenseNumbers.Count > 0)
            {
                foreach (var dl in p.DrivingLicenseNumbers)
                {
                    if (!string.IsNullOrWhiteSpace(dl.DriverNumber))
                    {
                        var license = ProcessLicenseMatch(_ctx, (Operation)p.Operation, person.Id, dl, incidentId, newRiskClaim, existingBaseRiskClaimId);

                        if (license != null)
                        {
                            if (dl.Pe2Dl_LinkData.LinkConfidence == -1)
                                dl.Pe2Dl_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            var link = ProcessPerson2DrivingLicenseLink(_ctx, person.Id, license.Id, dl.Pe2Dl_LinkData, (Operation)p.Operation, newRiskClaim);
                        }
                    }
                }
            }

            #endregion

            #region Match WebSite. Create Person2WebSite link
            #endregion

            #region Match Telephone Numbers - Create Person2Telephone links for all numbers
            if (p.Telephones != null && p.Telephones.Count > 0)
            {
                foreach (var tp in p.Telephones)
                {
                    if (!string.IsNullOrEmpty(tp.FullNumber))
                    {
                        var t = ProcessPersonTelephoneNumberMatch(_ctx, (Operation)p.Operation, person.Id, tp, incidentId, newRiskClaim, existingBaseRiskClaimId);

                        if (t != null)
                        {
                            if (tp.PO2T_LinkData.LinkConfidence == -1)
                                tp.PO2T_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            var link = ProcessPerson2TelephoneLink(_ctx, person.Id, t.Id, tp.PO2T_LinkData, (Operation)p.Operation, newRiskClaim);
                        }
                    }
                }
            }

            #endregion

            #region Process each Organisation record
            foreach (MDA.Pipeline.Model.PipelineOrganisation o in p.Organisations)
            {
                Organisation org = ProcessOrganisationMatch(_ctx, (Operation)o.Operation, o, pipeClaim, incidentId, newRiskClaim, existingBaseRiskClaimId);

                if (org == null) continue;

                #region Add person2organisation confirmed link
                if (o.Operation != (int)Operation.Normal || LinkHelper.CreatePerson2OrganisationLink(p, o))
                {
                    if (o.P2O_LinkData.LinkConfidence == -1)
                        o.P2O_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                    var l = ProcessPerson2OrganisationLink(_ctx, person.Id, org.Id, o.P2O_LinkData, (Operation)o.Operation, newRiskClaim);
                }
                #endregion

                #region Match each Organisation Address - Create Organisation2Address links
                foreach (MDA.Pipeline.Model.PipelineAddress aa in o.Addresses)
                {
                    aa.AddressType_Id = (int)MDA.Common.Enum.AddressType.Commercial;
                    Address address = ProcessOrgAddressMatch(_ctx, (Operation)aa.Operation, org.Id, aa, o.OrganisationName, incidentId, newRiskClaim, existingBaseRiskClaimId);

                    if (address != null)
                    {
                        if (aa.PO2A_LinkData.LinkConfidence == -1)
                            aa.PO2A_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                        var link = ProcessOrganisation2AddressLink(_ctx, org.Id, address.Id, aa.PO2A_LinkData,
                            (Operation)aa.Operation, newRiskClaim);
                    }

                    //#region Add Address2Vehicle (primary vehicle) if OrgType = storage and address is storage address
                    //if (GetOperation((Operation)v.Operation, (Operation)aa.Operation) != Operation.Normal || LinkHelper.CreatePrimaryVehicle2OrgAddressLink(v, o, aa))
                    //{
                    //    MDA.Common.Enum.Operation op = GetOperation((Operation)v.Operation, (Operation)aa.Operation);

                    //    if (aa.V2A_LinkData.LinkConfidence == -1)
                    //    {
                    //        aa.V2A_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;
                    //        aa.V2A_LinkData.AddressLinkType_Id = (int)Common.Enum.AddressLinkType.StorageAddress;
                    //    }

                    //    var l = ProcessVehicle2AddressLink(_ctx, vehicle.Id, address.Id, aa.V2A_LinkData, op, newRiskClaim);
                    //}
                    //#endregion
                }
                #endregion

                #region Match EMAIL address. Create Organisation2Email link
                if (o.EmailAddresses != null)
                {
                    foreach (var ea in o.EmailAddresses)
                    {
                        if (!string.IsNullOrEmpty(ea.EmailAddress))
                        {
                            var email = ProcessOrgEmailAddressMatch(_ctx, (Operation)o.Operation, org.Id, ea, incidentId, newRiskClaim, existingBaseRiskClaimId);

                            if (email != null)
                            {
                                if (ea.PO2E_LinkData.LinkConfidence == -1)
                                    ea.PO2E_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                var link = ProcessOrganisation2EmailLink(_ctx, org.Id, email.Id, ea.PO2E_LinkData, (Operation)o.Operation, newRiskClaim);
                            }
                        }
                    }
                }
                #endregion

                #region Match WEBSITE address. Create Organisation2Website link
                if (o.WebSites != null)
                {
                    foreach (var ws in o.WebSites)
                    {
                        if (!string.IsNullOrEmpty(ws.URL))
                        {
                            var webSite = ProcessOrgWebsiteAddressMatch(_ctx, (Operation)o.Operation, org.Id, ws, incidentId, newRiskClaim, existingBaseRiskClaimId);

                            if (webSite != null)
                            {
                                if (ws.O2Ws_LinkData.LinkConfidence == -1)
                                    ws.O2Ws_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                var link = ProcessOrganisation2WebsiteLink(_ctx, org.Id, webSite.Id, ws.O2Ws_LinkData, (Operation)o.Operation, newRiskClaim);
                            }
                        }
                    }
                }
                #endregion

                #region Match Telephone Numbers - Create Organisation2Telephone links for all numbers
                if (o.Telephones != null && o.Telephones.Count > 0)
                {
                    foreach (var tp in o.Telephones)
                    {
                        if (!string.IsNullOrEmpty(tp.FullNumber))
                        {
                            var tt = ProcessOrgTelephoneNumberMatch(_ctx, (Operation)o.Operation, org.Id, tp, /*Common.Enum.TelephoneType.Unknown, Common.Enum.TelephoneLinkType.Uses,*/ incidentId, newRiskClaim, existingBaseRiskClaimId);

                            if (tt != null)
                            {
                                if (tp.PO2T_LinkData.LinkConfidence == -1)
                                    tp.PO2T_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                var link = ProcessOrganisation2TelephoneLink(_ctx, org.Id, tt.Id, tp.PO2T_LinkData, (Operation)o.Operation, newRiskClaim);
                            }
                        }
                    }
                }

                #endregion

                #region Add Incident2Organisation confirmed link
                if (o.Operation != (int)Operation.Normal || LinkHelper.CreateIncident2OrganisationLink(pipeClaim, o, p))
                {
                    if (o.I2O_LinkData.LinkConfidence == -1)
                        o.I2O_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                    var l = ProcessIncident2OrganisationLink(_ctx, incident.Id, org.Id, o.I2O_LinkData, false, /*xmlClaim, o, xmlClaim.MotorClaimInfo,*/ (Operation)o.Operation, newRiskClaim);
                }
                #endregion

                //#region Add Organisation2Vehicle (primary vehicle) if OrgType = recovery, storage, engineer, repairer
                //if (GetOperation((Operation)v.Operation, (Operation)o.Operation) != Operation.Normal || LinkHelper.CreatePrimaryVehicle2OrganisationLink(v, o))
                //{
                //    MDA.Common.Enum.Operation op = GetOperation((Operation)v.Operation, (Operation)o.Operation);

                //    if (o.pV2O_LinkData.LinkConfidence == -1)
                //        o.pV2O_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                //    //o.pV2O_LinkData.VehicleLinkType_Id = (int)linkType;

                //    var l = ProcessOrgVehicle2OrganisationLink(_ctx, vehicle.Id, org.Id, o.pV2O_LinkData, /*linkType, null, null, null,*/ op, newRiskClaim);
                //}
                //#endregion

                #region Process each Vehicle Record
                foreach (MDA.Pipeline.Model.PipelineOrganisationVehicle ov in o.Vehicles)
                {
                    Vehicle orgVehicle = ProcessOrgVehicleMatch(_ctx, (Operation)ov.Operation, org.Id, org.OrganisationName, ov, incidentId, newRiskClaim, existingBaseRiskClaimId);

                    if (orgVehicle == null) continue;

                    if (ov.V2O_LinkData.LinkConfidence == -1)
                        ov.V2O_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                    var link = ProcessOrgVehicle2OrganisationLink(_ctx, orgVehicle.Id, org.Id, ov.V2O_LinkData, (Operation)ov.Operation,
                                                        newRiskClaim);

                    if (ov.V2Pe_LinkData != null)
                    {
                        ProcessVehicle2PersonLink(_ctx, orgVehicle.Id, person.Id, ov.V2Pe_LinkData, (Operation)ov.Operation, newRiskClaim);
                    }
                }
                #endregion  Process each Vehicle Record
            }
            #endregion Organisation

                #endregion Person

            #region Process each Claim Level Organisation record
            foreach (MDA.Pipeline.Model.PipelineOrganisation o in pipeClaim.Organisations)
            {
                //if (pipeClaim.Operation != (int)Operation.Normal && o.Operation == (int)Operation.Normal)
                //    o.Operation = pipeClaim.Operation;

                Organisation org = ProcessOrganisationMatch(_ctx, (Operation)o.Operation, o, pipeClaim, incidentId, newRiskClaim, existingBaseRiskClaimId);

                if (org == null) continue;

                #region Create Incident2Organisation confirmed link (always)
                if (o.Operation != (int)Operation.Normal || LinkHelper.CreateIncident2OrganisationLink(pipeClaim, o))
                {
                    if (o.I2O_LinkData.LinkConfidence == -1)
                        o.I2O_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                    var l = ProcessIncident2OrganisationLink(_ctx, incident.Id, org.Id, o.I2O_LinkData, true, /*xmlClaim, o, xmlClaim.MotorClaimInfo,*/ (Operation)o.Operation, newRiskClaim);
                }
                #endregion

                #region Match each Organisation Address - Create Organisation2Address links
                foreach (MDA.Pipeline.Model.PipelineAddress aa in o.Addresses)
                {
                    aa.AddressType_Id = (int)MDA.Common.Enum.AddressType.Commercial;
                    Address address = ProcessOrgAddressMatch(_ctx, (Operation)aa.Operation, org.Id, aa, o.OrganisationName, incidentId, newRiskClaim, existingBaseRiskClaimId);

                    if (address != null)
                    {
                        if (aa.PO2A_LinkData.LinkConfidence == -1)
                            aa.PO2A_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                        var link = ProcessOrganisation2AddressLink(_ctx, org.Id, address.Id, aa.PO2A_LinkData,
                            (Operation)aa.Operation, newRiskClaim);
                    }

                    //#region Add Address2Vehicle (primary vehicle) if OrgType = storage and address is storage address
                    //if (GetOperation(v.Operation, aa.Operation) != Operation.Normal || LinkHelper.CreatePrimaryVehicle2OrgAddressLink(v, o, aa))
                    //{
                    //    MDA.Common.Enum.Operation op = GetOperation(v.Operation, aa.Operation);

                    //    var l = ProcessVehicle2AddressLink(ctx, vehicle.Id, address.Id, Common.Enum.AddressLinkType.StorageAddress, op, LinkConfidence.Confirmed, newRiskClaim);

                    //    auditVehicle.Vehicle2AddressLink = l;
                    //}
                    //#endregion
                }
                #endregion

                #region Create Organisation2Policy link - only orgType is policyHolder
                if (GetOperation((Operation)o.Operation, (Operation)pipeClaim.Policy.Operation) != Operation.Normal || LinkHelper.CreateOrganisation2PolicyLink(o, pipeClaim.Policy))
                {
                    MDA.Common.Enum.Operation op = GetOperation((Operation)o.Operation, (Operation)pipeClaim.Policy.Operation);

                    if (o.O2Po_LinkData.LinkConfidence == -1)
                        o.O2Po_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                    //o.O2Po_LinkData.PolicyLinkType_Id = (int)Common.Enum.PolicyLinkType.Policyholder;

                    var l = ProcessOrganisation2PolicyLink(_ctx, org.Id, policy.Id, o.O2Po_LinkData, /*Common.Enum.PolicyLinkType.Policyholder,*/ op, newRiskClaim);
                }
                #endregion

                #region Match EMAIL address. Create Org2Email link
                if (o.EmailAddresses != null)
                {
                    foreach (var ea in o.EmailAddresses)
                    {
                        if (!string.IsNullOrEmpty(ea.EmailAddress))
                        {
                            var em = ProcessOrgEmailAddressMatch(_ctx, (Operation)o.Operation, org.Id, ea, incidentId, newRiskClaim, existingBaseRiskClaimId);

                            if (em != null)
                            {
                                if (ea.PO2E_LinkData.LinkConfidence == -1)
                                    ea.PO2E_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                var link = ProcessOrganisation2EmailLink(_ctx, org.Id, (int)em.Id, ea.PO2E_LinkData, (Operation)o.Operation, newRiskClaim);
                            }
                        }
                    }
                }

                #endregion

                #region Match Telephone Numbers - Create Organisation2Telephone links for all numbers
                if (o.Telephones != null && o.Telephones.Count > 0)
                {
                    foreach (var tp in o.Telephones)
                    {
                        if (!string.IsNullOrEmpty(tp.FullNumber))
                        {
                            var tt = ProcessOrgTelephoneNumberMatch(_ctx, (Operation)o.Operation, org.Id, tp, /*Common.Enum.TelephoneType.Unknown, Common.Enum.TelephoneLinkType.Uses,*/ incidentId, newRiskClaim, existingBaseRiskClaimId);

                            if (tt != null)
                            {
                                if (tp.PO2T_LinkData.LinkConfidence == -1)
                                    tp.PO2T_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                var link = ProcessOrganisation2TelephoneLink(_ctx, org.Id, tt.Id, tp.PO2T_LinkData, /*Common.Enum.TelephoneLinkType.Uses,*/ (Operation)o.Operation, newRiskClaim);
                            }
                        }
                    }
                }

                #endregion


                #region Match Organisation Bank Account record - Create Org2Account link
                if (o.BankAccounts != null && o.BankAccounts.Count > 0)
                {
                    foreach (var ba in o.BankAccounts)
                    {
                        if (!string.IsNullOrWhiteSpace(ba.AccountNumber) &&
                                                 !string.IsNullOrWhiteSpace(ba.SortCode))
                        {
                            DAL.BankAccount bankAccount = ProcessOrgBankAccountMatch(_ctx, (Operation)ba.Operation, org.Id, ba, pipeClaim, incidentId, newRiskClaim, existingBaseRiskClaimId);

                            if (bankAccount != null)
                            {
                                if (ba.PO2Ba_LinkData.LinkConfidence == -1)
                                    ba.PO2Ba_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                var link = ProcessOrganisation2BankAccountLink(_ctx, org.Id, bankAccount.Id, ba.PO2Ba_LinkData, (Operation)ba.Operation, newRiskClaim);
                            }

                            if (GetOperation((Operation)pipeClaim.Policy.Operation, (Operation)ba.Operation) != Operation.Normal || LinkHelper.CreatePolicy2BankAccountLink(pipeClaim.Policy, o, ba))
                            {
                                MDA.Common.Enum.Operation op = GetOperation((Operation)pipeClaim.Policy.Operation, (Operation)ba.Operation);

                                if (ba.Po2Ba_LinkData.LinkConfidence == -1)
                                    ba.Po2Ba_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                var l = ProcessPolicy2BankAccountLink(_ctx, policy.Id, bankAccount.Id, ba.Po2Ba_LinkData, op, newRiskClaim);
                            }
                        }
                    }
                }
                #endregion

                #region Match Organisation Payment Card record - Create Org2PaymentCard link
                if (o.PaymentCards != null && o.PaymentCards.Count > 0)
                {
                    foreach (var pc in o.PaymentCards)
                    {
                        if (!string.IsNullOrWhiteSpace(pc.PaymentCardNumber))
                        {
                            PaymentCard paymentCard = ProcessOrgPaymentCardMatch(_ctx, (Operation)pc.Operation, org.Id, pc, incidentId, newRiskClaim, existingBaseRiskClaimId);

                            if (paymentCard != null)
                            {
                                if (pc.PO2Pc_LinkData.LinkConfidence == -1)
                                    pc.PO2Pc_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                var link = ProcessOrganisation2PaymentCardLink(_ctx, org.Id, paymentCard.Id, pc.PO2Pc_LinkData, (Operation)pc.Operation, newRiskClaim);
                            }

                            if (GetOperation((Operation)pipeClaim.Policy.Operation, (Operation)pc.Operation) != Operation.Normal || LinkHelper.CreatePolicy2PaymentCardLink(pipeClaim.Policy, o, pc))
                            {
                                MDA.Common.Enum.Operation op = GetOperation((Operation)pipeClaim.Policy.Operation, (Operation)pc.Operation);

                                if (pc.Po2Pc_LinkData.LinkConfidence == -1)
                                    pc.Po2Pc_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                var l = ProcessPolicy2PaymentCardLink(_ctx, policy.Id, paymentCard.Id, pc.Po2Pc_LinkData, /*pc,*/ op, newRiskClaim);
                            }
                        }
                    }
                }

                #endregion

                #region Process each Vehicle Record
                foreach (MDA.Pipeline.Model.PipelineOrganisationVehicle ov in o.Vehicles)
                {
                    Vehicle orgVehicle = ProcessOrgVehicleMatch(_ctx, (Operation)ov.Operation, org.Id, org.OrganisationName, ov, incidentId, newRiskClaim, existingBaseRiskClaimId);

                    if (orgVehicle != null)
                    {
                        if (ov.V2O_LinkData.LinkConfidence == -1)
                            ov.V2O_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                        //ov.V2O_LinkData.VehicleLinkType_Id = (int)MDA.Common.Enum.VehicleLinkType.HiredFrom;
                        //ov.V2O_LinkData.HireCompany = org.OrganisationName;

                        var link = ProcessOrgVehicle2OrganisationLink(_ctx, orgVehicle.Id, org.Id, ov.V2O_LinkData, /*MDA.Common.Enum.VehicleLinkType.HiredFrom,
                                        ov.V2O_LinkData.HireStartDate, ov.V2O_LinkData.HireEndDate, org.OrganisationName,*/ (Operation)ov.Operation,
                                        newRiskClaim);
                    }
                }
                #endregion  Process each Vehicle Record

            }
            #endregion

            #region Clean up Links from previous versions of this claim

            CleanUpLinksAfterLoading(newRiskClaim);

            #endregion

            return incident;
        }
        #endregion

        #region Fetch claim for S5-Scoring

        /// <summary>
        /// This method is calledby ScoreClaim in the base class. You override this method to extract the RiskEntity map that will be recursively scored.
        /// This method should return a FullClaimToScore structure and populate the RootRiskEntity field in that structure.
        /// </summary>
        /// <param name="riskClaimId">The ID of the RiskClaim to be scored</param>
        /// <returns>A class that holds the RiskEntity Map and fields to hold the score</returns>
        public override FullClaimToScore FetchRiskEntityMapForScoring(int riskClaimId)
        {
            FullClaimToScore ret = new FullClaimToScore();

            MDA.DataService.DataServices dataServices = new MDA.DataService.DataServices(_ctx);
            MDA.DataService.PropertyMethods.PropertyDataServices propertyService = new MDA.DataService.PropertyMethods.PropertyDataServices(_ctx);

            var riskClaim = (from x in _db.RiskClaims.AsNoTracking() where x.Id == riskClaimId select x).First();

            // Get all people on this incident 
            List<int> peopleOnIncident = (from i2p in _db.Incident2Person.AsNoTracking()
                                          where i2p.Incident_Id == riskClaim.Incident_Id && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                                          select i2p.Person_Id).ToList();

            // Get the aliases of all people
            List<int> aliases = new List<int>();
            foreach (var pp in peopleOnIncident)
            {
                aliases.AddRange(dataServices.FindAllPersonAliasesFromDb(pp, true, false, false, false));
                aliases.AddRange(dataServices.FindAllPersonAliasesFromDb(pp, false, true, false, false));
            }

            // Build list of people and their aliases
            ListOfInts peopleOnClaimAndTheirAliasIds = new ListOfInts() ; 
            peopleOnClaimAndTheirAliasIds.AddRange(peopleOnIncident);
            peopleOnClaimAndTheirAliasIds.AddRange(aliases);

            int baseRiskClaimId = riskClaim.BaseRiskClaim_Id;

            RiskEngine.Scoring.Entities.PIClaimIncidentRisk incident = null;

            #region Incident
            incident = (from x in _db.Incidents.AsNoTracking()
                        where x.Id == riskClaim.Incident_Id
                        select new RiskEngine.Scoring.Entities.PIClaimIncidentRisk
                        {
                            Db_Id               = x.Id,
                            RiskClaimId         = riskClaimId,
                            BaseRiskClaimId     = baseRiskClaimId,
                            v_ClaimNumber       = "#Error#",
                            v_Reserve           = x.Reserve,
                            v_PaymentsToDate    = x.PaymentsToDate,
                            v_AmbulanceAttended = false,
                            v_IncidentDateTime  = x.IncidentDate,
                            v_IncidentType      = x.IncidentType_Id,
                            v_IncidentType_Text = x.IncidentType.IncidentType1,
                            v_PoliceAttended    = false
                        }).FirstOrDefault();


            var PersonPolicyHolder = (from i2p in _db.Incident2Person.AsNoTracking()
                                      where i2p.Incident_Id == riskClaim.Incident_Id
                                      && i2p.RiskClaim_Id == riskClaimId
                                      && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                                      && i2p.PartyType_Id == (int)MDA.Common.Enum.PartyType.Policyholder
                                      select i2p).FirstOrDefault();

            if (PersonPolicyHolder == null)
            {
                var OrgPolicyHolder = (from i2o in _db.Incident2Organisation.AsNoTracking()
                                       where i2o.Incident_Id == riskClaim.Incident_Id
                                       && i2o.RiskClaim_Id == riskClaimId
                                       && i2o.ADARecordStatus == (byte)ADARecordStatus.Current
                                       && i2o.PartyType_Id == (int)MDA.Common.Enum.PartyType.Policyholder
                                       select i2o).FirstOrDefault();

                if (OrgPolicyHolder != null)
                {
                    incident.v_ClaimNumber           = OrgPolicyHolder.ClaimNumber;
                    incident.v_AmbulanceAttended     = OrgPolicyHolder.AmbulanceAttended;
                    incident.v_PoliceAttended        = OrgPolicyHolder.PoliceAttended;
                    incident.v_IncidentCircumstances = OrgPolicyHolder.IncidentCircs;
                    incident.v_IncidentLocation      = OrgPolicyHolder.IncidentLocation;
                    incident.v_ClaimCode             = OrgPolicyHolder.ClaimCode;
                }
                else
                {
                    // try and find any Incident2Person link

                    var PersonLink = (from x in _db.Incident2Person.AsNoTracking()
                                      where x.Incident_Id == riskClaim.Incident_Id
                                      && x.RiskClaim_Id == riskClaimId
                                      && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                      select x).FirstOrDefault();

                    if (PersonLink != null)
                    {
                        incident.v_ClaimNumber           = PersonLink.ClaimNumber;
                        incident.v_AmbulanceAttended     = PersonLink.AmbulanceAttended;
                        incident.v_PoliceAttended        = PersonLink.PoliceAttended;
                        if (PersonLink.IncidentTime != null)
                            incident.v_IncidentDateTime += PersonLink.IncidentTime;
                        incident.v_IncidentCircumstances = PersonLink.IncidentCircs;
                        incident.v_IncidentLocation      = PersonLink.IncidentLocation;
                        incident.v_ClaimCode             = PersonLink.ClaimCode;
                    }
                    else
                    {
                        var OrgLink = (from x in _db.Incident2Organisation.AsNoTracking()
                                       where x.Incident_Id == riskClaim.Incident_Id
                                       && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select x).FirstOrDefault();

                        if (OrgLink != null)
                        {
                            incident.v_ClaimNumber           = OrgLink.ClaimNumber;
                            incident.v_AmbulanceAttended     = OrgLink.AmbulanceAttended;
                            incident.v_PoliceAttended        = OrgLink.PoliceAttended;
                            if (OrgLink.IncidentTime != null)
                                incident.v_IncidentDateTime += OrgLink.IncidentTime;
                            incident.v_IncidentCircumstances = OrgLink.IncidentCircs;
                            incident.v_IncidentLocation      = OrgLink.IncidentLocation;
                            incident.v_ClaimCode             = OrgLink.ClaimCode;
                        }
                    }
                }
            }
            else
            {
                incident.v_ClaimNumber               = PersonPolicyHolder.ClaimNumber;
                incident.v_AmbulanceAttended         = PersonPolicyHolder.AmbulanceAttended;
                incident.v_PoliceAttended            = PersonPolicyHolder.PoliceAttended;
                if (PersonPolicyHolder.IncidentTime != null)
                    incident.v_IncidentDateTime     += PersonPolicyHolder.IncidentTime;
                incident.v_IncidentCircumstances     = PersonPolicyHolder.IncidentCircs;
                incident.v_IncidentLocation          = PersonPolicyHolder.IncidentLocation;
                incident.v_ClaimCode                 = PersonPolicyHolder.ClaimCode;
            }

            DateTime theIncidentDateTime = (DateTime)incident.v_IncidentDateTime;

            #endregion Incident

            if (incident == null)
            {
                throw new System.Exception("INCIDENT not found in database");
            }

            //incident.v_AgeRangeCount = (from x in _db.Incident2Person where x.RiskClaim_Id == riskClaimId select x).Count();

            int incidentId = incident.Db_Id;
            ret.RiskEntityMap = incident;

            #region Incident Policy
            incident.IncidentPolicy = (from i2p in _db.Incident2Policy.AsNoTracking()
                                       where i2p.Incident_Id == incidentId
                                       && i2p.RiskClaim_Id == riskClaimId
                                       && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                                       select new RiskEngine.Scoring.Entities.PolicyRisk
                                       {
                                           Db_Id                        = i2p.Policy.Id,
                                           RiskClaimId                  = riskClaimId,
                                           BaseRiskClaimId              = baseRiskClaimId,
                                           v_Broker                     = i2p.Policy.Broker,
                                           v_CoverType                  = i2p.Policy.PolicyCoverType_Id, //(MDA.Common.Enum.PolicyCoverType)i2p.Policy.PolicyCoverType_Id,
                                           v_CoverType_Text             = i2p.Policy.PolicyCoverType.CoverText,
                                           v_PolicyEndDate              = i2p.Policy.PolicyEndDate,
                                           v_PolicyNumber               = i2p.Policy.PolicyNumber,
                                           v_PolicyStartDate            = i2p.Policy.PolicyStartDate,
                                           v_PolicyType                 = i2p.Policy.PolicyType_Id, // (MDA.Common.Enum.PolicyType)i2p.Policy.PolicyType_Id,
                                           v_PolicyType_Text            = i2p.Policy.PolicyType.Text,
                                           v_Premium                    = i2p.Policy.Premium,
                                           v_PreviousFaultClaimsCount   = i2p.Policy.PreviousFaultClaimsCount,
                                           v_PreviousNoFaultClaimsCount = i2p.Policy.PreviousNoFaultClaimsCount,
                                           v_TradingName                = i2p.Policy.InsurerTradingName,
                                           //v_IncidentDate             = incident.IncidentDate
                                       }).FirstOrDefault();
            #endregion Incident Policy

            if (incident.IncidentPolicy != null && incident.v_IncidentDateTime != null)
                incident.IncidentPolicy.v_IncidentDate = theIncidentDateTime;

            if (incident.IncidentPolicy != null)
                incident.v_PolicyStartDate = incident.IncidentPolicy.v_PolicyStartDate;

            if (incident.IncidentPolicy != null)
                incident.v_PolicyEndDate = incident.IncidentPolicy.v_PolicyEndDate;

            #region Person

            incident.Person = (from i2p in _db.Incident2Person.AsNoTracking()
                               where i2p.RiskClaim_Id == riskClaimId
                               && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                               select new RiskEngine.Scoring.Entities.PersonRisk
                               {
                                   Db_Id = i2p.Person_Id,
                                   Db_Incident_Id = i2p.Incident_Id,
                                   RiskClaimId = riskClaimId,
                                   BaseRiskClaimId = baseRiskClaimId,
                                   v_FirstName = i2p.Person.FirstName,
                                   v_DateOfBirth = i2p.Person.DateOfBirth,
                                   v_Gender = i2p.Person.Gender_Id,
                                   v_Gender_Text = i2p.Person.Gender.Text,
                                   v_LastName = i2p.Person.LastName,
                                   v_MiddleName = i2p.Person.MiddleName,
                                   v_Nationality = i2p.Person.Nationality,
                                   v_Occupation = i2p.Person.Occupation,
                                   v_PartyType = i2p.PartyType_Id, //(MDA.Common.Enum.PartyType)i2p.PartyType_Id,
                                   v_PartyType_Text = i2p.PartyType.PartyTypeText,
                                   v_Salutation = i2p.Person.Salutation_Id,
                                   v_Salutation_Text = i2p.Person.Salutation.Text,
                                   v_SubPartyType = i2p.SubPartyType_Id, //(MDA.Common.Enum.SubPartyType)i2p.SubPartyType_Id,
                                   v_SubPartyType_Text = i2p.SubPartyType.SubPartyText,
                                   v_AmbulanceAttended = i2p.AmbulanceAttended,
                                   v_AttendedHospital = i2p.AttendedHospital,
                                   v_ClaimNotificationDate = i2p.ClaimNotificationDate,
                                   v_ClaimStatus = i2p.ClaimStatus_Id, //(MDA.Common.Enum.ClaimStatus)i2p.ClaimStatus_Id,
                                   v_ClaimStatus_Text = i2p.ClaimStatu.Text,
                                   //v_BankAccountAtMultipleAddressCount   = -1,
                                   //v_BankAccountUsageCount               = -1,
                                   v_ClaimCode = i2p.ClaimCode,
                                   //v_EmailUsageCount                     = -1,
                                   v_IncidentCircumstances = i2p.IncidentCircs,
                                   v_IncidentLocation = i2p.IncidentLocation,
                                   v_MojStatus = i2p.MojStatus_Id, //(MDA.Common.Enum.MojStatus)i2p.MojStatus_Id,
                                   v_MojStatus_Text = i2p.MojStatu.Text,
                                   v_PoliceAttended = i2p.PoliceAttended,
                                   v_PoliceForce = i2p.PoliceForce,
                                   v_PoliceReference = i2p.PoliceReference,
                                   v_Reserve = i2p.Reserve,
                                   v_PaymentsToDate = i2p.PaymentsToDate,
                                   v_IncidentDateTime = theIncidentDateTime, //EntityFunctions.CreateDateTime(null, null, null, i2p.IncidentTime.Value.Hours, i2p.IncidentTime.Value.Minutes, 0.0),
                                   //v_IsKeyAttractor                      = !string.IsNullOrEmpty(i2p.Person.KeyAttractor),
                                   v_KeyAttractor = i2p.Person.KeyAttractor,
                                   v_IncidentDate = i2p.Incident.IncidentDate,
                                   v_SanctionDate = i2p.Person.SanctionDate,
                                   v_SanctionSource = i2p.Person.SanctionSource,

                                   //v_Salutation = i2p.Person.Salutation,
                                   //v_Occupation = i2p.Person.Occupation,
                                   //v_NINumberValidated = i2p.Person.Ni

                               }).FirstOrDefault();

            PersonRisk p = incident.Person;

            #region Process Tracemart P2A link
            var tracemartAddress = (from x in _db.Person2Address.AsNoTracking()
                                    where x.RiskClaim_Id == riskClaimId
                                    && x.Person_Id == p.Db_Id
                                    && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                    && x.TracesmartIKey != null
                                    select x).FirstOrDefault();

            if (tracemartAddress != null)
            {
                p.v_TSIDUAMLResult        = tracemartAddress.TSIDUAMLResult;
                p.v_TracemartDOB          = tracemartAddress.TracesmartDOB;
                p.v_ExperianDOB           = tracemartAddress.ExperianDOB;
                p.v_GoneAway              = tracemartAddress.GoneAway;
                p.v_Insolvency            = tracemartAddress.Insolvency;
                p.v_InsolvencyHistory     = tracemartAddress.InsolvencyHistory;
                //p.v_InsolvencyStatus    = tracemartAddress.InsolvencyStatus;
                //p.v_CCJDate             = tracemartAddress.CCJDate;
                //p.v_CCJType             = tracemartAddress.CCJType;
                p.v_CCJHistory            = tracemartAddress.CCJHistory;
                p.v_CredivaCheck          = tracemartAddress.CredivaCheck;

                p.v_DeathScreenMatchType = tracemartAddress.DeathScreenMatchType;
                p.v_DeathScreenDoD       = tracemartAddress.DeathScreenDoD;
                p.v_DeathScreenRegNo     = tracemartAddress.DeathScreenRegNo;

                var hist = (from x in _db.RiskExternalServicesRequests.AsNoTracking()
                            where x.Unique_Id == tracemartAddress.Id
                            select x).FirstOrDefault();

                if (hist != null)
                {
                    p.v_TracesmartServicesCalled = hist.ServicesCalledMask;
                }
            }
            #endregion

            #region Addresses

            p.PersonsAddresses = (from x in _db.Person2Address.AsNoTracking()
                                    where x.RiskClaim_Id == riskClaimId
                                    && x.LinkConfidence == (int)LinkConfidence.Confirmed
                                    && x.Person_Id == p.Db_Id
                                    && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                    select new RiskEngine.Scoring.Entities.AddressRisk
                                    {
                                        Db_Id                    = x.Address.Id,
                                        v_IsConfirmed            = true,
                                        RiskClaimId              = riskClaimId,
                                        BaseRiskClaimId          = baseRiskClaimId,
                                        v_AddressLinkType        = x.AddressLinkType_Id, //(MDA.Common.Enum.AddressLinkType)x.AddressLinkType_Id,
                                        v_AddressLinkType_Text   = x.AddressLinkType.LinkType,
                                        v_Building               = x.Address.Building,
                                        v_BuildingNumber         = x.Address.BuildingNumber,
                                        v_County                 = x.Address.County,
                                        v_EndOfResidency         = x.EndOfResidency,
                                        v_Locality               = x.Address.Locality,
                                        v_Postcode               = x.Address.PostCode,
                                        v_StartOfResidency       = x.StartOfResidency,
                                        v_Street                 = x.Address.Street,
                                        v_SubBuilding            = x.Address.SubBuilding,
                                        v_Town                   = x.Address.Town,
                                        //v_IsKeyAttractor       = !string.IsNullOrEmpty(x.Address.KeyAttractor),
                                        v_PafValidation          = (x.Address.PafValidation != 0),
                                        v_PropertyType           = x.Address.PropertyType,
                                        v_MosaicCode             = x.Address.MosaicCode,
                                        v_IncidentDate           = theIncidentDateTime
                                              
                                    }).Distinct().ToList();

            foreach (var a in p.PersonsAddresses)
            {
                a.PeopleOnClaimAndTheirAliasIds = peopleOnClaimAndTheirAliasIds;
            }

            p.PersonsUnconfirmedAddresses = (from x in _db.Person2Address.AsNoTracking()
                                                where x.RiskClaim_Id == riskClaimId
                                                && x.LinkConfidence == (int)LinkConfidence.Unconfirmed
                                                && x.Person_Id == p.Db_Id
                                                && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select new RiskEngine.Scoring.Entities.AddressRisk
                                                {
                                                    Db_Id = x.Address.Id,
                                                    v_IsConfirmed = false,
                                                    RiskClaimId = riskClaimId,
                                                    BaseRiskClaimId = baseRiskClaimId,
                                                    v_AddressLinkType = x.AddressLinkType_Id, //(MDA.Common.Enum.AddressLinkType)x.AddressLinkType_Id,
                                                    v_AddressLinkType_Text = x.AddressLinkType.LinkType,
                                                    v_Building = x.Address.Building,
                                                    v_BuildingNumber = x.Address.BuildingNumber,
                                                    v_County = x.Address.County,
                                                    v_EndOfResidency = x.EndOfResidency,
                                                    v_Locality = x.Address.Locality,
                                                    v_Postcode = x.Address.PostCode,
                                                    v_StartOfResidency = x.StartOfResidency,
                                                    v_Street = x.Address.Street,
                                                    v_SubBuilding = x.Address.SubBuilding,
                                                    v_Town = x.Address.Town,
                                                    //v_IsKeyAttractor       = !string.IsNullOrEmpty(x.Address.KeyAttractor),
                                                    v_PafValidation = (x.Address.PafValidation != 0),
                                                    v_PropertyType = x.Address.PropertyType,
                                                    v_MosaicCode = x.Address.MosaicCode,
                                                    v_IncidentDate = theIncidentDateTime

                                                }).Distinct().ToList();

            foreach (var a in p.PersonsUnconfirmedAddresses)
            {
                a.PeopleOnClaimAndTheirAliasIds = peopleOnClaimAndTheirAliasIds;
            }

            #endregion

            #region BankAccount
            var p2ba = (from x in _db.Person2BankAccount.AsNoTracking()
                        where x.Person_Id == p.Db_Id
                        && x.RiskClaim_Id == riskClaimId
                        && x.ADARecordStatus == (byte)ADARecordStatus.Current
                        select x).FirstOrDefault();

            if (p2ba != null)
            {
                p.v_BankAccountValidated        = p2ba.Validated;
                p.v_BankAccountDb_Id            = p2ba.BankAccount_Id;
                p.v_BankAccountSortCode         = p2ba.BankAccount.SortCode;
                p.v_BankAccountAccountNumber    = p2ba.BankAccount.AccountNumber;
                //p.v_IsBankAccountKeyAttractor = !string.IsNullOrEmpty(p2ba.BankAccount.KeyAttractor);
                p.v_BankAccountKeyAttractor     = p2ba.BankAccount.KeyAttractor;
                p.v_BankAccountName             = p2ba.BankAccount.BankName;
                p.v_BankAccountFraudRingStatus  = null;
            }
            #endregion Bank Account

            #region Payment Card
            var p2pc = (from x in _db.Person2PaymentCard.AsNoTracking()
                        where x.Person_Id == p.Db_Id
                        && x.RiskClaim_Id == riskClaimId
                        && x.ADARecordStatus == (byte)ADARecordStatus.Current
                        select x).FirstOrDefault();

            if (p2pc != null)
            {
                p.v_PaymentCardNumberValid      = p2pc.CardNumberValid;
                p.v_PaymentCardDb_Id            = p2pc.PaymentCard_Id;
                p.v_PaymentCardSortCode         = p2pc.PaymentCard.SortCode;
                p.v_PaymentCardBankName         = p2pc.PaymentCard.BankName;
                p.v_PaymentCardAccountNumber    = p2pc.PaymentCard.PaymentCardNumber;
                //p.v_IsPaymentCardKeyAttractor = !string.IsNullOrEmpty(p2pc.PaymentCard.KeyAttractor);
                p.v_PaymentCardKeyAttractor     = p2pc.PaymentCard.KeyAttractor;
                p.v_PaymentCardFraudRingStatus  = null;
            }
            #endregion payment Card

            #region Driving License
            var p2dl = (from x in _db.Person2DrivingLicense.AsNoTracking()
                        where x.Person_Id == p.Db_Id
                        && x.RiskClaim_Id == riskClaimId
                        && x.ADARecordStatus == (byte)ADARecordStatus.Current
                        select x).FirstOrDefault();

            if (p2dl != null)
            {
                p.v_DrivingLicenseValidated = p2dl.Validated;
                p.v_DrivingLicenseNumber    = p2dl.DrivingLicense.DriverNumber;
            }
            #endregion Driving License

            #region NI Number
            var p2ni = (from x in _db.Person2NINumber.AsNoTracking()
                        where x.Person_Id == p.Db_Id
                        && x.RiskClaim_Id == riskClaimId
                        && x.ADARecordStatus == (byte)ADARecordStatus.Current
                        select x).FirstOrDefault();

            if (p2ni != null)
            {
                p.v_NINumber          = p2ni.NINumber.NINumber1;
                p.v_NINumberValidated = p2ni.Validated;
            }
            #endregion NI Number

            #region Passport
            var p2pass = (from x in _db.Person2Passport.AsNoTracking()
                            where x.Person_Id == p.Db_Id
                            && x.RiskClaim_Id == riskClaimId
                            && x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).FirstOrDefault();

            if (p2pass != null)
            {
                p.v_PassportNumber      = p2pass.Passport.PassportNumber;
                p.v_PassportMRZValid    = p2pass.MRZValid;
                p.v_PassportDOBValid    = p2pass.DOBValid;
                p.v_PassportGenderValid = p2pass.GenderValid;
            }
            #endregion Passport

            #region Landline Telephone
            p.PersonsTelephones = (from x in _db.Person2Telephone.AsNoTracking()
                                    where x.Person_Id == p.Db_Id
                                    && x.RiskClaim_Id == riskClaimId
                                    && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                    select new TelephoneRisk()
                                    {
                                        Db_Id               = x.Telephone_Id,
                                        v_TelephoneType     = x.Telephone.TelephoneType_Id,
                                        v_TelephoneTypeText = x.Telephone.TelephoneType.PhoneText,
                                        v_TelephoneNumber   = x.Telephone.TelephoneNumber,
                                        v_Validated         = x.Validated,
                                        v_Status            = x.Status,
                                        v_CurrentLocation   = x.CurrentLocation
                                    }).Distinct().ToList();

            #endregion

            #region Email
            var p2em = (from x in _db.Person2Email.AsNoTracking()
                        where x.Person_Id == p.Db_Id
                        && x.RiskClaim_Id == riskClaimId
                        && x.ADARecordStatus == (byte)ADARecordStatus.Current
                        select x).FirstOrDefault();

            if (p2em != null)
            {
                p.v_EmailAddress = p2em.Email.EmailAddress;
            }
            #endregion Email


            p.ConfirmedAliases   = dataServices.FindAllPersonAliasesFromDb(p.Db_Id, true, false, false, _trace);
            p.UnconfirmedAliases = dataServices.FindAllPersonAliasesFromDb(p.Db_Id, false, true, false, _trace);
            p.TentativeAliases   = dataServices.FindAllPersonAliasesFromDb(p.Db_Id, false, false, true, _trace);

            p.PeopleOnClaimAndTheirAliasIds = peopleOnClaimAndTheirAliasIds;

            EntityAliases personAliases = new EntityAliases("CONFIRMED+UNCONFIRMED", p.ConfirmedAliases, p.UnconfirmedAliases, p.TentativeAliases);



            p.v_LinkedEmails                = propertyService.Person_LinkedEmails(personAliases, p.Db_Id, riskClaimId, _trace);
            p.v_LinkedNINumbers             = propertyService.Person_LinkedNINumbers(personAliases, p.Db_Id, riskClaimId, _trace);
            p.v_LinkedTelephone             = propertyService.Person_LinkedTelephones(personAliases, p.Db_Id, riskClaimId, _trace);
            p.v_LinkedBankAccounts          = propertyService.Person_LinkedBankAccounts(personAliases, p.Db_Id, riskClaimId, _trace);
            p.v_LinkedDrivingLicenceNumbers = propertyService.Person_LinkedDrivingLicenceNumbers(personAliases, p.Db_Id, riskClaimId, _trace);
            p.v_LinkedPassportNumbers       = propertyService.Person_LinkedPassportNumbers(personAliases, p.Db_Id, riskClaimId, _trace);
            p.v_LinkedAddress               = propertyService.Person_LinkedAddresses(personAliases, p.Db_Id, riskClaimId, _trace);
            p.v_LinkedAlias                 = propertyService.Person_AliasInformation(personAliases, p.Db_Id, _trace);

            #region Person Organisations
            p.PersonsOrganisations = (from x in _db.Person2Organisation.AsNoTracking()
                                        where x.RiskClaim_Id == riskClaimId
                                        && x.Person_Id == p.Db_Id
                                        && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                        select new RiskEngine.Scoring.Entities.OrganisationRisk
                                        {
                                            Db_Id                               = x.Organisation.Id,
                                            RiskClaimId                         = riskClaimId,
                                            BaseRiskClaimId                     = baseRiskClaimId,
                                            v_OrganisationName                  = x.Organisation.OrganisationName,
                                            v_OrganisationPersonLinkType        = x.Person2OrganisationLinkType_Id, //(MDA.Common.Enum.Person2OrganisationLinkType)x.Person2OrganisationLinkType_Id,
                                            v_OrganisationPersonLinkType_Text   = x.Person2OrganisationLinkType.LinkTypeText,
                                            v_OrganisationType                  = x.Organisation.OrganisationType_Id, //(MDA.Common.Enum.OrganisationType)x.Organisation.OrganisationType_Id,
                                            v_OrganisationType_Text             = x.Organisation.OrganisationType.TypeText,
                                            v_RegisteredNumber                  = x.Organisation.RegisteredNumber,
                                            v_VatNumber                         = x.Organisation.VATNumber,
                                            //v_IsKeyAttractor                  = !string.IsNullOrEmpty(x.Organisation.KeyAttractor),
                                            v_IsClaimLevel                      = false,

                                        }).Distinct().ToList();

            foreach (var o in p.PersonsOrganisations)
            {
                #region Organisation Addresses

                o.OrganisationsAddresses = (from x in _db.Organisation2Address.AsNoTracking()
                                            where x.RiskClaim_Id == riskClaimId
                                            && x.Organisation_Id == o.Db_Id
                                            && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select new RiskEngine.Scoring.Entities.AddressRisk
                                            {
                                                Db_Id                    = x.Address.Id,
                                                RiskClaimId              = riskClaimId,
                                                BaseRiskClaimId          = baseRiskClaimId,
                                                v_AddressLinkType        = x.AddressLinkType_Id, //(MDA.Common.Enum.AddressLinkType)x.AddressLinkType_Id,
                                                v_AddressLinkType_Text   = x.AddressLinkType.LinkType,
                                                v_Building               = x.Address.Building,
                                                v_BuildingNumber         = x.Address.BuildingNumber,
                                                v_County                 = x.Address.County,
                                                v_EndOfResidency         = x.EndOfResidency,
                                                v_Locality               = x.Address.Locality,
                                                v_Postcode               = x.Address.PostCode,
                                                v_StartOfResidency       = x.StartOfResidency,
                                                v_Street                 = x.Address.Street,
                                                v_SubBuilding            = x.Address.SubBuilding,
                                                v_Town                   = x.Address.Town,
                                                //v_IsKeyAttractor       = !string.IsNullOrEmpty(x.Address.KeyAttractor),
                                                v_PafValidation          = (x.Address.PafValidation != 0),
                                                v_IncidentDate           = theIncidentDateTime,
                                            }).Distinct().ToList();

                foreach (var a in o.OrganisationsAddresses)
                {
                    a.PeopleOnClaimAndTheirAliasIds = peopleOnClaimAndTheirAliasIds;
                }

                #endregion Organisation Addresses

                #region Organisation Vehicles

                o.OrganisationsVehicles = (from v2o in _db.Vehicle2Organisation.AsNoTracking()
                                            where v2o.RiskClaim_Id == riskClaimId
                                                && v2o.Organisation_Id == o.Db_Id
                                                && v2o.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select new RiskEngine.Scoring.Entities.OrganisationVehicleRisk
                                            {
                                                Db_Id                        = v2o.Vehicle.Id,
                                                RiskClaimId                  = riskClaimId,
                                                BaseRiskClaimId              = baseRiskClaimId,
                                                v_Colour                     = v2o.Vehicle.VehicleColour_Id, //(MDA.Common.Enum.VehicleColour)v2o.Vehicle.VehicleColour_Id,
                                                v_Colour_Text                = v2o.Vehicle.VehicleColour.Colour,
                                                v_EngineCapacity             = v2o.Vehicle.EngineCapacity,
                                                v_Fuel                       = v2o.Vehicle.VehicleFuel_Id, //(MDA.Common.Enum.VehicleFuelType)v2o.Vehicle.VehicleFuel_Id,
                                                v_Fuel_Text                  = v2o.Vehicle.VehicleFuel.FuelText,
                                                v_Make                       = v2o.Vehicle.VehicleMake,
                                                v_Model                      = v2o.Vehicle.Model,
                                                v_Transmission               = v2o.Vehicle.VehicleTransmission_Id, //(MDA.Common.Enum.VehicleTransmission)v2o.Vehicle.VehicleTransmission_Id,
                                                v_Transmission_Text          = v2o.Vehicle.VehicleTransmission.Text,
                                                //v_VehicleIncidentLink      = (MDA.Common.Enum.Incident2VehicleLinkType)x.Incident2VehicleLinkType_Id,
                                                //v_VehicleIncidentLink_Text = x.Incident2VehicleLinkType.Text,
                                                v_VehicleRegistration        = v2o.Vehicle.VehicleRegistration,
                                                v_VehicleType                = v2o.Vehicle.VehicleType_Id, //(MDA.Common.Enum.VehicleType)v2o.Vehicle.VehicleType_Id,
                                                v_VehicleType_Text           = v2o.Vehicle.VehicleType.Text,
                                                v_VIN                        = v2o.Vehicle.VIN,
                                                //v_CategoryOfLoss           = (MDA.Common.Enum.VehicleCategoryOfLoss)x.Vehicle.VehicleCategoryOfLoss_Id,
                                                //v_CategoryOfLoss_Text      = x.Vehicle.VehicleCategoryOfLoss.CatText,
                                                //v_OccupancyCount           = -1,
                                                //v_IsKeyAttractor           = !string.IsNullOrEmpty(x.Vehicle.KeyAttractor),
                                                v_HireStartDate              = v2o.HireStartDate,
                                                v_HireEndDate                = v2o.HireEndDate,
                                            }).Distinct().ToList();



                #endregion Organisation Vehicles

                #region Telephones
                o.OrganisationsTelephones = (from x in _db.Organisation2Telephone.AsNoTracking()
                                                where x.Organisation_Id == o.Db_Id
                                                    && x.RiskClaim_Id == riskClaimId
                                                    && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                                select new TelephoneRisk()
                                                {
                                                    Db_Id               = x.Telephone_Id,
                                                    v_TelephoneType     = x.Telephone.TelephoneType_Id,
                                                    v_TelephoneTypeText = x.Telephone.TelephoneType.PhoneText,
                                                    v_TelephoneNumber   = x.Telephone.TelephoneNumber,
                                                    //v_Validated       = x.Validated,
                                                    //v_Status          = x.Status,
                                                    //v_CurrentLocation = x.CurrentLocation
                                                }).Distinct().ToList();

                #endregion Telephones

                #region Email
                var o2em = (from x in _db.Organisation2Email.AsNoTracking()
                            where x.Organisation_Id == o.Db_Id
                            && x.RiskClaim_Id == riskClaimId
                            && x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).FirstOrDefault();

                if (o2em != null)
                {
                    o.v_EmailAddress = o2em.Email.EmailAddress;
                }
                #endregion Email

                #region WebSite
                var o2ws = (from x in _db.Organisation2WebSite.AsNoTracking()
                            where x.Organisation_Id == o.Db_Id
                            && x.RiskClaim_Id == riskClaimId
                            && x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).FirstOrDefault();

                if (o2ws != null)
                {
                    o.v_WebsiteAddress = o2ws.WebSite.URL;
                }
                #endregion Website

                o.ConfirmedAliases   = dataServices.FindAllOrganisationAliasesFromDb(o.Db_Id, true, false, false, false);
                o.UnconfirmedAliases = dataServices.FindAllOrganisationAliasesFromDb(o.Db_Id, false, true, false, false);
                o.TentativeAliases   = dataServices.FindAllOrganisationAliasesFromDb(o.Db_Id, false, false, true, false);

                EntityAliases organisationAliases = new EntityAliases("CONFIRMED+UNCONFIRMED", o.ConfirmedAliases, o.UnconfirmedAliases, o.TentativeAliases);

                o.v_LinkedAddresses  = propertyService.Organisation_LinkedAddresses(organisationAliases, o.Db_Id, riskClaimId, _trace);
                o.v_LinkedEmails     = propertyService.Organisation_LinkedEmails(organisationAliases, o.Db_Id, riskClaimId, _trace);
                o.v_LinkedWebSites   = propertyService.Organisation_LinkedWebSites(organisationAliases, o.Db_Id, riskClaimId, _trace);
                o.v_LinkedTelephones = propertyService.Organisation_LinkedTelephones(organisationAliases, o.Db_Id, riskClaimId, _trace);
                o.v_TradingNames     = propertyService.Organisation_TradingNames(organisationAliases, o.Db_Id, riskClaimId, _trace);
                o.v_FormerNames      = propertyService.Organisation_FormerNames(organisationAliases, o.Db_Id, riskClaimId, _trace);
            }
            #endregion Person Orgs

            #endregion Person


            #region Incident Organisations
            incident.IncidentOrganisations = (from i2o in _db.Incident2Organisation.AsNoTracking()
                                              where i2o.RiskClaim_Id == riskClaimId && i2o.IsClaimLevelLink == true
                                              && i2o.ADARecordStatus == (byte)ADARecordStatus.Current
                                              select new RiskEngine.Scoring.Entities.OrganisationRisk
                                              {
                                                  Db_Id                     = i2o.Organisation.Id,
                                                  RiskClaimId               = riskClaimId,
                                                  BaseRiskClaimId           = baseRiskClaimId,
                                                  v_OrganisationName        = i2o.Organisation.OrganisationName,
                                                  v_OrganisationType        = i2o.Organisation.OrganisationType_Id, //(MDA.Common.Enum.OrganisationType)i2o.Organisation.OrganisationType_Id,
                                                  v_OrganisationType_Text   = i2o.Organisation.OrganisationType.TypeText,
                                                  v_RegisteredNumber        = i2o.Organisation.RegisteredNumber,
                                                  v_VatNumber               = i2o.Organisation.VATNumber,
                                                  //v_IsKeyAttractor        = !string.IsNullOrEmpty(i2o.Organisation.KeyAttractor),
                                                  v_IsClaimLevel            = true,
                                                  v_PartyType               = i2o.PartyType_Id, //(MDA.Common.Enum.PartyType)i2o.PartyType_Id,
                                                  v_PartyType_Text          = i2o.PartyType.PartyTypeText,
                                              }).Distinct().ToList();



            foreach (var o in incident.IncidentOrganisations)
            {
                o.v_OrganisationPersonLinkType_Text = ReportStringsClaimLevel[(int)o.v_PartyType];

                o.ConfirmedAliases   = dataServices.FindAllOrganisationAliasesFromDb(o.Db_Id, true, false, false, false);
                o.UnconfirmedAliases = dataServices.FindAllOrganisationAliasesFromDb(o.Db_Id, false, true, false, false);
                o.TentativeAliases   = dataServices.FindAllOrganisationAliasesFromDb(o.Db_Id, false, false, true, false);


                #region Organisation Address

                o.OrganisationsAddresses = (from o2a in _db.Organisation2Address.AsNoTracking()
                                            where o2a.RiskClaim_Id == riskClaimId
                                            && o2a.Organisation_Id == o.Db_Id
                                            && o2a.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select new RiskEngine.Scoring.Entities.AddressRisk
                                            {
                                                Db_Id                    = o2a.Address.Id,
                                                RiskClaimId              = riskClaimId,
                                                BaseRiskClaimId          = baseRiskClaimId,
                                                v_AddressLinkType        = o2a.AddressLinkType_Id, //(MDA.Common.Enum.AddressLinkType)o2a.AddressLinkType_Id,
                                                v_AddressLinkType_Text   = o2a.AddressLinkType.LinkType,
                                                v_Building               = o2a.Address.Building,
                                                v_BuildingNumber         = o2a.Address.BuildingNumber,
                                                v_County                 = o2a.Address.County,
                                                v_EndOfResidency         = o2a.EndOfResidency,
                                                v_Locality               = o2a.Address.Locality,
                                                v_Postcode               = o2a.Address.PostCode,
                                                v_StartOfResidency       = o2a.StartOfResidency,
                                                v_Street                 = o2a.Address.Street,
                                                v_SubBuilding            = o2a.Address.SubBuilding,
                                                v_Town                   = o2a.Address.Town,
                                                //v_IsKeyAttractor       = !string.IsNullOrEmpty(o2a.Address.KeyAttractor),
                                                v_PafValidation          = (o2a.Address.PafValidation != 0)
                                            }).Distinct().ToList();
                #endregion  Organisation Address

                #region Telephones
                o.OrganisationsTelephones = (from x in _db.Organisation2Telephone.AsNoTracking()
                                             where x.Organisation_Id == o.Db_Id
                                                 && x.RiskClaim_Id == riskClaimId
                                                 && x.ADARecordStatus == (byte)ADARecordStatus.Current
                                             select new TelephoneRisk()
                                             {
                                                 Db_Id               = x.Telephone_Id,
                                                 v_TelephoneType     = x.Telephone.TelephoneType_Id,
                                                 v_TelephoneTypeText = x.Telephone.TelephoneType.PhoneText,
                                                 v_TelephoneNumber   = x.Telephone.TelephoneNumber,
                                                 //v_Validated       = x.Validated,
                                                 //v_Status          = x.Status,
                                                 //v_CurrentLocation = x.CurrentLocation
                                             }).Distinct().ToList();


                #endregion Telephones

                #region Email
                var o2em = (from x in _db.Organisation2Email.AsNoTracking()
                            where x.Organisation_Id == o.Db_Id
                            && x.RiskClaim_Id == riskClaimId
                            && x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).FirstOrDefault();

                if (o2em != null)
                {
                    o.v_EmailAddress = o2em.Email.EmailAddress;
                }
                #endregion Email

                #region WebSite
                var o2ws = (from x in _db.Organisation2WebSite.AsNoTracking()
                            where x.Organisation_Id == o.Db_Id
                            && x.RiskClaim_Id == riskClaimId
                            && x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).FirstOrDefault();

                if (o2ws != null)
                {
                    o.v_WebsiteAddress = o2ws.WebSite.URL;
                }
                #endregion Website

                #region Organisation Vehicles

                o.OrganisationsVehicles = (from v2o in _db.Vehicle2Organisation.AsNoTracking()
                                           where v2o.RiskClaim_Id == riskClaimId
                                              && v2o.Organisation_Id == o.Db_Id
                                              && v2o.ADARecordStatus == (byte)ADARecordStatus.Current
                                           select new RiskEngine.Scoring.Entities.OrganisationVehicleRisk
                                           {
                                               Db_Id                        = v2o.Vehicle.Id,
                                               RiskClaimId                  = riskClaimId,
                                               BaseRiskClaimId              = baseRiskClaimId,
                                               v_Colour                     = v2o.Vehicle.VehicleColour_Id, //(MDA.Common.Enum.VehicleColour)v2o.Vehicle.VehicleColour_Id,
                                               v_Colour_Text                = v2o.Vehicle.VehicleColour.Colour,
                                               v_EngineCapacity             = v2o.Vehicle.EngineCapacity,
                                               v_Fuel                       = v2o.Vehicle.VehicleFuel_Id, //(MDA.Common.Enum.VehicleFuelType)v2o.Vehicle.VehicleFuel_Id,
                                               v_Fuel_Text                  = v2o.Vehicle.VehicleFuel.FuelText,
                                               v_Make                       = v2o.Vehicle.VehicleMake,
                                               v_Model                      = v2o.Vehicle.Model,
                                               v_Transmission               = v2o.Vehicle.VehicleTransmission_Id, //(MDA.Common.Enum.VehicleTransmission)v2o.Vehicle.VehicleTransmission_Id,
                                               v_Transmission_Text          = v2o.Vehicle.VehicleTransmission.Text,
                                               //v_VehicleIncidentLink      = (MDA.Common.Enum.Incident2VehicleLinkType)x.Incident2VehicleLinkType_Id,
                                               //v_VehicleIncidentLink_Text = x.Incident2VehicleLinkType.Text,
                                               v_VehicleRegistration        = v2o.Vehicle.VehicleRegistration,
                                               v_VehicleType                = v2o.Vehicle.VehicleType_Id, //(MDA.Common.Enum.VehicleType)v2o.Vehicle.VehicleType_Id,
                                               v_VehicleType_Text           = v2o.Vehicle.VehicleType.Text,
                                               v_VIN                        = v2o.Vehicle.VIN,
                                               //v_CategoryOfLoss           = (MDA.Common.Enum.VehicleCategoryOfLoss)x.Vehicle.VehicleCategoryOfLoss_Id,
                                               //v_CategoryOfLoss_Text      = x.Vehicle.VehicleCategoryOfLoss.CatText,
                                               //v_OccupancyCount           = -1,
                                               //v_IsKeyAttractor           = !string.IsNullOrEmpty(x.Vehicle.KeyAttractor),
                                           }).Distinct().ToList();

                o.ConfirmedAliases   = dataServices.FindAllOrganisationAliasesFromDb(o.Db_Id, true, false, false, _trace);
                o.UnconfirmedAliases = dataServices.FindAllOrganisationAliasesFromDb(o.Db_Id, false, true, false, _trace);
                o.TentativeAliases   = dataServices.FindAllOrganisationAliasesFromDb(o.Db_Id, false, false, true, _trace);

                #endregion Organisation Vehicles

                EntityAliases organisationAliases = new EntityAliases("CONFIRMED+UNCONFIRMED", o.ConfirmedAliases, o.UnconfirmedAliases, o.TentativeAliases);

                o.v_LinkedAddresses  = propertyService.Organisation_LinkedAddresses(organisationAliases, o.Db_Id, riskClaimId, _trace);
                o.v_LinkedEmails     = propertyService.Organisation_LinkedEmails(organisationAliases, o.Db_Id, riskClaimId, _trace);
                o.v_LinkedWebSites   = propertyService.Organisation_LinkedWebSites(organisationAliases, o.Db_Id, riskClaimId, _trace);
                o.v_LinkedTelephones = propertyService.Organisation_LinkedTelephones(organisationAliases, o.Db_Id, riskClaimId, _trace);
                o.v_TradingNames     = propertyService.Organisation_TradingNames(organisationAliases, o.Db_Id, riskClaimId, _trace);
                o.v_FormerNames      = propertyService.Organisation_FormerNames(organisationAliases, o.Db_Id, riskClaimId, _trace);
            }
            #endregion Incident Organisations

            return ret;
        }

        #endregion
    }
}
