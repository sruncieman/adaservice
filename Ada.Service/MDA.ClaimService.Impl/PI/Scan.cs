﻿using System;
using MDA.Common.Server;
using MDA.Common.Enum;

namespace MDA.ClaimService.PI
{
    public partial class ClaimService
    {
        //private void ScanPipelineRecords(CurrentContext ctx, MDA.Pipeline.Model.PipelineClaimBatch claimBatch,
        //                                Func<CurrentContext, MDA.Pipeline.Model.PipeEntityBase, int> ProcessEntity,
        //                                Func<CurrentContext, MDA.Pipeline.Model.PipeLinkBase, int> ProcessLink)
        //{
        //    foreach (var c in claimBatch.Claims)
        //    {
        //        ScanPipelineRecord(ctx, (MDA.Pipeline.Model.PipelineMotorClaim)c, ProcessEntity, ProcessLink);
        //    }
        //}

        //public void TestScanPipelineRecord(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim)
        //{
        //    ScanPipelineRecord(ctx, (MDA.Pipeline.Model.PipelineMotorClaim)claim, InitialisePipelineEntityRecord, InitialisePipelineLinkRecord);
        //}

        protected override void ScanPipelineRecord(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim newClaim, 
                                                Func<CurrentContext, MDA.Pipeline.Model.PipeEntityBase, int> ProcessEntity,
                                                Func<CurrentContext, MDA.Pipeline.Model.PipeLinkBase, int> ProcessLink)
        {
            MDA.Pipeline.Model.PipelinePIClaim claim = newClaim as MDA.Pipeline.Model.PipelinePIClaim;

            CreatedDate = DateTime.Now;

            // Create and save a Source reference value for EVERY record and link
            ctx.SourceRef = ctx.ClientName.Substring(0, 3) + claim.ClaimNumber;

            claim.OriginalIncidentDateTime = claim.IncidentDate;

            #region Process CLAIM
            ProcessEntity(ctx, claim);

            claim.IncidentDate = claim.IncidentDate.Date;

            if (claim.Operation != (int)Operation.Normal)
                claim.Policy.Operation = claim.Operation;

            #endregion

            #region Process POLICY
            if (claim.Policy != null)
            {
                ProcessEntity(ctx, claim.Policy);
                ProcessLink(ctx, claim.Policy.I2Po_LinkData);

                if (claim.Policy.Insurer == null)
                    claim.Policy.Insurer = ctx.ClientName;
            }
            #endregion

            #region Process PERSON
            if (claim.Person != null)
            {
                var p = claim.Person;

                #region PERSON

                ProcessEntity(ctx, p);
                //ProcessLink(ctx, p.V2Pe_LinkData);
                ProcessLink(ctx, p.Pe2Po_LinkData);
                ProcessLink(ctx, p.P2P_LinkData);
                ProcessLink(ctx, p.I2Pe_LinkData);

                if ((p.I2Pe_LinkData.PartyType_Id == (int)Common.Enum.PartyType.Insured && 
                            p.I2Pe_LinkData.SubPartyType_Id == (int)Common.Enum.SubPartyType.Driver) ||
                        p.I2Pe_LinkData.PartyType_Id == (int)Common.Enum.PartyType.Policyholder)
                {
                    if (claim.Policy != null && claim.Policy.Insurer != null)
                        p.I2Pe_LinkData.Insurer = claim.Policy.Insurer;
                    else
                        p.I2Pe_LinkData.Insurer = ctx.ClientName;

                    p.I2Pe_LinkData.Broker = claim.Policy.Broker;
                    p.I2Pe_LinkData.ClaimNumber = claim.ClaimNumber;
                }

                TimeSpan? newTime;

                if (claim.OriginalIncidentDateTime != null && (claim.OriginalIncidentDateTime.Hour > 0 || claim.OriginalIncidentDateTime.Minute > 0))
                    newTime = new TimeSpan(claim.OriginalIncidentDateTime.Hour, claim.OriginalIncidentDateTime.Minute, 0);
                else
                    newTime = null;

                p.I2Pe_LinkData.IncidentTime = newTime;

                if (claim.ExtraClaimInfo != null)
                {
                    p.I2Pe_LinkData.ClaimInfo = new MDA.Pipeline.Model.PipelineClaimInfo()
                    {
                        ClaimCode             = claim.ExtraClaimInfo.ClaimCode,
                        ClaimNotificationDate = claim.ExtraClaimInfo.ClaimNotificationDate,
                        ClaimStatus_Id        = (int)claim.ExtraClaimInfo.ClaimStatus_Id,
                        IncidentCircumstances = claim.ExtraClaimInfo.IncidentCircumstances,
                        IncidentLocation      = claim.ExtraClaimInfo.IncidentLocation,
                        AmbulanceAttended     = claim.ExtraClaimInfo.AmbulanceAttended,
                        PoliceAttended        = claim.ExtraClaimInfo.PoliceAttended,
                        PoliceForce           = claim.ExtraClaimInfo.PoliceForce,
                        PoliceReference       = claim.ExtraClaimInfo.PoliceReference,
                        PaymentsToDate        = claim.ExtraClaimInfo.PaymentsToDate,
                        Reserve               = claim.ExtraClaimInfo.Reserve,
                    };
                }
                #endregion

                #region Process PEOPLE ADDRESS
                if (p.Addresses != null)
                {
                    foreach (var a in p.Addresses)
                    {
                        if (p.Operation != (int)Operation.Normal && a.Operation == (int)Operation.Normal)
                            a.Operation = p.Operation;

                        string rawAddress = MDA.Common.Helpers.AddressHelper.CreateRawAddress(a);

                        ProcessEntity(ctx, a);
                        ProcessLink(ctx, a.PO2A_LinkData);
                        ProcessLink(ctx, a.V2A_LinkData);
                        ProcessLink(ctx, a.A2A_LinkData);

                        a.PO2A_LinkData.RawAddress = rawAddress;
                        a.V2A_LinkData.RawAddress = rawAddress;
                        a.A2A_LinkData.RawAddress = rawAddress;
                    }
                }
                #endregion

                #region Process PEOPLE BAND ACCOUNTS
                if (p.BankAccounts != null)
                {
                    foreach (var ba in p.BankAccounts)
                    {
                        if (p.Operation != (int)Operation.Normal)
                            ba.Operation = p.Operation;

                        ProcessEntity(ctx, ba);
                        ProcessLink(ctx, ba.Po2Ba_LinkData);
                        ProcessLink(ctx, ba.PO2Ba_LinkData);
                    }
                }
                #endregion

                #region Process PEOPLE DRIVING LICENSES
                if (p.DrivingLicenseNumbers != null)
                {
                    foreach (var dl in p.DrivingLicenseNumbers)
                    {
                        ProcessEntity(ctx, dl);
                        ProcessLink(ctx, dl.Pe2Dl_LinkData);
                    }
                }
                #endregion

                #region Process PEOPLE EMAIL ADDRESSES
                if (p.EmailAddresses != null)
                {
                    foreach (var ea in p.EmailAddresses)
                    {
                        ProcessEntity(ctx, ea);
                        ProcessLink(ctx, ea.PO2E_LinkData);
                    }
                }
                #endregion

                #region Process NI NUMBERS
                if (p.NINumbers != null)
                {
                    foreach (var ni in p.NINumbers)
                    {
                        ProcessEntity(ctx, ni);
                        ProcessLink(ctx, ni.Pe2Ni_LinkData);
                    }
                }
                #endregion

                #region Process PEOPLE PASSPORT NUMBERS
                if (p.PassportNumbers != null)
                {
                    foreach (var pn in p.PassportNumbers)
                    {
                        ProcessEntity(ctx, pn);
                        ProcessLink(ctx, pn.Pe2Pa_LinkData);
                    }
                }
                #endregion

                #region Process PEOPLE PAYMENT CARDS
                if (p.PaymentCards != null)
                {
                    foreach (var pc in p.PaymentCards)
                    {
                        if (p.Operation != (int)Operation.Normal)
                            pc.Operation = p.Operation;

                        ProcessEntity(ctx, pc);
                        ProcessLink(ctx, pc.Po2Pc_LinkData);
                        ProcessLink(ctx, pc.PO2Pc_LinkData);
                    }
                }
                #endregion

                #region Process PEOPLE TELEPHONES
                if (p.Telephones != null)
                {
                    foreach (var tn in p.Telephones)
                    {
                        ProcessEntity(ctx, tn);
                        ProcessLink(ctx, tn.PO2T_LinkData);

                        //_matchingService.ProcessTelephoneNumber(tn);
                    }
                }
                #endregion

                #region Process PEOPLE ORGANISATIONS
                if (p.Organisations != null)
                {
                    foreach (var po in p.Organisations)
                    {
                        #region PEOPLE ORGANISATION

                        if (p.Operation != (int)Operation.Normal && po.Operation == (int)Operation.Normal)
                            po.Operation = p.Operation;

                        ProcessEntity(ctx, po);
                        ProcessLink(ctx, po.I2O_LinkData);
                        ProcessLink(ctx, po.O2O_LinkData);
                        ProcessLink(ctx, po.O2Po_LinkData);
                        ProcessLink(ctx, po.P2O_LinkData);
                        ProcessLink(ctx, po.pV2O_LinkData);

                        bool isPolicyHolder = po.OrganisationType_Id == (int)MDA.Common.Enum.OrganisationType.PolicyHolder;

                        po.I2O_LinkData.PartyType_Id = (isPolicyHolder) ? (int)MDA.Common.Enum.PartyType.Policyholder : (int)MDA.Common.Enum.PartyType.Unknown;
                        po.I2O_LinkData.SubPartyType_Id = (int)MDA.Common.Enum.SubPartyType.Unknown;

                        po.I2O_LinkData.OrganisationType_Id = po.OrganisationType_Id;

                        if (isPolicyHolder)
                        {
                            if (claim.Policy != null && claim.Policy.Insurer != null)
                                po.I2O_LinkData.Insurer = claim.Policy.Insurer;
                            else
                                po.I2O_LinkData.Insurer = ctx.ClientName;

                            po.I2O_LinkData.Broker = claim.Policy.Broker;
                            po.I2O_LinkData.ClaimNumber = claim.ClaimNumber;
                        }

                        TimeSpan? newTime2;

                        if (claim.OriginalIncidentDateTime != null && (claim.OriginalIncidentDateTime.Hour > 0 || claim.OriginalIncidentDateTime.Minute > 0))
                            newTime2 = new TimeSpan(claim.OriginalIncidentDateTime.Hour, claim.OriginalIncidentDateTime.Minute, 0);
                        else
                            newTime2 = null;

                        po.I2O_LinkData.IncidentTime = newTime2;

                        if (claim.ExtraClaimInfo != null)
                        {
                            po.I2O_LinkData.ClaimInfo = new MDA.Pipeline.Model.PipelineClaimInfo()
                            {
                                ClaimCode             = claim.ExtraClaimInfo.ClaimCode,
                                ClaimNotificationDate = claim.ExtraClaimInfo.ClaimNotificationDate,
                                ClaimStatus_Id        = (int)claim.ExtraClaimInfo.ClaimStatus_Id,
                                IncidentCircumstances = claim.ExtraClaimInfo.IncidentCircumstances,
                                IncidentLocation      = claim.ExtraClaimInfo.IncidentLocation,
                                AmbulanceAttended     = claim.ExtraClaimInfo.AmbulanceAttended,
                                PoliceAttended        = claim.ExtraClaimInfo.PoliceAttended,
                                PoliceForce           = claim.ExtraClaimInfo.PoliceForce,
                                PoliceReference       = claim.ExtraClaimInfo.PoliceReference,
                                PaymentsToDate        = claim.ExtraClaimInfo.PaymentsToDate,
                                Reserve               = claim.ExtraClaimInfo.Reserve,
                            };
                        }

                        switch ((MDA.Common.Enum.Person2OrganisationLinkType)po.P2O_LinkData.Person2OrganisationLinkType_Id)
                        {
                            case MDA.Common.Enum.Person2OrganisationLinkType.Recovery:
                                po.pV2O_LinkData.VehicleLinkType_Id = (int)Common.Enum.VehicleLinkType.RecoveredBy;
                                break;
                            case MDA.Common.Enum.Person2OrganisationLinkType.Storage:
                                po.pV2O_LinkData.VehicleLinkType_Id = (int)Common.Enum.VehicleLinkType.StoredAt;
                                break;
                            case MDA.Common.Enum.Person2OrganisationLinkType.Engineer:
                                po.pV2O_LinkData.VehicleLinkType_Id = (int)Common.Enum.VehicleLinkType.InspectedBy;
                                break;
                            case MDA.Common.Enum.Person2OrganisationLinkType.Repairer:
                                po.pV2O_LinkData.VehicleLinkType_Id = (int)Common.Enum.VehicleLinkType.Repairer;
                                break;
                            default:
                                po.pV2O_LinkData.VehicleLinkType_Id = (int)Common.Enum.VehicleLinkType.Unknown;
                                break;
                        }
                        #endregion

                        #region Process PEOPLE ORGANISATION ADDRESS
                        if (po.Addresses != null)
                        {
                            foreach (var a in po.Addresses)
                            {
                                if (po.Operation != (int)Operation.Normal)
                                    a.Operation = po.Operation;

                                string rawAddress = MDA.Common.Helpers.AddressHelper.CreateRawAddress(a);

                                ProcessEntity(ctx, a);

                                ProcessLink(ctx, a.PO2A_LinkData);
                                ProcessLink(ctx, a.V2A_LinkData);
                                ProcessLink(ctx, a.A2A_LinkData);

                                a.PO2A_LinkData.RawAddress = rawAddress;
                                a.V2A_LinkData.RawAddress = rawAddress;
                                a.A2A_LinkData.RawAddress = rawAddress;
                            }
                        }
                        #endregion

                        #region Process PEOPLE ORGANISATION BAND ACCOUNTS
                        if (po.BankAccounts != null)
                        {
                            foreach (var ba in po.BankAccounts)
                            {
                                ProcessEntity(ctx, ba);
                                ProcessLink(ctx, ba.Po2Ba_LinkData);
                                ProcessLink(ctx, ba.PO2Ba_LinkData);
                            }
                        }
                        #endregion

                        #region Process PEOPLE ORGANISATION EMAIL ADDRESSES
                        if (po.EmailAddresses != null)
                        {
                            foreach (var ea in po.EmailAddresses)
                            {
                                ProcessEntity(ctx, ea);
                                ProcessLink(ctx, ea.PO2E_LinkData);
                            }
                        }
                        #endregion

                        #region Process PEOPLE ORGANISATION PAYMENT CARDS
                        if (po.PaymentCards != null)
                        {
                            foreach (var pc in po.PaymentCards)
                            {
                                ProcessEntity(ctx, pc);
                                ProcessLink(ctx, pc.Po2Pc_LinkData);
                                ProcessLink(ctx, pc.PO2Pc_LinkData);
                            }
                        }
                        #endregion

                        #region Process PEOPLE ORGANISATION TELEPHONES
                        if (po.Telephones != null)
                        {
                            foreach (var tn in po.Telephones)
                            {
                                ProcessEntity(ctx, tn);
                                ProcessLink(ctx, tn.PO2T_LinkData);

                                //_matchingService.ProcessTelephoneNumber(tn);
                            }
                        }
                        #endregion

                        #region Process PEOPLE ORGANISATION WEBSITES
                        if (po.WebSites != null)
                        {
                            foreach (var ws in po.WebSites)
                            {
                                ProcessEntity(ctx, ws);
                                ProcessLink(ctx, ws.O2Ws_LinkData);
                            }
                        }
                        #endregion

                        #region Process PEOPLE ORGANISATION VEHICLES
                        if (po.Vehicles != null)
                        {
                            foreach (var pov in po.Vehicles)
                            {
                                if (po.Operation != (int)Operation.Normal && pov.Operation == (int)Operation.Normal)
                                    pov.Operation = po.Operation;

                                ProcessEntity(ctx, pov);
                                ProcessLink(ctx, pov.I2V_LinkData);
                                ProcessLink(ctx, pov.V2O_LinkData);
                                ProcessLink(ctx, pov.V2Po_LinkData);                                        
                                ProcessLink(ctx, pov.V2V_LinkData);

                                pov.V2O_LinkData.HireCompany = po.OrganisationName;
                                pov.V2O_LinkData.VehicleLinkType_Id = (int)MDA.Common.Enum.VehicleLinkType.HiredFrom;

                                if (MDA.Common.Helpers.LinkHelper.CreateHireVehicle2PersonLink(pov, po))
                                {
                                    pov.V2Pe_LinkData = new MDA.Pipeline.Model.PipelineVehicle2PersonLink()
                                    {
                                        VehicleLinkType_Id = (int)MDA.Common.Enum.VehicleLinkType.Hirer
                                    };

                                    ProcessLink(ctx, pov.V2Pe_LinkData);
                                }
                            }
                        }
                        #endregion
                    }
                }
                #endregion
            }

            #endregion

            #region Process CLAIM LEVEL ORGANISATIONS
            foreach (var co in claim.Organisations)
            {
                #region Process ORGANISATION
                ProcessEntity(ctx, co);
                ProcessLink(ctx, co.I2O_LinkData);
                ProcessLink(ctx, co.O2O_LinkData);
                ProcessLink(ctx, co.O2Po_LinkData);
                ProcessLink(ctx, co.P2O_LinkData);
                ProcessLink(ctx, co.pV2O_LinkData);

                if (claim.Operation != (int)Operation.Normal && co.Operation == (int)Operation.Normal)
                    co.Operation = claim.Operation;

                bool isPolicyHolder = co.OrganisationType_Id == (int)MDA.Common.Enum.OrganisationType.PolicyHolder;

                co.I2O_LinkData.PartyType_Id = (isPolicyHolder) ? (int)MDA.Common.Enum.PartyType.Policyholder : (int)MDA.Common.Enum.PartyType.Unknown;
                co.I2O_LinkData.SubPartyType_Id = (int)MDA.Common.Enum.SubPartyType.Unknown;

                co.I2O_LinkData.OrganisationType_Id = co.OrganisationType_Id;

                if (isPolicyHolder)
                {
                    if (claim.Policy != null && claim.Policy.Insurer != null)
                        co.I2O_LinkData.Insurer = claim.Policy.Insurer;
                    else
                        co.I2O_LinkData.Insurer = ctx.ClientName;

                    co.I2O_LinkData.Broker = claim.Policy.Broker;
                    co.I2O_LinkData.ClaimNumber = claim.ClaimNumber;
                }

                TimeSpan? newTime;

                if (claim.OriginalIncidentDateTime != null && (claim.OriginalIncidentDateTime.Hour > 0 || claim.OriginalIncidentDateTime.Minute > 0))
                    newTime = new TimeSpan(claim.OriginalIncidentDateTime.Hour, claim.OriginalIncidentDateTime.Minute, 0);
                else
                    newTime = null;

                co.I2O_LinkData.IncidentTime = newTime;

                if (claim.ExtraClaimInfo != null)
                {
                    co.I2O_LinkData.ClaimInfo = new MDA.Pipeline.Model.PipelineClaimInfo()
                    {
                        ClaimCode             = claim.ExtraClaimInfo.ClaimCode,
                        ClaimNotificationDate = claim.ExtraClaimInfo.ClaimNotificationDate,
                        ClaimStatus_Id        = (int)claim.ExtraClaimInfo.ClaimStatus_Id,
                        IncidentCircumstances = claim.ExtraClaimInfo.IncidentCircumstances,
                        IncidentLocation      = claim.ExtraClaimInfo.IncidentLocation,
                        AmbulanceAttended     = claim.ExtraClaimInfo.AmbulanceAttended,
                        PoliceAttended        = claim.ExtraClaimInfo.PoliceAttended,
                        PoliceForce           = claim.ExtraClaimInfo.PoliceForce,
                        PoliceReference       = claim.ExtraClaimInfo.PoliceReference,
                        PaymentsToDate        = claim.ExtraClaimInfo.PaymentsToDate,
                        Reserve               = claim.ExtraClaimInfo.Reserve,
                    };
                }

                switch ((MDA.Common.Enum.Person2OrganisationLinkType)co.P2O_LinkData.Person2OrganisationLinkType_Id)
                {
                    case MDA.Common.Enum.Person2OrganisationLinkType.Recovery:
                        co.pV2O_LinkData.VehicleLinkType_Id = (int)Common.Enum.VehicleLinkType.RecoveredBy;
                        break;
                    case MDA.Common.Enum.Person2OrganisationLinkType.Storage:
                        co.pV2O_LinkData.VehicleLinkType_Id = (int)Common.Enum.VehicleLinkType.StoredAt;
                        break;
                    case MDA.Common.Enum.Person2OrganisationLinkType.Engineer:
                        co.pV2O_LinkData.VehicleLinkType_Id = (int)Common.Enum.VehicleLinkType.InspectedBy;
                        break;
                    case MDA.Common.Enum.Person2OrganisationLinkType.Repairer:
                        co.pV2O_LinkData.VehicleLinkType_Id = (int)Common.Enum.VehicleLinkType.Repairer;
                        break;
                    default:
                        co.pV2O_LinkData.VehicleLinkType_Id = (int)Common.Enum.VehicleLinkType.Unknown;
                        break;
                }
                #endregion

                #region Process CLAIM ORGANISATION ADDRESSES
                if (co.Addresses != null)
                {
                    foreach (var a in co.Addresses)
                    {
                        if (co.Operation != (int)Operation.Normal)
                            a.Operation = co.Operation;

                        a.AddressType_Id = (int)MDA.Common.Enum.AddressType.Commercial;

                        string rawAddress = MDA.Common.Helpers.AddressHelper.CreateRawAddress(a);

                        ProcessEntity(ctx, a);
                        ProcessLink(ctx, a.PO2A_LinkData);
                        ProcessLink(ctx, a.V2A_LinkData);
                        ProcessLink(ctx, a.A2A_LinkData);

                        a.PO2A_LinkData.RawAddress = rawAddress;
                        a.V2A_LinkData.RawAddress = rawAddress;
                        a.A2A_LinkData.RawAddress = rawAddress;
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION BANK ACCOUNTS
                if (co.BankAccounts != null)
                {
                    foreach (var ba in co.BankAccounts)
                    {
                        if (co.Operation != (int)Operation.Normal)
                            ba.Operation = co.Operation;

                        ProcessEntity(ctx, ba);
                        ProcessLink(ctx, ba.Po2Ba_LinkData);
                        ProcessLink(ctx, ba.PO2Ba_LinkData);
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION EMAIL
                if (co.EmailAddresses != null)
                {
                    foreach (var ea in co.EmailAddresses)
                    {
                        ProcessEntity(ctx, ea);
                        ProcessLink(ctx, ea.PO2E_LinkData);
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION PAYMENT CARDS
                if (co.PaymentCards != null)
                {
                    foreach (var pc in co.PaymentCards)
                    {
                        if (co.Operation != (int)Operation.Normal)
                            pc.Operation = co.Operation;

                        ProcessEntity(ctx, pc);
                        ProcessLink(ctx, pc.Po2Pc_LinkData);
                        ProcessLink(ctx, pc.PO2Pc_LinkData);
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION TELEPHONES
                if (co.Telephones != null)
                {
                    foreach (var tn in co.Telephones)
                    {
                        ProcessEntity(ctx, tn);
                        ProcessLink(ctx, tn.PO2T_LinkData);

                        //_matchingService.ProcessTelephoneNumber(tn);
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION WEBSITES
                if (co.WebSites != null)
                {
                    foreach (var ws in co.WebSites)
                    {
                        ProcessEntity(ctx, ws);
                        ProcessLink(ctx, ws.O2Ws_LinkData);
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION VEHICLES
                if (co.Vehicles != null)
                {
                    foreach (var ov in co.Vehicles)
                    {
                        if (co.Operation != (int)Operation.Normal && ov.Operation == (int)Operation.Normal)
                            ov.Operation = co.Operation;

                        ProcessEntity(ctx, ov);
                        ProcessLink(ctx, ov.I2V_LinkData);
                        ProcessLink(ctx, ov.V2O_LinkData);
                        ProcessLink(ctx, ov.V2Po_LinkData);
                        ProcessLink(ctx, ov.V2Pe_LinkData);
                        ProcessLink(ctx, ov.V2V_LinkData);

                        ov.V2O_LinkData.HireCompany = co.OrganisationName;
                        ov.V2O_LinkData.VehicleLinkType_Id = (int)MDA.Common.Enum.VehicleLinkType.HiredFrom;
                    }
                }
                #endregion
            }
            #endregion

        }

        /// <summary>
        /// Simplified version fo scan which just makes the callbacks without changing any data itself.
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="claim"></param>
        /// <param name="ProcessEntity"></param>
        /// <param name="ProcessLink"></param>
        public void ScanAndCallBackPipelineClaim(CurrentContext ctx, MDA.Pipeline.Model.PipelineMotorClaim claim,
                                        Func<CurrentContext, MDA.Pipeline.Model.PipeEntityBase, int> ProcessEntity,
                                        Func<CurrentContext, MDA.Pipeline.Model.PipeLinkBase, int> ProcessLink)
        {
            #region Process CLAIM
            ProcessEntity(ctx, claim);
            #endregion

            #region Process POLICY
            if (claim.Policy != null)
            {
                ProcessEntity(ctx, claim.Policy);
                ProcessLink(ctx, claim.Policy.I2Po_LinkData);
            }
            #endregion

            #region Process VEHICLES
            foreach (var v in claim.Vehicles)
            {
                ProcessEntity(ctx, v);
                ProcessLink(ctx, v.I2V_LinkData);
                ProcessLink(ctx, v.V2Po_LinkData);
                ProcessLink(ctx, v.V2A_LinkData);
                ProcessLink(ctx, v.V2V_LinkData);

                #region Process PEOPLE
                if (v.People != null)
                {
                    foreach (var p in v.People)
                    {
                        #region PERSON
                        ProcessEntity(ctx, p);
                        ProcessLink(ctx, p.V2Pe_LinkData);
                        ProcessLink(ctx, p.Pe2Po_LinkData);
                        ProcessLink(ctx, p.P2P_LinkData);
                        ProcessLink(ctx, p.I2Pe_LinkData);
                        #endregion

                        #region Process PEOPLE ADDRESS
                        if (p.Addresses != null)
                        {
                            foreach (var a in p.Addresses)
                            {
                                ProcessEntity(ctx, a);
                                ProcessLink(ctx, a.PO2A_LinkData);
                                ProcessLink(ctx, a.V2A_LinkData);
                                ProcessLink(ctx, a.A2A_LinkData);
                            }
                        }
                        #endregion

                        #region Process PEOPLE BAND ACCOUNTS
                        if (p.BankAccounts != null)
                        {
                            foreach (var ba in p.BankAccounts)
                            {
                                ProcessEntity(ctx, ba);
                                ProcessLink(ctx, ba.Po2Ba_LinkData);
                                ProcessLink(ctx, ba.PO2Ba_LinkData);
                            }
                        }
                        #endregion

                        #region Process PEOPLE DRIVING LICENSES
                        if (p.DrivingLicenseNumbers != null)
                        {
                            foreach (var dl in p.DrivingLicenseNumbers)
                            {
                                ProcessEntity(ctx, dl);
                                ProcessLink(ctx, dl.Pe2Dl_LinkData);
                            }
                        }
                        #endregion

                        #region Process PEOPLE EMAIL ADDRESSES
                        if (p.EmailAddresses != null)
                        {
                            foreach (var ea in p.EmailAddresses)
                            {
                                ProcessEntity(ctx, ea);
                                ProcessLink(ctx, ea.PO2E_LinkData);
                            }
                        }
                        #endregion

                        #region Process NI NUMBERS
                        if (p.NINumbers != null)
                        {
                            foreach (var ni in p.NINumbers)
                            {
                                ProcessEntity(ctx, ni);
                                ProcessLink(ctx, ni.Pe2Ni_LinkData);
                            }
                        }
                        #endregion

                        #region Process PEOPLE PASSPORT NUMBERS
                        if (p.PassportNumbers != null)
                        {
                            foreach (var pn in p.PassportNumbers)
                            {
                                ProcessEntity(ctx, pn);
                                ProcessLink(ctx, pn.Pe2Pa_LinkData);
                            }
                        }
                        #endregion

                        #region Process PEOPLE PAYMENT CARDS
                        if (p.PaymentCards != null)
                        {
                            foreach (var pc in p.PaymentCards)
                            {
                                ProcessEntity(ctx, pc);
                                ProcessLink(ctx, pc.Po2Pc_LinkData);
                                ProcessLink(ctx, pc.PO2Pc_LinkData);
                            }
                        }
                        #endregion

                        #region Process PEOPLE TELEPHONES
                        if (p.Telephones != null)
                        {
                            foreach (var tn in p.Telephones)
                            {
                                ProcessEntity(ctx, tn);
                                ProcessLink(ctx, tn.PO2T_LinkData);
                            }
                        }
                        #endregion

                        #region Process PEOPLE ORGANISATIONS
                        if (p.Organisations != null)
                        {
                            foreach (var po in p.Organisations)
                            {
                                #region PEOPLE ORGANISATION
                                ProcessEntity(ctx, po);
                                ProcessLink(ctx, po.I2O_LinkData);
                                ProcessLink(ctx, po.O2O_LinkData);
                                ProcessLink(ctx, po.O2Po_LinkData);
                                ProcessLink(ctx, po.P2O_LinkData);
                                ProcessLink(ctx, po.pV2O_LinkData);
                                #endregion

                                #region Process PEOPLE ORGANISATION ADDRESS
                                if (po.Addresses != null)
                                {
                                    foreach (var a in po.Addresses)
                                    {
                                        ProcessEntity(ctx, a);

                                        ProcessLink(ctx, a.PO2A_LinkData);
                                        ProcessLink(ctx, a.V2A_LinkData);
                                        ProcessLink(ctx, a.A2A_LinkData);
                                    }
                                }
                                #endregion

                                #region Process PEOPLE ORGANISATION BAND ACCOUNTS
                                if (po.BankAccounts != null)
                                {
                                    foreach (var ba in po.BankAccounts)
                                    {
                                        ProcessEntity(ctx, ba);
                                        ProcessLink(ctx, ba.Po2Ba_LinkData);
                                        ProcessLink(ctx, ba.PO2Ba_LinkData);
                                    }
                                }
                                #endregion

                                #region Process PEOPLE ORGANISATION EMAIL ADDRESSES
                                if (po.EmailAddresses != null)
                                {
                                    foreach (var ea in po.EmailAddresses)
                                    {
                                        ProcessEntity(ctx, ea);
                                        ProcessLink(ctx, ea.PO2E_LinkData);
                                    }
                                }
                                #endregion

                                #region Process PEOPLE ORGANISATION PAYMENT CARDS
                                if (po.PaymentCards != null)
                                {
                                    foreach (var pc in po.PaymentCards)
                                    {
                                        ProcessEntity(ctx, pc);
                                        ProcessLink(ctx, pc.Po2Pc_LinkData);
                                        ProcessLink(ctx, pc.PO2Pc_LinkData);
                                    }
                                }
                                #endregion

                                #region Process PEOPLE ORGANISATION TELEPHONES
                                if (po.Telephones != null)
                                {
                                    foreach (var tn in po.Telephones)
                                    {
                                        ProcessEntity(ctx, tn);
                                        ProcessLink(ctx, tn.PO2T_LinkData);
                                    }
                                }
                                #endregion

                                #region Process PEOPLE ORGANISATION WEBSITES
                                if (po.WebSites != null)
                                {
                                    foreach (var ws in po.WebSites)
                                    {
                                        ProcessEntity(ctx, ws);
                                        ProcessLink(ctx, ws.O2Ws_LinkData);
                                    }
                                }
                                #endregion

                                #region Process PEOPLE ORGANISATION VEHICLES
                                if (po.Vehicles != null)
                                {
                                    foreach (var pov in po.Vehicles)
                                    {
                                        ProcessEntity(ctx, pov);
                                        ProcessLink(ctx, pov.I2V_LinkData);
                                        ProcessLink(ctx, pov.V2O_LinkData);
                                        ProcessLink(ctx, pov.V2Po_LinkData);
                                        ProcessLink(ctx, pov.V2V_LinkData);
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                }
                #endregion
            }
            #endregion

            #region Process CLAIM LEVEL ORGANISATIONS
            foreach (var co in claim.Organisations)
            {
                #region Process ORGANISATION
                ProcessEntity(ctx, co);
                ProcessLink(ctx, co.I2O_LinkData);
                ProcessLink(ctx, co.O2O_LinkData);
                ProcessLink(ctx, co.O2Po_LinkData);
                ProcessLink(ctx, co.P2O_LinkData);
                ProcessLink(ctx, co.pV2O_LinkData);
                #endregion

                #region Process CLAIM ORGANISATION ADDRESSES
                if (co.Addresses != null)
                {
                    foreach (var a in co.Addresses)
                    {
                        ProcessEntity(ctx, a);
                        ProcessLink(ctx, a.PO2A_LinkData);
                        ProcessLink(ctx, a.V2A_LinkData);
                        ProcessLink(ctx, a.A2A_LinkData);
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION BANK ACCOUNTS
                if (co.BankAccounts != null)
                {
                    foreach (var ba in co.BankAccounts)
                    {
                        ProcessEntity(ctx, ba);
                        ProcessLink(ctx, ba.Po2Ba_LinkData);
                        ProcessLink(ctx, ba.PO2Ba_LinkData);
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION EMAIL
                if (co.EmailAddresses != null)
                {
                    foreach (var ea in co.EmailAddresses)
                    {
                        ProcessEntity(ctx, ea);
                        ProcessLink(ctx, ea.PO2E_LinkData);
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION PAYMENT CARDS
                if (co.PaymentCards != null)
                {
                    foreach (var pc in co.PaymentCards)
                    {
                        ProcessEntity(ctx, pc);
                        ProcessLink(ctx, pc.Po2Pc_LinkData);
                        ProcessLink(ctx, pc.PO2Pc_LinkData);
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION TELEPHONES
                if (co.Telephones != null)
                {
                    foreach (var tn in co.Telephones)
                    {
                        ProcessEntity(ctx, tn);
                        ProcessLink(ctx, tn.PO2T_LinkData);
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION WEBSITES
                if (co.WebSites != null)
                {
                    foreach (var ws in co.WebSites)
                    {
                        ProcessEntity(ctx, ws);
                        ProcessLink(ctx, ws.O2Ws_LinkData);
                    }
                }
                #endregion

                #region Process CLAIM ORGANISATION VEHICLES
                if (co.Vehicles != null)
                {
                    foreach (var ov in co.Vehicles)
                    {
                        ProcessEntity(ctx, ov);
                        ProcessLink(ctx, ov.I2V_LinkData);
                        ProcessLink(ctx, ov.V2O_LinkData);
                        ProcessLink(ctx, ov.V2Po_LinkData);
                        ProcessLink(ctx, ov.V2Pe_LinkData);
                        ProcessLink(ctx, ov.V2V_LinkData);
                    }
                }
                #endregion
            }
            #endregion

        }

        public void ScanAndCallBackPipelineClaim(CurrentContext ctx, MDA.Pipeline.Model.PipelineMobileClaim claim,
                        Func<CurrentContext, MDA.Pipeline.Model.PipeEntityBase, int> ProcessEntity,
                        Func<CurrentContext, MDA.Pipeline.Model.PipeLinkBase, int> ProcessLink)
        {
            throw new NotImplementedException();
        }
    }
}
