﻿using System;
using System.Diagnostics;
using MDA.Common.Debug;
using MDA.DataService;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Enum;


namespace MDA.ClaimService
{
    /// <summary>
    /// Class that implements methods to process each link record. All method are protected virtual and virtual so they can be overridden if a client has special processing
    /// </summary>
    public partial class ClaimServiceBase
    {
        protected virtual Incident2Organisation ProcessIncident2OrganisationLink(CurrentContext ctx, int incidentId, int organisationId, MDA.Pipeline.Model.PipelineIncident2OrganisationLink pipeLink, bool claimLevelLink,
                                        MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessIncident2OrganisationLink : Find Link -> incidentId[" + incidentId.ToString() + "] organisationId[" + organisationId.ToString() + "]" );

            var link = new DataServices(ctx).GetLatestIncident2OrganisationLink(incidentId, organisationId, claimLevelLink, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateIncident2OrganisationLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);

            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertIncident2OrganisationLink(incidentId, organisationId, pipeLink, claimLevelLink, newRiskClaim);
            }
            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Incident2Person ProcessIncident2PersonLink(CurrentContext ctx, int incidentId, int personId, MDA.Pipeline.Model.PipelineIncident2PersonLink pipeLink,
                                        MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessIncident2PersonLink : Find Link -> incidentId[" + incidentId.ToString() + "] personId[" + personId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestIncident2PersonLink(incidentId, personId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateIncident2PersonLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertIncident2PersonLink(incidentId, personId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Incident2Vehicle ProcessIncident2VehicleLink(CurrentContext ctx, int incidentId, int vehicleId, MDA.Pipeline.Model.PipelineIncident2VehicleLink pipeLink,
                                 MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessIncident2VehicleLink : Find Link -> incidentId[" + incidentId.ToString() + "] vehicleId[" + vehicleId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestIncident2VehicleLink(incidentId, vehicleId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateIncident2VehicleLink((MDA.Common.Enum.Operation)op, link, pipeLink,newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertIncident2VehicleLink(incidentId, vehicleId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Incident2Handset ProcessIncident2HandsetLink(CurrentContext ctx, int incidentId, int handsetId, MDA.Pipeline.Model.PipelineIncident2HandsetLink pipeLink,
                                MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessIncident2HandsetLink : Find Link -> incidentId[" + incidentId.ToString() + "] handsetId[" + handsetId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestIncident2HandsetLink(incidentId, handsetId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateIncident2HandsetLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertIncident2HandsetLink(incidentId, handsetId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Incident2Policy ProcessIncident2PolicyLink(CurrentContext ctx, int incidentId, int policyId,
                         MDA.Pipeline.Model.PipeLinkBase pipeBase, MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessIncident2PolicyLink : Find Link -> incidentId[" + incidentId.ToString() + "] policyId[" + policyId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestIncident2PolicyLink(incidentId, policyId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateIncident2PolicyLink((MDA.Common.Enum.Operation)op, link, pipeBase, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertIncident2PolicyLink(incidentId, policyId, pipeBase, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Handset2Policy ProcessHandset2PolicyLink(CurrentContext ctx, int handsetId, int policyId, MDA.Pipeline.Model.PipelineHandset2PolicyLink pipeLink,
                                    MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessHandset2PolicyLink : Find Link -> handsetId[" + handsetId.ToString() + "] policyId[" + policyId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestHandset2PolicyLink(handsetId, policyId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateHandset2PolicyLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertHandset2PolicyLink(handsetId, policyId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Vehicle2Policy ProcessVehicle2PolicyLink(CurrentContext ctx, int vehicleId, int policyId, MDA.Pipeline.Model.PipelineVehicle2PolicyLink pipeLink,
                                    MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessVehicle2PolicyLink : Find Link -> vehicleId[" + vehicleId.ToString() + "] policyId[" + policyId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestVehicle2PolicyLink(vehicleId, policyId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateVehicle2PolicyLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertVehicle2PolicyLink(vehicleId, policyId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Vehicle2Person ProcessVehicle2PersonLink(CurrentContext ctx, int vehicleId, int personId, MDA.Pipeline.Model.PipelineVehicle2PersonLink pipeLink,
                                    MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessVehicle2PersonLink : Find Link -> vehicleId[" + vehicleId.ToString() + "] personId[" + personId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestVehicle2PersonLink(vehicleId, personId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateVehicle2PersonLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertVehicle2PersonLink(vehicleId, personId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;

        }

        protected virtual Handset2Person ProcessHandset2PersonLink(CurrentContext ctx, int HandsetId, int personId, MDA.Pipeline.Model.PipelineHandset2PersonLink pipeLink,
                                    MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessHandset2PersonLink : Find Link -> HandsetId[" + HandsetId.ToString() + "] personId[" + personId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestHandset2PersonLink(HandsetId, personId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateHandset2PersonLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertHandset2PersonLink(HandsetId, personId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;

        }

        protected virtual Handset2Telephone ProcessHandset2TelephoneLink(CurrentContext ctx, int HandsetId, int telephoneId, MDA.Pipeline.Model.PipelineHandset2TelephoneLink pipeLink,
                                   MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessHandset2TelephoneLink : Find Link -> HandsetId[" + HandsetId.ToString() + "] telephoneId[" + telephoneId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestHandset2TelephoneLink(HandsetId, telephoneId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateHandset2TelephoneLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertHandset2TelephoneLink(HandsetId, telephoneId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;

        }


        protected virtual Person2Policy ProcessPerson2PolicyLink(CurrentContext ctx, int personId, int policyId, MDA.Pipeline.Model.PipelinePolicyLink pipeLink,
                                    MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPerson2PolicyLink : Find Link -> personId[" + personId.ToString() + "] policyId[" + policyId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPerson2PolicyLink(personId, policyId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePerson2PolicyLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPerson2PolicyLink(personId, policyId, pipeLink, newRiskClaim);
            }
            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Policy2BankAccount ProcessPolicy2BankAccountLink(CurrentContext ctx, int policyId, int accountId, 
                                    MDA.Pipeline.Model.PipelinePolicy2BankAccountLink pipeLink,
                                    MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPolicy2BankAccountLink : Find Link -> policyId[" + policyId.ToString() + "] accountId[" + accountId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPolicy2BankAccountLink(policyId, accountId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePolicy2BankAccountLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPolicy2BankAccountLink(policyId, accountId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Policy2PaymentCard ProcessPolicy2PaymentCardLink(CurrentContext ctx, int policyId, int paymentCardId,
                            MDA.Pipeline.Model.PipelinePolicy2PaymentCardLink pipeLink, MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPolicy2PaymentCardLink : Find Link -> policyId[" + policyId.ToString() + "] paymentCardId[" + paymentCardId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPolicy2PaymentCardLink(policyId, paymentCardId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePolicy2PaymentCardLink((MDA.Common.Enum.Operation)op, link, /*xmlPaymentCard,*/ pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPolicy2PaymentCardLink(policyId, paymentCardId, pipeLink, /*xmlPaymentCard.DatePaymentDetailsTaken,*/ newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Person2Organisation ProcessPerson2OrganisationLink(CurrentContext ctx, int personId, int organisationId,
            MDA.Pipeline.Model.PipelinePerson2OrganisationLink pipeLink, MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPerson2OrganisationLink : Find Link -> personId[" + personId.ToString() + "] organisationId[" + organisationId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPerson2OrganisationLink(personId, organisationId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePerson2OrganisationLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPerson2OrganisationLink(personId, organisationId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Vehicle2Address ProcessVehicle2AddressLink(CurrentContext ctx, int vehicleId, int addressId, MDA.Pipeline.Model.PipelineAddressLink pipeLink, 
                                                MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessVehicle2AddressLink : Find Link -> vehicleId[" + vehicleId.ToString() + "] addressId[" + addressId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestVehicle2AddressLink(vehicleId, addressId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateVehicle2AddressLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertVehicle2AddressLink(vehicleId, addressId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Handset2Address ProcessHandset2AddressLink(CurrentContext ctx, int handsetId, int addressId, MDA.Pipeline.Model.PipelineAddressLink pipeLink,
                                                MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessHandset2AddressLink : Find Link -> handsetId[" + handsetId.ToString() + "] addressId[" + addressId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestHandset2AddressLink(handsetId, addressId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateHandset2AddressLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertHandset2AddressLink(handsetId, addressId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Vehicle2Organisation ProcessOrgVehicle2OrganisationLink(CurrentContext ctx, int orgVehicleId, int organisationId,
                                                            MDA.Pipeline.Model.PipelineOrgVehicleLink pipeLink, 
                                                            MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessVehicle2OrganisationLink : Find Link -> orgVehicleId[" + orgVehicleId.ToString() + "] organisationId[" + organisationId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestOrgVehicle2OrganisationLink(orgVehicleId, organisationId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateOrgVehicle2OrganisationLink((MDA.Common.Enum.Operation)op, link,  /*linkType,hireStartDate, hireEndDate, hireCompany,*/ pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);

            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertOrgVehicle2OrganisationLink(orgVehicleId, organisationId, pipeLink, /*linkType, hireStartDate, hireEndDate, hireCompany,*/ newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Handset2Organisation ProcessOrgHandset2OrganisationLink(CurrentContext ctx, int orgHandsetId, int organisationId,
                                                            MDA.Pipeline.Model.PipelineOrgHandsetLink pipeLink,
                                                            MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessHandset2OrganisationLink : Find Link -> orgHandsetId[" + orgHandsetId.ToString() + "] organisationId[" + organisationId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestOrgHandset2OrganisationLink(orgHandsetId, organisationId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateOrgHandset2OrganisationLink((MDA.Common.Enum.Operation)op, link,  /*linkType,hireStartDate, hireEndDate, hireCompany,*/ pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);

            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertOrgHandset2OrganisationLink(orgHandsetId, organisationId, pipeLink, /*linkType, hireStartDate, hireEndDate, hireCompany,*/ newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Organisation2Policy ProcessOrganisation2PolicyLink(CurrentContext ctx, int organisationId, int policyId,
                                            MDA.Pipeline.Model.PipelinePolicyLink pipeLink, 
                                            MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessOrganisation2PolicyLink : Find Link -> organisationId[" + organisationId.ToString() + "] policyId[" + policyId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestOrganisation2PolicyLink(organisationId, policyId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateOrganisation2PolicyLink((MDA.Common.Enum.Operation)op, link, /*linkType,*/ pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertOrganisation2PolicyLink(organisationId, policyId, pipeLink, /*linkType,*/ newRiskClaim);
            }
            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Person2Address ProcessPerson2AddressLink(CurrentContext ctx, int personId, int addressId,
                                    MDA.Pipeline.Model.PipelineAddressLink pipeLink,
                                    MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPerson2AddressLink : Find Link -> personId[" + personId.ToString() + "] addressId[" + addressId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPerson2AddressLink(personId, addressId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePerson2AddressLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPerson2AddressLink(personId, addressId, pipeLink, newRiskClaim);
            }
            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Organisation2Address ProcessOrganisation2AddressLink(CurrentContext ctx, int orgId, int addressId, MDA.Pipeline.Model.PipelineAddressLink pipeLink,
                            MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessOrganisation2AddressLink : Find Link -> orgId[" + orgId.ToString() + "] addressId[" + addressId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestOrganisation2AddressLink(orgId, addressId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateOrganisation2AddressLink((MDA.Common.Enum.Operation)op, link, 
                    pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertOrganisation2AddressLink(orgId, addressId, pipeLink, newRiskClaim);
            }
            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Person2BankAccount ProcessPerson2BankAccountLink(CurrentContext ctx, int personId, int accountId,
            MDA.Pipeline.Model.PipeLinkBase pipeBase, MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPerson2BankAccountLink : Find Link -> personId[" + personId.ToString() + "] accountId[" + accountId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPerson2BankAccountLink(personId, accountId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePerson2BankAccountLink((MDA.Common.Enum.Operation)op, link, pipeBase, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPerson2BankAccountLink(personId, accountId, pipeBase, newRiskClaim);
            }
            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Organisation2BankAccount ProcessOrganisation2BankAccountLink(CurrentContext ctx, int organisationId, int accountId,
            MDA.Pipeline.Model.PipeLinkBase pipeBase, MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessOrganisation2BankAccountLink : Find Link -> organisationId[" + organisationId.ToString() + "] accountId[" + accountId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestOrganisation2BankAccountLink(organisationId, accountId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateOrganisation2BankAccountLink((MDA.Common.Enum.Operation)op, link, pipeBase, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertOrganisation2BankAccountLink(organisationId, accountId, pipeBase, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Person2PaymentCard ProcessPerson2PaymentCardLink(CurrentContext ctx, int personId, int cardId,
            MDA.Pipeline.Model.PipeLinkBase pipeBase, MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPerson2PaymentCardLink : Find Link -> personId[" + personId.ToString() + "] cardId[" + cardId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPerson2PaymentCardLink(personId, cardId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePerson2PaymentCardLink((MDA.Common.Enum.Operation)op, link, pipeBase, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPerson2PaymentCardLink(personId, cardId, pipeBase, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Organisation2PaymentCard ProcessOrganisation2PaymentCardLink(CurrentContext ctx, int organisationId, int paymentCardId,
            MDA.Pipeline.Model.PipeLinkBase pipeBase, MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessOrganisation2PaymentCardLink : Find Link -> organisationId[" + organisationId.ToString() + "] paymentCardId[" + paymentCardId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestOrganisation2PaymentCardLink(organisationId, paymentCardId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateOrganisation2PaymentCardLink((MDA.Common.Enum.Operation)op, link, pipeBase, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertOrganisation2PaymentCardLink(organisationId, paymentCardId, pipeBase, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Person2Email ProcessPerson2EmailLink(CurrentContext ctx, int personId, int emailId, MDA.Pipeline.Model.PipeLinkBase pipeBase,
                                        MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPerson2EmailLink : Find Link -> personId[" + personId.ToString() + "] emailId[" + emailId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPerson2EmailLink(personId, emailId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePerson2EmailLink((MDA.Common.Enum.Operation)op, link, pipeBase, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPerson2EmailLink(personId, emailId, pipeBase, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Organisation2Email ProcessOrganisation2EmailLink(CurrentContext ctx, int orgId, int emailId,
            MDA.Pipeline.Model.PipeLinkBase pipeBase, MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessOrganisation2EmailLink : Find Link -> orgId[" + orgId.ToString() + "] emailId[" + emailId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestOrganisation2EmailLink(orgId, emailId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateOrganisation2EmailLink((MDA.Common.Enum.Operation)op, link, pipeBase, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertOrganisation2EmailLink(orgId, emailId, pipeBase, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Organisation2WebSite ProcessOrganisation2WebsiteLink(CurrentContext ctx, int orgId, int websiteId,
            MDA.Pipeline.Model.PipeLinkBase pipeBase, MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessOrganisation2WebsiteLink : Find Link -> orgId[" + orgId.ToString() + "] websiteId[" + websiteId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestOrganisation2WebsiteLink(orgId, websiteId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateOrganisation2WebsiteLink((MDA.Common.Enum.Operation)op, link, pipeBase, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertOrganisation2WebsiteLink(orgId, websiteId, pipeBase, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Person2Passport ProcessPerson2PassportLink(CurrentContext ctx, int personId, int passportId, MDA.Pipeline.Model.PipeLinkBase pipeBase,
                                            MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPerson2PassportLink : Find Link -> personId[" + personId.ToString() + "] passportId[" + passportId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPerson2PassportLink(personId, passportId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePerson2PassportLink((MDA.Common.Enum.Operation)op, link, pipeBase, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPerson2PassportLink(personId, passportId, pipeBase, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Person2NINumber ProcessPerson2NINumberLink(CurrentContext ctx, int personId, int niNumberId, MDA.Pipeline.Model.PipeLinkBase pipeBase, MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPerson2NINumberLink : Find Link -> personId[" + personId.ToString() + "] niNumberId[" + niNumberId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPerson2NINumberLink(personId, niNumberId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePerson2NINumberLink((MDA.Common.Enum.Operation)op, link, pipeBase, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPerson2NINumberLink(personId, niNumberId, pipeBase, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Person2DrivingLicense ProcessPerson2DrivingLicenseLink(CurrentContext ctx, int personId, int licenseId, MDA.Pipeline.Model.PipeLinkBase pipeBase,
                                MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPerson2DrivingLicenseLink : Find Link -> personId[" + personId.ToString() + "] licenseId[" + licenseId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPerson2DrivingLicenseLink(personId, licenseId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePerson2DrivingLicenseLink((MDA.Common.Enum.Operation)op, link, pipeBase, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPerson2DrivingLicenseLink(personId, licenseId, pipeBase, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Person2Telephone ProcessPerson2TelephoneLink(CurrentContext ctx, int personId, int telephoneId, MDA.Pipeline.Model.PipelineTelephoneLink pipeLink,
                                    MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPerson2TelephoneLink : Find Link -> personId[" + personId.ToString() + "] telephoneId[" + telephoneId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPerson2TelephoneLink(personId, telephoneId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePerson2TelephoneLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPerson2TelephoneLink(personId, telephoneId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Organisation2Telephone ProcessOrganisation2TelephoneLink(CurrentContext ctx, int orgId, int telephoneId, MDA.Pipeline.Model.PipelineTelephoneLink pipeLink, 
                                    MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessOrganisation2TelephoneLink : Find Link -> orgId[" + orgId.ToString() + "] telephoneId[" + telephoneId.ToString() + "]");

            var link = new DataServices(ctx).GetLatestOrganisation2TelephoneLink(orgId, telephoneId, newRiskClaim.BaseRiskClaim_Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateOrganisation2TelephoneLink((MDA.Common.Enum.Operation)op, link, pipeLink, newRiskClaim, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertOrganisation2TelephoneLink(orgId, telephoneId, pipeLink, newRiskClaim);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Vehicle2Vehicle ProcessVehicle2VehicleLink(CurrentContext ctx, int vehicle1Id, int vehicle2Id, MDA.Pipeline.Model.PipelineVehicle2VehicleLink pipeLink, /*DateTime? dateOfRegChange,*/ MDA.Common.Enum.Operation op = Operation.Normal ) //, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessVehicle2VehicleLink : Find Link -> vehicle1Id[" + vehicle1Id.ToString() + "] vehicle2Id[" + vehicle2Id.ToString() + "]");

            var link = new DataServices(ctx).GetLatestVehicle2VehicleLink(vehicle1Id, vehicle2Id); 

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateVehicle2VehicleLink((MDA.Common.Enum.Operation)op, link, pipeLink, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertVehicle2VehicleLink(vehicle1Id, vehicle2Id, pipeLink); 
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Handset2Handset ProcessHandset2HandsetLink(CurrentContext ctx, int handset1Id, int handset2Id, MDA.Pipeline.Model.PipelineHandset2HandsetLink pipeLink, /*DateTime? dateOfRegChange,*/ MDA.Common.Enum.Operation op = Operation.Normal) //, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessHandset2HandsetLink : Find Link -> handset1Id[" + handset1Id.ToString() + "] handset2Id[" + handset2Id.ToString() + "]");

            var link = new DataServices(ctx).GetLatestHandset2HandsetLink(handset1Id, handset2Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateHandset2HandsetLink((MDA.Common.Enum.Operation)op, link, pipeLink, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertHandset2HandsetLink(handset1Id, handset2Id, pipeLink);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }



        protected virtual Organisation2Organisation ProcessOrganisation2OrganisationLink(CurrentContext ctx, int org1Id, int org2Id, MDA.Pipeline.Model.PipelineOrganisation2OrganisationLink pipeLink, 
                                      MDA.Common.Enum.Operation op = Operation.Normal ) 
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessOrganisation2OrganisationLink : Find Link -> org1Id[" + org1Id.ToString() + "] org2Id[" + org2Id.ToString() + "]");

            var link = new DataServices(ctx).GetLatestOrganisation2OrganisationLink(org1Id, org2Id); 

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateOrganisation2OrganisationLink((MDA.Common.Enum.Operation)op, link, pipeLink, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertOrganisation2OrganisationLink(org1Id, org2Id, pipeLink); 
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Person2Person ProcessPerson2PersonLink(CurrentContext ctx, int person1Id, int person2Id, MDA.Pipeline.Model.PipelinePerson2PersonLink pipeLink, 
                                      MDA.Common.Enum.Operation op = Operation.Normal ) 
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessPerson2PersonLink : Find Link -> person1Id[" + person1Id.ToString() + "] person2Id[" + person2Id.ToString() + "]");

            var link = new DataServices(ctx).GetLatestPerson2PersonLink(person1Id, person2Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdatePerson2PersonLink((MDA.Common.Enum.Operation)op, link, pipeLink, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertPerson2PersonLink(person1Id, person2Id, pipeLink); 
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }

        protected virtual Address2Address ProcessAddress2AddressLink(CurrentContext ctx, int addr1Id, int addr2Id, MDA.Pipeline.Model.PipelineAddressLink pipeLink, 
                                                            MDA.Common.Enum.Operation op, RiskClaim newRiskClaim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (_trace) ADATrace.WriteLine("ProcessAddress2AddressLink : Find Link -> addr1Id[" + addr1Id.ToString() + "] addr2Id[" + addr2Id.ToString() + "]");

            var link = new DataServices(ctx).GetLatestAddress2AddressLink(addr1Id, addr2Id);

            bool success = false;

            if (link != null)
            {
                if (_trace) ADATrace.WriteLine("Found :  Updating Link");
                link = new DataServices(ctx).UpdateAddress2AddressLink((MDA.Common.Enum.Operation)op, link, pipeLink, ref success);
            }
            else
                success = (op != Operation.Normal);


            if (!success)
            {
                if (_trace) ADATrace.WriteLine("Not Found :  Inserting Link");
                link = new DataServices(ctx).InsertAddress2AddressLink(addr1Id, addr2Id, pipeLink);
            }

            if (_trace)
            {
                ADATrace.WriteLine("Link Processing Complete returning Linkid[" + ((link == null) ? "null" : link.Id.ToString()) + "] [" + sw.ElapsedMilliseconds + "ms]");
                ADATrace.WriteLine("");
            }

            return link;
        }
    }
}
