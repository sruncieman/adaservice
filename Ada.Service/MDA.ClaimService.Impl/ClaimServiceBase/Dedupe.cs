﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.Common.Debug;
using MDA.Common.Server;
using MDA.DataService;


namespace MDA.ClaimService
{
    public partial class ClaimServiceBase
    {
        private readonly string[] _confidence = { "Confirmed", "Unconfirmed", "Tentative" };

        private Func<ProgressSeverity, string, bool, int> _showProgress;   // Only used by function below

        private int ShowDeDupeProgress(string s)
        {
            if (_traceDeDupe)
                ADATrace.WriteLine(s);

            return 0;
        }

        public int DedupeClaimList(CurrentContext ctx, List<int> claimList)
        {
            if (ctx == null) throw new ArgumentNullException("ctx");
            if (claimList == null || claimList.Count == 0) return 0;

            if (_traceDeDupe)
            {
                ADATrace.WriteLine("DDupe Claims Starting.");
                ADATrace.IndentLevel += 1;
            }

            var cnt = 0;

            try
            {
                foreach (var id in claimList)
                {
                    using (var ctxx = new CurrentContext(ctx))
                    {
                        _matchingService.CurrentContext = ctxx;
                        _cleansingService.CurrentContext = ctxx;
                        var dServices = new DataServices(ctxx);

                        dServices.CleanseClaimInfo(id, _cleansingService.CleanseClaimInfo);
                        // ReSharper disable once PossibleNullReferenceException
                        using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                        {
                            try
                            {
                                var matchRes = _matchingService.MatchExistingClaim(id, null);

                                // Remove THIS claim although having a status of 10 should exclude it from the matching results
                                if (matchRes != null && (matchRes.MatchIdList.Count > 0 && matchRes.MatchIdList.Contains(id)))
                                    matchRes.MatchIdList.Remove(id);                                

                                var iBaseIncidents = new List<int>();

                                if (matchRes != null)
                                {
                                    foreach (var m in matchRes.MatchIdList)
                                    {
                                        var i = ctx.db.Incidents.Where(x => x.Id == m);

                                        foreach (var incident in i)
                                            if(incident != null && incident.IncidentId != null)
                                                iBaseIncidents.Add(incident.Id);
                                    }
                                                                
                                    if (iBaseIncidents.Count > 0)
                                    {

                                        matchRes.MatchIdList = matchRes.MatchIdList.Except(iBaseIncidents).ToList(); // Remove iBase incidents as we should never dedup Incidents from iBase

                                        #region Trace
                                        ADATrace.WriteLine($"iBase Incidents Removed From Match List [{string.Join(",", iBaseIncidents)}]", _traceDeDupe);
                                        #endregion

                                    }

                                    // Check we have something to do
                                    if (matchRes.MatchIdList.Count > 0)
                                    {
                                        var incidentIdsAsCommaList = string.Join(",", matchRes.MatchIdList);

                                        #region Trace
                                        ADATrace.WriteLine("Incident [" + id + "]" + " Matches FOUND : " + _confidence[(int)matchRes.MatchTypeFound] + " Rule:" + matchRes.MatchRuleNumber, _traceDeDupe);
                                        ADATrace.WriteLine("Full Incident Match List [" + incidentIdsAsCommaList + "]", _traceDeDupe);
                                        #endregion

                                        dServices.DeduplicateIncidentList(id, matchRes.MatchIdList);

                                        cnt += matchRes.MatchIdList.Count;
                                    }
                                    else
                                        ADATrace.WriteLine("No Matches Found : [" + id + "]", _traceDeDupe);
                                }

                                dServices.ResetSyncIncidentAdaStatus(id); // set status back to 0

                                transaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();

                                throw ex;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                #region Trace
                if (_traceDeDupe)
                {
                    ADATrace.IndentLevel -= 1;
                    ADATrace.WriteLine("DeDupe Claims Failed");
                }
                ADATrace.TraceException(ex);
                #endregion
            }

            if (_traceDeDupe)
            {
                ADATrace.IndentLevel -= 1;
                ADATrace.WriteLine("DeDupe Claims Finished. Removed " + cnt + " duplicates");
                ADATrace.WriteLine("");
            }

            return cnt;
        }

        public int DedupeVehicleList(CurrentContext ctx, List<int> vehicleList)
        {
            if (ctx == null) throw new ArgumentNullException("ctx");
            if (vehicleList == null || vehicleList.Count == 0) return 0;

            if (_traceDeDupe)
            {
                ADATrace.WriteLine("DeDupe Vehicle Starting.");
                ADATrace.IndentLevel += 1;
            }

            var cnt = 0;

            try
            {
                foreach (var id in vehicleList)
                {
                    using (var ctxx = new CurrentContext(ctx))
                    {
                        _matchingService.CurrentContext = ctxx;
                        _cleansingService.CurrentContext = ctxx;
                        var dServices = new DataServices(ctxx);

                        // ReSharper disable once PossibleNullReferenceException
                        using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                        {
                            try
                            {
                                dServices.CleanseVehicle(id, _cleansingService.CleanseVehicle);

                                var matchRes = _matchingService.MatchExistingVehicle(id, null);

                                // If we found some matches remove the ID we started with
                                if (matchRes != null && (matchRes.MatchIdList.Count > 0 && matchRes.MatchIdList.Contains(id)))
                                    matchRes.MatchIdList.Remove(id);

                                // If we still have some ID's then more work to do
                                if (matchRes != null && matchRes.MatchIdList.Count > 0)
                                {
                                    var vehIdsAsCommaList = string.Join(",", matchRes.MatchIdList);

                                    var UniqIds = ctxx.db.uspUniqueVehicle(vehIdsAsCommaList, ",", (int)matchRes.MatchTypeFound).ToList();

                                    #region Trace
                                    ADATrace.WriteLine("Vehicle [" + id + "]" + " Matches FOUND : " + _confidence[(int)matchRes.MatchTypeFound] + " Rule:" + matchRes.MatchRuleNumber, _traceDeDupe);
                                    ADATrace.WriteLine("Full Vehicle Match List [" + vehIdsAsCommaList + "]", _traceDeDupe);
                                    ADATrace.WriteLine("Unique Vehicle Match List [" + string.Join(",", UniqIds) + "]", _traceDeDupe);
                                    #endregion

                                    cnt += UniqIds.Count;

                                    if (UniqIds.Count > 0)
                                    {
                                        #region Create V2V links

                                        var pipeLink = new MDA.Pipeline.Model.PipelineVehicle2VehicleLink();

                                        InitialisePipelineEntityRecord(ctxx, pipeLink);

                                        pipeLink.LinkConfidence = (int)matchRes.MatchTypeFound;

                                        foreach (var vehicleId in UniqIds)
                                            ProcessVehicle2VehicleLink(ctxx, id, (int)vehicleId, pipeLink);

                                        #endregion
                                    }
                                }
                                else
                                    ADATrace.WriteLine("No Matches Found : [" + id + "]", _traceDeDupe);

                                dServices.ResetSyncVehicleAdaStatus(id);   // Reduce status -10

                                transaction.Commit();
                            }
                            catch (Exception)
                            {
                                transaction.Rollback();

                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                #region Trace
                if (_traceDeDupe)
                {
                    ADATrace.IndentLevel -= 1;
                    ADATrace.WriteLine("DeDupe Claims Failed");
                }
                ADATrace.TraceException(ex);
                #endregion
            }

            if (_traceDeDupe)
            {
                ADATrace.IndentLevel -= 1;
                ADATrace.WriteLine("DeDupe Vehicles Finished. Removed " + cnt + " duplicates");
                ADATrace.WriteLine("");
            }

            return cnt;
        }

        public int DedupePeopleList(CurrentContext ctx, List<int> peopleList)
        {
            if (ctx == null) throw new ArgumentNullException("ctx");
            if (peopleList == null || peopleList.Count == 0) return 0;

            if (_traceDeDupe)
            {
                ADATrace.WriteLine("DeDupe People Starting.");
                ADATrace.IndentLevel += 1;
            }

            var cnt = 0;

            try
            {
                foreach (var id in peopleList)
                {
                    using (var ctxx = new CurrentContext(ctx))
                    {                        

                        _matchingService.CurrentContext = ctxx;
                        _cleansingService.CurrentContext = ctxx;
                        var dServices = new DataServices(ctxx);

                        // ReSharper disable once PossibleNullReferenceException
                        using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                        {
                            try
                            {
                                dServices.CleansePerson(id, _cleansingService.CleansePerson);

                                var matchRes = _matchingService.MatchExistingPerson(id, null);

                                if (matchRes != null && (matchRes.MatchIdList.Count > 0 && matchRes.MatchIdList.Contains(id)))
                                    matchRes.MatchIdList.Remove(id);

                                if (matchRes != null && matchRes.MatchIdList.Count > 0)
                                {
                                    matchRes.MatchIdList.Sort();

                                    var peopleIdsAsCommaList = string.Join(",", matchRes.MatchIdList);

                                    var UniqIds = ctxx.db.uspUniquePerson(peopleIdsAsCommaList, ",", (int)matchRes.MatchTypeFound).ToList();

                                    #region Trace
                                    ADATrace.WriteLine("Person [" + id + "]" + " Matches FOUND : " + _confidence[(int)matchRes.MatchTypeFound] + " Rule:" + matchRes.MatchRuleNumber, _traceDeDupe);
                                    ADATrace.WriteLine("Full People Match List [" + peopleIdsAsCommaList + "]", _traceDeDupe);
                                    ADATrace.WriteLine("Unique People Match List [" + string.Join(",", UniqIds) + "]", _traceDeDupe);
                                    #endregion

                                    cnt += UniqIds.Count;

                                    if (UniqIds.Count > 0)
                                    {
                                        #region Create P2P Links

                                        var pipeLink = new MDA.Pipeline.Model.PipelinePerson2PersonLink();

                                        InitialisePipelineEntityRecord(ctxx, pipeLink);

                                        pipeLink.LinkConfidence = (int)matchRes.MatchTypeFound;

                                        foreach (var personId in UniqIds)
                                            ProcessPerson2PersonLink(ctxx, id, (int)personId, pipeLink);

                                        #endregion
                                    }
                                }
                                else
                                    ADATrace.WriteLine("No Matches Found : [" + id + "]", _traceDeDupe);

                                dServices.ResetSyncPersonAdaStatus(id);

                                transaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();

                                throw ex;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                #region Trace
                if (_traceDeDupe)
                {
                    ADATrace.IndentLevel -= 1;
                    ADATrace.WriteLine("DeDupe People Failed");
                }
                ADATrace.TraceException(ex);
                #endregion
            }

            if (_traceDeDupe)
            {
                ADATrace.IndentLevel -= 1;
                ADATrace.WriteLine("DeDupe People Finished. Removed " + cnt + " duplicates");
                ADATrace.WriteLine("");
            }

            return cnt;
        }

        public int DedupeOrganisationList(CurrentContext ctx, List<int> orgList)
        {
            if (ctx == null) throw new ArgumentNullException("ctx");
            if (orgList == null || orgList.Count == 0) return 0;

            if (_traceDeDupe)
            {
                ADATrace.WriteLine("DeDupe Orgs Starting.");
                ADATrace.IndentLevel += 1;
            }

            var cnt = 0;

            try
            {
                foreach (var id in orgList)
                {
                    using (var ctxx = new CurrentContext(ctx))
                    {
                        _matchingService.CurrentContext = ctxx;
                        _cleansingService.CurrentContext = ctxx;
                        var dServices = new DataServices(ctxx);

                        using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                        {
                            try
                            {
                                dServices.CleanseOrganisation(id, _cleansingService.CleanseOrganisation);

                                var matchRes = _matchingService.MatchExistingOrganisation(id, null);

                                if (matchRes != null && (matchRes.MatchIdList.Count > 0 && matchRes.MatchIdList.Contains(id)))
                                    matchRes.MatchIdList.Remove(id);

                                if (matchRes != null && matchRes.MatchIdList.Count > 0)
                                {
                                    var orgIdsAsCommaList = string.Join(",", matchRes.MatchIdList);

                                    var UniqIds = ctxx.db.uspUniqueOrganisation(orgIdsAsCommaList, ",", (int)matchRes.MatchTypeFound).ToList();

                                    #region Trace
                                    ADATrace.WriteLine("Organisation [" + id + "]" + " Matches FOUND : " + _confidence[(int)matchRes.MatchTypeFound] + " Rule:" + matchRes.MatchRuleNumber, _traceDeDupe);
                                    ADATrace.WriteLine("Full Orgs Match List [" + orgIdsAsCommaList + "]", _traceDeDupe);
                                    ADATrace.WriteLine("Unique Orgs Match List [" + string.Join(",", UniqIds) + "]", _traceDeDupe);
                                    #endregion

                                    cnt += UniqIds.Count;

                                    if (UniqIds.Count > 0)
                                    {
                                        #region Create O2O Links
                                        var pipeLink = new MDA.Pipeline.Model.PipelineOrganisation2OrganisationLink();

                                        InitialisePipelineEntityRecord(ctxx, pipeLink);

                                        pipeLink.LinkConfidence = (int)matchRes.MatchTypeFound;

                                        foreach (var orgId in UniqIds)
                                            ProcessOrganisation2OrganisationLink(ctxx, id, (int)orgId, pipeLink);

                                        #endregion
                                    }
                                }
                                else
                                    ADATrace.WriteLine("No Matches Found : [" + id + "]", _traceDeDupe);

                                dServices.ResetSyncOrganisationAdaStatus(id);

                                transaction.Commit();
                            }
                            catch (Exception)
                            {
                                transaction.Rollback();

                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                #region Trace
                if (_traceDeDupe)
                {
                    ADATrace.IndentLevel -= 1;
                    ADATrace.WriteLine("DeDupe Orgs Failed");
                }
                ADATrace.TraceException(ex);
                #endregion
            }

            if (_traceDeDupe)
            {
                ADATrace.IndentLevel -= 1;
                ADATrace.WriteLine("DeDupe Orgs Finished. Removed " + cnt + " duplicates");
                ADATrace.WriteLine("");
            }

            return cnt;
        }

        public int DedupeBankAccountList(CurrentContext ctx, List<int> baList)
        {
            if (ctx == null) throw new ArgumentNullException("ctx");
            if (baList == null || baList.Count == 0) return 0;

            if (_traceDeDupe)
            {
                ADATrace.WriteLine("DeDupe Account Starting.");
                ADATrace.IndentLevel += 1;
            }

            var cnt = 0;

            try
            {
                foreach (var id in baList)
                {
                    using (var ctxx = new CurrentContext(ctx))
                    {
                        _matchingService.CurrentContext = ctxx;
                        _cleansingService.CurrentContext = ctxx;
                        var dServices = new DataServices(ctxx);
                        // ReSharper disable once PossibleNullReferenceException
                        using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                        {
                            try
                            {
                                dServices.CleanseBankAccount(id, _cleansingService.CleanseBankAccount);

                                var matchRes = _matchingService.MatchExistingBankAccount(id, null);

                                if (matchRes != null && (matchRes.MatchIdList.Count > 0 && matchRes.MatchIdList.Contains(id)))
                                    matchRes.MatchIdList.Remove(id);

                                if (matchRes != null && matchRes.MatchIdList.Count > 0)
                                {
                                    cnt += matchRes.MatchIdList.Count;

                                    var accountIdsAsCommaList = string.Join(",", matchRes.MatchIdList);

                                    #region Trace
                                    ADATrace.WriteLine("Bank Account [" + id + "]" + " Matches FOUND : " + _confidence[(int)matchRes.MatchTypeFound] + " Rule:" + matchRes.MatchRuleNumber, _traceDeDupe);
                                    ADATrace.WriteLine("Full Bank Account Match List [" + accountIdsAsCommaList + "]", _traceDeDupe);
                                    #endregion

                                    dServices.DeduplicateBankAccountList(id, matchRes.MatchIdList);
                                }
                                else
                                    ADATrace.WriteLine("No Matches Found : [" + id + "]", _traceDeDupe);

                                dServices.ResetSyncBankAccountAdaStatus(id);

                                transaction.Commit();
                            }
                            catch (Exception)
                            {
                                transaction.Rollback();

                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                #region Trace
                if (_traceDeDupe)
                {
                    ADATrace.IndentLevel -= 1;
                    ADATrace.WriteLine("DeDupe Account Failed");
                }
                ADATrace.TraceException(ex);
                #endregion
            }

            if (_traceDeDupe)
            {
                ADATrace.IndentLevel -= 1;
                ADATrace.WriteLine("DeDupe Account Finished. Removed " + cnt + " duplicates");
                ADATrace.WriteLine("");
            }

            return cnt;
        }

        public int DedupePaymentCardList(CurrentContext ctx, List<int> pcList)
        {
            if (ctx == null) throw new ArgumentNullException("ctx");
            if (pcList == null || pcList.Count == 0) return 0;

            if (_traceDeDupe)
            {
                ADATrace.WriteLine("DeDupe PCard Starting...");
                ADATrace.IndentLevel += 1;
            }

            var cnt = 0;

            try
            {
                foreach (var id in pcList)
                {
                    using (var ctxx = new CurrentContext(ctx))
                    {
                        _matchingService.CurrentContext = ctxx;
                        _cleansingService.CurrentContext = ctxx;
                        var dServices = new DataServices(ctxx);

                        // ReSharper disable once PossibleNullReferenceException
                        using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                        {
                            try
                            {
                                dServices.CleansePaymentCard(id, _cleansingService.CleansePaymentCard);

                                var matchRes = _matchingService.MatchExistingPaymentCard(id, null);

                                if (matchRes != null && (matchRes.MatchIdList.Count > 0 && matchRes.MatchIdList.Contains(id)))
                                    matchRes.MatchIdList.Remove(id);


                                if (matchRes != null && matchRes.MatchIdList.Count > 0)
                                {
                                    cnt += matchRes.MatchIdList.Count;

                                    var accountIdsAsCommaList = string.Join(",", matchRes.MatchIdList);

                                    #region Trace
                                    ADATrace.WriteLine("Payment Card [" + id + "]" + " Matches FOUND : " + _confidence[(int)matchRes.MatchTypeFound] + " Rule:" + matchRes.MatchRuleNumber, _traceDeDupe);
                                    ADATrace.WriteLine("Full Payment Card Match List [" + accountIdsAsCommaList + "]", _traceDeDupe);
                                    #endregion

                                    dServices.DeduplicatePaymentCardList(id, matchRes.MatchIdList);
                                }
                                else
                                    ADATrace.WriteLine("No Matches Found : [" + id + "]", _traceDeDupe);

                                dServices.ResetSyncPaymentCardAdaStatus(id);

                                transaction.Commit();
                            }
                            catch (Exception)
                            {
                                transaction.Rollback();

                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                #region Trace
                if (_traceDeDupe)
                {
                    ADATrace.IndentLevel -= 1;
                    ADATrace.WriteLine("DeDupe PCard Failed");
                }
                ADATrace.TraceException(ex);
                #endregion
            }

            if (_traceDeDupe)
            {
                ADATrace.IndentLevel -= 1;
                ADATrace.WriteLine("DeDupe PCard Finished. Removed " + cnt + " duplicates");
                ADATrace.WriteLine("");
            }

            return cnt;
        }

        public int DedupePolicyList(CurrentContext ctx, List<int> ppList)
        {
            if (ctx == null) throw new ArgumentNullException("ctx");
            if (ppList == null || ppList.Count == 0) return 0;

            if (_traceDeDupe)
            {
                ADATrace.WriteLine("DDupe Policy Starting...");
                ADATrace.IndentLevel += 1;
            }
            
            var cnt = 0;

            try
            {
                foreach (var id in ppList)
                {
                    using (var ctxx = new CurrentContext(ctx))
                    {
                        _matchingService.CurrentContext = ctxx;
                        _cleansingService.CurrentContext = ctxx;
                        var dServices = new DataServices(ctxx);

                        using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                        {
                            try
                            {
                                dServices.CleansePolicy(id, _cleansingService.CleansePolicy);

                                var matchRes = _matchingService.MatchExistingPolicy(id, null);

                                if (matchRes != null && (matchRes.MatchIdList.Count > 0 && matchRes.MatchIdList.Contains(id)))
                                    matchRes.MatchIdList.Remove(id);

                                if (matchRes != null && matchRes.MatchIdList.Count > 0)
                                {
                                    cnt += matchRes.MatchIdList.Count;

                                    var policyIdsAsCommaList = string.Join(",", matchRes.MatchIdList);

                                    #region Trace
                                    ADATrace.WriteLine("Policy [" + id + "]" + " Matches FOUND : " + _confidence[(int)matchRes.MatchTypeFound] + " Rule:" + matchRes.MatchRuleNumber, _traceDeDupe);
                                    ADATrace.WriteLine("Full Policy Match List [" + policyIdsAsCommaList + "]", _traceDeDupe);
                                    #endregion

                                    dServices.DeduplicatePolicyList(id, matchRes.MatchIdList);
                                }
                                else
                                    ADATrace.WriteLine("No Matches Found : [" + id + "]", _traceDeDupe);

                                dServices.ResetSyncPolicyAdaStatus(id);

                                transaction.Commit();
                            }
                            catch (Exception)
                            {
                                transaction.Rollback();

                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                #region Trace
                if (_traceDeDupe)
                {
                    ADATrace.IndentLevel -= 1;
                    ADATrace.WriteLine("DeDupe Policy List Failed");
                }
                ADATrace.TraceException(ex);
                #endregion
            }

            if (_traceDeDupe)
            {
                ADATrace.IndentLevel -= 1;
                ADATrace.WriteLine("DeDupe Policy List Finished. Removed " + cnt + " duplicates");
                ADATrace.WriteLine("");
            }

            return cnt;
        }

        public int DedupeAddressList(CurrentContext ctx, List<int> addrList)
        {
            if (ctx == null) throw new ArgumentNullException("ctx");
            if (addrList == null || addrList.Count == 0) return 0;

            if (_traceDeDupe)
            {
                ADATrace.WriteLine("DeDupe Address Starting...");
                ADATrace.IndentLevel += 1;
            }
            
            var cnt = 0;

            try
            {
                foreach (var id in addrList)
                {
                    using (var ctxx = new CurrentContext(ctx))
                    {
                        _matchingService.CurrentContext = ctxx;
                        _cleansingService.CurrentContext = ctxx;
                        var dServices = new DataServices(ctxx);

                        using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                        {
                            try
                            {
                                //dServices.CleansePolicy(id, _cleansingService.CleansePolicy);

                                var matchRes = _matchingService.MatchExistingAddress(id, null);

                                // Bug 8583 Addresses should only be deduped for confirmed matches
                                if (matchRes != null && matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                                {
                                    if (matchRes.MatchIdList.Count > 0 && matchRes.MatchIdList.Contains(id))
                                        matchRes.MatchIdList.Remove(id);

                                    if (matchRes.MatchIdList.Count > 0)
                                    {
                                        cnt += matchRes.MatchIdList.Count;

                                        var addrIdsAsCommaList = string.Join(",", matchRes.MatchIdList);

                                        #region Trace
                                        ADATrace.WriteLine("Address [" + id + "]" + " Matches FOUND : " + _confidence[(int)matchRes.MatchTypeFound] + " Rule:" + matchRes.MatchRuleNumber, _traceDeDupe);
                                        ADATrace.WriteLine("Full Address Match List [" + addrIdsAsCommaList + "]", _traceDeDupe);
                                        #endregion

                                        dServices.DeduplicateAddressList(id, matchRes.MatchIdList);
                                    }
                                    else
                                        ADATrace.WriteLine("No Matches Found : [" + id + "]", _traceDeDupe);
                                }

                                dServices.ResetSyncAddressAdaStatus(id);

                                transaction.Commit();
                            }
                            catch (Exception)
                            {
                                transaction.Rollback();

                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                #region Trace
                if (_traceDeDupe)
                {
                    ADATrace.IndentLevel -= 1;
                    ADATrace.WriteLine("DeDupe Address List Failed");
                }
                ADATrace.TraceException(ex);
                #endregion
            }

            if (_traceDeDupe)
            {
                ADATrace.IndentLevel -= 1;
                ADATrace.WriteLine("DeDupe Address List Finished. Removed " + cnt + " duplicates");
                ADATrace.WriteLine("");
            }

            return cnt;
        }

        public int DedupeAll(CurrentContext ctx, string entityName, Func<CurrentContext, Func<string, int>, int> DeDupeFn)
        {
            var cnt = 0;

            if (_traceDeDupe)
            {
                ADATrace.WriteLine("DeDupe " + entityName + " Starting.");
                ADATrace.IndentLevel += 1;
            }

            try
            {
                cnt = DeDupeFn(ctx, ShowDeDupeProgress);
            }
            catch (Exception ex)
            {
                #region Trace
                if (_traceDeDupe)
                {
                    ADATrace.IndentLevel -= 1;
                    ADATrace.WriteLine("DeDupe " + entityName + " Failed");
                }
                ADATrace.TraceException(ex);
                #endregion
            }

            if (_traceDeDupe)
            {
                ADATrace.IndentLevel -= 1;
                ADATrace.WriteLine("DeDupe " + entityName + " Finished. Removed " + cnt + " duplicates");
                ADATrace.WriteLine("");
            }

            return cnt;
        }

        public void Deduplicate(CurrentContext ctx)
        {
            _showProgress = ctx.ShowProgress;

            #region Trace
            ADATrace.WriteLine("DeDupe Starting, fetching lists.", _traceDeDupe);
            #endregion

            var dataServices = new DataServices(ctx);

            CreatedDate = DateTime.Now;

            var cnt = 0;

            cnt += DedupeAll(ctx, "Address (UPRN)", dataServices.DeduplicateUPRNAddresses); // This dataservice method copies the CTX and creates its own transaction

            cnt += DedupeClaimList(ctx, dataServices.GetAllSyncIncidentIds());
            cnt += DedupeVehicleList(ctx, dataServices.GetAllSyncVehicleIds());
            cnt += DedupePeopleList(ctx, dataServices.GetAllSyncPeopleIds());
            cnt += DedupeOrganisationList(ctx, dataServices.GetAllSyncOrganisationIds());
            cnt += DedupeBankAccountList(ctx, dataServices.GetAllSyncBankAccountIds());
            cnt += DedupePaymentCardList(ctx, dataServices.GetAllSyncPaymentCardIds());
            cnt += DedupePolicyList(ctx, dataServices.GetAllSyncPolicyIds());
            cnt += DedupeAddressList(ctx, dataServices.GetAllSyncAddressIds());

            // All these dataservices methods copy the CTX and create their own transaction
            cnt += DedupeAll(ctx, "Email", dataServices.DeduplicateEmails);
            cnt += DedupeAll(ctx, "License", dataServices.DeduplicateDrivingLicense);
            cnt += DedupeAll(ctx, "NI Numbers", dataServices.DeduplicateNINumbers);
            cnt += DedupeAll(ctx, "Passports", dataServices.DeduplicatePassports);
            cnt += DedupeAll(ctx, "Telephones", dataServices.DeduplicateTelephones);
            cnt += DedupeAll(ctx, "WebSites", dataServices.DeduplicateWebSites);

            #region Trace
            if (_traceDeDupe)
            {
                if ( cnt == 0 )
                    ADATrace.WriteLine("Nothing to dedupe");
                
                ADATrace.WriteLine("Reset all Record status values Starting");
                ADATrace.IndentLevel += 1;
            }
            #endregion

            using (var ctxx = new CurrentContext(ctx))
            {
                var dServices = new DataServices(ctxx);
                using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        dServices.ResetAllAdaRecordStatus();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();

                        #region Trace
                        if (_traceDeDupe)
                        {
                            ADATrace.IndentLevel -= 1;
                            ADATrace.WriteLine("ResetAllEntityAdaRecordStatus Failed");
                        }
                        #endregion

                        throw;
                    }
                }
            }

            #region Trace
            if (_traceDeDupe)
            {
                ADATrace.IndentLevel -= 1;
                ADATrace.WriteLine("");

                ADATrace.WriteLine("Reset all Record status values Complete");
            }
            #endregion

        }
    }
}