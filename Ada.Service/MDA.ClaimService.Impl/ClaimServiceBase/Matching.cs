﻿using System;
using System.Collections.Generic;
using System.Text;
using MDA.Common.Debug;
using MDA.DataService;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Enum;
using System.Data.Entity.Validation;
using System.Diagnostics;
using MDA.MatchingService.Model;
using System.Configuration;

namespace MDA.ClaimService
{
    /// <summary>
    /// Class that implements method to process each Entity Type. Calls Matching and then processes the return type. 
    /// All methods are protected virtual so can be overridden if client has special processing
    /// </summary>
    public partial class ClaimServiceBase
    {
        protected virtual WebSite ProcessOrgWebsiteAddressMatch(CurrentContext ctx, Operation op, int orgId, MDA.Pipeline.Model.PipelineWebSite pipeWebsite, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeWebsite.ShouldBeDiscarded) return null;

            WebSite dalWebsite = null;
                           
            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                DataServices dataServices = new DataServices(ctx);

                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessOrgWebsiteAddressMatch : [" + pipeWebsite.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }
 
                var matchRes = _matchingService.MatchWebSite(pipeWebsite, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "Website");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeWebsite = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineWebSite;

                

                #region Process Match record
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;

                        bool ok;

                        // Yes, so update any existing empty fields
                        dalWebsite = dataServices.UpdateWebSite((int)matchRes.MatchIdList[0], pipeWebsite, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record and add a confirmed link
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalWebsite = dataServices.InsertWebSite(pipeWebsite);

                            if (_trace) ADATrace.WriteLine("Inserted new WebSite [" + dalWebsite.Id + "]");
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new website
                        dalWebsite = dataServices.InsertWebSite(pipeWebsite);

                        if (_trace) ADATrace.WriteLine("Inserted new WebSite [" + dalWebsite.Id + "]");

                        // Link this Org with any partial website matches we got returned
                        foreach (var websiteId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeWebsite.O2Ws_LinkData.LinkConfidence;

                            pipeWebsite.O2Ws_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) 
                                                     ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessOrganisation2WebsiteLink(ctx, orgId, websiteId, pipeWebsite.O2Ws_LinkData, op, newRiskClaim);

                            pipeWebsite.O2Ws_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalWebsite = dataServices.SelectWebSite((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessOrgWebsiteAddressMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessOrgWebsiteAddressMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessOrgWebsiteAddressMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalWebsite.Id + "]");
                    ADATrace.WriteLine("");
                } 
            }
            return dalWebsite;
        }

        protected virtual Incident ProcessClaimMatch(CurrentContext ctx, Operation op, MDA.Pipeline.Model.IPipelineClaim pipeClaim, RiskClaim newRiskClaim, int? existingIncidentId, int? existingBaseRiskClaimId)
        {
            if (pipeClaim.ShouldBeDiscarded) return null;

            Incident dalIncident = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessClaimMatch: [" + pipeClaim.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                MatchResults matchRes = null;

                if (pipeClaim is MDA.Pipeline.Model.PipelineMotorClaim)
                {
                    matchRes = _matchingService.MatchClaim((MDA.Pipeline.Model.PipelineMotorClaim)pipeClaim, existingIncidentId, existingBaseRiskClaimId);
                }
                else if (pipeClaim is MDA.Pipeline.Model.PipelinePIClaim)
                {
                    matchRes = _matchingService.MatchClaim((MDA.Pipeline.Model.PipelinePIClaim)pipeClaim, existingIncidentId, existingBaseRiskClaimId);
                }
                else
                {
                    matchRes = _matchingService.MatchClaim((MDA.Pipeline.Model.PipelineMobileClaim)pipeClaim, existingIncidentId, existingBaseRiskClaimId);
                }
                    if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                    if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "Claim");
                   

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes); 
                pipeClaim = matchRes.ModifiedEntity as MDA.Pipeline.Model.IPipelineClaim;

                if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                {
                    if (existingIncidentId != null && existingBaseRiskClaimId != null && !matchRes.MatchIdList.Contains((int)existingIncidentId))
                    {
                        if (_trace) ADATrace.WriteLine("Safely Remove Incident [" + existingIncidentId.ToString() + "]" );

                        // We have previously created an incident but have now found a better match
                        dataServices.SafelyRemoveIncident((int)existingIncidentId, (int)existingBaseRiskClaimId);
                    }
                }

                #region Process Match
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalIncident = dataServices.UpdateIncident((int)matchRes.MatchIdList[0], pipeClaim, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record and add a confirmed link
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new : NOT INSERTED (disabled)");
                            // Assumes that always OK, We can always overwrite fields successfully
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new incident
                        dalIncident = dataServices.InsertIncident(pipeClaim);

                        if (_trace) ADATrace.WriteLine("Inserted new Incident [" + dalIncident.Id + "]");
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalIncident = dataServices.SelectIncident((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }
                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessClaimMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessClaimMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessClaimMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    if (dalIncident != null)
                    {
                        ADATrace.WriteLine("Returning ID[" + dalIncident.Id + "]");
                    }
                    ADATrace.WriteLine("");
                }
            }

            return dalIncident;

         }

        

        protected virtual Policy ProcessPolicyMatch(CurrentContext ctx, Operation op, MDA.Pipeline.Model.PipelinePolicy pipePolicy, int incidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipePolicy.ShouldBeDiscarded) return null;

            Policy dalPolicy = null;
            DataServices dataServices = new DataServices(ctx);


            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessPolicyMatch");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }


                var matchRes = _matchingService.MatchPolicy(pipePolicy, incidentId, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "Policy");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipePolicy = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelinePolicy;

                #region Process Match
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalPolicy = dataServices.UpdatePolicy((int)matchRes.MatchIdList[0], pipePolicy, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalPolicy = dataServices.InsertPolicy(pipePolicy);

                            if (_trace) ADATrace.WriteLine("Inserted new Policy [" + dalPolicy.Id + "]");
                        }

                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new policy
                        dalPolicy = dataServices.InsertPolicy(pipePolicy);

                        if (_trace) ADATrace.WriteLine("Inserted new Policy [" + dalPolicy.Id + "]");
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalPolicy = dataServices.SelectPolicy((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessPolicyMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessPolicyMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessPolicyMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalPolicy.Id + "]"); 
                    ADATrace.WriteLine("");
                }
            }

            return dalPolicy;

        }

        protected virtual Address ProcessPersonAddressMatch(CurrentContext ctx, Operation op, int personId, MDA.Pipeline.Model.PipelineAddress pipeAddress, int incidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            Address dalAddress = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessPersonAddressMatch: [" + pipeAddress.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }


                var matchRes = _matchingService.MatchAddress(pipeAddress, null, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "PersonAddress");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeAddress = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineAddress;

                //if (matchRes.PAFAddress)
                //{
                //    if (pipeAddress != null) pipeAddress.PafValidation = (matchRes.PAFAddress) ? 1 : 0;
                //}

                #region Process Match record

                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;
                        string error;
                        // Yes, so update any existing empty fields
                        dalAddress = dataServices.UpdateAddress((int)matchRes.MatchIdList[0], pipeAddress, out ok, out error);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                             
                            if (_trace) ADATrace.WriteLine("UPDATE Failed [" + error + "] : Insert new");

                            //xmlAddress.AddressType_Id = (int)addressType;
                            if (!dataServices.AddressEmpty(pipeAddress))
                                dalAddress = dataServices.InsertAddress(pipeAddress);
                            else
                                return dalAddress;


                                if (_trace) ADATrace.WriteLine("Inserted new Address [" + dalAddress.Id + "]");
                        }

                        if (matchRes.MatchIdList.Count > 1)
                        {
                            if (_trace) ADATrace.WriteLine("Found Multi-MATCH");

                            int cnt = 0;
                            // ADDED to link this person to all the confirmed addresses : 3/7/2014
                            foreach (var addrId in matchRes.MatchIdList)
                            {
                                if (cnt++ == 0) continue;  // a link to this will be created by the calling function

                                if (_trace) ADATrace.WriteLine("Adding additional Confirmed Link to [" + addrId.ToString() + "]");

                                pipeAddress.PO2A_LinkData.LinkConfidence = (int)MatchingService.Model.MatchType.Confirmed;

                                // Make these links have ClaimId's of zero. So save value, swap to zero and then write back in
                                //var s1 = newRiskClaim.Id;
                                //var s2 = newRiskClaim.BaseRiskClaim_Id;

                                //newRiskClaim.Id = 0;
                                //newRiskClaim.BaseRiskClaim_Id = 0;

                                ProcessPerson2AddressLink(ctx, personId, addrId, pipeAddress.PO2A_LinkData, op, newRiskClaim);

                                //newRiskClaim.Id = s1;
                                //newRiskClaim.BaseRiskClaim_Id = s2;
                            }
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }

                        // No, so insert a new address
                        if (!dataServices.AddressEmpty(pipeAddress))
                            dalAddress = dataServices.InsertAddress(pipeAddress);
                        else
                            return dalAddress;

                        if (_trace) ADATrace.WriteLine("Inserted new Address [" + dalAddress.Id + "]");

                        // Link this Person with any partial matches we got returned
                        foreach (var addrId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeAddress.PO2A_LinkData.LinkConfidence;

                            pipeAddress.PO2A_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                    ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            // Make these links have ClaimId's of zero. So save value, swap to zero and then write back in
                            //var s1 = newRiskClaim.Id;
                            //var s2 = newRiskClaim.BaseRiskClaim_Id;

                            //newRiskClaim.Id = 0;
                            //newRiskClaim.BaseRiskClaim_Id = 0;

                            ProcessPerson2AddressLink(ctx, personId, addrId, pipeAddress.PO2A_LinkData, op, newRiskClaim);

                            //newRiskClaim.Id = s1;
                            //newRiskClaim.BaseRiskClaim_Id = s2;

                            pipeAddress.PO2A_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalAddress = dataServices.SelectAddress((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessPersonAddressMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessPersonAddressMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessPersonAddressMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    if (dalAddress != null)
                    {
                        ADATrace.WriteLine("Returning ID[" + dalAddress.Id + "]");
                    }
                    ADATrace.WriteLine("");
                }
            }
            return dalAddress;
        }

        protected virtual Address ProcessOrgAddressMatch(CurrentContext ctx, Operation op, int orgId, MDA.Pipeline.Model.PipelineAddress pipeAddress, string orgName, int incidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeAddress.ShouldBeDiscarded) return null;

            Address dalAddress = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessOrgAddressMatch: [" + pipeAddress.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }


                var matchRes = _matchingService.MatchAddress(pipeAddress, orgName, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "OrgAddress");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeAddress = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineAddress;

                //if (matchRes.PAFAddress)
                //{
                //if (pipeAddress != null) pipeAddress.PafValidation = (matchRes.PAFAddress) ? 1 : 0;
                //}

                #region Process Match record
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;
                        string error;
                        // Yes, so update any existing empty fields
                        dalAddress = dataServices.UpdateAddress((int)matchRes.MatchIdList[0], pipeAddress, out ok, out error);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed [" + error + "] : Insert new");

                            if (!dataServices.AddressEmpty(pipeAddress))
                                dalAddress = dataServices.InsertAddress(pipeAddress);
                            else
                                return dalAddress;

                            if (_trace) ADATrace.WriteLine("Inserted new Address [" + dalAddress.Id + "]");
                        }

                        if (matchRes.MatchIdList.Count > 1)
                        {
                            if (_trace) ADATrace.WriteLine("Found Multi-MATCH");

                            int cnt = 0;
                            // ADDED to link this person to all the confirmed addresses : 3/7/2014
                            foreach (var addrId in matchRes.MatchIdList)
                            {
                                if (cnt++ == 0) continue;  // a link to this will be created by the calling function

                                if (_trace) ADATrace.WriteLine("Adding additional Confirmed Link to [" + addrId.ToString() + "]");

                                pipeAddress.PO2A_LinkData.LinkConfidence = (int)MatchingService.Model.MatchType.Confirmed;

                                // Make these links have ClaimId's of zero. So save value, swap to zero and then write back in
                                //var s1 = newRiskClaim.Id;
                                //var s2 = newRiskClaim.BaseRiskClaim_Id;

                                //newRiskClaim.Id = 0;
                                //newRiskClaim.BaseRiskClaim_Id = 0;

                                //ProcessPerson2AddressLink(ctx, orgId, addrId, pipeAddress.PO2A_LinkData, op, newRiskClaim);

                                ProcessOrganisation2AddressLink(ctx, orgId, addrId, pipeAddress.PO2A_LinkData, op, newRiskClaim);


                                //newRiskClaim.Id = s1;
                                //newRiskClaim.BaseRiskClaim_Id = s2;
                            }
                        }

                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new address
                        if (!dataServices.AddressEmpty(pipeAddress))
                            dalAddress = dataServices.InsertAddress(pipeAddress);
                        else
                            return dalAddress;

                        if (_trace) ADATrace.WriteLine("Inserted new Address [" + dalAddress.Id + "]");

                        // Link this Org with any partial matches we got returned
                        foreach (var addrId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeAddress.PO2A_LinkData.LinkConfidence;

                            pipeAddress.PO2A_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                    ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            // Make these links have ClaimId's of zero. So save value, swap to zero and then write back in
                            //var s1 = newRiskClaim.Id;
                            //var s2 = newRiskClaim.BaseRiskClaim_Id;

                            //newRiskClaim.Id = 0;
                            //newRiskClaim.BaseRiskClaim_Id = 0;

                            ProcessOrganisation2AddressLink(ctx, orgId, addrId, pipeAddress.PO2A_LinkData, op, newRiskClaim);

                            //newRiskClaim.Id = s1;
                            //newRiskClaim.BaseRiskClaim_Id = s2;

                            pipeAddress.PO2A_LinkData.LinkConfidence = saveLc;
                        }

                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");
                    
                    dalAddress = dataServices.SelectAddress((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessOrgAddressMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessOrgAddressMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessOrgAddressMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalAddress.Id + "]");
                    ADATrace.WriteLine("");
                }
            }

            return dalAddress;

         }

        protected virtual BankAccount ProcessPersonBankAccountMatch(CurrentContext ctx, Operation op, int personId, MDA.Pipeline.Model.PipelineBankAccount pipeAccount, MDA.Pipeline.Model.IPipelineClaim pipeClaim, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeAccount.ShouldBeDiscarded) return null;

            BankAccount dalBankAccount = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessPersonBankAccountMatch: [" + pipeAccount.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }


                var matchRes = _matchingService.MatchBankAccount(pipeAccount, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "PersonBankAccount");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeAccount = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineBankAccount;

                #region Process Match record

                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalBankAccount = dataServices.UpdateBankAccount((int)matchRes.MatchIdList[0], pipeAccount, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalBankAccount = dataServices.InsertBankAccount(pipeAccount);

                            if (_trace) ADATrace.WriteLine("Inserted new Bank Account [" + dalBankAccount.Id + "]");
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new bank account
                        dalBankAccount = dataServices.InsertBankAccount(pipeAccount);

                        if (_trace) ADATrace.WriteLine("Inserted new Bank Account [" + dalBankAccount.Id + "]");

                        // Link this with any partial matches we got returned
                        foreach (var accId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeAccount.PO2Ba_LinkData.LinkConfidence;

                            pipeAccount.PO2Ba_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                    ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessPerson2BankAccountLink(ctx, personId, accId, pipeAccount.PO2Ba_LinkData, op, 
                                                            newRiskClaim);

                            pipeAccount.PO2Ba_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalBankAccount = dataServices.GetBankAccount((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessPersonBankAccountMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessPersonBankAccountMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessPersonBankAccountMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalBankAccount.Id + "]");
                    ADATrace.WriteLine("");
                }
            }

            return dalBankAccount;

        }


        protected virtual BankAccount ProcessOrgBankAccountMatch(CurrentContext ctx, Operation op, int orgId, MDA.Pipeline.Model.PipelineBankAccount pipeAccount, MDA.Pipeline.Model.IPipelineClaim pipeClaim, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeAccount.ShouldBeDiscarded) return null;

            BankAccount dalBankAccount = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessOrgBankAccountMatch: [" + pipeAccount.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }


                var matchRes = _matchingService.MatchBankAccount(pipeAccount, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "OrgBankAccount");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeAccount = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineBankAccount;

                #region Process Match record

                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalBankAccount = dataServices.UpdateBankAccount((int)matchRes.MatchIdList[0], pipeAccount, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalBankAccount = dataServices.InsertBankAccount(pipeAccount);

                            if (_trace) ADATrace.WriteLine("Inserted new Bank Account [" + dalBankAccount.Id + "]");
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        
                        // No, so insert a new bank account
                        dalBankAccount = dataServices.InsertBankAccount(pipeAccount);

                        if (_trace) ADATrace.WriteLine("Inserted new Bank Account [" + dalBankAccount.Id + "]");

                        // Link this Org with any partial matches we got returned
                        foreach (var accId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeAccount.PO2Ba_LinkData.LinkConfidence;

                            pipeAccount.PO2Ba_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                            ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessOrganisation2BankAccountLink(ctx, orgId, accId, pipeAccount.PO2Ba_LinkData, op, 
                                                                newRiskClaim);

                            pipeAccount.PO2Ba_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalBankAccount = dataServices.GetBankAccount((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessOrgBankAccountMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessOrgBankAccountMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessOrgBankAccountMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalBankAccount.Id + "]");
                    ADATrace.WriteLine("");
                }
            }

            return dalBankAccount;

        }

        protected virtual PaymentCard ProcessPersonPaymentCardMatch(CurrentContext ctx, Operation op, int personId, MDA.Pipeline.Model.PipelinePaymentCard pipeCard, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeCard.ShouldBeDiscarded) return null;

            PaymentCard dalPaymentCard = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessPersonPaymentCardMatch: [" + pipeCard.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                var matchRes = _matchingService.MatchPaymentCard(pipeCard, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "PersonPaymentCard");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeCard = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelinePaymentCard;

                #region Process Match record
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalPaymentCard = dataServices.UpdatePaymentCard((int)matchRes.MatchIdList[0], pipeCard, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalPaymentCard = dataServices.InsertPaymentCard(pipeCard);

                            if (_trace) ADATrace.WriteLine("Inserted new Payment Card [" + dalPaymentCard.Id + "]");
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new payment card
                        dalPaymentCard = dataServices.InsertPaymentCard(pipeCard);

                        if (_trace) ADATrace.WriteLine("Inserted new Payment Card [" + dalPaymentCard.Id + "]");

                        // Link this Person with any partial matches we got returned
                        foreach (var cId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeCard.PO2Pc_LinkData.LinkConfidence;

                            pipeCard.PO2Pc_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                    ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessPerson2PaymentCardLink(ctx, personId, cId, pipeCard.PO2Pc_LinkData, op, newRiskClaim);

                            pipeCard.PO2Pc_LinkData.LinkConfidence = saveLc;
                        }

                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalPaymentCard = dataServices.SelectPaymentCard((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessPersonPaymentCardMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessPersonPaymentCardMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessPersonPaymentCardMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalPaymentCard.Id + "]");
                    ADATrace.WriteLine("");
                }
            }

            return dalPaymentCard;
        }

        protected virtual PaymentCard ProcessOrgPaymentCardMatch(CurrentContext ctx, Operation op, int orgId, MDA.Pipeline.Model.PipelinePaymentCard pipeCard, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeCard.ShouldBeDiscarded) return null;

            PaymentCard dalPaymentCard = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessOrgPaymentCardMatch: [" + pipeCard.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                var matchRes = _matchingService.MatchPaymentCard(pipeCard, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "OrgPaymentCard");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeCard = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelinePaymentCard;

                #region Process Match record
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalPaymentCard = dataServices.UpdatePaymentCard((int)matchRes.MatchIdList[0], pipeCard, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalPaymentCard = dataServices.InsertPaymentCard(pipeCard);

                            if (_trace) ADATrace.WriteLine("Inserted new Payment Card [" + dalPaymentCard.Id + "]");
                        }

                        //if (dalPaymentCard != null)
                        //    cardId = dalPaymentCard.Id;
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new payment card
                        dalPaymentCard = dataServices.InsertPaymentCard(pipeCard);

                        if (_trace) ADATrace.WriteLine("Inserted new Payment Card [" + dalPaymentCard.Id + "]");

                        // Link this Org with any partial matches we got returned
                        foreach (var cId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeCard.PO2Pc_LinkData.LinkConfidence;

                            pipeCard.PO2Pc_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessOrganisation2PaymentCardLink(ctx, orgId, cId, pipeCard.PO2Pc_LinkData, op, 
                                                                newRiskClaim);

                            pipeCard.PO2Pc_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalPaymentCard = dataServices.SelectPaymentCard((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessOrgPaymentCardMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessOrgPaymentCardMatch : Match exception", ex);
            }
            finally
            {

                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessOrgPaymentCardMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalPaymentCard.Id + "]");
                    ADATrace.WriteLine("");
                }
            }

            return dalPaymentCard;

        }

        protected virtual Email ProcessPersonEmailAddressMatch(CurrentContext ctx, Operation op, int personId, MDA.Pipeline.Model.PipelineEmail pipeEmail, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeEmail.ShouldBeDiscarded) return null;

            Email dalEmail = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessPersonEmailAddressMatch: [" + pipeEmail.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                var matchRes = _matchingService.MatchEmailAddress(pipeEmail, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "PersonEmailAddress");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeEmail = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineEmail;

                #region Process Match record

                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalEmail = dataServices.UpdateEmail((int)matchRes.MatchIdList[0], pipeEmail, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record and add a confirmed link
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalEmail = dataServices.InsertEmail(pipeEmail);

                            if (_trace) ADATrace.WriteLine("Inserted new Email [" + dalEmail.Id + "]");
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }

                        // No, so insert a new record
                        dalEmail = dataServices.InsertEmail(pipeEmail);

                        if (_trace) ADATrace.WriteLine("Inserted new Email [" + dalEmail.Id + "]");

                        // Link this Person with any partial matches we got returned
                        foreach (var emId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeEmail.PO2E_LinkData.LinkConfidence;

                            pipeEmail.PO2E_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                 ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessPerson2EmailLink(ctx, personId, emId, pipeEmail.PO2E_LinkData, op, newRiskClaim);

                            pipeEmail.PO2E_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalEmail = dataServices.SelectEmail((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessPersonEmailAddressMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessPersonEmailAddressMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessPersonEmailAddressMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalEmail.Id + "]");
                    ADATrace.WriteLine("");
                }
            }
            return dalEmail;

        }

        protected virtual Email ProcessOrgEmailAddressMatch(CurrentContext ctx, Operation op, int orgId, MDA.Pipeline.Model.PipelineEmail pipeEmail, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeEmail.ShouldBeDiscarded) return null;

            Email dalEmail = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessOrgEmailAddressMatch: [" + pipeEmail.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                var matchRes = _matchingService.MatchEmailAddress(pipeEmail, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "OrgEmailAddress");
                
                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeEmail = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineEmail;

                #region Process Match record
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalEmail = dataServices.UpdateEmail((int)matchRes.MatchIdList[0], pipeEmail, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record and add a confirmed link
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalEmail = dataServices.InsertEmail(pipeEmail);

                            if (_trace) ADATrace.WriteLine("Inserted new Email [" + dalEmail.Id + "]");
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        
                        // No, so insert a new payment card
                        dalEmail = dataServices.InsertEmail(pipeEmail);

                        if (_trace) ADATrace.WriteLine("Inserted new Email [" + dalEmail.Id + "]");


                        // Link this Org with any partial Email matches we got returned
                        foreach (var eId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeEmail.PO2E_LinkData.LinkConfidence;

                            pipeEmail.PO2E_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                 ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessOrganisation2EmailLink(ctx, orgId, eId, pipeEmail.PO2E_LinkData, op, newRiskClaim);

                            pipeEmail.PO2E_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalEmail = dataServices.SelectEmail((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessOrgEmailAddressMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessOrgEmailAddressMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessOrgEmailAddressMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalEmail.Id + "]");
                    ADATrace.WriteLine("");
                }
            }

            return dalEmail;
        }

        protected virtual Passport ProcessPassportMatch(CurrentContext ctx, Operation op, int personId, MDA.Pipeline.Model.PipelinePassport pipePassport, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipePassport.ShouldBeDiscarded) return null;

            Passport dalPassport = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessPassportMatch: [" + pipePassport.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                var matchRes = _matchingService.MatchPassportNumber(pipePassport, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "Passport");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipePassport = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelinePassport;

                #region Process Match record

                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalPassport = dataServices.UpdatePassport((int)matchRes.MatchIdList[0], pipePassport, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalPassport = dataServices.InsertPassport(pipePassport);

                            if (_trace) ADATrace.WriteLine("Insert new Passport [" + dalPassport.Id + "]");
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        
                        // No, so insert a new record
                        dalPassport = dataServices.InsertPassport(pipePassport);

                        if (_trace) ADATrace.WriteLine("Insert new Passport [" + dalPassport.Id + "]");

                        // Link this with any partial matches we got returned
                        foreach (var pId in matchRes.MatchIdList)
                        {
                            int saveLc = pipePassport.Pe2Pa_LinkData.LinkConfidence;

                            pipePassport.Pe2Pa_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                 ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessPerson2PassportLink(ctx, personId, pId, pipePassport.Pe2Pa_LinkData, op, newRiskClaim);

                            pipePassport.Pe2Pa_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalPassport = dataServices.SelectPassport((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessPassportMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessPassportMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessPassportMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalPassport.Id + "]");
                    ADATrace.WriteLine("");
                }
            }

            return dalPassport;
        }

        protected virtual NINumber ProcessNINumberMatch(CurrentContext ctx, Operation op, int personId, MDA.Pipeline.Model.PipelineNINumber pipeNiNumber, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeNiNumber.ShouldBeDiscarded) return null;

            NINumber dalNINumber = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessNINumberMatch: [" + pipeNiNumber.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }


                var matchRes = _matchingService.MatchNINumber(pipeNiNumber, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "NINumber");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeNiNumber = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineNINumber;

                #region Process Match record
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalNINumber = dataServices.UpdateNINumber((int)matchRes.MatchIdList[0], pipeNiNumber, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record and add a confirmed link
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalNINumber = dataServices.InsertNINumber(pipeNiNumber);

                            if (_trace) ADATrace.WriteLine("Inserted new NINumber [" + dalNINumber.Id + "]");
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new payment card
                        dalNINumber = dataServices.InsertNINumber(pipeNiNumber);

                        if (_trace) ADATrace.WriteLine("Inserted new NINumber [" + dalNINumber.Id + "]");

                        // Link this with any partial matches we got returned
                        foreach (var NIId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeNiNumber.Pe2Ni_LinkData.LinkConfidence;

                            pipeNiNumber.Pe2Ni_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                             ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessPerson2NINumberLink(ctx, personId, NIId, pipeNiNumber.Pe2Ni_LinkData, op, newRiskClaim);

                            pipeNiNumber.Pe2Ni_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalNINumber = dataServices.SelectNINumber((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessNINumberMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessNINumberMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessNINumberMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalNINumber.Id + "]");
                    ADATrace.WriteLine("");
                }
            }
            return dalNINumber;
        }

        protected virtual DrivingLicense ProcessLicenseMatch(CurrentContext ctx, Operation op, int personId, MDA.Pipeline.Model.PipelineDrivingLicense pipeLicense, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeLicense.ShouldBeDiscarded) return null;

            DrivingLicense dalLicense = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessLicenseMatch: [" + pipeLicense.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }


                var matchRes = _matchingService.MatchLicenseNumber(pipeLicense, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "DrivingLicence");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeLicense = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineDrivingLicense;

                #region Process Match record

                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalLicense = dataServices.UpdateDrivingLicenseNumber((int)matchRes.MatchIdList[0], pipeLicense, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalLicense = dataServices.InsertDrivingLicenseNumber(pipeLicense);

                            if (_trace) ADATrace.WriteLine("Inserted new License [" + dalLicense.Id + "]");
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new record
                        dalLicense = dataServices.InsertDrivingLicenseNumber(pipeLicense);

                        if (_trace) ADATrace.WriteLine("Inserted new License [" + dalLicense.Id + "]");


                        // Link this Person with any partial NINumber matches we got returned
                        foreach (var LicenseId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeLicense.Pe2Dl_LinkData.LinkConfidence;

                            pipeLicense.Pe2Dl_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                 ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessPerson2DrivingLicenseLink(ctx, personId, LicenseId, pipeLicense.Pe2Dl_LinkData, op, newRiskClaim);

                            pipeLicense.Pe2Dl_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalLicense = dataServices.SelectDrivingLicense((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessLicenseMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessLicenseMatch : Match exception", ex);
            }
            finally
            {

                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessLicenseMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalLicense.Id + "]");
                    ADATrace.WriteLine("");
                }
            }
            return dalLicense;
        }

        protected virtual Telephone ProcessPersonTelephoneNumberMatch(CurrentContext ctx, Operation op, int personId, MDA.Pipeline.Model.PipelineTelephone pipelineTelephone, 
                                int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipelineTelephone.ShouldBeDiscarded) return null;

            Telephone dalTelephone = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessPersonTelephoneNumberMatch: [" + pipelineTelephone.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                var matchRes = _matchingService.MatchTelephoneNumber(pipelineTelephone, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "PersonTelephone");

                sw.Restart();

                if (_trace)
                {
                    ADATrace.WriteLine("Matching Phone: Number supplied [" + pipelineTelephone.ClientSuppliedNumber + "]");
                    ADATrace.WriteLine("Matching Phone: International Code [" + pipelineTelephone.InternationalCode + "]");
                    ADATrace.WriteLine("Matching Phone: Area Code [" + pipelineTelephone.AreaCode + "]");
                    ADATrace.WriteLine("Matching Phone: Full Number [" + pipelineTelephone.FullNumber + "]");
                    ADATrace.WriteLine("Matching Phone: Type will be [" + pipelineTelephone.TelephoneType_Id.ToString() + "]");
                }

                #region Process Match record
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalTelephone = dataServices.UpdateTelephone((int)matchRes.MatchIdList[0], pipelineTelephone, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalTelephone = dataServices.InsertTelephone(pipelineTelephone);

                            if (_trace) ADATrace.WriteLine("Inserted new Telephone [" + dalTelephone.Id + "]");
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new telephone
                        dalTelephone = dataServices.InsertTelephone(pipelineTelephone);

                        if (_trace) ADATrace.WriteLine("Inserted new Telephone [" + dalTelephone.Id + "]");

                        // Link the person to with any partial telephone matches we got returned
                        foreach (var teleId in matchRes.MatchIdList)
                        {
                            int saveLc = pipelineTelephone.PO2T_LinkData.LinkConfidence;

                            pipelineTelephone.PO2T_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                 ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessPerson2TelephoneLink(ctx, personId, teleId, pipelineTelephone.PO2T_LinkData, /*telephoneLinkType,*/ op, newRiskClaim);

                            pipelineTelephone.PO2T_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalTelephone = dataServices.SelectTelephone((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessPersonTelephoneNumberMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessPersonTelephoneNumberMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessPersonTelephoneNumberMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalTelephone.Id + "]");
                    ADATrace.WriteLine("");
                }
            }
            return dalTelephone;
        }

        protected virtual Telephone ProcessOrgTelephoneNumberMatch(CurrentContext ctx, Operation op, int orgId, 
                                        MDA.Pipeline.Model.PipelineTelephone pipeTelephone, 
                                        int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeTelephone.ShouldBeDiscarded) return null;

            Telephone dalTelephone = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessOrgTelephoneNumberMatch [" + pipeTelephone.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                var matchRes = _matchingService.MatchTelephoneNumber(pipeTelephone, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "OrgTelephone");

                sw.Restart();

                if (pipeTelephone.FullNumber != null)
                {
                    if (pipeTelephone.FullNumber.StartsWith("07"))
                        pipeTelephone.TelephoneType_Id = (int)Common.Enum.TelephoneType.Mobile;
                    else if (pipeTelephone.TelephoneType_Id == (int)Common.Enum.TelephoneType.Mobile)
                        pipeTelephone.TelephoneType_Id = (int)Common.Enum.TelephoneType.Unknown;
                }

                if (_trace)
                {
                    ADATrace.WriteLine("Matching Phone: Number supplied [" + pipeTelephone.ClientSuppliedNumber + "]");
                    ADATrace.WriteLine("Matching Phone: International Code [" + pipeTelephone.InternationalCode + "]");
                    ADATrace.WriteLine("Matching Phone: Area Code [" + pipeTelephone.AreaCode + "]");
                    ADATrace.WriteLine("Matching Phone: Full Number [" + pipeTelephone.FullNumber + "]");
                    ADATrace.WriteLine("Matching Phone: Type will be [" + pipeTelephone.TelephoneType_Id.ToString() + "]");                      
                }

                #region Process Match record
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;

                        // Yes, so update any existing empty fields
                        dalTelephone = dataServices.UpdateTelephone((int)matchRes.MatchIdList[0], pipeTelephone, out ok);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed : Insert new");

                            dalTelephone = dataServices.InsertTelephone(pipeTelephone);

                            if (_trace) ADATrace.WriteLine("Inserted new Telephone [" + dalTelephone.Id + "]");
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new telephone
                        dalTelephone = dataServices.InsertTelephone(pipeTelephone);

                        if (_trace) ADATrace.WriteLine("Inserted new Telephone [" + dalTelephone.Id + "]");

                        // Link organisation to the partial telephone matches we found
                        foreach (var teleId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeTelephone.PO2T_LinkData.LinkConfidence;

                            pipeTelephone.PO2T_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessOrganisation2TelephoneLink(ctx, orgId, teleId, pipeTelephone.PO2T_LinkData, /*telephoneLinkType,*/ op, newRiskClaim);

                            pipeTelephone.PO2T_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalTelephone = dataServices.SelectTelephone((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessOrgTelephoneNumberMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessOrgTelephoneNumberMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessOrgTelephoneNumberMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalTelephone.Id + "]");
                    ADATrace.WriteLine("");
                }
            }
            return dalTelephone;
        }

        protected virtual Vehicle ProcessVehicleMatch(CurrentContext ctx, Operation op, MDA.Pipeline.Model.PipelineVehicle pipeVehicle, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeVehicle.ShouldBeDiscarded) return null;

            Vehicle dalVehicle = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessVehicleMatch: [" + pipeVehicle.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                List<MDA.Pipeline.Model.PipelineAddress> addresses = new List<MDA.Pipeline.Model.PipelineAddress>();
                foreach (var _person in pipeVehicle.People)
                {
                    foreach (var _address in _person.Addresses)
                    {
                        addresses.Add(_address);
                    }
                }

                var matchRes = _matchingService.MatchVehicle(pipeVehicle, addresses, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "Vehicle");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeVehicle = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineVehicle;

                #region Process Match record
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;
                        string error;

                        // Yes, so update any existing empty fields
                        dalVehicle = dataServices.UpdateVehicle((int)matchRes.MatchIdList[0], pipeVehicle, out ok, out error);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed [" + error + "] : Insert new : Create V2V link");

                            dalVehicle = dataServices.InsertVehicle(pipeVehicle);

                            if (_trace) ADATrace.WriteLine("Inserted new Vehicle [" + dalVehicle.Id + "]");

                            pipeVehicle.V2V_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            ProcessVehicle2VehicleLink(ctx, (int)matchRes.MatchIdList[0], dalVehicle.Id, pipeVehicle.V2V_LinkData, /*null,*/ op); //, newRiskClaim);
                        }

                        if (matchRes.MatchIdList.Count > 1)
                        {
                            if (_trace) ADATrace.WriteLine("Found Multi-MATCH");

                            int cnt = 0;
                            foreach (var pid in matchRes.MatchIdList)
                            {
                                if (cnt++ == 0) continue;

                                if (_trace) ADATrace.WriteLine("Adding additional Confirmed Link to [" + pid.ToString() + "]");

                                pipeVehicle.V2V_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                ProcessVehicle2VehicleLink(ctx, dalVehicle.Id, pid, pipeVehicle.V2V_LinkData, /*null,*/ op); //, newRiskClaim);
                            }
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }

                        // No, so insert a new vehicle
                        dalVehicle = dataServices.InsertVehicle(pipeVehicle);

                        if (_trace) ADATrace.WriteLine("Inserted new Vehicle [" + dalVehicle.Id + "]");

                        // matchService.AddVehicle(dalVehicle);

                        // Link this new vehicle with any partial matches we got returned.
                        foreach (var vehicleId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeVehicle.V2V_LinkData.LinkConfidence;

                            pipeVehicle.V2V_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                    ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessVehicle2VehicleLink(ctx, dalVehicle.Id, vehicleId, pipeVehicle.V2V_LinkData, /*null,*/ op); //, newRiskClaim);

                            pipeVehicle.V2V_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");
                    
                    dalVehicle = dataServices.SelectVehicle((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                // We can't create Vehicle2Person Link because at this point we have not processed a person

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessVehicleMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessVehicleMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessPersonVehicleMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalVehicle.Id + "]");
                    ADATrace.WriteLine("");
                }
            }
            return dalVehicle;
        }

        protected virtual Handset ProcessHandsetMatch(CurrentContext ctx, Operation op, MDA.Pipeline.Model.PipelineHandset pipeHandset, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeHandset.ShouldBeDiscarded) return null;

            Handset dalHandset = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessHandsetMatch: [" + pipeHandset.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                List<MDA.Pipeline.Model.PipelineAddress> addresses = new List<MDA.Pipeline.Model.PipelineAddress>();
                foreach (var _person in pipeHandset.People)
                {
                    foreach (var _address in _person.Addresses)
                    {
                        addresses.Add(_address);
                    }
                }

                var matchRes = _matchingService.MatchHandsetIMEI(pipeHandset, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "Handset");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeHandset = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineHandset;

                #region Process Match record
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList)); ;
                        bool ok;
                        string error;

                        // Yes, so update any existing empty fields
                        dalHandset = dataServices.UpdateHandset((int)matchRes.MatchIdList[0], pipeHandset, out ok, out error);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed [" + error + "] : Insert new : Create H2H link");

                            dalHandset = dataServices.InsertHandset(pipeHandset);

                            if (_trace) ADATrace.WriteLine("Inserted new Handset [" + dalHandset.Id + "]");

                            pipeHandset.H2H_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            ProcessHandset2HandsetLink(ctx, (int)matchRes.MatchIdList[0], dalHandset.Id, pipeHandset.H2H_LinkData, /*null,*/ op); //, newRiskClaim);
                        }

                        if (matchRes.MatchIdList.Count > 1)
                        {
                            if (_trace) ADATrace.WriteLine("Found Multi-MATCH");

                            int cnt = 0;
                            foreach (var pid in matchRes.MatchIdList)
                            {
                                if (cnt++ == 0) continue;

                                if (_trace) ADATrace.WriteLine("Adding additional Confirmed Link to [" + pid.ToString() + "]");

                                pipeHandset.H2H_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                ProcessHandset2HandsetLink(ctx, dalHandset.Id, pid, pipeHandset.H2H_LinkData, /*null,*/ op); //, newRiskClaim);
                            }
                        }
                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }

                        // No, so insert a new vehicle
                        dalHandset = dataServices.InsertHandset(pipeHandset);

                        if (_trace) ADATrace.WriteLine("Inserted new Handset [" + dalHandset.Id + "]");

                        // matchService.AddVehicle(dalVehicle);

                        // Link this new handset with any partial matches we got returned.
                        foreach (var handsetId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeHandset.H2H_LinkData.LinkConfidence;

                            pipeHandset.H2H_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                    ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessHandset2HandsetLink(ctx, dalHandset.Id, handsetId, pipeHandset.H2H_LinkData, /*null,*/ op); //, newRiskClaim);

                            pipeHandset.H2H_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalHandset = dataServices.SelectHandset((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                // We can't create Handset2Person Link because at this point we have not processed a person

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessHandsetMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessHandsetMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessPersonHandsetMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalHandset.Id + "]");
                    ADATrace.WriteLine("");
                }
            }
            return dalHandset;
        }
    

        protected virtual Vehicle ProcessOrgVehicleMatch(CurrentContext ctx, Operation op, int orgId, string orgName, MDA.Pipeline.Model.PipelineOrganisationVehicle pipeOrgVehicle, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeOrgVehicle.ShouldBeDiscarded) return null;

            Vehicle dalVehicle = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessOrgVehicleMatch: [" + pipeOrgVehicle.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                // Create a vehicle record to re-use matching rules
                MDA.Pipeline.Model.PipelineVehicle pipeVehicle = new MDA.Pipeline.Model.PipelineVehicle()
                {
                    VehicleColour_Id              = pipeOrgVehicle.VehicleColour_Id,
                    EngineCapacity                = pipeOrgVehicle.EngineCapacity,
                    VehicleFuel_Id                = pipeOrgVehicle.VehicleFuel_Id,
                    VehicleMake                   = pipeOrgVehicle.VehicleMake,
                    VehicleModel                  = pipeOrgVehicle.VehicleModel,
                    VehicleTransmission_Id        = pipeOrgVehicle.VehicleTransmission_Id,
                    //Incident2VehicleLinkType_Id = xmlOrgVehicle.I2V_LinkData.Incident2VehicleLinkType_Id,
                    VehicleRegistration           = pipeOrgVehicle.VehicleRegistration,
                    VehicleType_Id                = pipeOrgVehicle.VehicleType_Id,
                    VIN                           = pipeOrgVehicle.VIN,
                    ADARecordStatus = pipeOrgVehicle.ADARecordStatus,
                    CreatedBy = pipeOrgVehicle.CreatedBy,
                    CreatedDate = pipeOrgVehicle.CreatedDate,
                    DamageDescription = pipeOrgVehicle.DamageDescription,
                    KeyAttractor = pipeOrgVehicle.KeyAttractor,
                    IBaseId = pipeOrgVehicle.IBaseId,
                    ModifiedBy = pipeOrgVehicle.ModifiedBy,
                    Operation = pipeOrgVehicle.Operation,
                    ModifiedDate = pipeOrgVehicle.ModifiedDate,
                    RecordStatus = pipeOrgVehicle.RecordStatus,
                    Source = pipeOrgVehicle.Source,
                    SourceDescription = pipeOrgVehicle.SourceDescription,
                    SourceReference = pipeOrgVehicle.SourceDescription,
                };

                pipeVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = pipeOrgVehicle.I2V_LinkData.Incident2VehicleLinkType_Id;

                List<MDA.Pipeline.Model.PipelineAddress> addresses = new List<MDA.Pipeline.Model.PipelineAddress>();


                var matchRes = _matchingService.MatchVehicle(pipeVehicle, addresses, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "OrgVehicle");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeVehicle = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineVehicle;

                #region Process Match record

                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;
                        string error;

                        // Yes, so update any existing empty fields
                        dalVehicle = dataServices.UpdateVehicle((int)matchRes.MatchIdList[0], pipeVehicle, out ok, out error);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed [" + error + "] : Insert new : Create V2V link");

                            dalVehicle = dataServices.InsertVehicle(pipeVehicle);

                            if (_trace) ADATrace.WriteLine("Insert new Vehicle [" + dalVehicle.Id + "]");

                            pipeOrgVehicle.V2V_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            ProcessVehicle2VehicleLink(ctx, (int)matchRes.MatchIdList[0], dalVehicle.Id, pipeOrgVehicle.V2V_LinkData, /*null,*/ op); //, newRiskClaim);
                        }

                        if (matchRes.MatchIdList.Count > 1)
                        {
                            if (_trace) ADATrace.WriteLine("Found Multi-MATCH");

                            int cnt = 0;
                            foreach (var vid in matchRes.MatchIdList)
                            {
                                if (cnt++ == 0) continue;

                                if (_trace) ADATrace.WriteLine("Adding additional Confirmed Link to [" + vid.ToString() + "]");

                                pipeOrgVehicle.V2V_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                ProcessVehicle2VehicleLink(ctx, vid, dalVehicle.Id, pipeOrgVehicle.V2V_LinkData, /*null,*/ op); //, newRiskClaim);
                            }
                        }
                    }

                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new vehicle
                        dalVehicle = dataServices.InsertVehicle(pipeVehicle);

                        if (_trace) ADATrace.WriteLine("Insert new Vehicle [" + dalVehicle.Id + "]");

                        // Link this new vehicle with any partial vehicle matches we got returned - Vehicle2Vehicle Link
                        foreach (var vehId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeVehicle.V2V_LinkData.LinkConfidence;

                            pipeVehicle.V2V_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                    ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessVehicle2VehicleLink(ctx, dalVehicle.Id, vehId, pipeVehicle.V2V_LinkData, /*null,*/ op); //, newRiskClaim);

                            pipeVehicle.V2V_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");
                    
                    dalVehicle = dataServices.SelectVehicle((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessOrgVehicleMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessOrgVehicleMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessOrgVehicleMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalVehicle.Id + "]");
                    ADATrace.WriteLine("");
                }
            }
            return dalVehicle;
        }


        protected virtual Handset ProcessOrgHandsetMatch(CurrentContext ctx, Operation op, int orgId, string orgName, MDA.Pipeline.Model.PipelineOrganisationHandset pipeOrgHandset, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeOrgHandset.ShouldBeDiscarded) return null;

            Handset dalHandset = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessOrgHandsetMatch: [" + pipeOrgHandset.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                // Create a handset record to re-use matching rules
                MDA.Pipeline.Model.PipelineHandset pipeHandset = new MDA.Pipeline.Model.PipelineHandset()
                {
                    //HandsetType_Id = pipeOrgHandset.HandsetType_Id,
                    HandsetIMEI = pipeOrgHandset.HandsetIMEI,
                    HandsetMake = pipeOrgHandset.HandsetMake,
                    HandsetModel = pipeOrgHandset.HandsetModel,
                    HandsetColour_Id = pipeOrgHandset.HandsetColour_Id,
                    HandsetValue = pipeOrgHandset.HandsetValue,
                    ADARecordStatus = pipeOrgHandset.ADARecordStatus,
                    CreatedBy = pipeOrgHandset.CreatedBy,
                    CreatedDate = pipeOrgHandset.CreatedDate,
                    KeyAttractor = pipeOrgHandset.KeyAttractor,
                    IBaseId = pipeOrgHandset.IBaseId,
                    ModifiedBy = pipeOrgHandset.ModifiedBy,
                    Operation = pipeOrgHandset.Operation,
                    ModifiedDate = pipeOrgHandset.ModifiedDate,
                    RecordStatus = pipeOrgHandset.RecordStatus,
                    Source = pipeOrgHandset.Source,
                    SourceDescription = pipeOrgHandset.SourceDescription,
                    SourceReference = pipeOrgHandset.SourceDescription
                };

                pipeHandset.I2H_LinkData.Incident2HandsetLinkType_Id = pipeOrgHandset.I2H_LinkData.Incident2HandsetLinkType_Id;

                List<MDA.Pipeline.Model.PipelineAddress> addresses = new List<MDA.Pipeline.Model.PipelineAddress>();


                var matchRes = _matchingService.MatchHandsetIMEI(pipeHandset, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "OrgHandset");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeHandset = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineHandset;

                #region Process Match record

                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList)); ;
                        bool ok;
                        string error;

                        // Yes, so update any existing empty fields
                        dalHandset = dataServices.UpdateHandset((int)matchRes.MatchIdList[0], pipeHandset, out ok, out error);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed [" + error + "] : Insert new : Create H2H link");

                            dalHandset = dataServices.InsertHandset(pipeHandset);

                            if (_trace) ADATrace.WriteLine("Insert new Handset [" + dalHandset.Id + "]");

                            pipeOrgHandset.H2H_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            ProcessHandset2HandsetLink(ctx, (int)matchRes.MatchIdList[0], dalHandset.Id, pipeOrgHandset.H2H_LinkData, /*null,*/ op); //, newRiskClaim);
                        }

                        if (matchRes.MatchIdList.Count > 1)
                        {
                            if (_trace) ADATrace.WriteLine("Found Multi-MATCH");

                            int cnt = 0;
                            foreach (var hid in matchRes.MatchIdList)
                            {
                                if (cnt++ == 0) continue;

                                if (_trace) ADATrace.WriteLine("Adding additional Confirmed Link to [" + hid.ToString() + "]");

                                pipeOrgHandset.H2H_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                ProcessHandset2HandsetLink(ctx, hid, dalHandset.Id, pipeOrgHandset.H2H_LinkData, /*null,*/ op); //, newRiskClaim);
                            }
                        }
                    }

                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new vehicle
                        dalHandset = dataServices.InsertHandset(pipeHandset);

                        if (_trace) ADATrace.WriteLine("Insert new Handset [" + dalHandset.Id + "]");

                        // Link this new handset with any partial handset matches we got returned - Handset2Handset Link
                        foreach (var handId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeHandset.H2H_LinkData.LinkConfidence;

                            pipeHandset.H2H_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                    ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessHandset2HandsetLink(ctx, dalHandset.Id, handId, pipeHandset.H2H_LinkData, /*null,*/ op); //, newRiskClaim);

                            pipeHandset.H2H_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalHandset = dataServices.SelectHandset((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessOrgHandsetMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessOrgHandsetMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessOrgHandsetMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalHandset.Id + "]");
                    ADATrace.WriteLine("");
                }
            }
            return dalHandset;
        }

        protected virtual Organisation ProcessOrganisationMatch(CurrentContext ctx, Operation op, MDA.Pipeline.Model.PipelineOrganisation pipeOrganisation,
                            MDA.Pipeline.Model.IPipelineClaim pipeClaim, int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipeOrganisation.ShouldBeDiscarded) return null;

            Organisation dalOrganisation = null;

            MatchResults matchOrgSanctionsRes = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessOrganisationMatch: [" + pipeOrganisation.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                var matchRes = _matchingService.MatchOrganisation(pipeOrganisation, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "Org");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipeOrganisation = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelineOrganisation;

                #region Process Match record

                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");
                 
                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));;
                        bool ok;
                        string error;

                        // Yes, so update any existing empty fields
                        dalOrganisation = dataServices.UpdateOrganisation((int)matchRes.MatchIdList[0], pipeOrganisation, out ok, out error);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed [" + error + "] : Insert new : Add O2O Link");

                            dalOrganisation = dataServices.InsertOrganisation(pipeOrganisation); //,
                                                                   //(int)MDA.Common.Enum.OrganisationStatus.Active);

                            if (_trace) ADATrace.WriteLine("Inserted new Organisation [" + dalOrganisation.Id + "]");

                            pipeOrganisation.O2O_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            // Link this new org with org with the matched org
                            ProcessOrganisation2OrganisationLink(ctx, (int)matchRes.MatchIdList[0], dalOrganisation.Id, pipeOrganisation.O2O_LinkData, /*MDA.Common.Enum.OrganisationLinkType.AlsoKnownAs,*/ op); //, newRiskClaim);
                        }

                        if (matchRes.MatchIdList.Count > 1)
                        {
                            if (_trace) ADATrace.WriteLine("Found Multi-MATCH");

                            int cnt = 0;
                            foreach (var oid in matchRes.MatchIdList)
                            {
                                if (cnt++ == 0) continue;

                                if (_trace) ADATrace.WriteLine("Adding additional Confirmed Link to [" + oid.ToString() + "]");

                                pipeOrganisation.O2O_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                ProcessOrganisation2OrganisationLink(ctx, oid, dalOrganisation.Id, pipeOrganisation.O2O_LinkData, /*MDA.Common.Enum.OrganisationLinkType.AlsoKnownAs,*/ op); //, newRiskClaim);
                            }
                        }

                        ProcessSanctionsOrganisation(ctx, pipeOrganisation, dalOrganisation, matchOrgSanctionsRes, dataServices);

                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }
                        // No, so insert a new record
                        dalOrganisation = dataServices.InsertOrganisation(pipeOrganisation); //,
                                                                            //(int)MDA.Common.Enum.OrganisationStatus.Active);


                        ProcessSanctionsOrganisation(ctx, pipeOrganisation, dalOrganisation, matchOrgSanctionsRes, dataServices);

                        if (_trace) ADATrace.WriteLine("Inserted new Organisation [" + dalOrganisation.Id + "]");

                        //if (dalOrganisation != null)
                        //    organisationId = dalOrganisation.Id;

                        // Link this new org with any partial matches we got returned
                        foreach (var orgId in matchRes.MatchIdList)
                        {
                            int saveLc = pipeOrganisation.O2O_LinkData.LinkConfidence;

                            if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Tentative)
                            {
                                pipeOrganisation.O2O_LinkData.LinkConfidence = (int)LinkConfidence.Tentative;
                            }
                            else
                            {
                                pipeOrganisation.O2O_LinkData.LinkConfidence = (int)LinkConfidence.Unconfirmed;
                            }

                            ProcessOrganisation2OrganisationLink(ctx, dalOrganisation.Id, orgId, pipeOrganisation.O2O_LinkData,
                                                                    //MDA.Common.Enum.OrganisationLinkType.AlsoKnownAs,
                                                                    op); //, newRiskClaim);

                            pipeOrganisation.O2O_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");
                    
                    dalOrganisation = dataServices.SelectOrganisation((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessOrganisationMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessOrganisationMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessOrganisationMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalOrganisation.Id + "]");
                    ADATrace.WriteLine("");
                }
            }

            return dalOrganisation;
        }

        private void ProcessSanctionsOrganisation(CurrentContext ctx, MDA.Pipeline.Model.PipelineOrganisation pipeOrganisation, Organisation dalOrganisation, MatchResults matchOrgSanctionsRes, DataServices dataServices)
        {
            if (ctx.RiskClientId == 5) // Liberty - Should only use sanctions. NB. If this turns out to me more than 1 client will need refactoring
            {
                matchOrgSanctionsRes = _matchingService.MatchSanctionsOrganisation(pipeOrganisation, null);

                if (matchOrgSanctionsRes.MatchSanctionsList.Count > 0)
                {
                    dataServices.UpdateSanctionsOrganisation(dalOrganisation.Id, matchOrgSanctionsRes.MatchSanctionsList[0], pipeOrganisation);
                }
                else
                {
                    dataServices.ResetSanctionsOrganisation(dalOrganisation.Id);
                }
            }
        }

        protected virtual Person ProcessPersonMatch(CurrentContext ctx, Operation op, MDA.Pipeline.Model.PipelinePerson pipePerson, 
                                    MDA.Pipeline.Model.PipelineVehicle pipeVehicle, MDA.Pipeline.Model.IPipelineClaim pipeClaim, 
                                    int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipePerson.ShouldBeDiscarded) return null;

            Person dalPerson = null;

            MatchResults matchPersonSanctionsRes = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessPersonMatch: [" + pipePerson.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                var matchRes = _matchingService.MatchPerson(pipePerson, pipeVehicle, pipeClaim, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "Person");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipePerson = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelinePerson;

                #region Process Match record
                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));
                        bool ok;
                        string error;

                        // Yes, so update any existing empty fields
                        dalPerson = dataServices.UpdatePerson((int)matchRes.MatchIdList[0], pipePerson, out ok, out error);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record and add a confirmed link
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed [" + error + "] : Add P2P Link");

                            dalPerson = dataServices.InsertPerson(pipePerson);

                            if (_trace) ADATrace.WriteLine("Inserted new Person [" + dalPerson.Id + "]");

                            pipePerson.P2P_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            ProcessPerson2PersonLink(ctx, dalPerson.Id, (int)matchRes.MatchIdList[0], pipePerson.P2P_LinkData, /*MDA.Common.Enum.Person2PersonLinkType.AlsoKnownAs,*/ op); //, newRiskClaim);
                        }

                        if (matchRes.MatchIdList.Count > 1)
                        {
                            if (_trace) ADATrace.WriteLine("Found Multi-MATCH");

                            int cnt = 0;
                            foreach (var pid in matchRes.MatchIdList)
                            {
                                if (cnt++ == 0) continue;

                                if (_trace) ADATrace.WriteLine("Adding additional Confirmed Link to [" + pid.ToString() + "]");

                                pipePerson.P2P_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                ProcessPerson2PersonLink(ctx, dalPerson.Id, pid, pipePerson.P2P_LinkData, /*MDA.Common.Enum.Person2PersonLinkType.AlsoKnownAs,*/ op); //, newRiskClaim);
                            }
                        }

                        ProcessSanctionsPerson(ctx, pipePerson, dalPerson, matchPersonSanctionsRes, dataServices);

                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }

                        dalPerson = dataServices.InsertPerson(pipePerson);

                        ProcessSanctionsPerson(ctx, pipePerson, dalPerson, matchPersonSanctionsRes, dataServices);

                        if (_trace) ADATrace.WriteLine("Inserted new Person [" + dalPerson.Id + "]");

                        // Link this new person with any partial person matches we got returned
                        foreach (var perId in matchRes.MatchIdList)
                        {
                            int saveLc = pipePerson.P2P_LinkData.LinkConfidence;

                            pipePerson.P2P_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                        ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessPerson2PersonLink(ctx, dalPerson.Id, perId, pipePerson.P2P_LinkData, op);

                            pipePerson.P2P_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalPerson = dataServices.SelectPerson((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }

                

                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessPersonMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessPersonMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessPersonMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalPerson.Id + "]");
                    ADATrace.WriteLine("");
                }
            }

            return dalPerson;
        }


        protected virtual Person ProcessPersonMatch(CurrentContext ctx, Operation op, MDA.Pipeline.Model.PipelinePerson pipePerson,
                                    MDA.Pipeline.Model.PipelineHandset pipeHandset, MDA.Pipeline.Model.IPipelineClaim pipeClaim,
                                    int IncidentId, RiskClaim newRiskClaim, int? existingBaseRiskClaimId)
        {
            if (pipePerson.ShouldBeDiscarded) return null;

            Person dalPerson = null;

            MatchResults matchPersonSanctionsRes = null;

            DataServices dataServices = new DataServices(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Matching: ProcessPersonMatch: [" + pipePerson.DisplayText + "]");
                    ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                }

                var matchRes = _matchingService.MatchPerson(pipePerson, pipeHandset, pipeClaim, existingBaseRiskClaimId);

                if (_trace) ADATrace.WriteLine("Matching Rule Triggered : [" + ((matchRes.MatchRuleNumber != null) ? matchRes.MatchRuleNumber.ToString() : "Unknown") + "] [" + sw.ElapsedMilliseconds + "ms]");

                if (_recordTimingMetrics) InsertTimingMetric(newRiskClaim, existingBaseRiskClaimId, dataServices, sw, matchRes, "Person");

                sw.Restart();

                DebugCheck.NotNull<MDA.MatchingService.Model.MatchResults>(matchRes);
                pipePerson = matchRes.ModifiedEntity as MDA.Pipeline.Model.PipelinePerson;

                #region Process Match record

                if (op == Operation.Normal)
                {
                    if (_trace) ADATrace.WriteLine("Operation:Normal");

                    // Did we find a perfect match?
                    if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed)
                    {
                        if (_trace) ADATrace.WriteLine("Found MATCH : Try Update : " + BuildMatchTrace(matchRes.MatchIdList));
                        bool ok;
                        string error;

                        // Yes, so update any existing empty fields
                        dalPerson = dataServices.UpdatePerson((int)matchRes.MatchIdList[0], pipePerson, out ok, out error);

                        if (!ok)   // Fields exist that could not be overwritten. So create a new record and add a confirmed link
                        {
                            if (_trace) ADATrace.WriteLine("UPDATE Failed [" + error + "] : Add P2P Link");

                            dalPerson = dataServices.InsertPerson(pipePerson);

                            if (_trace) ADATrace.WriteLine("Inserted new Person [" + dalPerson.Id + "]");

                            pipePerson.P2P_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                            ProcessPerson2PersonLink(ctx, dalPerson.Id, (int)matchRes.MatchIdList[0], pipePerson.P2P_LinkData, /*MDA.Common.Enum.Person2PersonLinkType.AlsoKnownAs,*/ op); //, newRiskClaim);
                        }

                        if (matchRes.MatchIdList.Count > 1)
                        {
                            if (_trace) ADATrace.WriteLine("Found Multi-MATCH");

                            int cnt = 0;
                            foreach (var pid in matchRes.MatchIdList)
                            {
                                if (cnt++ == 0) continue;

                                if (_trace) ADATrace.WriteLine("Adding additional Confirmed Link to [" + pid.ToString() + "]");

                                pipePerson.P2P_LinkData.LinkConfidence = (int)LinkConfidence.Confirmed;

                                ProcessPerson2PersonLink(ctx, dalPerson.Id, pid, pipePerson.P2P_LinkData, /*MDA.Common.Enum.Person2PersonLinkType.AlsoKnownAs,*/ op); //, newRiskClaim);
                            }
                        }

                        ProcessSanctionsPerson(ctx, pipePerson, dalPerson, matchPersonSanctionsRes, dataServices);

                    }
                    else
                    {
                        if (_trace)
                        {
                            if (matchRes.MatchTypeFound != MatchingService.Model.MatchType.NotFound)
                                ADATrace.WriteLine("Partial MATCH " + ((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed) ? "Unconfirmed" : "Tentative") + " matches = " + BuildMatchTrace(matchRes.MatchIdList) + " : Insert New");
                            else
                                ADATrace.WriteLine("No MATCH : Insert New");
                        }

                        dalPerson = dataServices.InsertPerson(pipePerson);

                        ProcessSanctionsPerson(ctx, pipePerson, dalPerson, matchPersonSanctionsRes, dataServices);

                        if (_trace) ADATrace.WriteLine("Inserted new Person [" + dalPerson.Id + "]");

                        // Link this new person with any partial person matches we got returned
                        foreach (var perId in matchRes.MatchIdList)
                        {
                            int saveLc = pipePerson.P2P_LinkData.LinkConfidence;

                            pipePerson.P2P_LinkData.LinkConfidence = (int)((matchRes.MatchTypeFound == MatchingService.Model.MatchType.Unconfirmed)
                                                        ? LinkConfidence.Unconfirmed : LinkConfidence.Tentative);

                            ProcessPerson2PersonLink(ctx, dalPerson.Id, perId, pipePerson.P2P_LinkData, op);

                            pipePerson.P2P_LinkData.LinkConfidence = saveLc;
                        }
                    }
                }
                else if (matchRes.MatchTypeFound == MatchingService.Model.MatchType.Confirmed && matchRes.MatchIdList.Count == 1)  // we found a match so we need to delete/withdraw it
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + ". Confirmed Single Match [" + matchRes.MatchIdList[0].ToString() + "]");

                    dalPerson = dataServices.SelectPerson((int)matchRes.MatchIdList[0]);
                }
                else
                {
                    if (_trace) ADATrace.WriteLine("Operation:" + ((op == Operation.Delete) ? "Delete" : "Withdraw") + " BUT RECORD NOT MATCHED");
                }



                #endregion
            }
            catch (DbEntityValidationException dbEx)
            {
                if (_trace) ADATrace.TraceException(dbEx);

                throw new Exception("ProcessPersonMatch : Match exception", dbEx);
            }
            catch (Exception ex)
            {
                if (_trace) ADATrace.TraceException(ex);

                throw new Exception("ProcessPersonMatch : Match exception", ex);
            }
            finally
            {
                if (_trace)
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("Matching: ProcessPersonMatch : Done : [" + sw.ElapsedMilliseconds + "ms]");
                    ADATrace.WriteLine("Returning ID[" + dalPerson.Id + "]");
                    ADATrace.WriteLine("");
                }
            }

            return dalPerson;
        }

        private void ProcessSanctionsPerson(CurrentContext ctx, MDA.Pipeline.Model.PipelinePerson pipePerson, Person dalPerson, MatchResults matchPersonSanctionsRes, DataServices dataServices)
        {
            if (ctx.RiskClientId == 5) // Liberty - Should only use sanctions. NB. If this turns out to me more than 1 client will need refactoring
            {
                matchPersonSanctionsRes = _matchingService.MatchSanctionsPerson(pipePerson, null);

                if (matchPersonSanctionsRes.MatchSanctionsList.Count > 0)
                {
                    dataServices.UpdateSanctionsPerson(dalPerson.Id, matchPersonSanctionsRes.MatchSanctionsList[0], pipePerson);
                }
                else
                {
                    dataServices.ResetSanctionsPerson(dalPerson.Id);
                }
            }
        }

        protected string BuildMatchTrace(IEnumerable<int> ids)
        {
            var sb = new StringBuilder();
            string comma = "";

            sb.Append("[");
            foreach (int id in ids)
            {
                sb.Append(comma + id);
                comma = ",";
            }
            sb.Append("]");

            return sb.ToString();
        }

        private void InsertTimingMetric(RiskClaim newRiskClaim, int? existingBaseRiskClaimId, DataServices dataServices, Stopwatch sw, MatchResults matchRes, string entityType)
        {
            dataServices.InsertTimingMetric (new TimingMetric
            {
                RuleNumber = matchRes.MatchRuleNumber ?? -1,
                TimingInMs = Convert.ToInt32(sw.ElapsedMilliseconds),
                Created = DateTime.Now,
                CreatedBy = _ctx.Who,
                RiskClaim_Id = newRiskClaim.Id,
                BaseRiskClaim_Id = Convert.ToInt32(existingBaseRiskClaimId),
                TimingType = (int)MDA.Common.Enum.TimingType.Matching,
                EntityType = entityType
            });
        }
    }
}
