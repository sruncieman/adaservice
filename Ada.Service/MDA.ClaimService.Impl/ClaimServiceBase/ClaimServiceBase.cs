﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Configuration;
using System.Diagnostics;

using MDA.Common.Server;
using MDA.DAL;
using MDA.Common;
using MDA.Common.Enum;
using MDA.MatchingService.Interface;
using MDA.CleansingService.Interface;
using MDA.VerificationService.Interface;
using MDA.RiskService.Interface;
using MDA.ExternalServices.Model;
using MDA.Common.Debug;
using MDA.DataService;
using MDA.ClaimService.Interface;

using RiskEngine.Scoring.Entities;
using RiskEngine.Model;
using RiskEngine.Interfaces;
using RiskEngine.Scoring.Model;



namespace MDA.ClaimService
{
    public abstract partial class ClaimServiceBase
    {
        protected bool _trace = false;
        protected bool _traceDeDupe = false;
        protected bool _recordTimingMetrics = false;
        protected CurrentContext _ctx;
        protected MdaDbContext _db;
        protected IMatchingService _matchingService;
        protected ICleansingService _cleansingService;
        protected IValidationService _validationService;
        protected IRiskServices _riskService;
        protected IRiskEngine _riskEngine;

        protected DateTime CreatedDate;

        protected static string[] ReportStringsClaimLevel = new string[]
        {
             "Involvement Unknown",
             "Involvement Unknown",
             "Policyholder",
             "Involvement Unknown",
             "Insurer",
             "Broker",
             "Solicitor", 
             "Vehicle Engineer",
             "Recovery",
             "Storage",
             "Repairer",
             "Credit Hire",
             "Accident Management", 
             "Medical Examiner", 
        };

        public ClaimServiceBase(CurrentContext ctx, ICleansingService cleansingService, IMatchingService matchingService, IValidationService validationService, 
                                                     IRiskServices riskService, IRiskEngine riskEngineService)
        {
            _ctx               = ctx;
            _db                = ctx.db;
            _trace             = ctx.TraceLoad;
            _traceDeDupe       = ctx.TraceDeDupe;
            _recordTimingMetrics = (ConfigurationManager.AppSettings["RecordTimingMetrics"].ToUpper() == "TRUE") ? true : false;
            _matchingService   = matchingService;
            _cleansingService  = cleansingService;
            _validationService = validationService;
            _riskService       = riskService;
            _riskEngine        = riskEngineService;
          
            CreatedDate = DateTime.Now;
        }

        public ClaimServiceBase(CurrentContext ctx)
        {
            _ctx               = ctx;
            _db                = ctx.db;
            _trace             = ctx.TraceLoad;
            _recordTimingMetrics = (ConfigurationManager.AppSettings["RecordTimingMetrics"].ToUpper() == "TRUE") ? true : false;
            _matchingService   = null;
            _cleansingService  = null;
            _validationService = null;
            _riskService       = null;
            _riskEngine        = null;

            CreatedDate = DateTime.Now;
        }
        protected void SetCurrentContext(CurrentContext newCtx)
        {
            _ctx = newCtx;
            _db = _ctx.db as MdaDbContext;

            if (_matchingService != null) _matchingService.CurrentContext = _ctx;
            if (_riskService != null) _riskService.CurrentContext = _ctx;
        }

        protected int InitialisePipelineEntityRecord(CurrentContext ctx, MDA.Pipeline.Model.PipeEntityBase r)
        {
            if (r == null) return 0;

            r.ModifiedBy = null;
            r.ModifiedDate = null;

            r.CreatedBy = ctx.Who;
            r.CreatedDate = CreatedDate;

            r.Source = ctx.ClientName;
            r.SourceDescription = ctx.SourceDescription;
            r.SourceReference = ctx.SourceRef;

            r.RecordStatus = 0;
            r.ADARecordStatus = 0;
            r.IBaseId = null;
            r.KeyAttractor = null;

            r.Operation = 0;

            return 0;
        }

        protected int InitialisePipelineLinkRecord(CurrentContext ctx, MDA.Pipeline.Model.PipeLinkBase r)
        {
            if (r == null) return 0;

            InitialisePipelineEntityRecord(ctx, r);

            r.LinkConfidence = -1;

            return 0;
        }

        protected Operation GetOperation(MDA.Common.Enum.Operation? op1, MDA.Common.Enum.Operation? op2)
        {
            if (op1 == MDA.Common.Enum.Operation.Delete || op2 == MDA.Common.Enum.Operation.Delete) return Operation.Delete;
            if (op1 == MDA.Common.Enum.Operation.Withdrawn || op2 == MDA.Common.Enum.Operation.Withdrawn) return Operation.Withdrawn;

            return Operation.Normal;
        }

        /// <summary>
        /// Create the MASK of the external services to be called for this claim.  This is specified in the database but can 
        /// be overidden in the XML. A service defined in the XML that is not defined in the Db is ignored
        /// </summary>
        /// <param name="db"></param>
        /// <param name="riskClientId"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        protected int CreateServicesMask(int riskClientId, MDA.Pipeline.Model.IPipelineClaim c)
        {
            int mask = 0;

            // Get the external services defined in the Db for this client
            var services = _riskService.GetClientExternalServices(riskClientId);

            // Get the external service defined in the XML
            string[] xmlServices = (c.ExternalServices != null) ? c.ExternalServices.Replace(" ", "").ToUpper().Split(',') : null;

            // Now process each service defined in the database. A service defined in the XML that is not defined in the Db is ignored
            foreach (var s in services)
            {
                // If the Db says ALWAYS CALL then set the bit in the mask
                if (s.ServiceUsage == (int)RiskExternalServiceUsage.AlwaysCall)
                {
                    mask |= s.MaskValue;
                }

                // If Db says SPECIFIED IN XML and it is listed in the XML set the bit in the mask
                if (xmlServices != null && s.ServiceUsage == (int)RiskExternalServiceUsage.OnlyIfSpecifiedInXml &&
                                             xmlServices.Contains(s.ServiceName.ToUpper()))
                {
                    mask |= s.MaskValue;
                }
            }

            return mask;
        }


        /// <summary>
        /// Score the entity against the rules provided.  Simple creates a request object for the RiskEngine and calls it
        /// </summary>
        /// <param name="entity">IRiskEntity to be scored</param>
        /// <param name="ruleGroupData">Rules to be executed</param>
        /// <returns></returns>
        protected CalculateRiskResponse ScoreEntity(IRiskEntity entity, RuleGroupData ruleGroupData)
        {
            CalculateRiskRequest crr = new CalculateRiskRequest()
            {
                Entity = entity,
                RuleGroupData = ruleGroupData
            };

            using (var ctx = new CurrentContext(_ctx))
            {
                crr.Entity.ctx = ctx;

                var x = _riskEngine.CalculateRisk(crr);

                return x;
            }
        }

        /// <summary>
        /// This method is calledby ScoreClaim in this base class. You must override this method to extract the RiskEntity map that will be recursively scored.
        /// This method should return a FullClaimToScore structure and populate the RootRiskEntity field in that structure.
        /// </summary>
        /// <param name="riskClaimId">The ID of the RiskClaim to be scored</param>
        /// <returns>A class that holds the RiskEntity Map and fields to hold the score</returns>
        public virtual FullClaimToScore FetchRiskEntityMapForScoring(int riskClaimId)
        {
            throw new System.NotImplementedException("FetchClaimForScoring needs overriding in ClaimService");
        }

        //protected virtual ClaimScoreResults ScoreClaim(FullClaimToScore claim, int clientId, bool scoreWithLiveRules, string who)
        //{
        //    throw new System.NotImplementedException("ScoreClaim needs overriding in ClaimService");
        //}

        /// <summary>
        /// Method to recursively score any Risk Entity map.  Assumes [ScorableEntity] attributes are attached to every entity and collection
        /// that needs scoring. This a recursive method that walks the "map" calling "score" for each property with the
        /// attribute attached. You can override this method if you have special needs
        /// </summary>
        /// <param name="entity">Entity to be scored</param>
        /// <param name="pathToEntityRules">The path in RULES where the rulesets are found for this entity</param>
        /// <param name="clientId">riskClientId</param>
        /// <param name="scoreWithLiveRules">Scoring with Live Rules?</param>
        /// <param name="trackId">A list which tracks the entity type and DbId so we don't double score</param>
        /// <param name="totalScore">returns the total score for this entity map</param>
        /// <param name="totalKACount">returns the total KA count for this entity map</param>
        protected virtual void ScoreAnyClaim(IRiskEntity entity, string pathToEntityRules, int clientId, bool scoreWithLiveRules, List<string> trackId, ref int totalScore, ref int totalKACount)
        {
            // If we have been passed nothing return
            if (entity == null) return;

            // Build a key which uniquely identified THIS entity. Build from EntityName + "-" + this entities ID in ADA database
            // You end up with something like "Person-21"
            string key = entity.GetType().Name + "-" + entity.Db_Id.ToString();

            // Check if this KEY exists in our list of all keys on this claim. If it does return, we have already scored it
            if (trackId.Contains(key))
                return;

            // It wasn't in the list so ADD it and carry on
            trackId.Add(key);

            // Fetch the RuleGroups for this entity using the PATH supplied
            RuleGroupData rgd = (scoreWithLiveRules) ? _riskEngine.GetLiveRuleGroupByPath(pathToEntityRules, clientId)
                                                     : _riskEngine.GetWorkingRuleGroupByPath(pathToEntityRules, clientId);

            // If there are some rules, score the entity and add on the scores
            if (rgd != null)
            {
                entity._ScoreResults = ScoreEntity(entity, rgd);
                entity.TotalEntityScore = entity._ScoreResults.RuleGroupScore;

                totalScore += entity._ScoreResults.RuleGroupScore;
                totalKACount += entity._ScoreResults.RuleGroupKeyAttractorCount;
            }

            // Now find all the PROPERTIES on THIS entity with the [SCOREABLE] attribute 
            var propList = from p in entity.GetType().GetProperties().AsParallel()
                     let attr = p.GetCustomAttributes(typeof(ScoreableItemAttribute), true)
                     where attr.Length == 1
                     select new { Property = p, Attribute = attr.First() as ScoreableItemAttribute };

            // Have a local total score so we can add up the CHILD score
            int _totalScore = 0;

            // Now cycle through each property
            foreach (var p in propList)
            {
                // Work out the next rulePath.  If the attribute RulePath starts with "\" us the path as-is (with \ removed).  It it doesn't start 
                // with "\" then append the path provide to the path we have separated by a "\"
                string nextRulePath = (p.Attribute.RulePath.StartsWith("\\"))   ? p.Attribute.RulePath.Substring(1) 
                                                                                : pathToEntityRules + "\\" + p.Attribute.RulePath;

                // Now check if the property is a collection.  If so score each item in the collection otherwise score the single property value
                if (p.Attribute.IsCollection)
                {
                    IEnumerable items = (IEnumerable)p.Property.GetValue(entity);   // Get the list to cycle through

                    if (items != null)
                    {
                        foreach (var item in items)  // Score each list item
                        {
                            ScoreAnyClaim((IRiskEntity)item, nextRulePath, clientId, scoreWithLiveRules, trackId, ref _totalScore, ref totalKACount);
                        }
                    }
                }
                else
                {
                    var item = (IRiskEntity)p.Property.GetValue(entity);  // Get the single value

                    ScoreAnyClaim(item, nextRulePath, clientId, scoreWithLiveRules, trackId, ref _totalScore, ref totalKACount);
                }
            }

            // Save the CHILD total score
            entity.TotalChildScore = _totalScore;

            // Add the child scores to the total entity score
            totalScore += _totalScore;
        }

        /// <summary>
        /// Retrieves all entities associated with a claim and score them
        /// </summary>
        /// <param name="riskClaimId">Claim to score</param>
        /// <param name="clientId">Client ID</param>
        /// <param name="scoreWithLiveRules">Use live rulegroups?</param>
        /// <param name="who">user name</param>
        /// <returns></returns>
        public ClaimScoreResults ScoreRiskEntityMap(int riskClaimId, int clientId, bool scoreWithLiveRules, string who)
        {
            return ScoreRiskEntityMap(FetchRiskEntityMapForScoring(riskClaimId), clientId, scoreWithLiveRules, who);
        }

        public ClaimScoreResults ScoreRiskEntityMap(FullClaimToScore claim, int clientId, bool scoreWithLiveRules, string who)
        {
            //  if (_trace)
            //     ADATrace.WriteLine(SerialiseRecords(claim));

            int totalScore = 0;
            int totalKA = 0;
            List<string> trackId = new List<string>();

            ScoreAnyClaim((IRiskEntity)claim.RiskEntityMap, "Incident", clientId, scoreWithLiveRules, trackId, ref totalScore, ref totalKA);

            ClaimScoreResults ret = new ClaimScoreResults()
            {
                CompleteScoreResults = claim.RiskEntityMap,
                TotalScore = totalScore,
                TotalKeyAttractorCount = totalKA
            };

            return (ClaimScoreResults)ret;
        }

        //public ClaimScoreResults ScoreClaim(int riskClaimId, int clientId, bool scoreWithLiveRules, string who)
        //{
        //    // get the whole claim from the database
        //    FullClaimToScore claim = FetchClaimForScoring(riskClaimId);

        //    //  if (_trace)
        //    //     ADATrace.WriteLine(SerialiseRecords(claim));

        //    return ScoreClaim(claim, clientId, scoreWithLiveRules, who);
        //}

        protected virtual void ScanPipelineRecord(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim newClaim, 
                                                Func<CurrentContext, MDA.Pipeline.Model.PipeEntityBase, int> ProcessEntity,
                                                Func<CurrentContext, MDA.Pipeline.Model.PipeLinkBase, int> ProcessLink)
        {
            throw new System.NotImplementedException("ScanPipelineRecord needs overriding in ClaimService");
        }

        public void TestScanPipelineRecord(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim)
        {
            switch (claim.ClaimType_Id)
            {
                case (int)MDA.Common.Enum.ClaimType.MobilePhone:
                case (int)MDA.Common.Enum.ClaimType.MobilePhoneAccidentalDamage:
                case (int)MDA.Common.Enum.ClaimType.MobilePhoneLoss:
                case (int)MDA.Common.Enum.ClaimType.MobilePhoneTheft:
                    ScanPipelineRecord(ctx, (MDA.Pipeline.Model.PipelineMobileClaim)claim, InitialisePipelineEntityRecord, InitialisePipelineLinkRecord);
                    break;
                case (int)MDA.Common.Enum.ClaimType.Motor:
                    ScanPipelineRecord(ctx, (MDA.Pipeline.Model.PipelineMotorClaim)claim, InitialisePipelineEntityRecord, InitialisePipelineLinkRecord);
                    break;
                default:
                    ScanPipelineRecord(ctx, (MDA.Pipeline.Model.PipelineMotorClaim)claim, InitialisePipelineEntityRecord, InitialisePipelineLinkRecord);
                    break;
            }
        }

        #region Process Claim for Mapping

        /// <summary>
        /// This is the callback method (delegate) that is called from within the Mapping code. This function is passed to mapping
        /// and is called for each CLAIM found in the batch file being processed by the S2-CREATE pipeline
        /// </summary>
        /// <param name="ctx">Current Context</param>
        /// <param name="newClaim">Claim to be loaded (serialised) into the RiskClaim table</param>
        /// <param name="o">Mapping is passed an OBJECT and this gets passed onto here. Normally used to track process (or something)</param>
        /// <returns>Returns 0 as a delegate FUNC<> call MUST return something</returns>
        public int ProcessClaimIntoRiskClaimTable(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim newClaim, object o)
        {
            switch (newClaim.ClaimType_Id)
            {
                case (int)MDA.Common.Enum.ClaimType.MobilePhone:
                case (int)MDA.Common.Enum.ClaimType.MobilePhoneAccidentalDamage:
                case (int)MDA.Common.Enum.ClaimType.MobilePhoneLoss:
                case (int)MDA.Common.Enum.ClaimType.MobilePhoneTheft:
                    return _ProcessClaimIntoRiskClaimTable(ctx, (MDA.Pipeline.Model.PipelineMobileClaim)newClaim, o);
                case (int)MDA.Common.Enum.ClaimType.Motor:
                    return _ProcessClaimIntoRiskClaimTable(ctx, (MDA.Pipeline.Model.PipelineMotorClaim)newClaim, o);
                default:
                    return _ProcessClaimIntoRiskClaimTable(ctx, (MDA.Pipeline.Model.PipelineMotorClaim)newClaim, o);
            }
        }


        private int _ProcessClaimIntoRiskClaimTable(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim, object o)
        {
            //MDA.Pipeline.Model.PipelineMotorClaim claim = newClaim as MDA.Pipeline.Model.PipelineMotorClaim;

            Stopwatch sw = Stopwatch.StartNew();

            using (var ctxx = new CurrentContext(ctx))
            {
                SetCurrentContext(ctxx);   // Assign this new context to THIS and THIS _services we already have

                ClaimCreationStatus claimCreationStatus = o as ClaimCreationStatus;

                claimCreationStatus.CountTotal++;

                ScanPipelineRecord(ctxx, claim, InitialisePipelineEntityRecord, InitialisePipelineLinkRecord);

                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Create : " + claim.ClaimNumber + " : ", false);

                if (_riskService.AlreadyHaveNewerVersionOfSameClaim(ctx.RiskClientId, claim.ClaimNumber, claim.IncidentDate))
                {
                    if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: Already have newer version of same claim.  Skip to next claim");
                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Skipped", true);

                    claimCreationStatus.CountSkipped++;

                    return 0;
                }

                ProcessingResults validationResults = null;

                #region VALIDATE
                if (_validationService != null)
                {
                    validationResults = _validationService.ValidateClaim(claim);

                    //Do NOT save results, this is done later
                    //riskServices.SaveValidationResults(riskBatchId, validationResults);

                    if (!validationResults.IsValid)
                    {
                        if (_trace)
                        {
                            ADATrace.WriteLine("ProcessBatchCreateClaims: FAILED validation [" + claim.ClaimNumber + "] Consecutive Errors = " + claimCreationStatus.ConsecutiveErrorCount.ToString());
                            ADATrace.DumpProcessingResults(validationResults);
                        }

                        if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "INVALID : ", false);

                        claimCreationStatus.ConsecutiveErrorCount++;
                        claimCreationStatus.CountErrors++;

                        if (claimCreationStatus.ConsecutiveErrorCount > 5)
                        {
                            if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "ABORTING (too many errors) ", true);

                            throw new TooManyErrorsException("Too many consecutive claims caused an error. Batch rejected");
                        }
                    }
                    else
                    {
                        if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: Passed validation [" + claim.ClaimNumber + "]");

                        if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Valid : ", false);
                    }
                }
                #endregion

                #region Detect Duplicates
                bool detectDuplicates = Convert.ToBoolean(ConfigurationManager.AppSettings["DetectAndSkipDuplicateClaims"]);

                if (_riskService.FindExistingIdenticalClaim(ctxx.RiskClientId, claim))
                {
                    if (detectDuplicates)
                    {
                        if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: Identical Claim found.  Skip to next claim");
                        if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Skipped : Done : " + sw.ElapsedMilliseconds + "ms", true);

                        claimCreationStatus.CountSkipped++;

                        return 0;
                    }
                    else
                    {
                        if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: Identical Claim found.  <<<<SKIPPING DISABLED>>>> so claim will be processed");
                        if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "NOT Skipped (skip disabled) : ", false);
                    }
                }
                #endregion

                #region Process Claim (Insert or Update)

                using (var transaction = ctxx.db.Database.BeginTransaction())
                {
                    try
                    {
                        //((IObjectContextAdapter)db).ObjectContext.Connection.Open();

                        int servicesMask = 0; // CreateServicesMask(ctxx.RiskClientId, claim);

                        if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: Processing Claim [" + claim.ClaimNumber + "]. ServiceMask [" + servicesMask.ToString() + "]");

                        RiskClient riskClient = _riskService.GetRiskClient(ctxx.RiskClientId);

                        string sourceRef = riskClient.ClientName.Substring(0, 3) + claim.ClaimNumber;

                        ctxx.SourceRef = sourceRef;

                        if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: Created sourceRef [" + sourceRef + "]");

                        int suppliedClaimStatus = (claim.ExtraClaimInfo != null) ? claim.ExtraClaimInfo.ClaimStatus_Id
                                                                                    : (int)MDA.Common.Enum.ClaimStatus.Unknown;

                        RiskClaim existingBaseRiskClaim = _riskService.FindExistingBaseClaim(ctxx.RiskClientId, claim.ClaimNumber);
                        if (existingBaseRiskClaim == null)
                        {
                            #region Insert NEW Claim (and validation results)
                            try
                            {
                                if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: New Claim : DOING INSERT");
                                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Inserting : ", false);

                                var newRiskClaim = _riskService.CreateRiskClaimRecord(claim, (MDA.Common.Enum.ClaimStatus)suppliedClaimStatus, claimCreationStatus.RiskBatchId, claim.ClaimNumber, sourceRef, servicesMask, validationResults, ctxx.Who);

                                if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: New Claim : INSERTED Claim Id [" + newRiskClaim.Id.ToString() + "]");

                                transaction.Commit();

                                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Done : " + sw.ElapsedMilliseconds + "ms", true);

                                claimCreationStatus.ConsecutiveErrorCount = 0;
                                claimCreationStatus.CountInsert++;

                            }
                            catch (System.Data.Entity.Validation.DbEntityValidationException)
                            {
                                claimCreationStatus.ConsecutiveErrorCount++;
                                claimCreationStatus.CountErrors++;

                                if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: FAILED to insert claim. Consecutive Errors = " + claimCreationStatus.ConsecutiveErrorCount.ToString());

                                throw;
                            }
                            #endregion
                        }
                        else
                        {
                            #region UPDATE existing claim (and validation results)
                            try
                            {
                                if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: Existing Claim : DOING UPDATE Claim Id [" + existingBaseRiskClaim.Id.ToString() + "]");
                                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, ": Updating ", false);

                                var newRiskClaim = _riskService.CreateRiskClaimUpdateRecord(claim, (MDA.Common.Enum.ClaimStatus)suppliedClaimStatus, existingBaseRiskClaim.Id, claimCreationStatus.RiskBatchId, existingBaseRiskClaim.Incident_Id, claim.ClaimNumber, sourceRef, servicesMask, validationResults, ctxx.Who);

                                if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: Updated Claim : Created Claim Id [" + newRiskClaim.Id.ToString() + "]");

                                transaction.Commit();

                                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, ": Done : " + sw.ElapsedMilliseconds + "ms", true);

                                claimCreationStatus.ConsecutiveErrorCount = 0;
                                claimCreationStatus.CountUpdate++;
                            }
                            catch (System.Data.Entity.Validation.DbEntityValidationException)
                            {
                                claimCreationStatus.ConsecutiveErrorCount++;
                                claimCreationStatus.CountErrors++;

                                if (_trace) ADATrace.WriteLine("ProcessBatchCreateClaims: FAILED to update claim. Consecutive Errors = " + claimCreationStatus.ConsecutiveErrorCount.ToString());

                                throw;
                            }
                            #endregion
                        }
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                }


                if (claimCreationStatus.ConsecutiveErrorCount > 5)
                {
                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, ": Too many errors, batch rejected", true);

                    throw new TooManyErrorsException("Too many consecutive claims caused an error. Batch rejected");
                }
            }
                #endregion Process Claim (Insert or Update)

            SetCurrentContext(_ctx);

            return 0;
        }

        #endregion

        protected void CleanUpLinksAfterLoading(RiskClaim newRiskClaim)
        {
            if (_trace)
            {
                ADATrace.WriteLine("Clean up old links : Starting");
                ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
            }

            DataServices dataServices = new DataServices(_ctx);

            //dataServices.CleanUpIncident2IncidentLinks(newRiskClaim);

            dataServices.CleanUpIncident2OrganisationLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpIncident2OrganisationLinks : Done");

            dataServices.CleanUpIncident2PersonLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpIncident2PersonLinks : Done");

            dataServices.CleanUpIncident2VehicleLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpIncident2VehicleLinks : Done");

            dataServices.CleanUpIncident2HandsetLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpIncident2HandsetLinks : Done");


            dataServices.CleanUpOrganisation2AddressLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpOrganisation2AddressLinks : Done");

            dataServices.CleanUpOrganisation2PolicyLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpOrganisation2PolicyLinks : Done");

            dataServices.CleanUpOrganisation2PaymentCardLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpOrganisation2PaymentCardLinks : Done");

            dataServices.CleanUpOrganisation2EmailLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpOrganisation2EmailLinks : Done");

            dataServices.CleanUpOrganisation2WebsiteLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpOrganisation2WebsiteLinks : Done");

            dataServices.CleanUpOrganisation2TelephoneLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpOrganisation2TelephoneLinks : Done");

            //dataServices.CleanUpOrganisation2OrganisationLinks(newRiskClaim);
            //if (_trace) ADATrace.WriteLine("CleanUpIncident2OrganisationLinks : Done");

            dataServices.CleanUpOrganisation2BankAccountLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpOrganisation2BankAccountLinks : Done");


            dataServices.CleanUpPerson2AddressLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPerson2AddressLinks : Done");

            dataServices.CleanUpPerson2BankAccountLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPerson2BankAccountLinks : Done");

            dataServices.CleanUpPerson2PaymentCardLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPerson2PaymentCardLinks : Done");

            dataServices.CleanUpPerson2EmailLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPerson2EmailLinks : Done");

            dataServices.CleanUpPerson2PassportLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPerson2PassportLinks : Done");

            dataServices.CleanUpPerson2NINumberLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPerson2NINumberLinks : Done");

            dataServices.CleanUpPerson2DrivingLicenseLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPerson2DrivingLicenseLinks : Done");

            dataServices.CleanUpPerson2TelephoneLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPerson2TelephoneLinks : Done");

            dataServices.CleanUpPerson2OrganisationLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPerson2OrganisationLinks : Done");

            //dataServices.CleanUpPerson2PersonLinks(newRiskClaim);
            dataServices.CleanUpPerson2PolicyLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPerson2PolicyLinks : Done");

            //dataServices.CleanUpPerson2WebSiteLinks(newRiskClaim);

            dataServices.CleanUpPolicy2BankAccountLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPolicy2BankAccountLinks : Done");

            dataServices.CleanUpPolicy2PaymentCardLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpPolicy2PaymentCardLinks : Done");


            dataServices.CleanUpHandset2AddressLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpHandset2AddressLinks : Done");

            dataServices.CleanUpOrgHandset2OrganisationLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpOrgHandset2OrganisationLinks : Done");

            dataServices.CleanUpHandset2PersonLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpHandset2PersonLinks : Done");

            dataServices.CleanUpHandset2PolicyLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpHandset2PolicyLinks : Done");

            dataServices.CleanUpOrgVehicle2OrganisationLinks(newRiskClaim);
            if (_trace) ADATrace.WriteLine("CleanUpOrgVehicle2OrganisationLinks : Done");

            //dataServices.CleanUpVehicle2VehicleLinks(newRiskClaim);

            if (_trace)
            {
                ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                ADATrace.WriteLine("Clean up old links : Complete");
            }
        }
    }
}
