﻿using System.Collections.Generic;
using System.ComponentModel;

namespace MDA.MatchingService.Model
{
    public enum MatchType
    {
        NotFound = -1,
        Confirmed = 0,
        Unconfirmed = 1,
        Tentative = 2
    };

    public class MatchResults
    {
        //[Browsable(false)] 
        //public bool PAFAddress { get; set; }
        public List<int> MatchIdList { get; set; }

        public List<string> MatchSanctionsList { get; set; }

        [Browsable(false)] 
        public object ModifiedEntity { get; set; }
        public MatchType MatchTypeFound { get; set; }
        public int? MatchRuleNumber { get; set; }

        public MatchResults(object entityToMatch)
        {
            //MatchAudit = null;
            //PAFAddress = false;
            MatchIdList = new List<int>();
            MatchTypeFound = MatchType.NotFound;
            ModifiedEntity = entityToMatch;
            MatchRuleNumber = -1;
        }
    }
}
