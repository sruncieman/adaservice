﻿using System.Collections.Generic;
using MDA.Pipeline.Model;
using MDA.MatchingService.Model;
using MDA.DAL;
using MDA.Common.Server;

namespace MDA.MatchingService.Interface
{
    public interface IMatchingService
    {
        MatchResults MatchClaim(PipelineMotorClaim incident, int? incidentId, int? existingBaseRiskClaimId);
        MatchResults MatchClaim(PipelinePIClaim incident, int? incidentId, int? existingBaseRiskClaimId);
        MatchResults MatchClaim(PipelineMobileClaim incident, int? incidentId, int? existingBaseRiskClaimId);
        MatchResults MatchEmailAddress(MDA.Pipeline.Model.PipelineEmail pipeEmail, int? existingBaseRiskClaimId);
        MatchResults MatchPolicy(PipelinePolicy policy, int? IncidentId, int? existingBaseRiskClaimId);
        MatchResults MatchPassportNumber(MDA.Pipeline.Model.PipelinePassport passportNumber, int? existingBaseRiskClaimId);
        MatchResults MatchNINumber(MDA.Pipeline.Model.PipelineNINumber NINumber, int? existingBaseRiskClaimId);
        MatchResults MatchLicenseNumber(MDA.Pipeline.Model.PipelineDrivingLicense licenseNumber, int? existingBaseRiskClaimId);
        MatchResults MatchWebSite(MDA.Pipeline.Model.PipelineWebSite webSite, int? existingBaseRiskClaimId);
        MatchResults MatchTelephoneNumber(PipelineTelephone telephoneNumber, int? existingBaseRiskClaimId);
        MatchResults MatchBankAccount(PipelineBankAccount bankAccount, int? existingBaseRiskClaimId);
        MatchResults MatchPaymentCard(PipelinePaymentCard paymentCard, int? existingBaseRiskClaimId);
        MatchResults MatchPerson(PipelinePerson person, PipelineVehicle vehicle, IPipelineClaim claim, int? existingBaseRiskClaimId);
        MatchResults MatchPerson(PipelinePerson person, PipelineHandset handset, IPipelineClaim claim, int? existingBaseRiskClaimId);
        MatchResults MatchVehicle(PipelineVehicle vehicle, List<PipelineAddress> addresses, int? existingBaseRiskClaimId);
        MatchResults MatchHandset(PipelineHandset handset, List<PipelineAddress> addresses, int? existingBaseRiskClaimId);
        MatchResults MatchHandsetIMEI(PipelineHandset handset, int? existingBaseRiskClaimId);
        MatchResults MatchAddress(PipelineAddress address, string orgName, int? existingBaseRiskClaimId);
        MatchResults MatchOrganisation(PipelineOrganisation organisation, int? existingBaseRiskClaimId);

        MatchResults MatchSanctionsOrganisation(PipelineOrganisation organisation, int? existingBaseRiskClaimId);
        MatchResults MatchSanctionsPerson(PipelinePerson person, int? existingBaseRiskClaimId);

        MatchResults MatchExistingPerson(int personId, int? existingBaseRiskClaimId);
        MatchResults MatchExistingVehicle(int vehicleId, int? existingBaseRiskClaimId);
        MatchResults MatchExistingOrganisation(int orgId, int? existingBaseRiskClaimId);

        MatchResults MatchExistingBankAccount(int accountId, int? existingRiskClaimId);
        MatchResults MatchExistingPaymentCard(int cardId, int? existingRiskClaimId);
        MatchResults MatchExistingPolicy(int policyId, int? existingRiskClaimId);
        MatchResults MatchExistingClaim(int incidentId, int? existingBaseRiskClaimId);
        MatchResults MatchExistingAddress(int addressId, int? existingBaseRiskClaimId);

        CurrentContext CurrentContext { set; }
    }
}
