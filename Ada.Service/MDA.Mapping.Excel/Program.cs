﻿using System;
using System.IO;
using System.Runtime.Serialization;
using MDA.Pipeline.Model;
using MDA.MappingService.Excel.Motor;
using MDA.Common;
using MDA.Common.Server;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Xml;

namespace MDA.Mapping.Excel
{
    class Program
    {
        static PipelineClaimBatch motorClaimBatch = new PipelineClaimBatch();

        private static int ProcessClaimIntoDb(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim, object o)
        {
            motorClaimBatch.Claims.Add((PipelineMotorClaim)claim);

            return 0;
        }

        static void Main(string[] args)
        {
            Stream mockStream = null;

            CurrentContext ctx = new CurrentContext(0, 1, "Test");

            Console.WriteLine("Start");

            ProcessingResults pr;

            new FileMapping().ConvertToClaimBatch(ctx, mockStream, null, ProcessClaimIntoDb, out pr);

            //string xml = WriteToXml();

            Console.ReadLine();
        }

        private static void WriteToXmlOriginal(PipelineClaimBatch batch)
        {
            string strXmlFileFullSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Excel\XmlOriginal\ExcelFull.xml";
            var ser = new DataContractSerializer(batch.GetType());

            FileStream fs = new FileStream(strXmlFileFullSave, FileMode.Create);
            ser.WriteObject(fs, batch);

            fs.Close();
        }

        private static string WriteToXml()
        {
            string strXmlFileFullSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Excel\Xml\ExcelFull.xml";

            string xml = null;

            var ms = new MemoryStream();

            var ser = new XmlSerializer(typeof(MDA.Pipeline.Model.PipelineClaimBatch));

            ser.Serialize(ms, motorClaimBatch);

            ms.Position = 0;

            var sr = new StreamReader(ms);

            xml = sr.ReadToEnd();

            TextWriter WriteFileStream = new StreamWriter(strXmlFileFullSave);
            ser.Serialize(WriteFileStream, motorClaimBatch);

            WriteFileStream.Close();
            return xml;
        }

        private static void WriteToJson(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            string json = JsonConvert.SerializeXmlNode(doc);

            System.IO.File.WriteAllText(@"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Co-Op\Json\ExcelFull.txt", json);
        }

    }
}