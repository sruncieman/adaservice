﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using MDA.Common;
using MDA.Common.Server;
using MDA.MappingService.Impl.Mulsanne.Motor;
using MDA.Pipeline.Model;

namespace MDA.Mapping.Mulsanne
{
    internal class Program
    {
        private static readonly List<IPipelineClaim> MotorClaimBatch = new List<IPipelineClaim>();

        private static int ProcessClaimIntoDb(CurrentContext ctx, IPipelineClaim claim, object o)
        {
            MotorClaimBatch.Add(claim);

            return 0;
        }
        static void Main(string[] args)
        {

            var ctx = new CurrentContext(0, 1, "Test");

            Console.WriteLine("Start");

            ProcessingResults pr;

            new FileMapping().ConvertToClaimBatch(ctx, null, null, ProcessClaimIntoDb, out pr);

            var xml = WriteToXml();

            Console.ReadLine();
        }


        private static string WriteToXml()
        {            
            const string strXmlFileFullSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Mulsanne\XML\";

            string xml = null;

            foreach (var item in MotorClaimBatch)
            {

                Console.WriteLine(item.ClaimNumber);

                var ms = new MemoryStream();

                var ser = new XmlSerializer(typeof(PipelineMotorClaim));

                ser.Serialize(ms, item);

                ms.Position = 0;

                var sr = new StreamReader(ms);

                xml = sr.ReadToEnd();

                TextWriter writeFileStream = new StreamWriter(strXmlFileFullSave + item.ClaimNumber + ".xml");
                ser.Serialize(writeFileStream, item);

                writeFileStream.Close();

            }

            return xml;
        }
    }
}
