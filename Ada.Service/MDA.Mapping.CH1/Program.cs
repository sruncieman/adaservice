﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using MDA.Pipeline.Model;
using MDA.MappingService.CH1.Motor;
using MDA.Common;
using MDA.Common.Server;

namespace MDA.Mapping.CH1
{
    class Program
    {
        static PipelineClaimBatch motorClaimBatch = new PipelineClaimBatch();

        private static int ProcessClaimIntoDb(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim, object o)
        {
            motorClaimBatch.Claims.Add((PipelineMotorClaim)claim);

            return 0;
        }

        static void Main(string[] args)
        {
            //string strFile = @"C:\Dev\Projects\MDA\MDASolution\MDA.Mapping.CH1\Resource\Example Zip File.zip";
                                  //C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource
            string clientFolder = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\CH1\Resource\";

            //using (ZipFile zip1 = ZipFile.Read(strFile))
            //{
            //    foreach (ZipEntry e in zip1)
            //    {
            //        string unpackDirectory = clientFolder;
            //        //filePath = clientFolder + @"\" + e.FileName;
            //        e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
            //    }
            //}
            
            GetMotorClaimBatch claimBatch = new GetMotorClaimBatch();
            Stream mockStream = null;

            CurrentContext ctx = new CurrentContext(0, 1, "Test");

            ProcessingResults pr;

            new FileMapping().ConvertToClaimBatch(ctx, mockStream, null, ProcessClaimIntoDb, out pr);

            //PipelineClaimBatch motorClaimBatch = claimBatch.RetrieveMotorClaimBatch(mockStream, null, false);

            Stopwatch stopwatch = new Stopwatch();

            string strXmlFileSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\CH1\Xml\CH1Xml.xml";
            //string strXmlFileSave = @"C:\Dev\Projects\MDA\MDASolution\MDA.Mapping.FirstCentral\Xml\1stCentral.xml";

            using (var writer = new FileStream(strXmlFileSave, FileMode.Create))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(PipelineClaimBatch));
                Console.WriteLine("Serializing to XML");
                ser.WriteObject(writer, motorClaimBatch);
            }
            stopwatch.Stop();
            GetMotorClaimBatch.GetTimeSpan(stopwatch);
        }
    }
}
