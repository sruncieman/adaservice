﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using MDA.Pipeline.Model;
using MDA.RiskService.Model;
using MDA.Common;

namespace ADAServices.Interface
{
    public interface IADAServices
    {
        void UploadBatchOfClaimsFile(Stream sourceStream, string batchEntityType, string fileName, string mimeType, int clientId,
            int userId, string clientBatchReference, string who);

        //Byte[] GetReportBytes(int riskClaimId);
        //Stream GetReportStream(int riskClaimId);
        //Stream GetBatchReportStream(int riskBatchId, string reportFormat);

        Stream GetLevel1ReportStream(GetLevel1ReportStreamParam param);
        Stream GetLevel2ReportStream(GetLevel2ReportStreamParam param);
        Stream GetBatchReportStream(GetBatchReportStreamParam param);
        Stream GetCueReportStream(GetCueReportStreamParam param);
        byte[] GenerateBatchReport(GenerateBatchReportParam param);
        byte[] GenerateLevel1Report(GenerateLevel1ReportParam param);
        byte[] GenerateLevel2Report(GenerateLevel2ReportParam param);


        ProcessClaimBatchResult ProcessClaimBatch(ProcessClaimBatchParam param);

        ProcessMobileClaimBatchResult ProcessMobileClaimBatch(ProcessMobileClaimBatchParam param);

        ProcessClaimResult ProcessClaim(ProcessClaimParam param);

        ClaimsForClientResult GetClaimsForClient(ClaimsForClientParam param);

        GetListOfBatchPriorityClientsResult GetListOfBatchPriorityClients(GetListOfBatchPriorityClientsParam param);

        SaveBatchPriorityClientsResult SaveBatchPriorityClients(SaveBatchPriorityClientsParam param);


        //ClaimsForUserResult GetClaimsForUser(ClaimsForUserParam param);

        //SingleClaimForUserResult GetSingleClaimForUser(SingleClaimForUserParam param);

        RegisterNewClientResult RegisterNewClient(RegisterNewClientParam param);

        RequestLevelOneReportResult RequestLevelOneReport(RequestLevelOneReportParam param);

        RequestLevelTwoReportResult RequestLevelTwoReport(RequestLevelTwoReportParam param);

        RequestCallbackResult RequestCallback(RequestCallbackParam param);

        ClearCallbackRequestResult ClearCallbackRequest(ClearCallbackRequestParam param);

        SetClaimReadStatusResult SetClaimReadStatus(SetClaimReadStatusParam param);

        GetBatchStatusResult GetBatchStatus(GetBatchStatusParam param);
        GetBatchProcessingResultsResult GetBatchProcessingResults(GetBatchProcessingResultsParam param);

        IsClientBatchReferenceUniqueResult IsClientBatchReferenceUnique(IsClientBatchReferenceUniqueParam param);

        IsClientNameUniqueResult IsClientNameUnique(IsClientNameUniqueParam param);

        IsPasswordUniqueResult IsPasswordUnique(IsPasswordUniqueParam param);

        IsUserNameUniqueResult IsUserNameUnique(IsUserNameUniqueParam param);


        GetRiskUserInfoResult GetRiskUserInfo(GetRiskUserInfoParam param);

        #region File Editor Methods

        BatchListResult GetClientBatchList(BatchListParam param);

        ClaimsInBatchResult GetClaimsInBatch(ClaimsInBatchParam param);

        GetClaimScoreDataResult GetClaimScoreData(GetClaimScoreDataParam param);

        #endregion

        #region membership 

        int GetPasswordFailuresSinceLastSuccess(GetPasswordFailuresSinceLastSuccessParam param);
        int GetUserId(GetUserIdParam param);
        bool IsUserInRole(IsUserInRoleParam param);
        string[] GetRolesForUser(GetRolesForUserParam param);
        bool ValidateUser(ValidateUserParam param);
        void CreateUser(CreateUserParam param);
        void CreateClient(CreateClientParam param);
        void UpdateClient(UpdateClientParam param);
        void UpdateUser(UpdateUserParam param);
        void CreateRiskUser2Client(CreateRiskUser2ClientParam param);
        void DeleteRiskUser2Client(DeleteRiskUser2ClientParam param);
        bool ChangePassword(ChangePasswordParam param);

        #endregion

        #region WebSite Methods

        void CreateRiskWord(CreateRiskWordParam param);

        void CreateRiskDefaultData(CreateRiskDefaultDataParam param);

        void CreateRiskNoteDecision(CreateRiskNoteDecisionParam param);

        void DeleteRiskDefaultData(DeleteRiskDefaultDataParam param);

        void DeleteRiskNoteDecision(DeleteRiskNoteDecisionParam param);

        /// <summary>
        /// Get a list of all risk clients
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskClientsResult GetRiskClients(RiskClientsParam param);
                
        /// <summary>
        /// Get a list of all risk words
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskWordsResult GetRiskWords(RiskWordsParam param);

        /// <summary>
        /// Get a list of all risk notes
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskNotesResult GetRiskNotes(RiskNotesParam param);

        /// <summary>
        /// Get a risk note by Id
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        GetRiskNoteResult GetRiskNote(GetRiskNoteParam param);

        /// <summary>
        /// Get a list of all risk decisions
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskNoteDecisionsResult GetRiskNoteDecisions(RiskNoteDecisionsParam param);

        /// <summary>
        /// Delete risk word
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskWordDeleteResult DeleteRiskWord(RiskWordDeleteParam param);

        /// <summary>
        /// Create risk note
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskNoteCreateResult CreateRiskNote(RiskNoteCreateParam param);

        /// <summary>
        /// Create risk note
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskNoteUpdateResult UpdateRiskNote(RiskNoteUpdateParam param);

        /// <summary>
        /// Get a list of all risk default data
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskDefaultDataResult GetRiskDefaultData(RiskDefaultDataParam param);

        /// <summary>
        /// Get a list of all assigned risk clients for a user
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        GetAssignedRiskClientsForUserResult GetAssignedRiskClientsForUser(GetAssignedRiskClientsForUserParam param);
        

        /// <summary>
        /// Get a list of all risk roles
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskRolesResult GetRiskRoles(RiskRolesParam param);

        /// <summary>
        /// Get a list of all risk roles
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        InsurersClientsResult GetInsurersClients(InsurersClientsParam param);

        /// <summary>
        /// Get a list of all locked user accounts
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskUsersLockedResult GetRiskUsersLocked(RiskUsersLockedParam param);

        /// <summary>
        /// Unlock risk user by risk user id
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskUserUnlockResult UnlockRiskUser(RiskUserUnlockParam param);


        /// <summary>
        /// Generate a Password Reset Token
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        GeneratePasswordResetTokenResult GeneratePasswordResetToken(GeneratePasswordResetTokenParam param);

        /// <summary>
        /// Validate a Password Reset Token
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        ValidatePasswordResetTokenResult ValidatePasswordResetToken(ValidatePasswordResetTokenParam param);


        /// <summary>
        /// Get a list of all users
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        RiskUsersResult GetRiskUsers(RiskUsersParam param);


        SendElmahErrorResult SendElmahEmail(SendElmahErrorParam param);




        /// <summary>
        /// Get a list of all batches based on the passed in params
        /// Used to display the list of batches on website batch upload page.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        WebsiteBatchListResult GetBatchList(WebsiteBatchListParam param);

        /// <summary>
        /// Get a list of single claims based on the passed in params.
        /// Used to display a summary list of single claims on website outstanding claims page.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        WebsiteSingleClaimsListResult GetSingleClaims(WebsiteSingleClaimsListParam param);

        WebsiteSaveSingleClaimResult SaveSingleClaim(WebsiteSaveSingleClaimParam param);

        WebsiteGetTotalSingleClaimsForClientResult GetTotalSingleClaimForClient(
            WebsiteGetTotalSingleClaimsForClientParam param);

        WebsiteFetchSingleClaimDetailResult FetchSingleClaimDetail(WebsiteFetchSingleClaimDetailParam param);

        WebsiteDeleteSingleClaimResult DeleteSingleClaim(WebsiteDeleteSingleClaimParam param);

        WebsiteFilterBatchNumbersResult FilterBatchNumbers(WebsiteFilterBatchNumbersParam param);

        WebSearchResultsResult FindSearchResultDetails(WebSearchResultsParam param);

        GetBatchQueueResult GetBatchQueue(GetBatchQueueParam param);

        SaveOverriddenBatchPriorityResult SaveOverriddenBatchPriority(SaveOverriddenBatchPriorityParam param);
       

        #region CUE
        WebsiteCueInvolvementsResult GetCueInvolvements(WebsiteCueInvolvementsParam param);

        WebsiteCueSearchResult SubmitCueSearch(WebsiteCueSearchParam param);

        WebsiteCueReportResult GetCueReport(WebsiteCueReportParam param);

        WebsiteSaveTemplateFunctionsResult SaveTemplateFunctions(WebsiteSaveTemplateFunctionsParam param);

        WebsiteLoadTemplateFunctionsResult LoadTemplateFunctions(WebsiteLoadTemplateFunctionsParam param);


        WebsiteRiskRoleCreateResult CreateRiskRole(WebsiteRiskRoleCreateParam param);

        WebsiteRiskRoleEditResult EditRiskRole(WebsiteRiskRoleEditParam param);

        WebsiteRiskRoleDeleteResult DeleteRiskRole(WebsiteRiskRoleDeleteParam param);

        FindUserNameResult FindUserName(FindUserNameParam param);
        #endregion


        #endregion

        #region Security

        bool CheckUserBelongsToClient(CheckUserBelongsToClientParam param);
        bool CheckUserHasAccessToReport(CheckUserHasAccessToReportParam param);

        #endregion





        
    }

    //public class EEntityScoreMessageList
    //{
    //    public string EntityHeader { get; set; }
    //    public List<string> Messages { get; set; }
    //}

    //public class ERiskClaimDetails
    //{
    //    public int RiskBatch_Id { get; set; }
    //    public string BatchReference { get; set; }
    //    public string ClientBatchReference { get; set; }
    //    public int BatchStatus { get; set; }
    //    public string BatchStatusAsString { get; set; }

    //    public int RiskClaim_Id { get; set; }
    //    public int BaseRiskClaim_Id { get; set; }
    //    public string MDAClaimRef { get; set; }
    //    public DateTime CreatedDate { get; set; }
    //    public string CreatedBy { get; set; }
    //    public string ClientClaimRefNumber { get; set; }
    //    public int LevelOneRequestedCount { get; set; }
    //    public int LevelTwoRequestedCount { get; set; }
    //    public DateTime? LevelOneRequestedWhen { get; set; }
    //    public DateTime? LevelTwoRequestedWhen { get; set; }
    //    public string LevelOneRequestedWho { get; set; }
    //    public string LevelTwoRequestedWho { get; set; }
    //    public int ClaimStatus { get; set; }
    //    public int ClaimReadStatus { get; set; }
    //    public string ClaimStatusAsString { get; set; }

    //    public MessageNode ValidationResults { get; set; }
    //    public MessageNode CleansingResults { get; set; }

    //    public int Incident_Id { get; set; }
    //    public DateTime IncidentDate { get; set; }

    //    public decimal? PaymentsToDate { get; set; }
    //    public decimal? Reserve { get; set; }
    //    public int RiskClaimRun_Id { get; set; }
    //    public int TotalScore { get; set; }
    //    public int TotalKeyAttractorCount { get; set; }

    //    public string RiskRatingAsString { get; set; }

    //    public List<EEntityScoreMessageList> MessageList { get; set; }
    //}


    #region File Editor Messages

    //public class ERiskClaim
    //{

    //    public int Id { get; set; }

    //    public int RiskBatch_Id { get; set; }

    //    public int Incident_Id { get; set; }

    //    public string MdaClaimRef { get; set; }

    //    public int BaseRiskClaim_Id { get; set; }

    //    public int ClaimStatus { get; set; }

    //    public int ClaimReadStatus { get; set; }

    //    public string ClientClaimRefNumber { get; set; }

    //    public int LevelOneRequestedCount { get; set; }

    //    public int LevelTwoRequestedCount { get; set; }

    //    public Nullable<System.DateTime> LevelOneRequestedWhen { get; set; }

    //    public Nullable<System.DateTime> LevelTwoRequestedWhen { get; set; }

    //    public string LevelOneRequestedWho { get; set; }

    //    public string LevelTwoRequestedWho { get; set; }

    //    public string SourceReference { get; set; }
    //    //public string SourceEntityXML { get; set; }

    //    public System.DateTime CreatedDate { get; set; }

    //    public string CreatedBy { get; set; }

    //    public int TotalScore { get; set; }

    //    public MessageNode ValidationResults { get; set; }

    //    public MessageNode CleansingResults { get; set; }
    //}

    //public class ERiskClient
    //{
    //    public int Id { get; set; }
    //    public string ClientName { get; set; }
    //    public int AmberThreshold { get; set; }
    //    public int RedThreshold { get; set; }
    //}

    //public class ERiskTeam
    //{
    //    public int Id { get; set; }
    //    public string TeamName { get; set; }
    //}

    //public class ERiskUser
    //{
    //    public int Id { get; set; }
    //    public string UserName { get; set; }
    //    public string FirstName { get; set; }
    //    public string LastName { get; set; }
    //    public string TelephoneNumber { get; set; }
    //    public int RiskClientId { get; set; }
    //    public string RiskClientName { get; set; }
    //    public int RiskTeamId { get; set; }
    //    public string RiskTeamName { get; set; }
    //    public int RiskRoleId { get; set; }
    //    public string RiskRoleName { get; set; }
    //}

    //public class ERiskClaimRun
    //{

    //    public int Id { get; set; }

    //    public int RiskClaim_Id { get; set; }

    //    public string EntityType { get; set; }
    //    //public string ScoreEntityJson { get; set; }
    //    //public string ScoreEntityXml { get; set; }

    //    public int TotalScore { get; set; }

    //    public int TotalKeyAttractorCount { get; set; }
    //    //public string ReportOneVersion { get; set; }
    //    //public string ReportTwoVersion { get; set; }

    //    public string ExecutedBy { get; set; }

    //    public System.DateTime ExecutedWhen { get; set; }


    //    public List<EEntityScoreMessageList> MessageList { get; set; }
    //}

    #endregion

    public class GenerateBatchReportParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int RiskBatchId { get; set; }
    }

    public class GenerateLevel1ReportParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int RiskClaimId { get; set; }
    }

    public class GenerateLevel2ReportParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int RiskClaimId { get; set; }
    }

    public class ProcessClaimBatchParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string BatchEntityType { get; set; }

        public string ClientBatchReference { get; set; }
        public MDA.Common.FileModel.MotorClaimBatch Batch { get; set; }

    }

    public class ProcessMobileClaimBatchParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string BatchEntityType { get; set; }

        public string ClientBatchReference { get; set; }
        public MDA.Common.FileModel.MobileClaimBatch Batch { get; set; }

    }

    public class ProcessClaimBatchResult
    {
        public ProcessingResults Results { get; set; }
        public int RiskClaimId { get; set; }
    }

    public class ProcessMobileClaimBatchResult
    {
        public ProcessingResults Results { get; set; }
        public int RiskClaimId { get; set; }
    }

    public class ProcessClaimParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string BatchEntityType { get; set; }

        public string ClientBatchReference { get; set; }
        public MDA.Common.FileModel.MotorClaim Claim { get; set; }
    }

    public class ProcessClaimResult
    {
        public ProcessingResults Results { get; set; }
        public int RiskClaim_Id { get; set; }
    }

    public class ClaimsForClientParam
    {
        public string Who { get; set; }
        public int ClientId { get; set; }
        public int UserId { get; set; }

        /// <summary>
        /// The client ID. If set to -1 all claims for all clients returned
        /// </summary>
        public int FilterClientId { get; set; }

        /// <summary>
        /// The team ID. If set to -1 and client ID is specified all claims 
        /// for the specified client are returned.
        /// </summary>
        public int TeamId { get; set; }

        /// <summary>
        /// The user ID. If set to -1 and team id is specified, all claims for
        /// the specified team are returned.
        /// </summary>
        public int FilterUserId { get; set; }

        /// <summary>
        /// Set to NULL to not apply filter.  Set to a NoneEmpty string to use as filter on RiskClaim.CreatedBy
        /// </summary>
        public string FilterUploadedBy { get; set; }

        /// <summary>
        /// Set to NULL to not apply filter.  Set to a NoneEmpty string to use as filter on RiskClaim.BatchReference
        /// </summary>
        public string[] FilterBatchRef { get; set; }

        /// <summary>
        /// Set to NULL to not apply filter.  Set to a NoneEmpty string to use as filter on RiskClaim.ClientClaimRefNumber
        /// </summary>
        public string FilterClaimNumber { get; set; }

        /// <summary>
        /// Set to NULL to not apply filter.  Set to a DateTime to use as filter on RiskClaim.IncidentDate >= FilterStartDate
        /// </summary>
        public DateTime? FilterStartDate { get; set; }

        /// <summary>
        /// Set to NULL to not apply filter.  Set to a DateTime to use as filter on RiskClaim.IncidentDate <= FilterEndDate
        /// </summary>
        public DateTime? FilterEndDate { get; set; }

        /// <summary>
        /// Set to true to return all high risk claims
        /// </summary>
        public bool FilterHighRisk { get; set; }

        /// <summary>
        /// Set to true to return all medium risk claims
        /// </summary>
        public bool FilterMediumRisk { get; set; }

        /// <summary>
        /// Set to true to return all low risk claims
        /// </summary>
        public bool FilterLowRisk { get; set; }

        /// <summary>
        /// Set to true to return all Keoghs CFS claims
        /// </summary>
        public bool FilterKeoghsCFS { get; set; }

        /// <summary>
        /// Set to true to return all previous claims
        /// </summary>
        public bool FilterPreviousVersion { get; set; }

        /// <summary>
        /// Set to true to return all error claims
        /// </summary>
        public bool FilterError { get; set; }

        /// <summary>
        /// Set to true to return all unread claims
        /// </summary>
        public bool FilterStatusUnread { get; set; }
       
        /// <summary>
        /// Set to true to return all read claims
        /// </summary>
        public bool FilterStatusRead { get; set; }
     
        /// <summary>
        /// Set to true to return all unread claims with score change
        /// </summary>
        public bool FilterStatusUnreadScoreChanges { get; set; }

        public bool FilterStatusReserveChange { get; set; }
     
        /// <summary>
        /// Set to true to return all unrequested reports
        /// </summary>
        public bool FilterStatusUnrequestedReports { get; set; }
     
        /// <summary>
        /// Set to true to return all pending reports
        /// </summary>
        public bool FilterStatusPendingReports { get; set; }
       
        /// <summary>
        /// Set to true to return all available reports
        /// </summary>
        public bool FilterStatusAvailableReports { get; set; }

        /// <summary>
        /// Set to true to return all claims with no reports requested
        /// </summary>
        public bool FilterNoReportsRequested { get; set; }

        /// <summary>
        /// Set to true to return all claims with level 1 reports requested
        /// </summary>
        public bool FilterLevel1ReportsRequested { get; set; }

        /// <summary>
        /// Set to true to return all claims with level 2 reports requested
        /// </summary>
        public bool FilterLevel2ReportsRequested { get; set; }

        /// <summary>
        /// Set to true to return all claims with callbacks requested
        /// </summary>
        public bool FilterCallbackReportsRequested { get; set; }

        /// <summary>
        /// Set to true to return all claims with notes
        /// </summary>
        public bool FilterNotes { get; set; }

        /// <summary>
        /// Set to true to return all claims with intel attachments
        /// </summary>
        public bool FilterIntel { get; set; }

        /// <summary>
        /// Set to true to return all claims with decisions
        /// </summary>
        public bool FilterDecisions { get; set; }

        /// <summary>
        /// Set to return all claims with matching decision id
        /// </summary>
        public int?[] FilterDecisionIds { get; set; }

        /// <summary>
        /// Can be one of "INCIDENTDATE", "CLIENTCLAIMREFNUMBER", "CLIENTBATCHREFERENCE", 
        /// "KEYATTRACTORCOUNT", "RESERVE", "PAYMENTSTODATE", "UPLOADDATETIME"
        /// </summary>
        public string SortColumn { get; set; }

        /// <summary>
        /// True if the sort order is ASC otherwise false
        /// </summary>
        public bool SortAscending { get; set; }

        /// <summary>
        /// Page number required. Starts at 1
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Number of rows in a page
        /// </summary>
        public int PageSize { get; set; }
    }

    public class ClaimsForClientResult
    {
        public ClaimsForClientResult()
        {
            ClaimsList = new List<ERiskClaimDetails>();
        }

        public List<ERiskClaimDetails> ClaimsList { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Total rows in ResultSet (not the number of rows in the page)
        /// </summary>
        public int TotalRows { get; set; }
    }

    public class ClaimsForUserParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class SingleClaimForUserParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int EditClaimId { get; set; }
    }

    public class SingleClaimForUserResult
    {
        /// <summary>
        /// Claim to Edit
        /// </summary>
        public ERiskClaimEdit Claim { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        public string Error { get; set; }
    }

    public class ClaimsForUserResult
    {
        public ClaimsForUserResult()
        {
            ClaimsList = new List<ERiskClaimEdit>();
        }

        public List<ERiskClaimEdit> ClaimsList { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Total rows in ResultSet (not the number of rows in the page)
        /// </summary>
        public int TotalRows { get; set; }
    }

    public class SaveClaimForUserResult
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>

        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>

        public string Error { get; set; }
    }

    public class SaveClaimForUserParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public PipelineMotorClaim ClaimToSave { get; set; }

        public int? EditClaimId { get; set; }
    }


    public class RegisterNewClientResult
    {
        public int Result { get; set; }
        public int ClientId { get; set; }
        public string Error { get; set; }
    }

    public class RegisterNewClientParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
        public string ClientName { get; set; }
    }


    #region RequestLevelOneReport

    public class RequestLevelOneReportResult
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        public string Error { get; set; }
    }

    public class RequestLevelOneReportParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
        public int RiskClaimId { get; set; }

    }

    #endregion

    #region RequestLevelTwoReport

    public class RequestLevelTwoReportResult
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        public string Error { get; set; }
    }

    public class RequestLevelTwoReportParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
        public int RiskClaimId { get; set; }

    }

    #endregion


    #region RequestCallback

    public class RequestCallbackResult
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        public string Error { get; set; }
    }

    public class RequestCallbackParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int RiskClaimId { get; set; }
    }

    #endregion

    #region ClearCallback

    public class ClearCallbackRequestResult
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        public string Error { get; set; }
    }

    public class ClearCallbackRequestParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
        public int RiskClaimId { get; set; }

    }

    #endregion

    public class SetClaimReadStatusResult
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>      
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        public string Error { get; set; }

        public int CurrentReadStatus { get; set; }

    }

    public class SetClaimReadStatusParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int RiskClaimId { get; set; }

        public int RiskClaimReadStatus { get; set; }


    }

    public class GetBatchStatusResult
    {
        public ERiskBatch RiskBatch { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        public string Error { get; set; }
    }

    public class GetBatchStatusParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string ClientBatchReference { get; set; }
    }



    public class GetBatchProcessingResultsResult
    {
        public MessageNode ProcessingResults { get; set; }
        public string Error { get; set; }
    }

    public class GetBatchProcessingResultsParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
        public int RiskBatchId { get; set; }

    }

    #region File Editor Messages


    public class EBatchDetails
    {
        public int BatchId { get; set; }
        public string Reference { get; set; }
        public System.DateTime UploadedDate { get; set; }

        public string UploadedBy { get; set; }

        public int BatchStatusId { get; set; }

        public string Status { get; set; }

        public string BatchStatusValue { get; set; }

        public string Description { get; set; }

        // | delimited string
        public string Rejections { get; set; }

        public int TotalClaimsReceived { get; set; }
        public int TotalNewClaims { get; set; }
        public int TotalModifiedClaims { get; set; }
        public int TotalClaimsLoaded { get; set; }
        public int TotalClaimsInError { get; set; }
        public int TotalClaimsSkipped { get; set; }
        public int LoadingDuration { get; set; }
    }

    public class BatchListParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int StatusFilter { get; set; }
    }

    public class BatchListResult
    {
        public List<EBatchDetails> BatchList { get; set; }

        public string Error { get; set; }
    }

    #endregion

    #region ClaimsInBatch - File Editor

    public class ClaimsInBatchParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
        public int BatchId { get; set; }
    }

    public class ClaimsInBatchResult
    {
        public List<ERiskClaim> ClaimsList { get; set; }

        public string Error { get; set; }
    }

    #endregion

    //#endregion

    public class GetClaimScoreDataResult
    {
        public ERiskClaimRun RiskClaimRun { get; set; }

        public string Error { get; set; }

        public MessageNode ScoreNodes { get; set; }
    }

    public class GetClaimScoreDataParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
        public int RiskClaimId { get; set; }


    }

    public class TeamsForClientParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class RiskUsersParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class TeamsForClientResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public List<ERiskTeam> TeamsList { get; set; }
    }

    public class RiskUsersResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public List<ERiskUser> UsersList { get; set; }
    }

    public class UsersForTeamParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
        public int TeamId { get; set; }
    }

    public class UsersForTeamResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public List<ERiskUser> UsersList { get; set; }
    }

    public class RiskUsersLockedResult{
        public List<ERiskUserLocked> LockedUserList { get; set; }
        public int Result { get; set; }
        public string Error { get; set; }
    }


    public class RiskUsersLockedParam{
        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string Who { get; set; }
    }
   
    public class RiskUserUnlockParam
    {
        public int UserId { get; set; }
       
        public int ClientId { get; set; }
        
        public string Who { get; set; }
       
        public int UnlockUserId { get; set; }
    }

    public class GeneratePasswordResetTokenParam
    {
 
        public string UserName { get; set; }

        public string Token { get; set; }

        public string ResetLink { get; set; }

        public DateTime TokenExpiration { get; set; }

        
    }

    public class ValidatePasswordResetTokenParam
    {
        public string Token { get; set; }
    }
   
    public class RiskUserUnlockResult
    {
        public int Result { get; set; }
        public string Error { get; set; }
    }

    public class GeneratePasswordResetTokenResult
    {
        public int Result { get; set; }
        public string Error { get; set; }
        public string Token { get; set; }
    }

    public class ValidatePasswordResetTokenResult
    {
        public int Result { get; set; }
        public string Error { get; set; }
        public string UserName { get; set; }
    }

    public class RiskClientsResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public List<ERiskClient> ClientList { get; set; }
        public int TotalRows { get; set; }
    }

    public class RiskWordsResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public List<ERiskWord> WordList { get; set; }
        public int TotalRows { get; set; }
    }

    public class RiskNotesResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public List<ERiskNote> NoteList { get; set; }
        public int TotalRows { get; set; }
    }

    public class GetRiskNoteResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public ERiskNote RiskNote { get; set; }

    }

    public class RiskNoteDecisionsResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public List<ERiskNoteDecision> DecisionList { get; set; }
        public int TotalRows { get; set; }
    }

    public class RiskWordDeleteResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

    }

    public class RiskNoteCreateResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

    }

    public class RiskNoteUpdateResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

    }

    public class RiskDefaultDataResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public List<ERiskDefaultData> DefaultDataList { get; set; }
        public int TotalRows { get; set; }
    }

    public class GetAssignedRiskClientsForUserResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public List<ERiskClient> ClientList { get; set; }
        public int TotalRows { get; set; }
    }

    public class RiskRolesResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public List<ERiskRole> RoleList { get; set; }
    }

    public class InsurersClientsResult
    {
        public int Result { get; set; }
        public string Error { get; set; }

        public List<EInsurersClient> ClientList { get; set; }
    }

    public class RiskClientsParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int FilterClientId { get; set; }

    }

    public class RiskWordsParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int FilterClientId { get; set; }
    }

    public class RiskNotesParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int FilterRiskCliamId { get; set; }
    }

    public class GetRiskNoteParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int FilterRiskNoteId { get; set; }
    }

    public class RiskNoteDecisionsParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int? FilterRiskClientId { get; set; }
    }

    public class RiskWordDeleteParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int RiskWordId { get; set; }
    }

    public class RiskNoteCreateParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string NoteDesc { get; set; }

        public int[] RiskClaim_Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public int? Decision_Id { get; set; }

        public int? Visibilty { get; set; }

        public bool Deleted { get; set; }

        public int? UserTypeId { get; set; }

        public byte[] OriginalFile { get; set; }

        public string FileName { get; set; }

        public string FileType { get; set; }
    }

    public class RiskNoteUpdateParam
    {
        public int ClientId { get; set; }

        public int UserId { get; set; }

        public int RiskNoteId { get; set; }

        public string Who { get; set; }

        public string NoteDesc { get; set; }

        public int? Decision_Id { get; set; }

        public int? Visibilty { get; set; }

        public bool Deleted { get; set; }

        public int? UserTypeId { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }
    }


    public class RiskDefaultDataParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int FilterClientId { get; set; }
    }

    public class GetAssignedRiskClientsForUserParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int FilterUserId { get; set; }

    }

    public class RiskRolesParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int FilterClientId { get; set; }

    }

    public class InsurersClientsParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int FilterClientId { get; set; }

    }

    public class GetRiskUserInfoParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class GetRiskUserInfoResult
    {
        public ERiskUser UserInfo { get; set; }
    }


   
    public class GetListOfBatchPriorityClientsParam
    {
       
        public int ClientId { get; set; }

      
        public int UserId { get; set; }

     
        public string Who { get; set; }
    }

  
    public class GetListOfBatchPriorityClientsResult
    {
        public int Result { get; set; }
      
        public string Error { get; set; }
        
        public List<ERiskClientBatchPriority> ListRiskClientBatchPriority { get; set; }
    }

    public class SaveBatchPriorityClientsParam
    {
        public int ClientId { get; set; }
        public int UserId { get; set; }
        public string Who { get; set; }
        public int[] ClientBatchPriorityArray { get; set; }
    }


   
    public class SaveBatchPriorityClientsResult
    {
        public int Result { get; set; }
        public string Error { get; set; }
    }



    public class EWebsiteBatchDetails
    {
        public int BatchId { get; set; }

        public int RiskClientId { get; set; }

        public string BatchReference { get; set; }

        public string BatchEntityType { get; set; }

        public string ClientBatchReference { get; set; }

        public string BatchStatusAsString { get; set; }

        public int BatchStatusId { get; set; }

        public System.DateTime UploadedDate { get; set; }

        public string UploadedBy { get; set; }

        public string VerificationResults { get; set; }

        public string MappingResults { get; set; }

        public int TotalNumberOfClaims { get; set; }

    }

    public class WebsiteBatchListParam
    {


        public string Who { get; set; }
        public int UserId { get; set; }
        public int ClientId { get; set; }

        /// <summary>
        /// The client ID. If set to -1 all batches for all clients returned
        /// </summary>
        public int FilterClientId { get; set; }

        /// <summary>
        /// The team ID. If set to -1 and client ID is specified all batches 
        /// for the specified client are returned.
        /// </summary>
        public int TeamId { get; set; }

        /// <summary>
        /// The user ID. If set to -1 and team id is specified, all batches for
        /// the specified team are returned.
        /// </summary>
        public int FilterUserId { get; set; }


        /// <summary>
        /// Can be one of "CLIENTBATCHREFERENCE", 
        ///  "UPLOADDATETIME"
        /// </summary>
        public string SortColumn { get; set; }

        /// <summary>
        /// True if the sort order is ASC otherwise false
        /// </summary>
        public bool SortAscending { get; set; }

        /// <summary>
        /// Page number required. Starts at 1
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Number of rows in a page
        /// </summary>
        public int PageSize { get; set; }

        public int StatusFilter { get; set; }
    }

    public class WebsiteBatchListResult
    {

        public WebsiteBatchListResult()
        {
            BatchList = new List<EWebsiteBatchDetails>();
        }

        public List<EWebsiteBatchDetails> BatchList { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Total rows in ResultSet (not the number of rows in the page)
        /// </summary>
        public int TotalRows { get; set; }
    }

    public class EWebsiteSingleClaimsDetails
    {
        public int RiskClaim_Id { get; set; }

        public string ClaimType { get; set; }

        public string ClaimNumber { get; set; }

        public DateTime IncidentDate { get; set; }

        public string SavedBy { get; set; }

        public DateTime DateSaved { get; set; }
    }

    public class EFilterMatchNumbers
    {
        public string BatchNumber { get; set; }
    }


    public class EWebsiteSingleClaimsDetail
    {
        public int RiskClaim_Id { get; set; }
        public MDA.Common.FileModel.MotorClaim claim { get; set; }
    }

    public class EBatchQueue
    {
        public int BatchId { get; set; }
        public string Client { get; set; }
        public string BatchRef { get; set; }
        public DateTime? Uploaded { get; set; }
        public int ClaimsReceieved { get; set; }

        public EBatchQueue()
        {
            Uploaded = DateTime.MinValue;
        }
    }


    public class WebsiteGetTotalSingleClaimsForClientParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class WebsiteGetTotalSingleClaimsForClientResult
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Total rows in ResultSet (not the number of rows in the page)
        /// </summary>
        public int TotalRows { get; set; }
    }

    public class WebsiteFetchSingleClaimDetailParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int EditClaimId { get; set; }
    }

    public class WebsiteFetchSingleClaimDetailResult
    {

        public EWebsiteSingleClaimsDetail claimDetail { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        public string Error { get; set; }
    }


    public class WebsiteDeleteSingleClaimParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int EditClaimId { get; set; }
    }

    public class WebsiteDeleteSingleClaimResult
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        public string Error { get; set; }
    }

    public class WebsiteFilterBatchNumbersParam
    {
        public string Role { get; set; }
        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string Who { get; set; }
        public int? SelectedClientId { get; set; }
    }

    public class WebsiteRulesConnectionStringResult
    {
        public string RulesConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["RulesEntities"].ConnectionString; }
        }
    }

    public class WebsiteFilterBatchNumbersResult
    {
        public WebsiteFilterBatchNumbersResult()
        {
            BatchNumbersList = new List<string>();
        }

        public List<string> BatchNumbersList { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        public string Error { get; set; }
    }


    public class WebsiteSingleClaimsListParam
    {

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int TeamId { get; set; }

        public string SortColumn { get; set; }

        public bool SortAscending { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }

        public int StatusFilter { get; set; }

    }

    public class WebsiteSingleClaimsListResult
    {
        public List<EWebsiteSingleClaimsDetails> WebsiteSingleClaimsList { get; set; }

        public WebsiteSingleClaimsListResult()
        {
            WebsiteSingleClaimsList = new List<EWebsiteSingleClaimsDetails>();
        }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Total rows in ResultSet (not the number of rows in the page)
        /// </summary>
        public int TotalRows { get; set; }

    }

    public class WebsiteSaveSingleClaimParam
    {
        public int? EditClaimId { get; set; }

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
        public string RiskClaimNumber { get; set; }
        public string ClaimType { get; set; }
        public DateTime IncidentDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Status { get; set; }
        public string ModifiedBy { get; set; }
        public MDA.Common.FileModel.IClaim ClaimToSave { get; set; }
    }

    public class WebsiteSaveSingleClaimResult
    {
        public int Result { get; set; }
        public string Error { get; set; }
        public int EditClaimId { get; set; }
    }

    public class WebSearchResultsParam
    {
        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string Who { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string FilterFirstName { get; set; }
        public string FilterSurname { get; set; }
        public DateTime? FilterDob { get; set; }
        public string FilterAddressLine { get; set; }
        public string FilterVehicleRegNumber { get; set; }
        public string FilterAddressPostcode { get; set; }
    }

    public class ESearchDetails
    {
        public string MatchedOn { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string VehicleReg { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleModel { get; set; }
        public string Involvement { get; set; }
        public string ClaimType { get; set; }
        public string Status { get; set; }
        public DateTime? Date { get; set; }
        public string Source { get; set; }
        public string Reference { get; set; }
    }
    public class WebSearchResultsResult
    {
        public WebSearchResultsResult()
        {
            SearchResultsList = new List<ESearchDetails>();
        }
      
        public List<ESearchDetails> SearchResultsList { get; set; }

        public int Result { get; set; }
        public string Error { get; set; }
        public int TotalRows { get; set; }
    }



    #region CUE
   
    public class WebsiteCueInvolvementsParam
    {
        public int RiskClaim_Id { get; set; }
        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string Who { get; set; }

    }

  
    public class WebsiteCueInvolvementsResult
    {
        public WebsiteCueInvolvementsResult()
        {
            CueInvolvements = new List<ECueInvolvement>();
        }
        public int Result { get; set; }
        public string Error { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime IncidentDate { get; set; }
        public DateTime UploadedDate { get; set; }
        public List<ECueInvolvement> CueInvolvements { get; set; }
    }

    public class WebsiteCueSearchParam
    {
        public int RiskClaim_Id { get; set; }
        public int UniqueId { get; set; }
        public List<int> ListPerson_Id { get; set; }

        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string Who { get; set; }

    }

  
    public class WebsiteCueSearchResult
    {
        public int Result { get; set; }
        public string Error { get; set; }
    }


    public class WebsiteCueReportParam
    {
        public int RiskClaim_Id { get; set; }
        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string Who { get; set; }
    }

    public class WebsiteCueReportResult{
        public ECueReportDetails CueReportDetails { get; set; }
        public int Result { get; set; }
        public string Error { get; set; }
    }

    public class WebsiteSaveTemplateFunctionsResult
    {
        public int Result { get; set; }
        public string Error { get; set; }
    }

    public class WebsiteSaveTemplateFunctionsParam
    {
        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string Who { get; set; }
        public int Template_Id { get; set; }
        public string TemplateFunctionsJson { get; set; }
        public List<Dictionary<string, string>> ListTemplateFunctionsJson { get; set; }
    }

    public class WebsiteRiskRoleCreateResult
    {
        
        public int Result { get; set; }
        
        public string Error { get; set; }
    }

    public class WebsiteRiskRoleCreateParam
    {
        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string Who { get; set; }
        public string TemplateName { get; set; }
    }

    public class WebsiteRiskRoleEditResult
    {

        public int Result { get; set; }

        public string Error { get; set; }
    }

    public class WebsiteRiskRoleEditParam
    {
        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string Who { get; set; }
        public string TemplateName { get; set; }
        public int Profile_Id { get; set; }
    }


   
    public class SendElmahErrorResult
    {
        public int Result { get; set; }
        public string Error { get; set; }
    }

   
    public class SendElmahErrorParam
    {
        public string Error { get; set; }
    }

    public class WebsiteRiskRoleDeleteResult
    {
        public int Result { get; set; }

        public string Error { get; set; }
    }

    public class WebsiteRiskRoleDeleteParam
    {
        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string Who { get; set; }
        public int Profile_Id { get; set; }
    }

    public class FindUserNameResult
    {
        public int Result { get; set; }
        public string Error { get; set; }
    }

    public class FindUserNameParam
    {
        public string UserName { get; set; }
    }
    



    public class WebsiteLoadTemplateFunctionsResult
    {
        public string TemplateFunctionsJson { get; set; }
        public int Result { get; set; }
        public string Error { get; set; }
    }


    public class WebsiteLoadTemplateFunctionsParam
    {
        public int UserId { get; set; }
        public int ClientId { get; set; }
 
        public string Who { get; set; }
        public int Template_Id { get; set; }
    }
    
#endregion


    public class IsClientBatchReferenceUniqueResult
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public bool IsUnique { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        public string Error { get; set; }
    }

    public class IsClientNameUniqueResult
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public bool IsUnique { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        public string Error { get; set; }
    }

    public class IsPasswordUniqueResult
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public bool IsUnique { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        public string Error { get; set; }
    }

    public class IsUserNameUniqueResult
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        public bool IsUnique { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        public string Error { get; set; }
    }

    public class IsClientBatchReferenceUniqueParam
    {
        public string ClientBatchReference { get; set; }


        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }


    public class IsClientNameUniqueParam
    {
        public string ClientName { get; set; }


        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class IsPasswordUniqueParam
    {
        public string Password { get; set; }

        public string Username { get; set; }

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class IsUserNameUniqueParam
    {
        public string UserName { get; set; }


        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class GetLevel1ReportStreamParam
    {
        public int RiskClaimId { get; set; }


        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class GetLevel2ReportStreamParam
    {
        public int RiskClaimId { get; set; }


        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class GetBatchReportStreamParam
    {
        public int RiskBatchId { get; set; }


        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class GetCueReportStreamParam
    {
        public int RiskClaimId { get; set; }

        public int Unique_Id { get; set; }

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class GenerateAnyBatchReportsOutstandingParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class GetPasswordFailuresSinceLastSuccessParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string UserName { get; set; }
    }

    public class IsUserInRoleParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string UserName { get; set; }
        public string RoleName { get; set; }
    }

    public class GetRolesForUserParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string UserName { get; set; }
    }

    public class ValidateUserParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string IPAddress { get; set; }
    }

    public class CreateUserParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TelephoneNumber { get; set; }
        public int RoleId { get; set; }
        public int TeamId { get; set; }
        public int? Status { get; set; }
        public bool? Autoloader { get; set; }
        public int? UserTypeId { get; set; }
    }

    public class CreateClientParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string ClientName { get; set; }
        public int? StatusId { get; set; }
        public string FileExtensions { get; set; }
        public int AmberThreshold { get; set; }
        public int RedThreshold { get; set; }
        public int InsurersClientsId { get; set; }

    }

    public class CreateRiskWordParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int RiskClientId { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string LookupWord { get; set; }
        public string ReplacementWord { get; set; }
        public bool ReplaceWholeString { get; set; }
        public System.DateTime DateAdded { get; set; }
        public string SearchType { get; set; }

    }

    public class CreateRiskDefaultDataParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
        
        public int RiskClient_Id { get; set; }
        public string ConfigurationValue { get; set; }
        public bool IsActive { get; set; }
        public int RiskConfigurationDescriptionId { get; set; }

    }

    public class CreateRiskNoteDecisionParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int RiskClient_Id { get; set; }
        public string Decision { get; set; }
        public byte IsActive { get; set; }

    }

    public class DeleteRiskDefaultDataParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int Id { get; set; }

    }

    public class DeleteRiskNoteDecisionParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int Id { get; set; }

    }

    public class UpdateClientParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string ClientName { get; set; }
        public int Id { get; set; }
        public int? StatusId { get; set; }
        public string FileExtensions { get; set; }
        public int AmberThreshold { get; set; }
        public int RedThreshold { get; set; }
        public int InsurersClientsId { get; set; }
        public bool? RecordReserveChanges { get; set; }
        public int? BatchPriority { get; set; }

    }

    public class UpdateUserParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TelephoneNumber { get; set; }
        public int RoleId { get; set; }
        public int TeamId { get; set; }
        public int? Status { get; set; }
        public bool? Autoloader { get; set; }
        public int? UserTypeId { get; set; }
    }

    public class CreateRiskUser2ClientParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int SelectedUserId { get; set; }

        public int SelectedClientId { get; set; }


    }

    public class DeleteRiskUser2ClientParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int SelectedUserId { get; set; }

        public int SelectedClientId { get; set; }


    }

    public class ChangePasswordParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string UserName { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class GetUserIdParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public string UserName { get; set; }
    }

    public class CheckUserBelongsToClientParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }
    }

    public class CheckUserHasAccessToReportParam
    {
        public int UserId { get; set; }

        public int ClientId { get; set; }

        public string Who { get; set; }

        public int LoggedInUserId { get; set; }
       public int RiskBatchId { get; set; }
    }


    public class GetBatchQueueParam
    {
      
        public int UserId { get; set; }

       
        public int ClientId { get; set; }

      
        public string Who { get; set; }
    }

   
    public class GetBatchQueueResult
    {
       
        public List<EBatchQueue> BatchQueue { get; set; }
    }


    public class SaveOverriddenBatchPriorityParam
    {
        public int UserId { get; set; }
        public int ClientId { get; set; }
        public string Who { get; set; }
        public int[] OverriddenBatchPriorityArray { get; set; }
    }

   
    public class SaveOverriddenBatchPriorityResult
    {
        public int Result { get; set; }
        public string Error { get; set; }
    }

}