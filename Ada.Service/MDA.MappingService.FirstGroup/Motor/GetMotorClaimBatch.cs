﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FileHelpers;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.Common.Helpers;
using MDA.MappingService.FirstGroup.Model;
using MDA.Common.Server;
using MDA.Common;
using System.Globalization;

namespace MDA.MappingService.FirstGroup
{
    public class GetMotorClaimBatch
    {

        private string _claimDataFile;
        private FileHelperEngine _claimDataEngine; 
        private ClaimData[] _claimData;
        private StreamReader _claimDataFileStreamReader;

        public void RetrieveMotorClaimBatch(CurrentContext ctx, Stream filem, string clientFolder, object statusTracking, Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn, ProcessingResults processingResults) 
        {
            #region Files

            if (filem == null)
            {
                _claimDataFile = @"D:\Dev\Projects\MDASolution\MDA.Mapping.FirstGroup\Resource\MotorKeoghs.csv";
            }
            else
            {
                try
                {
                    _claimDataFileStreamReader = new StreamReader(filem);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in creating csv file: " + ex);
                }

            }
            #endregion

            #region Initialise FileHelper Engine
            try
            {
                _claimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
            #endregion

            #region Populate FileHelper Engine with data from file

            try
            {
                if (filem == null)
                {
                    _claimData = _claimDataEngine.ReadFile(_claimDataFile) as ClaimData[];
                }
                else
                {
                    _claimData = _claimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in reading csv file via filehelper engine: " + ex);
            }

            #endregion

            TranslateToXml(ctx, _claimData, statusTracking, ProcessClaimFn, processingResults);

        }

        private void TranslateToXml(CurrentContext ctx, ClaimData[] claimData, object statusTracking, Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn, ProcessingResults processingResults)
        {
            List<int?> processedClaims = new List<int?>(); //Create list of proccessed claims to ensure same claim not translated twice

            foreach (var claim in claimData)
            {

                if (!processedClaims.Contains(claim.IncidentNo))
                {

                    ClaimData[] uniqueCliams = (from x in claimData     // Find all rows relating to one claim, claim number may appear on several rows linking parties to claim
                                                where x.IncidentNo == claim.IncidentNo
                                                select x).ToArray();

                    PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                    motorClaim.ClaimNumber = claim.IncidentNo.ToString();
                    if (claim.AccDate != null && claim.AccDate != "?")
                    {
                        DateTime incidentDate = DateTime.ParseExact(claim.AccDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                        motorClaim.IncidentDate = incidentDate;
                    }
                    
                    if (claim.S == "C" || claim.S == "I")
                    {
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                    }
                    else if(claim.S == "F")
                    {
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                    }
                    else
                    {
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                    }

                    processedClaims.Add(claim.IncidentNo); // Add claim number to list so that the claim is not processed again

                    #region Organisation - Solicitors

                    foreach (var c in uniqueCliams)
                    {
                        if (string.IsNullOrEmpty(c.Giv) && c.Surname.Split().Length > 1 && c.PTy == "Solicitor")
                        {
                            PipelineOrganisation organisation = new PipelineOrganisation();
                            organisation.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.Solicitor;
                            organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;


                            organisation.OrganisationName = c.Surname;

                            #region Address

                            if (c.AddressLine1 != null || c.AddressLine2 != null || c.Add3 != null || c.Add4 != null || c.PostCode != null)
                            {
                                PipelineAddress address = new PipelineAddress();

                                address.AddressType_Id = (int)AddressLinkType.TradingAddress;

                                address.Street = c.AddressLine1 + " " + c.AddressLine2 + " " + c.Add3 + " " + c.Add4;
                                address.PostCode = c.PostCode;

                                organisation.Addresses.Add(address);
                            }

                            #endregion

                            #region Landline Telephone Number

                            if (!string.IsNullOrEmpty(c.Ph))
                            {
                                PipelineTelephone telephoneNumber = new PipelineTelephone();
                                telephoneNumber.TelephoneType_Id = (int)TelephoneType.Landline;
                                telephoneNumber.ClientSuppliedNumber = "0" + c.Ph;

                                organisation.Telephones.Add(telephoneNumber);
                            }
                            #endregion

                            #region Mobile Telephone Number

                            if (!string.IsNullOrEmpty(c.MobileNo))
                            {
                                PipelineTelephone telephoneNumber = new PipelineTelephone();
                                telephoneNumber.TelephoneType_Id = (int)TelephoneType.Mobile;
                                telephoneNumber.ClientSuppliedNumber = "0" + c.MobileNo;

                                organisation.Telephones.Add(telephoneNumber);
                            }

                            #endregion

                            motorClaim.Organisations.Add(organisation);
                        }
                    }

                    #endregion

                    #region Insured Vehicle

                    var uniqueInsuredVehicle = (from x in uniqueCliams  // Insured Vehicle reg can appear against multiple rows, get distinct values
                                                where x.RegNo != null
                                                && !string.IsNullOrEmpty(x.RegNo)
                                                select x.RegNo).Distinct();

                    foreach (var v in uniqueInsuredVehicle)
                    {
                        PipelineVehicle insuredVehicle = new PipelineVehicle();
                        insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                        insuredVehicle.VehicleRegistration = v;
                        motorClaim.Vehicles.Add(insuredVehicle);
                    }

                    #endregion

                    #region Third Party Vehicle

                    foreach (var c in uniqueCliams)
                    {

                        if (c.TPRegno != null && !string.IsNullOrEmpty(c.TPRegno))
                        {
                            PipelineVehicle thirdPartyVehicle = new PipelineVehicle();
                            thirdPartyVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                            thirdPartyVehicle.VehicleRegistration = c.TPRegno;
                            motorClaim.Vehicles.Add(thirdPartyVehicle);

                            if (c.Giv != null && !string.IsNullOrEmpty(c.Giv) && c.PTy == "Third Party")
                            {
                                PipelinePerson thirdPartyPerson = new PipelinePerson();
                                thirdPartyPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                thirdPartyPerson.FirstName = c.Giv;
                                thirdPartyPerson.LastName = c.Surname;
                                if (c.DOB != null && c.DOB != "?")
                                {
                                    DateTime dateOfBirth = DateTime.ParseExact(c.DOB, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                                    thirdPartyPerson.DateOfBirth = dateOfBirth;
                                }

                                #region Address

                                if (c.AddressLine1 != null || c.AddressLine2 != null || c.Add3 != null || c.Add4 != null || c.PostCode != null)
                                {
                                    PipelineAddress address = new PipelineAddress();

                                    address.Street = c.AddressLine1 + " " + c.AddressLine2 + " " + c.Add3 + " "+ c.Add4;
                                    address.PostCode = c.PostCode;

                                    thirdPartyPerson.Addresses.Add(address);
                                }

                                #endregion

                                #region Landline Telephone Number

                                if (!string.IsNullOrEmpty(c.Ph))
                                {
                                    PipelineTelephone telephoneNumber = new PipelineTelephone();
                                    telephoneNumber.TelephoneType_Id = (int)TelephoneType.Landline;
                                    telephoneNumber.ClientSuppliedNumber = "0" + c.Ph;

                                    thirdPartyPerson.Telephones.Add(telephoneNumber);
                                }
                                #endregion

                                #region Mobile Telephone Number

                                if (!string.IsNullOrEmpty(c.MobileNo))
                                {
                                    PipelineTelephone telephoneNumber = new PipelineTelephone();
                                    telephoneNumber.TelephoneType_Id = (int)TelephoneType.Mobile;
                                    telephoneNumber.ClientSuppliedNumber = "0" + c.MobileNo;

                                    thirdPartyPerson.Telephones.Add(telephoneNumber);
                                }

                                #endregion

                                thirdPartyVehicle.People.Add(thirdPartyPerson);
                            }
                        }
                        else
                        {

                            if (c.Giv != null && !string.IsNullOrEmpty(c.Giv) && c.PTy == "Third Party")
                            {
                                PipelinePerson thirdPartyPerson = new PipelinePerson();
                                thirdPartyPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                thirdPartyPerson.FirstName = c.Giv;
                                thirdPartyPerson.LastName = c.Surname;
                                if (c.DOB != null && c.DOB != "?")
                                {
                                    DateTime dateOfBirth = DateTime.ParseExact(c.DOB, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                                    thirdPartyPerson.DateOfBirth = dateOfBirth;
                                }

                                #region Address

                                if (c.AddressLine1 != null || c.AddressLine2 != null || c.Add3 != null || c.Add4 != null || c.PostCode != null)
                                {
                                    PipelineAddress address = new PipelineAddress();

                                    address.Street = c.AddressLine1 + " " + c.AddressLine2 + " " + c.Add3 + " " + c.Add4;
                                    address.PostCode = c.PostCode;

                                    thirdPartyPerson.Addresses.Add(address);
                                }

                                #endregion

                                #region Landline Telephone Number

                                if (!string.IsNullOrEmpty(c.Ph))
                                {
                                    PipelineTelephone telephoneNumber = new PipelineTelephone();
                                    telephoneNumber.TelephoneType_Id = (int)TelephoneType.Landline;
                                    telephoneNumber.ClientSuppliedNumber = "0" + c.Ph;

                                    thirdPartyPerson.Telephones.Add(telephoneNumber);
                                }
                                #endregion

                                #region Mobile Telephone Number

                                if (!string.IsNullOrEmpty(c.MobileNo))
                                {
                                    PipelineTelephone telephoneNumber = new PipelineTelephone();
                                    telephoneNumber.TelephoneType_Id = (int)TelephoneType.Mobile;
                                    telephoneNumber.ClientSuppliedNumber = "0" + c.MobileNo;

                                    thirdPartyPerson.Telephones.Add(telephoneNumber);
                                }

                                #endregion

                                int count = 0;

                                foreach (var v in motorClaim.Vehicles.Reverse<PipelineVehicle>())
                                {
                                    if (v.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.ThirdPartyVehicle && count == 0)
                                    {
                                        v.People.Add(thirdPartyPerson);

                                        count++;
                                    }
                                }

                                if (count == 0)
                                {
                                    PipelineVehicle thirdPartyVehicle = new PipelineVehicle();
                                    thirdPartyVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;
                                    thirdPartyVehicle.People.Add(thirdPartyPerson);
                                    motorClaim.Vehicles.Add(thirdPartyVehicle);
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(c.Giv) && c.Surname.Split().Length > 1 && c.PTy == "Third Party")
                        {
                            PipelineOrganisation organisation = new PipelineOrganisation();
                            
                            organisation.OrganisationName = c.Surname;

                            #region Address

                            if (c.AddressLine1 != null || c.AddressLine2 != null || c.Add3 != null || c.Add4 != null || c.PostCode != null)
                            {
                                PipelineAddress address = new PipelineAddress();

                                address.Street = c.AddressLine1 + " " + c.AddressLine2 + " " + c.Add3 + " " + c.Add4;
                                address.PostCode = c.PostCode;

                                organisation.Addresses.Add(address);
                            }

                            #endregion

                            #region Landline Telephone Number

                            if (!string.IsNullOrEmpty(c.Ph))
                            {
                                PipelineTelephone telephoneNumber = new PipelineTelephone();
                                telephoneNumber.TelephoneType_Id = (int)TelephoneType.Landline;
                                telephoneNumber.ClientSuppliedNumber = "0" + c.Ph;

                                organisation.Telephones.Add(telephoneNumber);
                            }
                            #endregion

                            #region Mobile Telephone Number

                            if (!string.IsNullOrEmpty(c.MobileNo))
                            {
                                PipelineTelephone telephoneNumber = new PipelineTelephone();
                                telephoneNumber.TelephoneType_Id = (int)TelephoneType.Mobile;
                                telephoneNumber.ClientSuppliedNumber = "0" + c.MobileNo;

                                organisation.Telephones.Add(telephoneNumber);
                            }

                            #endregion

                            PipelineVehicle thirdPartyVehicle = new PipelineVehicle();
                            thirdPartyVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;
                            PipelinePerson thirdPartyPerson = new PipelinePerson();
                            thirdPartyPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                            thirdPartyPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                            thirdPartyPerson.Organisations.Add(organisation);
                            thirdPartyVehicle.People.Add(thirdPartyPerson);
                            motorClaim.Vehicles.Add(thirdPartyVehicle);

                        }
                    }

                    #endregion

                    #region Insured Vehicle and Driver Check

                    bool insuredVehicleCheck = false;
                    bool insuredDriverCheck = false;

                    foreach (var vehicle in motorClaim.Vehicles)
                    {
                        if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                        {
                            insuredVehicleCheck = true;

                            foreach (var person in vehicle.People)
                            {
                                if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                                {
                                    insuredDriverCheck = true;
                                }
                            }
                        }
                    }

                    if (insuredVehicleCheck == false)
                    {
                        PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                        defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                        if (insuredDriverCheck == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                        }

                        motorClaim.Vehicles.Add(defaultInsuredVehicle);
                    }
                    else
                    {
                        if (insuredDriverCheck == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            foreach (var vehicle in motorClaim.Vehicles)
                            {
                                if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                {

                                    vehicle.People.Add(defaultInsuredDriver);

                                }

                            }

                        }
                    }

                    #endregion

                    if (ProcessClaimFn(ctx, motorClaim, statusTracking) == -1) return;
                }
            }
        }
    }
}
