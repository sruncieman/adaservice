﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.FirstGroup.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Divs;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? IncidentNo;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Giv;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Surname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AddressLine1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AddressLine2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Add3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Add4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MobileNo;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Ph;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PTy;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPRegno;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Broker;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPInsr;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Solicitor;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String S;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RegNo;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DOB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyYear;

    }
}
