﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDA.Common.Debug
{
    public partial class TraceWindow : Form
    {
        public TraceWindow()
        {
            InitializeComponent();
        }

        public void WriteLine(string s)
        {
            if (listBox1.Items.Count > 10000)
                listBox1.Items.Clear();

            listBox1.Items.Add(s); // Insert(0, s);

            listBox1.Update();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox1.Update();
        }

    }
}
