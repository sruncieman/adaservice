﻿using System.Windows.Forms;
using System;
using log4net;
using MDA.Common;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common.Debug;
using System.Data.Entity.Validation;

//using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using log4net.Repository;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace MDA.Common.Debug
{
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;


    public class DebugCheck
    {
        [Conditional("DEBUG")]
        public static void NotNull<T>(T value) where T : class
        {
            Debug.Assert(value != null);
        }

        [Conditional("DEBUG")]
        public static void NotNull<T>(T? value) where T : struct
        {
            Debug.Assert(value != null);
        }

        [Conditional("DEBUG")]
        public static void NotEmpty(string value)
        {
            Debug.Assert(!string.IsNullOrWhiteSpace(value));
        }
    }

    public class ADATrace
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static string CreateCleanFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));

            string invalidReStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(name, invalidReStr, "_");
        }

        public static void SwitchToDefaultLogFile()
        {
            ILoggerRepository logRepos = LogManager.GetRepository();
            IAppender[] logAppenders = logRepos.GetAppenders();

            string targetAppenderName = "LogFileAppender";

            // first find the logger we are actually looking for

            for (int i = 0; i < logAppenders.Length; i++)
            {
                IAppender appender = logAppenders[i];

                if (appender.Name == targetAppenderName)
                {
                    // cast generic appender to our type
                    FileAppender rfa = appender as FileAppender;

                    string root = ConfigurationManager.AppSettings["TraceFilePath"].ToString();

                    string newFullFileName = root + "\\" + "ADA-Trace.log";

                    // set new file name
                    rfa.File = newFullFileName;

                    // indicate to change
                    rfa.ActivateOptions();

                    break;
                }
            }
        }

        public static void SwitchToNewUniqueLogFile(int? clientId, string folderName, string fileName)
        {
            ILoggerRepository logRepos = LogManager.GetRepository();
            IAppender[] logAppenders = logRepos.GetAppenders();

            string targetAppenderName = "LogFileAppender";

            // first find the logger we are actually looking for

            for (int i = 0; i < logAppenders.Length; i++)
            {
                IAppender appender = logAppenders[i];

                if (appender.Name == targetAppenderName )
                {
                    // cast generic appender to our type
                    FileAppender rfa = appender as FileAppender;

                    // get full name of current log file 
                    //string oldFName = rfa.File;

                    // extract path
                    //int indexOfLastSlash = oldFName.LastIndexOf(@"\");
                    string root = ConfigurationManager.AppSettings["TraceFilePath"].ToString();

                    string NewDir = root + ((clientId != null) ? "\\" + string.Format("{0:000}", clientId) : "");

                    if (!Directory.Exists(NewDir))
                        Directory.CreateDirectory(NewDir);

                    folderName = string.Format("{0}", CreateCleanFileName(folderName));

                    NewDir = NewDir + "\\" + folderName;

                    if (!Directory.Exists(NewDir))
                        Directory.CreateDirectory(NewDir);

                    string newFullFileName = NewDir + "\\" + fileName + ".log";

                    // generate new log file name
                    //string newLogFileName = name + ".log";
                    //string newFullFileName = path + newLogFileName;

                    // set new file name
                    rfa.File = newFullFileName;

                    // indicate to change
                    rfa.ActivateOptions();

                    break;
                }
            }
        }

        //static bool writeToFile = Convert.ToBoolean(ConfigurationManager.AppSettings["TraceToFile"]);

        //static ADATrace()
        //{
        //    writeToFile = Convert.ToBoolean(ConfigurationManager.AppSettings["TraceToFile"]);
        //}

        //private static TraceWindow _TraceWindow = null;
        //private static StreamWriter file;

        public static void Close()
        {
            //

            //if (writeToFile && file != null)
            //{
            //    file.Close();
            //    file = null;
            //}
        }

        public static void SetFileName(string s)
        {
        }

        public static void WriteList(List<string> trace)
        {
            foreach (var s in trace)
                WriteLine(s);
        }

        public static void WriteLine(string trace)
        {
            string tabs = "";

            for (int i = 0; i < _indent; i++)
                tabs = tabs + "    ";

            log.Info(tabs + trace);
        }

        public static void WriteLine(string trace, bool traceEnabled = false)
        {
            if (!traceEnabled)
                return;

            StringBuilder traceMessage = new StringBuilder();
            for (int i = 0; i < _indent; i++)
                traceMessage.Append("     ");

            traceMessage.Append(trace);

            log.Info(traceMessage.ToString());
        }

        public static void Write(string trace)
        {
            string tabs = "";

            for (int i = 0; i < _indent; i++)
                tabs = tabs + "    ";

            log.Info(tabs + trace);
        }

        public static void TraceException(Exception ex)
        {
            if (ex is DbEntityValidationException)
            {
                var dbEx = ex as DbEntityValidationException;

                ADATrace.WriteLine("EXCEPTION " + dbEx.Message);

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ADATrace.WriteLine(string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }

                Exception e = ex.InnerException;

                while (e != null)
                {
                    ADATrace.WriteLine("INNER EXCEPTION " + e.Message);
                    e = e.InnerException;
                }
            }
            else
            {
                ADATrace.WriteLine("EXCEPTION " + ex.Message);

                Exception e = ex.InnerException;

                while (e != null)
                {
                    ADATrace.WriteLine("INNER EXCEPTION " + e.Message);
                    e = e.InnerException;
                }
            }
        }

        public static void TraceException(System.Data.Entity.Core.EntityException ex)
        {
            TraceException(ex);
        }

        public static void TraceException(DbEntityValidationException dbEx)
        {
            ADATrace.WriteLine("EXCEPTION " + dbEx.Message);

            foreach (var validationErrors in dbEx.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    ADATrace.WriteLine(string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                }
            }

            Exception e = dbEx.InnerException;

            while (e != null)
            {
                ADATrace.WriteLine("INNER EXCEPTION " + e.Message);
                e = e.InnerException;
            }

        }

        private static void DumpNode(MessageNode node)
        {
            WriteLine("Start:" + node.MessageType.ToString() + ": " + node.Text);
            IndentLevel += 1;

            if ( node.Nodes != null)
                foreach (var n in node.Nodes)
                    DumpNode(n);

            IndentLevel -= 1;
            WriteLine("End:" + node.MessageType.ToString() + ": " + node.Text);
        }

        public static void DumpProcessingResults(ProcessingResults results)
        {
            WriteLine("Dumping Processing Results Errors");

            DumpNode(results.Message);
        }

        private static int _indent = 0;

        public static int IndentLevel
        {
            get
            {
                return _indent;
            }

            set
            {
                _indent = value;
                Trace.IndentLevel = value;
            }
        }
    }
}


