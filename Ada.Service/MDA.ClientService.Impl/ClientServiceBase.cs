﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.ClientService.Interface;
using MDA.Common.Server;
using RiskEngine.Scoring.Model;
using MDA.Pipeline.Model;
using MDA.RiskService.Model;
using MDA.ExternalServices.Model;

namespace MDA.ClientService.Impl
{
    public abstract class ClientServiceBase : IClientService
    {
        public virtual void BeforeCreate(CurrentContext ctx, int riskBatchId)
        { }

        public virtual void AfterCreate(CurrentContext ctx, int riskBatchId)
        { }

        public virtual void BeforeCleansing(CurrentContext ctx, RiskClaimToProcess claimToProcess)
        { }

        public virtual void AfterCleansing(CurrentContext ctx, RiskClaimToProcess claimToProcess, IPipelineClaim claimXml)
        { }

        public virtual void BeforeLoading(CurrentContext ctx, RiskClaimToProcess claimToProcess)
        { }

        public virtual void AfterLoading(CurrentContext ctx, RiskClaimToProcess claimToProcess)
        { }

        public virtual void BeforeExternal(CurrentContext ctx, ExternalServiceCallRequest req)
        { }

        public virtual void AfterExternal(CurrentContext ctx, ExternalServiceCallResponse res)
        { }

        public virtual void BeforeScore(CurrentContext ctx, RiskClaimToProcess claimToProcess, bool scoreWithLiveRules)
        { }
        public virtual void AfterScore(CurrentContext ctx, RiskClaimToProcess claimToProcess, bool scoreWithLiveRules, bool unableToScore, ClaimScoreResults results)
        { }
    }
}
