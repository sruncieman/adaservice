﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Pipeline.Model;

namespace MDA.MappingService.Eldon
{
    internal static class AddressFormatHelper
    {
        public static PipelineAddress PopulateAddress(PipelineAddress address, string p1, string p2, string p3, string p4, string p5, string p6, string p7, string p8)
        {
            int countPopulated = 0;
            bool a1 = false;
            bool a2 = false;
            bool a3 = false;
            bool a4 = false;
            bool a5 = false;
            bool a6 = false;
            bool a7 = false;
            bool a8 = false;


            if (!string.IsNullOrEmpty(p1))
            {
                countPopulated++;
                a1 = true;
            }

            if (!string.IsNullOrEmpty(p2))
            {
                countPopulated++;
                a2 = true;
            }

            if (!string.IsNullOrEmpty(p3))
            {
                countPopulated++;
                a3 = true;
            }


            if (!string.IsNullOrEmpty(p4))
            {
                countPopulated++;
                a4 = true;
            }


            if (!string.IsNullOrEmpty(p5))
            {
                countPopulated++;
                a5 = true;
            }


            if (!string.IsNullOrEmpty(p6))
            {
                countPopulated++;
                a6 = true;
            }


            if (!string.IsNullOrEmpty(p7))
            {
                countPopulated++;
                a7 = true;
            }


            if (!string.IsNullOrEmpty(p8))
            {
                countPopulated++;
                a8 = true;
            }

            #region Breakpoint test

            if (countPopulated == 1)
            {
                var z1 = 1;
            }

            if (countPopulated == 2)
            {
                var z2 = 1;
            }

            if (countPopulated == 3)
            {
                var z3 = 1;
            }

            if (countPopulated == 4)
            {
                var z4 = 1;
            }

            if (countPopulated == 5)
            {
                var z5 = 1;
            }

            if (countPopulated == 6)
            {
                var z6 = 1;
            }

            if (countPopulated == 7)
            {
                var z7 = 1;
            }
            #endregion


            switch (countPopulated)
            {
                case 7:
                    address.SubBuilding = p1;
                    address.Building = p2;
                    address.Street = p3;
                    address.Locality = p4;
                    if (a5)
                    {
                        address.Town = p5;
                        if (a6)
                            address.Town += " " + p6;
                    }
                    else if (a6)
                    {
                        address.Town = p6;
                        if (a7)
                            address.Town += " " + p7;
                    }

                    address.PostCode = p8;
                    break;

                case 6:
                    address.SubBuilding = p1;
                    address.Street = p2;
                    address.Locality = p3;
                    address.Town = p6;
                    if (a7)
                        address.Town += " " + p7;

                    address.Town = address.Town.Trim();
                    address.PostCode = p8;
                    break;

                case 5:
                    address.Street = p1;
                    address.Locality = p2;

                    if (a3)
                        address.Town = p3;

                    if (a4)
                        address.Town += " " + p4;

                    if (a5)
                        address.Town += " " + p5;

                    if (a6)
                        address.Town += " " + p6;

                    if (a7)
                        address.Town += " " + p7;

                    address.Town = address.Town.Trim();

                    address.PostCode = p8;
                    break;

                case 4:
                    address.Street = p1;

                    if (a7)
                    {
                        address.Town = p7;
                        if (a6)
                            address.Locality = p6;
                        else if (a5)
                            address.Locality = p5;
                        else if (a4)
                            address.Locality = p4;
                        else if (a3)
                            address.Locality = p3;
                        else
                            address.Locality = p2;
                    }
                    else if (a6)
                    {
                        address.Town = p6;
                        if (a5)
                            address.Locality = p5;
                        else if (a4)
                            address.Locality = p4;
                        else if (a3)
                            address.Locality = p3;
                        else
                            address.Locality = p2;
                    }
                    else if (a5)
                    {
                        address.Town = p5;
                        if (a4)
                            address.Locality = p4;
                        else if (a3)
                            address.Locality = p3;
                        else
                            address.Locality = p2;
                    }

                    else if (a4)
                    {
                        address.Town = p4;
                        if (a3)
                            address.Locality = p3;
                        else
                            address.Locality = p2;
                    }

                    else if (a3)
                    {
                        address.Town = p3;
                        if (a3)
                            address.Locality = p3;
                        else
                            address.Locality = p2;

                    }

                    address.PostCode = p8;
                    break;

                case 3:
                    address.Street = p1;
                    address.PostCode = p8;
                    if (a2)
                        address.Town = p2;
                    else if (a3)
                        address.Town = p3;
                    else if (a4)
                        address.Town = p4;
                    else if (a5)
                        address.Town = p5;
                    else if (a6)
                        address.Town = p6;
                    else
                        address.Town = p7;
                    break;

                case 2:
                    address.Street = p1;
                    if (a8)
                        address.PostCode = p8;
                    else if (a7)
                        address.PostCode = p7;
                    else if (a6)
                        address.PostCode = p6;
                    else if (a5)
                        address.PostCode = p5;
                    else if (a4)
                        address.PostCode = p4;
                    else if (a3)
                        address.PostCode = p3;
                    else
                        address.PostCode = p2;
                    break;

                case 1:
                    address.Street = p1;
                    break;
            }

            return address;
        }
    }
}
