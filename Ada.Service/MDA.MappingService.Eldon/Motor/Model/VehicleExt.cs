﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Pipeline.Model;

namespace MDA.MappingService.Eldon.Model
{
    public class VehicleExt : PipelineVehicle
    {
        public string VehicleTypeCode { get; set; }

    }
}
