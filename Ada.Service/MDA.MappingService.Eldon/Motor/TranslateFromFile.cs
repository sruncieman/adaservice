﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.MappingService.Eldon.Model;

namespace MDA.MappingService.Eldon
{
    public class TranslateFromFile
    {
        public IEnumerable<string> ClaimBatchRows { get; set; }
        public List<int> ClaimRowNumbers { get; set; }
        public PipelineClaimBatch MotorClaimBatch { get; set; }

        public DateTime _incidentDate { get; set; }

        public DataTable ClaimsToDataTable(Stream filem)
        {
            DataTable dtTable = new DataTable();

            #region DataTable Columns
            dtTable.Columns.Add("FEED_TYPE");
            dtTable.Columns.Add("ROW_TYPE");
            dtTable.Columns.Add("ENQUIRY_EXTERNAL_UID"); // Claim Number
            dtTable.Columns.Add("PARTY_EXTERNAL_UID");
            dtTable.Columns.Add("PARTY_TYPE");
            dtTable.Columns.Add("ENTITY_EXTERNAL_UID");
            dtTable.Columns.Add("ENTITY_TYPE");
            dtTable.Columns.Add("01");
            dtTable.Columns.Add("02");
            dtTable.Columns.Add("03");
            dtTable.Columns.Add("04");
            dtTable.Columns.Add("05");
            dtTable.Columns.Add("06");
            dtTable.Columns.Add("07");
            dtTable.Columns.Add("08");
            dtTable.Columns.Add("09");
            dtTable.Columns.Add("10");
            dtTable.Columns.Add("11");
            dtTable.Columns.Add("12");
            dtTable.Columns.Add("13");
            dtTable.Columns.Add("14");
            dtTable.Columns.Add("15");
            dtTable.Columns.Add("16");
            dtTable.Columns.Add("17");
            dtTable.Columns.Add("18");
            dtTable.Columns.Add("19");
            dtTable.Columns.Add("20");
            dtTable.Columns.Add("21");
            dtTable.Columns.Add("22");
            dtTable.Columns.Add("23");
            dtTable.Columns.Add("24");
            dtTable.Columns.Add("25");
            dtTable.Columns.Add("26");
            dtTable.Columns.Add("27");
            dtTable.Columns.Add("28");
            dtTable.Columns.Add("29");
            dtTable.Columns.Add("30");
            dtTable.Columns.Add("31");
            dtTable.Columns.Add("32");
            dtTable.Columns.Add("33");
            dtTable.Columns.Add("34");
            dtTable.Columns.Add("35");
            dtTable.Columns.Add("36");
            dtTable.Columns.Add("37");
            dtTable.Columns.Add("38");
            dtTable.Columns.Add("39");
            dtTable.Columns.Add("40");
            dtTable.Columns.Add("41");
            dtTable.Columns.Add("42");
            dtTable.Columns.Add("43");
            dtTable.Columns.Add("44");
            dtTable.Columns.Add("45");
            dtTable.Columns.Add("46");
            dtTable.Columns.Add("47");
            dtTable.Columns.Add("48");
            dtTable.Columns.Add("49");
            dtTable.Columns.Add("50");
            dtTable.Columns.Add("51");
            dtTable.Columns.Add("52");
            dtTable.Columns.Add("53");
            dtTable.Columns.Add("54");
            #endregion

            try
            {
                // Read File into data table
                using (StreamReader sr = new StreamReader(filem))
                {
                    while (sr.Peek() > 1)
                    {
                        object[] currentLine = sr.ReadLine().Split(new char[] { '|' });
                        dtTable.Rows.Add(currentLine);
                    }
                }
                

                #region Add Row Counter

                dtTable.Columns.Add("RowCount", typeof(string));

                int count = 1;

                foreach (DataRow dr in dtTable.Rows)
                {
                    dr["RowCount"] = count;
                    count++;
                }

                var query = (from DataRow dr in dtTable.Rows
                             select dr);

                string counter = null;
                foreach (DataRow item in query)
                {
                    if (item["ROW_TYPE"].ToString() == "CLAIM_MOTOR")
                    {
                        counter = item[61].ToString();
                    }
                    else
                    {
                        item[61] = counter;
                    }
                }

            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method ClaimsToDataTable: " + ex);
            }

                #endregion

            return dtTable;
        }

        public Dictionary<string, string> GetDistinctClaims(DataTable dtt)
        {

            Dictionary<string, string> DictClaimNumber = new Dictionary<string, string>();

            try
            {

                ClaimBatchRows = (from DataRow dr in dtt.Rows
                                  where
                                      dr.Field<string>("ENQUIRY_EXTERNAL_UID") != null &&
                                      dr.Field<string>("ROW_TYPE") == "CLAIM_MOTOR"
                                  // && dr.Field<string>("Claim Number") == "13/0000565"
                                  select (string)dr["RowCount"]).Distinct();


                foreach (var cbr in ClaimBatchRows)
                {
                    var claimNum = (from DataRow dr in dtt.Rows
                                    where dr.Field<string>("RowCount") == cbr
                                    select (string)dr["ENQUIRY_EXTERNAL_UID"]).Distinct().ToList();

                    string ClaimNumber = claimNum.FirstOrDefault();

                    Console.WriteLine(cbr);
                    Console.WriteLine(ClaimNumber);
                    DictClaimNumber.Add(cbr, ClaimNumber);
                }

            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetDistinctClaims: " + ex);
            }

            return DictClaimNumber;
        }

        public List<int> GetDistinctClaimLinesForThisClaimNumber(string claimRow, DataTable dtt)
        {

            try
            {
                ClaimRowNumbers = (from DataRow dr in dtt.AsEnumerable()
                                   where
                                       dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimRow &&
                                       dr.Field<string>("ROW_TYPE") == "CLAIM_MOTOR"
                                   select (int)dr["RowCount"]).Distinct().ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetDistinctClaimLinesForThisClaimNumber: " + ex);
            }

            return ClaimRowNumbers;
        }

        private DateTime _ConvertDate(string s)
        {
            int year = 0;
            int month = 0;
            int day = 0;

            try
            {
                year = Convert.ToInt32(s.Substring(0, 4));
                month = Convert.ToInt32(s.Substring(4, 2));
                day = Convert.ToInt32(s.Substring(6, 2));
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method _ConvertDate: " + ex);
            }

            return new DateTime(year, month, day);
        }

        public PipelineMotorClaim GetClaim(DataTable dtt, KeyValuePair<string, string> claimNumber)
        {
            DateTime incidentDate = new DateTime();
            TimeSpan incidentTime;

            PipelineMotorClaim claim = new PipelineMotorClaim();

            var result = (from DataRow dr in dtt.AsEnumerable()
                          where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                          && dr.Field<string>("ROW_TYPE") == "CLAIM_MOTOR"
                          && dr.Field<string>("RowCount") == claimNumber.Key
                          select dr).FirstOrDefault();

            claim.ExtraClaimInfo = new PipelineClaimInfo();

            //DateTime claimNotificationDate;
            string incidentCircumstances = null;


            var partyMotorCollection = (from DataRow dr in dtt.AsEnumerable()
                                        where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value 
                                        && dr.Field<string>("ROW_TYPE") == "PARTY_MOTOR" 
                                        && dr[4].ToString() == "CLM"
                                        && dr.Field<string>("RowCount") == claimNumber.Key
                                        select dr).ToList();

            
            if (partyMotorCollection.Any())
            {
                foreach (var claimInfoRow in partyMotorCollection)
                {
                    if (!string.IsNullOrEmpty(claimInfoRow[44].ToString()))
                    {
                        claim.ExtraClaimInfo.PoliceReference = claimInfoRow[44].ToString();

                        claim.ExtraClaimInfo.PoliceAttended = true;
                    }

                }
            }

            var claimInfoCollection = (from DataRow dr in dtt.AsEnumerable()
                                       where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value 
                                       && dr.Field<string>("ROW_TYPE") == "CLAIM_MOTOR"
                                       && dr.Field<string>("RowCount") == claimNumber.Key
                                       select dr).ToList();

            if (claimInfoCollection.Any())
            {
                foreach (var claimInfoRow in claimInfoCollection)
                {
                    if (!string.IsNullOrEmpty(claimInfoRow[15].ToString()))
                    {
                        switch (claimInfoRow[15].ToString().ToUpper())
                        {
                            case "OPEN":
                                claim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                break;
                            case "CLOSED":
                                claim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                break;
                            case "REOPENED":
                                claim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                                break;
                            default:
                                claim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                                break;
                        }
                    }

                    var previousClaimInfoCollection = (from DataRow dr in dtt.AsEnumerable()
                                                       where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                                       && dr.Field<string>("ROW_TYPE") == "PREVIOUS_CLAIM"
                                                       && dr.Field<string>("RowCount") == claimNumber.Key
                                                       select dr).ToList();

                    if (!string.IsNullOrEmpty(claimInfoRow[21].ToString()))
                    {
                        claim.ExtraClaimInfo.ClaimCode = claimInfoRow[21].ToString();


                        claim.ExtraClaimInfo.ClaimCode += ";";

                        foreach (var c in previousClaimInfoCollection)
                        {
                            claim.ExtraClaimInfo.ClaimCode += "[" + c[7] + "," + c[8] + "," + c[9] + "],";

                        }
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[13].ToString()))
                    {
                        //string incidentTempTimeDate = DateTime.Now.ToShortDateString();

                        //incidentDate = Convert.ToDateTime(incidentTempTimeDate);

                        //TimeSpan incidentTime;

                        //if (TimeSpan.TryParse(claimInfoRow[13].ToString(), out incidentTime))
                        //    claimInfo.IncidentTime = incidentTime;
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[32].ToString()))
                    {
                        claim.ExtraClaimInfo.IncidentLocation = claimInfoRow[32].ToString();
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[31].ToString()))
                    {
                        incidentCircumstances += claimInfoRow[31].ToString();
                        claim.ExtraClaimInfo.IncidentCircumstances = incidentCircumstances;
                    }
                    if (!string.IsNullOrEmpty(claimInfoRow[10].ToString()))
                    {
                        claim.ExtraClaimInfo.ClaimNotificationDate = _ConvertDate(claimInfoRow[10].ToString());
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[24].ToString()))
                    {
                        claim.ExtraClaimInfo.PaymentsToDate = Convert.ToDecimal(claimInfoRow[24].ToString());
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[23].ToString()))
                    {
                        claim.ExtraClaimInfo.Reserve = Convert.ToDecimal(claimInfoRow[23].ToString());
                    }

                }
            }

            //if (claimInfo != null)
            //{
            //    claim.MotorClaimInfo = claimInfo;
            //}

            if (!string.IsNullOrEmpty(result[2].ToString()))
            {
                claim.ClaimNumber = (string)result[2];
            }

            claim.ClaimType_Id = (int)ClaimType.Motor;

            if (!string.IsNullOrEmpty(result[12].ToString()))
            {
                incidentDate = _ConvertDate(result[12].ToString());
                claim.IncidentDate = incidentDate;
            }

            if (!string.IsNullOrEmpty(result[13].ToString()))
            {
                if (TimeSpan.TryParse(result[13].ToString(), out incidentTime))
                    incidentDate = incidentDate.Add(incidentTime);
                claim.IncidentDate = incidentDate;
            }

            if (claim.ExtraClaimInfo != null && !string.IsNullOrEmpty(result[24].ToString()))
            {
                if (Convert.ToDecimal(result[24]) > (decimal)0.00)
                    claim.ExtraClaimInfo.PaymentsToDate = Convert.ToDecimal(result[24]);
            }

            if (claim.ExtraClaimInfo != null && !string.IsNullOrEmpty(result[23].ToString()))
            {
                if (Convert.ToDecimal(result[23]) > (decimal)0.00)
                    claim.ExtraClaimInfo.Reserve = Convert.ToDecimal(result[23]);
            }

            return claim;
        }

        public PipelinePolicy GetPolicyDetails(DataTable dtt, KeyValuePair<string, string> claimNumber)
        {
            PipelinePolicy policy = new PipelinePolicy();

            policy.PolicyType_Id = (int)PolicyType.PersonalMotor;
            policy.PolicyCoverType_Id = (int)PolicyCoverType.Unknown;

            try
            {

                var result = (from DataRow dr in dtt.AsEnumerable()
                              where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value  
                              && dr.Field<string>("ROW_TYPE") == "CLAIM_MOTOR"
                              && dr.Field<string>("RowCount") == claimNumber.Key
                              select dr).FirstOrDefault();

                if (!string.IsNullOrEmpty(result[18].ToString()))
                {
                    policy.PolicyNumber = (string)result[18];
                }

                int noFaultClaimCount = (from DataRow dr in dtt.AsEnumerable()
                                         where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                         && dr.Field<string>("ROW_TYPE") == "PREVIOUS_CLAIM"
                                         && dr[9].ToString().Contains("N")
                                         && dr.Field<string>("RowCount") == claimNumber.Key
                                         select dr).Count();

                if (noFaultClaimCount > 0)
                {
                    policy.PreviousNoFaultClaimsCount = noFaultClaimCount;
                }


                int faultClaimCount = (from DataRow dr in dtt.AsEnumerable()
                                         where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                         && dr.Field<string>("ROW_TYPE") == "PREVIOUS_CLAIM"
                                         && dr[9].ToString().Contains("Y")
                                         && dr.Field<string>("RowCount") == claimNumber.Key
                                         select dr).Count();

                if (faultClaimCount > 0)
                {
                    policy.PreviousFaultClaimsCount = faultClaimCount;
                }


            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetPolicyDetails(ENQUIRY_EXTERNAL_UID " + claimNumber + ": " + ex);
            }

            return policy;
        }

        public List<PipelineVehicle> GetVehicleDetails(DataTable dtt, KeyValuePair<string, string> claimNumber)
        {
            List<PipelineVehicle> vehicles = new List<PipelineVehicle>();

            List<string> partyTypeCodes = new List<string>();
            List<string> subPartyType = new List<string>();

            try
            {

                var result = (from DataRow dr in dtt.AsEnumerable()
                              where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                              && dr.Field<string>("ROW_TYPE") == "VEHICLE"
                              && dr.Field<string>("RowCount") == claimNumber.Key
                              select dr).ToList();

                if (result != null)
                {
                    foreach (var dataRow in result)
                    {
                        PipelineVehicle vehicle = new PipelineVehicle();

                        vehicle.VehicleType_Id = (int)VehicleType.Unknown;

                        //if (!string.IsNullOrEmpty(dataRow[6].ToString()))
                        //{
                        //    vehicle.VehicleTypeCode = (string)dataRow[6];
                        //}

                        if (!string.IsNullOrEmpty(dataRow[7].ToString()))
                        {
                            vehicle.VehicleRegistration = (string)dataRow[7];

                        }
                            // Find VehicleInvolvementGroup
                            //var assetRelationshipCollection = (from DataRow dr in dtt.AsEnumerable()
                            //                                   where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                            //                                   && dr.Field<string>("ROW_TYPE") == "ASSET_RELATIONSHIP"
                            //                                   && dr[7].ToString() == vehicle.VehicleRegistration
                            //                                   && dr.Field<string>("RowCount") == claimNumber.Key
                            //                                   select dr).ToList();

                            //if (assetRelationshipCollection != null)
                            //{
                            //    foreach (var assetRelationshipRow in assetRelationshipCollection)
                            //    {
                            //        if (!string.IsNullOrEmpty(assetRelationshipRow[4].ToString())) { partyTypeCodes.Add(assetRelationshipRow[4].ToString()); }
                            //        if (!string.IsNullOrEmpty(assetRelationshipRow[6].ToString())) { subPartyType.Add(assetRelationshipRow[6].ToString()); }
                            //    }
                            //}

                        #region Vehicle Involvement Groups (Incident2VehcileLinkType)

                        if (((string)dataRow[6]).Contains("INV"))
                        {
                            vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                        }

                        if (((string)dataRow[6]).Contains("TPV"))
                        {
                            vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                        }

                        #endregion

                        #region Color
                        if (!string.IsNullOrEmpty(dataRow[26].ToString()))
                        {
                            GetVehicleColor(dataRow, vehicle);
                        }
                        #endregion

                        if (!string.IsNullOrEmpty(dataRow[14].ToString()))
                        {
                            vehicle.VehicleMake = (string)dataRow[14];
                        }
                        if (!string.IsNullOrEmpty(dataRow[15].ToString()))
                        {
                            vehicle.VehicleModel = (string)dataRow[15];
                        }
                        if (!string.IsNullOrEmpty(dataRow[9].ToString()))
                        {
                            vehicle.EngineCapacity = (string)dataRow[9];
                        }
                        if (!string.IsNullOrEmpty(dataRow[25].ToString()))
                        {
                            vehicle.VIN = (string)dataRow[25];
                        }

                        vehicles.Add(vehicle);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetVehicleDetails(ENQUIRY_EXTERNAL_UID " + claimNumber + ": " + ex);
            }
            return vehicles;
        }

        private static void GetVehicleColor(DataRow dataRow, PipelineVehicle vehicle)
        {

            switch (dataRow[26].ToString().ToUpper())
            {
                case "BLUE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Blue;
                    break;
                case "BEIGE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Beige;
                    break;
                case "BLACK":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Black;
                    break;
                case "BRONZE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Bronze;
                    break;
                case "BROWN":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Brown;
                    break;
                case "CREAM":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Cream;
                    break;
                case "GOLD":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Gold;
                    break;
                case "GRAPHITE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Graphite;
                    break;
                case "GREEN":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Green;
                    break;
                case "GREY":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Grey;
                    break;
                case "LILAC":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Lilac;
                    break;
                case "MAROON":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Maroon;
                    break;
                case "MAUVE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Mauve;
                    break;
                case "ORANGE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Orange;
                    break;
                case "PINK":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Pink;
                    break;
                case "PURPLE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Purple;
                    break;
                case "RED":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Red;
                    break;
                case "SILVER":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Silver;
                    break;
                case "TURQUOISE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Turquoise;
                    break;
                case "WHITE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.White;
                    break;
                case "YELLOW":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Yellow;
                    break;
                default:
                    vehicle.VehicleColour_Id = (int)VehicleColour.Unknown;
                    break;
            }
        }

        public List<PipelinePerson> GetNoneVehiclePeopleDetails(DataTable dtt, KeyValuePair<string, string> claimNumber)
        {
            List<PipelinePerson> people = new List<PipelinePerson>();

            try
            {
                // WITNESS (No Vehicle)
                var distinctWitness = (from DataRow dr in dtt.AsEnumerable()
                                       where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                       && dr.Field<string>("ROW_TYPE") == "PARTY_MOTOR"
                                       && dr[4].ToString().Contains("WIT") 
                                       && dr[26].ToString() == ""
                                       && dr.Field<string>("RowCount") == claimNumber.Key
                                       select dr[3]).ToList().Distinct();


                foreach (var witness in distinctWitness)
                {

                    PipelinePerson person = new PipelinePerson();
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    //??person.ClaimInfo = null;

                    var getWitnessPartyMotorCollection = (from DataRow dr in dtt.AsEnumerable()
                                                          where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value 
                                                          && dr[3].ToString().Contains(witness.ToString()) 
                                                          && dr.Field<string>("ROW_TYPE") == "PARTY_MOTOR" 
                                                          && dr.Field<string>("RowCount") == claimNumber.Key
                                                          select dr).ToList();

                    if (getWitnessPartyMotorCollection.Any())
                    {
                        foreach (var dataRow in getWitnessPartyMotorCollection)
                        {
                            #region Saltutation

                            if (!string.IsNullOrEmpty(dataRow[7].ToString()))
                            {
                                switch (dataRow[7].ToString().ToUpper())
                                {
                                    case "MR":
                                        person.Salutation_Id = (int)Salutation.Mr;
                                        break;
                                    case "MRS":
                                        person.Salutation_Id = (int)Salutation.Mrs;
                                        break;
                                    case "MS":
                                        person.Salutation_Id = (int)Salutation.Ms;
                                        break;
                                    case "MISS":
                                        person.Salutation_Id = (int)Salutation.Miss;
                                        break;
                                    case "MASTER":
                                        person.Salutation_Id = (int)Salutation.Master;
                                        break;
                                    case "DR":
                                        person.Salutation_Id = (int)Salutation.Dr;
                                        break;
                                    case "Rev":
                                        person.Salutation_Id = (int)Salutation.Reverend;
                                        break;
                                    default:
                                        person.Salutation_Id = (int)Salutation.Unknown;
                                        break;
                                }
                            }

                            #endregion

                            if (!string.IsNullOrEmpty(dataRow[9].ToString()))
                            {
                                person.FirstName = dataRow[9].ToString();
                            }
                            if (!string.IsNullOrEmpty(dataRow[10].ToString()))
                            {
                                person.MiddleName = dataRow[10].ToString();
                            }
                            if (!string.IsNullOrEmpty(dataRow[11].ToString()))
                            {
                                person.LastName = dataRow[11].ToString();
                            }
                            if (!string.IsNullOrEmpty(dataRow[12].ToString()))
                            {
                                person.DateOfBirth = _ConvertDate(dataRow[12].ToString());

                                //string dobTempDate = dataRow[12].ToString().Substring(6, 2) + "/" +
                                //                     dataRow[12].ToString().Substring(4, 2) + "/" +
                                //                     dataRow[12].ToString().Substring(0, 4);

                                //person.DateOfBirth = Convert.ToDateTime(dobTempDate);
                            }

                            if (!string.IsNullOrEmpty(dataRow[15].ToString()))
                            {
                                switch (dataRow[15].ToString().ToUpper())
                                {
                                    case "MALE":
                                        person.Gender_Id = (int)Gender.Male;
                                        break;
                                    case "FEMALE":
                                        person.Gender_Id = (int)Gender.Female;
                                        break;
                                    default:
                                        person.Gender_Id = (int)Gender.Unknown;
                                        break;
                                }
                            }

                            if (!string.IsNullOrEmpty(dataRow[17].ToString()))
                                person.Nationality = dataRow[17].ToString();
                        }


                        var contactCollection = (from DataRow dr in dtt.AsEnumerable()
                                                 where
                                                     dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value &&
                                                     dr.Field<string>("ROW_TYPE") == "CONTACT" &&
                                                     dr[3].ToString().Contains(witness.ToString())
                                                     && dr.Field<string>("RowCount") == claimNumber.Key
                                                 select dr).ToList();

                        if (contactCollection.Any())
                        {
                            foreach (var dataRowContact in contactCollection)
                            {
                                if (!string.IsNullOrEmpty(dataRowContact[6].ToString()))
                                {
                                    switch (dataRowContact[6].ToString())
                                    {
                                        case "WRKEML":
                                        case "GENEML":
                                        case "HOMEML":
                                            person.EmailAddresses.Add(new PipelineEmail() { EmailAddress = dataRowContact[7].ToString() });
                                            //person.EmailAddress = dataRowContact[7].ToString();
                                            break;
                                    }

                                    if (dataRowContact[6].ToString() == "HOMTEL")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                        //person.LandlineTelephone = dataRowContact[7].ToString();
                                    }

                                    if (dataRowContact[6].ToString() == "WRKTEL")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                        //person.WorkTelephone = dataRowContact[7].ToString();
                                    }

                                    if (dataRowContact[6].ToString() == "MOBILE")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                        //person.MobileTelephone = dataRowContact[7].ToString();
                                    }

                                    if (dataRowContact[6].ToString() == "OTHTEL")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                                        //person.OtherTelephone = dataRowContact[7].ToString();
                                    }
                                }
                            }
                        }

                        #region Address

                        var addressCollection = (from DataRow dr in dtt.AsEnumerable()
                                                 where
                                                     dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value &&
                                                     dr.Field<string>("ROW_TYPE") == "ADDRESS" &&
                                                     dr[3].ToString().Contains(witness.ToString())
                                                     && dr.Field<string>("RowCount") == claimNumber.Key
                                                 select dr).ToList();

                        PipelineAddress address = new PipelineAddress();
                        if (addressCollection.Any())
                        {

                            foreach (var dataRowAddress in addressCollection)
                            {


                                if (!string.IsNullOrEmpty(dataRowAddress[6].ToString()))
                                {
                                    if (dataRowAddress[6].ToString() == "CURRNT")
                                        address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[17].ToString()))
                                {
                                    address.PO2A_LinkData.StartOfResidency = _ConvertDate(dataRowAddress[17].ToString());
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[18].ToString()))
                                {
                                    address.PO2A_LinkData.EndOfResidency = _ConvertDate(dataRowAddress[18].ToString());
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[7].ToString()))
                                {
                                    address.SubBuilding = dataRowAddress[7].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[9].ToString()))
                                {
                                    address.BuildingNumber = dataRowAddress[9].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[10].ToString()))
                                {
                                    address.Street = dataRowAddress[10].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[11].ToString()))
                                {
                                    address.Locality = dataRowAddress[11].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[12].ToString()))
                                {
                                    address.Town = dataRowAddress[12].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[13].ToString()))
                                {
                                    address.County = dataRowAddress[13].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[14].ToString()))
                                {
                                    address.PostCode = dataRowAddress[14].ToString();
                                }

                                //address = AddressFormatHelper.PopulateAddress(address, dataRowAddress[7].ToString(), dataRowAddress[8].ToString(), dataRowAddress[9].ToString(), dataRowAddress[10].ToString(), dataRowAddress[11].ToString(), dataRowAddress[12].ToString(), dataRowAddress[13].ToString(), dataRowAddress[14].ToString());


                                person.Addresses.Add(address);
                            }
                        }

                        #endregion

                    }

                    people.Add(person);
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetNoneVehiclePeopleDetails(ENQUIRY_EXTERNAL_UID " + claimNumber + ": " + ex);
            }

            return people;
        }

        public List<PipelinePerson> GetTPPeopleDetailsWithNoVehicle(DataTable dtt, KeyValuePair<string, string> claimNumber)
        {
            List<PipelinePerson> people = new List<PipelinePerson>();

            try
            {


                var assetRelationshipCollection = (from DataRow dr in dtt.AsEnumerable()
                                                   where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                                   && dr.Field<string>("ROW_TYPE") == "ASSET_RELATIONSHIP"
                                                   //&& dr[4].ToString().Contains("THP") || dr[4].ToString().Contains("THPDRV")
                                                   && dr.Field<string>("RowCount") == claimNumber.Key
                                                   select dr[3]).ToList();


                // THP (No Vehicle)
                var thirdParties = (from DataRow dr in dtt.AsEnumerable()
                                            where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                            && dr.Field<string>("ROW_TYPE") == "PARTY_MOTOR"
                                            && dr[4].ToString().Contains("THP") || dr[4].ToString().Contains("THPDRV") 
                                            && dr[26].ToString() == ""
                                            //&& !assetRelationshipCollection.Contains(dr.Field<string>("03")) //(dr[3].ToString())
                                            && dr.Field<string>("RowCount") == claimNumber.Key
                                            select dr[3]).ToList().Distinct();

                var distinctThirdParties = thirdParties.Except(assetRelationshipCollection);



                foreach (var thirdParty in distinctThirdParties)
                {

                    PipelinePerson person = new PipelinePerson();
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                    
                    //??person.ClaimInfo = null;

                    var getWitnessPartyMotorCollection = (from DataRow dr in dtt.AsEnumerable()
                                                          where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                                          && dr[3].ToString().Contains(thirdParty.ToString())
                                                          && dr.Field<string>("ROW_TYPE") == "PARTY_MOTOR"
                                                          && dr.Field<string>("RowCount") == claimNumber.Key
                                                          select dr).ToList();

                    if (getWitnessPartyMotorCollection.Any())
                    {
                        foreach (var dataRow in getWitnessPartyMotorCollection)
                        {
                            #region Saltutation

                            if (!string.IsNullOrEmpty(dataRow[7].ToString()))
                            {
                                switch (dataRow[7].ToString().ToUpper())
                                {
                                    case "MR":
                                        person.Salutation_Id = (int)Salutation.Mr;
                                        break;
                                    case "MRS":
                                        person.Salutation_Id = (int)Salutation.Mrs;
                                        break;
                                    case "MS":
                                        person.Salutation_Id = (int)Salutation.Ms;
                                        break;
                                    case "MISS":
                                        person.Salutation_Id = (int)Salutation.Miss;
                                        break;
                                    case "MASTER":
                                        person.Salutation_Id = (int)Salutation.Master;
                                        break;
                                    case "DR":
                                        person.Salutation_Id = (int)Salutation.Dr;
                                        break;
                                    case "Rev":
                                        person.Salutation_Id = (int)Salutation.Reverend;
                                        break;
                                    default:
                                        person.Salutation_Id = (int)Salutation.Unknown;
                                        break;
                                }
                            }

                            #endregion

                            if (!string.IsNullOrEmpty(dataRow[9].ToString()))
                            {
                                person.FirstName = dataRow[9].ToString();
                            }
                            if (!string.IsNullOrEmpty(dataRow[10].ToString()))
                            {
                                person.MiddleName = dataRow[10].ToString();
                            }
                            if (!string.IsNullOrEmpty(dataRow[11].ToString()))
                            {
                                person.LastName = dataRow[11].ToString();
                            }
                            if (!string.IsNullOrEmpty(dataRow[12].ToString()))
                            {
                                person.DateOfBirth = _ConvertDate(dataRow[12].ToString());

                                //string dobTempDate = dataRow[12].ToString().Substring(6, 2) + "/" +
                                //                     dataRow[12].ToString().Substring(4, 2) + "/" +
                                //                     dataRow[12].ToString().Substring(0, 4);

                                //person.DateOfBirth = Convert.ToDateTime(dobTempDate);
                            }

                            if (!string.IsNullOrEmpty(dataRow[15].ToString()))
                            {
                                switch (dataRow[15].ToString().ToUpper())
                                {
                                    case "MALE":
                                        person.Gender_Id = (int)Gender.Male;
                                        break;
                                    case "FEMALE":
                                        person.Gender_Id = (int)Gender.Female;
                                        break;
                                    default:
                                        person.Gender_Id = (int)Gender.Unknown;
                                        break;
                                }
                            }

                            if (!string.IsNullOrEmpty(dataRow[17].ToString()))
                                person.Nationality = dataRow[17].ToString();
                        }


                        var contactCollection = (from DataRow dr in dtt.AsEnumerable()
                                                 where
                                                     dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value &&
                                                     dr.Field<string>("ROW_TYPE") == "CONTACT" &&
                                                     dr[3].ToString().Contains(thirdParty.ToString())
                                                     && dr.Field<string>("RowCount") == claimNumber.Key
                                                 select dr).ToList();

                        if (contactCollection.Any())
                        {
                            foreach (var dataRowContact in contactCollection)
                            {
                                if (!string.IsNullOrEmpty(dataRowContact[6].ToString()))
                                {
                                    switch (dataRowContact[6].ToString())
                                    {
                                        case "WRKEML":
                                        case "GENEML":
                                        case "HOMEML":
                                            person.EmailAddresses.Add(new PipelineEmail() { EmailAddress = dataRowContact[7].ToString() });
                                            //person.EmailAddress = dataRowContact[7].ToString();
                                            break;
                                    }

                                    if (dataRowContact[6].ToString() == "HOMTEL")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                        //person.LandlineTelephone = dataRowContact[7].ToString();
                                    }

                                    if (dataRowContact[6].ToString() == "WRKTEL")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                        //person.WorkTelephone = dataRowContact[7].ToString();
                                    }

                                    if (dataRowContact[6].ToString() == "MOBILE")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                        //person.MobileTelephone = dataRowContact[7].ToString();
                                    }

                                    if (dataRowContact[6].ToString() == "OTHTEL")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                                        //person.OtherTelephone = dataRowContact[7].ToString();
                                    }
                                }
                            }
                        }

                        #region Address

                        var addressCollection = (from DataRow dr in dtt.AsEnumerable()
                                                 where
                                                     dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value &&
                                                     dr.Field<string>("ROW_TYPE") == "ADDRESS" &&
                                                     dr[3].ToString().Contains(thirdParty.ToString())
                                                     && dr.Field<string>("RowCount") == claimNumber.Key
                                                 select dr).ToList();

                        PipelineAddress address = new PipelineAddress();
                        if (addressCollection.Any())
                        {

                            foreach (var dataRowAddress in addressCollection)
                            {


                                if (!string.IsNullOrEmpty(dataRowAddress[6].ToString()))
                                {
                                    if (dataRowAddress[6].ToString() == "CURRNT")
                                        address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[17].ToString()))
                                {
                                    address.PO2A_LinkData.StartOfResidency = _ConvertDate(dataRowAddress[17].ToString());
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[18].ToString()))
                                {
                                    address.PO2A_LinkData.EndOfResidency = _ConvertDate(dataRowAddress[18].ToString());
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[7].ToString()))
                                {
                                    address.SubBuilding = dataRowAddress[7].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[9].ToString()))
                                {
                                    address.BuildingNumber = dataRowAddress[9].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[10].ToString()))
                                {
                                    address.Street = dataRowAddress[10].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[11].ToString()))
                                {
                                    address.Locality = dataRowAddress[11].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[12].ToString()))
                                {
                                    address.Town = dataRowAddress[12].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[13].ToString()))
                                {
                                    address.County = dataRowAddress[13].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[14].ToString()))
                                {
                                    address.PostCode = dataRowAddress[14].ToString();
                                }

                                //address = AddressFormatHelper.PopulateAddress(address, dataRowAddress[7].ToString(), dataRowAddress[8].ToString(), dataRowAddress[9].ToString(), dataRowAddress[10].ToString(), dataRowAddress[11].ToString(), dataRowAddress[12].ToString(), dataRowAddress[13].ToString(), dataRowAddress[14].ToString());


                                person.Addresses.Add(address);
                            }
                        }

                        #endregion

                    }

                    people.Add(person);
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetNoneVehiclePeopleDetails(ENQUIRY_EXTERNAL_UID " + claimNumber + ": " + ex);
            }

            return people;
        }

        public List<PipelineOrganisation> GetNoneVehicleOrganisationDetails(DataTable dtt, KeyValuePair<string, string> claimNumber)
        {

            List<PipelineOrganisation> organisations = new List<PipelineOrganisation>();

            var organisationCollection = (from DataRow dr in dtt.AsEnumerable()
                                          where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                          && dr[4].ToString() == "WIT"
                                          && dr[26].ToString() != ""
                                          && dr.Field<string>("RowCount") == claimNumber.Key
                                          select dr).ToList().Distinct();


            if (organisationCollection.Any())
            {

                foreach (var dataRowOrganisation in organisationCollection)
                {

                    PipelineOrganisation organisation = new PipelineOrganisation();

                    organisation.I2O_LinkData.PartyType_Id = (int)PartyType.Witness;
                    organisation.I2O_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                    if (!string.IsNullOrEmpty(dataRowOrganisation[26].ToString()))
                    {
                        organisation.OrganisationName = dataRowOrganisation[26].ToString();
                    }
                    if (!string.IsNullOrEmpty(dataRowOrganisation[28].ToString()))
                    {
                        organisation.RegisteredNumber = dataRowOrganisation[28].ToString();
                    }
                    if (!string.IsNullOrEmpty(dataRowOrganisation[33].ToString()))
                    {
                        organisation.VatNumber = dataRowOrganisation[33].ToString();
                    }


                    var organisationContactCollection = (from DataRow dr in dtt.AsEnumerable()
                                                            where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value 
                                                            && dr.Field<string>("ROW_TYPE") == "CONTACT" 
                                                            && dr[3].ToString() == dataRowOrganisation[3].ToString()
                                                            && dr.Field<string>("RowCount") == claimNumber.Key
                                                            select dr).ToList();

                    if (organisationContactCollection.Any())
                    {

                        foreach (var dataRowOrganisationContactCollection in organisationContactCollection)
                        {
                            if (!string.IsNullOrEmpty(dataRowOrganisationContactCollection[6].ToString()))
                            {
                                switch (dataRowOrganisationContactCollection[6].ToString())
                                {
                                    case "WRKEML":
                                    case "GENEML":
                                    case "HOMEML":
                                        organisation.EmailAddresses.Add(new PipelineEmail() { EmailAddress = dataRowOrganisationContactCollection[6].ToString() });
                                        //organisation.Email = dataRowOrganisationContactCollection[7].ToString();
                                        break;
                                }

                                if (dataRowOrganisationContactCollection[6].ToString() == "CONTEL")
                                {
                                    organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                    //organisation.Telephone1 = dataRowOrganisationContactCollection[7].ToString();
                                }

                                if (dataRowOrganisationContactCollection[6].ToString() == "BUSTEL")
                                {
                                    organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                    //organisation.Telephone2 = dataRowOrganisationContactCollection[7].ToString();
                                }

                                if (dataRowOrganisationContactCollection[6].ToString() == "MOBILE")
                                {
                                    organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                    //organisation.Telephone3 = dataRowOrganisationContactCollection[7].ToString();
                                }

                                if (dataRowOrganisationContactCollection[6].ToString() == "GENTEL")
                                {
                                    organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                                    //organisation.Telephone3 =dataRowOrganisationContactCollection[7].ToString();
                                }
                            }
                        
                        }
                    }

                    organisations.Add(organisation);
                }

            }

            return organisations;
        }

        public List<PipelinePerson> GetVehiclePeopleDetails(DataTable dtt, KeyValuePair<string, string> claimNumber, string vehicleRegistration)
        {
            List<PipelinePerson> people = new List<PipelinePerson>();

            //string vehicleInvolvementType = "";

            try
            {
                Dictionary<string, string> DictParties = new Dictionary<string, string>();
                string occupation = null;


                var vehicleInvolvementType = (from DataRow dr in dtt.AsEnumerable()
                                              where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                              && dr.Field<string>("ROW_TYPE") == "VEHICLE"
                                              && dr.Field<string>("RowCount") == claimNumber.Key
                                              && dr[5].ToString() == vehicleRegistration
                                              select dr[6]).FirstOrDefault();

                var assetRelationshipCollection = (from DataRow dr in dtt.AsEnumerable()
                                                   where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value 
                                                   && dr.Field<string>("ROW_TYPE") == "ASSET_RELATIONSHIP" 
                                                   && dr[7].ToString() == vehicleRegistration 
                                                   && dr.Field<string>("RowCount") == claimNumber.Key
                                                   select dr).ToList();

                
                if (assetRelationshipCollection != null)
                {
                    foreach (var dataRow in assetRelationshipCollection)
                    {
                        DictParties.Add(dataRow[3].ToString(), dataRow[6].ToString());
                    }

                }

                if (DictParties != null)
                {
                    foreach (var dictItem in DictParties)
                    {
                        var partyMotorCollection = (from DataRow dr in dtt.AsEnumerable()
                                                    where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value 
                                                    && dr.Field<string>("ROW_TYPE") == "PARTY_MOTOR" 
                                                    && dr[3].ToString() == dictItem.Key
                                                    && !dr[4].ToString().Contains("TPC") 
                                                    && !dr[4].ToString().Contains("THPRPR")
                                                    && dr.Field<string>("RowCount") == claimNumber.Key
                                                    select dr).ToList();

                        if (partyMotorCollection != null)
                        {
                            foreach (var dataRow in partyMotorCollection)
                            {
                                PipelinePerson person = new PipelinePerson();
                                if (dataRow[4].ToString() == "CLM")
                                {
                                    PipelineClaimInfo ci = GetPersonClaimInfo(dtt, claimNumber, dataRow);
                                }

                                #region PartyTypes

                                if (dataRow[4].ToString() == "CLM" && dictItem.Value.Contains("DRV"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (dataRow[4].ToString() == "CLM" && dictItem.Value.Contains("OWN"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                if (dataRow[4].ToString() == "CLM" && dictItem.Value.Contains("PAS"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                }

                                if (dataRow[4].ToString() == "THP" && dictItem.Value.Contains("OWN"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                if (dataRow[4].ToString() == "THP" && dictItem.Value.Contains("PAS"))
                                {

                                    if (vehicleInvolvementType.ToString() == "INV")
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                    }
                                    else if (vehicleInvolvementType.ToString() == "TPV")
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                    }
                                }

                                if (dataRow[4].ToString() == "TVO" && dictItem.Value.Contains("DRV"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (dataRow[4].ToString() == "TVO" && dictItem.Value.Contains("OWN"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                if (dataRow[4].ToString() == "THP" && dictItem.Value.Contains("DRV"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (dataRow[4].ToString() == "THPDRV" && dictItem.Value.Contains("DRV"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (dataRow[4].ToString() == "TVO" && dictItem.Value.Contains("DRV"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (dataRow[4].ToString() == "TVO" && dictItem.Value.Contains("OWN"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                if (dataRow[4].ToString() == "THP" && dictItem.Value.Contains("OWN"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                //if (dataRow[4].ToString() == "THP" && dictItem.Value.Contains("PAS"))
                                //{
                                //    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                //    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                //}

                                if (dataRow[4].ToString() == "THP" && dictItem.Value.Contains("KPR"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                if (dataRow[4].ToString() == "THP" && string.IsNullOrEmpty(dictItem.Value))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                if (dataRow[4].ToString() == "THP" && dictItem.Value.Contains("UNKNOWN"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }


                                #endregion

                                #region Saltutation

                                if (!string.IsNullOrEmpty(dataRow[7].ToString()))
                                {
                                    switch (dataRow[7].ToString().ToUpper())
                                    {
                                        case "MR":
                                            person.Salutation_Id = (int)Salutation.Mr;
                                            person.Gender_Id = (int)Gender.Male;
                                            break;
                                        case "MRS":
                                            person.Salutation_Id = (int)Salutation.Mrs;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                        case "MS":
                                            person.Salutation_Id = (int)Salutation.Ms;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                        case "MISS":
                                            person.Salutation_Id = (int)Salutation.Miss;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                        case "MASTER":
                                            person.Salutation_Id = (int)Salutation.Master;
                                            person.Gender_Id = (int)Gender.Male;
                                            break;
                                        case "DR":
                                            person.Salutation_Id = (int)Salutation.Dr;
                                            break;
                                        case "Rev":
                                            person.Salutation_Id = (int)Salutation.Reverend;
                                            break;
                                        default:
                                            person.Salutation_Id = (int)Salutation.Unknown;
                                            break;
                                    }
                                }

                                #endregion

                                if (!string.IsNullOrEmpty(dataRow[9].ToString()))
                                {
                                    person.FirstName = dataRow[9].ToString();
                                }
                                if (!string.IsNullOrEmpty(dataRow[10].ToString()))
                                {
                                    person.MiddleName = dataRow[10].ToString();
                                }
                                if (!string.IsNullOrEmpty(dataRow[11].ToString()))
                                {
                                    person.LastName = dataRow[11].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRow[12].ToString()))
                                {
                                    person.DateOfBirth = _ConvertDate(dataRow[12].ToString());
                                }

                                if (!string.IsNullOrEmpty(dataRow[15].ToString()))
                                {
                                    switch (dataRow[15].ToString().ToUpper())
                                    {
                                        case "MALE":
                                            person.Gender_Id = (int)Gender.Male;
                                            break;
                                        case "FEMALE":
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                        default:
                                            person.Gender_Id = (int)Gender.Unknown;
                                            break;
                                    }
                                }

                                if (!string.IsNullOrEmpty(dataRow[17].ToString()))
                                    person.Nationality = dataRow[17].ToString();


                                var identificationCollection = (from DataRow dr in dtt.AsEnumerable()
                                                                where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value 
                                                                && dr.Field<string>("ROW_TYPE") == "IDENTIFICATION" 
                                                                && dr[3].ToString() == dictItem.Key
                                                                && dr.Field<string>("RowCount") == claimNumber.Key
                                                                select dr).ToList();

                                if (identificationCollection != null)
                                {
                                    foreach (var dataRowIdent in identificationCollection)
                                    {
                                        if (!string.IsNullOrEmpty(dataRowIdent[7].ToString()))
                                            person.NINumbers.Add(new PipelineNINumber() { NINumber1 = dataRowIdent[7].ToString() });
                                        //person.NINumber = dataRowIdent[7].ToString();

                                        if (!string.IsNullOrEmpty(dataRowIdent[9].ToString()))
                                            person.DrivingLicenseNumbers.Add(new PipelineDrivingLicense() { DriverNumber = dataRowIdent[9].ToString() });
                                        //person.DrivingLicenseNumber = dataRowIdent[9].ToString();

                                        if (!string.IsNullOrEmpty(dataRowIdent[8].ToString()))
                                            person.PassportNumbers.Add(new PipelinePassport() { PassportNumber = dataRowIdent[8].ToString() });
                                        // person.PassportNumber = dataRowIdent[8].ToString();
                                    }
                                }


                                var occupationCollection = (from DataRow dr in dtt.AsEnumerable()
                                                            where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value 
                                                            && dr.Field<string>("ROW_TYPE") == "EMPLOYMENT" 
                                                            && dr[3].ToString() == dictItem.Key
                                                            && dr.Field<string>("RowCount") == claimNumber.Key
                                                            select dr).ToList();

                                if (occupationCollection != null)
                                {
                                    foreach (var dataRowOccupation in occupationCollection)
                                    {
                                        if (!string.IsNullOrEmpty(dataRowOccupation[7].ToString()))
                                        {
                                            occupation = dataRowOccupation[7].ToString();
                                            occupation += " - ";
                                        }

                                        if (!string.IsNullOrEmpty(dataRowOccupation[8].ToString()))
                                        {
                                            occupation += dataRowOccupation[8].ToString();
                                        }
                                        else
                                        {
                                            occupation += "Occupation Unknown";
                                        }

                                        if (occupation != null)
                                            person.Occupation = occupation;
                                    }
                                }

                                var contactCollection = (from DataRow dr in dtt.AsEnumerable()
                                                         where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value 
                                                         && dr.Field<string>("ROW_TYPE") == "CONTACT" 
                                                         && dr[3].ToString() == dictItem.Key
                                                         && dr.Field<string>("RowCount") == claimNumber.Key
                                                         select dr).ToList();

                                if (contactCollection != null)
                                {
                                    foreach (var dataRowContact in contactCollection)
                                    {
                                        if (!string.IsNullOrEmpty(dataRowContact[6].ToString()))
                                        {
                                            switch (dataRowContact[6].ToString())
                                            {
                                                case "WRKEML":
                                                case "GENEML":
                                                case "HOMEML":
                                                    person.EmailAddresses.Add(new PipelineEmail() { EmailAddress = dataRowContact[7].ToString() });
                                                    //person.EmailAddress = dataRowContact[7].ToString();
                                                    break;
                                            }

                                            if (dataRowContact[6].ToString() == "HOMTEL" || dataRowContact[6].ToString() == "CONTEL" || dataRowContact[6].ToString() == "GENTEL")
                                            {
                                                person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                                //person.LandlineTelephone = dataRowContact[7].ToString();
                                            }

                                            if (dataRowContact[6].ToString() == "WRKTEL" ||
                                                dataRowContact[6].ToString() == "CONTEL")
                                            {
                                                person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                                //person.WorkTelephone = dataRowContact[7].ToString();
                                            }

                                            if (dataRowContact[6].ToString() == "MOBILE")
                                            {
                                                person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                                //person.MobileTelephone = dataRowContact[7].ToString();
                                            }

                                            if (dataRowContact[6].ToString() == "OTHTEL" ||
                                                dataRowContact[6].ToString() == "CORRESTEL")
                                            {
                                                person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                                                //person.OtherTelephone = dataRowContact[7].ToString();
                                            }
                                        }
                                    }
                                }

                                #region Address

                                var addressCollection = (from DataRow dr in dtt.AsEnumerable()
                                                         where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value 
                                                         && dr.Field<string>("ROW_TYPE") == "ADDRESS" 
                                                         && dr[3].ToString() == dictItem.Key
                                                         && dr.Field<string>("RowCount") == claimNumber.Key
                                                         select dr).ToList();


                                if (addressCollection != null)
                                {

                                    foreach (var dataRowAddress in addressCollection)
                                    {
                                        PipelineAddress address = new PipelineAddress();

                                        address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                                        if (!string.IsNullOrEmpty(dataRowAddress[17].ToString()))
                                        {
                                            address.PO2A_LinkData.StartOfResidency = _ConvertDate(dataRowAddress[17].ToString());
                                        }

                                        if (!string.IsNullOrEmpty(dataRowAddress[18].ToString()))
                                        {
                                            address.PO2A_LinkData.EndOfResidency = _ConvertDate(dataRowAddress[18].ToString());
                                        }

                                        if (!string.IsNullOrEmpty(dataRowAddress[7].ToString()))
                                        {
                                            address.SubBuilding = dataRowAddress[7].ToString();
                                        }

                                        if (!string.IsNullOrEmpty(dataRowAddress[9].ToString()))
                                        {
                                            address.BuildingNumber = dataRowAddress[9].ToString();
                                        }

                                        if (!string.IsNullOrEmpty(dataRowAddress[10].ToString()))
                                        {
                                            address.Street = dataRowAddress[10].ToString();
                                        }

                                        if (!string.IsNullOrEmpty(dataRowAddress[11].ToString()))
                                        {
                                            address.Locality = dataRowAddress[11].ToString();
                                        }

                                        if (!string.IsNullOrEmpty(dataRowAddress[12].ToString()))
                                        {
                                            address.Town = dataRowAddress[12].ToString();
                                        }

                                        if (!string.IsNullOrEmpty(dataRowAddress[13].ToString()))
                                        {
                                            address.County = dataRowAddress[13].ToString();
                                        }

                                        if (!string.IsNullOrEmpty(dataRowAddress[14].ToString()))
                                        {
                                            address.PostCode = dataRowAddress[14].ToString();
                                        }

                                        //address = AddressFormatHelper.PopulateAddress(address, dataRowAddress[7].ToString(), dataRowAddress[8].ToString(), dataRowAddress[9].ToString(), dataRowAddress[10].ToString(), dataRowAddress[11].ToString(), dataRowAddress[12].ToString(), dataRowAddress[13].ToString(), dataRowAddress[14].ToString());

                                        person.Addresses.Add(address);

                                    }
                                }

                                #endregion

                                #region Organistion

                                Dictionary<string, string> DictOrgs = new Dictionary<string, string>();

                                var partyRelationshipCollection = (from DataRow dr in dtt.AsEnumerable()
                                                                   where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                                                   && dr.Field<string>("ROW_TYPE") == "PARTY_RELATIONSHIP"
                                                                   && dr[8].ToString() == dictItem.Key
                                                                   && dr.Field<string>("RowCount") == claimNumber.Key
                                                                   select dr).ToList();


                                if (partyRelationshipCollection != null)
                                {
                                    foreach (var row in partyRelationshipCollection)
                                    {
                                        DictOrgs.Add(row[3].ToString(), row[8].ToString());
                                    }
                                }


                                if (DictOrgs != null)
                                {
                                    foreach (var item in DictOrgs)
                                    {

                                        var organisationCollection = (from DataRow dr in dtt.AsEnumerable()
                                                                      where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                                                      && dr.Field<string>("ROW_TYPE") == "PARTY_MOTOR"
                                                                      && dr[26].ToString() != ""
                                                                      && !dr[4].ToString().Contains("TPC")
                                                                      && !dr[4].ToString().Contains("WIT")
                                                                      && !dr[4].ToString().Contains("THPRPR")
                                                                      && dr.Field<string>("RowCount") == claimNumber.Key
                                                                      && dr[3].ToString() == item.Key
                                                                      select dr).ToList();


                                        if (organisationCollection != null)
                                        {


                                            foreach (var dataRowOrganisation in organisationCollection)
                                            {
                                                PipelineOrganisation organisation = new PipelineOrganisation();

                                                switch (dataRowOrganisation[4].ToString())
                                                {
                                                    case "TPI":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Insurer;
                                                        organisation.I2O_LinkData.PartyType_Id = (int)OrganisationType.Insurer;
                                                        break;
                                                    case "TPA":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.AccidentManagement;
                                                        organisation.I2O_LinkData.PartyType_Id = (int)OrganisationType.AccidentManagement;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.AccidentManagement;
                                                        break;
                                                    case "TPS":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                                        organisation.I2O_LinkData.PartyType_Id = (int)OrganisationType.Solicitor;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                                        break;
                                                    case "SUP":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Repairer;
                                                        organisation.I2O_LinkData.PartyType_Id = (int)OrganisationType.Repairer;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Repairer;
                                                        break;
                                                    case "CHO":
                                                    case "HRP":
                                                        organisation.OrganisationType_Id = (int)OrganisationType.CreditHire;
                                                        organisation.I2O_LinkData.PartyType_Id = (int)OrganisationType.CreditHire;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire;
                                                        break;
                                                    default:
                                                        organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                                        organisation.I2O_LinkData.PartyType_Id = (int)OrganisationType.Unknown;
                                                        organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                                        break;
                                                }                                        

                                                if (!string.IsNullOrEmpty(dataRowOrganisation[26].ToString()))
                                                {
                                                    organisation.OrganisationName = dataRowOrganisation[26].ToString();
                                                }
                                                if (!string.IsNullOrEmpty(dataRowOrganisation[28].ToString()))
                                                {
                                                    organisation.RegisteredNumber = dataRowOrganisation[28].ToString();
                                                }
                                                if (!string.IsNullOrEmpty(dataRowOrganisation[33].ToString()))
                                                {
                                                    organisation.VatNumber = dataRowOrganisation[33].ToString();
                                                }


                                                var organisationContactCollection = (from DataRow dr in dtt.AsEnumerable()
                                                                                     where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                                                                     && dr.Field<string>("ROW_TYPE") == "CONTACT"
                                                                                     && dr[3].ToString() == dataRowOrganisation[3].ToString()
                                                                                     && dr.Field<string>("RowCount") == claimNumber.Key
                                                                                     select dr).ToList();

                                                if (organisationContactCollection != null)
                                                {

                                                    foreach (
                                                        var dataRowOrganisationContactCollection in
                                                            organisationContactCollection)
                                                    {
                                                        if (
                                                            !string.IsNullOrEmpty(
                                                                dataRowOrganisationContactCollection[6].ToString()))
                                                        {
                                                            switch (dataRowOrganisationContactCollection[6].ToString())
                                                            {
                                                                case "WRKEML":
                                                                case "GENEML":
                                                                case "HOMEML":
                                                                    organisation.EmailAddresses.Add(new PipelineEmail() { EmailAddress = dataRowOrganisationContactCollection[7].ToString() });
                                                                    //organisation.Email =dataRowOrganisationContactCollection[7].ToString();
                                                                    break;
                                                            }

                                                            if (dataRowOrganisationContactCollection[6].ToString() == "CONTEL")
                                                            {
                                                                organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                                                //organisation.Telephone1 =dataRowOrganisationContactCollection[7].ToString();
                                                            }

                                                            if (dataRowOrganisationContactCollection[6].ToString() == "BUSTEL")
                                                            {
                                                                organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                                                //organisation.Telephone2 =dataRowOrganisationContactCollection[7].ToString();
                                                            }

                                                            if (dataRowOrganisationContactCollection[6].ToString() == "MOBILE")
                                                            {
                                                                organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                                                //organisation.Telephone3 =dataRowOrganisationContactCollection[7].ToString();
                                                            }

                                                            if (dataRowOrganisationContactCollection[6].ToString() == "GENTEL")
                                                            {
                                                                organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                                                                //organisation.Telephone3 =dataRowOrganisationContactCollection[7].ToString();
                                                            }
                                                        }
                                                    }
                                                }

                                                #region Address

                                                var orgAddressCollection = (from DataRow dr in dtt.AsEnumerable()
                                                                            where dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value
                                                                            && dr.Field<string>("ROW_TYPE") == "ADDRESS"
                                                                            && dr[3].ToString() == dataRowOrganisation[3].ToString()
                                                                            && dr.Field<string>("RowCount") == claimNumber.Key
                                                                            select dr).ToList();


                                                if (orgAddressCollection != null)
                                                {

                                                    foreach (var dataRowAddress in orgAddressCollection)
                                                    {
                                                        PipelineAddress address = new PipelineAddress();

                                                        address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                                                        if (!string.IsNullOrEmpty(dataRowAddress[17].ToString()))
                                                        {
                                                            address.PO2A_LinkData.StartOfResidency = _ConvertDate(dataRowAddress[17].ToString());
                                                        }

                                                        if (!string.IsNullOrEmpty(dataRowAddress[18].ToString()))
                                                        {
                                                            address.PO2A_LinkData.EndOfResidency = _ConvertDate(dataRowAddress[18].ToString());
                                                        }

                                                        if (!string.IsNullOrEmpty(dataRowAddress[7].ToString()))
                                                        {
                                                            address.SubBuilding = dataRowAddress[7].ToString();
                                                        }

                                                        if (!string.IsNullOrEmpty(dataRowAddress[9].ToString()))
                                                        {
                                                            address.BuildingNumber = dataRowAddress[9].ToString();
                                                        }

                                                        if (!string.IsNullOrEmpty(dataRowAddress[10].ToString()))
                                                        {
                                                            address.Street = dataRowAddress[10].ToString();
                                                        }

                                                        if (!string.IsNullOrEmpty(dataRowAddress[11].ToString()))
                                                        {
                                                            address.Locality = dataRowAddress[11].ToString();
                                                        }

                                                        if (!string.IsNullOrEmpty(dataRowAddress[12].ToString()))
                                                        {
                                                            address.Town = dataRowAddress[12].ToString();
                                                        }

                                                        if (!string.IsNullOrEmpty(dataRowAddress[13].ToString()))
                                                        {
                                                            address.County = dataRowAddress[13].ToString();
                                                        }

                                                        if (!string.IsNullOrEmpty(dataRowAddress[14].ToString()))
                                                        {
                                                            address.PostCode = dataRowAddress[14].ToString();
                                                        }

                                                        //address = AddressFormatHelper.PopulateAddress(address, dataRowAddress[7].ToString(), dataRowAddress[8].ToString(), dataRowAddress[9].ToString(), dataRowAddress[10].ToString(), dataRowAddress[11].ToString(), dataRowAddress[12].ToString(), dataRowAddress[13].ToString(), dataRowAddress[14].ToString());

                                                        organisation.Addresses.Add(address);

                                                    }
                                                }

                                                #endregion

                                                //??organisation.ClaimInfo = null;
                                                person.Organisations.Add(organisation);
                                            }

                                        }

                                    }
                                }

                                #endregion

                                people.Add(person);
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetVehiclePeopleDetails(ENQUIRY_EXTERNAL_UID " + claimNumber + ": " + ex);
            }

            return people;
        }

        private PipelineClaimInfo GetPersonClaimInfo(DataTable dtt, KeyValuePair<string, string> claimNumber, DataRow dataRow)
        {
            try
            {
                //bool? policeAttended = null;
                //string policeNumber = null;
                //string policeCrimeNumber = null;

                //DateTime incidentDate;
                //DateTime claimNotificationDate;
                string incidentCircumstances = null;


                //if (policeNumber != null || policeCrimeNumber != null)
                //{
                //    policeAttended = true;
                //}

                PipelineClaimInfo claimInfo = new PipelineClaimInfo();

                var claimInfoCollection = (from DataRow dr in dtt.AsEnumerable()
                                           where
                                               dr.Field<string>("ENQUIRY_EXTERNAL_UID") == claimNumber.Value &&
                                               dr.Field<string>("ROW_TYPE") == "CLAIM_MOTOR"
                                               && dr.Field<string>("RowCount") == claimNumber.Key
                                           select dr).ToList();

                if (claimInfoCollection != null)
                {
                    foreach (var claimInfoRow in claimInfoCollection)
                    {
                        if (!string.IsNullOrEmpty(claimInfoRow[15].ToString()))
                        {
                            switch (claimInfoRow[15].ToString().ToUpper())
                            {
                                case "OPEN":
                                    claimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                    break;
                                case "CLOSED":
                                    claimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                    break;
                                case "REOPENED":
                                    claimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                                    break;
                                default:
                                    claimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                                    break;
                            }
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[21].ToString()))
                        {
                            claimInfo.ClaimCode = claimInfoRow[21].ToString();
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[13].ToString()))
                        {
                            //string incidentTempTimeDate = DateTime.Now.ToShortDateString();

                            //incidentDate = DateTime.Now; // Convert.ToDateTime(incidentTempTimeDate);

                            //TimeSpan incidentTime;

                            //if (TimeSpan.TryParse(claimInfoRow[13].ToString(), out incidentTime))
                            //    claimInfo.IncidentTime = incidentTime; // _incidentDate.Add(incidentTime);
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[32].ToString()))
                        {
                            claimInfo.IncidentLocation = claimInfoRow[32].ToString();
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[21].ToString()))
                        {
                            incidentCircumstances = claimInfoRow[21].ToString();
                            incidentCircumstances += " - ";
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[22].ToString()))
                        {
                            incidentCircumstances += claimInfoRow[22].ToString();
                            claimInfo.IncidentCircumstances = incidentCircumstances;
                        }
                        if (!string.IsNullOrEmpty(claimInfoRow[10].ToString()))
                        {
                            claimInfo.ClaimNotificationDate = _ConvertDate(claimInfoRow[10].ToString());
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[24].ToString()))
                        {
                            claimInfo.PaymentsToDate =
                                Convert.ToDecimal(claimInfoRow[24].ToString());
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[23].ToString()))
                        {
                            claimInfo.Reserve = Convert.ToDecimal(claimInfoRow[23].ToString());
                        }
                    }
                    return claimInfo;
                }

                else
                {
                    return null;
                }



            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetPersonClaimInfo(ENQUIRY_EXTERNAL_UID " + claimNumber + ": " + ex);
            }
        }
    }
}