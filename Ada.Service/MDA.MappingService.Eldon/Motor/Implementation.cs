﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.MappingService.Eldon.Model;
using MDA.MappingService.Interface;
using MDA.Common;
using MDA.Common.Server;

namespace MDA.MappingService.Eldon.Motor
{
    public class EldonFileMapping : IMappingService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx">Current Context containing DB connection to use</param>
        /// <param name="fs">FileStream containing the file to be loaded</param>
        /// <param name="statusTracking"></param>
        /// <param name="processClaim">CallBack function to call for each PipelineClaim created</param>
        /// <param name="ShowProgress">Show Progress callback</param>
        /// <param name="processingResults">Structure to hold ALL processing results</param>
        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking,
                 Func<CurrentContext, IPipelineClaim, object, int> processClaim,
                 out ProcessingResults processingResults)
        {

            processingResults = new ProcessingResults("Mapping");

            PipelineClaimBatch ret = new PipelineClaimBatch();

            try
            {
                //string strFile = fileStream.Name;

                getMotorClaimBatch(ctx, fs, statusTracking, processClaim, processingResults);
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping Eldon file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void getMotorClaimBatch(CurrentContext ctx, Stream filem, object statusTracking,
                                Func<CurrentContext, MDA.Pipeline.Model.PipelineMotorClaim, object, int> ProcessClaimFn,
                                ProcessingResults processingResults)
        {


            PipelineClaimBatch claimBatch = new PipelineClaimBatch();

            DataTable dtt = new DataTable();

            TranslateFromFile translateFromFile = new TranslateFromFile();

            // Put all claims in a data table
            dtt = translateFromFile.ClaimsToDataTable(filem);

            // Get Distinct Claim Rows
            Dictionary<string, string> claimBatchRows = translateFromFile.GetDistinctClaims(dtt);

            // Loop through claims
            foreach (var claimNumber in claimBatchRows)
            {

                PipelineMotorClaim claim = translateFromFile.GetClaim(dtt, claimNumber);
                claim.Policy = translateFromFile.GetPolicyDetails(dtt, claimNumber);
                claim.Vehicles = translateFromFile.GetVehicleDetails(dtt, claimNumber);

                #region Vehicle Level

                foreach (var v in claim.Vehicles)
                {
                    v.People = translateFromFile.GetVehiclePeopleDetails(dtt, claimNumber, v.VehicleRegistration);
                }
                #endregion

                #region NoneVehiclePeople

                List<PipelinePerson> noneVehiclePeople = translateFromFile.GetNoneVehiclePeopleDetails(dtt, claimNumber);
                if (noneVehiclePeople.Any())
                {
                    foreach (var person in noneVehiclePeople)
                    {
                        PipelineVehicle noneVehicle = new PipelineVehicle();
                        noneVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;
                        noneVehicle.People.Add(person);
                        claim.Vehicles.Add(noneVehicle);
                    }
                }
                #endregion

                #region NoneVehicleThirdPartyPeople

                List<PipelinePerson> noneVehicleThirdPartyPeople = translateFromFile.GetTPPeopleDetailsWithNoVehicle(dtt, claimNumber);
                if (noneVehicleThirdPartyPeople.Any())
                {
                    foreach (var person in noneVehicleThirdPartyPeople)
                    {
                        PipelineVehicle noneVehicle = new PipelineVehicle();
                        noneVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;
                        noneVehicle.People.Add(person);
                        claim.Vehicles.Add(noneVehicle);
                    }
                }
                #endregion
                
                #region NoneVehicleOrganisations
                
                List<PipelineOrganisation> noneVehicleOrganisations = translateFromFile.GetNoneVehicleOrganisationDetails(dtt, claimNumber);
                if (noneVehicleOrganisations.Any())
                {
                    foreach (var org in noneVehicleOrganisations)
                    {
                        org.I2O_LinkData.PartyType_Id = (int)PartyType.Witness;
                        claim.Organisations.Add(org);
                    }
                }
                #endregion

                #region Have we seen this claim number before?
                List<PipelineMotorClaim> modifiedClaims = new List<PipelineMotorClaim>();

                foreach (var claimItem in claimBatch.Claims)
                {
                    if (claimItem.ClaimNumber == claimNumber.Value)
                    {
                        modifiedClaims.Add((PipelineMotorClaim)claimItem);
                    }
                }

                if (modifiedClaims.Any())
                {
                    foreach (var modifiedClaim in modifiedClaims)
                    {
                        claimBatch.Claims.Remove(modifiedClaim);
                    }
                }
                #endregion

                //claimBatch.Claims.Add(claim);

                if (ProcessClaimFn(ctx, claim, statusTracking) == -1) return;
            }

            foreach (var claim in claimBatchRows)
            {

                
            }

            //return claimBatch;
        }


        public void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard, int settleTime, int batchSize, string postOp, string archiveFolder, Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {
            throw new NotImplementedException();
        }
    }
}
