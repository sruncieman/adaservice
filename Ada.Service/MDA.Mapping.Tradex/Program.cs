﻿using MDA.Common;
using MDA.Common.Server;
using MDA.MappingService.Tradex.Motor;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Data;
using System.Data.OleDb;

namespace MDA.Mapping.Tradex
{
    class Program
    {

        static List<IPipelineClaim> motorClaimBatch = new List<IPipelineClaim>();

        private static int ProcessClaimIntoDb(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim, object o)
        {
            motorClaimBatch.Add(claim);

            return 0;
        }

        static void Main(string[] args)
        {

            Stream mockStream = null;

            CurrentContext ctx = new CurrentContext(0, 1, "Test");

            Console.WriteLine("Start");

            ProcessingResults pr;

            new FileMapping().ConvertToClaimBatch(ctx, mockStream, null, ProcessClaimIntoDb, out pr);

            string xml = WriteToXml();

            Console.ReadLine();
        }

        private static string WriteToXml()
        {
            string strXmlFileFullSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Tradex\XML\";

            string xml = null;

            foreach (var item in motorClaimBatch)
            {

                Console.WriteLine(item.ClaimNumber);

                var ms = new MemoryStream();

                var ser = new XmlSerializer(typeof(MDA.Pipeline.Model.PipelineMotorClaim));

                ser.Serialize(ms, item);

                ms.Position = 0;

                var sr = new StreamReader(ms);

                xml = sr.ReadToEnd();

                TextWriter WriteFileStream = new StreamWriter(strXmlFileFullSave + item.ClaimNumber + ".xml");
                ser.Serialize(WriteFileStream, item);

                WriteFileStream.Close();

            }

            Console.WriteLine("Finished");

            return xml;
        }

        static void ConvertExcelToCsv(string excelFilePath, string csvOutputFile, int worksheetNumber = 1)
        {
            if (!File.Exists(excelFilePath)) throw new FileNotFoundException(excelFilePath);
            if (File.Exists(csvOutputFile)) throw new ArgumentException("File exists: " + csvOutputFile);

            // connection string
            var cnnStr = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;IMEX=1;HDR=NO\"", excelFilePath);
            var cnn = new OleDbConnection(cnnStr);

            // get schema, then data
            var dt = new DataTable();
            try
            {
                cnn.Open();
                var schemaTable = cnn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (schemaTable.Rows.Count < worksheetNumber) throw new ArgumentException("The worksheet number provided cannot be found in the spreadsheet");
                string worksheet = schemaTable.Rows[worksheetNumber - 1]["table_name"].ToString().Replace("'", "");
                string sql = String.Format("select * from [{0}]", worksheet);
                var da = new OleDbDataAdapter(sql, cnn);
                da.Fill(dt);
            }
            catch (Exception e)
            {
                // ???
                throw e;
            }
            finally
            {
                // free resources
                cnn.Close();
            }

            // write out CSV data
            using (var wtr = new StreamWriter(csvOutputFile))
            {
                foreach (DataRow row in dt.Rows)
                {
                    bool firstLine = true;
                    foreach (DataColumn col in dt.Columns)
                    {
                        if (!firstLine) { wtr.Write(","); } else { firstLine = false; }
                        var data = row[col.ColumnName].ToString().Replace("\"", "\"\"");
                        wtr.Write(String.Format("\"{0}\"", data));
                    }
                    wtr.WriteLine();
                }
            }
        }
    }
}
