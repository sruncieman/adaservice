﻿using System.Configuration;

namespace MDA.Common.Config
{
    public class UserAccountConfig
    {
        private static UserAccountSection _config;

        public static UserAccountSection GetConfig
        {
            get
            {
                if (_config == null)
                    _config = ConfigurationManager.GetSection("userAccountSection") as MDA.Common.Config.UserAccountSection;

                return _config;
            }
        }
    }
}

