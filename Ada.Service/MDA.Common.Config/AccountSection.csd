﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="25969ac5-0296-448e-a961-db002fc13d63" namespace="MDA.Common.Config" xmlSchemaNamespace="urn:MDA.Common.Config" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
  </typeDefinitions>
  <configurationElements>
    <configurationSection name="UserAccountSection" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="userAccountSection">
      <elementProperties>
        <elementProperty name="UserName" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="userName" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/25969ac5-0296-448e-a961-db002fc13d63/SimpleString" />
          </type>
        </elementProperty>
        <elementProperty name="UserDomain" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="userDomain" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/25969ac5-0296-448e-a961-db002fc13d63/SimpleString" />
          </type>
        </elementProperty>
        <elementProperty name="UserPassword" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="userPassword" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/25969ac5-0296-448e-a961-db002fc13d63/SimpleString" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElement name="SimpleString">
      <attributeProperties>
        <attributeProperty name="Value" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="value" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/25969ac5-0296-448e-a961-db002fc13d63/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>