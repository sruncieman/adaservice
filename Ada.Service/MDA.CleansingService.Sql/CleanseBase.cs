﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.CleansingService.Interface;
using MDA.CleansingService.Model;
using MDA.Common;
using MDA.Common.Server;
using System.Text.RegularExpressions;
using MDA.DAL;
using MDA.RiskService;

namespace MDA.CleansingService
{
    public partial class CleanseBase : ICleansingService
    {
        protected List<RiskWordFilter> _riskWords;
        protected bool _trace = false;
        protected CurrentContext _ctx;
        //protected MdaDbContext _db;

        public CleanseBase(CurrentContext ctx)
        {
            _ctx = ctx;
            _trace = ctx.TraceLoad;

            using (var ctxx = new CurrentContext(ctx))
            {
                _riskWords = new MDA.RiskService.RiskServices(ctxx).GetRiskWordsForClient(ctxx.RiskClientId);
            }
        }

        public CurrentContext CurrentContext
        {
            set
            {
                _ctx = value;
                _trace = _ctx.TraceLoad;
            }
        }

        #region Claim (Pipeline)
        public virtual ProcessingResults CleanseTheClaim(MDA.Pipeline.Model.IPipelineClaim claim)
        {
            return new ProcessingResults("No cleanse defined");
        }

        protected virtual void CleanseClaim(MDA.Pipeline.Model.IPipelineClaim claim, MessageNode errorNode)
        {
            claim.ClaimNumber = CheckString(_riskWords, "Claim", "AccountNumber", claim.ClaimNumber, 50, true, errorNode);

            // leave date as dateTime, me strip time off as it goes into database
            var date = (DateTime)CheckDate(_riskWords, "Claim", "IncidentDate", claim.IncidentDate, "SmallDateTime", true, errorNode);

            if (date != null)
                claim.IncidentDate = date;
            else
            {
                errorNode.AddError("Date out of Range : " + claim.IncidentDate.ToString());
            }

            //claim.PaymentsToDate = CheckString("Claim", "PaymentsToDate", claim.PaymentsToDate, 10,true, errorNode);
            //claim.Reserve = CheckString("Claim", "Reserve", claim.Reserve, 10, true, errorNode);
        }
        #endregion

        #region Policy (Pipeline and DAL)
        protected virtual void CleansePolicy(MDA.Pipeline.Model.PipelinePolicy policy, MessageNode errorNode)
        {
            policy.Broker             = CheckString(_riskWords, "Policy", "Broker", policy.Broker, 50, false, errorNode);
            policy.PolicyEndDate      = CheckDate(_riskWords, "Policy", "PolicyEndDate", policy.PolicyEndDate, "SmallDateTime", true, errorNode);
            policy.PolicyNumber       = CheckString(_riskWords, "Policy", "PolicyNumber", policy.PolicyNumber, 50, false, errorNode);
            policy.PolicyStartDate    = CheckDate(_riskWords, "Policy", "PolicyStartDate", policy.PolicyStartDate, "SmallDateTime", true, errorNode);
            policy.InsurerTradingName = CheckString(_riskWords, "Policy", "InsurerTradingName", policy.InsurerTradingName, 50, true, errorNode);
            policy.Insurer            = CheckString(_riskWords, "Policy", "Insurer", policy.Insurer, 50, false, errorNode);
        }

        public virtual int CleansePolicy(MDA.DAL.Policy policy, MessageNode errorNode)
        {
            policy.Broker             = CheckString(_riskWords, "Policy", "Broker", policy.Broker, 50, false, errorNode);
            policy.PolicyEndDate      = CheckDate(_riskWords, "Policy", "PolicyEndDate", policy.PolicyEndDate, "SmallDateTime", true, errorNode);
            policy.PolicyNumber       = CheckString(_riskWords, "Policy", "PolicyNumber", policy.PolicyNumber, 50, false, errorNode);
            policy.PolicyStartDate    = CheckDate(_riskWords, "Policy", "PolicyStartDate", policy.PolicyStartDate, "SmallDateTime", true, errorNode);
            policy.InsurerTradingName = CheckString(_riskWords, "Policy", "InsurerTradingName", policy.InsurerTradingName, 50, true, errorNode);

            return 0;

        }
        #endregion

        #region ClaimInfo (Pipeline)
        protected virtual void CleanseClaimInfo(MDA.Pipeline.Model.IPipelineClaim claim, MessageNode errorNode)
        {
            claim.ExtraClaimInfo.ClaimCode              = CheckString(_riskWords, "ClaimInfo", "ClaimCode", claim.ExtraClaimInfo.ClaimCode, 50, true, errorNode);
            claim.ExtraClaimInfo.IncidentLocation       = CheckString(_riskWords, "ClaimInfo", "IncidentLocation", claim.ExtraClaimInfo.IncidentLocation, 50, true, errorNode);
            claim.ExtraClaimInfo.IncidentCircumstances  = CheckString(_riskWords, "ClaimInfo", "IncidentCircs", claim.ExtraClaimInfo.IncidentCircumstances, 50, true, errorNode);
            claim.ExtraClaimInfo.PoliceForce            = CheckString(_riskWords, "ClaimInfo", "PoliceForce", claim.ExtraClaimInfo.PoliceForce, 50, true, errorNode);
            claim.ExtraClaimInfo.PoliceReference        = CheckString(_riskWords, "ClaimInfo", "PoliceReference", claim.ExtraClaimInfo.PoliceReference, 50, true, errorNode);

        }
        #endregion

        #region ClaimInfo (Dedupe)
        public virtual int CleanseClaimInfo(MDA.DAL.Incident2Person i2P, MessageNode errorNode)
        {
            i2P.ClaimCode = CheckString(_riskWords, "ClaimInfo", "ClaimCode", i2P.ClaimCode, 50, true, errorNode);
            i2P.IncidentLocation = CheckString(_riskWords, "ClaimInfo", "IncidentLocation", i2P.IncidentLocation, 50, true, errorNode);
            i2P.IncidentCircs = CheckString(_riskWords, "i2PInfo", "IncidentCircs", i2P.IncidentCircs, 50, true, errorNode);
            i2P.PoliceForce = CheckString(_riskWords, "i2PInfo", "PoliceForce", i2P.PoliceForce, 50, true, errorNode);
            i2P.PoliceReference = CheckString(_riskWords, "i2PInfo", "PoliceReference", i2P.PoliceReference, 50, true, errorNode);
           
            return 0;
        }
        #endregion

        #region Incident2Person (Pipeline)
        protected virtual void CleanseIncident2Person(MDA.Pipeline.Model.PipelineIncident2PersonLink incident2Person, MessageNode errorNode)
        {
            incident2Person.Broker = CheckString(_riskWords, "Policy", "Broker", incident2Person.Broker, 255, true, errorNode);
            incident2Person.ClaimInfo.ClaimCode = CheckString(_riskWords, "ClaimInfo", "ClaimCode", incident2Person.ClaimInfo.ClaimCode, 80, true, errorNode);
            incident2Person.ClaimInfo.IncidentCircumstances = CheckString(_riskWords, "ClaimInfo", "IncidentCircs", incident2Person.ClaimInfo.IncidentCircumstances, 1024, true, errorNode);
            incident2Person.ClaimInfo.IncidentLocation = CheckString(_riskWords, "ClaimInfo", "IncidentLocation", incident2Person.ClaimInfo.IncidentLocation, 255, true, errorNode);
            incident2Person.ClaimInfo.PoliceForce = CheckString(_riskWords, "ClaimInfo", "PoliceForce", incident2Person.ClaimInfo.PoliceForce, 50, true, errorNode);
            incident2Person.ClaimInfo.PoliceReference = CheckString(_riskWords, "ClaimInfo", "PoliceReference", incident2Person.ClaimInfo.PoliceReference, 50, true, errorNode);
        }
        #endregion

        #region Incident2Organisation (Pipeline)
        protected virtual void CleanseIncident2Organisation(MDA.Pipeline.Model.PipelineIncident2OrganisationLink incident2Organisation, MessageNode errorNode)
        {
            incident2Organisation.Broker = CheckString(_riskWords, "Policy", "Broker", incident2Organisation.Broker, 255, true, errorNode);
            incident2Organisation.ClaimInfo.ClaimCode = CheckString(_riskWords, "ClaimInfo", "ClaimCode", incident2Organisation.ClaimInfo.ClaimCode, 80, true, errorNode);
            incident2Organisation.ClaimInfo.IncidentCircumstances = CheckString(_riskWords, "ClaimInfo", "IncidentCircs", incident2Organisation.ClaimInfo.IncidentCircumstances, 1024, true, errorNode);
            incident2Organisation.ClaimInfo.IncidentLocation = CheckString(_riskWords, "ClaimInfo", "IncidentLocation", incident2Organisation.ClaimInfo.IncidentLocation, 255, true, errorNode);
            incident2Organisation.ClaimInfo.PoliceForce = CheckString(_riskWords, "ClaimInfo", "PoliceForce", incident2Organisation.ClaimInfo.PoliceForce, 50, true, errorNode);
            incident2Organisation.ClaimInfo.PoliceReference = CheckString(_riskWords, "ClaimInfo", "PoliceReference", incident2Organisation.ClaimInfo.PoliceReference, 50, true, errorNode);
        }
        #endregion

        #region Address (Pipeline and DAL)
        protected virtual int CleanseAddress(MDA.Pipeline.Model.PipelineAddress address, MessageNode errorNode)
        {
            address.Building       = CheckString(_riskWords, "Address", "Building", address.Building, 50, true, errorNode);
            address.BuildingNumber = CheckString(_riskWords, "Address", "BuildingNumber", address.BuildingNumber, 50, true, errorNode);
            address.County         = CheckString(_riskWords, "Address", "County", address.County, 50, true, errorNode);
            address.Locality       = CheckString(_riskWords, "Address", "Locality", address.Locality, 50, true, errorNode);
            address.PostCode       = CheckString(_riskWords, "Address", "PostCode", address.PostCode, 20, true, errorNode);
            address.PostCode       = string.IsNullOrEmpty(address.PostCode) ? null : address.PostCode.Replace(" ", "");
            address.Street         = CheckString(_riskWords, "Address", "Street", address.Street, 255, true, errorNode);
            address.SubBuilding    = CheckString(_riskWords, "Address", "SubBuilding", address.SubBuilding, 50, true, errorNode);
            address.Town           = CheckString(_riskWords, "Address", "Town", address.Town, 50, true, errorNode);
            //address.PAFAddress   = address.PAFAddress;

            return 0;
        }

        public virtual int CleanseAddress(MDA.DAL.Address address, MessageNode errorNode)
        {
            address.Building       = CheckString(_riskWords, "Address", "Building", address.Building, 50, true, errorNode);
            address.BuildingNumber = CheckString(_riskWords, "Address", "BuildingNumber", address.BuildingNumber, 50, true, errorNode);
            address.County         = CheckString(_riskWords, "Address", "County", address.County, 50, true, errorNode);
            address.Locality       = CheckString(_riskWords, "Address", "Locality", address.Locality, 50, true, errorNode);
            address.PostCode       = CheckString(_riskWords, "Address", "PostCode", address.PostCode, 20, true, errorNode);
            address.PostCode       = string.IsNullOrEmpty(address.PostCode) ? null : address.PostCode.Replace(" ", "");
            address.Street         = CheckString(_riskWords, "Address", "Street", address.Street, 255, true, errorNode);
            address.SubBuilding    = CheckString(_riskWords, "Address", "SubBuilding", address.SubBuilding, 50, true, errorNode);
            address.Town           = CheckString(_riskWords, "Address", "Town", address.Town, 50, true, errorNode);

            return 0;
            //address.PAFAddress = address.PAFAddress;
        }
        #endregion

        #region Bank Account (Pipeline and DAL)
        protected virtual void CleanseBankAccount(MDA.Pipeline.Model.PipelineBankAccount bankAccount, MessageNode errorNode)
        {
            bankAccount.AccountNumber                          = CheckString(_riskWords, "BankAccount", "AccountNumber", bankAccount.AccountNumber, 50, false, errorNode);
            bankAccount.BankName                               = CheckString(_riskWords, "BankAccount", "BankName", bankAccount.BankName, 50, true, errorNode);
            bankAccount.Po2Ba_LinkData.DatePaymentDetailsTaken = CheckDate(_riskWords, "BankAccount", "DatePaymentDetailsTaken", bankAccount.Po2Ba_LinkData.DatePaymentDetailsTaken, "SmallDate", true, errorNode);
            bankAccount.SortCode                               = CheckString(_riskWords, "BankAccount", "SortCode", bankAccount.SortCode, 50, true, errorNode);
        }

        public virtual int CleanseBankAccount(MDA.DAL.BankAccount bankAccount, MessageNode errorNode)
        {
            bankAccount.AccountNumber                            = CheckString(_riskWords, "BankAccount", "AccountNumber", bankAccount.AccountNumber, 50, false, errorNode);
            bankAccount.BankName                                 = CheckString(_riskWords, "BankAccount", "BankName", bankAccount.BankName, 50, true, errorNode);
            //bankAccount.Po2Ba_LinkData.DatePaymentDetailsTaken = CheckDate("BankAccount", "DatePaymentDetailsTaken", bankAccount.Po2Ba_LinkData.DatePaymentDetailsTaken, "SmallDate", true, errorNode);
            bankAccount.SortCode                                 = CheckString(_riskWords, "BankAccount", "SortCode", bankAccount.SortCode, 50, true, errorNode);

            return 0;
        }
        #endregion

        #region Organisation (Pipeline and DAL)
        protected virtual void CleanseOrganisation(MDA.Pipeline.Model.PipelineOrganisation organisation, MessageNode errorNode)
        {
            organisation.OrganisationName = CheckString(_riskWords, "Organisation", "OrganisationName", organisation.OrganisationName, 50, true, errorNode);
            organisation.RegisteredNumber = CheckString(_riskWords, "Organisation", "RegisteredNumber", organisation.RegisteredNumber, 50, true, errorNode);
            organisation.VatNumber        = CheckString(_riskWords, "Organisation", "VatNumber", organisation.VatNumber, 50, true, errorNode);
            organisation.MojCrmNumber     = CheckString(_riskWords, "Organisation", "MojCrmNumber", organisation.MojCrmNumber, 50, true, errorNode);
            
            foreach (var em in organisation.EmailAddresses)
            {
                em.EmailAddress = CheckString(_riskWords, "Email", "EmailAddress", em.EmailAddress, 50, true, errorNode);
            }
            
            foreach (var ws in organisation.WebSites){
                ws.URL = CheckString(_riskWords, "Organisation", "WebSite", ws.URL, 50, true, errorNode);
            }
            
            foreach (var tp in organisation.Telephones)
            {
                tp.ClientSuppliedNumber = CheckString(_riskWords, "Organisation", "Telephone", tp.ClientSuppliedNumber, 50, true, errorNode);
            }
        }

        public virtual int CleanseOrganisation(MDA.DAL.Organisation organisation, MessageNode errorNode)
        {
            organisation.OrganisationName = CheckString(_riskWords, "Organisation", "OrganisationName", organisation.OrganisationName, 50, true, errorNode);
            organisation.RegisteredNumber = CheckString(_riskWords, "Organisation", "RegisteredNumber", organisation.RegisteredNumber, 50, true, errorNode);
            organisation.VATNumber        = CheckString(_riskWords, "Organisation", "VatNumber", organisation.VATNumber, 50, true, errorNode);
            organisation.MojCRMNumber     = CheckString(_riskWords, "Organisation", "MojCrmNumber", organisation.MojCRMNumber, 50, true, errorNode);

            foreach (var em in organisation.Organisation2Email)
            {
                em.Email.EmailAddress = CheckString(_riskWords, "Email", "EmailAddress", em.Email.EmailAddress, 50, true, errorNode);
            }

            foreach (var ws in organisation.Organisation2WebSite)
            {
                ws.WebSite.URL = CheckString(_riskWords, "Organisation", "WebSite", ws.WebSite.URL, 50, true, errorNode);
            }

            foreach (var tp in organisation.Organisation2Telephone)
            {
                tp.Telephone.TelephoneNumber = CheckString(_riskWords, "Organisation", "Telephone", tp.Telephone.TelephoneNumber, 50, true, errorNode);
            }

            return 0;
        }
        #endregion

        #region Payment Card (Pipeline and DAL)
        protected virtual void CleansePaymentCard(MDA.Pipeline.Model.PipelinePaymentCard paymentCard, MessageNode errorNode)
        {
            paymentCard.BankName                               = CheckString(_riskWords, "PaymentCard", "BankName", paymentCard.BankName, 50, true, errorNode);
            paymentCard.Po2Pc_LinkData.DatePaymentDetailsTaken = CheckDate(_riskWords, "PaymentCard", "DatePaymentDetailsTaken", paymentCard.Po2Pc_LinkData.DatePaymentDetailsTaken, "SmallDate", true, errorNode);
            paymentCard.SortCode                               = CheckString(_riskWords, "PaymentCard", "SortCode", paymentCard.SortCode, 50, true, errorNode);
            paymentCard.PaymentCardNumber                      = CheckString(_riskWords, "PaymentCard", "PaymentCardNumber", paymentCard.PaymentCardNumber, 50, false, errorNode);
        }

        public virtual int CleansePaymentCard(MDA.DAL.PaymentCard paymentCard, MessageNode errorNode)
        {
            paymentCard.BankName                                 = CheckString(_riskWords, "PaymentCard", "BankName", paymentCard.BankName, 50, true, errorNode);
            //paymentCard.Po2Pc_LinkData.DatePaymentDetailsTaken = CheckDate("PaymentCard", "DatePaymentDetailsTaken", paymentCard.Po2Pc_LinkData.DatePaymentDetailsTaken, "SmallDate", true, errorNode);
            paymentCard.SortCode                                 = CheckString(_riskWords, "PaymentCard", "SortCode", paymentCard.SortCode, 50, true, errorNode);
            paymentCard.PaymentCardNumber                        = CheckString(_riskWords, "PaymentCard", "PaymentCardNumber", paymentCard.PaymentCardNumber, 50, false, errorNode);

            return 0;
        }
        #endregion

        #region Person (Pipeline and DAL)
        protected virtual void CleansePerson(MDA.Pipeline.Model.PipelinePerson person, MessageNode errorNode)
        {

            person.FirstName = CheckString(_riskWords, "Person", "FirstName", person.FirstName, 100, true, errorNode);
            person.FirstName = RemoveDigits(person.FirstName);

            person.MiddleName = CheckString(_riskWords, "Person", "MiddleName", person.MiddleName, 100, true, errorNode);
            person.MiddleName = RemoveDigits(person.MiddleName);

            person.LastName = CheckString(_riskWords, "Person", "LastName", person.LastName, 100, true, errorNode);
            person.LastName = RemoveDigits(person.LastName);

            if (person.DateOfBirth != null) person.DateOfBirth = person.DateOfBirth.Value.Date;

            person.DateOfBirth = CheckDate(_riskWords, "Person", "DateOfBirth", person.DateOfBirth, "SmallDate", true, errorNode);
            person.Nationality = CheckString(_riskWords, "Person", "Nationality", person.Nationality, 50, true, errorNode);

            foreach (var ni in person.NINumbers)
            {
                if ( ni.NINumber1 != null )
                    ni.NINumber1 = CheckString(_riskWords, "NINumber", "NINumber", GetAlphaNumericOnly(ni.NINumber1.Trim()).ToUpper(), 30, true, errorNode);
            }

            foreach (var dl in person.DrivingLicenseNumbers)
            {
                if (dl.DriverNumber != null)
                    dl.DriverNumber = CheckString(_riskWords, "Person", "DriverNumber", GetAlphaNumericOnly(dl.DriverNumber.Trim()).ToUpper(), 50, true, errorNode);
            }

            foreach (var pn in person.PassportNumbers)
            {
                if (pn.PassportNumber != null)
                    pn.PassportNumber = CheckString(_riskWords, "Person", "PassportNumber", GetAlphaNumericOnly(pn.PassportNumber.Trim()).ToUpper(), 50, true, errorNode);
            }

            person.Occupation = CheckString(_riskWords, "Person", "Occupation", person.Occupation, 225, true, errorNode);

            foreach (var em in person.EmailAddresses)
            {
                if (em.EmailAddress != null)
                    em.EmailAddress = CheckString(_riskWords, "Email", "EmailAddress", em.EmailAddress, 225, true, errorNode);
            }


            foreach (var tp in person.Telephones)
            {
                tp.ClientSuppliedNumber = CheckString(_riskWords, "Person", "Telephone", tp.ClientSuppliedNumber, 50, true, errorNode);
            }
        }

        public virtual int CleansePerson(MDA.DAL.Person person, MessageNode errorNode)
        {

            person.FirstName = CheckString(_riskWords, "Person", "FirstName", person.FirstName, 100, true, errorNode);
            person.FirstName = RemoveDigits(person.FirstName);

            person.MiddleName = CheckString(_riskWords, "Person", "MiddleName", person.MiddleName, 100, true, errorNode);
            person.MiddleName = RemoveDigits(person.MiddleName);

            person.LastName = CheckString(_riskWords, "Person", "LastName", person.LastName, 100, true, errorNode);
            person.LastName = RemoveDigits(person.LastName);

            if (person.DateOfBirth != null) person.DateOfBirth = person.DateOfBirth.Value.Date;

            person.DateOfBirth = CheckDate(_riskWords, "Person", "DateOfBirth", person.DateOfBirth, "SmallDate", true, errorNode);
            person.Nationality = CheckString(_riskWords, "Person", "Nationality", person.Nationality, 50, true, errorNode);

            return 0;
        }


        protected virtual void CleanseTelephone(MDA.Pipeline.Model.PipelineTelephone telephone, MessageNode errorNode)
        {
            telephone.AreaCode             = CheckString(_riskWords, "Telephone", "AreaCode", telephone.AreaCode, 50, false, errorNode);
            telephone.ClientSuppliedNumber = CheckString(_riskWords, "Telephone", "ClientSuppliedNumber", telephone.ClientSuppliedNumber, 50, false, errorNode);
            telephone.FullNumber           = CheckString(_riskWords, "Telephone", "FullNumber", telephone.FullNumber, 50, false, errorNode);
            telephone.InternationalCode    = CheckString(_riskWords, "Telephone", "InternationalCode", telephone.InternationalCode, 50, false, errorNode);
        }

        public virtual int CleanseTelephone(MDA.DAL.Telephone telephone, MessageNode errorNode)
        {
            telephone.STDCode             = CheckString(_riskWords, "Telephone", "AreaCode", telephone.STDCode, 50, false, errorNode);
            telephone.TelephoneNumber     = CheckString(_riskWords, "Telephone", "ClientSuppliedNumber", telephone.TelephoneNumber, 50, false, errorNode);
            //telephone.FullNumber        = CheckString(_riskWords, "Telephone", "FullNumber", telephone.FullNumber, 50, false, errorNode);
            //telephone.InternationalCode = CheckString(_riskWords, "Telephone", "InternationalCode", telephone.InternationalCode, 50, false, errorNode);

            return 0;
        }
        #endregion

        #region Vehicle (Pipeline and DAL)
        protected virtual void CleanseVehicle(MDA.Pipeline.Model.PipelineVehicle vehicle, MessageNode errorNode)
        {
            vehicle.EngineCapacity                 = CheckString(_riskWords, "Vehicle", "EngineCapacity", vehicle.EngineCapacity, 50, false, errorNode);
            vehicle.VehicleMake                    = CheckString(_riskWords, "Vehicle", "Make", vehicle.VehicleMake, 50, false, errorNode);
            vehicle.VehicleModel                   = CheckString(_riskWords, "Vehicle", "Model", vehicle.VehicleModel, 50, false, errorNode);
            vehicle.VehicleRegistration            = CheckString(_riskWords, "Vehicle", "VehicleRegistration", vehicle.VehicleRegistration, 20, false, errorNode, true, true, new string[] {" "});
            vehicle.VIN                            = CheckString(_riskWords, "Vehicle", "VIN", vehicle.VIN, 50, false, errorNode);
            vehicle.I2V_LinkData.DamageDescription = CheckString(_riskWords, "Vehicle", "DamageDescription", vehicle.I2V_LinkData.DamageDescription, 250, false, errorNode);
        }

        public virtual int CleanseVehicle(MDA.DAL.Vehicle vehicle, MessageNode errorNode)
        {
            vehicle.EngineCapacity      = CheckString(_riskWords, "Vehicle", "EngineCapacity", vehicle.EngineCapacity, 50, false, errorNode);
            vehicle.VehicleMake         = CheckString(_riskWords, "Vehicle", "Make", vehicle.VehicleMake, 50, false, errorNode);
            vehicle.Model               = CheckString(_riskWords, "Vehicle", "Model", vehicle.Model, 50, false, errorNode);
            vehicle.VehicleRegistration = CheckString(_riskWords, "Vehicle", "VehicleRegistration", vehicle.VehicleRegistration, 20, false, errorNode, true, true, new string[] { " " });
            vehicle.VIN                 = CheckString(_riskWords, "Vehicle", "VIN", vehicle.VIN, 50, false, errorNode);

            return 0;
        }
        #endregion

        #region Incident2Vehicle (Pipeline)
        protected virtual void CleanseIncident2Vehicle(MDA.Pipeline.Model.PipelineIncident2VehicleLink incident2Vehicle, MessageNode errorNode)
        {
            incident2Vehicle.DamageDescription = CheckString(_riskWords, "Vehicle", "DamageDescription", incident2Vehicle.DamageDescription, 250, false, errorNode);
        }
        #endregion

        #region Vehicle2Organisation (Pipeline)
        protected virtual void CleanseVehicle2Organisation(MDA.Pipeline.Model.PipelineOrgVehicleLink vehicle2Organisation, MessageNode errorNode)
        {
            vehicle2Organisation.HireCompany = CheckString(_riskWords, "Vehicle", "HireCompany", vehicle2Organisation.HireCompany, 255, false, errorNode);
        }
        #endregion

        #region Org Vehicle (Pipeline)

        protected virtual void CleanseOrgVehicle(MDA.Pipeline.Model.PipelineOrganisationVehicle orgVehicle, MessageNode errorNode)
        {
            orgVehicle.EngineCapacity = CheckString(_riskWords, "Vehicle", "EngineCapacity", orgVehicle.EngineCapacity, 50, false, errorNode);
            orgVehicle.VehicleMake = CheckString(_riskWords, "Vehicle", "Make", orgVehicle.VehicleMake, 50, false, errorNode);
            orgVehicle.VehicleModel = CheckString(_riskWords, "Vehicle", "Model", orgVehicle.VehicleModel, 50, false, errorNode);
            orgVehicle.VehicleRegistration = CheckString(_riskWords, "Vehicle", "VehicleRegistration", orgVehicle.VehicleRegistration, 20, false, errorNode, true, true, new string[] { " " });
            orgVehicle.VIN = CheckString(_riskWords, "Vehicle", "VIN", orgVehicle.VIN, 50, false, errorNode);
        }

        #endregion

        #region Org Handset (Pipeline)

        protected virtual void CleanseOrgHandset(MDA.Pipeline.Model.PipelineOrganisationHandset orgHandset, MessageNode errorNode)
        {
            orgHandset.HandsetIMEI = CheckString(_riskWords, "Handset", "IMEI", orgHandset.HandsetIMEI, 255, false, errorNode);
            orgHandset.HandsetMake = CheckString(_riskWords, "Handset", "Make", orgHandset.HandsetMake, 50, false, errorNode);
            orgHandset.HandsetModel = CheckString(_riskWords, "Handset", "Model", orgHandset.HandsetModel, 50, false, errorNode);
        }

        #endregion


        // Incident2Handset DeviceFault Cleanse needed.

        #region Handset (Pipeline and DAL)
        protected virtual void CleanseHandset(MDA.Pipeline.Model.PipelineHandset handset, MessageNode errorNode)
        {
            handset.HandsetIMEI = CheckString(_riskWords, "Handset", "IMEI", handset.HandsetIMEI, 50, false, errorNode);
            handset.HandsetMake = CheckString(_riskWords, "Handset", "Make", handset.HandsetMake, 50, false, errorNode);
            handset.HandsetModel = CheckString(_riskWords, "Handset", "Model", handset.HandsetModel, 50, false, errorNode);
        }

        public virtual int CleanseHandset(MDA.DAL.Handset handset, MessageNode errorNode)
        {
            handset.HandsetIMEI = CheckString(_riskWords, "Handset", "IMEI", handset.HandsetIMEI, 50, false, errorNode);
            handset.HandsetMake = CheckString(_riskWords, "Handset", "Make", handset.HandsetMake, 50, false, errorNode);
            handset.HandsetModel = CheckString(_riskWords, "Handset", "Model", handset.HandsetModel, 50, false, errorNode);

            return 0;
        }
        #endregion


        #region Incident2Handset (Pipeline)
        protected virtual void CleanseIncident2Handset(MDA.Pipeline.Model.PipelineIncident2HandsetLink incident2Handset, MessageNode errorNode)
        {
            incident2Handset.DeviceFault = CheckString(_riskWords, "Handset", "DeviceFault", incident2Handset.DeviceFault, 250, false, errorNode);
            incident2Handset.HandsetValueCategory = CheckString(_riskWords, "Handset", "HandsetValueCategory", incident2Handset.HandsetValueCategory, 50, false, errorNode);
        }
        #endregion




      
    }
}
