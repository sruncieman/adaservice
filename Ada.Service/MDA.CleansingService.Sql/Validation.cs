﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common.Server;
using System.Net.Mail;
using System.Text.RegularExpressions;
using MDA.Common;
using MDA.DAL;

namespace MDA.CleansingService
{
    public partial class CleanseBase
    {
        protected static string CheckString(List<RiskWordFilter> riskWords, string entityName, string fieldName, string s, int maxLength, bool canBeNull, MessageNode errorNode, bool logTruncate = true, bool logReplace = true, string[] logExclusion = null)
        {
            if ( s == null )
            {
                if (canBeNull)
                    return null;
                else
                    return string.Empty;
            }

            s = s.Trim();

            // we know s is not null from now on

            if (s.Contains('"'))
            {
                s = s.Replace("\"", "");
                if (logReplace)
                    errorNode.AddWarning(string.Format("{0}.{1} (double quotes removed)", entityName, fieldName));
            }

            if (s.ToUpper() == "NULL")
            {
                if ( canBeNull )
                    return null;
                else
                    return string.Empty;
            }

            if (s.Length == 0 && canBeNull)
            {
                return null;
            }

            #region Remove XML reserved characters

            s = s.Replace("<", "");

            s = s.Replace(">", "");

            s = s.Replace("'", "");

            s = s.Replace("\"", "");

            #endregion

            if (riskWords != null)
            {
                var lookupWords = (from x in riskWords
                                   where x.TableName == entityName
                                   && x.FieldName == fieldName
                                   orderby x.Id ascending
                                   select x).ToList();

                foreach (var word in lookupWords)
                {
                    if (word.LookupWord == null)
                        continue;
                        
                    if (s != null)
                    {

                        bool match = false;
                        string search = word.LookupWord;
                        string replacement = word.ReplacementWord;
                        string st = s.ToLower();

                        search = search.ToLower();

                        #region Search Type

                        if (search == "")
                            word.SearchType = "=";

                        if (word.SearchType == "~")
                        {
                            match = st.Contains(search);
                        }

                        else if (word.SearchType == "=")
                        {
                            match = st.Equals(search);
                        }

                        else if (word.SearchType == "<")
                        {
                            match = st.StartsWith(search);
                        }

                        else if (word.SearchType == ">")
                        {
                            match = st.EndsWith(search);
                        }

                        else if (word.SearchType == "<>")
                        {
                            match = st.StartsWith(search) || st.EndsWith(search);
                        }
                        #endregion

                        if (match == true)
                        {
                            if (replacement == null)
                            {
                                replacement = string.Empty;
                            }
                            

                            if (word.ReplaceWholeString == true || search == string.Empty)
                            {
                                if (logReplace && (logExclusion == null || !logExclusion.Contains(search)))
                                    errorNode.AddWarning(string.Format("{0}.{1}([{2}] replaced by [{3}])", entityName, fieldName, s, replacement));

                                s = replacement;
                            }
                            else
                            {
                                if (logReplace && (logExclusion == null || !logExclusion.Contains(search)))
                                    errorNode.AddWarning(string.Format("{0}.{1}([{2}] partial [{3}] replaced by [{4}])", entityName, fieldName, s, search, replacement));

                                s = s.Replace(search, replacement);
                            }
                        }
                    }
                }
            }


            if (s != null && s.Length > maxLength)
            {
                if (logTruncate)
                    errorNode.AddWarning(string.Format("{0}.{1}[{2}] : Exceeds maximum field length of {3}. Field Truncated", entityName, fieldName, s, maxLength.ToString()));
                return s.Substring(0, maxLength);
            }

            return s;
        }

        protected static string GetAlphaNumericOnly(string s)
        {
            StringBuilder ret = new StringBuilder();

            foreach (char c in s)
            {
                if (Char.IsLetterOrDigit(c))
                    ret.Append(c);
            }
            return ret.ToString();
        }

        protected static DateTime? CheckDate(List<RiskWordFilter> riskWords, string entityName, string fieldName, DateTime? t, string dateTimeFormat, bool canBeNull, MessageNode errorNode)
        {

            if (t == null && canBeNull) return null;

            if (dateTimeFormat == "SmallDateTime")
            {
                t = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(t);
                //return t;
            }

            if (dateTimeFormat == "SmallDate")
            {
                t = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(t);
                //return t;
            }

            if (riskWords != null)
            {
                var lookupWords = (from x in riskWords
                                   where x.TableName == entityName
                                   && x.FieldName == fieldName
                                   orderby x.Id ascending
                                   select x).ToList();

                foreach (var word in lookupWords)
                {
                    if (word.LookupWord == null)
                        continue;


                    DateTime date = new DateTime();

                    if (DateTime.TryParse(word.LookupWord, out date))
                    {
                        if (date == t)
                        {
                            if (!string.IsNullOrEmpty(word.ReplacementWord))
                            {
                                if (word.SearchType == "=")
                                {
                                    DateTime replacemenyDate = new DateTime();

                                    if (DateTime.TryParse(word.ReplacementWord, out replacemenyDate))
                                    {
                                        t = replacemenyDate;

                                        errorNode.AddWarning(string.Format("{0} replaced with {1}", t, replacemenyDate));
                                    }
                                    else
                                    {
                                        t = null;

                                        errorNode.AddWarning(string.Format("{0} replaced NULL", t));
                                    }
                                }
                            }
                            else
                            {
                                t = null;

                                errorNode.AddWarning(string.Format("{0} replaced NULL", t));
                            }
                        }
                    }
                }
            }

            return t;
        }

        protected static string IsValidEmail(string emailaddress, MessageNode errorNode)
        {
            try
            {
                if (emailaddress != null)
                {
                    MailAddress m = new MailAddress(emailaddress);
                }
                return emailaddress;
            }
            catch (FormatException)
            {
                errorNode.AddWarning(string.Format("Invalid email address {0}.", emailaddress));
                return "";
            }
        }

        protected static string IsValidWebsite(string website, MessageNode errorNode)
        {
            try
            {
                if (website != null)
                {
                    Uri m = new Uri(website);
                }
                return website;
            }
            catch (FormatException)
            {
                errorNode.AddWarning(string.Format("Invalid website {0}.", website));
                return "";
            }
        }

        protected static string CheckPhoneNumberValid(string entityName, string fieldName, string phoneNumber, int maxLength, MessageNode errorNode)
        {
            if (phoneNumber == null) return null;

            string cleanPhoneNumber = CleanNumber(phoneNumber); // remove all non-numeric characters

            if (cleanPhoneNumber.Length > maxLength)
            {
                errorNode.AddWarning(string.Format("{0}.{1}[{2}] : Exceeds maximum field length of {3}. Field Truncated", entityName, fieldName, phoneNumber, maxLength.ToString()));
                return cleanPhoneNumber.Substring(0, maxLength);
            }

            return cleanPhoneNumber;
        }

        protected static string CleanNumber(string number)
        {
            if (number == null) return null;

            Regex digitsOnly = new Regex(@"[^\d]");
            return digitsOnly.Replace(number, "");
        }

        protected static string RemoveDigits(string key)
        {
            if (key == null) return null;

            return Regex.Replace(key, @"\d", "");
        }

    }
}
