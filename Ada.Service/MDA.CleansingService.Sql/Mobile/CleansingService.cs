﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.CleansingService.Interface;
using MDA.CleansingService.Model;
using System.Text.RegularExpressions;
using MDA.Common.Server;
using System.Net.Mail;
using MDA.Common;
using MDA.Common.Enum;
using MDA.AddressService.Interface;
using MDA.AddressService.Model;
using MDA.Pipeline.Model;

namespace MDA.CleansingService.Mobile
{
    public partial class CleansingService : CleanseBase
    {
        private IAddressService _addressService;

        public CleansingService(CurrentContext ctx, IAddressService addressService)
            : base(ctx)
        {
            _addressService = addressService;
        }

        /// <summary>
        /// Cleanse a complete Pipeline Claim
        /// </summary>
        /// <param name="claim">Pipeline Claim to process</param>
        /// <returns>Processing Results structure</returns>
        public override ProcessingResults CleanseTheClaim(MDA.Pipeline.Model.IPipelineClaim pipelineClaim)
        {
            PipelineMobileClaim claim = pipelineClaim as PipelineMobileClaim;

            //Create new results structure with Top Node = "Cleansing"
            ProcessingResults cleansingResults = new ProcessingResults("Cleansing");

            try
            {
                string claimNum = claim.ClaimNumber ?? "#NoClaimNum#";

                MessageNode claimErrorNode = new MessageNode(claimNum);

                CleanseClaim(claim, claimErrorNode);

                #region ClaimInfo
                if (claim.ExtraClaimInfo != null)
                {
                    MessageNode claimInfoErrorNode = new MessageNode(claimNum);

                    CleanseClaimInfo(claim, claimInfoErrorNode);
                }
                #endregion

                #region Policy
                if (claim.Policy != null)
                {
                    MessageNode policyErrorNode = new MessageNode("Policy");

                    CleansePolicy(claim.Policy, policyErrorNode);

                    if (policyErrorNode.Nodes.Count > 0)
                        claimErrorNode.AddNode(policyErrorNode);
                }
                #endregion

                #region Organisations
                if (claim.Organisations != null)
                {
                    MessageNode orgsErrorNode = new MessageNode("Organisations");

                    foreach (var organisation in claim.Organisations)
                    {
                        MessageNode orgErrorNode = new MessageNode("Organisation:" + organisation.DisplayText);

                        CleanseOrganisation(organisation, orgErrorNode);

                        #region Organisation Addresses
                        if (organisation.Addresses != null)
                        {
                            MessageNode orgAddressesErrorNode = new MessageNode("Addresses");

                            foreach (var address in organisation.Addresses)
                            {
                                MessageNode orgAddrErrorNode = new MessageNode(address.DisplayText);

                                CleanseAddress(address, orgAddrErrorNode);

                                if (orgAddrErrorNode.Nodes.Count > 0)
                                    orgAddressesErrorNode.AddNode(orgAddrErrorNode);
                            }

                            if (orgAddressesErrorNode.Nodes.Count > 0)
                                orgErrorNode.AddNode(orgAddressesErrorNode);
                        }
                        #endregion

                        #region Organisation Bank Account
                        if (organisation.BankAccounts != null)
                        {
                            MessageNode orgBankAccountsErrorNode = new MessageNode("Bank Accounts");

                            foreach (var ba in organisation.BankAccounts)
                            {
                                MessageNode orgBankAccountErrorNode = new MessageNode("Account");

                                CleanseBankAccount(ba, orgBankAccountErrorNode);

                                if (orgBankAccountErrorNode.Nodes.Count > 0)
                                    orgBankAccountsErrorNode.AddNode(orgBankAccountErrorNode);
                            }

                            if (orgBankAccountsErrorNode.Nodes.Count > 0)
                                orgErrorNode.AddNode(orgBankAccountsErrorNode);
                        }
                        #endregion

                        #region Organisation Payment Cards
                        if (organisation.PaymentCards != null)
                        {
                            MessageNode orgPaymentCardsErrorNode = new MessageNode("Payment Card");

                            foreach (var pc in organisation.PaymentCards)
                            {
                                MessageNode orgPaymentCardErrorNode = new MessageNode("Card");

                                CleansePaymentCard(pc, orgPaymentCardErrorNode);

                                if (orgPaymentCardErrorNode.Nodes.Count > 0)
                                    orgPaymentCardsErrorNode.AddNode(orgPaymentCardErrorNode);
                            }

                            if (orgPaymentCardsErrorNode.Nodes.Count > 0)
                                orgErrorNode.AddNode(orgPaymentCardsErrorNode);
                        }
                        #endregion

                        #region Telephones
                        if (organisation.Telephones != null)
                        {
                            MessageNode orgTelephonesErrorNode = new MessageNode("Telephones");

                            foreach (var tele in organisation.Telephones)
                            {
                                MessageNode orgTelephoneErrorNode = new MessageNode("Telephone");

                                CleanseTelephone(tele, orgTelephoneErrorNode);

                                if (orgTelephoneErrorNode.Nodes.Count > 0)
                                    orgTelephonesErrorNode.AddNode(orgTelephoneErrorNode);
                            }

                            if (orgTelephonesErrorNode.Nodes.Count > 0)
                                orgErrorNode.AddNode(orgTelephonesErrorNode);


                        }
                        #endregion

                        #region I2O_LinkData

                        CleanseIncident2Organisation(organisation.I2O_LinkData, orgErrorNode);

                        #endregion

                        #region Organisation Handsets
                        if (organisation.Handsets != null)
                        {
                            MessageNode orgHandsetsErrorNode = new MessageNode("Handsets");

                            foreach (var h in organisation.Handsets)
                            {
                                MessageNode orgHandsetErrorNode = new MessageNode(organisation.DisplayText);

                                CleanseOrgHandset(h, orgHandsetErrorNode);

                                #region I2H_LinkData

                                CleanseIncident2Handset(h.I2H_LinkData, orgHandsetErrorNode);

                                #endregion

                                //#region H2O_LinkData

                                //CleanseVehicle2Organisation(h.H2O_LinkData, orgHandsetErrorNode);

                                //#endregion

                                if (orgHandsetErrorNode.Nodes.Count > 0)
                                    orgHandsetsErrorNode.AddNode(orgHandsetErrorNode);
                            }

                            if (orgHandsetsErrorNode.Nodes.Count > 0)
                                orgErrorNode.AddNode(orgHandsetsErrorNode);


                        }
                        #endregion

                    }

                    if (orgsErrorNode.Nodes.Count > 0)
                        claimErrorNode.AddNode(orgsErrorNode);
                }
                #endregion Organisations

                #region Handsets
                if (claim.Handsets != null)
                {
                    MessageNode handsetsErrorNode = new MessageNode("Handsets");

                    foreach (var handset in claim.Handsets)
                    {
                        MessageNode handsetErrorNode = new MessageNode("Handset:" + handset.DisplayText);

                        CleanseHandset(handset, handsetErrorNode);

                        #region I2V_LinkData

                        CleanseIncident2Handset(handset.I2H_LinkData, handsetErrorNode);

                        #endregion

                        #region Handset People
                        if (handset.People != null)
                        {
                            MessageNode peopleErrorNode = new MessageNode("People");

                            foreach (var person in handset.People)
                            {
                                MessageNode personErrorNode = new MessageNode("Person:" + person.DisplayText);

                                CleansePerson(person, personErrorNode);

                                #region Person Address
                                if (person.Addresses != null)
                                {
                                    MessageNode perAddressesErrorNode = new MessageNode("Addresses");

                                    foreach (var address in person.Addresses)
                                    {
                                        MessageNode perAddrErrorNode = new MessageNode(address.DisplayText);

                                        CleanseAddress(address, perAddrErrorNode);

                                        if (perAddrErrorNode.Nodes.Count > 0)
                                            perAddressesErrorNode.AddNode(perAddrErrorNode);
                                    }

                                    if (perAddressesErrorNode.Nodes.Count > 0)
                                        personErrorNode.AddNode(perAddressesErrorNode);
                                }
                                #endregion

                                #region Person Bank Accounts
                                if (person.BankAccounts != null)
                                {
                                    MessageNode perBankAccountsErrorNode = new MessageNode("Bank Accounts");

                                    foreach (var ba in person.BankAccounts)
                                    {
                                        MessageNode perBankAccountErrorNode = new MessageNode("Account");

                                        CleanseBankAccount(ba, perBankAccountErrorNode);

                                        if (perBankAccountErrorNode.Nodes.Count > 0)
                                            perBankAccountsErrorNode.AddNode(perBankAccountErrorNode);
                                    }

                                    if (perBankAccountsErrorNode.Nodes.Count > 0)
                                        personErrorNode.AddNode(perBankAccountsErrorNode);
                                }
                                #endregion

                                #region Person Payment Cards
                                if (person.PaymentCards != null)
                                {
                                    MessageNode perPaymentCardsErrorNode = new MessageNode("Payment Cards");

                                    foreach (var pc in person.PaymentCards)
                                    {
                                        MessageNode perPaymentCardErrorNode = new MessageNode("Card");

                                        CleansePaymentCard(pc, perPaymentCardErrorNode);

                                        if (perPaymentCardErrorNode.Nodes.Count > 0)
                                            perPaymentCardsErrorNode.AddNode(perPaymentCardErrorNode);
                                    }

                                    if (perPaymentCardsErrorNode.Nodes.Count > 0)
                                        personErrorNode.AddNode(perPaymentCardsErrorNode);
                                }
                                #endregion

                                #region Telephones
                                if (person.Telephones != null)
                                {
                                    MessageNode perTelephonesErrorNode = new MessageNode("Telephones");

                                    foreach (var tele in person.Telephones)
                                    {
                                        MessageNode perTelephoneErrorNode = new MessageNode("Telephone");

                                        CleanseTelephone(tele, perTelephoneErrorNode);

                                        if (perTelephoneErrorNode.Nodes.Count > 0)
                                            perTelephonesErrorNode.AddNode(perTelephoneErrorNode);
                                    }

                                    if (perTelephonesErrorNode.Nodes.Count > 0)
                                        personErrorNode.AddNode(perTelephonesErrorNode);


                                }
                                #endregion

                                #region EMail Addresses
                                #endregion

                                #region Driving License
                                #endregion

                                #region NI Numbers
                                #endregion

                                #region Passport Numbers
                                #endregion

                                #region I2Pe_LinkData

                                CleanseIncident2Person(person.I2Pe_LinkData, personErrorNode);
                                #endregion

                                #region Person Organisations
                                if (person.Organisations != null)
                                {
                                    MessageNode perOrgsErrorNode = new MessageNode("Organisations");

                                    foreach (var organisation in person.Organisations)
                                    {
                                        MessageNode orgErrorNode = new MessageNode("Organisation:" + organisation.DisplayText);

                                        CleanseOrganisation(organisation, orgErrorNode);

                                        #region Organisation Handsets
                                        if (organisation.Handsets != null)
                                        {
                                            MessageNode orgHandsetsErrorNode = new MessageNode("OrgHandsets");

                                            foreach (var orgHandset in organisation.Handsets)
                                            {
                                                MessageNode orgHandErrorNode = new MessageNode(organisation.DisplayText);

                                                CleanseOrgHandset(orgHandset, orgHandErrorNode);

                                                #region I2H_LinkData

                                                CleanseIncident2Handset(orgHandset.I2H_LinkData, orgHandErrorNode);

                                                #endregion

                                                //#region V2O_LinkData

                                                //CleanseVehicle2Organisation(orgHandset.H2O_LinkData, orgHandErrorNode);

                                                //#endregion

                                                if (orgHandErrorNode.Nodes.Count > 0)
                                                    orgHandsetsErrorNode.AddNode(orgHandErrorNode);
                                            }

                                            if (orgHandsetsErrorNode.Nodes.Count > 0)
                                                orgErrorNode.AddNode(orgHandsetsErrorNode);
                                        }

                                        #endregion

                                        #region Organisation Addresses
                                        if (organisation.Addresses != null)
                                        {
                                            MessageNode orgAddressesErrorNode = new MessageNode("Addresses");

                                            foreach (var address in organisation.Addresses)
                                            {
                                                MessageNode orgAddrErrorNode = new MessageNode(address.DisplayText);

                                                CleanseAddress(address, orgAddrErrorNode);

                                                if (orgAddrErrorNode.Nodes.Count > 0)
                                                    orgAddressesErrorNode.AddNode(orgAddrErrorNode);
                                            }

                                            if (orgAddressesErrorNode.Nodes.Count > 0)
                                                orgErrorNode.AddNode(orgAddressesErrorNode);
                                        }
                                        #endregion

                                        #region Telephones
                                        if (organisation.Telephones != null)
                                        {
                                            MessageNode orgTelephonesErrorNode = new MessageNode("Telephones");

                                            foreach (var tele in organisation.Telephones)
                                            {
                                                MessageNode orgTelephoneErrorNode = new MessageNode("Telephone");

                                                CleanseTelephone(tele, orgTelephoneErrorNode);

                                                if (orgTelephoneErrorNode.Nodes.Count > 0)
                                                    orgTelephonesErrorNode.AddNode(orgTelephoneErrorNode);
                                            }

                                            if (orgTelephonesErrorNode.Nodes.Count > 0)
                                                orgErrorNode.AddNode(orgTelephonesErrorNode);


                                        }
                                        #endregion

                                        #region I2O_LinkData

                                        CleanseIncident2Organisation(organisation.I2O_LinkData, orgErrorNode);

                                        #endregion

                                    }

                                    if (perOrgsErrorNode.Nodes.Count > 0)
                                        personErrorNode.AddNode(perOrgsErrorNode);
                                }
                                #endregion

                                if (personErrorNode.Nodes.Count > 0)
                                    peopleErrorNode.AddNode(personErrorNode);


                            }

                            if (peopleErrorNode.Nodes.Count > 0)
                                handsetErrorNode.AddNode(peopleErrorNode);
                        }

                        #endregion


                        if (handsetErrorNode.Nodes.Count > 0)
                            handsetsErrorNode.AddNode(handsetErrorNode);

                    }

                    if (handsetsErrorNode.Nodes.Count > 0)
                        claimErrorNode.AddNode(handsetsErrorNode);
                }
                #endregion Handsets


                if (claimErrorNode.Nodes.Count > 0)
                    cleansingResults.Message.AddNode(claimErrorNode);
            }
            catch (Exception ex)
            {
                cleansingResults.Message.AddError("Unexpected Error : " + ex.Message);
            }

            return cleansingResults;
        }

        protected override int CleanseAddress(MDA.Pipeline.Model.PipelineAddress address, MessageNode errorNode)
        {
            try
            {
                base.CleanseAddress(address, errorNode);

                var newAddress = _addressService.CleanseAddress(new AddressService.Model.Address()
                {
                    Building = address.Building,
                    BuildingNumber = address.BuildingNumber,
                    County = address.County,
                    Locality = address.Locality,
                    PafUPRN = address.PafUPRN,
                    PafValidation = address.PafValidation,
                    PostCode = address.PostCode,
                    Street = address.Street,
                    SubBuilding = address.SubBuilding,
                    Town = address.Town
                });

                address.Building = newAddress.Building;
                address.BuildingNumber = newAddress.BuildingNumber;
                address.County = newAddress.County;
                address.Locality = newAddress.Locality;
                address.PostCode = newAddress.PostCode;
                address.Street = newAddress.Street;
                address.SubBuilding = newAddress.SubBuilding;
                address.Town = newAddress.Town;
                address.PafValidation = newAddress.PafValidation;
                address.PafUPRN = newAddress.PafUPRN;
            }
            catch (Exception ex)
            {
                errorNode.AddError("Address Db Cleanse Failed : " + ex.Message);
            }

            return 0;
        }

        protected override void CleanseTelephone(MDA.Pipeline.Model.PipelineTelephone telephone, MessageNode errorNode)
        {
            try
            {
                base.CleanseTelephone(telephone, errorNode);

                var x = _ctx.db.uspGetMatchingTelephone(telephone.ClientSuppliedNumber).ToList();

                // one record.  With ID it was found. even if not found reformatted telephone returned
                if (x.Count() == 1)
                {
                    telephone.AreaCode = x[0].AreaCode;
                    telephone.FullNumber = x[0].Number;
                    telephone.InternationalCode = x[0].IntCode;
                }

                if (telephone.FullNumber != null)
                {
                    if (telephone.FullNumber.StartsWith("07"))
                        telephone.TelephoneType_Id = (int)Common.Enum.TelephoneType.Mobile;
                    else if (telephone.TelephoneType_Id == (int)Common.Enum.TelephoneType.Mobile)
                        telephone.TelephoneType_Id = (int)Common.Enum.TelephoneType.Unknown;
                }
            }
            catch (Exception ex)
            {
                errorNode.AddError("Telephone Db Cleanse Failed : " + ex.Message);
            }
        }
    }
}
