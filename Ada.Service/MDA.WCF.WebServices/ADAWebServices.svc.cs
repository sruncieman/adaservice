﻿using MDA.WCF.WebServices.Messages;
using MDA.WCF.WebServices.Interface;
using ADAServices.Factory;
using MDA.WCF.WebServices.Translate;
using MDA.WCF.WebServices.Entities;

namespace MDA.WCF.WebServices
{

    public class ADAWebServices : IADAWebServices
    {
        public void GenerateBatchReport(GenerateBatchReportRequest request)
        {
            ADAServicesFactory.CreateADAServices().GenerateBatchReport(Translator.Translate(request));
        }
        public void GenerateLevel1Report(GenerateLevel1ReportRequest request)
        {
            ADAServicesFactory.CreateADAServices().GenerateLevel1Report(Translator.Translate(request));
        }
        public void GenerateLevel2Report(GenerateLevel2ReportRequest request)
        {
            ADAServicesFactory.CreateADAServices().GenerateLevel2Report(Translator.Translate(request));
        }

        public ProcessClaimBatchResponse ProcessClaimBatch(ProcessClaimBatchRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().ProcessClaimBatch(Translator.Translate(request)));
        }

        public ProcessMobileClaimBatchResponse ProcessMobileClaimBatch(ProcessMobileClaimBatchRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().ProcessMobileClaimBatch(Translator.Translate(request)));
        }

        public ProcessClaimResponse ProcessClaim(ProcessClaimRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().ProcessClaim(Translator.Translate(request)));
        }

        public ClaimsForClientResponse GetClaimsForClient(ClaimsForClientRequest request)
        {
            if (!CheckUserBelongsToClient(new CheckUserBelongsToClientRequest() { ClientId = request.ClientId, UserId = request.UserId, Who = request.Who } ))
            {
                return new ClaimsForClientResponse
                    {
                        Result = -1,
                        Error = "User has insufficient access rights"
                    };
            }

            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetClaimsForClient(Translator.Translate(request)));
        }

        //public SingleClaimForUserResponse GetSingleClaimForUser(SingleClaimForUserRequest request)
        //{
        //    return
        //        Translator.Translate(
        //            ADAServicesFactory.CreateADAServices().GetSingleClaimForUser(Translator.Translate(request)));
        //}

        //public ClaimsForUserResponse GetClaimsForUser(ClaimsForUserRequest request)
        //{
        //    return
        //        Translator.Translate(
        //            ADAServicesFactory.CreateADAServices().GetClaimsForUser(Translator.Translate(request)));

        //}

        public RegisterNewClientResponse RegisterNewClient(RegisterNewClientRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().RegisterNewClient(Translator.Translate(request)));

        }

        public RequestLevelOneReportResponse RequestLevelOneReport(RequestLevelOneReportRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().RequestLevelOneReport(Translator.Translate(request)));
        }

        public RequestLevelTwoReportResponse RequestLevelTwoReport(RequestLevelTwoReportRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().RequestLevelTwoReport(Translator.Translate(request)));
        }

        public RequestCallbackResponse RequestCallback(RequestCallbackRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().RequestCallback(Translator.Translate(request)));
        }

        public ClearCallbackRequestResponse ClearCallbackRequest(ClearCallbackRequestRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().ClearCallbackRequest(Translator.Translate(request)));
        }

        public SetClaimReadStatusResponse SetClaimReadStatus(SetClaimReadStatusRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().SetClaimReadStatus(Translator.Translate(request)));
        }

        public GetBatchStatusResponse GetBatchStatus(GetBatchStatusRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetBatchStatus(Translator.Translate(request)));
        }

        public GetBatchProcessingResultsResponse GetBatchProcessingResults(GetBatchProcessingResultsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetBatchProcessingResults(Translator.Translate(request)));
        }

        public IsClientBatchReferenceUniqueResponse IsClientBatchReferenceUnique(IsClientBatchReferenceUniqueRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().IsClientBatchReferenceUnique(Translator.Translate(request)));
        }

        public IsClientNameUniqueResponse IsClientNameUnique(IsClientNameUniqueRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().IsClientNameUnique(Translator.Translate(request)));
        }

        public IsPasswordUniqueResponse IsPasswordUnique(IsPasswordUniqueRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().IsPasswordUnique(Translator.Translate(request)));
        }

        public IsUserNameUniqueResponse IsUserNameUnique(IsUserNameUniqueRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().IsUserNameUnique(Translator.Translate(request)));
        }

        #region File Editor Methods
        public ClaimsInBatchResponse GetClaimsInBatch(ClaimsInBatchRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetClaimsInBatch(Translator.Translate(request)));
        }

        public GetClaimScoreDataResponse GetClaimScoreData(GetClaimScoreDataRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetClaimScoreData(Translator.Translate(request)));
        }

        public BatchListResponse GetClientBatchList(BatchListRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetClientBatchList(Translator.Translate(request)));
        }
        #endregion

        public GetRiskUserInfoResponse GetRiskUserInfo(GetRiskUserInfoRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskUserInfo(Translator.Translate(request)));
        }

        #region membership

        public int GetPasswordFailuresSinceLastSuccess(GetPasswordFailuresSinceLastSuccessRequest request)
        {
            return ADAServicesFactory.CreateADAServices().GetPasswordFailuresSinceLastSuccess(Translator.Translate(request));
        }

        public int GetUserId(GetUserIdRequest request)
        {
            return ADAServicesFactory.CreateADAServices().GetUserId(Translator.Translate(request));
        }

        public bool IsUserInRole(IsUserInRoleRequest request)
        {
            return ADAServicesFactory.CreateADAServices().IsUserInRole(Translator.Translate(request));
        }

        public string[] GetRolesForUser(GetRolesForUserRequest request)
        {
            return ADAServicesFactory.CreateADAServices().GetRolesForUser(Translator.Translate(request));
        }

        public bool ValidateUser(ValidateUserRequest request)
        {
            return ADAServicesFactory.CreateADAServices().ValidateUser(Translator.Translate(request));
        }

        public void CreateUser(CreateUserRequest request)
        {
            ADAServicesFactory.CreateADAServices().CreateUser(Translator.Translate(request));
        }

        public void CreateClient(CreateClientRequest request)
        {
            ADAServicesFactory.CreateADAServices().CreateClient(Translator.Translate(request));
        }

        public void CreateRiskWord(CreateRiskWordRequest request)
        {
            ADAServicesFactory.CreateADAServices().CreateRiskWord(Translator.Translate(request));
        }

        public void CreateRiskDefaultData(CreateRiskDefaultDataRequest request)
        {
            ADAServicesFactory.CreateADAServices().CreateRiskDefaultData(Translator.Translate(request));
        }

        public void CreateRiskNoteDecision(CreateRiskNoteDecisionRequest request)
        {
            ADAServicesFactory.CreateADAServices().CreateRiskNoteDecision(Translator.Translate(request));
        }

        public void DeleteRiskDefaultData(DeleteRiskDefaultDataRequest request)
        {
            ADAServicesFactory.CreateADAServices().DeleteRiskDefaultData(Translator.Translate(request));
        }

        public void DeleteRiskNoteDecision(DeleteRiskNoteDecisionRequest request)
        {
            ADAServicesFactory.CreateADAServices().DeleteRiskNoteDecision(Translator.Translate(request));
        }
                

        public void UpdateClient(UpdateClientRequest request)
        {
            ADAServicesFactory.CreateADAServices().UpdateClient(Translator.Translate(request));
        }

        public void UpdateUser(UpdateUserRequest request)
        {
            ADAServicesFactory.CreateADAServices().UpdateUser(Translator.Translate(request));
        }

        public void CreateRiskUser2Client(CreateRiskUser2ClientRequest request)
        {
            ADAServicesFactory.CreateADAServices().CreateRiskUser2Client(Translator.Translate(request));
        }

        public void DeleteRiskUser2Client(DeleteRiskUser2ClientRequest request)
        {
            ADAServicesFactory.CreateADAServices().DeleteRiskUser2Client(Translator.Translate(request));
        }

        public bool ChangePassword(ChangePasswordRequest request)
        {
            return ADAServicesFactory.CreateADAServices().ChangePassword(Translator.Translate(request));
        }

        #endregion

        #region Website Methods

        public RiskClientsResponse GetRiskClients(RiskClientsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskClients(Translator.Translate(request)));
        }

        public RiskWordsResponse GetRiskWords(RiskWordsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskWords(Translator.Translate(request)));
        }

        public RiskNotesResponse GetRiskNotes(RiskNotesRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskNotes(Translator.Translate(request)));
        }

        public GetRiskNoteResponse GetRiskNote(GetRiskNoteRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskNote(Translator.Translate(request)));
        }

        public RiskNoteDecisionsResponse GetRiskNoteDecisions(RiskNoteDecisionsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskNoteDecisions(Translator.Translate(request)));
        }

        public RiskWordDeleteResponse DeleteRiskWord(RiskWordDeleteRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().DeleteRiskWord(Translator.Translate(request)));
        }

        public RiskNoteCreateResponse CreateRiskNote(RiskNoteCreateRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().CreateRiskNote(Translator.Translate(request)));
        }

        public RiskNoteUpdateResponse UpdateRiskNote(RiskNoteUpdateRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().UpdateRiskNote(Translator.Translate(request)));
        }

        public RiskDefaultDataResponse GetRiskDefaultData(RiskDefaultDataRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskDefaultData(Translator.Translate(request)));
        }


        public GetAssignedRiskClientsForUserResponse GetAssignedRiskClientsForUser(GetAssignedRiskClientsForUserRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetAssignedRiskClientsForUser(Translator.Translate(request)));
        }


        public RiskRolesResponse GetRiskRoles(RiskRolesRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskRoles(Translator.Translate(request)));
        }

        public InsurersClientsResponse GetInsurersClients(InsurersClientsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetInsurersClients(Translator.Translate(request)));
        }

        public RiskUsersLockedResponse GetRiskUsersLocked(RiskUsersLockedRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskUsersLocked(Translator.Translate(request)));
        }

        public RiskUserUnlockResponse UnlockRiskUser(RiskUserUnlockRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().UnlockRiskUser(Translator.Translate(request)));
        }

        public GeneratePasswordResetTokenResponse GeneratePasswordResetToken(GeneratePasswordResetTokenRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GeneratePasswordResetToken(Translator.Translate(request)));
        }

        public ValidatePasswordResetTokenResponse ValidatePasswordResetToken(ValidatePasswordResetTokenRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().ValidatePasswordResetToken(Translator.Translate(request)));
        }


        public SendElmahErrorResponse SendElmahError(SendElmahErrorRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().SendElmahEmail(Translator.Translate(request)));
        }

        
        public RiskUsersResponse GetRiskUsers(RiskUserRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetRiskUsers(Translator.Translate(request)));
        }

        public WebsiteBatchListResponse GetBatchList(WebsiteBatchListRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetBatchList(Translator.Translate(request)));
        }

        public WebsiteSingleClaimsListResponse GetSingleClaimsList(WebsiteSingleClaimsListRequest request)
        {
            return
                Translator.Translate(
                    ADAServicesFactory.CreateADAServices().GetSingleClaims(Translator.Translate(request)));
        }

        public WebsiteSaveSingleClaimResponse SaveSingleClaim(WebsiteSaveSingleClaimRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().SaveSingleClaim(Translator.Translate(request)));
        }

        public WebsiteGetTotalSingleClaimsForClientResponse GetTotalSingleClaimForClient(WebsiteGetTotalSingleClaimsForClientRequest request)
        {
            return
                Translator.Translate(
                    ADAServicesFactory.CreateADAServices().GetTotalSingleClaimForClient(Translator.Translate(request)));
        }

        public WebsiteFetchSingleClaimResponse FetchSingleClaimDetail(WebsiteFetchSingleClaimRequest request)
        {
            return
                Translator.Translate(
                    ADAServicesFactory.CreateADAServices().FetchSingleClaimDetail(Translator.Translate(request)));
        }

        public WebsiteDeleteSingleClaimResponse DeleteSingleClaim(WebsiteDeleteSingleClaimRequest request)
        {
            return
                Translator.Translate(
                    ADAServicesFactory.CreateADAServices().DeleteSingleClaim(Translator.Translate(request)));
        }

        public WebsiteFilterBatchNumbersResponse FilterBatchNumbers(WebsiteFilterBatchNumbersRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().FilterBatchNumbers(Translator.Translate(request)));
        }

        public WebsiteCueInvolvementsResponse GetCueInvolvements(WebsiteCueInvolvementsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetCueInvolvements(Translator.Translate(request)));               
        }

        public WebsiteCueSearchResponse SubmitCueSearch(WebsiteCueSearchRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().SubmitCueSearch(Translator.Translate(request))); 
        }

        public WebsiteCueReportResponse GetCueReport(WebsiteCueReportRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetCueReport(Translator.Translate(request)));
        }

        public WebsiteSaveTemplateFunctionsResponse SaveTemplateFunctions(WebsiteSaveTemplateFunctionsRequest request)
        {

            if (!CheckUserBelongsToClient(new CheckUserBelongsToClientRequest() { ClientId = request.ClientId, UserId = request.UserId, Who = request.Who }))
            {
                return new WebsiteSaveTemplateFunctionsResponse
                {
                    Result = -1,
                    Error = "User has insufficient access rights"
                };
            }

            return Translator.Translate(ADAServicesFactory.CreateADAServices().SaveTemplateFunctions(Translator.Translate(request)));
        }


        public WebsiteLoadTemplateFunctionsResponse LoadTemplateFunctions(WebsiteLoadTemplateFunctionsRequest request)
        {
            if (!CheckUserBelongsToClient(new CheckUserBelongsToClientRequest() { ClientId = request.ClientId, UserId = request.UserId, Who = request.Who }))
            {
                return new WebsiteLoadTemplateFunctionsResponse
                {
                    Result = -1,
                    Error = "User has insufficient access rights"
                };
            }

            return Translator.Translate(ADAServicesFactory.CreateADAServices().LoadTemplateFunctions(Translator.Translate(request)));
        }

        public WebsiteRiskRoleCreateResponse CreateRiskRole(WebsiteRiskRoleCreateRequest request)
        {
            if (!CheckUserBelongsToClient(new CheckUserBelongsToClientRequest() { ClientId = request.ClientId, UserId = request.UserId, Who = request.Who }))
            {
                return new WebsiteRiskRoleCreateResponse
                {
                    Result = -1,
                    Error = "User has insufficient access rights"
                };

            }

            return Translator.Translate(ADAServicesFactory.CreateADAServices().CreateRiskRole(Translator.Translate(request)));
        }

        public WebsiteRiskRoleEditResponse EditRiskRole(WebsiteRiskRoleEditRequest request)
        {
            if (!CheckUserBelongsToClient(new CheckUserBelongsToClientRequest() { ClientId = request.ClientId, UserId = request.UserId, Who = request.Who }))
            {
                return new WebsiteRiskRoleEditResponse
                {
                    Result = -1,
                    Error = "User has insufficient access rights"
                };

            }

            return Translator.Translate(ADAServicesFactory.CreateADAServices().EditRiskRole(Translator.Translate(request)));
        }

        public WebsiteRiskRoleDeleteResponse DeleteRiskRole(WebsiteRiskRoleDeleteRequest request)
        {
            if (!CheckUserBelongsToClient(new CheckUserBelongsToClientRequest() { ClientId = request.ClientId, UserId = request.UserId, Who = request.Who }))
            {
                return new WebsiteRiskRoleDeleteResponse
                {
                    Result = -1,
                    Error = "User has insufficient access rights"
                };

            }

            return Translator.Translate(ADAServicesFactory.CreateADAServices().DeleteRiskRole(Translator.Translate(request)));
        }

        public FindUserNameResponse FindUserName(FindUserNameRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().FindUserName(Translator.Translate(request)));
        }

        public WebSearchResultsResponse FindSearchResults(WebSearchResultsRequest request)
        {
            if (!CheckUserBelongsToClient(new CheckUserBelongsToClientRequest() { ClientId = request.ClientId, UserId = request.UserId, Who = request.Who }))
            {
                return new WebSearchResultsResponse
                {
                    Result = -1,
                    Error = "User has insufficient access rights"
                };
            }

            return Translator.Translate(ADAServicesFactory.CreateADAServices().FindSearchResultDetails(Translator.Translate(request)));
        }

        public GetListOfBatchPriorityClientsResponse GetListOfBatchPriorityClients(GetListOfBatchPriorityClientsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetListOfBatchPriorityClients(Translator.Translate(request)));
        }

        public SaveBatchPriorityClientsResponse SaveBatchPriorityClients(SaveBatchPriorityClientsRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().SaveBatchPriorityClients(Translator.Translate(request)));
        }

        public GetBatchQueueResponse GetBatchQueue(GetBatchQueueRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().GetBatchQueue(Translator.Translate(request)));
        }

        public SaveOverriddenBatchPriorityResponse SaveOverriddenBatchPriority(SaveOverriddenBatchPriorityRequest request)
        {
            return Translator.Translate(ADAServicesFactory.CreateADAServices().SaveOverriddenBatchPriority(Translator.Translate(request)));
        }

        #endregion

        #region Security

        private bool CheckUserBelongsToClient(CheckUserBelongsToClientRequest request) //(int clientId, int userId)
        {
            return ADAServicesFactory.CreateADAServices().CheckUserBelongsToClient(Translator.Translate(request));
        }

        public bool CheckUserHasAccessToReport(CheckUserHasAccessToReportRequest request)
        {
            return ADAServicesFactory.CreateADAServices().CheckUserHasAccessToReport(Translator.Translate(request));
        }

        #endregion


















      
    }
}
