﻿using System.ServiceModel;
using MDA.WCF.WebServices.Messages;
using MDA.WCF.WebServices.Entities;


namespace MDA.WCF.WebServices.Interface
{

    [ServiceContract(Namespace = "http://www.keoghs.co.uk/ADA")]
    public interface IADAWebServices
    {
        [OperationContract]
        void GenerateBatchReport(GenerateBatchReportRequest request);
        [OperationContract]
        void GenerateLevel1Report(GenerateLevel1ReportRequest request);
        [OperationContract]
        void GenerateLevel2Report(GenerateLevel2ReportRequest request);

        [OperationContract]
        bool CheckUserHasAccessToReport(CheckUserHasAccessToReportRequest request);

        /// <summary>
        /// Process a BATCH of claims
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        ProcessClaimBatchResponse ProcessClaimBatch(ProcessClaimBatchRequest request);


        /// <summary>
        /// Process a BATCH of mobile phone claims
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        ProcessMobileClaimBatchResponse ProcessMobileClaimBatch(ProcessMobileClaimBatchRequest request);

        /// <summary>
        /// Process a single claim
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        ProcessClaimResponse ProcessClaim(ProcessClaimRequest request);

        /// <summary>
        /// Get a filtered list of claims
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        ClaimsForClientResponse GetClaimsForClient(ClaimsForClientRequest request);

        


        /// <summary>
        /// Register a new client by name. Returns the new ID
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        RegisterNewClientResponse RegisterNewClient(RegisterNewClientRequest request);

        /// <summary>
        /// Request a Level 1 report for given claim
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        RequestLevelOneReportResponse RequestLevelOneReport(RequestLevelOneReportRequest request);

        /// <summary>
        /// Request a level 2 report for given claim 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        RequestLevelTwoReportResponse RequestLevelTwoReport(RequestLevelTwoReportRequest request);

        /// <summary>
        /// request a callback for the given claim
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        RequestCallbackResponse RequestCallback(RequestCallbackRequest request);

        /// <summary>
        /// Reset a request for a callback on the given claim
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        ClearCallbackRequestResponse ClearCallbackRequest(ClearCallbackRequestRequest request);

        /// <summary>
        /// Set the READ STATUS on a claim
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        SetClaimReadStatusResponse SetClaimReadStatus(SetClaimReadStatusRequest request);


        [OperationContract]
        WebsiteSaveTemplateFunctionsResponse SaveTemplateFunctions(WebsiteSaveTemplateFunctionsRequest request);
         
        [OperationContract]
        WebsiteLoadTemplateFunctionsResponse LoadTemplateFunctions(WebsiteLoadTemplateFunctionsRequest request);

        [OperationContract]
        GetBatchStatusResponse GetBatchStatus(GetBatchStatusRequest request);

        [OperationContract]
        GetBatchProcessingResultsResponse GetBatchProcessingResults(GetBatchProcessingResultsRequest request);

        [OperationContract]
        IsClientBatchReferenceUniqueResponse IsClientBatchReferenceUnique(IsClientBatchReferenceUniqueRequest request);

        [OperationContract]
        IsClientNameUniqueResponse IsClientNameUnique(IsClientNameUniqueRequest request);

        [OperationContract]
        IsPasswordUniqueResponse IsPasswordUnique(IsPasswordUniqueRequest request);

        [OperationContract]
        IsUserNameUniqueResponse IsUserNameUnique(IsUserNameUniqueRequest request);


        #region File Editor Methods

        [OperationContract]
        ClaimsInBatchResponse GetClaimsInBatch(ClaimsInBatchRequest request);   // File Editor

        [OperationContract]
        BatchListResponse GetClientBatchList(BatchListRequest batchListRequest);     // File Editor

        [OperationContract]
        GetClaimScoreDataResponse GetClaimScoreData(GetClaimScoreDataRequest request);   // File Editor uses this
        #endregion

        [OperationContract]
        GetRiskUserInfoResponse GetRiskUserInfo(GetRiskUserInfoRequest request);

        #region Membership

        [OperationContract]
        int GetPasswordFailuresSinceLastSuccess(GetPasswordFailuresSinceLastSuccessRequest request);

        [OperationContract]
        bool IsUserInRole(IsUserInRoleRequest request);

        [OperationContract]
        string[] GetRolesForUser(GetRolesForUserRequest request);

        [OperationContract]
        int GetUserId(GetUserIdRequest request);

        [OperationContract]
        bool ValidateUser(ValidateUserRequest request);

        [OperationContract]
        void CreateUser(CreateUserRequest request);

        [OperationContract]
        void CreateClient(CreateClientRequest request);

        [OperationContract]
        void CreateRiskWord(CreateRiskWordRequest request);

        [OperationContract]
        void CreateRiskDefaultData(CreateRiskDefaultDataRequest request);

        [OperationContract]
        void CreateRiskNoteDecision(CreateRiskNoteDecisionRequest request);

        [OperationContract]
        void DeleteRiskDefaultData(DeleteRiskDefaultDataRequest request);

        [OperationContract]
        void DeleteRiskNoteDecision(DeleteRiskNoteDecisionRequest request);

        [OperationContract]
        void UpdateClient(UpdateClientRequest request);

        [OperationContract]
        void UpdateUser(UpdateUserRequest request);

        [OperationContract]
        void CreateRiskUser2Client(CreateRiskUser2ClientRequest request);

        [OperationContract]
        void DeleteRiskUser2Client(DeleteRiskUser2ClientRequest request);

        [OperationContract]
        bool ChangePassword(ChangePasswordRequest request);

        #endregion

        #region Website Methods

        [OperationContract]
        GetListOfBatchPriorityClientsResponse GetListOfBatchPriorityClients(GetListOfBatchPriorityClientsRequest request);

        [OperationContract]
        SaveBatchPriorityClientsResponse SaveBatchPriorityClients(SaveBatchPriorityClientsRequest request);

        [OperationContract]
        RiskClientsResponse GetRiskClients(RiskClientsRequest request);

        [OperationContract]
        RiskWordsResponse GetRiskWords(RiskWordsRequest request);

        [OperationContract]
        RiskNotesResponse GetRiskNotes(RiskNotesRequest request);

        [OperationContract]
        GetRiskNoteResponse GetRiskNote(GetRiskNoteRequest request);

        [OperationContract]
        RiskNoteDecisionsResponse GetRiskNoteDecisions(RiskNoteDecisionsRequest request);

        [OperationContract]
        RiskWordDeleteResponse DeleteRiskWord(RiskWordDeleteRequest request);

        [OperationContract]
        RiskNoteCreateResponse CreateRiskNote(RiskNoteCreateRequest request);

        [OperationContract]
        RiskNoteUpdateResponse UpdateRiskNote(RiskNoteUpdateRequest request);

        [OperationContract]
        RiskDefaultDataResponse GetRiskDefaultData(RiskDefaultDataRequest request);

        [OperationContract]
        GetAssignedRiskClientsForUserResponse GetAssignedRiskClientsForUser(GetAssignedRiskClientsForUserRequest request);

        [OperationContract]
        RiskRolesResponse GetRiskRoles(RiskRolesRequest request);

        [OperationContract]
        InsurersClientsResponse GetInsurersClients(InsurersClientsRequest request);
 
        [OperationContract]
        RiskUsersLockedResponse GetRiskUsersLocked(RiskUsersLockedRequest riskUsersLockedRequest);

        [OperationContract]
        RiskUserUnlockResponse UnlockRiskUser(RiskUserUnlockRequest riskUserUnlockRequest);

        [OperationContract]
        RiskUsersResponse GetRiskUsers(RiskUserRequest request);

        [OperationContract]
        GeneratePasswordResetTokenResponse GeneratePasswordResetToken(GeneratePasswordResetTokenRequest request);

        [OperationContract]
        ValidatePasswordResetTokenResponse ValidatePasswordResetToken(ValidatePasswordResetTokenRequest request);

        [OperationContract]
        SendElmahErrorResponse SendElmahError(SendElmahErrorRequest request);

        [OperationContract]
        WebsiteBatchListResponse GetBatchList(WebsiteBatchListRequest batchListRequest);

        [OperationContract]
        WebsiteSingleClaimsListResponse GetSingleClaimsList(WebsiteSingleClaimsListRequest websiteSingleClaimsListRequest);

        [OperationContract]
        WebsiteSaveSingleClaimResponse SaveSingleClaim(WebsiteSaveSingleClaimRequest websiteSaveSingleClaimRequest);

        [OperationContract]
        WebsiteGetTotalSingleClaimsForClientResponse GetTotalSingleClaimForClient(WebsiteGetTotalSingleClaimsForClientRequest websiteGetTotalSingleClaimForClientRequest);

        [OperationContract]
        WebsiteDeleteSingleClaimResponse DeleteSingleClaim(WebsiteDeleteSingleClaimRequest websiteDeleteSingleClaimRequest);

        [OperationContract]
        WebsiteFetchSingleClaimResponse FetchSingleClaimDetail(WebsiteFetchSingleClaimRequest websiteFetchSingleClaimRequest);

        [OperationContract]
        WebsiteFilterBatchNumbersResponse FilterBatchNumbers(WebsiteFilterBatchNumbersRequest websiteFilterBatchNumbersRequest);

        [OperationContract]
        WebsiteCueInvolvementsResponse GetCueInvolvements(WebsiteCueInvolvementsRequest websiteCueInvolvementsRequest);

        [OperationContract]
        WebsiteCueSearchResponse SubmitCueSearch(WebsiteCueSearchRequest websiteCueSearchRequest);

        [OperationContract]
        WebsiteCueReportResponse GetCueReport(WebsiteCueReportRequest websiteCueReportRequest);

        [OperationContract]
        WebsiteRiskRoleCreateResponse CreateRiskRole(WebsiteRiskRoleCreateRequest websiteRiskRoleCreateRequest);

        [OperationContract]
        WebsiteRiskRoleEditResponse EditRiskRole(WebsiteRiskRoleEditRequest websiteRiskRoleEditRequest);

        [OperationContract]
        WebsiteRiskRoleDeleteResponse DeleteRiskRole(WebsiteRiskRoleDeleteRequest websiteRiskRoleDeleteRequest);


        [OperationContract]
        FindUserNameResponse FindUserName(FindUserNameRequest findUserNameRequest);


        [OperationContract]
        WebSearchResultsResponse FindSearchResults(WebSearchResultsRequest request);

        [OperationContract]
        GetBatchQueueResponse GetBatchQueue(GetBatchQueueRequest request);


        [OperationContract]
        SaveOverriddenBatchPriorityResponse SaveOverriddenBatchPriority(SaveOverriddenBatchPriorityRequest request);

        #endregion

    }
}
