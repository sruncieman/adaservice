﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MDA.Pipeline.Model;
using MDA.RiskService.Model;
using MDA.Common;
using MDA.WCF.WebServices;
using System.ServiceModel;

namespace MDA.WCF.WebServices.Messages
{


    #region ProcessClaimBatch

    [DataContract]
    public class ProcessClaimBatchRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string BatchEntityType { get; set; }

        [DataMember]
        public string ClientBatchReference { get; set; }

        [DataMember]
        public MDA.Common.FileModel.MotorClaimBatch Batch { get; set; }
    }

    [DataContract]
    public class ProcessMobileClaimBatchRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string BatchEntityType { get; set; }

        [DataMember]
        public string ClientBatchReference { get; set; }

        [DataMember]
        public MDA.Common.FileModel.MobileClaimBatch Batch { get; set; }
    }

    [DataContract]
    public class ProcessClaimBatchResponse
    {
        [DataMember]
        public ProcessingResults Results { get; set; }
    }

    [DataContract]
    public class SaveBatchPriorityClientsRequest
    {
        [DataMember]
        public int ClientId { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string Who { get; set; }
        [DataMember]
        public int[] ClientBatchPriorityArray { get; set; }
    }


    [DataContract]
    public class SaveBatchPriorityClientsResponse
    {
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }


    [DataContract]
    public class ProcessMobileClaimBatchResponse
    {
        [DataMember]
        public ProcessingResults Results { get; set; }
    }

    #endregion

    #region ClaimsForClient

    [DataContract]
    public class ClaimsForClientRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterUserId { get; set; }

        [DataMember]
        public int FilterClientId { get; set; }


        /// <summary>
        /// The team ID
        /// </summary>
        [DataMember]
        public int TeamId { get; set; }

        /// <summary>
        /// The client ID of the Logged In user from RiskUser table.
        /// </summary>
        //[DataMember]
        //public int LoggedInClientId { get; set; }

        /// <summary>
        /// The Logged In user ID
        /// </summary>
        //[DataMember]
        //public int LoggedInUserId { get; set; }

        /// <summary>
        /// Set to NULL to not apply filter.  Set to a NoneEmpty string to use as filter on RiskClaim.CreatedBy
        /// </summary>
        [DataMember]
        public string FilterUploadedBy { get; set; }

        /// <summary>
        /// Set to NULL to not apply filter.  Set to a NoneEmpty string to use as filter on RiskClaim.BatchReference
        /// </summary>
        [DataMember]
        public string[] FilterBatchRef { get; set; }

        /// <summary>
        /// Set to NULL to not apply filter.  Set to a NoneEmpty string to use as filter on RiskClaim.ClientClaimRefNumber
        /// </summary>
        [DataMember]
        public string FilterClaimNumber { get; set; }

        /// <summary>
        /// Set to NULL to not apply filter.  Set to a DateTime to use as filter on RiskClaim.IncidentDate >= FilterStartDate
        /// </summary>
        [DataMember]
        public DateTime? FilterStartDate { get; set; }

        /// <summary>
        /// Set to NULL to not apply filter.  Set to a DateTime to use as filter on RiskClaim.IncidentDate <= FilterEndDate
        /// </summary>
        [DataMember]
        public DateTime? FilterEndDate { get; set; }

        /// <summary>
        /// Set to true to return all high risk claims
        /// </summary>
        [DataMember]
        public bool FilterHighRisk { get; set; }

        /// <summary>
        /// Set to true to return all medium risk claims
        /// </summary>
        [DataMember]
        public bool FilterMediumRisk { get; set; }

        /// <summary>
        /// Set to true to return all low risk claims
        /// </summary>
        [DataMember]
        public bool FilterLowRisk { get; set; }

        /// <summary>
        /// Set to true to return all Keoghs CFS risk claims
        /// </summary>
        [DataMember]
        public bool FilterKeoghsCFS { get; set; }

        /// <summary>
        /// Set to true to return all previous versions of risk claims
        /// </summary>
        [DataMember]
        public bool FilterPreviousVersion { get; set; }

        /// <summary>
        /// Set to true to return all errors of risk claims
        /// </summary>
        [DataMember]
        public bool FilterError { get; set; }

        /// <summary>
        /// Set to true to return all unread claims
        /// </summary>
        [DataMember]
        public bool FilterStatusUnread { get; set; }

        /// <summary>
        /// Set to true to return all read claims
        /// </summary>
        [DataMember]
        public bool FilterStatusRead { get; set; }

        /// <summary>
        /// Set to true to return all unread claims with score change
        /// </summary>
        [DataMember]
        public bool FilterStatusUnreadScoreChanges { get; set; }

        [DataMember]
        public bool FilterStatusReserveChange { get; set; }

        /// <summary>
        /// Set to true to return all unrequested reports
        /// </summary>
        [DataMember]
        public bool FilterStatusUnrequestedReports { get; set; }

        /// <summary>
        /// Set to true to return all pending reports
        /// </summary>
        [DataMember]
        public bool FilterStatusPendingReports { get; set; }

        /// <summary>
        /// Set to true to return all available reports
        /// </summary>
        [DataMember]
        public bool FilterStatusAvailableReports { get; set; }

        /// <summary>
        /// Set to true to return all claims with no reports requested
        /// </summary>
        [DataMember]
        public bool FilterNoReportsRequested { get; set; }

        /// <summary>
        /// Set to true to return all claims with level 1 reports requested
        /// </summary>
        [DataMember]
        public bool FilterLevel1ReportsRequested { get; set; }

        /// <summary>
        /// Set to true to return all claims with level 2 reports requested
        /// </summary>
        [DataMember]
        public bool FilterLevel2ReportsRequested { get; set; }

        /// <summary>
        /// Set to true to return all claims with callbacks requested
        /// </summary>
        [DataMember]
        public bool FilterCallbackReportsRequested { get; set; }

        /// <summary>
        /// Set to true to return all claims with notes
        /// </summary>
        [DataMember]
        public bool FilterNotes { get; set; }

        /// <summary>
        /// Set to true to return all claims with intel attachments
        /// </summary>
        [DataMember]
        public bool FilterIntel { get; set; }

        /// <summary>
        /// Set to true to return all claims with decisions
        /// </summary>
        [DataMember]
        public bool FilterDecisions { get; set; }

        /// <summary>
        /// Set to true to return all claims with decision id
        /// </summary>
        [DataMember]
        public int?[] FilterDecisionIds { get; set; }

        /// <summary>
        /// Can be one of "INCIDENTDATE", "CLIENTCLAIMREFNUMBER", "CLIENTBATCHREFERENCE", 
        /// "KEYATTRACTORCOUNT", "RESERVE", "PAYMENTSTODATE", "UPLOADDATETIME"
        /// </summary>
        [DataMember]
        public string SortColumn { get; set; }

        /// <summary>
        /// Set to True to set sort order to ascending otherwise descending.
        /// </summary>
        [DataMember]
        public bool SortAscending { get; set; }

        /// <summary>
        /// Page number required. Starts at 1
        /// </summary>
        [DataMember]
        public int Page { get; set; }

        /// <summary>
        /// Number of rows in a page
        /// </summary>
        [DataMember]
        public int PageSize { get; set; }
    }

    [DataContract]
    [KnownType(typeof(MDA.RiskService.Model.ListOfMsgStrings))]
    public class ClaimsForClientResponse
    {
        public ClaimsForClientResponse()
        {
            ClaimsList = new List<ExRiskClaimDetails>();
        }

        [DataMember]
        public List<ExRiskClaimDetails> ClaimsList { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        /// <summary>
        /// Total rows in ResultSet (not the number of rows in the page)
        /// </summary>
        [DataMember]
        public int TotalRows { get; set; }
    }

    #endregion

    #region ClaimsForUser

    [DataContract]
    public class ClaimsForUserRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class ClaimsForUserResponse
    {
        public ClaimsForUserResponse()
        {
            ClaimsList = new List<ExRiskClaimEdit>();
        }

        [DataMember]
        public List<ExRiskClaimEdit> ClaimsList { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        /// <summary>
        /// Total rows in ResultSet (not the number of rows in the page)
        /// </summary>
        [DataMember]
        public int TotalRows { get; set; }
    }

    #endregion

    #region SingleClaimForUser

    [DataContract]
    public class SingleClaimForUserRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        /// <summary>
        /// Claim To Edit
        /// </summary>
        [DataMember]
        public int EditClaimId { get; set; }
    }

    [DataContract]
    public class SingleClaimForUserResponse
    {
        [DataMember]
        public ERiskClaimEdit Claim { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    //[DataMember]
    //    public List<ExRiskClaimEdit> ClaimsList { get; set; }

    //    /// <summary>
    //    /// 0 = success, -1 = error
    //    /// </summary>
    //    [DataMember]
    //    public int Result { get; set; }

    //    /// <summary>
    //    /// Error string indicating an error
    //    /// </summary>
    //    [DataMember]
    //    public string Error { get; set; }

    #endregion



    #region Process Claim

    [DataContract]
    public class ProcessClaimResponse
    {
        [DataMember]
        public ProcessingResults Results { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }
    }

    [DataContract]
    public class ProcessClaimRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string BatchEntityType { get; set; }

        [DataMember]
        public string ClientBatchReference { get; set; }

        [DataMember]
        public MDA.Common.FileModel.MotorClaim Claim { get; set; }
    }

    #endregion

    #region RegisterNewClient

    [DataContract]
    public class RegisterNewClientResponse
    {
        [DataMember]
        public int Result { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class RegisterNewClientRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string ClientName { get; set; }
    }

    #endregion

    #region RequestLevelOneReport

    [DataContract]
    public class RequestLevelOneReportResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class RequestLevelOneReportRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }
    }

    #endregion

    #region RequestLevelTwoReport

    [DataContract]
    public class RequestLevelTwoReportResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class RequestLevelTwoReportRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }


    }

    #endregion

    #region RequestCallback

    [DataContract]
    public class RequestCallbackResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class RequestCallbackRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }
    }

    #endregion

    #region ClearCallback

    [DataContract]
    public class ClearCallbackRequestResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class ClearCallbackRequestRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }
    }

    #endregion

    [DataContract]
    public class WebsiteRiskRoleCreateRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string TemplateName { get; set; }
    }

    [DataContract]
    public class WebsiteRiskRoleCreateResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class WebsiteRiskRoleEditRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int Profile_Id { get; set; }

        [DataMember]
        public string TemplateName { get; set; }
    }

    [DataContract]
    public class WebsiteRiskRoleEditResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class WebsiteRiskRoleDeleteRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int Profile_Id { get; set; }
    }

    [DataContract]
    public class WebsiteRiskRoleDeleteResponse
    {
        /// <summary>
        /// 0 = success, 1 = Users Exist for Role, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }




    [DataContract]
    public class FindUserNameRequest
    {
        [DataMember]
        public string UserName { get; set; }
    }

    [DataContract]
    public class FindUserNameResponse
    {
        /// <summary>
        /// 0 = success, 1 = Username doesn't exist, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class WebsiteSaveTemplateFunctionsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int Template_Id { get; set; }

        [DataMember]
        public string TemplateFunctionsJson { get; set; }
        
        [DataMember]
        public List<Dictionary<string,string>> ListTemplateFunctionsJson { get; set; }

    }

    [DataContract]
    public class WebsiteSaveTemplateFunctionsResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

    }


    [DataContract]
    public class WebsiteLoadTemplateFunctionsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int Template_Id { get; set; }

    }

    [DataContract]
    public class WebsiteLoadTemplateFunctionsResponse
    {
        [DataMember]
        public string TemplateFunctionsJson { get; set; }

        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class SetClaimReadStatusResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public int CurrentReadStatus { get; set; }
    }

    [DataContract]
    public class SetClaimReadStatusRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }

        [DataMember]
        public int RiskClaimReadStatus { get; set; }


    }

    [DataContract]
    public class GetBatchStatusResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public ExRiskBatch RiskBatch { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class GetBatchStatusRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string ClientBatchReference { get; set; }
    }



    [DataContract]
    public class GetBatchProcessingResultsResponse
    {
        [DataMember]
        public MessageNode ProcessingResults { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class GetBatchProcessingResultsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskBatchId { get; set; }

    }

    [DataContract]
    public class IsClientBatchReferenceUniqueResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public bool IsUnique { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class IsPasswordUniqueResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public bool IsUnique { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class IsClientNameUniqueResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public bool IsUnique { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class IsUserNameUniqueResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public bool IsUnique { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class IsClientBatchReferenceUniqueRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string ClientBatchReference { get; set; }

    }

    [DataContract]
    public class IsPasswordUniqueRequest
    {
        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Username { get; set; }

    }       

    
    [DataContract]
    public class IsClientNameUniqueRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string ClientName { get; set; }

    }

    [DataContract]
    public class IsUserNameUniqueRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string UserName { get; set; }

    }

   

    [DataContract]
   

    public class ExEntityScoreMessageList
    {
        [DataMember]
        public string EntityHeader { get; set; }

        [DataMember]
        public List<string> Messages { get; set; }
    }

    [DataContract]
    public class ExRiskClaimEdit
    {
        [DataMember]
        public int RiskBatch_Id { get; set; }

        [DataMember]
        public string BatchReference { get; set; }

        [DataMember]
        public string ClientBatchReference { get; set; }


    }

    [DataContract]
    public class ExRiskClientBatchPriority
    {
      [DataMember]
      public string Client { get; set; }
      [DataMember]
      public int PriorityId { get; set; }
    }

    [DataContract]
   
    public class ExRiskClaimDetails
    {
        [DataMember]
        public int RiskBatch_Id { get; set; }

        [DataMember]
        public string BatchReference { get; set; }

        [DataMember]
        public string ClientBatchReference { get; set; }

        [DataMember]
        public int BatchStatus { get; set; }

        [DataMember]
        public string BatchStatusAsString { get; set; }

        [DataMember]
        public int RiskClaim_Id { get; set; }

        [DataMember]
        public int BaseRiskClaim_Id { get; set; }

        [DataMember]
        public string MDAClaimRef { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public string ClientClaimRefNumber { get; set; }

        [DataMember]
        public int LevelOneRequestedCount { get; set; }

        [DataMember]
        public int LevelTwoRequestedCount { get; set; }

        [DataMember]
        public DateTime? LevelOneRequestedWhen { get; set; }

        [DataMember]
        public DateTime? LevelTwoRequestedWhen { get; set; }

        [DataMember]
        public string LevelOneRequestedWho { get; set; }

        [DataMember]
        public string LevelTwoRequestedWho { get; set; }

        [DataMember]
        public string LevelOneRequestedStatusAsString { get; set; }

        [DataMember]
        public string LevelTwoRequestedStatusAsString { get; set; }

        [DataMember]
        public int ClaimStatus { get; set; }

        [DataMember]
        public string ClaimStatusAsString { get; set; }

        [DataMember]
        public int ClaimReadStatus { get; set; }

        [DataMember]
        public MessageNode ValidationResults { get; set; }

        [DataMember]
        public MessageNode CleansingResults { get; set; }

        [DataMember]
        public int Incident_Id { get; set; }

        [DataMember]
        public DateTime IncidentDate { get; set; }

        [DataMember]
        public decimal? Reserve { get; set; }

        [DataMember]
        public decimal? PaymentsToDate { get; set; }

        [DataMember]
        public int RiskClaimRun_Id { get; set; }

        [DataMember]
        public int TotalScore { get; set; }

        [DataMember]
        public int TotalKeyAttractorCount { get; set; }

        [DataMember]
        public string RiskRatingAsString { get; set; }

        [DataMember]
        public bool LatestVersion { get; set; }

        [DataMember]
        public bool LatestScored { get; set; }

        [DataMember]
        public bool NoteAdded { get; set; }

        [DataMember]
        public bool IntelAdded { get; set; }

        [DataMember]
        public bool DecisionAdded { get; set; }

        [DataMember]
        public int? DecisionId { get; set; }

        [DataMember]
        public int? ReserveChanged { get; set; }

        [DataMember]
        public List<ExEntityScoreMessageList> MessageList { get; set; }
    }

    [DataContract]
    public class ExRiskBatch
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RiskClient_Id { get; set; }

        [DataMember]
        public string BatchReference { get; set; }

        [DataMember]
        public string ClientBatchReference { get; set; }

        [DataMember]
        public int BatchStatus { get; set; }

        [DataMember]
        public string BatchStatusAsString { get; set; }

        [DataMember]
        public System.DateTime CreatedDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        //[DataMember]
        //public MessageNode VerificationResults { get; set; }
        //[DataMember]
        //public MessageNode MappingResults { get; set; }
    }

    #region File Editor Messages

    [DataContract]
    public class ExRiskClaim
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RiskBatch_Id { get; set; }

        [DataMember]
        public int Incident_Id { get; set; }

        [DataMember]
        public string MdaClaimRef { get; set; }

        [DataMember]
        public int BaseRiskClaim_Id { get; set; }

        [DataMember]
        public int ClaimStatus { get; set; }

        [DataMember]
        public int ClaimReadStatus { get; set; }

        [DataMember]
        public string ClientClaimRefNumber { get; set; }

        [DataMember]
        public int LevelOneRequestedCount { get; set; }

        [DataMember]
        public int LevelTwoRequestedCount { get; set; }

        [DataMember]
        public Nullable<System.DateTime> LevelOneRequestedWhen { get; set; }

        [DataMember]
        public Nullable<System.DateTime> LevelTwoRequestedWhen { get; set; }

        [DataMember]
        public string LevelOneRequestedWho { get; set; }

        [DataMember]
        public string LevelTwoRequestedWho { get; set; }

        [DataMember]
        public string SourceReference { get; set; }

        //public string SourceEntityXML { get; set; }
        [DataMember]
        public System.DateTime CreatedDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public int TotalScore { get; set; }

        [DataMember]
        public MessageNode ValidationResults { get; set; }

        [DataMember]
        public MessageNode CleansingResults { get; set; }
    }

    [DataContract]
    public class ExRiskClaimRun
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RiskClaim_Id { get; set; }

        [DataMember]
        public string EntityType { get; set; }

        //public string ScoreEntityJson { get; set; }
        //public string ScoreEntityXml { get; set; }
        [DataMember]
        public int TotalScore { get; set; }

        [DataMember]
        public int TotalKeyAttractorCount { get; set; }

        //public string ReportOneVersion { get; set; }
        //public string ReportTwoVersion { get; set; }
        [DataMember]
        public string ExecutedBy { get; set; }

        [DataMember]
        public System.DateTime ExecutedWhen { get; set; }

        [DataMember]
        public List<ExEntityScoreMessageList> MessageList { get; set; }
    }

    [DataContract]
    public class ExRiskUserLocked
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Forename { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Template { get; set; }
    }


    [DataContract]
    public class ExRiskClient
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public int AmberThreshold { get; set; }

        [DataMember]
        public int RedThreshold { get; set; }

        [DataMember]
        public int LevelOneReportDelay { get; set; }

        [DataMember]
        public int LevelTwoReportDelay { get; set; }

        [DataMember]
        public string ExpectedFileExtension { get; set; }

        [DataMember]
        public string InsurerName { get; set; }

        [DataMember]
        public int InsurersClientsId { get; set; }

        [DataMember]
        public int? Status { get; set; }

        [DataMember]
        public bool? RecordReserveChanges { get; set; }

        [DataMember]
        public int? BatchPriority { get; set; }
    }

    [DataContract]
    public class ExRiskWord
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RiskClient_Id { get; set; }

        [DataMember]
        public string TableName { get; set; }

        [DataMember]
        public string FieldName { get; set; }

        [DataMember]
        public string LookupWord { get; set; }

        [DataMember]
        public string ReplacementWord { get; set; }

        [DataMember]
        public bool ReplaceWholeString { get; set; }

        [DataMember]
        public System.DateTime DateAdded { get; set; }

        [DataMember]
        public string SearchType { get; set; }

    }

    [DataContract]
    public class ExRiskNote
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int BaseRiskClaim_Id  { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string CreatedBy  { get; set; }

        [DataMember]
        public DateTime CreatedDate  { get; set; }

        [DataMember]
        public string Decision { get; set; }

        [DataMember]
        public int? DecisionId  { get; set; }

        [DataMember]
        public bool Deleted  { get; set; }

        [DataMember]
        public string NoteDesc  { get; set; }

        [DataMember]
        public int RiskClaim_Id  { get; set; }

        [DataMember]
        public int UserId  { get; set; }

        [DataMember]
        public int? Visibilty { get; set; }

        [DataMember]
        public DateTime? UpdatedDate { get; set; }

        [DataMember]
        public string UpdatedBy { get; set; }

        [DataMember]
        public byte[] OriginalFile { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public string FileType { get; set; }

    }

    [DataContract]
    public class ExRiskNoteDecision
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Decision { get; set; }

        [DataMember]
        public int RiskClient_Id { get; set; }

        [DataMember]
        public int ADARecordStatus { get; set; }

    }


    [DataContract]
    public class ExRiskDefaultData
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RiskClient_Id { get; set; }

        [DataMember]
        public string ConfigurationValue { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public string RiskConfigurationDescription { get; set; }

    }

    [DataContract]
    public class ExRiskRole
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string RoleName { get; set; }
    }

    [DataContract]
    public class ExInsurersClients
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string ClientName { get; set; }
    }

    [DataContract]
    public class ExRiskTeam
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string TeamName { get; set; }
    }


    [DataContract]
    public class ExRiskUser
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string TelephoneNumber { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public DateTime? PasswordResetDate { get; set; }

        [DataMember]
        public int RiskClientId { get; set; }

        [DataMember]
        public string RiskClientName { get; set; }

        [DataMember]
        public int RiskTeamId { get; set; }

        [DataMember]
        public string RiskTeamName { get; set; }

        [DataMember]
        public int RiskRoleId { get; set; }

        [DataMember]
        public string RiskRoleName { get; set; }

        [DataMember]
        public int? Status { get; set; }

        [DataMember]
        public bool? AutoLoader { get; set; }

        [DataMember]
        public Dictionary<string, string> TemplateFunctions { get; set; }

        [DataMember]
        public int? UserTypeId { get; set; }


    }

    #region BatchList - File Editor

    [DataContract]
    public class ExBatchDetails
    {

        [DataMember]
        public int BatchId { get; set; }

        [DataMember]
        public string Reference { get; set; }

        [DataMember]
        public System.DateTime UploadedDate { get; set; }

        [DataMember]
        public string UploadedBy { get; set; }

        [DataMember]
        public int BatchStatusId { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string BatchStatusValue { get; set; }

        [DataMember]
        public string Description { get; set; }

        // | delimited string
        [DataMember]
        public string Rejections { get; set; }

    }

    [DataContract]
    public class BatchListRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int StatusFilter { get; set; }
    }

    [DataContract]
    public class BatchListResponse
    {
        [DataMember]
        public List<ExBatchDetails> BatchList { get; set; }

        [DataMember]
        public string Error { get; set; }
    }

    #endregion

    #region ClaimsInBatch - File Editor

    [DataContract]
    public class ClaimsInBatchRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int BatchId { get; set; }
    }

    [DataContract]
    public class ClaimsInBatchResponse
    {
        [DataMember]
        public List<ExRiskClaim> ClaimsList { get; set; }

        [DataMember]
        public string Error { get; set; }
    }

    #endregion

    #region GetClaimScoreData  -  File Editor

    [DataContract]
    public class GetClaimScoreDataResponse
    {
        [DataMember]
        public ExRiskClaimRun RiskClaimRun { get; set; }

        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public MessageNode ScoreNodes { get; set; }
    }

    [DataContract]
    public class GetClaimScoreDataRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }


    }

    #endregion


    #endregion

    #region Website

    [DataContract]
    public class GetRiskUserInfoRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class GetRiskUserInfoResponse
    {
        [DataMember]
        public ExRiskUser UserInfo { get; set; }
    }

    

    [DataContract]
    public class RiskUserRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }

    

    [DataContract]
    public class RiskUsersResponse
    {
        [DataMember]
        public List<ExRiskUser> RiskUserList { get; set; }
    }

    [DataContract]
    public class RiskUsersLockedRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class RiskUsersLockedResponse
    {
        [DataMember]
        public List<ExRiskUserLocked> LockedUserList { get; set; }
        [DataMember]
        public int Result { get; set; }
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class RiskUserUnlockRequest{
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
     
        [DataMember]
        public int UnlockUserId { get; set; }
    }

    [DataContract]
    public class RiskUserUnlockResponse
    {
        [DataMember]
        public int Result { get; set; }
        [DataMember]
        public string Error { get; set; }
    }


    [DataContract]
    public class RiskClientsResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskClient> ClientList { get; set; }
    }

    [DataContract]
    public class RiskWordsResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskWord> WordList { get; set; }
    }

    [DataContract]
    public class RiskNotesResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskNote> NoteList { get; set; }
    }

    [DataContract]
    public class GetRiskNoteResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public ExRiskNote RiskNote { get; set; }
    }

    [DataContract]
    public class RiskNoteDecisionsResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskNoteDecision> DecisionList { get; set; }
    }

    [DataContract]
    public class RiskWordDeleteResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

    }

    [DataContract]
    public class RiskNoteCreateResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

    }

    [DataContract]
    public class RiskNoteUpdateResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

    }

    [DataContract]
    public class RiskDefaultDataResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskDefaultData> DefaultDataList { get; set; }
    }


    [DataContract]
    public class GetAssignedRiskClientsForUserRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterUserId { get; set; }

    }

    [DataContract]
    public class GetAssignedRiskClientsForUserResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskClient> ClientList { get; set; }
    }

    [DataContract]
    public class RiskRolesResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExRiskRole> RoleList { get; set; }
    }

    //InsurersClientsRequest
    [DataContract]
    public class InsurersClientsResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Null if Success otherwise an error message
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public List<ExInsurersClients> ClientList { get; set; }
    }

    [DataContract]
    public class RiskClientsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterClientId { get; set; }

    }

    [DataContract]
    public class RiskWordsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterClientId { get; set; }

    }

    [DataContract]
    public class RiskNotesRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterRiskClaimId { get; set; }

    }

    [DataContract]
    public class GetRiskNoteRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterRiskNoteId { get; set; }

    }

    [DataContract]
    public class RiskNoteDecisionsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int? FilterRiskClientId { get; set; }

    }

    [DataContract]
    public class RiskWordDeleteRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskWordId { get; set; }

    }

    [DataContract]
    public class RiskNoteCreateRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string NoteDesc { get; set; }

        [DataMember]
        public int[] RiskClaim_Id { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public int? Decision_Id { get; set; }

        [DataMember]
        public int? Visibilty { get; set; }

        [DataMember]
        public bool Deleted { get; set; }

        [DataMember]
        public int? UserTypeId { get; set; }

        [DataMember]
        public byte[] OriginalFile { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public string FileType { get; set; }

    }

    [DataContract]
    public class RiskNoteUpdateRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskNoteId { get; set; }

        [DataMember]
        public string NoteDesc { get; set; }

        [DataMember]
        public int? Decision_Id { get; set; }

        [DataMember]
        public int? Visibilty { get; set; }

        [DataMember]
        public bool Deleted { get; set; }

        [DataMember]
        public int? UserTypeId { get; set; }

        [DataMember]
        public DateTime UpdatedDate { get; set; }

        [DataMember]
        public string UpdatedBy { get; set; }

    }

    [DataContract]
    public class RiskDefaultDataRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterClientId { get; set; }

    }


    [DataContract]
    public class RiskRolesRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterClientId { get; set; }

    }

    [DataContract]
    public class InsurersClientsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int FilterClientId { get; set; }

    }

    [DataContract]
    public class WebsiteBatchListRequest
    {
        [DataMember]
        public string Who { get; set; }
        [DataMember]
        public int ClientId { get; set; }
        [DataMember]
        public int UserId { get; set; }
        /// <summary>
        /// The client ID. If set to -1 all batches for all clients returned
        /// </summary>
        [DataMember]
        public int FilterClientId { get; set; }

        /// <summary>
        /// The team ID. If set to -1 and client ID is specified all batches 
        /// for the specified client are returned.
        /// </summary>
        [DataMember]
        public int TeamId { get; set; }

        /// <summary>
        /// The user ID. If set to -1 and team id is specified, all batches for
        /// the specified team are returned.
        /// </summary>
        [DataMember]
        public int FilterUserId { get; set; }

        /// <summary>
        /// Can be one of "CLIENTBATCHREFERENCE", 
        ///  "UPLOADDATETIME"
        /// </summary>
        [DataMember]
        public string SortColumn { get; set; }

        /// <summary>
        /// True if the sort order is ASC otherwise false
        /// </summary>
        [DataMember]
        public bool SortAscending { get; set; }

        /// <summary>
        /// Page number required. Starts at 1
        /// </summary>
        [DataMember]
        public int Page { get; set; }

        /// <summary>
        /// Number of rows in a page
        /// </summary>
        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public int StatusFilter { get; set; }
    }

    [DataContract]
    public class WebsiteBatchListResponse
    {
        public WebsiteBatchListResponse()
        {
            BatchList = new List<ExWebsiteBatchDetails>();
        }

        [DataMember]
        public List<ExWebsiteBatchDetails> BatchList { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        /// <summary>
        /// Total rows in ResultSet (not the number of rows in the page)
        /// </summary>
        [DataMember]
        public int TotalRows { get; set; }
    }

    public class ExWebsiteBatchDetails
    {
        [DataMember]
        public int BatchId { get; set; }

        [DataMember]
        public int RiskClientId { get; set; }

        [DataMember]
        public string BatchReference { get; set; }

        [DataMember]
        public string BatchEntityType { get; set; }

        [DataMember]
        public string ClientBatchReference { get; set; }

        [DataMember]
        public string BatchStatusAsString { get; set; }

        [DataMember]
        public int BatchStatusId { get; set; }

        [DataMember]
        public System.DateTime UploadedDate { get; set; }

        [DataMember]
        public string UploadedBy { get; set; }

        [DataMember]
        public string VerificationResults { get; set; }

        [DataMember]
        public string MappingResults { get; set; }

        [DataMember]
        public int TotalNumberOfClaims { get; set; }

    }


    [DataContract]
    public class WebsiteGetTotalSingleClaimsForClientRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class WebsiteGetTotalSingleClaimsForClientResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public int TotalRows { get; set; }
    }

    [DataContract]

    public class WebsiteSaveSingleClaimForUserRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public PipelineMotorClaim ClaimToSave { get; set; }

        [DataMember]
        public int? EditClaimId { get; set; }

    }

    [DataContract]
    public class WebsiteSaveSingleClaimForUserResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class WebsiteDeleteSingleClaimRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int EditClaimId { get; set; }
    }

    [DataContract]
    public class WebsiteDeleteSingleClaimResponse
    {
        [DataMember]
        public int Result { get; set; }

        [DataMember]
        public string Error { get; set; }
    }


    [DataContract]
    public class WebsiteFetchSingleClaimRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int EditClaimId { get; set; }
    }

    [DataContract]
    public class WebsiteFetchSingleClaimResponse
    {
        [DataMember]
        public ExWebsiteSingleClaimDetail SingleClaimDetail { get; set; }

        [DataMember]
        public int Result { get; set; }

        [DataMember]
        public string Error { get; set; }

    }

    [DataContract]
    public class ExWebsiteSingleClaimDetail
    {
        [DataMember]
        public int RiskClaim_Id { get; set; }

        [DataMember]
        public MDA.Common.FileModel.MotorClaim Claim { get; set; }
    }

    #region SingleClaimsSummaryList

    [DataContract]
    public class WebsiteSingleClaimsListRequest
    {
        [DataMember]
        public string Who { get; set; }

        /// <summary>
        /// The client ID. If set to -1 all batches for all clients returned
        /// </summary>
        [DataMember]
        public int ClientId { get; set; }

        /// <summary>
        /// The team ID. If set to -1 and client ID is specified all batches 
        /// for the specified client are returned.
        /// </summary>
        [DataMember]
        public int TeamId { get; set; }

        /// <summary>
        /// The user ID. If set to -1 and team id is specified, all batches for
        /// the specified team are returned.
        /// </summary>
        [DataMember]
        public int UserId { get; set; }

        /// <summary>
        /// Can be one of "CLIENTBATCHREFERENCE", 
        ///  "UPLOADDATETIME"
        /// </summary>
        [DataMember]
        public string SortColumn { get; set; }

        /// <summary>
        /// True if the sort order is ASC otherwise false
        /// </summary>
        [DataMember]
        public bool SortAscending { get; set; }

        /// <summary>
        /// Page number required. Starts at 1
        /// </summary>
        [DataMember]
        public int Page { get; set; }

        /// <summary>
        /// Number of rows in a page
        /// </summary>
        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public int StatusFilter { get; set; }
    }


    [DataContract]
    public class WebsiteSingleClaimsListResponse
    {
        public WebsiteSingleClaimsListResponse()
        {
            WebsiteSingleClaimsList = new List<ExWebsiteSingleClaim>();
        }

        [DataMember]
        public List<ExWebsiteSingleClaim> WebsiteSingleClaimsList { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        /// <summary>
        /// Total rows in ResultSet (not the number of rows in the page)
        /// </summary>
        [DataMember]
        public int TotalRows { get; set; }
    }


    [DataContract]
    public class ExWebsiteSingleClaim
    {
        [DataMember]
        public int RiskClaim_Id { get; set; }

        [DataMember]
        public string ClaimType { get; set; }

        [DataMember]
        public string ClaimNumber { get; set; }

        [DataMember]
        public DateTime IncidentDate { get; set; }

        [DataMember]
        public string SavedBy { get; set; }

        [DataMember]
        public DateTime DateSaved { get; set; }
    }

    #endregion

    #region CUE
    [DataContract]
    public class WebsiteCueSearchRequest
    {
        public WebsiteCueSearchRequest()
        {
            ListPerson_Id = new List<int>();
        }
        
        [DataMember]
        public int RiskClaim_Id { get; set; }
        [DataMember]
        public List<int> ListPerson_Id { get; set; }

        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int ClientId { get; set; }
        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class WebsiteCueSearchResponse
    {
        [DataMember]
        public int Result { get; set; }

        [DataMember]
        public string Error { get; set; }

       
    }

    [DataContract]
    public class WebsiteCueInvolvementsRequest
    {
        [DataMember]
        public int RiskClaim_Id { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int ClientId { get; set; }
        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class WebsiteCueInvolvementsResponse
    {
        public WebsiteCueInvolvementsResponse()
	    {
            CueInvolvements = new List<ExCueInvolvement>();
	    }
        [DataMember]
        public int Result { get; set; }
        [DataMember]
        public string Error { get; set; }
        [DataMember]
        public string ClaimNumber { get; set; }
        [DataMember]
        public DateTime IncidentDate { get; set; }
        [DataMember]
        public DateTime UploadedDate { get; set; }
        [DataMember]
        public List<ExCueInvolvement> CueInvolvements { get; set; }
    }

    #region WebsiteCueReport
    [DataContract]
    public class WebsiteCueReportRequest
    {
        [DataMember]
        public int RiskClaim_Id { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int ClientId { get; set; }
        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class WebsiteCueReportResponse
    {
        [DataMember]
        public ExCueReportDetails CueReportDetails { get; set; }
        [DataMember]
        public int Result { get; set; }

        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class ExCueReportDetails
    {

        [DataMember]
        public string ClaimNumber { get; set; }
        [DataMember]
        public DateTime SearchDate { get; set; }

        [DataMember]
        public List<Dictionary<string, ExCueReportResult>> ListResultInfo { get; set; }
    }

    [DataContract]
    public class ExCueReportResult
    {
      
        [DataMember]
        public List<ExCUESearchSummary> SearchSummary { get; set; }
        [DataMember]
        public List<ExCUEResultsSummary> ResultsSummary { get; set; }
        [DataMember]
        public ExCueSearchCriteria SearchCriteria { get; set; }
        [DataMember]
        public List<ExCUEIncidentSummary> IncidentSummary { get; set; }
        [DataMember]
        public List<ExCUEIncident> CUE_Incidents { get; set; }
    }

    [DataContract]
    public class ExCUEIncident
    {
        [DataMember]
        public int IncidentId { get; set; }
        [DataMember]
        public string IncidentType { get; set; }
        [DataMember]
        public ExCUEClaimData ClaimData { get; set; }
        [DataMember]
        public ExCUEGeneralData GeneralData { get; set; }
        [DataMember]
        public List<ExCUEIncidentInvolvement> CUEIncidentInvolvements { get; set; }
        [DataMember]
        public List<ExCUEIncidentInvolvementVehicleDetails> CUEIncidentInvolvementVehicleDetails { get; set; }
        [DataMember]
        public List<ExCUESupplier> Suppliers { get; set; }
        [DataMember]
        public ExCUEIncidentDetails IncidentDetails { get; set; }
    }

    [DataContract]
    public class ExCUEIncidentDetails
    {
        [DataMember]
        public string InjuryDescription { get; set; }
        [DataMember]
        public string HospitalAttended { get; set; }
        [DataMember]
        public string IllnessDiseaseDescription { get; set; }
    }

    [DataContract]
    public class ExCUESupplier
    {
        [DataMember]
        public int IncidentId { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string SupplierStatus { get; set; }
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public string AddressHouseName { get; set; }
        [DataMember]
        public string AddressNumber { get; set; }
        [DataMember]
        public string AddressStreet { get; set; }
        [DataMember]
        public string AddressLocality { get; set; }
        [DataMember]
        public string AddressTown { get; set; }
        [DataMember]
        public string AddressCity { get; set; }
        [DataMember]
        public string AddressPostCode { get; set; }
        [DataMember]
        public string AddressIndicator { get; set; }
        [DataMember]
        public int VAT { get; set; }
        [DataMember]
        public decimal PaymentAmount { get; set; }
    }



    [DataContract]
    public class ExCUEIncidentInvolvementVehicleDetails : ExCUEIncidentInvolvement
    {
        [DataMember]
        public string VehicleRegistration { get; set; }
        [DataMember]
        public string VehicleIdentificationNumber { get; set; }
        [DataMember]
        public string Make { get; set; }
        [DataMember]
        public string Model { get; set; }
        [DataMember]
        public string Colour { get; set; }
        [DataMember]
        public string CoverType { get; set; }
        [DataMember]
        public string VIN { get; set; }
        [DataMember]
        public string DamageStatus { get; set; }
        [DataMember]
        public string RecoveredStatus { get; set; }
        [DataMember]
        public string RegistrationStatus { get; set; }
        [DataMember]
        public string NewForOldIndicator { get; set; }
    }


    [DataContract]
    public class ExCUEIncidentInvolvement
    {
        [DataMember]
        public int Incident_Id { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Forename { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string NI_Number { get; set; }
        [DataMember]
        public string AddressHouseName { get; set; }
        [DataMember]
        public string AddressNumber { get; set; }
        [DataMember]
        public string AddressStreet { get; set; }
        [DataMember]
        public string AddressLocality { get; set; }
        [DataMember]
        public string AddressTown { get; set; }
        [DataMember]
        public string AddressCity { get; set; }
        [DataMember]
        public string AddressPostCode { get; set; }
        [DataMember]
        public string AddressIndicator { get; set; }
        [DataMember]
        public string Sex { get; set; }
        [DataMember]
        public DateTime Dob { get; set; }
        [DataMember]
        public int VAT { get; set; }
        [DataMember]
        public decimal PaymentAmount { get; set; }
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public string Occupation { get; set; }
    }


    [DataContract]
    public class ExCUEGeneralData
    {
        [DataMember]
        public string ClaimStatus { get; set; }
        [DataMember]
        public string IncidentDescription { get; set; }
        [DataMember]
        public string PolicyType { get; set; }
        [DataMember]
        public string PolicyNumber { get; set; }
        [DataMember]
        public string PolicyNumberID { get; set; }
        [DataMember]
        public string CollectivePolicyIndicator { get; set; }
        [DataMember]
        public DateTime LossSetupDate { get; set; }
        [DataMember]
        public string InsurerContactName { get; set; }
        [DataMember]
        public string InsurerContactTel { get; set; }
        [DataMember]
        public DateTime IncidentDate { get; set; }
        [DataMember]
        public DateTime PolicyInceptionDate { get; set; }
        [DataMember]
        public DateTime PolicyPeriodEndDate { get; set; }
        [DataMember]
        public string CauseOfLoss { get; set; }
        [DataMember]
        public DateTime ClosedDate { get; set; }
        [DataMember]
        public string CatastropheRelated { get; set; }
        [DataMember]
        public string RiskAddressHouseName { get; set; }
        [DataMember]
        public string RiskAddressNumber { get; set; }
        [DataMember]
        public string RiskAddressStreet { get; set; }
        [DataMember]
        public string RiskAddressLocality { get; set; }
        [DataMember]
        public string RiskAddressTown { get; set; }
        [DataMember]
        public string RiskAddressCity { get; set; }
        [DataMember]
        public string RiskAddressPostCode { get; set; }
        [DataMember]
        public List<ExCUEPayment> CUEPayments { get; set; }
    }

    [DataContract]
    public class ExCUEPayment
    {
        [DataMember]
        public int IncidentId { get; set; }
        [DataMember]
        public string PaymentType { get; set; }
        [DataMember]
        public decimal PaymentAmount { get; set; }
    }


    [DataContract]
    public class ExCUEClaimData
    {
        [DataMember]
        public string Insurer { get; set; }
        [DataMember]
        public string ClaimNumber { get; set; }
        [DataMember]
        public List<ExCUEClaimDataPolicyHolder> PolicyHolders { get; set; }

    }

    [DataContract]
    public class ExCUEClaimDataPolicyHolder
    {
        [DataMember]
        public int IncidentId { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Address { get; set; }
    }


    [DataContract]
    public class ExCUEIncidentSummary
    {
        [DataMember]
        public int IncidentId { get; set; }
        [DataMember]
        public DateTime From { get; set; }
        [DataMember]
        public DateTime To { get; set; }
        [DataMember]
        public int PI_Claims { get; set; }
        [DataMember]
        public int MO_Claims { get; set; }
        [DataMember]
        public int HH_Claims { get; set; }
        [DataMember]
        public List<ExCUESummaryIncident> CUE_Incidents { get; set; }
    }


    [DataContract]
    public class ExCUESummaryIncident
    {
        [DataMember]
        public int IncidentId { get; set; }
        [DataMember]
        public string IncidentType { get; set; }
        [DataMember]
        public string Subject { get; set; }
        [DataMember]
        public string InvolvementType { get; set; }
        [DataMember]
        public DateTime IncidentDate { get; set; }
        [DataMember]
        public string ClaimStatus { get; set; }
        [DataMember]
        public string Insurer { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string MatchCode { get; set; }
    }


    [DataContract]
    public class ExCueSearchCriteria
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Dob { get; set; }
        [DataMember]
        public string NI_Number { get; set; }
        [DataMember]
        public string DrivingLicenceNum { get; set; }
        [DataMember]
        public string VIN { get; set; }
        [DataMember]
        public string VehicleReg { get; set; }
        [DataMember]
        public string AddressHouseName { get; set; }
        [DataMember]
        public string AddressNumber { get; set; }
        [DataMember]
        public string AddressStreet { get; set; }
        [DataMember]
        public string AddressLocality { get; set; }
        [DataMember]
        public string AddressTown { get; set; }
        [DataMember]
        public string AddressCity { get; set; }
        [DataMember]
        public string AddressPostCode { get; set; }
    }


    [DataContract]
    public class ExCUEResultsSummary
    {
        [DataMember]
        public string Incidents { get; set; }
        [DataMember]
        public string Involved_PI_Claims { get; set; }
        [DataMember]
        public string Involved_Motor_Claims { get; set; }
        [DataMember]
        public string Involved_Household_Claims { get; set; }
    }


    [DataContract]
    public class ExCUESearchSummary
    {
        [DataMember]
        public string Subject { get; set; }
        [DataMember]
        public string Involvement { get; set; }
        [DataMember]
        public bool Motor { get; set; }
        [DataMember]
        public bool PI { get; set; }
        [DataMember]
        public bool Household { get; set; }
        [DataMember]
        public bool Cross_Enquiry { get; set; }
        [DataMember]
        public DateTime SearchDate { get; set; }
    }

    #endregion

    [DataContract]
    public class ExCueInvolvement
    {
        [DataMember]
        public ExCuePerson CuePerson { get; set; }
        [DataMember]
        public ExCueAddress CueAddress { get; set; }
        [DataMember]
        public ExCueVehicle CueVehicle { get; set; }
        [DataMember]
        public bool CueDatabaseAll { get; set; }
    }

    [DataContract]
    public class ExCuePerson
    {
        [DataMember]
        public int PersonId { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Involvement { get; set; }
        [DataMember]
        public DateTime Dob { get; set; }
        [DataMember]
        public string NI_Number { get; set; }
        [DataMember]
        public string DrivingLicenceNumber { get; set; }
    }

    [DataContract]
    public class ExCueAddress
    {
        [DataMember]
        public string HouseName { get; set; }
        [DataMember]
        public string Number { get; set; }
        [DataMember]
        public string Street { get; set; }
        [DataMember]
        public string Locality { get; set; }
        [DataMember]
        public string Town { get; set; }
        [DataMember]
        public string PostCode { get; set; }
    }

    [DataContract]
    public class ExCueVehicle
    {
        [DataMember]
        public string VehicleReg { get; set; }
        [DataMember]
        public string VIN { get; set; }
    }


    #endregion

    #region SaveSingleClaimForUser

    [DataContract]
    [KnownType(typeof(MDA.Common.FileModel.MotorClaim))]
    public class WebsiteSaveSingleClaimRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        /// <summary>
        /// if null this a new single claim else existing
        /// </summary>
        [DataMember]
        public int? EditClaimId { get; set; }

        [DataMember]
        public string RiskClaimNumber { get; set; }

        [DataMember]
        public string ClaimType { get; set; }

        [DataMember]
        public DateTime IncidentDate { get; set; }

        [DataMember]
        public DateTime ModifiedDate { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string ModifiedBy { get; set; }

        [DataMember]
        public MDA.Common.FileModel.IClaim ClaimToSave { get; set; }
    }

    [DataContract]
    public class WebsiteSaveSingleClaimResponse
    {
        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public int EditClaimId { get; set; }

    }

    [DataContract]
    public class WebsiteFilterBatchNumbersRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string Role { get; set; }

        [DataMember]
        public int? SelectedClientId { get; set; }
    }

    [DataContract]
    public class WebsiteFilterBatchNumbersResponse
    {
        public WebsiteFilterBatchNumbersResponse()
        {
            //BatchNumbersList = new List<ExBatchNumbers>();
        }

        [DataMember]
        public List<string> BatchNumbersList { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        [DataMember]
        public string Error { get; set; }


    }

    [DataContract]
    public class ExBatchNumbers
    {
        [DataMember]
        public string BatchNumber { get; set; }
    }

    #endregion


    #endregion

    [DataContract]
    public class GenerateAnyBatchReportsOutstandingRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class GetPasswordFailuresSinceLastSuccessRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string UserName { get; set; }
    }

    [DataContract]
    public class IsUserInRoleRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string RoleName { get; set; }
    }

    [DataContract]
    public class GetRolesForUserRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string UserName { get; set; }
    }

    [DataContract]
    public class ValidateUserRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string IPAddress { get; set; }

    }

    [DataContract]
    public class GeneratePasswordResetTokenRequest
    {
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public string ResetLink { get; set; }

        [DataMember]
        public DateTime TokenExpiration { get; set; }

    }

    [DataContract]
    public class GetListOfBatchPriorityClientsRequest
    {
        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }


    public class GetListOfBatchPriorityClientsResponse
    {
        [DataMember]
        public int Result { get; set; }
        [DataMember]
        public string Error { get; set; }
        [DataMember]
        public List<ExRiskClientBatchPriority> ListRiskClientBatchPriority { get; set; }
    }



    [DataContract]
    public class SendElmahErrorResponse
    {
        [DataMember]
        public int Result { get; set; }

        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class SendElmahErrorRequest
    {
        [DataMember]
        public string Error { get; set; }
    }


    [DataContract]
    public class ValidatePasswordResetTokenRequest
    {
        [DataMember]
        public string Token { get; set; }
    }

    [DataContract]
    public class GeneratePasswordResetTokenResponse
    {
        [DataMember]
        public int Result { get; set; }
        [DataMember]
        public string Error { get; set; }
        [DataMember]
        public string Token { get; set; }
    }

    [DataContract]
    public class ValidatePasswordResetTokenResponse
    {
        [DataMember]
        public int Result { get; set; }
        [DataMember]
        public string Error { get; set; }
        [DataMember]
        public string UserName { get; set; }
    }

    [DataContract]
    public class CreateUserRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string TelephoneNumber { get; set; }

        [DataMember]
        public int RoleId { get; set; }

        [DataMember]
        public int TeamId { get; set; }

        [DataMember]
        public int? Status { get; set; }

        [DataMember]
        public bool? Autoloader { get; set; }

        [DataMember]
        public int? UserTypeId { get; set; }
    }


    [DataContract]
    public class CreateClientRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string ClientName { get; set; }
        
        [DataMember]
        public int? StatusId { get; set; }

        [DataMember]
        public string FileExtensions { get; set; }

        [DataMember]
        public int AmberThreshold { get; set; }

        [DataMember]
        public int RedThreshold { get; set; }

        [DataMember]
        public int InsurersClientsId { get; set; }

    }

    [DataContract]
    public class CreateRiskWordRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClient_Id { get; set; }

        [DataMember]
        public string TableName { get; set; }

        [DataMember]
        public string FieldName { get; set; }

        [DataMember]
        public string LookupWord { get; set; }

        [DataMember]
        public string ReplacementWord { get; set; }

        [DataMember]
        public bool ReplaceWholeString { get; set; }

        [DataMember]
        public System.DateTime DateAdded { get; set; }

        [DataMember]
        public string SearchType { get; set; }

    }

    [DataContract]
    public class CreateRiskDefaultDataRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClient_Id { get; set; }

        [DataMember]
        public string ConfigurationValue { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public int RiskConfigurationDescriptionId { get; set; }

    }

    [DataContract]
    public class CreateRiskNoteDecisionRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }



        [DataMember]
        public int RiskClient_Id { get; set; }

        [DataMember]
        public string Decision { get; set; }

        [DataMember]
        public byte IsActive { get; set; }

    }

    [DataContract]
    public class DeleteRiskDefaultDataRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int Id { get; set; }
    }

    [DataContract]
    public class DeleteRiskNoteDecisionRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int Id { get; set; }
    }

    [DataContract]
    public class UpdateClientRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int? StatusId { get; set; }

        [DataMember]
        public string FileExtensions { get; set; }

        [DataMember]
        public int AmberThreshold { get; set; }

        [DataMember]
        public int RedThreshold { get; set; }

        [DataMember]
        public int InsurersClientsId { get; set; }

        [DataMember]
        public bool? RecordReserveChanges { get; set; }

        [DataMember]
        public int? BatchPriority { get; set; }

    }

    [DataContract]
    public class UpdateUserRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string TelephoneNumber { get; set; }

        [DataMember]
        public int RoleId { get; set; }

        [DataMember]
        public int TeamId { get; set; }

        [DataMember]
        public int? Status { get; set; }

        [DataMember]
        public bool? Autoloader { get; set; }

        [DataMember]
        public int? UserTypeId { get; set; }
    }

    [DataContract]
    public class CreateRiskUser2ClientRequest
    {

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int SelectedUserId { get; set; }

        [DataMember]
        public int SelectedClientId { get; set; }

    }

    [DataContract]
    public class DeleteRiskUser2ClientRequest
    {

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int SelectedUserId { get; set; }

        [DataMember]
        public int SelectedClientId { get; set; }

    }

    [DataContract]
    public class ChangePasswordRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string OldPassword { get; set; }

        [DataMember]
        public string NewPassword { get; set; }
    }

    [DataContract]
    public class GetUserIdRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public string UserName { get; set; }
    }

    [DataContract]
    public class CheckUserBelongsToClientRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class CheckUserHasAccessToReportRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int LoggedInUserId { get; set; }

        [DataMember]
        public int RiskBatchId { get; set; }
    }

    [DataContract]
    public class GenerateBatchReportRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskBatchId { get; set; }
    }

    [DataContract]
    public class GenerateLevel1ReportRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }
    }

    [DataContract]
    public class GenerateLevel2ReportRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }
    }
}
