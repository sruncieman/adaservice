﻿using MDA.WCF.WebServices.Messages;
using System.Collections.Generic;
using ADAServices.Interface;
using MDA.RiskService.Model;
using MDA.WCF.WebServices.Entities;

namespace MDA.WCF.WebServices.Translate
{
    public partial class Translator
    {
        public static ExEntityScoreMessageList Translate(EEntityScoreMessageList l)
        {
            return new ExEntityScoreMessageList()
            {
                EntityHeader = l.EntityHeader,
                Messages = l.Messages
            };
        }

        public static ExRiskClaimEdit Translate(ERiskClaimEdit rce)
        {
            ExRiskClaimEdit r = new ExRiskClaimEdit()
                {

                };



            return r;

        }

        //public static ExBatchNumbers Translate(EBatchNumbers esc)
        //{
        //    ExBatchNumbers e = new ExBatchNumbers()
        //    {

        //    };

        //    return e;
        //}

        public static ExWebsiteSingleClaimDetail Translate(EWebsiteSingleClaimsDetail esc)
        {
            ExWebsiteSingleClaimDetail e = new ExWebsiteSingleClaimDetail()
                {
                    Claim = esc.claim,
                    RiskClaim_Id = esc.RiskClaim_Id
                };

            return e;
        }

        public static ExRiskClaimDetails Translate(ERiskClaimDetails rcr)
        {
            ExRiskClaimDetails r = new ExRiskClaimDetails()
            {
                RiskBatch_Id = rcr.RiskBatch_Id,
                BatchReference = rcr.BatchReference,
                ClientBatchReference = rcr.ClientBatchReference,
                BatchStatus = rcr.BatchStatus,
                BatchStatusAsString = rcr.BatchStatusAsString,
                RiskClaim_Id = rcr.RiskClaim_Id,
                BaseRiskClaim_Id = rcr.BaseRiskClaim_Id,
                MDAClaimRef = rcr.MDAClaimRef,
                CreatedDate = rcr.CreatedDate,
                CreatedBy = rcr.CreatedBy,
                ClientClaimRefNumber = rcr.ClientClaimRefNumber,
                LevelOneRequestedCount = rcr.LevelOneRequestedCount,
                LevelTwoRequestedCount = rcr.LevelTwoRequestedCount,
                LevelOneRequestedWhen = rcr.LevelOneRequestedWhen,
                LevelTwoRequestedWhen = rcr.LevelTwoRequestedWhen,
                LevelOneRequestedWho = rcr.LevelOneRequestedWho,
                LevelTwoRequestedWho = rcr.LevelTwoRequestedWho,
                LevelOneRequestedStatusAsString = rcr.LevelOneRequestedStatusAsString,
                LevelTwoRequestedStatusAsString = rcr.LevelTwoRequestedStatusAsString,
                ClaimStatus = rcr.ClaimStatus,
                ClaimReadStatus = rcr.ClaimReadStatus,
                ClaimStatusAsString = rcr.ClaimStatusAsString,
                Incident_Id = rcr.Incident_Id,
                IncidentDate = rcr.IncidentDate,
                Reserve = rcr.Reserve,
                PaymentsToDate = rcr.PaymentsToDate,
                RiskClaimRun_Id = rcr.RiskClaimRun_Id,
                TotalScore = rcr.TotalScore,
                TotalKeyAttractorCount = rcr.TotalKeyAttractorCount,
                RiskRatingAsString = rcr.RiskRatingAsString,
                LatestScored = rcr.LatestScored,
                LatestVersion = rcr.LatestVersion,
                ValidationResults = rcr.ValidationResults,
                CleansingResults = rcr.CleansingResults,
                NoteAdded = rcr.NoteAdded,
                IntelAdded = rcr.IntelAdded,
                DecisionAdded = rcr.DecisionAdded,
                ReserveChanged = rcr.ReserveChanged,
                DecisionId = rcr.DecisionId

            };

            r.MessageList = new List<ExEntityScoreMessageList>();

            foreach (var i in rcr.MessageList)
                r.MessageList.Add(Translator.Translate(i));

            return r;
        }

        private static ExWebsiteSingleClaim Translate(EWebsiteSingleClaimsDetails websiteSingleClaimsDetails)
        {
            ExWebsiteSingleClaim r = new ExWebsiteSingleClaim()
                {
                    ClaimNumber = websiteSingleClaimsDetails.ClaimNumber,
                    ClaimType = websiteSingleClaimsDetails.ClaimType,
                    DateSaved = websiteSingleClaimsDetails.DateSaved,
                    IncidentDate = websiteSingleClaimsDetails.IncidentDate,
                    RiskClaim_Id = websiteSingleClaimsDetails.RiskClaim_Id,
                    SavedBy = websiteSingleClaimsDetails.SavedBy
                };

            return r;
        }

        public static ExRiskBatch Translate(ERiskBatch rc)
        {
            if (rc == null)
                return null;

            return new ExRiskBatch()
            {
                BatchReference = rc.BatchReference,
                BatchStatus = rc.BatchStatus,
                BatchStatusAsString = rc.BatchStatusAsString,
                ClientBatchReference = rc.ClientBatchReference,
                CreatedBy = rc.CreatedBy,
                CreatedDate = rc.CreatedDate,
                Id = rc.Id,
                RiskClient_Id = rc.RiskClient_Id,
                //MappingResults = rc.MappingResults,
                //VerificationResults = rc.VerificationResults
            };
        }

        public static ExRiskClaim Translate(ERiskClaim rc)
        {
            return new ExRiskClaim()
            {
                Id = rc.Id,
                RiskBatch_Id = rc.RiskBatch_Id,
                Incident_Id = rc.Incident_Id,
                MdaClaimRef = rc.MdaClaimRef,
                BaseRiskClaim_Id = rc.BaseRiskClaim_Id,
                ClaimStatus = rc.ClaimStatus,
                ClaimReadStatus = rc.ClaimReadStatus,
                ClientClaimRefNumber = rc.ClientClaimRefNumber,
                LevelOneRequestedCount = rc.LevelOneRequestedCount,
                LevelTwoRequestedCount = rc.LevelTwoRequestedCount,
                LevelOneRequestedWhen = rc.LevelOneRequestedWhen,
                LevelTwoRequestedWhen = rc.LevelTwoRequestedWhen,
                LevelOneRequestedWho = rc.LevelOneRequestedWho,
                LevelTwoRequestedWho = rc.LevelTwoRequestedWho,
                SourceReference = rc.SourceReference,
                CreatedDate = rc.CreatedDate,
                CreatedBy = rc.CreatedBy,
                TotalScore = rc.TotalScore,
                CleansingResults = rc.CleansingResults,
                ValidationResults = rc.ValidationResults,

            };
        }

        public static ExRiskClaimRun Translate(ERiskClaimRun rcr)
        {
            if (rcr == null) return null;

            return new ExRiskClaimRun()
            {
                Id = rcr.Id,
                RiskClaim_Id = rcr.RiskClaim_Id,
                TotalScore = rcr.TotalScore,
                TotalKeyAttractorCount = rcr.TotalKeyAttractorCount,
                ExecutedBy = rcr.ExecutedBy,
                ExecutedWhen = rcr.ExecutedWhen
            };
        }

        public static ExRiskClient Translate(ERiskClient erc)
        {
            return new ExRiskClient()
            {
                Id = erc.Id,
                ClientName = erc.ClientName,
                AmberThreshold = erc.AmberThreshold,
                RedThreshold = erc.RedThreshold,
                ExpectedFileExtension = erc.ExpectedFileExtension,
                LevelOneReportDelay = erc.LevelOneReportDelay,
                LevelTwoReportDelay = erc.LevelTwoReportDelay,
                InsurerName = erc.InsurerName,
                InsurersClientsId = erc.InsuersClientsId,
                Status = erc.Status,
                RecordReserveChanges = erc.RecordReserveChanges,
                BatchPriority = erc.BatchPriority
            };
        }

        public static ExRiskWord Translate(ERiskWord rw)
        {
            return new ExRiskWord()
            {
                Id = rw.Id,
                DateAdded = rw.DateAdded,
                FieldName = rw.FieldName,
                LookupWord = rw.LookupWord,
                ReplacementWord = rw.ReplacementWord,
                ReplaceWholeString = rw.ReplaceWholeString,
                RiskClient_Id = rw.RiskClient_Id,
                SearchType = rw.SearchType,
                TableName = rw.TableName,
            };
        }

        public static ExRiskNote Translate(ERiskNote rn)
        {
            return new ExRiskNote()
            {
                Id = rn.Id,
                BaseRiskClaim_Id = rn.BaseRiskClaim_Id,
                CreatedBy = rn.CreatedBy,
                CreatedDate = rn.CreatedDate,
                DecisionId = rn.DecisionId,
                Deleted = rn.Deleted,
                NoteDesc = rn.NoteDesc,
                RiskClaim_Id = rn.RiskClaim_Id,
                Visibilty = rn.Visibilty,
                UpdatedBy = rn.UpdatedBy,
                UpdatedDate = rn.UpdatedDate,
                Decision = rn.Decision,
                FileName = rn.FileName,
                FileType = rn.FileType,
                OriginalFile = rn.OriginalFile

            };
        }

        public static ExRiskNoteDecision Translate(ERiskNoteDecision rn)
        {
            return new ExRiskNoteDecision()
            {
                Id = rn.Id,
                ADARecordStatus = rn.ADARecordStatus,
                Decision = rn.Decision,
                RiskClient_Id = rn.RiskClient_Id
            };
        }

        public static ExRiskDefaultData Translate(ERiskDefaultData rw)
        {
            return new ExRiskDefaultData()
            {
                Id = rw.Id,
                ConfigurationValue = rw.ConfigurationValue,
                IsActive = rw.IsActive,
                RiskClient_Id = rw.RiskClient_Id,
                RiskConfigurationDescription = rw.RiskConfigurationDescription

            };
        }

        public static ExRiskRole Translate(ERiskRole erc)
        {
            return new ExRiskRole()
            {
                Id = erc.Id,
                RoleName = erc.RoleName,

            };
        }

        //InsurersClients
        public static ExInsurersClients Translate(EInsurersClient eic)
        {
            return new ExInsurersClients()
            {
                Id = eic.Id,
                ClientName = eic.ClientName,

            };
        }

        public static ExRiskUserLocked Translate(ERiskUserLocked erl)
        {
            return new ExRiskUserLocked()
            {
                Id = erl.Id,
                Username = erl.Username,
                Forename = erl.Forename,
                Surname = erl.Surname,
                Template = erl.Template

            };
        }


        public static ExRiskTeam Translate(ERiskTeam ert)
        {
            return new ExRiskTeam()
            {
                Id = ert.Id,
                TeamName = ert.TeamName,
            };
        }

        public static ExRiskUser Translate(ERiskUser eru)
        {
            return new ExRiskUser()
            {
                Id = eru.Id,
                RiskClientId = eru.RiskClientId,
                RiskClientName = eru.RiskClientName,
                RiskRoleId = eru.RiskRoleId,
                RiskRoleName = eru.RiskRoleName,
                RiskTeamId = eru.RiskTeamId,
                RiskTeamName = eru.RiskTeamName,
                UserName = eru.UserName,
                FirstName = eru.FirstName,
                LastName = eru.LastName,
                TelephoneNumber = eru.TelephoneNumber,
                Status = eru.Status,
                AutoLoader = eru.AutoLoader,
                Password = eru.Password,
                PasswordResetDate = eru.PasswordResetDate,
                TemplateFunctions = eru.TemplateFunctions,
                UserTypeId = eru.UserTypeId,

            };
        }

        public static ProcessClaimBatchParam Translate(ProcessClaimBatchRequest request)
        {
            return new ProcessClaimBatchParam()
            {
                Batch = request.Batch,
                BatchEntityType = request.BatchEntityType,
                ClientId = request.ClientId,
                ClientBatchReference = request.ClientBatchReference,
                UserId = request.UserId,
                Who = request.Who
            };
        }

        public static ProcessMobileClaimBatchParam Translate(ProcessMobileClaimBatchRequest request)
        {
            return new ProcessMobileClaimBatchParam()
            {
                Batch = request.Batch,
                BatchEntityType = request.BatchEntityType,
                ClientId = request.ClientId,
                ClientBatchReference = request.ClientBatchReference,
                UserId = request.UserId,
                Who = request.Who
            };
        }

        public static ProcessClaimBatchResponse Translate(ProcessClaimBatchResult result)
        {
            return new ProcessClaimBatchResponse()
            {
                Results = result.Results
            };
        }

        public static ProcessMobileClaimBatchResponse Translate(ProcessMobileClaimBatchResult result)
        {
            return new ProcessMobileClaimBatchResponse()
            {
                Results = result.Results
            };
        }

        public static ProcessClaimParam Translate(ProcessClaimRequest request)
        {
            return new ProcessClaimParam()
            {
                Claim = request.Claim,
                BatchEntityType = request.BatchEntityType,
                Who = request.Who,
                ClientId = request.ClientId,
                ClientBatchReference = request.ClientBatchReference,
                UserId = request.UserId
            };
        }

        public static ProcessClaimResponse Translate(ProcessClaimResult result)
        {
            return new ProcessClaimResponse()
            {
                Results = result.Results,
                RiskClaimId = result.RiskClaim_Id
            };
        }

        public static ClaimsForClientParam Translate(ClaimsForClientRequest request)
        {
            return new ClaimsForClientParam()
            {
                ClientId = request.ClientId,
                FilterClientId = request.FilterClientId,
                TeamId = request.TeamId,
                FilterUserId = request.FilterUserId,
                UserId = request.UserId,
                FilterBatchRef = request.FilterBatchRef,
                FilterClaimNumber = request.FilterClaimNumber,
                FilterEndDate = request.FilterEndDate,
                FilterHighRisk = request.FilterHighRisk,
                FilterMediumRisk = request.FilterMediumRisk,
                FilterLowRisk = request.FilterLowRisk,
                FilterKeoghsCFS = request.FilterKeoghsCFS,
                FilterPreviousVersion = request.FilterPreviousVersion,
                FilterError = request.FilterError,
                FilterStatusUnread = request.FilterStatusUnread,
                FilterStatusRead = request.FilterStatusRead,
                FilterStatusUnreadScoreChanges = request.FilterStatusUnreadScoreChanges,
                FilterStatusReserveChange = request.FilterStatusReserveChange,
                FilterStatusPendingReports = request.FilterStatusPendingReports,
                FilterStatusUnrequestedReports = request.FilterStatusUnrequestedReports,
                FilterStatusAvailableReports = request.FilterStatusAvailableReports,
                FilterNoReportsRequested = request.FilterNoReportsRequested,
                FilterLevel1ReportsRequested = request.FilterLevel1ReportsRequested,
                FilterLevel2ReportsRequested = request.FilterLevel2ReportsRequested,
                FilterCallbackReportsRequested = request.FilterCallbackReportsRequested,
                FilterStartDate = request.FilterStartDate,
                FilterUploadedBy = request.FilterUploadedBy,
                Page = request.Page,
                PageSize = request.PageSize,
                SortColumn = request.SortColumn,
                SortAscending = request.SortAscending,
                FilterNotes = request.FilterNotes,
                FilterIntel = request.FilterIntel,
                FilterDecisions = request.FilterDecisions,
                FilterDecisionIds = request.FilterDecisionIds,
            };
        }

        public static ClaimsForClientResponse Translate(ClaimsForClientResult result)
        {
            var res = new ClaimsForClientResponse()
            {
                Error = result.Error,
                Result = result.Result,
                TotalRows = result.TotalRows
            };

            res.ClaimsList = new List<ExRiskClaimDetails>();
            foreach (var r in result.ClaimsList)
                res.ClaimsList.Add(Translator.Translate(r));

            return res;
        }

        public static SingleClaimForUserParam Translate(SingleClaimForUserRequest request)
        {
            return new SingleClaimForUserParam()
            {
                UserId = request.UserId,
                EditClaimId = request.EditClaimId
            };
        }

        public static SingleClaimForUserResponse Translate(SingleClaimForUserResult result)
        {
            var res = new SingleClaimForUserResponse()
                {
                    Error = result.Error,
                    Result = result.Result
                };

            res.Claim = result.Claim;

            return res;
        }

        public static ClaimsForUserResponse Translate(ClaimsForUserResult result)
        {
            var res = new ClaimsForUserResponse()
            {
                Error = result.Error,
                Result = result.Result
                //TotalRows = result.TotalRows
            };

            res.ClaimsList = new List<ExRiskClaimEdit>();
            foreach (var r in result.ClaimsList)
                res.ClaimsList.Add(Translator.Translate(r));

            return res;
        }

        public static ClaimsForUserParam Translate(ClaimsForUserRequest request)
        {
            return new ClaimsForUserParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }


        public static RegisterNewClientParam Translate(RegisterNewClientRequest request)
        {
            return new RegisterNewClientParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                ClientName = request.ClientName
            };
        }

        public static RegisterNewClientResponse Translate(RegisterNewClientResult result)
        {
            return new RegisterNewClientResponse()
            {
                ClientId = result.ClientId,
                Error = result.Error,
                Result = result.Result
            };
        }

        public static RequestLevelOneReportParam Translate(RequestLevelOneReportRequest request)
        {
            return new RequestLevelOneReportParam()
            {
                RiskClaimId = request.RiskClaimId,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static RequestLevelOneReportResponse Translate(RequestLevelOneReportResult result)
        {
            return new RequestLevelOneReportResponse()
            {
                Result = result.Result,
                Error = result.Error
            };
        }

        public static RequestLevelTwoReportParam Translate(RequestLevelTwoReportRequest request)
        {
            return new RequestLevelTwoReportParam()
            {
                RiskClaimId = request.RiskClaimId,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static RequestLevelTwoReportResponse Translate(RequestLevelTwoReportResult result)
        {
            return new RequestLevelTwoReportResponse()
            {
                Result = result.Result,
                Error = result.Error
            };
        }

        public static RequestCallbackParam Translate(RequestCallbackRequest request)
        {
            return new RequestCallbackParam()
            {
                RiskClaimId = request.RiskClaimId,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static RequestCallbackResponse Translate(RequestCallbackResult result)
        {
            return new RequestCallbackResponse()
            {
                Result = result.Result,
                Error = result.Error
            };
        }

        public static ClearCallbackRequestParam Translate(ClearCallbackRequestRequest request)
        {
            return new ClearCallbackRequestParam()
            {
                RiskClaimId = request.RiskClaimId,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static ClearCallbackRequestResponse Translate(ClearCallbackRequestResult result)
        {
            return new ClearCallbackRequestResponse()
            {
                Result = result.Result,
                Error = result.Error
            };
        }

        public static SetClaimReadStatusParam Translate(SetClaimReadStatusRequest request)
        {
            return new SetClaimReadStatusParam()
            {
                RiskClaimId = request.RiskClaimId,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                RiskClaimReadStatus = request.RiskClaimReadStatus
            };
        }

        public static SetClaimReadStatusResponse Translate(SetClaimReadStatusResult result)
        {
            return new SetClaimReadStatusResponse()
            {
                Result = result.Result,
                Error = result.Error,
                CurrentReadStatus = result.CurrentReadStatus
            };
        }

        public static GetBatchStatusResponse Translate(GetBatchStatusResult result)
        {
            return new GetBatchStatusResponse()
            {
                RiskBatch = Translator.Translate(result.RiskBatch),
                Error = result.Error
            };
        }

        public static GetBatchStatusParam Translate(GetBatchStatusRequest request)
        {
            return new GetBatchStatusParam()
            {
                ClientBatchReference = request.ClientBatchReference,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static GetBatchProcessingResultsParam Translate(GetBatchProcessingResultsRequest request)
        {
            return new GetBatchProcessingResultsParam()
            {
                RiskBatchId = request.RiskBatchId,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
            };
        }

        public static GetBatchProcessingResultsResponse Translate(GetBatchProcessingResultsResult result)
        {
            return new GetBatchProcessingResultsResponse()
            {
                ProcessingResults = result.ProcessingResults,
                Error = result.Error
            };
        }

        public static IsClientBatchReferenceUniqueResponse Translate(IsClientBatchReferenceUniqueResult result)
        {
            return new IsClientBatchReferenceUniqueResponse()
            {
                IsUnique = result.IsUnique,
                Error = result.Error
            };
        }

        public static IsPasswordUniqueResponse Translate(IsPasswordUniqueResult result)
        {
            return new IsPasswordUniqueResponse()
            {
                IsUnique = result.IsUnique,
                Error = result.Error
            };
        }

        public static IsClientNameUniqueResponse Translate(IsClientNameUniqueResult result)
        {
            return new IsClientNameUniqueResponse()
            {
                IsUnique = result.IsUnique,
                Error = result.Error
            };
        }

        public static IsUserNameUniqueResponse Translate(IsUserNameUniqueResult result)
        {
            return new IsUserNameUniqueResponse()
            {
                IsUnique = result.IsUnique,
                Error = result.Error
            };
        }

        public static IsClientBatchReferenceUniqueParam Translate(IsClientBatchReferenceUniqueRequest request)
        {
            return new IsClientBatchReferenceUniqueParam()
            {
                ClientBatchReference = request.ClientBatchReference,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static IsClientNameUniqueParam Translate(IsClientNameUniqueRequest request)
        {
            return new IsClientNameUniqueParam()
            {
                ClientName = request.ClientName,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static IsPasswordUniqueParam Translate(IsPasswordUniqueRequest request)
        {
            return new IsPasswordUniqueParam()
            {
                Username = request.Username,
                Password = request.Password,
            };
        }

        public static IsUserNameUniqueParam Translate(IsUserNameUniqueRequest request)
        {
            return new IsUserNameUniqueParam()
            {
                UserName = request.UserName,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static ClaimsInBatchParam Translate(ClaimsInBatchRequest request)
        {
            return new ClaimsInBatchParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                BatchId = request.BatchId
            };
        }

        public static ClaimsInBatchResponse Translate(ClaimsInBatchResult result)
        {
            var res = new ClaimsInBatchResponse();

            res.Error = result.Error;
            res.ClaimsList = new List<ExRiskClaim>();

            foreach (var r in result.ClaimsList)
                res.ClaimsList.Add(Translator.Translate(r));

            return res;
        }

        public static GetClaimScoreDataParam Translate(GetClaimScoreDataRequest request)
        {
            return new GetClaimScoreDataParam()
            {
                RiskClaimId = request.RiskClaimId,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static GetClaimScoreDataResponse Translate(GetClaimScoreDataResult result)
        {
            return new GetClaimScoreDataResponse()
            {
                ScoreNodes = result.ScoreNodes,
                Error = result.Error,
                RiskClaimRun = Translator.Translate(result.RiskClaimRun)
            };
        }

        public static BatchListParam Translate(BatchListRequest request)
        {
            return new BatchListParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                StatusFilter = request.StatusFilter
            };
        }

        public static ExBatchDetails Translate(EBatchDetails result)
        {
            return new ExBatchDetails()
            {
                BatchId = result.BatchId,
                BatchStatusId = result.BatchStatusId,
                BatchStatusValue = result.BatchStatusValue,
                Description = result.Description,
                Reference = result.Reference,
                Rejections = result.Rejections,
                Status = result.Status,
                UploadedBy = result.UploadedBy,
                UploadedDate = result.UploadedDate
            };
        }

        public static BatchListResponse Translate(BatchListResult result)
        {
            var ret = new BatchListResponse()
            {
                Error = result.Error
            };

            ret.BatchList = new List<ExBatchDetails>();

            foreach (var r in result.BatchList)
                ret.BatchList.Add(Translate(r));

            return ret;
        }


        public static RiskUsersResponse Translate(RiskUsersResult res)
        {
            List<ExRiskUser> userList = new List<ExRiskUser>();
            //
            foreach (var r in res.UsersList)
            {
                userList.Add(Translate(r));
            }
            //
            return new RiskUsersResponse()
            {
                RiskUserList = userList,
            };
        }



        public static RiskUsersParam Translate(RiskUserRequest request)
        {
            return new RiskUsersParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
            };
        }


        public static RiskClientsResponse Translate(RiskClientsResult res)
        {
            List<ExRiskClient> clientList = new List<ExRiskClient>();

            foreach (var r in res.ClientList)
            {
                clientList.Add(Translate(r));
            }
            return new RiskClientsResponse()
            {
                ClientList = clientList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskWordsResponse Translate(RiskWordsResult res)
        {
            List<ExRiskWord> wordList = new List<ExRiskWord>();

            foreach (var r in res.WordList)
            {
                wordList.Add(Translate(r));
            }
            return new RiskWordsResponse()
            {
                WordList = wordList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskNotesResponse Translate(RiskNotesResult res)
        {
            List<ExRiskNote> noteList = new List<ExRiskNote>();

            foreach (var r in res.NoteList)
            {
                noteList.Add(Translate(r));
            }
            return new RiskNotesResponse()
            {
                NoteList = noteList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static GetRiskNoteResponse Translate(GetRiskNoteResult res)
        {
            return new GetRiskNoteResponse()
            {
                Error = res.Error,
                Result = res.Result,
                RiskNote = Translator.Translate(res.RiskNote)
            };
        }

        public static RiskNoteDecisionsResponse Translate(RiskNoteDecisionsResult res)
        {
            List<ExRiskNoteDecision> noteList = new List<ExRiskNoteDecision>();

            foreach (var r in res.DecisionList)
            {
                noteList.Add(Translate(r));
            }
            return new RiskNoteDecisionsResponse()
            {
                DecisionList = noteList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskWordDeleteResponse Translate(RiskWordDeleteResult res)
        {
            return new RiskWordDeleteResponse()
            {

                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskNoteCreateResponse Translate(RiskNoteCreateResult res)
        {
            return new RiskNoteCreateResponse()
            {

                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskNoteUpdateResponse Translate(RiskNoteUpdateResult res)
        {
            return new RiskNoteUpdateResponse()
            {

                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskDefaultDataResponse Translate(RiskDefaultDataResult res)
        {
            List<ExRiskDefaultData> defaultDataList = new List<ExRiskDefaultData>();

            foreach (var r in res.DefaultDataList)
            {
                defaultDataList.Add(Translate(r));
            }
            return new RiskDefaultDataResponse()
            {
                DefaultDataList = defaultDataList,
                Error = res.Error,
                Result = res.Result,

            };
        }


        public static GetAssignedRiskClientsForUserResponse Translate(GetAssignedRiskClientsForUserResult res)
        {
            List<ExRiskClient> clientList = new List<ExRiskClient>();

            foreach (var r in res.ClientList)
            {
                clientList.Add(Translate(r));
            }
            return new GetAssignedRiskClientsForUserResponse()
            {
                ClientList = clientList,
                Error = res.Error,
                Result = res.Result,

            };
        }

        public static RiskRolesResponse Translate(RiskRolesResult res)
        {
            List<ExRiskRole> roleList = new List<ExRiskRole>();

            foreach (var r in res.RoleList)
            {
                roleList.Add(Translate(r));
            }
            return new RiskRolesResponse()
            {
                RoleList = roleList,
                Error = res.Error,
                Result = res.Result,

            };
        }


        public static InsurersClientsResponse Translate(InsurersClientsResult res)
        {
            List<ExInsurersClients> clientList = new List<ExInsurersClients>();

            foreach (var r in res.ClientList)
            {
                clientList.Add(Translate(r));
            }
            return new InsurersClientsResponse()
            {
                ClientList = clientList,
                Error = res.Error,
                Result = res.Result,

            };
        }


        public static RiskUserUnlockResponse Translate(RiskUserUnlockResult res)
        {
            return new RiskUserUnlockResponse()
            {
                Error = res.Error,
                Result = res.Result
            };
        }

        public static GeneratePasswordResetTokenResponse Translate(GeneratePasswordResetTokenResult res)
        {
            return new GeneratePasswordResetTokenResponse()
            {
                Error = res.Error,
                Result = res.Result,
                Token = res.Token
            };
        }

        public static ValidatePasswordResetTokenResponse Translate(ValidatePasswordResetTokenResult res)
        {
            return new ValidatePasswordResetTokenResponse()
            {
                Error = res.Error,
                Result = res.Result,
                UserName = res.UserName
            };
        }




        public static RiskUsersLockedResponse Translate(RiskUsersLockedResult res)
        {
            List<ExRiskUserLocked> lockedUserList = new List<ExRiskUserLocked>();

            foreach (var u in res.LockedUserList)
            {
                lockedUserList.Add(Translate(u));
            }

            return new RiskUsersLockedResponse()
            {
                LockedUserList = lockedUserList,
                Error = res.Error,
                Result = res.Result
            };
        }

        public static RiskClientsParam Translate(RiskClientsRequest request)
        {
            return new RiskClientsParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,

                FilterClientId = request.FilterClientId

            };
        }

        public static RiskWordsParam Translate(RiskWordsRequest request)
        {
            return new RiskWordsParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,

                FilterClientId = request.FilterClientId

            };
        }

        public static RiskNotesParam Translate(RiskNotesRequest request)
        {
            return new RiskNotesParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,

                FilterRiskCliamId = request.FilterRiskClaimId

            };
        }

        public static GetRiskNoteParam Translate(GetRiskNoteRequest request)
        {
            return new GetRiskNoteParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,

                FilterRiskNoteId = request.FilterRiskNoteId

            };
        }

        public static RiskNoteDecisionsParam Translate(RiskNoteDecisionsRequest request)
        {
            return new RiskNoteDecisionsParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,

                FilterRiskClientId = request.FilterRiskClientId

            };
        }

        public static RiskWordDeleteParam Translate(RiskWordDeleteRequest request)
        {
            return new RiskWordDeleteParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,

                RiskWordId = request.RiskWordId

            };
        }

        public static RiskNoteCreateParam Translate(RiskNoteCreateRequest request)
        {
            return new RiskNoteCreateParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                CreatedBy = request.CreatedBy,
                CreatedDate = request.CreatedDate,
                Decision_Id = request.Decision_Id,
                Deleted = request.Deleted,
                NoteDesc = request.NoteDesc,
                RiskClaim_Id = request.RiskClaim_Id,
                Visibilty = request.Visibilty,
                UserTypeId = request.UserTypeId,
                OriginalFile = request.OriginalFile,
                FileName = request.FileName,
                FileType = request.FileType


            };
        }

        public static RiskNoteUpdateParam Translate(RiskNoteUpdateRequest request)
        {
            return new RiskNoteUpdateParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                Decision_Id = request.Decision_Id,
                Deleted = request.Deleted,
                NoteDesc = request.NoteDesc,
                Visibilty = request.Visibilty,
                RiskNoteId = request.RiskNoteId,
                UserTypeId = request.UserTypeId,
                UpdatedBy = request.UpdatedBy,
                UpdatedDate = request.UpdatedDate


            };
        }

        public static RiskDefaultDataParam Translate(RiskDefaultDataRequest request)
        {
            return new RiskDefaultDataParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,

                FilterClientId = request.FilterClientId

            };
        }

        public static GetAssignedRiskClientsForUserParam Translate(GetAssignedRiskClientsForUserRequest request)
        {
            return new GetAssignedRiskClientsForUserParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                FilterUserId = request.FilterUserId

            };
        }

        public static RiskRolesParam Translate(RiskRolesRequest request)
        {
            return new RiskRolesParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,

                FilterClientId = request.FilterClientId,

            };
        }

        public static InsurersClientsParam Translate(InsurersClientsRequest request)
        {
            return new InsurersClientsParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,

                FilterClientId = request.FilterClientId,

            };
        }

        public static RiskUsersLockedParam Translate(RiskUsersLockedRequest request)
        {
            return new RiskUsersLockedParam()
            {
                ClientId = request.ClientId,
                UserId = request.UserId,
                Who = request.Who
            };
        }

        public static RiskUserUnlockParam Translate(RiskUserUnlockRequest request)
        {
            return new RiskUserUnlockParam()
            {
                ClientId = request.ClientId,
                UserId = request.UserId,
                Who = request.Who,
                UnlockUserId = request.UnlockUserId
            };
        }

        public static GeneratePasswordResetTokenParam Translate(GeneratePasswordResetTokenRequest request)
        {
            return new GeneratePasswordResetTokenParam()
            {
                UserName = request.UserName,
                Token = request.Token,
                TokenExpiration = request.TokenExpiration,
                ResetLink = request.ResetLink,
            };
        }

        public static ValidatePasswordResetTokenParam Translate(ValidatePasswordResetTokenRequest request)
        {
            return new ValidatePasswordResetTokenParam()
            {
                Token = request.Token,
            };
        }

        public static GetRiskUserInfoParam Translate(GetRiskUserInfoRequest request)
        {
            return new GetRiskUserInfoParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static GetRiskUserInfoResponse Translate(GetRiskUserInfoResult result)
        {
            return new GetRiskUserInfoResponse()
            {
                UserInfo = Translate(result.UserInfo),
            };
        }

        public static WebsiteBatchListParam Translate(WebsiteBatchListRequest request)
        {
            return new WebsiteBatchListParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                FilterUserId = request.FilterUserId,
                FilterClientId = request.FilterClientId,
                Who = request.Who,
                TeamId = request.TeamId,
                SortColumn = request.SortColumn,
                SortAscending = request.SortAscending,
                Page = request.Page,
                PageSize = request.PageSize,
                StatusFilter = request.StatusFilter
            };
        }


        public static WebsiteSingleClaimsListParam Translate(WebsiteSingleClaimsListRequest request)
        {
            return new WebsiteSingleClaimsListParam()
                {
                    UserId = request.UserId,
                    Who = request.Who,
                    ClientId = request.ClientId,
                    TeamId = request.TeamId,
                    SortColumn = request.SortColumn,
                    SortAscending = request.SortAscending,
                    Page = request.Page,
                    PageSize = request.PageSize,
                    StatusFilter = request.StatusFilter
                };
        }



        public static WebsiteSingleClaimsListResponse Translate(WebsiteSingleClaimsListResult result)
        {
            var ret = new WebsiteSingleClaimsListResponse()
                {
                    Error = result.Error,
                    TotalRows = result.TotalRows
                };

            ret.WebsiteSingleClaimsList = new List<ExWebsiteSingleClaim>();

            foreach (var r in result.WebsiteSingleClaimsList)
                ret.WebsiteSingleClaimsList.Add(Translate(r));

            return ret;
        }

        public static WebsiteSaveSingleClaimParam Translate(WebsiteSaveSingleClaimRequest request)
        {
            return new WebsiteSaveSingleClaimParam()
                {
                    UserId = request.UserId,
                    ClientId = request.ClientId,
                    Who = request.Who,
                    ClaimToSave = request.ClaimToSave,
                    ClaimType = request.ClaimType,
                    EditClaimId = request.EditClaimId,
                    RiskClaimNumber = request.RiskClaimNumber,
                    IncidentDate = request.IncidentDate,
                    ModifiedDate = request.ModifiedDate,
                    ModifiedBy = request.ModifiedBy,
                    Status = request.Status
                };
        }

        public static WebsiteSaveSingleClaimResponse Translate(WebsiteSaveSingleClaimResult result)
        {
            var ret = new WebsiteSaveSingleClaimResponse()
            {
                Error = result.Error,
                Result = result.Result,
                EditClaimId = result.EditClaimId
            };

            return ret;
        }

        public static WebsiteSaveTemplateFunctionsResponse Translate(WebsiteSaveTemplateFunctionsResult result)
        {
            var ret = new WebsiteSaveTemplateFunctionsResponse()
            {
                Error = result.Error,
                Result = result.Result
            };

            return ret;
        }

        public static WebsiteLoadTemplateFunctionsResponse Translate(WebsiteLoadTemplateFunctionsResult result)
        {
            var ret = new WebsiteLoadTemplateFunctionsResponse()
            {
                Error = result.Error,
                Result = result.Result,
                TemplateFunctionsJson = result.TemplateFunctionsJson
            };

            return ret;
        }







        public static WebsiteFetchSingleClaimDetailParam Translate(WebsiteFetchSingleClaimRequest request)
        {
            return new WebsiteFetchSingleClaimDetailParam()
                {
                    UserId = request.UserId,
                    ClientId = request.ClientId,
                    Who = request.Who,
                    EditClaimId = request.EditClaimId
                };
        }


        public static WebsiteDeleteSingleClaimParam Translate(WebsiteDeleteSingleClaimRequest request)
        {
            return new WebsiteDeleteSingleClaimParam()
                {
                    EditClaimId = request.EditClaimId,
                    UserId = request.UserId,
                    ClientId = request.ClientId,
                    Who = request.Who
                };
        }

        public static WebsiteDeleteSingleClaimResponse Translate(WebsiteDeleteSingleClaimResult result)
        {
            var ret = new WebsiteDeleteSingleClaimResponse()
                {
                    Error = result.Error,
                    Result = result.Result
                };

            return ret;
        }

        public static WebsiteFetchSingleClaimResponse Translate(WebsiteFetchSingleClaimDetailResult result)
        {
            var ret = new WebsiteFetchSingleClaimResponse()
            {
                Error = result.Error,
                Result = result.Result,
                SingleClaimDetail = Translate(result.claimDetail)
            };

            return ret;
        }


        public static WebsiteGetTotalSingleClaimsForClientParam Translate(WebsiteGetTotalSingleClaimsForClientRequest request)
        {
            return new WebsiteGetTotalSingleClaimsForClientParam()
                {
                    UserId = request.UserId,
                    ClientId = request.ClientId,
                    Who = request.Who,
                };
        }

        public static WebsiteGetTotalSingleClaimsForClientResponse Translate(
            WebsiteGetTotalSingleClaimsForClientResult result)
        {
            var ret = new WebsiteGetTotalSingleClaimsForClientResponse()
                {
                    Error = result.Error,
                    Result = result.Result,
                    TotalRows = result.TotalRows
                };

            return ret;
        }


        public static ExWebsiteBatchDetails Translate(EWebsiteBatchDetails result)
        {
            return new ExWebsiteBatchDetails()
            {
                BatchId = result.BatchId,
                RiskClientId = result.RiskClientId,
                BatchReference = result.BatchReference,
                BatchEntityType = result.BatchEntityType,
                ClientBatchReference = result.ClientBatchReference,
                BatchStatusAsString = result.BatchStatusAsString,
                BatchStatusId = result.BatchStatusId,
                UploadedDate = result.UploadedDate,
                UploadedBy = result.UploadedBy,
                VerificationResults = result.VerificationResults,
                MappingResults = result.MappingResults,
                TotalNumberOfClaims = result.TotalNumberOfClaims,
            };
        }

        //public static ExCueInvolvement Translate(MDA.RiskService.Model.ECueInvolvement result)
        //{
        //    return new ExCueInvolvement()
        //    {
        //        CueAddress = result.CueAddress,
        //        CuePerson = result.CuePerson,
        //        CueVehicle = result.CueVehicle,
        //        CueDatabaseAll = result.CueDatabaseAll
        //    };
        //}

        public static WebsiteBatchListResponse Translate(WebsiteBatchListResult result)
        {
            var ret = new WebsiteBatchListResponse()
            {
                Error = result.Error,
                TotalRows = result.TotalRows
            };

            ret.BatchList = new List<ExWebsiteBatchDetails>();

            foreach (var r in result.BatchList)
                ret.BatchList.Add(Translate(r));

            return ret;
        }


        public static WebsiteFilterBatchNumbersParam Translate(WebsiteFilterBatchNumbersRequest request)
        {
            return new WebsiteFilterBatchNumbersParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                Role = request.Role,
                SelectedClientId = request.SelectedClientId
            };
        }




        public static WebsiteFilterBatchNumbersResponse Translate(WebsiteFilterBatchNumbersResult result)
        {
            var ret = new WebsiteFilterBatchNumbersResponse()
                          {
                              Error = result.Error,
                              Result = result.Result,
                              BatchNumbersList = new List<string>(result.BatchNumbersList)
                          };

            //ret.BatchNumbersList = new List<string>(result.BatchNumbersList);

            //ret.BatchNumbersList.AddRange(result.BatchNumbersList);

            //foreach (var r in result.BatchNumbersList)
            //{
            //    ret.BatchNumbersList.Add(r);
            //}


            return ret;
        }

        public static WebsiteCueInvolvementsParam Translate(WebsiteCueInvolvementsRequest request)
        {
            return new WebsiteCueInvolvementsParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                RiskClaim_Id = request.RiskClaim_Id
            };
        }

        public static WebsiteCueInvolvementsResponse Translate(WebsiteCueInvolvementsResult request)
        {
            return new WebsiteCueInvolvementsResponse()
            {
                Error = request.Error,
                Result = request.Result,
                ClaimNumber = request.ClaimNumber,
                IncidentDate = request.IncidentDate,
                UploadedDate = request.UploadedDate,
                CueInvolvements = Translator.Translate(request.CueInvolvements)
            };
        }

        private static List<ExCueInvolvement> Translate(List<ECueInvolvement> request)
        {
            List<ExCueInvolvement> lstExCueInvolvement = new List<ExCueInvolvement>();

            foreach (var item in request)
            {
                ExCueInvolvement cueInvolvement = new ExCueInvolvement();
                cueInvolvement.CueAddress = Translator.Translate(item.CueAddress);
                cueInvolvement.CueDatabaseAll = item.CueDatabaseAll;
                cueInvolvement.CuePerson = Translator.Translate(item.CuePerson);
                cueInvolvement.CueVehicle = Translator.Translate(item.CueVehicle);

                lstExCueInvolvement.Add(cueInvolvement);
            }

            return lstExCueInvolvement;
        }

        private static ExCueVehicle Translate(ECueVehicle eCueVehicle)
        {
            return new ExCueVehicle()
            {
                VehicleReg = eCueVehicle.VehicleReg,
                VIN = eCueVehicle.VIN
            };
        }

        private static ExCuePerson Translate(ECuePerson eCuePerson)
        {
            return new ExCuePerson()
            {
                FirstName = eCuePerson.FirstName,
                LastName = eCuePerson.LastName,
                PersonId = eCuePerson.PersonId,
                Dob = eCuePerson.Dob,
                DrivingLicenceNumber = eCuePerson.DrivingLicenceNumber,
                Involvement = eCuePerson.Involvement,
                NI_Number = eCuePerson.NI_Number
            };
        }

        private static ExCueAddress Translate(ECueAddress eCueAddress)
        {
            return new ExCueAddress()
            {
                HouseName = eCueAddress.HouseName,
                Locality = eCueAddress.Locality,
                Number = eCueAddress.Number,
                PostCode = eCueAddress.PostCode,
                Street = eCueAddress.Street,
                Town = eCueAddress.Town
            };
        }


        public static GenerateAnyBatchReportsOutstandingParam Translate(GenerateAnyBatchReportsOutstandingRequest request)
        {
            return new GenerateAnyBatchReportsOutstandingParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
            };
        }

        public static GetPasswordFailuresSinceLastSuccessParam Translate(GetPasswordFailuresSinceLastSuccessRequest request)
        {
            return new GetPasswordFailuresSinceLastSuccessParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                UserName = request.UserName
            };
        }

        public static IsUserInRoleParam Translate(IsUserInRoleRequest request)
        {
            return new IsUserInRoleParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                UserName = request.UserName,
                RoleName = request.RoleName
            };
        }

        public static GetRolesForUserParam Translate(GetRolesForUserRequest request)
        {
            return new GetRolesForUserParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                UserName = request.UserName
            };
        }

        public static ValidateUserParam Translate(ValidateUserRequest request)
        {
            return new ValidateUserParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                UserName = request.UserName,
                Password = request.Password,
                IPAddress = request.IPAddress
            };
        }


        public static CreateUserParam Translate(CreateUserRequest request)
        {
            return new CreateUserParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                UserName = request.UserName,
                Password = request.Password,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                TelephoneNumber = request.TelephoneNumber,
                RoleId = request.RoleId,
                TeamId = request.TeamId,
                Status = request.Status,
                Autoloader = request.Autoloader,
                UserTypeId = request.UserTypeId

            };
        }

        public static CreateClientParam Translate(CreateClientRequest request)
        {
            return new CreateClientParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                ClientName = request.ClientName,
                AmberThreshold = request.AmberThreshold,
                RedThreshold = request.RedThreshold,
                FileExtensions = request.FileExtensions,
                StatusId = request.StatusId,
                InsurersClientsId = request.InsurersClientsId

            };
        }

        public static CreateRiskWordParam Translate(CreateRiskWordRequest request)
        {
            return new CreateRiskWordParam()
            {
                DateAdded = request.DateAdded,
                FieldName = request.FieldName,
                LookupWord = request.LookupWord,
                ReplacementWord = request.ReplacementWord,
                ReplaceWholeString = request.ReplaceWholeString,
                RiskClientId = request.RiskClient_Id,
                SearchType = request.SearchType,
                TableName = request.TableName
            };
        }

        public static CreateRiskDefaultDataParam Translate(CreateRiskDefaultDataRequest request)
        {
            return new CreateRiskDefaultDataParam()
            {
                ClientId = request.ClientId,
                ConfigurationValue = request.ConfigurationValue,
                IsActive = request.IsActive,
                RiskClient_Id = request.RiskClient_Id,
                RiskConfigurationDescriptionId = request.RiskConfigurationDescriptionId,
                UserId = request.UserId,
                Who = request.Who
            };
        }

        public static CreateRiskNoteDecisionParam Translate(CreateRiskNoteDecisionRequest request)
        {
            return new CreateRiskNoteDecisionParam()
            {
                ClientId = request.ClientId,
                IsActive = request.IsActive,
                RiskClient_Id = request.RiskClient_Id,
                UserId = request.UserId,
                Who = request.Who,
                Decision = request.Decision
            };
        }

        public static DeleteRiskDefaultDataParam Translate(DeleteRiskDefaultDataRequest request)
        {
            return new DeleteRiskDefaultDataParam()
            {
                ClientId = request.ClientId,
                UserId = request.UserId,
                Who = request.Who,
                Id = request.Id
            };
        }

        public static DeleteRiskNoteDecisionParam Translate(DeleteRiskNoteDecisionRequest request)
        {
            return new DeleteRiskNoteDecisionParam()
            {
                ClientId = request.ClientId,
                UserId = request.UserId,
                Who = request.Who,
                Id = request.Id
            };
        }

        public static UpdateClientParam Translate(UpdateClientRequest request)
        {
            return new UpdateClientParam()
            {
                Id = request.Id,
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                ClientName = request.ClientName,
                AmberThreshold = request.AmberThreshold,
                RedThreshold = request.RedThreshold,
                FileExtensions = request.FileExtensions,
                StatusId = request.StatusId,
                InsurersClientsId = request.InsurersClientsId,
                RecordReserveChanges = request.RecordReserveChanges,
                BatchPriority = request.BatchPriority
            };
        }

        public static UpdateUserParam Translate(UpdateUserRequest request)
        {
            return new UpdateUserParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                UserName = request.UserName,
                Password = request.Password,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                TelephoneNumber = request.TelephoneNumber,
                RoleId = request.RoleId,
                TeamId = request.TeamId,
                Status = request.Status,
                Autoloader = request.Autoloader,
                UserTypeId = request.UserTypeId
            };
        }

        public static CreateRiskUser2ClientParam Translate(CreateRiskUser2ClientRequest request)
        {
            return new CreateRiskUser2ClientParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                SelectedClientId = request.SelectedClientId,
                SelectedUserId = request.SelectedUserId

            };
        }

        public static DeleteRiskUser2ClientParam Translate(DeleteRiskUser2ClientRequest request)
        {
            return new DeleteRiskUser2ClientParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                SelectedClientId = request.SelectedClientId,
                SelectedUserId = request.SelectedUserId

            };
        }



        public static ChangePasswordParam Translate(ChangePasswordRequest request)
        {
            return new ChangePasswordParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                UserName = request.UserName,
                OldPassword = request.OldPassword,
                NewPassword = request.NewPassword
            };
        }

        public static GetUserIdParam Translate(GetUserIdRequest request)
        {
            return new GetUserIdParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                UserName = request.UserName
            };
        }

        public static CheckUserBelongsToClientParam Translate(CheckUserBelongsToClientRequest request)
        {
            return new CheckUserBelongsToClientParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who
            };
        }

        public static CheckUserHasAccessToReportParam Translate(CheckUserHasAccessToReportRequest request)
        {
            return new CheckUserHasAccessToReportParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                LoggedInUserId = request.LoggedInUserId,
                RiskBatchId = request.RiskBatchId
            };
        }

        public static GenerateBatchReportParam Translate(GenerateBatchReportRequest request)
        {
            return new GenerateBatchReportParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                RiskBatchId = request.RiskBatchId
            };
        }

        public static GenerateLevel1ReportParam Translate(GenerateLevel1ReportRequest request)
        {
            return new GenerateLevel1ReportParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                RiskClaimId = request.RiskClaimId
            };
        }

        public static GenerateLevel2ReportParam Translate(GenerateLevel2ReportRequest request)
        {
            return new GenerateLevel2ReportParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                RiskClaimId = request.RiskClaimId
            };
        }

        public static WebsiteCueSearchParam Translate(WebsiteCueSearchRequest request)
        {
            return new WebsiteCueSearchParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                RiskClaim_Id = request.RiskClaim_Id,
                ListPerson_Id = request.ListPerson_Id
            };
        }

        public static WebsiteCueSearchResponse Translate(WebsiteCueSearchResult result)
        {
            var ret = new WebsiteCueSearchResponse()
            {
                Error = result.Error,
                Result = result.Result

            };

            return ret;
        }

        public static WebsiteCueReportParam Translate(WebsiteCueReportRequest request)
        {
            return new WebsiteCueReportParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                RiskClaim_Id = request.RiskClaim_Id
            };
        }

        public static WebsiteCueReportResponse Translate(WebsiteCueReportResult result)
        {
            return new WebsiteCueReportResponse()
            {
                CueReportDetails = Translator.Translate(result.CueReportDetails)
            };
        }

        private static ExCueReportDetails Translate(ECueReportDetails eCueReportDetails)
        {
            return new ExCueReportDetails()
            {
                ClaimNumber = eCueReportDetails.ClaimNumber,
                SearchDate = eCueReportDetails.SearchDate,
                ListResultInfo = Translator.Translate(eCueReportDetails.ListResultInfo)
            };
        }

        private static List<Dictionary<string, ExCueReportResult>> Translate(List<Dictionary<string, ECUE_ReportModel>> lstResultInfo)
        {
            List<Dictionary<string, ExCueReportResult>> lstReport = new List<Dictionary<string, ExCueReportResult>>();

            foreach (var item in lstResultInfo)
            {

                Dictionary<string, ExCueReportResult> dictExCUE_ReportModel = new Dictionary<string, ExCueReportResult>();

                foreach (var dictItem in item)
                {
                    dictExCUE_ReportModel.Add(dictItem.Key, Translator.Translate(dictItem.Value));
                }

                lstReport.Add(dictExCUE_ReportModel);
            }

            return lstReport;
        }

        private static ExCueReportResult Translate(ECUE_ReportModel eCUE_ReportModel)
        {
            ExCueReportResult reportModel = new ExCueReportResult();

            //reportModel.ClaimNumber = eCUE_ReportModel.ClaimNumber;
            //reportModel.SearchDate = eCUE_ReportModel.SearchDate;
            reportModel.SearchSummary = Translator.Translate(eCUE_ReportModel.SearchSummary);
            reportModel.ResultsSummary = Translator.Translate(eCUE_ReportModel.ResultsSummary);
            reportModel.SearchCriteria = Translator.Translate(eCUE_ReportModel.SearchCriteria);
            reportModel.IncidentSummary = Translator.Translate(eCUE_ReportModel.IncidentSummary);
            reportModel.CUE_Incidents = Translator.Translate(eCUE_ReportModel.CUE_Incidents);

            return reportModel;
        }

        private static List<ExCUEIncident> Translate(List<ECUEIncident> cueLstIncident)
        {
            List<ExCUEIncident> lstIncident = new List<ExCUEIncident>();

            foreach (var item in cueLstIncident)
            {
                ExCUEIncident incident = new ExCUEIncident();
                incident.IncidentId = item.IncidentId;
                incident.IncidentType = item.IncidentType;
                if (item.IncidentDetails != null)
                {
                    if (item.IncidentDetails != null)
                    {
                        var iDetails = incident.IncidentDetails = new ExCUEIncidentDetails();
                        iDetails.HospitalAttended = item.IncidentDetails.HospitalAttended;
                        iDetails.IllnessDiseaseDescription = item.IncidentDetails.IllnessDiseaseDescription;
                        iDetails.InjuryDescription = item.IncidentDetails.InjuryDescription;

                        incident.IncidentDetails = iDetails;
                    }
                }

                incident.ClaimData = Translator.Translate(item.ClaimData);
                incident.GeneralData = Translator.Translate(item.GeneralData);
                incident.CUEIncidentInvolvements = Translator.Translate(item.CUEIncidentInvolvements);
                if (item.CUEIncidentInvolvementVehicleDetails != null)
                    incident.CUEIncidentInvolvementVehicleDetails = Translator.Translate(item.CUEIncidentInvolvementVehicleDetails);

                if (item.Suppliers != null)
                    incident.Suppliers = Translator.Translate(item.Suppliers);

                if (item.IncidentDetails != null)
                    incident.IncidentDetails = Translator.Translate(item.IncidentDetails);

                lstIncident.Add(incident);
            }

            return lstIncident;
        }

        private static ExCUEGeneralData Translate(ECUEGeneralData cueGeneralData)
        {
            ExCUEGeneralData cueGeneralDataInfo = new ExCUEGeneralData();

            cueGeneralDataInfo.CatastropheRelated = cueGeneralData.CatastropheRelated;
            cueGeneralDataInfo.CauseOfLoss = cueGeneralData.CauseOfLoss;
            cueGeneralDataInfo.ClaimStatus = cueGeneralData.ClaimStatus;
            cueGeneralDataInfo.ClosedDate = cueGeneralData.ClosedDate;
            cueGeneralDataInfo.CollectivePolicyIndicator = cueGeneralData.CollectivePolicyIndicator;
            cueGeneralDataInfo.CUEPayments = Translator.Translate(cueGeneralData.CUEPayments);
            cueGeneralDataInfo.IncidentDescription = cueGeneralData.IncidentDescription;
            cueGeneralDataInfo.InsurerContactName = cueGeneralData.InsurerContactName;
            cueGeneralDataInfo.InsurerContactTel = cueGeneralData.InsurerContactTel;
            cueGeneralDataInfo.LossSetupDate = cueGeneralData.LossSetupDate;
            cueGeneralDataInfo.PolicyInceptionDate = cueGeneralData.PolicyInceptionDate;
            cueGeneralDataInfo.PolicyNumber = cueGeneralData.PolicyNumber;
            cueGeneralDataInfo.PolicyNumberID = cueGeneralData.PolicyNumberID;
            cueGeneralDataInfo.PolicyPeriodEndDate = cueGeneralData.PolicyPeriodEndDate;
            cueGeneralDataInfo.PolicyType = cueGeneralData.PolicyType;
            cueGeneralDataInfo.RiskAddressCity = cueGeneralData.RiskAddressCity;
            cueGeneralDataInfo.RiskAddressHouseName = cueGeneralData.RiskAddressHouseName;
            cueGeneralDataInfo.RiskAddressLocality = cueGeneralData.RiskAddressLocality;
            cueGeneralDataInfo.RiskAddressNumber = cueGeneralData.RiskAddressNumber;
            cueGeneralDataInfo.RiskAddressPostCode = cueGeneralData.RiskAddressPostCode;
            cueGeneralDataInfo.RiskAddressStreet = cueGeneralData.RiskAddressStreet;
            cueGeneralDataInfo.RiskAddressTown = cueGeneralData.RiskAddressTown;
            cueGeneralDataInfo.IncidentDate = cueGeneralData.IncidentDate;

            return cueGeneralDataInfo;
        }

        private static List<ExCUEPayment> Translate(List<ECUEPayment> cuePaymentList)
        {
            List<ExCUEPayment> lstPayments = new List<ExCUEPayment>();

            foreach (var item in cuePaymentList)
            {
                ExCUEPayment cuePayment = new ExCUEPayment();
                cuePayment.IncidentId = item.IncidentId;
                cuePayment.PaymentAmount = item.PaymentAmount;
                cuePayment.PaymentType = item.PaymentType;
                lstPayments.Add(cuePayment);
            }

            return lstPayments;
        }

        private static ExCUEIncidentDetails Translate(ECUEIncidentDetails cueIncidentDetails)
        {
            ExCUEIncidentDetails cueIncidentDetail = new ExCUEIncidentDetails();

            cueIncidentDetail.HospitalAttended = cueIncidentDetails.HospitalAttended;
            cueIncidentDetail.IllnessDiseaseDescription = cueIncidentDetails.IllnessDiseaseDescription;
            cueIncidentDetail.InjuryDescription = cueIncidentDetails.InjuryDescription;

            return cueIncidentDetail;
        }

        private static List<ExCUESupplier> Translate(List<ECUESupplier> cueListSuppliers)
        {
            List<ExCUESupplier> lstSuppliers = new List<ExCUESupplier>();
            foreach (var item in cueListSuppliers)
            {
                ExCUESupplier cueSupplier = new ExCUESupplier();
                cueSupplier.AddressCity = item.AddressCity;
                cueSupplier.AddressHouseName = item.AddressHouseName;
                cueSupplier.AddressIndicator = item.AddressIndicator;
                cueSupplier.AddressLocality = item.AddressLocality;
                cueSupplier.AddressNumber = item.AddressNumber;
                cueSupplier.AddressPostCode = item.AddressPostCode;
                cueSupplier.AddressStreet = item.AddressStreet;
                cueSupplier.AddressTown = item.AddressTown;
                cueSupplier.CompanyName = item.CompanyName;
                cueSupplier.IncidentId = item.IncidentId;
                cueSupplier.PaymentAmount = item.PaymentAmount;
                cueSupplier.SupplierStatus = item.SupplierStatus;
                cueSupplier.Telephone = item.Telephone;
                cueSupplier.VAT = item.VAT;

                lstSuppliers.Add(cueSupplier);
            }

            return lstSuppliers;
        }

        private static List<ExCUEIncidentInvolvementVehicleDetails> Translate(List<ECUEIncidentInvolvementVehicleDetails> cueListInvolvementVehicleDetails)
        {
            List<ExCUEIncidentInvolvementVehicleDetails> lstIncidentInvolvementVehicleDetails = new List<ExCUEIncidentInvolvementVehicleDetails>();

            foreach (var item in cueListInvolvementVehicleDetails)
            {
                ExCUEIncidentInvolvementVehicleDetails incidentInvolvementVehicleDetail = new ExCUEIncidentInvolvementVehicleDetails();
                incidentInvolvementVehicleDetail.AddressCity = item.AddressCity;
                incidentInvolvementVehicleDetail.AddressHouseName = item.AddressHouseName;
                incidentInvolvementVehicleDetail.AddressIndicator = item.AddressIndicator;
                incidentInvolvementVehicleDetail.AddressLocality = item.AddressLocality;
                incidentInvolvementVehicleDetail.AddressNumber = item.AddressNumber;
                incidentInvolvementVehicleDetail.AddressPostCode = item.AddressPostCode;
                incidentInvolvementVehicleDetail.AddressStreet = item.AddressStreet;
                incidentInvolvementVehicleDetail.AddressTown = item.AddressTown;
                incidentInvolvementVehicleDetail.Colour = item.Colour;
                incidentInvolvementVehicleDetail.CoverType = item.CoverType;
                incidentInvolvementVehicleDetail.DamageStatus = item.DamageStatus;
                incidentInvolvementVehicleDetail.Dob = item.Dob;
                incidentInvolvementVehicleDetail.Forename = item.Forename;
                incidentInvolvementVehicleDetail.Incident_Id = item.Incident_Id;
                incidentInvolvementVehicleDetail.LastName = item.LastName;
                incidentInvolvementVehicleDetail.Make = item.Make;
                incidentInvolvementVehicleDetail.MiddleName = item.MiddleName;
                incidentInvolvementVehicleDetail.Model = item.Model;
                incidentInvolvementVehicleDetail.NewForOldIndicator = item.NewForOldIndicator;
                incidentInvolvementVehicleDetail.NI_Number = item.NI_Number;
                incidentInvolvementVehicleDetail.Occupation = item.Occupation;
                incidentInvolvementVehicleDetail.PaymentAmount = item.PaymentAmount;
                incidentInvolvementVehicleDetail.RecoveredStatus = item.RecoveredStatus;
                incidentInvolvementVehicleDetail.RegistrationStatus = item.RegistrationStatus;
                incidentInvolvementVehicleDetail.Sex = item.Sex;
                incidentInvolvementVehicleDetail.Status = item.Status;
                incidentInvolvementVehicleDetail.Telephone = item.Telephone;
                incidentInvolvementVehicleDetail.Title = item.Title;
                incidentInvolvementVehicleDetail.Type = item.Type;
                incidentInvolvementVehicleDetail.VAT = item.VAT;
                incidentInvolvementVehicleDetail.VehicleIdentificationNumber = item.VehicleIdentificationNumber;
                incidentInvolvementVehicleDetail.VehicleRegistration = item.VehicleRegistration;
                incidentInvolvementVehicleDetail.VIN = item.VIN;

                lstIncidentInvolvementVehicleDetails.Add(incidentInvolvementVehicleDetail);
            }

            return lstIncidentInvolvementVehicleDetails;
        }
        private static List<ExCUEIncidentInvolvement> Translate(List<ECUEIncidentInvolvement> cueListIncidentInvolvements)
        {
            List<ExCUEIncidentInvolvement> lstIncidentInvolvements = new List<ExCUEIncidentInvolvement>();

            foreach (var item in cueListIncidentInvolvements)
            {
                ExCUEIncidentInvolvement incidentInvolvement = new ExCUEIncidentInvolvement();
                incidentInvolvement.AddressCity = item.AddressCity;
                incidentInvolvement.AddressHouseName = item.AddressHouseName;
                incidentInvolvement.AddressIndicator = item.AddressIndicator;
                incidentInvolvement.AddressLocality = item.AddressLocality;
                incidentInvolvement.AddressNumber = item.AddressNumber;
                incidentInvolvement.AddressPostCode = item.AddressPostCode;
                incidentInvolvement.AddressStreet = item.AddressStreet;
                incidentInvolvement.AddressTown = item.AddressTown;
                incidentInvolvement.Dob = item.Dob;
                incidentInvolvement.Forename = item.Forename;
                incidentInvolvement.Incident_Id = item.Incident_Id;
                incidentInvolvement.LastName = item.LastName;
                incidentInvolvement.MiddleName = item.MiddleName;
                incidentInvolvement.NI_Number = item.NI_Number;
                incidentInvolvement.Occupation = item.Occupation;
                incidentInvolvement.PaymentAmount = item.PaymentAmount;
                incidentInvolvement.Sex = item.Sex;
                incidentInvolvement.Status = item.Status;
                incidentInvolvement.Telephone = item.Telephone;
                incidentInvolvement.Title = item.Title;
                incidentInvolvement.Type = item.Type;
                incidentInvolvement.VAT = item.VAT;

                lstIncidentInvolvements.Add(incidentInvolvement);

            }

            return lstIncidentInvolvements;
        }

        private static ExCUEClaimData Translate(ECUEClaimData cueClaimData)
        {
            ExCUEClaimData claimData = new ExCUEClaimData();
            claimData.ClaimNumber = cueClaimData.ClaimNumber;
            claimData.Insurer = cueClaimData.Insurer;
            claimData.PolicyHolders = Translator.Translate(cueClaimData.PolicyHolders);

            return claimData;
        }




        private static List<ExCUEClaimDataPolicyHolder> Translate(List<ECUEClaimDataPolicyHolder> cueLstClaimDataPolicyHolders)
        {
            List<ExCUEClaimDataPolicyHolder> lstClaimDataPolicyHolders = new List<ExCUEClaimDataPolicyHolder>();

            foreach (var item in cueLstClaimDataPolicyHolders)
            {
                ExCUEClaimDataPolicyHolder claimDataPolicyHolder = new ExCUEClaimDataPolicyHolder();
                claimDataPolicyHolder.Address = item.Address;
                claimDataPolicyHolder.FullName = item.FullName;
                claimDataPolicyHolder.IncidentId = item.IncidentId;

                lstClaimDataPolicyHolders.Add(claimDataPolicyHolder);
            }

            return lstClaimDataPolicyHolders;
        }

        private static List<ExCUEIncidentSummary> Translate(List<ECUEIncidentSummary> cueLstIncidentSummary)
        {
            List<ExCUEIncidentSummary> lstIncidentSummary = new List<ExCUEIncidentSummary>();

            foreach (var item in cueLstIncidentSummary)
            {
                ExCUEIncidentSummary incidentSummary = new ExCUEIncidentSummary();
                incidentSummary.From = item.From;
                incidentSummary.HH_Claims = item.HH_Claims;
                incidentSummary.IncidentId = item.IncidentId;
                incidentSummary.MO_Claims = item.MO_Claims;
                incidentSummary.PI_Claims = item.PI_Claims;
                incidentSummary.To = item.To;
                incidentSummary.CUE_Incidents = Translator.Translate(item.CUE_Incidents);

                lstIncidentSummary.Add(incidentSummary);

            }

            return lstIncidentSummary;
        }

        private static List<ExCUESummaryIncident> Translate(List<ECUESummaryIncident> cueLstSummaryIncident)
        {
            List<ExCUESummaryIncident> lstSummaryIncidents = new List<ExCUESummaryIncident>();

            foreach (var item in cueLstSummaryIncident)
            {
                ExCUESummaryIncident summaryIncident = new ExCUESummaryIncident();
                summaryIncident.ClaimStatus = item.ClaimStatus;
                summaryIncident.Description = item.Description;
                summaryIncident.IncidentDate = item.IncidentDate;
                summaryIncident.IncidentId = item.IncidentId;
                summaryIncident.IncidentType = item.IncidentType;
                summaryIncident.Insurer = item.Insurer;
                summaryIncident.InvolvementType = item.InvolvementType;
                summaryIncident.MatchCode = item.MatchCode;
                summaryIncident.Subject = item.Subject;

                lstSummaryIncidents.Add(summaryIncident);
            }

            return lstSummaryIncidents;
        }

        private static ExCueSearchCriteria Translate(ECueSearchCriteria cueSearchCriteria)
        {
            return new ExCueSearchCriteria()
            {
                AddressCity = cueSearchCriteria.AddressCity,
                AddressHouseName = cueSearchCriteria.AddressHouseName,
                AddressLocality = cueSearchCriteria.AddressLocality,
                AddressNumber = cueSearchCriteria.AddressNumber,
                AddressPostCode = cueSearchCriteria.AddressPostCode,
                AddressStreet = cueSearchCriteria.AddressCity,
                AddressTown = cueSearchCriteria.AddressTown,
                Dob = cueSearchCriteria.Dob,
                DrivingLicenceNum = cueSearchCriteria.DrivingLicenceNum,
                Name = cueSearchCriteria.Name,
                NI_Number = cueSearchCriteria.NI_Number,
                VehicleReg = cueSearchCriteria.VehicleReg,
                VIN = cueSearchCriteria.VIN
            };
        }
        private static List<ExCUEResultsSummary> Translate(List<ECUEResultsSummary> cueLstResultsSummary)
        {
            List<ExCUEResultsSummary> lstResultsSummary = new List<ExCUEResultsSummary>();
            foreach (var item in cueLstResultsSummary)
            {
                ExCUEResultsSummary resultsSummary = new ExCUEResultsSummary();
                resultsSummary.Incidents = item.Incidents;
                resultsSummary.Involved_Household_Claims = item.Involved_Household_Claims;
                resultsSummary.Involved_Motor_Claims = item.Involved_Motor_Claims;
                resultsSummary.Involved_PI_Claims = item.Involved_PI_Claims;

                lstResultsSummary.Add(resultsSummary);
            }

            return lstResultsSummary;
        }

        private static List<ExCUESearchSummary> Translate(List<ECUESearchSummary> cueLstSearchSummary)
        {
            List<ExCUESearchSummary> lstSearchSummary = new List<ExCUESearchSummary>();

            foreach (var item in cueLstSearchSummary)
            {
                ExCUESearchSummary searchSummary = new ExCUESearchSummary();
                searchSummary.Cross_Enquiry = item.Cross_Enquiry;
                searchSummary.Household = item.Household;
                searchSummary.Involvement = item.Involvement;
                searchSummary.Motor = item.Motor;
                searchSummary.PI = item.PI;
                searchSummary.SearchDate = item.SearchDate;
                searchSummary.Subject = item.Subject;

                lstSearchSummary.Add(searchSummary);
            }

            return lstSearchSummary;
        }







        internal static WebsiteSaveTemplateFunctionsParam Translate(WebsiteSaveTemplateFunctionsRequest request)
        {
            return new WebsiteSaveTemplateFunctionsParam()
            {
                ClientId = request.ClientId,
                ListTemplateFunctionsJson = request.ListTemplateFunctionsJson,
                Template_Id = request.Template_Id,
                TemplateFunctionsJson = request.TemplateFunctionsJson,
                UserId = request.UserId,
                Who = request.Who
            };
        }

        internal static WebsiteRiskRoleCreateResponse Translate(WebsiteRiskRoleCreateResult result)
        {
            return new WebsiteRiskRoleCreateResponse()
            {
                Error = result.Error,
                Result = result.Result
            };
        }

        internal static SendElmahErrorResponse Translate(SendElmahErrorResult result)
        {
            return new SendElmahErrorResponse()
            {
                Error = result.Error,
                Result = result.Result
            };
        }


        internal static WebsiteRiskRoleCreateParam Translate(WebsiteRiskRoleCreateRequest request)
        {
            return new WebsiteRiskRoleCreateParam()
            {
                ClientId = request.ClientId,
                TemplateName = request.TemplateName,
                UserId = request.UserId,
                Who = request.Who
            };
        }


        internal static WebsiteRiskRoleEditResponse Translate(WebsiteRiskRoleEditResult result)
        {
            return new WebsiteRiskRoleEditResponse()
            {
                Error = result.Error,
                Result = result.Result
            };
        }

        internal static WebsiteRiskRoleEditParam Translate(WebsiteRiskRoleEditRequest request)
        {
            return new WebsiteRiskRoleEditParam()
            {
                ClientId = request.ClientId,
                TemplateName = request.TemplateName,
                UserId = request.UserId,
                Who = request.Who,
                Profile_Id = request.Profile_Id
            };
        }

        internal static WebsiteRiskRoleDeleteResponse Translate(WebsiteRiskRoleDeleteResult result)
        {
            return new WebsiteRiskRoleDeleteResponse()
            {
                Error = result.Error,
                Result = result.Result
            };
        }



        //private static List<ERiskClientBatchPriority> Translate(List<ERiskClientBatchPriority> list)
        //{
        //    List<ERiskClientBatchPriority> lstRiskClientBatchPriority = new List<ERiskClientBatchPriority>();

        //    foreach (var item in list)
        //    {
        //        lstRiskClientBatchPriority.Add(new ERiskClientBatchPriority { Client = item.Client, PriorityId = item.PriorityId });
        //    }

        //    return lstRiskClientBatchPriority;
        //}

        internal static SaveBatchPriorityClientsResponse Translate(SaveBatchPriorityClientsResult result)
        {
            return new SaveBatchPriorityClientsResponse()
            {
                Error = result.Error,
                Result = result.Result
            };
        }

        internal static SaveOverriddenBatchPriorityResponse Translate(SaveOverriddenBatchPriorityResult result)
        {
            return new SaveOverriddenBatchPriorityResponse()
            {
                Error = result.Error,
                Result = result.Result
            };
        }

        internal static GetBatchQueueResponse Translate(GetBatchQueueResult result)
        {
            return new GetBatchQueueResponse()
            {
                BatchQueue = Translate(result.BatchQueue)
            };
        }

        private static List<ExBatchQueue> Translate(List<ADAServices.Interface.EBatchQueue> list)
        {
            List<ExBatchQueue> batchQueue = new List<ExBatchQueue>();

            foreach (var item in list)
            {
                batchQueue.Add(new ExBatchQueue { BatchId = item.BatchId, BatchRef = item.BatchRef, ClaimsReceieved = item.ClaimsReceieved, Client = item.Client, Uploaded = item.Uploaded });
            }

            return batchQueue;
        }

        internal static WebsiteRiskRoleDeleteParam Translate(WebsiteRiskRoleDeleteRequest request)
        {
            return new WebsiteRiskRoleDeleteParam()
            {
                ClientId = request.ClientId,
                UserId = request.UserId,
                Who = request.Who,
                Profile_Id = request.Profile_Id
            };
        }

        internal static FindUserNameResponse Translate(FindUserNameResult result)
        {
            return new FindUserNameResponse()
            {
                Error = result.Error,
                Result = result.Result
            };
        }

        internal static GetListOfBatchPriorityClientsResponse Translate(GetListOfBatchPriorityClientsResult result)
        {
            return new GetListOfBatchPriorityClientsResponse()
            {
                Result = result.Result,
                Error = result.Error,
                ListRiskClientBatchPriority = Translate(result.ListRiskClientBatchPriority)
            };
        }

        private static List<ExRiskClientBatchPriority> Translate(List<ERiskClientBatchPriority> list)
        {
            List<ExRiskClientBatchPriority> lstRiskClientBatchPriority = new List<ExRiskClientBatchPriority>();

            foreach (var item in list)
            {
                lstRiskClientBatchPriority.Add(new ExRiskClientBatchPriority { Client = item.Client, PriorityId = item.PriorityId });
            }

            return lstRiskClientBatchPriority;
        }



        internal static FindUserNameParam Translate(FindUserNameRequest request)
        {
            return new FindUserNameParam()
            {
                UserName = request.UserName
            };
        }


        internal static WebsiteLoadTemplateFunctionsParam Translate(WebsiteLoadTemplateFunctionsRequest request)
        {
            return new WebsiteLoadTemplateFunctionsParam()
            {
                ClientId = request.ClientId,
                Template_Id = request.Template_Id,
                UserId = request.UserId,
                Who = request.Who
            };
        }

        internal static SendElmahErrorParam Translate(SendElmahErrorRequest request)
        {
            return new SendElmahErrorParam()
            {
                Error = request.Error
            };
        }
        internal static WebSearchResultsParam Translate(Entities.WebSearchResultsRequest request)
        {
            return new WebSearchResultsParam()
            {
                ClientId = request.ClientId,
                FilterDob = request.FilterDob,
                FilterFirstName = request.FilterFirstName,
                FilterSurname = request.FilterSurname,
                FilterAddressLine = request.FilterAddressLine,
                FilterAddressPostcode = request.FilterAddressPostcode,
                FilterVehicleRegNumber = request.FilterVehicleRegNumber,
                Page = request.Page,
                PageSize = request.PageSize,
                UserId = request.UserId,
                Who = request.Who
            };
        }

        internal static Entities.WebSearchResultsResponse Translate(WebSearchResultsResult result)
        {
            var res = new Entities.WebSearchResultsResponse()
            {
                Error = result.Error,
                Result = result.Result,
                TotalRows = result.TotalRows
            };

            res.SearchResultsList = new List<Entities.ExSearchDetails>();

            foreach (var r in result.SearchResultsList)
                res.SearchResultsList.Add(Translator.Translate(r));

            return res;

        }

        private static Entities.ExSearchDetails Translate(ADAServices.Interface.ESearchDetails r)
        {
            return new Entities.ExSearchDetails()
            {
                ClaimType = r.ClaimType,
                Date = r.Date,
                Involvement = r.Involvement,
                MatchedOn = r.MatchedOn,
                Title = r.Title,
                FirstName = r.FirstName,
                LastName = r.LastName,
                Address = r.Address,
                DateOfBirth = r.DateOfBirth,
                MiddleName = r.MiddleName,
                Postcode = r.Postcode,
                VehicleMake = r.VehicleMake,
                VehicleModel = r.VehicleModel,
                VehicleReg = r.VehicleReg,
                Reference = r.Reference,
                Source = r.Source,
                Status = r.Status
            };
        }

        internal static GetListOfBatchPriorityClientsParam Translate(GetListOfBatchPriorityClientsRequest request)
        {
            return new GetListOfBatchPriorityClientsParam()
            {
                ClientId = request.ClientId,
                UserId = request.UserId,
                Who = request.Who
            };
        }

        internal static SaveBatchPriorityClientsParam Translate(SaveBatchPriorityClientsRequest request)
        {
            return new SaveBatchPriorityClientsParam()
            {
                ClientBatchPriorityArray = request.ClientBatchPriorityArray,
                ClientId = request.ClientId,
                UserId = request.UserId,
                Who = request.Who
            };
        }

        internal static GetBatchQueueParam Translate(GetBatchQueueRequest request)
        {
            return new GetBatchQueueParam()
            {
                ClientId = request.ClientId,
                UserId = request.UserId,
                Who = request.Who
            };
        }


        internal static SaveOverriddenBatchPriorityParam Translate(SaveOverriddenBatchPriorityRequest request)
        {
            return new SaveOverriddenBatchPriorityParam()
            {
                ClientId = request.ClientId,
                UserId = request.UserId,
                Who = request.Who,
                OverriddenBatchPriorityArray = request.OverriddenBatchPriorityArray
            };
        }
    }
         
}
