﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MDA.Pipeline.Model;
using MDA.RiskService.Model;
using MDA.Common;
using MDA.WCF.WebServices;
using System.ServiceModel;

namespace MDA.WCF.WebServices.Entities
{
    [DataContract]
    public class SaveOverriddenBatchPriorityResponse
    {
        [DataMember]
        public int Result { get; set; }
       
        [DataMember]
        public string Error { get; set; }
    }

    [DataContract]
    public class SaveOverriddenBatchPriorityRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
        
        [DataMember]
        public int[] OverriddenBatchPriorityArray { get; set; }
    }
}