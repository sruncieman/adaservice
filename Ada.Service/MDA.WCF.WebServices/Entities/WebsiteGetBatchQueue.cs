﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MDA.Pipeline.Model;
using MDA.RiskService.Model;
using MDA.Common;
using MDA.WCF.WebServices;
using System.ServiceModel;

namespace MDA.WCF.WebServices.Entities
{
    [DataContract]
    public class GetBatchQueueRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class GetBatchQueueResponse
    {
        [DataMember]
        public List<ExBatchQueue> BatchQueue { get; set; }
    }

}