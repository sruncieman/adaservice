﻿using System;
using System.Runtime.Serialization;

namespace MDA.WCF.WebServices.Entities
{
    [DataContract]
    public class ExSearchDetails
    {
        [DataMember]
        public string MatchedOn { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public DateTime? DateOfBirth { get; set; }
     
        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Postcode { get; set; }

        [DataMember]
        public string VehicleReg { get; set; }

        [DataMember]
        public string VehicleMake { get; set; }

        [DataMember]
        public string VehicleModel { get; set; }

        [DataMember]
        public string Involvement { get; set; }

        [DataMember]
        public string ClaimType { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public DateTime? Date { get; set; }

        [DataMember]
        public string Source { get; set; }

        [DataMember]
        public string Reference { get; set; }
    }
}