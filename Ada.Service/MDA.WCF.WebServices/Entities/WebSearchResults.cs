﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MDA.WCF.WebServices.Entities
{
    [DataContract]
    public class WebSearchResultsRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int Page { get; set; }

        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public string FilterFirstName { get; set; }

        [DataMember]
        public string FilterSurname { get; set; }

        [DataMember]
        public DateTime? FilterDob { get; set; }

        [DataMember]
        public string FilterAddressLine { get; set; }

        [DataMember]
        public string FilterAddressPostcode { get; set; }

        [DataMember]
        public string FilterVehicleRegNumber { get; set; }
    }

    [DataContract]
    public class WebSearchResultsResponse
    {
        public WebSearchResultsResponse()
	    {
            SearchResultsList = new List<ExSearchDetails>();
	    }

        [DataMember]
        public List<ExSearchDetails> SearchResultsList { get; set; }

        /// <summary>
        /// 0 = success, -1 = error
        /// </summary>
        [DataMember]
        public int Result { get; set; }

        /// <summary>
        /// Error string indicating an error
        /// </summary>
        [DataMember]
        public string Error { get; set; }

        /// <summary>
        /// Total rows in ResultSet (not the number of rows in the page)
        /// </summary>
        [DataMember]
        public int TotalRows { get; set; }
    }
}