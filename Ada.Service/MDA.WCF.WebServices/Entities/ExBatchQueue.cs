﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDA.WCF.WebServices.Entities
{
    public class ExBatchQueue
    {
        public int BatchId { get; set; }
        public string Client { get; set; }
        public string BatchRef { get; set; }
        public DateTime? Uploaded { get; set; }
        public int ClaimsReceieved { get; set; }
    }
}