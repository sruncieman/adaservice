﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPerson2PersonCardAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2Person set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectPerson2PersonLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Person set Person1_Id = " + masterPersonId +
                                           " where Person1_Id = " + personId);

            _db.Database.ExecuteSqlCommand("update Person2Person set Person2_Id = " + masterPersonId +
                                           " where Person2_Id = " + personId);
        }

        public List<Person2Person> GetPerson2PersonLinks(int person1Id, int person2Id) //, int baseRiskClaimId)
        {
            return (from x in _db.Person2Person
                    where ((x.Person1_Id == person1Id && x.Person2_Id == person2Id) || (x.Person2_Id == person1Id && x.Person1_Id == person2Id))
                    //orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Person2Person GetLatestPerson2PersonLink(int person1Id, int person2Id) //, int baseRiskClaimId)
        {
            return (from x in _db.Person2Person
                    where ((x.Person1_Id == person1Id && x.Person2_Id == person2Id) || (x.Person2_Id == person1Id && x.Person1_Id == person2Id)) &&
                          (x.ADARecordStatus == (byte)ADARecordStatus.Current || x.ADARecordStatus == (byte)ADARecordStatus.SyncCurrent)
                    select x).FirstOrDefault();
        }

        //public void CleanUpPerson2PersonLinks(/*Person2Person latestLink,*/ RiskClaim newRiskClaim)
        //{
        //    var oldlinks = (from x in _db.Person2Person
        //                    where //(x.Person1_Id == latestLink.Person1_Id || x.Person2_Id == latestLink.Person2_Id) &&
        //                          x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
        //                          x.RiskClaim_Id != newRiskClaim.Id &&
        //                          x.ADARecordStatus == (byte)ADARecordStatus.Current &&
        //                          x.SourceDescription == "ADA Auto-Matching"
        //                    select x).ToList();

        //    foreach (var link in oldlinks)
        //    {
        //        if (ctx.Tracing)
        //            ADATrace.WriteLine("Person2Person Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

        //        link.RecordStatus = Constants.IBaseDeleteValue;
        //        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
        //    }
        //    _db.SaveChanges();
        //}

        public Person2Person UpdatePerson2PersonLink(MDA.Common.Enum.Operation op, Person2Person link,
                                                    //MDA.Common.Enum.Person2PersonLinkType linkType, 
                                                    MDA.Pipeline.Model.PipelinePerson2PersonLink pipeLink,
                                                    /*RiskClaim newRiskClaim, */ ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        //link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence && link.LinkConfidence > (int)pipeLink.LinkConfidence)
                        {
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;
                        }

                        if (link.Person2PersonLinkType_Id != (int)pipeLink.Person2PersonLinkType_Id) //linkType)
                            link.Person2PersonLinkType_Id = (int)pipeLink.Person2PersonLinkType_Id;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Person2Person UpdatePerson2PersonLink(MDA.Common.Enum.Operation op, int linkId,
                                                //MDA.Common.Enum.Person2PersonLinkType linkType, 
                                                MDA.Pipeline.Model.PipelinePerson2PersonLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Person2Person where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePerson2PersonLink(op, link, /*linkType,*/ pipeLink, /*claim,*/ ref success);
        }


        public Person2Person InsertPerson2PersonLink(int person1Id, int person2Id, MDA.Pipeline.Model.PipelinePerson2PersonLink pipeLink )//, MDA.Common.Enum.Person2PersonLinkType linkType) //, RiskClaim claim)
        {
            Person2Person p2p = _db.Person2Person.Create();

            p2p.CreatedBy         = pipeLink.CreatedBy;
            p2p.CreatedDate       = pipeLink.CreatedDate;
            p2p.ModifiedBy        = pipeLink.ModifiedBy;
            p2p.ModifiedDate      = pipeLink.ModifiedDate;
            p2p.Source            = pipeLink.Source;
            p2p.SourceDescription = pipeLink.SourceDescription;
            p2p.SourceReference   = pipeLink.SourceReference;
            p2p.ADARecordStatus   = pipeLink.ADARecordStatus;
            p2p.RecordStatus      = pipeLink.RecordStatus;
            p2p.LinkConfidence    = pipeLink.LinkConfidence;

            //p2p.RiskClaim_Id = claim.Id;
            //p2p.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2p.Person1_Id = person1Id;
            p2p.Person2_Id = person2Id;
            p2p.Person2PersonLinkType_Id = (int)pipeLink.Person2PersonLinkType_Id; // linkType;

            p2p.Source = "Keoghs CFS";
            p2p.SourceDescription = "ADA Auto-Matching";
            p2p.SourceReference = null;
            p2p.FiveGrading = GetFiveGrading();

            _db.Person2Person.Add(p2p);

            _db.SaveChanges();

            return p2p;
        }


        //public Person2Person InsertPerson2PersonLink(Person2Person person2person)
        //{
        //    Person2Person p2p = _db.Person2Person.Create();

        //    p2p.CreatedBy = person2person.CreatedBy;
        //    p2p.CreatedDate = DateTime.Now;
        //    //#p2p.BaseRiskClaim_Id = person2person.BaseRiskClaim_Id;

        //    p2p.Source = person2person.Source;
        //    p2p.SourceDescription = person2person.SourceDescription;
        //    p2p.SourceReference = person2person.SourceReference;

        //    p2p.Person1_Id = person2person.Person1_Id;
        //    p2p.Person2_Id = person2person.Person2_Id;
        //    p2p.Person2PersonLinkId = person2person.Person2PersonLinkId;
        //    p2p.Person2PersonLinkType_Id = person2person.Person2PersonLinkType_Id;
        //    p2p.LinkConfidence = person2person.LinkConfidence;

        //    p2p.IBaseId = person2person.IBaseId;
        //    //#p2p.RiskClaim_Id = person2person.RiskClaim_Id;
        //    p2p.LinkConfidence = person2person.LinkConfidence;
        //    p2p.FiveGrading = person2person.FiveGrading;

        //    _db.Person2Person.Add(p2p);

        //    _db.SaveChanges();

        //    return p2p;
        //}

    }

}
