﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPerson2PolicyAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2Policy set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectPolicy2PersonLinks(int masterPolicyId, int policyId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Policy set Policy_Id = " + masterPolicyId +
                                           " where Policy_Id = " + policyId);
        }

        public void RedirectPerson2PolicyLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Policy set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        public List<Person2Policy> GetPerson2PolicyLinks(int personId, int policyId, int baseRiskClaimId)
        {
            return (from x in _db.Person2Policy
                    where x.Person_Id == personId && x.Policy_Id == policyId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Person2Policy GetLatestPerson2PolicyLink(int personId, int policyId, int baseRiskClaimId)
        {
            return (from x in _db.Person2Policy
                    where x.Person_Id == personId && x.Policy_Id == policyId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPerson2PolicyLinks(/*Person2Policy latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Person2Policy
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Person2Policy Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Person2Policy UpdatePerson2PolicyLink(MDA.Common.Enum.Operation op, Person2Policy link,
                                               MDA.Pipeline.Model.PipelinePolicyLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {

            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (link.PolicyLinkType_Id != 0 && link.PolicyLinkType_Id != (int)pipeLink.PolicyLinkType_Id) // we have value and it's different
                    {
                        success = false;
                    }

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.PolicyLinkType_Id != (int)pipeLink.PolicyLinkType_Id)
                            link.PolicyLinkType_Id = (int)pipeLink.PolicyLinkType_Id;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Person2Policy UpdatePerson2PolicyLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Pipeline.Model.PipelinePolicyLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Person2Policy where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePerson2PolicyLink(op, link, pipeLink, claim, ref success);
        }

        public Person2Policy InsertPerson2PolicyLink(int personId, int policyId,
                                        MDA.Pipeline.Model.PipelinePolicyLink pipeLink, RiskClaim claim)
        {
            Person2Policy p2p = _db.Person2Policy.Create();

            p2p.CreatedBy         = pipeLink.CreatedBy;
            p2p.CreatedDate       = pipeLink.CreatedDate;
            p2p.ModifiedBy        = pipeLink.ModifiedBy;
            p2p.ModifiedDate      = pipeLink.ModifiedDate;
            p2p.Source            = pipeLink.Source;
            p2p.SourceDescription = pipeLink.SourceDescription;
            p2p.SourceReference   = pipeLink.SourceReference;
            p2p.ADARecordStatus   = pipeLink.ADARecordStatus;
            p2p.RecordStatus      = pipeLink.RecordStatus;
            p2p.LinkConfidence    = pipeLink.LinkConfidence;

            p2p.RiskClaim_Id = claim.Id;
            p2p.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2p.Person_Id = personId;
            p2p.Policy_Id = policyId;
            p2p.PolicyLinkType_Id = (int)pipeLink.PolicyLinkType_Id;
            
            p2p.FiveGrading = GetFiveGrading();

            _db.Person2Policy.Add(p2p);

            _db.SaveChanges();

            return p2p;
        }

        //public Person2Policy InsertPerson2PolicyLink(Person2Policy person2policy)
        //{
        //    Person2Policy p2p = _db.Person2Policy.Create();

        //    p2p.CreatedBy = person2policy.CreatedBy;
        //    p2p.CreatedDate = DateTime.Now;
        //    p2p.BaseRiskClaim_Id = person2policy.BaseRiskClaim_Id;

        //    p2p.Source = person2policy.Source;
        //    p2p.SourceDescription = person2policy.SourceDescription;
        //    p2p.SourceReference = person2policy.SourceReference;

        //    p2p.Person_Id = person2policy.Person_Id;
        //    p2p.Policy_Id = person2policy.Policy_Id;
        //    p2p.PolicyLinkId = person2policy.PolicyLinkId;
        //    p2p.PolicyLinkType_Id = person2policy.PolicyLinkType_Id;

        //    p2p.IBaseId = person2policy.IBaseId;
        //    p2p.RiskClaim_Id = person2policy.RiskClaim_Id;
        //    p2p.LinkConfidence = person2policy.LinkConfidence;
        //    p2p.FiveGrading = person2policy.FiveGrading;

        //    _db.Person2Policy.Add(p2p);

        //    _db.SaveChanges();

        //    return p2p;
        //}
    }

}
