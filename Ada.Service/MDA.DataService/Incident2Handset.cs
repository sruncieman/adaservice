﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public Incident2Handset GetLatestIncident2HandsetLink(int incidentId, int handsetId, int baseRiskClaimId)
        {
            return (from x in _db.Incident2Handset
                    where x.Incident_Id == incidentId && x.Handset_Id == handsetId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public Incident2Handset InsertIncident2HandsetLink(int incidentId, int handsetId,
                                                    MDA.Pipeline.Model.PipelineIncident2HandsetLink pipeLink,
            //MDA.Common.Enum.VehicleCategoryOfLoss catLoss, 
            //string damageDesc, 
            //MDA.Common.Enum.Incident2VehicleLinkType linkType, 
                                                    RiskClaim claim)
        {
            Incident2Handset i2h = _db.Incident2Handset.Create();

            i2h.CreatedBy = pipeLink.CreatedBy;
            i2h.CreatedDate = pipeLink.CreatedDate;
            i2h.ModifiedBy = pipeLink.ModifiedBy;
            i2h.ModifiedDate = pipeLink.ModifiedDate;
            i2h.Source = pipeLink.Source;
            i2h.SourceDescription = pipeLink.SourceDescription;
            i2h.SourceReference = pipeLink.SourceReference;
            i2h.ADARecordStatus = pipeLink.ADARecordStatus;
            i2h.RecordStatus = pipeLink.RecordStatus;
            i2h.LinkConfidence = pipeLink.LinkConfidence;

            i2h.RiskClaim_Id = claim.Id;
            i2h.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            i2h.Handset_Id = handsetId;
            i2h.Incident_Id = incidentId;
            i2h.DeviceFault = pipeLink.DeviceFault;
            i2h.HandsetValueCategory = pipeLink.HandsetValueCategory;
            i2h.Incident2HandsetLinkType_Id = (int)pipeLink.Incident2HandsetLinkType_Id;
            i2h.FiveGrading = GetFiveGrading();

            _db.Incident2Handset.Add(i2h);

            _db.SaveChanges();

            return i2h;
        }

        public Incident2Handset UpdateIncident2HandsetLink(
                                           MDA.Common.Enum.Operation op, int linkId,
            //MDA.Common.Enum.VehicleCategoryOfLoss catLoss, string damageDesc, 
            //MDA.Common.Enum.Incident2VehicleLinkType linkType,
                                           MDA.Pipeline.Model.PipelineIncident2HandsetLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Incident2Handset where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateIncident2HandsetLink(op, link, pipeLink, /*catLoss, damageDesc, linkType,*/ claim, ref success);
        }


        public Incident2Handset UpdateIncident2HandsetLink(MDA.Common.Enum.Operation op, Incident2Handset link,
                                                    MDA.Pipeline.Model.PipelineIncident2HandsetLink pipeLink,
            //MDA.Common.Enum.VehicleCategoryOfLoss catLoss, string damageDesc, 
            //MDA.Common.Enum.Incident2VehicleLinkType linkType,
                                                    RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.Incident2HandsetLinkType_Id != (int)pipeLink.Incident2HandsetLinkType_Id)
                            link.Incident2HandsetLinkType_Id = (int)pipeLink.Incident2HandsetLinkType_Id;

                        if (link.DeviceFault != pipeLink.DeviceFault)
                            link.DeviceFault = pipeLink.DeviceFault;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public void CleanUpIncident2HandsetLinks(/*Incident2Handset latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all I2H links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Incident2Handset
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Incident2Handset Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }
    }
}
