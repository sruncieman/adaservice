﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {

        public List<Handset2Policy> GetHandset2PolicyLinks(int HandsetId, int policyId, int baseRiskClaimId)
        {
            return (from x in _db.Handset2Policy
                    where x.Policy_Id == policyId && x.Handset_Id == HandsetId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Handset2Policy GetLatestHandset2PolicyLink(int HandsetId, int policyId, int baseRiskClaimId)
        {
            return (from x in _db.Handset2Policy
                    where x.Policy_Id == policyId && x.Handset_Id == HandsetId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void RedirectHandset2PolicyLinks(int masterHandsetId, int HandsetId)
        {
            _db.Database.ExecuteSqlCommand("update Handset2Policy set Handset_Id = " + masterHandsetId +
                                           " where Handset_Id = " + HandsetId);
        }

        public void RedirectPolicy2HandsetLinks(int masterPolicyId, int policyId)
        {
            _db.Database.ExecuteSqlCommand("update Handset2Policy set Policy_Id = " + masterPolicyId +
                                           " where Policy_Id = " + policyId);
        }

        public void CleanUpHandset2PolicyLinks(/*Handset2Policy latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Handset2Policy
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Handset2Policy Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Handset2Policy UpdateHandset2PolicyLink(MDA.Common.Enum.Operation op, Handset2Policy link,
            //MDA.Common.Enum.PolicyLinkType linkType,
                                            MDA.Pipeline.Model.PipelineHandset2PolicyLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Handset.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.PolicyLinkType_Id != (int)pipeLink.PolicyLinkType_Id)
                            link.PolicyLinkType_Id = (int)pipeLink.PolicyLinkType_Id;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;

        }


        public Handset2Policy UpdateHandset2PolicyLink(MDA.Common.Enum.Operation op, int linkId,
            //MDA.Common.Enum.PolicyLinkType linkType,
                                            MDA.Pipeline.Model.PipelineHandset2PolicyLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Handset2Policy where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateHandset2PolicyLink(op, link, pipeLink, claim, ref success);
        }


        public Handset2Policy InsertHandset2PolicyLink(int HandsetId, int policyId,
                                MDA.Pipeline.Model.PipelineHandset2PolicyLink pipeLink,
                                RiskClaim claim)
        {
            Handset2Policy v2p = _db.Handset2Policy.Create();

            v2p.CreatedBy = pipeLink.CreatedBy;
            v2p.CreatedDate = pipeLink.CreatedDate;
            v2p.ModifiedBy = pipeLink.ModifiedBy;
            v2p.ModifiedDate = pipeLink.ModifiedDate;
            v2p.Source = pipeLink.Source;
            v2p.SourceDescription = pipeLink.SourceDescription;
            v2p.SourceReference = pipeLink.SourceReference;
            v2p.ADARecordStatus = pipeLink.ADARecordStatus;
            v2p.RecordStatus = pipeLink.RecordStatus;
            v2p.LinkConfidence = pipeLink.LinkConfidence;

            v2p.RiskClaim_Id = claim.Id;
            v2p.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            v2p.Policy_Id = policyId;
            v2p.Handset_Id = HandsetId;
            v2p.PolicyLinkType_Id = pipeLink.PolicyLinkType_Id;

            v2p.FiveGrading = GetFiveGrading();

            _db.Handset2Policy.Add(v2p);

            _db.SaveChanges();

            return v2p;
        }
    }
}
