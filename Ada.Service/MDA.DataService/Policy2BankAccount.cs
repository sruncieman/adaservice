﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{

    public partial class DataServices
    {
        public void RedirectBankAccount2PolicyLinks(int masterBankAccountId, int bankAccountId)
        {
            _db.Database.ExecuteSqlCommand("update Policy2BankAccount set BankAccount_Id = " + masterBankAccountId +
                                           " where BankAccount_Id = " + bankAccountId);
        }

        public void RedirectPolicy2BankAccountLinks(int masterPolicyId, int policyId)
        {
            _db.Database.ExecuteSqlCommand("update Policy2BankAccount set Policy_Id = " + masterPolicyId +
                                           " where Policy_Id = " + policyId);
        }

        //public void ResetAllPolicy2BankAccountAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Policy2BankAccount set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}
        public List<Policy2BankAccount> GetPolicy2BankAccountLinks(int policyId, int accountId, int baseRiskClaimId)
        {
            return (from x in _db.Policy2BankAccount
                    where x.Policy_Id == policyId && x.BankAccount_Id == accountId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Policy2BankAccount GetLatestPolicy2BankAccountLink(int policyId, int accountId, int baseRiskClaimId)
        {
            return (from x in _db.Policy2BankAccount
                    where x.Policy_Id == policyId && x.BankAccount_Id == accountId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPolicy2BankAccountLinks(/*Policy2BankAccount latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Policy2BankAccount
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Policy2Bank Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Policy2BankAccount UpdatePolicy2BankAccountLink(MDA.Common.Enum.Operation op, Policy2BankAccount link,
                                MDA.Pipeline.Model.PipelinePolicy2BankAccountLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (link.PolicyPaymentType_Id != 0 && link.PolicyPaymentType_Id != (int)pipeLink.PolicyPaymentType_Id) // we have value and it's different
                    {
                        success = false;
                    }

                    if (link.DatePaymentDetailsTaken != null && link.DatePaymentDetailsTaken != pipeLink.DatePaymentDetailsTaken) // we have value and it's different
                    {
                        success = false;
                    }

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;


                        if (link.PolicyPaymentType_Id != (int)pipeLink.PolicyPaymentType_Id)
                            link.PolicyPaymentType_Id = (int)pipeLink.PolicyPaymentType_Id;

                        if (link.DatePaymentDetailsTaken != MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(pipeLink.DatePaymentDetailsTaken))
                            link.DatePaymentDetailsTaken = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(pipeLink.DatePaymentDetailsTaken);
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;
        }


        public Policy2BankAccount UpdatePolicy2BankAccountLink(MDA.Common.Enum.Operation op, int linkId,
                                MDA.Pipeline.Model.PipelinePolicy2BankAccountLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Policy2BankAccount where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePolicy2BankAccountLink(op, link, pipeLink, claim, ref success);
        }

        public Policy2BankAccount InsertPolicy2BankAccountLink(int policyId, int accountId,
                                                    MDA.Pipeline.Model.PipelinePolicy2BankAccountLink pipeLink, RiskClaim claim)
        {
            Policy2BankAccount p2a = _db.Policy2BankAccount.Create();

            p2a.CreatedBy         = pipeLink.CreatedBy;
            p2a.CreatedDate       = pipeLink.CreatedDate;
            p2a.ModifiedBy        = pipeLink.ModifiedBy;
            p2a.ModifiedDate      = pipeLink.ModifiedDate;
            p2a.Source            = pipeLink.Source;
            p2a.SourceDescription = pipeLink.SourceDescription;
            p2a.SourceReference   = pipeLink.SourceReference;
            p2a.ADARecordStatus   = pipeLink.ADARecordStatus;
            p2a.RecordStatus      = pipeLink.RecordStatus;
            p2a.LinkConfidence    = pipeLink.LinkConfidence;

            p2a.RiskClaim_Id = claim.Id;
            p2a.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2a.Policy_Id = policyId;
            p2a.BankAccount_Id = accountId;

            p2a.PolicyPaymentType_Id = (int)pipeLink.PolicyPaymentType_Id;
            p2a.DatePaymentDetailsTaken = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(pipeLink.DatePaymentDetailsTaken);

            //if (pipeLink.DatePaymentDetailsTaken.HasValue)
            //    p2a.DatePaymentDetailsTaken = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(pipeLink.DatePaymentDetailsTaken);
            //else
            //    p2a.DatePaymentDetailsTaken = null;

            p2a.FiveGrading = GetFiveGrading();

            _db.Policy2BankAccount.Add(p2a);

            _db.SaveChanges();

            return p2a;
        }

        //public Policy2BankAccount InsertPolicy2BankAccountLink(Policy2BankAccount policy2bankaccount)
        //{
        //    Policy2BankAccount p2b = _db.Policy2BankAccount.Create();

        //    p2b.CreatedBy = policy2bankaccount.CreatedBy;
        //    p2b.CreatedDate = DateTime.Now;
        //    p2b.BaseRiskClaim_Id = policy2bankaccount.BaseRiskClaim_Id;

        //    p2b.Source = policy2bankaccount.Source;
        //    p2b.SourceDescription = policy2bankaccount.SourceDescription;
        //    p2b.SourceReference = policy2bankaccount.SourceReference;

        //    p2b.BankAccount_Id = policy2bankaccount.BankAccount_Id;
        //    p2b.DatePaymentDetailsTaken = policy2bankaccount.DatePaymentDetailsTaken;
        //    p2b.PolicyPaymentType_Id = policy2bankaccount.PolicyPaymentType_Id;
        //    p2b.Policy_Id = policy2bankaccount.Policy_Id;
        //    p2b.PolicyPaymentLinkId = policy2bankaccount.PolicyPaymentLinkId;

        //    p2b.IBaseId = policy2bankaccount.IBaseId;
        //    p2b.RiskClaim_Id = policy2bankaccount.RiskClaim_Id;
        //    p2b.LinkConfidence = policy2bankaccount.LinkConfidence;
        //    p2b.FiveGrading = policy2bankaccount.FiveGrading;

        //    _db.Policy2BankAccount.Add(p2b);

        //    _db.SaveChanges();

        //    return p2b;
        //}
    }

}
