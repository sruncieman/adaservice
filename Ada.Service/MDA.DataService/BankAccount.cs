﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Helpers;
using MDA.Common;

namespace MDA.DataService
{

    public partial class DataServices
    {
        //public void ResetAllBankAccountAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update BankAccount set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}
        public BankAccount GetBankAccount(int id)
        {
            return GetBankAccount(id);
        }

        public List<int> GetAllSyncBankAccountIds()
        {
            return (from x in _db.BankAccounts where x.ADARecordStatus >= 10 select x.Id).ToList();
        }

        public void ResetSyncBankAccountAdaStatus(int id)
        {
            var r = (from x in _db.BankAccounts where x.Id == id select x).FirstOrDefault();

            if (r != null && r.ADARecordStatus >= 10)
            {
                r.ADARecordStatus -= 10;

                _db.SaveChanges();
            }
        }

        public void CleanseBankAccount(int id, Func<BankAccount, MessageNode, int> cleanseMethod)
        {
            var r = (from x in _db.BankAccounts where x.Id == id select x).FirstOrDefault();

            if (r == null) return;

            MessageNode n = new MessageNode();

            cleanseMethod(r, n);

            _db.SaveChanges();
        }

        public void DeduplicateBankAccountList(int masterBankAccountId, List<int> matchList)
        {
            foreach (var i in matchList)
            {
                var ir = (from x in _db.BankAccounts where x.Id == i select x).FirstOrDefault();

                if (ir != null)
                {
                    RedirectBankAccount2OrganisationLinks(masterBankAccountId, i);
                    RedirectBankAccount2PolicyLinks(masterBankAccountId, i);
                    RedirectBankAccount2PersonLinks(masterBankAccountId, i);

                    ir.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                    ir.RecordStatus = Constants.IBaseDeleteValue;

                    _db.SaveChanges();
                }
            }
        }

        public BankAccount GetBankAccount(MdaDbContext db, int id)
        {
            return (from x in db.BankAccounts where x.Id == id select x).FirstOrDefault();
        }

        public List<int> VerifyBankAccountListExists(List<int> listId)
        {
            List<int> outList = new List<int>();

            foreach (int id in listId)
            {
                if (_db.BankAccounts.Any(x => x.Id == id))
                    outList.Add(id);
            }
            return outList;
        }

        public BankAccount UpdateBankAccount(int bankAccountId, MDA.Pipeline.Model.PipelineBankAccount bankAccount, out bool success)
        {
            success = false;

            var ba = (from x in _db.BankAccounts where x.Id == bankAccountId select x).FirstOrDefault();
            if (ba == null) return ba;

            try
            {
                success = true;

                bool modified = false;

                if (ba.AccountNumber == null)
                {
                    ba.AccountNumber = bankAccount.AccountNumber;
                    modified = true;
                }

                if (ba.BankName == null)
                {
                    ba.BankName = bankAccount.BankName;
                    modified = true;
                }

                if (ba.SortCode == null)
                {
                    ba.SortCode = bankAccount.SortCode;
                    modified = true;
                }

                if (modified)
                {
                    ba.ModifiedDate = DateTime.Now;
                    ba.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
            }

            catch (Exception)
            {
                success = false;
            }
            return ba;
        }

        //public BankAccount InsertBankAccount(string accountNumber, string bankName, string sortCode)
        //{
        //    BankAccount e = _db.BankAccounts.Create();

        //    e.CreatedBy = _ctx.Who;
        //    e.CreatedDate = DateTime.Now;
        //    e.AccountNumber = accountNumber;
        //    e.BankName = bankName;
        //    e.SortCode = sortCode;
        //    e.Source = ctx.RiskClient.ClientName;
        //    e.SourceDescription = GetSourceDescription();
        //    e.SourceReference = ctx.SourceRef;

        //    _db.BankAccounts.Add(e);

        //    _db.SaveChanges();

        //    return e;
        //}

        public BankAccount InsertBankAccount(MDA.Pipeline.Model.PipelineBankAccount bankAccount)
        {
            BankAccount e = _db.BankAccounts.Create();

            e.AccountId         = bankAccount.AccountId;
            e.AccountNumber     = bankAccount.AccountNumber;
            e.SortCode          = bankAccount.SortCode;
            e.BankName          = bankAccount.BankName;
            e.KeyAttractor      = bankAccount.KeyAttractor;
            e.Source            = bankAccount.Source;
            e.SourceDescription = bankAccount.SourceDescription;
            e.SourceReference   = bankAccount.SourceReference;
            e.CreatedBy         = bankAccount.CreatedBy;
            e.CreatedDate       = bankAccount.CreatedDate;
            e.ModifiedBy        = bankAccount.ModifiedBy;
            e.ModifiedDate      = bankAccount.ModifiedDate;
            e.IBaseId           = bankAccount.IBaseId;
            e.RecordStatus      = bankAccount.RecordStatus;
            e.ADARecordStatus   = bankAccount.ADARecordStatus;

            _db.BankAccounts.Add(e);

            _db.SaveChanges();

            return e;
        }

        //public BankAccount InsertBankAccount(BankAccount account)
        //{
        //    BankAccount a = _db.BankAccounts.Create();

        //    a.CreatedBy = account.CreatedBy;
        //    a.CreatedDate = account.CreatedDate;
        //    a.AccountId = account.AccountId;
        //    a.AccountNumber = account.AccountNumber;
        //    a.BankName = account.BankName;
        //    a.IBaseId = account.IBaseId;
        //    a.SortCode = account.SortCode;
        //    a.Source = account.Source;
        //    a.SourceDescription = account.SourceDescription;
        //    a.SourceReference = account.SourceReference;

        //    _db.BankAccounts.Add(a);

        //    _db.SaveChanges();

        //    return a;
        //}

        public BankAccount FindBankAccount(string accountNumber, string sortCode)
        {
            return (from x in _db.BankAccounts where x.AccountNumber == accountNumber && x.SortCode == sortCode select x).FirstOrDefault();
        }

    }

}
