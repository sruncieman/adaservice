﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllOrganisation2TelephoneAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Organisation2Telephone set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectTelephone2OrganisationLinks(int masterTelephoneId, int telephoneId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2Telephone set Telephone_Id = " + masterTelephoneId +
                                           " where Telephone_Id = " + telephoneId);
        }

        public void RedirectOrganisation2TelephoneLinks(int masterOrganisationId, int organisationId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2Telephone set Organisation_Id = " + masterOrganisationId +
                                           " where Organisation_Id = " + organisationId);
        }

        public List<Organisation2Telephone> GetOrganisation2TelephoneLinks(int organisationId, int telephoneId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2Telephone
                    where x.Organisation_Id == organisationId && x.Telephone_Id == telephoneId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Organisation2Telephone GetLatestOrganisation2TelephoneLink(int organisationId, int telephoneId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2Telephone
                    where x.Organisation_Id == organisationId && x.Telephone_Id == telephoneId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpOrganisation2TelephoneLinks(/*Organisation2Telephone latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Organisation2Telephone
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Organisation2Telephone Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Organisation2Telephone UpdateOrganisation2TelephoneLink(MDA.Common.Enum.Operation op, Organisation2Telephone link,
                                                    //MDA.Common.Enum.TelephoneLinkType linkType, 
                                                    MDA.Pipeline.Model.PipelineTelephoneLink pipeLink,
                                                    RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.TelephoneLinkType_Id != (int)pipeLink.TelephoneLinkType_Id) //linkType)
                            link.TelephoneLinkType_Id = (int)pipeLink.TelephoneLinkType_Id; //linkType;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Organisation2Telephone UpdateOrganisation2TelephoneLink(MDA.Common.Enum.Operation op, int linkId,
                                               //MDA.Common.Enum.TelephoneLinkType linkType, 
                                                MDA.Pipeline.Model.PipelineTelephoneLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Organisation2Telephone where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateOrganisation2TelephoneLink(op, link, pipeLink, claim, ref success);
        }

        public Organisation2Telephone InsertOrganisation2TelephoneLink(int orgId, int telephoneId, MDA.Pipeline.Model.PipelineTelephoneLink pipeLink, 
                                                                //MDA.Common.Enum.TelephoneLinkType linkType, 
                                                                RiskClaim claim)
        {
            Organisation2Telephone o2t = _db.Organisation2Telephone.Create();

            o2t.CreatedBy         = pipeLink.CreatedBy;
            o2t.CreatedDate       = pipeLink.CreatedDate;
            o2t.ModifiedBy        = pipeLink.ModifiedBy;
            o2t.ModifiedDate      = pipeLink.ModifiedDate;
            o2t.Source            = pipeLink.Source;
            o2t.SourceDescription = pipeLink.SourceDescription;
            o2t.SourceReference   = pipeLink.SourceReference;
            o2t.ADARecordStatus   = pipeLink.ADARecordStatus;
            o2t.RecordStatus      = pipeLink.RecordStatus;
            o2t.LinkConfidence    = pipeLink.LinkConfidence;

            o2t.RiskClaim_Id = claim.Id;
            o2t.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            o2t.Organisation_Id = orgId;
            o2t.Telephone_Id = telephoneId;

            o2t.FiveGrading = GetFiveGrading();
            o2t.TelephoneLinkType_Id = (int)pipeLink.TelephoneLinkType_Id; // linkType;

            _db.Organisation2Telephone.Add(o2t);

            _db.SaveChanges();

            return o2t;
        }

        //public Organisation2Telephone InsertOrganisation2TelephoneLink(Organisation2Telephone org2phone)
        //{
        //    Organisation2Telephone o2t = _db.Organisation2Telephone.Create();

        //    o2t.CreatedBy = org2phone.CreatedBy;
        //    o2t.CreatedDate = DateTime.Now;
        //    o2t.BaseRiskClaim_Id = org2phone.BaseRiskClaim_Id;

        //    o2t.Source = org2phone.Source;
        //    o2t.SourceDescription = org2phone.SourceDescription;
        //    o2t.SourceReference = org2phone.SourceReference;
        //    o2t.IBaseId = org2phone.IBaseId;

        //    o2t.Organisation_Id = org2phone.Organisation_Id;
        //    o2t.Telephone_Id = org2phone.Telephone_Id;
        //    o2t.TelephoneLinkId = org2phone.TelephoneLinkId;
        //    o2t.LinkConfidence = org2phone.LinkConfidence;
        //    o2t.FiveGrading = org2phone.FiveGrading;
        //    o2t.TelephoneLinkType_Id = (int)MDA.Common.Enum.TelephoneLinkType.Uses;

        //    o2t.RiskClaim_Id = org2phone.RiskClaim_Id;

        //    _db.Organisation2Telephone.Add(o2t);

        //    _db.SaveChanges();

        //    return o2t;
        //}
    }

}
