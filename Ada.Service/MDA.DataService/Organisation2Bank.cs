﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public void RedirectBankAccount2OrganisationLinks(int masterBankAccountId, int bankAccountId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2BankAccount set BankAccount_Id = " + masterBankAccountId +
                                           " where BankAccount_Id = " + bankAccountId);
        }

        public void RedirectOrganisation2BankAccountLinks(int masterOrganisationId, int organisationId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2BankAccount set Organisation_Id = " + masterOrganisationId +
                                           " where Organisation_Id = " + organisationId);
        }

        //public void ResetAllOrganisation2BankAccountAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Organisation2BankAccount set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}
        public List<Organisation2BankAccount> GetOrganisation2BankAccountLinks(int organisationId, int accountId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2BankAccount
                    where x.Organisation_Id == organisationId && x.BankAccount_Id == accountId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Organisation2BankAccount GetLatestOrganisation2BankAccountLink(int organisationId, int accountId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2BankAccount
                    where x.Organisation_Id == organisationId && x.BankAccount_Id == accountId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpOrganisation2BankAccountLinks(/*Organisation2BankAccount latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2BA links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Organisation2BankAccount
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Organisation2BankAccount Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Organisation2BankAccount UpdateOrganisation2BankAccountLink(MDA.Common.Enum.Operation op, Organisation2BankAccount link,
                                                                            MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeBase.LinkConfidence)
                            link.LinkConfidence = (int)pipeBase.LinkConfidence;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Organisation2BankAccount UpdateOrganisation2BankAccountLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Organisation2BankAccount where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateOrganisation2BankAccountLink(op, link, pipeBase, claim, ref success);
        }

        public Organisation2BankAccount InsertOrganisation2BankAccountLink(int organisationId, int bankAccountId, MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim)
        {
            Organisation2BankAccount o2b = _db.Organisation2BankAccount.Create();

            o2b.CreatedBy         = pipeBase.CreatedBy;
            o2b.CreatedDate       = pipeBase.CreatedDate;
            o2b.ModifiedBy        = pipeBase.ModifiedBy;
            o2b.ModifiedDate      = pipeBase.ModifiedDate;
            o2b.Source            = pipeBase.Source;
            o2b.SourceDescription = pipeBase.SourceDescription;
            o2b.SourceReference   = pipeBase.SourceReference;
            o2b.ADARecordStatus   = pipeBase.ADARecordStatus;
            o2b.RecordStatus      = pipeBase.RecordStatus;
            o2b.LinkConfidence    = pipeBase.LinkConfidence;

            o2b.RiskClaim_Id = claim.Id;
            o2b.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            o2b.Organisation_Id = organisationId;
            o2b.BankAccount_Id = bankAccountId;

            o2b.FiveGrading = GetFiveGrading();

            _db.Organisation2BankAccount.Add(o2b);

            _db.SaveChanges();

            return o2b;
        }

        //public Organisation2BankAccount InsertOrganisation2BankAccountLink(Organisation2BankAccount org2bank)
        //{
        //    Organisation2BankAccount o2b = _db.Organisation2BankAccount.Create();

        //    o2b.CreatedBy = org2bank.CreatedBy;
        //    o2b.CreatedDate = DateTime.Now;
        //    o2b.BaseRiskClaim_Id = org2bank.BaseRiskClaim_Id;

        //    o2b.Source = org2bank.Source;
        //    o2b.SourceDescription = org2bank.SourceDescription;
        //    o2b.SourceReference = org2bank.SourceReference;
        //    o2b.IBaseId = org2bank.IBaseId;

        //    o2b.Organisation_Id = org2bank.Organisation_Id;
        //    o2b.RiskClaim_Id = org2bank.RiskClaim_Id;

        //    o2b.AccountLinkId = org2bank.AccountLinkId;
        //    o2b.BankAccount_Id = org2bank.BankAccount_Id;
        //    o2b.LinkConfidence = org2bank.LinkConfidence;
        //    o2b.FiveGrading = org2bank.FiveGrading;

        //    _db.Organisation2BankAccount.Add(o2b);

        //    _db.SaveChanges();

        //    return o2b;
        //}
    }

}
