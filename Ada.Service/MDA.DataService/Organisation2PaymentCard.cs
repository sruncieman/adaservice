﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;


namespace MDA.DataService
{
    public partial class DataServices
    {
        public void RedirectPaymentCard2OrganisationLinks(int masterPaymentCardId, int paymentCardId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2PaymentCard set PaymentCard_Id = " + masterPaymentCardId +
                                           " where PaymentCard_Id = " + paymentCardId);
        }

        public void RedirectOrganisation2PaymentCardLinks(int masterOrganisationId, int organisationId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2PaymentCard set Organisation_Id = " + masterOrganisationId +
                                           " where Organisation_Id = " + organisationId);
        }

        //public void ResetAllOrganisation2PaymentCardAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Organisation2PaymentCard set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public List<Organisation2PaymentCard> GetOrganisation2PaymentCardLinks(int orgId, int cardId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2PaymentCard
                    where x.Organisation_Id == orgId && x.PaymentCard_Id == cardId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Organisation2PaymentCard GetLatestOrganisation2PaymentCardLink(int orgId, int cardId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2PaymentCard
                    where x.Organisation_Id == orgId && x.PaymentCard_Id == cardId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpOrganisation2PaymentCardLinks(/*Organisation2PaymentCard latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2PC links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Organisation2PaymentCard
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Organisation2PaymentCard Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Organisation2PaymentCard UpdateOrganisation2PaymentCardLink(MDA.Common.Enum.Operation op, Organisation2PaymentCard link,
                                                                            MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeBase.LinkConfidence)
                            link.LinkConfidence = (int)pipeBase.LinkConfidence;

                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Organisation2PaymentCard UpdateOrganisation2PaymentCardLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Organisation2PaymentCard where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateOrganisation2PaymentCardLink(op, link, pipeBase, claim, ref success);
        }

        public Organisation2PaymentCard InsertOrganisation2PaymentCardLink(int organisationId, int paymentCardId, MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim)
        {
            Organisation2PaymentCard o2p = _db.Organisation2PaymentCard.Create();

            o2p.CreatedBy         = pipeBase.CreatedBy;
            o2p.CreatedDate       = pipeBase.CreatedDate;
            o2p.ModifiedBy        = pipeBase.ModifiedBy;
            o2p.ModifiedDate      = pipeBase.ModifiedDate;
            o2p.Source            = pipeBase.Source;
            o2p.SourceDescription = pipeBase.SourceDescription;
            o2p.SourceReference   = pipeBase.SourceReference;
            o2p.ADARecordStatus   = pipeBase.ADARecordStatus;
            o2p.RecordStatus      = pipeBase.RecordStatus;
            o2p.LinkConfidence    = pipeBase.LinkConfidence;

            o2p.RiskClaim_Id = claim.Id;
            o2p.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            o2p.Organisation_Id = organisationId;
            o2p.PaymentCard_Id = paymentCardId;

            o2p.FiveGrading = GetFiveGrading();

            _db.Organisation2PaymentCard.Add(o2p);

            _db.SaveChanges();

            return o2p;
        }

        //public Organisation2PaymentCard InsertOrganisation2PaymentCardLink(Organisation2PaymentCard org2card)
        //{
        //    Organisation2PaymentCard o2p = _db.Organisation2PaymentCard.Create();

        //    o2p.CreatedBy = org2card.CreatedBy;
        //    o2p.CreatedDate = DateTime.Now;
        //    o2p.BaseRiskClaim_Id = org2card.BaseRiskClaim_Id;

        //    o2p.Source = org2card.Source;
        //    o2p.SourceDescription = org2card.SourceDescription;
        //    o2p.SourceReference = org2card.SourceReference;
        //    o2p.IBaseId = org2card.IBaseId;

        //    o2p.Organisation_Id = org2card.Organisation_Id;
        //    o2p.PaymentCard_Id = org2card.PaymentCard_Id;
        //    o2p.PaymentCardLinkId = org2card.PaymentCardLinkId;
        //    o2p.LinkConfidence = org2card.LinkConfidence;
        //    o2p.FiveGrading = org2card.FiveGrading;

        //    o2p.RiskClaim_Id = org2card.RiskClaim_Id;

        //    _db.Organisation2PaymentCard.Add(o2p);

        //    _db.SaveChanges();

        //    return o2p;
        //}
    }

}
