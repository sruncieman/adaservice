﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllVehicle2VehicleAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Vehicle2Vehicle set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectVehicle2VehicleLinks(int masterVehicleId, int vehicleId)
        {
            _db.Database.ExecuteSqlCommand("update Vehicle2Vehicle set Vehicle1_Id = " + masterVehicleId +
                                           " where Vehicle1_Id = " + vehicleId);

            _db.Database.ExecuteSqlCommand("update Vehicle2Vehicle set Vehicle2_Id = " + masterVehicleId +
                                           " where Vehicle2_Id = " + vehicleId);
        }

        public List<Vehicle2Vehicle> GetVehicle2VehicleLinks(int Vehicle1Id, int vehicle2Id) //, int baseRiskClaimId)
        {
            return (from x in _db.Vehicle2Vehicle
                    where ((x.Vehicle1_Id == Vehicle1Id && x.Vehicle2_Id == vehicle2Id) || (x.Vehicle2_Id == Vehicle1Id && x.Vehicle1_Id == vehicle2Id))
                    //&& x.BaseRiskClaim_Id == baseRiskClaimId
                    //#orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Vehicle2Vehicle GetLatestVehicle2VehicleLink(int Vehicle1Id, int vehicle2Id) //, int baseRiskClaimId)
        {
            return (from x in _db.Vehicle2Vehicle
                    where ((x.Vehicle1_Id == Vehicle1Id && x.Vehicle2_Id == vehicle2Id) || (x.Vehicle2_Id == Vehicle1Id && x.Vehicle1_Id == vehicle2Id)) &&
                          (x.ADARecordStatus == (byte)ADARecordStatus.Current || x.ADARecordStatus == (byte)ADARecordStatus.SyncCurrent)
                    select x).FirstOrDefault();
        }

        //public void CleanUpVehicle2VehicleLinks(/*Vehicle2Vehicle latestLink,*/ RiskClaim newRiskClaim)
        //{
        //    var oldlinks = (from x in _db.Vehicle2Vehicle
        //                    where //(x.Vehicle1_Id == latestLink.Vehicle1_Id || x.Vehicle2_Id == latestLink.Vehicle2_Id) &&
        //                          x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
        //                          x.RiskClaim_Id != newRiskClaim.Id &&
        //                          x.ADARecordStatus == (byte)ADARecordStatus.Current &&
        //                          x.SourceDescription == "ADA Auto-Matching"
        //                    select x).ToList();

        //    foreach (var link in oldlinks)
        //    {
        //        if (ctx.Tracing)
        //            ADATrace.WriteLine("Vehicle2Vehicle Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

        //        link.RecordStatus = Constants.IBaseDeleteValue;
        //        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
        //    }
        //    _db.SaveChanges();
        //}

        public Vehicle2Vehicle UpdateVehicle2VehicleLink(MDA.Common.Enum.Operation op, Vehicle2Vehicle link,
                                                            //DateTime? dateOfRegChange, 
                                                            MDA.Pipeline.Model.PipelineVehicle2VehicleLink pipeLink, /*RiskClaim newRiskClaim,*/ ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        //link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence && link.LinkConfidence > (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.DateOfRegChange != MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.DateOfRegChange))
                            link.DateOfRegChange = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.DateOfRegChange);
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;
        }


        public Vehicle2Vehicle UpdateVehicle2VehicleLink(MDA.Common.Enum.Operation op, int linkId,
                                               DateTime? dateOfRegChange, MDA.Pipeline.Model.PipelineVehicle2VehicleLink pipeLink, /*RiskClaim claim, */ ref bool success)
        {
            success = false;

            var link = (from x in _db.Vehicle2Vehicle where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateVehicle2VehicleLink(op, link, /*dateOfRegChange,*/ pipeLink, /*claim,*/ ref success);
        }

        public Vehicle2Vehicle InsertVehicle2VehicleLink(int vehicle1Id, int vehicle2Id, MDA.Pipeline.Model.PipelineVehicle2VehicleLink pipeLink /*DateTime? dateOfRegChange*/) //, RiskClaim claim)
        {
            Vehicle2Vehicle v2v = _db.Vehicle2Vehicle.Create();

            v2v.CreatedBy         = pipeLink.CreatedBy;
            v2v.CreatedDate       = pipeLink.CreatedDate;
            v2v.ModifiedBy        = pipeLink.ModifiedBy;
            v2v.ModifiedDate      = pipeLink.ModifiedDate;
            //v2v.Source            = pipeLink.Source;
            //v2v.SourceDescription = pipeLink.SourceDescription;
            //v2v.SourceReference   = pipeLink.SourceReference;
            v2v.ADARecordStatus   = pipeLink.ADARecordStatus;
            v2v.RecordStatus      = pipeLink.RecordStatus;
            v2v.LinkConfidence    = pipeLink.LinkConfidence;

            v2v.Vehicle1_Id = vehicle1Id;
            v2v.Vehicle2_Id = vehicle2Id;
           // v2v.RiskClaim_Id = claim.Id;
            //v2v.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            v2v.Source = "Keoghs CFS";
            v2v.SourceDescription = "ADA Auto-Matching";
            v2v.SourceReference = null;

            v2v.FiveGrading = GetFiveGrading();
            v2v.DateOfRegChange = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.DateOfRegChange);

            _db.Vehicle2Vehicle.Add(v2v);

            _db.SaveChanges();

            return v2v;
        }

        //public Vehicle2Vehicle InsertVehicle2VehicleLink(Vehicle2Vehicle vehicle2vehicle)
        //{
        //    Vehicle2Vehicle v2v = _db.Vehicle2Vehicle.Create();

        //    v2v.CreatedBy = vehicle2vehicle.CreatedBy;
        //    v2v.CreatedDate = DateTime.Now;
        //    //#v2v.BaseRiskClaim_Id = vehicle2vehicle.BaseRiskClaim_Id;

        //    v2v.Source = vehicle2vehicle.Source;
        //    v2v.SourceDescription = vehicle2vehicle.SourceDescription;
        //    v2v.SourceReference = vehicle2vehicle.SourceReference;

        //    v2v.DateOfRegChange = vehicle2vehicle.DateOfRegChange;
        //    v2v.Vehicle1_Id = vehicle2vehicle.Vehicle1_Id;
        //    v2v.Vehicle2_Id = vehicle2vehicle.Vehicle2_Id;
        //    v2v.Vehicle2VehicleLinkId = vehicle2vehicle.Vehicle2VehicleLinkId;

        //    v2v.IBaseId = vehicle2vehicle.IBaseId;
        //    //#v2v.RiskClaim_Id = vehicle2vehicle.RiskClaim_Id;
        //    v2v.LinkConfidence = vehicle2vehicle.LinkConfidence;
        //    v2v.FiveGrading = vehicle2vehicle.FiveGrading;

        //    _db.Vehicle2Vehicle.Add(v2v);

        //    _db.SaveChanges();

        //    return v2v;
        //}
    }

}
