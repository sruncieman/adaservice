﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public Handset2Person GetLatestHandset2PersonLink(int HandsetId, int personId, int baseRiskClaimId)
        {
            return (from x in _db.Handset2Person
                    where x.Person_Id == personId && x.Handset_Id == HandsetId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public Handset2Person InsertHandset2PersonLink(int HandsetId, int personId,
                       MDA.Pipeline.Model.PipelineHandset2PersonLink pipeLink, RiskClaim claim)
        {
            Handset2Person h2p = _db.Handset2Person.Create();

            h2p.CreatedBy = pipeLink.CreatedBy;
            h2p.CreatedDate = pipeLink.CreatedDate;
            h2p.ModifiedBy = pipeLink.ModifiedBy;
            h2p.ModifiedDate = pipeLink.ModifiedDate;
            h2p.Source = pipeLink.Source;
            h2p.SourceDescription = pipeLink.SourceDescription;
            h2p.SourceReference = pipeLink.SourceReference;
            h2p.ADARecordStatus = pipeLink.ADARecordStatus;
            h2p.RecordStatus = pipeLink.RecordStatus;
            h2p.LinkConfidence = pipeLink.LinkConfidence;

            //h2p.HireStartDate = pipeLink.HireStartDate;
            //h2p.HireEndDate = pipeLink.HireEndDate;
            //h2p.HireCompany = pipeLink.HireCompany;

            h2p.RiskClaim_Id = claim.Id;
            h2p.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            h2p.Person_Id = personId;
            h2p.Handset_Id = HandsetId;
            //h2p.HandsetLinkType_Id = (int)pipeLink.HandsetLinkType_Id;

            h2p.FiveGrading = GetFiveGrading();

            _db.Handset2Person.Add(h2p);

            _db.SaveChanges();

            return h2p;
        }

        public Handset2Person UpdateHandset2PersonLink(MDA.Common.Enum.Operation op, int linkId,
            //MDA.Common.Enum.HandsetLinkType linkType, 
                                            MDA.Pipeline.Model.PipelineHandset2PersonLink pipeLink,
                                            RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Handset2Person where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateHandset2PersonLink(op, link, pipeLink, claim, ref success);
        }

        public Handset2Person UpdateHandset2PersonLink(MDA.Common.Enum.Operation op, Handset2Person link,
                                           MDA.Pipeline.Model.PipelineHandset2PersonLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Handset.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE
                    //if (link.HandsetLinkType_Id != 0 && link.HandsetLinkType_Id != (int)pipeLink.HandsetLinkType_Id) // we have value and it's different
                    //{
                    //    success = false;
                    //}

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        //if (link.HandsetLinkType_Id != (int)pipeLink.HandsetLinkType_Id)
                        //    link.HandsetLinkType_Id = (int)pipeLink.HandsetLinkType_Id;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;

        }

        public void CleanUpHandset2PersonLinks(/*Handset2Person latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Handset2Person
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Handset2Person Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }
    }
}
