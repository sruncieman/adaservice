﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllAddress2AddressLinkAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Address2Address set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectAddress2AddressLinks(int masterAddressId, int addressId)
        {
            _db.Database.ExecuteSqlCommand("update Address2Address set Address1_Id = " + masterAddressId +
                                           " where Address1_Id = " + addressId);

            _db.Database.ExecuteSqlCommand("update Address2Address set Address2_Id = " + masterAddressId +
                                           " where Address2_Id = " + addressId);
        }

        public List<Address2Address> GetAddress2AddressLinks(int addr1Id, int addr2Id) //, int baseRiskClaimId)
        {
            return (from x in _db.Address2Address
                    where ((x.Address1_Id == addr1Id && x.Address2_Id == addr2Id) || (x.Address2_Id == addr1Id && x.Address1_Id == addr2Id))
                    //orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Address2Address GetLatestAddress2AddressLink(int addr1Id, int addr2Id) //, int baseRiskClaimId)
        {
            return (from x in _db.Address2Address
                    where ((x.Address1_Id == addr1Id && x.Address2_Id == addr2Id) || (x.Address2_Id == addr1Id && x.Address1_Id == addr2Id)) &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }


        public Address2Address UpdateAddress2AddressLink(MDA.Common.Enum.Operation op, Address2Address link,
                                                    //MDA.Common.Enum.AddressLinkType linkType, 
                                                    MDA.Pipeline.Model.PipelineAddressLink pipeLink, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        //link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence && link.LinkConfidence > (int)pipeLink.LinkConfidence)
                        {
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;
                        }

                        if (link.AddressLinkType_Id != (int)pipeLink.AddressLinkType_Id) //linkType)
                        {
                            link.AddressLinkType_Id = (int)pipeLink.AddressLinkType_Id;
                        }
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Address2Address UpdateAddress2AddressLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Common.Enum.AddressLinkType linkType, MDA.Pipeline.Model.PipelineAddressLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Address2Address where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateAddress2AddressLink(op, link, /*linkType,*/ pipeLink, ref success);
        }


        public Address2Address InsertAddress2AddressLink(int addr1Id, int addr2Id, /*MDA.Common.Enum.AddressLinkType linkType,*/ MDA.Pipeline.Model.PipelineAddressLink pipeLink) //, RiskClaim claim)
        {
            Address2Address a2a = _db.Address2Address.Create();

            a2a.CreatedBy         = pipeLink.CreatedBy;
            a2a.CreatedDate       = pipeLink.CreatedDate;
            a2a.ModifiedBy        = pipeLink.ModifiedBy;
            a2a.ModifiedDate      = pipeLink.ModifiedDate;
            //a2a.Source            = pipeLink.Source;
            //a2a.SourceDescription = pipeLink.SourceDescription;
            //a2a.SourceReference   = pipeLink.SourceReference;
            a2a.ADARecordStatus   = pipeLink.ADARecordStatus;
            a2a.RecordStatus      = pipeLink.RecordStatus;
            a2a.LinkConfidence    = pipeLink.LinkConfidence;

            //a2a.RiskClaim_Id = claim.Id;
            //a2a.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            a2a.Address1_Id = addr1Id;
            a2a.Address2_Id = addr2Id;
            a2a.AddressLinkType_Id = (int)pipeLink.AddressLinkType_Id; // linkType;

            a2a.Source = "Keoghs CFS";
            a2a.SourceDescription = "ADA Auto-Matching";
            a2a.SourceReference = null;

            a2a.FiveGrading = GetFiveGrading();

            _db.Address2Address.Add(a2a);

            _db.SaveChanges();

            return a2a;
        }


        //public Address2Address InsertAddress2AddressLink(Address2Address address2address)
        //{
        //    Address2Address a2a = _db.Address2Address.Create();

        //    a2a.CreatedBy = address2address.CreatedBy;
        //    a2a.CreatedDate = DateTime.Now;

        //    a2a.Source = address2address.Source;
        //    a2a.SourceDescription = address2address.SourceDescription;
        //    a2a.SourceReference = address2address.SourceReference;

        //    a2a.Address1_Id = address2address.Address1_Id;
        //    a2a.Address2_Id = address2address.Address2_Id;
        //    a2a.AddressLinkType_Id = address2address.AddressLinkType_Id;
        //    a2a.LinkConfidence = address2address.LinkConfidence;

        //    a2a.IBaseId = address2address.IBaseId;
        //    a2a.LinkConfidence = address2address.LinkConfidence;
        //    a2a.FiveGrading = address2address.FiveGrading;

        //    _db.Address2Address.Add(a2a);

        //    _db.SaveChanges();

        //    return a2a;
        //}

    }

}
