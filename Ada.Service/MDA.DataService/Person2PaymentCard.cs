﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPerson2PaymentCardAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2PaymentCard set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectPaymentCard2PersonLinks(int masterPaymentCardId, int paymentCardId)
        {
            _db.Database.ExecuteSqlCommand("update Person2PaymentCard set PaymentCard_Id = " + masterPaymentCardId +
                                           " where PaymentCard_Id = " + paymentCardId);
        }

        public void RedirectPerson2PaymentCardLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Person2PaymentCard set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        public Person2PaymentCard GetPerson2PaymentCardLink(MdaDbContext db, int id)
        {
            return (from x in db.Person2PaymentCard where x.Id == id select x).FirstOrDefault();
        }

        public Person2PaymentCard GetPerson2PaymentCardLink(int id)
        {
            return GetPerson2PaymentCardLink(id);
        }

        public List<Person2PaymentCard> GetPerson2PaymentCardLinks(int personId, int cardId, int baseRiskClaimId)
        {
            return (from x in _db.Person2PaymentCard
                    where x.Person_Id == personId && x.PaymentCard_Id == cardId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Person2PaymentCard GetLatestPerson2PaymentCardLink(int personId, int cardId, int baseRiskClaimId)
        {
            return (from x in _db.Person2PaymentCard
                    where x.Person_Id == personId && x.PaymentCard_Id == cardId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPerson2PaymentCardLinks(/*Person2PaymentCard latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Person2PaymentCard
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Person2PaymentCard Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Person2PaymentCard UpdatePerson2PaymentCardLink(MDA.Common.Enum.Operation op, Person2PaymentCard link,
                                                                            MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeBase.LinkConfidence)
                            link.LinkConfidence = (int)pipeBase.LinkConfidence;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Person2PaymentCard UpdatePerson2PaymentCardLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Person2PaymentCard where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePerson2PaymentCardLink(op, link, pipeBase, claim, ref success);
        }

        public Person2PaymentCard InsertPerson2PaymentCardLink(int personId, int cardId, MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim)
        {
            Person2PaymentCard p2c = _db.Person2PaymentCard.Create();

            p2c.CreatedBy         = pipeBase.CreatedBy;
            p2c.CreatedDate       = pipeBase.CreatedDate;
            p2c.ModifiedBy        = pipeBase.ModifiedBy;
            p2c.ModifiedDate      = pipeBase.ModifiedDate;
            p2c.Source            = pipeBase.Source;
            p2c.SourceDescription = pipeBase.SourceDescription;
            p2c.SourceReference   = pipeBase.SourceReference;
            p2c.ADARecordStatus   = pipeBase.ADARecordStatus;
            p2c.RecordStatus      = pipeBase.RecordStatus;
            p2c.LinkConfidence    = pipeBase.LinkConfidence;

            p2c.RiskClaim_Id = claim.Id;
            p2c.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2c.Person_Id = personId;
            p2c.PaymentCard_Id = cardId;

            p2c.FiveGrading = GetFiveGrading();

            _db.Person2PaymentCard.Add(p2c);

            _db.SaveChanges();

            return p2c;
        }

        //public Person2PaymentCard InsertPerson2PaymentCardLink(Person2PaymentCard person2card)
        //{
        //    Person2PaymentCard p2c = _db.Person2PaymentCard.Create();

        //    p2c.CreatedBy = person2card.CreatedBy;
        //    p2c.CreatedDate = DateTime.Now;
        //    p2c.BaseRiskClaim_Id = person2card.BaseRiskClaim_Id;

        //    p2c.Source = person2card.Source;
        //    p2c.SourceDescription = person2card.SourceDescription;
        //    p2c.SourceReference = person2card.SourceReference;

        //    p2c.PaymentCard_Id = person2card.PaymentCard_Id;
        //    p2c.PaymentCardLinkId = person2card.PaymentCardLinkId;

        //    p2c.IBaseId = person2card.IBaseId;
        //    p2c.Person_Id = person2card.Person_Id;
        //    p2c.RiskClaim_Id = person2card.RiskClaim_Id;
        //    p2c.LinkConfidence = person2card.LinkConfidence;
        //    p2c.FiveGrading = person2card.FiveGrading;

        //    _db.Person2PaymentCard.Add(p2c);

        //    _db.SaveChanges();

        //    return p2c;
        //}

    }

}
