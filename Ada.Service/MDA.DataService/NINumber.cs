﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;


namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllNINumberAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update NINumber set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public int DeduplicateNINumbers(CurrentContext ctx, Func<string, int> ShowProgress = null)
        {
            //trace = new List<string>();
            int duplicateCount = 0;

            var q = ctx.db.NINumbers.Where(x => x.ADARecordStatus == 0 || x.ADARecordStatus >= 10);

            var query = (q.GroupBy(i => i.NINumber1)
                        .Where(g => g.Count() > 1)
                        .Select(g => g.Key)).ToList();

            foreach (var dNi in query)
            {
                if (dNi == null) continue;

                duplicateCount++;

                using (var ctxx = new CurrentContext(ctx))
                {
                    var master = (from t in ctxx.db.NINumbers 
                                        where t.NINumber1 == dNi && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10) && t.NINumberId != null
                                        orderby t.CreatedDate ascending
                                        select t).FirstOrDefault();

                    if (master == null)
                    {
                        master = (from t in ctxx.db.NINumbers
                                        where t.NINumber1 == dNi && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                        orderby t.CreatedDate ascending
                                        select t).FirstOrDefault();
                    }

                    if (master == null) continue;

                    var allDuplicates =  from t in ctxx.db.NINumbers
                                         where t.NINumber1 == dNi && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                         && t.Id != master.Id
                                         select t;

                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                    {
                        try
                        {
                            foreach (var td in allDuplicates)
                            {
                                List<int> PId = new List<int>();

                                ctxx.db.Entry(td).Collection(z => z.Person2NINumber).Load();

                                if (td.Person2NINumber.Any())
                                {
                                    foreach (var l in td.Person2NINumber)
                                    {
                                        l.NINumber_Id = master.Id;
                                        PId.Add(l.Person_Id);
                                    }
                                }

                                td.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                                td.RecordStatus = Constants.IBaseDeleteValue;

                                if (ShowProgress != null)
                                    ShowProgress(string.Format("Duplicate NI Removed [{0}] : New Master Id [{1}] : People ID's Moved [{2}]",
                                                td.Id, master.Id, string.Join(",", PId)));
                            }
                            ctxx.db.SaveChanges();

                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();

                            throw;
                        }
                    }
                }
            }

            if (ShowProgress != null)
                ShowProgress(string.Format("Duplicates Found [{0}]", duplicateCount));

            return duplicateCount;
        }

        public NINumber SelectNINumber(int id)
        {
            return (from x in _db.NINumbers where x.Id == id select x).FirstOrDefault();
        }

        public List<int> VerifyNINumberListExists(List<int> listId)
        {
            List<int> outList = new List<int>();

            foreach (int id in listId)
            {
                if (_db.NINumbers.Any(x => x.Id == id))
                    outList.Add(id);
            }
            return outList;
        }

        public NINumber UpdateNINumber(int NINumberId, MDA.Pipeline.Model.PipelineNINumber pipeNiNumber, out bool success)
        {
            success = false;

            var pp = (from x in _db.NINumbers where x.Id == NINumberId select x).FirstOrDefault();

            if (pp == null) return pp;

            try
            {
                success = true;

                bool modified = false;

                if (pp.NINumber1 == null)
                {
                    pp.NINumber1 = pipeNiNumber.NINumber1;
                    modified = true;
                }

                if (modified)
                {
                    pp.ModifiedDate = DateTime.Now;
                    pp.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }

            }
            catch (Exception)
            {
                success = false;
            }

            return pp;
        }

        public NINumber InsertNINumber(MDA.Pipeline.Model.PipelineNINumber pipeNiNumber)
        {
            NINumber e = _db.NINumbers.Create();

            e.ADARecordStatus   = pipeNiNumber.ADARecordStatus;
            e.CreatedBy         = pipeNiNumber.CreatedBy;
            e.CreatedDate       = pipeNiNumber.CreatedDate;
            e.IBaseId           = pipeNiNumber.IBaseId;
            e.KeyAttractor      = pipeNiNumber.KeyAttractor;
            e.ModifiedBy        = pipeNiNumber.ModifiedBy;
            e.ModifiedDate      = pipeNiNumber.ModifiedDate;
            e.NINumber1         = pipeNiNumber.NINumber1;
            e.NINumberId        = pipeNiNumber.NINumberId;
            e.RecordStatus      = pipeNiNumber.RecordStatus;
            e.Source            = pipeNiNumber.Source;
            e.SourceDescription = pipeNiNumber.SourceDescription;
            e.SourceReference   = pipeNiNumber.SourceReference;

            _db.NINumbers.Add(e);

            _db.SaveChanges();

            return e;
        }

        //public NINumber InsertNINumber(NINumber NINumber)
        //{
        //    NINumber c = _db.NINumbers.Create();

        //    c.CreatedBy = NINumber.CreatedBy;
        //    c.CreatedDate = NINumber.CreatedDate;
        //    c.NINumber1 = NINumber.NINumber1;
        //    c.NINumberId = NINumber.NINumberId;
        //    c.IBaseId = NINumber.IBaseId;
        //    c.Source = NINumber.Source;
        //    c.SourceDescription = NINumber.SourceDescription;
        //    c.SourceReference = NINumber.SourceReference;

        //    _db.NINumbers.Add(c);

        //    _db.SaveChanges();

        //    return c;
         
        //}

        public NINumber FindNINumber(string NINumber)
        {
            return (from x in _db.NINumbers where x.NINumber1 == NINumber select x).FirstOrDefault();
        }
    }
}
