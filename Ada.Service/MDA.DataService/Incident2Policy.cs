﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {

        public void RedirectIncident2PolicyLinks(int masterIncidentId, int incidentId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2Policy set Incident_Id = " + masterIncidentId +
                                           " where Incident_Id = " + incidentId);
        }

        public void RedirectPolicy2IncidentLinks(int masterPolicyId, int policyId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2Policy set Policy_Id = " + masterPolicyId +
                                           " where Policy_Id = " + policyId);
        }

        //public void ResetAllIncident2PolicyLinkAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Incident2Policy set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public List<Incident2Policy> GetIncident2PolicyLinks(int incidentId, int policyId, int baseRiskClaimId)
        {
            return (from x in _db.Incident2Policy
                    where x.Incident_Id == incidentId && x.Policy_Id == policyId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Incident2Policy GetLatestIncident2PolicyLink(int incidentId, int policyId, int baseRiskClaimId)
        {
            return (from x in _db.Incident2Policy
                    where x.Incident_Id == incidentId && x.Policy_Id == policyId
                          && x.BaseRiskClaim_Id == baseRiskClaimId
                          && x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpIncident2PolicyLinks(RiskClaim newRiskClaim)
        {
            // Get all I2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Incident2Policy
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Incident2Policy Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Incident2Policy UpdateIncident2PolicyLink(MDA.Common.Enum.Operation op, Incident2Policy link,
                                                    MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Policy.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {
 
                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != pipeBase.LinkConfidence)
                            link.LinkConfidence = pipeBase.LinkConfidence;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Incident2Policy UpdateIncident2PolicyLink(MDA.Common.Enum.Operation op, int linkId,
                                            MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Incident2Policy where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateIncident2PolicyLink(op, link, pipeBase, claim, ref success);
        }


        public Incident2Policy InsertIncident2PolicyLink(int incidentId, int policyId, MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim)
        {
            Incident2Policy i2p = _db.Incident2Policy.Create();

            i2p.CreatedBy           = pipeBase.CreatedBy;
            i2p.CreatedDate         = pipeBase.CreatedDate;
            i2p.ModifiedBy          = pipeBase.ModifiedBy;
            i2p.ModifiedDate        = pipeBase.ModifiedDate;
            i2p.Source              = pipeBase.Source;
            i2p.SourceDescription   = pipeBase.SourceDescription;
            i2p.SourceReference     = pipeBase.SourceReference;
            i2p.ADARecordStatus     = pipeBase.ADARecordStatus;
            i2p.RecordStatus        = pipeBase.RecordStatus;
            i2p.VehiclePolicyLinkId = pipeBase.IBaseId;
            i2p.LinkConfidence      = pipeBase.LinkConfidence;

            i2p.RiskClaim_Id     = claim.Id;
            i2p.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            i2p.Policy_Id      = policyId;
            i2p.Incident_Id    = incidentId;
            
            i2p.FiveGrading    = GetFiveGrading();

            _db.Incident2Policy.Add(i2p);

            _db.SaveChanges();

            return i2p;
        }

        //public Incident2Policy InsertIncident2PolicyLink(Incident2Policy Incident2Policy)
        //{
        //    Incident2Policy i2p = _db.Incident2Policy.Create();

        //    i2p.CreatedBy = Incident2Policy.CreatedBy;
        //    i2p.CreatedDate = DateTime.Now;
        //    i2p.BaseRiskClaim_Id = Incident2Policy.BaseRiskClaim_Id;

        //    i2p.Source = Incident2Policy.Source;
        //    i2p.SourceDescription = Incident2Policy.SourceDescription;
        //    i2p.SourceReference = Incident2Policy.SourceReference;
        //    i2p.IBaseId = Incident2Policy.IBaseId;

        //    i2p.Incident_Id = Incident2Policy.Incident_Id;
        //    i2p.Policy_Id = Incident2Policy.Policy_Id;
        //    i2p.VehiclePolicyLinkId = Incident2Policy.VehiclePolicyLinkId;
        //    i2p.LinkConfidence = Incident2Policy.LinkConfidence;
        //    i2p.FiveGrading = Incident2Policy.FiveGrading;

        //    i2p.RiskClaim_Id = Incident2Policy.RiskClaim_Id;

        //    _db.Incident2Policy.Add(i2p);

        //    _db.SaveChanges();

        //    return i2p;
        //}
    }

}
