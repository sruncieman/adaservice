﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;


namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllTelephoneAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Telephone set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}
        public int DeduplicateTelephones(CurrentContext ctx, Func<string, int> ShowProgress = null)
        {
            //trace = new List<string>();
            int duplicateCount = 0;

            var q = ctx.db.Telephones.Where(x => x.ADARecordStatus == 0 || x.ADARecordStatus >= 10);

            var query = (q.GroupBy(i => i.TelephoneNumber)
                        .Where(g => g.Count() > 1)
                        .Select(g => g.Key)).ToList();

            foreach (var tele in query)
            {
                if (tele == null) continue;

                duplicateCount++;

                using (var ctxx = new CurrentContext(ctx))
                {
                    var master = (from t in ctxx.db.Telephones
                                  where t.TelephoneNumber == tele && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10) && t.TelephoneId != null
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();

                    if (master == null)
                    {
                        master = (from t in ctxx.db.Telephones
                                  where t.TelephoneNumber == tele && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();
                    }

                    if (master == null) continue;

                    var allDuplicates = from t in ctxx.db.Telephones
                                        where t.TelephoneNumber == tele && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                        && t.Id != master.Id
                                        select t;

                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                    {
                        try
                        {
                            foreach (var td in allDuplicates)
                            {
                                List<int> AId = new List<int>();
                                List<int> OId = new List<int>();
                                List<int> PId = new List<int>();
                                List<int> TId = new List<int>();

                                ctxx.db.Entry(td).Collection(z => z.Address2Telephone).Load();

                                if (td.Address2Telephone.Any())
                                {
                                    foreach (var l in td.Address2Telephone)
                                    {
                                        l.Telephone_Id = master.Id;
                                        AId.Add(l.Address_Id);
                                    }
                                }

                                ctxx.db.Entry(td).Collection(z => z.Organisation2Telephone).Load();

                                if (td.Organisation2Telephone.Any())
                                {
                                    foreach (var l in td.Organisation2Telephone)
                                    {
                                        l.Telephone_Id = master.Id;
                                        OId.Add(l.Organisation_Id);
                                    }
                                }

                                ctxx.db.Entry(td).Collection(z => z.Person2Telephone).Load();

                                if (td.Person2Telephone.Any())
                                {
                                    foreach (var l in td.Person2Telephone)
                                    {
                                        l.Telephone_Id = master.Id;
                                        PId.Add(l.Person_Id);
                                    }
                                }

                                ctxx.db.Entry(td).Collection(z => z.TOG2Telephone).Load();

                                if (td.TOG2Telephone.Any())
                                {
                                    foreach (var l in td.TOG2Telephone)
                                    {
                                        l.Telephone_Id = master.Id;
                                        TId.Add(l.TOG_Id);
                                    }
                                }

                                td.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                                td.RecordStatus = Constants.IBaseDeleteValue;

                                if (ShowProgress != null)
                                    ShowProgress(string.Format("Duplicate Telephone Removed [{0}] : New Master Id [{1}] : Addr ID's Moved [{2}] : Org ID's Moved [{3}] : People ID's Moved [{4}] : Tog ID's Moved [{5}] :",
                                                td.Id, master.Id, string.Join(",", AId), string.Join(",", OId), string.Join(",", PId), string.Join(",", TId)));
                            }
                            ctxx.db.SaveChanges();

                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();

                            throw;
                        }
                    }
                }
            }



            if (ShowProgress != null)
                ShowProgress(string.Format("Duplicates Found [{0}]", duplicateCount));

            return duplicateCount;
        }

        public Telephone SelectTelephone(int id)
        {
            return (from x in _db.Telephones where x.Id == id select x).FirstOrDefault();
        }

        public List<int> VerifyTelephoneListExists(List<int> listId)
        {
            List<int> outList = new List<int>();

            foreach (int id in listId)
            {
                if (_db.Telephones.Any(x => x.Id == id))
                    outList.Add(id);
            }
            return outList;
        }

        public Telephone UpdateTelephone(int telephoneId, MDA.Pipeline.Model.PipelineTelephone telephone, out bool success)
        {
            success = false;

            var tn = (from x in _db.Telephones where x.Id == telephoneId select x).FirstOrDefault();

            if (tn == null) return tn;

            try
            {
                success = true;
                bool modified = false;

                if (tn.TelephoneNumber == null)
                {
                    tn.TelephoneNumber = telephone.FullNumber;
                    modified = true;
                }

                if (tn.CountryCode == null)
                {
                    tn.CountryCode = telephone.InternationalCode;
                    modified = true;
                }

                if (tn.STDCode == null)
                {
                    tn.STDCode = telephone.AreaCode;
                    modified = true;
                }

                //if (tn.TelephoneType_Id == null)
                //{
                //    tn.TelephoneType_Id = (int)telephone.TelephoneType;
                //    modified = true;
                //}

                if (modified)
                {
                    tn.ModifiedDate = DateTime.Now;
                    tn.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return tn;
        }

        public Telephone InsertTelephone(MDA.Pipeline.Model.PipelineTelephone telephone)
        {
            Telephone e = _db.Telephones.Create();

            e.ADARecordStatus   = telephone.ADARecordStatus;
            e.CountryCode       = telephone.InternationalCode;
            e.CreatedBy         = telephone.CreatedBy;
            e.CreatedDate       = telephone.CreatedDate;
            e.IBaseId           = telephone.IBaseId;
            e.KeyAttractor      = telephone.KeyAttractor;
            e.ModifiedBy        = telephone.ModifiedBy;
            e.ModifiedDate      = telephone.ModifiedDate;
            e.RecordStatus      = telephone.RecordStatus;
            e.Source            = telephone.Source;
            e.SourceDescription = telephone.SourceDescription;
            e.SourceReference   = telephone.SourceReference;
            e.STDCode           = telephone.AreaCode;
            e.TelephoneId       = telephone.TelephoneId;
            e.TelephoneNumber   = telephone.FullNumber;
            e.TelephoneType_Id  = telephone.TelephoneType_Id;

            _db.Telephones.Add(e);

            _db.SaveChanges();

            return e;
        }

        //public Telephone InsertTelephone(Telephone passport)
        //{
        //    Telephone t = _db.Telephones.Create();

        //    t.CreatedBy = passport.CreatedBy;
        //    t.CreatedDate = passport.CreatedDate;
        //    t.TelephoneNumber = passport.TelephoneNumber;
        //    t.STDCode = passport.STDCode;
        //    t.TelephoneType = passport.TelephoneType;
        //    t.TelephoneType_Id = passport.TelephoneType_Id;
        //    t.CountryCode = passport.CountryCode;
        //    t.TelephoneId = passport.TelephoneId;
        //    t.IBaseId = passport.IBaseId;
        //    t.Source = passport.Source;
        //    t.SourceDescription = passport.SourceDescription;
        //    t.SourceReference = passport.SourceReference;

        //    _db.Telephones.Add(t);

        //    _db.SaveChanges();

        //    return t;
        //}

        public Telephone FindTelephone(string telephoneNumber)
        {
            return (from x in _db.Telephones where x.TelephoneNumber == telephoneNumber select x).FirstOrDefault();
        }

    }

}
