﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPerson2BankAccountAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2BankAccount set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}
        public void RedirectBankAccount2PersonLinks(int masterBankAccountId, int bankAccountId)
        {
            _db.Database.ExecuteSqlCommand("update Person2BankAccount set BankAccount_Id = " + masterBankAccountId +
                                           " where BankAccount_Id = " + bankAccountId);
        }

        public void RedirectPerson2BankAccountLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Person2BankAccount set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        public Person2BankAccount GetPerson2BankAccountLink(MdaDbContext db, int id)
        {
            return (from x in db.Person2BankAccount where x.Id == id select x).FirstOrDefault();
        }

        public Person2BankAccount GetPerson2BankAccountLink(int id)
        {
            return GetPerson2BankAccountLink(id);
        }

        public List<Person2BankAccount> GetPerson2BankAccountLinks(int personId, int accountId, int baseRiskClaimId)
        {
            return (from x in _db.Person2BankAccount
                    where x.Person_Id == personId && x.BankAccount_Id == accountId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Person2BankAccount GetLatestPerson2BankAccountLink(int personId, int accountId, int baseRiskClaimId)
        {
            return (from x in _db.Person2BankAccount
                    where x.Person_Id == personId && x.BankAccount_Id == accountId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPerson2BankAccountLinks(/*Person2BankAccount latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Person2BankAccount
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Person2BankAccount Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Person2BankAccount UpdatePerson2BankAccountLink(MDA.Common.Enum.Operation op, Person2BankAccount link,
                                                                            MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeBase.LinkConfidence)
                            link.LinkConfidence = (int)pipeBase.LinkConfidence;

                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Person2BankAccount UpdatePerson2BankAccountLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Person2BankAccount where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePerson2BankAccountLink(op, link, pipeBase, claim, ref success);
        }

        public Person2BankAccount InsertPerson2BankAccountLink(int personId, int bankAccountId, MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim)
        {
            Person2BankAccount p2b = _db.Person2BankAccount.Create();

            p2b.CreatedBy         = pipeBase.CreatedBy;
            p2b.CreatedDate       = pipeBase.CreatedDate;
            p2b.ModifiedBy        = pipeBase.ModifiedBy;
            p2b.ModifiedDate      = pipeBase.ModifiedDate;
            p2b.Source            = pipeBase.Source;
            p2b.SourceDescription = pipeBase.SourceDescription;
            p2b.SourceReference   = pipeBase.SourceReference;
            p2b.ADARecordStatus   = pipeBase.ADARecordStatus;
            p2b.RecordStatus      = pipeBase.RecordStatus;
            p2b.LinkConfidence    = pipeBase.LinkConfidence;

            p2b.RiskClaim_Id = claim.Id;
            p2b.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2b.Person_Id = personId;
            p2b.BankAccount_Id = bankAccountId;

            p2b.FiveGrading = GetFiveGrading();

            _db.Person2BankAccount.Add(p2b);

            _db.SaveChanges();

            return p2b;
        }

        //public Person2BankAccount InsertPerson2BankAccountLink(Person2BankAccount person2bank)
        //{
        //    Person2BankAccount p2ba = _db.Person2BankAccount.Create();

        //    p2ba.CreatedBy = person2bank.CreatedBy;
        //    p2ba.CreatedDate = DateTime.Now;
        //    p2ba.BaseRiskClaim_Id = person2bank.BaseRiskClaim_Id;

        //    p2ba.Source = person2bank.Source;
        //    p2ba.SourceDescription = person2bank.SourceDescription;
        //    p2ba.SourceReference = person2bank.SourceReference;

        //    p2ba.AccountLinkId = person2bank.AccountLinkId;
        //    p2ba.BankAccount_Id = person2bank.BankAccount_Id;
        //    p2ba.IBaseId = person2bank.IBaseId;
        //    p2ba.Person_Id = person2bank.Person_Id;
        //    p2ba.RiskClaim_Id = person2bank.RiskClaim_Id;
        //    p2ba.LinkConfidence = person2bank.LinkConfidence;
        //    p2ba.FiveGrading = person2bank.FiveGrading;

        //    _db.Person2BankAccount.Add(p2ba);

        //    _db.SaveChanges();

        //    return p2ba;
        //}
    }

}
