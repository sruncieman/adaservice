﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public Handset2Telephone GetLatestHandset2TelephoneLink(int HandsetId, int telephoneId, int baseRiskClaimId)
        {
            return (from x in _db.Handset2Telephone
                    where x.Telephone_Id == telephoneId && x.Handset_Id == HandsetId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public Handset2Telephone UpdateHandset2TelephoneLink(MDA.Common.Enum.Operation op, int linkId,
            //MDA.Common.Enum.HandsetLinkType linkType, 
                                            MDA.Pipeline.Model.PipelineHandset2TelephoneLink pipeLink,
                                            RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Handset2Telephone where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateHandset2TelephoneLink(op, link, pipeLink, claim, ref success);
        }

        public Handset2Telephone UpdateHandset2TelephoneLink(MDA.Common.Enum.Operation op, Handset2Telephone link,
                                           MDA.Pipeline.Model.PipelineHandset2TelephoneLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Handset.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE
                    //if (link.HandsetLinkType_Id != 0 && link.HandsetLinkType_Id != (int)pipeLink.HandsetLinkType_Id) // we have value and it's different
                    //{
                    //    success = false;
                    //}

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        //if (link.HandsetLinkType_Id != (int)pipeLink.HandsetLinkType_Id)
                        //    link.HandsetLinkType_Id = (int)pipeLink.HandsetLinkType_Id;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;

        }


        public Handset2Telephone InsertHandset2TelephoneLink(int HandsetId, int telephoneId,
                      MDA.Pipeline.Model.PipelineHandset2TelephoneLink pipeLink, RiskClaim claim)
        {
            Handset2Telephone h2t = _db.Handset2Telephone.Create();

            h2t.CreatedBy = pipeLink.CreatedBy;
            h2t.CreatedDate = pipeLink.CreatedDate;
            h2t.ModifiedBy = pipeLink.ModifiedBy;
            h2t.ModifiedDate = pipeLink.ModifiedDate;
            h2t.Source = pipeLink.Source;
            h2t.SourceDescription = pipeLink.SourceDescription;
            h2t.SourceReference = pipeLink.SourceReference;
            h2t.ADARecordStatus = pipeLink.ADARecordStatus;
            h2t.RecordStatus = pipeLink.RecordStatus;
            h2t.LinkConfidence = pipeLink.LinkConfidence;


            h2t.RiskClaim_Id = claim.Id;
            h2t.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            h2t.Telephone_Id = telephoneId;
            h2t.Handset_Id = HandsetId;

            h2t.FiveGrading = GetFiveGrading();

            _db.Handset2Telephone.Add(h2t);

            _db.SaveChanges();

            return h2t;
        }
    }
}
