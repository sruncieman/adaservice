﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public Handset2Address GetLatestHandset2AddressLink(int HandsetId, int addressId, int baseRiskClaimId)
        {
            return (from x in _db.Handset2Address
                    where x.Handset_Id == HandsetId && x.Address_Id == addressId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public Handset2Address InsertHandset2AddressLink(int handsetId, int addressId, MDA.Pipeline.Model.PipelineAddressLink pipeLink,
            //MDA.Common.Enum.AddressLinkType linkType, 
            RiskClaim claim)
        {
            Handset2Address h2a = _db.Handset2Address.Create();

            h2a.CreatedBy = pipeLink.CreatedBy;
            h2a.CreatedDate = pipeLink.CreatedDate;
            h2a.ModifiedBy = pipeLink.ModifiedBy;
            h2a.ModifiedDate = pipeLink.ModifiedDate;
            h2a.Source = pipeLink.Source;
            h2a.SourceDescription = pipeLink.SourceDescription;
            h2a.SourceReference = pipeLink.SourceReference;
            h2a.ADARecordStatus = pipeLink.ADARecordStatus;
            h2a.RecordStatus = pipeLink.RecordStatus;
            h2a.LinkConfidence = pipeLink.LinkConfidence;

            h2a.RiskClaim_Id = claim.Id;
            h2a.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            h2a.Address_Id = addressId;
            h2a.Handset_Id = handsetId;
            h2a.AddressLinkType_Id = (int)pipeLink.AddressLinkType_Id;
            h2a.RawAddress = pipeLink.RawAddress;

            h2a.FiveGrading = GetFiveGrading();

            _db.Handset2Address.Add(h2a);

            _db.SaveChanges();

            return h2a;
        }

        public Handset2Address UpdateHandset2AddressLink(MDA.Common.Enum.Operation op, int linkId,
            //MDA.Common.Enum.AddressLinkType linkType, 
                                            MDA.Pipeline.Model.PipelineAddressLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Handset2Address where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateHandset2AddressLink(op, link, pipeLink, claim, ref success);
        }

        public Handset2Address UpdateHandset2AddressLink(MDA.Common.Enum.Operation op, Handset2Address link,
            //MDA.Common.Enum.AddressLinkType linkType, 
                                         MDA.Pipeline.Model.PipelineAddressLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (link.AddressLinkType_Id != 0 && link.AddressLinkType_Id != (int)pipeLink.AddressLinkType_Id) // we have value and it's different
                    {
                        success = false;
                    }

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.AddressLinkType_Id != (int)pipeLink.AddressLinkType_Id)
                            link.AddressLinkType_Id = (int)pipeLink.AddressLinkType_Id;

                        link.RawAddress = pipeLink.RawAddress;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;

        }

        public void CleanUpHandset2AddressLinks(/*Handset2Address latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Handset2Address
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Handset2Address Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }
    }
}
