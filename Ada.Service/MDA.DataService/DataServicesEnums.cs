﻿using MDA.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.DataService
{
    public class Constants
    {
        public const int IBaseDeleteValue = 254;
        public const int IBaseDedupeValue = 254;
    }

    public enum LinkConfidence
    {
        Confirmed = 0,
        Unconfirmed = 1,
        Tentative = 2
    };

    public enum ADARecordStatus
    {
        Current = 0,
        ClientDeleted = 1,
        ClientWithdrawn = 2,
        UpdateRemoved = 3,
        AnalystDeleted = 4,
        DedupeDeleted = 5,

        SyncCurrent = 10,
        SyncClientDeleted = 11,
        SyncClientWithdrawn = 12,
        SyncUpdateRemoved = 13
    };


    public enum CaseClaimType
    {
        NA = 0,
        Commercial = 1,
        EmployersLiability = 2,
        Financial = 3,
        Holiday = 4,
        Household = 5,
        PublicLiability = 6,
        RTA_Bogus = 7,
        RTA_CreditHire = 8,
        RTA_ExaggeratedLoss = 9,
        RTA_FireTheft = 10,
        RTA_InducedCollision = 11,
        RTA_IntelReport = 12,
        RTA_LowSpeedImpact = 13,
        RTA_StagedContrived = 14
    };


}
