﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPerson2PassportAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2Passport set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectPassport2PersonLinks(int masterPassportId, int passportId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Passport set Passport_Id = " + masterPassportId +
                                           " where Passport_Id = " + passportId);
        }

        public void RedirectPerson2PassportLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Passport set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        public Person2Passport GetPerson2PassportLink(MdaDbContext db, int id)
        {
            return (from x in db.Person2Passport where x.Id == id select x).FirstOrDefault();
        }

        public Person2Passport GetPerson2PassportLink(int id)
        {
            return GetPerson2PassportLink(id);
        }

        public List<Person2Passport> GetPerson2PassportLinks(int personId, int passportId, int baseRiskClaimId)
        {
            return (from x in _db.Person2Passport
                    where x.Person_Id == personId && x.Passport_Id == passportId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Person2Passport GetLatestPerson2PassportLink(int personId, int passportId, int baseRiskClaimId)
        {
            return (from x in _db.Person2Passport
                    where x.Person_Id == personId && x.Passport_Id == passportId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPerson2PassportLinks(/*Person2Passport latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Person2Passport
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Person2Passport Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Person2Passport UpdatePerson2PassportLink(MDA.Common.Enum.Operation op, Person2Passport link,
                                                    MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeBase.LinkConfidence)
                            link.LinkConfidence = (int)pipeBase.LinkConfidence;

                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Person2Passport UpdatePerson2PassportLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Person2Passport where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePerson2PassportLink(op, link, pipeBase, claim, ref success);
        }

        public Person2Passport InsertPerson2PassportLink(int personId, int passportId, MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim)
        {
            Person2Passport p2pa = _db.Person2Passport.Create();

            p2pa.CreatedBy         = pipeBase.CreatedBy;
            p2pa.CreatedDate       = pipeBase.CreatedDate;
            p2pa.ModifiedBy        = pipeBase.ModifiedBy;
            p2pa.ModifiedDate      = pipeBase.ModifiedDate;
            p2pa.Source            = pipeBase.Source;
            p2pa.SourceDescription = pipeBase.SourceDescription;
            p2pa.SourceReference   = pipeBase.SourceReference;
            p2pa.ADARecordStatus   = pipeBase.ADARecordStatus;
            p2pa.RecordStatus      = pipeBase.RecordStatus;
            p2pa.LinkConfidence    = pipeBase.LinkConfidence;

            p2pa.RiskClaim_Id = claim.Id;
            p2pa.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2pa.Person_Id = personId;
            p2pa.Passport_Id = passportId;

            p2pa.FiveGrading = GetFiveGrading();

            _db.Person2Passport.Add(p2pa);

            _db.SaveChanges();

            return p2pa;
        }

        //public Person2Passport InsertPerson2PassportLink(Person2Passport person2passport)
        //{
        //    Person2Passport p2c = _db.Person2Passport.Create();

        //    p2c.CreatedBy = person2passport.CreatedBy;
        //    p2c.CreatedDate = DateTime.Now;
        //    p2c.BaseRiskClaim_Id = person2passport.BaseRiskClaim_Id;

        //    p2c.Source = person2passport.Source;
        //    p2c.SourceDescription = person2passport.SourceDescription;
        //    p2c.SourceReference = person2passport.SourceReference;

        //    p2c.Passport_Id = person2passport.Passport_Id;
        //    p2c.PassportLinkId = person2passport.PassportLinkId;

        //    p2c.IBaseId = person2passport.IBaseId;
        //    p2c.Person_Id = person2passport.Person_Id;
        //    p2c.RiskClaim_Id = person2passport.RiskClaim_Id;
        //    p2c.LinkConfidence = person2passport.LinkConfidence;
        //    p2c.FiveGrading = person2passport.FiveGrading;

        //    _db.Person2Passport.Add(p2c);

        //    _db.SaveChanges();

        //    return p2c;
        //}
    }

}
