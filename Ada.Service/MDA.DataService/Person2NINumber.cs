﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
 

    public partial class DataServices
    {
        //public void ResetAllPerson2NINumberAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2NINumber set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectNINumber2PersonLinks(int masterNiId, int niId)
        {
            _db.Database.ExecuteSqlCommand("update Person2NINumber set NINumber_Id = " + masterNiId +
                                           " where NINumber_Id = " + niId);
        }

        public void RedirectPerson2NINumberLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Person2NINumber set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        public Person2NINumber GetPerson2NINumberLink(MdaDbContext db, int id)
        {
            return (from x in db.Person2NINumber where x.Id == id select x).FirstOrDefault();
        }

        public Person2NINumber GetPerson2NINumberLink(int id)
        {
            return GetPerson2NINumberLink(id);
        }

        public List<Person2NINumber> GetPerson2NINumberLinks(int personId, int niNumberId, int baseRiskClaimId)
        {
            return (from x in _db.Person2NINumber
                    where x.Person_Id == personId && x.NINumber_Id == niNumberId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Person2NINumber GetLatestPerson2NINumberLink(int personId, int niNumberId, int baseRiskClaimId)
        {
            return (from x in _db.Person2NINumber
                    where x.Person_Id == personId && x.NINumber_Id == niNumberId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPerson2NINumberLinks(/*Person2NINumber latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Person2NINumber
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Person2NINumber Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Person2NINumber UpdatePerson2NINumberLink(MDA.Common.Enum.Operation op, Person2NINumber link,
                                                MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeBase.LinkConfidence)
                            link.LinkConfidence = (int)pipeBase.LinkConfidence;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Person2NINumber UpdatePerson2NINumberLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Person2NINumber where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePerson2NINumberLink(op, link, pipeBase, claim, ref success);
        }

        public Person2NINumber InsertPerson2NINumberLink(int personId, int niNumberId, MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim)
        {
            Person2NINumber p2n = _db.Person2NINumber.Create();

            p2n.CreatedBy         = pipeBase.CreatedBy;
            p2n.CreatedDate       = pipeBase.CreatedDate;
            p2n.ModifiedBy        = pipeBase.ModifiedBy;
            p2n.ModifiedDate      = pipeBase.ModifiedDate;
            p2n.Source            = pipeBase.Source;
            p2n.SourceDescription = pipeBase.SourceDescription;
            p2n.SourceReference   = pipeBase.SourceReference;
            p2n.ADARecordStatus   = pipeBase.ADARecordStatus;
            p2n.RecordStatus      = pipeBase.RecordStatus;
            p2n.LinkConfidence    = pipeBase.LinkConfidence;

            p2n.RiskClaim_Id = claim.Id;
            p2n.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2n.Person_Id = personId;
            p2n.NINumber_Id = niNumberId;
            
            p2n.FiveGrading = GetFiveGrading();

            _db.Person2NINumber.Add(p2n);

            _db.SaveChanges();

            return p2n;
        }

        //public Person2NINumber InsertPerson2NINumberLink(Person2NINumber person2ni)
        //{
        //    Person2NINumber p2n = _db.Person2NINumber.Create();

        //    p2n.CreatedBy = person2ni.CreatedBy;
        //    p2n.CreatedDate = DateTime.Now;
        //    p2n.BaseRiskClaim_Id = person2ni.BaseRiskClaim_Id;

        //    p2n.Source = person2ni.Source;
        //    p2n.SourceDescription = person2ni.SourceDescription;
        //    p2n.SourceReference = person2ni.SourceReference;

        //    p2n.NINumber_Id = person2ni.NINumber_Id;
        //    p2n.NINumberLinkId = person2ni.NINumberLinkId;

        //    p2n.IBaseId = person2ni.IBaseId;
        //    p2n.Person_Id = person2ni.Person_Id;
        //    p2n.RiskClaim_Id = person2ni.RiskClaim_Id;
        //    p2n.LinkConfidence = person2ni.LinkConfidence;
        //    p2n.FiveGrading = person2ni.FiveGrading;

        //    _db.Person2NINumber.Add(p2n);

        //    _db.SaveChanges();

        //    return p2n;
        //}
    }

}
