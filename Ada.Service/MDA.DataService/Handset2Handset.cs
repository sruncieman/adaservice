﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;


namespace MDA.DataService
{
    public partial class DataServices
    {
        public Handset2Handset GetLatestHandset2HandsetLink(int handset1Id, int handset2Id) //, int baseRiskClaimId)
        {
            return (from x in _db.Handset2Handset
                    where ((x.Handset1_Id == handset1Id && x.Handset2_Id == handset2Id) || (x.Handset2_Id == handset1Id && x.Handset1_Id == handset2Id)) &&
                          (x.ADARecordStatus == (byte)ADARecordStatus.Current || x.ADARecordStatus == (byte)ADARecordStatus.SyncCurrent)
                    select x).FirstOrDefault();
        }

        public Handset2Handset UpdateHandset2HandsetLink(MDA.Common.Enum.Operation op, int linkId,
                                               DateTime? dateOfRegChange, MDA.Pipeline.Model.PipelineHandset2HandsetLink pipeLink, /*RiskClaim claim, */ ref bool success)
        {
            success = false;

            var link = (from x in _db.Handset2Handset where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateHandset2HandsetLink(op, link, /*dateOfRegChange,*/ pipeLink, /*claim,*/ ref success);
        }

        public Handset2Handset UpdateHandset2HandsetLink(MDA.Common.Enum.Operation op, Handset2Handset link,
            //DateTime? dateOfRegChange, 
                                                            MDA.Pipeline.Model.PipelineHandset2HandsetLink pipeLink, /*RiskClaim newRiskClaim,*/ ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Handset.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        //link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence && link.LinkConfidence > (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        //if (link.DateOfRegChange != MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.DateOfRegChange))
                        //    link.DateOfRegChange = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.DateOfRegChange);
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;
        }


        public Handset2Handset InsertHandset2HandsetLink(int Handset1Id, int Handset2Id, MDA.Pipeline.Model.PipelineHandset2HandsetLink pipeLink /*DateTime? dateOfRegChange*/) //, RiskClaim claim)
        {
            Handset2Handset h2h = _db.Handset2Handset.Create();

            h2h.CreatedBy = pipeLink.CreatedBy;
            h2h.CreatedDate = pipeLink.CreatedDate;
            h2h.ModifiedBy = pipeLink.ModifiedBy;
            h2h.ModifiedDate = pipeLink.ModifiedDate;
            //h2h.Source            = pipeLink.Source;
            //h2h.SourceDescription = pipeLink.SourceDescription;
            //h2h.SourceReference   = pipeLink.SourceReference;
            h2h.ADARecordStatus = pipeLink.ADARecordStatus;
            h2h.RecordStatus = pipeLink.RecordStatus;
            h2h.LinkConfidence = pipeLink.LinkConfidence;

            h2h.Handset1_Id = Handset1Id;
            h2h.Handset2_Id = Handset2Id;
            // h2h.RiskClaim_Id = claim.Id;
            //h2h.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            h2h.Source = "Keoghs CFS";
            h2h.SourceDescription = "ADA Auto-Matching";
            h2h.SourceReference = null;

            h2h.FiveGrading = GetFiveGrading();
            //h2h.DateOfRegChange = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.DateOfRegChange);

            _db.Handset2Handset.Add(h2h);

            _db.SaveChanges();

            return h2h;
        }
    }
}
