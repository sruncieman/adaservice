﻿using System;
using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {

        //public void ResetAllOrganisation2WebsiteAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Organisation2Website set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectWebsite2OrganisationLinks(int masterWebsiteId, int websiteId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2Website set WebSite_Id = " + masterWebsiteId +
                                           " where WebSite_Id = " + websiteId);
        }

        public void RedirectOrganisation2WebsiteLinks(int masterOrganisationId, int organisationId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2Website set Organisation_Id = " + masterOrganisationId +
                                           " where Organisation_Id = " + organisationId);
        }

        public List<Organisation2WebSite> GetOrganisation2WebsiteLinks(int orgId, int websiteId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2WebSite
                    where x.Organisation_Id == orgId && x.WebSite_Id == websiteId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Organisation2WebSite GetLatestOrganisation2WebsiteLink(int orgId, int websiteId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2WebSite
                    where x.Organisation_Id == orgId && x.WebSite_Id == websiteId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpOrganisation2WebsiteLinks(/*Organisation2WebSite latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Organisation2WebSite
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Organisation2Website Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Organisation2WebSite UpdateOrganisation2WebsiteLink(MDA.Common.Enum.Operation op, Organisation2WebSite link,
                                                        MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeBase.LinkConfidence)
                            link.LinkConfidence = (int)pipeBase.LinkConfidence;

                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Organisation2WebSite UpdateOrganisation2WebSiteLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Organisation2WebSite where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateOrganisation2WebsiteLink(op, link, pipeBase, claim, ref success);
        }

        public Organisation2WebSite InsertOrganisation2WebsiteLink(int orgId, int websiteId, MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim)
        {
            Organisation2WebSite o2w = _db.Organisation2WebSite.Create();

            o2w.CreatedBy         = pipeBase.CreatedBy;
            o2w.CreatedDate       = pipeBase.CreatedDate;
            o2w.ModifiedBy        = pipeBase.ModifiedBy;
            o2w.ModifiedDate      = pipeBase.ModifiedDate;
            o2w.Source            = pipeBase.Source;
            o2w.SourceDescription = pipeBase.SourceDescription;
            o2w.SourceReference   = pipeBase.SourceReference;
            o2w.ADARecordStatus   = pipeBase.ADARecordStatus;
            o2w.RecordStatus      = pipeBase.RecordStatus;
            o2w.LinkConfidence    = pipeBase.LinkConfidence;

            o2w.RiskClaim_Id = claim.Id;
            o2w.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            o2w.WebSite_Id = websiteId;
            o2w.Organisation_Id = orgId;
           
            o2w.FiveGrading = GetFiveGrading();

            _db.Organisation2WebSite.Add(o2w);

            _db.SaveChanges();

            return o2w;
        }

        //public Organisation2WebSite InsertOrganisation2WebsiteLink(Organisation2WebSite org2website)
        //{
        //    Organisation2WebSite o2w = _db.Organisation2WebSite.Create();

        //    o2w.CreatedBy = org2website.CreatedBy;
        //    o2w.CreatedDate = DateTime.Now;
        //    o2w.BaseRiskClaim_Id = org2website.BaseRiskClaim_Id;

        //    o2w.Source = org2website.Source;
        //    o2w.SourceDescription = org2website.SourceDescription;
        //    o2w.SourceReference = org2website.SourceReference;
        //    o2w.IBaseId = org2website.IBaseId;

        //    o2w.Organisation_Id = org2website.Organisation_Id;
        //    o2w.RiskClaim_Id = org2website.RiskClaim_Id;

        //    o2w.WebSite_Id = org2website.WebSite_Id;
        //    o2w.WebSiteLinkId = org2website.WebSiteLinkId;
        //    o2w.LinkConfidence = org2website.LinkConfidence;
        //    o2w.FiveGrading = org2website.FiveGrading;

        //    _db.Organisation2WebSite.Add(o2w);

        //    _db.SaveChanges();

        //    return o2w;
        //}
    }

}

