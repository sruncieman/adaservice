﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllVehicle2AddressAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Vehicle2Address set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectVehicle2AddressLinks(int masterVehicleId, int vehicleId)
        {
            _db.Database.ExecuteSqlCommand("update Vehicle2Address set Vehicle_Id = " + masterVehicleId +
                                           " where Vehicle_Id = " + vehicleId);
        }

        public void RedirectAddress2VehicleLinks(int masterAddressId, int addressId)
        {
            _db.Database.ExecuteSqlCommand("update Vehicle2Address set Address_Id = " + masterAddressId +
                                           " where Address_Id = " + addressId);
        }

        public List<Vehicle2Address> GetVehicle2AddressLinks(int vehicleId, int addressId, int baseRiskClaimId)
        {
            return (from x in _db.Vehicle2Address
                    where x.Vehicle_Id == vehicleId && x.Address_Id == addressId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Vehicle2Address GetLatestVehicle2AddressLink(int vehicleId, int addressId, int baseRiskClaimId)
        {
            return (from x in _db.Vehicle2Address
                    where x.Vehicle_Id == vehicleId && x.Address_Id == addressId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpVehicle2AddressLinks(/*Vehicle2Address latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Vehicle2Address
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Vehicle2Address Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Vehicle2Address UpdateVehicle2AddressLink(MDA.Common.Enum.Operation op, Vehicle2Address link,
                                            //MDA.Common.Enum.AddressLinkType linkType, 
                                            MDA.Pipeline.Model.PipelineAddressLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (link.AddressLinkType_Id != 0 && link.AddressLinkType_Id != (int)pipeLink.AddressLinkType_Id) // we have value and it's different
                    {
                        success = false;
                    }

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.AddressLinkType_Id != (int)pipeLink.AddressLinkType_Id)
                            link.AddressLinkType_Id = (int)pipeLink.AddressLinkType_Id;

                        link.RawAddress = pipeLink.RawAddress;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;

        }


        public Vehicle2Address UpdateVehicle2AddressLink(MDA.Common.Enum.Operation op, int linkId,
                                            //MDA.Common.Enum.AddressLinkType linkType, 
                                            MDA.Pipeline.Model.PipelineAddressLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Vehicle2Address where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateVehicle2AddressLink(op, link, pipeLink, claim, ref success);
        }

        //public Vehicle2Address UpdateVehicle2Address(int linkId, MDA.Pipeline.Model.PipelineAddress address, out bool success)
        //{
        //    success = false;

        //    var p = (from x in _db.Vehicle2Address where x.Id == linkId select x).FirstOrDefault();

        //    if (p == null) return p;

        //    try
        //    {
        //        success = true;

        //        bool modified = false;

        //        // Check all the breaking changes first

        //        if (p.AddressLinkType_Id != 0 && p.AddressLinkType_Id != (int)address.AddressLinkType) // we have value and it's different
        //        {
        //            success = false;
        //        }

        //        if (!success) return p;

        //        if (p.AddressLinkType_Id != (int)address.AddressLinkType)
        //        {
        //            p.AddressLinkType_Id = (int)address.AddressLinkType;
        //            modified = true;
        //        }

        //        if (modified)
        //        {
        //            p.ModifiedDate = DateTime.Now;
        //            p.ModifiedBy = _ctx.Who;

        //            _db.SaveChanges();
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        success = false;
        //    }

        //    return p;
        //}

        public Vehicle2Address InsertVehicle2AddressLink(int vehicleId, int addressId, MDA.Pipeline.Model.PipelineAddressLink pipeLink, 
            //MDA.Common.Enum.AddressLinkType linkType, 
            RiskClaim claim)
        {
            Vehicle2Address v2a = _db.Vehicle2Address.Create();

            v2a.CreatedBy         = pipeLink.CreatedBy;
            v2a.CreatedDate       = pipeLink.CreatedDate;
            v2a.ModifiedBy        = pipeLink.ModifiedBy;
            v2a.ModifiedDate      = pipeLink.ModifiedDate;
            v2a.Source            = pipeLink.Source;
            v2a.SourceDescription = pipeLink.SourceDescription;
            v2a.SourceReference   = pipeLink.SourceReference;
            v2a.ADARecordStatus   = pipeLink.ADARecordStatus;
            v2a.RecordStatus      = pipeLink.RecordStatus;
            v2a.LinkConfidence    = pipeLink.LinkConfidence;

            v2a.RiskClaim_Id = claim.Id;
            v2a.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            v2a.Address_Id = addressId;
            v2a.Vehicle_Id = vehicleId;
            v2a.AddressLinkType_Id = (int)pipeLink.AddressLinkType_Id;
            v2a.RawAddress = pipeLink.RawAddress;

            v2a.FiveGrading = GetFiveGrading();

            _db.Vehicle2Address.Add(v2a);

            _db.SaveChanges();

            return v2a;
        }

        //public Vehicle2Address InsertVehicle2AddressLink(Vehicle2Address vehicle2address)
        //{
        //    Vehicle2Address v2a = _db.Vehicle2Address.Create();

        //    v2a.CreatedBy = vehicle2address.CreatedBy;
        //    v2a.CreatedDate = DateTime.Now;
        //    v2a.BaseRiskClaim_Id = vehicle2address.BaseRiskClaim_Id;

        //    v2a.Source = vehicle2address.Source;
        //    v2a.SourceDescription = vehicle2address.SourceDescription;
        //    v2a.SourceReference = vehicle2address.SourceReference;

        //    v2a.Address_Id = vehicle2address.Address_Id;
        //    v2a.AddressLinkId = vehicle2address.AddressLinkId;
        //    v2a.AddressLinkType_Id = vehicle2address.AddressLinkType_Id;
        //    v2a.EndOfResidency = vehicle2address.EndOfResidency;
        //    v2a.Notes = vehicle2address.Notes;
        //    v2a.StartOfResidency = vehicle2address.StartOfResidency;
        //    v2a.Vehicle_Id = vehicle2address.Vehicle_Id;

        //    v2a.IBaseId = vehicle2address.IBaseId;
        //    v2a.RiskClaim_Id = vehicle2address.RiskClaim_Id;
        //    v2a.LinkConfidence = vehicle2address.LinkConfidence;
        //    v2a.FiveGrading = GetFiveGrading();

        //    _db.Vehicle2Address.Add(v2a);

        //    _db.SaveChanges();

        //    return v2a;
        //}
    }

}
