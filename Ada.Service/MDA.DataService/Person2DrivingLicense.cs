﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPerson2DrivingLicenseAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2DrivingLicense set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectDrivingLicense2PersonLinks(int masterDrivingLicenseId, int drivingLicenseId)
        {
            _db.Database.ExecuteSqlCommand("update Person2DrivingLicense set DrivingLicense_Id = " + masterDrivingLicenseId +
                                           " where DrivingLicense_Id = " + drivingLicenseId);
        }

        public void RedirectPerson2DrivingLicenseLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Person2DrivingLicense set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        public Person2DrivingLicense GetPerson2DrivingLicenseLink(MdaDbContext db, int id)
        {
            return (from x in db.Person2DrivingLicense where x.Id == id select x).FirstOrDefault();
        }

        public Person2DrivingLicense GetPerson2DrivingLicenseLink(int id)
        {
            return GetPerson2DrivingLicenseLink(id);
        }

        public List<Person2DrivingLicense> GetPerson2DrivingLicenseLinks(int personId, int licenseId, int baseRiskClaimId)
        {
            return (from x in _db.Person2DrivingLicense
                    where x.Person_Id == personId && x.DrivingLicense_Id == licenseId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Person2DrivingLicense GetLatestPerson2DrivingLicenseLink(int personId, int licenseId, int baseRiskClaimId)
        {
            return (from x in _db.Person2DrivingLicense
                    where x.Person_Id == personId && x.DrivingLicense_Id == licenseId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPerson2DrivingLicenseLinks(/*Person2DrivingLicense latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Person2DrivingLicense
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Person2DrivingLicense Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Person2DrivingLicense UpdatePerson2DrivingLicenseLink(MDA.Common.Enum.Operation op, Person2DrivingLicense link,
                                                    MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeBase.LinkConfidence)
                            link.LinkConfidence = (int)pipeBase.LinkConfidence;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Person2DrivingLicense UpdatePerson2DrivingLicenseLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Person2DrivingLicense where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePerson2DrivingLicenseLink(op, link, pipeBase, claim, ref success);
        }

        public Person2DrivingLicense InsertPerson2DrivingLicenseLink(int personId, int licenseId, MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim)
        {
            Person2DrivingLicense p2d = _db.Person2DrivingLicense.Create();

            p2d.CreatedBy         = pipeBase.CreatedBy;
            p2d.CreatedDate       = pipeBase.CreatedDate;
            p2d.ModifiedBy        = pipeBase.ModifiedBy;
            p2d.ModifiedDate      = pipeBase.ModifiedDate;
            p2d.Source            = pipeBase.Source;
            p2d.SourceDescription = pipeBase.SourceDescription;
            p2d.SourceReference   = pipeBase.SourceReference;
            p2d.ADARecordStatus   = pipeBase.ADARecordStatus;
            p2d.RecordStatus      = pipeBase.RecordStatus;
            p2d.LinkConfidence    = pipeBase.LinkConfidence;

            p2d.RiskClaim_Id = claim.Id;
            p2d.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2d.Person_Id = personId;
            p2d.DrivingLicense_Id = licenseId;
            
            p2d.FiveGrading = GetFiveGrading();

            _db.Person2DrivingLicense.Add(p2d);

            _db.SaveChanges();

            return p2d;
        }

        //public Person2DrivingLicense InsertPerson2DrivingLicenseLink(Person2DrivingLicense person2license)
        //{
        //    Person2DrivingLicense p2dl = _db.Person2DrivingLicense.Create();

        //    p2dl.CreatedBy = person2license.CreatedBy;
        //    p2dl.CreatedDate = DateTime.Now;
        //    p2dl.BaseRiskClaim_Id = person2license.BaseRiskClaim_Id;

        //    p2dl.Source = person2license.Source;
        //    p2dl.SourceDescription = person2license.SourceDescription;
        //    p2dl.SourceReference = person2license.SourceReference;

        //    p2dl.DrivingLicense_Id = person2license.DrivingLicense_Id;
        //    p2dl.DrivingLicenseLinkId = person2license.DrivingLicenseLinkId;
        //    p2dl.Person_Id = person2license.Person_Id;
        //    p2dl.RiskClaim_Id = person2license.RiskClaim_Id;

        //    p2dl.IBaseId = person2license.IBaseId;
        //    p2dl.Person_Id = person2license.Person_Id;
        //    p2dl.RiskClaim_Id = person2license.RiskClaim_Id;
        //    p2dl.LinkConfidence = person2license.LinkConfidence;
        //    p2dl.FiveGrading = person2license.FiveGrading;

        //    _db.Person2DrivingLicense.Add(p2dl);

        //    _db.SaveChanges();

        //    return p2dl;
        //}
    }

}
