﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Helpers;
using LinqKit;
using RiskEngine.Model;
using MDA.Common.Debug;
using RiskEngine.Interfaces;
using MDA.Common;

namespace MDA.DataService
{
    public partial class DataServices
    {

        public void CleanseHandset(int id, Func<Handset, MessageNode, int> cleanseMethod)
        {
            var h = (from x in _db.Handsets where x.Id == id select x).FirstOrDefault();

            if (h == null) return;

            MessageNode n = new MessageNode();

            cleanseMethod(h, n);

            _db.SaveChanges();
        }

        public List<int> FindAllHandsetAliases(int id, EntityAliases handsetAliases, bool _trace)
        {

            List<int> retList1 = new List<int>();

            bool includeConfirmed;
            bool includeUnconfirmed;
            bool includeTentative;
            bool includeThis;

            ProcessMatchType(handsetAliases.MatchType, out includeThis, out includeConfirmed, out includeUnconfirmed, out includeTentative);

            if (includeThis)
                retList1.Add(id);

            if (includeConfirmed)
                retList1.AddRange(handsetAliases.ConfirmedAliases);

            if (includeUnconfirmed)
                retList1.AddRange(handsetAliases.UnconfirmedAliases);

            if (includeTentative)
                retList1.AddRange(handsetAliases.TentativeAliases);

            retList1 = retList1.Distinct().ToList();

            //if (_trace)
            //{
            //    ADATrace.WriteLine("FindVehicleAliasesCached: VehicleId=" + id.ToString() + " : " + vehicleAliases.MatchType + "=[" + string.Join(",", retList1) + "]");
            //}

            return retList1;
        }

        public Handset UpdateHandset(int handsetId, MDA.Pipeline.Model.PipelineHandset handset, out bool success, out string error)
        {
            success = false;
            error = "";

            var h = (from x in _db.Handsets where x.Id == handsetId select x).FirstOrDefault();

            if (h == null) return h;

            try
            {
                StringBuilder err = new StringBuilder();

                success = true;

                bool modified = false;

                if (h.HandsetIMEI != null && !string.IsNullOrEmpty(h.HandsetIMEI) && string.Compare(h.HandsetIMEI, handset.HandsetIMEI, true) != 0) // we have value and it's different
                {
                    success = false;
                    err.Append("Handset IMEI - ");
                }

                if (handset.HandsetMake != null && !string.IsNullOrEmpty(h.HandsetMake) && string.Compare(h.HandsetMake, handset.HandsetMake, true) != 0) // we have value and it's different
                {
                    success = false;
                    err.Append("Handset Make - ");
                }

                if (handset.HandsetModel != null && !string.IsNullOrEmpty(h.HandsetModel) && string.Compare(h.HandsetModel, handset.HandsetModel, true) != 0) // we have value and it's different
                {
                    success = false;
                    err.Append("Handset Model - ");
                }

                if (!success)
                {
                    err.Append("overwrites existing value");
                    error = err.ToString();
                    return h;
                }

                if (string.IsNullOrEmpty(h.HandsetIMEI))
                {
                    h.HandsetIMEI = handset.HandsetIMEI;
                    modified = true;
                }

                if (string.IsNullOrEmpty(h.HandsetMake))
                {
                    h.HandsetMake = handset.HandsetMake;
                    modified = true;
                }

                if (string.IsNullOrEmpty(h.HandsetModel))
                {
                    h.HandsetModel = handset.HandsetModel;
                    modified = true;
                }

                if (h.HandsetColour_Id != (int)handset.HandsetColour_Id && (int)handset.HandsetColour_Id > 0)
                {
                    h.HandsetColour_Id = (int)handset.HandsetColour_Id;
                    modified = true;
                }

                

                // Value and Value Category?
                
               
                if (modified)
                {
                    h.ModifiedDate = DateTime.Now;
                    h.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return h;

        }

        public Handset InsertHandset(MDA.Pipeline.Model.PipelineHandset handset)
        {
            Handset h = _db.Handsets.Create();

            h.ADARecordStatus = handset.ADARecordStatus;
            h.CreatedBy = handset.CreatedBy;
            h.CreatedDate = handset.CreatedDate;
            h.IBaseId = handset.IBaseId;
            h.KeyAttractor = handset.KeyAttractor;
            h.ModifiedBy = handset.ModifiedBy;
            h.ModifiedDate = handset.ModifiedDate;
            h.RecordStatus = handset.RecordStatus;
            h.Source = handset.Source;
            h.SourceDescription = handset.SourceDescription;
            h.SourceReference = handset.SourceReference;
            h.HandsetIMEI = handset.HandsetIMEI;
            h.HandsetMake = handset.HandsetMake;
            h.HandsetModel = handset.HandsetModel;
            h.HandsetColour_Id = handset.HandsetColour_Id;
            h.HandsetValue = handset.HandsetValue;
            
            _db.Handsets.Add(h);

            _db.SaveChanges();

            return h;
        }

        public Handset SelectHandset(int id)
        {
            return (from x in _db.Handsets where x.Id == id select x).FirstOrDefault();
        }
    }
}
