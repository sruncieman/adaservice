﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public void RedirectIncident2IncidentLinks(int masterIncidentId, int incidentId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2Incident set Incident1_Id = " + masterIncidentId +
                                           " where Incident1_Id = " + incidentId);

            _db.Database.ExecuteSqlCommand("update Incident2Incident set Incident2_Id = " + masterIncidentId +
                                           " where Incident2_Id = " + incidentId);
        }

        //public void ResetAllIncident2IncidentLinkAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Incident2Incident set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        //public List<Incident2Incident> GetIncident2IncidentLinks(int incident1Id, int incident2Id) //#, int baseRiskClaimId)
        //{
        //    return (from x in _db.Incident2Incident
        //            where x.Incident1_Id == incident1Id 
        //               && x.Incident2_Id == incident2Id
        //            //#&& x.BaseRiskClaim_Id == baseRiskClaimId
        //            orderby x.CreatedDate ascending
        //            select x).ToList();
        //}

        //public Incident2Incident GetLatestIncident2IncidentLink(int incident1Id, int incident2Id) //#, int baseRiskClaimId)
        //{
        //    return (from x in _db.Incident2Incident
        //            where x.Incident1_Id == incident1Id && x.Incident2_Id == incident2Id
        //                   && x.ADARecordStatus == (byte)ADARecordStatus.Current
        //                //#&& x.BaseRiskClaim_Id == baseRiskClaimId 
        //                  //&& x.SourceDescription == "ADA"
        //            orderby x.CreatedDate descending
        //            select x).FirstOrDefault();
        //}

        //public void CleanUpIncident2IncidentLinks(/*Incident2Incident latestLink,*/ RiskClaim newRiskClaim)
        //{
        //    // Get all I2V links for this vehicle and BaseId except the latest
        //    var oldlinks = (from x in _db.Incident2Incident
        //                    where //(x.Incident1_Id == latestLink.Incident1_Id || 
        //                          // x.Incident2_Id == latestLink.Incident2_Id) &&
        //                          x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
        //                          x.RiskClaim_Id != newRiskClaim.Id &&
        //                          x.ADARecordStatus == (byte)ADARecordStatus.Current &&
        //                          x.SourceDescription == "ADA"
        //                    select x).ToList();

        //    foreach (var link in oldlinks)
        //    {
        //        if (ctx.Tracing)
        //            ADATrace.WriteLine("Incident2Incident Link[" + link.Id.ToString() + "] marked as UpdateRemoved" );

        //        link.RecordStatus = Constants.IBaseDeleteValue;
        //        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
        //    }
        //    _db.SaveChanges();
        //}

        //public Incident2Incident UpdateIncident2IncidentLink(MDA.Common.Enum.Operation op, Incident2Incident link,
        //                                            LinkConfidence linkConfidence, /*RiskClaim newRiskClaim,*/ ref bool success)
        //{
        //    success = true;

        //    try
        //    {
        //        if (op == MDA.Common.Enum.Operation.Delete)
        //        {
        //            //_db.Incident2Vehicle.Remove(link);

        //            link.RecordStatus = Constants.IBaseDeleteValue;
        //            link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
        //        }
        //        else if (op == MDA.Common.Enum.Operation.Withdrawn)
        //        {
        //            link.RecordStatus = Constants.IBaseDeleteValue;
        //            link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
        //        }
        //        else
        //        {

        //            // check for invalid updates and if so set success = false : NONE

        //            if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
        //            {
        //                link.RecordStatus = Constants.IBaseDeleteValue;
        //                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
        //            }
        //            else
        //            {
        //                //#link.RiskClaim_Id = newRiskClaim.Id;

        //                if (link.LinkConfidence != (int)linkConfidence)
        //                    link.LinkConfidence = (int)linkConfidence;
        //            }

        //        }

        //        link.ModifiedDate = DateTime.Now;
        //        link.ModifiedBy = _ctx.Who;

        //        _db.SaveChanges();
        //    }
        //    catch (Exception)
        //    {
        //        success = false;
        //    }

        //    return link;
        //}


        //public Incident2Incident UpdateIncident2IncidentLink(MDA.Common.Enum.Operation op, int linkId,
        //                                       LinkConfidence linkConfidence, /*RiskClaim claim,*/ ref bool success)
        //{
        //    success = false;

        //    var link = (from x in _db.Incident2Incident where x.Id == linkId select x).FirstOrDefault();

        //    if (link == null) return link;

        //    return UpdateIncident2IncidentLink(op, link, linkConfidence, /*claim,*/ ref success);
        //}

        //public Incident2Incident InsertIncident2IncidentLink(int incidentId1, int incidentId2, LinkConfidence linkConfidence, RiskClaim claim)
        //{
        //    Incident2Incident i2i = _db.Incident2Incident.Create();

        //    i2i.CreatedBy = _ctx.Who;
        //    i2i.CreatedDate = DateTime.Now;
        //    //#i2i.RiskClaim_Id = claim.Id;
        //    //#i2i.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;
        //    i2i.Source = "Keoghs CFS";
        //    i2i.SourceDescription = "ADA Auto-Matching";
        //    i2i.SourceReference = null;
        //    i2i.LinkConfidence = (int)linkConfidence;
        //    i2i.FiveGrading = GetFiveGrading();

        //    i2i.Incident1_Id = incidentId1;
        //    i2i.Incident2_Id = incidentId2;

        //    _db.Incident2Incident.Add(i2i);

        //    _db.SaveChanges();

        //    return i2i;
        //}

        //public Incident2Incident InsertIncident2IncidentLink(Incident2Incident incident2incident)
        //{
        //    Incident2Incident i2i = _db.Incident2Incident.Create();

        //    i2i.CreatedBy = incident2incident.CreatedBy;
        //    i2i.CreatedDate = DateTime.Now;
        //    //#i2i.BaseRiskClaim_Id = incident2incident.BaseRiskClaim_Id;

        //    i2i.Source = incident2incident.Source;
        //    i2i.SourceDescription = incident2incident.SourceDescription;
        //    i2i.SourceReference = incident2incident.SourceReference;
        //    i2i.IBaseId = incident2incident.IBaseId;

        //    i2i.Incident1_Id = incident2incident.Incident1_Id;
        //    i2i.Incident2_Id = incident2incident.Incident2_Id;
        //    i2i.IncidentMatchLinkId = incident2incident.IncidentMatchLinkId;
        //    i2i.LinkConfidence = incident2incident.LinkConfidence;
        //    i2i.FiveGrading = incident2incident.FiveGrading;

        //    //#i2i.RiskClaim_Id = incident2incident.RiskClaim_Id;

        //    _db.Incident2Incident.Add(i2i);

        //    _db.SaveChanges();

        //    return i2i;
        //}
    }

}
