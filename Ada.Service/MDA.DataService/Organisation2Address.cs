﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllOrganisation2AddressAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Organisation2Address set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectAddress2OrganisationLinks(int masterAddressId, int addressId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2Address set Address_Id = " + masterAddressId +
                                           " where Address_Id = " + addressId);
        }

        public void RedirectOrganisation2AddressLinks(int masterOrganisationId, int organisationId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2Address set Organisation_Id = " + masterOrganisationId +
                                           " where Organisation_Id = " + organisationId);
        }

        public List<Organisation2Address> GetOrganisation2AddressLinks(int organisationId, int addressId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2Address
                    where x.Organisation_Id == organisationId && x.Address_Id == addressId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Organisation2Address GetLatestOrganisation2AddressLink(int organisationId, int addressId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2Address
                    where x.Organisation_Id == organisationId && x.Address_Id == addressId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpOrganisation2AddressLinks(RiskClaim newRiskClaim)
        {
            // Get all O2A links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Organisation2Address
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Organisation2Address Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Organisation2Address UpdateOrganisation2AddressLink(MDA.Common.Enum.Operation op, Organisation2Address link,
                                           // MDA.Common.Enum.AddressLinkType linkType, DateTime? startOfResidency, DateTime? endOfResidency,
                                            MDA.Pipeline.Model.PipelineAddressLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (_ctx.SubmitDirect == false)
	                    {
                            link.AddressLinkType_Id = (int)MDA.Common.Enum.AddressLinkType.TradingAddress;
	                    }
                      

                        if (link.StartOfResidency != MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.StartOfResidency))
                            link.StartOfResidency = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.StartOfResidency);

                        if (link.EndOfResidency != MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.EndOfResidency))
                            link.EndOfResidency = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.EndOfResidency);

                        link.RawAddress = pipeLink.RawAddress;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;

        }


        public Organisation2Address UpdateOrganisation2AddressLink(MDA.Common.Enum.Operation op, int linkId,
                                           //MDA.Common.Enum.AddressLinkType linkType, DateTime? startOfResidency, DateTime? endOfResidency,
                                           MDA.Pipeline.Model.PipelineAddressLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Organisation2Address where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateOrganisation2AddressLink(op, link, /*linkType, startOfResidency, endOfResidency,*/ pipeLink, claim, ref success);
        }


        public Organisation2Address InsertOrganisation2AddressLink(int orgId, int addressId, MDA.Pipeline.Model.PipelineAddressLink pipeLink,
            //MDA.Common.Enum.AddressLinkType linkType, DateTime? startOfResidency, DateTime? endOfResidency, 
            RiskClaim claim)
        {
            Organisation2Address o2a = _db.Organisation2Address.Create();

            o2a.CreatedBy         = pipeLink.CreatedBy;
            o2a.CreatedDate       = pipeLink.CreatedDate;
            o2a.ModifiedBy        = pipeLink.ModifiedBy;
            o2a.ModifiedDate      = pipeLink.ModifiedDate;
            o2a.Source            = pipeLink.Source;
            o2a.SourceDescription = pipeLink.SourceDescription;
            o2a.SourceReference   = pipeLink.SourceReference;
            o2a.ADARecordStatus   = pipeLink.ADARecordStatus;
            o2a.RecordStatus      = pipeLink.RecordStatus;
            o2a.LinkConfidence    = pipeLink.LinkConfidence;

            o2a.RiskClaim_Id     = claim.Id;
            o2a.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            o2a.Organisation_Id    = orgId;
            o2a.Address_Id         = addressId;

            if (_ctx.SubmitDirect == false)
            {
                o2a.AddressLinkType_Id = (int)MDA.Common.Enum.AddressLinkType.TradingAddress;
            }
            else
            {
                if (pipeLink.AddressLinkType_Id == (int)MDA.Common.Enum.AddressLinkType.StorageAddress)
                {
                    o2a.AddressLinkType_Id = (int)MDA.Common.Enum.AddressLinkType.TradingAddress;
                }
                else
                {
                    o2a.AddressLinkType_Id = pipeLink.AddressLinkType_Id;
                }
            }

            
            o2a.EndOfResidency     = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.EndOfResidency);
            o2a.StartOfResidency   = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.StartOfResidency);
            o2a.RawAddress         = pipeLink.RawAddress;

            o2a.FiveGrading = GetFiveGrading();

            _db.Organisation2Address.Add(o2a);

            _db.SaveChanges();

            return o2a;
        }

        //public Organisation2Address InsertOrganisation2AddressLink(Organisation2Address org2address)
        //{
        //    Organisation2Address o2a = _db.Organisation2Address.Create();

        //    o2a.CreatedBy = org2address.CreatedBy;
        //    o2a.CreatedDate = DateTime.Now;
        //    o2a.BaseRiskClaim_Id = org2address.BaseRiskClaim_Id;

        //    o2a.Source = org2address.Source;
        //    o2a.SourceDescription = org2address.SourceDescription;
        //    o2a.SourceReference = org2address.SourceReference;

        //    o2a.Address_Id = org2address.Address_Id;
        //    o2a.AddressLinkId = org2address.AddressLinkId;
        //    o2a.AddressLinkType_Id = org2address.AddressLinkType_Id;
        //    o2a.EndOfResidency = org2address.EndOfResidency;
        //    o2a.IBaseId = org2address.IBaseId;
        //    o2a.Notes = org2address.Notes;
        //    o2a.Organisation_Id = org2address.Organisation_Id;
        //    o2a.RiskClaim_Id = org2address.RiskClaim_Id;
        //    o2a.StartOfResidency = org2address.StartOfResidency;
        //    o2a.LinkConfidence = org2address.LinkConfidence;
        //    o2a.FiveGrading = org2address.FiveGrading;

        //    _db.Organisation2Address.Add(o2a);

        //    _db.SaveChanges();

        //    return o2a;
        //}
    }

}
