﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllOrganisation2EmailAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Organisation2Email set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectEmail2OrganisationLinks(int masterEmailId, int emailId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2Email set Email_Id = " + masterEmailId +
                                           " where Email_Id = " + emailId);
        }

        public void RedirectOrganisation2EmailLinks(int masterOrganisationId, int organisationId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2Email set Organisation_Id = " + masterOrganisationId +
                                           " where Organisation_Id = " + organisationId);
        }

        public List<Organisation2Email> GetOrganisation2EmailLinks(int orgId, int emailId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2Email
                    where x.Organisation_Id == orgId && x.Email_Id == emailId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Organisation2Email GetLatestOrganisation2EmailLink(int orgId, int emailId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2Email
                    where x.Organisation_Id == orgId && x.Email_Id == emailId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpOrganisation2EmailLinks(/*Organisation2Email latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2E links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Organisation2Email
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Organisation2Email Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Organisation2Email UpdateOrganisation2EmailLink(MDA.Common.Enum.Operation op, Organisation2Email link,
                                                MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeBase.LinkConfidence)
                            link.LinkConfidence = (int)pipeBase.LinkConfidence;

                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Organisation2Email UpdateOrganisation2EmailLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Organisation2Email where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateOrganisation2EmailLink(op, link, pipeBase, claim, ref success);
        }

        public Organisation2Email InsertOrganisation2EmailLink(int orgId, int emailId, MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim)
        {
            Organisation2Email o2e = _db.Organisation2Email.Create();

            o2e.CreatedBy         = pipeBase.CreatedBy;
            o2e.CreatedDate       = pipeBase.CreatedDate;
            o2e.ModifiedBy        = pipeBase.ModifiedBy;
            o2e.ModifiedDate      = pipeBase.ModifiedDate;
            o2e.Source            = pipeBase.Source;
            o2e.SourceDescription = pipeBase.SourceDescription;
            o2e.SourceReference   = pipeBase.SourceReference;
            o2e.ADARecordStatus   = pipeBase.ADARecordStatus;
            o2e.RecordStatus      = pipeBase.RecordStatus;
            o2e.LinkConfidence    = pipeBase.LinkConfidence;

            o2e.RiskClaim_Id = claim.Id;
            o2e.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            o2e.Email_Id = emailId;
            o2e.Organisation_Id = orgId;

            o2e.FiveGrading = GetFiveGrading();

            _db.Organisation2Email.Add(o2e);

            _db.SaveChanges();

            return o2e;
        }

        //public Organisation2Email InsertOrganisation2EmailLink(Organisation2Email org2mail)
        //{
        //    Organisation2Email o2e = _db.Organisation2Email.Create();

        //    o2e.CreatedBy = org2mail.CreatedBy;
        //    o2e.CreatedDate = DateTime.Now;
        //    o2e.BaseRiskClaim_Id = org2mail.BaseRiskClaim_Id;

        //    o2e.Source = org2mail.Source;
        //    o2e.SourceDescription = org2mail.SourceDescription;
        //    o2e.SourceReference = org2mail.SourceReference;
        //    o2e.IBaseId = org2mail.IBaseId;

        //    o2e.Organisation_Id = org2mail.Organisation_Id;
        //    o2e.RiskClaim_Id = org2mail.RiskClaim_Id;

        //    o2e.Email_Id = org2mail.Email_Id;
        //    o2e.EmailLinkId = org2mail.EmailLinkId;
        //    o2e.LinkConfidence = org2mail.LinkConfidence;
        //    o2e.FiveGrading = org2mail.FiveGrading;

        //    _db.Organisation2Email.Add(o2e);

        //    _db.SaveChanges();

        //    return o2e;
        //}
    }

}
