﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;


namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllDrivingLicenseAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update DrivingLicense set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public int DeduplicateDrivingLicense(CurrentContext ctx, Func<string, int> ShowProgress = null)
        {
            //trace = new List<string>();
            int duplicateCount = 0;

            var q = ctx.db.DrivingLicenses.Where(x => x.ADARecordStatus == 0 || x.ADARecordStatus >= 10);

            var query = (q.GroupBy(i => i.DriverNumber)
                        .Where(g => g.Count() > 1)
                        .Select(g => g.Key)).ToList();

            foreach (var dlicense in query)
            {
                if (dlicense == null) continue;

                duplicateCount++;

                using (var ctxx = new CurrentContext(ctx))
                {
                    var master = (from t in ctxx.db.DrivingLicenses
                                  where t.DriverNumber == dlicense && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10) && t.DrivingLicenseId != null
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();

                    if (master == null)
                    {
                        master = (from t in ctxx.db.DrivingLicenses
                                  where t.DriverNumber == dlicense && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();
                    }

                    if (master == null) continue;

                    var allDuplicates = from t in ctxx.db.DrivingLicenses
                                        where t.DriverNumber == dlicense && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                        && t.Id != master.Id
                                        select t;

                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                    {
                        try
                        {
                            foreach (var td in allDuplicates)
                            {
                                List<int> PId = new List<int>();

                                ctxx.db.Entry(td).Collection(z => z.Person2DrivingLicense).Load();

                                if (td.Person2DrivingLicense.Any())
                                {
                                    foreach (var l in td.Person2DrivingLicense)
                                    {
                                        l.DrivingLicense_Id = master.Id;
                                        PId.Add(l.Person_Id);
                                    }
                                }

                                td.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                                td.RecordStatus = Constants.IBaseDeleteValue;

                                if (ShowProgress != null)
                                    ShowProgress(string.Format("Duplicate License Removed [{0}] : New Master Id [{1}] : People ID's Moved [{2}]",
                                                td.Id, master.Id, string.Join(",", PId)));
                            }
                            ctxx.db.SaveChanges();

                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();

                            throw;
                        }
                    }
                }
            }

 
            if (ShowProgress != null)
                ShowProgress(string.Format("Duplicates Found [{0}]", duplicateCount));

            return duplicateCount;
        }

        public DrivingLicense SelectDrivingLicense(int id)
        {
            return (from x in _db.DrivingLicenses where x.Id == id select x).FirstOrDefault();
        }

        public DrivingLicense UpdateDrivingLicenseNumber(int LicenseNumberId, MDA.Pipeline.Model.PipelineDrivingLicense pipeDrivingLicense, out bool success)
        {
            success = false;

            var pp = (from x in _db.DrivingLicenses where x.Id == LicenseNumberId select x).FirstOrDefault();

            if (pp == null) return pp;

            try
            {
                success = true;

                bool modified = false;

                if (pp.DriverNumber == null)
                {
                    pp.DriverNumber = pipeDrivingLicense.DriverNumber;
                    modified = true;
                }

                if (modified)
                {
                    pp.ModifiedDate = DateTime.Now;
                    pp.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }

            }
            catch (Exception)
            {
                success = false;
            }

            return pp;
        }

        public DrivingLicense InsertDrivingLicenseNumber(MDA.Pipeline.Model.PipelineDrivingLicense pipeDrivingLicense)
        {
            DrivingLicense e = _db.DrivingLicenses.Create();

            e.ADARecordStatus   = pipeDrivingLicense.ADARecordStatus;
            e.CreatedBy         = pipeDrivingLicense.CreatedBy;
            e.CreatedDate       = pipeDrivingLicense.CreatedDate;
            e.DriverNumber      = pipeDrivingLicense.DriverNumber;
            e.DrivingLicenseId  = pipeDrivingLicense.DrivingLicenseId;
            e.IBaseId           = pipeDrivingLicense.IBaseId;
            e.KeyAttractor      = pipeDrivingLicense.KeyAttractor;
            e.ModifiedBy        = pipeDrivingLicense.ModifiedBy;
            e.ModifiedDate      = pipeDrivingLicense.ModifiedDate;
            e.RecordStatus      = pipeDrivingLicense.RecordStatus;
            e.Source            = pipeDrivingLicense.Source;
            e.SourceDescription = pipeDrivingLicense.SourceDescription;
            e.SourceReference   = pipeDrivingLicense.SourceReference;

            _db.DrivingLicenses.Add(e);

            _db.SaveChanges();

            return e;
        }

        //public DrivingLicense InsertDrivingLicenseNumber(DrivingLicense drivingLicense)
        //{
        //    DrivingLicense e = _db.DrivingLicenses.Create();

        //    e.ADARecordStatus   = drivingLicense.ADARecordStatus;
        //    e.CreatedBy         = drivingLicense.CreatedBy;
        //    e.CreatedDate       = drivingLicense.CreatedDate;
        //    e.DriverNumber      = drivingLicense.DriverNumber;
        //    e.DrivingLicenseId  = drivingLicense.DrivingLicenseId;
        //    e.IBaseId           = drivingLicense.IBaseId;
        //    e.KeyAttractor      = drivingLicense.KeyAttractor;
        //    e.ModifiedBy        = drivingLicense.ModifiedBy;
        //    e.ModifiedDate      = drivingLicense.ModifiedDate;
        //    e.RecordStatus      = drivingLicense.RecordStatus;
        //    e.Source            = drivingLicense.Source;
        //    e.SourceDescription = drivingLicense.SourceDescription;
        //    e.SourceReference   = drivingLicense.SourceReference;
            
        //    _db.DrivingLicenses.Add(e);

        //    _db.SaveChanges();

        //    return e;
        //}

        public DrivingLicense FindLicenseNumber(string DrivingLicenseNumber)
        {
            return (from x in _db.DrivingLicenses where x.DriverNumber == DrivingLicenseNumber select x).FirstOrDefault();
        }
    }

}
