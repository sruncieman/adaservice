﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;
using MDA.Common;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public void RedirectIncident2PersonLinks(int masterIncidentId, int incidentId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2Person set Incident_Id = " + masterIncidentId +
                                           " where Incident_Id = " + incidentId);
        }

        public void RedirectPerson2IncidentLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2Person set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        //public void ResetAllIncident2PersonLinkAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Incident2Person set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public List<Incident2Person> GetIncident2PersonLinks(int incidentId, int personId, int baseRiskClaimId)
        {
            return (from x in _db.Incident2Person
                    where x.Incident_Id == incidentId && x.Person_Id == personId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Incident2Person GetLatestIncident2PersonLink(int incidentId, int personId, int baseRiskClaimId)
        {
            return (from x in _db.Incident2Person
                     where x.Incident_Id == incidentId && x.Person_Id == personId 
                        && x.BaseRiskClaim_Id == baseRiskClaimId
                        && x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanseClaimInfo(int id, Func<Incident2Person, MessageNode, int> cleanseMethod)
        {
            var p = (from x in _db.Incident2Person where x.Incident_Id == id select x).ToList();
            if (p == null) return;

            MessageNode n = new MessageNode();

            foreach (var item in p)
            {
                cleanseMethod(item, n);
            }

            _db.SaveChanges();
        }

        public void CleanUpIncident2PersonLinks(RiskClaim newRiskClaim)
        {
            // Get all I2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Incident2Person
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Incident2Person Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Incident2Person UpdateIncident2PersonLink( MDA.Common.Enum.Operation op, Incident2Person link,
                                                MDA.Pipeline.Model.PipelineIncident2PersonLink pipeLink,
                                                //MDA.Pipeline.Model.PipelineClaim xmlClaim, MDA.Pipeline.Model.PipelinePerson xmlPerson,
                                                //MDA.Pipeline.Model.PipelineClaimInfo xmlClaimInfo, 
                                                RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        link.LinkConfidence  = (int)pipeLink.LinkConfidence;
                        link.PartyType_Id    = (int)pipeLink.PartyType_Id;
                        link.SubPartyType_Id = (int)pipeLink.SubPartyType_Id;

                        link.Broker       = pipeLink.Broker;
                        link.Insurer      = pipeLink.Insurer;
                        link.ClaimNumber  = pipeLink.ClaimNumber;
                        link.IncidentTime = pipeLink.IncidentTime;

                        MDA.Pipeline.Model.PipelineClaimInfo pipeClaimInfo = pipeLink.ClaimInfo;

                        if (pipeClaimInfo != null)
                        {
                            link.ClaimCode                = pipeClaimInfo.ClaimCode;
                            link.ClaimNotificationDate    = pipeClaimInfo.ClaimNotificationDate;
                            link.ClaimStatus_Id           = (int)pipeClaimInfo.ClaimStatus_Id;
                            link.IncidentCircs            = pipeClaimInfo.IncidentCircumstances;
                            link.IncidentLocation         = pipeClaimInfo.IncidentLocation;
                            link.AmbulanceAttended        = pipeClaimInfo.AmbulanceAttended;
                            link.PoliceAttended           = pipeClaimInfo.PoliceAttended;
                            link.PoliceForce              = pipeClaimInfo.PoliceForce;
                            link.PoliceReference          = pipeClaimInfo.PoliceReference;
                            link.PaymentsToDate           = pipeClaimInfo.PaymentsToDate;
                            link.Reserve                  = pipeClaimInfo.Reserve;
                            link.SourceClaimStatus        = pipeClaimInfo.SourceClaimStatus;
                            link.TotalClaimCost           = pipeClaimInfo.TotalClaimCost;
                            link.TotalClaimCostLessExcess = pipeClaimInfo.TotalClaimCostLessExcess;
                            link.BypassFraud              = pipeClaimInfo.BypassFraud;
                           
                        }

                        //if ((xmlPerson.I2Pe_LinkData.PartyType_Id == (int)Common.Enum.PartyType.Insured && xmlPerson.I2Pe_LinkData.SubPartyType_Id == (int)Common.Enum.SubPartyType.Driver) ||
                        //     xmlPerson.I2Pe_LinkData.PartyType_Id == (int)Common.Enum.PartyType.Policyholder)
                        //{
                        //    if ( xmlClaim.Policy != null && xmlClaim.Policy.Insurer != null )
                        //        link.Insurer = xmlClaim.Policy.Insurer;
                        //    else
                        //        link.Insurer = ctx.RiskClient.ClientName;
                        //}

                        //TimeSpan? newTime;

                        //if (xmlClaim.IncidentDate != null)
                        //    newTime = new TimeSpan(xmlClaim.IncidentDate.Hour, xmlClaim.IncidentDate.Minute, 0);
                        //else
                        //    newTime = null;

                        //link.IncidentTime = newTime;
                        //link.ClaimNumber = xmlClaim.ClaimNumber;
                        //link.MojStatus_Id = (int)xmlPerson.I2Pe_LinkData.MojStatus_Id;

                        //if (xmlClaimInfo != null)
                        //{                         
                        //    link.IncidentCircs         = xmlClaimInfo.IncidentCircumstances;
                        //    link.IncidentLocation      = xmlClaimInfo.IncidentLocation;
                        //    link.ClaimStatus_Id        = (int)xmlClaimInfo.ClaimStatus_Id;
                        //    link.ClaimNotificationDate = xmlClaimInfo.ClaimNotificationDate;
                        //    link.PoliceAttended        = xmlClaimInfo.PoliceAttended;
                        //    link.PoliceForce           = xmlClaimInfo.PoliceForce;
                        //    link.PoliceReference       = xmlClaimInfo.PoliceReference;
                        //    link.AmbulanceAttended     = xmlClaimInfo.AmbulanceAttended;
                        //    link.Reserve               = xmlClaimInfo.Reserve;
                        //    link.PaymentsToDate        = xmlClaimInfo.PaymentsToDate;
                        //}

                    }
                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;

        }

        public Incident2Person UpdateIncident2PersonLink(MDA.Common.Enum.Operation op, int linkId,
                                                MDA.Pipeline.Model.PipelineIncident2PersonLink pipeLink, 
                                                //MDA.Pipeline.Model.PipelineClaim xmlClaim,
                                                //MDA.Pipeline.Model.PipelinePerson xmlPerson, MDA.Pipeline.Model.PipelineClaimInfo xmlClaimInfo,
                                                RiskClaim claim, ref bool success)
        {
            success = false;

            var a = (from x in _db.Incident2Person where x.Id == linkId select x).FirstOrDefault();

            if (a == null) return a;

            return UpdateIncident2PersonLink(op, a, pipeLink, /*xmlClaim, xmlPerson, xmlClaimInfo,*/ claim, ref success);
        }

        public Incident2Person InsertIncident2PersonLink(int incidentId, int personId, MDA.Pipeline.Model.PipelineIncident2PersonLink pipeLink,
                                                                //MDA.Pipeline.Model.PipelineClaim xmlClaim, MDA.Pipeline.Model.PipelinePerson xmlPerson,
                                                                //MDA.Pipeline.Model.PipelineClaimInfo xmlClaimInfo, 
                                                                RiskClaim claim)
        {
            MDA.Pipeline.Model.PipelineClaimInfo pipeClaimInfo = pipeLink.ClaimInfo;

            Incident2Person i2p = _db.Incident2Person.Create();

            i2p.CreatedBy         = pipeLink.CreatedBy;
            i2p.CreatedDate       = pipeLink.CreatedDate;
            i2p.ModifiedBy        = pipeLink.ModifiedBy;
            i2p.ModifiedDate      = pipeLink.ModifiedDate;
            i2p.Source            = pipeLink.Source;
            i2p.SourceDescription = pipeLink.SourceDescription;
            i2p.SourceReference   = pipeLink.SourceReference;
            i2p.ADARecordStatus   = pipeLink.ADARecordStatus;
            i2p.RecordStatus      = pipeLink.RecordStatus;
            i2p.LinkConfidence    = pipeLink.LinkConfidence;

            i2p.RiskClaim_Id     = claim.Id;
            i2p.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            i2p.Person_Id        = personId;
            i2p.Incident_Id      = incidentId;
            i2p.MojStatus_Id     = (int)pipeLink.MojStatus_Id;
            i2p.FiveGrading      = GetFiveGrading();
            i2p.PartyType_Id     = (int)pipeLink.PartyType_Id;
            i2p.SubPartyType_Id  = (int)pipeLink.SubPartyType_Id;
            i2p.AttendedHospital = pipeLink.AttendedHospital;

            i2p.Broker       = pipeLink.Broker;
            i2p.Insurer      = pipeLink.Insurer;
            i2p.ClaimNumber  = pipeLink.ClaimNumber;
            i2p.IncidentTime = pipeLink.IncidentTime;

            if (pipeClaimInfo != null)
            {
                i2p.ClaimCode                = pipeClaimInfo.ClaimCode;
                i2p.ClaimNotificationDate    = pipeClaimInfo.ClaimNotificationDate;
                i2p.ClaimStatus_Id           = (int)pipeClaimInfo.ClaimStatus_Id;
                i2p.IncidentCircs            = pipeClaimInfo.IncidentCircumstances;
                i2p.IncidentLocation         = pipeClaimInfo.IncidentLocation;
                i2p.AmbulanceAttended        = pipeClaimInfo.AmbulanceAttended;
                i2p.PoliceAttended           = pipeClaimInfo.PoliceAttended;
                i2p.PoliceForce              = pipeClaimInfo.PoliceForce;
                i2p.PoliceReference          = pipeClaimInfo.PoliceReference;
                i2p.PaymentsToDate           = pipeClaimInfo.PaymentsToDate;
                i2p.Reserve                  = pipeClaimInfo.Reserve;
                i2p.SourceClaimStatus        = pipeClaimInfo.SourceClaimStatus;
                i2p.TotalClaimCost           = pipeClaimInfo.TotalClaimCost;
                i2p.TotalClaimCostLessExcess = pipeClaimInfo.TotalClaimCostLessExcess;
                i2p.BypassFraud              = pipeClaimInfo.BypassFraud;
            }

            //if ((pipeLink.PartyType_Id == (int)Common.Enum.PartyType.Insured && pipeLink.SubPartyType_Id == (int)Common.Enum.SubPartyType.Driver) ||
            //     pipeLink.PartyType_Id == (int)Common.Enum.PartyType.Policyholder)
            //{
            //    if (xmlClaim.Policy != null && xmlClaim.Policy.Insurer != null)
            //        i2p.Insurer = xmlClaim.Policy.Insurer;
            //    else
            //        i2p.Insurer = ctx.RiskClient.ClientName;

            //    i2p.Broker = xmlClaim.Policy.Broker;
            //    i2p.ClaimNumber = xmlClaim.ClaimNumber;

            //    TimeSpan? newTime;

            //    if (xmlClaim.IncidentDate != null)
            //        newTime = new TimeSpan(xmlClaim.IncidentDate.Hour, xmlClaim.IncidentDate.Minute, 0);
            //    else
            //        newTime = null;

            //    i2p.IncidentTime = newTime;

            //    if (xmlClaimInfo != null)
            //    {
            //        i2p.ClaimCode             = xmlClaimInfo.ClaimCode;
            //        i2p.ClaimNotificationDate = xmlClaimInfo.ClaimNotificationDate;
            //        i2p.ClaimStatus_Id        = (int)xmlClaimInfo.ClaimStatus_Id;
            //        i2p.IncidentCircs         = xmlClaimInfo.IncidentCircumstances;
            //        i2p.IncidentLocation      = xmlClaimInfo.IncidentLocation;
            //        i2p.AmbulanceAttended     = xmlClaimInfo.AmbulanceAttended;
            //        i2p.PoliceAttended        = xmlClaimInfo.PoliceAttended;
            //        i2p.PoliceForce           = xmlClaimInfo.PoliceForce;
            //        i2p.PoliceReference       = xmlClaimInfo.PoliceReference;
            //        i2p.PaymentsToDate        = xmlClaimInfo.PaymentsToDate;
            //        i2p.Reserve               = xmlClaimInfo.Reserve;
            //    }
            //}

            _db.Incident2Person.Add(i2p);

            _db.SaveChanges();

            return i2p;
        }

        //public Incident2Person InsertIncident2PersonLink(Incident2Person incident2person)
        //{
        //    Incident2Person i2p = _db.Incident2Person.Create();

        //    i2p.CreatedBy = incident2person.CreatedBy;
        //    i2p.CreatedDate = DateTime.Now;
        //    i2p.BaseRiskClaim_Id = incident2person.BaseRiskClaim_Id;

        //    i2p.Source = incident2person.Source;
        //    i2p.SourceDescription = incident2person.SourceDescription;
        //    i2p.SourceReference = incident2person.SourceReference;
        //    i2p.IBaseId = incident2person.IBaseId;

        //    i2p.AccidentManagement = incident2person.AccidentManagement;
        //    i2p.AttendedHospital = incident2person.AttendedHospital;
        //    i2p.AmbulanceAttended = incident2person.AmbulanceAttended;
        //    i2p.Broker = incident2person.Broker;
        //    i2p.ClaimCode = incident2person.ClaimCode;
        //    i2p.ClaimNotificationDate = incident2person.ClaimNotificationDate;
        //    i2p.ClaimNumber = incident2person.ClaimNumber;
        //    i2p.ClaimStatus_Id = incident2person.ClaimStatus_Id;
        //    i2p.ClaimType = incident2person.ClaimType;
        //    //i2p.DateOfResolution = incident2person.DateOfResolution;
        //    i2p.Engineer = incident2person.Engineer;
        //    i2p.Hire = incident2person.Hire;
        //    i2p.Incident_Id = incident2person.Incident_Id;
        //    i2p.IncidentCircs = incident2person.IncidentCircs;
        //    i2p.IncidentLinkId = incident2person.IncidentLinkId;
        //    i2p.IncidentLocation = incident2person.IncidentLocation;
        //    i2p.IncidentTime = incident2person.IncidentTime;
        //    i2p.Insurer = incident2person.Insurer;
        //    //i2p.MannerOfResolution = incident2person.MannerOfResolution;
        //    i2p.MedicalExaminer = incident2person.MedicalExaminer;
        //    i2p.MojStatus_Id = incident2person.MojStatus_Id;
        //    i2p.Person_Id = incident2person.Person_Id;
        //    i2p.PartyType_Id = incident2person.PartyType_Id;
        //    i2p.PoliceAttended = incident2person.PoliceAttended;
        //    i2p.PoliceForce = incident2person.PoliceForce;
        //    i2p.PoliceReference = incident2person.PoliceReference;
        //    i2p.Recovery = incident2person.Recovery;
        //    i2p.ReferralSource = incident2person.ReferralSource;
        //    i2p.Repairer = incident2person.Repairer;
        //    i2p.Solicitors = incident2person.Solicitors;
        //    i2p.Storage = incident2person.Storage;
        //    i2p.StorageAddress = incident2person.StorageAddress;
        //    i2p.SubPartyType_Id = incident2person.SubPartyType_Id;
        //    i2p.LinkConfidence = incident2person.LinkConfidence;
        //    i2p.FiveGrading = incident2person.FiveGrading;

        //    i2p.RiskClaim_Id = incident2person.RiskClaim_Id;

        //    _db.Incident2Person.Add(i2p);

        //    _db.SaveChanges();

        //    return i2p;
        //}
    }

}
