﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Helpers;
using MDA.Common;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPaymentCardAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update PaymentCard set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public List<int> GetAllSyncPaymentCardIds()
        {
            return (from x in _db.PaymentCards where x.ADARecordStatus >= 10 select x.Id).ToList();
        }

        public void ResetSyncPaymentCardAdaStatus(int id)
        {
            var r = (from x in _db.PaymentCards where x.Id == id select x).FirstOrDefault();

            if (r != null && r.ADARecordStatus >= 10)
            {
                r.ADARecordStatus -= 10;

                _db.SaveChanges();
            }
        }
        public void CleansePaymentCard(int id, Func<PaymentCard, MessageNode, int> cleanseMethod)
        {
            var r = (from x in _db.PaymentCards where x.Id == id select x).FirstOrDefault();

            if (r == null) return;

            MessageNode n = new MessageNode();

            cleanseMethod(r, n);

            _db.SaveChanges();
        }

        public void DeduplicatePaymentCardList(int masterPaymentCardId, List<int> matchList)
        {
            foreach (var i in matchList)
            {
                var ir = (from x in _db.PaymentCards where x.Id == i select x).FirstOrDefault();

                if (ir != null)
                {
                    RedirectPaymentCard2OrganisationLinks(masterPaymentCardId, i);
                    RedirectPaymentCard2PersonLinks(masterPaymentCardId, i);
                    RedirectPaymentCard2PolicyLinks(masterPaymentCardId, i);

                    ir.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                    ir.RecordStatus = Constants.IBaseDeleteValue;

                    _db.SaveChanges();
                }
            }
        }

        public PaymentCard SelectPaymentCard(int id)
        {
            return (from x in _db.PaymentCards where x.Id == id select x).FirstOrDefault();
        }

        public List<int> VerifyPaymentCardListExists(List<int> listId)
        {
            List<int> outList = new List<int>();

            foreach (int id in listId)
            {
                if (_db.PaymentCards.Any(x => x.Id == id))
                    outList.Add(id);
            }
            return outList;
        }

        public PaymentCard UpdatePaymentCard(int paymentCardId, MDA.Pipeline.Model.PipelinePaymentCard paymentCard, out bool success)
        {
            success = false;

            var pc = (from x in _db.PaymentCards where x.Id == paymentCardId select x).FirstOrDefault();

            if (pc == null) return pc;

            try
            {
                success = true;

                bool modified = false;

                if (pc.PaymentCardNumber == null)
                {
                    pc.PaymentCardNumber = paymentCard.PaymentCardNumber;
                    modified = true;
                }

                //if (pc.HashedCardNumber == null)
                //{
                //    pc.HashedCardNumber = paymentCard.HashedCardNumber;
                //    modified = true;
                //}

                if (pc.ExpiryDate == null)
                {
                    pc.ExpiryDate = paymentCard.ExpiryDate;
                    modified = true;
                }

                if (pc.BankName == null)
                {
                    pc.BankName = paymentCard.BankName;
                    modified = true;
                }

                if (pc.SortCode == null)
                {
                    pc.SortCode = paymentCard.SortCode;
                    modified = true;
                }

                if (modified)
                {
                    pc.ModifiedDate = DateTime.Now;
                    pc.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
            }
            catch (Exception)
            {
                success = false;
            }

            return pc;
        }

        public PaymentCard InsertPaymentCard(MDA.Pipeline.Model.PipelinePaymentCard paymentCard)
        {
            PaymentCard e = _db.PaymentCards.Create();

            e.ADARecordStatus    = paymentCard.ADARecordStatus;
            e.BankName           = paymentCard.BankName;
            e.CreatedBy          = paymentCard.CreatedBy;
            e.CreatedDate        = paymentCard.CreatedDate;
            e.ExpiryDate         = paymentCard.ExpiryDate;
            //e.HashedCardNumber = paymentCard.HashedCardNumber;
            e.IBaseId            = paymentCard.IBaseId;
            e.KeyAttractor       = paymentCard.KeyAttractor;
            e.ModifiedBy         = paymentCard.ModifiedBy;
            e.ModifiedDate       = paymentCard.ModifiedDate;
            e.PaymentCardId      = paymentCard.PaymentCardId;
            e.PaymentCardNumber  = paymentCard.PaymentCardNumber;
            e.PaymentCardType_Id = paymentCard.PaymentCardType_Id;
            e.RecordStatus       = paymentCard.RecordStatus;
            e.SortCode           = paymentCard.SortCode;
            e.Source             = paymentCard.Source;
            e.SourceDescription  = paymentCard.SourceDescription;
            e.SourceReference    = paymentCard.SourceReference;

            _db.PaymentCards.Add(e);

            _db.SaveChanges();

            return e;
        }

        //public PaymentCard InsertPaymentCard(string cardNumber, string bankName, string sortCode, string expiryDate, int cardType)
        //{
        //    PaymentCard e = _db.PaymentCards.Create();

        //    e.ADARecordStatus = e.ADARecordStatus;
        //    e.BankName = e.BankName;
        //    e.CreatedBy = e.CreatedBy;
        //    e.CreatedDate = e.CreatedDate;
        //    e.ExpiryDate = e.ExpiryDate;
        //    e.HashedCardNumber = e.HashedCardNumber;
        //    e.IBaseId = e.IBaseId;
        //    e.KeyAttractor = e.KeyAttractor;
        //    e.ModifiedBy = e.ModifiedBy;
        //    e.ModifiedDate = e.ModifiedDate;
        //    e.PaymentCardId = e.PaymentCardId;
        //    e.PaymentCardNumber = e.PaymentCardNumber;
        //    e.PaymentCardType_Id = e.PaymentCardType_Id;
        //    e.RecordStatus = e.RecordStatus;
        //    e.SortCode = e.SortCode;
        //    e.Source = e.Source;
        //    e.SourceDescription = e.SourceDescription;
        //    e.SourceReference = e.SourceReference;

        //    _db.PaymentCards.Add(e);

        //    _db.SaveChanges();

        //    return e;
        //}

        //public PaymentCard InsertPaymentCard(PaymentCard card)
        //{
        //    PaymentCard c = _db.PaymentCards.Create();

        //    c.CreatedBy = card.CreatedBy;
        //    c.CreatedDate = card.CreatedDate;
        //    c.BankName = card.BankName;
        //    c.IBaseId = card.IBaseId;
        //    c.PaymentCardId = card.PaymentCardId;
        //    c.PaymentCardNumber = card.PaymentCardNumber;
        //    c.HashedCardNumber = card.HashedCardNumber;
        //    c.ExpiryDate = card.ExpiryDate;
        //    c.PaymentCardType_Id = card.PaymentCardType_Id;
        //    c.SortCode = card.SortCode;
        //    c.Source = card.Source;
        //    c.SourceDescription = card.SourceDescription;
        //    c.SourceReference = card.SourceReference;

        //    _db.PaymentCards.Add(c);

        //    _db.SaveChanges();

        //    return c;
        //}

        public PaymentCard FindPaymentCard(string cardNumber, string sortCode)
        {
            return (from x in _db.PaymentCards where x.PaymentCardNumber == cardNumber && x.SortCode == sortCode select x).FirstOrDefault();
        }

      
    }

}
