﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public void RedirectPaymentCard2PolicyLinks(int masterPaymentCardId, int paymentCardId)
        {
            _db.Database.ExecuteSqlCommand("update Policy2PaymentCard set PaymentCard_Id = " + masterPaymentCardId +
                                           " where PaymentCard_Id = " + paymentCardId);
        }

        public void RedirectPolicy2PaymentCardLinks(int masterPolicyId, int policyId)
        {
            _db.Database.ExecuteSqlCommand("update Policy2PaymentCard set Policy_Id = " + masterPolicyId +
                                           " where Policy_Id = " + policyId);
        }

        //public void ResetAllPolicy2PaymentCardAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Policy2PaymentCard set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}
        public List<Policy2PaymentCard> GetPolicy2PaymentCardLinks(int policyId, int cardId, int baseRiskClaimId)
        {
            return (from x in _db.Policy2PaymentCard
                    where x.Policy_Id == policyId && x.PaymentCard_Id == cardId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Policy2PaymentCard GetLatestPolicy2PaymentCardLink(int policyId, int cardId, int baseRiskClaimId)
        {
            return (from x in _db.Policy2PaymentCard
                    where x.Policy_Id == policyId && x.PaymentCard_Id == cardId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPolicy2PaymentCardLinks(/*Policy2PaymentCard latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Policy2PaymentCard
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Policy2Bank Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Policy2PaymentCard UpdatePolicy2PaymentCardLink(MDA.Common.Enum.Operation op, Policy2PaymentCard link,
                                        //MDA.Pipeline.Model.PipelinePaymentCard paymentCard, 
                                        MDA.Pipeline.Model.PipelinePolicy2PaymentCardLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE
                    if (link.PolicyPaymentType_Id != 0 && link.PolicyPaymentType_Id != (int)pipeLink.PolicyPaymentType_Id) // we have value and it's different
                    {
                        success = false;
                    }

                    if (link.DatePaymentDetailsTaken != null && link.DatePaymentDetailsTaken != pipeLink.DatePaymentDetailsTaken) // we have value and it's different
                    {
                        success = false;
                    }

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.PolicyPaymentType_Id != (int)pipeLink.PolicyPaymentType_Id)
                            link.PolicyPaymentType_Id = (int)pipeLink.PolicyPaymentType_Id;

                        if (link.DatePaymentDetailsTaken != MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(pipeLink.DatePaymentDetailsTaken))
                            link.DatePaymentDetailsTaken = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(pipeLink.DatePaymentDetailsTaken);
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;
        }


        public Policy2PaymentCard UpdatePolicy2PaymentCardLink(MDA.Common.Enum.Operation op, int linkId,
                                        //  MDA.Pipeline.Model.PipelinePaymentCard paymentCard, 
                                        MDA.Pipeline.Model.PipelinePolicy2PaymentCardLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Policy2PaymentCard where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePolicy2PaymentCardLink(op, link, /*paymentCard,*/ pipeLink, claim, ref success);
        }

        public Policy2PaymentCard InsertPolicy2PaymentCardLink(int policyId, int cardId, MDA.Pipeline.Model.PipelinePolicy2PaymentCardLink pipeLink,
                                            /*DateTime? datePaymentTaken,*/ RiskClaim claim)
        {
            Policy2PaymentCard p2a = _db.Policy2PaymentCard.Create();

            p2a.CreatedBy         = pipeLink.CreatedBy;
            p2a.CreatedDate       = pipeLink.CreatedDate;
            p2a.ModifiedBy        = pipeLink.ModifiedBy;
            p2a.ModifiedDate      = pipeLink.ModifiedDate;
            p2a.Source            = pipeLink.Source;
            p2a.SourceDescription = pipeLink.SourceDescription;
            p2a.SourceReference   = pipeLink.SourceReference;
            p2a.ADARecordStatus   = pipeLink.ADARecordStatus;
            p2a.RecordStatus      = pipeLink.RecordStatus;
            p2a.LinkConfidence    = pipeLink.LinkConfidence;

            p2a.RiskClaim_Id = claim.Id;
            p2a.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2a.Policy_Id = policyId;
            p2a.PaymentCard_Id = cardId;

            //if (datePaymentTaken.HasValue)
            //    p2a.DatePaymentDetailsTaken = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(datePaymentTaken);
            //else
            //    p2a.DatePaymentDetailsTaken = null;

            p2a.DatePaymentDetailsTaken = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(pipeLink.DatePaymentDetailsTaken);
            p2a.PolicyPaymentType_Id = pipeLink.PolicyPaymentType_Id;

            p2a.FiveGrading = GetFiveGrading();

            _db.Policy2PaymentCard.Add(p2a);

            _db.SaveChanges();

            return p2a;
        }

        //public Policy2PaymentCard InsertPolicy2PaymentCardLink(Policy2PaymentCard policy2payment)
        //{
        //    Policy2PaymentCard p2p = _db.Policy2PaymentCard.Create();

        //    p2p.CreatedBy = policy2payment.CreatedBy;
        //    p2p.CreatedDate = DateTime.Now;
        //    p2p.BaseRiskClaim_Id = policy2payment.BaseRiskClaim_Id;

        //    p2p.Source = policy2payment.Source;
        //    p2p.SourceDescription = policy2payment.SourceDescription;
        //    p2p.SourceReference = policy2payment.SourceReference;

        //    p2p.PaymentCard_Id = policy2payment.PaymentCard_Id;
        //    p2p.DatePaymentDetailsTaken = policy2payment.DatePaymentDetailsTaken;
        //    p2p.PolicyPaymentType_Id = policy2payment.PolicyPaymentType_Id;
        //    p2p.Policy_Id = policy2payment.Policy_Id;
        //    p2p.PolicyPaymentLinkId = policy2payment.PolicyPaymentLinkId;

        //    p2p.IBaseId = policy2payment.IBaseId;
        //    p2p.RiskClaim_Id = policy2payment.RiskClaim_Id;
        //    p2p.LinkConfidence = policy2payment.LinkConfidence;
        //    p2p.FiveGrading = policy2payment.FiveGrading;

        //    _db.Policy2PaymentCard.Add(p2p);

        //    _db.SaveChanges();

        //    return p2p;
        //}

    }

}
