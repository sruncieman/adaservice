﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Helpers;
using LinqKit;
using RiskEngine.Model;
using MDA.Common.Debug;
using RiskEngine.Interfaces;
using MDA.Common;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllVehicleAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Vehicle set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public List<int> GetAllSyncVehicleIds()
        {
            return (from x in _db.Vehicles where x.ADARecordStatus >= 10 select x.Id).ToList();
        }

        public void ResetSyncVehicleAdaStatus(int id)
        {
            var r = (from x in _db.Vehicles where x.Id == id select x).FirstOrDefault();

            if (r != null && r.ADARecordStatus >= 10)
            {
                r.ADARecordStatus -= 10;

                _db.SaveChanges();
            }
        }

        private void _FindAllVehicleAliases(int id, bool confirmed, bool unconfirmed, bool tentative, List<int> fullList)
        {
            var LinkList = from x in _db.Vehicle2Vehicle
                           where (x.Vehicle1_Id == id || x.Vehicle2_Id == id) 
                               && (x.RecordStatus == 0)
                           select new
                           {
                               LinkConfidence = x.LinkConfidence,
                               V1_ID = x.Vehicle1_Id,
                               V2_ID = x.Vehicle2_Id
                           };

            if ((confirmed && unconfirmed && tentative) || (!confirmed && !unconfirmed && !tentative))
            {
                // do nothing and you get everything. All False treated as All True
            }
            else
            {
                if (confirmed)
                {
                    if (unconfirmed)
                    {
                        if (tentative)
                            LinkList = LinkList.Where(x => (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Confirmed) ||
                                                           (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Unconfirmed) ||
                                                           (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Tentative));
                        else
                            LinkList = LinkList.Where(x => (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Confirmed) ||
                                                           (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Unconfirmed));
                    }
                    else
                    {
                        if (tentative)
                            LinkList = LinkList.Where(x => (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Confirmed) ||
                                                           (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Tentative));
                        else
                            LinkList = LinkList.Where(x => (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Confirmed));
                    }
                }
                else
                {
                    if (unconfirmed)
                    {
                        if (tentative)
                            LinkList = LinkList.Where(x => (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Unconfirmed) ||
                                                           (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Tentative));
                        else
                            LinkList = LinkList.Where(x => (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Unconfirmed));
                    }
                    else
                    {
                        if (tentative)
                            LinkList = LinkList.Where(x => (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Tentative));

                    }
                }
            }

            foreach (var linkRec in LinkList.ToList())
            {
                int nextId = (linkRec.V1_ID == id) ? linkRec.V2_ID : linkRec.V1_ID;

                if (!fullList.Contains(nextId))
                {
                    fullList.Add(nextId);

                    _FindAllVehicleAliases(nextId, confirmed, unconfirmed, tentative, fullList);
                }
            }
        }

        public List<int> FindAllVehicleAliases(int id, bool includeConfirmed, bool includeUnconfirmed, bool includeTentative)
        {
            List<int> fullList = new List<int>();

            fullList.Add(id);

            _FindAllVehicleAliases(id, includeConfirmed, includeUnconfirmed, includeTentative, fullList);

            return fullList;
        }

        public List<int> FindAllVehicleAliases(int id, EntityAliases vehicleAliases, bool _trace)
        {

            List<int> retList1 = new List<int>();

            bool includeConfirmed;
            bool includeUnconfirmed;
            bool includeTentative;
            bool includeThis;

            ProcessMatchType(vehicleAliases.MatchType, out includeThis, out includeConfirmed, out includeUnconfirmed, out includeTentative);

            if (includeThis)
                retList1.Add(id);

            if (includeConfirmed)
                retList1.AddRange(vehicleAliases.ConfirmedAliases);

            if (includeUnconfirmed)
                retList1.AddRange(vehicleAliases.UnconfirmedAliases);

            if (includeTentative)
                retList1.AddRange(vehicleAliases.TentativeAliases);

            retList1 = retList1.Distinct().ToList();

            //if (_trace)
            //{
            //    ADATrace.WriteLine("FindVehicleAliasesCached: VehicleId=" + id.ToString() + " : " + vehicleAliases.MatchType + "=[" + string.Join(",", retList1) + "]");
            //}

            return retList1;
        }

        public ListOfInts FindAllVehicleAliasesFromDb(int id, bool includeConfirmed, bool includeUnconfirmed, bool includeTentative, bool _trace)
        {
            ListOfInts retList1 = new ListOfInts();

            var treeId = _db.uspGetVehicleTree(id, includeConfirmed, includeUnconfirmed, includeTentative, false);

            foreach (var vid in treeId)
            {
                if (vid != id)
                    retList1.Add((int)vid);
            }

            if (_trace)
                ADATrace.WriteLine("FindVehicleAliasesFromDb: VehicleId=" + id.ToString() + " [" + string.Join(",", retList1) + "]");

            return retList1;
        }

        public List<int> VerifyVehicleListExists(List<int> listId)
        {
            List<int> outList = new List<int>();

            foreach (int id in listId)
            {
                if (_db.Vehicles.Any(x => x.Id == id))
                    outList.Add(id);
            }
            return outList;
        }

        public Vehicle SelectVehicle(int id)
        {
            return (from x in _db.Vehicles where x.Id == id select x).FirstOrDefault();
        }

        public void CleanseVehicle(int id, Func<Vehicle, MessageNode, int> cleanseMethod)
        {
            var v = (from x in _db.Vehicles where x.Id == id select x).FirstOrDefault();

            if (v == null) return;

            MessageNode n = new MessageNode();

            cleanseMethod(v, n);

            _db.SaveChanges();
        }

        public Vehicle UpdateVehicle(int vehicleId, MDA.Pipeline.Model.PipelineVehicle vehicle, out bool success, out string error)
        {
            success = false;
            error = "";

            var v = (from x in _db.Vehicles where x.Id == vehicleId select x).FirstOrDefault();

            if (v == null) return v;

            if (v.VehicleId != null)
            {
                success = false;
                error = "Unable to update IBASE record";
                return v;
            }

            try
            {
                StringBuilder err = new StringBuilder();

                success = true;

                bool modified = false;

                if (vehicle.VehicleRegistration != null && !string.IsNullOrEmpty(v.VehicleRegistration) && string.Compare(v.VehicleRegistration, vehicle.VehicleRegistration, true) != 0) // we have value and it's different
                {
                    success = false;
                    err.Append("Vehicle Reg - ");
                }

                if (vehicle.VehicleMake != null && !string.IsNullOrEmpty(v.VehicleMake) && string.Compare(v.VehicleMake, vehicle.VehicleMake, true) != 0) // we have value and it's different
                {
                    success = false;
                    err.Append("Vehicle Make - ");
                }

                if (vehicle.VehicleModel != null && !string.IsNullOrEmpty(v.Model) && string.Compare(v.Model, vehicle.VehicleModel, true) != 0) // we have value and it's different
                {
                    success = false;
                    err.Append("Vehicle Model - ");
                }

                if (vehicle.VIN != null && !string.IsNullOrEmpty(v.VIN) && string.Compare(v.VIN, vehicle.VIN, true) != 0) // we have value and it's different
                {
                    success = false;
                    err.Append("VehicleVIN - ");
                }

                if (!success)
                {
                    err.Append("overwrites existing value");
                    error = err.ToString();
                    return v;
                }

                if (string.IsNullOrEmpty(v.VehicleRegistration))
                {
                    v.VehicleRegistration = vehicle.VehicleRegistration;
                    modified = true;
                }

                if (string.IsNullOrEmpty(v.Model))
                {
                    v.Model = vehicle.VehicleModel;
                    modified = true;
                }

                if (string.IsNullOrEmpty(v.VehicleMake))
                {
                    v.VehicleMake = vehicle.VehicleMake;
                    modified = true;
                }

                if (string.IsNullOrEmpty(v.VIN))
                {
                    v.VIN = vehicle.VIN;
                    modified = true;
                }

                if (v.VehicleColour_Id != (int)vehicle.VehicleColour_Id && (int)vehicle.VehicleColour_Id > 0)
                {
                    v.VehicleColour_Id = (int)vehicle.VehicleColour_Id;
                    modified = true;
                }

                if (v.VehicleFuel_Id != (int)vehicle.VehicleFuel_Id && (int)vehicle.VehicleFuel_Id > 0)
                {
                    v.VehicleFuel_Id = (int)vehicle.VehicleFuel_Id;
                    modified = true;
                }

                if (v.VehicleType_Id != (int)vehicle.VehicleType_Id && (int)vehicle.VehicleType_Id > 0)
                {
                    v.VehicleType_Id = (int)vehicle.VehicleType_Id;
                    modified = true;
                }

                if (v.EngineCapacity == null || string.Compare(v.EngineCapacity, vehicle.EngineCapacity, true) != 0)
                {
                    v.EngineCapacity = vehicle.EngineCapacity;
                    modified = true;
                }

                if (modified)
                {
                    v.ModifiedDate = DateTime.Now;
                    v.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return v;

        }

        public Vehicle InsertVehicle(MDA.Pipeline.Model.PipelineVehicle vehicle)
        {
            Vehicle v = _db.Vehicles.Create();

            v.ADARecordStatus          = vehicle.ADARecordStatus;
            v.CreatedBy                = vehicle.CreatedBy;
            v.CreatedDate              = vehicle.CreatedDate;
            v.EngineCapacity           = vehicle.EngineCapacity;
            v.IBaseId                  = vehicle.IBaseId;
            v.KeyAttractor             = vehicle.KeyAttractor;
            v.Model                    = vehicle.VehicleModel;
            v.ModifiedBy               = vehicle.ModifiedBy;
            v.ModifiedDate             = vehicle.ModifiedDate;
            v.Notes                    = vehicle.Notes;
            v.RecordStatus             = vehicle.RecordStatus;
            v.Source                   = vehicle.Source;
            v.SourceDescription        = vehicle.SourceDescription;
            v.SourceReference          = vehicle.SourceReference;
            //v.VehicleCategoryOfLoss_Id = vehicle.VehicleCategoryOfLoss_Id;
            v.VehicleColour_Id         = vehicle.VehicleColour_Id;
            v.VehicleFuel_Id           = vehicle.VehicleFuel_Id;
            v.VehicleId                = vehicle.VehicleId;
            v.VehicleMake              = vehicle.VehicleMake;
            v.VehicleRegistration      = vehicle.VehicleRegistration;
            v.VehicleTransmission_Id   = vehicle.VehicleTransmission_Id;
            v.VehicleType_Id           = vehicle.VehicleType_Id;
            v.VIN                      = vehicle.VIN;

            _db.Vehicles.Add(v);

            _db.SaveChanges();

            return v;
        }

        //public Vehicle InsertVehicle(Vehicle vehicle)
        //{
        //    Vehicle v = _db.Vehicles.Create();

        //    v.CreatedBy = vehicle.CreatedBy;
        //    v.CreatedDate = DateTime.Now;
        //    v.VehicleRegistration = vehicle.VehicleRegistration;
        //    v.EngineCapacity = vehicle.EngineCapacity;
        //    v.IBaseId = vehicle.IBaseId;
        //    v.Model = vehicle.Model;
        //    v.Notes = vehicle.Notes;
        //    v.Source = vehicle.Source;
        //    v.SourceDescription = vehicle.SourceDescription;
        //    v.SourceReference = vehicle.SourceReference;
        //    v.VehicleColour_Id = vehicle.VehicleColour_Id;
        //    v.VehicleFuel_Id = vehicle.VehicleFuel_Id;
        //    v.VehicleId = vehicle.VehicleId;
        //    v.VehicleMake = vehicle.VehicleMake;
        //    v.VehicleTransmission_Id = vehicle.VehicleTransmission_Id;
        //    v.VehicleType_Id = vehicle.VehicleType_Id;
        //    v.VIN = vehicle.VIN;

        //    _db.Vehicles.Add(v);

        //    _db.SaveChanges();

        //    return v;
        //}

        //public int Vehicle_OccupancyCount(int excludeIncidentId, DateTime incidentDate, int baseRiskClaimId, int vehicleId, EntityAliases vehicleAliases, string incidentSource, string incidentType, string periodSkip, string periodCount, string searchVehicleLinkType, string indexVehicleLinkType, bool _trace)
        //{
        //    return 0;
        //}
#if false
        public int Vehicle_OccupancyCount(int riskClaimId, int vehicleId)
        {
            List<int> claimantTypes = MDA.Common.Helpers.LinqHelper.GetVehicleOccupancySubPartyTypes();

            var a = (from x in _db.Vehicle2Person
                     join y in _db.Incident2Person on x.Person_Id equals y.Person_Id
                     where x.RiskClaim_Id == riskClaimId
                     && x.Vehicle_Id == vehicleId
                     && claimantTypes.Contains(y.SubPartyType_Id)
                     && x.ADARecordStatus == (byte)ADARecordStatus.Current
                     select x.Id).Distinct();

            return a.Count();

        }


        public List<Vehicle_NumberOfIncidents_Result> Vehicle_NumberOfIncidents(int excludeIncidentId, DateTime incidentDate, int baseRiskClaimId, int vehicleId, EntityAliases vehicleAliases, string incidentSource, string incidentType, string periodSkip, string periodCount, string vehicleLinkType, /*string searchDate,*/ bool _trace)
        {

            List<int> vehicleAliasIds = FindAllVehicleAliases(vehicleId, vehicleAliases, _trace);

            DateTime enddate; // = DateTime.Now.AddMonths(-monthSkip).Date;
            DateTime startdate; // = enddate.AddMonths(-monthCount).Date;
            
            PropertyDateRange dateRange = new PropertyDateRange(incidentDate, periodSkip, periodCount);

            enddate = dateRange.EndDate; // incidentDate.AddMonths(+monthSkip).Date;
            startdate = dateRange.StartDate; // incidentDate.AddMonths(-monthCount).Date;

            int vehicleLinkType_Id;

            switch (vehicleLinkType)
            {
                case "Insured Vehicle":
                    vehicleLinkType_Id = 1;
                    break;
                case "Third Party Vehicle":
                    vehicleLinkType_Id = 2;
                    break;
                case "Insured Hire Vehicle":
                    vehicleLinkType_Id = 3;
                    break;
                case "Third Party Hire Vehicle":
                    vehicleLinkType_Id = 4;
                    break;
                case "Witness Vehicle":
                    vehicleLinkType_Id = 5;
                    break;
                case "No Vehicle Involved":
                    vehicleLinkType_Id = 6;
                    break;

                default:
                    vehicleLinkType_Id = 0;
                    break;
            }

            if (_trace)
                 ADATrace.WriteLine(string.Format("Date Range: StartDate[{0}] : EndDate[{1}] : IncidentDate[{2}]", startdate.ToString(), enddate.ToString(), incidentDate.ToString()));

            string[] incidentSources = incidentSource.Split('|');
            string[] incidentTypes = incidentType.Split('|');

            var incidents = (from i2v in _db.Incident2Vehicle
                                join i in _db.Incidents on i2v.Incident_Id equals i.Id
                                join v in _db.Vehicles on i2v.Vehicle_Id equals v.Id
                                where vehicleAliasIds.Contains(i2v.Vehicle_Id)
                                && i2v.ADARecordStatus == (byte)ADARecordStatus.Current
                                && i.ADARecordStatus == (byte)ADARecordStatus.Current
                                && v.ADARecordStatus == (byte)ADARecordStatus.Current
                                && (i2v.BaseRiskClaim_Id != baseRiskClaimId)
                                && i.IncidentDate >= startdate && i.IncidentDate <= enddate
                                && i2v.Incident_Id != excludeIncidentId
                                && i2v.Incident2VehicleLinkType_Id == vehicleLinkType_Id
                                select new Vehicle_NumberOfIncidents_Query()
                                {
                                    Vehicle = v,
                                    Incident = i,
                                    //Incident2Person = i2p,
                                    Incident2Vehicle = i2v,
                                    RiskClaim = i2v.RiskClaim,
                                    //Person = v2p.Person,
                                    Involvement = i2v.Incident2VehicleLinkType.Text,
                                });

            var predicate1 = PredicateBuilder.False<Vehicle_NumberOfIncidents_Query>();

            bool pred1used = false;

            foreach (string s in incidentSources)
            {
                if (string.Compare(s, "CUE", true) == 0)
                {
                    predicate1 = predicate1.Or(i => i.Incident.Source == "CUE");
                    pred1used = true;
                }
                else if (string.Compare(s, "ADA", true) == 0)
                {
                    predicate1 = predicate1.Or(i => i.Incident.Source == "ADA");
                    pred1used = true;
                }
                else if (string.Compare(s, "ACTIVE", true) == 0)
                {
                    predicate1 = predicate1.Or(i => i.Incident.Source != "Keoghs CMS");
                    pred1used = true;
                }
                else if (string.Compare(s, "INFO", true) == 0)
                {
                    predicate1 = predicate1.Or(i => i.Incident.Source == "Keoghs InfoOnly");
                    pred1used = true;
                }
            }

            var predicate2 = PredicateBuilder.False<Vehicle_NumberOfIncidents_Query>();

            bool pred2used = false;

            foreach (string s in incidentTypes)
            {
                if (string.Compare(s, "RTC", true) == 0)
                {
                    predicate2 = predicate2.Or(i => i.Incident.IncidentType_Id == (int)MDA.Common.Enum.ClaimType.Motor);
                    pred2used = true;
                }
            }

            if (pred1used)
                incidents = incidents.AsExpandable().Where(predicate1);

            if (pred2used)
                incidents = incidents.AsExpandable().Where(predicate2);

            var incidentList = incidents.ToList();
            
            var incidentList2 = incidentList.Distinct(new CompareVehicleNumberOfIncidents_Query()).ToList();

            List<Vehicle_NumberOfIncidents_Result> res = (from x in incidentList2
                                                            select new Vehicle_NumberOfIncidents_Result()
                                                            {
                                                                IncidentDate = x.Incident.IncidentDate,
                                                                //Insurer = x.Incident2Person.Insurer,
                                                                ClaimRef = x.RiskClaim.ClientClaimRefNumber,
                                                                //DriverDetails = x.Person.FirstName + " " + x.Person.LastName + " " + x.Person.DateOfBirth.ToString(),
                                                                Involvement = x.Involvement,
                                                                //VehicleDetails = x.Vehicle.VehicleRegistration + " " + x.Vehicle.VehicleMake + " " + x.Vehicle.Model
                                                            }).ToList();
            return res;
        }
#endif
    
    }

#if false
    /// <summary>
    /// A class to allow us to weed out duplicate INCIDENTS in this records set.  Called in a DISTINCT()
    /// </summary>
    class CompareVehicleNumberOfIncidents_Query : IEqualityComparer<Vehicle_NumberOfIncidents_Query>
    {
        public bool Equals(Vehicle_NumberOfIncidents_Query x, Vehicle_NumberOfIncidents_Query y)
        {
            return x.Incident.Id == y.Incident.Id;
        }
        public int GetHashCode(Vehicle_NumberOfIncidents_Query codeh)
        {
            return codeh.Incident.Id.GetHashCode();
        }
    }
#endif
}
