﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllEmailAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Email set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}
        public int DeduplicateEmails(CurrentContext ctx, Func<string, int> ShowProgress = null)
        {
            //trace = new List<string>();
            int duplicateCount = 0;

            var q = ctx.db.Emails.Where(x => x.ADARecordStatus == 0 || x.ADARecordStatus >= 10);

            var query = (q.GroupBy(i => i.EmailAddress)
                        .Where(g => g.Count() > 1)
                        .Select(g => g.Key)).ToList();

            foreach (var demail in query)
            {
                if (demail == null) continue;

                duplicateCount++;

                using (var ctxx = new CurrentContext(ctx))
                {
                    var master = (from t in ctxx.db.Emails
                                  where t.EmailAddress == demail && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10) && t.EmailId != null
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();

                    if (master == null)
                    {
                        master = (from t in ctxx.db.Emails
                                  where t.EmailAddress == demail && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();
                    }

                    if (master == null) continue;

                    var allDuplicates = from t in ctxx.db.Emails
                                        where t.EmailAddress == demail && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                        && t.Id != master.Id
                                        select t;

                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                    {
                        try
                        {
                            foreach (var td in allDuplicates)
                            {
                                List<int> PId = new List<int>();
                                List<int> OId = new List<int>();

                                ctxx.db.Entry(td).Collection(z => z.Organisation2Email).Load();

                                if (td.Organisation2Email.Any())
                                {
                                    foreach (var l in td.Organisation2Email)
                                    {
                                        l.Email_Id = master.Id;
                                        OId.Add(l.Organisation_Id);
                                    }
                                }

                                ctxx.db.Entry(td).Collection(z => z.Person2Email).Load();

                                if (td.Person2Email.Any())
                                {
                                    foreach (var l in td.Person2Email)
                                    {
                                        l.Email_Id = master.Id;
                                        PId.Add(l.Person_Id);
                                    }
                                }

                                td.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                                td.RecordStatus = Constants.IBaseDeleteValue;

                                if (ShowProgress != null)
                                    ShowProgress(string.Format("Duplicate Email Removed [{0}] : New Master Id [{1}] : Org ID's Moved [{2}] : People ID's Moved [{3}]",
                                                td.Id, master.Id, string.Join(",", OId), string.Join(",", PId)));
                            }
                            ctxx.db.SaveChanges();

                            transaction.Commit();

                        }
                        catch (Exception)
                        {
                            transaction.Rollback();

                            throw;
                        }
                    }
                }
            }



            if (ShowProgress != null) 
                ShowProgress(string.Format("Duplicates Found [{0}]", duplicateCount));

            return duplicateCount;

        }

        public Email SelectEmail(int id)
        {
            return (from x in _db.Emails where x.Id == id select x).FirstOrDefault();
        }

        public Email FindEmail(string email)
        {
            return (from x in _db.Emails where x.EmailAddress == email select x).FirstOrDefault();
        }

        public Email UpdateEmail(int emailId, MDA.Pipeline.Model.PipelineEmail pipeEmail, out bool success)
        {
            success = false;

            var e = (from x in _db.Emails where x.Id == emailId select x).FirstOrDefault();

            if (e == null) return e;

            try
            {
                success = true;


                bool modified = false;

                if (e.EmailAddress == null)
                {
                    e.EmailAddress = pipeEmail.EmailAddress;
                    modified = true;
                }

                if (modified)
                {
                    e.ModifiedDate = DateTime.Now;
                    e.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
            }
            catch (Exception)
            {
                success = false;
            }

            return e;
        }

        public Email InsertEmail(MDA.Pipeline.Model.PipelineEmail pipeEmail)
        {
            Email e = _db.Emails.Create();

            e.EmailId           = pipeEmail.EmailId;
            e.EmailAddress      = pipeEmail.EmailAddress;
            e.KeyAttractor      = pipeEmail.KeyAttractor;
            e.Source            = pipeEmail.Source;
            e.SourceDescription = pipeEmail.SourceDescription;
            e.SourceReference   = pipeEmail.SourceReference;
            e.CreatedBy         = pipeEmail.CreatedBy;
            e.CreatedDate       = pipeEmail.CreatedDate;
            e.ModifiedBy        = pipeEmail.ModifiedBy;
            e.ModifiedDate      = pipeEmail.ModifiedDate;
            e.IBaseId           = pipeEmail.IBaseId;
            e.RecordStatus      = pipeEmail.RecordStatus;
            e.ADARecordStatus   = pipeEmail.ADARecordStatus;

            _db.Emails.Add(e);

            _db.SaveChanges();

            return e;
        }
    }
}
