﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MDA.DAL;
using MDA.Common.Debug;
using RiskEngine.Model;
using MDA.Common;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public List<int> GetAllSyncOrganisationIds()
        {
            return (from x in _db.Organisations where x.ADARecordStatus >= 10 select x.Id).ToList();
        }

        public void ResetSyncOrganisationAdaStatus(int id)
        {
            var r = (from x in _db.Organisations where x.Id == id select x).FirstOrDefault();

            if (r != null && r.ADARecordStatus >= 10)
            {
                r.ADARecordStatus -= 10;

                _db.SaveChanges();
            }
        }

        public void CleanseOrganisation(int id, Func<Organisation, MessageNode, int> cleanseMethod)
        {
            var o = (from x in _db.Organisations where x.Id == id select x).FirstOrDefault();

            if (o == null) return;

            MessageNode n = new MessageNode();

            cleanseMethod(o, n);

            _db.SaveChanges();
        }

        public Organisation SelectOrganisation(int id)
        {
            return (from x in _db.Organisations where x.Id == id select x).FirstOrDefault();
        }

        public List<int> VerifyOrganisationListExists(List<int> listId)
        {
            List<int> outList = new List<int>();

            foreach (int id in listId)
            {
                if (_db.Organisations.Any(x => x.Id == id))
                    outList.Add(id);
            }
            return outList;
        }

        public void UpdateSanctionsOrganisation(int organisationId, string sanctionMatchStr,  MDA.Pipeline.Model.PipelineOrganisation organisation)
        {
            
            var o = (from x in _db.Organisations where x.Id == organisationId select x).FirstOrDefault();

            if (o != null)
            {
                try
                {
                    o.SanctionList = sanctionMatchStr;
                    _db.SaveChanges();
                }

                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void ResetSanctionsOrganisation(int organisationId)
        {
            var o = (from x in _db.Organisations where x.Id == organisationId select x).FirstOrDefault();

            if (o != null)
            {
                try
                {
                    o.SanctionList = null;
                    _db.SaveChanges();
                }

                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Strip out sub strings that will affect org name comparison
        /// </summary>
        /// <param name="value">Initial Org Name</param>
        /// <returns>Cleansed org name for comparison</returns>
        private string CleanOrgName(string value)
        {
            if (value == null) throw new ArgumentNullException("value");
            value = value.ToLowerInvariant();

            var lookups = new HashSet<string>
            {
                "limited",
                ".",
                "uk",
                "ltd",
                "plc",
                "llp",
                " ",
                ":",
                " ",
                "?",
                "*",
                "(",
                ")",
                " and ",
                "&"


            };

            var sb = new StringBuilder(value, value.Length);
            foreach (var k in lookups)
            {
                sb.Replace(k, string.Empty);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Compare two org names for equality
        /// </summary>
        /// <param name="orgName1">first org name</param>
        /// <param name="orgName2">second org name</param>
        /// <returns>true if match</returns>
        private bool OrgNameEqual(string orgName1, string orgName2)
        {
            if (orgName1 != null && orgName1.Equals(orgName2, StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            var cleanedOrgName1 = CleanOrgName(orgName1);
            var cleanedOrgName2 = CleanOrgName(orgName2);

            if (cleanedOrgName1 != null && cleanedOrgName1.Equals(cleanedOrgName2, StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///  Updates certain Organisation fields
        /// </summary>
        /// <param name="organisationId">Id of existing organisation to update</param>
        /// <param name="organisation">New <see cref="MDA.Pipeline.Model.PipelineOrganisation"/>organisation to load values from</param>
        /// <param name="success">true if update successful</param>
        /// <param name="error">error message</param>
        /// <returns><see cref="Organisation"/>Existing organisation</returns>
        public Organisation UpdateOrganisation(int organisationId, MDA.Pipeline.Model.PipelineOrganisation organisation, out bool success, out string error)
        {
            if (organisation == null) throw new ArgumentNullException("organisation");

            success = false;
            error = "";

            var o = (from x in _db.Organisations where x.Id == organisationId && x.ADARecordStatus == 0 select x).FirstOrDefault();

            if (o == null) return o;

            if (o.OrganisationId != null)
            {
                success = false;
                error = "Unable to update IBASE record";
                return o;
            }

            try
            {
                success = true;
                StringBuilder err = new StringBuilder();

                bool modified = false;

                if (organisation.RegisteredNumber != null && !string.IsNullOrEmpty(o.RegisteredNumber) && string.Compare(o.RegisteredNumber, organisation.RegisteredNumber, true) != 0) // we have value and it's different
                {
                    err.Append("RegisteredNumber - ");
                    success = false;
                }

                if (organisation.OrganisationName != null && !string.IsNullOrEmpty(o.OrganisationName) && !OrgNameEqual(organisation.OrganisationName, o.OrganisationName)) // we have value and it's different
                {
                    err.Append("OrganisationName - ");
                    success = false;
                }

                if (!success)
                {
                    err.Append("overwrites existing value");
                    error = err.ToString();
                    return o;
                }

                if (o.RegisteredNumber == null)
                {
                    o.RegisteredNumber = organisation.RegisteredNumber;
                    modified = true;
                }

                if (o.OrganisationName == null)
                {
                    o.OrganisationName = organisation.OrganisationName;
                    modified = true;
                }

                if (o.OrganisationType_Id != (int)organisation.OrganisationType_Id && (int)organisation.OrganisationType_Id > 0)
                {
                    o.OrganisationType_Id = (int)organisation.OrganisationType_Id;
                    modified = true;
                }

                if (o.OrganisationStatus_Id != (int)organisation.OrganisationStatus_Id && (int)organisation.OrganisationStatus_Id > 0)
                {
                    o.OrganisationStatus_Id = (int)organisation.OrganisationStatus_Id;
                    modified = true;
                }

                if (o.VATNumber == null)
                {
                    if (!string.IsNullOrEmpty(organisation.VatNumber))
                    {
                        o.VATNumber = organisation.VatNumber;
                        modified = true;
                    }
                }
                if (o.MojCRMNumber == null) 
                {
                    if (!string.IsNullOrEmpty(organisation.MojCrmNumber))
                    {
                        o.MojCRMNumber = organisation.MojCrmNumber;
                        modified = true;
                    }
                }

                if (o.MojCRMStatus_Id != (int)organisation.MojCrmStatus_Id && (int)organisation.MojCrmStatus_Id > 0)
                {
                    o.MojCRMStatus_Id = (int)organisation.MojCrmStatus_Id;
                    modified = true;
                }

                if (o.IncorporatedDate == null) 
                {
                    if (organisation.IncorporatedDate.HasValue)
                    {
                        o.IncorporatedDate = organisation.IncorporatedDate;
                        modified = true;
                    }
                }

                if (o.EffectiveDateOfStatus == null)
                {
                    if (o.EffectiveDateOfStatus.HasValue)
                    {
                        o.EffectiveDateOfStatus = organisation.EffectiveDateOfStatus;
                        modified = true;
                    }
                }

                if (modified)
                {
                    o.ModifiedDate = DateTime.Now;
                    o.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
            }
            catch (Exception)
            {
                success = false;
            }

            return o;

        }

        public Organisation InsertOrganisation(MDA.Pipeline.Model.PipelineOrganisation organisation) //, int orgStatus)
        {
            Organisation o = _db.Organisations.Create();

            o.ADARecordStatus         = organisation.ADARecordStatus;
            o.CreatedBy               = organisation.CreatedBy;
            o.CreatedDate             = organisation.CreatedDate;
            o.CreditLicense           = organisation.CreditLicense;
            o.EffectiveDateOfStatus   = organisation.EffectiveDateOfStatus;
            o.IBaseId                 = organisation.IBaseId;
            o.IncorporatedDate        = organisation.IncorporatedDate;
            o.KeyAttractor            = organisation.KeyAttractor;
            o.ModifiedBy              = organisation.ModifiedBy;
            o.ModifiedDate            = organisation.ModifiedDate;
            o.MojCRMNumber            = organisation.MojCrmNumber;
            o.MojCRMStatus_Id         = organisation.MojCrmStatus_Id;
            o.OrganisationName        = organisation.OrganisationName;
            o.OrganisationStatus_Id   = organisation.OrganisationStatus_Id;  
            o.OrganisationType_Id     = organisation.OrganisationType_Id;
            o.RecordStatus            = organisation.RecordStatus;
            o.RegisteredNumber        = organisation.RegisteredNumber;
            o.SIC                     = organisation.SIC;
            o.Source                  = organisation.Source;
            o.SourceDescription       = organisation.SourceDescription;
            o.SourceReference         = organisation.SourceReference;
            o.VATNumber               = organisation.VatNumber;
            o.VATNumberValidationDate = organisation.VATNumberValidationDate;

            _db.Organisations.Add(o);

            _db.SaveChanges();

            return o;
        }

        private void _FindAllOrganisationAliases(int id, bool confirmed, bool unconfirmed, bool tentative, List<int> fullList, List<int> retList, bool _trace)
        {
            ADATrace.IndentLevel += 1;

            var LinkList = from x in _db.Organisation2Organisation.AsNoTracking()
                           where (x.Organisation1_Id == id || x.Organisation2_Id == id)
                               && (x.ADARecordStatus == 0)
                               && (x.OrganisationLinkType_Id == 7) //(int)MDA.Common.Enum.Person2PersonLinkType.AlsoKnownAs)
                           select new
                           {
                               LinkConfidence = x.LinkConfidence,
                               O1_ID = x.Organisation1_Id,
                               O2_ID = x.Organisation2_Id
                           };

            if ((confirmed && unconfirmed && tentative) || (!confirmed && !unconfirmed && !tentative))
            {
                // do nothing and you get everything. All False treated as All True
            }
            else
            {
                if (tentative) // follow all links
                {
                    LinkList = LinkList.Where(x => (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Confirmed) ||
                                                   (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Unconfirmed) ||
                                                   (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Tentative));
                }
                else if (unconfirmed)   // follow confirmed and unconfirmed
                {
                    LinkList = LinkList.Where(x => (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Confirmed) ||
                                                    (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Unconfirmed));
                }
                else
                {
                    LinkList = LinkList.Where(x => (x.LinkConfidence == (int)MDA.DataService.LinkConfidence.Confirmed));
                }
            }

            foreach (var linkRec in LinkList.ToList())
            {
                int nextId = (linkRec.O1_ID == id) ? linkRec.O2_ID : linkRec.O1_ID;

                if (fullList.Contains(nextId)) continue;

                bool saveId = false;

                if (linkRec.LinkConfidence == (int)MDA.DataService.LinkConfidence.Tentative)
                {
                    if (tentative)
                        saveId = true;
                }
                else if (linkRec.LinkConfidence == (int)MDA.DataService.LinkConfidence.Unconfirmed)
                {
                    if (unconfirmed)
                        saveId = true;
                }
                else if (linkRec.LinkConfidence == (int)MDA.DataService.LinkConfidence.Confirmed)
                {
                    if (confirmed)
                        saveId = true;
                }

                if (saveId && !retList.Contains(nextId))
                {
                    retList.Add(nextId);

                    if (_trace)
                        ADATrace.WriteLine("Save organisation [" + nextId.ToString() + "]");
                }

                if (!fullList.Contains(nextId))
                {
                    fullList.Add(nextId);

                    if (_trace)
                        ADATrace.WriteLine("Scan for organisation [" + nextId.ToString() + "] children");

                    _FindAllOrganisationAliases(nextId, confirmed, unconfirmed, tentative, fullList, retList, _trace);
                }
                else
                {
                    if (_trace)
                        ADATrace.WriteLine("Do not scan for organisation [" + nextId.ToString() + "] children");
                }
            }
            ADATrace.IndentLevel -= 1;
        }

        public ListOfInts FindAllOrganisationAliasesFromDb(int id, bool includeConfirmed, bool includeUnconfirmed, bool includeTentative, bool _trace)
        {
            ListOfInts retList1 = new ListOfInts();

            var treeId = _db.uspGetOrganisationTree(id, includeConfirmed, includeUnconfirmed, includeTentative, false);

            foreach (var oid in treeId)
            {
                if (oid != id)
                    retList1.Add((int)oid);
            }

            if (_trace)
                ADATrace.WriteLine("FindOrganisationAliasesFromDb: OrganisationId=" + id.ToString() + " [" + string.Join(",", retList1) + "]");

            return retList1;
        }

        public List<int> FindAllOrganisationAliases(int id, EntityAliases organisationAliases, bool _trace)
        {
            List<int> retList1 = new List<int>();

            if (organisationAliases.IncludeThis)
                retList1.Add(id);

            if (organisationAliases.IncludeConfirmed)
                retList1.AddRange(organisationAliases.ConfirmedAliases);

            if (organisationAliases.IncludeUnconfirmed)
                retList1.AddRange(organisationAliases.UnconfirmedAliases);

            if (organisationAliases.IncludeTentative)
                retList1.AddRange(organisationAliases.TentativeAliases);

            retList1 = retList1.Distinct().ToList();

            //if (_trace)
            //    ADATrace.WriteLine("FindOrganisationAliasesCached: PersonId=" + id.ToString() + " : " + organisationAliases.MatchType + "=[" + string.Join(",", retList1) + "]");

            return retList1;
        }

#if false
        public List<Organisation_IsKeyAttractor_Result> Organisation_IsKeyAttractor(EntityAliases organisationAliases, int organisationId, string cacheKey, MessageCache messageCache, bool _trace)
        {

            List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o in _db.Organisations
                    where organisationsId.Contains(o.Id)
                      && o.KeyAttractor != null
                      && o.KeyAttractor.Length > 0
                    select new Organisation_IsKeyAttractor_Result()
                    {
                        OrganisationName = o.OrganisationName,
                        KeyAttractor = o.KeyAttractor
                    }).ToList();


        }

        public List<Organisation_IsTOGAttractor_Result> Organisation_IsTOGAttractor(EntityAliases organisationAliases, int organisationId, string cacheKey, MessageCache messageCache, bool _trace)
        {

            List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o in _db.Organisations
                    where organisationsId.Contains(o.Id)
                    && o.CHFKeyAttractor == true
                    && o.RecordStatus == (byte)ADARecordStatus.Current
                    select new Organisation_IsTOGAttractor_Result()
                    {
                        OrganisationName = o.OrganisationName
                    }).ToList();
        }

        public List<Organisation_AliasInformation_Result> Organisation_AliasInformation(EntityAliases organisationAliases, int organisationId, bool _trace)
        {
            List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o in _db.Organisations
                    //join o2a in db.Organisation2Address on o.Id equals o2a.Organisation_Id
                    //join o2t in db.Organisation2Telephone on o.Id equals o2t.Organisation_Id
                    where organisationsId.Contains(o.Id)
                    select new Organisation_AliasInformation_Result()
                    {
                        OrganisationId = o.Id,
                        OrganisationName = o.OrganisationName,

                    }).ToList();
        }

        public List<Organisation_LinkedAddresses> Organisation_LinkedAddresses(EntityAliases organisationAliases, int organisationId, int riskClaimId, bool _trace)
        {
            List<int> addresses = (from a in _db.Organisation2Address.AsNoTracking()
                                   where a.Organisation_Id == organisationId
                                   && a.RiskClaim_Id == riskClaimId
                                   select a.Address.Id).ToList();

            List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            foreach (var oId in organisationsId)
            {

                var addressList = (from o2a in _db.Organisation2Address.AsNoTracking()
                                   where organisationsId.Contains(o2a.Organisation_Id)
                                   && !addresses.Contains(o2a.Address.Id)
                                   && (o2a.RiskClaim_Id != riskClaimId || o2a.RiskClaim_Id == null)
                                   && o2a.LinkConfidence == (int)LinkConfidence.Confirmed
                                   && o2a.ADARecordStatus == (byte)ADARecordStatus.Current
                                   select o2a).ToList();

                List<Organisation_LinkedAddresses> res = (from x in addressList
                                                          select new Organisation_LinkedAddresses()
                                                          {
                                                              Confirmed = x.LinkConfidence == (int)LinkConfidence.Confirmed,
                                                              FullAddress = MDA.Common.Helpers.AddressHelper.FormatAddress(x.Address) //x.Address.BuildingNumber + " " + x.Address.SubBuilding + " " + x.Address.Building + " " + x.Address.Street + " " + x.Address.Locality + " " + x.Address.Town + " " + x.Address.County + " " + x.Address.PostCode
                                                          }).ToList();
                return res;
            }

            return null;

        }

        public List<Organisation_LinkedTelephones> Organisation_LinkedTelephones(EntityAliases organisationAliases, int organisationId, int riskClaimId, bool _trace)
        {
            //List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            //return (from o2t in _db.Organisation2Telephone.AsNoTracking()
            //        where organisationsId.Contains(o2t.Organisation_Id)
            //        && (o2t.RiskClaim_Id != riskClaimId || o2t.RiskClaim_Id == null)
            //        && (o2t.LinkConfidence == (int)LinkConfidence.Confirmed || o2t.LinkConfidence == (int)LinkConfidence.Unconfirmed)
            //        && o2t.ADARecordStatus == (byte)ADARecordStatus.Current
            //        select new Organisation_LinkedTelephones()
            //        {
            //            Confirmed = o2t.LinkConfidence == (int)LinkConfidence.Confirmed,
            //            Telephone = o2t.Telephone.TelephoneNumber,
            //        }).ToList();

            List<int> telephones = (from t in _db.Organisation2Telephone
                                    where t.Organisation_Id == organisationId
                                    && t.RiskClaim_Id == riskClaimId
                                    select t.Telephone_Id).ToList();

            List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o2t in _db.Organisation2Telephone
                    where organisationsId.Contains(o2t.Organisation_Id)
                    && !telephones.Contains(o2t.Telephone.Id)
                    && (o2t.RiskClaim_Id != riskClaimId || o2t.RiskClaim_Id == null)
                    && (o2t.LinkConfidence == (int)LinkConfidence.Confirmed || o2t.LinkConfidence == (int)LinkConfidence.Unconfirmed)
                    && o2t.ADARecordStatus == (byte)ADARecordStatus.Current
                    select new Organisation_LinkedTelephones()
                    {
                        Confirmed = o2t.LinkConfidence == (int)LinkConfidence.Confirmed,
                        Telephone = o2t.Telephone.TelephoneNumber,
                    }).ToList();
        }

        public List<Organisation_LinkedWebSites> Organisation_LinkedWebSites(EntityAliases organisationAliases, int organisationId, int riskClaimId, bool _trace)
        {
            //List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            //return (from o2w in _db.Organisation2WebSite.AsNoTracking()
            //        where organisationsId.Contains(o2w.Organisation_Id)
            //        && (o2w.RiskClaim_Id != riskClaimId || o2w.RiskClaim_Id == null)
            //        && (o2w.LinkConfidence == (int)LinkConfidence.Confirmed || o2w.LinkConfidence == (int)LinkConfidence.Unconfirmed)
            //        && o2w.ADARecordStatus == (byte)ADARecordStatus.Current
            //        select new Organisation_LinkedWebSites()
            //        {
            //            Confirmed = o2w.LinkConfidence == (int)LinkConfidence.Confirmed,
            //            WebSiteURL = o2w.WebSite.URL,
            //        }).ToList();

            List<int> webSites = (from w in _db.Organisation2WebSite.AsNoTracking()
                                      where w.Organisation_Id == organisationId
                                      && w.RiskClaim_Id == riskClaimId
                                      select w.WebSite.Id).ToList();

            List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o2w in _db.Organisation2WebSite.AsNoTracking()
                    where organisationsId.Contains(o2w.Organisation_Id)
                    && !webSites.Contains(o2w.WebSite.Id)
                    && (o2w.RiskClaim_Id != riskClaimId || o2w.RiskClaim_Id == null)
                    && (o2w.LinkConfidence == (int)LinkConfidence.Confirmed || o2w.LinkConfidence == (int)LinkConfidence.Unconfirmed)
                    && o2w.ADARecordStatus == (byte)ADARecordStatus.Current
                    select new Organisation_LinkedWebSites()
                    {
                        Confirmed = o2w.LinkConfidence == (int)LinkConfidence.Confirmed,
                        WebSiteURL = o2w.WebSite.URL,
                    }).ToList();
        }

        public List<Organisation_LinkedEmails> Organisation_LinkedEmails(EntityAliases organisationAliases, int organisationId, int riskClaimId, bool _trace)
        {
            //List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            //return (from o2e in _db.Organisation2Email.AsNoTracking()
            //        where organisationsId.Contains(o2e.Organisation_Id)
            //        && (o2e.RiskClaim_Id != riskClaimId || o2e.RiskClaim_Id == null)
            //        && (o2e.LinkConfidence == (int)LinkConfidence.Confirmed || o2e.LinkConfidence == (int)LinkConfidence.Unconfirmed)
            //        && o2e.ADARecordStatus == (byte)ADARecordStatus.Current
            //        select new Organisation_LinkedEmails()
            //        {
            //            Confirmed = o2e.LinkConfidence == (int)LinkConfidence.Confirmed,
            //            EmailAddress = o2e.Email.EmailAddress
            //        }).ToList();

            List<int> emails = (from e in _db.Organisation2Email.AsNoTracking()
                                where e.Organisation_Id == organisationId
                                && e.RiskClaim_Id == riskClaimId
                                select e.Email.Id).ToList();

            List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            return (from o2e in _db.Organisation2Email.AsNoTracking()
                    where organisationsId.Contains(o2e.Organisation_Id)
                    && !emails.Contains(o2e.Email.Id)
                    && (o2e.RiskClaim_Id != riskClaimId || o2e.RiskClaim_Id == null)
                    && (o2e.LinkConfidence == (int)LinkConfidence.Confirmed || o2e.LinkConfidence == (int)LinkConfidence.Unconfirmed)
                    && o2e.ADARecordStatus == (byte)ADARecordStatus.Current
                    select new Organisation_LinkedEmails()
                    {
                        Confirmed = o2e.LinkConfidence == (int)LinkConfidence.Confirmed,
                        EmailAddress = o2e.Email.EmailAddress
                    }).ToList();
        }

        public List<Organisation_TradingNames> Organisation_TradingNames(EntityAliases organisationAliases, int organisationId, int riskClaimId, bool _trace)
        {
            List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            #region Get list of links where the OrgId we want is at one end of the link
            var o2oList1 = (from o2o in _db.Organisation2Organisation.AsNoTracking()
                            where organisationsId.Contains(o2o.Organisation1_Id)
                            && (o2o.LinkConfidence == (int)LinkConfidence.Confirmed)
                            && o2o.ADARecordStatus == (byte)ADARecordStatus.Current
                            && o2o.OrganisationLinkType_Id == (int)MDA.Common.Enum.OrganisationLinkType.TradingNameOf
                            select new
                            {
                                id = o2o.Organisation1_Id,
                                name = o2o.Organisation.OrganisationName,
                                confidence = o2o.LinkConfidence
                            });
            #endregion

            #region Get list of links where the OrgId we want is at the other end of the link
            var o2oList2 = (from o2o in _db.Organisation2Organisation.AsNoTracking()
                            where organisationsId.Contains(o2o.Organisation2_Id)
                            && (o2o.LinkConfidence == (int)LinkConfidence.Unconfirmed)
                            && o2o.ADARecordStatus == (byte)ADARecordStatus.Current
                            && o2o.OrganisationLinkType_Id == (int)MDA.Common.Enum.OrganisationLinkType.TradingNameOf
                            select new
                            {
                                id = o2o.Organisation1_Id,
                                name = o2o.Organisation.OrganisationName,
                                confidence = o2o.LinkConfidence
                            });
            #endregion

            #region Join both lists together (UNION does a distinct)
            var fullList = o2oList1.Union(o2oList2);
            #endregion

            // Not sure if we have to worry about removing aliases here????

            #region Build return list. use compare class to only add a company name once
            List<Organisation_TradingNames> ret = new List<Organisation_TradingNames>();

            foreach (var x in fullList)
            {
                var r = new Organisation_TradingNames()
                {
                    TradingName = x.name,
                    Confirmed = x.confidence == (int)LinkConfidence.Confirmed
                };

                if (!ret.Contains(r, new Organisation_TradingNames_Comparer()))
                    ret.Add(r);
            }
            #endregion
            return ret;
        }

        public List<Organisation_FormallyKnownAs> Organisation_FormerNames(EntityAliases organisationAliases, int organisationId, int riskClaimId, bool _trace)
        {
            List<int> organisationsId = FindAllOrganisationAliases(organisationId, organisationAliases, _trace);

            #region Get list of links where the OrgId we want is at one end of the link
            var o2oList1 = (from o2o in _db.Organisation2Organisation.AsNoTracking()
                            where organisationsId.Contains(o2o.Organisation1_Id)
                            && o2o.LinkConfidence == (int)LinkConfidence.Confirmed
                            && o2o.ADARecordStatus == (byte)ADARecordStatus.Current
                            && o2o.OrganisationLinkType_Id == (int)MDA.Common.Enum.OrganisationLinkType.FormerlyKnownAs
                            select new
                            {
                                id = o2o.Organisation1_Id,
                                name = o2o.Organisation.OrganisationName,
                                confidence = o2o.LinkConfidence
                            });
            #endregion

            #region Get list of links where the OrgId we want is at the other end of the link
            var o2oList2 = (from o2o in _db.Organisation2Organisation.AsNoTracking()
                            where organisationsId.Contains(o2o.Organisation2_Id)
                            && o2o.LinkConfidence == (int)LinkConfidence.Confirmed
                            && o2o.ADARecordStatus == (byte)ADARecordStatus.Current
                            && o2o.OrganisationLinkType_Id == (int)MDA.Common.Enum.OrganisationLinkType.FormerlyKnownAs
                            select new
                            {
                                id = o2o.Organisation1_Id,
                                name = o2o.Organisation1.OrganisationName,
                                confidence = o2o.LinkConfidence
                            });
            #endregion

            #region Join both lists together (UNION does a distinct)
            var fullList = o2oList1.Union(o2oList2);
            #endregion

            // Not sure if we have to worry about removing aliases here????

            #region Build return list. use compare class to only add a company name once
            List<Organisation_FormallyKnownAs> ret = new List<Organisation_FormallyKnownAs>();

            foreach (var x in fullList)
            {
                var r = new Organisation_FormallyKnownAs()
                        {
                            FormerName = x.name,
                            Confirmed = x.confidence == (int)LinkConfidence.Confirmed
                        };

                if (!ret.Contains(r, new Organisation_FormallyKnownAs_Comparer()))
                    ret.Add(r);
            }
            #endregion

            return ret;
        }
#endif
    }
    
#if false
    public class Organisation_TradingNames_Comparer : IEqualityComparer<Organisation_TradingNames>
    {
        public bool Equals(Organisation_TradingNames x1, Organisation_TradingNames x2)
        {
            if (object.ReferenceEquals(x1, x2))
                return true;

            if (x1 == null || x2 == null)
                return false;

            return x1.TradingName.Equals(x2.TradingName);
        }

        public int GetHashCode(Organisation_TradingNames x)
        {
            return x.TradingName.GetHashCode();
        }
    }

    public class Organisation_FormallyKnownAs_Comparer : IEqualityComparer<Organisation_FormallyKnownAs>
    {
        public bool Equals(Organisation_FormallyKnownAs x1, Organisation_FormallyKnownAs x2)
        {
            if (object.ReferenceEquals(x1, x2))
                return true;

            if (x1 == null || x2 == null)
                return false;

            return x1.FormerName.Equals(x2.FormerName);
        }

        public int GetHashCode(Organisation_FormallyKnownAs x)
        {
            return x.FormerName.GetHashCode();
        }
    }
#endif
}
