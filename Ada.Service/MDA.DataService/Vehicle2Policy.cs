﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllVehicle2PolicyAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Vehicle2Policy set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}
        public List<Vehicle2Policy> GetVehicle2PolicyLinks(int vehicleId, int policyId, int baseRiskClaimId)
        {
            return (from x in _db.Vehicle2Policy
                    where x.Policy_Id == policyId && x.Vehicle_Id == vehicleId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Vehicle2Policy GetLatestVehicle2PolicyLink(int vehicleId, int policyId, int baseRiskClaimId)
        {
            return (from x in _db.Vehicle2Policy
                    where x.Policy_Id == policyId && x.Vehicle_Id == vehicleId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void RedirectVehicle2PolicyLinks(int masterVehicleId, int vehicleId)
        {
            _db.Database.ExecuteSqlCommand("update Vehicle2Policy set Vehicle_Id = " + masterVehicleId +
                                           " where Vehicle_Id = " + vehicleId);
        }

        public void RedirectPolicy2VehicleLinks(int masterPolicyId, int policyId)
        {
            _db.Database.ExecuteSqlCommand("update Vehicle2Policy set Policy_Id = " + masterPolicyId +
                                           " where Policy_Id = " + policyId);
        }

        public void CleanUpVehicle2PolicyLinks(/*Vehicle2Policy latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Vehicle2Policy
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Vehicle2Policy Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Vehicle2Policy UpdateVehicle2PolicyLink(MDA.Common.Enum.Operation op, Vehicle2Policy link,
                                            //MDA.Common.Enum.PolicyLinkType linkType,
                                            MDA.Pipeline.Model.PipelineVehicle2PolicyLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.PolicyLinkType_Id != (int)pipeLink.PolicyLinkType_Id)
                            link.PolicyLinkType_Id = (int)pipeLink.PolicyLinkType_Id;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;

        }


        public Vehicle2Policy UpdateVehicle2PolicyLink(MDA.Common.Enum.Operation op, int linkId,
                                            //MDA.Common.Enum.PolicyLinkType linkType,
                                            MDA.Pipeline.Model.PipelineVehicle2PolicyLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Vehicle2Policy where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateVehicle2PolicyLink(op, link, pipeLink, claim, ref success);
        }

        public Vehicle2Policy InsertVehicle2PolicyLink(int vehicleId, int policyId,
                                        MDA.Pipeline.Model.PipelineVehicle2PolicyLink pipeLink,
                                        RiskClaim claim)
        {
            Vehicle2Policy v2p = _db.Vehicle2Policy.Create();

            v2p.CreatedBy         = pipeLink.CreatedBy;
            v2p.CreatedDate       = pipeLink.CreatedDate;
            v2p.ModifiedBy        = pipeLink.ModifiedBy;
            v2p.ModifiedDate      = pipeLink.ModifiedDate;
            v2p.Source            = pipeLink.Source;
            v2p.SourceDescription = pipeLink.SourceDescription;
            v2p.SourceReference   = pipeLink.SourceReference;
            v2p.ADARecordStatus   = pipeLink.ADARecordStatus;
            v2p.RecordStatus      = pipeLink.RecordStatus;
            v2p.LinkConfidence    = pipeLink.LinkConfidence;

            v2p.RiskClaim_Id     = claim.Id;
            v2p.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            v2p.Policy_Id         = policyId;
            v2p.Vehicle_Id        = vehicleId;
            v2p.PolicyLinkType_Id = pipeLink.PolicyLinkType_Id;
            
            v2p.FiveGrading = GetFiveGrading();

            _db.Vehicle2Policy.Add(v2p);

            _db.SaveChanges();

            return v2p;
        }

        //public Vehicle2Policy InsertVehicle2PolicyLink(Vehicle2Policy vehicle2policy)
        //{
        //    Vehicle2Policy v2p = _db.Vehicle2Policy.Create();

        //    v2p.CreatedBy = vehicle2policy.CreatedBy;
        //    v2p.CreatedDate = DateTime.Now;
        //    v2p.BaseRiskClaim_Id = vehicle2policy.BaseRiskClaim_Id;

        //    v2p.Source = vehicle2policy.Source;
        //    v2p.SourceDescription = vehicle2policy.SourceDescription;
        //    v2p.SourceReference = vehicle2policy.SourceReference;

        //    v2p.Policy_Id = vehicle2policy.Policy_Id;
        //    v2p.PolicyLinkId = vehicle2policy.PolicyLinkId;
        //    v2p.PolicyLinkType_Id = vehicle2policy.PolicyLinkType_Id;
        //    v2p.Vehicle_Id = vehicle2policy.Vehicle_Id;

        //    v2p.IBaseId = vehicle2policy.IBaseId;
        //    v2p.RiskClaim_Id = vehicle2policy.RiskClaim_Id;
        //    v2p.LinkConfidence = vehicle2policy.LinkConfidence;
        //    v2p.FiveGrading = vehicle2policy.FiveGrading;

        //    _db.Vehicle2Policy.Add(v2p);

        //    _db.SaveChanges();

        //    return v2p;
        //}

    }

}
