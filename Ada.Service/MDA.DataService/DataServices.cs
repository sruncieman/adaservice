﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Enum;
//using System.Data.Objects;

namespace MDA.DataService
{
    public partial class DataServices
    {
        private bool _trace = false;
        private CurrentContext _ctx;
        private MdaDbContext _db;

        public DataServices(CurrentContext ctx)
        {
            _ctx = ctx;
            _db = ctx.db as MdaDbContext;
            _trace = ctx.TraceLoad;
        }

        private string GetSourceDescription()
        {
            return "ADA";
        }

        private string GetFiveGrading()
        {
            return "B21";
        }

        private DateTime? ConvertToTime(TimeSpan t)
        {
            return new DateTime(t.Ticks);
        }

        private long? GetCurrentTimestamp(MdaDbContext db)
        {
            return db.usp_GetCurrentRowVersion().First();
        }

        //public void ResetAllKeoghsCase2IncidentLinkAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update KeoghsCase2Incident set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        //public void ResetAllPerson2BBPinLinkAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2BBPin set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        //public void ResetAllPerson2WebsiteAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2Website set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        /// <summary>
        /// Set all inks record with an AdaRecordStatus >= 10 back to a value 0..n
        /// </summary>
        //public void ResetAllLinkAdaRecordStatus()
        //{
            //ResetAllKeoghsCase2IncidentLinkAdaStatus();

            //ResetAllAddress2AddressLinkAdaStatus();

            //ResetAllIncident2AddressLinkAdaStatus();
            //ResetAllIncident2FraudRingLinkAdaStatus();
            //ResetAllIncident2IncidentLinkAdaStatus();
            //ResetAllIncident2OrganisationLinkAdaStatus();
            //ResetAllIncident2OrganisationOutcomeLinkAdaStatus();
            //ResetAllIncident2PersonLinkAdaStatus();
            //ResetAllIncident2PersonOutcomeLinkAdaStatus();
            //ResetAllIncident2PolicyLinkAdaStatus();
            //ResetAllIncident2VehicleLinkAdaStatus();

            //ResetAllOrganisation2AddressAdaStatus();
            //ResetAllOrganisation2BankAccountAdaStatus();
            //ResetAllOrganisation2EmailAdaStatus();
            //ResetAllOrganisation2OrganisationAdaStatus();
            //ResetAllOrganisation2PaymentCardAdaStatus();
            //ResetAllOrganisation2PolicyAdaStatus();
            //ResetAllOrganisation2TelephoneAdaStatus();
            //ResetAllOrganisation2WebsiteAdaStatus();

            //ResetAllPerson2AddressAdaStatus();
            //ResetAllPerson2BankAccountAdaStatus();
            //ResetAllPerson2BBPinLinkAdaStatus();
            //ResetAllPerson2DrivingLicenseAdaStatus();
            //ResetAllPerson2EmailAdaStatus();
            //ResetAllPerson2NINumberAdaStatus();
            //ResetAllPerson2OrganisationAdaStatus();
            //ResetAllPerson2PassportAdaStatus();
            //ResetAllPerson2PaymentCardAdaStatus();
            //ResetAllPerson2PersonCardAdaStatus();
            //ResetAllPerson2PolicyAdaStatus();
            //ResetAllPerson2TelephoneAdaStatus();
            //ResetAllPerson2WebsiteAdaStatus();

            //ResetAllPolicy2BankAccountAdaStatus();
            //ResetAllPolicy2PaymentCardAdaStatus();

            //ResetAllVehicle2AddressAdaStatus();
            //ResetAllVehicle2OrganisationAdaStatus();
            //ResetAllVehicle2PersonAdaStatus();
            //ResetAllVehicle2PolicyAdaStatus();
            //ResetAllVehicle2VehicleAdaStatus();
        //}

        //public void ResetAllKeoghsCaseAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update KeoghsCase set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        //public void ResetAllBBPinAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update BBPin set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void ResetAllAdaRecordStatus()
        {
            _db.uspResetADARecordStatus();

            //ResetAllKeoghsCaseAdaStatus();
            //ResetAllBBPinAdaStatus();

            //ResetAllAddressAdaStatus();
            //ResetAllBankAccountAdaStatus();
            //ResetAllEmailAdaStatus();
            //ResetAllIncidentAdaStatus();
            //ResetAllDrivingLicenseAdaStatus();
            //ResetAllNINumberAdaStatus();
            //ResetAllOrganisationAdaStatus();
            //ResetAllPassportAdaStatus();
            //ResetAllPaymentCardAdaStatus();
            //ResetAllPersonAdaStatus();
            //ResetAllPolicyAdaStatus();
            //ResetAllTelephoneAdaStatus();
            //ResetAllVehicleAdaStatus();
            //ResetAllWebSiteAdaStatus();
        }
    }
}
