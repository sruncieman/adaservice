﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllOrganisation2OrganisationAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Organisation2Organisation set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectOrganisation2OrganisationLinks(int masterOrganisationId, int organisationId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2Organisation set Organisation1_Id = " + masterOrganisationId +
                                           " where Organisation1_Id = " + organisationId);

            _db.Database.ExecuteSqlCommand("update Organisation2Organisation set Organisation2_Id = " + masterOrganisationId +
                                           " where Organisation2_Id = " + organisationId);
        }

        public List<Organisation2Organisation> GetOrganisation2OrganisationLinks(int org1Id, int org2Id)
        {
            return (from x in _db.Organisation2Organisation
                    where ((x.Organisation1_Id == org1Id && x.Organisation2_Id == org2Id) || (x.Organisation2_Id == org1Id && x.Organisation1_Id == org2Id))
                    orderby x.CreatedDate ascending
                    select x).ToList();
        }

        public Organisation2Organisation GetLatestOrganisation2OrganisationLink(int org1Id, int org2Id)
        {
            return (from x in _db.Organisation2Organisation
                    where ((x.Organisation1_Id == org1Id && x.Organisation2_Id == org2Id) || (x.Organisation2_Id == org1Id && x.Organisation1_Id == org2Id)) &&
                          (x.ADARecordStatus == (byte)ADARecordStatus.Current || x.ADARecordStatus == (byte)ADARecordStatus.SyncCurrent)
                    select x).FirstOrDefault();
        }

        //public void CleanUpOrganisation2OrganisationLinks(/*Organisation2Organisation latestLink, RiskClaim newRiskClaim*/)
        //{
        //    var oldlinks = (from x in _db.Organisation2Organisation
        //                    where //(x.Organisation1_Id == latestLink.Organisation1_Id || x.Organisation2_Id == latestLink.Organisation2_Id) &&
        //                          x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
        //                          x.RiskClaim_Id != newRiskClaim.Id &&
        //                          x.ADARecordStatus == (byte)ADARecordStatus.Current &&
        //                          x.SourceDescription == "ADA Auto-Matching"
        //                    select x).ToList();

        //    foreach (var link in oldlinks)
        //    {
        //        if (ctx.Tracing)
        //            ADATrace.WriteLine("Organisation2Organisation Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

        //        link.RecordStatus = Constants.IBaseDeleteValue;
        //        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
        //    }
        //    _db.SaveChanges();
        //}

        public Organisation2Organisation UpdateOrganisation2OrganisationLink(MDA.Common.Enum.Operation op, Organisation2Organisation link,
                                                    //MDA.Common.Enum.OrganisationLinkType linkType, 
                                                    MDA.Pipeline.Model.PipelineOrganisation2OrganisationLink pipeLink, /*RiskClaim newRiskClaim,*/ ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {
 
                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        //link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence && link.LinkConfidence > (int)pipeLink.LinkConfidence)
                        {
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;
                        }

                        if (link.OrganisationLinkType_Id != (int)pipeLink.OrganisationLinkType_Id)
                            link.OrganisationLinkType_Id = (int)pipeLink.OrganisationLinkType_Id;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Organisation2Organisation UpdateOrganisation2OrganisationLink(MDA.Common.Enum.Operation op, int linkId,
                                               //MDA.Common.Enum.OrganisationLinkType linkType, 
                                                MDA.Pipeline.Model.PipelineOrganisation2OrganisationLink pipeLink, /*RiskClaim claim,*/ ref bool success)
        {
            success = false;

            var link = (from x in _db.Organisation2Organisation where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateOrganisation2OrganisationLink(op, link, /*linkType,*/ pipeLink, /*claim,*/ ref success);
        }

        public Organisation2Organisation InsertOrganisation2OrganisationLink(int org1Id, int org2Id, MDA.Pipeline.Model.PipelineOrganisation2OrganisationLink pipeLink) //, MDA.Common.Enum.OrganisationLinkType linkType) //, RiskClaim claim)
        {
            Organisation2Organisation o2o = _db.Organisation2Organisation.Create();

            o2o.CreatedBy         = pipeLink.CreatedBy;
            o2o.CreatedDate       = pipeLink.CreatedDate;
            o2o.ModifiedBy        = pipeLink.ModifiedBy;
            o2o.ModifiedDate      = pipeLink.ModifiedDate;
            o2o.Source            = pipeLink.Source;
            o2o.SourceDescription = pipeLink.SourceDescription;
            o2o.SourceReference   = pipeLink.SourceReference;
            o2o.ADARecordStatus   = pipeLink.ADARecordStatus;
            o2o.RecordStatus      = pipeLink.RecordStatus;
            o2o.LinkConfidence    = pipeLink.LinkConfidence;

            //o2o.RiskClaim_Id = claim.Id;
            //o2o.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;


            o2o.Organisation1_Id = org1Id;
            o2o.Organisation2_Id = org2Id;

            o2o.OrganisationLinkType_Id = (int)pipeLink.OrganisationLinkType_Id; // linkType;

            o2o.Source = "Keoghs CFS";
            o2o.SourceDescription = "ADA Auto-Matching";
            o2o.SourceReference = null;
            o2o.FiveGrading = GetFiveGrading();

            _db.Organisation2Organisation.Add(o2o);

            _db.SaveChanges();

            return o2o;
        }

        //public Organisation2Organisation InsertOrganisation2OrganisationLink(Organisation2Organisation org2org)
        //{
        //    Organisation2Organisation o2o = _db.Organisation2Organisation.Create();

        //    o2o.CreatedBy = org2org.CreatedBy;
        //    o2o.CreatedDate = DateTime.Now;
        //    //#o2o.BaseRiskClaim_Id = org2org.BaseRiskClaim_Id;

        //    o2o.Source = org2org.Source;
        //    o2o.SourceDescription = org2org.SourceDescription;
        //    o2o.SourceReference = org2org.SourceReference;
        //    o2o.IBaseId = org2org.IBaseId;

        //    o2o.Organisation1_Id = org2org.Organisation1_Id;
        //    o2o.Organisation2_Id = org2org.Organisation2_Id;
        //    o2o.OrganisationLinkType_Id = org2org.OrganisationLinkType_Id;
        //    o2o.LinkConfidence = org2org.LinkConfidence;
        //    o2o.FiveGrading = org2org.FiveGrading;

        //    //#o2o.RiskClaim_Id = org2org.RiskClaim_Id;

        //    _db.Organisation2Organisation.Add(o2o);

        //    _db.SaveChanges();

        //    return o2o;
        //}
    }

}
