﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public void RedirectAddress2IncidentLinks(int masterAddressId, int addressId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2Address set Address_Id = " + masterAddressId +
                                           " where Address_Id = " + addressId);
        }

        public void RedirectIncident2AddressLinks(int masterIncidentId, int incidentId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2Address set Incident_Id = " + masterIncidentId +
                                           " where Incident_Id = " + incidentId);
        }

        //public void ResetAllIncident2AddressLinkAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Incident2Address set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}
    }

}
