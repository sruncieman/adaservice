﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllOrganisation2PolicyAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Organisation2Policy set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public List<Organisation2Policy> GetOrganisation2PolicyLinks(int organisationId, int policyId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2Policy
                    where x.Organisation_Id == organisationId && x.Policy_Id == policyId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Organisation2Policy GetLatestOrganisation2PolicyLink(int organisationId, int policyId, int baseRiskClaimId)
        {
            return (from x in _db.Organisation2Policy
                    where x.Organisation_Id == organisationId && x.Policy_Id == policyId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void RedirectPolicy2OrganisationLinks(int masterPolicyId, int policyId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2Policy set Policy_Id = " + masterPolicyId +
                                           " where Policy_Id = " + policyId);
        }

        public void RedirectOrganisation2PolicyLinks(int masterOrganisationId, int organisationId)
        {
            _db.Database.ExecuteSqlCommand("update Organisation2Policy set Organisation_Id = " + masterOrganisationId +
                                           " where Organisation_Id = " + organisationId);
        }

        public void CleanUpOrganisation2PolicyLinks(RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Organisation2Policy
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Organisation2Policy Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Organisation2Policy UpdateOrganisation2PolicyLink(MDA.Common.Enum.Operation op, Organisation2Policy link,
                                           MDA.Pipeline.Model.PipelinePolicyLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Organisation2Policy UpdateOrganisation2PolicyLink(MDA.Common.Enum.Operation op, int linkId,
                                           MDA.Pipeline.Model.PipelinePolicyLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Organisation2Policy where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateOrganisation2PolicyLink(op, link, pipeLink, claim, ref success);
        }


        public Organisation2Policy InsertOrganisation2PolicyLink(int organisationId, int policyId, MDA.Pipeline.Model.PipelinePolicyLink pipeLink,
                                                        //.Common.Enum.PolicyLinkType linkType, 
                                                        RiskClaim claim)
        {
            Organisation2Policy o2p = _db.Organisation2Policy.Create();

            o2p.CreatedBy         = pipeLink.CreatedBy;
            o2p.CreatedDate       = pipeLink.CreatedDate;
            o2p.ModifiedBy        = pipeLink.ModifiedBy;
            o2p.ModifiedDate      = pipeLink.ModifiedDate;
            o2p.Source            = pipeLink.Source;
            o2p.SourceDescription = pipeLink.SourceDescription;
            o2p.SourceReference   = pipeLink.SourceReference;
            o2p.ADARecordStatus   = pipeLink.ADARecordStatus;
            o2p.RecordStatus      = pipeLink.RecordStatus;
            o2p.LinkConfidence    = pipeLink.LinkConfidence;

            o2p.RiskClaim_Id = claim.Id;
            o2p.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            o2p.Organisation_Id = organisationId;
            o2p.Policy_Id = policyId;
            o2p.PolicyLinkType_Id = (int)pipeLink.PolicyLinkType_Id; // linkType;
            
            o2p.FiveGrading = GetFiveGrading();

            _db.Organisation2Policy.Add(o2p);

            _db.SaveChanges();

            return o2p;
        }

        //public Organisation2Policy InsertOrganisation2PolicyLink(Organisation2Policy org2policy)
        //{
        //    Organisation2Policy o2p = _db.Organisation2Policy.Create();

        //    o2p.CreatedBy = org2policy.CreatedBy;
        //    o2p.CreatedDate = DateTime.Now;
        //    o2p.BaseRiskClaim_Id = org2policy.BaseRiskClaim_Id;

        //    o2p.Source = org2policy.Source;
        //    o2p.SourceDescription = org2policy.SourceDescription;
        //    o2p.SourceReference = org2policy.SourceReference;
        //    o2p.IBaseId = org2policy.IBaseId;

        //    o2p.Organisation_Id = org2policy.Organisation_Id;
        //    o2p.Policy_Id = org2policy.Policy_Id;
        //    o2p.PolicyLinkId = org2policy.PolicyLinkId;
        //    o2p.PolicyLinkType_Id = org2policy.PolicyLinkType_Id;
        //    o2p.LinkConfidence = org2policy.LinkConfidence;
        //    o2p.FiveGrading = GetFiveGrading();

        //    o2p.RiskClaim_Id = org2policy.RiskClaim_Id;

        //    _db.Organisation2Policy.Add(o2p);

        //    _db.SaveChanges();

        //    return o2p;
        //}
    }

}
