﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public RiskClaim GetBaseRiskClaim(int? existingRiskClaimId)
        {
            RiskClaim rc = null;
            RiskClaim result = null;
            
         
            int baseId = 0;

            rc = _db.RiskClaims.FirstOrDefault(x => x.Id == existingRiskClaimId);

            if (rc != null)
            {
                baseId = rc.BaseRiskClaim_Id;
                result = _db.RiskClaims.FirstOrDefault(x => x.Id == baseId);
            }
            return result;
        }
    }
}
