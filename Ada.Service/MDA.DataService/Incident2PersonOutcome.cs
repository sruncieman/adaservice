﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public void RedirectIncident2PersonOutcomeLinks(int masterIncidentId, int incidentId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2PersonOutcome set Incident_Id = " + masterIncidentId +
                                           " where Incident_Id = " + incidentId);
        }

        //public void ResetAllIncident2PersonOutcomeLinkAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Incident2PersonOutcome set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}
    }

}
