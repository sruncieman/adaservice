﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Helpers;
using MDA.Common;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPolicyAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Policy set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public List<int> GetAllSyncPolicyIds()
        {
            return (from x in _db.Policies where x.ADARecordStatus >= 10 select x.Id).ToList();
        }

        public void ResetSyncPolicyAdaStatus(int id)
        {
            var r = (from x in _db.Policies where x.Id == id select x).FirstOrDefault();

            if (r != null && r.ADARecordStatus >= 10)
            {
                r.ADARecordStatus -= 10;

                _db.SaveChanges();
            }
        }

        public void CleansePolicy(int id, Func<Policy, MessageNode, int> cleanseMethod)
        {
            var p = (from x in _db.Policies where x.Id == id select x).FirstOrDefault();

            if (p == null) return;

            MessageNode n = new MessageNode();

            cleanseMethod(p, n);

            _db.SaveChanges();
        }

        public void DeduplicatePolicyList(int masterPolicyId, List<int> matchList)
        {
            foreach (var i in matchList)
            {
                var ir = (from x in _db.Policies where x.Id == i select x).FirstOrDefault();

                if (ir != null)
                {
                    RedirectPolicy2BankAccountLinks(masterPolicyId, i);
                    RedirectPolicy2PaymentCardLinks(masterPolicyId, i);
                    RedirectPolicy2IncidentLinks(masterPolicyId, i);
                    RedirectPolicy2PersonLinks(masterPolicyId, i);
                    RedirectPolicy2OrganisationLinks(masterPolicyId, i);
                    RedirectPolicy2VehicleLinks(masterPolicyId, i);


                    ir.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                    ir.RecordStatus = Constants.IBaseDeleteValue;

                    _db.SaveChanges();
                }
            }
        }

        public Policy SelectPolicy(int id)
        {
            return (from x in _db.Policies where x.Id == id select x).FirstOrDefault();
        }

        public List<int> VerifyPolicyListExists(List<int> listId)
        {
            List<int> outList = new List<int>();

            foreach (int id in listId)
            {
                if (_db.Policies.Any(x => x.Id == id))
                    outList.Add(id);
            }
            return outList;
        }

        public Policy FindPolicy(string policyNumber)
        {
            return (from x in _db.Policies where x.PolicyNumber == policyNumber select x).FirstOrDefault();
        }

        public Policy UpdatePolicy(int policyId, MDA.Pipeline.Model.PipelinePolicy policy, out bool success)
        {
            success = false;

            var p = (from x in _db.Policies where x.Id == policyId select x).FirstOrDefault();
            if (p == null) return p;

            try
            {
                success = true;

                bool modified = false;

                if (success == false) return p;

                if (string.Compare(p.PolicyNumber, policy.PolicyNumber, true) != 0)
                {
                    p.PolicyNumber = policy.PolicyNumber;
                    modified = true;
                }

                if (string.Compare(p.InsurerTradingName, policy.InsurerTradingName, true) != 0)
                {
                    p.InsurerTradingName = policy.InsurerTradingName;
                    modified = true;
                }

                if (string.Compare(p.Broker, policy.Broker, true) != 0)
                {
                    p.Broker = policy.Broker;
                    modified = true;
                }

                if (p.PolicyType_Id != (int)policy.PolicyType_Id)
                {
                    p.PolicyType_Id = (int)policy.PolicyType_Id;
                    modified = true;
                }

                if (p.PolicyCoverType_Id != (int)policy.PolicyCoverType_Id)
                {
                    p.PolicyCoverType_Id = (int)policy.PolicyCoverType_Id;
                    modified = true;
                }

                if (p.PolicyStartDate != MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(policy.PolicyStartDate))
                {
                    p.PolicyStartDate = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(policy.PolicyStartDate);
                    modified = true;
                }

                if (p.PolicyEndDate != MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(policy.PolicyEndDate))
                {
                    p.PolicyEndDate = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(policy.PolicyEndDate);
                    modified = true;
                }

                if (modified)
                {
                    p.ModifiedDate = DateTime.Now;
                    p.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return p;
        }

        public Policy InsertPolicy(MDA.Pipeline.Model.PipelinePolicy policy)
        {
            Policy i = _db.Policies.Create();

            i.ADARecordStatus            = policy.ADARecordStatus;
            i.Broker                     = policy.Broker;
            i.CreatedBy                  = policy.CreatedBy;
            i.CreatedDate                = policy.CreatedDate;
            i.IBaseId                    = policy.IBaseId;
            i.Insurer                    = policy.Insurer;
            i.InsurerTradingName         = policy.InsurerTradingName;
            i.KeyAttractor               = policy.KeyAttractor;
            i.ModifiedBy                 = policy.ModifiedBy;
            i.ModifiedDate               = policy.ModifiedDate;
            i.PolicyCoverType_Id         = policy.PolicyCoverType_Id;
            i.PolicyEndDate              = policy.PolicyEndDate; // MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(policy.PolicyEndDate);
            i.PolicyId                   = policy.PolicyId;
            i.PolicyNumber               = policy.PolicyNumber;
            i.PolicyStartDate            = policy.PolicyStartDate; // MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(policy.PolicyStartDate);
            i.PolicyType_Id              = policy.PolicyType_Id;
            i.Premium                    = policy.Premium;
            i.PreviousFaultClaimsCount   = policy.PreviousFaultClaimsCount;
            i.PreviousNoFaultClaimsCount = policy.PreviousNoFaultClaimsCount;
            i.RecordStatus               = policy.RecordStatus;
            i.Source                     = policy.Source;
            i.SourceDescription          = policy.SourceDescription;
            i.SourceReference            = policy.SourceReference;

            _db.Policies.Add(i);

            _db.SaveChanges();

            return i;
        }

        //public Policy InsertPolicy(Policy policy)
        //{
        //    Policy p = _db.Policies.Create();

        //    p.CreatedBy = policy.CreatedBy;
        //    p.CreatedDate = policy.CreatedDate;
        //    p.Broker = policy.Broker;
        //    p.IBaseId = policy.IBaseId;
        //    p.Insurer = policy.Insurer;
        //    p.InsurerTradingName = policy.InsurerTradingName;
        //    p.PolicyCoverType_Id = policy.PolicyCoverType_Id;
        //    p.PolicyEndDate = policy.PolicyEndDate;
        //    p.PolicyId = policy.PolicyId;
        //    p.PolicyNumber = policy.PolicyNumber;
        //    p.PolicyStartDate = policy.PolicyStartDate;
        //    p.PolicyType_Id = policy.PolicyType_Id;
        //    p.Premium = policy.Premium;
        //    p.PreviousFaultClaimsCount = policy.PreviousFaultClaimsCount;
        //    p.PreviousNoFaultClaimsCount = policy.PreviousNoFaultClaimsCount;
        //    p.Source = policy.Source;
        //    p.SourceDescription = policy.SourceDescription;
        //    p.SourceReference = policy.SourceReference;

        //    _db.Policies.Add(p);

        //    _db.SaveChanges();

        //    return p;
        //}
    }

}
