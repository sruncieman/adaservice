﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Helpers;
using RiskEngine.Model;
using MDA.Pipeline.Model;
using MDA.Common.Debug;


namespace MDA.DataService 
{
    public partial class DataServices
    {
        //public void ResetAllAddressAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Address set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public List<int> GetAllSyncAddressIds()
        {
            //return (from x in _db.Addresses where x.ADARecordStatus == 10 && (x.PafUPRN == null || x.PafUPRN == "") select x.Id).ToList();
            return (from x in _db.Addresses where x.ADARecordStatus >= 10 select x.Id).ToList(); // removed condition to only return non UPRN addresses to fix Bug 8756 - UPRN addresses not being duped against non UPRN addresses
        }

        /// <summary>
        /// Look for addresses with ADARecordStatus >= 10
        /// </summary>
        /// <param name="db"></param>
        /// <param name="trace"></param>
        public int DeduplicateUPRNAddresses(CurrentContext ctx, Func<string, int> ShowProgress = null)
        {
            //trace = new List<string>();
            int duplicateCount = 0;

            var q = ctx.db.Addresses.Where(x => (x.ADARecordStatus == 0 || x.ADARecordStatus >= 10) && x.PafUPRN != null && x.PafUPRN != "").Select(x=>x.PafUPRN);

            var query = (q.GroupBy(i => i)
                        .Where(g => g.Count() > 1)
                        .Select(g => g.Key)).ToList();

            foreach (var duprn in query)
            {
                if (duprn == null) continue;

                duplicateCount++;

                using (var ctxx = new CurrentContext(ctx))
                {
                    var master = (from t in ctxx.db.Addresses
                                  where t.PafUPRN == duprn && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10) && t.AddressId != null
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();

                    if (master == null)
                    {
                        master = (from t in ctxx.db.Addresses
                                  where t.PafUPRN == duprn && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();
                    }

                    if (master == null) continue;

                    var allDuplicates = from t in ctxx.db.Addresses
                                        where t.PafUPRN == duprn && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                        && t.Id != master.Id
                                        select t;


                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                    {
                        try
                        {
                            foreach (var td in allDuplicates)
                            {
                                List<int> AId = new List<int>();   // Keep track of Address ID's changed
                                List<int> IId = new List<int>();   // Keep track of Incident ID's changed
                                List<int> OId = new List<int>();   // Keep track of Org ID's changed
                                List<int> PId = new List<int>();   // Keep track of People ID's changed
                                List<int> VId = new List<int>();   // Keep track of Vehicle ID's changed

                                ctxx.db.Entry(td).Collection(z => z.Address2Address).Load();

                                if (td.Address2Address.Any())
                                {
                                    foreach (var l in td.Address2Address)
                                    {
                                        if (l.Address1_Id == td.Id)
                                            l.Address1_Id = master.Id;

                                        if (l.Address2_Id == td.Id)
                                            l.Address2_Id = master.Id;

                                        AId.Add(l.Id);
                                    }
                                }

                                ctxx.db.Entry(td).Collection(z => z.Incident2Address).Load();

                                if (td.Incident2Address.Any())
                                {
                                    foreach (var l in td.Incident2Address)
                                    {
                                        l.Address_Id = master.Id;
                                        IId.Add(l.Incident_Id);
                                    }
                                }

                                ctxx.db.Entry(td).Collection(z => z.Organisation2Address).Load();

                                if (td.Organisation2Address.Any())
                                {
                                    foreach (var l in td.Organisation2Address)
                                    {
                                        l.Address_Id = master.Id;
                                        OId.Add(l.Organisation_Id);
                                    }
                                }

                                ctxx.db.Entry(td).Collection(z => z.Person2Address).Load();

                                if (td.Person2Address.Any())
                                {
                                    foreach (var l in td.Person2Address)
                                    {
                                        l.Address_Id = master.Id;
                                        PId.Add(l.Person_Id);
                                    }
                                }

                                ctxx.db.Entry(td).Collection(z => z.Vehicle2Address).Load();

                                if (td.Vehicle2Address.Any())
                                {
                                    foreach (var l in td.Vehicle2Address)
                                    {
                                        l.Address_Id = master.Id;
                                        VId.Add(l.Vehicle_Id);
                                    }
                                }

                                td.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                                td.RecordStatus = Constants.IBaseDeleteValue;

                                //db.Addresses.Remove(td);

                                string s = string.Format("Duplicate Addr Removed [{0}] : New Master Id [{1}] : A2A ID's Moved [{2}] : Incident ID's Moved [{3}] : Org ID's Moved [{4}] : People ID's Moved [{5}] : Vehicle ID's Moved [{6}]",
                                                td.Id, master.Id, string.Join(",", AId), string.Join(",", IId), string.Join(",", OId), string.Join(",", PId), string.Join(",", VId));

                                if (ShowProgress != null)
                                    ShowProgress(s);

                                //trace.Add(s);
                            }
                            ctxx.db.SaveChanges();

                            transaction.Commit();

                        }
                        catch (Exception)
                        {
                            transaction.Rollback();

                            throw;
                        }
                    }
                }
            }

            if (ShowProgress != null)
                ShowProgress(string.Format("Duplicates Found [{0}]", duplicateCount));

            return duplicateCount;
        }

        public void DeduplicateAddressList(int masterAddressId, List<int> matchList)
        {

            var address = (from i in _db.Addresses where i.Id == masterAddressId select i).FirstOrDefault();
                       

            foreach (var i in matchList)
            {
                var ir = (from x in _db.Addresses where x.Id == i select x).FirstOrDefault();

                if (ir != null)
                {
                    if (!string.IsNullOrWhiteSpace(ir.PafUPRN) && string.IsNullOrWhiteSpace(address.PafUPRN)) // Fix to defect 7902 - If we match against a PAFUPRN address and the index address has no UPRN then we reuse the address with the UPRN and 254 the index address
                    {

                        RedirectAddress2AddressLinks(i, masterAddressId);
                        RedirectAddress2IncidentLinks(i, masterAddressId);
                        RedirectAddress2OrganisationLinks(i, masterAddressId);
                        RedirectAddress2PersonLinks(i, masterAddressId);
                        RedirectAddress2VehicleLinks(i, masterAddressId);

                        address.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                        address.RecordStatus = Constants.IBaseDeleteValue;

                        _db.SaveChanges();

                    }
                    else
                    {

                        RedirectAddress2AddressLinks(masterAddressId, i);
                        RedirectAddress2IncidentLinks(masterAddressId, i);
                        RedirectAddress2OrganisationLinks(masterAddressId, i);
                        RedirectAddress2PersonLinks(masterAddressId, i);
                        RedirectAddress2VehicleLinks(masterAddressId, i);

                        ir.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                        ir.RecordStatus = Constants.IBaseDeleteValue;

                        _db.SaveChanges();

                    }
                }
            }
        }

        public void ResetSyncAddressAdaStatus(int id)
        {
            var r = (from x in _db.Addresses where x.Id == id select x).FirstOrDefault();

            if (r != null && r.ADARecordStatus >= 10)
            {
                r.ADARecordStatus -= 10;

                _db.SaveChanges();
            }
        }

        public Address SelectAddress(int id)
        {
            return (from x in _db.Addresses where x.Id == id select x).FirstOrDefault();
        }

        public List<int> VerifyAddressListExists(List<int> listId)
        {
            List<int> outList = new List<int>();

            foreach (int id in listId)
            {
                if (_db.Addresses.Any(x => x.Id == id))
                    outList.Add(id);
            }
            return outList;
        }

        public Address UpdateAddress(int addressId, MDA.Pipeline.Model.PipelineAddress address, out bool success, out string error)
        {
            success = false;
            error = string.Empty;

            var a = (from x in _db.Addresses where x.Id == addressId select x).FirstOrDefault();
            if (a == null) return a;

            if (a.PafUPRN == address.PafUPRN && address.PafUPRN != null)
            {
                success = true;
                return a;
            }

            //if (a.AddressId != null)
            //{
            //    success = false;
            //    error = "Unable to update IBASE record";
            //    return a;
            //}

            try
            {
                success = true;

                bool modified = false;

                if (address.PafUPRN != null && address.PafUPRN != "")
                {
                    a.Building = address.Building;
                    modified = true;
                }

                if (address.PafUPRN != null && address.PafUPRN != "")
                {
                    a.BuildingNumber = address.BuildingNumber;
                    modified = true;
                }

                if (address.PafUPRN != null && address.PafUPRN != "")
                {
                    a.PostCode = address.PostCode;
                    modified = true;
                }

                if (address.PafUPRN != null && address.PafUPRN != "")
                {
                    a.Street = address.Street;
                    modified = true;
                }

                if (address.PafUPRN != null && address.PafUPRN != "")
                {
                    a.County = address.County;
                    modified = true;
                }

                if (address.PafUPRN != null && address.PafUPRN != "")
                {
                    a.Locality = address.Locality;
                    modified = true;
                }

                if (address.PafUPRN != null && address.PafUPRN != "")
                {
                    a.SubBuilding = address.SubBuilding;
                    modified = true;
                }

                if (address.PafUPRN != null && address.PafUPRN != "")
                {
                    a.Town = address.Town;
                    modified = true;
                }

                if (address.PafUPRN != null && address.PafUPRN != "")
                {
                    a.PafValidation = address.PafValidation;
                    a.PafUPRN = address.PafUPRN;
                    modified = true;
                }


                if (success && modified)
                {
                    a.ModifiedDate = DateTime.Now;
                    a.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
                else
                {
                    a = (from x in _db.Addresses where x.Id == addressId select x).FirstOrDefault();
                }
            }
            catch (Exception)
            {
                success = false;
            }

            return a;

        }


        public bool AddressEmpty(PipelineAddress address)
        {
            bool test = false;

            test =  (
                     string.IsNullOrEmpty(address.Building) &&
                     string.IsNullOrEmpty(address.BuildingNumber) &&
                     string.IsNullOrEmpty(address.County) &&
                     string.IsNullOrEmpty(address.Locality) &&
                     string.IsNullOrEmpty(address.PostCode) &&
                     string.IsNullOrEmpty(address.Street) &&
                     string.IsNullOrEmpty(address.SubBuilding) &&
                     string.IsNullOrEmpty(address.Town)
                   );

            return test;
        }


        public Address InsertAddress(PipelineAddress address)
        {
            Address a = null;
            
                a = _db.Addresses.Create();

                a.AddressId = address.AddressId;
                a.SubBuilding = address.SubBuilding;
                a.Building = address.Building;
                a.BuildingNumber = address.BuildingNumber;
                a.Street = address.Street;
                a.Locality = address.Locality;
                a.Town = address.Town;
                a.County = address.County;
                a.PostCode = address.PostCode;
                a.DxNumber = address.DxNumber;
                a.DxExchange = address.DxExchange;
                a.GridX = address.GridX;
                a.GridY = address.GridY;
                a.PafValidation = address.PafValidation;
                a.PafUPRN = address.PafUPRN;
                a.DocumentLink = address.DocumentLink;
                a.KeyAttractor = address.KeyAttractor;
                a.PropertyType = address.PropertyType;
                a.MosaicCode = address.MosaicCode;
                a.Source = address.Source;
                a.SourceReference = address.SourceReference;
                a.SourceDescription = address.SourceDescription;
                a.CreatedBy = address.CreatedBy;
                a.CreatedDate = address.CreatedDate;
                a.ModifiedBy = address.ModifiedBy;
                a.ModifiedDate = address.ModifiedDate;
                a.AddressType_Id = address.AddressType_Id;
                a.IBaseId = address.IBaseId;
                a.RecordStatus = address.RecordStatus;
                a.ADARecordStatus = address.ADARecordStatus;

                
                _db.Addresses.Add(a);
                _db.SaveChanges();
          
            return a;
        }
#if false
        #region Backup
        //public int Address_NumberOfCases(int addressId, string matchType, /*string caseSource,*/ bool? isPotentialClaimant, string periodSkip, string periodCount, int? personId, string cacheKey, MessageCache messageCache, bool _trace)
        //{
        //    int count = 0;

        //    bool LookForConfirmed = string.Compare(matchType, "confirmed", true) == 0;
        //    bool LookForUnConfirmed = string.Compare(matchType, "unconfirmed", true) == 0;
        //    //bool LookForTentative = string.Compare(matchType, "tentative", true) == 0;

        //    //PropertyDateRange dateRange = new PropertyDateRange(periodSkip, periodCount);

        //    //DateTime enddate = dateRange.EndDate; //DateTime.Now.AddMonths(-monthSkip).Date;
        //   // DateTime startdate = dateRange.StartDate; //enddate.AddMonths(-monthCount).Date;

        //    IQueryable<int> people = null;

        //    if (LookForConfirmed)
        //    {
        //        people = from p2a in _db.Person2Address.AsNoTracking()
        //                 where
        //                     p2a.Address_Id == addressId
        //                     && p2a.ADARecordStatus == (byte)ADARecordStatus.Current
        //                     && p2a.LinkConfidence == (int)LinkConfidence.Confirmed
        //                 select p2a.Person_Id;
        //    }
        //    else if (LookForUnConfirmed)
        //    {
        //        people = from p2a in _db.Person2Address.AsNoTracking()
        //                 where
        //                    p2a.Address_Id == addressId
        //                    && p2a.ADARecordStatus == (byte)ADARecordStatus.Current
        //                    && p2a.LinkConfidence == (int)LinkConfidence.Unconfirmed
        //                 select p2a.Person_Id;
        //    }


        //    foreach (int pid in people)
        //    {
        //        if (pid != personId)
        //        {
        //            ListOfInts aliases = FindAllOrganisationAliasesFromDb(pid, true, false, false, _trace);   // Only confirmed people

        //            EntityAliases personAliases = new EntityAliases("confirmed", aliases, null, null);

        //            var x = Person_NumberOfCases(pid, personAliases, /*caseSource,*/ isPotentialClaimant, periodSkip, periodCount, _trace);

        //            count += (x != null) ? x.Count() : 0;
        //        }
        //    }

        //    return count;
        //}
        #endregion 

        public List<Address_NumberOfCases_Result> Address_NumberOfCases(int addressId, int riskClaimId, string linkType, bool? isPotentialClaimant, DateTime incidentDate, ListOfInts peopleAliasIds, string incidentType, string claimType, string periodSkip, string periodCount, string caseLinkType, string cacheKey, MessageCache messageCache, bool _trace)
        {
                     
            #region Build list of acceptable p2a address links
            List<int> addressLinkType = new List<int>();

            linkType = linkType.ToLower();

            if (linkType == "current")
            {
                addressLinkType.Add(3); // Current Address
                addressLinkType.Add(10);// Lives At
                addressLinkType.Add(11);// Owner
            }
            else if (linkType == "notcurrent")
            {

                addressLinkType = (from alt in _db.AddressLinkTypes
                                   where alt.Id != 3
                                   && alt.Id != 10
                                   && alt.Id != 11
                                   select alt.Id).ToList();
            }
            else
	        {
                addressLinkType = (from alt in _db.AddressLinkTypes
                                   select alt.Id).ToList();
	        }
            #endregion

            // Get people attached to THIS address who are not on this claim/incident or an alias of a person on this claim/incident
            List<int> peopleNotOnClaim = (from p2a in _db.Person2Address
                                         where p2a.Address_Id == addressId
                                         && addressLinkType.Contains(p2a.AddressLinkType_Id)  // Check the linktype between the person and the address
                                         && p2a.RiskClaim_Id != riskClaimId
                                         && !peopleAliasIds.Contains(p2a.Person_Id)
                                         select p2a.Person_Id).ToList();

            #region Set start and end dates

            DateTime enddate; 
            DateTime startdate; 

            PropertyDateRange dateRange = new PropertyDateRange(incidentDate, periodSkip, periodCount);

            enddate = dateRange.EndDate; 
            startdate = dateRange.StartDate;

            #endregion

            #region Build list of incident types

            int it = 99;
            List<int> incidentTypes = new List<int>();

            if (string.IsNullOrEmpty(incidentType))
            {
                incidentTypes = (from x in _db.IncidentTypes select x.Id).ToList();
            }
            else if (incidentType.StartsWith("-"))
            {
                //incidentType = incidentType.Replace("-","");

                it = Math.Abs(Convert.ToInt32(incidentType));

                incidentTypes = (from x in _db.IncidentTypes
                                 where x.Id != it
                                 select x.Id).ToList();

            }
            else
            {
                it = Convert.ToInt32(incidentType);
                incidentTypes.Add(it);
            }

            #endregion

            #region Build list of claim types

            int ct = 99;
            List<int> claimTypes = new List<int>();

            if (string.IsNullOrEmpty(claimType))
            {
                claimTypes = (from x in _db.ClaimTypes select x.Id).ToList();
            }
            else if (claimType.StartsWith("-"))
            {
                //claimType = claimType.Replace("-", "");
                ct = Math.Abs(Convert.ToInt32(claimType));

                claimTypes = (from x in _db.ClaimTypes
                              where x.Id != ct
                              select x.Id).ToList();

            }
            else
            {
                ct = Convert.ToInt32(claimType);
                claimTypes.Add(ct);
            }

            #endregion

            int keoghsCase2IncidentLinkType = 1;

            if (caseLinkType == "Info Only")
            {
                keoghsCase2IncidentLinkType = 2;
            }

            if (peopleNotOnClaim.Count() > 0)
            {

                List<int> i = new List<int>();

                if (isPotentialClaimant != null)
                {
                    i.Add(Convert.ToInt32(isPotentialClaimant));                  
                }
                else
                {
                    i.Add(0);
                    i.Add(1);
                }

                // Get cases for all people not on this claim
                var cases = (from i2p in _db.Incident2Person
                             join kc2i in _db.KeoghsCase2Incident on i2p.Incident_Id equals kc2i.Incident_Id
                             join pc in _db.PotentialClaimants on new { i2p.PartyType_Id, i2p.SubPartyType_Id } equals new { PartyType_Id = pc.PartyType, SubPartyType_Id = pc.SubPartyType }
                             where peopleNotOnClaim.Contains(i2p.Person_Id)
                             && i2p.Incident.IncidentDate >= startdate && i2p.Incident.IncidentDate <= enddate
                             && claimTypes.Contains(kc2i.Incident.ClaimType_Id)
                             && incidentTypes.Contains(kc2i.Incident.IncidentType_Id) 
                             && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                             && i2p.Incident.ADARecordStatus == (byte)ADARecordStatus.Current
                             && kc2i.ADARecordStatus == (byte)ADARecordStatus.Current
                             && kc2i.KeoghsCase.ADARecordStatus == (byte)ADARecordStatus.Current
                             && kc2i.CaseIncidentLinkType_Id == keoghsCase2IncidentLinkType
                             && i.Contains(pc.Flag) //|| i == 3
                             select new Address_NumberOfCases_Result
                             {
                                 DateOfBirth = i2p.Person.DateOfBirth,
                                 EliteReference = kc2i.KeoghsCase.KeoghsEliteReference,
                                 FirstName = i2p.Person.FirstName,
                                 FiveGrading = kc2i.FiveGrading,
                                 IncidentDate = i2p.Incident.IncidentDate,
                                 LastName = i2p.Person.LastName,
                                 PartyTypeText = i2p.PartyType.PartyTypeText,
                                 SubPartyText = i2p.SubPartyType.SubPartyText,
                                 LinkedClaimType = i2p.Incident.ClaimType.ClaimType1,
                                 Insurer = i2p.Insurer,
                                 InsurerClaimRef = i2p.ClaimNumber
                             }).Distinct().ToList();


                return cases;

            }

            return new List<Address_NumberOfCases_Result>();

        }

        public List<Address_NumberOfIncidents_Result> Address_NumberOfIncidents(int addressId, int riskClaimId, string linkType, DateTime incidentDate, ListOfInts peopleAliasIds, string incidentType, string claimType, string periodSkip, string periodCount, string cacheKey, MessageCache messageCache, bool _trace)
        {

            #region Build list of acceptable p2a address links
            List<int> addressLinkType = new List<int>();

            linkType = linkType.ToLower();

            if (linkType == "current")
            {
                addressLinkType.Add(3); // Current Address
                addressLinkType.Add(10);// Lives At
                addressLinkType.Add(11);// Owner
            }
            else if (linkType == "notcurrent")
            {

                addressLinkType = (from alt in _db.AddressLinkTypes
                                   where alt.Id != 3
                                   && alt.Id != 10
                                   && alt.Id != 11
                                   select alt.Id).ToList();
            }
            else
            {
                addressLinkType = (from alt in _db.AddressLinkTypes
                                   select alt.Id).ToList();
            }
            #endregion

            // Get people attached to THIS address who are not on this claim/incident or an alias of a person on this claim/incident
            List<int> peopleNotOnClaim = (from p2a in _db.Person2Address
                                          where p2a.Address_Id == addressId
                                          && addressLinkType.Contains(p2a.AddressLinkType_Id)  // Check the linktype between the person and the address
                                          && p2a.RiskClaim_Id != riskClaimId
                                          && !peopleAliasIds.Contains(p2a.Person_Id)
                                          select p2a.Person_Id).ToList();

            #region Set start and end dates

            DateTime enddate;
            DateTime startdate;

            PropertyDateRange dateRange = new PropertyDateRange(incidentDate, periodSkip, periodCount);

            enddate = dateRange.EndDate;
            startdate = dateRange.StartDate;

            #endregion

            #region Build list of incident types

            int it = 99;
            List<int> incidentTypes = new List<int>();

            if (string.IsNullOrEmpty(incidentType))
            {
                incidentTypes = (from x in _db.IncidentTypes select x.Id).ToList();
            }
            else if (incidentType.StartsWith("-"))
            {
                //incidentType = incidentType.Replace("-","");

                it = Math.Abs(Convert.ToInt32(incidentType));

                incidentTypes = (from x in _db.IncidentTypes
                                 where x.Id != it
                                 select x.Id).ToList();

            }
            else
            {
                it = Convert.ToInt32(incidentType);
                incidentTypes.Add(it);
            }

            #endregion

            #region Build list of claim types

            int ct = 99;
            List<int> claimTypes = new List<int>();

            if (string.IsNullOrEmpty(claimType))
            {
                claimTypes = (from x in _db.ClaimTypes select x.Id).ToList();
            }
            else if (claimType.StartsWith("-"))
            {
                //claimType = claimType.Replace("-", "");
                ct = Math.Abs(Convert.ToInt32(claimType));

                claimTypes = (from x in _db.ClaimTypes
                              where x.Id != ct
                              select x.Id).ToList();

            }
            else
            {
                ct = Convert.ToInt32(claimType);
                claimTypes.Add(ct);
            }

            #endregion

            if (peopleNotOnClaim.Count() > 0)
            {

                // Get cases for all people not on this claim
                var cases = (from i2p in _db.Incident2Person
                             join p2a in _db.Person2Address on i2p.Person_Id equals p2a.Person_Id 
                             where peopleNotOnClaim.Contains(i2p.Person_Id)
                             && p2a.Address_Id == addressId
                             && i2p.Incident.IncidentDate >= startdate && i2p.Incident.IncidentDate <= enddate
                             && claimTypes.Contains(i2p.Incident.ClaimType_Id)
                             && incidentTypes.Contains(i2p.Incident.IncidentType_Id)
                             && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                             && i2p.Incident.ADARecordStatus == (byte)ADARecordStatus.Current
                             select new Address_NumberOfIncidents_Result
                             {
                                 Id = i2p.Incident.Id,
                                 DateOfBirth = i2p.Person.DateOfBirth,
                                 FirstName = i2p.Person.FirstName,
                                 IncidentDate = i2p.Incident.IncidentDate,
                                 LastName = i2p.Person.LastName,
                                 PartyTypeText = i2p.PartyType.PartyTypeText,
                                 SubPartyText = i2p.SubPartyType.SubPartyText,
                                 LinkedClaimType = i2p.Incident.ClaimType.ClaimType1,
                                 Insurer = i2p.Insurer,
                                 InsurerClaimRef = i2p.ClaimNumber,
                                 RawAddress = p2a.RawAddress,
                             }).Distinct().ToList();


                return cases;

            }

            return new List<Address_NumberOfIncidents_Result >();

        }

        public string Address_PostCodeKeyAttractor(int addressId)
        {
            var keyStrings = from x in _db.Address2Address
                             where x.Address1_Id == addressId
                             && x.RecordStatus == (byte)ADARecordStatus.Current
                             select new
                             {
                                 KeyString = x.Address.KeyAttractor,
                                 PostCode = x.Address.PostCode
                             };

            StringBuilder s = new StringBuilder();

            string comma = "";

            foreach (var k in keyStrings)
            {
                s.Append(string.Format("{0} [{1}] {2}", comma, k.PostCode, k.KeyString));
                comma = ", ";
            }

            return s.ToString();
        }

        public bool Address_IsPostCodeKeyAttractor(int addressId)
        {
            return (from x in _db.Address2Address
                    where x.Address1_Id == addressId
                    && x.RecordStatus == (byte)ADARecordStatus.Current
                    && x.Address.KeyAttractor != null && x.Address.KeyAttractor.Length > 0
                    select x).Count() > 0;

        }

        public List<Address_IsKeyAttractor_Result> Address_IsKeyAttractor(int addressId)
        {

            return (from x in _db.Addresses
                    where x.Id == addressId
                    && x.RecordStatus == (byte)ADARecordStatus.Current
                    && x.KeyAttractor != null && x.KeyAttractor.Length > 0
                    select new Address_IsKeyAttractor_Result()
                    {
                        //Address = x.BuildingNumber + " " + x.SubBuilding + " " + x.Building + " " + x.Street + " " + x.Locality + " " + x.Town + " " + x.County + " " + x.PostCode,
                        Address = x,
                        KeyAttractor = x.KeyAttractor
                    }).ToList();

        }
#endif
    }
}
