﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllVehicle2PersonAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Vehicle2Person set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectVehicle2PersonLinks(int masterVehicleId, int vehicleId)
        {
            _db.Database.ExecuteSqlCommand("update Vehicle2Person set Vehicle_Id = " + masterVehicleId +
                                           " where Vehicle_Id = " + vehicleId);
        }

        public void RedirectPerson2VehicleLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Vehicle2Person set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        public List<Vehicle2Person> GetVehicle2PersonLinks(int vehicleId, int personId, int baseRiskClaimId)
        {
            return (from x in _db.Vehicle2Person
                    where x.Person_Id == personId && x.Vehicle_Id == vehicleId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Vehicle2Person GetLatestVehicle2PersonLink(int vehicleId, int personId, int baseRiskClaimId)
        {
            return (from x in _db.Vehicle2Person
                    where x.Person_Id == personId && x.Vehicle_Id == vehicleId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpVehicle2PersonLinks(/*Vehicle2Person latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Vehicle2Person
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Vehicle2Person Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Vehicle2Person UpdateVehicle2PersonLink(MDA.Common.Enum.Operation op, Vehicle2Person link,
                                            MDA.Pipeline.Model.PipelineVehicle2PersonLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE
                    if (link.VehicleLinkType_Id != 0 && link.VehicleLinkType_Id != (int)pipeLink.VehicleLinkType_Id) // we have value and it's different
                    {
                        success = false;
                    }

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.VehicleLinkType_Id != (int)pipeLink.VehicleLinkType_Id)
                            link.VehicleLinkType_Id = (int)pipeLink.VehicleLinkType_Id;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;

        }


        public Vehicle2Person UpdateVehicle2PersonLink(MDA.Common.Enum.Operation op, int linkId,
                                            //MDA.Common.Enum.VehicleLinkType linkType, 
                                            MDA.Pipeline.Model.PipelineVehicle2PersonLink pipeLink,
                                            RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Vehicle2Person where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateVehicle2PersonLink(op, link, pipeLink, claim, ref success);
        }

        //public Vehicle2Person UpdateOrgVehicle2PersonLink(MDA.Common.Enum.Operation op, Vehicle2Person link,
        //                                //MDA.Pipeline.Model.PipelineOrganisationVehicle vehicle, MDA.Pipeline.Model.PipelineOrganisation organisation,
        //                                MDA.Pipeline.Model.PipelineOrgVehicle2PersonLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        //{
        //    success = true;

        //    try
        //    {
        //        if (op == MDA.Common.Enum.Operation.Delete)
        //        {
        //            //_db.Incident2Vehicle.Remove(link);

        //            link.RecordStatus = Constants.IBaseDeleteValue;
        //            link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
        //        }
        //        else if (op == MDA.Common.Enum.Operation.Withdrawn)
        //        {
        //            link.RecordStatus = Constants.IBaseDeleteValue;
        //            link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
        //        }
        //        else
        //        {

        //            // check for invalid updates and if so set success = false : NONE
        //            if (!string.IsNullOrEmpty(link.HireCompany) && string.Compare(link.HireCompany, pipeLink.OrganisationName, true) != 0) // we have value and it's different
        //            {
        //                success = false;
        //            }

        //            if (link.HireStartDate != null && link.HireStartDate != pipeLink.HireStartDate)
        //            {
        //                success = false;
        //            }

        //            if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
        //            {
        //                link.RecordStatus = Constants.IBaseDeleteValue;
        //                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
        //            }
        //            else
        //            {
        //                link.RiskClaim_Id = newRiskClaim.Id;

        //                if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
        //                    link.LinkConfidence = (int)pipeLink.LinkConfidence;

        //                if (string.Compare(link.HireCompany, pipeLink.OrganisationName, true) != 0)
        //                    link.HireCompany = pipeLink.OrganisationName;

        //                if (link.HireStartDate != pipeLink.HireStartDate)
        //                    link.HireStartDate = pipeLink.HireStartDate;

        //                if (link.HireEndDate != pipeLink.HireEndDate)
        //                    link.HireEndDate = pipeLink.HireEndDate;
        //            }

        //        }

        //        link.ModifiedDate = DateTime.Now;
        //        link.ModifiedBy = _ctx.Who;

        //        _db.SaveChanges();
        //    }
        //    catch (Exception)
        //    {
        //        success = false;
        //    }

        //    return link;
        //}


        //public Vehicle2Person UpdateOrgVehicle2PersonLink(MDA.Common.Enum.Operation op, int linkId,
        //                                    //MDA.Pipeline.Model.PipelineOrganisationVehicle vehicle, MDA.Pipeline.Model.PipelineOrganisation organisation,
        //                                    MDA.Pipeline.Model.PipelineOrgVehicle2PersonLink pipeLink, RiskClaim claim, ref bool success)
        //{
        //    success = false;

        //    var link = (from x in _db.Vehicle2Person where x.Id == linkId select x).FirstOrDefault();

        //    if (link == null) return link;

        //    return UpdateOrgVehicle2PersonLink(op, link, pipeLink, claim, ref success);
        //}

        public Vehicle2Person InsertVehicle2PersonLink(int vehicleId, int personId, 
                        MDA.Pipeline.Model.PipelineVehicle2PersonLink pipeLink, RiskClaim claim)
        {
            Vehicle2Person v2p = _db.Vehicle2Person.Create();

            v2p.CreatedBy         = pipeLink.CreatedBy;
            v2p.CreatedDate       = pipeLink.CreatedDate;
            v2p.ModifiedBy        = pipeLink.ModifiedBy;
            v2p.ModifiedDate      = pipeLink.ModifiedDate;
            v2p.Source            = pipeLink.Source;
            v2p.SourceDescription = pipeLink.SourceDescription;
            v2p.SourceReference   = pipeLink.SourceReference;
            v2p.ADARecordStatus   = pipeLink.ADARecordStatus;
            v2p.RecordStatus      = pipeLink.RecordStatus;
            v2p.LinkConfidence    = pipeLink.LinkConfidence;

            v2p.HireStartDate     = pipeLink.HireStartDate;
            v2p.HireEndDate       = pipeLink.HireEndDate;
            v2p.HireCompany       = pipeLink.HireCompany;

            v2p.RiskClaim_Id = claim.Id;
            v2p.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            v2p.Person_Id = personId;
            v2p.Vehicle_Id = vehicleId;
            v2p.VehicleLinkType_Id = (int)pipeLink.VehicleLinkType_Id;

            v2p.FiveGrading = GetFiveGrading();

            _db.Vehicle2Person.Add(v2p);

            _db.SaveChanges();

            return v2p;
        }

        //public Vehicle2Person InsertOrgVehicle2PersonLink(int personId, int vehicleId, 
        //                    //MDA.Pipeline.Model.PipelineOrganisationVehicle vehicle, MDA.Pipeline.Model.PipelineOrganisation organisation, 
        //                    MDA.Pipeline.Model.PipelineOrgVehicle2PersonLink pipeLink, RiskClaim claim)
        //{
        //    Vehicle2Person v2p = _db.Vehicle2Person.Create();

        //    v2p.CreatedBy         = pipeLink.CreatedBy;
        //    v2p.CreatedDate       = pipeLink.CreatedDate;
        //    v2p.ModifiedBy        = pipeLink.ModifiedBy;
        //    v2p.ModifiedDate      = pipeLink.ModifiedDate;
        //    v2p.Source            = pipeLink.Source;
        //    v2p.SourceDescription = pipeLink.SourceDescription;
        //    v2p.SourceReference   = pipeLink.SourceReference;
        //    v2p.ADARecordStatus   = pipeLink.ADARecordStatus;
        //    v2p.RecordStatus      = pipeLink.RecordStatus;
        //    v2p.LinkConfidence    = pipeLink.LinkConfidence;

        //    v2p.RiskClaim_Id = claim.Id;
        //    v2p.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;
            
        //    v2p.Person_Id = personId;
        //    v2p.Vehicle_Id = vehicleId;
        //    v2p.VehicleLinkType_Id = (int)MDA.Common.Enum.VehicleLinkType.Hirer;

        //    v2p.HireCompany = pipeLink.OrganisationName;
        //    v2p.HireStartDate = pipeLink.HireStartDate;
        //    v2p.HireEndDate = pipeLink.HireEndDate;

        //    v2p.FiveGrading = GetFiveGrading();

        //    _db.Vehicle2Person.Add(v2p);

        //    _db.SaveChanges();

        //    return v2p;
        //}

        //public Vehicle2Person InsertVehicle2PersonLink(Vehicle2Person vehicle2person)
        //{
        //    Vehicle2Person v2p = _db.Vehicle2Person.Create();

        //    v2p.CreatedBy = vehicle2person.CreatedBy;
        //    v2p.CreatedDate = DateTime.Now;
        //    v2p.BaseRiskClaim_Id = vehicle2person.BaseRiskClaim_Id;

        //    v2p.CrossHireCompany = vehicle2person.CrossHireCompany;
        //    v2p.HireCompany = vehicle2person.HireCompany;
        //    v2p.HireEndDate = vehicle2person.HireEndDate;
        //    v2p.HireStartDate = vehicle2person.HireStartDate;
        //    v2p.IBaseId = vehicle2person.IBaseId;
        //    v2p.Person_Id = vehicle2person.Person_Id;
        //    v2p.RegKeeperEndDate = vehicle2person.RegKeeperEndDate;
        //    v2p.RegKeeperStartDate = vehicle2person.RegKeeperStartDate;
        //    v2p.RiskClaim_Id = vehicle2person.RiskClaim_Id;
        //    v2p.Source = vehicle2person.Source;
        //    v2p.SourceDescription = vehicle2person.SourceDescription;
        //    v2p.SourceReference = vehicle2person.SourceReference;
        //    v2p.Vehicle_Id = vehicle2person.Vehicle_Id;
        //    v2p.VehicleLinkId = vehicle2person.VehicleLinkId;
        //    v2p.VehicleLinkType_Id = vehicle2person.VehicleLinkType_Id;
        //    v2p.LinkConfidence = vehicle2person.LinkConfidence;
        //    v2p.FiveGrading = vehicle2person.FiveGrading;

        //    _db.Vehicle2Person.Add(v2p);

        //    _db.SaveChanges();

        //    return v2p;
        //}
    }

}
