﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using RiskEngine.Model;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllIncidentAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Incident set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public Incident SelectIncident(int id)
        {
            return (from x in _db.Incidents where x.Id == id select x).FirstOrDefault();
        }

        public List<int> VerifyIncidentListExists(List<int> listId)
        {
            List<int> outList = new List<int>();

            foreach (int id in listId)
            {
                if (_db.Incidents.Any(x => x.Id == id))
                    outList.Add(id);
            }
            return outList;
        }

        public IEnumerable GetAllIncidents(DateTime? incidentDate)
        {
            DateTime t = DateTime.MinValue;
            
            if (incidentDate != null)
            {
                t = new DateTime(incidentDate.Value.Year, incidentDate.Value.Month, incidentDate.Value.Day, incidentDate.Value.Hour, incidentDate.Value.Minute, 0);
               
            }
            return (from x in _db.Incidents where x.IncidentDate == t select x).ToList();
        }

        /// <summary>
        /// Make all links that point to incidents in the match list point to the new master
        /// </summary>
        /// <param name="masterIncidentId"></param>
        /// <param name="matchList"></param>
        public void DeduplicateIncidentList(int masterIncidentId , List<int> matchList)
        {
            foreach (var i in matchList)
            {
                var ir = SelectIncident(i);

                if (ir != null)
                {
                    RedirectIncident2AddressLinks(masterIncidentId, i);
                    RedirectIncident2FraudRingLinks(masterIncidentId, i);
                    RedirectIncident2IncidentLinks(masterIncidentId, i);
                    RedirectIncident2OrganisationLinks(masterIncidentId, i);
                    RedirectIncident2OrganisationOutcomeLinks(masterIncidentId, i);
                    RedirectIncident2PersonLinks(masterIncidentId, i);
                    RedirectIncident2PersonOutcomeLinks(masterIncidentId, i);
                    RedirectIncident2PolicyLinks(masterIncidentId, i);
                    RedirectIncident2VehicleLinks(masterIncidentId, i);

                    ir.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                    ir.RecordStatus = Constants.IBaseDeleteValue;

                    _db.SaveChanges();
                }
            }
         }

        public List<int> GetAllSyncIncidentIds()
        {
            return (from x in _db.Incidents where x.ADARecordStatus >= 10 select x.Id).ToList();
        }

        public void ResetSyncIncidentAdaStatus(int id)
        {
            var r = (from x in _db.Incidents where x.Id == id select x).FirstOrDefault();

            if (r != null && r.ADARecordStatus >= 10)
            {
                r.ADARecordStatus -= 10;

                _db.SaveChanges();
            }
        }

        /// <summary>
        /// Mark Incident as Update Deleted
        /// </summary>
        /// <param name="incidentId"></param>
        /// <param name="baseRiskClaimId"></param>
        public void SafelyRemoveIncident(int incidentId, int baseRiskClaimId)
        {
            var cnt = 0;

            cnt += _db.Incident2Address.Count(g => g.Incident_Id == incidentId && g.ADARecordStatus == (int)ADARecordStatus.Current && g.BaseRiskClaim_Id != baseRiskClaimId);
            cnt += _db.Incident2FraudRing.Count(g => g.Incident_Id == incidentId && g.ADARecordStatus == (int)ADARecordStatus.Current && g.BaseRiskClaim_Id != baseRiskClaimId);
            cnt += _db.Incident2Incident.Count(g => (g.Incident1_Id == incidentId || g.Incident2_Id == incidentId) && g.ADARecordStatus == (int)ADARecordStatus.Current);
            cnt += _db.Incident2Organisation.Count(g => g.Incident_Id == incidentId && g.ADARecordStatus == (int)ADARecordStatus.Current && g.BaseRiskClaim_Id != baseRiskClaimId);
            cnt += _db.Incident2OrganisationOutcome.Count(g => g.Incident_Id == incidentId && g.ADARecordStatus == (int)ADARecordStatus.Current && g.BaseRiskClaim_Id != baseRiskClaimId);
            cnt += _db.Incident2Person.Count(g => g.Incident_Id == incidentId && g.ADARecordStatus == (int)ADARecordStatus.Current && g.BaseRiskClaim_Id != baseRiskClaimId);
            cnt += _db.Incident2PersonOutcome.Count(g => g.Incident_Id == incidentId && g.ADARecordStatus == (int)ADARecordStatus.Current && g.BaseRiskClaim_Id != baseRiskClaimId);
            cnt += _db.Incident2Policy.Count(g => g.Incident_Id == incidentId && g.ADARecordStatus == (int)ADARecordStatus.Current && g.BaseRiskClaim_Id != baseRiskClaimId);
            cnt += _db.Incident2Vehicle.Count(g => g.Incident_Id == incidentId && g.ADARecordStatus == (int)ADARecordStatus.Current && g.BaseRiskClaim_Id != baseRiskClaimId);

            if (cnt == 0)
            {
                var i = SelectIncident(incidentId);

                if (i != null)
                {
                    i.ADARecordStatus = (int) ADARecordStatus.UpdateRemoved;
                    i.RecordStatus = Constants.IBaseDeleteValue;

                    _db.SaveChanges();
                }
            }
        }

        public Incident UpdateIncident(int incidentId, MDA.Pipeline.Model.IPipelineClaim xmlClaim, out bool success)
        {
            success = false;

            var a = (from x in _db.Incidents where x.Id == incidentId select x).FirstOrDefault();

            if (a == null) return a;

            try
            {
                success = true;

                bool modified = false;

                //if (a.IncidentType_Id != (int)claim.ClaimType)
                //{
                //    a.IncidentType_Id = (int)claim.ClaimType;
                //    modified = true;
                //}

                DateTime? newDate = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(xmlClaim.IncidentDate);

                if (a.IncidentDate != newDate)
                {
                    a.IncidentDate = (DateTime)newDate;
                    modified = true;
                }

                //if (a.PaymentsToDate != xmlClaim.PaymentsToDate)
                //{
                //    a.PaymentsToDate = xmlClaim.PaymentsToDate;
                //    modified = true;
                //}

                //if (a.Reserve != xmlClaim.Reserve)
                //{
                //    a.Reserve = xmlClaim.Reserve;
                //    modified = true;
                //}

                if (modified)
                {
                    a.ModifiedDate = DateTime.Now;
                    a.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
            }
            catch (Exception)
            {
                success = false;
            }

            return a;
        }

        public Incident InsertIncident(MDA.Pipeline.Model.IPipelineClaim claim)
        {
            Incident i = _db.Incidents.Create();

            i.ADARecordStatus      = claim.ADARecordStatus;
            i.CreatedBy            = claim.CreatedBy;
            i.CreatedDate          = claim.CreatedDate;
            //i.ClaimType_Id         = claim.ClaimType_Id;
            i.FraudRingName        = claim.FraudRingName;
            i.IBaseId              = claim.IBaseId;
            i.IfbReference         = claim.IfbReference;
            i.IncidentDate         = claim.IncidentDate;
            i.IncidentId           = claim.IncidentId;
            i.IncidentType_Id      = (int)claim.ClaimType_Id;
            i.KeoghsEliteReference = claim.KeoghsEliteReference;
            i.KeyAttractor         = claim.KeyAttractor;
            i.ModifiedBy           = claim.ModifiedBy;
            i.ModifiedDate         = claim.ModifiedDate;
            //i.PaymentsToDate       = claim.PaymentsToDate;
            //i.Reserve              = claim.Reserve;
            i.RecordStatus         = claim.RecordStatus;
            i.Source               = claim.Source;
            i.SourceDescription    = claim.SourceDescription;
            i.SourceReference      = claim.SourceReference;

            _db.Incidents.Add(i);

            _db.SaveChanges();

            return i;
        }

        //public Incident InsertIncident(Incident incident)
        //{
        //    Incident i = _db.Incidents.Create();

        //    i.ADARecordStatus      = incident.ADARecordStatus;
        //    i.CreatedBy            = incident.CreatedBy;
        //    i.CreatedDate          = incident.CreatedDate;
        //    i.ClaimType_Id         = incident.ClaimType_Id;
        //    i.FraudRingName        = incident.FraudRingName;
        //    i.IBaseId              = incident.IBaseId;
        //    i.IfbReference         = incident.IfbReference;
        //    i.IncidentDate         = incident.IncidentDate;
        //    i.IncidentId           = incident.IncidentId;
        //    i.IncidentType_Id      = incident.IncidentType_Id;
        //    i.KeoghsEliteReference = incident.KeoghsEliteReference;
        //    i.KeyAttractor         = incident.KeyAttractor;
        //    i.ModifiedBy           = incident.ModifiedBy;
        //    i.ModifiedDate         = incident.ModifiedDate;
        //    i.PaymentsToDate       = incident.PaymentsToDate;
        //    i.Reserve              = incident.Reserve;
        //    i.RecordStatus         = incident.RecordStatus; 
        //    i.Source               = incident.Source;
        //    i.SourceDescription    = incident.SourceDescription;
        //    i.SourceReference      = incident.SourceReference;

        //    _db.Incidents.Add(i);

        //    _db.SaveChanges();

        //    return i;
        //}
#if false
        public int Incident_NumberOfClaimants(int incidentId, int riskClaimId)
        {

            int count = 0;

            var people = (from i2p in _db.Incident2Person 
                          where i2p.Incident_Id == incidentId
                          && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                          && i2p.RiskClaim_Id == riskClaimId
                          select i2p).Distinct();

            if (people == null) return 0;

            foreach (var p in people)
            {
                int partyType = p.PartyType_Id;
                int subPartyType = p.SubPartyType_Id;

                var claimantTypes = _db.uspGetPotentialClaimant(partyType, subPartyType);

                foreach (var pc in claimantTypes)
                {
                    if (pc.Flag == 1)
                    {
                        count++;
                    }
                }

            }

            return count;
        }

        public int Incident_NumberOfPolicies(int incidentId)
        {

            return (from i2p in _db.Incident2Policy
                    join i in _db.Incidents on i2p.Incident_Id equals i.Id
                    where i2p.Incident_Id == incidentId
                    && i.IncidentType_Id == 8
                    && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                    select i2p).Distinct().Count();
        }


        public bool Incident_AnyVehiclesWithMultipleOccupancy(int incidentId, int riskClaimId ,int limit, string cacheKey, MessageCache messageCache, bool _trace)
        {

            bool result = false;

            List<int> claimantTypes = MDA.Common.Helpers.LinqHelper.GetVehicleOccupancySubPartyTypes();   // driver, passenger, unknown ONLY

            // Get all the vehicles on this incident
            var vehicles = (from x in _db.Incident2Vehicle
                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                            && x.RiskClaim_Id == riskClaimId
                            && x.Incident_Id == incidentId
                            && x.Incident2VehicleLinkType_Id != (int)MDA.Common.Enum.Incident2VehicleLinkType.NoVehicleInvolved
                            select x);

            if (vehicles == null) return false;

            foreach (var vehicle in vehicles)
            {
                result = (from x in _db.Vehicle2Person
                             join y in _db.Incident2Person on x.Person_Id equals y.Person_Id
                             where claimantTypes.Contains(y.SubPartyType_Id)
                             && x.ADARecordStatus == (byte)ADARecordStatus.Current 
                             && x.RiskClaim_Id == riskClaimId
                             && x.Vehicle_Id == vehicle.Vehicle_Id
                             && y.Incident_Id == incidentId
                             select x.Id).Distinct().Count() > limit;
                                        

                if (result)
                {
                    break;
                }
            }

            return result;

        }

        public int Incident_VehiclesOccupancyCount(int incidentId, int riskClaimId)
        {

            int result = 0;

            List<int> claimantTypes = MDA.Common.Helpers.LinqHelper.GetVehicleOccupancySubPartyTypes();   // driver, passenger, unknown ONLY

            // Get all the vehicles on this incident
            var vehicles = (from x in _db.Incident2Vehicle
                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                            && x.RiskClaim_Id == riskClaimId
                            && x.Incident_Id == incidentId
                            && x.Incident2VehicleLinkType_Id != (int)MDA.Common.Enum.Incident2VehicleLinkType.NoVehicleInvolved
                            select x);

            if (vehicles == null) return 0;

            foreach (var vehicle in vehicles)
            {
                result += (from x in _db.Vehicle2Person
                          join y in _db.Incident2Person on x.Person_Id equals y.Person_Id
                          where claimantTypes.Contains(y.SubPartyType_Id)
                          && x.ADARecordStatus == (byte)ADARecordStatus.Current
                          && x.RiskClaim_Id == riskClaimId
                          && x.Vehicle_Id == vehicle.Vehicle_Id
                          && y.Incident_Id == incidentId
                          select x.Id).Distinct().Count();
            }

            return result;

        }

        public int Incident_VehiclesPassengerCount(int incidentId, int riskClaimId)
        {

            int result = 0;


            // Get all the vehicles on this incident
            var vehicles = (from x in _db.Incident2Vehicle
                            where x.ADARecordStatus == (byte)ADARecordStatus.Current
                            && x.RiskClaim_Id == riskClaimId
                            && x.Incident_Id == incidentId
                            && x.Incident2VehicleLinkType_Id != (int)MDA.Common.Enum.Incident2VehicleLinkType.NoVehicleInvolved
                            select x);

            if (vehicles == null) return 0;

            foreach (var vehicle in vehicles)
            {
                result += (from x in _db.Vehicle2Person
                           join y in _db.Incident2Person on x.Person_Id equals y.Person_Id
                           where y.SubPartyType_Id == 2
                           && x.ADARecordStatus == (byte)ADARecordStatus.Current
                           && x.RiskClaim_Id == riskClaimId
                           && x.Vehicle_Id == vehicle.Vehicle_Id
                           && y.Incident_Id == incidentId
                           select x.Id).Distinct().Count();
            }

            return result;

        }

        public bool Incident_MultiplePoliciesFromSameAddress(int incidentId, int limit, string cacheKey, MessageCache messageCache, bool _trace)
        {
            bool result = false;
            
            int numberOfPolicies = 0;

            List<int> policyHolderTypes = MDA.Common.Helpers.LinqHelper.GetPolicyHolderPartyTypes(); 

            var queryResult = from i in _db.Incidents
                              join i2p in _db.Incident2Person on i.Id equals i2p.Incident_Id
                              join p2p in _db.Person2Policy on i2p.Person_Id equals p2p.Person_Id
                              join p2a in _db.Person2Address on p2p.Person_Id equals p2a.Person_Id
                              where policyHolderTypes.Contains(i2p.PartyType_Id)
                                    && i.Id == incidentId
                              group p2a by p2a.Address_Id into g
                              
                              select new
                                  {
                                      AddressID = g.Key,
                                      AddressCount = g.Count()
                                  };

            if (queryResult == null) return false;

            foreach (var item in queryResult)
            {
                numberOfPolicies = item.AddressCount;
            }


            if (numberOfPolicies >= limit)
                result = true;

            return result;
        }


        public int Incident_AgeRangeCount(int incidentId, int riskClaimId ,int ageFrom, int ageTo, string cacheKey, MessageCache messageCache, bool _trace)
        {
            var people = (from i2p in _db.Incident2Person
                          where i2p.Incident_Id == incidentId
                          && i2p.RiskClaim_Id == riskClaimId
                          && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                          select new 
                          {
                              i2p.Person.DateOfBirth,
                              i2p.Incident.IncidentDate
                          });

            if (people == null) return 0;

            int count = 0;
            foreach (var x in people)
            {
                if (x.DateOfBirth.HasValue && x.IncidentDate != null)
                {
                    if (LinkHelper.CalculateAge(x.DateOfBirth.Value.Date, x.IncidentDate.Date) >= ageFrom
                         && LinkHelper.CalculateAge(x.DateOfBirth.Value.Date, x.IncidentDate.Date) <= ageTo)
                    {
                        count++;
                    }
                }
            }

            messageCache.AddLowOutputMessage(cacheKey, null, null);
            messageCache.AddMediumOutputMessage(cacheKey, null, null);
            messageCache.AddHighOutputMessage(cacheKey, null, null);

            return count;
        }
#endif
        //public DateTime? Incident_IncidentTime(MdaDbContext db, int incidentId, int riskClaimId)
        //{
        //    var incidentDate = (from i2p in db.Incident2Person 
        //                        where i2p.RiskClaim_Id == riskClaimId && i2p.Id == incidentId 
        //                        select i2p.IncidentDateTime).FirstOrDefault();

        //    //var incidentDate = (from x in db.Incidents where x.Id == incidentId select x.IncidentDate).FirstOrDefault();

        //    //var incidentTime = (from p in db.People
        //    //                       join ip in db.Incident2Person on p.Id equals ip.Id
        //    //                       where
        //    //                       (new int[] { 3, 5 }).Contains(ip.PartyType_Id)
        //    //                       group ip by new
        //    //                       {
        //    //                        ip.IncidentDateTime,
        //    //                        ip.PartyType_Id
        //    //                       } into g
        //    //                       select new
        //    //                       {
        //    //                            IncidentTime =
        //    //                                g.Key.PartyType_Id == 5 ? g.Key.IncidentTime :
        //    //                                g.Key.PartyType_Id == 3 ? g.Key.IncidentTime : null
        //    //                       }).FirstOrDefault();

        //    //try
        //    //{
        //    //    if (incidentTime != null)
        //    //    {
        //    //        TimeSpan? ts = incidentTime.IncidentTime;

        //    //        return new DateTime(incidentDate.Year, incidentDate.Month, incidentDate.Day, ts.Value.Hours, ts.Value.Minutes, 0);
        //    //    }
        //    //}
        //    //catch (Exception)
        //    //{
        //    //}

        //    return incidentDate;
        //}

    }

}
