﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public Handset2Organisation GetLatestOrgHandset2OrganisationLink(int handsetId, int organisationId, int baseRiskClaimId)
        {
            return (from x in _db.Handset2Organisation
                    where x.Handset_Id == handsetId && x.Organisation_Id == organisationId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public Handset2Organisation InsertOrgHandset2OrganisationLink(int orgHandsetId, int organisationId,
                                    MDA.Pipeline.Model.PipelineOrgHandsetLink pipeLink,
            //MDA.Common.Enum.HandsetLinkType linkType, DateTime? hireStartDate, DateTime? hireEndDate, string hireCompany, 
                                    RiskClaim claim)
        {
            Handset2Organisation h2o = _db.Handset2Organisation.Create();

            h2o.CreatedBy = pipeLink.CreatedBy;
            h2o.CreatedDate = pipeLink.CreatedDate;
            h2o.ModifiedBy = pipeLink.ModifiedBy;
            h2o.ModifiedDate = pipeLink.ModifiedDate;
            h2o.Source = pipeLink.Source;
            h2o.SourceDescription = pipeLink.SourceDescription;
            h2o.SourceReference = pipeLink.SourceReference;
            h2o.ADARecordStatus = pipeLink.ADARecordStatus;
            h2o.RecordStatus = pipeLink.RecordStatus;
            h2o.LinkConfidence = pipeLink.LinkConfidence;

            h2o.RiskClaim_Id = claim.Id;
            h2o.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            h2o.Organisation_Id = organisationId;
            h2o.Handset_Id = orgHandsetId;
            h2o.HandsetLinkType_Id = (int)pipeLink.HandsetLinkType_Id;
            //h2o.HireStartDate = pipeLink.HireStartDate;
            //h2o.HireEndDate = pipeLink.HireEndDate;

            h2o.FiveGrading = GetFiveGrading();
            //h2o.HireCompany = pipeLink.HireCompany;

            _db.Handset2Organisation.Add(h2o);

            _db.SaveChanges();

            return h2o;
        }

        public Handset2Organisation UpdateOrgHandset2OrganisationLink(MDA.Common.Enum.Operation op, int linkId,
            //MDA.Common.Enum.HandsetLinkType linkType,
            //DateTime? hireStartDate, DateTime? hireEndDate, string hireCompany, 
                                            MDA.Pipeline.Model.PipelineOrgHandsetLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Handset2Organisation where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateOrgHandset2OrganisationLink(op, link, /*linkType, hireStartDate, hireEndDate, hireCompany, */ pipeLink, claim, ref success);
        }


        public Handset2Organisation UpdateOrgHandset2OrganisationLink(MDA.Common.Enum.Operation op, Handset2Organisation link,
            //MDA.Common.Enum.HandsetLinkType linkType,
            //DateTime? hireStartDate, DateTime? hireEndDate, string hireCompany,
                                          MDA.Pipeline.Model.PipelineOrgHandsetLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Handset.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    //hireStartDate = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(pipeLink.HireStartDate);

                    // check for invalid updates and if so set success = false : NONE
                    //if (link.HireStartDate != null && link.HireStartDate != pipeLink.HireStartDate) // we have value and it's different
                    //{
                    //    success = false;
                    //}

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        //if (link.HireStartDate != pipeLink.HireStartDate)
                        //    link.HireStartDate = pipeLink.HireStartDate;

                        //if (link.HireEndDate != pipeLink.HireEndDate)
                        //    link.HireEndDate = pipeLink.HireEndDate;

                        //if (string.Compare(link.HireCompany, pipeLink.HireCompany, true) != 0)
                        //    link.HireCompany = pipeLink.HireCompany;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;

        }

        public void CleanUpOrgHandset2OrganisationLinks(/*Handset2Organisation latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Handset2Organisation
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Handset2Organisation Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }
    }

   
}
