﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Enum;
using MDA.Common.Helpers;
//using MDA.TracesmartService.Model;
using LinqKit;
using MDA.Common.Debug;
using RiskEngine.Model;
using RiskEngine.Interfaces;
using MDA.Common;

namespace MDA.DataService
{
	public partial class DataServices
	{
        //public void ResetAllPersonAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

		public List<int> GetAllSyncPeopleIds()
		{
			return (from x in _db.People where x.ADARecordStatus >= 10 select x.Id).ToList();
		}

		public void ResetSyncPersonAdaStatus(int id)
		{
			var r = (from x in _db.People where x.Id == id select x).FirstOrDefault();

			if (r != null && r.ADARecordStatus >= 10)
			{
				r.ADARecordStatus -= 10;

				_db.SaveChanges();
			}
		}

        public void CleansePerson(int id, Func<Person, MessageNode, int> cleanseMethod)
        {
            var p = (from x in _db.People where x.Id == id select x).FirstOrDefault();

            if (p == null) return;

            MessageNode n = new MessageNode();

            cleanseMethod(p, n);

            _db.SaveChanges();
        }

		/// <summary>
		/// Given a match type string made up of "CONFIRMED+UNCONFIRMED+TENTATIVE+THIS" (no spaces) it will
		/// return 4 booleans flags to indicate which were present in the string
		/// </summary>
		/// <param name="matchType">The string containing the parts</param>
		/// <param name="includeThis">TRUE is string contained THIS</param>
		/// <param name="includeConfirmed">TRUE is string contained CONFIRMED</param>
		/// <param name="includeUnconfirmed">TRUE is string contained UNCONFIRMED</param>
		/// <param name="includeTentative">TRUE is string contained TENTATIVE</param>
		public void ProcessMatchType(string matchType, out bool includeThis, out bool includeConfirmed, out bool includeUnconfirmed, out bool includeTentative)
		{
			includeConfirmed = false;
			includeUnconfirmed = false;
			includeTentative = false;
			includeThis = false;

			string[] match = matchType.Split('+');

			for (int i = 0; i < match.Length; i++)
			{
				string m = match[i].ToUpper();

				if (m == "CONFIRMED") includeConfirmed = true;
				if (m == "UNCONFIRMED") includeUnconfirmed = true;
				if (m == "TENTATIVE") includeTentative = true;
				if (m == "THIS") includeThis = true;
			}
		}

		public Person GetPerson(int personId)
		{
			return (from x in _db.People where x.Id == personId select x).FirstOrDefault();
		}

		public ListOfInts FindAllPersonAliasesFromDb(int id, bool includeConfirmed, bool includeUnconfirmed, bool includeTentative, bool _trace)
		{
			ListOfInts retList1 = new ListOfInts();

			var treeId = _db.uspGetPersonTree(id, includeConfirmed, includeUnconfirmed, includeTentative, false);

			foreach (var pid in treeId)
			{
				if (pid != id)
					retList1.Add((int)pid);
			}

			if (_trace)
			{
				ADATrace.WriteLine("FindPersonAliasesFromDb: PersonId=" + id.ToString() + " [" + string.Join(",", retList1) + "]");
			}

			return retList1;
		}

		public List<int> FindAllPersonAliases(int id, EntityAliases personAliases, bool _trace)
		{
			List<int> retList1 = new List<int>();

			if (personAliases.IncludeThis)
				retList1.Add(id);

			if (personAliases.IncludeConfirmed)
				retList1.AddRange(personAliases.ConfirmedAliases);

			if (personAliases.IncludeUnconfirmed)
				retList1.AddRange(personAliases.UnconfirmedAliases);

			if (personAliases.IncludeTentative)
				retList1.AddRange(personAliases.TentativeAliases);

			retList1 = retList1.Distinct().ToList();

			return retList1;
		}

		public Person SelectPerson(int id)
		{
			return (from x in _db.People where x.Id == id select x).FirstOrDefault();
		}

		public List<int> StripNonExistantPeopleFromList(List<int> listId)
		{
			List<int> outList = new List<int>();

			foreach (int id in listId)
			{
				if (_db.People.Any(x => x.Id == id))
					outList.Add(id);
			}
			return outList;
		}

        public void UpdateSanctionsPerson(int personId, string sanctionMatchStr, MDA.Pipeline.Model.PipelinePerson person)
        {

            var p = (from x in _db.People where x.Id == personId select x).FirstOrDefault();

            if (p != null)
            {
                try
                {
                    p.SanctionList = sanctionMatchStr;
                    _db.SaveChanges();
                }

                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void ResetSanctionsPerson(int personId)
        {
            var p = (from x in _db.People where x.Id == personId select x).FirstOrDefault();

            if (p != null)
            {
                try
                {
                    p.SanctionList = null;
                    _db.SaveChanges();
                }

                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

		public Person UpdatePerson(int personId, MDA.Pipeline.Model.PipelinePerson person, out bool success, out string error)
		{
			success = false;
			error = "";

			var p = (from x in _db.People where x.Id == personId select x).FirstOrDefault();

			if (p == null) return p;

			if (p.PersonId != null)
			{
				success = false;
				error = "Unable to update IBASE record";
				return p;
			}

			try
			{
				success = true;
				StringBuilder err = new StringBuilder();

				bool modified = false;

				// Check all the breaking changes first

				if (person.FirstName != null && !string.IsNullOrEmpty(p.FirstName) && string.Compare(p.FirstName, person.FirstName, true) != 0) // we have value and it's different
				{
					success = false;
					err.Append("FirstName - ");
				}
				if (person.MiddleName != null && !string.IsNullOrEmpty(p.MiddleName) && string.Compare(p.MiddleName, person.MiddleName, true) != 0) // we have value and it's different
				{
					success = false;
					err.Append("MiddleName - ");
				}
				if (person.LastName != null && !string.IsNullOrEmpty(p.LastName) && string.Compare(p.LastName, person.LastName, true) != 0) // we have value and it's different
				{
					success = false;
					err.Append("LastName - ");
				}

				if (person.DateOfBirth != null) // we have a new value
				{
					if (p.DateOfBirth != null)
					{
						DateTime? old_DoB = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(p.DateOfBirth.Value);
						DateTime? new_DoB = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(person.DateOfBirth);

						if (!old_DoB.Equals(new_DoB))   // and it's different
						{
							success = false;
							err.Append("DoB - ");
						}
					}
				}

				if (!success)
				{
					err.Append("overwrites existing value");
					error = err.ToString();
					return p;
				}

				if (p.FirstName == null)
				{
					p.FirstName = person.FirstName;
					modified = true;
				}

				if (p.MiddleName == null)
				{
					p.MiddleName = person.MiddleName;
					modified = true;
				}

				if (p.LastName == null)
				{
					p.LastName = person.LastName;
					modified = true;
				}

				if (p.DateOfBirth == null && person.DateOfBirth != null)
				{
					p.DateOfBirth = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(person.DateOfBirth);
					modified = true;
				}

				if (p.Salutation_Id != (int)person.Salutation_Id && (int)person.Salutation_Id > 0)
				{
					p.Salutation_Id = (int)person.Salutation_Id;
					modified = true;
				}

				if ((p.Gender_Id != (int)person.Gender_Id) && (int)person.Gender_Id > 0)
				{
					p.Gender_Id = (int)person.Gender_Id;
					modified = true;
				}

				if (p.Nationality == null || string.Compare(p.Nationality, person.Nationality, true) != 0)
				{
					p.Nationality = person.Nationality;
					modified = true;
				}

				if (!string.IsNullOrEmpty(person.Occupation))
				{
					if (string.IsNullOrEmpty(p.Occupation))
					{
						p.Occupation = person.Occupation;
						modified = true;
					}
					else
					{
						string[] occs = p.Occupation.Split(',');
						bool foundIt = false;

						foreach (string o in occs)
						{
							if (string.Compare(o, person.Occupation, true) == 0)
								foundIt = true;
						}

						if (!foundIt)
						{
							StringBuilder newOcc = new StringBuilder();
							string comma = "";

							foreach (string o in occs)
							{
								if (newOcc.Length + o.Length + comma.Length > 254) break;

								newOcc.Append(comma + o);
								comma = ",";
							}

							p.Occupation = newOcc.ToString();
							modified = true;
						}
					}
				}

				if (modified)
				{
					p.ModifiedDate = DateTime.Now;
					p.ModifiedBy = _ctx.Who;

					_db.SaveChanges();
				}
			}
			catch (Exception)
			{
				success = false;
			}

			return p;
		}

		public Person InsertPerson(MDA.Pipeline.Model.PipelinePerson person)
		{
			Person p = _db.People.Create();

			p.ADARecordStatus   = person.ADARecordStatus;
			p.CreatedBy         = person.CreatedBy;
			p.CreatedDate       = person.CreatedDate;
			p.DateOfBirth       = person.DateOfBirth;
			p.Documents         = person.Documents;
			p.FirstName         = person.FirstName;
			p.Gender_Id         = person.Gender_Id;
			p.Hobbies           = person.Hobbies;
			p.IBaseId           = person.IBaseId;
			p.KeyAttractor      = person.KeyAttractor;
			p.LastName          = person.LastName;
			p.MiddleName        = person.MiddleName;
			p.ModifiedBy        = person.ModifiedBy;
			p.ModifiedDate      = person.ModifiedDate;
			p.Nationality       = person.Nationality;
			p.Notes             = person.Notes;
			p.Occupation        = person.Occupation;
			p.PersonId          = person.PersonId;
			p.RecordStatus      = person.RecordStatus;
			p.Salutation_Id     = person.Salutation_Id;
			p.SanctionDate      = person.SanctionDate;
			p.SanctionSource    = person.SanctionSource;
			p.Schools           = person.Schools;
			p.Source            = person.Source;
			p.SourceDescription = person.SourceDescription;
			p.SourceReference   = person.SourceReference;
			p.TaxiDriverLicense = person.TaxiDriverLicense;
		   


			_db.People.Add(p);

			_db.SaveChanges();

			return p;
		}
	}
}
