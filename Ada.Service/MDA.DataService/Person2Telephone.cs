﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPerson2TelephoneAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2Telephone set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectTelephone2PersonLinks(int masterTelephoneId, int telephoneId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Telephone set Telephone_Id = " + masterTelephoneId +
                                           " where Telephone_Id = " + telephoneId);
        }

        public void RedirectPerson2TelephoneLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Telephone set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        public Person2Telephone GetPerson2TelephoneLink(MdaDbContext db, int id)
        {
            return (from x in db.Person2Telephone where x.Id == id select x).FirstOrDefault();
        }

        public Person2Telephone GetPerson2TelephoneLink(int id)
        {
            return GetPerson2TelephoneLink(id);
        }

        public List<Person2Telephone> GetPerson2TelephoneLinks(int personId, int telephoneId, int baseRiskClaimId)
        {
            return (from x in _db.Person2Telephone
                    where x.Person_Id == personId && x.Telephone_Id == telephoneId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Person2Telephone GetLatestPerson2TelephoneLink(int personId, int telephoneId, int baseRiskClaimId)
        {
            return (from x in _db.Person2Telephone
                    where x.Person_Id == personId && x.Telephone_Id == telephoneId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPerson2TelephoneLinks(/*Person2Telephone latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Person2Telephone
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Person2Telephone Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Person2Telephone UpdatePerson2TelephoneLink(MDA.Common.Enum.Operation op, Person2Telephone link,
                                                    //MDA.Common.Enum.TelephoneLinkType linkType,
                                                    MDA.Pipeline.Model.PipelineTelephoneLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {

            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.TelephoneLinkType_Id != (int)pipeLink.TelephoneLinkType_Id) //linkType)
                            link.TelephoneLinkType_Id = (int)pipeLink.TelephoneLinkType_Id;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;
        }


        public Person2Telephone UpdatePerson2TelephoneLink(MDA.Common.Enum.Operation op, int linkId,
                                               //MDA.Common.Enum.TelephoneLinkType linkType, 
                                                MDA.Pipeline.Model.PipelineTelephoneLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Person2Telephone where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePerson2TelephoneLink(op, link, /*linkType,*/ pipeLink, claim, ref success);
        }

        public Person2Telephone InsertPerson2TelephoneLink(int personId, int telephoneId,
                                MDA.Pipeline.Model.PipelineTelephoneLink pipeLink, 
                                //MDA.Common.Enum.TelephoneLinkType linkType, 
                                RiskClaim claim)
        {
            Person2Telephone p2t = _db.Person2Telephone.Create();

            p2t.CreatedBy         = pipeLink.CreatedBy;
            p2t.CreatedDate       = pipeLink.CreatedDate;
            p2t.ModifiedBy        = pipeLink.ModifiedBy;
            p2t.ModifiedDate      = pipeLink.ModifiedDate;
            p2t.Source            = pipeLink.Source;
            p2t.SourceDescription = pipeLink.SourceDescription;
            p2t.SourceReference   = pipeLink.SourceReference;
            p2t.ADARecordStatus   = pipeLink.ADARecordStatus;
            p2t.RecordStatus      = pipeLink.RecordStatus;
            p2t.LinkConfidence    = pipeLink.LinkConfidence;

            p2t.RiskClaim_Id = claim.Id;
            p2t.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2t.Person_Id = personId;
            p2t.Telephone_Id = telephoneId;
            
            p2t.FiveGrading = GetFiveGrading();
            p2t.TelephoneLinkType_Id = (int)pipeLink.TelephoneLinkType_Id;

            _db.Person2Telephone.Add(p2t);

            _db.SaveChanges();

            return p2t;
        }

        //public Person2Telephone InsertPerson2TelephoneLink(Person2Telephone person2phone)
        //{
        //    Person2Telephone p2p = _db.Person2Telephone.Create();

        //    p2p.CreatedBy = person2phone.CreatedBy;
        //    p2p.CreatedDate = DateTime.Now;
        //    p2p.BaseRiskClaim_Id = person2phone.BaseRiskClaim_Id;

        //    p2p.Source = person2phone.Source;
        //    p2p.SourceDescription = person2phone.SourceDescription;
        //    p2p.SourceReference = person2phone.SourceReference;

        //    p2p.Person_Id = person2phone.Person_Id;
        //    p2p.Telephone_Id = person2phone.Telephone_Id;
        //    p2p.TelephoneLinkId = person2phone.TelephoneLinkId;

        //    p2p.IBaseId = person2phone.IBaseId;
        //    p2p.RiskClaim_Id = person2phone.RiskClaim_Id;
        //    p2p.LinkConfidence = person2phone.LinkConfidence;
        //    p2p.FiveGrading = person2phone.FiveGrading;

        //    _db.Person2Telephone.Add(p2p);

        //    _db.SaveChanges();

        //    return p2p;
        //}
    }
}
