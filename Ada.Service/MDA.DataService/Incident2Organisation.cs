﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public void RedirectIncident2OrganisationLinks(int masterIncidentId, int incidentId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2Organisation set Incident_Id = " + masterIncidentId +
                                           " where Incident_Id = " + incidentId);
        }

        public void RedirectOrganisation2IncidentLinks(int masterOrganisationId, int organisationId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2Organisation set Organisation_Id = " + masterOrganisationId +
                                           " where Organisation_Id = " + organisationId);
        }

        //public void ResetAllIncident2OrganisationLinkAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Incident2Organisation set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public List<Incident2Organisation> GetIncident2OrganisationLinks(int incidentId, int organisationId, int baseRiskClaimId)
        {
            return (from x in _db.Incident2Organisation
                    where x.Incident_Id == incidentId && x.Organisation_Id == organisationId 
                        && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Incident2Organisation GetLatestIncident2OrganisationLink(int incidentId, int organisationId, bool claimLevelLink, int baseRiskClaimId)
        {
            return (from x in _db.Incident2Organisation
                    where x.Incident_Id == incidentId && x.Organisation_Id == organisationId 
                        && x.IsClaimLevelLink == claimLevelLink 
                        && x.BaseRiskClaim_Id == baseRiskClaimId
                        && x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpIncident2OrganisationLinks(RiskClaim newRiskClaim)
        {
            // Get all I2O links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Incident2Organisation
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Incident2Organisation Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Incident2Organisation UpdateIncident2OrganisationLink(MDA.Common.Enum.Operation op, Incident2Organisation link,
                                                //MDA.Pipeline.Model.PipelineClaim xmlClaim, MDA.Pipeline.Model.PipelineOrganisation xmlOrganisation,
                                                //MDA.Pipeline.Model.PipelineClaimInfo xmlClaimInfo, 
                                                MDA.Pipeline.Model.PipelineIncident2OrganisationLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            MDA.Pipeline.Model.PipelineClaimInfo pipeClaimInfo = pipeLink.ClaimInfo;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.Broker != pipeLink.Broker)
                            link.Broker = pipeLink.Broker;

                        link.Insurer = pipeLink.Insurer;
                        link.ClaimNumber = pipeLink.ClaimNumber;
                        link.IncidentTime = pipeLink.IncidentTime;

                        if (link.OrganisationType_Id == 0)
                            link.OrganisationType_Id = pipeLink.OrganisationType_Id;

                        if (pipeClaimInfo != null)
                        {
                            link.ClaimCode             = pipeClaimInfo.ClaimCode;
                            link.ClaimNotificationDate = pipeClaimInfo.ClaimNotificationDate;
                            link.ClaimStatus_Id        = (int)pipeClaimInfo.ClaimStatus_Id;
                            link.IncidentCircs         = pipeClaimInfo.IncidentCircumstances;
                            link.IncidentLocation      = pipeClaimInfo.IncidentLocation;
                            link.AmbulanceAttended     = pipeClaimInfo.AmbulanceAttended;
                            link.PoliceAttended        = pipeClaimInfo.PoliceAttended;
                            link.PoliceForce           = pipeClaimInfo.PoliceForce;
                            link.PoliceReference       = pipeClaimInfo.PoliceReference;
                            link.PaymentsToDate        = pipeClaimInfo.PaymentsToDate;
                            link.Reserve               = pipeClaimInfo.Reserve;
                        }

                        //bool isPolicyHolder = xmlOrganisation.OrganisationType_Id == (int)MDA.Common.Enum.OrganisationType.PolicyHolder;

                        //if (isPolicyHolder)
                        //{
                        //    if (xmlClaim.Policy != null && xmlClaim.Policy.Insurer != null)
                        //        link.Insurer = xmlClaim.Policy.Insurer;
                        //    else
                        //        link.Insurer = ctx.RiskClient.ClientName;
                        //}

                        //int PartyType_Id = (isPolicyHolder) ? (int)MDA.Common.Enum.PartyType.Policyholder : (int)MDA.Common.Enum.PartyType.Unknown;
                        //int SubPartyType_Id = (int)MDA.Common.Enum.SubPartyType.Unknown;

                        //if (link.PartyType_Id != PartyType_Id)
                        //    link.PartyType_Id = PartyType_Id;

                        //if (link.SubPartyType_Id != SubPartyType_Id)
                        //    link.SubPartyType_Id = SubPartyType_Id;

                        //TimeSpan? newTime;

                        //if (xmlClaim.IncidentDate != null)
                        //    newTime = new TimeSpan(xmlClaim.IncidentDate.Hour, xmlClaim.IncidentDate.Minute, 0);
                        //else
                        //    newTime = null;

                        //link.IncidentTime = newTime;
                        //link.ClaimNumber = xmlClaim.ClaimNumber;

                        //if (xmlClaimInfo != null)
                        //{
                        //    link.MojStatus_Id = 0; // (int)xmlClaimInfo.MojStatus;
                        //    link.IncidentCircs = xmlClaimInfo.IncidentCircumstances;
                        //    link.IncidentLocation = xmlClaimInfo.IncidentLocation;
                        //    link.ClaimStatus_Id = (int)xmlClaimInfo.ClaimStatus_Id;
                        //    link.ClaimStatus_Id = (int)xmlClaimInfo.ClaimStatus_Id;
                        //    link.ClaimNotificationDate = xmlClaimInfo.ClaimNotificationDate;
                        //    link.PoliceAttended = xmlClaimInfo.PoliceAttended;
                        //    link.PoliceForce = xmlClaimInfo.PoliceForce;
                        //    link.PoliceReference = xmlClaimInfo.PoliceReference;
                        //    link.AmbulanceAttended = xmlClaimInfo.AmbulanceAttended;
                        //    link.Reserve = xmlClaimInfo.Reserve;
                        //    link.PaymentsToDate = xmlClaimInfo.PaymentsToDate;
                        //}
                    }
                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }

        public Incident2Organisation UpdateIncident2OrganisationLink(MDA.Common.Enum.Operation op, int linkId, //MDA.Pipeline.Model.PipelineClaim xmlClaim,
                                               //MDA.Pipeline.Model.PipelineOrganisation xmlOrganisation, MDA.Pipeline.Model.PipelineClaimInfo xmlClaimInfo,
                                               MDA.Pipeline.Model.PipelineIncident2OrganisationLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var a = (from x in _db.Incident2Organisation where x.Id == linkId select x).FirstOrDefault();

            if (a == null) return a;

            return UpdateIncident2OrganisationLink(op, a, /*xmlClaim, xmlOrganisation, xmlClaimInfo,*/ pipeLink, claim, ref success);
        }

        public Incident2Organisation InsertIncident2OrganisationLink(int incidentId, int organisationId, MDA.Pipeline.Model.PipelineIncident2OrganisationLink pipeLink, bool claimLevelLink,
            //MDA.Pipeline.Model.PipelineClaim xmlClaim, MDA.Pipeline.Model.PipelineOrganisation xmlOrganisation, 
            //MDA.Pipeline.Model.PipelineClaimInfo xmlClaimInfo, 
            RiskClaim claim)
        {
            MDA.Pipeline.Model.PipelineClaimInfo pipeClaimInfo = pipeLink.ClaimInfo;

            Incident2Organisation i2o = _db.Incident2Organisation.Create();

            i2o.CreatedBy         = pipeLink.CreatedBy;
            i2o.CreatedDate       = pipeLink.CreatedDate;
            i2o.ModifiedBy        = pipeLink.ModifiedBy;
            i2o.ModifiedDate      = pipeLink.ModifiedDate;
            i2o.Source            = pipeLink.Source;
            i2o.SourceDescription = pipeLink.SourceDescription;
            i2o.SourceReference   = pipeLink.SourceReference;
            i2o.ADARecordStatus   = pipeLink.ADARecordStatus;
            i2o.RecordStatus      = pipeLink.RecordStatus;
            i2o.LinkConfidence    = pipeLink.LinkConfidence;

            i2o.RiskClaim_Id     = claim.Id;
            i2o.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            i2o.Organisation_Id = organisationId;
            i2o.Incident_Id     = incidentId;

            i2o.IsClaimLevelLink    = claimLevelLink;
            i2o.OrganisationType_Id = (int)pipeLink.OrganisationType_Id;
            i2o.MojStatus_Id        = pipeLink.MojStatus_Id;

            i2o.FiveGrading = GetFiveGrading();

            i2o.PartyType_Id = (int)pipeLink.PartyType_Id;
            i2o.SubPartyType_Id = (int)pipeLink.SubPartyType_Id;
            i2o.AttendedHospital = pipeLink.AttendedHospital;

            i2o.Broker = pipeLink.Broker;
            i2o.Insurer = pipeLink.Insurer;
            i2o.ClaimNumber = pipeLink.ClaimNumber;
            i2o.IncidentTime = pipeLink.IncidentTime;

            if (pipeClaimInfo != null)
            {
                i2o.ClaimCode = pipeClaimInfo.ClaimCode;
                i2o.ClaimNotificationDate = pipeClaimInfo.ClaimNotificationDate;
                i2o.ClaimStatus_Id = (int)pipeClaimInfo.ClaimStatus_Id;
                i2o.IncidentCircs = pipeClaimInfo.IncidentCircumstances;
                i2o.IncidentLocation = pipeClaimInfo.IncidentLocation;
                i2o.AmbulanceAttended = pipeClaimInfo.AmbulanceAttended;
                i2o.PoliceAttended = pipeClaimInfo.PoliceAttended;
                i2o.PoliceForce = pipeClaimInfo.PoliceForce;
                i2o.PoliceReference = pipeClaimInfo.PoliceReference;
                i2o.PaymentsToDate = pipeClaimInfo.PaymentsToDate;
                i2o.Reserve = pipeClaimInfo.Reserve;
            }

            //bool isPolicyHolder = pipeLink.OrganisationType_Id == (int)MDA.Common.Enum.OrganisationType.PolicyHolder;

            //i2o.PartyType_Id    = (isPolicyHolder) ? (int)MDA.Common.Enum.PartyType.Policyholder : (int)MDA.Common.Enum.PartyType.Unknown;
            //i2o.SubPartyType_Id = (int)MDA.Common.Enum.SubPartyType.Unknown;

            //i2o.Broker       = pipeLink.Broker;
            //i2o.Insurer      = pipeLink.Insurer;
            //i2o.ClaimNumber  = pipeLink.ClaimNumber;
            //i2o.IncidentTime = pipeLink.IncidentTime;

            //if (pipeClaimInfo != null)
            //{
            //    i2o.ClaimCode             = pipeClaimInfo.ClaimCode;
            //    i2o.ClaimNotificationDate = pipeClaimInfo.ClaimNotificationDate;
            //    i2o.ClaimStatus_Id        = (int)pipeClaimInfo.ClaimStatus_Id;
            //    i2o.IncidentCircs         = pipeClaimInfo.IncidentCircumstances;
            //    i2o.IncidentLocation      = pipeClaimInfo.IncidentLocation;
            //    i2o.AmbulanceAttended     = pipeClaimInfo.AmbulanceAttended;
            //    i2o.PoliceAttended        = pipeClaimInfo.PoliceAttended;
            //    i2o.PoliceForce           = pipeClaimInfo.PoliceForce;
            //    i2o.PoliceReference       = pipeClaimInfo.PoliceReference;
            //    i2o.PaymentsToDate        = pipeClaimInfo.PaymentsToDate;
            //    i2o.Reserve               = pipeClaimInfo.Reserve;
            //}

            //if (isPolicyHolder)
            //{
            //    i2o.ClaimNumber = xmlClaim.ClaimNumber;
            //    i2o.Broker = xmlClaim.Policy.Broker;

            //    if (xmlClaim.Policy != null && xmlClaim.Policy.Insurer != null)
            //        i2o.Insurer = xmlClaim.Policy.Insurer;
            //    else
            //        i2o.Insurer = ctx.RiskClient.ClientName;
            //}

            //TimeSpan? newTime;

            //if (xmlClaim.IncidentDate != null && (xmlClaim.IncidentDate.Hour > 0 || xmlClaim.IncidentDate.Minute > 0))
            //    newTime = new TimeSpan(xmlClaim.IncidentDate.Hour, xmlClaim.IncidentDate.Minute, 0);
            //else
            //    newTime = null;

            //i2o.IncidentTime = newTime;

            //if (xmlClaimInfo != null)
            //{
            //    i2o.AmbulanceAttended     = xmlClaimInfo.AmbulanceAttended;
            //    i2o.ClaimCode             = xmlClaimInfo.ClaimCode;
            //    i2o.ClaimNotificationDate = xmlClaimInfo.ClaimNotificationDate;
            //    i2o.ClaimStatus_Id        = (int)xmlClaimInfo.ClaimStatus_Id;
            //    i2o.IncidentCircs         = xmlClaimInfo.IncidentCircumstances;
            //    i2o.IncidentLocation      = xmlClaimInfo.IncidentLocation;
            //    i2o.PoliceAttended        = xmlClaimInfo.PoliceAttended;
            //    i2o.PoliceForce           = xmlClaimInfo.PoliceForce;
            //    i2o.PoliceReference       = xmlClaimInfo.PoliceReference;
            //    i2o.PaymentsToDate        = xmlClaimInfo.PaymentsToDate;
            //    i2o.Reserve               = xmlClaimInfo.Reserve;
            //}

            _db.Incident2Organisation.Add(i2o);

            _db.SaveChanges();

            return i2o;
        }

        //public Incident2Organisation InsertIncident2OrganisationLink(Incident2Organisation incident2org)
        //{
        //    Incident2Organisation i2o = _db.Incident2Organisation.Create();

        //    i2o.CreatedBy = incident2org.CreatedBy;
        //    i2o.CreatedDate = DateTime.Now;
        //    i2o.BaseRiskClaim_Id = incident2org.BaseRiskClaim_Id;

        //    i2o.Source = incident2org.Source;
        //    i2o.SourceDescription = incident2org.SourceDescription;
        //    i2o.SourceReference = incident2org.SourceReference;
        //    i2o.IBaseId = incident2org.IBaseId;
        //    i2o.IsClaimLevelLink = incident2org.IsClaimLevelLink;

        //    i2o.OrganisationType_Id = incident2org.OrganisationType_Id;

        //    i2o.AccidentManagement = incident2org.AccidentManagement;
        //    i2o.AmbulanceAttended = incident2org.AmbulanceAttended;
        //    i2o.Broker = incident2org.Broker;
        //    i2o.ClaimCode = incident2org.ClaimCode;
        //    i2o.ClaimNotificationDate = incident2org.ClaimNotificationDate;
        //    i2o.ClaimNumber = incident2org.ClaimNumber;
        //    i2o.ClaimStatus_Id = incident2org.ClaimStatus_Id;
        //    i2o.ClaimType = incident2org.ClaimType;                
        //    //i2o.DateOfResolution = incident2org.DateOfResolution;
        //    i2o.Engineer = incident2org.Engineer;
        //    i2o.Hire = incident2org.Hire;
        //    i2o.Incident_Id = incident2org.Incident_Id;
        //    i2o.IncidentCircs = incident2org.IncidentCircs;
        //    i2o.IncidentLinkId = incident2org.IncidentLinkId;
        //    i2o.IncidentLocation = incident2org.IncidentLocation;
        //    i2o.IncidentTime = incident2org.IncidentTime;
        //    i2o.Insurer = incident2org.Insurer;
        //    //i2o.MannerOfResolution = incident2org.MannerOfResolution;
        //    i2o.MedicalExaminer = incident2org.MedicalExaminer;
        //    i2o.MojStatus_Id = incident2org.MojStatus_Id;
        //    i2o.Organisation_Id = incident2org.Organisation_Id;
        //    i2o.PartyType_Id = incident2org.PartyType_Id;
        //    i2o.PoliceAttended = incident2org.PoliceAttended;
        //    i2o.PoliceForce = incident2org.PoliceForce;
        //    i2o.PoliceReference = incident2org.PoliceReference;
        //    i2o.Recovery = incident2org.Recovery;
        //    i2o.ReferralSource = incident2org.ReferralSource;
        //    i2o.Repairer = incident2org.Repairer;
        //    i2o.RiskClaim_Id = incident2org.RiskClaim_Id;
        //    i2o.Solicitors = incident2org.Solicitors;
        //    i2o.Storage = incident2org.Storage;
        //    i2o.StorageAddress = incident2org.StorageAddress;
        //    i2o.SubPartyType_Id = incident2org.SubPartyType_Id;
        //    i2o.LinkConfidence = incident2org.LinkConfidence;
        //    i2o.FiveGrading = incident2org.FiveGrading;
        //    i2o.AttendedHospital = incident2org.AttendedHospital;

        //    i2o.RiskClaim_Id = incident2org.RiskClaim_Id;

        //    _db.Incident2Organisation.Add(i2o);

        //    _db.SaveChanges();

        //    return i2o;
        //}
    }

}
