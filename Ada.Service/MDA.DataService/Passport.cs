﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;


namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPassportAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Passport set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}
        public int DeduplicatePassports(CurrentContext ctx, Func<string, int> ShowProgress = null)
        {
            //trace = new List<string>();
            int duplicateCount = 0;

            var q = ctx.db.Passports.Where(x => x.ADARecordStatus == 0 || x.ADARecordStatus >= 10);

            var query = (q.GroupBy(i => i.PassportNumber)
                        .Where(g => g.Count() > 1)
                        .Select(g => g.Key)).ToList();

            foreach (var pport in query)
            {
                if (pport == null) continue;

                duplicateCount++;

                using (var ctxx = new CurrentContext(ctx))
                {
                    var master = (from t in ctxx.db.Passports
                                  where t.PassportNumber == pport && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10) && t.PassportId != null
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();

                    if (master == null)
                    {
                        master = (from t in ctxx.db.Passports
                                  where t.PassportNumber == pport && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();
                    }

                    if (master == null) continue;

                    var allDuplicates = from t in ctxx.db.Passports
                                        where t.PassportNumber == pport && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                        && t.Id != master.Id
                                        select t;

                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                    {
                        try
                        {
                            foreach (var td in allDuplicates)
                            {
                                List<int> PId = new List<int>();

                                ctxx.db.Entry(td).Collection(z => z.Person2Passport).Load();

                                if (td.Person2Passport.Any())
                                {
                                    foreach (var l in td.Person2Passport)
                                    {
                                        l.Passport_Id = master.Id;
                                        PId.Add(l.Person_Id);
                                    }
                                }

                                td.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                                td.RecordStatus = Constants.IBaseDeleteValue;

                                if (ShowProgress != null)
                                    ShowProgress(string.Format("Duplicate Passport Removed [{0}] : New Master Id [{1}] : People ID's Moved [{2}]",
                                                td.Id, master.Id, string.Join(",", PId)));
                            }
                            ctxx.db.SaveChanges();

                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();

                            throw;
                        }
                    }
                }
            }


            if (ShowProgress != null)
                ShowProgress(string.Format("Duplicates Found [{0}]", duplicateCount));

            return duplicateCount;
        }

        public Passport SelectPassport(int id)
        {
            return (from x in _db.Passports where x.Id == id select x).FirstOrDefault();
        }

        public List<int> VerifyPassportListExists(List<int> listId)
        {
            List<int> outList = new List<int>();

            foreach (int id in listId)
            {
                if (_db.Passports.Any(x => x.Id == id))
                    outList.Add(id);
            }
            return outList;
        }

        public Passport UpdatePassport(int passportId, MDA.Pipeline.Model.PipelinePassport pipePassport, out bool success)
        {
            success = false;


            var pp = (from x in _db.Passports where x.Id == passportId select x).FirstOrDefault();

            if (pp == null) return pp;

            try
            {
                success = true;

                bool modified = false;

                if (pp.PassportNumber == null)
                {
                    pp.PassportNumber = pipePassport.PassportNumber;
                    modified = true;
                }

                if (modified)
                {
                    pp.ModifiedDate = DateTime.Now;
                    pp.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
            }
            catch (Exception)
            {
                success = false;
            }

            return pp;
        }

        public Passport InsertPassport(MDA.Pipeline.Model.PipelinePassport pipePassport)
        {
            Passport c = _db.Passports.Create();

            c.ADARecordStatus   = pipePassport.ADARecordStatus;
            c.CreatedBy         = pipePassport.CreatedBy;
            c.CreatedDate       = pipePassport.CreatedDate;
            c.IBaseId           = pipePassport.IBaseId;
            c.KeyAttractor      = pipePassport.KeyAttractor;
            c.ModifiedBy        = pipePassport.ModifiedBy;
            c.ModifiedDate      = pipePassport.ModifiedDate;
            c.PassportId        = pipePassport.PassportId; 
            c.PassportNumber    = pipePassport.PassportNumber;
            c.RecordStatus      = pipePassport.RecordStatus;
            c.Source            = pipePassport.Source;
            c.SourceDescription = pipePassport.SourceDescription;
            c.SourceReference   = pipePassport.SourceReference;

            _db.Passports.Add(c);

            _db.SaveChanges();

            return c;
        }

        //public Passport InsertPassport(Passport passport)
        //{
        //    Passport c = _db.Passports.Create();

        //    c.CreatedBy = passport.CreatedBy;
        //    c.CreatedDate = passport.CreatedDate;
        //    c.PassportNumber = passport.PassportNumber;
        //    c.PassportId = passport.PassportId;
        //    c.IBaseId = passport.IBaseId;
        //    c.Source = passport.Source;
        //    c.SourceDescription = passport.SourceDescription;
        //    c.SourceReference = passport.SourceReference;

        //    _db.Passports.Add(c);

        //    _db.SaveChanges();
           
        //    return c;
        //}

        public Passport FindPassport(string passportNumber)
        {
            return (from x in _db.Passports where x.PassportNumber == passportNumber select x).FirstOrDefault();
        }

    }

}
