﻿using MDA.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.DataService
{
    public partial class DataServices
    {

        public void InsertTimingMetric(TimingMetric timingsMatching)
        {
            TimingMetric tm = _db.TimingMetrics.Create();

            tm.Created = timingsMatching.Created;
            tm.CreatedBy = timingsMatching.CreatedBy;
            tm.RuleNumber = timingsMatching.RuleNumber;
            tm.TimingInMs = timingsMatching.TimingInMs;
            tm.RiskClaim_Id = timingsMatching.RiskClaim_Id;
            tm.BaseRiskClaim_Id = timingsMatching.BaseRiskClaim_Id;
            tm.TimingType = timingsMatching.TimingType;
            tm.EntityType = timingsMatching.EntityType;

            _db.TimingMetrics.Add(tm);

            _db.SaveChanges();
        }
    }
}
