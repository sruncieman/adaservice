﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;


namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllWebSiteAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update WebSite set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public int DeduplicateWebSites(CurrentContext ctx, Func<string, int> ShowProgress = null)
        {
            //trace = new List<string>();
            int duplicateCount = 0;

            var q = ctx.db.WebSites.Where(x => x.ADARecordStatus == 0 || x.ADARecordStatus >= 10);

            var query = (q.GroupBy(i => i.URL)
                        .Where(g => g.Count() > 1)
                        .Select(g => g.Key)).ToList();

            foreach (var dWeb in query)
            {
                if (dWeb == null) continue;

                duplicateCount++;

                using (var ctxx = new CurrentContext(ctx))
                {
                    var master = (from t in ctxx.db.WebSites
                                  where t.URL == dWeb && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10) && t.WebSiteId != null
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();

                    if (master == null)
                    {
                        master = (from t in ctxx.db.WebSites
                                  where t.URL == dWeb && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                  orderby t.CreatedDate ascending
                                  select t).FirstOrDefault();
                    }

                    if (master == null) continue;

                    var allDuplicates = from t in ctxx.db.WebSites
                                        where t.URL == dWeb && (t.ADARecordStatus == 0 || t.ADARecordStatus >= 10)
                                        && t.Id != master.Id
                                        select t;

                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                    {
                        try
                        {
                            foreach (var td in allDuplicates)
                            {
                                List<int> OId = new List<int>();  // Keep Org Id's that get moved

                                ctxx.db.Entry(td).Collection(z => z.Organisation2WebSite).Load();

                                if (td.Organisation2WebSite.Any())
                                {
                                    foreach (var l in td.Organisation2WebSite)
                                    {
                                        l.WebSite_Id = master.Id;
                                        OId.Add(l.Organisation_Id);
                                    }
                                }

                                td.ADARecordStatus = (int)ADARecordStatus.UpdateRemoved;
                                td.RecordStatus = Constants.IBaseDeleteValue;

                                if (ShowProgress != null)
                                    ShowProgress(string.Format("Duplicate Website Removed [{0}] : New Master Id [{1}] : Org ID's Moved [{2}]",
                                                td.Id, master.Id, string.Join(",", OId)));
                            }
                            ctxx.db.SaveChanges();

                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();

                            throw;
                        }
                    }
                }
            }



            if (ShowProgress != null)
                ShowProgress(string.Format("Duplicates Found [{0}]", duplicateCount));

            return duplicateCount;
        }

        public WebSite SelectWebSite(int id)
        {
            return (from x in _db.WebSites where x.Id == id select x).FirstOrDefault();
        }

        public List<int> VerifyWebSiteListExists(List<int> listId)
        {
            List<int> outList = new List<int>();

            foreach (int id in listId)
            {
                if (_db.WebSites.Any(x => x.Id == id))
                    outList.Add(id);
            }
            return outList;
        }

        public WebSite UpdateWebSite(int websiteId, MDA.Pipeline.Model.PipelineWebSite website, out bool success)
        {
            success = false;

            var pp = (from x in _db.WebSites where x.Id == websiteId select x).FirstOrDefault();

            if (pp == null) return pp;

            try
            {
                success = true;


                bool modified = false;

                if (pp.URL == null)
                {
                    pp.URL = website.URL;
                    modified = true;
                }

                if (modified)
                {
                    pp.ModifiedDate = DateTime.Now;
                    pp.ModifiedBy = _ctx.Who;

                    _db.SaveChanges();
                }
            }
            catch (Exception)
            {
                success = false;
            }

            return pp;
        }

        public WebSite InsertWebSite(MDA.Pipeline.Model.PipelineWebSite website)
        {
            WebSite e = _db.WebSites.Create();

            e.ADARecordStatus   = website.ADARecordStatus;
            e.CreatedBy         = website.CreatedBy;
            e.CreatedDate       = website.CreatedDate;
            e.IBaseId           = website.IBaseId;
            e.KeyAttractor      = website.KeyAttractor;
            e.ModifiedBy        = website.ModifiedBy;
            e.ModifiedDate      = website.ModifiedDate;
            e.RecordStatus      = website.RecordStatus;
            e.Source            = website.Source;
            e.SourceDescription = website.SourceDescription;
            e.SourceReference   = website.SourceReference;
            e.URL               = website.URL;
            e.WebSiteId         = website.WebSiteId;

            _db.WebSites.Add(e);

            _db.SaveChanges();

            return e;
        }

        //public WebSite InsertWebSite(WebSite webSite)
        //{
        //    WebSite c = _db.WebSites.Create();

        //    c.CreatedBy = webSite.CreatedBy;
        //    c.CreatedDate = webSite.CreatedDate;
        //    c.URL = webSite.URL;
        //    c.WebSiteId = webSite.WebSiteId;
        //    c.IBaseId = webSite.IBaseId;
        //    c.Source = webSite.Source;
        //    c.SourceDescription = webSite.SourceDescription;
        //    c.SourceReference = webSite.SourceReference;

        //    _db.WebSites.Add(c);

        //    _db.SaveChanges();

        //    return c;
        //}

        public WebSite FindWebSite(string URL)
        {
            return (from x in _db.WebSites where x.URL == URL select x).FirstOrDefault();
        }

    }

}
