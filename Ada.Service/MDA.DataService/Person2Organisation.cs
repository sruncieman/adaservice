﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPerson2OrganisationAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2Organisation set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectOrganisation2PersonLinks(int masterOrganisationId, int organisationId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Organisation set Organisation_Id = " + masterOrganisationId +
                                           " where Organisation_Id = " + organisationId);
        }

        public void RedirectPerson2OrganisationLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Organisation set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        public List<Person2Organisation> GetPerson2OrganisationLinks(int personId, int organisationId, int baseRiskClaimId)
        {
            return (from x in _db.Person2Organisation
                    where x.Person_Id == personId && x.Organisation_Id == organisationId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Person2Organisation GetLatestPerson2OrganisationLink(int personId, int organisationId, int baseRiskClaimId)
        {
            return (from x in _db.Person2Organisation
                    where x.Person_Id == personId && x.Organisation_Id == organisationId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPerson2OrganisationLinks(/*Person2Organisation latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Person2Organisation
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Person2Organisation Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Person2Organisation UpdatePerson2OrganisationLink(MDA.Common.Enum.Operation op, Person2Organisation link,
                                          MDA.Pipeline.Model.PipelinePerson2OrganisationLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {

            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false

                    if (link.Person2OrganisationLinkType_Id != 0 && link.Person2OrganisationLinkType_Id != (int)pipeLink.Person2OrganisationLinkType_Id) // we have value and it's different
                    {
                        success = false;
                    }

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.Person2OrganisationLinkType_Id != (int)pipeLink.Person2OrganisationLinkType_Id)
                            link.Person2OrganisationLinkType_Id = (int)pipeLink.Person2OrganisationLinkType_Id;

                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Person2Organisation UpdatePerson2OrganisationLink(MDA.Common.Enum.Operation op, int linkId,
                                      MDA.Pipeline.Model.PipelinePerson2OrganisationLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Person2Organisation where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePerson2OrganisationLink(op, link, pipeLink, claim, ref success);
        }

        public Person2Organisation InsertPerson2OrganisationLink(int personId, int organisationId, 
                                    MDA.Pipeline.Model.PipelinePerson2OrganisationLink pipeLink, RiskClaim claim)
        {
            Person2Organisation p2o = _db.Person2Organisation.Create();

            p2o.CreatedBy         = pipeLink.CreatedBy;
            p2o.CreatedDate       = pipeLink.CreatedDate;
            p2o.ModifiedBy        = pipeLink.ModifiedBy;
            p2o.ModifiedDate      = pipeLink.ModifiedDate;
            p2o.Source            = pipeLink.Source;
            p2o.SourceDescription = pipeLink.SourceDescription;
            p2o.SourceReference   = pipeLink.SourceReference;
            p2o.ADARecordStatus   = pipeLink.ADARecordStatus;
            p2o.RecordStatus      = pipeLink.RecordStatus;
            p2o.LinkConfidence    = pipeLink.LinkConfidence;

            p2o.RiskClaim_Id = claim.Id;
            p2o.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;


            p2o.Person_Id = personId;
            p2o.Organisation_Id = organisationId;
            p2o.Person2OrganisationLinkType_Id = (int)pipeLink.Person2OrganisationLinkType_Id;
            
            p2o.FiveGrading = GetFiveGrading();

            _db.Person2Organisation.Add(p2o);

            _db.SaveChanges();

            return p2o;
        }

        //public Person2Organisation InsertPerson2OrganisationLink(Person2Organisation person2org)
        //{
        //    Person2Organisation p2o = _db.Person2Organisation.Create();

        //    p2o.CreatedBy = person2org.CreatedBy;
        //    p2o.CreatedDate = DateTime.Now;
        //    p2o.BaseRiskClaim_Id = person2org.BaseRiskClaim_Id;

        //    p2o.Source = person2org.Source;
        //    p2o.SourceDescription = person2org.SourceDescription;
        //    p2o.SourceReference = person2org.SourceReference;

        //    p2o.Organisation_Id = person2org.Organisation_Id;
        //    p2o.Person2OrganisationLinkId = person2org.Person2OrganisationLinkId;
        //    p2o.Person2OrganisationLinkType_Id = person2org.Person2OrganisationLinkType_Id;

        //    p2o.IBaseId = person2org.IBaseId;
        //    p2o.Person_Id = person2org.Person_Id;
        //    p2o.RiskClaim_Id = person2org.RiskClaim_Id;
        //    p2o.LinkConfidence = person2org.LinkConfidence;
        //    p2o.FiveGrading = person2org.FiveGrading;

        //    _db.Person2Organisation.Add(p2o);

        //    _db.SaveChanges();

        //    return p2o;
        //}

    }

}
