﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllPerson2EmailAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2Email set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectEmail2PersonLinks(int masterEmailId, int emailId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Email set Email_Id = " + masterEmailId +
                                           " where Email_Id = " + emailId);
        }

        public void RedirectPerson2EmailLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Email set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        public List<Person2Email> GetPerson2EmailLinks(int personId, int emailId, int baseRiskClaimId)
        {
            return (from x in _db.Person2Email
                    where x.Person_Id == personId && x.Email_Id == emailId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Person2Email GetLatestPerson2EmailLink(int personId, int emailId, int baseRiskClaimId)
        {
            return (from x in _db.Person2Email
                    where x.Person_Id == personId && x.Email_Id == emailId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPerson2EmailLinks(/*Person2Email latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Person2Email
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Person2Email Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Person2Email UpdatePerson2EmailLink(MDA.Common.Enum.Operation op, Person2Email link,
                                                            MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeBase.LinkConfidence)
                            link.LinkConfidence = (int)pipeBase.LinkConfidence;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Person2Email UpdatePerson2EmailLink(MDA.Common.Enum.Operation op, int linkId,
                                               MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Person2Email where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePerson2EmailLink(op, link, pipeBase, claim, ref success);
        }

        public Person2Email InsertPerson2EmailLink(int personId, int emailId, MDA.Pipeline.Model.PipeLinkBase pipeBase, RiskClaim claim)
        {
            Person2Email p2e = _db.Person2Email.Create();

            p2e.CreatedBy         = pipeBase.CreatedBy;
            p2e.CreatedDate       = pipeBase.CreatedDate;
            p2e.ModifiedBy        = pipeBase.ModifiedBy;
            p2e.ModifiedDate      = pipeBase.ModifiedDate;
            p2e.Source            = pipeBase.Source;
            p2e.SourceDescription = pipeBase.SourceDescription;
            p2e.SourceReference   = pipeBase.SourceReference;
            p2e.ADARecordStatus   = pipeBase.ADARecordStatus;
            p2e.RecordStatus      = pipeBase.RecordStatus;
            p2e.LinkConfidence    = pipeBase.LinkConfidence;

            p2e.RiskClaim_Id     = claim.Id;
            p2e.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2e.Email_Id  = emailId;
            p2e.Person_Id = personId;

            p2e.FiveGrading = GetFiveGrading();

            _db.Person2Email.Add(p2e);

            _db.SaveChanges();

            return p2e;
        }

        //public Person2Email InsertPerson2EmailLink(Person2Email person2email)
        //{
        //    Person2Email p2e = _db.Person2Email.Create();

        //    p2e.CreatedBy = person2email.CreatedBy;
        //    p2e.CreatedDate = DateTime.Now;
        //    p2e.BaseRiskClaim_Id = person2email.BaseRiskClaim_Id;

        //    p2e.Source = person2email.Source;
        //    p2e.SourceDescription = person2email.SourceDescription;
        //    p2e.SourceReference = person2email.SourceReference;

        //    p2e.Email_Id = person2email.Email_Id;
        //    p2e.EmailLinkId = person2email.EmailLinkId;

        //    p2e.IBaseId = person2email.IBaseId;
        //    p2e.Person_Id = person2email.Person_Id;
        //    p2e.RiskClaim_Id = person2email.RiskClaim_Id;
        //    p2e.LinkConfidence = person2email.LinkConfidence;
        //    p2e.FiveGrading = person2email.FiveGrading;

        //    _db.Person2Email.Add(p2e);

        //    _db.SaveChanges();

        //    return p2e;
        //}
    }

}
