﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        public void RedirectVehicle2IncidentLinks(int masterVehicleId, int vehicleId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2Vehicle set Vehicle_Id = " + masterVehicleId +
                                           " where Vehicle_Id = " + vehicleId);
        }

        public void RedirectIncident2VehicleLinks(int masterIncidentId, int incidentId)
        {
            _db.Database.ExecuteSqlCommand("update Incident2Vehicle set Incident_Id = " + masterIncidentId +
                                           " where Incident_Id = " + incidentId);
        }

        //public void ResetAllIncident2VehicleLinkAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Incident2Vehicle set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public List<Incident2Vehicle> GetIncident2VehicleLinks(int incidentId, int vehicleId, int baseRiskClaimId)
        {
            return (from x in _db.Incident2Vehicle
                    where x.Incident_Id == incidentId && x.Vehicle_Id == vehicleId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Incident2Vehicle GetLatestIncident2VehicleLink(int incidentId, int vehicleId, int baseRiskClaimId)
        {
            return (from x in _db.Incident2Vehicle
                    where x.Incident_Id == incidentId && x.Vehicle_Id == vehicleId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpIncident2VehicleLinks(/*Incident2Vehicle latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all I2V links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Incident2Vehicle
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Incident2Vehicle Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Incident2Vehicle UpdateIncident2VehicleLink(MDA.Common.Enum.Operation op, Incident2Vehicle link,
                                                    MDA.Pipeline.Model.PipelineIncident2VehicleLink pipeLink,
                                                    //MDA.Common.Enum.VehicleCategoryOfLoss catLoss, string damageDesc, 
                                                    //MDA.Common.Enum.Incident2VehicleLinkType linkType,
                                                    RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {
 
                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.Incident2VehicleLinkType_Id != (int)pipeLink.Incident2VehicleLinkType_Id)
                            link.Incident2VehicleLinkType_Id = (int)pipeLink.Incident2VehicleLinkType_Id;

                        if (link.VehicleCategoryOfLoss_Id != (int)pipeLink.VehicleCategoryOfLoss_Id)
                            link.VehicleCategoryOfLoss_Id = (int)pipeLink.VehicleCategoryOfLoss_Id;

                        if (link.DamageDescription != pipeLink.DamageDescription)
                            link.DamageDescription = pipeLink.DamageDescription;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;
        }


        public Incident2Vehicle UpdateIncident2VehicleLink(
                                            MDA.Common.Enum.Operation op, int linkId,
                                            //MDA.Common.Enum.VehicleCategoryOfLoss catLoss, string damageDesc, 
                                            //MDA.Common.Enum.Incident2VehicleLinkType linkType,
                                            MDA.Pipeline.Model.PipelineIncident2VehicleLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Incident2Vehicle where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateIncident2VehicleLink(op, link, pipeLink, /*catLoss, damageDesc, linkType,*/ claim, ref success);
        }


        public Incident2Vehicle InsertIncident2VehicleLink(int incidentId, int vehicleId,
                                                    MDA.Pipeline.Model.PipelineIncident2VehicleLink pipeLink,
                                                    //MDA.Common.Enum.VehicleCategoryOfLoss catLoss, 
                                                    //string damageDesc, 
                                                    //MDA.Common.Enum.Incident2VehicleLinkType linkType, 
                                                    RiskClaim claim)
        {
            Incident2Vehicle i2v = _db.Incident2Vehicle.Create();

            i2v.CreatedBy         = pipeLink.CreatedBy;
            i2v.CreatedDate       = pipeLink.CreatedDate;
            i2v.ModifiedBy        = pipeLink.ModifiedBy;
            i2v.ModifiedDate      = pipeLink.ModifiedDate;
            i2v.Source            = pipeLink.Source;
            i2v.SourceDescription = pipeLink.SourceDescription;
            i2v.SourceReference   = pipeLink.SourceReference;
            i2v.ADARecordStatus   = pipeLink.ADARecordStatus;
            i2v.RecordStatus      = pipeLink.RecordStatus;
            i2v.LinkConfidence    = pipeLink.LinkConfidence;

            i2v.RiskClaim_Id = claim.Id;
            i2v.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            i2v.Vehicle_Id = vehicleId;
            i2v.Incident_Id = incidentId;

            i2v.VehicleCategoryOfLoss_Id = (int)pipeLink.VehicleCategoryOfLoss_Id; // catLoss;
            i2v.DamageDescription = pipeLink.DamageDescription;
            i2v.Incident2VehicleLinkType_Id = (int)pipeLink.Incident2VehicleLinkType_Id;

            i2v.FiveGrading = GetFiveGrading();

            _db.Incident2Vehicle.Add(i2v);

            _db.SaveChanges();

            return i2v;
        }

        //public Incident2Vehicle InsertIncident2VehicleLink(Incident2Vehicle incident2vehicle)
        //{
        //    Incident2Vehicle i2v = _db.Incident2Vehicle.Create();

        //    i2v.CreatedBy = incident2vehicle.CreatedBy;
        //    i2v.CreatedDate = DateTime.Now;
        //    i2v.BaseRiskClaim_Id = incident2vehicle.BaseRiskClaim_Id;
        //    i2v.DamageDescription = incident2vehicle.DamageDescription;

        //    i2v.Source = incident2vehicle.Source;
        //    i2v.SourceDescription = incident2vehicle.SourceDescription;
        //    i2v.SourceReference = incident2vehicle.SourceReference;
        //    i2v.IBaseId = incident2vehicle.IBaseId;

        //    i2v.Incident_Id = incident2vehicle.Incident_Id;
        //    i2v.Incident2VehicleLinkType_Id = incident2vehicle.Incident2VehicleLinkType_Id;
        //    i2v.Vehicle_Id = incident2vehicle.Vehicle_Id;
        //    i2v.VehicleCategoryOfLoss_Id = incident2vehicle.VehicleCategoryOfLoss_Id;
        //    i2v.VehicleIncidentLinkId = incident2vehicle.VehicleIncidentLinkId;
        //    i2v.LinkConfidence = incident2vehicle.LinkConfidence;
        //    i2v.FiveGrading = incident2vehicle.FiveGrading;

        //    i2v.RiskClaim_Id = incident2vehicle.RiskClaim_Id;

        //    _db.Incident2Vehicle.Add(i2v);

        //    _db.SaveChanges();

        //    return i2v;
        //}
    }

}
