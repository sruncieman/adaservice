﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;


namespace MDA.DataService
{
    public partial class DataServices
    {

        //public void ResetAllPerson2AddressAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Person2Address set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectAddress2PersonLinks(int masterAddressId, int addressId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Address set Address_Id = " + masterAddressId +
                                           " where Address_Id = " + addressId);
        }

        public void RedirectPerson2AddressLinks(int masterPersonId, int personId)
        {
            _db.Database.ExecuteSqlCommand("update Person2Address set Person_Id = " + masterPersonId +
                                           " where Person_Id = " + personId);
        }

        public Person2Address GetPerson2AddressLink(MdaDbContext db, int id)
        {
            return (from x in db.Person2Address where x.Id == id select x).FirstOrDefault();
        }

        public List<Person2Address> GetPerson2AddressLinks(int personId, int addressId, int baseRiskClaimId = -1)
        {
            if (baseRiskClaimId == -1)
            {
                return (from x in _db.Person2Address
                        where x.Person_Id == personId && x.Address_Id == addressId && x.BaseRiskClaim_Id == baseRiskClaimId
                        orderby x.RiskClaim_Id ascending
                        select x).ToList();
            }
            else
            {
                return (from x in _db.Person2Address
                        where x.Person_Id == personId && x.Address_Id == addressId
                        orderby x.RiskClaim_Id ascending
                        select x).ToList();
            }
        }

        public Person2Address GetLatestPerson2AddressLink(int personId, int addressId, int baseRiskClaimId)
        {
            return (from x in _db.Person2Address
                    where x.Person_Id == personId && x.Address_Id == addressId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpPerson2AddressLinks(RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Person2Address
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Person2Address Link[" + link.Id.ToString() + "] marked as UpdateRemoved");


                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Person2Address UpdatePerson2AddressLink(MDA.Common.Enum.Operation op, Person2Address link,
                                            //MDA.Common.Enum.AddressLinkType linkType, DateTime? startOfResidency, DateTime? endOfResidency,
                                            MDA.Pipeline.Model.PipelineAddressLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {

            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    // check for invalid updates and if so set success = false : NONE

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.AddressLinkType_Id != (int)pipeLink.AddressLinkType_Id)
                            link.AddressLinkType_Id = (int)pipeLink.AddressLinkType_Id;

                        if (link.StartOfResidency != MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.StartOfResidency))
                            link.StartOfResidency = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.StartOfResidency);

                        if (link.EndOfResidency != MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.EndOfResidency))
                            link.EndOfResidency = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.EndOfResidency);

                        link.RawAddress = pipeLink.RawAddress;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception)
            {
                success = false;
            }

            return link;

        }


        public Person2Address UpdatePerson2AddressLink(MDA.Common.Enum.Operation op, int linkId,
                                           //MDA.Common.Enum.AddressLinkType linkType, DateTime? startOfResidency, DateTime? endOfResidency,
                                           MDA.Pipeline.Model.PipelineAddressLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Person2Address where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdatePerson2AddressLink(op, link, /*linkType, startOfResidency, endOfResidency,*/ pipeLink, claim, ref success);
        }


        public Person2Address InsertPerson2AddressLink(int personId, int addressId, MDA.Pipeline.Model.PipelineAddressLink pipeLink, 
            //MDA.Common.Enum.AddressLinkType linkType, DateTime? startOfResidency, DateTime? endOfResidency, 
            RiskClaim claim)
        {
            Person2Address p2a = _db.Person2Address.Create();

            p2a.CreatedBy         = pipeLink.CreatedBy;
            p2a.CreatedDate       = pipeLink.CreatedDate;
            p2a.ModifiedBy        = pipeLink.ModifiedBy;
            p2a.ModifiedDate      = pipeLink.ModifiedDate;
            p2a.Source            = pipeLink.Source;
            p2a.SourceDescription = pipeLink.SourceDescription;
            p2a.SourceReference   = pipeLink.SourceReference;
            p2a.ADARecordStatus   = pipeLink.ADARecordStatus;
            p2a.RecordStatus      = pipeLink.RecordStatus;
            p2a.LinkConfidence    = pipeLink.LinkConfidence;

            p2a.RiskClaim_Id = claim.Id;
            p2a.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            p2a.Person_Id = personId;
            p2a.Address_Id = addressId;
            p2a.AddressLinkType_Id = (int)pipeLink.AddressLinkType_Id;
            p2a.EndOfResidency = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.EndOfResidency);
            p2a.StartOfResidency = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDate(pipeLink.StartOfResidency);
            p2a.RawAddress = pipeLink.RawAddress;

            p2a.FiveGrading = GetFiveGrading();

            _db.Person2Address.Add(p2a);

            _db.SaveChanges();

            return p2a;
        }

        //public Person2Address InsertPerson2AddressLink(Person2Address person2address)
        //{
        //    Person2Address p2a = _db.Person2Address.Create();

        //    p2a.CreatedBy = person2address.CreatedBy;
        //    p2a.CreatedDate = DateTime.Now;
        //    p2a.BaseRiskClaim_Id = person2address.BaseRiskClaim_Id;

        //    p2a.Source = person2address.Source;
        //    p2a.SourceDescription = person2address.SourceDescription;
        //    p2a.SourceReference = person2address.SourceReference;

        //    p2a.Address_Id = person2address.Address_Id;
        //    p2a.AddressLinkId = person2address.AddressLinkId;
        //    p2a.AddressLinkType_Id = person2address.AddressLinkType_Id;
        //    p2a.EndOfResidency = person2address.EndOfResidency;
        //    p2a.IBaseId = person2address.IBaseId;
        //    p2a.Notes = person2address.Notes;
        //    p2a.Person_Id = person2address.Person_Id;
        //    p2a.RiskClaim_Id = person2address.RiskClaim_Id;
        //    p2a.StartOfResidency = person2address.StartOfResidency;
        //    p2a.LinkConfidence = person2address.LinkConfidence;
        //    p2a.FiveGrading = person2address.FiveGrading;

        //    _db.Person2Address.Add(p2a);

        //    _db.SaveChanges();

        //    return p2a;
        //}
    }

}
