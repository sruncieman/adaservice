﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Server;
using MDA.Common.Debug;

namespace MDA.DataService
{
    public partial class DataServices
    {
        //public void ResetAllVehicle2OrganisationAdaStatus()
        //{
        //    _db.Database.ExecuteSqlCommand("update Vehicle2Organisation set ADARecordStatus = ADARecordStatus - 10 where ADARecordStatus >= 10");
        //}

        public void RedirectVehicle2OrganisationLinks(int masterVehicleId, int vehicleId)
        {
            _db.Database.ExecuteSqlCommand("update Vehicle2Organisation set Vehicle_Id = " + masterVehicleId +
                                           " where Vehicle_Id = " + vehicleId);
        }

        public void RedirectOrganisation2VehicleLinks(int masterOrganisationId, int organisationId)
        {
            _db.Database.ExecuteSqlCommand("update Vehicle2Organisation set Organisation_Id = " + masterOrganisationId +
                                           " where Organisation_Id = " + organisationId);
        }

        public List<Vehicle2Organisation> GetOrgVehicle2OrganisationLinks(int vehicleId, int organisationId, int baseRiskClaimId)
        {
            return (from x in _db.Vehicle2Organisation
                    where x.Vehicle_Id == vehicleId && x.Organisation_Id == organisationId && x.BaseRiskClaim_Id == baseRiskClaimId
                    orderby x.RiskClaim_Id ascending
                    select x).ToList();
        }

        public Vehicle2Organisation GetLatestOrgVehicle2OrganisationLink(int vehicleId, int organisationId, int baseRiskClaimId)
        {
            return (from x in _db.Vehicle2Organisation
                    where x.Vehicle_Id == vehicleId && x.Organisation_Id == organisationId &&
                          x.BaseRiskClaim_Id == baseRiskClaimId &&
                          x.ADARecordStatus == (byte)ADARecordStatus.Current
                    select x).FirstOrDefault();
        }

        public void CleanUpOrgVehicle2OrganisationLinks(/*Vehicle2Organisation latestLink,*/ RiskClaim newRiskClaim)
        {
            // Get all O2P links for same BaseId but has an old ClaimId
            var oldlinks = (from x in _db.Vehicle2Organisation
                            where x.BaseRiskClaim_Id == newRiskClaim.BaseRiskClaim_Id &&
                                  x.RiskClaim_Id != newRiskClaim.Id &&
                                  x.ADARecordStatus == (byte)ADARecordStatus.Current
                            select x).ToList();

            if (oldlinks.Count() == 0) return;

            foreach (var link in oldlinks)
            {
                if (_trace)
                    ADATrace.WriteLine("Vehicle2Organisation Link[" + link.Id.ToString() + "] marked as UpdateRemoved");

                link.RecordStatus = Constants.IBaseDeleteValue;
                link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
            }
            _db.SaveChanges();
        }

        public Vehicle2Organisation UpdateOrgVehicle2OrganisationLink(MDA.Common.Enum.Operation op, Vehicle2Organisation link,
                                           //MDA.Common.Enum.VehicleLinkType linkType,
                                           //DateTime? hireStartDate, DateTime? hireEndDate, string hireCompany,
                                           MDA.Pipeline.Model.PipelineOrgVehicleLink pipeLink, RiskClaim newRiskClaim, ref bool success)
        {
            success = true;

            try
            {
                if (op == MDA.Common.Enum.Operation.Delete)
                {
                    //_db.Incident2Vehicle.Remove(link);

                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientDeleted;
                }
                else if (op == MDA.Common.Enum.Operation.Withdrawn)
                {
                    link.RecordStatus = Constants.IBaseDeleteValue;
                    link.ADARecordStatus = (byte)ADARecordStatus.ClientWithdrawn;
                }
                else
                {

                    //hireStartDate = MDA.Common.Helpers.DateHelper.ConvertToSqlSmallDateTime(pipeLink.HireStartDate);

                    // check for invalid updates and if so set success = false : NONE
                    if (link.HireStartDate != null && link.HireStartDate != pipeLink.HireStartDate) // we have value and it's different
                    {
                        success = false;
                    }

                    if (!success)  // we know this will trigger an INSERT in the calling function to replace this link
                    {
                        link.RecordStatus = Constants.IBaseDeleteValue;
                        link.ADARecordStatus = (byte)ADARecordStatus.UpdateRemoved;
                    }
                    else
                    {
                        link.RiskClaim_Id = newRiskClaim.Id;

                        if (link.LinkConfidence != (int)pipeLink.LinkConfidence)
                            link.LinkConfidence = (int)pipeLink.LinkConfidence;

                        if (link.HireStartDate != pipeLink.HireStartDate)
                            link.HireStartDate = pipeLink.HireStartDate;

                        if (link.HireEndDate != pipeLink.HireEndDate)
                            link.HireEndDate = pipeLink.HireEndDate;

                        if (string.Compare(link.HireCompany, pipeLink.HireCompany, true) != 0)
                            link.HireCompany = pipeLink.HireCompany;
                    }

                }

                link.ModifiedDate = DateTime.Now;
                link.ModifiedBy = _ctx.Who;

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                success = false;

                throw ex;
            }

            return link;

        }


        public Vehicle2Organisation UpdateOrgVehicle2OrganisationLink(MDA.Common.Enum.Operation op, int linkId,
                                           //MDA.Common.Enum.VehicleLinkType linkType,
                                           //DateTime? hireStartDate, DateTime? hireEndDate, string hireCompany, 
                                            MDA.Pipeline.Model.PipelineOrgVehicleLink pipeLink, RiskClaim claim, ref bool success)
        {
            success = false;

            var link = (from x in _db.Vehicle2Organisation where x.Id == linkId select x).FirstOrDefault();

            if (link == null) return link;

            return UpdateOrgVehicle2OrganisationLink(op, link, /*linkType, hireStartDate, hireEndDate, hireCompany, */ pipeLink, claim, ref success);
        }

        public Vehicle2Organisation InsertOrgVehicle2OrganisationLink(int orgVehicleId, int organisationId,
                                    MDA.Pipeline.Model.PipelineOrgVehicleLink pipeLink, 
                                    //MDA.Common.Enum.VehicleLinkType linkType, DateTime? hireStartDate, DateTime? hireEndDate, string hireCompany, 
                                    RiskClaim claim)
        {
            Vehicle2Organisation v2o = _db.Vehicle2Organisation.Create();

            v2o.CreatedBy         = pipeLink.CreatedBy;
            v2o.CreatedDate       = pipeLink.CreatedDate;
            v2o.ModifiedBy        = pipeLink.ModifiedBy;
            v2o.ModifiedDate      = pipeLink.ModifiedDate;
            v2o.Source            = pipeLink.Source;
            v2o.SourceDescription = pipeLink.SourceDescription;
            v2o.SourceReference   = pipeLink.SourceReference;
            v2o.ADARecordStatus   = pipeLink.ADARecordStatus;
            v2o.RecordStatus      = pipeLink.RecordStatus;
            v2o.LinkConfidence    = pipeLink.LinkConfidence;

            v2o.RiskClaim_Id = claim.Id;
            v2o.BaseRiskClaim_Id = claim.BaseRiskClaim_Id;

            v2o.Organisation_Id = organisationId;
            v2o.Vehicle_Id = orgVehicleId;
            v2o.VehicleLinkType_Id = (int)pipeLink.VehicleLinkType_Id;
            v2o.HireStartDate = pipeLink.HireStartDate;
            v2o.HireEndDate = pipeLink.HireEndDate;
            
            v2o.FiveGrading = GetFiveGrading();
            v2o.HireCompany = pipeLink.HireCompany;

            _db.Vehicle2Organisation.Add(v2o);

            _db.SaveChanges();

            return v2o;
        }

        //public Vehicle2Organisation InsertVehicle2OrganisationLink(Vehicle2Organisation vehicle2org)
        //{
        //    Vehicle2Organisation v2o = _db.Vehicle2Organisation.Create();

        //    v2o.CreatedBy = vehicle2org.CreatedBy;
        //    v2o.CreatedDate = DateTime.Now;
        //    v2o.BaseRiskClaim_Id = vehicle2org.BaseRiskClaim_Id;

        //    v2o.Source = vehicle2org.Source;
        //    v2o.SourceDescription = vehicle2org.SourceDescription;
        //    v2o.SourceReference = vehicle2org.SourceReference;

        //    v2o.CrossHireCompany = vehicle2org.CrossHireCompany;
        //    v2o.HireCompany = vehicle2org.HireCompany;
        //    v2o.HireEndDate = vehicle2org.HireEndDate;
        //    v2o.HireStartDate = vehicle2org.HireStartDate;
        //    v2o.Organisation_Id = vehicle2org.Organisation_Id;
        //    v2o.RegKeeperEndDate = vehicle2org.RegKeeperEndDate;
        //    v2o.RegKeeperStartDate = vehicle2org.RegKeeperStartDate;
        //    v2o.VehicleLinkId = vehicle2org.VehicleLinkId;
        //    v2o.VehicleLinkType_Id = vehicle2org.VehicleLinkType_Id;
        //    v2o.Vehicle_Id = vehicle2org.Vehicle_Id;

        //    v2o.IBaseId = vehicle2org.IBaseId;
        //    v2o.RiskClaim_Id = vehicle2org.RiskClaim_Id;
        //    v2o.LinkConfidence = vehicle2org.LinkConfidence;
        //    v2o.FiveGrading = vehicle2org.FiveGrading;

        //    _db.Vehicle2Organisation.Add(v2o);

        //    _db.SaveChanges();

        //    return v2o;
        //}
    }

}
