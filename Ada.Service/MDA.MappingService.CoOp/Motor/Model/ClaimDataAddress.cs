﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CoOp.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClaimDataAddress
    {
        [FieldOptional()][FieldIgnored]
        private String ClaimantName;
        [FieldOptional()]
        [FieldIgnored]
        private String Claimant_Title;
        [FieldOptional()]
        [FieldIgnored]
        private String Claimant_Forename;
        [FieldOptional()]
        [FieldIgnored]
        private String Claimant_SurnameOrCompany;
        [FieldOptional()]
        public String Claimant_Address1;
        [FieldOptional()]
        public String Claimant_Address2;
        [FieldOptional()]
        public String Claimant_Address3;
        [FieldOptional()]
        public String Claimant_Address4;
        [FieldOptional()]
        public String Claimant_Postcode;
        [FieldOptional()]
        private String Claimant_Telephone;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldOptional()]
        private DateTime? DOB;
        [FieldOptional()]
        private String NiNumber;
        [FieldOptional()]
        private String Claimant_Insurer;
        [FieldOptional()]
        private String Claimant_Solicitors_Ref;
        [FieldOptional()]
        private String Claimant_Solicitors;
        [FieldOptional()]
        private String Claimant_VehicleReg;
        [FieldOptional()]
        private String Claimant_VehicleMake;
        [FieldOptional()]
        private String Claimant_VehicleModel;
        [FieldOptional()]
        private String Client_Name;
        [FieldOptional()]
        private String Client_Reference;
        [FieldOptional()]
        private String CHOName;
        [FieldOptional()]
        private String CHOType;
        [FieldOptional()]
        private String CHORef;
        [FieldOptional()]
        public String CHOAddressName;
         [FieldOptional()]
        public String CHOAddressNum;
         [FieldOptional()]
        public String CHOAddress1;
         [FieldOptional()]
        public String CHOAddress2;
         [FieldOptional()]
        public String CHOAddress3;
         [FieldOptional()]
        public String CHOPostcode;
        [FieldOptional()]
        private String CHOEmail;
        [FieldOptional()]
        private String CHOTel;
        [FieldOptional()]
        private String CHOContact;
        [FieldOptional()]
        private String VehicleClaimType;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldOptional()]
        private DateTime? HireStartDate;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldOptional()]
        private DateTime? HireEndDate;
        [FieldOptional()]
        private String ReasonForDiscontinuance;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldOptional()]
        private DateTime? DateOfDisputeResolution;
        [FieldOptional()]
        private String MethodOfResolution;
        [FieldOptional()]
        private String HireExAdminFee;
        [FieldOptional()]
        private String DriverTitle;
        [FieldOptional()]
        private String DriverForename;
        [FieldOptional()]
        private String DriverSurname;
         [FieldOptional()]
        public String DriverAddr1;
         [FieldOptional()]
        public String DriverAddr2;
         [FieldOptional()]
        public String DriverAddr3;
         [FieldOptional()]
        public String DriverAddr4;
         [FieldOptional()]
        public String DriverPostcode;
        [FieldOptional()]
        private String HireVehicleReg;
        [FieldOptional()]
        private String HireVehicleMake;
        [FieldOptional()]
        private String HireVehicleModel;
        [FieldOptional()]
        private String NameOfInsured;
        [FieldOptional()]
        private String InsuredTitle;
        [FieldOptional()]
        private String InsuredForename;
        [FieldOptional()]
        private String InsuredSurname;
         [FieldOptional()]
        public String InsuredAddr1;
         [FieldOptional()]
        public String InsuredAddr2;
         [FieldOptional()]
        public String InsuredAddr3;
         [FieldOptional()]
        public String InsuredAddr4;
         [FieldOptional()]
        public String InsuredPostcode;
        [FieldOptional()]
        private String InsuredVehicleReg;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldOptional()]
        private DateTime? InsuredDOB;
        [FieldOptional()]
        private String InsurerClientName;
        [FieldOptional()]
        private String InsurerClientRef;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldOptional()]
        private DateTime? AccDate;
         [FieldOptional()]
        public int MatterNum;
        [FieldOptional()]
        private String FeeEarner;
        [FieldOptional()]
        private String Engineer;
        [FieldOptional()]
        private String Repairer;
        [FieldOptional()]
        private String RecoveryAgent;
    }
}
