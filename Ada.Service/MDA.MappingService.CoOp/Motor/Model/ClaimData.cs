﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CoOp.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Title;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Forename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_SurnameOrCompany;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Address1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Address2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Address3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Address4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Telephone;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? DOB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NiNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Insurer;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Solicitors_Ref;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Solicitors;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_VehicleReg;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_VehicleMake;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_VehicleModel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Client_Name;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Client_Reference;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHOName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHOType;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHORef;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHOAddressName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHOAddressNum;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHOAddress1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHOAddress2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHOAddress3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHOPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHOEmail;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHOTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CHOContact;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleClaimType;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? HireStartDate;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? HireEndDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ReasonForDiscontinuance;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? DateOfDisputeResolution;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MethodOfResolution;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireExAdminFee;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DriverTitle;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DriverForename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DriverSurname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DriverAddr1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DriverAddr2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DriverAddr3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DriverAddr4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DriverPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehicleReg;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehicleMake;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HireVehicleModel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NameOfInsured;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredTitle;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredForename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredSurname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredAddr1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredAddr2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredAddr3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredAddr4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredVehicleReg;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? InsuredDOB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsurerClientName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsurerClientRef;
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? AccDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? MatterNum;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String FeeEarner;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Engineer;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Repairer;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RecoveryAgent;
    }
}
