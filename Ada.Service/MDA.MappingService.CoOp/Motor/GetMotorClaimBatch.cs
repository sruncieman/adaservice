﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FileHelpers;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.Common.Helpers;
using MDA.MappingService.CoOp.Motor.Model;
using MDA.Common.Server;
using MDA.Common;

namespace MDA.MappingService.CoOp.Motor
{
    public class GetMotorClaimBatch
    {
        #region Initialise Fields
        private string _claimDataFile;
        private FileHelperEngine _claimDataEngine;
        //private FileHelperEngine _claimDataAddressEngine;
        private ClaimData[] _claimData;
        //private ClaimDataAddress[] _claimAddressData;
        //private PipelineClaimBatch _claimBatch;
        //private List<ClaimAddress> _TPAddresses;
        private List<ClaimData> _filteredClaimData;
        private List<ClaimData> _duplicateClaimData;
        private List<int?> _duplicateMatterClaims;
        private StreamReader _claimDataFileStreamReader;
        //private string _connectionString;
        private bool _debug;
        private List<string> _PrevInsurerClientRef;

        #endregion

        public GetMotorClaimBatch()
        {
            //_claimBatch = new PipelineClaimBatch();
            _filteredClaimData = new List<ClaimData>();
            _duplicateMatterClaims = new List<int?>();
            _duplicateClaimData = new List<ClaimData>();

            _PrevInsurerClientRef = new List<string>();
        }

        public void RetrieveMotorClaimBatch(CurrentContext ctx, Stream filem, string clientFolder, object statusTracking,
                        Func<CurrentContext, MDA.Pipeline.Model.PipelineMotorClaim, object, int> ProcessClaimFn,
                       ProcessingResults processingResults)
        {
            #region Files
            if (filem == null)
            {
                _claimDataFile = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Co-Op\Resource\Test Data.csv";
                _debug = true;
            }
            else
            {
                try
                {
                    _claimDataFileStreamReader = new StreamReader(filem);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in creating csv file: " + ex);
                }

            }
            #endregion

            #region Initialise FileHelper Engine
            try
            {
                _claimDataEngine = new FileHelperEngine(typeof(ClaimData));
                //_claimDataAddressEngine = new FileHelperEngine(typeof(ClaimDataAddress));
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
            #endregion

            #region Populate FileHelper Engine with data from file
           
            try
            {
                if (filem == null)
                {
                    _claimData = _claimDataEngine.ReadFile(_claimDataFile) as ClaimData[];
                    //_claimAddressData = _claimDataAddressEngine.ReadFile(_claimDataFile) as ClaimDataAddress[];
                }
                else
                {
                    _claimData = _claimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in reading csv file via filehelper engine: " + ex);
            }

            #endregion

            #region Format Addresses
            //Address claimantAddress = new Address();
            
            //List<ClaimAddress> _claimDataAddressesTemp = new List<ClaimAddress>();
            //foreach (var claim in _claimData)
            //{
            //    if (!string.IsNullOrEmpty(claim.Claimant_Address1))
            //        claimantAddress.Street = claim.Claimant_Address1;

            //    if (!string.IsNullOrEmpty(claim.Claimant_Address2))
            //        claimantAddress.Street += " " + claim.Claimant_Address2;

            //    if (!string.IsNullOrEmpty(claim.Claimant_Address3))
            //        claimantAddress.Street += " " + claim.Claimant_Address3;

            //    if (!string.IsNullOrEmpty(claim.Claimant_Address4))
            //        claimantAddress.Street += " " + claim.Claimant_Address4;

            //    if (!string.IsNullOrEmpty(claim.Claimant_Postcode))
            //        claimantAddress.Street += " " + claim.Claimant_Postcode;


            //    if (!string.IsNullOrEmpty(claim.Claimant_Postcode))
            //        claimantAddress.PostCode = claim.Claimant_Postcode;

            //    //if (!string.IsNullOrEmpty(claimantAddress.Street) && _debug)
            //    //{
            //    //    _claimDataAddressesTemp.Add(new ClaimAddress { Street = claimantAddress.Street, AddressMatterNumber = claim.MatterNum});
            //    //}
            //}

            ////_TPAddresses = FormatAddress(_claimDataAddressesTemp);
            #endregion


            #region Find Duplicate Matter Records

            try
            {
                if (_claimData != null)
                {
                    var duplicateMatters = _claimData
                        .GroupBy(i => i.MatterNum)
                        .Where(g => g.Count() > 1)
                        .Select(g => g.Key);

                    foreach (var duplicateMatter in duplicateMatters)
                    {
                        _duplicateMatterClaims.Add(duplicateMatter);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in finding duplicate matter records: " + ex);
            }


            #endregion

            #region Filter Records

            try
            {

                //foreach (ClaimData claimData in _claimData.Where(x => x.InsurerClientRef.ToUpper() == "232724912M" || x.InsurerClientRef.ToUpper() == "S632UEY" || x.InsurerClientRef.ToUpper() == "041824413M" || x.InsurerClientRef.ToUpper() == "232334712M" || x.InsurerClientRef.ToUpper() == "042503413F"))
                foreach (ClaimData claimData in _claimData)
                {
                    if (!_duplicateMatterClaims.Contains(claimData.MatterNum) &&
                        claimData.MethodOfResolution != "Transfer to Fraud Unit" && claimData.AccDate != null && !string.IsNullOrEmpty(claimData.InsurerClientRef))
                    {
                        _filteredClaimData.Add(claimData);
                    }
                }

                //foreach (ClaimData claimData in _claimData.Where(x => x.InsurerClientRef.ToUpper() == "232724912M" || x.InsurerClientRef.ToUpper() == "S632UEY" || x.InsurerClientRef.ToUpper() == "041824413M" || x.InsurerClientRef.ToUpper() == "232334712M" || x.InsurerClientRef.ToUpper() == "042503413F"))
                foreach (ClaimData claimData in _claimData)
                {
                    if (_duplicateMatterClaims.Contains(claimData.MatterNum) &&
                        claimData.MethodOfResolution != "Transfer to Fraud Unit" && claimData.AccDate != null && !string.IsNullOrEmpty(claimData.InsurerClientRef))
                    {
                        _duplicateClaimData.Add(claimData);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in filtering records: " + ex);
            }


            #endregion

            TranslateToXml(ctx, _filteredClaimData, false, statusTracking, ProcessClaimFn, processingResults);
            TranslateToXml(ctx, _duplicateClaimData, true, statusTracking, ProcessClaimFn, processingResults);

           // return _claimBatch;
        }



        private void TranslateToXml(CurrentContext ctx, List<ClaimData> claimData, bool duplicateMatterClaims, object statusTracking,
                        Func<CurrentContext, MDA.Pipeline.Model.PipelineMotorClaim, object, int> ProcessClaimFn,
                        ProcessingResults processingResults)
        {
            foreach (var claim in claimData)
            {
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                try
                {
                    if (duplicateMatterClaims)
                    {
                        if (_PrevInsurerClientRef.Contains(claim.InsurerClientRef))
                            continue;

                        //if (_claimBatch.Claims.Select(x => x.ClaimNumber).Contains(claim.InsurerClientRef))
                        //{
                        //    continue;
                        //}
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in finding duplicate claim: " + ex);
                }

                #region GeneralClaimData

                try
                {

                    //if (!string.IsNullOrEmpty(claim.InsurerClientRef))
                    //    motorClaim.ClaimNumber = claim.InsurerClientRef;
                    if (claim.MatterNum != null) // Claim number changed from InsurerClientRef to MatterNum - Product Backlog Item 6366
                        motorClaim.ClaimNumber = claim.MatterNum.ToString();

                    motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                    motorClaim.IncidentDate = Convert.ToDateTime(claim.AccDate);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general claim data: " + ex);
                }

                #endregion

                #region ClaimInfo

                try
                {
                    switch (claim.MethodOfResolution.ToUpper())
                    {
                        case "NULL":
                        case "":
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                            break;
                        case "NEGOTIATED SETTLEMENT":
                        case "LITIGATED":
                        case "TRANSFER TO COMPLEX CREDIT HIRE TEAM":
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                            break;
                        case "DISCONTINUED":
                        case "PASS TO ALTERNATIVE SUPPLIER - ALREADY INSTRUCTED":
                        case "PASS TO ALTERNATIVE SUPPLER - ALREADY INSTRUCTED":
                        case "TRANSFER TO ALTERNATIVE SUPPLIER AS NEW INSTRUCTIONS":
                        case "CAPTURED & RETURNED TO INSURER":
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Withdrawn;
                            break;
                        case "REOPENED":
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                            break;
                        case "DELETE":
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Delete;
                            break;
                        default:
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                            break;
                    }


                    if (String.IsNullOrEmpty(claim.MethodOfResolution))
                    {

                        switch (claim.ReasonForDiscontinuance.ToUpper())
                        {
                            case "NULL":
                            case "":
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                break;
                            case "MAKE MATTER INACTIVE":
                            case "NO CLAIM BEING MADE":
                            case "CHO NOT PROVIDING HIRE":
                            case "CLAIM WITHDRAWN ON KEOGHS INVESTIGATION":
                            case "CLAIMANT AT FAULT FROM OUTSET":
                            case "MISTAKEN IDENTITY":
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Withdrawn;
                                break;
                            default:
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                                break;
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating claim info data: " + ex);
                }


                if (!string.IsNullOrEmpty(claim.VehicleClaimType))
                    motorClaim.ExtraClaimInfo.ClaimCode = claim.VehicleClaimType;

                #endregion

                #region Policy

                try
                {

                    if (!string.IsNullOrEmpty(claim.InsurerClientName))
                        motorClaim.Policy.Insurer = claim.InsurerClientName;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general policy data: " + ex);
                }

                #endregion

                #region Claim Organisation

                try
                {

                    if (string.IsNullOrEmpty(claim.Claimant_Title) && string.IsNullOrEmpty(claim.Claimant_Forename))
                    {
                        PipelineOrganisation claimOrg = new PipelineOrganisation();
                        PipelineAddress claimAddress = new PipelineAddress();

                        claimAddress.AddressType_Id = (int)AddressLinkType.TradingAddress;

                        if (!string.IsNullOrEmpty(claim.Claimant_SurnameOrCompany))
                            claimOrg.OrganisationName = claim.Claimant_SurnameOrCompany;

                        if (!string.IsNullOrEmpty(claim.Claimant_Telephone))
                        {
                            claimOrg.Telephones.Add( new PipelineTelephone() 
                            { 
                                ClientSuppliedNumber = claim.Claimant_Telephone.Replace("(Work)", "").Replace(".(Work)", "").Replace(". (Work)", "").Replace(".", "").Trim(), 
                                TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown
                            });

                            //claimOrg.Telephone1 = claim.Claimant_Telephone.Replace("(Work)", "").Replace(".(Work)", "").Replace(". (Work)", "").Replace(".", "").Trim();
                        }


                        if (!string.IsNullOrEmpty(claim.Claimant_Address1))
                            claimAddress.SubBuilding = claim.Claimant_Address1;

                        if (!string.IsNullOrEmpty(claim.Claimant_Address2))
                            claimAddress.Building += " " + claim.Claimant_Address2;

                        if (!string.IsNullOrEmpty(claim.Claimant_Address3))
                            claimAddress.BuildingNumber += " " + claim.Claimant_Address3;

                        if (!string.IsNullOrEmpty(claim.Claimant_Address4))
                            claimAddress.Street += " " + claim.Claimant_Address4;

                        if (!string.IsNullOrEmpty(claim.Claimant_Postcode))
                            claimAddress.PostCode = claim.Claimant_Postcode;

                        if (claimOrg.OrganisationName != null)
                        {
                            if (claimAddress.Street != null || claimAddress.SubBuilding != null || claimAddress.Building != null || claimAddress.BuildingNumber != null || claimAddress.Locality != null || claimAddress.PostCode != null)
                                claimOrg.Addresses.Add(claimAddress);

                            motorClaim.Organisations.Add(claimOrg);
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating claim organisation data: " + ex);
                }


                #endregion

                #region Insured Vehicle

                PipelineVehicle insuredVehicle = new PipelineVehicle();
                PipelinePerson insuredVPerson = new PipelinePerson();
                PipelineAddress insuredVPAddress = new PipelineAddress();

                PipelinePerson insuredDPerson = new PipelinePerson();
                PipelineAddress insuredDPAddress = new PipelineAddress();

                #region Vehicle Details

                try
                {

                    if (!String.IsNullOrEmpty(claim.InsuredVehicleReg))
                    {
                        insuredVehicle.VehicleRegistration = claim.InsuredVehicleReg;
                    }

                    insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general vehicle details data: " + ex);
                }

                #endregion

                #endregion

                #region Insured Vehicle Person

                try
                {

                    if (!String.IsNullOrEmpty(claim.InsuredTitle))
                        insuredVPerson.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.InsuredTitle);

                    insuredVPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    insuredVPerson.Gender_Id = (int)GenderHelper.Salutation2Gender((Salutation)insuredVPerson.Salutation_Id);
                    if (!string.IsNullOrEmpty(claim.InsuredForename))
                        insuredVPerson.FirstName = claim.InsuredForename;

                    if (!string.IsNullOrEmpty(claim.InsuredSurname))
                        insuredVPerson.LastName = claim.InsuredSurname;

                    if (claim.InsuredDOB != null)
                        insuredVPerson.DateOfBirth = claim.InsuredDOB;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured vehicle person details data: " + ex);
                }

                #endregion

                #region Insured Vehicle Person Address


                if (!string.IsNullOrEmpty(claim.InsuredAddr1))
                    insuredVPAddress.SubBuilding = claim.InsuredAddr1;

                if (!string.IsNullOrEmpty(claim.InsuredAddr2))
                    insuredVPAddress.BuildingNumber = " " + claim.InsuredAddr2;

                if (!string.IsNullOrEmpty(claim.InsuredAddr3))
                    insuredVPAddress.Building = " " + claim.InsuredAddr3;

                if (!string.IsNullOrEmpty(claim.InsuredAddr4))
                    insuredVPAddress.Street = " " + claim.InsuredAddr4;

                if (!string.IsNullOrEmpty(claim.InsuredPostcode))
                    insuredVPAddress.PostCode = claim.InsuredPostcode;

                //if (!string.IsNullOrEmpty(insuredVPAddress.Street) && _debug)
                //{
                //    insuredVPAddress = FormatAddress(insuredVPAddress);
                //}

                #endregion

                #region Insured Vehicle Driver Person

                try
                {

                    if (!String.IsNullOrEmpty(claim.DriverTitle))
                        insuredDPerson.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.DriverTitle);

                    insuredDPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    insuredDPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                    if (!String.IsNullOrEmpty(claim.DriverForename))
                        insuredDPerson.FirstName = claim.DriverForename;

                    if (!String.IsNullOrEmpty(claim.DriverSurname))
                        insuredDPerson.LastName = claim.DriverSurname;

                    insuredDPerson.Gender_Id = (int)GenderHelper.Salutation2Gender((Salutation)insuredDPerson.Salutation_Id);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured vehicle driver person data: " + ex);
                }

                #endregion

                #region Insured Vehicle Driver Person Address

                try
                {
                    if (!string.IsNullOrEmpty(claim.DriverAddr1))
                        insuredDPAddress.SubBuilding = claim.DriverAddr1;

                    if (!string.IsNullOrEmpty(claim.DriverAddr2))
                        insuredDPAddress.BuildingNumber = " " + claim.DriverAddr2;

                    if (!string.IsNullOrEmpty(claim.DriverAddr3))
                        insuredDPAddress.Building = " " + claim.DriverAddr3;

                    if (!string.IsNullOrEmpty(claim.DriverAddr4))
                        insuredDPAddress.Street = " " + claim.DriverAddr4;
                    
                    if (!string.IsNullOrEmpty(claim.DriverPostcode))
                        insuredDPAddress.PostCode = " " + claim.DriverPostcode;


                    //if (!string.IsNullOrEmpty(insuredDPAddress.Street) && _debug)
                    //{
                    //    insuredDPAddress = FormatAddress(insuredDPAddress);
                    //}
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured vehicle driver person address data: " + ex);
                }

                #endregion

                try
                {
                    if (insuredDPerson != null)
                    {
                        if (insuredDPAddress.Street != null || insuredDPAddress.SubBuilding != null || insuredDPAddress.Building != null || insuredDPAddress.BuildingNumber != null || insuredDPAddress.Locality != null || insuredDPAddress.PostCode != null)
                            insuredDPerson.Addresses.Add(insuredDPAddress);

                        if (!String.IsNullOrEmpty(claim.DriverTitle) && !String.IsNullOrEmpty(claim.DriverForename) &&
                            !String.IsNullOrEmpty(claim.DriverSurname))
                        {
                            insuredVehicle.People.Add(insuredDPerson);
                        }
                        else
                        {
                            insuredVPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        }
                    }

                    if (insuredVPerson != null)
                    {
                        if (insuredVPAddress.Street != null || insuredVPAddress.SubBuilding != null || insuredVPAddress.Building != null || insuredVPAddress.BuildingNumber != null || insuredVPAddress.Locality != null || insuredVPAddress.PostCode != null)
                            insuredVPerson.Addresses.Add(insuredVPAddress);

                        insuredVehicle.People.Add(insuredVPerson);
                    }

                    if (insuredVPAddress.Street != null || insuredVPAddress.SubBuilding != null || insuredVPAddress.Building != null || insuredVPAddress.BuildingNumber != null || insuredVPAddress.Locality != null || insuredVPAddress.PostCode != null)
                        motorClaim.Vehicles.Add(insuredVehicle);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured vehicle: " + ex);
                }


                #region TP Vehicle

                PipelineVehicle TPVehicle = new PipelineVehicle();
                PipelinePerson TPVPerson = new PipelinePerson();
                PipelineAddress TPVPAddress = new PipelineAddress();
                PipelineOrganisation TPOrganisation = new PipelineOrganisation();
                List<PipelineOrganisationVehicle> TPOrgVehicles = new List<PipelineOrganisationVehicle>();
                PipelineAddress TPOrgAddress = new PipelineAddress();
                PipelineOrganisation TPOrganisationSolicitor = new PipelineOrganisation();
                PipelineOrganisation TPOrganisationEngineer = new PipelineOrganisation();
                PipelineOrganisation TPOrganisationRepairer = new PipelineOrganisation();
                PipelineOrganisation TPorganisationRecovery = new PipelineOrganisation();


                TPOrgAddress.AddressType_Id = (int)AddressLinkType.TradingAddress;



                #region Vehicle Details

                try
                {
                    TPVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                    if (!String.IsNullOrEmpty(claim.Claimant_VehicleReg))
                        TPVehicle.VehicleRegistration = claim.Claimant_VehicleReg;

                    if (!String.IsNullOrEmpty(claim.Claimant_VehicleMake))
                        TPVehicle.VehicleMake = claim.Claimant_VehicleMake;

                    if (!String.IsNullOrEmpty(claim.Claimant_VehicleModel))
                        TPVehicle.VehicleModel = claim.Claimant_VehicleModel;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general TP vehicle details: " + ex);
                }

                #endregion

                #region InsuredTPPerson

                try
                {
                    //Organisation found at Claim level check
                    if (!string.IsNullOrEmpty(claim.Claimant_Title) && !string.IsNullOrEmpty(claim.Claimant_Forename))
                    {

                        TPVPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                        if (!String.IsNullOrEmpty(claim.Claimant_Title))
                            TPVPerson.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.Claimant_Title);

                        TPVPerson.Gender_Id = (int)GenderHelper.Salutation2Gender((Salutation)TPVPerson.Salutation_Id);

                        if (!String.IsNullOrEmpty(claim.Claimant_Forename))
                            TPVPerson.FirstName = claim.Claimant_Forename;

                        if (!String.IsNullOrEmpty(claim.Claimant_SurnameOrCompany))
                            TPVPerson.LastName = claim.Claimant_SurnameOrCompany;

                        if (claim.DOB != null)
                            TPVPerson.DateOfBirth = claim.DOB;

                        if (!String.IsNullOrEmpty(claim.NiNumber))
                            TPVPerson.NINumbers.Add(new PipelineNINumber() { NINumber1 = claim.NiNumber });
                            //TPVPerson.NINumber = claim.NiNumber;

                        if (!String.IsNullOrEmpty(claim.Claimant_Telephone))
                        {
                            TPVPerson.Telephones.Add(new PipelineTelephone()
                                {
                                    ClientSuppliedNumber = claim.Claimant_Telephone.Replace("(Work)", "")
                                    .Replace(".(Work)", "")
                                    .Replace(". (Work)", "")
                                    .Replace(".", "")
                                    .Trim(),
                                    TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown
                                });
                            //TPVPerson.LandlineTelephone =
                            //    claim.Claimant_Telephone.Replace("(Work)", "")
                            //         .Replace(".(Work)", "")
                            //         .Replace(". (Work)", "")
                            //         .Replace(".", "")
                            //         .Trim();
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured TP person details: " + ex);
                }


                #endregion

                #region Insured TP Person Address

                try
                {
                    //Organisation found at Claim level check
                    if (!string.IsNullOrEmpty(claim.Claimant_Title) && !string.IsNullOrEmpty(claim.Claimant_Forename))
                    {

                        if (!string.IsNullOrEmpty(claim.Claimant_Address1))
                            TPVPAddress.Street = claim.Claimant_Address1;

                        if (!string.IsNullOrEmpty(claim.Claimant_Address2))
                            TPVPAddress.Street += " " + claim.Claimant_Address2;

                        if (!string.IsNullOrEmpty(claim.Claimant_Address3))
                            TPVPAddress.Street += " " + claim.Claimant_Address3;

                        if (!string.IsNullOrEmpty(claim.Claimant_Address4))
                            TPVPAddress.Street += " " + claim.Claimant_Address4;

                        if (!string.IsNullOrEmpty(claim.Claimant_Postcode))
                            TPVPAddress.Street += " " + claim.Claimant_Postcode;

                        if (!string.IsNullOrEmpty(claim.Claimant_Postcode))
                            TPVPAddress.PostCode = claim.Claimant_Postcode;

                        //if (!string.IsNullOrEmpty(TPVPAddress.Street) && _debug)
                        //{
                        //    var TPVPAddressLkp = _TPAddresses.Where(x => x.AddressMatterNumber == claim.MatterNum).FirstOrDefault();

                        //    TPVPAddress = new Address { Street = TPVPAddressLkp.Street, Building = TPVPAddressLkp.Building, SubBuilding = TPVPAddressLkp.SubBuilding
                        //    ,BuildingNumber = TPVPAddressLkp.BuildingNumber, Town = TPVPAddressLkp.Town, PostCode = TPVPAddressLkp.PostCode};
                        //}
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured TP person address details: " + ex);
                }


                #endregion

                #region Insured TP Organisation

                try
                {

                    TPOrganisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire;
                    TPOrganisation.OrganisationType_Id = (int)OrganisationType.CreditHire;

                    if (!string.IsNullOrEmpty(claim.CHOName))
                        TPOrganisation.OrganisationName = claim.CHOName;

                    if (!string.IsNullOrEmpty(claim.CHOName))
                        TPOrganisation.OrganisationName = claim.CHOName;

                    if (!string.IsNullOrEmpty(claim.CHOTel))
                    {
                        TPOrganisation.Telephones.Add(new PipelineTelephone()
                            {
                                ClientSuppliedNumber = claim.CHOTel.Replace("(Work)", "").Replace(".(Work)", "").Replace(". (Work)", "").Replace(".", "").Trim(),
                                 TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown
                            });
                        //TPOrganisation.Telephone1 = claim.CHOTel.Replace("(Work)", "").Replace(".(Work)", "").Replace(". (Work)", "").Replace(".", "").Trim();
                    }

                    if (!string.IsNullOrEmpty(claim.CHOEmail))
                        TPOrganisation.EmailAddresses.Add(new PipelineEmail() { EmailAddress = claim.CHOEmail });
                       // TPOrganisation.Email = claim.CHOEmail;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured TP organisation details: " + ex);
                }


                #endregion

                #region Insured TP Organisation Address

                try
                {
                    if (!string.IsNullOrEmpty(claim.CHOAddressName))
                        TPOrgAddress.Street = claim.CHOAddressName;

                    if (!string.IsNullOrEmpty(claim.CHOAddressNum))
                        TPOrgAddress.Street += " " + claim.CHOAddressNum;

                    if (!string.IsNullOrEmpty(claim.CHOAddress1))
                        TPOrgAddress.Street += " " + claim.CHOAddress1;

                    if (!string.IsNullOrEmpty(claim.CHOAddress2))
                        TPOrgAddress.Street += " " + claim.CHOAddress2;

                    if (!string.IsNullOrEmpty(claim.CHOAddress3))
                        TPOrgAddress.Street += " " + claim.CHOAddress3;

                    if (!string.IsNullOrEmpty(claim.CHOPostcode))
                        TPOrgAddress.Street += " " + claim.CHOPostcode;

                    if (!string.IsNullOrEmpty(claim.CHOPostcode))
                        TPOrgAddress.PostCode = claim.CHOPostcode;

                    //if (!string.IsNullOrEmpty(TPOrgAddress.Street) && _debug)
                    //{
                    //    TPOrgAddress = FormatAddress(TPOrgAddress);
                    //}
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured TP organisation address details: " + ex);
                }


                #endregion

                #region TP Organisation Vehicle

                try
                {
                    if (!duplicateMatterClaims)
                    {
                        PipelineOrganisationVehicle TPOrgVehicle = new PipelineOrganisationVehicle();
                        if (!string.IsNullOrEmpty(claim.HireVehicleReg))
                        {
                            TPOrgVehicle.VehicleRegistration = claim.HireVehicleReg;
                        }

                        if (!string.IsNullOrEmpty(claim.HireVehicleMake))
                            TPOrgVehicle.VehicleMake = claim.HireVehicleMake;

                        if (!string.IsNullOrEmpty(claim.HireVehicleModel))
                            TPOrgVehicle.VehicleModel = claim.HireVehicleModel;

                        if (claim.HireStartDate != null)
                            TPOrgVehicle.V2O_LinkData.HireStartDate = claim.HireStartDate;

                        if (claim.HireEndDate != null)
                            TPOrgVehicle.V2O_LinkData.HireEndDate = claim.HireEndDate;

                        if (TPOrgVehicle.VehicleRegistration != null || TPOrgVehicle.VehicleMake != null ||
                            TPOrgVehicle.VehicleModel != null)
                            TPOrgVehicles.Add(TPOrgVehicle);
                    }
                    else
                    {
                        //foreach (var duplicateMatterClaim in _duplicateMatterClaims)
                        //{
                            var duplicateClaims =
                                _duplicateClaimData.Where(x => x.MatterNum == claim.MatterNum);

                            foreach (var duplicateClaim in duplicateClaims)
                            {
                                PipelineOrganisationVehicle TPOrgVehicle = new PipelineOrganisationVehicle();

                                if (!string.IsNullOrEmpty(duplicateClaim.HireVehicleReg))
                                {
                                    TPOrgVehicle.VehicleRegistration = duplicateClaim.HireVehicleReg;
                                }

                                if (!string.IsNullOrEmpty(duplicateClaim.HireVehicleMake))
                                    TPOrgVehicle.VehicleMake = duplicateClaim.HireVehicleMake;

                                if (!string.IsNullOrEmpty(duplicateClaim.HireVehicleModel))
                                    TPOrgVehicle.VehicleModel = duplicateClaim.HireVehicleModel;


                                if (TPOrgVehicle.VehicleRegistration != null || TPOrgVehicle.VehicleMake != null ||
                                    TPOrgVehicle.VehicleModel != null)
                                    TPOrgVehicles.Add(TPOrgVehicle);
                            }
                        //}
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured TP organisation vehicle: " + ex);
                }

                #region TP OrganisationSolicitor

                try
                {
                    if (!String.IsNullOrEmpty(claim.Claimant_Solicitors))
                    {
                        TPOrganisationSolicitor.OrganisationName = claim.Claimant_Solicitors;
                        TPOrganisationSolicitor.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                        TPOrganisationSolicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured TP organisation solicitor details: " + ex);
                }

                #endregion

                #region TP OrganisationEngineer

                try
                {
                    if (!String.IsNullOrEmpty(claim.Engineer))
                    {
                        TPOrganisationEngineer.OrganisationName = claim.Engineer;
                        TPOrganisationEngineer.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Engineer;
                        TPOrganisationEngineer.OrganisationType_Id = (int)OrganisationType.VehicleEngineer;
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured TP organisation engineer details: " + ex);
                }


                #endregion

                #region TP OrganisationRepairer

                try
                {
                    if (!String.IsNullOrEmpty(claim.Repairer))
                    {
                        TPOrganisationRepairer.OrganisationName = claim.Repairer;
                        TPOrganisationRepairer.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Repairer;
                        TPOrganisationRepairer.OrganisationType_Id = (int)OrganisationType.Repairer;
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured TP organisation repairer details: " + ex);
                }

                #endregion

                #region TP OrganisationRecovery

                try
                {
                    if (!String.IsNullOrEmpty(claim.RecoveryAgent))
                    {
                        TPorganisationRecovery.OrganisationName = claim.RecoveryAgent;
                        TPorganisationRecovery.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Recovery;
                        TPorganisationRecovery.OrganisationType_Id = (int)OrganisationType.Recovery;
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating insured TP organisation recovery details: " + ex);
                }

                #endregion

                #endregion

                #endregion

                try
                {

                    if (TPVehicle != null)
                    {
                        if (TPVPAddress.Street != null || TPVPAddress.SubBuilding != null || TPVPAddress.Building != null || TPVPAddress.BuildingNumber != null || TPVPAddress.Locality != null || TPVPAddress.PostCode != null)
                            TPVPerson.Addresses.Add(TPVPAddress);

                        if (TPOrganisation.OrganisationName != null)
                            TPVPerson.Organisations.Add(TPOrganisation);

                        if (TPOrgAddress.Street != null || TPOrgAddress.SubBuilding != null || TPOrgAddress.Building != null || TPOrgAddress.BuildingNumber != null || TPOrgAddress.Locality != null || TPOrgAddress.PostCode != null)
                            TPOrganisation.Addresses.Add(TPOrgAddress);

                        foreach (var organisationVehicle in TPOrgVehicles)
                        {
                            TPOrganisation.Vehicles.Add(organisationVehicle);
                        }

                        if (!String.IsNullOrEmpty(TPOrganisationSolicitor.OrganisationName) &&
                            TPOrganisationSolicitor.OrganisationName.ToUpper() != "NULL")
                            TPVPerson.Organisations.Add(TPOrganisationSolicitor);

                        if (!String.IsNullOrEmpty(TPOrganisationEngineer.OrganisationName) &&
                            TPOrganisationEngineer.OrganisationName.ToUpper() != "NULL")
                            TPVPerson.Organisations.Add(TPOrganisationEngineer);

                        if (!String.IsNullOrEmpty(TPOrganisationRepairer.OrganisationName) &&
                            TPOrganisationRepairer.OrganisationName.ToUpper() != "NULL")
                            TPVPerson.Organisations.Add(TPOrganisationRepairer);

                        if (!String.IsNullOrEmpty(TPorganisationRecovery.OrganisationName) &&
                            TPorganisationRecovery.OrganisationName.ToUpper() != "NULL")
                            TPVPerson.Organisations.Add(TPorganisationRecovery);

                        TPVehicle.People.Add(TPVPerson);
                    }

                    if (TPVPAddress.Street != null || TPVPAddress.SubBuilding != null || TPVPAddress.Building != null || TPVPAddress.BuildingNumber != null || TPVPAddress.Locality != null || TPVPAddress.PostCode != null || TPVPerson.Organisations.Any())
                        motorClaim.Vehicles.Add(TPVehicle);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating TP vehicle: " + ex);
                }

                //_claimBatch.Claims.Add(motorClaim);

                _PrevInsurerClientRef.Add(motorClaim.ClaimNumber);

                if (ProcessClaimFn(ctx, motorClaim, statusTracking) == -1) return;
            }
        }


        //private List<ClaimAddress> FormatAddress(List<ClaimAddress> _listAddressesToFormat)
        //{
        //    var stopwatch = new Stopwatch();
        //    Console.WriteLine("Start");
        //    stopwatch.Start();
            
        //    List<ClaimAddress> _claimDataAddressesFormatted = new List<ClaimAddress>();

        //    DataTable DtAddress = new DataTable("AddressDataTable");
        //    DtAddress.Columns.Add("Street", typeof(string));
        //    //DtAddress.Columns.Add("MatterNumber", typeof(int));
        //    SqlDataReader dr = null;

        //    foreach (var addressToFormat in _listAddressesToFormat.Take(100))
        //    {
        //        DtAddress.Rows.Add(addressToFormat.Street);
        //        //.Rows.Add(addressToFormat.AddressMatterNumber);
        //    }

        //    using (var conn = new SqlConnection(_connectionString))
        //    {
        //        conn.Open();

        //        using (var cmd = new SqlCommand("uspFormatAddressTableBeta", conn))
        //        {
        //            try
        //            {
        //                SqlParameter parameter = new SqlParameter();
        //                parameter.ParameterName = "@tblAddressIn";
        //                parameter.TypeName = "dbo.tblAddressIn";
        //                parameter.SqlDbType = System.Data.SqlDbType.Structured;
        //                parameter.Value = DtAddress;
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.Add(parameter);

        //                dr = cmd.ExecuteReader();

        //                if (dr.HasRows)
        //                {
        //                    while (dr.Read())
        //                    {
        //                        ClaimAddress formattedAddress = new ClaimAddress();

        //                        if (dr[0].ToString() != ""
        //                            || dr[1].ToString() != ""
        //                            || dr[2].ToString() != ""
        //                            || dr[3].ToString() != ""
        //                            || dr[4].ToString() != ""
        //                            || dr[5].ToString() != ""
        //                            || dr[6].ToString() != "")
        //                        {
        //                            formattedAddress.SubBuilding = dr[0].ToString().Trim();
        //                            formattedAddress.Building = dr[1].ToString().Trim();
        //                            formattedAddress.BuildingNumber = dr[2].ToString().Trim();
        //                            formattedAddress.Street = dr[3].ToString().Trim();
        //                            formattedAddress.Locality = dr[4].ToString().Trim();
        //                            formattedAddress.Town = dr[5].ToString().Trim();
        //                            formattedAddress.PostCode = dr[6].ToString().Trim();
        //                            string PostCode = dr[6].ToString().Trim();
        //                            PostCode = PostCode.Replace("   ", " ");
        //                            PostCode = PostCode.Replace("  ", " ");
        //                            if (!PostCode.Contains(" ") && PostCode.Length == 7)
        //                            {
        //                                PostCode = PostCode.Substring(0, 4) + " " + PostCode.Substring(4);
        //                            }
        //                            formattedAddress.PostCode = PostCode;

        //                            _claimDataAddressesFormatted.Add(new ClaimAddress
        //                                {
        //                                    BuildingNumber = formattedAddress.BuildingNumber,
        //                                    SubBuilding = formattedAddress.SubBuilding,
        //                                    Locality = formattedAddress.Locality,
        //                                    PostCode = formattedAddress.PostCode,
        //                                    Building = formattedAddress.Building,
        //                                    Street = formattedAddress.Street,
        //                                    Town = formattedAddress.Town,
        //                                    AddressMatterNumber = 1 
        //                                });
        //                        }
        //                    }
        //                }
        //            }

        //            catch (Exception ex)
        //            {
        //                throw new CustomException("Error in Format Address Stored Procedure: " + ex);
        //            }

        //            finally
        //            {
        //                if (dr != null)
        //                    dr.Close();

        //                if (conn.State == ConnectionState.Open)
        //                    conn.Close();
        //            }
        //        }
        //    }
        //    stopwatch.Stop();
        //    GetTimeSpan(stopwatch);
        //    return _claimDataAddressesFormatted;
        //}

        public static void GetTimeSpan(Stopwatch stopwatch)
        {
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopwatch.Elapsed;

            // Format and display the TimeSpan value. 
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                               ts.Hours, ts.Minutes, ts.Seconds,
                                               ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
            Console.WriteLine("Finish");

            Console.ReadLine();
        }

        //private Address FormatAddress(Address address)
        //{
        //    SqlDataReader dr = null;
        //    using (var conn = new SqlConnection(_connectionString))
        //    {
        //        conn.Open();
        //        using (var cmd = new SqlCommand("uspFormatAddressLineBeta", conn))
        //        {
        //            try
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@Add", SqlDbType.VarChar).Value = address.Street;//  ?? (object)DBNull.Value;
        //                cmd.Parameters.AddWithValue("@MergeStreet", SqlDbType.VarChar).Value = 1;

        //                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

        //                if (dr.HasRows)
        //                {
        //                    while (dr.Read())
        //                    {
        //                        if (dr[0].ToString() != ""
        //                            || dr[1].ToString() != ""
        //                            || dr[2].ToString() != ""
        //                            || dr[3].ToString() != ""
        //                            || dr[4].ToString() != ""
        //                            || dr[5].ToString() != ""
        //                            || dr[6].ToString() != "")
        //                        {
        //                            address.SubBuilding = dr[0].ToString().Trim();
        //                            address.Building = dr[1].ToString().Trim();
        //                            address.BuildingNumber = dr[2].ToString().Trim();
        //                            address.Street = dr[3].ToString().Trim();
        //                            address.Locality = dr[4].ToString().Trim();
        //                            address.Town = dr[5].ToString().Trim();
        //                            address.PostCode = dr[6].ToString().Trim();
        //                            string PostCode = dr[6].ToString().Trim();
        //                            PostCode = PostCode.Replace("   ", " ");
        //                            PostCode = PostCode.Replace("  ", " ");
        //                            if (!PostCode.Contains(" ") && PostCode.Length == 7)
        //                            {
        //                                PostCode = PostCode.Substring(0, 4) + " " + PostCode.Substring(4);
        //                            }
        //                            address.PostCode = PostCode;
        //                        }
        //                    }
        //                }
        //            }

        //            catch (Exception ex)
        //            {
        //                throw new CustomException("Error in Format Address Stored Procedure: " + ex);
        //            }

        //            finally
        //            {
        //                if (dr != null)
        //                    dr.Close();

        //                if (conn.State == ConnectionState.Open)
        //                    conn.Close();
        //            }
        //        }
        //    }

        //    return address;
        //}

        private string GetNewConnection()
        {
            return ConfigurationManager.ConnectionStrings["PAFDbContext"].ConnectionString;
        }
    }
}
