﻿using MDA.Common.Server;
using MDA.ExternalService.Interface;
using MDA.ExternalServices.Model;
using MDA.CueService.Interface;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Debug;
using MDA.CueService.Model;

namespace MDA.CueService.Mock
{
    public class CueService : ICueService, IExternalService
    {
        public CueService()
        {
        }

        /// <summary>
        /// This method accepts the 3-field KEY in the request object that identifies the UNQIUE record in the
        /// RiskExternalServicesRequests table. From that information it will read data direct from ADA, make an external call, update ADA
        /// directly and return a populated Response object.
        /// return a populated Response object.
        /// </summary>
        /// <param name="ctx">The current context</param>
        /// <param name="resr">An object holding the 3-key index into the RiskExternalServicesRequests table</param>
        /// <returns>A Response object that contains the call status, any errors and the data sent and received via the service</returns>
        public ExternalServiceCallResponse ProcessPipelineClaim(CurrentContext ctx, ExternalServiceCallRequest exServiceReq)
        {
            int riskClaimId = exServiceReq.RiskClaim_Id;
            var riskClaim = (from x in ctx.db.RiskClaims where x.Id == riskClaimId select x).First();

            #region get UserName and Password of account

            string sname = ConfigurationManager.AppSettings["CueServiceNameInDb"].ToString();


            var servicesToCall = (from x in ctx.db.RiskClient2RiskExternalServices
                                  where x.RiskClient_Id == ctx.RiskClientId && x.RiskExternalService.ServiceName == sname
                                  select new ServicesToCall() { Client2Services = x, ExternalService = x.RiskExternalService }).First();

            string username = servicesToCall.Client2Services.ServiceUserName;
            string password = servicesToCall.Client2Services.ServicePassword;
            #endregion
            
            #region Get the PERSON record identified by the UniqueID which is the ID of a P2A link record. Abort if not found
            MDA.DAL.Person p = ctx.db.People.Where(x=>x.Id == exServiceReq.Unique_Id).First();
            if (p.Person2Address != null)
            {
                var p2a = p.Person2Address;
            }
            #endregion

            CUERequestInfo requestInfo = PopulateCueRequestInfo(riskClaimId);
            
            try
            {
                Result results = CallCueService(requestInfo, username, password, ctx.TraceLoad);

                var retData = new Dictionary<string, object>();
                retData.Add("Motor", results);
                retData.Add("Household", results);
                retData.Add("PI", results);
                //retData.Add("Return", results);

                return new ExternalServiceCallResponse()
                {
                    RequestData = "request data",
                    ReturnedData = retData,
                    ServicesCalledMask = 0,
                    CanCreateBatch = false,
                    CallStatus = RiskExternalServiceCallStatus.Success,
                    UseJsonSerialisation = true
                };
            }

            catch (Exception)
            {
                if (ctx.TraceLoad) ADATrace.WriteLine("Handling Unexpected error, queue for retry");

                return new ExternalServiceCallResponse
                {
                    CallStatus = RiskExternalServiceCallStatus.FailedNeedsRetry,
                    //ServicesCalledMask = serviceCallMask,
                    ErrorMessage = "Unexpected Error, Retry required",
                    RequestData = requestInfo
                };
            }
        }

        private CUERequestInfo PopulateCueRequestInfo(int riskClaimId)
        {
            CUERequestInfo requestInfo = new CUERequestInfo();
           

            return requestInfo;
        }

        private Result CallCueService(CUERequestInfo cri, string username, string password, bool _trace)
        {
            try
            {
                //iduPortClient proxy = new iduPortClient();

                //var a = proxy.IDUProcess(request);

                //if (_trace) ADATrace.WriteLine("Output :: " + SerializeResults(a));
                //if (_trace) ADATrace.WriteLine("------------------------------------------");

                //ResultInfo results = Translator.Translate(a);

                
                
                Result results = new Result();

                //results.ClaimNumber = "AB123456789";
                //results.SearchDate = DateTime.Now;

                List<CUESearchSummary> lstSearchSummary = new List<CUESearchSummary>();
                List<CUEResultsSummary> resultsSummary = new List<CUEResultsSummary>();
                CueSearchCriteria searchCriteria = new CueSearchCriteria();
                List<CUEIncidentSummary> lstIncidentSummary = new List<CUEIncidentSummary>();
                List<CUESummaryIncident> lstCUESummary_Incidents = new List<CUESummaryIncident>();
                List<CUEIncident> lstCUEIncidents = new List<CUEIncident>();
                List<CUEClaimDataPolicyHolder> lstClaimDataPolicyHolders = new List<CUEClaimDataPolicyHolder>();
                List<CUEPayment> lstClaimDataPayments = new List<CUEPayment>();

                List<CUEIncidentInvolvement> lstCUEIncidentInvolvements = new List<CUEIncidentInvolvement>();
                List<CUESupplier> lstCUESuppliers = new List<CUESupplier>();


                lstSearchSummary.Add(new CUESearchSummary { Subject = "David Compton", Involvement = "Insured Driver", Motor = false, PI = false, Household = false, Cross_Enquiry = true, SearchDate = Convert.ToDateTime("01/06/2014 14:18") });
                lstSearchSummary.Add(new CUESearchSummary { Subject = "Anne Compton", Involvement = "Insured Passenger", Motor = true, PI = true, Household = false, Cross_Enquiry = false, SearchDate = Convert.ToDateTime("01/06/2014 14:18") });
                lstSearchSummary.Add(new CUESearchSummary { Subject = "James Gayle", Involvement = "Third Party Driver", Motor = false, PI = false, Household = false, Cross_Enquiry = true, SearchDate = Convert.ToDateTime("27/05/2014 09:16") });

                resultsSummary.Add(new CUEResultsSummary { Incidents = "8", Involved_Household_Claims = "4", Involved_Motor_Claims = "2", Involved_PI_Claims = "3" });

                searchCriteria.Name = "David Compton";
                searchCriteria.Dob = "29/05/1964";
                searchCriteria.NI_Number = "AB123456C";
                searchCriteria.DrivingLicenceNum = "45621452";
                searchCriteria.AddressNumber = "26";
                searchCriteria.AddressTown = "Lancaster";
                searchCriteria.AddressCity = "Lancashire";
                searchCriteria.AddressPostCode = "LA1 2JE";
                searchCriteria.VehicleReg = "MK59 FQL";
                searchCriteria.VIN = "5GZCZ43D13S812715";

                lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 1, Subject = "David Compton", IncidentType = "HH", InvolvementType = "Policyholder", IncidentDate = Convert.ToDateTime("01/08/2012"), ClaimStatus = "Outstanding", Insurer = "AXA", Description = "", MatchCode = "" });

                lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 2, Subject = "David Compton", IncidentType = "HH", InvolvementType = "Policyholder", IncidentDate = Convert.ToDateTime("12/05/2011"), ClaimStatus = "Breach of Condition", Insurer = "LV=", Description = "", MatchCode = "" });

                lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 3, Subject = "Anne Compton", IncidentType = "PI", InvolvementType = "Claimant", IncidentDate = Convert.ToDateTime("24/02/2010"), ClaimStatus = "", Insurer = "DLG", Description = "", MatchCode = "" });

                lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 4, Subject = "Anne Compton", IncidentType = "M", InvolvementType = "PolicyHolder", IncidentDate = Convert.ToDateTime("17/03/2009"), ClaimStatus = "Settled", Insurer = "Aviva", Description = "Struck TP Vehicle in read", MatchCode = "" });

                lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 4, Subject = "Anne Compton", IncidentType = "PI", InvolvementType = "Claimant", IncidentDate = Convert.ToDateTime("17/03/2009"), ClaimStatus = "Outstanding", Insurer = "Aviva", Description = "Whiplash", MatchCode = "" });

                lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 5, Subject = "David Compton", IncidentType = "PI", InvolvementType = "Claimant", IncidentDate = Convert.ToDateTime("09/08/2008"), ClaimStatus = "Settled", Insurer = "RBS", Description = "Broken Leg at workplace", MatchCode = "" });

                lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 6, Subject = "James Compton", IncidentType = "PI", InvolvementType = "Claimant", IncidentDate = Convert.ToDateTime("26/12/2007"), ClaimStatus = "Settled", Insurer = "Liberty", Description = "", MatchCode = "" });

                lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 7, Subject = "Henry Winter", IncidentType = "HH", InvolvementType = "PolicyHolder", IncidentDate = Convert.ToDateTime("04/03/2002"), ClaimStatus = "Settled", Insurer = "DLG", Description = "", MatchCode = "" });

                lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 8, Subject = "David Compton", IncidentType = "M", InvolvementType = "Third Party", IncidentDate = Convert.ToDateTime("19/11/1999"), ClaimStatus = "Settled", Insurer = "LV=", Description = "", MatchCode = "" });

                lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 1, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 0, MO_Claims = 0, HH_Claims = 1, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 1).ToList() });

                lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 2, From = Convert.ToDateTime("12/05/2011"), To = Convert.ToDateTime("12/05/2011"), PI_Claims = 0, MO_Claims = 0, HH_Claims = 1, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 2).ToList() });

                lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 3, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 1, MO_Claims = 0, HH_Claims = 0, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 3).ToList() });

                lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 4, From = Convert.ToDateTime("17/03/2009"), To = Convert.ToDateTime("17/03/2009"), PI_Claims = 1, MO_Claims = 1, HH_Claims = 0, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 4).ToList() });

                lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 5, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 1, MO_Claims = 0, HH_Claims = 0, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 5).ToList() });

                lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 6, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 1, MO_Claims = 0, HH_Claims = 0, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 6).ToList() });

                lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 7, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 0, MO_Claims = 0, HH_Claims = 1, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 7).ToList() });

                lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 8, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 0, MO_Claims = 1, HH_Claims = 0, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 8).ToList() });


                #region Incident 1

                CUEIncident incident1 = new CUEIncident();
                lstClaimDataPolicyHolders.Add(new CUEClaimDataPolicyHolder { IncidentId = 1, FullName = "David Compton", Address = "26 Wood Road, Lancaster, Lancashire, LA1 2JE" });
                lstClaimDataPolicyHolders.Add(new CUEClaimDataPolicyHolder { IncidentId = 1, FullName = "Anne Compton", Address = "26 Wood Road, Lancaster, Lancashire, LA1 2JE" });

                lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "Buildings", PaymentAmount = 0.00M });
                lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "Contents", PaymentAmount = 0.00M });
                lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "All Risks", PaymentAmount = 0.00M });
                lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "Freezer", PaymentAmount = 0.00M });
                lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "Caravan", PaymentAmount = 0.00M });
                lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "Other", PaymentAmount = 0.00M });

                CUEClaimData cueClaimData = new CUEClaimData();
                CUEGeneralData cueGeneralData = new CUEGeneralData();

                cueClaimData.ClaimNumber = "5796679W";
                cueClaimData.Insurer = "AXA";
                cueClaimData.PolicyHolders = lstClaimDataPolicyHolders.Where(x => x.IncidentId == 1).ToList();

                cueGeneralData.ClaimStatus = "Outstanding";
                cueGeneralData.IncidentDescription = "Not Provided";
                cueGeneralData.PolicyType = "Buildings";
                cueGeneralData.PolicyNumber = "AB6054987";
                cueGeneralData.PolicyNumberID = "Standard";
                cueGeneralData.CollectivePolicyIndicator = "Non Collective Policy";
                cueGeneralData.LossSetupDate = Convert.ToDateTime("20/08/2012");
                cueGeneralData.InsurerContactName = "Jamie Bull";
                cueGeneralData.InsurerContactTel = "0845 166 5177";
                cueGeneralData.IncidentDate = Convert.ToDateTime("01/08/2012");
                cueGeneralData.PolicyInceptionDate = Convert.ToDateTime("01/08/2009");
                cueGeneralData.PolicyPeriodEndDate = Convert.ToDateTime("01/08/2011");
                cueGeneralData.CauseOfLoss = "Storm";
                cueGeneralData.CatastropheRelated = "Unknown";
                cueGeneralData.RiskAddressNumber = "26";
                cueGeneralData.RiskAddressStreet = "Wood Road";
                cueGeneralData.RiskAddressTown = "Lancaster";
                cueGeneralData.RiskAddressCity = "Lancashire";
                cueGeneralData.RiskAddressPostCode = "LA1 2JE";
                cueGeneralData.CUEPayments = lstClaimDataPayments.Where(x => x.IncidentId == 1).ToList();

                incident1.IncidentId = 1;
                incident1.IncidentType = "HH";
                incident1.ClaimData = cueClaimData;
                incident1.GeneralData = cueGeneralData;

                CUEIncidentInvolvement incident1CUEIncidentInvolvement = new CUEIncidentInvolvement();
                incident1CUEIncidentInvolvement.Incident_Id = 1;
                incident1CUEIncidentInvolvement.Type = "Policyholder 1 (Claimant)";
                incident1CUEIncidentInvolvement.Status = "Personal";
                incident1CUEIncidentInvolvement.Title = "Mr";
                incident1CUEIncidentInvolvement.Forename = "David";
                incident1CUEIncidentInvolvement.MiddleName = "";
                incident1CUEIncidentInvolvement.LastName = "Compton";
                incident1CUEIncidentInvolvement.NI_Number = "LW021290C";
                incident1CUEIncidentInvolvement.AddressNumber = "26";
                incident1CUEIncidentInvolvement.AddressStreet = "Wood Road";
                incident1CUEIncidentInvolvement.AddressTown = "Lancaster";
                incident1CUEIncidentInvolvement.AddressCity = "Lancashire";
                incident1CUEIncidentInvolvement.AddressPostCode = "LA1 2JE";
                incident1CUEIncidentInvolvement.AddressIndicator = "PAF Valid";
                incident1CUEIncidentInvolvement.Sex = "Male";
                incident1CUEIncidentInvolvement.Dob = Convert.ToDateTime("29/05/1964");
                incident1CUEIncidentInvolvement.PaymentAmount = 700.58M;
                incident1CUEIncidentInvolvement.Telephone = "01789 469106";
                incident1CUEIncidentInvolvement.Occupation = "Unknown";

                CUEIncidentInvolvement incident2CUEIncidentInvolvement = new CUEIncidentInvolvement();
                incident2CUEIncidentInvolvement.Incident_Id = 1;
                incident2CUEIncidentInvolvement.Type = "Policyholder 2";
                incident2CUEIncidentInvolvement.Status = "Personal";
                incident2CUEIncidentInvolvement.Title = "Mrs";
                incident2CUEIncidentInvolvement.Forename = "Anne";
                incident2CUEIncidentInvolvement.MiddleName = "";
                incident2CUEIncidentInvolvement.LastName = "Compton";
                incident2CUEIncidentInvolvement.NI_Number = "LW021290C";
                incident2CUEIncidentInvolvement.AddressNumber = "26";
                incident2CUEIncidentInvolvement.AddressStreet = "Wood Road";
                incident2CUEIncidentInvolvement.AddressTown = "Lancaster";
                incident2CUEIncidentInvolvement.AddressCity = "Lancashire";
                incident2CUEIncidentInvolvement.AddressPostCode = "LA1 2JE";
                incident2CUEIncidentInvolvement.AddressIndicator = "PAF Valid";
                incident2CUEIncidentInvolvement.Sex = "Female";
                incident2CUEIncidentInvolvement.Dob = Convert.ToDateTime("14/09/1967");
                incident2CUEIncidentInvolvement.PaymentAmount = 0.00M;
                incident2CUEIncidentInvolvement.Telephone = "01789 469106";
                incident2CUEIncidentInvolvement.Occupation = "Unknown";

                CUESupplier supplier1 = new CUESupplier();
                supplier1.IncidentId = 1;
                supplier1.CompanyName = "Metric Consulatants";
                supplier1.SupplierStatus = "Business";
                supplier1.Telephone = "0845 401 8644";
                supplier1.AddressHouseName = "Metric House";
                supplier1.AddressStreet = "Gibson Street";
                supplier1.AddressCity = "Blackburn";
                supplier1.AddressPostCode = "BB4 8JL";
                supplier1.AddressIndicator = "PAF Valid";


                lstCUEIncidentInvolvements.Add(incident1CUEIncidentInvolvement);
                lstCUEIncidentInvolvements.Add(incident2CUEIncidentInvolvement);

                lstCUESuppliers.Add(supplier1);
                incident1.Suppliers = lstCUESuppliers;
                incident1.CUEIncidentInvolvements = lstCUEIncidentInvolvements;

                #endregion

                #region Incident 2

                CUEIncident incident2 = new CUEIncident();
                incident2.IncidentId = 2;
                incident2.IncidentType = "M";
                lstClaimDataPolicyHolders.Add(new CUEClaimDataPolicyHolder { IncidentId = 2, FullName = "David Compton", Address = "26 Wood Road, Lancaster, Lancashire, LA1 2JE" });

                CUEClaimData cueClaimData2 = new CUEClaimData();
                cueClaimData2.ClaimNumber = "48662001479892";
                cueClaimData2.Insurer = "LV=";
                cueClaimData2.PolicyHolders = lstClaimDataPolicyHolders.Where(x => x.IncidentId == 2).ToList();

                incident2.ClaimData = cueClaimData2;

                List<CUEPayment> ClaimDataPayments = new List<CUEPayment>();
                ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "Buildings", PaymentAmount = 0.00M });
                ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "Contents", PaymentAmount = 0.00M });
                ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "All Risks", PaymentAmount = 0.00M });
                ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "Freezer", PaymentAmount = 0.00M });
                ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "Caravan", PaymentAmount = 0.00M });
                ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "Other", PaymentAmount = 0.00M });

                CUEGeneralData cueGeneralData2 = new CUEGeneralData();
                cueGeneralData2.ClaimStatus = "Outstanding";
                cueGeneralData2.IncidentDescription = "Struck third party vehicle in rear";
                cueGeneralData2.PolicyType = "Motor";
                cueGeneralData2.PolicyNumber = "AB6054987";
                cueGeneralData2.PolicyNumberID = "Standard";
                cueGeneralData2.CollectivePolicyIndicator = "Non Collective Policy";
                cueGeneralData2.LossSetupDate = Convert.ToDateTime("20/08/2012");
                cueGeneralData2.InsurerContactName = "Jamie Bull";
                cueGeneralData2.InsurerContactTel = "0845 166 5177";
                cueGeneralData2.IncidentDate = Convert.ToDateTime("01/08/2012");
                cueGeneralData2.PolicyInceptionDate = Convert.ToDateTime("01/08/2009");
                cueGeneralData2.PolicyPeriodEndDate = Convert.ToDateTime("01/08/2011");
                cueGeneralData2.CauseOfLoss = "Storm";
                cueGeneralData2.CatastropheRelated = "Unknown";
                cueGeneralData2.RiskAddressNumber = "26";
                cueGeneralData2.RiskAddressStreet = "Wood Road";
                cueGeneralData2.RiskAddressTown = "Lancaster";
                cueGeneralData2.RiskAddressCity = "Lancashire";
                cueGeneralData2.RiskAddressPostCode = "LA1 2JE";
                cueGeneralData2.CUEPayments = ClaimDataPayments.Where(x => x.IncidentId == 2).ToList();

                incident2.GeneralData = cueGeneralData2;

                CUEIncidentInvolvementVehicleDetails policyHolder1 = new CUEIncidentInvolvementVehicleDetails();
                policyHolder1.Incident_Id = 1;
                policyHolder1.Type = "Policyholder 1";
                policyHolder1.Status = "Personal";
                policyHolder1.Title = "Mr";
                policyHolder1.Forename = "David";
                policyHolder1.MiddleName = "";
                policyHolder1.LastName = "Compton";
                policyHolder1.NI_Number = "LW021290C";
                policyHolder1.AddressNumber = "26";
                policyHolder1.AddressStreet = "Wood Road";
                policyHolder1.AddressTown = "Lancaster";
                policyHolder1.AddressCity = "Lancashire";
                policyHolder1.AddressPostCode = "LA1 2JE";
                policyHolder1.AddressIndicator = "PAF Valid";
                policyHolder1.Sex = "Male";
                policyHolder1.Dob = Convert.ToDateTime("29/05/1964");
                policyHolder1.PaymentAmount = 700.58M;
                policyHolder1.Telephone = "01789 469106";
                policyHolder1.Occupation = "Unknown";

                policyHolder1.VehicleRegistration = "MA04 DFP";
                policyHolder1.VehicleIdentificationNumber = "5GZCZ43D13S812715";
                policyHolder1.Make = "Audi";
                policyHolder1.DamageStatus = "Cat 1";
                policyHolder1.Model = "A1";
                policyHolder1.RecoveredStatus = "n/a";
                policyHolder1.Colour = "White";
                policyHolder1.RegistrationStatus = "n/a";
                policyHolder1.CoverType = "Comprehensive";

                CUEIncidentInvolvementVehicleDetails thirdParty1 = new CUEIncidentInvolvementVehicleDetails();
                thirdParty1.Incident_Id = 1;
                thirdParty1.Type = "Third Party 1";
                thirdParty1.Status = "Personal";
                thirdParty1.Title = "Mrs";
                thirdParty1.Forename = "Janet";
                thirdParty1.MiddleName = "Laura";
                thirdParty1.LastName = "Heywood";
                thirdParty1.NI_Number = "LW021290C";
                thirdParty1.AddressNumber = "543";
                thirdParty1.AddressStreet = "Church Road";
                thirdParty1.AddressTown = "Morecambe";
                thirdParty1.AddressCity = "Lancashire";
                thirdParty1.AddressPostCode = "MC1 8WN";
                thirdParty1.AddressIndicator = "Not PAF Valid";
                thirdParty1.Sex = "Female";
                thirdParty1.Dob = Convert.ToDateTime("16/11/1983");
                thirdParty1.PaymentAmount = 0.00M;
                thirdParty1.Telephone = "01789 746004";
                thirdParty1.Occupation = "Unknown";

                thirdParty1.VehicleRegistration = "MA04 DFP";
                thirdParty1.VehicleIdentificationNumber = "5GZCZ43D13S812715";
                thirdParty1.Make = "Audi";
                thirdParty1.DamageStatus = "Cat 1";
                thirdParty1.Model = "A1";
                thirdParty1.RecoveredStatus = "n/a";
                thirdParty1.Colour = "White";
                thirdParty1.RegistrationStatus = "n/a";
                thirdParty1.CoverType = "Comprehensive";

                CUESupplier supplier2 = new CUESupplier();
                supplier2.IncidentId = 2;
                supplier2.CompanyName = "Coles Law Ltd.";
                supplier2.SupplierStatus = "Business";
                supplier2.Telephone = "0845 559 1430";
                supplier2.AddressHouseName = "121";
                supplier2.AddressStreet = "Stump Street";
                supplier2.AddressCity = "Harrogate";
                supplier2.AddressPostCode = "HA12 7HW";
                supplier2.AddressIndicator = "PAF Valid";
                supplier2.PaymentAmount = 1450.55M;

                List<CUEIncidentInvolvement> CUEIncident2Involvements = new List<CUEIncidentInvolvement>();
                CUEIncident2Involvements.Add(policyHolder1);
                CUEIncident2Involvements.Add(thirdParty1);

                List<CUESupplier> CUESuppliers2 = new List<CUESupplier>();
                CUESuppliers2.Add(supplier2);
                incident2.Suppliers = CUESuppliers2;
                incident2.CUEIncidentInvolvements = CUEIncident2Involvements;

                #endregion

                #region Incident 3

                CUEIncident incident3 = new CUEIncident();
                incident3.IncidentId = 3;
                incident3.IncidentType = "PI";
                lstClaimDataPolicyHolders.Add(new CUEClaimDataPolicyHolder { IncidentId = 3, FullName = "David Compton", Address = "26 Wood Road, Lancaster, Lancashire, LA1 2JE" });

                CUEClaimData cueClaimData3 = new CUEClaimData();
                cueClaimData3.ClaimNumber = "48662001479892";
                cueClaimData3.Insurer = "LV=";
                cueClaimData3.PolicyHolders = lstClaimDataPolicyHolders.Where(x => x.IncidentId == 3).ToList();

                incident3.ClaimData = cueClaimData3;

                CUEGeneralData cueGeneralData3 = new CUEGeneralData();
                cueGeneralData3.ClaimStatus = "Outstanding";
                cueGeneralData3.IncidentDescription = "Struck third party vehicle in rear";
                cueGeneralData3.PolicyType = "Motor";
                cueGeneralData3.PolicyNumber = "AB6054987";
                cueGeneralData3.PolicyNumberID = "Standard";
                cueGeneralData3.CollectivePolicyIndicator = "Non Collective Policy";
                cueGeneralData3.LossSetupDate = Convert.ToDateTime("20/08/2012");
                cueGeneralData3.InsurerContactName = "Jamie Bull";
                cueGeneralData3.InsurerContactTel = "0845 166 5177";
                cueGeneralData3.IncidentDate = Convert.ToDateTime("01/08/2012");
                cueGeneralData3.PolicyInceptionDate = Convert.ToDateTime("01/08/2009");
                cueGeneralData3.PolicyPeriodEndDate = Convert.ToDateTime("01/08/2011");
                cueGeneralData3.CauseOfLoss = "Storm";
                cueGeneralData3.CatastropheRelated = "Unknown";
                cueGeneralData3.RiskAddressNumber = "26";
                cueGeneralData3.RiskAddressStreet = "Wood Road";
                cueGeneralData3.RiskAddressTown = "Lancaster";
                cueGeneralData3.RiskAddressCity = "Lancashire";
                cueGeneralData3.RiskAddressPostCode = "LA1 2JE";
                cueGeneralData3.CUEPayments = lstClaimDataPayments.Where(x => x.IncidentId == 3).ToList();

                incident3.GeneralData = cueGeneralData3;

                CUEIncidentInvolvement claimant1 = new CUEIncidentInvolvement();
                claimant1.Incident_Id = 3;
                claimant1.Type = "Claimant 1";
                claimant1.Status = "Personal";
                claimant1.Title = "Mr";
                claimant1.Forename = "David";
                claimant1.MiddleName = "";
                claimant1.LastName = "Compton";
                claimant1.NI_Number = "LW021290C";
                claimant1.AddressNumber = "26";
                claimant1.AddressStreet = "Wood Road";
                claimant1.AddressTown = "Lancaster";
                claimant1.AddressCity = "Lancashire";
                claimant1.AddressPostCode = "LA1 2JE";
                claimant1.AddressIndicator = "PAF Valid";
                claimant1.Sex = "Male";
                claimant1.Dob = Convert.ToDateTime("29/05/1964");
                claimant1.PaymentAmount = 700.58M;
                claimant1.Telephone = "01789 469106";
                claimant1.Occupation = "Unknown";

                CUEIncidentDetails incidentDetails = new CUEIncidentDetails();

                incidentDetails.InjuryDescription = "Whiplash";
                incidentDetails.HospitalAttended = "Yes";

                incident3.IncidentDetails = incidentDetails;

                List<CUEIncidentInvolvement> CUEIncident3Involvements = new List<CUEIncidentInvolvement>();
                CUEIncident3Involvements.Add(claimant1);
                incident3.CUEIncidentInvolvements = CUEIncident3Involvements;

                #endregion

                lstCUEIncidents.Add(incident1);
                lstCUEIncidents.Add(incident2);
                lstCUEIncidents.Add(incident3);


                results.SearchSummary = lstSearchSummary;
                results.ResultsSummary = resultsSummary;
                results.SearchCriteria = searchCriteria;
                results.IncidentSummary = lstIncidentSummary;
                results.CUE_Incidents = lstCUEIncidents;

                return results;
            }
            catch (Exception ex)
            {
                if (_trace)
                {
                    ADATrace.WriteLine("Unexpected Exception - Tracesmart Proxy Call");
                    ADATrace.TraceException(ex);
                }

                throw ex;
            }
        }

       

        /// <summary>
        /// Take the riskClaimId and uniqueId and work out which sub-services of the external service can be called.  Typically you would fetch all the data
        /// taking the uniqueId as the starting point. You would then work out which fields are available to call the sub-service.  You need to create an
        /// inityial mask of 0 and then or (|) into the mask the bit the spcifies the services that can be called. eg  mask |= 0x0001 sets the first bit
        /// </summary>
        /// <param name="ctx">Current Context</param>
        /// <param name="riskClaimId">RiskClaim_Id of this claim</param>
        /// <param name="uniqueId">The unique ID specific to this service</param>
        /// <returns>The bit mask and optional error message string</returns>
        public ExternalServicesMaskResponse BuildMaskOfServicesThatCanBeCalled(CurrentContext ctx, int riskClaimId, int uniqueId)
        {
            return new ExternalServicesMaskResponse() { ServicesThatCanBeCalledMask = 0, ErrorMessage = string.Empty };
        }

        class ServicesToCall
        {
            public RiskClient2RiskExternalServices Client2Services { get; set; }
            public RiskExternalService ExternalService { get; set; }
        }
    }

    
}
