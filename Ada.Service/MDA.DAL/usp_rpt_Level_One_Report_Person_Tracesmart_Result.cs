//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    
    public partial class usp_rpt_Level_One_Report_Person_Tracesmart_Result
    {
        public string NameAndDOB { get; set; }
        public string Address_ { get; set; }
        public Nullable<int> Person_Id { get; set; }
        public string AddressSources { get; set; }
        public int AddressSourcesCheck { get; set; }
        public string AddressGoneAway { get; set; }
        public int AddressGoneAwayCheck { get; set; }
        public string VerificationExperianDOB { get; set; }
        public Nullable<int> VerificationExperianDOBCheck { get; set; }
        public string VerificationTracemartDOB { get; set; }
        public int VerificationTracemartDOBCheck { get; set; }
        public string LandlineStatus { get; set; }
        public Nullable<int> LandlineStatusCheck { get; set; }
        public string MobileStatus { get; set; }
        public Nullable<int> MobileStatusCheck { get; set; }
        public string MobileCurrentLocation { get; set; }
        public Nullable<int> MobileCurrentLocationCheck { get; set; }
        public string SanctionDate { get; set; }
        public int SanctionDateCheck { get; set; }
        public string SanctionSource { get; set; }
        public int SanctionSourceCheck { get; set; }
        public string CCJDate { get; set; }
        public int CCJDateCheck { get; set; }
        public string CCJType { get; set; }
        public int CCJTypeCheck { get; set; }
        public string CCJAmount { get; set; }
        public int CCJAmountCheck { get; set; }
        public string CCJEndDate { get; set; }
        public int CCJEndDateCheck { get; set; }
        public string InsolvencyStartDate { get; set; }
        public int InsolvencyStartDateCheck { get; set; }
        public string InsolvencyStatus { get; set; }
        public int InsolvencyStatusCheck { get; set; }
        public Nullable<int> IDVCheck { get; set; }
        public int ElectoralRoll { get; set; }
        public int TelephoneDirectory { get; set; }
        public int CreditActive { get; set; }
        public Nullable<int> DOBVerification { get; set; }
        public Nullable<int> MobileVerification { get; set; }
        public int PEPAndSanction { get; set; }
        public int Mortality { get; set; }
        public int CIFAS { get; set; }
        public int GoneAway { get; set; }
        public int CCJs { get; set; }
        public int Bankruptcy { get; set; }
        public Nullable<int> IVA { get; set; }
    }
}
