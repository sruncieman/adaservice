﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Data.Entity.Core.EntityClient;

namespace MDA.DAL
{
    public partial class MdaDbContext : DbContext
    {
        public MdaDbContext(string connectionString)
            : base(connectionString)
        {
            var adapter = (IObjectContextAdapter)this;
            var objectContext = adapter.ObjectContext;
            objectContext.CommandTimeout = 60000;

            #if DEBUG
                        this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
            #endif
        }
    }
}
