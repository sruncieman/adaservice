//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    
    public partial class usp_rpt_Level_One_Report_Message_Data_Vehicle_Result
    {
        public Nullable<int> RiskClaim_Id { get; set; }
        public string Per_DbId { get; set; }
        public string Veh_DbId { get; set; }
        public string Add_DbId { get; set; }
        public string Org_DbId { get; set; }
        public string LocalName { get; set; }
        public string Rule_Key { get; set; }
        public string Rule_Set_Key { get; set; }
        public string DataMed1 { get; set; }
        public string DataMed2 { get; set; }
        public string DataMed3 { get; set; }
        public string DataMed4 { get; set; }
        public string DataMed5 { get; set; }
        public string DataMed6 { get; set; }
        public string DataMed7 { get; set; }
        public string DataMed8 { get; set; }
        public string DataMed9 { get; set; }
        public string DataMed10 { get; set; }
        public string DataMed11 { get; set; }
        public string DataMed12 { get; set; }
        public string DataMed13 { get; set; }
        public string DataMed14 { get; set; }
        public string DataMed15 { get; set; }
        public string DataMed16 { get; set; }
        public string DataMed17 { get; set; }
        public string DataMed18 { get; set; }
        public string DataMed19 { get; set; }
        public string DataMed20 { get; set; }
        public string MessageHeader { get; set; }
        public string ReportHeader { get; set; }
    }
}
