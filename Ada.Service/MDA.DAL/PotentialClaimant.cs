//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class PotentialClaimant
    {
        public int ID { get; set; }
        public int PartyType { get; set; }
        public int SubPartyType { get; set; }
        public int Flag { get; set; }
        public byte RecordStatus { get; set; }
        public byte ADARecordStatus { get; set; }
    }
}
