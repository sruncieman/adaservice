//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class VehicleColour
    {
        public VehicleColour()
        {
            this.Vehicles = new HashSet<Vehicle>();
        }
    
        public int Id { get; set; }
        public string Colour { get; set; }
        public byte RecordStatus { get; set; }
        public byte ADARecordStatus { get; set; }
    
        public virtual ICollection<Vehicle> Vehicles { get; set; }
    }
}
