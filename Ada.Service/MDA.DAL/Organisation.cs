//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Organisation
    {
        public Organisation()
        {
            this.Handset2Organisation = new HashSet<Handset2Organisation>();
            this.Incident2Organisation = new HashSet<Incident2Organisation>();
            this.Incident2OrganisationOutcome = new HashSet<Incident2OrganisationOutcome>();
            this.Organisation2Address = new HashSet<Organisation2Address>();
            this.Organisation2BankAccount = new HashSet<Organisation2BankAccount>();
            this.Organisation2Email = new HashSet<Organisation2Email>();
            this.Organisation2IPAddress = new HashSet<Organisation2IPAddress>();
            this.Organisation2Organisation = new HashSet<Organisation2Organisation>();
            this.Organisation2Organisation1 = new HashSet<Organisation2Organisation>();
            this.Organisation2PaymentCard = new HashSet<Organisation2PaymentCard>();
            this.Organisation2Policy = new HashSet<Organisation2Policy>();
            this.Organisation2Telephone = new HashSet<Organisation2Telephone>();
            this.Organisation2WebSite = new HashSet<Organisation2WebSite>();
            this.Person2Organisation = new HashSet<Person2Organisation>();
            this.TOG2Organisation = new HashSet<TOG2Organisation>();
            this.Vehicle2Organisation = new HashSet<Vehicle2Organisation>();
        }
    
        public int Id { get; set; }
        public string OrganisationId { get; set; }
        public string OrganisationName { get; set; }
        public int OrganisationType_Id { get; set; }
        public Nullable<System.DateTime> IncorporatedDate { get; set; }
        public int OrganisationStatus_Id { get; set; }
        public Nullable<System.DateTime> EffectiveDateOfStatus { get; set; }
        public string RegisteredNumber { get; set; }
        public string SIC { get; set; }
        public string VATNumber { get; set; }
        public Nullable<System.DateTime> VATNumberValidationDate { get; set; }
        public string MojCRMNumber { get; set; }
        public int MojCRMStatus_Id { get; set; }
        public string CreditLicense { get; set; }
        public string KeyAttractor { get; set; }
        public bool CHFKeyAttractor { get; set; }
        public Nullable<System.DateTime> CHFKeyAttractorRemoveDate { get; set; }
        public string Source { get; set; }
        public string SourceReference { get; set; }
        public string SourceDescription { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string IBaseId { get; set; }
        public byte RecordStatus { get; set; }
        public byte ADARecordStatus { get; set; }
        public byte[] RowVersion { get; set; }
        public string SanctionList { get; set; }
        public string OrganisationNameCalcuated { get; set; }
        public string FraudRingClosed_ { get; set; }
    
        public virtual ICollection<Handset2Organisation> Handset2Organisation { get; set; }
        public virtual ICollection<Incident2Organisation> Incident2Organisation { get; set; }
        public virtual ICollection<Incident2OrganisationOutcome> Incident2OrganisationOutcome { get; set; }
        public virtual MojCRMStatu MojCRMStatu { get; set; }
        public virtual OrganisationStatu OrganisationStatu { get; set; }
        public virtual OrganisationType OrganisationType { get; set; }
        public virtual ICollection<Organisation2Address> Organisation2Address { get; set; }
        public virtual ICollection<Organisation2BankAccount> Organisation2BankAccount { get; set; }
        public virtual ICollection<Organisation2Email> Organisation2Email { get; set; }
        public virtual ICollection<Organisation2IPAddress> Organisation2IPAddress { get; set; }
        public virtual ICollection<Organisation2Organisation> Organisation2Organisation { get; set; }
        public virtual ICollection<Organisation2Organisation> Organisation2Organisation1 { get; set; }
        public virtual ICollection<Organisation2PaymentCard> Organisation2PaymentCard { get; set; }
        public virtual ICollection<Organisation2Policy> Organisation2Policy { get; set; }
        public virtual ICollection<Organisation2Telephone> Organisation2Telephone { get; set; }
        public virtual ICollection<Organisation2WebSite> Organisation2WebSite { get; set; }
        public virtual ICollection<Person2Organisation> Person2Organisation { get; set; }
        public virtual ICollection<TOG2Organisation> TOG2Organisation { get; set; }
        public virtual ICollection<Vehicle2Organisation> Vehicle2Organisation { get; set; }
    }
}
