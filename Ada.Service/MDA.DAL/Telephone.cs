//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Telephone
    {
        public Telephone()
        {
            this.Address2Telephone = new HashSet<Address2Telephone>();
            this.Handset2Telephone = new HashSet<Handset2Telephone>();
            this.Organisation2Telephone = new HashSet<Organisation2Telephone>();
            this.Person2Telephone = new HashSet<Person2Telephone>();
            this.TOG2Telephone = new HashSet<TOG2Telephone>();
        }
    
        public int Id { get; set; }
        public string TelephoneId { get; set; }
        public string TelephoneNumber { get; set; }
        public string CountryCode { get; set; }
        public string STDCode { get; set; }
        public int TelephoneType_Id { get; set; }
        public string KeyAttractor { get; set; }
        public string Source { get; set; }
        public string SourceReference { get; set; }
        public string SourceDescription { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string IBaseId { get; set; }
        public byte RecordStatus { get; set; }
        public byte ADARecordStatus { get; set; }
        public byte[] RowVersion { get; set; }
        public string FraudRingClosed_ { get; set; }
    
        public virtual ICollection<Address2Telephone> Address2Telephone { get; set; }
        public virtual ICollection<Handset2Telephone> Handset2Telephone { get; set; }
        public virtual ICollection<Organisation2Telephone> Organisation2Telephone { get; set; }
        public virtual ICollection<Person2Telephone> Person2Telephone { get; set; }
        public virtual TelephoneType TelephoneType { get; set; }
        public virtual ICollection<TOG2Telephone> TOG2Telephone { get; set; }
    }
}
