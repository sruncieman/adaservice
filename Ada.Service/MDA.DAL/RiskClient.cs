//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class RiskClient
    {
        public RiskClient()
        {
            this.MatchingRulesClaims = new HashSet<MatchingRulesClaim>();
            this.RiskBatches = new HashSet<RiskBatch>();
            this.RiskClient2RiskExternalServices = new HashSet<RiskClient2RiskExternalServices>();
            this.RiskConfigurationValues = new HashSet<RiskConfigurationValue>();
            this.RiskNoteDecisions = new HashSet<RiskNoteDecision>();
            this.RiskSendMeAReports = new HashSet<RiskSendMeAReport>();
            this.RiskTeams = new HashSet<RiskTeam>();
            this.RiskUsers = new HashSet<RiskUser>();
            this.RiskUser2Client = new HashSet<RiskUser2Client>();
            this.RiskWordFilters = new HashSet<RiskWordFilter>();
        }
    
        public int Id { get; set; }
        public string ClientName { get; set; }
        public int InsurersClients_Id { get; set; }
        public int AmberThreshold { get; set; }
        public int RedThreshold { get; set; }
        public string EmailDomain { get; set; }
        public string ExpectedFileExtension { get; set; }
        public int LevelOneReportDelay { get; set; }
        public int LevelTwoReportDelay { get; set; }
        public int ClaimsActivePeriodInDays { get; set; }
        public bool ScoreClosedClaims { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<bool> RecordReserveChanges { get; set; }
        public Nullable<int> InternalClient { get; set; }
        public Nullable<int> BatchPriority { get; set; }
    
        public virtual InsurersClient InsurersClient { get; set; }
        public virtual ICollection<MatchingRulesClaim> MatchingRulesClaims { get; set; }
        public virtual ICollection<RiskBatch> RiskBatches { get; set; }
        public virtual ICollection<RiskClient2RiskExternalServices> RiskClient2RiskExternalServices { get; set; }
        public virtual ICollection<RiskConfigurationValue> RiskConfigurationValues { get; set; }
        public virtual ICollection<RiskNoteDecision> RiskNoteDecisions { get; set; }
        public virtual ICollection<RiskSendMeAReport> RiskSendMeAReports { get; set; }
        public virtual ICollection<RiskTeam> RiskTeams { get; set; }
        public virtual ICollection<RiskUser> RiskUsers { get; set; }
        public virtual ICollection<RiskUser2Client> RiskUser2Client { get; set; }
        public virtual ICollection<RiskWordFilter> RiskWordFilters { get; set; }
    }
}
