//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class rpt_Level_One_Report_Person
    {
        public Nullable<int> RiskClaim_Id { get; set; }
        public Nullable<int> Person_Id { get; set; }
        public Nullable<int> Vehicle_Id { get; set; }
        public string ConfirmedAliases { get; set; }
        public string Gender { get; set; }
        public string Salutation { get; set; }
        public string FullName { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string MobileNumber { get; set; }
        public string NINumber { get; set; }
        public string PartyType { get; set; }
        public string SubPartyType { get; set; }
        public string PossibleAlias { get; set; }
        public string Occupation { get; set; }
        public string OtherLinkedAddresses { get; set; }
        public string LinkedTelephone { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public string MessageHeader { get; set; }
        public string ReportHeader { get; set; }
        public string Telephone { get; set; }
        public string Nationality { get; set; }
        public string DrivingLicenceNumber { get; set; }
        public string PassportNumber { get; set; }
        public string OccupationClaim { get; set; }
        public string EmailAddress { get; set; }
        public string BankAccount { get; set; }
        public string LinkedNINumber { get; set; }
        public string LinkedDrivingLicenceNumber { get; set; }
        public string LinkedPassportNumber { get; set; }
        public string LinkedTaxiDriverLicenceNumber { get; set; }
        public string LinkedEmailAddress { get; set; }
        public string LinkedBankAccount { get; set; }
        public string LinkedRelativesAssociates { get; set; }
    }
}
