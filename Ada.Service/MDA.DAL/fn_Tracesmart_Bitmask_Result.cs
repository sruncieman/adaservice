//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    
    public partial class fn_Tracesmart_Bitmask_Result
    {
        public Nullable<bool> Tracesmart_Passport { get; set; }
        public Nullable<bool> Tracesmart_Telephone { get; set; }
        public Nullable<bool> Tracesmart_Driving { get; set; }
        public Nullable<bool> Tracesmart_Birth { get; set; }
        public Nullable<bool> Tracesmart_SmartLink { get; set; }
        public Nullable<bool> Tracesmart_NI { get; set; }
        public Nullable<bool> Tracesmart_CardNumber { get; set; }
        public Nullable<bool> Tracesmart_BankAccount { get; set; }
        public Nullable<bool> Tracesmart_Mobile { get; set; }
        public Nullable<bool> Tracesmart_Crediva { get; set; }
        public Nullable<bool> Tracesmart_CreditActive { get; set; }
        public Nullable<bool> Tracesmart_NHS { get; set; }
        public Nullable<bool> Tracesmart_Cardavs { get; set; }
        public Nullable<bool> Tracesmart_MPan { get; set; }
        public Nullable<bool> Tracesmart_Address { get; set; }
        public Nullable<bool> Tracesmart_DeathScreen { get; set; }
        public Nullable<bool> Tracesmart_DoB { get; set; }
        public Nullable<bool> Tracesmart_Sanction { get; set; }
        public Nullable<bool> Tracesmart_Insolvency { get; set; }
        public Nullable<bool> Tracesmart_CCJ { get; set; }
    }
}
