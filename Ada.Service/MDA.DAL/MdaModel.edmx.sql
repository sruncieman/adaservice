
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 05/24/2013 15:13:29
-- Generated from EDMX file: C:\DevProjects\MDA\MDASolution\MDA.DAL\MdaModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [MDA-SC-TEST];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Incident2Organisation_ClaimStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Incident2Organisation] DROP CONSTRAINT [FK_Incident2Organisation_ClaimStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_Incident2Organisation_MojStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Incident2Organisation] DROP CONSTRAINT [FK_Incident2Organisation_MojStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_Incident2Person_ClaimStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Incident2Person] DROP CONSTRAINT [FK_Incident2Person_ClaimStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_Incident2Person_MojStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Incident2Person] DROP CONSTRAINT [FK_Incident2Person_MojStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_Incident2Vehicle_Incident2VehicleLinkType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Incident2Vehicle] DROP CONSTRAINT [FK_Incident2Vehicle_Incident2VehicleLinkType];
GO
IF OBJECT_ID(N'[dbo].[FK_KeoghsCase2Incident_CaseIncidentLinkType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[KeoghsCase2Incident] DROP CONSTRAINT [FK_KeoghsCase2Incident_CaseIncidentLinkType];
GO
IF OBJECT_ID(N'[dbo].[FK_Person2Organisation_PersonOrganisationLinkType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Person2Organisation] DROP CONSTRAINT [FK_Person2Organisation_PersonOrganisationLinkType];
GO
IF OBJECT_ID(N'[dbo].[FK_Person2Person_Person2PersonLinkType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Person2Person] DROP CONSTRAINT [FK_Person2Person_Person2PersonLinkType];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Address2Address]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Address2Address];
GO
IF OBJECT_ID(N'[dbo].[Address2Telephone]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Address2Telephone];
GO
IF OBJECT_ID(N'[dbo].[ClaimStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClaimStatus];
GO
IF OBJECT_ID(N'[dbo].[ClearcoreSync]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClearcoreSync];
GO
IF OBJECT_ID(N'[dbo].[Incident2FraudRing]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Incident2FraudRing];
GO
IF OBJECT_ID(N'[dbo].[Incident2Incident]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Incident2Incident];
GO
IF OBJECT_ID(N'[dbo].[Incident2Organisation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Incident2Organisation];
GO
IF OBJECT_ID(N'[dbo].[Incident2OrganisationOutcome]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Incident2OrganisationOutcome];
GO
IF OBJECT_ID(N'[dbo].[Incident2Person]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Incident2Person];
GO
IF OBJECT_ID(N'[dbo].[Incident2PersonOutcome]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Incident2PersonOutcome];
GO
IF OBJECT_ID(N'[dbo].[Incident2Vehicle]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Incident2Vehicle];
GO
IF OBJECT_ID(N'[dbo].[Incident2VehicleLinkType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Incident2VehicleLinkType];
GO
IF OBJECT_ID(N'[dbo].[InsurersClients]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InsurersClients];
GO
IF OBJECT_ID(N'[dbo].[KeoghsCase2Incident]', 'U') IS NOT NULL
    DROP TABLE [dbo].[KeoghsCase2Incident];
GO
IF OBJECT_ID(N'[dbo].[KeoghsCase2IncidentLinkType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[KeoghsCase2IncidentLinkType];
GO
IF OBJECT_ID(N'[dbo].[KeoghsCaseStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[KeoghsCaseStatus];
GO
IF OBJECT_ID(N'[dbo].[MojCRMStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MojCRMStatus];
GO
IF OBJECT_ID(N'[dbo].[MojStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MojStatus];
GO
IF OBJECT_ID(N'[dbo].[Organisation2Address]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organisation2Address];
GO
IF OBJECT_ID(N'[dbo].[Organisation2BankAccount]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organisation2BankAccount];
GO
IF OBJECT_ID(N'[dbo].[Organisation2Email]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organisation2Email];
GO
IF OBJECT_ID(N'[dbo].[Organisation2IPAddress]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organisation2IPAddress];
GO
IF OBJECT_ID(N'[dbo].[Organisation2Organisation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organisation2Organisation];
GO
IF OBJECT_ID(N'[dbo].[Organisation2PaymentCard]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organisation2PaymentCard];
GO
IF OBJECT_ID(N'[dbo].[Organisation2Policy]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organisation2Policy];
GO
IF OBJECT_ID(N'[dbo].[Organisation2Telephone]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organisation2Telephone];
GO
IF OBJECT_ID(N'[dbo].[Organisation2WebSite]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organisation2WebSite];
GO
IF OBJECT_ID(N'[dbo].[OrganisationStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrganisationStatus];
GO
IF OBJECT_ID(N'[dbo].[Person2Address]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2Address];
GO
IF OBJECT_ID(N'[dbo].[Person2BankAccount]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2BankAccount];
GO
IF OBJECT_ID(N'[dbo].[Person2BBPin]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2BBPin];
GO
IF OBJECT_ID(N'[dbo].[Person2DrivingLicense]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2DrivingLicense];
GO
IF OBJECT_ID(N'[dbo].[Person2Email]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2Email];
GO
IF OBJECT_ID(N'[dbo].[Person2IPAddress]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2IPAddress];
GO
IF OBJECT_ID(N'[dbo].[Person2NINumber]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2NINumber];
GO
IF OBJECT_ID(N'[dbo].[Person2Organisation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2Organisation];
GO
IF OBJECT_ID(N'[dbo].[Person2OrganisationLinkType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2OrganisationLinkType];
GO
IF OBJECT_ID(N'[dbo].[Person2Passport]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2Passport];
GO
IF OBJECT_ID(N'[dbo].[Person2PaymentCard]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2PaymentCard];
GO
IF OBJECT_ID(N'[dbo].[Person2Person]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2Person];
GO
IF OBJECT_ID(N'[dbo].[Person2PersonLinkType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2PersonLinkType];
GO
IF OBJECT_ID(N'[dbo].[Person2Policy]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2Policy];
GO
IF OBJECT_ID(N'[dbo].[Person2Telephone]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2Telephone];
GO
IF OBJECT_ID(N'[dbo].[Person2WebSite]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person2WebSite];
GO
IF OBJECT_ID(N'[dbo].[Policy2BankAccount]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Policy2BankAccount];
GO
IF OBJECT_ID(N'[dbo].[Policy2PaymentCard]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Policy2PaymentCard];
GO
IF OBJECT_ID(N'[dbo].[RiskClaimRunMessages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RiskClaimRunMessages];
GO
IF OBJECT_ID(N'[dbo].[TOG2Account]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2Account];
GO
IF OBJECT_ID(N'[dbo].[TOG2Address]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2Address];
GO
IF OBJECT_ID(N'[dbo].[TOG2DrivingLicense]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2DrivingLicense];
GO
IF OBJECT_ID(N'[dbo].[TOG2Email]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2Email];
GO
IF OBJECT_ID(N'[dbo].[TOG2FraudRing]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2FraudRing];
GO
IF OBJECT_ID(N'[dbo].[TOG2Incident]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2Incident];
GO
IF OBJECT_ID(N'[dbo].[TOG2IPAddress]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2IPAddress];
GO
IF OBJECT_ID(N'[dbo].[TOG2KeoghsCase]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2KeoghsCase];
GO
IF OBJECT_ID(N'[dbo].[TOG2NINumber]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2NINumber];
GO
IF OBJECT_ID(N'[dbo].[TOG2Organisation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2Organisation];
GO
IF OBJECT_ID(N'[dbo].[TOG2Passport]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2Passport];
GO
IF OBJECT_ID(N'[dbo].[TOG2PaymentCard]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2PaymentCard];
GO
IF OBJECT_ID(N'[dbo].[TOG2Person]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2Person];
GO
IF OBJECT_ID(N'[dbo].[TOG2Policy]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2Policy];
GO
IF OBJECT_ID(N'[dbo].[TOG2Telephone]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2Telephone];
GO
IF OBJECT_ID(N'[dbo].[TOG2TOG]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2TOG];
GO
IF OBJECT_ID(N'[dbo].[TOG2Vehicle]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2Vehicle];
GO
IF OBJECT_ID(N'[dbo].[TOG2Website]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TOG2Website];
GO
IF OBJECT_ID(N'[dbo].[Vehicle2Address]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Vehicle2Address];
GO
IF OBJECT_ID(N'[dbo].[Vehicle2Organisation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Vehicle2Organisation];
GO
IF OBJECT_ID(N'[dbo].[Vehicle2Person]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Vehicle2Person];
GO
IF OBJECT_ID(N'[dbo].[Vehicle2Policy]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Vehicle2Policy];
GO
IF OBJECT_ID(N'[dbo].[Vehicle2Vehicle]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Vehicle2Vehicle];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Addresses'
CREATE TABLE [dbo].[Addresses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [AddressId] nvarchar(50)  NULL,
    [SubBuilding] nvarchar(50)  NULL,
    [Building] nvarchar(50)  NULL,
    [BuildingNumber] nvarchar(50)  NULL,
    [Street] nvarchar(50)  NULL,
    [Locality] nvarchar(50)  NULL,
    [Town] nvarchar(50)  NULL,
    [County] nvarchar(50)  NULL,
    [PostCode] nvarchar(20)  NULL,
    [DxNumber] nvarchar(50)  NULL,
    [DxExchange] nvarchar(150)  NULL,
    [GridX] nvarchar(50)  NULL,
    [GridY] nvarchar(50)  NULL,
    [PafValidation] bit  NULL,
    [DocumentLink] nvarchar(256)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [AddressType_Id] int  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Address2Address'
CREATE TABLE [dbo].[Address2Address] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Address1_Id] int  NOT NULL,
    [Address2_Id] int  NOT NULL,
    [AddressLinkId] nvarchar(50)  NULL,
    [AddressLinkType_Id] int  NOT NULL,
    [StartOfResidency] datetime  NULL,
    [EndOfResidency] datetime  NULL,
    [Notes] nvarchar(1024)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Address2Telephone'
CREATE TABLE [dbo].[Address2Telephone] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Address_Id] int  NOT NULL,
    [Telephone_Id] int  NOT NULL,
    [TelephoneLinkId] nvarchar(50)  NULL,
    [TelephoneLinkType_Id] int  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'AddressLinkTypes'
CREATE TABLE [dbo].[AddressLinkTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [LinkType] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'AddressTypes'
CREATE TABLE [dbo].[AddressTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'BankAccounts'
CREATE TABLE [dbo].[BankAccounts] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [AccountId] nvarchar(50)  NULL,
    [AccountNumber] varchar(50)  NOT NULL,
    [SortCode] nvarchar(12)  NULL,
    [BankName] nvarchar(50)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'BankNames'
CREATE TABLE [dbo].[BankNames] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BankName1] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'BBPins'
CREATE TABLE [dbo].[BBPins] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BBPinId] nvarchar(50)  NULL,
    [BBPin1] nvarchar(50)  NULL,
    [DisplayName] nvarchar(50)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'ClaimStatus'
CREATE TABLE [dbo].[ClaimStatus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] varchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'ClaimTypes'
CREATE TABLE [dbo].[ClaimTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ClaimType1] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Counties'
CREATE TABLE [dbo].[Counties] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [County1] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'DrivingLicenses'
CREATE TABLE [dbo].[DrivingLicenses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DrivingLicenseId] nvarchar(50)  NULL,
    [DriverNumber] nvarchar(50)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Emails'
CREATE TABLE [dbo].[Emails] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EmailId] nvarchar(50)  NULL,
    [EmailAddress] nvarchar(255)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'FraudRings'
CREATE TABLE [dbo].[FraudRings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FraudRingId] nvarchar(50)  NULL,
    [FraudRingName] nvarchar(50)  NULL,
    [Status] nvarchar(10)  NULL,
    [LeadAnalyst] nvarchar(50)  NULL,
    [Champion] nvarchar(50)  NULL,
    [BriefingDocument] nvarchar(255)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Genders'
CREATE TABLE [dbo].[Genders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Incidents'
CREATE TABLE [dbo].[Incidents] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IncidentId] nvarchar(50)  NULL,
    [KeoghsEliteReference] nvarchar(50)  NULL,
    [IncidentType_Id] int  NOT NULL,
    [ClaimType_Id] int  NOT NULL,
    [IfbReference] nvarchar(50)  NULL,
    [IncidentDate] datetime  NOT NULL,
    [FraudRingName] nvarchar(50)  NULL,
    [PaymentsToDate] decimal(19,4)  NULL,
    [Reserve] decimal(19,4)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Incident2FraudRing'
CREATE TABLE [dbo].[Incident2FraudRing] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Incident_Id] int  NOT NULL,
    [FraudRing_Id] int  NOT NULL,
    [FraudRingLinkId] nvarchar(50)  NULL,
    [LinkType] int  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Incident2Incident'
CREATE TABLE [dbo].[Incident2Incident] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Incident1_Id] int  NOT NULL,
    [Incident2_Id] int  NOT NULL,
    [IncidentMatchLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Incident2Organisation'
CREATE TABLE [dbo].[Incident2Organisation] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Incident_Id] int  NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [IsClaimLevelLink] bit  NOT NULL,
    [IncidentLinkId] nvarchar(50)  NULL,
    [OrganisationType_Id] int  NOT NULL,
    [PartyType_Id] int  NOT NULL,
    [SubPartyType_Id] int  NOT NULL,
    [IncidentTime] time  NULL,
    [IncidentCircs] nvarchar(1024)  NULL,
    [IncidentLocation] nvarchar(255)  NULL,
    [PaymentsToDate] decimal(19,4)  NULL,
    [Reserve] decimal(19,4)  NULL,
    [Insurer] nvarchar(255)  NULL,
    [ClaimNumber] nvarchar(50)  NULL,
    [ClaimStatus_Id] int  NOT NULL,
    [ClaimType] nvarchar(50)  NULL,
    [ClaimCode] nvarchar(50)  NULL,
    [MojStatus_Id] int  NOT NULL,
    [ClaimNotificationDate] datetime  NULL,
    [Broker] nvarchar(255)  NULL,
    [ReferralSource] nvarchar(255)  NULL,
    [Solicitors] nvarchar(255)  NULL,
    [Engineer] nvarchar(255)  NULL,
    [Recovery] nvarchar(255)  NULL,
    [Storage] nvarchar(255)  NULL,
    [StorageAddress] nvarchar(255)  NULL,
    [Repairer] nvarchar(255)  NULL,
    [Hire] nvarchar(255)  NULL,
    [AccidentManagement] nvarchar(255)  NULL,
    [MedicalExaminer] nvarchar(255)  NULL,
    [PoliceAttended] bit  NOT NULL,
    [PoliceForce] nvarchar(50)  NULL,
    [PoliceReference] nvarchar(50)  NULL,
    [AmbulanceAttended] bit  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Incident2OrganisationOutcome'
CREATE TABLE [dbo].[Incident2OrganisationOutcome] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Incident_Id] int  NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [OutcomeLinkId] nvarchar(50)  NULL,
    [SettlementDate] datetime  NULL,
    [MannerOfResolution] nvarchar(100)  NULL,
    [EnforcementRole] nvarchar(100)  NULL,
    [PropertyOwned] nvarchar(20)  NULL,
    [MethodOfEnforcement] nvarchar(100)  NULL,
    [DateOfSettlementAgreed] datetime  NULL,
    [SettlementAmountAgreed] nvarchar(20)  NULL,
    [AmountRecoveredToDate] nvarchar(20)  NULL,
    [AmountStillOutstanding] nvarchar(20)  NULL,
    [WasEnforcementSuccessful] nvarchar(20)  NULL,
    [TypeOfSuccess] nvarchar(100)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Incident2Person'
CREATE TABLE [dbo].[Incident2Person] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Incident_Id] int  NOT NULL,
    [Person_Id] int  NOT NULL,
    [IncidentLinkId] nvarchar(50)  NULL,
    [PartyType_Id] int  NOT NULL,
    [SubPartyType_Id] int  NOT NULL,
    [MojStatus_Id] int  NOT NULL,
    [IncidentTime] time  NULL,
    [IncidentCircs] nvarchar(1024)  NULL,
    [IncidentLocation] nvarchar(255)  NULL,
    [Insurer] nvarchar(255)  NULL,
    [ClaimNumber] nvarchar(50)  NULL,
    [ClaimStatus_Id] int  NOT NULL,
    [ClaimType] nvarchar(50)  NULL,
    [ClaimCode] nvarchar(50)  NULL,
    [ClaimNotificationDate] datetime  NULL,
    [PaymentsToDate] decimal(19,4)  NULL,
    [Reserve] decimal(19,4)  NULL,
    [Broker] nvarchar(255)  NULL,
    [ReferralSource] nvarchar(255)  NULL,
    [Solicitors] nvarchar(255)  NULL,
    [Engineer] nvarchar(255)  NULL,
    [Recovery] nvarchar(255)  NULL,
    [Storage] nvarchar(255)  NULL,
    [StorageAddress] nvarchar(255)  NULL,
    [Repairer] nvarchar(255)  NULL,
    [Hire] nvarchar(255)  NULL,
    [AccidentManagement] nvarchar(255)  NULL,
    [MedicalExaminer] nvarchar(255)  NULL,
    [PoliceAttended] bit  NOT NULL,
    [PoliceForce] nvarchar(50)  NULL,
    [PoliceReference] nvarchar(50)  NULL,
    [AmbulanceAttended] bit  NOT NULL,
    [AttendedHospital] bit  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Incident2PersonOutcome'
CREATE TABLE [dbo].[Incident2PersonOutcome] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Incident_Id] int  NOT NULL,
    [Person_Id] int  NOT NULL,
    [OutcomeLinkId] nvarchar(50)  NULL,
    [SettlementDate] datetime  NULL,
    [MannerOfResolution] nvarchar(100)  NULL,
    [EnforcementRole] nvarchar(100)  NULL,
    [PropertyOwned] nvarchar(20)  NULL,
    [MethodOfEnforcement] nvarchar(100)  NULL,
    [DateOfSettlementAgreed] datetime  NULL,
    [SettlementAmountAgreed] nvarchar(20)  NULL,
    [AmountRecoveredToDate] nvarchar(20)  NULL,
    [AmountStillOutstanding] nvarchar(20)  NULL,
    [WasEnforcementSuccessful] nvarchar(20)  NULL,
    [TypeOfSuccess] nvarchar(100)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Incident2Vehicle'
CREATE TABLE [dbo].[Incident2Vehicle] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Incident_Id] int  NOT NULL,
    [Vehicle_Id] int  NOT NULL,
    [VehicleIncidentLinkId] nvarchar(50)  NULL,
    [Incident2VehicleLinkType_Id] int  NOT NULL,
    [VehicleCategoryOfLoss_Id] int  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Incident2VehicleLinkType'
CREATE TABLE [dbo].[Incident2VehicleLinkType] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'IncidentTypes'
CREATE TABLE [dbo].[IncidentTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IncidentType1] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'InsurersClients'
CREATE TABLE [dbo].[InsurersClients] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ClientName] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'IPAddresses'
CREATE TABLE [dbo].[IPAddresses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IPAddressId] nvarchar(50)  NULL,
    [IPAddress1] nvarchar(20)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'KeoghsCases'
CREATE TABLE [dbo].[KeoghsCases] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [KeoghsCaseId] nvarchar(50)  NULL,
    [KeoghsEliteReference] nvarchar(50)  NULL,
    [KeoghsOffice_Id] int  NOT NULL,
    [FeeEarner] nvarchar(50)  NULL,
    [Client_Id] int  NOT NULL,
    [ClientReference] nvarchar(50)  NULL,
    [ClientClaimsHandler] nvarchar(50)  NULL,
    [CaseStatus_Id] int  NOT NULL,
    [CurrentReserve] nvarchar(59)  NULL,
    [ClosureDate] datetime  NULL,
    [WeedDate] datetime  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'KeoghsCase2Incident'
CREATE TABLE [dbo].[KeoghsCase2Incident] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [KeoghsCase_Id] int  NOT NULL,
    [Incident_Id] int  NOT NULL,
    [CaseIncidentLinkId] nvarchar(50)  NULL,
    [CaseIncidentLinkType_Id] int  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'KeoghsCase2IncidentLinkType'
CREATE TABLE [dbo].[KeoghsCase2IncidentLinkType] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [LinkType] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'KeoghsCaseStatus'
CREATE TABLE [dbo].[KeoghsCaseStatus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [StatusText] nvarchar(20)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'KeoghsOffices'
CREATE TABLE [dbo].[KeoghsOffices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OfficeName] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'MojCRMStatus'
CREATE TABLE [dbo].[MojCRMStatus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Status] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'MojStatus'
CREATE TABLE [dbo].[MojStatus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] varchar(50)  NOT NULL
);
GO

-- Creating table 'Nationalities'
CREATE TABLE [dbo].[Nationalities] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [NationalityText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'NINumbers'
CREATE TABLE [dbo].[NINumbers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [NINumberId] nvarchar(50)  NULL,
    [NINumber1] nvarchar(10)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Organisations'
CREATE TABLE [dbo].[Organisations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrganisationId] nvarchar(50)  NULL,
    [OrganisationName] nvarchar(255)  NULL,
    [OrganisationType_Id] int  NOT NULL,
    [IncorporatedDate] datetime  NULL,
    [OrganisationStatus_Id] int  NOT NULL,
    [EffectiveDateOfStatus] datetime  NULL,
    [RegisteredNumber] nvarchar(50)  NULL,
    [SIC] nvarchar(50)  NULL,
    [VATNumber] nvarchar(50)  NULL,
    [VATNumberValidationDate] datetime  NULL,
    [MojCRMNumber] nvarchar(10)  NULL,
    [MojCRMStatus_Id] int  NOT NULL,
    [CreditLicense] nvarchar(50)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Organisation2Address'
CREATE TABLE [dbo].[Organisation2Address] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [Address_Id] int  NOT NULL,
    [AddressLinkId] nvarchar(50)  NULL,
    [AddressLinkType_Id] int  NOT NULL,
    [StartOfResidency] datetime  NULL,
    [EndOfResidency] datetime  NULL,
    [Notes] nvarchar(1024)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Organisation2BankAccount'
CREATE TABLE [dbo].[Organisation2BankAccount] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [BankAccount_Id] int  NOT NULL,
    [AccountLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Organisation2Email'
CREATE TABLE [dbo].[Organisation2Email] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [Email_Id] int  NOT NULL,
    [EmailLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Organisation2IPAddress'
CREATE TABLE [dbo].[Organisation2IPAddress] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [IPAddress_Id] int  NOT NULL,
    [IPAddressLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Organisation2Organisation'
CREATE TABLE [dbo].[Organisation2Organisation] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Organisation1_Id] int  NOT NULL,
    [Organisation2_Id] int  NOT NULL,
    [Org2OrgLinkId] nvarchar(50)  NULL,
    [OrganisationLinkType_Id] int  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Organisation2PaymentCard'
CREATE TABLE [dbo].[Organisation2PaymentCard] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [PaymentCard_Id] int  NOT NULL,
    [PaymentCardLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Organisation2Policy'
CREATE TABLE [dbo].[Organisation2Policy] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [Policy_Id] int  NOT NULL,
    [PolicyLinkId] nvarchar(50)  NULL,
    [PolicyLinkType_Id] int  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Organisation2Telephone'
CREATE TABLE [dbo].[Organisation2Telephone] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [Telephone_Id] int  NOT NULL,
    [TelephoneLinkId] nvarchar(50)  NULL,
    [TelephoneLinkType_Id] int  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Organisation2WebSite'
CREATE TABLE [dbo].[Organisation2WebSite] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [WebSite_Id] int  NOT NULL,
    [WebSiteLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'OrganisationLinkTypes'
CREATE TABLE [dbo].[OrganisationLinkTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [LinkType] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'OrganisationStatus'
CREATE TABLE [dbo].[OrganisationStatus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [StatusText] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'OrganisationTypes'
CREATE TABLE [dbo].[OrganisationTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TypeText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'PartyTypes'
CREATE TABLE [dbo].[PartyTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PartyTypeText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Passports'
CREATE TABLE [dbo].[Passports] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PassportId] nvarchar(50)  NULL,
    [PassportNumber] nvarchar(50)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'PaymentCards'
CREATE TABLE [dbo].[PaymentCards] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PaymentCardId] nvarchar(50)  NULL,
    [PaymentCardNumber] nvarchar(50)  NOT NULL,
    [SortCode] nvarchar(8)  NULL,
    [ExpiryDate] nvarchar(4)  NULL,
    [BankName] nvarchar(50)  NULL,
    [PaymentCardType_Id] int  NOT NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'PaymentCardTypes'
CREATE TABLE [dbo].[PaymentCardTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CardTypeText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'People'
CREATE TABLE [dbo].[People] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PersonId] nvarchar(50)  NULL,
    [Gender_Id] int  NOT NULL,
    [Salutation_Id] int  NOT NULL,
    [FirstName] nvarchar(100)  NULL,
    [MiddleName] nvarchar(100)  NULL,
    [LastName] nvarchar(100)  NOT NULL,
    [DateOfBirth] datetime  NULL,
    [Nationality] nvarchar(50)  NULL,
    [Occupation] nvarchar(50)  NULL,
    [TaxiDriverLicense] nvarchar(20)  NULL,
    [Schools] nvarchar(150)  NULL,
    [Hobbies] nvarchar(150)  NULL,
    [Notes] nvarchar(1024)  NULL,
    [Documents] nvarchar(1024)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2Address'
CREATE TABLE [dbo].[Person2Address] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [Address_Id] int  NOT NULL,
    [AddressLinkId] nvarchar(50)  NULL,
    [AddressLinkType_Id] int  NOT NULL,
    [StartOfResidency] datetime  NULL,
    [EndOfResidency] datetime  NULL,
    [Notes] nvarchar(1024)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2BankAccount'
CREATE TABLE [dbo].[Person2BankAccount] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [BankAccount_Id] int  NOT NULL,
    [AccountLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2BBPin'
CREATE TABLE [dbo].[Person2BBPin] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [BBPin_Id] int  NOT NULL,
    [BBPinLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2DrivingLicense'
CREATE TABLE [dbo].[Person2DrivingLicense] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [DrivingLicense_Id] int  NOT NULL,
    [DrivingLicenseLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2Email'
CREATE TABLE [dbo].[Person2Email] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [Email_Id] int  NOT NULL,
    [EmailLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2IPAddress'
CREATE TABLE [dbo].[Person2IPAddress] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [IPAddress_Id] int  NOT NULL,
    [IPAddressLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2NINumber'
CREATE TABLE [dbo].[Person2NINumber] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [NINumber_Id] int  NOT NULL,
    [NINumberLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2Organisation'
CREATE TABLE [dbo].[Person2Organisation] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [Person2OrganisationLinkId] nvarchar(50)  NULL,
    [Person2OrganisationLinkType_Id] int  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2OrganisationLinkType'
CREATE TABLE [dbo].[Person2OrganisationLinkType] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [LinkTypeText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2Passport'
CREATE TABLE [dbo].[Person2Passport] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [Passport_Id] int  NOT NULL,
    [PassportLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2PaymentCard'
CREATE TABLE [dbo].[Person2PaymentCard] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [PaymentCard_Id] int  NOT NULL,
    [PaymentCardLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2Person'
CREATE TABLE [dbo].[Person2Person] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person1_Id] int  NOT NULL,
    [Person2_Id] int  NOT NULL,
    [Person2PersonLinkId] nvarchar(50)  NULL,
    [Person2PersonLinkType_Id] int  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2PersonLinkType'
CREATE TABLE [dbo].[Person2PersonLinkType] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [LinkText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2Policy'
CREATE TABLE [dbo].[Person2Policy] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [Policy_Id] int  NOT NULL,
    [PolicyLinkId] nvarchar(50)  NULL,
    [PolicyLinkType_Id] int  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2Telephone'
CREATE TABLE [dbo].[Person2Telephone] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [Telephone_Id] int  NOT NULL,
    [TelephoneLinkId] nvarchar(50)  NULL,
    [TelephoneLinkType_Id] int  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Person2WebSite'
CREATE TABLE [dbo].[Person2WebSite] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Person_Id] int  NOT NULL,
    [WebSite_Id] int  NOT NULL,
    [WebSiteLinkId] nvarchar(50)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Policies'
CREATE TABLE [dbo].[Policies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PolicyId] nvarchar(50)  NULL,
    [PolicyNumber] nvarchar(50)  NOT NULL,
    [Insurer] varchar(50)  NULL,
    [InsurerTradingName] varchar(50)  NULL,
    [Broker] varchar(50)  NULL,
    [PolicyType_Id] int  NOT NULL,
    [PolicyCoverType_Id] int  NOT NULL,
    [PolicyStartDate] datetime  NULL,
    [PolicyEndDate] datetime  NULL,
    [PreviousNoFaultClaimsCount] int  NULL,
    [PreviousFaultClaimsCount] int  NULL,
    [Premium] decimal(19,4)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Policy2BankAccount'
CREATE TABLE [dbo].[Policy2BankAccount] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Policy_Id] int  NOT NULL,
    [BankAccount_Id] int  NOT NULL,
    [PolicyPaymentLinkId] nvarchar(50)  NULL,
    [PolicyPaymentType_Id] int  NOT NULL,
    [DatePaymentDetailsTaken] datetime  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Policy2PaymentCard'
CREATE TABLE [dbo].[Policy2PaymentCard] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Policy_Id] int  NOT NULL,
    [PaymentCard_Id] int  NOT NULL,
    [PolicyPaymentLinkId] nvarchar(50)  NULL,
    [PolicyPaymentType_Id] int  NOT NULL,
    [DatePaymentDetailsTaken] datetime  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'PolicyCoverTypes'
CREATE TABLE [dbo].[PolicyCoverTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CoverText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'PolicyLinkTypes'
CREATE TABLE [dbo].[PolicyLinkTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [LinkText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'PolicyPaymentTypes'
CREATE TABLE [dbo].[PolicyPaymentTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] varchar(50)  NOT NULL
);
GO

-- Creating table 'PolicyTypes'
CREATE TABLE [dbo].[PolicyTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'RiskBatches'
CREATE TABLE [dbo].[RiskBatches] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Client_Id] int  NOT NULL,
    [BatchReference] varchar(50)  NOT NULL,
    [BatchStatus] int  NOT NULL,
    [OriginalFile_Id] int  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [CreatedBy] varchar(50)  NOT NULL
);
GO

-- Creating table 'RiskClaims'
CREATE TABLE [dbo].[RiskClaims] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [RiskBatch_Id] int  NOT NULL,
    [Incident_Id] int  NOT NULL,
    [MdaClaimRef] nvarchar(50)  NOT NULL,
    [BaseRiskClaim_Id] int  NOT NULL,
    [ClaimStatus] int  NOT NULL,
    [ClientClaimRefNumber] nvarchar(50)  NOT NULL,
    [LevelOneRequestedCount] int  NOT NULL,
    [LevelTwoRequestedCount] int  NOT NULL,
    [LevelOneRequestedWhen] datetime  NULL,
    [LevelTwoRequestedWhen] datetime  NULL,
    [LevelOneRequestedWho] nvarchar(50)  NULL,
    [LevelTwoRequestedWho] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NOT NULL,
    [SourceEntityXML] nvarchar(max)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [CreatedBy] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'RiskClaimRuns'
CREATE TABLE [dbo].[RiskClaimRuns] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [RiskClaim_Id] int  NOT NULL,
    [EntityType] varchar(200)  NULL,
    [ScoreEntityJson] nvarchar(max)  NULL,
    [ScoreEntityXml] nvarchar(max)  NULL,
    [TotalScore] int  NOT NULL,
    [TotalKeyAttractorCount] int  NOT NULL,
    [ReportOneVersion] nvarchar(10)  NULL,
    [ReportTwoVersion] nvarchar(10)  NULL,
    [ExecutedBy] nvarchar(50)  NOT NULL,
    [ExecutedWhen] datetime  NOT NULL
);
GO

-- Creating table 'RiskClients'
CREATE TABLE [dbo].[RiskClients] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ClientName] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'RiskOriginalFiles'
CREATE TABLE [dbo].[RiskOriginalFiles] (
    [PkId] int IDENTITY(1,1) NOT NULL,
    [Id] uniqueidentifier  NOT NULL,
    [Description] nvarchar(64)  NOT NULL,
    [FileData] varbinary(max)  NULL
);
GO

-- Creating table 'Salutations'
CREATE TABLE [dbo].[Salutations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(20)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'SubPartyTypes'
CREATE TABLE [dbo].[SubPartyTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SubPartyText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Telephones'
CREATE TABLE [dbo].[Telephones] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TelephoneId] nvarchar(50)  NULL,
    [TelephoneNumber] nvarchar(50)  NULL,
    [CountryCode] nvarchar(10)  NULL,
    [STDCode] varchar(10)  NULL,
    [TelephoneType_Id] int  NOT NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TelephoneLinkTypes'
CREATE TABLE [dbo].[TelephoneLinkTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TelephoneTypes'
CREATE TABLE [dbo].[TelephoneTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PhoneText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOGs'
CREATE TABLE [dbo].[TOGs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TogId] nvarchar(50)  NULL,
    [TogReference] nvarchar(50)  NULL,
    [ReferredBy] nvarchar(50)  NULL,
    [DateOfReview] datetime  NULL,
    [Decision] nvarchar(20)  NULL,
    [AllocatedTo] nvarchar(50)  NULL,
    [DateAllocated] datetime  NULL,
    [DateCompleted] datetime  NULL,
    [Report] nvarchar(250)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2Account'
CREATE TABLE [dbo].[TOG2Account] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [Account_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2Address'
CREATE TABLE [dbo].[TOG2Address] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [Address_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2DrivingLicense'
CREATE TABLE [dbo].[TOG2DrivingLicense] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [DrivingLicense_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2Email'
CREATE TABLE [dbo].[TOG2Email] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [Email_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2FraudRing'
CREATE TABLE [dbo].[TOG2FraudRing] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [FraudRing_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2Incident'
CREATE TABLE [dbo].[TOG2Incident] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [Incident_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2IPAddress'
CREATE TABLE [dbo].[TOG2IPAddress] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [IPAddress_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2KeoghsCase'
CREATE TABLE [dbo].[TOG2KeoghsCase] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [KeoghsCase_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2NINumber'
CREATE TABLE [dbo].[TOG2NINumber] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [NINumber_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2Organisation'
CREATE TABLE [dbo].[TOG2Organisation] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2Passport'
CREATE TABLE [dbo].[TOG2Passport] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [Passport_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2PaymentCard'
CREATE TABLE [dbo].[TOG2PaymentCard] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [PaymentCard_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2Person'
CREATE TABLE [dbo].[TOG2Person] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [Person_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2Policy'
CREATE TABLE [dbo].[TOG2Policy] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [Policy_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2Telephone'
CREATE TABLE [dbo].[TOG2Telephone] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [Telephone_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2TOG'
CREATE TABLE [dbo].[TOG2TOG] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG1_Id] int  NOT NULL,
    [TOG2_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2Vehicle'
CREATE TABLE [dbo].[TOG2Vehicle] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [Vehicle_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'TOG2Website'
CREATE TABLE [dbo].[TOG2Website] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TOG_Id] int  NOT NULL,
    [Website_Id] int  NOT NULL,
    [TogLinkId] nvarchar(50)  NULL,
    [Notes] nvarchar(500)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Vehicles'
CREATE TABLE [dbo].[Vehicles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [VehicleId] nvarchar(50)  NULL,
    [VehicleColour_Id] int  NOT NULL,
    [VehicleRegistration] nvarchar(20)  NULL,
    [VehicleMake] nvarchar(50)  NULL,
    [Model] nvarchar(20)  NULL,
    [EngineCapacity] nvarchar(50)  NULL,
    [VIN] nvarchar(20)  NULL,
    [VehicleFuel_Id] int  NOT NULL,
    [VehicleTransmission_Id] int  NOT NULL,
    [VehicleType_Id] int  NOT NULL,
    [VehicleCategoryOfLoss_Id] int  NOT NULL,
    [Notes] nvarchar(1024)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Vehicle2Address'
CREATE TABLE [dbo].[Vehicle2Address] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Vehicle_Id] int  NOT NULL,
    [Address_Id] int  NOT NULL,
    [AddressLinkId] nvarchar(50)  NULL,
    [AddressLinkType_Id] int  NOT NULL,
    [StartOfResidency] datetime  NULL,
    [EndOfResidency] datetime  NULL,
    [Notes] nvarchar(1024)  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Vehicle2Organisation'
CREATE TABLE [dbo].[Vehicle2Organisation] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Vehicle_Id] int  NOT NULL,
    [Organisation_Id] int  NOT NULL,
    [VehicleLinkId] nvarchar(50)  NULL,
    [VehicleLinkType_Id] int  NOT NULL,
    [RegKeeperStartDate] datetime  NULL,
    [RegKeeperEndDate] datetime  NULL,
    [HireCompany] nvarchar(255)  NULL,
    [CrossHireCompany] nvarchar(255)  NULL,
    [HireStartDate] datetime  NULL,
    [HireEndDate] datetime  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Vehicle2Person'
CREATE TABLE [dbo].[Vehicle2Person] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Vehicle_Id] int  NOT NULL,
    [Person_Id] int  NOT NULL,
    [VehicleLinkId] nvarchar(50)  NULL,
    [VehicleLinkType_Id] int  NOT NULL,
    [RegKeeperStartDate] datetime  NULL,
    [RegKeeperEndDate] datetime  NULL,
    [HireCompany] nvarchar(255)  NULL,
    [CrossHireCompany] nvarchar(255)  NULL,
    [HireStartDate] datetime  NULL,
    [HireEndDate] datetime  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Vehicle2Policy'
CREATE TABLE [dbo].[Vehicle2Policy] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Vehicle_Id] int  NOT NULL,
    [Policy_Id] int  NOT NULL,
    [PolicyLinkId] nvarchar(50)  NULL,
    [PolicyLinkType_Id] int  NOT NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'Vehicle2Vehicle'
CREATE TABLE [dbo].[Vehicle2Vehicle] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Vehicle1_Id] int  NOT NULL,
    [Vehicle2_Id] int  NOT NULL,
    [Vehicle2VehicleLinkId] nvarchar(50)  NULL,
    [DateOfRegChange] datetime  NULL,
    [FiveGrading] nvarchar(10)  NULL,
    [LinkConfidence] int  NOT NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [RiskClaim_Id] int  NULL,
    [BaseRiskClaim_Id] int  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'VehicleCategoryOfLosses'
CREATE TABLE [dbo].[VehicleCategoryOfLosses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CatText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'VehicleColours'
CREATE TABLE [dbo].[VehicleColours] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Colour] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'VehicleFuels'
CREATE TABLE [dbo].[VehicleFuels] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FuelText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'VehicleLinkTypes'
CREATE TABLE [dbo].[VehicleLinkTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [LinkText] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'VehicleMakes'
CREATE TABLE [dbo].[VehicleMakes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Make] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'VehicleTransmissions'
CREATE TABLE [dbo].[VehicleTransmissions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'VehicleTypes'
CREATE TABLE [dbo].[VehicleTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(50)  NOT NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'WebSites'
CREATE TABLE [dbo].[WebSites] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [WebSiteId] nvarchar(50)  NULL,
    [URL] nvarchar(255)  NULL,
    [KeyAttractor] nvarchar(100)  NULL,
    [Source] nvarchar(50)  NULL,
    [SourceReference] nvarchar(50)  NULL,
    [SourceDescription] nvarchar(1024)  NULL,
    [CreatedBy] nvarchar(50)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(50)  NULL,
    [ModifiedDate] datetime  NULL,
    [IBaseId] nvarchar(50)  NULL,
    [RecordStatus] tinyint  NOT NULL,
    [ADARecordStatus] tinyint  NOT NULL
);
GO

-- Creating table 'ClearcoreSyncs'
CREATE TABLE [dbo].[ClearcoreSyncs] (
    [Id] int  NOT NULL,
    [LastTimeStamp] datetime  NULL,
    [CurrentTimeStamp] datetime  NULL
);
GO

-- Creating table 'RiskClaimRunMessages'
CREATE TABLE [dbo].[RiskClaimRunMessages] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [RiskClaimRun_Id] int  NOT NULL,
    [ReportType] int  NOT NULL,
    [LineNumber] int  NOT NULL,
    [Message] varchar(2000)  NOT NULL
);
GO

-- Creating table 'ClearcoreSync1'
CREATE TABLE [dbo].[ClearcoreSync1] (
    [LastTimeStamp] datetime  NULL,
    [CurrentTimeStamp] datetime  NULL,
    [Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Addresses'
ALTER TABLE [dbo].[Addresses]
ADD CONSTRAINT [PK_Addresses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Address2Address'
ALTER TABLE [dbo].[Address2Address]
ADD CONSTRAINT [PK_Address2Address]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Address2Telephone'
ALTER TABLE [dbo].[Address2Telephone]
ADD CONSTRAINT [PK_Address2Telephone]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AddressLinkTypes'
ALTER TABLE [dbo].[AddressLinkTypes]
ADD CONSTRAINT [PK_AddressLinkTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AddressTypes'
ALTER TABLE [dbo].[AddressTypes]
ADD CONSTRAINT [PK_AddressTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BankAccounts'
ALTER TABLE [dbo].[BankAccounts]
ADD CONSTRAINT [PK_BankAccounts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BankNames'
ALTER TABLE [dbo].[BankNames]
ADD CONSTRAINT [PK_BankNames]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BBPins'
ALTER TABLE [dbo].[BBPins]
ADD CONSTRAINT [PK_BBPins]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClaimStatus'
ALTER TABLE [dbo].[ClaimStatus]
ADD CONSTRAINT [PK_ClaimStatus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClaimTypes'
ALTER TABLE [dbo].[ClaimTypes]
ADD CONSTRAINT [PK_ClaimTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Counties'
ALTER TABLE [dbo].[Counties]
ADD CONSTRAINT [PK_Counties]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DrivingLicenses'
ALTER TABLE [dbo].[DrivingLicenses]
ADD CONSTRAINT [PK_DrivingLicenses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Emails'
ALTER TABLE [dbo].[Emails]
ADD CONSTRAINT [PK_Emails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FraudRings'
ALTER TABLE [dbo].[FraudRings]
ADD CONSTRAINT [PK_FraudRings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Genders'
ALTER TABLE [dbo].[Genders]
ADD CONSTRAINT [PK_Genders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Incidents'
ALTER TABLE [dbo].[Incidents]
ADD CONSTRAINT [PK_Incidents]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Incident2FraudRing'
ALTER TABLE [dbo].[Incident2FraudRing]
ADD CONSTRAINT [PK_Incident2FraudRing]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Incident2Incident'
ALTER TABLE [dbo].[Incident2Incident]
ADD CONSTRAINT [PK_Incident2Incident]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Incident2Organisation'
ALTER TABLE [dbo].[Incident2Organisation]
ADD CONSTRAINT [PK_Incident2Organisation]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Incident2OrganisationOutcome'
ALTER TABLE [dbo].[Incident2OrganisationOutcome]
ADD CONSTRAINT [PK_Incident2OrganisationOutcome]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Incident2Person'
ALTER TABLE [dbo].[Incident2Person]
ADD CONSTRAINT [PK_Incident2Person]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Incident2PersonOutcome'
ALTER TABLE [dbo].[Incident2PersonOutcome]
ADD CONSTRAINT [PK_Incident2PersonOutcome]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Incident2Vehicle'
ALTER TABLE [dbo].[Incident2Vehicle]
ADD CONSTRAINT [PK_Incident2Vehicle]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Incident2VehicleLinkType'
ALTER TABLE [dbo].[Incident2VehicleLinkType]
ADD CONSTRAINT [PK_Incident2VehicleLinkType]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'IncidentTypes'
ALTER TABLE [dbo].[IncidentTypes]
ADD CONSTRAINT [PK_IncidentTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InsurersClients'
ALTER TABLE [dbo].[InsurersClients]
ADD CONSTRAINT [PK_InsurersClients]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'IPAddresses'
ALTER TABLE [dbo].[IPAddresses]
ADD CONSTRAINT [PK_IPAddresses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'KeoghsCases'
ALTER TABLE [dbo].[KeoghsCases]
ADD CONSTRAINT [PK_KeoghsCases]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'KeoghsCase2Incident'
ALTER TABLE [dbo].[KeoghsCase2Incident]
ADD CONSTRAINT [PK_KeoghsCase2Incident]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'KeoghsCase2IncidentLinkType'
ALTER TABLE [dbo].[KeoghsCase2IncidentLinkType]
ADD CONSTRAINT [PK_KeoghsCase2IncidentLinkType]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'KeoghsCaseStatus'
ALTER TABLE [dbo].[KeoghsCaseStatus]
ADD CONSTRAINT [PK_KeoghsCaseStatus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'KeoghsOffices'
ALTER TABLE [dbo].[KeoghsOffices]
ADD CONSTRAINT [PK_KeoghsOffices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MojCRMStatus'
ALTER TABLE [dbo].[MojCRMStatus]
ADD CONSTRAINT [PK_MojCRMStatus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MojStatus'
ALTER TABLE [dbo].[MojStatus]
ADD CONSTRAINT [PK_MojStatus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Nationalities'
ALTER TABLE [dbo].[Nationalities]
ADD CONSTRAINT [PK_Nationalities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'NINumbers'
ALTER TABLE [dbo].[NINumbers]
ADD CONSTRAINT [PK_NINumbers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organisations'
ALTER TABLE [dbo].[Organisations]
ADD CONSTRAINT [PK_Organisations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organisation2Address'
ALTER TABLE [dbo].[Organisation2Address]
ADD CONSTRAINT [PK_Organisation2Address]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organisation2BankAccount'
ALTER TABLE [dbo].[Organisation2BankAccount]
ADD CONSTRAINT [PK_Organisation2BankAccount]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organisation2Email'
ALTER TABLE [dbo].[Organisation2Email]
ADD CONSTRAINT [PK_Organisation2Email]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organisation2IPAddress'
ALTER TABLE [dbo].[Organisation2IPAddress]
ADD CONSTRAINT [PK_Organisation2IPAddress]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organisation2Organisation'
ALTER TABLE [dbo].[Organisation2Organisation]
ADD CONSTRAINT [PK_Organisation2Organisation]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organisation2PaymentCard'
ALTER TABLE [dbo].[Organisation2PaymentCard]
ADD CONSTRAINT [PK_Organisation2PaymentCard]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organisation2Policy'
ALTER TABLE [dbo].[Organisation2Policy]
ADD CONSTRAINT [PK_Organisation2Policy]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organisation2Telephone'
ALTER TABLE [dbo].[Organisation2Telephone]
ADD CONSTRAINT [PK_Organisation2Telephone]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organisation2WebSite'
ALTER TABLE [dbo].[Organisation2WebSite]
ADD CONSTRAINT [PK_Organisation2WebSite]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganisationLinkTypes'
ALTER TABLE [dbo].[OrganisationLinkTypes]
ADD CONSTRAINT [PK_OrganisationLinkTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganisationStatus'
ALTER TABLE [dbo].[OrganisationStatus]
ADD CONSTRAINT [PK_OrganisationStatus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganisationTypes'
ALTER TABLE [dbo].[OrganisationTypes]
ADD CONSTRAINT [PK_OrganisationTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PartyTypes'
ALTER TABLE [dbo].[PartyTypes]
ADD CONSTRAINT [PK_PartyTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Passports'
ALTER TABLE [dbo].[Passports]
ADD CONSTRAINT [PK_Passports]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PaymentCards'
ALTER TABLE [dbo].[PaymentCards]
ADD CONSTRAINT [PK_PaymentCards]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PaymentCardTypes'
ALTER TABLE [dbo].[PaymentCardTypes]
ADD CONSTRAINT [PK_PaymentCardTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'People'
ALTER TABLE [dbo].[People]
ADD CONSTRAINT [PK_People]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2Address'
ALTER TABLE [dbo].[Person2Address]
ADD CONSTRAINT [PK_Person2Address]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2BankAccount'
ALTER TABLE [dbo].[Person2BankAccount]
ADD CONSTRAINT [PK_Person2BankAccount]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2BBPin'
ALTER TABLE [dbo].[Person2BBPin]
ADD CONSTRAINT [PK_Person2BBPin]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2DrivingLicense'
ALTER TABLE [dbo].[Person2DrivingLicense]
ADD CONSTRAINT [PK_Person2DrivingLicense]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2Email'
ALTER TABLE [dbo].[Person2Email]
ADD CONSTRAINT [PK_Person2Email]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2IPAddress'
ALTER TABLE [dbo].[Person2IPAddress]
ADD CONSTRAINT [PK_Person2IPAddress]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2NINumber'
ALTER TABLE [dbo].[Person2NINumber]
ADD CONSTRAINT [PK_Person2NINumber]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2Organisation'
ALTER TABLE [dbo].[Person2Organisation]
ADD CONSTRAINT [PK_Person2Organisation]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2OrganisationLinkType'
ALTER TABLE [dbo].[Person2OrganisationLinkType]
ADD CONSTRAINT [PK_Person2OrganisationLinkType]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2Passport'
ALTER TABLE [dbo].[Person2Passport]
ADD CONSTRAINT [PK_Person2Passport]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2PaymentCard'
ALTER TABLE [dbo].[Person2PaymentCard]
ADD CONSTRAINT [PK_Person2PaymentCard]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2Person'
ALTER TABLE [dbo].[Person2Person]
ADD CONSTRAINT [PK_Person2Person]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2PersonLinkType'
ALTER TABLE [dbo].[Person2PersonLinkType]
ADD CONSTRAINT [PK_Person2PersonLinkType]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2Policy'
ALTER TABLE [dbo].[Person2Policy]
ADD CONSTRAINT [PK_Person2Policy]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2Telephone'
ALTER TABLE [dbo].[Person2Telephone]
ADD CONSTRAINT [PK_Person2Telephone]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Person2WebSite'
ALTER TABLE [dbo].[Person2WebSite]
ADD CONSTRAINT [PK_Person2WebSite]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Policies'
ALTER TABLE [dbo].[Policies]
ADD CONSTRAINT [PK_Policies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Policy2BankAccount'
ALTER TABLE [dbo].[Policy2BankAccount]
ADD CONSTRAINT [PK_Policy2BankAccount]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Policy2PaymentCard'
ALTER TABLE [dbo].[Policy2PaymentCard]
ADD CONSTRAINT [PK_Policy2PaymentCard]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PolicyCoverTypes'
ALTER TABLE [dbo].[PolicyCoverTypes]
ADD CONSTRAINT [PK_PolicyCoverTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PolicyLinkTypes'
ALTER TABLE [dbo].[PolicyLinkTypes]
ADD CONSTRAINT [PK_PolicyLinkTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PolicyPaymentTypes'
ALTER TABLE [dbo].[PolicyPaymentTypes]
ADD CONSTRAINT [PK_PolicyPaymentTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PolicyTypes'
ALTER TABLE [dbo].[PolicyTypes]
ADD CONSTRAINT [PK_PolicyTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RiskBatches'
ALTER TABLE [dbo].[RiskBatches]
ADD CONSTRAINT [PK_RiskBatches]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RiskClaims'
ALTER TABLE [dbo].[RiskClaims]
ADD CONSTRAINT [PK_RiskClaims]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RiskClaimRuns'
ALTER TABLE [dbo].[RiskClaimRuns]
ADD CONSTRAINT [PK_RiskClaimRuns]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RiskClients'
ALTER TABLE [dbo].[RiskClients]
ADD CONSTRAINT [PK_RiskClients]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [PkId] in table 'RiskOriginalFiles'
ALTER TABLE [dbo].[RiskOriginalFiles]
ADD CONSTRAINT [PK_RiskOriginalFiles]
    PRIMARY KEY CLUSTERED ([PkId] ASC);
GO

-- Creating primary key on [Id] in table 'Salutations'
ALTER TABLE [dbo].[Salutations]
ADD CONSTRAINT [PK_Salutations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SubPartyTypes'
ALTER TABLE [dbo].[SubPartyTypes]
ADD CONSTRAINT [PK_SubPartyTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Telephones'
ALTER TABLE [dbo].[Telephones]
ADD CONSTRAINT [PK_Telephones]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TelephoneLinkTypes'
ALTER TABLE [dbo].[TelephoneLinkTypes]
ADD CONSTRAINT [PK_TelephoneLinkTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TelephoneTypes'
ALTER TABLE [dbo].[TelephoneTypes]
ADD CONSTRAINT [PK_TelephoneTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOGs'
ALTER TABLE [dbo].[TOGs]
ADD CONSTRAINT [PK_TOGs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2Account'
ALTER TABLE [dbo].[TOG2Account]
ADD CONSTRAINT [PK_TOG2Account]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2Address'
ALTER TABLE [dbo].[TOG2Address]
ADD CONSTRAINT [PK_TOG2Address]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2DrivingLicense'
ALTER TABLE [dbo].[TOG2DrivingLicense]
ADD CONSTRAINT [PK_TOG2DrivingLicense]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2Email'
ALTER TABLE [dbo].[TOG2Email]
ADD CONSTRAINT [PK_TOG2Email]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2FraudRing'
ALTER TABLE [dbo].[TOG2FraudRing]
ADD CONSTRAINT [PK_TOG2FraudRing]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2Incident'
ALTER TABLE [dbo].[TOG2Incident]
ADD CONSTRAINT [PK_TOG2Incident]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2IPAddress'
ALTER TABLE [dbo].[TOG2IPAddress]
ADD CONSTRAINT [PK_TOG2IPAddress]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2KeoghsCase'
ALTER TABLE [dbo].[TOG2KeoghsCase]
ADD CONSTRAINT [PK_TOG2KeoghsCase]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2NINumber'
ALTER TABLE [dbo].[TOG2NINumber]
ADD CONSTRAINT [PK_TOG2NINumber]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2Organisation'
ALTER TABLE [dbo].[TOG2Organisation]
ADD CONSTRAINT [PK_TOG2Organisation]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2Passport'
ALTER TABLE [dbo].[TOG2Passport]
ADD CONSTRAINT [PK_TOG2Passport]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2PaymentCard'
ALTER TABLE [dbo].[TOG2PaymentCard]
ADD CONSTRAINT [PK_TOG2PaymentCard]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2Person'
ALTER TABLE [dbo].[TOG2Person]
ADD CONSTRAINT [PK_TOG2Person]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2Policy'
ALTER TABLE [dbo].[TOG2Policy]
ADD CONSTRAINT [PK_TOG2Policy]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2Telephone'
ALTER TABLE [dbo].[TOG2Telephone]
ADD CONSTRAINT [PK_TOG2Telephone]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2TOG'
ALTER TABLE [dbo].[TOG2TOG]
ADD CONSTRAINT [PK_TOG2TOG]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2Vehicle'
ALTER TABLE [dbo].[TOG2Vehicle]
ADD CONSTRAINT [PK_TOG2Vehicle]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TOG2Website'
ALTER TABLE [dbo].[TOG2Website]
ADD CONSTRAINT [PK_TOG2Website]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [PK_Vehicles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Vehicle2Address'
ALTER TABLE [dbo].[Vehicle2Address]
ADD CONSTRAINT [PK_Vehicle2Address]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Vehicle2Organisation'
ALTER TABLE [dbo].[Vehicle2Organisation]
ADD CONSTRAINT [PK_Vehicle2Organisation]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Vehicle2Person'
ALTER TABLE [dbo].[Vehicle2Person]
ADD CONSTRAINT [PK_Vehicle2Person]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Vehicle2Policy'
ALTER TABLE [dbo].[Vehicle2Policy]
ADD CONSTRAINT [PK_Vehicle2Policy]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Vehicle2Vehicle'
ALTER TABLE [dbo].[Vehicle2Vehicle]
ADD CONSTRAINT [PK_Vehicle2Vehicle]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VehicleCategoryOfLosses'
ALTER TABLE [dbo].[VehicleCategoryOfLosses]
ADD CONSTRAINT [PK_VehicleCategoryOfLosses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VehicleColours'
ALTER TABLE [dbo].[VehicleColours]
ADD CONSTRAINT [PK_VehicleColours]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VehicleFuels'
ALTER TABLE [dbo].[VehicleFuels]
ADD CONSTRAINT [PK_VehicleFuels]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VehicleLinkTypes'
ALTER TABLE [dbo].[VehicleLinkTypes]
ADD CONSTRAINT [PK_VehicleLinkTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VehicleMakes'
ALTER TABLE [dbo].[VehicleMakes]
ADD CONSTRAINT [PK_VehicleMakes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VehicleTransmissions'
ALTER TABLE [dbo].[VehicleTransmissions]
ADD CONSTRAINT [PK_VehicleTransmissions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VehicleTypes'
ALTER TABLE [dbo].[VehicleTypes]
ADD CONSTRAINT [PK_VehicleTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'WebSites'
ALTER TABLE [dbo].[WebSites]
ADD CONSTRAINT [PK_WebSites]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClearcoreSyncs'
ALTER TABLE [dbo].[ClearcoreSyncs]
ADD CONSTRAINT [PK_ClearcoreSyncs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RiskClaimRunMessages'
ALTER TABLE [dbo].[RiskClaimRunMessages]
ADD CONSTRAINT [PK_RiskClaimRunMessages]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClearcoreSync1'
ALTER TABLE [dbo].[ClearcoreSync1]
ADD CONSTRAINT [PK_ClearcoreSync1]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Address1_Id] in table 'Address2Address'
ALTER TABLE [dbo].[Address2Address]
ADD CONSTRAINT [FK_Address2Address_Address]
    FOREIGN KEY ([Address1_Id])
    REFERENCES [dbo].[Addresses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Address2Address_Address'
CREATE INDEX [IX_FK_Address2Address_Address]
ON [dbo].[Address2Address]
    ([Address1_Id]);
GO

-- Creating foreign key on [Address2_Id] in table 'Address2Address'
ALTER TABLE [dbo].[Address2Address]
ADD CONSTRAINT [FK_Address2Address_Address1]
    FOREIGN KEY ([Address2_Id])
    REFERENCES [dbo].[Addresses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Address2Address_Address1'
CREATE INDEX [IX_FK_Address2Address_Address1]
ON [dbo].[Address2Address]
    ([Address2_Id]);
GO

-- Creating foreign key on [Address_Id] in table 'Address2Telephone'
ALTER TABLE [dbo].[Address2Telephone]
ADD CONSTRAINT [FK_Address2Telephone_Address]
    FOREIGN KEY ([Address_Id])
    REFERENCES [dbo].[Addresses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Address2Telephone_Address'
CREATE INDEX [IX_FK_Address2Telephone_Address]
ON [dbo].[Address2Telephone]
    ([Address_Id]);
GO

-- Creating foreign key on [AddressType_Id] in table 'Addresses'
ALTER TABLE [dbo].[Addresses]
ADD CONSTRAINT [FK_AddressTypeAddress]
    FOREIGN KEY ([AddressType_Id])
    REFERENCES [dbo].[AddressTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AddressTypeAddress'
CREATE INDEX [IX_FK_AddressTypeAddress]
ON [dbo].[Addresses]
    ([AddressType_Id]);
GO

-- Creating foreign key on [Address_Id] in table 'Organisation2Address'
ALTER TABLE [dbo].[Organisation2Address]
ADD CONSTRAINT [FK_Organisation2Address_Address]
    FOREIGN KEY ([Address_Id])
    REFERENCES [dbo].[Addresses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Address_Address'
CREATE INDEX [IX_FK_Organisation2Address_Address]
ON [dbo].[Organisation2Address]
    ([Address_Id]);
GO

-- Creating foreign key on [Address_Id] in table 'Person2Address'
ALTER TABLE [dbo].[Person2Address]
ADD CONSTRAINT [FK_Person2Address_Address]
    FOREIGN KEY ([Address_Id])
    REFERENCES [dbo].[Addresses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Address_Address'
CREATE INDEX [IX_FK_Person2Address_Address]
ON [dbo].[Person2Address]
    ([Address_Id]);
GO

-- Creating foreign key on [Address_Id] in table 'Vehicle2Address'
ALTER TABLE [dbo].[Vehicle2Address]
ADD CONSTRAINT [FK_Vehicle2Address_Address]
    FOREIGN KEY ([Address_Id])
    REFERENCES [dbo].[Addresses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Address_Address'
CREATE INDEX [IX_FK_Vehicle2Address_Address]
ON [dbo].[Vehicle2Address]
    ([Address_Id]);
GO

-- Creating foreign key on [AddressLinkType_Id] in table 'Address2Address'
ALTER TABLE [dbo].[Address2Address]
ADD CONSTRAINT [FK_Address2Address_AddressLinkType]
    FOREIGN KEY ([AddressLinkType_Id])
    REFERENCES [dbo].[AddressLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Address2Address_AddressLinkType'
CREATE INDEX [IX_FK_Address2Address_AddressLinkType]
ON [dbo].[Address2Address]
    ([AddressLinkType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Address2Address'
ALTER TABLE [dbo].[Address2Address]
ADD CONSTRAINT [FK_Address2Address_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Address2Address_RiskClaim'
CREATE INDEX [IX_FK_Address2Address_RiskClaim]
ON [dbo].[Address2Address]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Address2Telephone'
ALTER TABLE [dbo].[Address2Telephone]
ADD CONSTRAINT [FK_Address2Telephone_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Address2Telephone_RiskClaim'
CREATE INDEX [IX_FK_Address2Telephone_RiskClaim]
ON [dbo].[Address2Telephone]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [Telephone_Id] in table 'Address2Telephone'
ALTER TABLE [dbo].[Address2Telephone]
ADD CONSTRAINT [FK_Address2Telephone_Telephone]
    FOREIGN KEY ([Telephone_Id])
    REFERENCES [dbo].[Telephones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Address2Telephone_Telephone'
CREATE INDEX [IX_FK_Address2Telephone_Telephone]
ON [dbo].[Address2Telephone]
    ([Telephone_Id]);
GO

-- Creating foreign key on [TelephoneLinkType_Id] in table 'Address2Telephone'
ALTER TABLE [dbo].[Address2Telephone]
ADD CONSTRAINT [FK_Address2Telephone_TelephoneLinkType]
    FOREIGN KEY ([TelephoneLinkType_Id])
    REFERENCES [dbo].[TelephoneLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Address2Telephone_TelephoneLinkType'
CREATE INDEX [IX_FK_Address2Telephone_TelephoneLinkType]
ON [dbo].[Address2Telephone]
    ([TelephoneLinkType_Id]);
GO

-- Creating foreign key on [AddressLinkType_Id] in table 'Organisation2Address'
ALTER TABLE [dbo].[Organisation2Address]
ADD CONSTRAINT [FK_Organisation2Address_AddressLinkType]
    FOREIGN KEY ([AddressLinkType_Id])
    REFERENCES [dbo].[AddressLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Address_AddressLinkType'
CREATE INDEX [IX_FK_Organisation2Address_AddressLinkType]
ON [dbo].[Organisation2Address]
    ([AddressLinkType_Id]);
GO

-- Creating foreign key on [AddressLinkType_Id] in table 'Person2Address'
ALTER TABLE [dbo].[Person2Address]
ADD CONSTRAINT [FK_Person2Address_AddressLinkType]
    FOREIGN KEY ([AddressLinkType_Id])
    REFERENCES [dbo].[AddressLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Address_AddressLinkType'
CREATE INDEX [IX_FK_Person2Address_AddressLinkType]
ON [dbo].[Person2Address]
    ([AddressLinkType_Id]);
GO

-- Creating foreign key on [AddressLinkType_Id] in table 'Vehicle2Address'
ALTER TABLE [dbo].[Vehicle2Address]
ADD CONSTRAINT [FK_Vehicle2Address_AddressLinkType]
    FOREIGN KEY ([AddressLinkType_Id])
    REFERENCES [dbo].[AddressLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Address_AddressLinkType'
CREATE INDEX [IX_FK_Vehicle2Address_AddressLinkType]
ON [dbo].[Vehicle2Address]
    ([AddressLinkType_Id]);
GO

-- Creating foreign key on [BankAccount_Id] in table 'Organisation2BankAccount'
ALTER TABLE [dbo].[Organisation2BankAccount]
ADD CONSTRAINT [FK_Organisation2BankAccount_BankAccount]
    FOREIGN KEY ([BankAccount_Id])
    REFERENCES [dbo].[BankAccounts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2BankAccount_BankAccount'
CREATE INDEX [IX_FK_Organisation2BankAccount_BankAccount]
ON [dbo].[Organisation2BankAccount]
    ([BankAccount_Id]);
GO

-- Creating foreign key on [BankAccount_Id] in table 'Person2BankAccount'
ALTER TABLE [dbo].[Person2BankAccount]
ADD CONSTRAINT [FK_Person2BankAccount_BankAccount]
    FOREIGN KEY ([BankAccount_Id])
    REFERENCES [dbo].[BankAccounts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2BankAccount_BankAccount'
CREATE INDEX [IX_FK_Person2BankAccount_BankAccount]
ON [dbo].[Person2BankAccount]
    ([BankAccount_Id]);
GO

-- Creating foreign key on [BankAccount_Id] in table 'Policy2BankAccount'
ALTER TABLE [dbo].[Policy2BankAccount]
ADD CONSTRAINT [FK_Policy2BankAccount_BankAccount]
    FOREIGN KEY ([BankAccount_Id])
    REFERENCES [dbo].[BankAccounts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Policy2BankAccount_BankAccount'
CREATE INDEX [IX_FK_Policy2BankAccount_BankAccount]
ON [dbo].[Policy2BankAccount]
    ([BankAccount_Id]);
GO

-- Creating foreign key on [Account_Id] in table 'TOG2Account'
ALTER TABLE [dbo].[TOG2Account]
ADD CONSTRAINT [FK_TOG2Account_BankAccount]
    FOREIGN KEY ([Account_Id])
    REFERENCES [dbo].[BankAccounts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Account_BankAccount'
CREATE INDEX [IX_FK_TOG2Account_BankAccount]
ON [dbo].[TOG2Account]
    ([Account_Id]);
GO

-- Creating foreign key on [BBPin_Id] in table 'Person2BBPin'
ALTER TABLE [dbo].[Person2BBPin]
ADD CONSTRAINT [FK_Person2BBPin_BBPin]
    FOREIGN KEY ([BBPin_Id])
    REFERENCES [dbo].[BBPins]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2BBPin_BBPin'
CREATE INDEX [IX_FK_Person2BBPin_BBPin]
ON [dbo].[Person2BBPin]
    ([BBPin_Id]);
GO

-- Creating foreign key on [ClaimStatus_Id] in table 'Incident2Organisation'
ALTER TABLE [dbo].[Incident2Organisation]
ADD CONSTRAINT [FK_Incident2Organisation_ClaimStatus]
    FOREIGN KEY ([ClaimStatus_Id])
    REFERENCES [dbo].[ClaimStatus]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Organisation_ClaimStatus'
CREATE INDEX [IX_FK_Incident2Organisation_ClaimStatus]
ON [dbo].[Incident2Organisation]
    ([ClaimStatus_Id]);
GO

-- Creating foreign key on [ClaimStatus_Id] in table 'Incident2Person'
ALTER TABLE [dbo].[Incident2Person]
ADD CONSTRAINT [FK_Incident2Person_ClaimStatus]
    FOREIGN KEY ([ClaimStatus_Id])
    REFERENCES [dbo].[ClaimStatus]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Person_ClaimStatus'
CREATE INDEX [IX_FK_Incident2Person_ClaimStatus]
ON [dbo].[Incident2Person]
    ([ClaimStatus_Id]);
GO

-- Creating foreign key on [ClaimType_Id] in table 'Incidents'
ALTER TABLE [dbo].[Incidents]
ADD CONSTRAINT [FK_Incident_ClaimType]
    FOREIGN KEY ([ClaimType_Id])
    REFERENCES [dbo].[ClaimTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident_ClaimType'
CREATE INDEX [IX_FK_Incident_ClaimType]
ON [dbo].[Incidents]
    ([ClaimType_Id]);
GO

-- Creating foreign key on [DrivingLicense_Id] in table 'Person2DrivingLicense'
ALTER TABLE [dbo].[Person2DrivingLicense]
ADD CONSTRAINT [FK_Person2DrivingLicense_DrivingLicense]
    FOREIGN KEY ([DrivingLicense_Id])
    REFERENCES [dbo].[DrivingLicenses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2DrivingLicense_DrivingLicense'
CREATE INDEX [IX_FK_Person2DrivingLicense_DrivingLicense]
ON [dbo].[Person2DrivingLicense]
    ([DrivingLicense_Id]);
GO

-- Creating foreign key on [DrivingLicense_Id] in table 'TOG2DrivingLicense'
ALTER TABLE [dbo].[TOG2DrivingLicense]
ADD CONSTRAINT [FK_TOG2DrivingLicense_DrivingLicense]
    FOREIGN KEY ([DrivingLicense_Id])
    REFERENCES [dbo].[DrivingLicenses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2DrivingLicense_DrivingLicense'
CREATE INDEX [IX_FK_TOG2DrivingLicense_DrivingLicense]
ON [dbo].[TOG2DrivingLicense]
    ([DrivingLicense_Id]);
GO

-- Creating foreign key on [Email_Id] in table 'Organisation2Email'
ALTER TABLE [dbo].[Organisation2Email]
ADD CONSTRAINT [FK_Organisation2Email_Email]
    FOREIGN KEY ([Email_Id])
    REFERENCES [dbo].[Emails]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Email_Email'
CREATE INDEX [IX_FK_Organisation2Email_Email]
ON [dbo].[Organisation2Email]
    ([Email_Id]);
GO

-- Creating foreign key on [Email_Id] in table 'Person2Email'
ALTER TABLE [dbo].[Person2Email]
ADD CONSTRAINT [FK_Person2Email_Email]
    FOREIGN KEY ([Email_Id])
    REFERENCES [dbo].[Emails]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Email_Email'
CREATE INDEX [IX_FK_Person2Email_Email]
ON [dbo].[Person2Email]
    ([Email_Id]);
GO

-- Creating foreign key on [Email_Id] in table 'TOG2Email'
ALTER TABLE [dbo].[TOG2Email]
ADD CONSTRAINT [FK_TOG2Email_Email]
    FOREIGN KEY ([Email_Id])
    REFERENCES [dbo].[Emails]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Email_Email'
CREATE INDEX [IX_FK_TOG2Email_Email]
ON [dbo].[TOG2Email]
    ([Email_Id]);
GO

-- Creating foreign key on [FraudRing_Id] in table 'Incident2FraudRing'
ALTER TABLE [dbo].[Incident2FraudRing]
ADD CONSTRAINT [FK_Incident2FraudRing_FraudRing]
    FOREIGN KEY ([FraudRing_Id])
    REFERENCES [dbo].[FraudRings]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2FraudRing_FraudRing'
CREATE INDEX [IX_FK_Incident2FraudRing_FraudRing]
ON [dbo].[Incident2FraudRing]
    ([FraudRing_Id]);
GO

-- Creating foreign key on [FraudRing_Id] in table 'TOG2FraudRing'
ALTER TABLE [dbo].[TOG2FraudRing]
ADD CONSTRAINT [FK_TOG2FraudRing_FraudRing]
    FOREIGN KEY ([FraudRing_Id])
    REFERENCES [dbo].[FraudRings]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2FraudRing_FraudRing'
CREATE INDEX [IX_FK_TOG2FraudRing_FraudRing]
ON [dbo].[TOG2FraudRing]
    ([FraudRing_Id]);
GO

-- Creating foreign key on [Gender_Id] in table 'People'
ALTER TABLE [dbo].[People]
ADD CONSTRAINT [FK_Person2Gender]
    FOREIGN KEY ([Gender_Id])
    REFERENCES [dbo].[Genders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Gender'
CREATE INDEX [IX_FK_Person2Gender]
ON [dbo].[People]
    ([Gender_Id]);
GO

-- Creating foreign key on [IncidentType_Id] in table 'Incidents'
ALTER TABLE [dbo].[Incidents]
ADD CONSTRAINT [FK_Incident_IncidentType]
    FOREIGN KEY ([IncidentType_Id])
    REFERENCES [dbo].[IncidentTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident_IncidentType'
CREATE INDEX [IX_FK_Incident_IncidentType]
ON [dbo].[Incidents]
    ([IncidentType_Id]);
GO

-- Creating foreign key on [Incident_Id] in table 'Incident2FraudRing'
ALTER TABLE [dbo].[Incident2FraudRing]
ADD CONSTRAINT [FK_Incident2FraudRing_Incident]
    FOREIGN KEY ([Incident_Id])
    REFERENCES [dbo].[Incidents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2FraudRing_Incident'
CREATE INDEX [IX_FK_Incident2FraudRing_Incident]
ON [dbo].[Incident2FraudRing]
    ([Incident_Id]);
GO

-- Creating foreign key on [Incident1_Id] in table 'Incident2Incident'
ALTER TABLE [dbo].[Incident2Incident]
ADD CONSTRAINT [FK_Incident2Incident_Incident1]
    FOREIGN KEY ([Incident1_Id])
    REFERENCES [dbo].[Incidents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Incident_Incident1'
CREATE INDEX [IX_FK_Incident2Incident_Incident1]
ON [dbo].[Incident2Incident]
    ([Incident1_Id]);
GO

-- Creating foreign key on [Incident2_Id] in table 'Incident2Incident'
ALTER TABLE [dbo].[Incident2Incident]
ADD CONSTRAINT [FK_Incident2Incident_Incident2]
    FOREIGN KEY ([Incident2_Id])
    REFERENCES [dbo].[Incidents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Incident_Incident2'
CREATE INDEX [IX_FK_Incident2Incident_Incident2]
ON [dbo].[Incident2Incident]
    ([Incident2_Id]);
GO

-- Creating foreign key on [Incident_Id] in table 'Incident2Organisation'
ALTER TABLE [dbo].[Incident2Organisation]
ADD CONSTRAINT [FK_Incident2Organisation_Incident]
    FOREIGN KEY ([Incident_Id])
    REFERENCES [dbo].[Incidents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Organisation_Incident'
CREATE INDEX [IX_FK_Incident2Organisation_Incident]
ON [dbo].[Incident2Organisation]
    ([Incident_Id]);
GO

-- Creating foreign key on [Incident_Id] in table 'Incident2OrganisationOutcome'
ALTER TABLE [dbo].[Incident2OrganisationOutcome]
ADD CONSTRAINT [FK_Incident2OrganisationOutcome_Incident]
    FOREIGN KEY ([Incident_Id])
    REFERENCES [dbo].[Incidents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2OrganisationOutcome_Incident'
CREATE INDEX [IX_FK_Incident2OrganisationOutcome_Incident]
ON [dbo].[Incident2OrganisationOutcome]
    ([Incident_Id]);
GO

-- Creating foreign key on [Incident_Id] in table 'Incident2Person'
ALTER TABLE [dbo].[Incident2Person]
ADD CONSTRAINT [FK_Incident2Person_Incident]
    FOREIGN KEY ([Incident_Id])
    REFERENCES [dbo].[Incidents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Person_Incident'
CREATE INDEX [IX_FK_Incident2Person_Incident]
ON [dbo].[Incident2Person]
    ([Incident_Id]);
GO

-- Creating foreign key on [Incident_Id] in table 'Incident2PersonOutcome'
ALTER TABLE [dbo].[Incident2PersonOutcome]
ADD CONSTRAINT [FK_Incident2PersonOutcome_Incident]
    FOREIGN KEY ([Incident_Id])
    REFERENCES [dbo].[Incidents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2PersonOutcome_Incident'
CREATE INDEX [IX_FK_Incident2PersonOutcome_Incident]
ON [dbo].[Incident2PersonOutcome]
    ([Incident_Id]);
GO

-- Creating foreign key on [Incident_Id] in table 'Incident2Vehicle'
ALTER TABLE [dbo].[Incident2Vehicle]
ADD CONSTRAINT [FK_Incident2Vehicle_Incident]
    FOREIGN KEY ([Incident_Id])
    REFERENCES [dbo].[Incidents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Vehicle_Incident'
CREATE INDEX [IX_FK_Incident2Vehicle_Incident]
ON [dbo].[Incident2Vehicle]
    ([Incident_Id]);
GO

-- Creating foreign key on [Incident_Id] in table 'KeoghsCase2Incident'
ALTER TABLE [dbo].[KeoghsCase2Incident]
ADD CONSTRAINT [FK_KeoghsCase2Incident_Incident]
    FOREIGN KEY ([Incident_Id])
    REFERENCES [dbo].[Incidents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_KeoghsCase2Incident_Incident'
CREATE INDEX [IX_FK_KeoghsCase2Incident_Incident]
ON [dbo].[KeoghsCase2Incident]
    ([Incident_Id]);
GO

-- Creating foreign key on [Incident_Id] in table 'RiskClaims'
ALTER TABLE [dbo].[RiskClaims]
ADD CONSTRAINT [FK_RiskClaim_Incident]
    FOREIGN KEY ([Incident_Id])
    REFERENCES [dbo].[Incidents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RiskClaim_Incident'
CREATE INDEX [IX_FK_RiskClaim_Incident]
ON [dbo].[RiskClaims]
    ([Incident_Id]);
GO

-- Creating foreign key on [Incident_Id] in table 'TOG2Incident'
ALTER TABLE [dbo].[TOG2Incident]
ADD CONSTRAINT [FK_TOG2Incident_Incident]
    FOREIGN KEY ([Incident_Id])
    REFERENCES [dbo].[Incidents]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Incident_Incident'
CREATE INDEX [IX_FK_TOG2Incident_Incident]
ON [dbo].[TOG2Incident]
    ([Incident_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Incident2FraudRing'
ALTER TABLE [dbo].[Incident2FraudRing]
ADD CONSTRAINT [FK_Incident2FraudRing_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2FraudRing_RiskClaim'
CREATE INDEX [IX_FK_Incident2FraudRing_RiskClaim]
ON [dbo].[Incident2FraudRing]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Incident2Incident'
ALTER TABLE [dbo].[Incident2Incident]
ADD CONSTRAINT [FK_Incident2Incident_RiskClaim1]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Incident_RiskClaim1'
CREATE INDEX [IX_FK_Incident2Incident_RiskClaim1]
ON [dbo].[Incident2Incident]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [MojStatus_Id] in table 'Incident2Organisation'
ALTER TABLE [dbo].[Incident2Organisation]
ADD CONSTRAINT [FK_Incident2Organisation_MojStatus]
    FOREIGN KEY ([MojStatus_Id])
    REFERENCES [dbo].[MojStatus]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Organisation_MojStatus'
CREATE INDEX [IX_FK_Incident2Organisation_MojStatus]
ON [dbo].[Incident2Organisation]
    ([MojStatus_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Incident2Organisation'
ALTER TABLE [dbo].[Incident2Organisation]
ADD CONSTRAINT [FK_Incident2Organisation_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Organisation_Organisation'
CREATE INDEX [IX_FK_Incident2Organisation_Organisation]
ON [dbo].[Incident2Organisation]
    ([Organisation_Id]);
GO

-- Creating foreign key on [PartyType_Id] in table 'Incident2Organisation'
ALTER TABLE [dbo].[Incident2Organisation]
ADD CONSTRAINT [FK_Incident2Organisation_PartyType]
    FOREIGN KEY ([PartyType_Id])
    REFERENCES [dbo].[PartyTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Organisation_PartyType'
CREATE INDEX [IX_FK_Incident2Organisation_PartyType]
ON [dbo].[Incident2Organisation]
    ([PartyType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Incident2Organisation'
ALTER TABLE [dbo].[Incident2Organisation]
ADD CONSTRAINT [FK_Incident2Organisation_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Organisation_RiskClaim'
CREATE INDEX [IX_FK_Incident2Organisation_RiskClaim]
ON [dbo].[Incident2Organisation]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [SubPartyType_Id] in table 'Incident2Organisation'
ALTER TABLE [dbo].[Incident2Organisation]
ADD CONSTRAINT [FK_Incident2Organisation_SubPartyType]
    FOREIGN KEY ([SubPartyType_Id])
    REFERENCES [dbo].[SubPartyTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Organisation_SubPartyType'
CREATE INDEX [IX_FK_Incident2Organisation_SubPartyType]
ON [dbo].[Incident2Organisation]
    ([SubPartyType_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Incident2OrganisationOutcome'
ALTER TABLE [dbo].[Incident2OrganisationOutcome]
ADD CONSTRAINT [FK_Incident2OrganisationOutcome_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2OrganisationOutcome_Organisation'
CREATE INDEX [IX_FK_Incident2OrganisationOutcome_Organisation]
ON [dbo].[Incident2OrganisationOutcome]
    ([Organisation_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Incident2OrganisationOutcome'
ALTER TABLE [dbo].[Incident2OrganisationOutcome]
ADD CONSTRAINT [FK_Incident2OrganisationOutcome_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2OrganisationOutcome_RiskClaim'
CREATE INDEX [IX_FK_Incident2OrganisationOutcome_RiskClaim]
ON [dbo].[Incident2OrganisationOutcome]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [MojStatus_Id] in table 'Incident2Person'
ALTER TABLE [dbo].[Incident2Person]
ADD CONSTRAINT [FK_Incident2Person_MojStatus]
    FOREIGN KEY ([MojStatus_Id])
    REFERENCES [dbo].[MojStatus]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Person_MojStatus'
CREATE INDEX [IX_FK_Incident2Person_MojStatus]
ON [dbo].[Incident2Person]
    ([MojStatus_Id]);
GO

-- Creating foreign key on [PartyType_Id] in table 'Incident2Person'
ALTER TABLE [dbo].[Incident2Person]
ADD CONSTRAINT [FK_Incident2Person_PartyType]
    FOREIGN KEY ([PartyType_Id])
    REFERENCES [dbo].[PartyTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Person_PartyType'
CREATE INDEX [IX_FK_Incident2Person_PartyType]
ON [dbo].[Incident2Person]
    ([PartyType_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Incident2Person'
ALTER TABLE [dbo].[Incident2Person]
ADD CONSTRAINT [FK_Incident2Person_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Person_Person'
CREATE INDEX [IX_FK_Incident2Person_Person]
ON [dbo].[Incident2Person]
    ([Person_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Incident2Person'
ALTER TABLE [dbo].[Incident2Person]
ADD CONSTRAINT [FK_Incident2Person_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Person_RiskClaim'
CREATE INDEX [IX_FK_Incident2Person_RiskClaim]
ON [dbo].[Incident2Person]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [SubPartyType_Id] in table 'Incident2Person'
ALTER TABLE [dbo].[Incident2Person]
ADD CONSTRAINT [FK_Incident2Person_SubPartyType]
    FOREIGN KEY ([SubPartyType_Id])
    REFERENCES [dbo].[SubPartyTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Person_SubPartyType'
CREATE INDEX [IX_FK_Incident2Person_SubPartyType]
ON [dbo].[Incident2Person]
    ([SubPartyType_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Incident2PersonOutcome'
ALTER TABLE [dbo].[Incident2PersonOutcome]
ADD CONSTRAINT [FK_Incident2PersonOutcome_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2PersonOutcome_Person'
CREATE INDEX [IX_FK_Incident2PersonOutcome_Person]
ON [dbo].[Incident2PersonOutcome]
    ([Person_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Incident2PersonOutcome'
ALTER TABLE [dbo].[Incident2PersonOutcome]
ADD CONSTRAINT [FK_Incident2PersonOutcome_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2PersonOutcome_RiskClaim'
CREATE INDEX [IX_FK_Incident2PersonOutcome_RiskClaim]
ON [dbo].[Incident2PersonOutcome]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [Incident2VehicleLinkType_Id] in table 'Incident2Vehicle'
ALTER TABLE [dbo].[Incident2Vehicle]
ADD CONSTRAINT [FK_Incident2Vehicle_Incident2VehicleLinkType]
    FOREIGN KEY ([Incident2VehicleLinkType_Id])
    REFERENCES [dbo].[Incident2VehicleLinkType]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Vehicle_Incident2VehicleLinkType'
CREATE INDEX [IX_FK_Incident2Vehicle_Incident2VehicleLinkType]
ON [dbo].[Incident2Vehicle]
    ([Incident2VehicleLinkType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Incident2Vehicle'
ALTER TABLE [dbo].[Incident2Vehicle]
ADD CONSTRAINT [FK_Incident2Vehicle_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Vehicle_RiskClaim'
CREATE INDEX [IX_FK_Incident2Vehicle_RiskClaim]
ON [dbo].[Incident2Vehicle]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [Vehicle_Id] in table 'Incident2Vehicle'
ALTER TABLE [dbo].[Incident2Vehicle]
ADD CONSTRAINT [FK_Incident2Vehicle_Vehicle]
    FOREIGN KEY ([Vehicle_Id])
    REFERENCES [dbo].[Vehicles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Vehicle_Vehicle'
CREATE INDEX [IX_FK_Incident2Vehicle_Vehicle]
ON [dbo].[Incident2Vehicle]
    ([Vehicle_Id]);
GO

-- Creating foreign key on [VehicleCategoryOfLoss_Id] in table 'Incident2Vehicle'
ALTER TABLE [dbo].[Incident2Vehicle]
ADD CONSTRAINT [FK_Incident2Vehicle_VehicleCategoryOfLoss]
    FOREIGN KEY ([VehicleCategoryOfLoss_Id])
    REFERENCES [dbo].[VehicleCategoryOfLosses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Incident2Vehicle_VehicleCategoryOfLoss'
CREATE INDEX [IX_FK_Incident2Vehicle_VehicleCategoryOfLoss]
ON [dbo].[Incident2Vehicle]
    ([VehicleCategoryOfLoss_Id]);
GO

-- Creating foreign key on [Client_Id] in table 'KeoghsCases'
ALTER TABLE [dbo].[KeoghsCases]
ADD CONSTRAINT [FK_KeoghsCase_InsurersClients]
    FOREIGN KEY ([Client_Id])
    REFERENCES [dbo].[InsurersClients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_KeoghsCase_InsurersClients'
CREATE INDEX [IX_FK_KeoghsCase_InsurersClients]
ON [dbo].[KeoghsCases]
    ([Client_Id]);
GO

-- Creating foreign key on [IPAddress_Id] in table 'Organisation2IPAddress'
ALTER TABLE [dbo].[Organisation2IPAddress]
ADD CONSTRAINT [FK_Organisation2IPAddress_IPAddress]
    FOREIGN KEY ([IPAddress_Id])
    REFERENCES [dbo].[IPAddresses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2IPAddress_IPAddress'
CREATE INDEX [IX_FK_Organisation2IPAddress_IPAddress]
ON [dbo].[Organisation2IPAddress]
    ([IPAddress_Id]);
GO

-- Creating foreign key on [IPAddress_Id] in table 'Person2IPAddress'
ALTER TABLE [dbo].[Person2IPAddress]
ADD CONSTRAINT [FK_Person2IPAddress_IPAddress]
    FOREIGN KEY ([IPAddress_Id])
    REFERENCES [dbo].[IPAddresses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2IPAddress_IPAddress'
CREATE INDEX [IX_FK_Person2IPAddress_IPAddress]
ON [dbo].[Person2IPAddress]
    ([IPAddress_Id]);
GO

-- Creating foreign key on [IPAddress_Id] in table 'TOG2IPAddress'
ALTER TABLE [dbo].[TOG2IPAddress]
ADD CONSTRAINT [FK_TOG2IPAddress_IPAddress]
    FOREIGN KEY ([IPAddress_Id])
    REFERENCES [dbo].[IPAddresses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2IPAddress_IPAddress'
CREATE INDEX [IX_FK_TOG2IPAddress_IPAddress]
ON [dbo].[TOG2IPAddress]
    ([IPAddress_Id]);
GO

-- Creating foreign key on [CaseStatus_Id] in table 'KeoghsCases'
ALTER TABLE [dbo].[KeoghsCases]
ADD CONSTRAINT [FK_KeoghsCase_KeoghsCaseStatus]
    FOREIGN KEY ([CaseStatus_Id])
    REFERENCES [dbo].[KeoghsCaseStatus]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_KeoghsCase_KeoghsCaseStatus'
CREATE INDEX [IX_FK_KeoghsCase_KeoghsCaseStatus]
ON [dbo].[KeoghsCases]
    ([CaseStatus_Id]);
GO

-- Creating foreign key on [KeoghsOffice_Id] in table 'KeoghsCases'
ALTER TABLE [dbo].[KeoghsCases]
ADD CONSTRAINT [FK_KeoghsCase_KeoghsOffice]
    FOREIGN KEY ([KeoghsOffice_Id])
    REFERENCES [dbo].[KeoghsOffices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_KeoghsCase_KeoghsOffice'
CREATE INDEX [IX_FK_KeoghsCase_KeoghsOffice]
ON [dbo].[KeoghsCases]
    ([KeoghsOffice_Id]);
GO

-- Creating foreign key on [KeoghsCase_Id] in table 'KeoghsCase2Incident'
ALTER TABLE [dbo].[KeoghsCase2Incident]
ADD CONSTRAINT [FK_KeoghsCase2Incident_KeoghsCase]
    FOREIGN KEY ([KeoghsCase_Id])
    REFERENCES [dbo].[KeoghsCases]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_KeoghsCase2Incident_KeoghsCase'
CREATE INDEX [IX_FK_KeoghsCase2Incident_KeoghsCase]
ON [dbo].[KeoghsCase2Incident]
    ([KeoghsCase_Id]);
GO

-- Creating foreign key on [KeoghsCase_Id] in table 'TOG2KeoghsCase'
ALTER TABLE [dbo].[TOG2KeoghsCase]
ADD CONSTRAINT [FK_TOG2KeoghsCase_KeoghsCase]
    FOREIGN KEY ([KeoghsCase_Id])
    REFERENCES [dbo].[KeoghsCases]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2KeoghsCase_KeoghsCase'
CREATE INDEX [IX_FK_TOG2KeoghsCase_KeoghsCase]
ON [dbo].[TOG2KeoghsCase]
    ([KeoghsCase_Id]);
GO

-- Creating foreign key on [CaseIncidentLinkType_Id] in table 'KeoghsCase2Incident'
ALTER TABLE [dbo].[KeoghsCase2Incident]
ADD CONSTRAINT [FK_KeoghsCase2Incident_CaseIncidentLinkType]
    FOREIGN KEY ([CaseIncidentLinkType_Id])
    REFERENCES [dbo].[KeoghsCase2IncidentLinkType]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_KeoghsCase2Incident_CaseIncidentLinkType'
CREATE INDEX [IX_FK_KeoghsCase2Incident_CaseIncidentLinkType]
ON [dbo].[KeoghsCase2Incident]
    ([CaseIncidentLinkType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'KeoghsCase2Incident'
ALTER TABLE [dbo].[KeoghsCase2Incident]
ADD CONSTRAINT [FK_KeoghsCase2Incident_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_KeoghsCase2Incident_RiskClaim'
CREATE INDEX [IX_FK_KeoghsCase2Incident_RiskClaim]
ON [dbo].[KeoghsCase2Incident]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [MojCRMStatus_Id] in table 'Organisations'
ALTER TABLE [dbo].[Organisations]
ADD CONSTRAINT [FK_Organisation_MojCRMStatus]
    FOREIGN KEY ([MojCRMStatus_Id])
    REFERENCES [dbo].[MojCRMStatus]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation_MojCRMStatus'
CREATE INDEX [IX_FK_Organisation_MojCRMStatus]
ON [dbo].[Organisations]
    ([MojCRMStatus_Id]);
GO

-- Creating foreign key on [NINumber_Id] in table 'Person2NINumber'
ALTER TABLE [dbo].[Person2NINumber]
ADD CONSTRAINT [FK_Person2NINumber_NINumber]
    FOREIGN KEY ([NINumber_Id])
    REFERENCES [dbo].[NINumbers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2NINumber_NINumber'
CREATE INDEX [IX_FK_Person2NINumber_NINumber]
ON [dbo].[Person2NINumber]
    ([NINumber_Id]);
GO

-- Creating foreign key on [NINumber_Id] in table 'TOG2NINumber'
ALTER TABLE [dbo].[TOG2NINumber]
ADD CONSTRAINT [FK_TOG2NINumber_NINumber]
    FOREIGN KEY ([NINumber_Id])
    REFERENCES [dbo].[NINumbers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2NINumber_NINumber'
CREATE INDEX [IX_FK_TOG2NINumber_NINumber]
ON [dbo].[TOG2NINumber]
    ([NINumber_Id]);
GO

-- Creating foreign key on [OrganisationStatus_Id] in table 'Organisations'
ALTER TABLE [dbo].[Organisations]
ADD CONSTRAINT [FK_Organisation_OrganisationStatus]
    FOREIGN KEY ([OrganisationStatus_Id])
    REFERENCES [dbo].[OrganisationStatus]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation_OrganisationStatus'
CREATE INDEX [IX_FK_Organisation_OrganisationStatus]
ON [dbo].[Organisations]
    ([OrganisationStatus_Id]);
GO

-- Creating foreign key on [OrganisationType_Id] in table 'Organisations'
ALTER TABLE [dbo].[Organisations]
ADD CONSTRAINT [FK_Organisation_OrganisationType]
    FOREIGN KEY ([OrganisationType_Id])
    REFERENCES [dbo].[OrganisationTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation_OrganisationType'
CREATE INDEX [IX_FK_Organisation_OrganisationType]
ON [dbo].[Organisations]
    ([OrganisationType_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Organisation2Address'
ALTER TABLE [dbo].[Organisation2Address]
ADD CONSTRAINT [FK_Organisation2Address_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Address_Organisation'
CREATE INDEX [IX_FK_Organisation2Address_Organisation]
ON [dbo].[Organisation2Address]
    ([Organisation_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Organisation2BankAccount'
ALTER TABLE [dbo].[Organisation2BankAccount]
ADD CONSTRAINT [FK_Organisation2BankAccount_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2BankAccount_Organisation'
CREATE INDEX [IX_FK_Organisation2BankAccount_Organisation]
ON [dbo].[Organisation2BankAccount]
    ([Organisation_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Organisation2Email'
ALTER TABLE [dbo].[Organisation2Email]
ADD CONSTRAINT [FK_Organisation2Email_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Email_Organisation'
CREATE INDEX [IX_FK_Organisation2Email_Organisation]
ON [dbo].[Organisation2Email]
    ([Organisation_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Organisation2IPAddress'
ALTER TABLE [dbo].[Organisation2IPAddress]
ADD CONSTRAINT [FK_Organisation2IPAddress_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2IPAddress_Organisation'
CREATE INDEX [IX_FK_Organisation2IPAddress_Organisation]
ON [dbo].[Organisation2IPAddress]
    ([Organisation_Id]);
GO

-- Creating foreign key on [Organisation1_Id] in table 'Organisation2Organisation'
ALTER TABLE [dbo].[Organisation2Organisation]
ADD CONSTRAINT [FK_Organisation2Organisation_Organisation1]
    FOREIGN KEY ([Organisation1_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Organisation_Organisation1'
CREATE INDEX [IX_FK_Organisation2Organisation_Organisation1]
ON [dbo].[Organisation2Organisation]
    ([Organisation1_Id]);
GO

-- Creating foreign key on [Organisation2_Id] in table 'Organisation2Organisation'
ALTER TABLE [dbo].[Organisation2Organisation]
ADD CONSTRAINT [FK_Organisation2Organisation_Organisation2]
    FOREIGN KEY ([Organisation2_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Organisation_Organisation2'
CREATE INDEX [IX_FK_Organisation2Organisation_Organisation2]
ON [dbo].[Organisation2Organisation]
    ([Organisation2_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Organisation2PaymentCard'
ALTER TABLE [dbo].[Organisation2PaymentCard]
ADD CONSTRAINT [FK_Organisation2PaymentCard_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2PaymentCard_Organisation'
CREATE INDEX [IX_FK_Organisation2PaymentCard_Organisation]
ON [dbo].[Organisation2PaymentCard]
    ([Organisation_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Organisation2Policy'
ALTER TABLE [dbo].[Organisation2Policy]
ADD CONSTRAINT [FK_Organisation2Policy_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Policy_Organisation'
CREATE INDEX [IX_FK_Organisation2Policy_Organisation]
ON [dbo].[Organisation2Policy]
    ([Organisation_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Organisation2Telephone'
ALTER TABLE [dbo].[Organisation2Telephone]
ADD CONSTRAINT [FK_Organisation2Telephone_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Telephone_Organisation'
CREATE INDEX [IX_FK_Organisation2Telephone_Organisation]
ON [dbo].[Organisation2Telephone]
    ([Organisation_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Organisation2WebSite'
ALTER TABLE [dbo].[Organisation2WebSite]
ADD CONSTRAINT [FK_Organisation2WebSite_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2WebSite_Organisation'
CREATE INDEX [IX_FK_Organisation2WebSite_Organisation]
ON [dbo].[Organisation2WebSite]
    ([Organisation_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Person2Organisation'
ALTER TABLE [dbo].[Person2Organisation]
ADD CONSTRAINT [FK_Person2Organisation_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Organisation_Organisation'
CREATE INDEX [IX_FK_Person2Organisation_Organisation]
ON [dbo].[Person2Organisation]
    ([Organisation_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'TOG2Organisation'
ALTER TABLE [dbo].[TOG2Organisation]
ADD CONSTRAINT [FK_TOG2Organisation_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Organisation_Organisation'
CREATE INDEX [IX_FK_TOG2Organisation_Organisation]
ON [dbo].[TOG2Organisation]
    ([Organisation_Id]);
GO

-- Creating foreign key on [Organisation_Id] in table 'Vehicle2Organisation'
ALTER TABLE [dbo].[Vehicle2Organisation]
ADD CONSTRAINT [FK_Vehicle2Organisation_Organisation]
    FOREIGN KEY ([Organisation_Id])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Organisation_Organisation'
CREATE INDEX [IX_FK_Vehicle2Organisation_Organisation]
ON [dbo].[Vehicle2Organisation]
    ([Organisation_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Organisation2Address'
ALTER TABLE [dbo].[Organisation2Address]
ADD CONSTRAINT [FK_Organisation2Address_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Address_RiskClaim'
CREATE INDEX [IX_FK_Organisation2Address_RiskClaim]
ON [dbo].[Organisation2Address]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Organisation2BankAccount'
ALTER TABLE [dbo].[Organisation2BankAccount]
ADD CONSTRAINT [FK_Organisation2BankAccount_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2BankAccount_RiskClaim'
CREATE INDEX [IX_FK_Organisation2BankAccount_RiskClaim]
ON [dbo].[Organisation2BankAccount]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Organisation2Email'
ALTER TABLE [dbo].[Organisation2Email]
ADD CONSTRAINT [FK_Organisation2Email_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Email_RiskClaim'
CREATE INDEX [IX_FK_Organisation2Email_RiskClaim]
ON [dbo].[Organisation2Email]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Organisation2IPAddress'
ALTER TABLE [dbo].[Organisation2IPAddress]
ADD CONSTRAINT [FK_Organisation2IPAddress_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2IPAddress_RiskClaim'
CREATE INDEX [IX_FK_Organisation2IPAddress_RiskClaim]
ON [dbo].[Organisation2IPAddress]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [OrganisationLinkType_Id] in table 'Organisation2Organisation'
ALTER TABLE [dbo].[Organisation2Organisation]
ADD CONSTRAINT [FK_Organisation2Organisation_OrganisationLinkType]
    FOREIGN KEY ([OrganisationLinkType_Id])
    REFERENCES [dbo].[OrganisationLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Organisation_OrganisationLinkType'
CREATE INDEX [IX_FK_Organisation2Organisation_OrganisationLinkType]
ON [dbo].[Organisation2Organisation]
    ([OrganisationLinkType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Organisation2Organisation'
ALTER TABLE [dbo].[Organisation2Organisation]
ADD CONSTRAINT [FK_Organisation2Organisation_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Organisation_RiskClaim'
CREATE INDEX [IX_FK_Organisation2Organisation_RiskClaim]
ON [dbo].[Organisation2Organisation]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [PaymentCard_Id] in table 'Organisation2PaymentCard'
ALTER TABLE [dbo].[Organisation2PaymentCard]
ADD CONSTRAINT [FK_Organisation2PaymentCard_PaymentCard]
    FOREIGN KEY ([PaymentCard_Id])
    REFERENCES [dbo].[PaymentCards]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2PaymentCard_PaymentCard'
CREATE INDEX [IX_FK_Organisation2PaymentCard_PaymentCard]
ON [dbo].[Organisation2PaymentCard]
    ([PaymentCard_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Organisation2PaymentCard'
ALTER TABLE [dbo].[Organisation2PaymentCard]
ADD CONSTRAINT [FK_Organisation2PaymentCard_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2PaymentCard_RiskClaim'
CREATE INDEX [IX_FK_Organisation2PaymentCard_RiskClaim]
ON [dbo].[Organisation2PaymentCard]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [Policy_Id] in table 'Organisation2Policy'
ALTER TABLE [dbo].[Organisation2Policy]
ADD CONSTRAINT [FK_Organisation2Policy_Policy]
    FOREIGN KEY ([Policy_Id])
    REFERENCES [dbo].[Policies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Policy_Policy'
CREATE INDEX [IX_FK_Organisation2Policy_Policy]
ON [dbo].[Organisation2Policy]
    ([Policy_Id]);
GO

-- Creating foreign key on [PolicyLinkType_Id] in table 'Organisation2Policy'
ALTER TABLE [dbo].[Organisation2Policy]
ADD CONSTRAINT [FK_Organisation2Policy_PolicyLinkType]
    FOREIGN KEY ([PolicyLinkType_Id])
    REFERENCES [dbo].[PolicyLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Policy_PolicyLinkType'
CREATE INDEX [IX_FK_Organisation2Policy_PolicyLinkType]
ON [dbo].[Organisation2Policy]
    ([PolicyLinkType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Organisation2Policy'
ALTER TABLE [dbo].[Organisation2Policy]
ADD CONSTRAINT [FK_Organisation2Policy_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Policy_RiskClaim'
CREATE INDEX [IX_FK_Organisation2Policy_RiskClaim]
ON [dbo].[Organisation2Policy]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Organisation2Telephone'
ALTER TABLE [dbo].[Organisation2Telephone]
ADD CONSTRAINT [FK_Organisation2Telephone_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Telephone_RiskClaim'
CREATE INDEX [IX_FK_Organisation2Telephone_RiskClaim]
ON [dbo].[Organisation2Telephone]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [Telephone_Id] in table 'Organisation2Telephone'
ALTER TABLE [dbo].[Organisation2Telephone]
ADD CONSTRAINT [FK_Organisation2Telephone_Telephone]
    FOREIGN KEY ([Telephone_Id])
    REFERENCES [dbo].[Telephones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Telephone_Telephone'
CREATE INDEX [IX_FK_Organisation2Telephone_Telephone]
ON [dbo].[Organisation2Telephone]
    ([Telephone_Id]);
GO

-- Creating foreign key on [TelephoneLinkType_Id] in table 'Organisation2Telephone'
ALTER TABLE [dbo].[Organisation2Telephone]
ADD CONSTRAINT [FK_Organisation2Telephone_TelephoneLinkType]
    FOREIGN KEY ([TelephoneLinkType_Id])
    REFERENCES [dbo].[TelephoneLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2Telephone_TelephoneLinkType'
CREATE INDEX [IX_FK_Organisation2Telephone_TelephoneLinkType]
ON [dbo].[Organisation2Telephone]
    ([TelephoneLinkType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Organisation2WebSite'
ALTER TABLE [dbo].[Organisation2WebSite]
ADD CONSTRAINT [FK_Organisation2WebSite_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2WebSite_RiskClaim'
CREATE INDEX [IX_FK_Organisation2WebSite_RiskClaim]
ON [dbo].[Organisation2WebSite]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [WebSite_Id] in table 'Organisation2WebSite'
ALTER TABLE [dbo].[Organisation2WebSite]
ADD CONSTRAINT [FK_Organisation2WebSite_WebSite]
    FOREIGN KEY ([WebSite_Id])
    REFERENCES [dbo].[WebSites]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Organisation2WebSite_WebSite'
CREATE INDEX [IX_FK_Organisation2WebSite_WebSite]
ON [dbo].[Organisation2WebSite]
    ([WebSite_Id]);
GO

-- Creating foreign key on [Passport_Id] in table 'Person2Passport'
ALTER TABLE [dbo].[Person2Passport]
ADD CONSTRAINT [FK_Person2Passport_Passport]
    FOREIGN KEY ([Passport_Id])
    REFERENCES [dbo].[Passports]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Passport_Passport'
CREATE INDEX [IX_FK_Person2Passport_Passport]
ON [dbo].[Person2Passport]
    ([Passport_Id]);
GO

-- Creating foreign key on [Passport_Id] in table 'TOG2Passport'
ALTER TABLE [dbo].[TOG2Passport]
ADD CONSTRAINT [FK_TOG2Passport_Passport]
    FOREIGN KEY ([Passport_Id])
    REFERENCES [dbo].[Passports]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Passport_Passport'
CREATE INDEX [IX_FK_TOG2Passport_Passport]
ON [dbo].[TOG2Passport]
    ([Passport_Id]);
GO

-- Creating foreign key on [PaymentCardType_Id] in table 'PaymentCards'
ALTER TABLE [dbo].[PaymentCards]
ADD CONSTRAINT [FK_PaymentCard_PaymentCardType]
    FOREIGN KEY ([PaymentCardType_Id])
    REFERENCES [dbo].[PaymentCardTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PaymentCard_PaymentCardType'
CREATE INDEX [IX_FK_PaymentCard_PaymentCardType]
ON [dbo].[PaymentCards]
    ([PaymentCardType_Id]);
GO

-- Creating foreign key on [PaymentCard_Id] in table 'Person2PaymentCard'
ALTER TABLE [dbo].[Person2PaymentCard]
ADD CONSTRAINT [FK_Person2PaymentCard_PaymentCard]
    FOREIGN KEY ([PaymentCard_Id])
    REFERENCES [dbo].[PaymentCards]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2PaymentCard_PaymentCard'
CREATE INDEX [IX_FK_Person2PaymentCard_PaymentCard]
ON [dbo].[Person2PaymentCard]
    ([PaymentCard_Id]);
GO

-- Creating foreign key on [PaymentCard_Id] in table 'Policy2PaymentCard'
ALTER TABLE [dbo].[Policy2PaymentCard]
ADD CONSTRAINT [FK_Policy2PaymentCard_PaymentCard]
    FOREIGN KEY ([PaymentCard_Id])
    REFERENCES [dbo].[PaymentCards]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Policy2PaymentCard_PaymentCard'
CREATE INDEX [IX_FK_Policy2PaymentCard_PaymentCard]
ON [dbo].[Policy2PaymentCard]
    ([PaymentCard_Id]);
GO

-- Creating foreign key on [PaymentCard_Id] in table 'TOG2PaymentCard'
ALTER TABLE [dbo].[TOG2PaymentCard]
ADD CONSTRAINT [FK_TOG2PaymentCard_PaymentCard]
    FOREIGN KEY ([PaymentCard_Id])
    REFERENCES [dbo].[PaymentCards]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2PaymentCard_PaymentCard'
CREATE INDEX [IX_FK_TOG2PaymentCard_PaymentCard]
ON [dbo].[TOG2PaymentCard]
    ([PaymentCard_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2Address'
ALTER TABLE [dbo].[Person2Address]
ADD CONSTRAINT [FK_Person2Address_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Address_Person'
CREATE INDEX [IX_FK_Person2Address_Person]
ON [dbo].[Person2Address]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2BankAccount'
ALTER TABLE [dbo].[Person2BankAccount]
ADD CONSTRAINT [FK_Person2BankAccount_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2BankAccount_Person'
CREATE INDEX [IX_FK_Person2BankAccount_Person]
ON [dbo].[Person2BankAccount]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2BBPin'
ALTER TABLE [dbo].[Person2BBPin]
ADD CONSTRAINT [FK_Person2BBPin_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2BBPin_Person'
CREATE INDEX [IX_FK_Person2BBPin_Person]
ON [dbo].[Person2BBPin]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2DrivingLicense'
ALTER TABLE [dbo].[Person2DrivingLicense]
ADD CONSTRAINT [FK_Person2DrivingLicense_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2DrivingLicense_Person'
CREATE INDEX [IX_FK_Person2DrivingLicense_Person]
ON [dbo].[Person2DrivingLicense]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2Email'
ALTER TABLE [dbo].[Person2Email]
ADD CONSTRAINT [FK_Person2Email_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Email_Person'
CREATE INDEX [IX_FK_Person2Email_Person]
ON [dbo].[Person2Email]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2IPAddress'
ALTER TABLE [dbo].[Person2IPAddress]
ADD CONSTRAINT [FK_Person2IPAddress_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2IPAddress_Person'
CREATE INDEX [IX_FK_Person2IPAddress_Person]
ON [dbo].[Person2IPAddress]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2NINumber'
ALTER TABLE [dbo].[Person2NINumber]
ADD CONSTRAINT [FK_Person2NINumber_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2NINumber_Person'
CREATE INDEX [IX_FK_Person2NINumber_Person]
ON [dbo].[Person2NINumber]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2Organisation'
ALTER TABLE [dbo].[Person2Organisation]
ADD CONSTRAINT [FK_Person2Organisation_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Organisation_Person'
CREATE INDEX [IX_FK_Person2Organisation_Person]
ON [dbo].[Person2Organisation]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2Passport'
ALTER TABLE [dbo].[Person2Passport]
ADD CONSTRAINT [FK_Person2Passport_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Passport_Person'
CREATE INDEX [IX_FK_Person2Passport_Person]
ON [dbo].[Person2Passport]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2PaymentCard'
ALTER TABLE [dbo].[Person2PaymentCard]
ADD CONSTRAINT [FK_Person2PaymentCard_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2PaymentCard_Person'
CREATE INDEX [IX_FK_Person2PaymentCard_Person]
ON [dbo].[Person2PaymentCard]
    ([Person_Id]);
GO

-- Creating foreign key on [Person1_Id] in table 'Person2Person'
ALTER TABLE [dbo].[Person2Person]
ADD CONSTRAINT [FK_Person2Person_Person1]
    FOREIGN KEY ([Person1_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Person_Person1'
CREATE INDEX [IX_FK_Person2Person_Person1]
ON [dbo].[Person2Person]
    ([Person1_Id]);
GO

-- Creating foreign key on [Person2_Id] in table 'Person2Person'
ALTER TABLE [dbo].[Person2Person]
ADD CONSTRAINT [FK_Person2Person_Person2]
    FOREIGN KEY ([Person2_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Person_Person2'
CREATE INDEX [IX_FK_Person2Person_Person2]
ON [dbo].[Person2Person]
    ([Person2_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2Policy'
ALTER TABLE [dbo].[Person2Policy]
ADD CONSTRAINT [FK_Person2Policy_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Policy_Person'
CREATE INDEX [IX_FK_Person2Policy_Person]
ON [dbo].[Person2Policy]
    ([Person_Id]);
GO

-- Creating foreign key on [Salutation_Id] in table 'People'
ALTER TABLE [dbo].[People]
ADD CONSTRAINT [FK_Person2Salutation]
    FOREIGN KEY ([Salutation_Id])
    REFERENCES [dbo].[Salutations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Salutation'
CREATE INDEX [IX_FK_Person2Salutation]
ON [dbo].[People]
    ([Salutation_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2Telephone'
ALTER TABLE [dbo].[Person2Telephone]
ADD CONSTRAINT [FK_Person2Telephone_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Telephone_Person'
CREATE INDEX [IX_FK_Person2Telephone_Person]
ON [dbo].[Person2Telephone]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Person2WebSite'
ALTER TABLE [dbo].[Person2WebSite]
ADD CONSTRAINT [FK_Person2WebSite_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2WebSite_Person'
CREATE INDEX [IX_FK_Person2WebSite_Person]
ON [dbo].[Person2WebSite]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'TOG2Person'
ALTER TABLE [dbo].[TOG2Person]
ADD CONSTRAINT [FK_TOG2Person_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Person_Person'
CREATE INDEX [IX_FK_TOG2Person_Person]
ON [dbo].[TOG2Person]
    ([Person_Id]);
GO

-- Creating foreign key on [Person_Id] in table 'Vehicle2Person'
ALTER TABLE [dbo].[Vehicle2Person]
ADD CONSTRAINT [FK_Vehicle2Person_Person]
    FOREIGN KEY ([Person_Id])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Person_Person'
CREATE INDEX [IX_FK_Vehicle2Person_Person]
ON [dbo].[Vehicle2Person]
    ([Person_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2Address'
ALTER TABLE [dbo].[Person2Address]
ADD CONSTRAINT [FK_Person2Address_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Address_RiskClaim'
CREATE INDEX [IX_FK_Person2Address_RiskClaim]
ON [dbo].[Person2Address]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2BankAccount'
ALTER TABLE [dbo].[Person2BankAccount]
ADD CONSTRAINT [FK_Person2BankAccount_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2BankAccount_RiskClaim'
CREATE INDEX [IX_FK_Person2BankAccount_RiskClaim]
ON [dbo].[Person2BankAccount]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2BBPin'
ALTER TABLE [dbo].[Person2BBPin]
ADD CONSTRAINT [FK_Person2BBPin_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2BBPin_RiskClaim'
CREATE INDEX [IX_FK_Person2BBPin_RiskClaim]
ON [dbo].[Person2BBPin]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2DrivingLicense'
ALTER TABLE [dbo].[Person2DrivingLicense]
ADD CONSTRAINT [FK_Person2DrivingLicense_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2DrivingLicense_RiskClaim'
CREATE INDEX [IX_FK_Person2DrivingLicense_RiskClaim]
ON [dbo].[Person2DrivingLicense]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2Email'
ALTER TABLE [dbo].[Person2Email]
ADD CONSTRAINT [FK_Person2Email_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Email_RiskClaim'
CREATE INDEX [IX_FK_Person2Email_RiskClaim]
ON [dbo].[Person2Email]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2IPAddress'
ALTER TABLE [dbo].[Person2IPAddress]
ADD CONSTRAINT [FK_Person2IPAddress_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2IPAddress_RiskClaim'
CREATE INDEX [IX_FK_Person2IPAddress_RiskClaim]
ON [dbo].[Person2IPAddress]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2NINumber'
ALTER TABLE [dbo].[Person2NINumber]
ADD CONSTRAINT [FK_Person2NINumber_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2NINumber_RiskClaim'
CREATE INDEX [IX_FK_Person2NINumber_RiskClaim]
ON [dbo].[Person2NINumber]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [Person2OrganisationLinkType_Id] in table 'Person2Organisation'
ALTER TABLE [dbo].[Person2Organisation]
ADD CONSTRAINT [FK_Person2Organisation_PersonOrganisationLinkType]
    FOREIGN KEY ([Person2OrganisationLinkType_Id])
    REFERENCES [dbo].[Person2OrganisationLinkType]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Organisation_PersonOrganisationLinkType'
CREATE INDEX [IX_FK_Person2Organisation_PersonOrganisationLinkType]
ON [dbo].[Person2Organisation]
    ([Person2OrganisationLinkType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2Organisation'
ALTER TABLE [dbo].[Person2Organisation]
ADD CONSTRAINT [FK_Person2Organisation_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Organisation_RiskClaim'
CREATE INDEX [IX_FK_Person2Organisation_RiskClaim]
ON [dbo].[Person2Organisation]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2Passport'
ALTER TABLE [dbo].[Person2Passport]
ADD CONSTRAINT [FK_Person2Passport_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Passport_RiskClaim'
CREATE INDEX [IX_FK_Person2Passport_RiskClaim]
ON [dbo].[Person2Passport]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2PaymentCard'
ALTER TABLE [dbo].[Person2PaymentCard]
ADD CONSTRAINT [FK_Person2PaymentCard_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2PaymentCard_RiskClaim'
CREATE INDEX [IX_FK_Person2PaymentCard_RiskClaim]
ON [dbo].[Person2PaymentCard]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [Person2PersonLinkType_Id] in table 'Person2Person'
ALTER TABLE [dbo].[Person2Person]
ADD CONSTRAINT [FK_Person2Person_Person2PersonLinkType]
    FOREIGN KEY ([Person2PersonLinkType_Id])
    REFERENCES [dbo].[Person2PersonLinkType]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Person_Person2PersonLinkType'
CREATE INDEX [IX_FK_Person2Person_Person2PersonLinkType]
ON [dbo].[Person2Person]
    ([Person2PersonLinkType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2Person'
ALTER TABLE [dbo].[Person2Person]
ADD CONSTRAINT [FK_Person2Person_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Person_RiskClaim'
CREATE INDEX [IX_FK_Person2Person_RiskClaim]
ON [dbo].[Person2Person]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [Policy_Id] in table 'Person2Policy'
ALTER TABLE [dbo].[Person2Policy]
ADD CONSTRAINT [FK_Person2Policy_Policy]
    FOREIGN KEY ([Policy_Id])
    REFERENCES [dbo].[Policies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Policy_Policy'
CREATE INDEX [IX_FK_Person2Policy_Policy]
ON [dbo].[Person2Policy]
    ([Policy_Id]);
GO

-- Creating foreign key on [PolicyLinkType_Id] in table 'Person2Policy'
ALTER TABLE [dbo].[Person2Policy]
ADD CONSTRAINT [FK_Person2Policy_PolicyLinkType]
    FOREIGN KEY ([PolicyLinkType_Id])
    REFERENCES [dbo].[PolicyLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Policy_PolicyLinkType'
CREATE INDEX [IX_FK_Person2Policy_PolicyLinkType]
ON [dbo].[Person2Policy]
    ([PolicyLinkType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2Policy'
ALTER TABLE [dbo].[Person2Policy]
ADD CONSTRAINT [FK_Person2Policy_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Policy_RiskClaim'
CREATE INDEX [IX_FK_Person2Policy_RiskClaim]
ON [dbo].[Person2Policy]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2Telephone'
ALTER TABLE [dbo].[Person2Telephone]
ADD CONSTRAINT [FK_Person2Telephone_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Telephone_RiskClaim'
CREATE INDEX [IX_FK_Person2Telephone_RiskClaim]
ON [dbo].[Person2Telephone]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [Telephone_Id] in table 'Person2Telephone'
ALTER TABLE [dbo].[Person2Telephone]
ADD CONSTRAINT [FK_Person2Telephone_Telephone]
    FOREIGN KEY ([Telephone_Id])
    REFERENCES [dbo].[Telephones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Telephone_Telephone'
CREATE INDEX [IX_FK_Person2Telephone_Telephone]
ON [dbo].[Person2Telephone]
    ([Telephone_Id]);
GO

-- Creating foreign key on [TelephoneLinkType_Id] in table 'Person2Telephone'
ALTER TABLE [dbo].[Person2Telephone]
ADD CONSTRAINT [FK_Person2Telephone_TelephoneLinkType]
    FOREIGN KEY ([TelephoneLinkType_Id])
    REFERENCES [dbo].[TelephoneLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2Telephone_TelephoneLinkType'
CREATE INDEX [IX_FK_Person2Telephone_TelephoneLinkType]
ON [dbo].[Person2Telephone]
    ([TelephoneLinkType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Person2WebSite'
ALTER TABLE [dbo].[Person2WebSite]
ADD CONSTRAINT [FK_Person2WebSite_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2WebSite_RiskClaim'
CREATE INDEX [IX_FK_Person2WebSite_RiskClaim]
ON [dbo].[Person2WebSite]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [WebSite_Id] in table 'Person2WebSite'
ALTER TABLE [dbo].[Person2WebSite]
ADD CONSTRAINT [FK_Person2WebSite_WebSite]
    FOREIGN KEY ([WebSite_Id])
    REFERENCES [dbo].[WebSites]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Person2WebSite_WebSite'
CREATE INDEX [IX_FK_Person2WebSite_WebSite]
ON [dbo].[Person2WebSite]
    ([WebSite_Id]);
GO

-- Creating foreign key on [PolicyCoverType_Id] in table 'Policies'
ALTER TABLE [dbo].[Policies]
ADD CONSTRAINT [FK_Policy_PolicyCoverType]
    FOREIGN KEY ([PolicyCoverType_Id])
    REFERENCES [dbo].[PolicyCoverTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Policy_PolicyCoverType'
CREATE INDEX [IX_FK_Policy_PolicyCoverType]
ON [dbo].[Policies]
    ([PolicyCoverType_Id]);
GO

-- Creating foreign key on [Policy_Id] in table 'Policy2BankAccount'
ALTER TABLE [dbo].[Policy2BankAccount]
ADD CONSTRAINT [FK_Policy2BankAccount_Policy]
    FOREIGN KEY ([Policy_Id])
    REFERENCES [dbo].[Policies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Policy2BankAccount_Policy'
CREATE INDEX [IX_FK_Policy2BankAccount_Policy]
ON [dbo].[Policy2BankAccount]
    ([Policy_Id]);
GO

-- Creating foreign key on [Policy_Id] in table 'Policy2PaymentCard'
ALTER TABLE [dbo].[Policy2PaymentCard]
ADD CONSTRAINT [FK_Policy2PaymentCard_Policy]
    FOREIGN KEY ([Policy_Id])
    REFERENCES [dbo].[Policies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Policy2PaymentCard_Policy'
CREATE INDEX [IX_FK_Policy2PaymentCard_Policy]
ON [dbo].[Policy2PaymentCard]
    ([Policy_Id]);
GO

-- Creating foreign key on [PolicyType_Id] in table 'Policies'
ALTER TABLE [dbo].[Policies]
ADD CONSTRAINT [FK_Policy2TypePolicy]
    FOREIGN KEY ([PolicyType_Id])
    REFERENCES [dbo].[PolicyTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Policy2TypePolicy'
CREATE INDEX [IX_FK_Policy2TypePolicy]
ON [dbo].[Policies]
    ([PolicyType_Id]);
GO

-- Creating foreign key on [Policy_Id] in table 'TOG2Policy'
ALTER TABLE [dbo].[TOG2Policy]
ADD CONSTRAINT [FK_TOG2Policy_Policy]
    FOREIGN KEY ([Policy_Id])
    REFERENCES [dbo].[Policies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Policy_Policy'
CREATE INDEX [IX_FK_TOG2Policy_Policy]
ON [dbo].[TOG2Policy]
    ([Policy_Id]);
GO

-- Creating foreign key on [Policy_Id] in table 'Vehicle2Policy'
ALTER TABLE [dbo].[Vehicle2Policy]
ADD CONSTRAINT [FK_Vehicle2Policy_Policy]
    FOREIGN KEY ([Policy_Id])
    REFERENCES [dbo].[Policies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Policy_Policy'
CREATE INDEX [IX_FK_Vehicle2Policy_Policy]
ON [dbo].[Vehicle2Policy]
    ([Policy_Id]);
GO

-- Creating foreign key on [PolicyPaymentType_Id] in table 'Policy2BankAccount'
ALTER TABLE [dbo].[Policy2BankAccount]
ADD CONSTRAINT [FK_Policy2BankAccount_PolicyPaymentType]
    FOREIGN KEY ([PolicyPaymentType_Id])
    REFERENCES [dbo].[PolicyPaymentTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Policy2BankAccount_PolicyPaymentType'
CREATE INDEX [IX_FK_Policy2BankAccount_PolicyPaymentType]
ON [dbo].[Policy2BankAccount]
    ([PolicyPaymentType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Policy2BankAccount'
ALTER TABLE [dbo].[Policy2BankAccount]
ADD CONSTRAINT [FK_Policy2BankAccount_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Policy2BankAccount_RiskClaim'
CREATE INDEX [IX_FK_Policy2BankAccount_RiskClaim]
ON [dbo].[Policy2BankAccount]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [PolicyPaymentType_Id] in table 'Policy2PaymentCard'
ALTER TABLE [dbo].[Policy2PaymentCard]
ADD CONSTRAINT [FK_Policy2PaymentCard_PolicyPaymentType]
    FOREIGN KEY ([PolicyPaymentType_Id])
    REFERENCES [dbo].[PolicyPaymentTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Policy2PaymentCard_PolicyPaymentType'
CREATE INDEX [IX_FK_Policy2PaymentCard_PolicyPaymentType]
ON [dbo].[Policy2PaymentCard]
    ([PolicyPaymentType_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Policy2PaymentCard'
ALTER TABLE [dbo].[Policy2PaymentCard]
ADD CONSTRAINT [FK_Policy2PaymentCard_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Policy2PaymentCard_RiskClaim'
CREATE INDEX [IX_FK_Policy2PaymentCard_RiskClaim]
ON [dbo].[Policy2PaymentCard]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [PolicyLinkType_Id] in table 'Vehicle2Policy'
ALTER TABLE [dbo].[Vehicle2Policy]
ADD CONSTRAINT [FK_Vehicle2Policy_PolicyLinkType]
    FOREIGN KEY ([PolicyLinkType_Id])
    REFERENCES [dbo].[PolicyLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Policy_PolicyLinkType'
CREATE INDEX [IX_FK_Vehicle2Policy_PolicyLinkType]
ON [dbo].[Vehicle2Policy]
    ([PolicyLinkType_Id]);
GO

-- Creating foreign key on [Client_Id] in table 'RiskBatches'
ALTER TABLE [dbo].[RiskBatches]
ADD CONSTRAINT [FK_RiskBatch_RiskClient]
    FOREIGN KEY ([Client_Id])
    REFERENCES [dbo].[RiskClients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RiskBatch_RiskClient'
CREATE INDEX [IX_FK_RiskBatch_RiskClient]
ON [dbo].[RiskBatches]
    ([Client_Id]);
GO

-- Creating foreign key on [OriginalFile_Id] in table 'RiskBatches'
ALTER TABLE [dbo].[RiskBatches]
ADD CONSTRAINT [FK_RiskBatch_RiskOriginalFile]
    FOREIGN KEY ([OriginalFile_Id])
    REFERENCES [dbo].[RiskOriginalFiles]
        ([PkId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RiskBatch_RiskOriginalFile'
CREATE INDEX [IX_FK_RiskBatch_RiskOriginalFile]
ON [dbo].[RiskBatches]
    ([OriginalFile_Id]);
GO

-- Creating foreign key on [RiskBatch_Id] in table 'RiskClaims'
ALTER TABLE [dbo].[RiskClaims]
ADD CONSTRAINT [FK_RiskClaim_RiskBatch]
    FOREIGN KEY ([RiskBatch_Id])
    REFERENCES [dbo].[RiskBatches]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RiskClaim_RiskBatch'
CREATE INDEX [IX_FK_RiskClaim_RiskBatch]
ON [dbo].[RiskClaims]
    ([RiskBatch_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'RiskClaimRuns'
ALTER TABLE [dbo].[RiskClaimRuns]
ADD CONSTRAINT [FK_RiskClaimRun_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RiskClaimRun_RiskClaim'
CREATE INDEX [IX_FK_RiskClaimRun_RiskClaim]
ON [dbo].[RiskClaimRuns]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2Account'
ALTER TABLE [dbo].[TOG2Account]
ADD CONSTRAINT [FK_TOG2Account_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Account_RiskClaim'
CREATE INDEX [IX_FK_TOG2Account_RiskClaim]
ON [dbo].[TOG2Account]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2Address'
ALTER TABLE [dbo].[TOG2Address]
ADD CONSTRAINT [FK_TOG2Address_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Address_RiskClaim'
CREATE INDEX [IX_FK_TOG2Address_RiskClaim]
ON [dbo].[TOG2Address]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2DrivingLicense'
ALTER TABLE [dbo].[TOG2DrivingLicense]
ADD CONSTRAINT [FK_TOG2DrivingLicense_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2DrivingLicense_RiskClaim'
CREATE INDEX [IX_FK_TOG2DrivingLicense_RiskClaim]
ON [dbo].[TOG2DrivingLicense]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2Email'
ALTER TABLE [dbo].[TOG2Email]
ADD CONSTRAINT [FK_TOG2Email_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Email_RiskClaim'
CREATE INDEX [IX_FK_TOG2Email_RiskClaim]
ON [dbo].[TOG2Email]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2FraudRing'
ALTER TABLE [dbo].[TOG2FraudRing]
ADD CONSTRAINT [FK_TOG2FraudRing_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2FraudRing_RiskClaim'
CREATE INDEX [IX_FK_TOG2FraudRing_RiskClaim]
ON [dbo].[TOG2FraudRing]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2Incident'
ALTER TABLE [dbo].[TOG2Incident]
ADD CONSTRAINT [FK_TOG2Incident_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Incident_RiskClaim'
CREATE INDEX [IX_FK_TOG2Incident_RiskClaim]
ON [dbo].[TOG2Incident]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2IPAddress'
ALTER TABLE [dbo].[TOG2IPAddress]
ADD CONSTRAINT [FK_TOG2IPAddress_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2IPAddress_RiskClaim'
CREATE INDEX [IX_FK_TOG2IPAddress_RiskClaim]
ON [dbo].[TOG2IPAddress]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2KeoghsCase'
ALTER TABLE [dbo].[TOG2KeoghsCase]
ADD CONSTRAINT [FK_TOG2KeoghsCase_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2KeoghsCase_RiskClaim'
CREATE INDEX [IX_FK_TOG2KeoghsCase_RiskClaim]
ON [dbo].[TOG2KeoghsCase]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2NINumber'
ALTER TABLE [dbo].[TOG2NINumber]
ADD CONSTRAINT [FK_TOG2NINumber_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2NINumber_RiskClaim'
CREATE INDEX [IX_FK_TOG2NINumber_RiskClaim]
ON [dbo].[TOG2NINumber]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2Organisation'
ALTER TABLE [dbo].[TOG2Organisation]
ADD CONSTRAINT [FK_TOG2Organisation_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Organisation_RiskClaim'
CREATE INDEX [IX_FK_TOG2Organisation_RiskClaim]
ON [dbo].[TOG2Organisation]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2Passport'
ALTER TABLE [dbo].[TOG2Passport]
ADD CONSTRAINT [FK_TOG2Passport_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Passport_RiskClaim'
CREATE INDEX [IX_FK_TOG2Passport_RiskClaim]
ON [dbo].[TOG2Passport]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2PaymentCard'
ALTER TABLE [dbo].[TOG2PaymentCard]
ADD CONSTRAINT [FK_TOG2PaymentCard_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2PaymentCard_RiskClaim'
CREATE INDEX [IX_FK_TOG2PaymentCard_RiskClaim]
ON [dbo].[TOG2PaymentCard]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2Person'
ALTER TABLE [dbo].[TOG2Person]
ADD CONSTRAINT [FK_TOG2Person_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Person_RiskClaim'
CREATE INDEX [IX_FK_TOG2Person_RiskClaim]
ON [dbo].[TOG2Person]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2Policy'
ALTER TABLE [dbo].[TOG2Policy]
ADD CONSTRAINT [FK_TOG2Policy_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Policy_RiskClaim'
CREATE INDEX [IX_FK_TOG2Policy_RiskClaim]
ON [dbo].[TOG2Policy]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2Telephone'
ALTER TABLE [dbo].[TOG2Telephone]
ADD CONSTRAINT [FK_TOG2Telephone_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Telephone_RiskClaim'
CREATE INDEX [IX_FK_TOG2Telephone_RiskClaim]
ON [dbo].[TOG2Telephone]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2TOG'
ALTER TABLE [dbo].[TOG2TOG]
ADD CONSTRAINT [FK_TOG2TOG_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2TOG_RiskClaim'
CREATE INDEX [IX_FK_TOG2TOG_RiskClaim]
ON [dbo].[TOG2TOG]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2Vehicle'
ALTER TABLE [dbo].[TOG2Vehicle]
ADD CONSTRAINT [FK_TOG2Vehicle_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Vehicle_RiskClaim'
CREATE INDEX [IX_FK_TOG2Vehicle_RiskClaim]
ON [dbo].[TOG2Vehicle]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [BaseRiskClaim_Id] in table 'TOG2Website'
ALTER TABLE [dbo].[TOG2Website]
ADD CONSTRAINT [FK_TOG2Website_RiskClaim]
    FOREIGN KEY ([BaseRiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Website_RiskClaim'
CREATE INDEX [IX_FK_TOG2Website_RiskClaim]
ON [dbo].[TOG2Website]
    ([BaseRiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Vehicle2Address'
ALTER TABLE [dbo].[Vehicle2Address]
ADD CONSTRAINT [FK_Vehicle2Address_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Address_RiskClaim'
CREATE INDEX [IX_FK_Vehicle2Address_RiskClaim]
ON [dbo].[Vehicle2Address]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Vehicle2Organisation'
ALTER TABLE [dbo].[Vehicle2Organisation]
ADD CONSTRAINT [FK_Vehicle2Organisation_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Organisation_RiskClaim'
CREATE INDEX [IX_FK_Vehicle2Organisation_RiskClaim]
ON [dbo].[Vehicle2Organisation]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Vehicle2Person'
ALTER TABLE [dbo].[Vehicle2Person]
ADD CONSTRAINT [FK_Vehicle2Person_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Person_RiskClaim'
CREATE INDEX [IX_FK_Vehicle2Person_RiskClaim]
ON [dbo].[Vehicle2Person]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Vehicle2Policy'
ALTER TABLE [dbo].[Vehicle2Policy]
ADD CONSTRAINT [FK_Vehicle2Policy_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Policy_RiskClaim'
CREATE INDEX [IX_FK_Vehicle2Policy_RiskClaim]
ON [dbo].[Vehicle2Policy]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [RiskClaim_Id] in table 'Vehicle2Vehicle'
ALTER TABLE [dbo].[Vehicle2Vehicle]
ADD CONSTRAINT [FK_Vehicle2Vehicle_RiskClaim]
    FOREIGN KEY ([RiskClaim_Id])
    REFERENCES [dbo].[RiskClaims]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Vehicle_RiskClaim'
CREATE INDEX [IX_FK_Vehicle2Vehicle_RiskClaim]
ON [dbo].[Vehicle2Vehicle]
    ([RiskClaim_Id]);
GO

-- Creating foreign key on [TelephoneType_Id] in table 'Telephones'
ALTER TABLE [dbo].[Telephones]
ADD CONSTRAINT [FK_Telephone_TelephoneType]
    FOREIGN KEY ([TelephoneType_Id])
    REFERENCES [dbo].[TelephoneTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Telephone_TelephoneType'
CREATE INDEX [IX_FK_Telephone_TelephoneType]
ON [dbo].[Telephones]
    ([TelephoneType_Id]);
GO

-- Creating foreign key on [Telephone_Id] in table 'TOG2Telephone'
ALTER TABLE [dbo].[TOG2Telephone]
ADD CONSTRAINT [FK_TOG2Telephone_Telephone]
    FOREIGN KEY ([Telephone_Id])
    REFERENCES [dbo].[Telephones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Telephone_Telephone'
CREATE INDEX [IX_FK_TOG2Telephone_Telephone]
ON [dbo].[TOG2Telephone]
    ([Telephone_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2Account'
ALTER TABLE [dbo].[TOG2Account]
ADD CONSTRAINT [FK_TOG2Account_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Account_TOG'
CREATE INDEX [IX_FK_TOG2Account_TOG]
ON [dbo].[TOG2Account]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2Address'
ALTER TABLE [dbo].[TOG2Address]
ADD CONSTRAINT [FK_TOG2Address_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Address_TOG'
CREATE INDEX [IX_FK_TOG2Address_TOG]
ON [dbo].[TOG2Address]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2DrivingLicense'
ALTER TABLE [dbo].[TOG2DrivingLicense]
ADD CONSTRAINT [FK_TOG2DrivingLicense_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2DrivingLicense_TOG'
CREATE INDEX [IX_FK_TOG2DrivingLicense_TOG]
ON [dbo].[TOG2DrivingLicense]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2Email'
ALTER TABLE [dbo].[TOG2Email]
ADD CONSTRAINT [FK_TOG2Email_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Email_TOG'
CREATE INDEX [IX_FK_TOG2Email_TOG]
ON [dbo].[TOG2Email]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2FraudRing'
ALTER TABLE [dbo].[TOG2FraudRing]
ADD CONSTRAINT [FK_TOG2FraudRing_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2FraudRing_TOG'
CREATE INDEX [IX_FK_TOG2FraudRing_TOG]
ON [dbo].[TOG2FraudRing]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2Incident'
ALTER TABLE [dbo].[TOG2Incident]
ADD CONSTRAINT [FK_TOG2Incident_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Incident_TOG'
CREATE INDEX [IX_FK_TOG2Incident_TOG]
ON [dbo].[TOG2Incident]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2IPAddress'
ALTER TABLE [dbo].[TOG2IPAddress]
ADD CONSTRAINT [FK_TOG2IPAddress_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2IPAddress_TOG'
CREATE INDEX [IX_FK_TOG2IPAddress_TOG]
ON [dbo].[TOG2IPAddress]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2KeoghsCase'
ALTER TABLE [dbo].[TOG2KeoghsCase]
ADD CONSTRAINT [FK_TOG2KeoghsCase_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2KeoghsCase_TOG'
CREATE INDEX [IX_FK_TOG2KeoghsCase_TOG]
ON [dbo].[TOG2KeoghsCase]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2NINumber'
ALTER TABLE [dbo].[TOG2NINumber]
ADD CONSTRAINT [FK_TOG2NINumber_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2NINumber_TOG'
CREATE INDEX [IX_FK_TOG2NINumber_TOG]
ON [dbo].[TOG2NINumber]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2Organisation'
ALTER TABLE [dbo].[TOG2Organisation]
ADD CONSTRAINT [FK_TOG2Organisation_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Organisation_TOG'
CREATE INDEX [IX_FK_TOG2Organisation_TOG]
ON [dbo].[TOG2Organisation]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2Passport'
ALTER TABLE [dbo].[TOG2Passport]
ADD CONSTRAINT [FK_TOG2Passport_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Passport_TOG'
CREATE INDEX [IX_FK_TOG2Passport_TOG]
ON [dbo].[TOG2Passport]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2PaymentCard'
ALTER TABLE [dbo].[TOG2PaymentCard]
ADD CONSTRAINT [FK_TOG2PaymentCard_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2PaymentCard_TOG'
CREATE INDEX [IX_FK_TOG2PaymentCard_TOG]
ON [dbo].[TOG2PaymentCard]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2Person'
ALTER TABLE [dbo].[TOG2Person]
ADD CONSTRAINT [FK_TOG2Person_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Person_TOG'
CREATE INDEX [IX_FK_TOG2Person_TOG]
ON [dbo].[TOG2Person]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2Policy'
ALTER TABLE [dbo].[TOG2Policy]
ADD CONSTRAINT [FK_TOG2Policy_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Policy_TOG'
CREATE INDEX [IX_FK_TOG2Policy_TOG]
ON [dbo].[TOG2Policy]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2Telephone'
ALTER TABLE [dbo].[TOG2Telephone]
ADD CONSTRAINT [FK_TOG2Telephone_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Telephone_TOG'
CREATE INDEX [IX_FK_TOG2Telephone_TOG]
ON [dbo].[TOG2Telephone]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG1_Id] in table 'TOG2TOG'
ALTER TABLE [dbo].[TOG2TOG]
ADD CONSTRAINT [FK_TOG2TOG_TOG]
    FOREIGN KEY ([TOG1_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2TOG_TOG'
CREATE INDEX [IX_FK_TOG2TOG_TOG]
ON [dbo].[TOG2TOG]
    ([TOG1_Id]);
GO

-- Creating foreign key on [TOG2_Id] in table 'TOG2TOG'
ALTER TABLE [dbo].[TOG2TOG]
ADD CONSTRAINT [FK_TOG2TOG_TOG1]
    FOREIGN KEY ([TOG2_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2TOG_TOG1'
CREATE INDEX [IX_FK_TOG2TOG_TOG1]
ON [dbo].[TOG2TOG]
    ([TOG2_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2Vehicle'
ALTER TABLE [dbo].[TOG2Vehicle]
ADD CONSTRAINT [FK_TOG2Vehicle_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Vehicle_TOG'
CREATE INDEX [IX_FK_TOG2Vehicle_TOG]
ON [dbo].[TOG2Vehicle]
    ([TOG_Id]);
GO

-- Creating foreign key on [TOG_Id] in table 'TOG2Website'
ALTER TABLE [dbo].[TOG2Website]
ADD CONSTRAINT [FK_TOG2Website_TOG]
    FOREIGN KEY ([TOG_Id])
    REFERENCES [dbo].[TOGs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Website_TOG'
CREATE INDEX [IX_FK_TOG2Website_TOG]
ON [dbo].[TOG2Website]
    ([TOG_Id]);
GO

-- Creating foreign key on [Vehicle_Id] in table 'TOG2Vehicle'
ALTER TABLE [dbo].[TOG2Vehicle]
ADD CONSTRAINT [FK_TOG2Vehicle_Vehicle]
    FOREIGN KEY ([Vehicle_Id])
    REFERENCES [dbo].[Vehicles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Vehicle_Vehicle'
CREATE INDEX [IX_FK_TOG2Vehicle_Vehicle]
ON [dbo].[TOG2Vehicle]
    ([Vehicle_Id]);
GO

-- Creating foreign key on [Website_Id] in table 'TOG2Website'
ALTER TABLE [dbo].[TOG2Website]
ADD CONSTRAINT [FK_TOG2Website_WebSite]
    FOREIGN KEY ([Website_Id])
    REFERENCES [dbo].[WebSites]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TOG2Website_WebSite'
CREATE INDEX [IX_FK_TOG2Website_WebSite]
ON [dbo].[TOG2Website]
    ([Website_Id]);
GO

-- Creating foreign key on [VehicleCategoryOfLoss_Id] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_Vehicle_VehicleCategoryOfLoss]
    FOREIGN KEY ([VehicleCategoryOfLoss_Id])
    REFERENCES [dbo].[VehicleCategoryOfLosses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_VehicleCategoryOfLoss'
CREATE INDEX [IX_FK_Vehicle_VehicleCategoryOfLoss]
ON [dbo].[Vehicles]
    ([VehicleCategoryOfLoss_Id]);
GO

-- Creating foreign key on [VehicleColour_Id] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_Vehicle_VehicleColour1]
    FOREIGN KEY ([VehicleColour_Id])
    REFERENCES [dbo].[VehicleColours]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_VehicleColour1'
CREATE INDEX [IX_FK_Vehicle_VehicleColour1]
ON [dbo].[Vehicles]
    ([VehicleColour_Id]);
GO

-- Creating foreign key on [VehicleFuel_Id] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_Vehicle_VehicleFuel]
    FOREIGN KEY ([VehicleFuel_Id])
    REFERENCES [dbo].[VehicleFuels]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_VehicleFuel'
CREATE INDEX [IX_FK_Vehicle_VehicleFuel]
ON [dbo].[Vehicles]
    ([VehicleFuel_Id]);
GO

-- Creating foreign key on [VehicleTransmission_Id] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_Vehicle_VehicleTransmission]
    FOREIGN KEY ([VehicleTransmission_Id])
    REFERENCES [dbo].[VehicleTransmissions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_VehicleTransmission'
CREATE INDEX [IX_FK_Vehicle_VehicleTransmission]
ON [dbo].[Vehicles]
    ([VehicleTransmission_Id]);
GO

-- Creating foreign key on [Vehicle_Id] in table 'Vehicle2Address'
ALTER TABLE [dbo].[Vehicle2Address]
ADD CONSTRAINT [FK_Vehicle2Address_Vehicle]
    FOREIGN KEY ([Vehicle_Id])
    REFERENCES [dbo].[Vehicles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Address_Vehicle'
CREATE INDEX [IX_FK_Vehicle2Address_Vehicle]
ON [dbo].[Vehicle2Address]
    ([Vehicle_Id]);
GO

-- Creating foreign key on [Vehicle_Id] in table 'Vehicle2Organisation'
ALTER TABLE [dbo].[Vehicle2Organisation]
ADD CONSTRAINT [FK_Vehicle2Organisation_Vehicle]
    FOREIGN KEY ([Vehicle_Id])
    REFERENCES [dbo].[Vehicles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Organisation_Vehicle'
CREATE INDEX [IX_FK_Vehicle2Organisation_Vehicle]
ON [dbo].[Vehicle2Organisation]
    ([Vehicle_Id]);
GO

-- Creating foreign key on [Vehicle_Id] in table 'Vehicle2Person'
ALTER TABLE [dbo].[Vehicle2Person]
ADD CONSTRAINT [FK_Vehicle2Person_Vehicle]
    FOREIGN KEY ([Vehicle_Id])
    REFERENCES [dbo].[Vehicles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Person_Vehicle'
CREATE INDEX [IX_FK_Vehicle2Person_Vehicle]
ON [dbo].[Vehicle2Person]
    ([Vehicle_Id]);
GO

-- Creating foreign key on [Vehicle_Id] in table 'Vehicle2Policy'
ALTER TABLE [dbo].[Vehicle2Policy]
ADD CONSTRAINT [FK_Vehicle2Policy_Vehicle]
    FOREIGN KEY ([Vehicle_Id])
    REFERENCES [dbo].[Vehicles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Policy_Vehicle'
CREATE INDEX [IX_FK_Vehicle2Policy_Vehicle]
ON [dbo].[Vehicle2Policy]
    ([Vehicle_Id]);
GO

-- Creating foreign key on [Vehicle1_Id] in table 'Vehicle2Vehicle'
ALTER TABLE [dbo].[Vehicle2Vehicle]
ADD CONSTRAINT [FK_Vehicle2Vehicle_Vehicle1]
    FOREIGN KEY ([Vehicle1_Id])
    REFERENCES [dbo].[Vehicles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Vehicle_Vehicle1'
CREATE INDEX [IX_FK_Vehicle2Vehicle_Vehicle1]
ON [dbo].[Vehicle2Vehicle]
    ([Vehicle1_Id]);
GO

-- Creating foreign key on [Vehicle2_Id] in table 'Vehicle2Vehicle'
ALTER TABLE [dbo].[Vehicle2Vehicle]
ADD CONSTRAINT [FK_Vehicle2Vehicle_Vehicle2]
    FOREIGN KEY ([Vehicle2_Id])
    REFERENCES [dbo].[Vehicles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Vehicle_Vehicle2'
CREATE INDEX [IX_FK_Vehicle2Vehicle_Vehicle2]
ON [dbo].[Vehicle2Vehicle]
    ([Vehicle2_Id]);
GO

-- Creating foreign key on [VehicleType_Id] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_VehicleTypeVehicle]
    FOREIGN KEY ([VehicleType_Id])
    REFERENCES [dbo].[VehicleTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_VehicleTypeVehicle'
CREATE INDEX [IX_FK_VehicleTypeVehicle]
ON [dbo].[Vehicles]
    ([VehicleType_Id]);
GO

-- Creating foreign key on [VehicleLinkType_Id] in table 'Vehicle2Organisation'
ALTER TABLE [dbo].[Vehicle2Organisation]
ADD CONSTRAINT [FK_Vehicle2Organisation_VehicleLinkType]
    FOREIGN KEY ([VehicleLinkType_Id])
    REFERENCES [dbo].[VehicleLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Organisation_VehicleLinkType'
CREATE INDEX [IX_FK_Vehicle2Organisation_VehicleLinkType]
ON [dbo].[Vehicle2Organisation]
    ([VehicleLinkType_Id]);
GO

-- Creating foreign key on [VehicleLinkType_Id] in table 'Vehicle2Person'
ALTER TABLE [dbo].[Vehicle2Person]
ADD CONSTRAINT [FK_Vehicle2Person_VehicleLinkType]
    FOREIGN KEY ([VehicleLinkType_Id])
    REFERENCES [dbo].[VehicleLinkTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle2Person_VehicleLinkType'
CREATE INDEX [IX_FK_Vehicle2Person_VehicleLinkType]
ON [dbo].[Vehicle2Person]
    ([VehicleLinkType_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------