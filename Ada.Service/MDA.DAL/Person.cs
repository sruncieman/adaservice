//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Person
    {
        public Person()
        {
            this.Handset2Person = new HashSet<Handset2Person>();
            this.Incident2Person = new HashSet<Incident2Person>();
            this.Incident2PersonOutcome = new HashSet<Incident2PersonOutcome>();
            this.Person2Address = new HashSet<Person2Address>();
            this.Person2BankAccount = new HashSet<Person2BankAccount>();
            this.Person2BBPin = new HashSet<Person2BBPin>();
            this.Person2DrivingLicense = new HashSet<Person2DrivingLicense>();
            this.Person2Email = new HashSet<Person2Email>();
            this.Person2IPAddress = new HashSet<Person2IPAddress>();
            this.Person2NINumber = new HashSet<Person2NINumber>();
            this.Person2Organisation = new HashSet<Person2Organisation>();
            this.Person2Passport = new HashSet<Person2Passport>();
            this.Person2PaymentCard = new HashSet<Person2PaymentCard>();
            this.Person2Person = new HashSet<Person2Person>();
            this.Person2Person1 = new HashSet<Person2Person>();
            this.Person2Policy = new HashSet<Person2Policy>();
            this.Person2Telephone = new HashSet<Person2Telephone>();
            this.Person2WebSite = new HashSet<Person2WebSite>();
            this.TOG2Person = new HashSet<TOG2Person>();
            this.Vehicle2Person = new HashSet<Vehicle2Person>();
        }
    
        public int Id { get; set; }
        public string PersonId { get; set; }
        public int Gender_Id { get; set; }
        public int Salutation_Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string Nationality { get; set; }
        public string Occupation { get; set; }
        public string TaxiDriverLicense { get; set; }
        public string Schools { get; set; }
        public string Hobbies { get; set; }
        public string Notes { get; set; }
        public string Documents { get; set; }
        public string KeyAttractor { get; set; }
        public Nullable<System.DateTime> SanctionDate { get; set; }
        public string SanctionSource { get; set; }
        public string Source { get; set; }
        public string SourceReference { get; set; }
        public string SourceDescription { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string IBaseId { get; set; }
        public byte RecordStatus { get; set; }
        public byte ADARecordStatus { get; set; }
        public byte[] RowVersion { get; set; }
        public string SanctionList { get; set; }
        public string FraudRingClosed_ { get; set; }
    
        public virtual Gender Gender { get; set; }
        public virtual ICollection<Handset2Person> Handset2Person { get; set; }
        public virtual ICollection<Incident2Person> Incident2Person { get; set; }
        public virtual ICollection<Incident2PersonOutcome> Incident2PersonOutcome { get; set; }
        public virtual ICollection<Person2Address> Person2Address { get; set; }
        public virtual ICollection<Person2BankAccount> Person2BankAccount { get; set; }
        public virtual ICollection<Person2BBPin> Person2BBPin { get; set; }
        public virtual ICollection<Person2DrivingLicense> Person2DrivingLicense { get; set; }
        public virtual ICollection<Person2Email> Person2Email { get; set; }
        public virtual ICollection<Person2IPAddress> Person2IPAddress { get; set; }
        public virtual ICollection<Person2NINumber> Person2NINumber { get; set; }
        public virtual ICollection<Person2Organisation> Person2Organisation { get; set; }
        public virtual ICollection<Person2Passport> Person2Passport { get; set; }
        public virtual ICollection<Person2PaymentCard> Person2PaymentCard { get; set; }
        public virtual ICollection<Person2Person> Person2Person { get; set; }
        public virtual ICollection<Person2Person> Person2Person1 { get; set; }
        public virtual ICollection<Person2Policy> Person2Policy { get; set; }
        public virtual Salutation Salutation { get; set; }
        public virtual ICollection<Person2Telephone> Person2Telephone { get; set; }
        public virtual ICollection<Person2WebSite> Person2WebSite { get; set; }
        public virtual ICollection<TOG2Person> TOG2Person { get; set; }
        public virtual ICollection<Vehicle2Person> Vehicle2Person { get; set; }
    }
}
