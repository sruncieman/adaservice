//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Incident2VehicleLinkType
    {
        public Incident2VehicleLinkType()
        {
            this.Incident2Vehicle = new HashSet<Incident2Vehicle>();
        }
    
        public int Id { get; set; }
        public string Text { get; set; }
        public byte RecordStatus { get; set; }
        public byte ADARecordStatus { get; set; }
    
        public virtual ICollection<Incident2Vehicle> Incident2Vehicle { get; set; }
    }
}
