//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MDA.DAL
{
    using System;
    
    public partial class fnGetMatchingSanctionsOrganisationToProcess_Result
    {
        public int RuleNo { get; set; }
        public Nullable<bool> OrganisationName { get; set; }
        public Nullable<bool> RegisteredNumber { get; set; }
        public Nullable<bool> VATNumber { get; set; }
        public Nullable<bool> EmailAddress { get; set; }
        public Nullable<bool> Website { get; set; }
        public Nullable<bool> IPAddress { get; set; }
        public Nullable<bool> TelephoneNumber { get; set; }
        public Nullable<bool> AccountNumber { get; set; }
        public Nullable<bool> PaymentCardNumber { get; set; }
        public Nullable<bool> SubBuilding { get; set; }
        public Nullable<bool> Building { get; set; }
        public Nullable<bool> BuildingNumber { get; set; }
        public Nullable<bool> Street { get; set; }
        public Nullable<bool> Locality { get; set; }
        public Nullable<bool> Town { get; set; }
        public Nullable<bool> PostCode { get; set; }
        public Nullable<bool> PafUPRN { get; set; }
        public Nullable<int> PriorityGroup { get; set; }
        public Nullable<int> MatchType { get; set; }
        public int RiskClaimFlag { get; set; }
    }
}
