﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using MDA.Common.Enum;
using MDA.Pipeline.Model;


namespace MDA.TestMapping
{
    public static class MapFromExcel
    {
        public static PipelineClaimBatch ReadExcelFile(String strFile, string strSheetName)
        {
            PipelineClaimBatch batch = new PipelineClaimBatch();

            //string strExtensionName = "";
            string strFileName = strFile;
            DataSet ds = new DataSet();
            DataTable dtt = new DataTable();

            if (!string.IsNullOrEmpty(strFileName))
            {
                //get the extension name, check if it's a spreadsheet                
                //strExtensionName = strFileName.Substring(strFileName.IndexOf(".") + 1);

                //open connection out to read excel 
                string strConnectionString = string.Empty;

                //if (strExtensionName == "xlsx")
                strConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strFile + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1\"";

                if (!string.IsNullOrEmpty(strConnectionString))
                {
                    OleDbConnection objConnection = new OleDbConnection(strConnectionString);
                    objConnection.Open();
                    DataTable oleDbSchemaTable = objConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                    OleDbCommand objCmd = new OleDbCommand(string.Format("Select * from [{0}$]", strSheetName), objConnection);
                    OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
                    objAdapter1.SelectCommand = objCmd;
                    DataSet objDataSet = new DataSet();
                    objAdapter1.Fill(objDataSet);
                    objConnection.Close();
                    dtt = objDataSet.Tables[0];

                    CreateXmlDocument(dtt, batch);

                }

            }
            return batch;
        }

        private static PipelineClaimBatch CreateXmlDocument(DataTable dtt, PipelineClaimBatch batch)
        {
            DateTime incidentDate = new DateTime();
            TimeSpan incidentTime;


            var distinctRows = (from DataRow dr in dtt.Rows
                                where dr.Field<string>("Claim Number") != null //&& dr.Field<string>("Claim Number") == "CLB0011/1"
                                select (string)dr["Claim Number"]).Take(1).Distinct();

            foreach (var distinctRow in distinctRows)
            {

   #region Batch Claim

                PipelineMotorClaim claim = new PipelineMotorClaim();
                
                DataTable claimRows = (from DataRow dr in dtt.AsEnumerable()
                                       where dr.Field<string>("Claim Number") == distinctRow
                                       select dr).CopyToDataTable();

                // returns unique vehicle registraions
                var distinctVehicles = (from DataRow dr in dtt.AsEnumerable()
                                        where dr.Field<string>("Vehicle Registration Number") != null
                                              && dr.Field<string>("Claim Number") == distinctRow
                                        select (string) dr["Vehicle Registration Number"]).Distinct().ToList();


    #region Claim Level

                DataTable claimRowsFirst = (from DataRow dr in dtt.AsEnumerable()
                                            where dr.Field<string>("Claim Number") == distinctRow
                                            select dr).Take(1).CopyToDataTable();

                foreach (DataRow row in claimRowsFirst.Rows)
                {
                    foreach (DataColumn column in claimRowsFirst.Columns)
                    {
                        
                        //if (row[column].ToString() != string.Empty)
                        //{
                            switch (column.ColumnName)
                            {
    #region Claim
                                case "Claim Number":
                                    claim.ClaimNumber = row[column].ToString();
                                    break;
                                case "Incident Date":
                                    string incidentTempDate = row[column].ToString();
                                    incidentDate = Convert.ToDateTime(incidentTempDate);
                                    claim.IncidentDate = incidentDate;
                                    break;
                                case "Incident Time":
                                    if (TimeSpan.TryParse(row[column].ToString(), out incidentTime))
                                        incidentDate = incidentDate.Add(incidentTime);
                                    claim.IncidentDate = incidentDate;
                                    break;
                                case "Reserve":
                                    if (claim.ExtraClaimInfo != null && !string.IsNullOrEmpty(row[column].ToString()))
                                    {
                                        decimal tempReserve = Convert.ToDecimal(row[column].ToString());
                                        claim.ExtraClaimInfo.Reserve = tempReserve;
                                    }
                                    break;


     #endregion

     #region Policy
                                case "Insurance Policy Number":
                                    //claim.Policy.PolicyNumber = row[column].ToString();
                                    //if (claim.Policy.PolicyNumber == null)
                                    //{
                                    //    claim.Policy.PolicyNumber = claim.ClaimNumber;
                                    //}
                                    claim.Policy.PolicyNumber = row[column].ToString();
                                    break;
                                case "Trading Name":
                                    claim.Policy.InsurerTradingName = row[column].ToString();
                                    break;
                                case "Broker":
                                    claim.Policy.Broker = row[column].ToString();
                                    break;
                                case "Policy Start Date":
                                    if (!string.IsNullOrEmpty(row[column].ToString()))
                                    {
                                        string policyStartTempDate = row[column].ToString();
                                        claim.Policy.PolicyStartDate = Convert.ToDateTime(policyStartTempDate);
                                    }
                                    break;
                                case "Policy End Date":
                                    if (!string.IsNullOrEmpty(row[column].ToString()))
                                    {
                                        string policyEndTempDate = row[column].ToString();
                                        claim.Policy.PolicyEndDate = Convert.ToDateTime(policyEndTempDate);
                                    }
                                    break;
                                case "Number Of Previous Fault Claims Disclosed":
                                    if (!string.IsNullOrEmpty(row[column].ToString()))
                                    {
                                        claim.Policy.PreviousFaultClaimsCount = Convert.ToInt32(row[column].ToString());
                                    }
                                    break;
                                case "Number Of Previous Non Fault Claims Disclosed":
                                    if (!string.IsNullOrEmpty(row[column].ToString()))
                                    {
                                        claim.Policy.PreviousNoFaultClaimsCount = Convert.ToInt32(row[column].ToString());
                                    }
                                    break;
                                case "Premium":
                                    if (!string.IsNullOrEmpty(row[column].ToString()))
                                    {
                                        decimal tempPolicyPremium = Convert.ToDecimal(row[column].ToString());
                                        claim.Policy.Premium = tempPolicyPremium;
                                    }
                                    break;
                                case "PolicyType":
                                    MDA.Common.Enum.PolicyType policyType = new PolicyType();
                                    switch (row[column].ToString())
                                    {
                                        case "Personal Motor":
                                            policyType = MDA.Common.Enum.PolicyType.PersonalMotor;
                                            break;
                                    }
                                    //claim.Policy.PolicyType = policyType;
                                    break;
                                case "CoverType":
                                    MDA.Common.Enum.PolicyCoverType policyCoverType = new PolicyCoverType();
                                    switch (row[column].ToString())
                                    {
                                        case "Comprehensive":
                                            policyCoverType = MDA.Common.Enum.PolicyCoverType.Comprehensive;
                                            break;
                                    }
                                    //claim.Policy.CoverType = policyCoverType;
                                    break;
                                #endregion
                            }
                        //}
                    }
                }

    #region Organisations

                #region Add Claim Organisation ClaimInfo
                #endregion

                #region Add Claim Organisation Addresses
                #endregion

                #region Add Claim Organisation Vehicles
                #endregion

    #endregion

    #endregion

    #region Vehicle Level

                foreach (var distinctVehicleReg in distinctVehicles)
                {

                    var personInVehicleDataRow = (from DataRow dr in claimRows.AsEnumerable()
                                                  where
                                                      dr.Field<string>("Vehicle Registration Number") == distinctVehicleReg
                                                  select dr).Distinct();
                    
                    PipelineVehicle vehicle = new PipelineVehicle();

                    foreach (var personDataRow in personInVehicleDataRow)
                    {
                        
                        PipelinePerson person = new PipelinePerson();

                        if (personDataRow.Field<string>("Vehicle Registration Number") == distinctVehicleReg)
                        {
          #region Vehicle Person

                            MDA.Common.Enum.Salutation salutation = new Salutation();
                            MDA.Common.Enum.Gender gender = new Gender();
                            MDA.Common.Enum.PartyType partyType = new PartyType();
                            MDA.Common.Enum.SubPartyType subPartyType = new SubPartyType();

                            switch (personDataRow.Field<string>("Salutation"))
                            {
                                case "Mr":
                                    salutation = MDA.Common.Enum.Salutation.Mr;
                                    break;
                                case "Miss":
                                    salutation = MDA.Common.Enum.Salutation.Miss;
                                    break;
                                case "Mrs":
                                    salutation = MDA.Common.Enum.Salutation.Mrs;
                                    break;
                                case "Ms":
                                    salutation = MDA.Common.Enum.Salutation.Ms;
                                    break;
                                case "Master":
                                    salutation = MDA.Common.Enum.Salutation.Master;
                                    break;
                                case "Dr":
                                    salutation = MDA.Common.Enum.Salutation.Dr;
                                    break;
                            }

                            person.Salutation_Id = (int)salutation;

                            switch (personDataRow.Field<string>("Gender"))
                            {
                                case "Male":
                                    gender = MDA.Common.Enum.Gender.Male;
                                    break;
                                case "Female":
                                    gender = MDA.Common.Enum.Gender.Female;
                                    break;
                            }

                            person.Gender_Id = (int)gender;

                            switch (personDataRow.Field<string>("Party Type"))
                            {
                                case "Insured":
                                    partyType = MDA.Common.Enum.PartyType.Insured;
                                    break;
                                case "Third Party":
                                    partyType = MDA.Common.Enum.PartyType.ThirdParty;
                                    break;
                            }

                            person.I2Pe_LinkData.PartyType_Id = (int)partyType;

                            switch (personDataRow.Field<string>("Sub Party Type"))
                            {
                                case "Driver":
                                    subPartyType = MDA.Common.Enum.SubPartyType.Driver;
                                    break;
                                case "Passenger":
                                    subPartyType = MDA.Common.Enum.SubPartyType.Passenger;
                                    break;
                                case "Hirer":
                                    subPartyType = MDA.Common.Enum.SubPartyType.Unknown;
                                    break;
                            }

                            person.I2Pe_LinkData.SubPartyType_Id = (int)subPartyType;
                            person.FirstName = personDataRow.Field<string>("First Name");
                            person.MiddleName = personDataRow.Field<string>("Middle Name");
                            person.LastName = personDataRow.Field<string>("Last Name");
                            if (string.IsNullOrEmpty(person.LastName))
                            {
                                person.LastName = person.FirstName;
                            }
                            if (personDataRow.Field<DateTime?>("Date Of Birth") != null)
                            {
                                person.DateOfBirth = personDataRow.Field<DateTime?>("Date Of Birth");
                            }
                            person.Nationality = personDataRow.Field<string>("Nationality");
                            person.NINumbers.Add( new PipelineNINumber() { NINumber1 = personDataRow.Field<string>("National Insurance Number") });
                            person.DrivingLicenseNumbers.Add(new PipelineDrivingLicense() { DriverNumber = personDataRow.Field<string>("Driving Licence Number") }) ;
                            person.PassportNumbers.Add( new PipelinePassport() { PassportNumber = personDataRow.Field<string>("Passport Number") });
                            person.Occupation = personDataRow.Field<string>("Occupation");
                            person.EmailAddresses.Add( new PipelineEmail() { EmailAddress = personDataRow.Field<string>("Email Address") });
                            person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = personDataRow.Field<string>("Landline Telephone"), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                            person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = personDataRow.Field<string>("Mobile Telephone"), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                            person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = personDataRow.Field<string>("Other Telephone"), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                            //person.MobileTelephone = personDataRow.Field<string>("Mobile Telephone");
                            //person.OtherTelephone = personDataRow.Field<string>("Other Telephone");
                            //person.AttendedHospital = personDataRow.Field<bool>("Attended Hospital");

                            #endregion

                            #region Vehicle Person Address

                            PipelineAddress address = new PipelineAddress();

                            int addressExistsCount = 0;

                            if (!string.IsNullOrEmpty(personDataRow.Field<string>("Sub-Building")))
                            {
                                address.SubBuilding = personDataRow.Field<string>("Sub-Building");
                                addressExistsCount++;
                            }

                            if (!string.IsNullOrEmpty(personDataRow.Field<string>("Building")))
                            {
                                address.Building = personDataRow.Field<string>("Building");
                                addressExistsCount++;
                            }

                            if (!string.IsNullOrEmpty(personDataRow.Field<string>("Building Number")))
                            {
                                address.BuildingNumber = personDataRow.Field<string>("Building Number");
                                addressExistsCount++;
                            }

                            if (!string.IsNullOrEmpty(personDataRow.Field<string>("Street")))
                            {
                                address.Street = personDataRow.Field<string>("Street");
                                addressExistsCount++;
                            }

                            if (!string.IsNullOrEmpty(personDataRow.Field<string>("Locality")))
                            {
                                address.Locality = personDataRow.Field<string>("Locality");
                                addressExistsCount++;
                            }

                            if (!string.IsNullOrEmpty(personDataRow.Field<string>("Town")))
                            {
                                address.Town = personDataRow.Field<string>("Town");
                                addressExistsCount++;
                            }

                            if (!string.IsNullOrEmpty(personDataRow.Field<string>("County")))
                            {
                                address.County = personDataRow.Field<string>("County");
                                addressExistsCount++;
                            }

                            if (!string.IsNullOrEmpty(personDataRow.Field<string>("Post Code")))
                            {
                                address.PostCode = personDataRow.Field<string>("Post Code");
                                addressExistsCount++;
                            }

                            if (addressExistsCount > 0)
                            {
                                person.Addresses.Add(address);
                            }

                            #endregion

                            #region Vehicle Details

                            var incident2VehicleLinkType = new Incident2VehicleLinkType();

                            switch (personDataRow.Field<string>("Vehicle Involvement Group"))
                            {
                                case "Insured":
                                    incident2VehicleLinkType = MDA.Common.Enum.Incident2VehicleLinkType.InsuredVehicle;
                                    break;
                                case "Third Party":
                                    incident2VehicleLinkType =
                                        MDA.Common.Enum.Incident2VehicleLinkType.ThirdPartyVehicle;
                                    break;
                                case "Third Party Hire":
                                    incident2VehicleLinkType =
                                        MDA.Common.Enum.Incident2VehicleLinkType.ThirdPartyHireVehicle;
                                    break;
                            }

                            vehicle.VehicleRegistration = distinctVehicleReg;
                            vehicle.EngineCapacity = null;
                            vehicle.VehicleMake = personDataRow.Field<string>("Vehicle Manufacturer");
                            vehicle.VehicleModel = personDataRow.Field<string>("Vehicle Model");
                            vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)incident2VehicleLinkType;
                            vehicle.VIN = string.Empty;

                            #endregion

                            #region Vehicle Person Organisations

                            PipelineOrganisation vehiclePersonOrganisation = new PipelineOrganisation();
                            PipelineBankAccount vehiclePersonOrgBankAccount = new PipelineBankAccount();
                            PipelineClaimInfo vehiclePersonOrgClaimInfo = new PipelineClaimInfo();
                           




                           


                            MDA.Common.Enum.PolicyPaymentType vehiclePersonOrgPaymentType = new PolicyPaymentType();
                            vehiclePersonOrgBankAccount.AccountNumber = personDataRow.Field<string>("Account Number");
                            vehiclePersonOrgBankAccount.SortCode = personDataRow.Field<string>("Account Sort Code");
                            vehiclePersonOrgBankAccount.BankName = personDataRow.Field<string>("Bank Name");

                            string datePaymentDetailsTakenTempDate =
                                personDataRow.Field<string>("Date Payment Details Taken");
                            if (!string.IsNullOrEmpty(datePaymentDetailsTakenTempDate))
                            {
                                vehiclePersonOrgBankAccount.Po2Ba_LinkData.DatePaymentDetailsTaken =
                                    Convert.ToDateTime(datePaymentDetailsTakenTempDate);
                            }
                        

                            switch (personDataRow.Field<string>("Payment Type"))
                            {
                                 case "Direct Debit":
                                    vehiclePersonOrgPaymentType = MDA.Common.Enum.PolicyPaymentType.DirectDebit;
                                  break;   
                                default:
                                    vehiclePersonOrgPaymentType = MDA.Common.Enum.PolicyPaymentType.Unknown;
                                    break;
                            }

                            vehiclePersonOrgBankAccount.Po2Ba_LinkData.PolicyPaymentType_Id = (int)vehiclePersonOrgPaymentType;
                            
                            vehiclePersonOrganisation.BankAccounts.Add( vehiclePersonOrgBankAccount );

                            vehiclePersonOrgClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                            vehiclePersonOrgClaimInfo.ClaimCode = personDataRow.Field<string>("Claim Code");
                            vehiclePersonOrgClaimInfo.IncidentLocation = personDataRow.Field<string>("Incident Location");
                            vehiclePersonOrgClaimInfo.IncidentCircumstances = personDataRow.Field<string>("Incident Circumstances");
                            vehiclePersonOrgClaimInfo.PoliceAttended = personDataRow.Field<string>("Police Force Attended") == "Yes";
                            vehiclePersonOrgClaimInfo.PoliceForce = personDataRow.Field<string>("Police Force");
                            vehiclePersonOrgClaimInfo.AmbulanceAttended = personDataRow.Field<string>("Ambulance Attended Scene") == "Yes";
                            //vehiclePersonOrgClaimInfo.MojStatus = MojStatus.Unknown;
                            string dateClaimNotificationDate = personDataRow.Field<string>("Claim Notification Date");
                            if (!string.IsNullOrEmpty(dateClaimNotificationDate))
                            {
                                vehiclePersonOrgClaimInfo.ClaimNotificationDate =
                                    Convert.ToDateTime(dateClaimNotificationDate);
                            }
                            decimal tempPaymentsToDate = Convert.ToDecimal(personDataRow.Field<Double>("Payments To Date"));
                            vehiclePersonOrgClaimInfo.PaymentsToDate = tempPaymentsToDate;
                            decimal tempReserve = Convert.ToDecimal(personDataRow.Field<Double>("Reserve"));
                            vehiclePersonOrgClaimInfo.Reserve = tempReserve;

                            //??vehiclePersonOrganisation.ClaimInfo = vehiclePersonOrgClaimInfo;

                            MDA.Common.Enum.OrganisationType vehiclePersonOrgType = new OrganisationType();

                            switch (personDataRow.Field<string>("Organisation Type"))
                            {
                                case "AMC":
                                    vehiclePersonOrgType = MDA.Common.Enum.OrganisationType.AccidentManagement;
                                    break;
                            }

                            vehiclePersonOrganisation.OrganisationName = personDataRow.Field<string>("Organisation Name");
                            vehiclePersonOrganisation.OrganisationType_Id = (int)vehiclePersonOrgType;
                            vehiclePersonOrganisation.EmailAddresses.Add( new PipelineEmail() { EmailAddress = personDataRow.Field<string>("Email") });
                            vehiclePersonOrganisation.WebSites.Add( new PipelineWebSite() { URL = personDataRow.Field<string>("Website") });
                            vehiclePersonOrganisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = personDataRow.Field<string>("Telephone 1"), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                            vehiclePersonOrganisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = personDataRow.Field<string>("Telephone 2"), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                            vehiclePersonOrganisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = personDataRow.Field<string>("Telephone 3"), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                            //vehiclePersonOrganisation.Telephone1 = personDataRow.Field<string>("Telephone 2");
                            //vehiclePersonOrganisation.Telephone1 = personDataRow.Field<string>("Telephone 3");
                            vehiclePersonOrganisation.VatNumber = personDataRow.Field<string>("VAT Number");
                            vehiclePersonOrganisation.RegisteredNumber = personDataRow.Field<string>("Registered Number");
                            vehiclePersonOrganisation.MojCrmNumber = personDataRow.Field<string>("MOJ CRM Number");

                            person.Organisations.Add(vehiclePersonOrganisation);

     #region Add Vehicle Person Organisation ClaimInfo
     #endregion

     #region Add Vehicle Person Organisation Addresses
     #endregion

     #region Add Vehicle Person Organisation Vehicles
     #endregion

   #endregion

                        }
                        vehicle.People.Add(person);
                    }
                    claim.Vehicles.Add(vehicle);
                }
    #endregion
                batch.Claims.Add(claim);
   #endregion
            }
            
            FileStream writer = null;
            string strXmlFileSave = @"C:\Dev\Projects\MDA\MDASolution\MDA.Mapping.Test\Xml\Test.xml";
           
            writer = new FileStream(strXmlFileSave, FileMode.Create);
            DataContractSerializer ser = new DataContractSerializer(typeof(PipelineClaimBatch));

            ser.WriteObject(writer, batch);

            return batch;
        }
    }
}
