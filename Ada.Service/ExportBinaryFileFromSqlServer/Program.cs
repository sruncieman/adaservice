﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportBinaryFileFromSqlServer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection sqlconnection = new SqlConnection(@"Data Source=intelprod-db1;Initial Catalog=MDA; Integrated Security=SSPI;"))

            {
                sqlconnection.Open();

                int ID = 1332;

                string selectQuery = string.Format(@"Select [FileData] From [MDA].[dbo].[RiskOriginalFile] Where pkid={0}"
                                    , ID);

                // Read File content from Sql Table 
                SqlCommand selectCommand = new SqlCommand(selectQuery, sqlconnection);
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read())
                {
                    byte[] fileData = (byte[])reader[0];
                    // Write/Export File content into new file
                    File.WriteAllBytes(@"D:\New folder (2)\New_Sample.xml", fileData);
                }
            }
        }
    }
}
