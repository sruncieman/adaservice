﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Pipeline.Model;
using MDA.Common;
using MDA.Common.Server;

namespace MDA.MappingService.Interface
{
    public interface IMappingService
    {
        /// <summary>
        /// Convert a customer file format into Pipeline Entities and call the CallBack function with each claim 
        /// as it is assembled. The Callback has the signature (ctx, pipelineClaim, ShowProgressCallback) and expects
        /// the pipelineClaim to be fully populated with vehicles, people etc
        /// </summary>
        /// <param name="ctx">Current Context containing DB connection to use</param>
        /// <param name="fs">FileStream containing the file to be loaded</param>
        /// <param name="statusTracking"></param>
        /// <param name="processClaim">CallBack function to call for each PipelineClaim created</param>
        /// <param name="ShowProgress">Show Progress callback</param>
        /// <param name="processingResults">Structure to hold ALL processing results</param>
        void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking, 
                                                Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> processClaim,
                                                out ProcessingResults processingResults);

        /// <summary>
        /// To implement this method you must
        /// + Use the folderPath and wildCard to check if there are any files to process. You will need to rename files AFTER processing.
        /// + Read the files(s) and process it/them into a stream or multiple streams. A STREAM holds a BATCH
        /// + Set the filename, mimeType, batchRef and Stream variables to some meaningful value
        /// + Make the Call to the processClientBatch() method ONCE PER STREAM
        /// + Lastly rename the files you processed to ensure you don't pick them up again
        /// </summary>
        /// <param name="ctx">Current Context</param>
        /// <param name="folderPath">Folder to check</param>
        /// <param name="wildCard">Filename wildcard filter</param>
        /// <param name="settleTime">Time to wait for file to settle</param>
        /// <param name="batchSize">MAximum  number of claim to put in batch</param>
        /// <param name="postOp">What do do with file after its read</param>
        /// <param name="archiveFolder">Destination folder if archiving files</param>
        /// <param name="processClientBatch">Callback to process batch</param>
        void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard, int settleTime, int batchSize, string postOp, string archiveFolder, 
                                                Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch);
    }

    public interface IMapper
    {
        void AssignFiles();

        void InitialiseFileHelperEngines();

        void PopulateFileHelperEngines();

        void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder);

        void RetrieveDistinctClaims();

        void Translate();
    }
}
