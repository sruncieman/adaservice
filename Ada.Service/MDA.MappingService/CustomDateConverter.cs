﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService
{
    public class CustomDateConverter : ConverterBase
    {
        public override object StringToField(string value)
        {
            DateTime dt;

            value = value.Replace("00:00:00", null);
            value = value.TrimEnd();

            if (DateTime.TryParseExact(value.Replace("-", ""), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
                return dt;

            return null;
        }
    }
}
