﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Helpers
{
    public static class TelephoneHelper
    {
        public static bool IsValid(string telephoneNumber)
        {
            bool result = true;

            if (string.IsNullOrEmpty(telephoneNumber) || IgnoredValuesHelper.IsIgnored(telephoneNumber))
                return false;

            return result;
        }
    }
}
