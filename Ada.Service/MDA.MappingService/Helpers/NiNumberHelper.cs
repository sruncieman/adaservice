﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MDA.MappingService.Helpers
{
    public static class NiNumberHelper
    {
        /// <summary>
        /// Uses regex to check the Ni Number provided is correct, uses the pattern provided
        /// by https://stackoverflow.com/questions/10204378/regular-expression-to-validate-uk-national-insurance-number
        /// will be extend shortly to try and trim/clean the input up before checking to improve chances
        /// of the data being saved.
        /// </summary>
        /// <param name="inputNiNumber"></param>
        /// <returns></returns>
        public static string ReturnValueOrNull(string niNumber)
        {
            string result = null;
            
            if (niNumber == null)
                return result;

            niNumber = niNumber.Replace(" ", "");

            Regex regex = new Regex(@"^(?!BG)(?!GB)(?!NK)(?!KN)(?!TN)(?!NT)(?!ZZ)(?:[A-CEGHJ-PR-TW-Z][A-CEGHJ-NPR-TW-Z])(?:\s*\d\s*){6}([A-D]|\s)$", RegexOptions.IgnoreCase);
            if (regex.IsMatch(niNumber))
                return result;

            if (!string.IsNullOrEmpty(result))
                result = result.ToString();

            return result;
        }

        public static bool IsValid(string niNumber)
        {
            if (NiNumberHelper.ReturnValueOrNull(niNumber) == null || IgnoredValuesHelper.IsIgnored(niNumber))
                return false;

            return true;
        }
    }
}
