﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Helpers
{
    public static class EmailHelper
    {
        public static string ReturnValueOrNull(string email)
        {
            string result = null;

            if (email.Contains("@"))
                return email;

            return result;
        }

        public static bool IsValid(string email)
        {
            if (EmailHelper.ReturnValueOrNull(email) == null || IgnoredValuesHelper.IsIgnored(email))
                return false;

            return true;
        }
    }
}
