﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Helpers
{
    public static class IgnoredValuesHelper
    {
        public static bool IsIgnored(string value)
        {
            bool result = false;

            value = value.ToLower();

            if (value == "n/a" || value == "na" || value == "empty" || value == "unknown")
                return true;

            return result;
        }
    }
}
