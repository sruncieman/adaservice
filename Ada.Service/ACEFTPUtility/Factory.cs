﻿using ACEFTPUtility.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACEFTPUtility
{
    public static class Factory
    {
        public static BaseClientFTP Get(string client)
        {
            string clientName;

            clientName = client.Replace("-", "");

            switch (clientName)
            {
                case "ACE":
                    return new AceFTP(clientName);
                default:
                    return null;
            }
        }
    }
}
