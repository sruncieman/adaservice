﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinSCP;

namespace ACEFTPUtility.Clients
{
    public class AceFTP : BaseClientFTP
    {
        public AceFTP(string client)
            : base(client)
        {


        }

        //public override void DownloadNewFiles(WinSCP.Session session)
        //{
        //    try
        //    {
        //        TransferOptions transferOptions = new TransferOptions();
        //        transferOptions.TransferMode = TransferMode.Binary;
        //        session.FileTransferred += FilesTransferred;
        //        SynchronizationResult synchronizationResult;
        //        synchronizationResult = session.SynchronizeDirectories(SynchronizationMode.Local, this.DestinationPath, this.StrRemoteDirectory, false, false, SynchronizationCriteria.Time, transferOptions);

        //        foreach (var download in synchronizationResult.Downloads.AsQueryable())
        //        {
        //            if (download.Error == null)
        //            {
        //                Console.WriteLine("Download of {0} succeeded, removing from source", download.FileName);
        //                var removalResult = session.RemoveFiles(session.EscapeFileMask(download.FileName));

        //                if (removalResult.IsSuccess)
        //                {
        //                    Console.WriteLine("Removing of file {0} succeeded", download.FileName);
        //                }
        //                else
        //                {
        //                    Console.WriteLine("Removing of file {0} failed: {1}", download.FileName, removalResult.Failures.First());
        //                }
        //            }

        //            else
        //            {
        //                Console.WriteLine("Download of {0} failed: {1}", download.FileName, download.Error.Message);
        //            }
        //        }

        //        DirectoryInfo dir = new DirectoryInfo(DestinationPath);
        //        int filesCount = Directory.GetFiles(DestinationPath).Count();

        //        int aceFileAmount = Convert.ToInt32(ConfigurationManager.AppSettings["AceFilesAmount"]);

        //        if (filesCount == aceFileAmount)
        //        {
        //            using (ZipFile zip = new ZipFile())
        //            {

        //                foreach (string file in Directory.GetFiles(DestinationPath))
        //                {
        //                    zip.AddFile(file, @"\");
        //                }

        //                if (zip.Any())
        //                {

        //                    string zipPath = DestinationPath + @"\ACE" + DateTime.Now.ToString("ddMMyyyy") + ".zip";

        //                    zip.Save(zipPath);
        //                }
        //            }

        //            //Clean up files
        //            foreach (string file in Directory.GetFiles(DestinationPath))
        //            {
        //                string temp = file.Substring(file.Length - 4);
        //                if (temp.ToUpper() != ".ZIP")
        //                {
        //                    File.Delete(file);
        //                }
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error: {0}", ex);
        //    }
        //}

        //public override void FilesTransferred(object sender, TransferEventArgs e)
        //{
        //    base.FilesTransferred(sender, e);
        //}


    }
}