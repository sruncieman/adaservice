﻿using ACEFTPUtility;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WinSCP;

namespace ADAFTPUtility
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {

                //Thread.Sleep(10000);

                var sessionOptions = BaseClientFTP.ConfigureSetupOptions();

                using (Session session = new Session())
                {
                    session.SessionLogPath = ConfigurationManager.AppSettings["LogFileLocation"] + "Log.txt";
                    session.Open(sessionOptions);

                    //var ftpClient = Factory.Get(args[0]);

                    var ftpClient = Factory.Get("-ACE");

                    ftpClient.DownloadNewFiles(session);

                    //BaseClientFTP.SendEmail(true, null, ftpClient.FtpClient);
                    if (ConfigurationManager.AppSettings["DeleteArchiveFiles"] == "1")
                        ftpClient.DeleteArchiveFiles(ftpClient.ClientMonitoredArchiveFolder);
                }
            }

            catch (Exception ex)
            {
                //BaseClientFTP.SendEmail(false, ex, args[0].Replace("-",""));
            }
        }
    }
}
