﻿using ADAServices.Interface;

namespace ADAServices.Factory
{
    public class ADAServicesFactory
    {
        public static IADAServices CreateADAServices()
        {
            return new ADAServices.Impl.ADAServices();

            //return new ADAServices.Mock.ADAServices();
        }
    }
}
