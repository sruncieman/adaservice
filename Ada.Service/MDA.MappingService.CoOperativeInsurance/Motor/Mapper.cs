﻿using FileHelpers;
using MDA.Common.Server;
using MDA.MappingService.CoOperativeInsurance.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using System.Text.RegularExpressions;

namespace MDA.MappingService.CoOperativeInsurance.Motor
{
    public class Mapper
    {
        private Stream _fs;
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        private object statusTracking;
        private static bool _boolDebug = true;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        public bool BoolDebug { get; set; }
        public string FilePath { get; set; }
        public string FolderPath { get; set; }
        public string ClaimFile { get; set; }
        public string PolicyFile { get; set; }
        public FileHelperEngine ClaimEngine { get; set; }

        private string _coreClaimDataFile;
        private bool _debug;
        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private List<string> _uniqueClaimNumberList;
        private string _claimCode;
        private IEnumerable<ClaimData> _claimItemRows;
        private string _filePath = null;
        private bool _deleteExtractedFiles;
        private int _claimCount = 0;
        private string _clientFolder;
        private StreamReader _claimDataFileStreamReader;


        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking)
        {
            this._fs = fs;
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _uniqueClaimNumberList = new List<string>();
        }

        public void AssignFiles()
        {

            if (_fs == null)
            {
                FolderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Tradex\Data";
                ClaimFile = FolderPath + @"\Copy of Keoghs Data Wash13-05-15-44319.csv";
            }
            else
            {
                try
                {
                    _claimDataFileStreamReader = new StreamReader(_fs);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in creating csv file: " + ex);
                }

            }


        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {

                if (_fs == null)
                {
                    _coreClaimData = _coreClaimDataEngine.ReadFile(ClaimFile) as ClaimData[];
                }
                else
                {
                    _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
                }


            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        private void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            try
            {
                if (_debug)
                {
                    Console.WriteLine("Start");
                }

                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Populate File Helper Engine (core) : ", false);
                }

                _coreClaimData = _coreClaimDataEngine.ReadFile(_coreClaimDataFile) as ClaimData[];

            }

            catch (Exception ex)
            {
                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "PopulateFileHelpersEngines Error : " + ex.Message, true);
                }

                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers 
                _uniqueClaimNumberList = _coreClaimData.Where(x => x.ClaimNumber != "").Select(x => x.ClaimNumber).Distinct().ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }

        }
        
        public void Translate()
        {
            Translate translate = new Translate();

            foreach (var claim in _uniqueClaimNumberList)
            {
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();                              

                var uniqueClaim = _coreClaimData.Where(x => x.ClaimNumber == claim).ToList();
                
                if (uniqueClaim != null)
                {
                    ClaimData claimInfo = uniqueClaim.FirstOrDefault();

                    motorClaim = translate.TranslateClaimInfo(motorClaim, claimInfo);

                    List<PipelinePerson> noneVehiclePeople = new List<PipelinePerson>(); // Where no vehicle is present all people should be placed within a single ‘No Vehicle Involved’ entity.

                    foreach (var c in uniqueClaim)
                    {
                        PipelineVehicle vehicle = translate.TranslateVehicle(c);

                        PipelinePerson person = translate.TranslatePerson(c);

                        PipelinePerson blankPerson = new PipelinePerson(); // Add blank berson if address/telephone number provided but no person details provided
                        blankPerson.I2Pe_LinkData.PartyType_Id = (int)translate.PartyTypeTranslation(claimInfo.Policyholder_Claimant);
                        blankPerson.I2Pe_LinkData.SubPartyType_Id = (int)translate.SubPartyTypeTranslation(claimInfo.Policyholder_Claimant);
                        bool blankPersonDetailsProvided = false;
                              
                        var address = translate.TranslateAddress(c);

                        if (address != null)
                        {
                            if (person != null)
                            {
                                person.Addresses.Add(address); 
                            }
                            else
                            {
                                blankPersonDetailsProvided = true;
                                blankPerson.Addresses.Add(address);
                            }                            
                        }

                        PipelineTelephone workTelephone = translate.TranslateWorkTelephone(c);

                        if (workTelephone != null)
                        {
                            if (person != null)
                            {
                                person.Telephones.Add(workTelephone);                                
                            }
                            else
                            {
                                blankPersonDetailsProvided = true;
                                blankPerson.Telephones.Add(workTelephone);
                            }                             
                        }

                        PipelineTelephone homeTelephone = translate.TranslateHomeTelephone(c);

                        if (homeTelephone != null)
                        {
                            if (person != null)
                            {
                                person.Telephones.Add(homeTelephone);                             
                            }
                            else
                            {
                                blankPersonDetailsProvided = true;
                                blankPerson.Telephones.Add(homeTelephone);
                            }                              
                        }

                        if (vehicle != null)
                        {
                            if (blankPersonDetailsProvided)
                            {
                                vehicle.People.Add(blankPerson);                               
                            }
                            else if (person != null)
                            {
                                vehicle.People.Add(person);
                            }
                            
                            motorClaim.Vehicles.Add(vehicle);
                            
                        }
                        else
	                    {
                            if (blankPersonDetailsProvided)
                            {
                                noneVehiclePeople.Add(blankPerson);                             
                            }
                            else if(person != null)
                            {
                                noneVehiclePeople.Add(person);                               
                            }
	                    }                        
                            
                    }                         
                    
                    if (noneVehiclePeople.Any())
                    {
                        PipelineVehicle noneVehicle = new PipelineVehicle();
                        noneVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;

                        foreach (PipelinePerson noneVehivlePerson in noneVehiclePeople)
                        {
                            noneVehicle.People.Add(noneVehivlePerson);                                                       
                        }

                        motorClaim.Vehicles.Add(noneVehicle);
                      
                    }

                    #region Insured Vehicle and Driver Check

                    bool insuredVehicle = false;
                    bool insuredDriver = false;

                    foreach (var vehicle in motorClaim.Vehicles)
                    {
                        if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                        {
                            insuredVehicle = true;

                        }

                        foreach (var person in vehicle.People)
                        {
                            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                            {
                                insuredDriver = true;
                            }
                        }
                    }

                    if (insuredVehicle == false)
                    {
                        PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                        defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                        }

                        motorClaim.Vehicles.Add(defaultInsuredVehicle);
                    }
                    else
                    {
                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            foreach (var vehicle in motorClaim.Vehicles)
                            {
                                if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                {

                                    vehicle.People.Add(defaultInsuredDriver);

                                }

                            }

                        }
                    }

                    #endregion

                    if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;

                }
            }
        }
       
    }

    public class Translate
    {
        private bool AddressPopulated(string AddressLine1, string AddressLine2, string AddressLine3, string AddressLine4, string AddressLine5, string PostCode)
        {

            if (!string.IsNullOrWhiteSpace(AddressLine1))
            {
                return true;
            }
            else if (!string.IsNullOrWhiteSpace(AddressLine2))
            {
                return true;
            }
            else if (!string.IsNullOrWhiteSpace(AddressLine3))
            {
                return true;
            }
            else if (!string.IsNullOrWhiteSpace(AddressLine4))
            {
                return true;
            }
            else if (!string.IsNullOrWhiteSpace(AddressLine5))
            {
                return true;
            }
            else if (!string.IsNullOrWhiteSpace(PostCode))
            {
                return true;
            }

            return false;
        }

        public PartyType PartyTypeTranslation(string clientSuppliedPartyType)
        {        
            if (clientSuppliedPartyType == "P")
            {
                return PartyType.Insured;   
            }
            else if (clientSuppliedPartyType == "C")
            {
                return PartyType.Claimant;   
            }

            return PartyType.Unknown;
        }

        public SubPartyType SubPartyTypeTranslation(string clientSuppliedPartyType)
        {
            if (clientSuppliedPartyType == "P")
            {
                return SubPartyType.Driver;
            }

            return SubPartyType.Unknown;
        }

        private ClaimStatus ClaimStatusTranslation(string claimStatus)
        {
            if (claimStatus.ToUpper() == "O")
            {
               return ClaimStatus.Open;
            }
            else if (claimStatus.ToUpper() == "C")
            {
                return ClaimStatus.Settled;
            }

            return ClaimStatus.Unknown;
        }

        private Incident2VehicleLinkType VehicleLinkTypeTranslation(string clientSuppliedPartyType)
        {
            if (clientSuppliedPartyType == "P")
            {
                return Incident2VehicleLinkType.InsuredVehicle;
            }

            return Incident2VehicleLinkType.ThirdPartyVehicle;
        }

        public PipelineMotorClaim TranslateClaimInfo(PipelineMotorClaim motorClaimInfo, ClaimData claimInfo)
        {

            try
            {
                motorClaimInfo.ClaimType_Id = (int)ClaimType.Motor;
                motorClaimInfo.ClaimNumber = claimInfo.ClaimNumber;
                motorClaimInfo.IncidentDate = Convert.ToDateTime(claimInfo.IncidentDate);
                motorClaimInfo.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatusTranslation(claimInfo.ClaimStatus);
                motorClaimInfo.ExtraClaimInfo.ClaimNotificationDate = claimInfo.NotificationDate;
                motorClaimInfo.ExtraClaimInfo.Reserve = Convert.ToDecimal(claimInfo.ClaimReserve);
                motorClaimInfo.ExtraClaimInfo.PaymentsToDate = Convert.ToDecimal(claimInfo.ClaimPayments);

                motorClaimInfo.Policy.Insurer = "The Co-operative Insurance";
                motorClaimInfo.Policy.PolicyNumber = claimInfo.PolicyId;
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in mapping claim information: " + ex);
            }

            return motorClaimInfo;

        }

        public PipelineVehicle TranslateVehicle(ClaimData claimInfo)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(claimInfo.VehicleReg))
                {
                    PipelineVehicle vehicle = new PipelineVehicle();
                    vehicle.VehicleRegistration = claimInfo.VehicleReg;
                    vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)VehicleLinkTypeTranslation(claimInfo.Policyholder_Claimant);
                    return vehicle;
                }
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in mapping vehicle information: " + ex);
            }

            return null;
            
        }

        public PipelinePerson TranslatePerson(ClaimData claimInfo)
        {
            bool populated = false;
            
            PipelinePerson person = new PipelinePerson();

            try
            {
                person.I2Pe_LinkData.PartyType_Id = (int)PartyTypeTranslation(claimInfo.Policyholder_Claimant);
                person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyTypeTranslation(claimInfo.Policyholder_Claimant);

                if (!string.IsNullOrWhiteSpace(claimInfo.Forename))
                {
                    person.FirstName = claimInfo.Forename;
                    populated = true;
                }

                if (!string.IsNullOrWhiteSpace(claimInfo.Surname))
                {
                    person.LastName = claimInfo.Surname;
                    populated = true;
                }

                person.DateOfBirth = Convert.ToDateTime(claimInfo.PartyDOB);

            }
            catch (Exception ex)
            {
                throw new CustomException("Error in mapping person information: " + ex);
            }

            if (populated)
            {
                return person;
            }
            else
            {
                return null;
            }
            
        }

        public PipelineAddress TranslateAddress(ClaimData claimInfo)
        {

            try
            {
                if (AddressPopulated(claimInfo.AddressLine1, claimInfo.AddressLine2, claimInfo.AddressLine3, claimInfo.AddressLine4, claimInfo.AddressLine5, claimInfo.Postcode))
                {
                    PipelineAddress address = new PipelineAddress();

                    address.Building = claimInfo.AddressLine1;
                    address.Street = claimInfo.AddressLine2;
                    address.Locality = claimInfo.AddressLine3;
                    address.Town = claimInfo.AddressLine4 + " " + claimInfo.AddressLine5;
                    address.PostCode = claimInfo.Postcode;

                    return address;
                }
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in mapping address information: " + ex);
            }

            return null;
            
        }

        public PipelineTelephone TranslateWorkTelephone(ClaimData claimInfo)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(claimInfo.WorkTel))
                {
                    PipelineTelephone telephone = new PipelineTelephone();

                    telephone.ClientSuppliedNumber = claimInfo.WorkTel;

                    return telephone;
                }
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in mapping telephone information: " + ex);
            }

            return null;
        }

        public PipelineTelephone TranslateHomeTelephone(ClaimData claimInfo)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(claimInfo.HomeTel))
                {
                    PipelineTelephone telephone = new PipelineTelephone();

                    telephone.ClientSuppliedNumber = claimInfo.HomeTel;

                    return telephone;
                }
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in mapping telephone information: " + ex);
            }

            return null;
        }

    }


}
