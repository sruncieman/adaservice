﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AdminConsole.Controls.PipelineService;

namespace AdminConsole.Controls
{
    public partial class ClientInterface : Form
    {
        private ExRiskClaim[] currentClaims;
        private ExBatchDetails[] currentBatches;

        public ClientInterface()
        {
            InitializeComponent();

            try
            {
                PipelineService.ADAServiceClient p = new PipelineService.ADAServiceClient();

                var clients = p.GetRiskClients(new RiskClientsRequest() { ClientId = 0, UserId = 0, Who = "admin", FilterClientId = -1 });
                foreach (var c in clients.ClientList)
                {
                    comboBox1.Items.Add(new ComboItem() { Name = c.ClientName, Id = c.Id });
                }

                comboBox1.SelectedIndex = 0;

                currentBatches = p.GetClientBatchList(new BatchListRequest() { ClientId = 0, StatusFilter = 1 }).BatchList;
            }
            catch (Exception)
            {
                currentBatches = null;
            }

            batchListBindingSource.DataSource = currentBatches;
        }

        public ClientInterface(int clientId)
        {
            InitializeComponent();

            try
            {
                PipelineService.ADAServiceClient p = new PipelineService.ADAServiceClient();

                var clients = p.GetRiskClients(new RiskClientsRequest() { ClientId = 0, UserId = 0, Who = "admin", FilterClientId = -1 });
                foreach (var c in clients.ClientList)
                {
                    comboBox1.Items.Add(new ComboItem() { Name = c.ClientName, Id = c.Id });
                }

                currentBatches = p.GetClientBatchList(new BatchListRequest() { ClientId = clientId, StatusFilter = 1 }).BatchList;
            }
            catch(Exception)
            {
                currentBatches = null;
            }

            batchListBindingSource.DataSource = currentBatches;
        }

        private int _currentBatchId = -1;

        private void batchListBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            if (batchListBindingSource.Current == null) return;

            ExBatchDetails bd = (ExBatchDetails)batchListBindingSource.Current;

            if (bd.BatchId == _currentBatchId)
            {
                claimsListBindingSource.DataSource = currentClaims;
                return;
            }
            _currentBatchId = bd.BatchId;

            try
            {
                PipelineService.ADAServiceClient p = new PipelineService.ADAServiceClient();

                currentClaims = p.GetClaimsInBatch(new ClaimsInBatchRequest() { BatchId = bd.BatchId }).ClaimsList;

                claimsListBindingSource.DataSource = currentClaims;

                var pr = p.GetBatchProcessingResults(new GetBatchProcessingResultsRequest() { ClientId = 0, UserId = 0, Who = "Console", RiskBatchId = bd.BatchId });

                tvBatch.Nodes.Clear();
                tvBatch.Nodes.Add(AssignMyNodeToTreeNode(pr.ProcessingResults));
                tvBatch.ExpandAll();
            }
            catch(Exception)
            {
                tvBatch.Nodes.Clear();

                currentClaims = null;
                claimsListBindingSource.DataSource = currentClaims;
            }


        }

        private TreeNode AssignMyNodeToTreeNode( MessageNode root )
        {
            TreeNode node = new TreeNode(root.Text);

            foreach (MessageNode n in root.Nodes)
                node.Nodes.Add(AssignMyNodeToTreeNode(n));

            return node;
        }

        private void claimsListBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            PipelineService.ADAServiceClient p = new PipelineService.ADAServiceClient();

            var cd = (ExRiskClaim)claimsListBindingSource.Current;

            if (cd != null)
            {
                try
                {
                    var aa = p.GetClaimScoreData(new GetClaimScoreDataRequest() { RiskClaimId = cd.Id });

                    propertyGrid1.SelectedObject = aa.RiskClaimRun;

                    //richTextBox1.Text = aa.RiskClaimRun.ScoreEntityXml;

                    treeView1.Nodes.Clear();

                    if (aa.RiskClaimRun != null)
                    {
                        ExRiskClaimRun rcr = aa.RiskClaimRun;

                        treeView1.Nodes.Add(AssignMyNodeToTreeNode(aa.ScoreNodes));

                        treeView1.Nodes[0].ExpandAll();
                    }
                }
                catch(Exception)
                {
                    propertyGrid1.SelectedObject = null;
                    treeView1.Nodes.Clear();
                }
            }
            else
            {
                propertyGrid1.SelectedObject = null;

                //richTextBox1.Text = "";

                treeView1.Nodes.Clear();
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PipelineService.ADAServiceClient p = new PipelineService.ADAServiceClient();

                var x = comboBox1.SelectedItem as ComboItem;

                currentBatches = p.GetClientBatchList(new BatchListRequest() { ClientId = x.Id, StatusFilter = 1 }).BatchList;

                _currentBatchId = -1;

                claimsListBindingSource.DataSource = null;


            }
            catch(Exception)
            {
                currentBatches = null;
            }
            tvBatch.Nodes.Clear();
            treeView1.Nodes.Clear();
            propertyGrid1.SelectedObject = null;

            batchListBindingSource.DataSource = currentBatches;
        }
    }

    public class ComboItem
    {
        public string Name { get; set; }
        public int Id { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
