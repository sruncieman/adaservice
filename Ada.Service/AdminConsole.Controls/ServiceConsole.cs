﻿using AdminConsole.Controls.PipelineService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminConsole.Controls
{
    public partial class ServiceControl : BaseAdminControl, IAdminControl
    {
        public RemoteConfigSettings settings = null;
        public RemoteRunningMetrics metrics = new RemoteRunningMetrics();

        public ServiceControl()
        {
            InitializeComponent();

            if (settings != null)
                this.bindingSource1.DataSource = settings;

            this.remoteRunningMetricsBindingSource.DataSource = metrics;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PipelineService.ADAServiceClient proxy = new PipelineService.ADAServiceClient();
                
                if (!settings.ServiceRunning)
                    settings = proxy.StartService(null);
                else
                    settings = proxy.SuspendService();

                this.bindingSource1.DataSource = settings;
                grpConfig.Enabled = true;

                button1.Text = (settings.ServiceRunning) ? "Running" : "Stopped";
                button1.BackColor = (settings.ServiceRunning) ? Color.Green : Color.Red;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                PipelineService.ADAServiceClient proxy = new PipelineService.ADAServiceClient();

                textBox1.Text = proxy.Endpoint.Address.Uri.Host;

                settings = proxy.SetConfigParameters(null);

                button1.Text = (settings.ServiceRunning) ? "Running" : "Stopped";
                button1.BackColor = (settings.ServiceRunning) ? Color.Green : Color.Red;

                this.bindingSource1.DataSource = settings;
                grpConfig.Enabled = true;

                timer1.Start();
            }
            catch(Exception)
            { }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                PipelineService.ADAServiceClient proxy = new PipelineService.ADAServiceClient();

                settings = proxy.SetConfigParameters(settings);

                button1.Text = (settings.ServiceRunning) ? "Running" : "Stopped";
                button1.BackColor = (settings.ServiceRunning) ? Color.Green : Color.Red;

                this.bindingSource1.DataSource = settings;
                grpConfig.Enabled = true;

            }
            catch(Exception)
            { }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                PipelineService.ADAServiceClient proxy = new PipelineService.ADAServiceClient();

                metrics = proxy.GetRunningMetrics();

                remoteRunningMetricsBindingSource.DataSource = metrics;
            }
            catch (Exception)
            { }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ClientInterface f = new ClientInterface();

            f.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                PipelineService.ADAServiceClient proxy = new PipelineService.ADAServiceClient();

                settings = proxy.SetConfigParameters(settings);

                button1.Text = (settings.ServiceRunning) ? "Running" : "Stopped";
                button1.BackColor = (settings.ServiceRunning) ? Color.Green : Color.Red;

                this.bindingSource1.DataSource = settings;
                grpConfig.Enabled = true;

                metrics = proxy.GetRunningMetrics();

                remoteRunningMetricsBindingSource.DataSource = metrics;
            }
            catch(Exception)
            { }
        }
    }
}
