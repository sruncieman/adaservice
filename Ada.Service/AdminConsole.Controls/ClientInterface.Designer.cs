﻿
namespace AdminConsole.Controls
{
    partial class ClientInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.batchListDataGridView = new System.Windows.Forms.DataGridView();
            this.batchIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.batchStatusIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.batchStatusValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loadingDurationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referenceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalClaimsInErrorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalClaimsLoadedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalClaimsReceivedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalClaimsSkippedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalModifiedClaimsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalNewClaimsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uploadedByDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uploadedDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.batchListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.claimsListDataGridView = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.claimsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tvBatch = new System.Windows.Forms.TreeView();
            ((System.ComponentModel.ISupportInitialize)(this.batchListDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimsListDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimsListBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // batchListDataGridView
            // 
            this.batchListDataGridView.AllowUserToAddRows = false;
            this.batchListDataGridView.AllowUserToDeleteRows = false;
            this.batchListDataGridView.AutoGenerateColumns = false;
            this.batchListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.batchListDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.batchIdDataGridViewTextBoxColumn,
            this.batchStatusIdDataGridViewTextBoxColumn,
            this.batchStatusValueDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.loadingDurationDataGridViewTextBoxColumn,
            this.referenceDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.totalClaimsInErrorDataGridViewTextBoxColumn,
            this.totalClaimsLoadedDataGridViewTextBoxColumn,
            this.totalClaimsReceivedDataGridViewTextBoxColumn,
            this.totalClaimsSkippedDataGridViewTextBoxColumn,
            this.totalModifiedClaimsDataGridViewTextBoxColumn,
            this.totalNewClaimsDataGridViewTextBoxColumn,
            this.uploadedByDataGridViewTextBoxColumn,
            this.uploadedDateDataGridViewTextBoxColumn});
            this.batchListDataGridView.DataSource = this.batchListBindingSource;
            this.batchListDataGridView.Location = new System.Drawing.Point(178, 12);
            this.batchListDataGridView.Name = "batchListDataGridView";
            this.batchListDataGridView.Size = new System.Drawing.Size(892, 133);
            this.batchListDataGridView.TabIndex = 1;
            // 
            // batchIdDataGridViewTextBoxColumn
            // 
            this.batchIdDataGridViewTextBoxColumn.DataPropertyName = "BatchId";
            this.batchIdDataGridViewTextBoxColumn.HeaderText = "BatchId";
            this.batchIdDataGridViewTextBoxColumn.Name = "batchIdDataGridViewTextBoxColumn";
            // 
            // batchStatusIdDataGridViewTextBoxColumn
            // 
            this.batchStatusIdDataGridViewTextBoxColumn.DataPropertyName = "BatchStatusId";
            this.batchStatusIdDataGridViewTextBoxColumn.HeaderText = "BatchStatusId";
            this.batchStatusIdDataGridViewTextBoxColumn.Name = "batchStatusIdDataGridViewTextBoxColumn";
            // 
            // batchStatusValueDataGridViewTextBoxColumn
            // 
            this.batchStatusValueDataGridViewTextBoxColumn.DataPropertyName = "BatchStatusValue";
            this.batchStatusValueDataGridViewTextBoxColumn.HeaderText = "BatchStatusValue";
            this.batchStatusValueDataGridViewTextBoxColumn.Name = "batchStatusValueDataGridViewTextBoxColumn";
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            // 
            // loadingDurationDataGridViewTextBoxColumn
            // 
            this.loadingDurationDataGridViewTextBoxColumn.DataPropertyName = "LoadingDuration";
            this.loadingDurationDataGridViewTextBoxColumn.HeaderText = "LoadingDuration";
            this.loadingDurationDataGridViewTextBoxColumn.Name = "loadingDurationDataGridViewTextBoxColumn";
            // 
            // referenceDataGridViewTextBoxColumn
            // 
            this.referenceDataGridViewTextBoxColumn.DataPropertyName = "Reference";
            this.referenceDataGridViewTextBoxColumn.HeaderText = "Reference";
            this.referenceDataGridViewTextBoxColumn.Name = "referenceDataGridViewTextBoxColumn";
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            // 
            // totalClaimsInErrorDataGridViewTextBoxColumn
            // 
            this.totalClaimsInErrorDataGridViewTextBoxColumn.DataPropertyName = "TotalClaimsInError";
            this.totalClaimsInErrorDataGridViewTextBoxColumn.HeaderText = "TotalClaimsInError";
            this.totalClaimsInErrorDataGridViewTextBoxColumn.Name = "totalClaimsInErrorDataGridViewTextBoxColumn";
            // 
            // totalClaimsLoadedDataGridViewTextBoxColumn
            // 
            this.totalClaimsLoadedDataGridViewTextBoxColumn.DataPropertyName = "TotalClaimsLoaded";
            this.totalClaimsLoadedDataGridViewTextBoxColumn.HeaderText = "TotalClaimsLoaded";
            this.totalClaimsLoadedDataGridViewTextBoxColumn.Name = "totalClaimsLoadedDataGridViewTextBoxColumn";
            // 
            // totalClaimsReceivedDataGridViewTextBoxColumn
            // 
            this.totalClaimsReceivedDataGridViewTextBoxColumn.DataPropertyName = "TotalClaimsReceived";
            this.totalClaimsReceivedDataGridViewTextBoxColumn.HeaderText = "TotalClaimsReceived";
            this.totalClaimsReceivedDataGridViewTextBoxColumn.Name = "totalClaimsReceivedDataGridViewTextBoxColumn";
            // 
            // totalClaimsSkippedDataGridViewTextBoxColumn
            // 
            this.totalClaimsSkippedDataGridViewTextBoxColumn.DataPropertyName = "TotalClaimsSkipped";
            this.totalClaimsSkippedDataGridViewTextBoxColumn.HeaderText = "TotalClaimsSkipped";
            this.totalClaimsSkippedDataGridViewTextBoxColumn.Name = "totalClaimsSkippedDataGridViewTextBoxColumn";
            // 
            // totalModifiedClaimsDataGridViewTextBoxColumn
            // 
            this.totalModifiedClaimsDataGridViewTextBoxColumn.DataPropertyName = "TotalModifiedClaims";
            this.totalModifiedClaimsDataGridViewTextBoxColumn.HeaderText = "TotalModifiedClaims";
            this.totalModifiedClaimsDataGridViewTextBoxColumn.Name = "totalModifiedClaimsDataGridViewTextBoxColumn";
            // 
            // totalNewClaimsDataGridViewTextBoxColumn
            // 
            this.totalNewClaimsDataGridViewTextBoxColumn.DataPropertyName = "TotalNewClaims";
            this.totalNewClaimsDataGridViewTextBoxColumn.HeaderText = "TotalNewClaims";
            this.totalNewClaimsDataGridViewTextBoxColumn.Name = "totalNewClaimsDataGridViewTextBoxColumn";
            // 
            // uploadedByDataGridViewTextBoxColumn
            // 
            this.uploadedByDataGridViewTextBoxColumn.DataPropertyName = "UploadedBy";
            this.uploadedByDataGridViewTextBoxColumn.HeaderText = "UploadedBy";
            this.uploadedByDataGridViewTextBoxColumn.Name = "uploadedByDataGridViewTextBoxColumn";
            // 
            // uploadedDateDataGridViewTextBoxColumn
            // 
            this.uploadedDateDataGridViewTextBoxColumn.DataPropertyName = "UploadedDate";
            this.uploadedDateDataGridViewTextBoxColumn.HeaderText = "UploadedDate";
            this.uploadedDateDataGridViewTextBoxColumn.Name = "uploadedDateDataGridViewTextBoxColumn";
            // 
            // batchListBindingSource
            // 
            this.batchListBindingSource.DataMember = "BatchList";
            this.batchListBindingSource.DataSource = typeof(AdminConsole.Controls.PipelineService.BatchListResponse);
            this.batchListBindingSource.CurrentChanged += new System.EventHandler(this.batchListBindingSource_CurrentChanged);
            // 
            // claimsListDataGridView
            // 
            this.claimsListDataGridView.AllowUserToAddRows = false;
            this.claimsListDataGridView.AllowUserToDeleteRows = false;
            this.claimsListDataGridView.AutoGenerateColumns = false;
            this.claimsListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.claimsListDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn14,
            this.TotalScore,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn15});
            this.claimsListDataGridView.DataSource = this.claimsListBindingSource;
            this.claimsListDataGridView.Location = new System.Drawing.Point(12, 151);
            this.claimsListDataGridView.Name = "claimsListDataGridView";
            this.claimsListDataGridView.Size = new System.Drawing.Size(1058, 230);
            this.claimsListDataGridView.TabIndex = 2;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "BaseRiskClaim_Id";
            this.dataGridViewTextBoxColumn11.HeaderText = "Base Claim Id";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "MdaClaimRef";
            this.dataGridViewTextBoxColumn10.HeaderText = "Mda Claim Ref";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "ClientClaimRefNumber";
            this.dataGridViewTextBoxColumn14.HeaderText = "Client Claim Ref";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // TotalScore
            // 
            this.TotalScore.DataPropertyName = "TotalScore";
            this.TotalScore.HeaderText = "Total Score";
            this.TotalScore.Name = "TotalScore";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "CreatedDate";
            this.dataGridViewTextBoxColumn12.HeaderText = "Created Date";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "CreatedBy";
            this.dataGridViewTextBoxColumn13.HeaderText = "Created By";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Incident_Id";
            this.dataGridViewTextBoxColumn15.HeaderText = "Incident Id";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // claimsListBindingSource
            // 
            this.claimsListBindingSource.DataSource = typeof(AdminConsole.Controls.PipelineService.ExRiskClaim);
            this.claimsListBindingSource.CurrentChanged += new System.EventHandler(this.claimsListBindingSource_CurrentChanged);
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(537, 387);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(533, 361);
            this.treeView1.TabIndex = 3;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.HelpVisible = false;
            this.propertyGrid1.Location = new System.Drawing.Point(250, 387);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.propertyGrid1.Size = new System.Drawing.Size(281, 361);
            this.propertyGrid1.TabIndex = 61;
            this.propertyGrid1.ToolbarVisible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(26, 33);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 62;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 63;
            this.label1.Text = "Client";
            // 
            // tvBatch
            // 
            this.tvBatch.Location = new System.Drawing.Point(12, 387);
            this.tvBatch.Name = "tvBatch";
            this.tvBatch.Size = new System.Drawing.Size(232, 361);
            this.tvBatch.TabIndex = 64;
            // 
            // ClientInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 760);
            this.Controls.Add(this.tvBatch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.propertyGrid1);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.claimsListDataGridView);
            this.Controls.Add(this.batchListDataGridView);
            this.Name = "ClientInterface";
            this.Text = "ClientInterface";
            ((System.ComponentModel.ISupportInitialize)(this.batchListDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimsListDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimsListBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource batchListBindingSource;
        private System.Windows.Forms.DataGridView batchListDataGridView;
        private System.Windows.Forms.BindingSource claimsListBindingSource;
        private System.Windows.Forms.DataGridView claimsListDataGridView;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn batchIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn batchStatusIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn batchStatusValueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn loadingDurationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn referenceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalClaimsInErrorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalClaimsLoadedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalClaimsReceivedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalClaimsSkippedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalModifiedClaimsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalNewClaimsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uploadedByDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uploadedDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.TreeView tvBatch;

    }
}