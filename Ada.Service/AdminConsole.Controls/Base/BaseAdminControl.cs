﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AdminConsole.Controls
{
	/// <summary>
	/// 
	/// </summary>
    public partial class BaseAdminControl : UserControl, IAdminControl
    {
		/// <summary>
		/// 
		/// </summary>
		/// <param name="priv"></param>
		/// <returns></returns>
        protected bool HasPrivilege(string priv)
        {
            string[] privs = ((string[])this.Tag);

            foreach (string s in privs)
            {
                if (string.Compare(s, priv.Trim(), true) == 0)
                    return true;
            }
            return false;
        }
    }
}
