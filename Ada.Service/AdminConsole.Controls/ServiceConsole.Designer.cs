﻿namespace AdminConsole.Controls
{
    partial class ServiceControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label serviceSleepTimeLabel;
            System.Windows.Forms.Label threadSleepTimeLabel;
            System.Windows.Forms.Label averageLoadingTimeLabel;
            System.Windows.Forms.Label averageScoringTimeLabel;
            System.Windows.Forms.Label batchesErrorLabel;
            System.Windows.Forms.Label batchesLoadingLabel;
            System.Windows.Forms.Label batchesTotalLabel;
            System.Windows.Forms.Label batchesWaitingLabel;
            System.Windows.Forms.Label claimsBeingScoredLabel;
            System.Windows.Forms.Label claimsWaitingOnExternalLabel;
            System.Windows.Forms.Label claimsWaitingToBeLoadedLabel;
            System.Windows.Forms.Label claimsWaitingToBeScoredLabel;
            System.Windows.Forms.Label externalErrorsLabel;
            System.Windows.Forms.Label externalRetriesRequiredLabel;
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.creatingThreadLimitTextBox = new System.Windows.Forms.TextBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.enableBackupsCheckBox = new System.Windows.Forms.CheckBox();
            this.enableDedupeCheckBox = new System.Windows.Forms.CheckBox();
            this.enableSyncCheckBox = new System.Windows.Forms.CheckBox();
            this.externalThreadLimitTextBox = new System.Windows.Forms.TextBox();
            this.loadingThreadLimitTextBox = new System.Windows.Forms.TextBox();
            this.process3rdPartyCheckBox = new System.Windows.Forms.CheckBox();
            this.processBatchesCheckBox = new System.Windows.Forms.CheckBox();
            this.processLoadingCheckBox = new System.Windows.Forms.CheckBox();
            this.processScoringCheckBox = new System.Windows.Forms.CheckBox();
            this.reqBatchReportsCheckBox = new System.Windows.Forms.CheckBox();
            this.scoringThreadLimitTextBox1 = new System.Windows.Forms.TextBox();
            this.serviceSleepTimeTextBox = new System.Windows.Forms.TextBox();
            this.threadSleepTimeTextBox = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.averageLoadingTimeTextBox = new System.Windows.Forms.TextBox();
            this.remoteRunningMetricsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.averageScoringTimeTextBox = new System.Windows.Forms.TextBox();
            this.batchesErrorTextBox = new System.Windows.Forms.TextBox();
            this.batchesLoadingTextBox = new System.Windows.Forms.TextBox();
            this.batchesTotalTextBox = new System.Windows.Forms.TextBox();
            this.batchesWaitingTextBox = new System.Windows.Forms.TextBox();
            this.claimsBeingScoredTextBox = new System.Windows.Forms.TextBox();
            this.claimsWaitingOnExternalTextBox = new System.Windows.Forms.TextBox();
            this.claimsWaitingToBeLoadedTextBox = new System.Windows.Forms.TextBox();
            this.claimsWaitingToBeScoredTextBox = new System.Windows.Forms.TextBox();
            this.externalErrorsTextBox = new System.Windows.Forms.TextBox();
            this.externalRetriesRequiredTextBox = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.grpConfig = new System.Windows.Forms.GroupBox();
            this.chkMonitor = new System.Windows.Forms.CheckBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            serviceSleepTimeLabel = new System.Windows.Forms.Label();
            threadSleepTimeLabel = new System.Windows.Forms.Label();
            averageLoadingTimeLabel = new System.Windows.Forms.Label();
            averageScoringTimeLabel = new System.Windows.Forms.Label();
            batchesErrorLabel = new System.Windows.Forms.Label();
            batchesLoadingLabel = new System.Windows.Forms.Label();
            batchesTotalLabel = new System.Windows.Forms.Label();
            batchesWaitingLabel = new System.Windows.Forms.Label();
            claimsBeingScoredLabel = new System.Windows.Forms.Label();
            claimsWaitingOnExternalLabel = new System.Windows.Forms.Label();
            claimsWaitingToBeLoadedLabel = new System.Windows.Forms.Label();
            claimsWaitingToBeScoredLabel = new System.Windows.Forms.Label();
            externalErrorsLabel = new System.Windows.Forms.Label();
            externalRetriesRequiredLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.remoteRunningMetricsBindingSource)).BeginInit();
            this.grpConfig.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // serviceSleepTimeLabel
            // 
            serviceSleepTimeLabel.AutoSize = true;
            serviceSleepTimeLabel.Location = new System.Drawing.Point(12, 331);
            serviceSleepTimeLabel.Name = "serviceSleepTimeLabel";
            serviceSleepTimeLabel.Size = new System.Drawing.Size(102, 13);
            serviceSleepTimeLabel.TabIndex = 31;
            serviceSleepTimeLabel.Text = "Service Sleep Time:";
            // 
            // threadSleepTimeLabel
            // 
            threadSleepTimeLabel.AutoSize = true;
            threadSleepTimeLabel.Location = new System.Drawing.Point(12, 357);
            threadSleepTimeLabel.Name = "threadSleepTimeLabel";
            threadSleepTimeLabel.Size = new System.Drawing.Size(100, 13);
            threadSleepTimeLabel.TabIndex = 33;
            threadSleepTimeLabel.Text = "Thread Sleep Time:";
            // 
            // averageLoadingTimeLabel
            // 
            averageLoadingTimeLabel.AutoSize = true;
            averageLoadingTimeLabel.Location = new System.Drawing.Point(6, 20);
            averageLoadingTimeLabel.Name = "averageLoadingTimeLabel";
            averageLoadingTimeLabel.Size = new System.Drawing.Size(117, 13);
            averageLoadingTimeLabel.TabIndex = 36;
            averageLoadingTimeLabel.Text = "Average Loading Time:";
            // 
            // averageScoringTimeLabel
            // 
            averageScoringTimeLabel.AutoSize = true;
            averageScoringTimeLabel.Location = new System.Drawing.Point(6, 46);
            averageScoringTimeLabel.Name = "averageScoringTimeLabel";
            averageScoringTimeLabel.Size = new System.Drawing.Size(115, 13);
            averageScoringTimeLabel.TabIndex = 38;
            averageScoringTimeLabel.Text = "Average Scoring Time:";
            // 
            // batchesErrorLabel
            // 
            batchesErrorLabel.AutoSize = true;
            batchesErrorLabel.Location = new System.Drawing.Point(6, 72);
            batchesErrorLabel.Name = "batchesErrorLabel";
            batchesErrorLabel.Size = new System.Drawing.Size(74, 13);
            batchesErrorLabel.TabIndex = 40;
            batchesErrorLabel.Text = "Batches Error:";
            // 
            // batchesLoadingLabel
            // 
            batchesLoadingLabel.AutoSize = true;
            batchesLoadingLabel.Location = new System.Drawing.Point(6, 98);
            batchesLoadingLabel.Name = "batchesLoadingLabel";
            batchesLoadingLabel.Size = new System.Drawing.Size(90, 13);
            batchesLoadingLabel.TabIndex = 42;
            batchesLoadingLabel.Text = "Batches Loading:";
            // 
            // batchesTotalLabel
            // 
            batchesTotalLabel.AutoSize = true;
            batchesTotalLabel.Location = new System.Drawing.Point(6, 124);
            batchesTotalLabel.Name = "batchesTotalLabel";
            batchesTotalLabel.Size = new System.Drawing.Size(76, 13);
            batchesTotalLabel.TabIndex = 44;
            batchesTotalLabel.Text = "Batches Total:";
            // 
            // batchesWaitingLabel
            // 
            batchesWaitingLabel.AutoSize = true;
            batchesWaitingLabel.Location = new System.Drawing.Point(6, 150);
            batchesWaitingLabel.Name = "batchesWaitingLabel";
            batchesWaitingLabel.Size = new System.Drawing.Size(88, 13);
            batchesWaitingLabel.TabIndex = 46;
            batchesWaitingLabel.Text = "Batches Waiting:";
            // 
            // claimsBeingScoredLabel
            // 
            claimsBeingScoredLabel.AutoSize = true;
            claimsBeingScoredLabel.Location = new System.Drawing.Point(6, 176);
            claimsBeingScoredLabel.Name = "claimsBeingScoredLabel";
            claimsBeingScoredLabel.Size = new System.Drawing.Size(107, 13);
            claimsBeingScoredLabel.TabIndex = 48;
            claimsBeingScoredLabel.Text = "Claims Being Scored:";
            // 
            // claimsWaitingOnExternalLabel
            // 
            claimsWaitingOnExternalLabel.AutoSize = true;
            claimsWaitingOnExternalLabel.Location = new System.Drawing.Point(6, 202);
            claimsWaitingOnExternalLabel.Name = "claimsWaitingOnExternalLabel";
            claimsWaitingOnExternalLabel.Size = new System.Drawing.Size(137, 13);
            claimsWaitingOnExternalLabel.TabIndex = 50;
            claimsWaitingOnExternalLabel.Text = "Claims Waiting On External:";
            // 
            // claimsWaitingToBeLoadedLabel
            // 
            claimsWaitingToBeLoadedLabel.AutoSize = true;
            claimsWaitingToBeLoadedLabel.Location = new System.Drawing.Point(6, 228);
            claimsWaitingToBeLoadedLabel.Name = "claimsWaitingToBeLoadedLabel";
            claimsWaitingToBeLoadedLabel.Size = new System.Drawing.Size(150, 13);
            claimsWaitingToBeLoadedLabel.TabIndex = 52;
            claimsWaitingToBeLoadedLabel.Text = "Claims Waiting To Be Loaded:";
            // 
            // claimsWaitingToBeScoredLabel
            // 
            claimsWaitingToBeScoredLabel.AutoSize = true;
            claimsWaitingToBeScoredLabel.Location = new System.Drawing.Point(6, 254);
            claimsWaitingToBeScoredLabel.Name = "claimsWaitingToBeScoredLabel";
            claimsWaitingToBeScoredLabel.Size = new System.Drawing.Size(148, 13);
            claimsWaitingToBeScoredLabel.TabIndex = 54;
            claimsWaitingToBeScoredLabel.Text = "Claims Waiting To Be Scored:";
            // 
            // externalErrorsLabel
            // 
            externalErrorsLabel.AutoSize = true;
            externalErrorsLabel.Location = new System.Drawing.Point(6, 280);
            externalErrorsLabel.Name = "externalErrorsLabel";
            externalErrorsLabel.Size = new System.Drawing.Size(78, 13);
            externalErrorsLabel.TabIndex = 56;
            externalErrorsLabel.Text = "External Errors:";
            // 
            // externalRetriesRequiredLabel
            // 
            externalRetriesRequiredLabel.AutoSize = true;
            externalRetriesRequiredLabel.Location = new System.Drawing.Point(6, 306);
            externalRetriesRequiredLabel.Name = "externalRetriesRequiredLabel";
            externalRetriesRequiredLabel.Size = new System.Drawing.Size(130, 13);
            externalRetriesRequiredLabel.TabIndex = 58;
            externalRetriesRequiredLabel.Text = "External Retries Required:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 383);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Monitor Sleep Time:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(282, 49);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 85);
            this.button1.TabIndex = 0;
            this.button1.Text = "Start Service";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // creatingThreadLimitTextBox
            // 
            this.creatingThreadLimitTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "CreatingThreadLimit", true));
            this.creatingThreadLimitTextBox.Location = new System.Drawing.Point(162, 21);
            this.creatingThreadLimitTextBox.Name = "creatingThreadLimitTextBox";
            this.creatingThreadLimitTextBox.ReadOnly = true;
            this.creatingThreadLimitTextBox.Size = new System.Drawing.Size(52, 20);
            this.creatingThreadLimitTextBox.TabIndex = 8;
            // 
            // bindingSource1
            // 
            this.bindingSource1.AllowNew = true;
            this.bindingSource1.DataSource = typeof(AdminConsole.Controls.PipelineService.RemoteConfigSettings);
            // 
            // enableBackupsCheckBox
            // 
            this.enableBackupsCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.bindingSource1, "EnableBackups", true));
            this.enableBackupsCheckBox.Location = new System.Drawing.Point(6, 195);
            this.enableBackupsCheckBox.Name = "enableBackupsCheckBox";
            this.enableBackupsCheckBox.Size = new System.Drawing.Size(104, 24);
            this.enableBackupsCheckBox.TabIndex = 10;
            this.enableBackupsCheckBox.Text = "Enable Backups";
            this.enableBackupsCheckBox.UseVisualStyleBackColor = true;
            // 
            // enableDedupeCheckBox
            // 
            this.enableDedupeCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.bindingSource1, "EnableDedupe", true));
            this.enableDedupeCheckBox.Location = new System.Drawing.Point(6, 225);
            this.enableDedupeCheckBox.Name = "enableDedupeCheckBox";
            this.enableDedupeCheckBox.Size = new System.Drawing.Size(104, 24);
            this.enableDedupeCheckBox.TabIndex = 12;
            this.enableDedupeCheckBox.Text = "Enable Dedupe";
            this.enableDedupeCheckBox.UseVisualStyleBackColor = true;
            // 
            // enableSyncCheckBox
            // 
            this.enableSyncCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.bindingSource1, "EnableSync", true));
            this.enableSyncCheckBox.Location = new System.Drawing.Point(6, 255);
            this.enableSyncCheckBox.Name = "enableSyncCheckBox";
            this.enableSyncCheckBox.Size = new System.Drawing.Size(104, 24);
            this.enableSyncCheckBox.TabIndex = 14;
            this.enableSyncCheckBox.Text = "Enable Sync";
            this.enableSyncCheckBox.UseVisualStyleBackColor = true;
            // 
            // externalThreadLimitTextBox
            // 
            this.externalThreadLimitTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "ExternalThreadLimit", true));
            this.externalThreadLimitTextBox.Location = new System.Drawing.Point(162, 81);
            this.externalThreadLimitTextBox.Name = "externalThreadLimitTextBox";
            this.externalThreadLimitTextBox.ReadOnly = true;
            this.externalThreadLimitTextBox.Size = new System.Drawing.Size(52, 20);
            this.externalThreadLimitTextBox.TabIndex = 16;
            // 
            // loadingThreadLimitTextBox
            // 
            this.loadingThreadLimitTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "LoadingThreadLimit", true));
            this.loadingThreadLimitTextBox.Location = new System.Drawing.Point(162, 51);
            this.loadingThreadLimitTextBox.Name = "loadingThreadLimitTextBox";
            this.loadingThreadLimitTextBox.ReadOnly = true;
            this.loadingThreadLimitTextBox.Size = new System.Drawing.Size(52, 20);
            this.loadingThreadLimitTextBox.TabIndex = 18;
            // 
            // process3rdPartyCheckBox
            // 
            this.process3rdPartyCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.bindingSource1, "Process3rdParty", true));
            this.process3rdPartyCheckBox.Location = new System.Drawing.Point(6, 79);
            this.process3rdPartyCheckBox.Name = "process3rdPartyCheckBox";
            this.process3rdPartyCheckBox.Size = new System.Drawing.Size(134, 24);
            this.process3rdPartyCheckBox.TabIndex = 20;
            this.process3rdPartyCheckBox.Text = "Process 3rd Parties";
            this.process3rdPartyCheckBox.UseVisualStyleBackColor = true;
            // 
            // processBatchesCheckBox
            // 
            this.processBatchesCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.bindingSource1, "ProcessBatches", true));
            this.processBatchesCheckBox.Location = new System.Drawing.Point(6, 19);
            this.processBatchesCheckBox.Name = "processBatchesCheckBox";
            this.processBatchesCheckBox.Size = new System.Drawing.Size(134, 24);
            this.processBatchesCheckBox.TabIndex = 22;
            this.processBatchesCheckBox.Text = "Process Batches";
            this.processBatchesCheckBox.UseVisualStyleBackColor = true;
            // 
            // processLoadingCheckBox
            // 
            this.processLoadingCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.bindingSource1, "ProcessLoading", true));
            this.processLoadingCheckBox.Location = new System.Drawing.Point(6, 49);
            this.processLoadingCheckBox.Name = "processLoadingCheckBox";
            this.processLoadingCheckBox.Size = new System.Drawing.Size(134, 24);
            this.processLoadingCheckBox.TabIndex = 24;
            this.processLoadingCheckBox.Text = "Process Loading";
            this.processLoadingCheckBox.UseVisualStyleBackColor = true;
            // 
            // processScoringCheckBox
            // 
            this.processScoringCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.bindingSource1, "ProcessScoring", true));
            this.processScoringCheckBox.Location = new System.Drawing.Point(6, 113);
            this.processScoringCheckBox.Name = "processScoringCheckBox";
            this.processScoringCheckBox.Size = new System.Drawing.Size(134, 24);
            this.processScoringCheckBox.TabIndex = 26;
            this.processScoringCheckBox.Text = "Process Scoring";
            this.processScoringCheckBox.UseVisualStyleBackColor = true;
            // 
            // reqBatchReportsCheckBox
            // 
            this.reqBatchReportsCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.bindingSource1, "ReqBatchReports", true));
            this.reqBatchReportsCheckBox.Location = new System.Drawing.Point(6, 164);
            this.reqBatchReportsCheckBox.Name = "reqBatchReportsCheckBox";
            this.reqBatchReportsCheckBox.Size = new System.Drawing.Size(178, 24);
            this.reqBatchReportsCheckBox.TabIndex = 28;
            this.reqBatchReportsCheckBox.Text = "PreFetch Batch Reports";
            this.reqBatchReportsCheckBox.UseVisualStyleBackColor = true;
            // 
            // scoringThreadLimitTextBox1
            // 
            this.scoringThreadLimitTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "ScoringThreadLimit", true));
            this.scoringThreadLimitTextBox1.Location = new System.Drawing.Point(162, 113);
            this.scoringThreadLimitTextBox1.Name = "scoringThreadLimitTextBox1";
            this.scoringThreadLimitTextBox1.Size = new System.Drawing.Size(52, 20);
            this.scoringThreadLimitTextBox1.TabIndex = 30;
            // 
            // serviceSleepTimeTextBox
            // 
            this.serviceSleepTimeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "ServiceSleepTime", true));
            this.serviceSleepTimeTextBox.Location = new System.Drawing.Point(128, 328);
            this.serviceSleepTimeTextBox.Name = "serviceSleepTimeTextBox";
            this.serviceSleepTimeTextBox.Size = new System.Drawing.Size(104, 20);
            this.serviceSleepTimeTextBox.TabIndex = 32;
            // 
            // threadSleepTimeTextBox
            // 
            this.threadSleepTimeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "ThreadSleepTime", true));
            this.threadSleepTimeTextBox.Location = new System.Drawing.Point(128, 354);
            this.threadSleepTimeTextBox.Name = "threadSleepTimeTextBox";
            this.threadSleepTimeTextBox.Size = new System.Drawing.Size(104, 20);
            this.threadSleepTimeTextBox.TabIndex = 34;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(65, 383);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(119, 43);
            this.button3.TabIndex = 35;
            this.button3.Text = "Apply Config";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(88, 373);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(119, 43);
            this.button4.TabIndex = 36;
            this.button4.Text = "Refresh";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // averageLoadingTimeTextBox
            // 
            this.averageLoadingTimeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "AverageLoadingTime", true));
            this.averageLoadingTimeTextBox.Location = new System.Drawing.Point(162, 17);
            this.averageLoadingTimeTextBox.Name = "averageLoadingTimeTextBox";
            this.averageLoadingTimeTextBox.ReadOnly = true;
            this.averageLoadingTimeTextBox.Size = new System.Drawing.Size(100, 20);
            this.averageLoadingTimeTextBox.TabIndex = 37;
            // 
            // remoteRunningMetricsBindingSource
            // 
            this.remoteRunningMetricsBindingSource.DataSource = typeof(AdminConsole.Controls.PipelineService.RemoteRunningMetrics);
            // 
            // averageScoringTimeTextBox
            // 
            this.averageScoringTimeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "AverageScoringTime", true));
            this.averageScoringTimeTextBox.Location = new System.Drawing.Point(162, 43);
            this.averageScoringTimeTextBox.Name = "averageScoringTimeTextBox";
            this.averageScoringTimeTextBox.ReadOnly = true;
            this.averageScoringTimeTextBox.Size = new System.Drawing.Size(100, 20);
            this.averageScoringTimeTextBox.TabIndex = 39;
            // 
            // batchesErrorTextBox
            // 
            this.batchesErrorTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "BatchesError", true));
            this.batchesErrorTextBox.Location = new System.Drawing.Point(162, 69);
            this.batchesErrorTextBox.Name = "batchesErrorTextBox";
            this.batchesErrorTextBox.ReadOnly = true;
            this.batchesErrorTextBox.Size = new System.Drawing.Size(100, 20);
            this.batchesErrorTextBox.TabIndex = 41;
            // 
            // batchesLoadingTextBox
            // 
            this.batchesLoadingTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "BatchesLoading", true));
            this.batchesLoadingTextBox.Location = new System.Drawing.Point(162, 95);
            this.batchesLoadingTextBox.Name = "batchesLoadingTextBox";
            this.batchesLoadingTextBox.ReadOnly = true;
            this.batchesLoadingTextBox.Size = new System.Drawing.Size(100, 20);
            this.batchesLoadingTextBox.TabIndex = 43;
            // 
            // batchesTotalTextBox
            // 
            this.batchesTotalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "BatchesTotal", true));
            this.batchesTotalTextBox.Location = new System.Drawing.Point(162, 121);
            this.batchesTotalTextBox.Name = "batchesTotalTextBox";
            this.batchesTotalTextBox.ReadOnly = true;
            this.batchesTotalTextBox.Size = new System.Drawing.Size(100, 20);
            this.batchesTotalTextBox.TabIndex = 45;
            // 
            // batchesWaitingTextBox
            // 
            this.batchesWaitingTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "BatchesWaiting", true));
            this.batchesWaitingTextBox.Location = new System.Drawing.Point(162, 147);
            this.batchesWaitingTextBox.Name = "batchesWaitingTextBox";
            this.batchesWaitingTextBox.ReadOnly = true;
            this.batchesWaitingTextBox.Size = new System.Drawing.Size(100, 20);
            this.batchesWaitingTextBox.TabIndex = 47;
            // 
            // claimsBeingScoredTextBox
            // 
            this.claimsBeingScoredTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "ClaimsBeingScored", true));
            this.claimsBeingScoredTextBox.Location = new System.Drawing.Point(162, 173);
            this.claimsBeingScoredTextBox.Name = "claimsBeingScoredTextBox";
            this.claimsBeingScoredTextBox.ReadOnly = true;
            this.claimsBeingScoredTextBox.Size = new System.Drawing.Size(100, 20);
            this.claimsBeingScoredTextBox.TabIndex = 49;
            // 
            // claimsWaitingOnExternalTextBox
            // 
            this.claimsWaitingOnExternalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "ClaimsWaitingOnExternal", true));
            this.claimsWaitingOnExternalTextBox.Location = new System.Drawing.Point(162, 199);
            this.claimsWaitingOnExternalTextBox.Name = "claimsWaitingOnExternalTextBox";
            this.claimsWaitingOnExternalTextBox.ReadOnly = true;
            this.claimsWaitingOnExternalTextBox.Size = new System.Drawing.Size(100, 20);
            this.claimsWaitingOnExternalTextBox.TabIndex = 51;
            // 
            // claimsWaitingToBeLoadedTextBox
            // 
            this.claimsWaitingToBeLoadedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "ClaimsWaitingToBeLoaded", true));
            this.claimsWaitingToBeLoadedTextBox.Location = new System.Drawing.Point(162, 225);
            this.claimsWaitingToBeLoadedTextBox.Name = "claimsWaitingToBeLoadedTextBox";
            this.claimsWaitingToBeLoadedTextBox.ReadOnly = true;
            this.claimsWaitingToBeLoadedTextBox.Size = new System.Drawing.Size(100, 20);
            this.claimsWaitingToBeLoadedTextBox.TabIndex = 53;
            // 
            // claimsWaitingToBeScoredTextBox
            // 
            this.claimsWaitingToBeScoredTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "ClaimsWaitingToBeScored", true));
            this.claimsWaitingToBeScoredTextBox.Location = new System.Drawing.Point(162, 251);
            this.claimsWaitingToBeScoredTextBox.Name = "claimsWaitingToBeScoredTextBox";
            this.claimsWaitingToBeScoredTextBox.ReadOnly = true;
            this.claimsWaitingToBeScoredTextBox.Size = new System.Drawing.Size(100, 20);
            this.claimsWaitingToBeScoredTextBox.TabIndex = 55;
            // 
            // externalErrorsTextBox
            // 
            this.externalErrorsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "ExternalErrors", true));
            this.externalErrorsTextBox.Location = new System.Drawing.Point(162, 277);
            this.externalErrorsTextBox.Name = "externalErrorsTextBox";
            this.externalErrorsTextBox.ReadOnly = true;
            this.externalErrorsTextBox.Size = new System.Drawing.Size(100, 20);
            this.externalErrorsTextBox.TabIndex = 57;
            // 
            // externalRetriesRequiredTextBox
            // 
            this.externalRetriesRequiredTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.remoteRunningMetricsBindingSource, "ExternalRetriesRequired", true));
            this.externalRetriesRequiredTextBox.Location = new System.Drawing.Point(162, 303);
            this.externalRetriesRequiredTextBox.Name = "externalRetriesRequiredTextBox";
            this.externalRetriesRequiredTextBox.ReadOnly = true;
            this.externalRetriesRequiredTextBox.Size = new System.Drawing.Size(100, 20);
            this.externalRetriesRequiredTextBox.TabIndex = 59;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(283, 381);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(119, 78);
            this.button5.TabIndex = 60;
            this.button5.Text = "Database";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // grpConfig
            // 
            this.grpConfig.Controls.Add(this.processBatchesCheckBox);
            this.grpConfig.Controls.Add(this.threadSleepTimeTextBox);
            this.grpConfig.Controls.Add(threadSleepTimeLabel);
            this.grpConfig.Controls.Add(this.serviceSleepTimeTextBox);
            this.grpConfig.Controls.Add(serviceSleepTimeLabel);
            this.grpConfig.Controls.Add(this.scoringThreadLimitTextBox1);
            this.grpConfig.Controls.Add(this.reqBatchReportsCheckBox);
            this.grpConfig.Controls.Add(this.processScoringCheckBox);
            this.grpConfig.Controls.Add(this.processLoadingCheckBox);
            this.grpConfig.Controls.Add(this.process3rdPartyCheckBox);
            this.grpConfig.Controls.Add(this.loadingThreadLimitTextBox);
            this.grpConfig.Controls.Add(this.externalThreadLimitTextBox);
            this.grpConfig.Controls.Add(this.enableSyncCheckBox);
            this.grpConfig.Controls.Add(this.enableDedupeCheckBox);
            this.grpConfig.Controls.Add(this.enableBackupsCheckBox);
            this.grpConfig.Controls.Add(this.creatingThreadLimitTextBox);
            this.grpConfig.Controls.Add(this.button3);
            this.grpConfig.Enabled = false;
            this.grpConfig.Location = new System.Drawing.Point(14, 28);
            this.grpConfig.Name = "grpConfig";
            this.grpConfig.Size = new System.Drawing.Size(248, 432);
            this.grpConfig.TabIndex = 61;
            this.grpConfig.TabStop = false;
            this.grpConfig.Text = "Config Settings";
            // 
            // chkMonitor
            // 
            this.chkMonitor.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.bindingSource1, "EnableMonitoring", true));
            this.chkMonitor.Location = new System.Drawing.Point(6, 284);
            this.chkMonitor.Name = "chkMonitor";
            this.chkMonitor.Size = new System.Drawing.Size(178, 24);
            this.chkMonitor.TabIndex = 38;
            this.chkMonitor.Text = "Enable Monitoring";
            this.chkMonitor.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "MonitorSleepTime", true));
            this.textBox2.Location = new System.Drawing.Point(128, 380);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(104, 20);
            this.textBox2.TabIndex = 37;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(averageLoadingTimeLabel);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.externalRetriesRequiredTextBox);
            this.groupBox2.Controls.Add(externalRetriesRequiredLabel);
            this.groupBox2.Controls.Add(this.averageLoadingTimeTextBox);
            this.groupBox2.Controls.Add(this.externalErrorsTextBox);
            this.groupBox2.Controls.Add(averageScoringTimeLabel);
            this.groupBox2.Controls.Add(externalErrorsLabel);
            this.groupBox2.Controls.Add(this.averageScoringTimeTextBox);
            this.groupBox2.Controls.Add(this.claimsWaitingToBeScoredTextBox);
            this.groupBox2.Controls.Add(batchesErrorLabel);
            this.groupBox2.Controls.Add(claimsWaitingToBeScoredLabel);
            this.groupBox2.Controls.Add(this.batchesErrorTextBox);
            this.groupBox2.Controls.Add(this.claimsWaitingToBeLoadedTextBox);
            this.groupBox2.Controls.Add(batchesLoadingLabel);
            this.groupBox2.Controls.Add(claimsWaitingToBeLoadedLabel);
            this.groupBox2.Controls.Add(this.batchesLoadingTextBox);
            this.groupBox2.Controls.Add(this.claimsWaitingOnExternalTextBox);
            this.groupBox2.Controls.Add(batchesTotalLabel);
            this.groupBox2.Controls.Add(claimsWaitingOnExternalLabel);
            this.groupBox2.Controls.Add(this.batchesTotalTextBox);
            this.groupBox2.Controls.Add(this.claimsBeingScoredTextBox);
            this.groupBox2.Controls.Add(batchesWaitingLabel);
            this.groupBox2.Controls.Add(claimsBeingScoredLabel);
            this.groupBox2.Controls.Add(this.batchesWaitingTextBox);
            this.groupBox2.Location = new System.Drawing.Point(422, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(277, 432);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Metrics";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(283, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(119, 20);
            this.textBox1.TabIndex = 63;
            this.textBox1.Text = "x";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ServiceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.grpConfig);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button1);
            this.Name = "ServiceControl";
            this.Size = new System.Drawing.Size(706, 472);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.remoteRunningMetricsBindingSource)).EndInit();
            this.grpConfig.ResumeLayout(false);
            this.grpConfig.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.TextBox creatingThreadLimitTextBox;
        private System.Windows.Forms.CheckBox enableBackupsCheckBox;
        private System.Windows.Forms.CheckBox enableDedupeCheckBox;
        private System.Windows.Forms.CheckBox enableSyncCheckBox;
        private System.Windows.Forms.TextBox externalThreadLimitTextBox;
        private System.Windows.Forms.TextBox loadingThreadLimitTextBox;
        private System.Windows.Forms.CheckBox process3rdPartyCheckBox;
        private System.Windows.Forms.CheckBox processBatchesCheckBox;
        private System.Windows.Forms.CheckBox processLoadingCheckBox;
        private System.Windows.Forms.CheckBox processScoringCheckBox;
        private System.Windows.Forms.CheckBox reqBatchReportsCheckBox;
        private System.Windows.Forms.TextBox scoringThreadLimitTextBox1;
        private System.Windows.Forms.TextBox serviceSleepTimeTextBox;
        private System.Windows.Forms.TextBox threadSleepTimeTextBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.BindingSource remoteRunningMetricsBindingSource;
        private System.Windows.Forms.TextBox averageLoadingTimeTextBox;
        private System.Windows.Forms.TextBox averageScoringTimeTextBox;
        private System.Windows.Forms.TextBox batchesErrorTextBox;
        private System.Windows.Forms.TextBox batchesLoadingTextBox;
        private System.Windows.Forms.TextBox batchesTotalTextBox;
        private System.Windows.Forms.TextBox batchesWaitingTextBox;
        private System.Windows.Forms.TextBox claimsBeingScoredTextBox;
        private System.Windows.Forms.TextBox claimsWaitingOnExternalTextBox;
        private System.Windows.Forms.TextBox claimsWaitingToBeLoadedTextBox;
        private System.Windows.Forms.TextBox claimsWaitingToBeScoredTextBox;
        private System.Windows.Forms.TextBox externalErrorsTextBox;
        private System.Windows.Forms.TextBox externalRetriesRequiredTextBox;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox grpConfig;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox chkMonitor;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
    }
}

