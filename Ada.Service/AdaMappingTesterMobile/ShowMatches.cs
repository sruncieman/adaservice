﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdaMappingTester
{
    public partial class ShowMatches : Form
    {
        public ShowMatches()
        {
            InitializeComponent();
        }

        public void AddMessages(List<string> msgs)
        {
            listBox1.Items.Clear();
            foreach (var s in msgs)
                listBox1.Items.Add(s);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
