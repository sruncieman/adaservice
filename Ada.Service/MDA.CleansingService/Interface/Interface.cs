﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common;
using MDA.Common.Server;
using MDA.DAL;


namespace MDA.CleansingService.Interface
{


    public interface ICleansingService
    {
        //MdaDbContext SetDbContext { set; }
        CurrentContext CurrentContext { set; }

        #region Pipeline Cleansing
        /// <summary>
        /// Cleanse all the entities in a PipelineClaim
        /// </summary>
        /// <param name="claim">Claim to cleanse</param>
        /// <returns>ProcessingResults structure</returns>
        ProcessingResults CleanseTheClaim(MDA.Pipeline.Model.IPipelineClaim claim);
        #endregion

        #region Dedupe Cleansing
        /// <summary>
        /// Cleans a Policy.  This is an entity read from the database
        /// </summary>
        /// <param name="policy">Policy to cleanse. Contents can be modified</param>
        /// <param name="errorNode">Strucure to hold Errors/Warnings found</param>
        /// <returns>Always returns 0</returns>
        int CleansePolicy(MDA.DAL.Policy policy, MessageNode errorNode);

        /// <summary>
        ///  Cleans claim info  This is an entity read from the database
        /// </summary>
        /// <param name="i2p"></param>
        /// <param name="errorNode"></param>
        /// <returns></returns>
        int CleanseClaimInfo(MDA.DAL.Incident2Person i2p, MessageNode errorNode);

        
        /// <summary>
        /// Cleans a Address.  This is an entity read from the database
        /// </summary>
        /// <param name="address">Address to cleanse. Contents can be modified</param>
        /// <param name="errorNode">Strucure to hold Errors/Warnings found</param>
        /// <returns>Always returns 0</returns>        
        int CleanseAddress(MDA.DAL.Address address, MessageNode errorNode);

        /// <summary>
        /// Cleans a BankAccount.  This is an entity read from the database
        /// </summary>
        /// <param name="bankAccount">BankAccount to cleanse. Contents can be modified</param>
        /// <param name="errorNode">Strucure to hold Errors/Warnings found</param>
        /// <returns>Always returns 0</returns>  
        int CleanseBankAccount(MDA.DAL.BankAccount bankAccount, MessageNode errorNode);

        /// <summary>
        /// Cleans a Organisation.  This is an entity read from the database
        /// </summary>
        /// <param name="organisation">Organisation to cleanse. Contents can be modified</param>
        /// <param name="errorNode">Strucure to hold Errors/Warnings found</param>
        /// <returns>Always returns 0</returns> 
        int CleanseOrganisation(MDA.DAL.Organisation organisation, MessageNode errorNode);

        /// <summary>
        /// Cleans a PaymentCard.  This is an entity read from the database
        /// </summary>
        /// <param name="paymentCard">PaymentCard to cleanse. Contents can be modified</param>
        /// <param name="errorNode">Strucure to hold Errors/Warnings found</param>
        /// <returns>Always returns 0</returns> 
        int CleansePaymentCard(MDA.DAL.PaymentCard paymentCard, MessageNode errorNode);

        /// <summary>
        /// Cleans a Person.  This is an entity read from the database
        /// </summary>
        /// <param name="person">Person to cleanse. Contents can be modified</param>
        /// <param name="errorNode">Strucure to hold Errors/Warnings found</param>
        /// <returns>Always returns 0</returns> 
        int CleansePerson(MDA.DAL.Person person, MessageNode errorNode);

        /// <summary>
        /// Cleans a Telephone.  This is an entity read from the database
        /// </summary>
        /// <param name="telephone">Telephone to cleanse. Contents can be modified</param>
        /// <param name="errorNode">Strucure to hold Errors/Warnings found</param>
        /// <returns>Always returns 0</returns> 
        int CleanseTelephone(MDA.DAL.Telephone telephone, MessageNode errorNode);

        /// <summary>
        /// Cleans a Vehicle.  This is an entity read from the database
        /// </summary>
        /// <param name="vehicle">Vehicle to cleanse. Contents can be modified</param>
        /// <param name="errorNode">Strucure to hold Errors/Warnings found</param>
        /// <returns>Always returns 0</returns> 
        int CleanseVehicle(MDA.DAL.Vehicle vehicle, MessageNode errorNode);
        #endregion

    }
}
