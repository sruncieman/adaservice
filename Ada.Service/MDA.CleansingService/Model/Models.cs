﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.CleansingService.Model
{
    public class Address
    {
        public string SubBuilding { get; set; }
        public string Building { get; set; }
        public string BuildingNumber { get; set; }
        public string Street { get; set; }
        public string Locality { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public int PAFValidation { get; set; }
        public string PAFUPRN { get; set; }
    }

    //public class BankAccount
    //{
    //    public string AccountNumber { get; set; }
    //    public string SortCode { get; set; }
    //    public string BankName { get; set; }
    //    //public PolicyPaymentType PolicyPaymentType { get; set; }
    //    public DateTime? DatePaymentDetailsTaken { get; set; }
    //}

    //public class Claim
    //{
    //    public string ClaimNumber { get; set; }
    //    //public ClaimType ClaimType { get; set; }
    //    public DateTime? IncidentDate { get; set; }
    //    public decimal? PaymentsToDate { get; set; }
    //    public decimal? Reserve { get; set; }
    //    //public Policy Policy { get; set; }
    //    //public ObservableCollection<Vehicle> Vehicles { get; set; }
    //    //public ObservableCollection<Organisation> Organisations { get; set; }
    //}

    //public class Organisation
    //{
    //    //public OrganisationType OrganisationType { get; set; }
    //    public string OrganisationName { get; set; }
    //    public string RegisteredNumber { get; set; }
    //    public string VatNumber { get; set; }
    //    public string MojCrmNumber { get; set; }
    //    //public MojCRMStatus MojCrmStatus { get; set; }
    //    public string Email { get; set; }
    //    public string WebSite { get; set; }
    //    public string Telephone1 { get; set; }
    //    public string Telephone2 { get; set; }
    //    public string Telephone3 { get; set; }
    //    //public ClaimInfo ClaimInfo { get; set; }
    //    //public BankAccount BankAccount { get; set; }   // Only populated when Org under Claim (not person)
    //    //public PaymentCard PaymentCard { get; set; }   // Only populated when Org under Claim (not person)
    //    //public ObservableCollection<Address> Addresses { get; set; }
    //    //public ObservableCollection<OrganisationVehicle> Vehicles { get; set; }
    //}

    //public class PaymentCard
    //{
    //    public string SortCode { get; set; }
    //    public string BankName { get; set; }
    //    public string ExpiryDate { get; set; }
    //    //public PaymentCardType PaymentCardType { get; set; }
    //    //public PolicyPaymentType PolicyPaymentType { get; set; }
    //    public DateTime? DatePaymentDetailsTaken { get; set; }
    //}

    //public class Person
    //{
    //    //public PartyType PartyType { get; set; }
    //    //public SubPartyType SubPartyType { get; set; }
    //    //public Salutation Salutation { get; set; }
    //    public string FirstName { get; set; }
    //    public string MiddleName { get; set; }
    //    public string LastName { get; set; }
    //    public DateTime? DateOfBirth { get; set; }
    //    //public Gender Gender { get; set; }
    //    public string Nationality { get; set; }
    //    public string NINumber { get; set; }
    //    public string DrivingLicenseNumber { get; set; }
    //    public string PassportNumber { get; set; }
    //    public string Occupation { get; set; }
    //    public string EmailAddress { get; set; }
    //    public string LandlineTelephone { get; set; }
    //    public string WorkTelephone { get; set; }
    //    public string MobileTelephone { get; set; }
    //    public string OtherTelephone { get; set; }
    //    //public ClaimInfo ClaimInfo { get; set; }
    //}

    //public class Policy
    //{
    //    //public PolicyType PolicyType { get; set; }
    //    public string PolicyNumber { get; set; }
    //    public string TradingName { get; set; }
    //    public string Broker { get; set; }
    //    //public PolicyCoverType CoverType { get; set; }
    //    public DateTime? PolicyStartDate { get; set; }
    //    public DateTime? PolicyEndDate { get; set; }
    //    public int? PreviousNoFaultClaimsCount { get; set; }
    //    public int? PreviousFaultClaimsCount { get; set; }
    //    public decimal? Premium { get; set; }
    //}

    //public class Telephone
    //{
    //    public string AreaCode { get; set; }
    //    public string InternationalCode { get; set; }
    //    public string FullNumber { get; set; }
    //    public string ClientSuppliedNumber { get; set; }
    //}

    //public class Vehicle
    //{
    //    //public VehicleType VehicleType { get; set; }
    //    //public Incident2VehicleLinkType Incident2VehicleLinkType { get; set; }
    //    public string VehicleRegistration { get; set; }
    //    //public VehicleColour Colour { get; set; }
    //    public string Make { get; set; }
    //    public string Model { get; set; }
    //    public string EngineCapacity { get; set; }
    //    public string VIN { get; set; }
    //    //public VehicleFuelType Fuel { get; set; }
    //    //public VehicleTransmission Transmission { get; set; }
    //    //public VehicleCategoryOfLoss CategoryOfLoss { get; set; }
    //    //public ObservableCollection<Person> People { get; set; }
    //    public DateTime? HireStartDate { get; set; }
    //    public DateTime? HireEndDate { get; set; }
    //}


}
