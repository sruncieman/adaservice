﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.MappingService.FirstCentral.Motor.Model;

namespace MDA.MappingService.FirstCentral.Motor
{
    public class TranslateFromFile
    {
        public IEnumerable<string> ClaimBatchRows { get; set; }
        public List<int> ClaimRowNumbers { get; set; }
        public PipelineClaimBatch MotorClaimBatch { get; set; }

        public DateTime _incidentDate { get; set; }

        public DataTable ClaimsToDataTable(Stream filem)
        {
            DataTable dtTable = new DataTable();

            #region DataTable Columns
            dtTable.Columns.Add("FeedType");
            dtTable.Columns.Add("RowType");
            dtTable.Columns.Add("Claim Number");
            dtTable.Columns.Add("PARTY_EXTERNAL_UID");
            dtTable.Columns.Add("PARTY_TYPE");
            dtTable.Columns.Add("ENTITY_EXTERNAL_UID");
            dtTable.Columns.Add("ENTITY_TYPE");
            dtTable.Columns.Add("01");
            dtTable.Columns.Add("02");
            dtTable.Columns.Add("03");
            dtTable.Columns.Add("04");
            dtTable.Columns.Add("05");
            dtTable.Columns.Add("06");
            dtTable.Columns.Add("07");
            dtTable.Columns.Add("08");
            dtTable.Columns.Add("09");
            dtTable.Columns.Add("10");
            dtTable.Columns.Add("11");
            dtTable.Columns.Add("12");
            dtTable.Columns.Add("13");
            dtTable.Columns.Add("14");
            dtTable.Columns.Add("15");
            dtTable.Columns.Add("16");
            dtTable.Columns.Add("17");
            dtTable.Columns.Add("18");
            dtTable.Columns.Add("19");
            dtTable.Columns.Add("20");
            dtTable.Columns.Add("21");
            dtTable.Columns.Add("22");
            dtTable.Columns.Add("23");
            dtTable.Columns.Add("24");
            dtTable.Columns.Add("25");
            dtTable.Columns.Add("26");
            dtTable.Columns.Add("27");
            dtTable.Columns.Add("28");
            dtTable.Columns.Add("29");
            dtTable.Columns.Add("30");
            dtTable.Columns.Add("31");
            dtTable.Columns.Add("32");
            dtTable.Columns.Add("33");
            dtTable.Columns.Add("34");
            dtTable.Columns.Add("35");
            dtTable.Columns.Add("36");
            dtTable.Columns.Add("37");
            dtTable.Columns.Add("38");
            dtTable.Columns.Add("39");
            dtTable.Columns.Add("40");
            dtTable.Columns.Add("41");
            dtTable.Columns.Add("42");
            dtTable.Columns.Add("43");
            dtTable.Columns.Add("44");
            dtTable.Columns.Add("45");
            dtTable.Columns.Add("46");
            dtTable.Columns.Add("47");
            dtTable.Columns.Add("48");
            dtTable.Columns.Add("49");
            dtTable.Columns.Add("50");
            dtTable.Columns.Add("51");
            dtTable.Columns.Add("52");
            dtTable.Columns.Add("53");
            dtTable.Columns.Add("54");
            #endregion

            try
            {
                // Read File into data table
                using (StreamReader sr = new StreamReader(filem))
                {
                    while (sr.Peek() > 1)
                    {
                        object[] currentLine = sr.ReadLine().Split(new char[] { '|' });
                        dtTable.Rows.Add(currentLine);
                    }
                }


                #region Add Row Counter

                dtTable.Columns.Add("RowCount", typeof(string));

                int count = 1;

                foreach (DataRow dr in dtTable.Rows)
                {
                    dr["RowCount"] = count;
                    count++;
                }

                var query = (from DataRow dr in dtTable.Rows
                             select dr);

                string counter = null;
                foreach (DataRow item in query)
                {
                    if (item["RowType"].ToString() == "CLAIM_MOTOR")
                    {
                        counter = item[61].ToString();
                    }
                    else
                    {
                        item[61] = counter;
                    }
                }

            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method ClaimsToDataTable: " + ex);
            }

                #endregion

            return dtTable;
        }

        public Dictionary<string, string> GetDistinctClaims(DataTable dtt)
        {

            Dictionary<string, string> DictClaimNumber = new Dictionary<string, string>();

            try
            {

                ClaimBatchRows = (from DataRow dr in dtt.Rows
                                  where
                                      dr.Field<string>("Claim Number") != null &&
                                      dr.Field<string>("RowType") == "CLAIM_MOTOR"
                                  // && dr.Field<string>("Claim Number") == "13/0000565"
                                  select (string)dr["RowCount"]).Distinct();


                foreach (var cbr in ClaimBatchRows)
                {
                    var claimNum = (from DataRow dr in dtt.Rows
                                    where dr.Field<string>("RowCount") == cbr
                                    select (string)dr["Claim Number"]).Distinct().ToList();

                    string ClaimNumber = claimNum.FirstOrDefault();

                    //Console.WriteLine(cbr);
                    //Console.WriteLine(ClaimNumber);
                    DictClaimNumber.Add(cbr, ClaimNumber);
                }

            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetDistinctClaims: " + ex);
            }

            return DictClaimNumber;
        }

        public List<int> GetDistinctClaimLinesForThisClaimNumber(string claimRow, DataTable dtt)
        {

            try
            {
                ClaimRowNumbers = (from DataRow dr in dtt.AsEnumerable()
                                   where
                                       dr.Field<string>("Claim Number") == claimRow &&
                                       dr.Field<string>("RowType") == "CLAIM_MOTOR"
                                   select (int)dr["RowCount"]).Distinct().ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetDistinctClaimLinesForThisClaimNumber: " + ex);
            }

            return ClaimRowNumbers;
        }

        private DateTime _ConvertDate(string s)
        {
            int year = 0;
            int month = 0;
            int day = 0;

            try
            {
                year = Convert.ToInt32(s.Substring(0, 4));
                month = Convert.ToInt32(s.Substring(4, 2));
                day = Convert.ToInt32(s.Substring(6, 2));
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method _ConvertDate: " + ex);
            }

            return new DateTime(year, month, day);
        }

        public PipelineMotorClaim GetClaimDetails(DataTable dtt, KeyValuePair<string, string> claimNumber)
        {
            DateTime incidentDate = new DateTime();
            TimeSpan incidentTime;

            PipelineMotorClaim claim = new PipelineMotorClaim();

            var result = (from DataRow dr in dtt.AsEnumerable()
                          where
                              dr.Field<string>("Claim Number") == claimNumber.Value &&
                              dr.Field<string>("RowType") == "CLAIM_MOTOR" && dr.Field<string>("RowCount") == claimNumber.Key
                          select dr).FirstOrDefault();

            PipelineClaimInfo claimInfo = new PipelineClaimInfo();

            DateTime claimNotificationDate;
            string incidentCircumstances = null;


            var partyMotorCollection = (from DataRow dr in dtt.AsEnumerable()
                                        where dr.Field<string>("Claim Number") == claimNumber.Value &&
                                            dr.Field<string>("RowType") == "PARTY_MOTOR" && dr[4].ToString() == "CLM0001"
                                            //&& !dr[4].ToString().Contains("TPC") && !dr[4].ToString().Contains("THPRPR")
                                            && dr.Field<string>("RowCount") == claimNumber.Key
                                        select dr).ToList();


            if (partyMotorCollection.Any())
            {
                foreach (var dataRow in partyMotorCollection)
                {
                    bool? policeAttended = null;
                    string policeNumber = null;
                    string policeCrimeNumber = null;

                    if (!string.IsNullOrEmpty(dataRow[43].ToString()))
                    {
                        policeNumber = dataRow[43].ToString();
                    }
                    if (!string.IsNullOrEmpty(dataRow[44].ToString()))
                    {
                        policeCrimeNumber = dataRow[44].ToString();
                    }

                    if (policeNumber != null || policeCrimeNumber != null)
                    {
                        policeAttended = true;
                    }


                    claimInfo.PoliceAttended = policeAttended;
                    if (!string.IsNullOrEmpty(dataRow[44].ToString()))
                    {
                        claimInfo.PoliceReference = dataRow[44].ToString();
                    }
                }
            }


            var claimInfoCollection = (from DataRow dr in dtt.AsEnumerable()
                                       where
                                           dr.Field<string>("Claim Number") == claimNumber.Value &&
                                           dr.Field<string>("RowType") == "CLAIM_MOTOR"
                                           && dr.Field<string>("RowCount") == claimNumber.Key
                                       select dr).ToList();

            if (claimInfoCollection.Any())
            {
                foreach (var claimInfoRow in claimInfoCollection)
                {
                    if (!string.IsNullOrEmpty(claimInfoRow[15].ToString()))
                    {
                        switch (claimInfoRow[15].ToString().ToUpper())
                        {
                            case "OPEN":
                                claimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                break;
                            case "CLOSED":
                                claimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                break;
                            case "REOPENED":
                                claimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                                break;
                            default:
                                claimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                                break;
                        }
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[21].ToString()))
                    {
                        claimInfo.ClaimCode = claimInfoRow[21].ToString();
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[13].ToString()))
                    {
                        //string incidentTempTimeDate = DateTime.Now.ToShortDateString();

                        //incidentDate = Convert.ToDateTime(incidentTempTimeDate);

                        //TimeSpan incidentTime;

                        //if (TimeSpan.TryParse(claimInfoRow[13].ToString(), out incidentTime))
                        //    claimInfo.IncidentTime = incidentTime;
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[32].ToString()))
                    {
                        claimInfo.IncidentLocation = claimInfoRow[32].ToString();
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[21].ToString()))
                    {
                        incidentCircumstances = claimInfoRow[21].ToString();
                        incidentCircumstances += " - ";
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[22].ToString()))
                    {
                        incidentCircumstances += claimInfoRow[22].ToString();
                        claimInfo.IncidentCircumstances = incidentCircumstances;
                    }
                    if (!string.IsNullOrEmpty(claimInfoRow[10].ToString()))
                    {
                        claimInfo.ClaimNotificationDate = _ConvertDate(claimInfoRow[10].ToString());
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[24].ToString()))
                    {
                        claimInfo.PaymentsToDate =
                            Convert.ToDecimal(claimInfoRow[24].ToString());
                    }

                    if (!string.IsNullOrEmpty(claimInfoRow[23].ToString()))
                    {
                        claimInfo.Reserve = Convert.ToDecimal(claimInfoRow[23].ToString());
                    }

                }
            }

            if (claimInfo != null)
            {
                claim.ExtraClaimInfo = claimInfo;
            }

            if (!string.IsNullOrEmpty(result[2].ToString()))
            {
                claim.ClaimNumber = (string)result[2];
            }

            claim.ClaimType_Id = (int)ClaimType.Motor;

            if (!string.IsNullOrEmpty(result[12].ToString()))
            {
                incidentDate = _ConvertDate(result[12].ToString());
                claim.IncidentDate = incidentDate;
            }

            if (!string.IsNullOrEmpty(result[13].ToString()))
            {
                if (TimeSpan.TryParse(result[13].ToString(), out incidentTime))
                    incidentDate = incidentDate.Add(incidentTime);
                claim.IncidentDate = incidentDate;
            }

            if (claim.ExtraClaimInfo != null && !string.IsNullOrEmpty(result[24].ToString()))
            {
                if (Convert.ToDecimal(result[24]) > (decimal)0.00)
                    claim.ExtraClaimInfo.PaymentsToDate = Convert.ToDecimal(result[24]);
            }

            if (claim.ExtraClaimInfo != null && !string.IsNullOrEmpty(result[23].ToString()))
            {
                if (Convert.ToDecimal(result[23]) > (decimal)0.00)
                    claim.ExtraClaimInfo.Reserve = Convert.ToDecimal(result[23]);
            }

            // Check for claim organisations
            #region ClaimOrganisations
            //THPRPR
            var distinctTHPRPR = (from DataRow dr in dtt.AsEnumerable()
                                  where
                                      dr.Field<string>("Claim Number") == claimNumber.Value &&
                                      dr[4].ToString().Contains("THPRPR") &&
                                      dr.Field<string>("RowCount") == claimNumber.Key
                                  select dr[4]).ToList().Distinct();


            foreach (var thprpr in distinctTHPRPR)
            {
                PipelineOrganisation organisation = new PipelineOrganisation();
                organisation.OrganisationType_Id = (int)OrganisationType.UndefinedSupplier;
                //??organisation.ClaimInfo = null;

                var getTHPRPRCollection = (from DataRow dr in dtt.AsEnumerable()
                                           where
                                               dr.Field<string>("Claim Number") == claimNumber.Value &&
                                               dr[4].ToString().Contains(thprpr.ToString()) &&
                                               dr.Field<string>("RowCount") == claimNumber.Key
                                           select dr).ToList();

                foreach (var dataRowTHPRPR in getTHPRPRCollection)
                {
                    switch (dataRowTHPRPR[1].ToString())
                    {
                        case "PARTY_MOTOR":
                            #region OrgGeneral
                            if (!string.IsNullOrEmpty(dataRowTHPRPR[26].ToString()))
                            {
                                organisation.OrganisationName = dataRowTHPRPR[26].ToString();
                            }
                            if (!string.IsNullOrEmpty(dataRowTHPRPR[33].ToString()))
                            {
                                organisation.VatNumber = dataRowTHPRPR[33].ToString();
                            }
                            break;
                            #endregion
                        case "CONTACT":
                            #region Contact
                            if (!string.IsNullOrEmpty(dataRowTHPRPR[6].ToString()))
                            {
                                switch (dataRowTHPRPR[6].ToString())
                                {
                                    case "WRKEML":
                                    case "GENEML":
                                    case "HOMEML":
                                        organisation.EmailAddresses.Add(new PipelineEmail() { EmailAddress = dataRowTHPRPR[7].ToString() });
                                        //organisation.Email = dataRowTHPRPR[7].ToString();
                                        break;
                                }

                                if (dataRowTHPRPR[6].ToString() == "HOMTEL")
                                {
                                    organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowTHPRPR[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                    //organisation.Telephone1 = dataRowTHPRPR[7].ToString();
                                }

                                if (dataRowTHPRPR[6].ToString() == "WRKTEL")
                                {
                                    organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowTHPRPR[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                    //organisation.Telephone2 = dataRowTHPRPR[7].ToString();
                                }

                                if (dataRowTHPRPR[6].ToString() == "MOBILE")
                                {
                                    organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowTHPRPR[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                    //organisation.Telephone3 = dataRowTHPRPR[7].ToString();
                                }
                            }
                            break;
                            #endregion
                        case "ADDRESS":
                            #region Address
                            PipelineAddress address = new PipelineAddress();

                            address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;

                            if (!string.IsNullOrEmpty(dataRowTHPRPR[17].ToString()))
                            {
                                address.PO2A_LinkData.StartOfResidency = _ConvertDate(dataRowTHPRPR[17].ToString());
                            }

                            if (!string.IsNullOrEmpty(dataRowTHPRPR[18].ToString()))
                            {
                                address.PO2A_LinkData.EndOfResidency = _ConvertDate(dataRowTHPRPR[18].ToString());
                            }

                            address = AddressFormatHelper.PopulateAddress(address, dataRowTHPRPR[7].ToString(), dataRowTHPRPR[8].ToString(), dataRowTHPRPR[9].ToString(), dataRowTHPRPR[10].ToString(), dataRowTHPRPR[11].ToString(), dataRowTHPRPR[12].ToString(), dataRowTHPRPR[13].ToString(), dataRowTHPRPR[14].ToString());

                            organisation.Addresses.Add(address);
                            break;
                            #endregion
                    }
                }
                claim.Organisations.Add(organisation);
            }

            //TPC
            var distinctTpc = (from DataRow dr in dtt.AsEnumerable()
                               where
                                   dr.Field<string>("Claim Number") == claimNumber.Value &&
                                   dr[4].ToString().Contains("TPC") &&
                                   dr.Field<string>("RowCount") == claimNumber.Key
                               select dr[4]).ToList().Distinct();


            foreach (var tpc in distinctTpc)
            {
                PipelineOrganisation organisation = new PipelineOrganisation();
                organisation.OrganisationType_Id = (int)OrganisationType.UndefinedSupplier;
                //??organisation.ClaimInfo = null;

                var getTPCCollection = (from DataRow dr in dtt.AsEnumerable()
                                        where
                                            dr.Field<string>("Claim Number") == claimNumber.Value &&
                                            dr[4].ToString().Contains(tpc.ToString()) &&
                                            dr.Field<string>("RowCount") == claimNumber.Key
                                        select dr).ToList();


                foreach (var dataRowTPC in getTPCCollection)
                {


                    switch (dataRowTPC[1].ToString())
                    {
                        case "PARTY_MOTOR":
                            #region OrgGeneral
                            if (!string.IsNullOrEmpty(dataRowTPC[26].ToString()))
                            {
                                organisation.OrganisationName = dataRowTPC[26].ToString();
                            }
                            if (!string.IsNullOrEmpty(dataRowTPC[33].ToString()))
                            {
                                organisation.VatNumber = dataRowTPC[33].ToString();
                            }
                            break;
                            #endregion
                        case "CONTACT":
                            #region Contact
                            if (!string.IsNullOrEmpty(dataRowTPC[6].ToString()))
                            {
                                switch (dataRowTPC[6].ToString())
                                {
                                    case "WRKEML":
                                    case "GENEML":
                                    case "HOMEML":
                                        organisation.EmailAddresses.Add(new PipelineEmail() { EmailAddress = dataRowTPC[7].ToString() });
                                        //organisation.Email = dataRowTPC[7].ToString();
                                        break;
                                }

                                if (dataRowTPC[6].ToString() == "HOMTEL")
                                {
                                    organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowTPC[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                    //organisation.Telephone1 = dataRowTPC[7].ToString();
                                }

                                if (dataRowTPC[6].ToString() == "WRKTEL")
                                {
                                    organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowTPC[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                    //organisation.Telephone2 = dataRowTPC[7].ToString();
                                }

                                if (dataRowTPC[6].ToString() == "MOBILE")
                                {
                                    organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowTPC[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                    //organisation.Telephone3 = dataRowTPC[7].ToString();
                                }
                            }
                            break;
                            #endregion
                        case "ADDRESS":
                            #region Address
                            PipelineAddress address = new PipelineAddress();

                            address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;

                            if (!string.IsNullOrEmpty(dataRowTPC[17].ToString()))
                            {
                                address.PO2A_LinkData.StartOfResidency = _ConvertDate(dataRowTPC[17].ToString());

                                //string StartOfResidency = dataRowTPC[17].ToString().Substring(6, 2) + "/" +
                                //                                  dataRowTPC[17].ToString().Substring(4, 2) + "/" +
                                //                                  dataRowTPC[17].ToString().Substring(0, 4);

                                //address.StartOfResidency = Convert.ToDateTime(startOfResidencyTempDate);
                            }

                            if (!string.IsNullOrEmpty(dataRowTPC[18].ToString()))
                            {
                                address.PO2A_LinkData.EndOfResidency = _ConvertDate(dataRowTPC[18].ToString());

                                //string endOfResidencyTempDate = dataRowTPC[18].ToString().Substring(6, 2) + "/" +
                                //                                dataRowTPC[18].ToString().Substring(4, 2) + "/" +
                                //                                dataRowTPC[18].ToString().Substring(0, 4);

                                //address.EndOfResidency = Convert.ToDateTime(endOfResidencyTempDate);
                            }


                            address = AddressFormatHelper.PopulateAddress(address, dataRowTPC[7].ToString(), dataRowTPC[8].ToString(), dataRowTPC[9].ToString(), dataRowTPC[10].ToString(), dataRowTPC[11].ToString(), dataRowTPC[12].ToString(), dataRowTPC[13].ToString(), dataRowTPC[14].ToString());
                            
                            organisation.Addresses.Add(address);
                            break;
                            #endregion
                    }

                }
                claim.Organisations.Add(organisation);
            }

            #endregion
            return claim;
        }

        public PipelinePolicy GetPolicyDetails(DataTable dtt, KeyValuePair<string, string> claimNumber)
        {
            PipelinePolicy policy = new PipelinePolicy();

            try
            {

                var result = (from DataRow dr in dtt.AsEnumerable()
                              where
                                  dr.Field<string>("Claim Number") == claimNumber.Value &&
                                  dr.Field<string>("RowType") == "CLAIM_MOTOR"
                                  && dr.Field<string>("RowCount") == claimNumber.Key
                              select dr).FirstOrDefault();

                if (!string.IsNullOrEmpty(result[18].ToString()))
                {
                    policy.PolicyNumber = (string)result[18];
                }

            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetPolicyDetails(Claim Number " + claimNumber + ": " + ex);
            }

            return policy;
        }

        public List<VehicleExt> GetVehicleDetails(DataTable dtt, KeyValuePair<string, string> claimNumber)
        {
            List<VehicleExt> vehicles = new List<VehicleExt>();
            List<string> partyTypeCodes = new List<string>();
            List<string> subPartyType = new List<string>();

            try
            {

                var result = (from DataRow dr in dtt.AsEnumerable()
                              where
                                  dr.Field<string>("Claim Number") == claimNumber.Value
                                  && dr.Field<string>("RowType") == "VEHICLE"
                                 && dr.Field<string>("RowCount") == claimNumber.Key
                              select dr).ToList();

                if (result != null)
                {
                    foreach (var dataRow in result)
                    {
                        VehicleExt vehicle = new VehicleExt();

                        if (!string.IsNullOrEmpty(dataRow[6].ToString()))
                        {
                            vehicle.VehicleTypeCode = (string)dataRow[6];

                            // Find VehicleInvolvementGroup
                            var assetRelationshipCollection = (from DataRow dr in dtt.AsEnumerable()
                                                               where
                                                                   dr.Field<string>("Claim Number") == claimNumber.Value &&
                                                                   dr.Field<string>("RowType") == "ASSET_RELATIONSHIP" &&
                                                                   dr[7].ToString() == vehicle.VehicleTypeCode &&
                                                                   dr.Field<string>("RowCount") == claimNumber.Key
                                                               select dr).ToList();

                            if (assetRelationshipCollection != null)
                            {
                                foreach (var assetRelationshipRow in assetRelationshipCollection)
                                {
                                    if (!string.IsNullOrEmpty(assetRelationshipRow[4].ToString())) { partyTypeCodes.Add(assetRelationshipRow[4].ToString()); }
                                    if (!string.IsNullOrEmpty(assetRelationshipRow[6].ToString())) { subPartyType.Add(assetRelationshipRow[6].ToString()); }
                                }
                            }

                            #region Vehicle Involvement Groups (Incident2VehcileLinkType)

                            if (vehicle.VehicleTypeCode.Contains("INV"))
                            {
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                            }

                            if (vehicle.VehicleTypeCode.Contains("TPV"))
                            {
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                            }

                            #endregion

                        }
                        if (!string.IsNullOrEmpty(dataRow[7].ToString()))
                        {
                            vehicle.VehicleRegistration = (string)dataRow[7];
                        }

                        #region Color
                        if (!string.IsNullOrEmpty(dataRow[26].ToString()))
                        {
                            GetVehicleColor(dataRow, vehicle);
                        }
                        #endregion

                        if (!string.IsNullOrEmpty(dataRow[14].ToString()))
                        {
                            vehicle.VehicleMake = (string)dataRow[14];
                        }
                        if (!string.IsNullOrEmpty(dataRow[15].ToString()))
                        {
                            vehicle.VehicleModel = (string)dataRow[15];
                        }
                        if (!string.IsNullOrEmpty(dataRow[9].ToString()))
                        {
                            vehicle.EngineCapacity = (string)dataRow[9];
                        }
                        if (!string.IsNullOrEmpty(dataRow[25].ToString()))
                        {
                            vehicle.VIN = (string)dataRow[25];
                        }

                        vehicles.Add(vehicle);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetVehicleDetails(Claim Number " + claimNumber + ": " + ex);
            }
            return vehicles;
        }

        private static void GetVehicleColor(DataRow dataRow, VehicleExt vehicle)
        {
            
            switch (dataRow[26].ToString().ToUpper())
            {
                case "BLUE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Blue;
                    break;
                case "BEIGE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Beige;
                    break;
                case "BLACK":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Black;
                    break;
                case "BRONZE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Bronze;
                    break;
                case "BROWN":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Brown;
                    break;
                case "CREAM":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Cream;
                    break;
                case "GOLD":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Gold;
                    break;
                case "GRAPHITE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Graphite;
                    break;
                case "GREEN":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Green;
                    break;
                case "GREY":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Grey;
                    break;
                case "LILAC":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Lilac;
                    break;
                case "MAROON":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Maroon;
                    break;
                case "MAUVE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Mauve;
                    break;
                case "ORANGE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Orange;
                    break;
                case "PINK":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Pink;
                    break;
                case "PURPLE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Purple;
                    break;
                case "RED":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Red;
                    break;
                case "SILVER":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Silver;
                    break;
                case "TURQUOISE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Turquoise;
                    break;
                case "WHITE":
                    vehicle.VehicleColour_Id = (int)VehicleColour.White;
                    break;
                case "YELLOW":
                    vehicle.VehicleColour_Id = (int)VehicleColour.Yellow;
                    break;
                default:
                    vehicle.VehicleColour_Id = (int)VehicleColour.Unknown;
                    break;
            }
        }

        public List<PipelinePerson> GetNoneVehiclePeopleDetails(DataTable dtt, KeyValuePair<string, string> claimNumber)
        {
            List<PipelinePerson> people = new List<PipelinePerson>();

            try
            {
                // WITNESS (No Vehicle)
                var distinctWitness = (from DataRow dr in dtt.AsEnumerable()
                                       where
                                           dr.Field<string>("Claim Number") == claimNumber.Value &&
                                           dr[4].ToString().Contains("WIT") &&
                                           dr.Field<string>("RowCount") == claimNumber.Key
                                       select dr[4]).ToList().Distinct();


                foreach (var witness in distinctWitness)
                {

                    PipelinePerson person = new PipelinePerson();
                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;
                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                    //??person.ClaimInfo = null;

                    var getWitnessPartyMotorCollection = (from DataRow dr in dtt.AsEnumerable()
                                                          where
                                                              dr.Field<string>("Claim Number") == claimNumber.Value &&
                                                              dr[4].ToString().Contains(witness.ToString()) &&
                                                              dr.Field<string>("RowType") == "PARTY_MOTOR" &&
                                                              dr.Field<string>("RowCount") == claimNumber.Key
                                                          select dr).ToList();

                    if (getWitnessPartyMotorCollection.Any())
                    {
                        foreach (var dataRow in getWitnessPartyMotorCollection)
                        {
                            #region Saltutation

                            if (!string.IsNullOrEmpty(dataRow[7].ToString()))
                            {
                                switch (dataRow[7].ToString().ToUpper())
                                {
                                    case "MR":
                                        person.Salutation_Id = (int)Salutation.Mr;
                                        break;
                                    case "MRS":
                                        person.Salutation_Id = (int)Salutation.Mrs;
                                        break;
                                    case "MS":
                                        person.Salutation_Id = (int)Salutation.Ms;
                                        break;
                                    case "MISS":
                                        person.Salutation_Id = (int)Salutation.Miss;
                                        break;
                                    case "MASTER":
                                        person.Salutation_Id = (int)Salutation.Master;
                                        break;
                                    case "DR":
                                        person.Salutation_Id = (int)Salutation.Dr;
                                        break;
                                    case "Rev":
                                        person.Salutation_Id = (int)Salutation.Reverend;
                                        break;
                                    default:
                                        person.Salutation_Id = (int)Salutation.Unknown;
                                        break;
                                }
                            }

                            #endregion

                            if (!string.IsNullOrEmpty(dataRow[9].ToString()))
                            {
                                person.FirstName = dataRow[9].ToString();
                            }
                            if (!string.IsNullOrEmpty(dataRow[10].ToString()))
                            {
                                person.MiddleName = dataRow[10].ToString();
                            }
                            if (!string.IsNullOrEmpty(dataRow[11].ToString()))
                            {
                                person.LastName = dataRow[11].ToString();
                            }
                            if (!string.IsNullOrEmpty(dataRow[12].ToString()))
                            {
                                bool isLetter = Char.IsLetter(dataRow[12].ToString()[0]);
                                if(!isLetter)
                                    person.DateOfBirth = _ConvertDate(dataRow[12].ToString());
                            }

                            if (!string.IsNullOrEmpty(dataRow[15].ToString()))
                            {
                                switch (dataRow[15].ToString().ToUpper())
                                {
                                    case "MALE":
                                        person.Gender_Id = (int)Gender.Male;
                                        break;
                                    case "FEMALE":
                                        person.Gender_Id = (int)Gender.Female;
                                        break;
                                    default:
                                        person.Gender_Id = (int)Gender.Unknown;
                                        break;
                                }
                            }

                            if (!string.IsNullOrEmpty(dataRow[17].ToString()))
                                person.Nationality = dataRow[17].ToString();
                        }


                        var contactCollection = (from DataRow dr in dtt.AsEnumerable()
                                                 where
                                                     dr.Field<string>("Claim Number") == claimNumber.Value &&
                                                     dr.Field<string>("RowType") == "CONTACT" &&
                                                     dr[4].ToString().Contains(witness.ToString())
                                                     && dr.Field<string>("RowCount") == claimNumber.Key
                                                 select dr).ToList();

                        if (contactCollection.Any())
                        {
                            foreach (var dataRowContact in contactCollection)
                            {
                                if (!string.IsNullOrEmpty(dataRowContact[6].ToString()))
                                {
                                    switch (dataRowContact[6].ToString())
                                    {
                                        case "WRKEML":
                                        case "GENEML":
                                        case "HOMEML":
                                            person.EmailAddresses.Add(new PipelineEmail() { EmailAddress = dataRowContact[7].ToString() });
                                            //person.EmailAddress = dataRowContact[7].ToString();
                                            break;
                                    }

                                    if (dataRowContact[6].ToString() == "HOMTEL")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                        //person.LandlineTelephone = dataRowContact[7].ToString();
                                    }

                                    if (dataRowContact[6].ToString() == "WRKTEL")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                        //person.WorkTelephone = dataRowContact[7].ToString();
                                    }

                                    if (dataRowContact[6].ToString() == "MOBILE")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                        //person.MobileTelephone = dataRowContact[7].ToString();
                                    }

                                    if (dataRowContact[6].ToString() == "OTHTEL")
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                                        //person.OtherTelephone = dataRowContact[7].ToString();
                                    }
                                }
                            }
                        }

                        #region Address

                        var addressCollection = (from DataRow dr in dtt.AsEnumerable()
                                                 where
                                                     dr.Field<string>("Claim Number") == claimNumber.Value &&
                                                     dr.Field<string>("RowType") == "ADDRESS" &&
                                                     dr[4].ToString().Contains(witness.ToString())
                                                     && dr.Field<string>("RowCount") == claimNumber.Key
                                                 select dr).ToList();

                        PipelineAddress address = new PipelineAddress();
                        if (addressCollection.Any())
                        {

                            foreach (var dataRowAddress in addressCollection)
                            {


                                if (!string.IsNullOrEmpty(dataRowAddress[6].ToString()))
                                {
                                    if (dataRowAddress[6].ToString() == "CURRNT")
                                        address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[17].ToString()))
                                {
                                    address.PO2A_LinkData.StartOfResidency = _ConvertDate(dataRowAddress[17].ToString());
                                }

                                if (!string.IsNullOrEmpty(dataRowAddress[18].ToString()))
                                {
                                    address.PO2A_LinkData.EndOfResidency = _ConvertDate(dataRowAddress[18].ToString());
                                }

                                address = AddressFormatHelper.PopulateAddress(address, dataRowAddress[7].ToString(), dataRowAddress[8].ToString(), dataRowAddress[9].ToString(), dataRowAddress[10].ToString(), dataRowAddress[11].ToString(), dataRowAddress[12].ToString(), dataRowAddress[13].ToString(), dataRowAddress[14].ToString());


                                person.Addresses.Add(address);
                            }
                        }

                        #endregion

                        #region Organistion

                        var organisationCollection = (from DataRow dr in dtt.AsEnumerable()
                                                      where
                                                          dr.Field<string>("Claim Number") == claimNumber.Value &&
                                                          dr.Field<string>("RowType") == "PARTY_MOTOR" &&
                                                          dr[26].ToString() != ""
                                                          && dr[4].ToString().Contains(witness.ToString())
                                                          && dr.Field<string>("RowCount") == claimNumber.Key
                                                      select dr).ToList();


                        if (organisationCollection.Any())
                        {
                            foreach (var dataRowOrganisation in organisationCollection)
                            {
                                PipelineOrganisation organisation = new PipelineOrganisation();

                                if (!string.IsNullOrEmpty(dataRowOrganisation[26].ToString()))
                                {
                                    organisation.OrganisationName = dataRowOrganisation[26].ToString();
                                }
                                if (!string.IsNullOrEmpty(dataRowOrganisation[28].ToString()))
                                {
                                    organisation.RegisteredNumber = dataRowOrganisation[28].ToString();
                                }
                                if (!string.IsNullOrEmpty(dataRowOrganisation[33].ToString()))
                                {
                                    organisation.VatNumber = dataRowOrganisation[33].ToString();
                                }


                                var organisationContactCollection = (from DataRow dr in dtt.AsEnumerable()
                                                                     where
                                                                         dr.Field<string>("Claim Number") ==
                                                                         claimNumber.Value &&
                                                                         dr.Field<string>("RowType") == "CONTACT" &&
                                                                         dr[4].ToString() ==
                                                                         dataRowOrganisation[4].ToString()
                                                                         &&
                                                                         dr.Field<string>("RowCount") ==
                                                                         claimNumber.Key
                                                                     select dr).ToList();

                                if (organisationContactCollection.Any())
                                {

                                    foreach (
                                        var dataRowOrganisationContactCollection in organisationContactCollection)
                                    {
                                        if (
                                            !string.IsNullOrEmpty(dataRowOrganisationContactCollection[7].ToString()))
                                        {
                                            switch (dataRowOrganisationContactCollection[7].ToString())
                                            {
                                                case "WRKEML":
                                                case "GENEML":
                                                case "HOMEML":
                                                    organisation.EmailAddresses.Add(new PipelineEmail() { EmailAddress = dataRowOrganisationContactCollection[7].ToString() });
                                                    //organisation.Email = dataRowOrganisationContactCollection[7].ToString();
                                                    break;
                                            }

                                            if (dataRowOrganisationContactCollection[7].ToString() == "CONTEL")
                                            {
                                                organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                                //organisation.Telephone1 = dataRowOrganisationContactCollection[7].ToString();
                                            }

                                            if (dataRowOrganisationContactCollection[7].ToString() == "BUSTEL")
                                            {
                                                organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                                //organisation.Telephone2 = dataRowOrganisationContactCollection[7].ToString();
                                            }

                                            if (dataRowOrganisationContactCollection[7].ToString() == "MOBILE")
                                            {
                                                organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                                //organisation.Telephone3 = dataRowOrganisationContactCollection[7].ToString();
                                            }
                                        }
                                    }
                                }
                                //??organisation.ClaimInfo = null;
                                person.Organisations.Add(organisation);
                            }
                        }

                        people.Add(person);
                    }

                    #endregion
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetNoneVehiclePeopleDetails(Claim Number " + claimNumber + ": " + ex);
            }

            return people;
        }

        public List<PipelinePerson> GetVehiclePeopleDetails(DataTable dtt, KeyValuePair<string, string> claimNumber, string vehicleTypeCode)
        {
            List<PipelinePerson> people = new List<PipelinePerson>();

            try
            {
                Dictionary<string, string> DictParties = new Dictionary<string, string>();
                string occupation = null;


                var assetRelationshipCollection = (from DataRow dr in dtt.AsEnumerable()
                                                   where
                                                       dr.Field<string>("Claim Number") == claimNumber.Value &&
                                                       dr.Field<string>("RowType") == "ASSET_RELATIONSHIP" &&
                                                       dr[7].ToString() == vehicleTypeCode &&
                                                       dr.Field<string>("RowCount") == claimNumber.Key
                                                   select dr).ToList();

                if (assetRelationshipCollection != null)
                {
                    foreach (var dataRow in assetRelationshipCollection)
                    {
                        DictParties.Add(dataRow[4].ToString(), dataRow[6].ToString());
                    }
                }

                if (DictParties != null)
                {
                    foreach (var dictItem in DictParties)
                    {
                        var partyMotorCollection = (from DataRow dr in dtt.AsEnumerable()
                                                    where
                                                        dr.Field<string>("Claim Number") == claimNumber.Value &&
                                                        dr.Field<string>("RowType") == "PARTY_MOTOR" &&
                                                        dr[4].ToString() == dictItem.Key
                                                        && !dr[4].ToString().Contains("TPC") &&
                                                        !dr[4].ToString().Contains("THPRPR")
                                                        && dr.Field<string>("RowCount") == claimNumber.Key
                                                    select dr).ToList();

                        if (partyMotorCollection != null)
                        {
                            foreach (var dataRow in partyMotorCollection)
                            {
                                PipelinePerson person = new PipelinePerson();
                                if (dictItem.Key == "CLM0001")
                                {
                                    PipelineClaimInfo ci = GetPersonClaimInfo(dtt, claimNumber, dataRow);
                                }

                                #region PartyTypes

                                if (vehicleTypeCode.Contains("INV") && dictItem.Key.Contains("CLM") &&
                                    dictItem.Value.Contains("DRV"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (vehicleTypeCode.Contains("INV") && dictItem.Key.Contains("CLM") &&
                                    dictItem.Value.Contains("OWN"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                if (vehicleTypeCode.Contains("INV") && dictItem.Key.Contains("CLM") &&
                                    dictItem.Value.Contains("PAS"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                }

                                if (vehicleTypeCode.Contains("INV") && dictItem.Key.Contains("THP") &&
                                    dictItem.Value.Contains("OWN"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                if (vehicleTypeCode.Contains("INV") && dictItem.Key.Contains("THP") &&
                                    dictItem.Value.Contains("PAS"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                }

                                if (vehicleTypeCode.Contains("INV") && dictItem.Key.Contains("TVO") &&
                                    dictItem.Value.Contains("DRV"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (vehicleTypeCode.Contains("INV") && dictItem.Key.Contains("TVO") &&
                                    dictItem.Value.Contains("OWN"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                if (vehicleTypeCode.Contains("TPV") && dictItem.Key.Contains("THP") &&
                                    dictItem.Value.Contains("DRV"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (vehicleTypeCode.Contains("TPV") && dictItem.Key.Contains("TVO") &&
                                    dictItem.Value.Contains("DRV"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (vehicleTypeCode.Contains("TPV") && dictItem.Key.Contains("TVO") &&
                                    dictItem.Value.Contains("OWN"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                if (vehicleTypeCode.Contains("TPV") && dictItem.Key.Contains("THP") &&
                                    dictItem.Value.Contains("OWN"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                }

                                if (vehicleTypeCode.Contains("TPV") && dictItem.Key.Contains("THP") &&
                                    dictItem.Value.Contains("PAS"))
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                }


                                #endregion

                                #region Saltutation

                                if (!string.IsNullOrEmpty(dataRow[7].ToString()))
                                {
                                    switch (dataRow[7].ToString().ToUpper())
                                    {
                                        case "MR":
                                            person.Salutation_Id = (int)Salutation.Mr;
                                            break;
                                        case "MRS":
                                            person.Salutation_Id = (int)Salutation.Mrs;
                                            break;
                                        case "MS":
                                            person.Salutation_Id = (int)Salutation.Ms;
                                            break;
                                        case "MISS":
                                            person.Salutation_Id = (int)Salutation.Miss;
                                            break;
                                        case "MASTER":
                                            person.Salutation_Id = (int)Salutation.Master;
                                            break;
                                        case "DR":
                                            person.Salutation_Id = (int)Salutation.Dr;
                                            break;
                                        case "Rev":
                                            person.Salutation_Id = (int)Salutation.Reverend;
                                            break;
                                        default:
                                            person.Salutation_Id = (int)Salutation.Unknown;
                                            break;
                                    }
                                }

                                #endregion

                                if (!string.IsNullOrEmpty(dataRow[9].ToString()))
                                {
                                    person.FirstName = dataRow[9].ToString();
                                }
                                if (!string.IsNullOrEmpty(dataRow[10].ToString()))
                                {
                                    person.MiddleName = dataRow[10].ToString();
                                }
                                if (!string.IsNullOrEmpty(dataRow[11].ToString()))
                                {
                                    person.LastName = dataRow[11].ToString();
                                }

                                if (!string.IsNullOrEmpty(dataRow[12].ToString()))
                                {

                                    bool isLetter = Char.IsLetter(dataRow[12].ToString()[0]);
                                    if (!isLetter)
                                        person.DateOfBirth = _ConvertDate(dataRow[12].ToString());
                                }

                                if (!string.IsNullOrEmpty(dataRow[15].ToString()))
                                {
                                    switch (dataRow[15].ToString().ToUpper())
                                    {
                                        case "MALE":
                                            person.Gender_Id = (int)Gender.Male;
                                            break;
                                        case "FEMALE":
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                        default:
                                            person.Gender_Id = (int)Gender.Unknown;
                                            break;
                                    }
                                }

                                if (!string.IsNullOrEmpty(dataRow[17].ToString()))
                                    person.Nationality = dataRow[17].ToString();


                                var identificationCollection = (from DataRow dr in dtt.AsEnumerable()
                                                                where
                                                                    dr.Field<string>("Claim Number") ==
                                                                    claimNumber.Value &&
                                                                    dr.Field<string>("RowType") == "IDENTIFICATION" &&
                                                                    dr[4].ToString() == dictItem.Key
                                                                    && dr.Field<string>("RowCount") == claimNumber.Key
                                                                select dr).ToList();

                                if (identificationCollection != null)
                                {
                                    foreach (var dataRowIdent in identificationCollection)
                                    {
                                        if (!string.IsNullOrEmpty(dataRowIdent[7].ToString()))
                                            person.NINumbers.Add(new PipelineNINumber() { NINumber1 = dataRowIdent[7].ToString() });
                                            //person.NINumber = dataRowIdent[7].ToString();

                                        if (!string.IsNullOrEmpty(dataRowIdent[9].ToString()))
                                            person.DrivingLicenseNumbers.Add(new PipelineDrivingLicense() { DriverNumber = dataRowIdent[9].ToString() });
                                            //person.DrivingLicenseNumber = dataRowIdent[9].ToString();

                                        if (!string.IsNullOrEmpty(dataRowIdent[8].ToString()))
                                            person.PassportNumbers.Add(new PipelinePassport() { PassportNumber = dataRowIdent[8].ToString() });
                                           // person.PassportNumber = dataRowIdent[8].ToString();
                                    }
                                }


                                var occupationCollection = (from DataRow dr in dtt.AsEnumerable()
                                                            where
                                                                dr.Field<string>("Claim Number") == claimNumber.Value &&
                                                                dr.Field<string>("RowType") == "EMPLOYMENT" &&
                                                                dr[4].ToString() == dictItem.Key
                                                                && dr.Field<string>("RowCount") == claimNumber.Key
                                                            select dr).ToList();

                                if (occupationCollection != null)
                                {
                                    foreach (var dataRowOccupation in occupationCollection)
                                    {
                                        if (!string.IsNullOrEmpty(dataRowOccupation[7].ToString()))
                                        {
                                            occupation = dataRowOccupation[7].ToString();
                                            occupation += " - ";
                                        }

                                        if (!string.IsNullOrEmpty(dataRowOccupation[8].ToString()))
                                        {
                                            occupation += dataRowOccupation[8].ToString();
                                        }
                                        else
                                        {
                                            occupation += "Occupation Unknown";
                                        }

                                        if (occupation != null)
                                            person.Occupation = occupation;
                                    }
                                }

                                var contactCollection = (from DataRow dr in dtt.AsEnumerable()
                                                         where
                                                             dr.Field<string>("Claim Number") == claimNumber.Value &&
                                                             dr.Field<string>("RowType") == "CONTACT" &&
                                                             dr[4].ToString() == dictItem.Key
                                                             && dr.Field<string>("RowCount") == claimNumber.Key
                                                         select dr).ToList();

                                if (contactCollection != null)
                                {
                                    foreach (var dataRowContact in contactCollection)
                                    {
                                        if (!string.IsNullOrEmpty(dataRowContact[6].ToString()))
                                        {
                                            switch (dataRowContact[6].ToString())
                                            {
                                                case "WRKEML":
                                                case "GENEML":
                                                case "HOMEML":
                                                    person.EmailAddresses.Add(new PipelineEmail() { EmailAddress = dataRowContact[7].ToString() });
                                                    //person.EmailAddress = dataRowContact[7].ToString();
                                                    break;
                                            }

                                            if (dataRowContact[6].ToString() == "HOMTEL" ||
                                                dataRowContact[6].ToString() == "CONTEL")
                                            {
                                                person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                                //person.LandlineTelephone = dataRowContact[7].ToString();
                                            }

                                            if (dataRowContact[6].ToString() == "WRKTEL" ||
                                                dataRowContact[6].ToString() == "CONTEL")
                                            {
                                                person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                                //person.WorkTelephone = dataRowContact[7].ToString();
                                            }

                                            if (dataRowContact[6].ToString() == "MOBILE")
                                            {
                                                person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                                //person.MobileTelephone = dataRowContact[7].ToString();
                                            }

                                            if (dataRowContact[6].ToString() == "OTHTEL" ||
                                                dataRowContact[6].ToString() == "CORRESTEL")
                                            {
                                                person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowContact[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                                                //person.OtherTelephone = dataRowContact[7].ToString();
                                            }
                                        }
                                    }
                                }

                                #region Address

                                var addressCollection = (from DataRow dr in dtt.AsEnumerable()
                                                         where
                                                             dr.Field<string>("Claim Number") == claimNumber.Value &&
                                                             dr.Field<string>("RowType") == "ADDRESS" &&
                                                             dr[4].ToString() == dictItem.Key
                                                             && dr.Field<string>("RowCount") == claimNumber.Key
                                                         select dr).ToList();


                                if (addressCollection != null)
                                {

                                    foreach (var dataRowAddress in addressCollection)
                                    {
                                        PipelineAddress address = new PipelineAddress();

                                        address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                                        if (!string.IsNullOrEmpty(dataRowAddress[17].ToString()))
                                        {
                                            address.PO2A_LinkData.StartOfResidency = _ConvertDate(dataRowAddress[17].ToString());
                                        }

                                        if (!string.IsNullOrEmpty(dataRowAddress[18].ToString()))
                                        {
                                            address.PO2A_LinkData.EndOfResidency = _ConvertDate(dataRowAddress[18].ToString());
                                        }

                                        address = AddressFormatHelper.PopulateAddress(address, dataRowAddress[7].ToString(), dataRowAddress[8].ToString(), dataRowAddress[9].ToString(), dataRowAddress[10].ToString(), dataRowAddress[11].ToString(), dataRowAddress[12].ToString(), dataRowAddress[13].ToString(), dataRowAddress[14].ToString());

                                        person.Addresses.Add(address);

                                    }
                                }

                                #endregion

                                #region Organistion

                                var organisationCollection = (from DataRow dr in dtt.AsEnumerable()
                                                              where
                                                                  dr.Field<string>("Claim Number") == claimNumber.Value &&
                                                                  dr.Field<string>("RowType") == "PARTY_MOTOR" &&
                                                                  dr[26].ToString() != ""
                                                                  && !dr[4].ToString().Contains("TPC") &&
                                                                  !dr[4].ToString().Contains("THPRPR")
                                                                  && dr.Field<string>("RowCount") == claimNumber.Key
                                                              select dr).ToList();


                                if (organisationCollection != null)
                                {


                                    foreach (var dataRowOrganisation in organisationCollection)
                                    {
                                        PipelineOrganisation organisation = new PipelineOrganisation();

                                        if (!string.IsNullOrEmpty(dataRowOrganisation[26].ToString()))
                                        {
                                            organisation.OrganisationName = dataRowOrganisation[26].ToString();
                                        }
                                        if (!string.IsNullOrEmpty(dataRowOrganisation[28].ToString()))
                                        {
                                            organisation.RegisteredNumber = dataRowOrganisation[28].ToString();
                                        }
                                        if (!string.IsNullOrEmpty(dataRowOrganisation[33].ToString()))
                                        {
                                            organisation.VatNumber = dataRowOrganisation[33].ToString();
                                        }


                                        var organisationContactCollection = (from DataRow dr in dtt.AsEnumerable()
                                                                             where
                                                                                 dr.Field<string>("Claim Number") ==
                                                                                 claimNumber.Value &&
                                                                                 dr.Field<string>("RowType") ==
                                                                                 "CONTACT" &&
                                                                                 dr[4].ToString() ==
                                                                                 dataRowOrganisation[4].ToString()
                                                                                 &&
                                                                                 dr.Field<string>("RowCount") ==
                                                                                 claimNumber.Key
                                                                             select dr).ToList();

                                        if (organisationContactCollection != null)
                                        {

                                            foreach (
                                                var dataRowOrganisationContactCollection in
                                                    organisationContactCollection)
                                            {
                                                if (
                                                    !string.IsNullOrEmpty(
                                                        dataRowOrganisationContactCollection[7].ToString()))
                                                {
                                                    switch (dataRowOrganisationContactCollection[7].ToString())
                                                    {
                                                        case "WRKEML":
                                                        case "GENEML":
                                                        case "HOMEML":
                                                            organisation.EmailAddresses.Add(new PipelineEmail() { EmailAddress = dataRowOrganisationContactCollection[7].ToString() });
                                                             //organisation.Email =dataRowOrganisationContactCollection[7].ToString();
                                                            break;
                                                    }

                                                    if (dataRowOrganisationContactCollection[7].ToString() == "CONTEL")
                                                    {
                                                        organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                                        //organisation.Telephone1 =dataRowOrganisationContactCollection[7].ToString();
                                                    }

                                                    if (dataRowOrganisationContactCollection[7].ToString() == "BUSTEL")
                                                    {
                                                        organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                                        //organisation.Telephone2 =dataRowOrganisationContactCollection[7].ToString();
                                                    }

                                                    if (dataRowOrganisationContactCollection[7].ToString() == "MOBILE")
                                                    {
                                                        organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = dataRowOrganisationContactCollection[7].ToString(), TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                                        //organisation.Telephone3 =dataRowOrganisationContactCollection[7].ToString();
                                                    }
                                                }
                                            }
                                        }

                                        //??organisation.ClaimInfo = null;
                                        person.Organisations.Add(organisation);
                                    }

                                }

                                #endregion

                                people.Add(person);
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetVehiclePeopleDetails(Claim Number " + claimNumber + ": " + ex);
            }

            return people;
        }

        private PipelineClaimInfo GetPersonClaimInfo(DataTable dtt, KeyValuePair<string, string> claimNumber, DataRow dataRow)
        {
            try
            {
                bool? policeAttended = null;
                string policeNumber = null;
                string policeCrimeNumber = null;
                
                //DateTime incidentDate;
                //DateTime claimNotificationDate;
                string incidentCircumstances = null;

                if (!string.IsNullOrEmpty(dataRow[43].ToString()))
                {
                    policeNumber = dataRow[43].ToString();
                }
                if (!string.IsNullOrEmpty(dataRow[44].ToString()))
                {
                    policeCrimeNumber = dataRow[44].ToString();
                }

                if (policeNumber != null || policeCrimeNumber != null)
                {
                    policeAttended = true;
                }

                PipelineClaimInfo claimInfo = new PipelineClaimInfo();

                claimInfo.PoliceAttended = policeAttended;
                if (!string.IsNullOrEmpty(dataRow[44].ToString()))
                {
                    claimInfo.PoliceReference = dataRow[44].ToString();
                }


                var claimInfoCollection = (from DataRow dr in dtt.AsEnumerable()
                                           where
                                               dr.Field<string>("Claim Number") == claimNumber.Value &&
                                               dr.Field<string>("RowType") == "CLAIM_MOTOR"
                                               && dr.Field<string>("RowCount") == claimNumber.Key
                                           select dr).ToList();

                if (claimInfoCollection != null)
                {
                    foreach (var claimInfoRow in claimInfoCollection)
                    {
                        if (!string.IsNullOrEmpty(claimInfoRow[15].ToString()))
                        {
                            switch (claimInfoRow[15].ToString().ToUpper())
                            {
                                case "OPEN":
                                    claimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                    break;
                                case "CLOSED":
                                    claimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                    break;
                                case "REOPENED":
                                    claimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                                    break;
                                default:
                                    claimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                                    break;
                            }
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[21].ToString()))
                        {
                            claimInfo.ClaimCode = claimInfoRow[21].ToString();
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[13].ToString()))
                        {
                            //string incidentTempTimeDate = DateTime.Now.ToShortDateString();

                            //incidentDate = DateTime.Now; // Convert.ToDateTime(incidentTempTimeDate);

                            //TimeSpan incidentTime;

                            //if (TimeSpan.TryParse(claimInfoRow[13].ToString(), out incidentTime))
                            //    claimInfo.IncidentTime = incidentTime; // _incidentDate.Add(incidentTime);
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[32].ToString()))
                        {
                            claimInfo.IncidentLocation = claimInfoRow[32].ToString();
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[21].ToString()))
                        {
                            incidentCircumstances = claimInfoRow[21].ToString();
                            incidentCircumstances += " - ";
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[22].ToString()))
                        {
                            incidentCircumstances += claimInfoRow[22].ToString();
                            claimInfo.IncidentCircumstances = incidentCircumstances;
                        }
                        if (!string.IsNullOrEmpty(claimInfoRow[10].ToString()))
                        {
                            claimInfo.ClaimNotificationDate = _ConvertDate(claimInfoRow[10].ToString());
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[24].ToString()))
                        {
                            claimInfo.PaymentsToDate =
                                Convert.ToDecimal(claimInfoRow[24].ToString());
                        }

                        if (!string.IsNullOrEmpty(claimInfoRow[23].ToString()))
                        {
                            claimInfo.Reserve = Convert.ToDecimal(claimInfoRow[23].ToString());
                        }
                    }
                    return claimInfo;
                }

                else
                {
                    return null;
                }

                

            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method GetPersonClaimInfo(Claim Number " + claimNumber + ": " + ex);
            }
        }
    }
}
