﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.MappingService.FirstCentral.Motor.Model;
using MDA.MappingService.Interface;
using MDA.Common;
using MDA.Common.Server;
using MDA.MappingService;

namespace MDA.MappingService.FirstCentral.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx">Current Context containing DB connection to use</param>
        /// <param name="fs">FileStream containing the file to be loaded</param>
        /// <param name="statusTracking"></param>
        /// <param name="processClaim">CallBack function to call for each PipelineClaim created</param>
        /// <param name="ShowProgress">Show Progress callback</param>
        /// <param name="processingResults">Structure to hold ALL processing results</param>
        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking,
                                Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn,
                                out ProcessingResults processingResults)
        {
            
            processingResults = new ProcessingResults("Mapping");

            PipelineClaimBatch ret = new PipelineClaimBatch();

            try
            {
                //string strFile = fileStream.Name;

                getMotorClaimBatch(ctx, fs, statusTracking, ProcessClaimFn, processingResults);
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping First Central file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void getMotorClaimBatch(CurrentContext ctx, Stream filem, object statusTracking,
                                Func<CurrentContext, MDA.Pipeline.Model.PipelineMotorClaim, object, int> ProcessClaimFn,
                                ProcessingResults processingResults)
        {


            PipelineClaimBatch claimBatch = new PipelineClaimBatch();

            DataTable dtt = new DataTable();

            TranslateFromFile translateFromFile = new TranslateFromFile();


            // Put all claims in a data table
            dtt = translateFromFile.ClaimsToDataTable(filem);

            // Get Distinct Claim Rows
            Dictionary<string, string> claimBatchRows = translateFromFile.GetDistinctClaims(dtt);

            // Loop through claims
            foreach (var claimNumber in claimBatchRows)
            {

                //Console.WriteLine(claimNumber);

                PipelineMotorClaim claim = new PipelineMotorClaim();
                PipelineMotorClaim claimDetails = translateFromFile.GetClaimDetails(dtt, claimNumber);
                List<VehicleExt> claimVehicles = translateFromFile.GetVehicleDetails(dtt, claimNumber);
                PipelinePolicy policyDetails = translateFromFile.GetPolicyDetails(dtt, claimNumber);


                #region Claim

                claim.ClaimNumber = claimDetails.ClaimNumber;
                claim.ClaimType_Id = claimDetails.ClaimType_Id;
                claim.IncidentDate = claimDetails.IncidentDate;
                claim.ExtraClaimInfo = claimDetails.ExtraClaimInfo;
                claim.ExtraClaimInfo.PaymentsToDate = claimDetails.ExtraClaimInfo.PaymentsToDate;
                claim.ExtraClaimInfo.Reserve = claimDetails.ExtraClaimInfo.Reserve;
                claim.Organisations = claimDetails.Organisations;


                #endregion

                #region Policy
                claim.Policy.PolicyType_Id = (int)PolicyType.PersonalMotor;
                claim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.Unknown;
                claim.Policy.PolicyNumber = policyDetails.PolicyNumber;
                claim.Policy.PreviousNoFaultClaimsCount = policyDetails.PreviousNoFaultClaimsCount;

                #endregion

                #region Vehicle Level

                foreach (var claimVehicle in claimVehicles)
                {

                    List<PipelinePerson> vehiclePeople = translateFromFile.GetVehiclePeopleDetails(dtt, claimNumber,
                                                                                           claimVehicle
                                                                                               .VehicleTypeCode);
                    PipelineVehicle vehicle = new PipelineVehicle();
                    vehicle.VehicleType_Id = (int)VehicleType.Unknown;
                    vehicle.VehicleRegistration = claimVehicle.VehicleRegistration;
                    vehicle.VehicleColour_Id = claimVehicle.VehicleColour_Id;
                    vehicle.VehicleMake = claimVehicle.VehicleMake;
                    vehicle.VehicleModel = claimVehicle.VehicleModel;
                    vehicle.EngineCapacity = claimVehicle.EngineCapacity;
                    vehicle.VIN = claimVehicle.VIN;
                    vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = claimVehicle.I2V_LinkData.Incident2VehicleLinkType_Id;

                    if (vehiclePeople.Any())
                    {
                        foreach (var person in vehiclePeople)
                        {
                            vehicle.People.Add(person);
                        }
                    }
                    claim.Vehicles.Add(vehicle);
                }
                #endregion

                #region NoneVehiclePeople

                List<PipelinePerson> noneVehiclePeople = translateFromFile.GetNoneVehiclePeopleDetails(dtt, claimNumber);
                if (noneVehiclePeople.Any())
                {
                    foreach (var person in noneVehiclePeople)
                    {
                        PipelineVehicle noneVehicle = new PipelineVehicle();
                        noneVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;
                        noneVehicle.People.Add(person);
                        claim.Vehicles.Add(noneVehicle);
                    }
                }
                #endregion

                #region Insured Vehicle and Driver Check

                bool insuredVehicle = false;
                bool insuredDriver = false;

                foreach (var vehicle in claim.Vehicles)
                {
                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                    {
                        insuredVehicle = true;

                        foreach (var person in vehicle.People)
                        {
                            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                            {
                                insuredDriver = true;
                            }
                        }
                    }
                }

                if (insuredVehicle == false)
                {
                    PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                    defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                    if (insuredDriver == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                    }

                    claim.Vehicles.Add(defaultInsuredVehicle);
                }
                else
                {
                    if (insuredDriver == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        foreach (var vehicle in claim.Vehicles)
                        {
                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                            {

                                vehicle.People.Add(defaultInsuredDriver);

                            }

                        }

                    }
                }

                #endregion

                #region Have we seen this claim number before?
                List<PipelineMotorClaim> modifiedClaims = new List<PipelineMotorClaim>();

                foreach (var claimItem in claimBatch.Claims)
                {
                    if (claimItem.ClaimNumber == claimNumber.Value)
                    {
                        modifiedClaims.Add((PipelineMotorClaim)claimItem);
                    }
                }

                if (modifiedClaims.Any())
                {
                    foreach (var modifiedClaim in modifiedClaims)
                    {
                        claimBatch.Claims.Remove(modifiedClaim);
                    }
                }
                #endregion

                //claimBatch.Claims.Add(claim);

                if (ProcessClaimFn(ctx, claim, statusTracking) == -1) return;
            }

            //return claimBatch;
        }


        public override void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard, int settleTime, int batchSize, string postOp, string archiveFolder, Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {
            // Create structure to hold our new batch of claims
            List<object> batch = new List<object>();

            int batchCount = 0;
            string firstFileName = null;

            // Get a list of the files in the folderPath filtered by the wildCard. Search the top level only (don't go into child directories)
            string[] files = Directory.GetFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

            // If we find none, return
            if (files.Count() == 0) return;

            // If ANY files in that list have been modified in the last "settleTime" minutes return.  The directory is not stable, files may still being
            // written.  Return now and wait for the next check
            if (files.Where(x => File.GetCreationTime(x).AddMinutes(settleTime) > DateTime.Now).Any())
                return;

            // If we get here we have files to process
            foreach (var file in files)
            {
                // Create a filename to use for this batch.  We will just use the first file name in this case
                if (firstFileName == null)
                    firstFileName = Path.GetFileName(file);

                // Add this new claim to the batch
                batch.Add(file);

                #region If Batch full process it

                if (batch.Count >= batchSize)
                {
                    batchCount++;


                    string fileName = firstFileName;
                    string mimeType = "txt";
                    //string batchRef = "Batch" + batchCount;
                    //string batchRef = firstFileName;
                    //batchRef = batchRef.ToUpper().Replace(".TXT", "");

                    string clientName = "FCEN";
                    string date;

                    date = DateTime.Now.ToString();
                    date = date.Replace("/", "");
                    date = date.Replace(" ", "");

                    string batchRef = clientName + date;

                    using (FileStream fileStream = File.OpenRead(file))
                    {
                        MemoryStream stream = new MemoryStream();
                        stream.SetLength(fileStream.Length);
                        fileStream.Read(stream.GetBuffer(), 0, (int)fileStream.Length);

                        #region Process the batch (call the callback function)
                        processClientBatch(ctx, stream, fileName, mimeType, "Motor", batchRef);
                        #endregion
                    }


                    // Setup for a new batch and go round the loop again
                    batch.Clear();
                    firstFileName = null;
                }
                #endregion

                #region Process all the options we invented for our postOp functionality

                // We are passed the string we entered into config.  Now we have to code it!
                // In this case we have implemented RENAME, DELETE, ARCHIVE
                // ARCHIVE checks and (if needed) creates the destination folder. It also checks if the file already exists
                // in the destination folder (which would break the MOVE) and (if needed) adds a random string to make it unique
                switch (postOp.ToUpper())
                {
                    case "RENAME":
                        File.Move(file, file + ".old");
                        break;

                    case "DELETE":
                        File.Delete(file);
                        break;

                    case "ARCHIVE":
                        if (!Directory.Exists(archiveFolder))
                            Directory.CreateDirectory(archiveFolder);   // Create destination if needed

                        string fname = archiveFolder + "\\" + Path.GetFileName(file);  // build dest path

                        if (File.Exists(fname))  // Check if it already exists
                        {
                            fname += "." + Path.GetRandomFileName();  // Adds a random string to end of filename
                        }

                        File.Move(file, fname);
                        break;
                }
                #endregion

            }

            //#region Process the part full batch left at the end. Lazy cut & paste!!
            //if (batch.Count > 0)
            //{
            //    batchCount++;


            //    string fileName = firstFileName;
            //    string mimeType = "csv";
            //    string batchRef = "Batch" + batchCount;

            //    using (FileStream fileStream = File.OpenRead(file))
            //    {
            //        MemoryStream stream = new MemoryStream();
            //        stream.SetLength(fileStream.Length);
            //        fileStream.Read(stream.GetBuffer(), 0, (int)fileStream.Length);

            //        #region Process the batch (call the callback function)
            //        processClientBatch(ctx, stream, fileName, mimeType, "Motor", batchRef);
            //        #endregion
            //    }

            //    batch.Clear();
            //}
            //#endregion
        }
    }
}
