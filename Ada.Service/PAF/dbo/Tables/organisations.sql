﻿CREATE TABLE [dbo].[organisations] (
    [OrganisationKey]  VARCHAR (8)  NOT NULL,
    [PostcodeType]     VARCHAR (1)  NOT NULL,
    [OrganisationName] VARCHAR (60) NULL,
    [DepartmentName]   VARCHAR (60) NULL,
    CONSTRAINT [PK_organisations] PRIMARY KEY CLUSTERED ([OrganisationKey] ASC, [PostcodeType] ASC)
);

