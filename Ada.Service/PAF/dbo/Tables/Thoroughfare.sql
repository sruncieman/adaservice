﻿CREATE TABLE [dbo].[thoroughfare] (
    [ThoroughfareKey]  VARCHAR (8)  NOT NULL,
    [ThoroughfareName] VARCHAR (60) NULL,
    CONSTRAINT [PK_thoroughfare] PRIMARY KEY CLUSTERED ([ThoroughfareKey] ASC)
);



