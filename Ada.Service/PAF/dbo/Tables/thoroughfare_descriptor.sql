﻿CREATE TABLE [dbo].[thoroughfare_descriptor] (
    [ThoroughfareDescriptorKey] VARCHAR (50) NOT NULL,
    [ThoroughfareDescriptor]    VARCHAR (20) NULL,
    [ApprovedAbbreviation]      VARCHAR (6)  NULL,
    CONSTRAINT [PK_thoroughfare_descriptor] PRIMARY KEY CLUSTERED ([ThoroughfareDescriptorKey] ASC)
);

