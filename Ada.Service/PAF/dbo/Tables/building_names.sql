﻿CREATE TABLE [dbo].[building_names] (
    [BuildingNameKey] VARCHAR (8)  NOT NULL,
    [BuildingName]    VARCHAR (50) NULL,
    CONSTRAINT [PK_building_names] PRIMARY KEY CLUSTERED ([BuildingNameKey] ASC)
);

