﻿CREATE TABLE [dbo].[sub_building_name] (
    [SubBuildingNameKey] VARCHAR (8)  NOT NULL,
    [SubBuildingName]    VARCHAR (30) NULL,
    CONSTRAINT [PK_sub_building_name] PRIMARY KEY CLUSTERED ([SubBuildingNameKey] ASC)
);

