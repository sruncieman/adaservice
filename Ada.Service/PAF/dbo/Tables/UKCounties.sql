﻿CREATE TABLE [dbo].[UKCounties] (
    [CountryName] VARCHAR (50) NOT NULL,
    [CountyName]  VARCHAR (50) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_ukcounties_CountyName]
    ON [dbo].[UKCounties]([CountyName] ASC);

