﻿CREATE TABLE [dbo].[localities] (
    [LocalityKey]             VARCHAR (6)  NOT NULL,
    [PostTown]                VARCHAR (30) NULL,
    [DependentLocality]       VARCHAR (35) NULL,
    [DoubleDependentLocality] VARCHAR (35) NULL,
    CONSTRAINT [PK_localities] PRIMARY KEY CLUSTERED ([LocalityKey] ASC)
);

