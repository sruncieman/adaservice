﻿CREATE TABLE [dbo].[paf_address] (
    [AddressKey]                         VARCHAR (8) NOT NULL,
    [PostcodeOutwardCode]                VARCHAR (4) NULL,
    [PostcodeInwardCode]                 VARCHAR (3) NULL,
    [Postcode]                           VARCHAR (8) NULL,
    [LocalityKey]                        VARCHAR (6) CONSTRAINT [DF__paf_addre__Local__09DE7BCC] DEFAULT (NULL) NULL,
    [ThoroughfareKey]                    VARCHAR (8) CONSTRAINT [DF__paf_addre__Thoro__0AD2A005] DEFAULT (NULL) NULL,
    [ThoroughfareDescriptorKey]          VARCHAR (4) CONSTRAINT [DF__paf_addre__Thoro__0BC6C43E] DEFAULT (NULL) NULL,
    [DependentThoroughfareKey]           VARCHAR (8) NULL,
    [DependentThoroughfareDescriptorKey] VARCHAR (4) NULL,
    [BuildingNumber]                     INT         NULL,
    [BuildingNameKey]                    VARCHAR (8) NULL,
    [SubBuildingNameKey]                 VARCHAR (8) CONSTRAINT [DF__paf_addre__SubBu__108B795B] DEFAULT (NULL) NULL,
    [NumberOfHouseholds]                 VARCHAR (4) NULL,
    [OrganisationKey]                    VARCHAR (8) CONSTRAINT [DF__paf_addre__Organ__117F9D94] DEFAULT (NULL) NOT NULL,
    [PostcodeType]                       CHAR (1)    NOT NULL,
    [ConcatenationIndicator]             VARCHAR (1) NULL,
    [DeliveryPointSuffix]                VARCHAR (2) NULL,
    [SmallUserOrganisationIndicator]     VARCHAR (1) NULL,
    [POBoxNumber]                        VARCHAR (6) NULL,
    [PafUPRN]                            AS          (([AddressKey]+[OrganisationKey])+[PostcodeType]),
    CONSTRAINT [PK_paf_address_1] PRIMARY KEY CLUSTERED ([AddressKey] ASC, [OrganisationKey] ASC, [PostcodeType] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_paf_address_Postcode_BuildingNumber_NC]
    ON [dbo].[paf_address]([Postcode] ASC, [BuildingNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_paf_address_ThoroughfareKey_NC]
    ON [dbo].[paf_address]([ThoroughfareKey] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_paf_address_ThoroughfareDescriptorKey_NC]
    ON [dbo].[paf_address]([ThoroughfareDescriptorKey] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_paf_address_SubBuildingNameKey_NC]
    ON [dbo].[paf_address]([SubBuildingNameKey] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_paf_address_Postcode_NC]
    ON [dbo].[paf_address]([Postcode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_paf_address_OrganisationKey_NC]
    ON [dbo].[paf_address]([OrganisationKey] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_paf_address_LocalityKey_NC]
    ON [dbo].[paf_address]([LocalityKey] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_paf_address_DependentThoroughfareKey_NC]
    ON [dbo].[paf_address]([DependentThoroughfareKey] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_paf_address_DependentThoroughfareDescriptorKey_NC]
    ON [dbo].[paf_address]([DependentThoroughfareDescriptorKey] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_paf_address_BuildingNameKey_NC]
    ON [dbo].[paf_address]([BuildingNameKey] ASC);

