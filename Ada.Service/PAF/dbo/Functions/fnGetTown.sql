﻿CREATE FUNCTION [dbo].[fnGetTown](@List NVARCHAR(500))
RETURNS @Table  TABLE (DependentLocality NVARCHAR(100),PostTown	NVARCHAR(100))
AS
BEGIN

	DECLARE @RList NVARCHAR(500) = REPLACE(REVERSE(@List), ' ', '.') + REPLICATE('.', 4)
	DECLARE @Index1 INT = CHARINDEX('.',  @RList, 1)
	DECLARE @Index2 INT = CHARINDEX('.',  @RList, @Index1 + 1)
	DECLARE @Index3 INT = CHARINDEX('.',  @RList, @Index2 + 2)
	DECLARE @Index4 INT = CHARINDEX('.',  @RList, @Index3 + 3)

	IF @Index4 > 0
	INSERT INTO @Table(DependentLocality, PostTown)
	SELECT DependentLocality, PostTown
	FROM dbo.localities 
	WHERE LTRIM(ISNULL(DependentLocality, '') + ' ' + PostTown) = RIGHT(@List, @Index4 - 1)
	IF @@ROWCOUNT > 0 RETURN

	IF @Index3 > 0
	INSERT INTO @Table(DependentLocality, PostTown)
	SELECT DependentLocality, PostTown
	FROM dbo.localities 
	WHERE LTRIM(ISNULL(DependentLocality, '')  + ' ' + PostTown) = RIGHT(@List, @Index3 - 1)
	IF @@ROWCOUNT > 0 RETURN

	IF @Index2 > 0
	INSERT INTO @Table(DependentLocality, PostTown)
	SELECT DependentLocality, PostTown
	FROM dbo.localities  
	WHERE LTRIM(ISNULL(DependentLocality, '')  + ' ' + PostTown) = RIGHT(@List, @Index2 - 1)
	IF @@ROWCOUNT > 0 RETURN

	IF @Index1 > 0
	INSERT INTO @Table(DependentLocality, PostTown)
	SELECT DependentLocality, PostTown
	FROM dbo.localities  
	WHERE LTRIM(ISNULL(DependentLocality, '')  + ' ' + PostTown) = RIGHT(@List, @Index1 - 1)
	
	RETURN 
END