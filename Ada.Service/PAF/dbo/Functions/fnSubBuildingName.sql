﻿CREATE FUNCTION [dbo].[fnSubBuildingName] (@List NVARCHAR(500))
RETURNS @Table TABLE (Building NVARCHAR(100))
AS
BEGIN
	
	DECLARE @RList NVARCHAR(500) = REPLACE(@List, ' ', '.') + REPLICATE('.', 2)
	DECLARE @Index1 INT = CHARINDEX('.',  @RList, 1)
	DECLARE @Index2 INT = CHARINDEX('.',  @RList, @Index1 + 1)

	INSERT INTO @Table(Building)
	SELECT LTRIM(RTRIM(SubBuildingName))
	FROM dbo.sub_building_name
	WHERE LTRIM(SubBuildingName)  = LEFT(@List, @Index2 - 1)
	IF @@ROWCOUNT > 0 RETURN

	INSERT INTO @Table(Building)
	SELECT LTRIM(RTRIM(SubBuildingName))
	FROM dbo.sub_building_name
	WHERE LTRIM(SubBuildingName)  = LEFT(@List, @Index1 - 1)
	IF @@ROWCOUNT > 0 RETURN

	INSERT INTO @Table(Building)
	SELECT LTRIM(RTRIM(SubBuildingName))
	FROM dbo.sub_building_name
	WHERE LTRIM(SubBuildingName) = SUBSTRING(@List, @Index1 + 2, @Index2 - @Index1 - 1)

	RETURN 
END