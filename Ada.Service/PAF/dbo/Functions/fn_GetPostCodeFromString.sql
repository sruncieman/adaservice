﻿CREATE FUNCTION [dbo].[fn_GetPostCodeFromString]
(
	@String VARCHAR(500) 
)
RETURNS NVARCHAR(10)
AS
BEGIN

	DECLARE @PostCode NVARCHAR(10) = NULL
	SET @String = REPLACE( @String, 'Flat ', '')
	SET @String = REPLACE( @String, 'Flat', '')
    DECLARE @MatchExpression VARCHAR(50)
	DECLARE @Start INT
	DECLARE @End INT
	
	SET @MatchExpression  =  '%[A-Z][A-Z][0-9][0-9][0-9][A-Z][A-Z]%' --LS205AB
	SET @Start = PatIndex(@MatchExpression, @String)
	IF (@Start > 0)
	BEGIN 
		SET @PostCode = SUBSTRING(@String, @Start, 7)
		IF EXISTS(SELECT * FROM dbo.paf_address WHERE Postcode =  REPLACE(@PostCode, ' ', ''))
			RETURN @PostCode
	END
	
	SET @MatchExpression =  '%[A-Z][A-Z][0-9][0-9][A-Z][A-Z]%' --LS25AB
	SET @Start = PatIndex(@MatchExpression, @String)
	IF (@Start > 0)
	BEGIN 
		SET @PostCode = SUBSTRING(@String, @Start, 6)
		IF EXISTS(SELECT * FROM dbo.paf_address WHERE Postcode =  REPLACE(@PostCode, ' ', ''))
			RETURN @PostCode	
	END
	
	SET @MatchExpression =  '%[A-Z][0-9][0-9][0-9][A-Z][A-Z]%' --L205AB
	SET @Start = PatIndex(@MatchExpression, @String)
	IF (@Start > 0)
	BEGIN 
		SET @PostCode = SUBSTRING(@String, @Start, 6)
		IF EXISTS(SELECT * FROM dbo.paf_address WHERE Postcode =  REPLACE(@PostCode, ' ', ''))	
			RETURN @PostCode
	END
		
	SET @MatchExpression =  '%[A-Z][0-9][0-9][A-Z][A-Z]%' --L25AB
	SET @Start = PatIndex(@MatchExpression, @String)
	IF (@Start > 0)
	BEGIN 
		SET @PostCode = SUBSTRING(@String, @Start, 5)
		IF EXISTS(SELECT * FROM dbo.paf_address WHERE Postcode =  REPLACE(@PostCode, ' ', ''))
			RETURN @PostCode	
	END

	RETURN @PostCode
END
