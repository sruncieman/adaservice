﻿CREATE FUNCTION [dbo].[fnBuildingName] (@List NVARCHAR(500))
RETURNS @Table  TABLE (Building NVARCHAR(100))
AS
BEGIN
	
	DECLARE @RList NVARCHAR(500) = REPLACE(@List, ' ', '.') + REPLICATE('.', 2)
	DECLARE @Index1 INT = CHARINDEX('.',  @RList, 1)
	DECLARE @Index2 INT = CHARINDEX('.',  @RList, @Index1 + 1)

	INSERT INTO @Table(Building)
	SELECT RTRIM(LTRIM(BuildingName))
	FROM dbo.building_names
	WHERE LTRIM(BuildingName)  = LEFT(@List, @Index2 - 1)
	IF @@ROWCOUNT > 0 RETURN

	INSERT INTO @Table(Building)
	SELECT RTRIM(LTRIM(BuildingName))
	FROM dbo.building_names
	WHERE LTRIM(BuildingName)  = LEFT(@List, @Index1 - 1)
	IF @@ROWCOUNT > 0 RETURN

	INSERT INTO @Table(Building)
	SELECT RTRIM(LTRIM(BuildingName))
	FROM dbo.building_names
	WHERE LTRIM(BuildingName) = SUBSTRING(@List, @Index1 + 2, @Index2 - @Index1 - 1)

	RETURN 
END