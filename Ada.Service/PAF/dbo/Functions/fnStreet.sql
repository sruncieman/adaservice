﻿CREATE FUNCTION [dbo].[fnStreet]
(
 @List NVARCHAR(500)
)
RETURNS @Table TABLE 
	(
	 Street				NVARCHAR(100)
	,DependentLocality	NVARCHAR(100)
	)
AS
BEGIN
	
	DECLARE @RList NVARCHAR(500) = REPLACE(REVERSE(@List), ' ', '.') + REPLICATE('.', 2)
	DECLARE @Index1 INT = CHARINDEX('.',  @RList, 1)
	DECLARE @Index2 INT = CHARINDEX('.',  @RList, @Index1 + 1)

	INSERT INTO @Table(Street)
	SELECT ThoroughfareDescriptor
	FROM dbo.thoroughfare_descriptor
	WHERE LTRIM(ThoroughfareDescriptor)  = RIGHT(@List, @Index2 - 1)
	IF @@ROWCOUNT > 0 RETURN

	INSERT INTO @Table(Street)
	SELECT ThoroughfareDescriptor
	FROM dbo.thoroughfare_descriptor
	WHERE LTRIM(ThoroughfareDescriptor)  = RIGHT(@List, @Index1 - 1)
	IF @@ROWCOUNT > 0 RETURN

	INSERT INTO @Table(Street, DependentLocality)
	SELECT ThoroughfareDescriptor, REVERSE(LEFT(@RList, @Index1 - 1))
	FROM dbo.thoroughfare_descriptor
	WHERE LTRIM(ThoroughfareDescriptor) = SUBSTRING(@List, @Index1 + 2, @Index2 - @Index1 - 1)

	RETURN 
END