﻿CREATE FUNCTION [dbo].[PafRemoveCountryCounty]
(
	@Add VARCHAR(500) 
)
RETURNS  VARCHAR(500) 
AS
BEGIN

	DECLARE @Result VARCHAR(500)
	SELECT @Result =  REPLACE(@Add,  CountyName, '') FROM dbo.UKCounties
	RETURN @Result

END