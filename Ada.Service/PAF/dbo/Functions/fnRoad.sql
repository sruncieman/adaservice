﻿CREATE FUNCTION [dbo].[fnRoad](@List NVARCHAR(500))
RETURNS @Table  TABLE (Road NVARCHAR(100))
AS
BEGIN
	
	DECLARE @RList NVARCHAR(500) = REPLACE(REVERSE(@List), ' ', '.') + REPLICATE('.', 3)
	DECLARE @Index1 INT = CHARINDEX('.',  @RList, 1)
	DECLARE @Index2 INT = CHARINDEX('.',  @RList, @Index1 + 1)
	DECLARE @Index3 INT = CHARINDEX('.',  @RList, @Index2 + 1)

	IF @Index3 > 0
		INSERT INTO @Table(Road)
		SELECT ThoroughfareName
		FROM dbo.Thoroughfare
		WHERE LTRIM(ThoroughfareName)  = RIGHT(@List, @Index3 - 1)

	IF @@ROWCOUNT > 0 RETURN
		IF @Index2 > 0 AND @Index1 > 0
			INSERT INTO @Table(Road)
			SELECT ThoroughfareName
			FROM dbo.Thoroughfare
			WHERE LTRIM(ThoroughfareName)  = RIGHT(@List, @Index2 - 1) + ' ' + RIGHT(@List, @Index1 - 1)

	IF @@ROWCOUNT > 0 RETURN
		IF @Index2 > 0
			INSERT INTO @Table(Road)
			SELECT ThoroughfareName
			FROM dbo.Thoroughfare
			WHERE LTRIM(ThoroughfareName)  = RIGHT(@List, @Index2 - 1)

	IF @@ROWCOUNT > 0 RETURN
		IF @Index1 > 0 
			INSERT INTO @Table(Road)
			SELECT ThoroughfareName
			FROM dbo.Thoroughfare
			WHERE LTRIM(ThoroughfareName)  = RIGHT(@List, @Index1 - 1)
	
	RETURN 
END