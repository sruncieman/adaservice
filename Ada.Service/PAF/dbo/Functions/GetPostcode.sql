﻿CREATE FUNCTION [dbo].[GetPostcode]
(
	@Adr VARCHAR(500)
)
RETURNS VARCHAR(8)
AS
BEGIN

	DECLARE @Result VARCHAR(8)

	SET @Result = CASE
		WHEN PATINDEX('%[A-Z][A-Z][0-9][0-9] [0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][A-Z][0-9][0-9] [0-9][A-Z][A-Z]%',@Adr),8)
		WHEN PATINDEX('%[A-Z][0-9][0-9] [0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][0-9][0-9] [0-9][A-Z][A-Z]%',@Adr),7)
		WHEN PATINDEX('%[A-Z][A-Z][0-9] [0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][A-Z][0-9] [0-9][A-Z][A-Z]%',@Adr),7)
		WHEN PATINDEX('%[A-Z][0-9] [0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][0-9] [0-9][A-Z][A-Z]%',@Adr),6)
		WHEN PATINDEX('%[A-Z][A-Z][0-9][A-Z] [0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][A-Z][0-9][A-Z] [0-9][A-Z][A-Z]%',@Adr),8)
		WHEN PATINDEX('%[A-Z][0-9][A-Z] [0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][0-9][A-Z] [0-9][A-Z][A-Z]%',@Adr),7) 
		WHEN PATINDEX('%[A-Z][A-Z][0-9][0-9][0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][A-Z][0-9][0-9][0-9][A-Z][A-Z]%',@Adr),8)
		WHEN PATINDEX('%[A-Z][0-9][0-9][0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][0-9][0-9][0-9][A-Z][A-Z]%',@Adr),7)
		WHEN PATINDEX('%[A-Z][A-Z][0-9][0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][A-Z][0-9][0-9][A-Z][A-Z]%',@Adr),7)
		WHEN PATINDEX('%[A-Z][0-9][0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][0-9][0-9][A-Z][A-Z]%',@Adr),6)
		WHEN PATINDEX('%[A-Z][A-Z][0-9][A-Z][0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][A-Z][0-9][A-Z][0-9][A-Z][A-Z]%',@Adr),8)
		WHEN PATINDEX('%[A-Z][0-9][A-Z][0-9][A-Z][A-Z]%', @Adr) > 0
		THEN SUBSTRING(@Adr, PATINDEX('%[A-Z][0-9][A-Z][0-9][A-Z][A-Z]%',@Adr),7) 
	END

	RETURN REPLACE(@Result,' ','')
END
