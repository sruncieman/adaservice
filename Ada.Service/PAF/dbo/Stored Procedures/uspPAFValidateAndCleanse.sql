﻿CREATE PROCEDURE [dbo].[uspPAFValidateAndCleanse]
AS
SET NOCOUNT ON

-----------------------------------------
--1)CreateAndDeclareObjects
-----------------------------------------
IF OBJECT_ID('tempdb..#RawAddress') IS NOT NULL
	DROP TABLE #RawAddress
	
IF OBJECT_ID('tempdb..#AddressValidation') IS NOT NULL
	DROP TABLE #AddressValidation
	
CREATE TABLE #AddressValidation
		(
		 AddressID			INT
		,SubBuildingName	VARCHAR(50)
		,BuildingNumber		VARCHAR(50)
		,Building			VARCHAR(50)
		,Street				VARCHAR(50)
		,Locality			VARCHAR(50)
		,Town				VARCHAR(50)
		,PostCode			VARCHAR(50)
		,PafValidation		INT
		,PafUPRN			VARCHAR(20)
		)
		
DECLARE @AddressValidation TABLE
		(
		 SubBuildingName	VARCHAR(50)
		,BuildingNumber		VARCHAR(50)
		,Building			VARCHAR(50)
		,Street				VARCHAR(50)
		,Locality			VARCHAR(50)
		,Town				VARCHAR(50)
		,PostCode			VARCHAR(50)
		,PafValidation		INT
		,Flag				INT
		,PafUPRN			VARCHAR(20)	
		)

DECLARE  @Id				INT
		,@SubBuilding		VARCHAR(50)
		,@Building			VARCHAR(50)
		,@BuildingNumber	VARCHAR(50)
		,@Street			VARCHAR(500)
		,@Locality			VARCHAR(50)
		,@Town				VARCHAR(50)
		,@PostCode			VARCHAR(10)
		
-----------------------------------------
--2)PopulateRawAddressInAll2AddressTables
-----------------------------------------
UPDATE A
SET [PafValidation] = 0
FROM [MDA].[dbo].[Address] A
WHERE [ADARecordStatus] = 10

--GetTheIDAndRawAddress
SELECT   Id AddressID
		,PAF.dbo.FnRawAddress (SubBuilding,BuildingNumber,Building,Street,Locality,Town,County,REPLACE(PostCode,' ','')) RawAddress
INTO #RawAddress
FROM MDA.dbo.Address
WHERE PafValidation = 0 --AddressNeverBeenValidated
ORDER BY Id

--Incident2AddressUpdate
UPDATE I2A
SET RawAddress = RA.RawAddress
FROM #RawAddress RA
INNER JOIN MDA.dbo.Incident2Address I2A ON I2A.Address_Id = RA.AddressID

--Organisation2Address
UPDATE O2A
SET RawAddress = RA.RawAddress
FROM #RawAddress RA
INNER JOIN MDA.dbo.Organisation2Address O2A ON O2A.Address_Id = RA.AddressID

--Person2Address
UPDATE P2A
SET RawAddress = RA.RawAddress
FROM #RawAddress RA
INNER JOIN MDA.dbo.Person2Address P2A ON P2A.Address_Id = RA.AddressID

--Vehicle2Address
UPDATE V2A
SET RawAddress = RA.RawAddress
FROM #RawAddress RA
INNER JOIN MDA.dbo.Vehicle2Address V2A ON V2A.Address_Id = RA.AddressID

-----------------------------------------
--3)PAFValidateTheAddress
-----------------------------------------
DECLARE Cur_AddressValidation CURSOR FOR 
SELECT ID, SubBuilding, Building, BuildingNumber, Street, Locality, Town, REPLACE(PostCode,' ','')
FROM MDA.dbo.Address 
WHERE PafValidation = 0

OPEN Cur_AddressValidation
FETCH NEXT FROM Cur_AddressValidation INTO @ID, @SubBuilding ,@Building , @BuildingNumber, @Street, @Locality, @Town, @PostCode
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO @AddressValidation
	EXECUTE PAF.dbo.usp_GetAddress
				 @SubBuilding			= @SubBuilding
				,@Building				= @Building
				,@BuildingNumber		= @BuildingNumber
				,@Street				= @Street
				,@Locality				= @Locality 
				,@Town					= @Town
				,@PostCode				= @PostCode
		
	IF @@ROWCOUNT > 0
		BEGIN
			INSERT INTO #AddressValidation (AddressID, SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, PafUPRN)
			SELECT @ID, SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, ISNULL(PafUPRN,'')
			FROM @AddressValidation
		END
		ELSE
		BEGIN
			INSERT INTO #AddressValidation (AddressID, SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, PafUPRN)
			SELECT @ID, @SubBuilding, @BuildingNumber ,@Building, @Street, @Locality, @Town, @PostCode, 1, NULL
		END
	
	DELETE FROM @AddressValidation
	FETCH NEXT FROM Cur_AddressValidation INTO @ID, @SubBuilding ,@Building , @BuildingNumber, @Street, @Locality, @Town, @PostCode
END
CLOSE Cur_AddressValidation
DEALLOCATE Cur_AddressValidation

UPDATE AD
SET		 SubBuilding	= UPPER(NULLIF(AV.SubBuildingName,''))
		,Building		= UPPER(NULLIF(AV.Building,''))
		,BuildingNumber	= UPPER(NULLIF(AV.BuildingNumber,''))
		,Street			= UPPER(NULLIF(AV.Street,''))
		,Locality		= UPPER(NULLIF(AV.Locality,''))
		,Town			= UPPER(NULLIF(AV.Town,''))
		,County			= NULL
		,PostCode		= UPPER(NULLIF(AV.PostCode,''))
		,PafValidation	= 1 --UPPER(NULLIF(AV.PafValidation,'')) PAFValidateCalledForEverything
		,PafUPRN		= AV.PafUPRN--UPPER(NULLIF(AV.PafUPRN,'')) CanBeNull
FROM #AddressValidation AV
INNER JOIN MDA.dbo.Address AD ON AD.ID = AV.AddressID

-----------------------------------------
--4)Tidy
-----------------------------------------
IF OBJECT_ID('tempdb..#RawAddress') IS NOT NULL
	DROP TABLE #RawAddress
	
IF OBJECT_ID('tempdb..#AddressValidation') IS NOT NULL
	DROP TABLE #AddressValidation