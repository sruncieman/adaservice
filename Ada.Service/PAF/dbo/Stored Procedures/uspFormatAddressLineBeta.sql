﻿CREATE PROCEDURE [dbo].[uspFormatAddressLineBeta]
(
 @Add						VARCHAR(500) 
,@OSubBuildingName			VARCHAR(60) = NULL OUTPUT 
,@OBuilding					VARCHAR(60) = NULL OUTPUT 
,@OHouseNo					VARCHAR(4)	= NULL OUTPUT 
,@OThoroughfare				VARCHAR(100)= NULL OUTPUT 
,@OThoroughfareDescriptor	VARCHAR(60)	= NULL OUTPUT 
,@ODependentLocality		VARCHAR(60)	= NULL OUTPUT 
,@OPostTown					VARCHAR(60)	= NULL OUTPUT 
,@OPostcode					VARCHAR(8)	= NULL OUTPUT 
,@OPafValidation			BIT = NULL OUTPUT 
,@OFlag						INT = NULL OUTPUT 
 )
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE  @Road						VARCHAR(60)
		,@Street					VARCHAR(60)
		,@OriAdd					VARCHAR(500) = @Add
		,@SubBuildingName			VARCHAR(60)
		,@Building					VARCHAR(60)
		,@HouseNo					VARCHAR(4)
		,@Thoroughfare				VARCHAR(60) 
		,@ThoroughfareDescriptor	VARCHAR(60)
		,@DependentLocality			VARCHAR(60)
		,@PostTown					VARCHAR(60)
		,@Postcode					VARCHAR(8)
				
SELECT  @Postcode = dbo.GetPostcode(@Add)
		 
SELECT	@Add = dbo.PafRemoveCountryCounty(@Add)
DECLARE	@AddLen INT = Len(REPLACE(@Add, ' ', ''))

IF @AddLen =0
GOTO OUTPUT

IF (@Postcode IS NOT NULL)
	SET @Add = LTRIM(RTRIM(REPLACE(@Add, @Postcode, '') ))
	
IF (ISNUMERIC(@Add) = 1) 
BEGIN
	SET @HouseNo = @Add	
	SELECT 
		NULL SubBuildingName, 
		NULL Building, 
		@HouseNo HouseNo,
		NULL Thoroughfare,   
		NULL ThoroughfareDescriptor, 
		NULL DependentLocality, 
		NULL PostTown, 
		REPLACE(UPPER(@Postcode), ' ', '') Postcode,
		0 PafValidation,
		0 Flag 
	RETURN 	
END

SELECT	 @DependentLocality = LTRIM(RTRIM(DependentLocality))
		,@PostTown = LTRIM(RTRIM(PostTown))
FROM dbo.fnGetTown(@Add)

IF (@DependentLocality IS NOT NULL)
	SET @Add = LTRIM(RTRIM(REPLACE(@Add, @DependentLocality, '') ))
IF (@PostTown IS NOT NULL)
	SET @Add = LTRIM(RTRIM(REPLACE(@Add, @PostTown, '') ))

IF (LEN(@DependentLocality) = 0)
BEGIN
	SELECT	 @Street = LTRIM(RTRIM(Street))
			,@DependentLocality = LTRIM(RTRIM(DependentLocality))
	FROM  dbo.fnStreet(@Add)
	
	IF (@DependentLocality IS NOT NULL)
		SET @Add = LTRIM(RTRIM(REPLACE(@Add, @DependentLocality, '') ))
		SET @Add = LTRIM(RTRIM(REPLACE(@Add, @Street, '') ))
END
ELSE
BEGIN
	SELECT    @Street = LTRIM(RTRIM(Street))
	FROM      dbo.fnStreet(@Add)
	SET @Add = LTRIM(RTRIM(REPLACE(@Add, @Street, '') ))
END

SELECT @Road = LTRIM(RTRIM(Road))
FROM dbo.fnRoad(@Add)

SET @Add = LTRIM(RTRIM(REPLACE(@Add, @Road, '') ))

IF (ISNUMERIC(@Add) = 1) SET @HouseNo = @Add
ELSE
BEGIN
	IF (LEN(@Add) > 0)
		SELECT @SubBuildingName = LTRIM(RTRIM(Building))
		FROM dbo.fnSubBuildingName(@Add)
		IF (@SubBuildingName IS NOT NULL)
			SET @Add = LTRIM(RTRIM(REPLACE(@Add, @SubBuildingName, '') ))

	IF (LEN(@Add) > 0)
		SELECT @Building = Building FROM dbo.fnBuildingName(@Add)
		IF (@Building IS NOT NULL)
			SET @Add = LTRIM(RTRIM(REPLACE(@Add, @Building, '') ))

	IF (ISNUMERIC(@Add) = 1) SET @HouseNo = @Add
END

OUTPUT:

IF @AddLen > LEN(ISNULL(@SubBuildingName, '') + ISNULL(@Building, '') + ISNULL(@HouseNo, '') + ISNULL(@Road, '') + ISNULL(@Street, '') + ISNULL(@DependentLocality, '') + ISNULL(@PostTown, '') + ISNULL(@Postcode, '') )+ 6
OR @AddLen = 0
OR (@SubBuildingName IS NULL AND @Building IS NULL AND @HouseNo IS NULL AND @Road IS NULL AND @Street IS NULL AND @DependentLocality IS NULL AND @PostTown IS NULL AND @Postcode IS NULL)
	PRINT 'No data'
	
ELSE
SELECT	 @OSubBuildingName			= NULLIF(UPPER(@SubBuildingName), '') 
		,@OBuilding					= NULLIF(UPPER(@Building), '') 
		,@OHouseNo					= NULLIF(@HouseNo, '')
		,@OThoroughfare				= NULLIF(UPPER(@Road), '')
		,@OThoroughfareDescriptor	= NULLIF(UPPER(@Street), '')
		,@ODependentLocality		= NULLIF(UPPER(@DependentLocality), '')
		,@OPostTown					= NULLIF(UPPER(@PostTown), '')
		,@OPostcode					= REPLACE(UPPER(@Postcode), ' ', '')
		,@OPafValidation			= 1
		,@OFlag						= 0