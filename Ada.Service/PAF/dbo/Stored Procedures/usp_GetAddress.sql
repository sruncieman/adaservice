﻿

CREATE PROCEDURE [dbo].[usp_GetAddress]
(
 @SubBuilding VARCHAR(50)
,@Building VARCHAR(50)
,@BuildingNumber VARCHAR(50)
,@Street VARCHAR(500)
,@Locality VARCHAR(50)
,@Town VARCHAR(50)
,@PostCode VARCHAR(10)
,@Debug BIT = 0
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

----------------------------------------------------
--DeclareCleanAndSetVariables
----------------------------------------------------
DECLARE  @THN VARCHAR(4)	
		,@HN VARCHAR(4)	
		,@Str VARCHAR(100)
		
SELECT	 @SubBuilding		= ISNULL(@SubBuilding,'')
		,@Building			= ISNULL(@Building,'')
		,@BuildingNumber	= ISNULL(@BuildingNumber,'')
		,@Street			= ISNULL(@Street,'')
		,@Locality			= ISNULL(@Locality,'')
		,@Town				= ISNULL(@Town,'')
		,@PostCode			= ISNULL(REPLACE(@PostCode,' ',''),'')
		
DECLARE @PafAddress TABLE 
	(
	 SubBuildingName	VARCHAR(50)
	,BuildingNumber		VARCHAR(4)
	,Building			VARCHAR(50)
	,Street				VARCHAR(50) 
	,Locality			VARCHAR(50)
	,Town				VARCHAR(30)
	,PostCode			VARCHAR(8)
	,PafUPRN			VARCHAR(20)
	,QStr				VARCHAR(500)
	,QStr2				VARCHAR(500)
	,QStr3				VARCHAR(500)
	,QStr4				VARCHAR(500)
	,PafValidation		BIT
	,Flag				INT
	)

DECLARE	 @Address	VARCHAR(600) = @SubBuilding + @Building +  @BuildingNumber + @Street + @Locality  + @Town 
		,@Address1	VARCHAR(600) = LTRIM(RTRIM(REPLACE(@SubBuilding + ' ' + @Building + ' ' +  @BuildingNumber + ' ' + @Street + ' ' + @Locality + ' '   + @Town + ' ' + @PostCode, '  ', ' ')))

SELECT	 @PostCode	= REPLACE(@PostCode, ' ', '')
		,@Address	= REPLACE(REPLACE(@Address, ' ', ''), ',', '')
	
IF (LEN(@PostCode) < 1) 
BEGIN
	SET @PostCode = dbo.fn_GetPostCodeFromString(@Address)
	IF  (LEN(@PostCode) > 4)
		SET @Address =  REPLACE(@Address, @PostCode, '')
END	

IF (LEN(@PostCode) > 1) 
BEGIN
	SET @THN = dbo.fnGetHouseNo(@Address) 
	IF (LEN(@THN) > 0)
	BEGIN
		INSERT INTO @PafAddress (SubBuildingName,Building, BuildingNumber, Street, Locality, Town, PostCode, PafUPRN, QStr, QStr2, QStr3, QStr4, PafValidation, Flag)
		SELECT	 LTRIM(RTRIM(SB.SubBuildingName)) SubBuildingName
				,LTRIM(RTRIM(B.BuildingName)) BuildingName
				,LTRIM(RTRIM(A.BuildingNumber)) BuildingNumber
				,LTRIM(RTRIM(T.ThoroughfareName)) + ' ' + LTRIM(RTRIM(TD.ThoroughfareDescriptor)) Street
				,LTRIM(RTRIM(L.DependentLocality)) Locality
				,LTRIM(RTRIM(L.PostTown)) PostTown
				,LTRIM(RTRIM(A.Postcode)) Postcode
				,LTRIM(RTRIM(A.PafUPRN)) PafUPRN
				,REPLACE(ISNULL(LTRIM(RTRIM(SB.SubBuildingName)), ''), ' ', '') +   REPLACE(ISNULL(LTRIM(RTRIM(B.BuildingName)), ''), ' ', '') + CASE WHEN ISNULL(A.BuildingNumber,0) = 0 THEN '' ELSE CONVERT(VARCHAR(4), A.BuildingNumber)   END + REPLACE(ISNULL(T.ThoroughfareName, ''), ' ', '') QStr
				,REPLACE(ISNULL(LTRIM(RTRIM(B.BuildingName)), ''), ' ', '') + CASE WHEN ISNULL(A.BuildingNumber,0) = 0 THEN '' ELSE CONVERT(VARCHAR(4), A.BuildingNumber)   END + REPLACE(ISNULL(LTRIM(RTRIM(T.ThoroughfareName)), ''), ' ', '') QStr2
				,REPLACE(ISNULL(LTRIM(RTRIM(SB.SubBuildingName)), ''), ' ', '') +  CASE WHEN ISNULL(A.BuildingNumber,0) = 0 THEN '' ELSE CONVERT(VARCHAR(4), A.BuildingNumber)   END + REPLACE(ISNULL(LTRIM(RTRIM(T.ThoroughfareName)), ''), ' ', '') QStr3
				,CASE WHEN ISNULL(A.BuildingNumber,0) = 0 THEN '' ELSE CONVERT(VARCHAR(4), A.BuildingNumber)   END + REPLACE(ISNULL(LTRIM(RTRIM(T.ThoroughfareName)), ''), ' ', '') QS
				,1
				,1
		FROM dbo.paf_address A
		INNER JOIN dbo.localities L ON L.LocalityKey = A.LocalityKey
		INNER JOIN dbo.thoroughfare T ON T.ThoroughfareKey = A.ThoroughfareKey
		LEFT JOIN dbo.thoroughfare_descriptor TD ON TD.ThoroughfareDescriptorKey = A.ThoroughfareDescriptorKey
		LEFT JOIN dbo.building_names B ON B.BuildingNameKey = A.BuildingNameKey
		LEFT JOIN dbo.sub_building_name SB ON SB.SubBuildingNameKey = A.SubBuildingNameKey
		WHERE A.Postcode = @PostCode
		AND CONVERT(VARCHAR(4), A.BuildingNumber) = @THN
	END
END
/*		
IF (LEN(@PostCode) > 4)
BEGIN
	INSERT INTO @PafAddress (SubBuildingName, Building, BuildingNumber, Street, Locality, Town, PostCode, PafUPRN, QStr, QStr2, QStr3, QStr4, PafValidation, Flag)
	SELECT	 LTRIM(RTRIM(SB.SubBuildingName)) SubBuildingName
			,LTRIM(RTRIM(B.BuildingName)) BuildingName
			,LTRIM(RTRIM(A.BuildingNumber)) BuildingNumber
			,LTRIM(RTRIM(T.ThoroughfareName)) + ' ' + LTRIM(RTRIM(TD.ThoroughfareDescriptor)) Street
			,LTRIM(RTRIM(L.DependentLocality)) Locality
			,LTRIM(RTRIM(L.PostTown)) PostTown
			,LTRIM(RTRIM(A.Postcode)) Postcode
			,LTRIM(RTRIM(A.PafUPRN)) PafUPRN
			,REPLACE(ISNULL(LTRIM(RTRIM(SB.SubBuildingName)), ''), ' ', '') +   REPLACE(ISNULL(LTRIM(RTRIM(B.BuildingName)), ''), ' ', '') + CASE WHEN ISNULL(A.BuildingNumber,0) = 0 THEN '' ELSE CONVERT(VARCHAR(4), A.BuildingNumber)   END + REPLACE(ISNULL(T.ThoroughfareName, ''), ' ', '') QStr
			,REPLACE(ISNULL(LTRIM(RTRIM(B.BuildingName)), ''), ' ', '') + CASE WHEN ISNULL(A.BuildingNumber,0) = 0 THEN '' ELSE CONVERT(VARCHAR(4), A.BuildingNumber)   END + REPLACE(ISNULL(LTRIM(RTRIM(T.ThoroughfareName)), ''), ' ', '') QStr2
			,REPLACE(ISNULL(LTRIM(RTRIM(SB.SubBuildingName)), ''), ' ', '') +  CASE WHEN ISNULL(A.BuildingNumber,0) = 0 THEN '' ELSE CONVERT(VARCHAR(4), A.BuildingNumber)   END + REPLACE(ISNULL(LTRIM(RTRIM(T.ThoroughfareName)), ''), ' ', '') QStr3
			,CASE WHEN ISNULL(A.BuildingNumber,0) = 0 THEN '' ELSE CONVERT(VARCHAR(4), A.BuildingNumber)   END + REPLACE(ISNULL(LTRIM(RTRIM(T.ThoroughfareName)), ''), ' ', '') QS
			,NULL
			,NULL
	FROM dbo.paf_address A
	INNER JOIN dbo.localities L ON L.LocalityKey = A.LocalityKey
	INNER JOIN dbo.thoroughfare T ON T.ThoroughfareKey = A.ThoroughfareKey
	LEFT JOIN dbo.thoroughfare_descriptor TD ON TD.ThoroughfareDescriptorKey = A.ThoroughfareDescriptorKey
	LEFT JOIN dbo.building_names B ON B.BuildingNameKey = A.BuildingNameKey
	LEFT JOIN dbo.sub_building_name SB ON SB.SubBuildingNameKey = A.SubBuildingNameKey
	WHERE A.Postcode = @PostCode
			
	UPDATE @PafAddress
	SET  PafValidation	= 1
		,Flag			= 1
	WHERE ISNULL(SubBuildingName, '') = @SubBuilding
	AND ISNULL(Building, '') = @Building
	AND ISNULL(BuildingNumber, '') = @BuildingNumber
	AND ISNULL(Street, '') = @Street 
	AND ISNULL(Locality, '') = @Locality
	AND ISNULL(Town, '') = @Town
	AND ISNULL(PostCode, '') = @PostCode

	IF LEN(@Address) > 0
	BEGIN
		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation  = 1
			,Flag			= 20
		WHERE PATINDEX('%' + @Address + '%',QStr ) > 0 

		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE   @PafAddress
		SET  PafValidation	= 1
			,Flag			= 30
		WHERE PATINDEX(QStr2 + '%', @Address) > 0
		AND LEN(QStr2) > 2

		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation  = 1
			,Flag			= 40
		WHERE PATINDEX('%' + @Address + '%',QStr2 ) > 0 

		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation	= 1
			,Flag			= 50
		WHERE PATINDEX(QStr3 + '%', @Address) > 0  
		AND LEN(QStr3) > 2 

		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation	= 1
			,Flag			= 60
		WHERE PATINDEX('%' + @Address + '%',QStr3 ) > 0 

		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation	= 1
			,Flag			= 80
		WHERE PATINDEX('%' + @Address + '%',QStr4 ) > 0 

		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation	= 1
			,Flag			= 90
		WHERE PATINDEX( QStr4 + '%',@Address ) > 0  
		AND LEN(QStr4) > 2

		SELECT   @HN  = LEFT(@Address,4)
				,@Str = @Address
				
		WHILE PATINDEX('%[^0-9]%', @HN) > 0
		BEGIN
			SET @HN = STUFF(@HN, PATINDEX('%[^0-9]%', @HN), 1, '')
		END
							
		WHILE PATINDEX('%[0-9]%', @Str) > 0
		BEGIN
			SET @Str = STUFF(@Str, PATINDEX('%[0-9]%', @Str), 1, '')
		END	

		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation = 1
			,Flag = 200
		WHERE BuildingNumber = @HN 
		AND DIFFERENCE(@Str,Street) = 4
			
		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation  = 1
			,Flag			= 300
		WHERE BuildingNumber = @HN 
		AND DIFFERENCE(@Str,Street) = 3
			
		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation  = 1
			,Flag			= 400
		WHERE BuildingNumber = @HN 
		AND DIFFERENCE(@Str,Street) = 2
			
		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation	= 1
			,Flag			= 500
		WHERE BuildingNumber = @HN

		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation	= 1
			,Flag			= 600
		WHERE REPLACE(Building, ' ', '') = LEFT(@Address, LEN(REPLACE(Building, ' ', '')))
			
		IF NOT EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE PafValidation = 1)
		UPDATE @PafAddress
		SET  PafValidation  = 1
			,Flag			= 700
		WHERE REPLACE(SubBuildingName, ' ', '') = LEFT(@Address, LEN(REPLACE(SubBuildingName, ' ', '')))
	END
	
END
*/
IF EXISTS (SELECT TOP 1 1 FROM @PafAddress WHERE (SELECT COUNT(1) FROM @PafAddress WHERE PafValidation = 1) = 1)
BEGIN
	IF (@Debug = 0)
	SELECT SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, Flag ,PafUPRN  FROM @PafAddress WHERE PafValidation = 1 ORDER BY PafUPRN ASC
	ELSE
	SELECT DISTINCT SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, Flag ,PafUPRN FROM @PafAddress
END		
ELSE
BEGIN
	DECLARE @Thoroughfare VARCHAR(50), @ThoroughfareDescriptor VARCHAR(50), @PafValidation BIT = NULL, @Flag INT = NULL
	SELECT @SubBuilding = NULL, @Building = NULL, @BuildingNumber = NULL, @Thoroughfare = NULL, @ThoroughfareDescriptor= NULL, @Locality = NULL, @Town = NULL, @Postcode = NULL
	--PRINT @Address1
	EXEC dbo.uspFormatAddressLineBeta
			 @Add						= @Address1
			,@OSubBuildingName			= @SubBuilding				OUTPUT 
			,@OBuilding					= @Building					OUTPUT
			,@OHouseNo					= @BuildingNumber			OUTPUT
			,@OThoroughfare				= @Thoroughfare				OUTPUT
			,@OThoroughfareDescriptor	= @ThoroughfareDescriptor	OUTPUT
			,@ODependentLocality		= @Locality					OUTPUT
			,@OPostTown					= @Town						OUTPUT
			,@OPostcode					= @Postcode					OUTPUT
			,@OPafValidation			= @PafValidation			OUTPUT
			,@OFlag						= @Flag						OUTPUT
	
	DELETE @PafAddress
	
	INSERT INTO @PafAddress (SubBuildingName, Building, BuildingNumber, Street, Locality, Town, PostCode, PafValidation, Flag, PafUPRN)
	SELECT   UPPER(NULLIF(@SubBuilding,'')) AS SubBuildingName
			,UPPER(NULLIF(@Building,'')) AS Building
			,UPPER(NULLIF(@BuildingNumber,'')) AS BuildingNumber
			,UPPER(NULLIF(LTRIM(RTRIM(ISNULL(@Thoroughfare,'') + ' ' + ISNULL(@ThoroughfareDescriptor,''))),'')) AS Street
			,UPPER(NULLIF(@Locality,'')) AS Locality
			,UPPER(NULLIF(@Town,'')) AS Town
			,UPPER(NULLIF(@Postcode,'')) AS PostCode
			,@PafValidation PafValidation
			,@Flag Flag
			,'' AS PafUPRN	
			
	SELECT SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, Flag ,PafUPRN 
	FROM @PafAddress
	WHERE (ISNULL(LEN(SubBuildingName),0) + ISNULL(LEN(BuildingNumber),0) + ISNULL(LEN(Building),0) + ISNULL(LEN(Street),0) + ISNULL(LEN(Locality),0) + ISNULL(LEN(Town),0) +  ISNULL(LEN(PostCode),0)) > 0
END