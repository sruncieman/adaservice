﻿using MDA.Common;
using MDA.Common.Server;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using MDA.MappingService.Haven;
using MDA.MappingService.Haven.Motor;
using System.Diagnostics;

namespace MDA.Mapping.Haven
{
    class Program
    {
        static List<IPipelineClaim> motorClaimBatch = new List<IPipelineClaim>();

        private static int ProcessClaimIntoDb(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim, object o)
        {
            motorClaimBatch.Add(claim);

            return 0;
        }

        static void Main(string[] args)
        {

            Stream mockStream = null;

            CurrentContext ctx = new CurrentContext(0, 1, "Test");

            Console.WriteLine("Start");

            ProcessingResults pr;

            new FileMapping().ConvertToClaimBatch(ctx, mockStream, null, ProcessClaimIntoDb, out pr);

            string xml = WriteToXml();

            Console.ReadLine();
        }

        private static string WriteToXml()
        {
            string strXmlFileFullSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Haven\XML\";

            string xml = null;

            foreach (var item in motorClaimBatch)
            {

                Console.WriteLine(item.ClaimNumber);

                var ms = new MemoryStream();

                var ser = new XmlSerializer(typeof(MDA.Pipeline.Model.PipelineMotorClaim));

                ser.Serialize(ms, item);

                ms.Position = 0;

                var sr = new StreamReader(ms);

                xml = sr.ReadToEnd();

                TextWriter WriteFileStream = new StreamWriter(strXmlFileFullSave + item.ClaimNumber + ".xml");
                ser.Serialize(WriteFileStream, item);

                WriteFileStream.Close();

            }

            Console.WriteLine("Finished");

            return xml;
        }
    }
}
