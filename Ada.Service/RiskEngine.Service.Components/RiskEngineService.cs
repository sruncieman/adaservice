﻿using System;
//using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Collections.Generic;
using RiskEngine.Model;
using RiskEngine.DAL;
using System.Transactions;

namespace RiskEngine.Service.Components
{
    /// <summary>
    /// Class to do RuleSet distinct test
    /// </summary>
    public class SelectListItemComparer : EqualityComparer<RuleSetData> 
    {
        public override bool Equals(RuleSetData x, RuleSetData y) 
        { 
            return x.RootRuleSetId.Equals(y.RootRuleSetId); 
        }
        public override int GetHashCode(RuleSetData obj) 
        { 
            return obj.RootRuleSetId.GetHashCode(); 
        } 
    }

    /// <summary>
    /// Class that implements the main Risk Engine business functionality
    /// </summary>
    public class RiskEngineService
    {
        private Dictionary<string, string> EntityStructure;

        /// <summary>
        /// Constructor that accepts the EntityStructure
        /// </summary>
        /// <param name="entityStructure"></param>
        public RiskEngineService(Dictionary<string,string> entityStructure)
        {
            EntityStructure = entityStructure;
        }

        /// <summary>
        /// Constructor that leaves EntityStructure as NULL, So can blow up certain methods
        /// </summary>
        public RiskEngineService()
        {
        }

        /// <summary>
        /// Return a list of Clients
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, string> GetClientList()
        {
            Dictionary<int, string> ret = new Dictionary<int,string>();

            using (var re = new RulesEntities())
            {
                var r = from x in re.RuleClientEntities select x;

                foreach (var x in r)
                {
                    ret.Add(x.RuleClientId, x.ClientName);
                }
            }
            return ret;
        }

        /// <summary>
        /// Return client name
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string GetClientName(int clientId)
        {
            string clientName = string.Empty;
            
            using (var re = new RulesEntities())
            {
                clientName =
                    re.RuleClientEntities.Where(x => x.RuleClientId == clientId)
                      .Select(x => x.ClientName)
                      .FirstOrDefault();
            }

            return clientName;
        }


        #region RuleSet Methods

        /// <summary>
        /// Insert a new RuleSet. This is used to create initial version only (sets RootID to same ID as inserted record)
        /// </summary>
        /// <param name="rsd"></param>
        /// <returns></returns>
        public RuleSetData InsertRuleSet(RuleSetData rsd, string who)
        {
            using (var re = new RulesEntities())
            {
                #region Create and Save new RuleSet
                var rs = re.RuleSetEntities.Create();

                rs.AssemblyPath    = rsd.AssemblyPath;
                rs.CreatedBy       = who;
                rs.CreatedDate     = DateTime.Now;
                rs.EntityClassType = rsd.EntityClassType;
                rs.Version         = rsd.Version;
                rs.Name            = rsd.Name;
                rs.RuleSetXML      = rsd.RuleSetXML;
                rs.Status          = rsd.Status;

                re.RuleSetEntities.Add(rs);

                re.SaveChanges();

                rs.RootRuleSetId = rs.RuleSetId;

                re.SaveChanges();
                #endregion

                return new RuleSetData(rs);
            }
        }

        /// <summary>
        /// Update the NAME and RULEXML on the given Rule Set
        /// </summary>
        /// <param name="rsd"></param>
        /// <returns></returns>
        public RuleSetData UpdateRuleSet(RuleSetData rsd)
        {
            using (var re = new RulesEntities())
            {

                var r = (from x in re.RuleSetEntities
                         where x.RuleSetId == rsd.RuleSetId
                         select x).FirstOrDefault();

                if (r == null) return rsd;

                r.Name         = rsd.Name;
                r.RuleSetXML   = rsd.RuleSetXML;
                //r.ModifiedBy   = "FRED";
                //r.ModifiedDate = DateTime.Now;

                re.SaveChanges();

                return new RuleSetData(r);
            }
        }

        /// <summary>
        /// Return a full list of DISTINCT rule set names
        /// </summary>
        /// <returns></returns>
        public List<string> FullListRuleSetsNames()
        {
            using (var re = new RulesEntities())
            {
                return (from x in re.RuleSetEntities select x.Name).Distinct().ToList();
            }

        }

        /// <summary>
        /// Get a list of all RuleSets based on entityClassType (c# type). Returns DISTINCT RoolRuleSetId's and orders
        /// them by descending ID's, so you should get the latest RuleSet for each Root.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<RuleSetData> GetListRuleSets(string entityClassType)
        {
            try
            {
                using (var re = new RulesEntities())
                {

                    IEqualityComparer<RuleSetData> customComparer = new SelectListItemComparer();

                    var r = from x in re.RuleSetEntities
                            where x.EntityClassType == entityClassType
                            orderby x.RuleSetId descending
                            select new RuleSetData()
                            {
                                RuleSetId       = x.RuleSetId,
                                RootRuleSetId   = x.RootRuleSetId,
                                Name            = x.Name,
                                AssemblyPath    = x.AssemblyPath,
                                EntityClassType = x.EntityClassType,
                                //ModifiedDate    = x.ModifiedDate,
                                Dirty           = false,
                                RuleSetXML      = x.RuleSetXML,
                                Status          = x.Status,
                                Version         = x.Version,
                            };

                    List<RuleSetData> l = r.ToList();

                    return l.Distinct(customComparer).ToList();
                }
            }
            catch (Exception ex)
            {
                string a = ex.Message;
            }
            return null;
        }

        /// <summary>
        /// Get a list off all RuleSets based on the EntityClassType (C# type). COULD BE LONG LIST
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<RuleSetData> GetRuleSetsByType(string entityClassType)
        {
            try
            {
                using (var re = new RulesEntities())
                {

                    var r = from x in re.RuleSetEntities
                            where x.EntityClassType == entityClassType
                            select new RuleSetData()
                            {
                                RuleSetId = x.RuleSetId,
                                RootRuleSetId = x.RootRuleSetId,
                                Name = x.Name,
                                AssemblyPath = x.AssemblyPath,
                                EntityClassType = x.EntityClassType,
                                //ModifiedDate = x.ModifiedDate,
                                Dirty = false
                            };

                    return r.ToList();
                }
            }
            catch (Exception ex)
            {
                string a = ex.Message;
            }
            return null;
        }

        //public RuleSetData GetFullRuleSetByName(RuleSetInfo ruleSetInfo)
        //{
        //    if (ruleSetInfo != null)
        //    {
        //        try
        //        {
        //            //RiskEngine.DAL.RulesEntities re = new RulesEntities();

        //            var r = from x in re.RuleSetEntities
        //                    where x.Name == ruleSetInfo.Name
        //                    //orderby x.MajorVersion descending, x.MinorVersion descending
        //                    select new RuleSetData(x)
        //                    {
        //                        RuleSetId = x.RuleSetId,
        //                        RootRuleSetId = x.RootRuleSetId,
        //                        Name = x.Name,
        //                        OriginalName = x.Name,
        //                        MajorVersion = x.MajorVersion,
        //                        MinorVersion = x.MinorVersion,
        //                        OriginalMinorVersion = x.MinorVersion,
        //                        RuleSetXML = x.RuleSetXML,
        //                        Status = x.Status,
        //                        AssemblyPath = x.AssemblyPath,
        //                        EntityClassType = x.EntityClassType,
        //                        ModifiedDate = x.ModifiedDate,
        //                        Dirty = false,
        //                        OriginalMajorVersion = x.MajorVersion
        //                    };

        //            bool specificVersionRequested = !(ruleSetInfo.MajorVersion == 0 && ruleSetInfo.MinorVersion == 0);

        //            if (specificVersionRequested)
        //                r = r.Where(x => x.MajorVersion == ruleSetInfo.MajorVersion && x.MinorVersion == ruleSetInfo.MinorVersion);

        //            RuleSetData rse = r.First();

        //            if (rse == null)
        //            {
        //                if (specificVersionRequested)
        //                    throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Specified RuleSet version does not exist: '{0}'", ruleSetInfo.ToString())); //could use a custom exception type here
        //                else
        //                    throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "No RuleSets exist with this name: '{0}'", ruleSetInfo.Name));
        //            }

        //            return rse;
        //        }
        //        catch (Exception ex)
        //        {
        //            string a = ex.Message;
        //        }
        //        return null;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //    return null;
        //}

        //public RuleSetData GetRuleSetById(int id)
        //{
        //    try
        //    {
        //        using (var re = new RulesEntities())
        //        {

        //            var r = from x in re.RuleSetEntities
        //                    where x.RuleSetId == id
        //                    select new RuleSetData()
        //                    {
        //                        RuleSetId       = x.RuleSetId,
        //                        RootRuleSetId   = x.RootRuleSetId,
        //                        Name            = x.Name,
        //                        Version         = x.Version,
        //                        RuleSetXML      = x.RuleSetXML,
        //                        Status          = x.Status,
        //                        AssemblyPath    = x.AssemblyPath,
        //                        EntityClassType = x.EntityClassType,
        //                        ModifiedDate    = x.ModifiedDate,
        //                        Dirty           = false,
        //                    };

        //            RuleSetData rse = r.First();

        //            return rse;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string a = ex.Message;
        //    }
        //    return null;
        //}

        /// <summary>
        /// Get the latest RuleSet given the Root RuleSetId.  Orders Version descending and returns first record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RuleSetData GetLatestRuleSetByRootId(int rootRuleSetId)
        {
            try
            {
                using (var re = new RulesEntities())
                {
                    #region Fetch latest (based on Version) RuleGroup belonging to the given RootRuleGroup
                    var r = from x in re.RuleSetEntities
                            where x.RootRuleSetId == rootRuleSetId
                            orderby x.Version descending
                            select new RuleSetData()
                            {
                                RuleSetId       = x.RuleSetId,
                                RootRuleSetId   = x.RootRuleSetId,
                                Name            = x.Name,
                                Version         = x.Version,
                                RuleSetXML      = x.RuleSetXML,
                                Status          = x.Status,
                                AssemblyPath    = x.AssemblyPath,
                                EntityClassType = x.EntityClassType,
                                //ModifiedDate    = x.ModifiedDate,
                                Dirty           = false
                            };

                    RuleSetData rse = r.First();

                    return rse;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                string a = ex.Message;
            }
            return null;
        }

        public void ImportRuleSetHistory(List<RuleSetDataExport> ruleSet, string who)
        {
            bool first = true;
            int commonRootId = -1;

            foreach (var rs in ruleSet)
            {
                if (first)
                {
                    RuleSetData rsd = new RuleSetData();

                    rsd.AssemblyPath = rs.AssemblyPath;
                    rsd.EntityClassType = rs.EntityClassType;
                    rsd.Version = rs.Version;
                    rsd.Status = rs.Status;
                    rsd.RuleSetXML = rs.RuleSetXML;
                    rsd.Name = rs.Name;
                    //rsd.ModifiedDate = rs.ModifiedDate;
                    //rsd.ModifiedBy = rs.ModifiedBy;
                    rsd.CreatedDate = rs.CreatedDate;
                    rsd.CreatedBy = rs.CreatedBy;

                    rsd = InsertRuleSet(rsd, who);

                    commonRootId = rsd.RootRuleSetId;
                }
                else if ( commonRootId >= 0 )
                {
                    using (var re = new RulesEntities())
                    {
                        #region Insert New Ruleset
                        var rsn = re.RuleSetEntities.Create();

                        rsn.RootRuleSetId = commonRootId;
                        rsn.AssemblyPath = rs.AssemblyPath;
                        rsn.EntityClassType = rs.EntityClassType;
                        rsn.Version = rs.Version;
                        rsn.Status = rs.Status;
                        rsn.RuleSetXML = rs.RuleSetXML;
                        rsn.Name = rs.Name;
                        //rsn.ModifiedDate = rs.ModifiedDate;
                        //rsn.ModifiedBy = rs.ModifiedBy;
                        rsn.CreatedDate = rs.CreatedDate;
                        rsn.CreatedBy = rs.CreatedBy;

                        re.RuleSetEntities.Add(rsn);

                        re.SaveChanges();
                        #endregion
                    }
                }
                first = false;
            }
        }

        public List<RuleSetDataExport> GetRuleSetHistoryByRootId(int rootRuleSetId)
        {
            try
            {
                using (var re = new RulesEntities())
                {
                    #region Fetch all rulesets with same root
                    var r = from x in re.RuleSetEntities
                            where x.RootRuleSetId == rootRuleSetId
                            orderby x.Version ascending
                            select new RuleSetDataExport()
                            {
                                RuleSetId = x.RuleSetId,
                                RootRuleSetId = x.RootRuleSetId,
                                Name = x.Name,
                                Version = x.Version,
                                RuleSetXML = x.RuleSetXML,
                                Status = x.Status,
                                AssemblyPath = x.AssemblyPath,
                                EntityClassType = x.EntityClassType,
                                //ModifiedDate = x.ModifiedDate,
                                Dirty = false,
                                CreatedBy = x.CreatedBy,
                                CreatedDate = x.CreatedDate,
                                //ModifiedBy = x.ModifiedBy
                            };

                    return r.ToList();
                    #endregion
                }
            }
            catch (Exception ex)
            {
                string a = ex.Message;
            }
            return null;
        }

        /// <summary>
        /// Assign a new RuleSet to a RuleGroup
        /// </summary>
        /// <param name="ruleGroupId"></param>
        /// <param name="ruleSetId"></param>
        public void AssignRuleSetToGroup(int ruleGroupId, int ruleSetId, string who)
        {
            using (var re = new RulesEntities())
            {
                #region Fetch the Rule Group with given ID (include RuleSets collection)
                var rge = (from x in re.RuleGroupEntities.Include("RuleSets") where x.RuleGroupId == ruleGroupId select x).FirstOrDefault();
                if (rge == null) return;
                #endregion

                #region Fetch the Rule Set with given ID
                var rse = (from x in re.RuleSetEntities where x.RuleSetId == ruleSetId select x).FirstOrDefault();
                if (rse == null) return;
                #endregion

                #region Add Ruleset to collection and save
                rge.RuleSets.Add(rse);

                re.SaveChanges();
                #endregion

                #region Add a SCORE per rule (only if DEFAULT ruleGroup)
                if (rge.RuleClientId == 0)
                {
                    RuleSetData rsd = new RuleSetData(rse);

                    foreach (System.Workflow.Activities.Rules.Rule rule in rsd.RuleSet.Rules)
                    {
                        CreateNewScore(ruleGroupId, rse.RootRuleSetId, rule.Name, who);
                    }
                }
                #endregion
            }
        }

        /// <summary>
        /// Remove a RuleSet from a RuleGroup
        /// </summary>
        /// <param name="ruleGroupId"></param>
        /// <param name="ruleSetId"></param>
        public void RemoveRuleSetFromGroup(int ruleGroupId, int ruleSetId)
        {
            using (var re = new RulesEntities())
            {
                #region Fetch the Rule Group with given ID
                var rge = (from x in re.RuleGroupEntities.Include("RuleScores")
                           where x.RuleGroupId == ruleGroupId
                           select x).FirstOrDefault();

                if (rge == null) return;
                #endregion

                #region Remove the RuleSet (and scores) from the Rule Group collection
                RuleSetEntity rse = (from x in rge.RuleSets where x.RuleSetId == ruleSetId select x).FirstOrDefault();

                if (rse != null)
                {
                    #region Remove SCORES
                    RuleSetData rsd = new RuleSetData(rse);

                    foreach (System.Workflow.Activities.Rules.Rule rule in rsd.RuleSet.Rules)
                    {
                        RuleScoreEntity se = (from x in rge.RuleScores where x.RuleName == rule.Name && x.RootRuleSetId == rse.RootRuleSetId select x).FirstOrDefault();

                        if (se != null)
                        {
                            rge.RuleScores.Remove(se);

                            re.RuleScoreEntities.Remove(se);
                        }
                    }
                    re.SaveChanges();

                    #endregion

                    rge.RuleSets.Remove(rse);
                }
                #endregion

                re.SaveChanges();
            }
        }

        /// <summary>
        /// Clone the given Rule Set to create a working copy
        /// </summary>
        /// <param name="ruleSetId"></param>
        /// <returns></returns>
        public RuleSetData CloneRuleSet(int ruleSetId, string who)
        {
            using (var re = new RulesEntities())
            {

                #region Fetch the given Rule Set
                var r = from x in re.RuleSetEntities
                        where x.RuleSetId == ruleSetId
                        select x;

                RuleSetEntity rse = r.FirstOrDefault();
                if (rse == null) return null;
                #endregion

                #region Create a copy of rule set and save
                RuleSetEntity newSet = re.RuleSetEntities.Create();

                newSet.RootRuleSetId   = rse.RootRuleSetId;   // same root
                newSet.AssemblyPath    = rse.AssemblyPath;
                newSet.CreatedBy       = who;
                newSet.CreatedDate     = DateTime.Now;
                newSet.EntityClassType = rse.EntityClassType;
                newSet.Version         = rse.Version + 1;
                //newSet.ModifiedBy      = rse.ModifiedBy;
                //newSet.ModifiedDate    = rse.ModifiedDate;
                newSet.Name            = rse.Name;
                newSet.RuleSetXML      = rse.RuleSetXML;
                newSet.Status          = 0;   // Working copy

                re.RuleSetEntities.Add(newSet);
                #endregion

                re.SaveChanges();

                return new RuleSetData(newSet);
            }
        }
        #endregion

        #region RuleSetGroup Methods

        /// <summary>
        /// Iterate through every Client and create initial Rule Group. If it already exists then nothing is created
        /// </summary>
        public void CreateAllInitialRuleSetGroups(string who)
        {
            using( var re = new RulesEntities() )
            {
                var r = from x in re.RuleClientEntities select x;

                foreach( RuleClientEntity client in r )
                {
                    foreach (KeyValuePair<string, String> kvp in EntityStructure)
                    {
                        if ( !string.IsNullOrEmpty(kvp.Value) )
                            CreateInitialRuleSetGroup(kvp.Key, client.RuleClientId, who);
                    }

                    re.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Create Rule Group for given entityPath for given client ID
        /// </summary>
        /// <param name="entityPath"></param>
        /// <param name="clientId"></param>
        /// <param name="who">User making the change</param>
        /// <returns></returns>
        public RuleGroupData CreateInitialRuleSetGroup(string entityPath, int clientId, string who)
        {
            using( var re = new RulesEntities() )
            {
                #region Check if Working Rule Group for this client exists. Return it if found
                var r = (from x in re.RuleGroupEntities
                         where x.RuleGroupName == entityPath && x.Version == 1 && x.RuleClientId == clientId
                         select x).FirstOrDefault();

                if (r != null) return new RuleGroupData(r);
                #endregion

                #region Create New Rule Group
                RuleGroupEntity rge = re.RuleGroupEntities.Create();

                rge.RuleGroupName   = entityPath;
                rge.EntityClassType = EntityStructure[entityPath];
                rge.RuleClientId    = clientId;
                rge.Status          = 0;
                rge.Version         = 1;
                rge.CreatedDate     = DateTime.Now;
                rge.CreatedBy       = who;

                re.RuleGroupEntities.Add(rge);

                re.SaveChanges();

                rge.RootRuleGroupId = rge.RuleGroupId;
                #endregion

                re.SaveChanges();

                return new RuleGroupData(rge);
            }
        }

        /// <summary>
        /// Check is client name exists (case insensitive)
        /// </summary>
        /// <param name="name">Name to check</param>
        /// <returns>True/False</returns>
        public bool ClientExists(string name)
        {
            using (var re = new RulesEntities())
            {
                var c = (from x in re.RuleClientEntities where x.ClientName == name select x).FirstOrDefault();

                return c != null;
            }
        }

        public void CreateNewClientRuleGroups(string newName, string who)
        {
            using( var re = new RulesEntities() )
            {
                using (var transaction = re.Database.BeginTransaction())
                {
                    try
                    {
                        var client = (from x in re.RuleClientEntities where x.ClientName == newName select x).FirstOrDefault();
                        int newClientId = -1;

                        if (client == null)
                        {
                            #region Creat new client record
                            var newClient = re.RuleClientEntities.Create();

                            newClient.ClientName = newName;

                            re.RuleClientEntities.Add(newClient);

                            re.SaveChanges();

                            newClientId = newClient.RuleClientId;
                            #endregion
                        }
                        else
                            newClientId = client.RuleClientId;

                        #region Clone all working groups for default
                        var workingrg = (from x in re.RuleGroupEntities
                                         where x.RuleClientId == 0 && x.Status == 0
                                         select x);

                        foreach (var rge in workingrg)
                        {
                            CloneDefaultRuleGroupForNewClient(re, rge.RuleGroupId, newClientId, who);
                        }
                        #endregion

                        #region Clone all Live groups for default that have not just been cloned as working groups
                        // Get default RuleGroupId List
                        var rg = (from x in re.RuleGroupEntities
                                  where x.RuleClientId == 0 && x.Status == 1 && !workingrg.Any(a => a.RootRuleGroupId == x.RootRuleGroupId)
                                  select x.RuleGroupId).Distinct();

                        foreach (int rge in rg)
                        {
                            CloneDefaultRuleGroupForNewClient(re, rge, newClientId, who);
                        }
                        #endregion

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// Call when a new ruleset version is created. This method will find all groups that use the "old" version, create a working
        /// copy of the group (if necessary) and assign the new rulesetid in the place of the old one.
        /// </summary>
        /// <param name="oldRuleSetId"></param>
        /// <param name="newRuleSetId"></param>
        public void AssignNewWorkingRuleSetToAllRuleGroups(int oldRuleSetId, int newRuleSetId, string who)
        {
            using (var re = new RulesEntities())
            {

                #region Create a new working copy of every RuleGroup that uses the old ruleset

                // Fetch ALL live rule groups

                var liveGroupList = (from x in re.RuleGroupEntities.Include("RuleSets")
                                     where x.Status == 1 && x.RuleSets.Any( rs => rs.RuleSetId == oldRuleSetId )
                                     select x).ToList();

                // iterate through and see which ones are using the old ruleset ID
                foreach (RuleGroupEntity rge in liveGroupList)
                {
                    // check if already have working version
                    var z = (from x in re.RuleGroupEntities
                             where x.RootRuleGroupId == rge.RootRuleGroupId && x.Status == 0
                             select x.RuleGroupId).FirstOrDefault();

                    if ( z == 0 )
                        //RuleSetEntity rse = (from x in rge.RuleSets where x.RuleSetId == oldRuleSetId select x).FirstOrDefault();

                    // if found then create a new working copy
                    //if (rse != null)
                    //{
                        CreateWorkingRuleGroup(rge.RuleGroupId, who);
                    //}
                }

                #endregion

                //-------------------
                // Now we know that ALL rule groups that refer to this rule set have a working copy
                //-------------------

                #region Reassign new ruleset to every working copy that used the old ruleset

                var newRuleSet = (from x in re.RuleSetEntities where x.RuleSetId == newRuleSetId select x).FirstOrDefault();

                if (newRuleSet != null)
                {
                    var workingGroupList = (from x in re.RuleGroupEntities.Include("RuleSets")
                                            where x.Status == 0 && x.RuleSets.Any(rs => rs.RuleSetId == oldRuleSetId)
                                            select x).ToList();

                    foreach (RuleGroupEntity rge in workingGroupList)
                    {
                        // find the ruleset in the group collection
                        RuleSetEntity rse = (from x in rge.RuleSets where x.RuleSetId == oldRuleSetId select x).FirstOrDefault();

                        // if found remove it and add the new rule set instead
                        if (rse != null)
                        {
                            rge.RuleSets.Remove(rse);
                            rge.RuleSets.Add(newRuleSet);
                        }
                    }
                }

                #endregion

                re.SaveChanges();
            }
        }

        /// <summary>
        /// Publish the given Rule Group
        /// </summary>
        /// <param name="ruleGroupId"></param>
        public void PublishRuleGroup(int ruleGroupId)
        {
            using (var re = new RulesEntities())
            {
                #region Fetch Group based on ID and set status to 1
                var rge = (from x in re.RuleGroupEntities
                           where x.RuleGroupId == ruleGroupId
                           select x).FirstOrDefault();

                if (rge == null) return;

                rge.Status = 1;
                #endregion

                re.SaveChanges();
            }
        }

        /// <summary>
        /// Release all groups and rulesets marked as "working" (status=0) to "live" (status=1). But first the current "live" (status=1)
        /// versions of these records must be moved to "archive" (status=2)
        /// </summary>
        public void ReleaseWorkingSetsToLive()
        {
            using (var re = new RulesEntities())
            {

                #region mark all working rulesets as status = live (1) and previous status live (1) to archive (2)

                // Get all the working rule sets
                var rse = from x in re.RuleSetEntities where x.Status == 0 select x;

                foreach (RuleSetEntity rs in rse)
                {
                    #region previous live to archive
                    RiskEngine.DAL.RulesEntities re2 = new RulesEntities();

                    var rse2 = (from x in re2.RuleSetEntities
                                where x.Status == 1 && x.RootRuleSetId == rs.RootRuleSetId
                                select x).FirstOrDefault();

                    if (rse2 != null)
                        rse2.Status = 2;

                    re2.SaveChanges();
                    #endregion

                    rs.Status = 1;
                }

                re.SaveChanges();
                #endregion

                #region mark all working groups as status = live (1) and previous status live (1) to archive (2)

                // get all the working rule groups
                var rge = from x in re.RuleGroupEntities where x.Status == 0 select x;

                foreach (RuleGroupEntity rg in rge)
                {
                    #region previous live to archive

                    RiskEngine.DAL.RulesEntities re2 = new RulesEntities();

                    var rge2 = (from x in re2.RuleGroupEntities
                                where x.Status == 1 && x.RootRuleGroupId == rg.RootRuleGroupId
                                select x).FirstOrDefault();

                    if (rge2 != null)
                        rge2.Status = 2;

                    re2.SaveChanges();

                    #endregion

                    rg.Status = 1;
                }

                re.SaveChanges();
                #endregion
            }

        }


        /// <summary>
        /// Create a working rule group from an existing rule group
        /// </summary>
        /// <param name="oldRuleGroupId">ID of the Group to clone</param>
        /// <param name="who">User making the change</param>
        /// <returns></returns>
        public RuleGroupData CreateWorkingRuleGroup(int oldRuleGroupId, string who)
        {
            RuleGroupEntity newrge = null;

            using (RiskEngine.DAL.RulesEntities re = new RulesEntities())
            {
                using (var transaction = re.Database.BeginTransaction())
                {
                    try
                    {
                        #region Fetch the Rule Group and associated RuleSets, Scores and Variables
                        var rge = (from x in re.RuleGroupEntities.Include("RuleSets").Include("RuleScores").Include("RuleVariables")
                                   where x.RuleGroupId == oldRuleGroupId
                                   select x).FirstOrDefault();

                        if (rge == null) return null;
                        #endregion

                        #region Check if a working rule group already exists with the same root rule group id
                        var chk = (from x in re.RuleGroupEntities
                                   where x.RootRuleGroupId == rge.RootRuleGroupId && x.Status == 0
                                   select x).FirstOrDefault();

                        if (chk != null)
                            return GetRuleGroupById(re, chk.RuleGroupId);
                        #endregion

                        #region Clone the group

                        newrge = re.RuleGroupEntities.Create();

                        newrge.RuleClientId = rge.RuleClientId;
                        newrge.RootRuleGroupId = rge.RootRuleGroupId;
                        newrge.Version = rge.Version + 1;
                        newrge.RuleGroupName = rge.RuleGroupName;
                        newrge.EntityClassType = rge.EntityClassType;
                        newrge.Status = 0;
                        newrge.CreatedDate = DateTime.Now;
                        newrge.CreatedBy = who;

                        re.RuleGroupEntities.Add(newrge);

                        #endregion

                        #region Clone RuleSets - point new group at the same rule sets

                        foreach (RuleSetEntity rse in rge.RuleSets)
                            newrge.RuleSets.Add(rse);

                        #endregion

                        #region Clone the Variables
                        foreach (RuleVariableEntity rve in rge.RuleVariables)
                        {
                            RuleVariableEntity newVar = re.RuleVariableEntities.Create();

                            newVar.VariableName = rve.VariableName;
                            newVar.VariableValue = rve.VariableValue;
                            newVar.CreatedBy = rve.CreatedBy;
                            newVar.CreatedDate = rve.CreatedDate;
                            newVar.ModifiedBy = who;
                            newVar.ModifiedDate = DateTime.Now;
                            newVar.RuleGroupId = newrge.RuleGroupId;

                            newrge.RuleVariables.Add(newVar);
                        }
                        #endregion

                        #region Clone the Scores
                        foreach (RuleScoreEntity rse in rge.RuleScores)
                        {
                            RuleScoreEntity newScore = re.RuleScoreEntities.Create();

                            newScore.ThenScore = rse.ThenScore;
                            newScore.ThenError = rse.ThenError;
                            newScore.ThenMessageHigh = rse.ThenMessageHigh;
                            newScore.ThenMessageLow = rse.ThenMessageLow;
                            newScore.ThenMessageMedium = rse.ThenMessageMedium;

                            newScore.ElseScore = rse.ElseScore;
                            newScore.ElseError = rse.ElseError;
                            newScore.ElseMessageHigh = rse.ElseMessageHigh;
                            newScore.ElseMessageLow = rse.ElseMessageLow;
                            newScore.ElseMessageMedium = rse.ElseMessageMedium;

                            newScore.CreatedBy = rse.CreatedBy;
                            newScore.CreatedDate = rse.CreatedDate;
                            newScore.ModifiedBy = who;
                            newScore.ModifiedDate = DateTime.Now;

                            newScore.RuleName = rse.RuleName;
                            newScore.RuleGroupId = newrge.RuleGroupId;
                            newScore.RootRuleSetId = rse.RootRuleSetId;

                            newrge.RuleScores.Add(newScore);
                        }
                        #endregion

                        re.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
                return GetRuleGroupById(re, newrge.RuleGroupId);
            }
         
        }

        /// <summary>
        /// This method will create the starter Rule Group for a new client based on the ID of the default Rule Group and the Client ID
        /// </summary>
        /// <param name="defaultRuleGroupId">Rule Group ID of the default rule group</param>
        /// <param name="newClientId">New client ID</param>
        /// <param name="who">User making the change</param>
        /// <returns></returns>
        public RuleGroupData CloneDefaultRuleGroupForNewClient(RulesEntities re, int defaultRuleGroupId, int newClientId, string who)
        {
            //using (RiskEngine.DAL.RulesEntities re = new RulesEntities())
            //{
                //using (var transaction = new TransactionScope())
                //{
                    #region Fetch the rule group
                    var rge = (from x in re.RuleGroupEntities
                               where x.RuleGroupId == defaultRuleGroupId
                               select x).FirstOrDefault();

                    if (rge == null || rge.RuleClientId != 0) return null;
                    #endregion

                    #region Clone the group

                    RuleGroupEntity newrge = re.RuleGroupEntities.Create();

                    //foreach(var x in rge.RuleSets)
                    //{
                    //    newrge.RuleSets.Add(x);
                    //}

                    newrge.RuleClientId    = newClientId;
                    newrge.RootRuleGroupId = 0;   //changed later below
                    newrge.Version         = 1;
                    newrge.RuleGroupName   = rge.RuleGroupName;
                    newrge.EntityClassType = rge.EntityClassType;
                    newrge.Status          = 0;
                    newrge.CreatedDate     = DateTime.Now;
                    newrge.CreatedBy       = who;

                    re.RuleGroupEntities.Add(newrge);

                    re.SaveChanges();
                    newrge.RootRuleGroupId = newrge.RuleGroupId;  // changed here
                    re.SaveChanges();

                    #endregion

                    //re.SaveChanges();

                    //transaction.Complete();

                    return GetRuleGroupById(re, newrge.RuleGroupId);
                //}
            //}
        }

        /// <summary>
        /// Fetch the current WORKING Rule Group for the given entityPath.  This method will fetch the CUSTOM group and then
        /// fetch the DEFAULT group. The Scores, Vars and Rule Sets of the custom group are assigned into "DEF" collections
        /// </summary>
        /// <param name="entityPath"></param>
        /// <param name="orgId"></param>
        /// <returns></returns>
        public RuleGroupData GetWorkingRuleGroupByPath(string entityPath, int orgId)
        {
            // Get default
            RuleGroupData rgd = GetRuleGroupByPath(entityPath, 0, 0);

            // We didn't find a working group so try and get the current LIVE group
            if (rgd == null)
                rgd = GetRuleGroupByPath(entityPath, 0, 1);

            if (orgId > 0)
            {
                //Fetch the Rule Group for the ID. This could be CUSTOM or DEFAULT
                RuleGroupData rgdCustom = GetRuleGroupByPath(entityPath, orgId, 0);

                if (rgdCustom == null)
                    rgdCustom = GetRuleGroupByPath(entityPath, orgId, 1);

                if (rgd == null)
                    rgd = rgdCustom;

                else if (rgdCustom != null)
                {
                    rgdCustom.DefRuleScores    = rgd.DefRuleScores;
                    rgdCustom.DefRuleVariables = rgd.DefRuleVariables;
                    rgdCustom.DefRuleSets      = rgd.DefRuleSets;

                    return rgdCustom;
                }
            }

            return rgd;
        }

        public RuleGroupData GetLiveRuleGroupByPath(string entityPath, int orgId)
        {
            // Now try and find the DEFAULT LIVE group
            RuleGroupData rgd = GetRuleGroupByPath(entityPath, 0, 1);

            if (orgId > 0)
            {
                //Fetch the Rule Group for the ID. This could be CUSTOM or DEFAULT
                RuleGroupData rgdCustom = GetRuleGroupByPath(entityPath, orgId, 1);

                if (rgd == null)
                    rgd = rgdCustom;
                
                else if (rgdCustom != null)
                {
                    rgdCustom.DefRuleScores = rgd.DefRuleScores;
                    rgdCustom.DefRuleVariables = rgd.DefRuleVariables;
                    rgdCustom.DefRuleSets = rgd.DefRuleSets;

                    return rgdCustom;
                }
            }

            return rgd;
        }

        /// <summary>
        /// Get the Rule Group for given path (rule group name)
        /// </summary>
        /// <param name="entityPath">Rule Group Name</param>
        /// <param name="clientId">Client ID - 0 means default</param>
        /// <param name="status"> 0 - working, 1 - live </param>
        /// <returns></returns>
        private RuleGroupData GetRuleGroupByPath(string entityPath, int clientId, int? status) 
        {
            using (var re = new RulesEntities())
            {
                #region Fetch Rule Group from Name/Path, client ID and status to get the RuleGroupId
                var rge = (from x in re.RuleGroupEntities
                           where x.RuleGroupName == entityPath && x.RuleClientId == clientId && x.Status == status
                           select x).FirstOrDefault();

                if (rge == null) return null;
                #endregion

                return GetRuleGroupById(re, rge.RuleGroupId);
            }
        }

        public RuleGroupData GetRuleGroupById(RulesEntities re, int ruleGroupId) 
        {
            //using (var re = new RulesEntities())
            //{
                #region Fetch the Rule Group and associated RuleSets, Scores and Variables based on ID
                var rge = (from x in re.RuleGroupEntities.Include("RuleSets").Include("RuleScores").Include("RuleVariables")
                           where x.RuleGroupId == ruleGroupId
                           select x).FirstOrDefault();

                if (rge == null) return null;
                #endregion

                RuleGroupData ret = new RuleGroupData(rge);

                List<RuleSetData> rsdlist = new List<RuleSetData>();

                #region Copy across and convert to RuleSetData all the Rule Sets
                foreach (RuleSetEntity rse in rge.RuleSets)
                {
                    rsdlist.Add(new RuleSetData(rse));
                }

                // if default client ID put the collection in the DEF collection
                if (rge.RuleClientId == 0)
                {
                    ret.DefRuleSets = rsdlist;
                    ret.RuleSets = new List<RuleSetData>();
                }
                else
                {
                    ret.DefRuleSets = new List<RuleSetData>();
                    ret.RuleSets = rsdlist;
                }
                #endregion

                #region Copy across and convert to RuleScoreData all the Rule Scores
                List<RuleScoreData> rslist = new List<RuleScoreData>();
                foreach (RuleScoreEntity x in rge.RuleScores)
                {
                    rslist.Add(new RuleScoreData(x));
                }

                // if default client ID put the collection in the DEF collection
                if (rge.RuleClientId == 0)
                {
                    ret.DefRuleScores = rslist;
                    ret.RuleScores = new List<RuleScoreData>();
                }
                else
                {
                    ret.RuleScores = rslist;
                    ret.DefRuleScores = new List<RuleScoreData>();
                }
                #endregion

                #region Copy across and convert to RuleVariableData all the Rule Variables
                List<RuleVariableData> rvlist = new List<RuleVariableData>();
                foreach (RuleVariableEntity x in rge.RuleVariables)
                {
                    rvlist.Add(new RuleVariableData(x));
                }

                // if default client ID put the collection in the DEF collection
                if (rge.RuleClientId == 0)
                {
                    ret.DefRuleVariables = rvlist;
                    ret.RuleVariables = new List<RuleVariableData>();
                }
                else
                {
                    ret.RuleVariables = rvlist;
                    ret.DefRuleVariables = new List<RuleVariableData>();
                }
                #endregion

                return ret;
            //}
        }
        #endregion

        #region RuleSet Scores Methods

        /// <summary>
        /// Go through all WORKING GROUPS and clean up the scores. Remove unused, add missing.
        /// </summary>
        /// <param name="who">User making the change</param>
        public void CleanUpAllRuleGroupScores(string who)
        {
            using (var re = new RulesEntities())
            {
                var rg = from x in re.RuleGroupEntities.Include("RuleSets").Include("RuleScores")
                         where x.Status == 0
                         select x;

                foreach (RuleGroupEntity rge in rg.ToList())
                    CleanUpRuleGroupScores(rge, who);
            }
        }

        /// <summary>
        /// Clean up the scores in given GROUP.  Remove unused, add missing.
        /// </summary>
        /// <param name="rge">Rule Group Entity to check</param>
        /// <param name="who">User making the change</param>
        public void CleanUpRuleGroupScores(RuleGroupEntity rge, string who)
        {
            using (var re = new RulesEntities())
            {
               

                //int t = (from xa in re.RuleScoreEntities where xa.RuleScoreId != null select xa).Count();



                foreach (RuleSetEntity rse in rge.RuleSets)
                {
                    RuleSetData rsd = new RuleSetData(rse);

                    #region Remove orphaned scores
                    foreach (RuleScoreEntity rs in rge.RuleScores.ToList())
                    {
                        if (rs.RootRuleSetId == rse.RootRuleSetId)
                        {
                            var rule = (from x in rsd.RuleSet.Rules where x.Name == rs.RuleName select x).FirstOrDefault();

                            if (rule == null)  // we have a score but no rule for it, so remove it
                            {
                                rge.RuleScores.Remove(rs);
                                // Error: (re.RuleScoreEntities.Remove(rs)) The object cannot be deleted because it was not found in the ObjectStateManager
                                // Resolution: As we're using a new context a new Select lookup is needed below as you need to fetch the entity you wish to delete from context first.
                                var removeRuleScore = (from c in re.RuleScoreEntities where c.RuleScoreId == rs.RuleScoreId select c).FirstOrDefault();
                                re.RuleScoreEntities.Remove(removeRuleScore);
                            }
                        }
                    }
                    #endregion

                    re.SaveChanges();

                    #region Add a SCORE per rule if DEFAULT ruleGroup
                    if (rge.RuleClientId == 0)
                    {
                        foreach (System.Workflow.Activities.Rules.Rule rule in rsd.RuleSet.Rules)
                        {
                            var rs = (from x in rge.RuleScores where x.RuleName == rule.Name && x.RootRuleSetId == rse.RootRuleSetId select x).FirstOrDefault();

                            if (rs == null)
                            {
                                CreateNewScore(rge.RuleGroupId, rse.RootRuleSetId, rule.Name, who);
                            }
                        }
                    }
                    #endregion
                }
            }
        }

        /// <summary>
        /// Create a new SCORE entry.
        /// </summary>
        /// <param name="ruleGroupId">GROUP ID add score too</param>
        /// <param name="rootRuleSetId">RULESET ID to add score too</param>
        /// <param name="ruleName">The Rule Name</param>
        /// <param name="who">User making the change</param>
        /// <returns></returns>
        public RuleScoreData CreateNewScore(int ruleGroupId, int rootRuleSetId, string ruleName, string who)
        {
            using (var re = new RulesEntities())
            {
                RuleScoreData rsd = null;

                #region See if we can find existing score

                var rsee = (from x in re.RuleScoreEntities
                           where x.RootRuleSetId == rootRuleSetId && x.RuleGroupId == ruleGroupId && x.RuleName == ruleName
                           select x).FirstOrDefault();

                if (rsee != null) return new RuleScoreData(rsee);
                #endregion

                #region See if we have an existing score 
                var rge = (from x in re.RuleGroupEntities where x.RuleGroupId == ruleGroupId select x).FirstOrDefault();

                if (rge != null)
                {
                    RuleGroupData rgd = GetWorkingRuleGroupByPath(rge.RuleGroupName, rge.RuleClientId);

                    if (rgd != null)
                    {
                        rsd = (from x in rgd.RuleScores where x.RuleName == ruleName && x.RootRuleSetId == rootRuleSetId select x).FirstOrDefault();

                        if (rsd == null)
                            rsd = (from x in rgd.DefRuleScores where x.RuleName == ruleName && x.RootRuleSetId == rootRuleSetId select x).FirstOrDefault();
                    }
                }
                #endregion

                #region Create new Score entity and commit
                RuleScoreEntity rse = re.RuleScoreEntities.Create();

                rse.RuleGroupId         = ruleGroupId;
                rse.RootRuleSetId       = rootRuleSetId;
                rse.RuleName            = ruleName;

                if (rsd != null)
                {
                    rse.ThenScore = rsd.ThenScore;
                    rse.ElseScore = rsd.ElseScore;

                    rse.ThenMessageLow = rsd.ThenMessageLow;
                    rse.ThenMessageMedium = rsd.ThenMessageMedium;
                    rse.ThenMessageHigh = rsd.ThenMessageHigh;
                    rse.ThenError = rsd.ThenError;


                    rse.ElseMessageLow = rsd.ElseMessageLow;
                    rse.ElseMessageMedium = rsd.ElseMessageMedium;
                    rse.ElseMessageHigh = rsd.ElseMessageHigh;
                    rse.ElseError = rsd.ElseError;
                }
                else
                {
                    rse.ThenScore = 0;
                    rse.ElseScore = 0;
#if false
                    rse.ThenMessageLow = "THENLOW : " + ruleGroupId.ToString() + ", " + rootRuleSetId.ToString() + ", " + ruleName;
                    rse.ThenMessageHigh = "THENHIGH : " + ruleGroupId.ToString() + ", " + rootRuleSetId.ToString() + ", " + ruleName;
                    rse.ThenMessageMedium = "THENMEDIUM : " + ruleGroupId.ToString() + ", " + rootRuleSetId.ToString() + ", " + ruleName;
                    rse.ThenError = "THENERROR : " + ruleGroupId.ToString() + ", " + rootRuleSetId.ToString() + ", " + ruleName;

                    rse.ElseMessageLow = "ELSELOW : " + ruleGroupId.ToString() + ", " + rootRuleSetId.ToString() + ", " + ruleName;
                    rse.ElseMessageHigh = "ELSEHIGH : " + ruleGroupId.ToString() + ", " + rootRuleSetId.ToString() + ", " + ruleName;
                    rse.ElseMessageMedium = "ELSEMEDIUM : " + ruleGroupId.ToString() + ", " + rootRuleSetId.ToString() + ", " + ruleName;
                    rse.ElseError = "ELSEERROR : " + ruleGroupId.ToString() + ", " + rootRuleSetId.ToString() + ", " + ruleName;
#else
                    rse.ThenMessageLow = string.Empty;
                    rse.ThenMessageHigh = string.Empty;
                    rse.ThenMessageMedium = string.Empty;
                    rse.ThenError           = string.Empty;


                    rse.ElseMessageLow = string.Empty;
                    rse.ElseMessageHigh = string.Empty;
                    rse.ElseMessageMedium = string.Empty;
                    rse.ElseError           = string.Empty;
#endif
                }
                rse.CreatedBy           = who;
                rse.CreatedDate         = DateTime.Now;

                re.RuleScoreEntities.Add(rse);

                re.SaveChanges();

                return new RuleScoreData(rse);
                #endregion
            }
        }

        /// <summary>
        /// Update the given SCORE. Uses the provided ID to locate the record and then overwrites the score fields.
        /// </summary>
        /// <param name="rsd"></param>
        /// <param name="who">User making the change</param>
        public void UpdateScore(RuleScoreData rsd, string who)
        {
            using (var re = new RulesEntities())
            {
                #region Fetch Rule Score based on rsd.RuleScoreId
                var rse = (from x in re.RuleScoreEntities
                           where x.RuleScoreId == rsd.RuleScoreId
                           select x).FirstOrDefault();

                if (rse == null) return;
                #endregion

                #region Assign all fields and save
                rse.ThenScore           = rsd.ThenScore;
                rse.ElseScore           = rsd.ElseScore;
                rse.ThenMessageLow      = rsd.ThenMessageLow;
                rse.ElseMessageLow      = rsd.ElseMessageLow;
                rse.ThenMessageMedium   = rsd.ThenMessageMedium;
                rse.ElseMessageMedium   = rsd.ElseMessageMedium;
                rse.ThenMessageHigh     = rsd.ThenMessageHigh;
                rse.ElseMessageHigh     = rsd.ElseMessageHigh;
                rse.ThenError           = rsd.ThenError;
                rse.ElseError           = rsd.ElseError;

                rse.ModifiedBy          = who;
                rse.ModifiedDate        = DateTime.Now;

                re.SaveChanges();
                #endregion
            }
        }

        //public List<RuleScoreData> GetRuleSetScoreData(int ruleSetId, int organisationId)
        //{
        //    List<RuleScoreData> list = new List<RuleScoreData>();

        //    RiskEngine.DAL.RulesEntities re = new RulesEntities();

        //    var rg = from x in re.RuleScoreEntities
        //             //join r in re.RuleSetEntities on x.RuleSetId equals r.ObjId
        //             where x.RuleSetId == ruleSetId && x.OrganisationId == organisationId
        //             select new RuleScoreData 
        //             { 
        //                RuleName = x.RuleName,
        //                ThenScore = x.ThenScore,
        //                ElseScore = x.ElseScore,
        //                ThenMessage = x.ThenMessage,
        //                ElseMessage = x.ElseMessage,
        //                ThenError = x.ThenError,
        //                ElseError = x.ElseError
        //             };

        //    return rg.ToList<RuleScoreData>();
        //}
        #endregion

        #region RuleSet Variables Methods
        //public List<RuleVariableData> GetRuleSetVariableData(int ruleSetId, int organisationId)
        //{
        //    List<RuleVariableData> list = new List<RuleVariableData>();

        //    RiskEngine.DAL.RulesEntities re = new RulesEntities();

        //    var rg = from x in re.RuleVariableEntities
        //             where x.RuleSetId == ruleSetId && x.OrganisationId == organisationId
        //             select new RuleVariableData
        //             {
        //                 RuleName = x.RuleName,
        //                 VariableName = x.VariableName,
        //                 VariableValue = x.VariableValue

        //             };

        //    return rg.ToList<RuleVariableData>();
        //}

        public RuleVariableData InsertVarValue(int ruleGroupid, string varName, string varValue, string who)
        {
            using (var re = new RulesEntities())
            {
                #region Check is var with same name, in same group, exists. If so Update it
                var v = (from x in re.RuleVariableEntities 
                         where x.RuleGroupId == ruleGroupid && x.VariableName == varName 
                         select x).FirstOrDefault();

                if (v != null)
                    return UpdateVarValue(v.RuleVariableId, varName, varValue, who);
                #endregion

                #region Create and Insert new variable
                RuleVariableEntity rve = re.RuleVariableEntities.Create();

                rve.RuleGroupId   = ruleGroupid;
                rve.VariableName  = varName;
                rve.VariableValue = varValue;
                rve.CreatedDate   = DateTime.Now;
                rve.CreatedBy     = who;

                re.RuleVariableEntities.Add(rve);

                re.SaveChanges();
                #endregion

                return new RuleVariableData(rve);
            }
        }

        public RuleVariableData UpdateVarValue(int ruleVariableId, string varName, string varValue, string who)
        {
            using (var re = new RulesEntities())
            {
                #region Fetch and update variable based on ID
                var rve = (from x in re.RuleVariableEntities
                           where x.RuleVariableId == ruleVariableId
                           select x).FirstOrDefault();

                if (rve != null)
                {
                    rve.VariableName  = varName;
                    rve.VariableValue = varValue;
                    rve.ModifiedDate  = DateTime.Now;
                    rve.ModifiedBy    = who;

                    re.SaveChanges();

                    return new RuleVariableData(rve);
                }
                return null;
                #endregion
            }
        }

        public void DeleteVarValue(int ruleVariableId)
        {
            using (var re = new RulesEntities())
            {
                #region Delete variable based on ID
                RuleVariableEntity rve = (from x in re.RuleVariableEntities
                                          where x.RuleVariableId == ruleVariableId
                                          select x).FirstOrDefault();
                if (rve != null)
                {
                    re.RuleVariableEntities.Remove(rve);

                    re.SaveChanges();
                }
                #endregion
            }
        }

        //public List<RuleVariableData> GetRuleSetVariableData(int ruleVariableId)
        //{
        //    List<RuleVariableData> list = new List<RuleVariableData>();

        //    RiskEngine.DAL.RulesEntities re = new RulesEntities();

        //    var rg = from x in re.RuleVariableEntities
        //             where x.RuleVariableId == ruleVariableId
        //             select new RuleVariableData()
        //             {
        //                 RuleVariableId = x.RuleVariableId,
        //                 CreatedBy = x.CreatedBy,
        //                 CreatedDate = x.CreatedDate,
        //                 ModifiedBy = x.ModifiedBy,
        //                 ModifiedDate = x.ModifiedDate,
        //                 VariableName = x.VariableName,
        //                 VariableValue = x.VariableValue

        //             };

        //    return rg.ToList<RuleVariableData>();
        //}
        #endregion

    }
}
