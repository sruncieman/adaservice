﻿namespace MDA.FileEditor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.claimsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.claimBatchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.vehiclesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.peopleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.addressesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBox6 = new System.Windows.Forms.ListBox();
            this.addressesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.organisationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBox5 = new System.Windows.Forms.ListBox();
            this.btnClaimNew = new System.Windows.Forms.Button();
            this.btnClaimDel = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnClaimEdit = new System.Windows.Forms.Button();
            this.btnVehicleEdit = new System.Windows.Forms.Button();
            this.btnVehicleDel = new System.Windows.Forms.Button();
            this.btnVehicleNew = new System.Windows.Forms.Button();
            this.btnPeopleEdit = new System.Windows.Forms.Button();
            this.btnPeopleDel = new System.Windows.Forms.Button();
            this.btnPeopleNew = new System.Windows.Forms.Button();
            this.btnOrgEdit = new System.Windows.Forms.Button();
            this.btnOrgDel = new System.Windows.Forms.Button();
            this.btnOrgNew = new System.Windows.Forms.Button();
            this.btnPerAddrEdit = new System.Windows.Forms.Button();
            this.btnPerAddrDel = new System.Windows.Forms.Button();
            this.btnPerAddrNew = new System.Windows.Forms.Button();
            this.btnOrgAddrEdit = new System.Windows.Forms.Button();
            this.btnOrgAddrDel = new System.Windows.Forms.Button();
            this.btnOrgAddrNew = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPersonOrgVehNew = new System.Windows.Forms.Button();
            this.lbPersonOrgAddrVeh = new System.Windows.Forms.ListBox();
            this.orgVehBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.btnPersonOrgVehDelete = new System.Windows.Forms.Button();
            this.btnPersonOrgVehEdit = new System.Windows.Forms.Button();
            this.lbClaimOrg = new System.Windows.Forms.ListBox();
            this.orgsClaimBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lbClaimOrgAddr = new System.Windows.Forms.ListBox();
            this.addressesOrgBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnClaimOrgVehNew = new System.Windows.Forms.Button();
            this.lbClaimOrgAddrVeh = new System.Windows.Forms.ListBox();
            this.orgVehBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.btnClaimOrgVehDelete = new System.Windows.Forms.Button();
            this.btnClaimOrgNew = new System.Windows.Forms.Button();
            this.btnClaimOrgVehEdit = new System.Windows.Forms.Button();
            this.btnClaimOrgDel = new System.Windows.Forms.Button();
            this.btnClaimOrgEdit = new System.Windows.Forms.Button();
            this.btnClaimOrgAddrNew = new System.Windows.Forms.Button();
            this.btnClaimOrgAddrDel = new System.Windows.Forms.Button();
            this.btnClaimOrgAddrEdit = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.btnUpload = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.btnScore = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.claimsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimBatchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehiclesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peopleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.organisationsBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orgVehBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orgsClaimBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesOrgBindingSource1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orgVehBindingSource1)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(849, 517);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 23);
            this.button1.TabIndex = 32;
            this.button1.Text = "Prime Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.DataSource = this.claimsBindingSource;
            this.listBox1.DisplayMember = "ClaimNumber";
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(10, 19);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(151, 433);
            this.listBox1.TabIndex = 1;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            this.listBox1.DoubleClick += new System.EventHandler(this.ClaimEdit_Click);
            // 
            // claimsBindingSource
            // 
            this.claimsBindingSource.DataSource = this.claimBatchBindingSource;
            // 
            // claimBatchBindingSource
            // 
            this.claimBatchBindingSource.DataMember = "Claims";
            this.claimBatchBindingSource.DataSource = typeof(MDA.Common.FileModel.MotorClaimBatch);
            // 
            // listBox2
            // 
            this.listBox2.DataSource = this.vehiclesBindingSource;
            this.listBox2.DisplayMember = "DisplayText";
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(6, 19);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(151, 407);
            this.listBox2.TabIndex = 6;
            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            this.listBox2.DoubleClick += new System.EventHandler(this.btnVehicleEdit_Click);
            // 
            // vehiclesBindingSource
            // 
            this.vehiclesBindingSource.DataMember = "Vehicles";
            this.vehiclesBindingSource.DataSource = this.claimsBindingSource;
            // 
            // listBox3
            // 
            this.listBox3.DataSource = this.peopleBindingSource;
            this.listBox3.DisplayMember = "DisplayText";
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Location = new System.Drawing.Point(9, 35);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(175, 121);
            this.listBox3.TabIndex = 11;
            this.listBox3.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            this.listBox3.DoubleClick += new System.EventHandler(this.btnPeopleEdit_Click);
            // 
            // peopleBindingSource
            // 
            this.peopleBindingSource.DataMember = "People";
            this.peopleBindingSource.DataSource = this.vehiclesBindingSource;
            // 
            // listBox4
            // 
            this.listBox4.DataSource = this.addressesBindingSource;
            this.listBox4.DisplayMember = "DisplayText";
            this.listBox4.FormattingEnabled = true;
            this.listBox4.Location = new System.Drawing.Point(190, 35);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(207, 121);
            this.listBox4.TabIndex = 21;
            this.listBox4.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            this.listBox4.DoubleClick += new System.EventHandler(this.btnPerAddrEdit_Click);
            // 
            // addressesBindingSource
            // 
            this.addressesBindingSource.DataMember = "Addresses";
            this.addressesBindingSource.DataSource = this.peopleBindingSource;
            // 
            // listBox6
            // 
            this.listBox6.DataSource = this.addressesBindingSource1;
            this.listBox6.DisplayMember = "DisplayText";
            this.listBox6.FormattingEnabled = true;
            this.listBox6.Location = new System.Drawing.Point(192, 210);
            this.listBox6.Name = "listBox6";
            this.listBox6.Size = new System.Drawing.Size(207, 82);
            this.listBox6.TabIndex = 26;
            this.listBox6.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            this.listBox6.DoubleClick += new System.EventHandler(this.btnOrgAddrEdit_Click);
            // 
            // addressesBindingSource1
            // 
            this.addressesBindingSource1.DataMember = "Addresses";
            this.addressesBindingSource1.DataSource = this.organisationsBindingSource;
            // 
            // organisationsBindingSource
            // 
            this.organisationsBindingSource.DataMember = "Organisations";
            this.organisationsBindingSource.DataSource = this.peopleBindingSource;
            // 
            // listBox5
            // 
            this.listBox5.DataSource = this.organisationsBindingSource;
            this.listBox5.DisplayMember = "DisplayText";
            this.listBox5.FormattingEnabled = true;
            this.listBox5.Location = new System.Drawing.Point(9, 210);
            this.listBox5.Name = "listBox5";
            this.listBox5.Size = new System.Drawing.Size(177, 82);
            this.listBox5.TabIndex = 16;
            this.listBox5.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            this.listBox5.DoubleClick += new System.EventHandler(this.btnOrgEdit_Click);
            // 
            // btnClaimNew
            // 
            this.btnClaimNew.Location = new System.Drawing.Point(117, 458);
            this.btnClaimNew.Name = "btnClaimNew";
            this.btnClaimNew.Size = new System.Drawing.Size(44, 23);
            this.btnClaimNew.TabIndex = 2;
            this.btnClaimNew.Text = "New";
            this.btnClaimNew.UseVisualStyleBackColor = true;
            this.btnClaimNew.Click += new System.EventHandler(this.ClaimNew_Click);
            // 
            // btnClaimDel
            // 
            this.btnClaimDel.Location = new System.Drawing.Point(10, 458);
            this.btnClaimDel.Name = "btnClaimDel";
            this.btnClaimDel.Size = new System.Drawing.Size(51, 23);
            this.btnClaimDel.TabIndex = 3;
            this.btnClaimDel.Text = "Delete";
            this.btnClaimDel.UseVisualStyleBackColor = true;
            this.btnClaimDel.Click += new System.EventHandler(this.ClaimDel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 194);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Organisations";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(187, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Addresses";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(189, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Addresses";
            // 
            // btnClaimEdit
            // 
            this.btnClaimEdit.Location = new System.Drawing.Point(67, 458);
            this.btnClaimEdit.Name = "btnClaimEdit";
            this.btnClaimEdit.Size = new System.Drawing.Size(44, 23);
            this.btnClaimEdit.TabIndex = 4;
            this.btnClaimEdit.Text = "Edit";
            this.btnClaimEdit.UseVisualStyleBackColor = true;
            this.btnClaimEdit.Click += new System.EventHandler(this.ClaimEdit_Click);
            // 
            // btnVehicleEdit
            // 
            this.btnVehicleEdit.Location = new System.Drawing.Point(63, 429);
            this.btnVehicleEdit.Name = "btnVehicleEdit";
            this.btnVehicleEdit.Size = new System.Drawing.Size(44, 23);
            this.btnVehicleEdit.TabIndex = 9;
            this.btnVehicleEdit.Text = "Edit";
            this.btnVehicleEdit.UseVisualStyleBackColor = true;
            this.btnVehicleEdit.Click += new System.EventHandler(this.btnVehicleEdit_Click);
            // 
            // btnVehicleDel
            // 
            this.btnVehicleDel.Location = new System.Drawing.Point(6, 429);
            this.btnVehicleDel.Name = "btnVehicleDel";
            this.btnVehicleDel.Size = new System.Drawing.Size(51, 23);
            this.btnVehicleDel.TabIndex = 8;
            this.btnVehicleDel.Text = "Delete";
            this.btnVehicleDel.UseVisualStyleBackColor = true;
            this.btnVehicleDel.Click += new System.EventHandler(this.btnVehicleDel_Click);
            // 
            // btnVehicleNew
            // 
            this.btnVehicleNew.Location = new System.Drawing.Point(113, 429);
            this.btnVehicleNew.Name = "btnVehicleNew";
            this.btnVehicleNew.Size = new System.Drawing.Size(44, 23);
            this.btnVehicleNew.TabIndex = 7;
            this.btnVehicleNew.Text = "New";
            this.btnVehicleNew.UseVisualStyleBackColor = true;
            this.btnVehicleNew.Click += new System.EventHandler(this.btnVehicleNew_Click);
            // 
            // btnPeopleEdit
            // 
            this.btnPeopleEdit.Location = new System.Drawing.Point(66, 160);
            this.btnPeopleEdit.Name = "btnPeopleEdit";
            this.btnPeopleEdit.Size = new System.Drawing.Size(44, 23);
            this.btnPeopleEdit.TabIndex = 14;
            this.btnPeopleEdit.Text = "Edit";
            this.btnPeopleEdit.UseVisualStyleBackColor = true;
            this.btnPeopleEdit.Click += new System.EventHandler(this.btnPeopleEdit_Click);
            // 
            // btnPeopleDel
            // 
            this.btnPeopleDel.Location = new System.Drawing.Point(9, 160);
            this.btnPeopleDel.Name = "btnPeopleDel";
            this.btnPeopleDel.Size = new System.Drawing.Size(51, 23);
            this.btnPeopleDel.TabIndex = 13;
            this.btnPeopleDel.Text = "Delete";
            this.btnPeopleDel.UseVisualStyleBackColor = true;
            this.btnPeopleDel.Click += new System.EventHandler(this.btnPeopleDel_Click);
            // 
            // btnPeopleNew
            // 
            this.btnPeopleNew.Location = new System.Drawing.Point(116, 160);
            this.btnPeopleNew.Name = "btnPeopleNew";
            this.btnPeopleNew.Size = new System.Drawing.Size(44, 23);
            this.btnPeopleNew.TabIndex = 12;
            this.btnPeopleNew.Text = "New";
            this.btnPeopleNew.UseVisualStyleBackColor = true;
            this.btnPeopleNew.Click += new System.EventHandler(this.btnPeopleNew_Click);
            // 
            // btnOrgEdit
            // 
            this.btnOrgEdit.Location = new System.Drawing.Point(68, 298);
            this.btnOrgEdit.Name = "btnOrgEdit";
            this.btnOrgEdit.Size = new System.Drawing.Size(44, 23);
            this.btnOrgEdit.TabIndex = 19;
            this.btnOrgEdit.Text = "Edit";
            this.btnOrgEdit.UseVisualStyleBackColor = true;
            this.btnOrgEdit.Click += new System.EventHandler(this.btnOrgEdit_Click);
            // 
            // btnOrgDel
            // 
            this.btnOrgDel.Location = new System.Drawing.Point(11, 298);
            this.btnOrgDel.Name = "btnOrgDel";
            this.btnOrgDel.Size = new System.Drawing.Size(51, 23);
            this.btnOrgDel.TabIndex = 18;
            this.btnOrgDel.Text = "Delete";
            this.btnOrgDel.UseVisualStyleBackColor = true;
            this.btnOrgDel.Click += new System.EventHandler(this.btnOrgDel_Click);
            // 
            // btnOrgNew
            // 
            this.btnOrgNew.Location = new System.Drawing.Point(118, 298);
            this.btnOrgNew.Name = "btnOrgNew";
            this.btnOrgNew.Size = new System.Drawing.Size(44, 23);
            this.btnOrgNew.TabIndex = 17;
            this.btnOrgNew.Text = "New";
            this.btnOrgNew.UseVisualStyleBackColor = true;
            this.btnOrgNew.Click += new System.EventHandler(this.btnOrgNew_Click);
            // 
            // btnPerAddrEdit
            // 
            this.btnPerAddrEdit.Location = new System.Drawing.Point(247, 160);
            this.btnPerAddrEdit.Name = "btnPerAddrEdit";
            this.btnPerAddrEdit.Size = new System.Drawing.Size(44, 23);
            this.btnPerAddrEdit.TabIndex = 24;
            this.btnPerAddrEdit.Text = "Edit";
            this.btnPerAddrEdit.UseVisualStyleBackColor = true;
            this.btnPerAddrEdit.Click += new System.EventHandler(this.btnPerAddrEdit_Click);
            // 
            // btnPerAddrDel
            // 
            this.btnPerAddrDel.Location = new System.Drawing.Point(192, 160);
            this.btnPerAddrDel.Name = "btnPerAddrDel";
            this.btnPerAddrDel.Size = new System.Drawing.Size(51, 23);
            this.btnPerAddrDel.TabIndex = 23;
            this.btnPerAddrDel.Text = "Delete";
            this.btnPerAddrDel.UseVisualStyleBackColor = true;
            this.btnPerAddrDel.Click += new System.EventHandler(this.btnPerAddrDel_Click);
            // 
            // btnPerAddrNew
            // 
            this.btnPerAddrNew.Location = new System.Drawing.Point(297, 160);
            this.btnPerAddrNew.Name = "btnPerAddrNew";
            this.btnPerAddrNew.Size = new System.Drawing.Size(44, 23);
            this.btnPerAddrNew.TabIndex = 22;
            this.btnPerAddrNew.Text = "New";
            this.btnPerAddrNew.UseVisualStyleBackColor = true;
            this.btnPerAddrNew.Click += new System.EventHandler(this.btnPerAddrNew_Click);
            // 
            // btnOrgAddrEdit
            // 
            this.btnOrgAddrEdit.Location = new System.Drawing.Point(249, 298);
            this.btnOrgAddrEdit.Name = "btnOrgAddrEdit";
            this.btnOrgAddrEdit.Size = new System.Drawing.Size(44, 23);
            this.btnOrgAddrEdit.TabIndex = 29;
            this.btnOrgAddrEdit.Text = "Edit";
            this.btnOrgAddrEdit.UseVisualStyleBackColor = true;
            this.btnOrgAddrEdit.Click += new System.EventHandler(this.btnOrgAddrEdit_Click);
            // 
            // btnOrgAddrDel
            // 
            this.btnOrgAddrDel.Location = new System.Drawing.Point(192, 298);
            this.btnOrgAddrDel.Name = "btnOrgAddrDel";
            this.btnOrgAddrDel.Size = new System.Drawing.Size(51, 23);
            this.btnOrgAddrDel.TabIndex = 28;
            this.btnOrgAddrDel.Text = "Delete";
            this.btnOrgAddrDel.UseVisualStyleBackColor = true;
            this.btnOrgAddrDel.Click += new System.EventHandler(this.btnOrgAddrDel_Click);
            // 
            // btnOrgAddrNew
            // 
            this.btnOrgAddrNew.Location = new System.Drawing.Point(299, 298);
            this.btnOrgAddrNew.Name = "btnOrgAddrNew";
            this.btnOrgAddrNew.Size = new System.Drawing.Size(44, 23);
            this.btnOrgAddrNew.TabIndex = 27;
            this.btnOrgAddrNew.Text = "New";
            this.btnOrgAddrNew.UseVisualStyleBackColor = true;
            this.btnOrgAddrNew.Click += new System.EventHandler(this.btnOrgAddrNew_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(12, 531);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(53, 23);
            this.btnLoad.TabIndex = 30;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(13, 566);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(52, 23);
            this.btnSave.TabIndex = 31;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xml";
            this.saveFileDialog1.Filter = "XML Files|*.xml";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(440, 531);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(176, 23);
            this.button4.TabIndex = 33;
            this.button4.Text = "Send this batch to web service";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(849, 546);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(117, 23);
            this.button5.TabIndex = 34;
            this.button5.Text = "Process Per Record";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnPersonOrgVehNew);
            this.groupBox1.Controls.Add(this.listBox3);
            this.groupBox1.Controls.Add(this.lbPersonOrgAddrVeh);
            this.groupBox1.Controls.Add(this.btnPersonOrgVehDelete);
            this.groupBox1.Controls.Add(this.listBox4);
            this.groupBox1.Controls.Add(this.btnPersonOrgVehEdit);
            this.groupBox1.Controls.Add(this.listBox5);
            this.groupBox1.Controls.Add(this.listBox6);
            this.groupBox1.Controls.Add(this.btnOrgAddrEdit);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnOrgAddrDel);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnOrgAddrNew);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnPerAddrEdit);
            this.groupBox1.Controls.Add(this.btnPeopleNew);
            this.groupBox1.Controls.Add(this.btnPerAddrDel);
            this.groupBox1.Controls.Add(this.btnPeopleDel);
            this.groupBox1.Controls.Add(this.btnPerAddrNew);
            this.groupBox1.Controls.Add(this.btnPeopleEdit);
            this.groupBox1.Controls.Add(this.btnOrgEdit);
            this.groupBox1.Controls.Add(this.btnOrgNew);
            this.groupBox1.Controls.Add(this.btnOrgDel);
            this.groupBox1.Location = new System.Drawing.Point(163, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(407, 451);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "People";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 328);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 48;
            this.label7.Text = "3rd Party Vehicles";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Person";
            // 
            // btnPersonOrgVehNew
            // 
            this.btnPersonOrgVehNew.Location = new System.Drawing.Point(118, 416);
            this.btnPersonOrgVehNew.Name = "btnPersonOrgVehNew";
            this.btnPersonOrgVehNew.Size = new System.Drawing.Size(44, 23);
            this.btnPersonOrgVehNew.TabIndex = 44;
            this.btnPersonOrgVehNew.Text = "New";
            this.btnPersonOrgVehNew.UseVisualStyleBackColor = true;
            this.btnPersonOrgVehNew.Click += new System.EventHandler(this.btnPersonOrgVehNew_Click);
            // 
            // lbPersonOrgAddrVeh
            // 
            this.lbPersonOrgAddrVeh.DataSource = this.orgVehBindingSource2;
            this.lbPersonOrgAddrVeh.DisplayMember = "DisplayText";
            this.lbPersonOrgAddrVeh.FormattingEnabled = true;
            this.lbPersonOrgAddrVeh.Location = new System.Drawing.Point(11, 344);
            this.lbPersonOrgAddrVeh.Name = "lbPersonOrgAddrVeh";
            this.lbPersonOrgAddrVeh.Size = new System.Drawing.Size(175, 69);
            this.lbPersonOrgAddrVeh.TabIndex = 45;
            this.lbPersonOrgAddrVeh.DoubleClick += new System.EventHandler(this.btnPersonOrgVehEdit_Click);
            // 
            // orgVehBindingSource2
            // 
            this.orgVehBindingSource2.DataMember = "Vehicles";
            this.orgVehBindingSource2.DataSource = this.organisationsBindingSource;
            // 
            // btnPersonOrgVehDelete
            // 
            this.btnPersonOrgVehDelete.Location = new System.Drawing.Point(11, 416);
            this.btnPersonOrgVehDelete.Name = "btnPersonOrgVehDelete";
            this.btnPersonOrgVehDelete.Size = new System.Drawing.Size(51, 23);
            this.btnPersonOrgVehDelete.TabIndex = 46;
            this.btnPersonOrgVehDelete.Text = "Delete";
            this.btnPersonOrgVehDelete.UseVisualStyleBackColor = true;
            this.btnPersonOrgVehDelete.Click += new System.EventHandler(this.btnPersonOrgVehDelete_Click);
            // 
            // btnPersonOrgVehEdit
            // 
            this.btnPersonOrgVehEdit.Location = new System.Drawing.Point(68, 416);
            this.btnPersonOrgVehEdit.Name = "btnPersonOrgVehEdit";
            this.btnPersonOrgVehEdit.Size = new System.Drawing.Size(44, 23);
            this.btnPersonOrgVehEdit.TabIndex = 47;
            this.btnPersonOrgVehEdit.Text = "Edit";
            this.btnPersonOrgVehEdit.UseVisualStyleBackColor = true;
            this.btnPersonOrgVehEdit.Click += new System.EventHandler(this.btnPersonOrgVehEdit_Click);
            // 
            // lbClaimOrg
            // 
            this.lbClaimOrg.DataSource = this.orgsClaimBindingSource;
            this.lbClaimOrg.DisplayMember = "DisplayText";
            this.lbClaimOrg.FormattingEnabled = true;
            this.lbClaimOrg.Location = new System.Drawing.Point(6, 16);
            this.lbClaimOrg.Name = "lbClaimOrg";
            this.lbClaimOrg.Size = new System.Drawing.Size(191, 121);
            this.lbClaimOrg.TabIndex = 37;
            this.lbClaimOrg.DoubleClick += new System.EventHandler(this.btnClaimOrgEdit_Click);
            // 
            // orgsClaimBindingSource
            // 
            this.orgsClaimBindingSource.DataMember = "Organisations";
            this.orgsClaimBindingSource.DataSource = this.claimsBindingSource;
            // 
            // lbClaimOrgAddr
            // 
            this.lbClaimOrgAddr.DataSource = this.addressesOrgBindingSource1;
            this.lbClaimOrgAddr.DisplayMember = "DisplayText";
            this.lbClaimOrgAddr.FormattingEnabled = true;
            this.lbClaimOrgAddr.Location = new System.Drawing.Point(6, 207);
            this.lbClaimOrgAddr.Name = "lbClaimOrgAddr";
            this.lbClaimOrgAddr.Size = new System.Drawing.Size(188, 82);
            this.lbClaimOrgAddr.TabIndex = 39;
            this.lbClaimOrgAddr.DoubleClick += new System.EventHandler(this.btnClaimOrgAddrEdit_Click);
            // 
            // addressesOrgBindingSource1
            // 
            this.addressesOrgBindingSource1.DataMember = "Addresses";
            this.addressesOrgBindingSource1.DataSource = this.orgsClaimBindingSource;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 191);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "Addresses";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listBox2);
            this.groupBox2.Controls.Add(this.btnVehicleNew);
            this.groupBox2.Controls.Add(this.btnVehicleDel);
            this.groupBox2.Controls.Add(this.btnVehicleEdit);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Location = new System.Drawing.Point(214, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(577, 470);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vehicles";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox2);
            this.groupBox3.Location = new System.Drawing.Point(179, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(798, 499);
            this.groupBox3.TabIndex = 41;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Claim";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.btnClaimOrgVehNew);
            this.groupBox5.Controls.Add(this.lbClaimOrgAddrVeh);
            this.groupBox5.Controls.Add(this.btnClaimOrgVehDelete);
            this.groupBox5.Controls.Add(this.btnClaimOrgNew);
            this.groupBox5.Controls.Add(this.btnClaimOrgVehEdit);
            this.groupBox5.Controls.Add(this.btnClaimOrgDel);
            this.groupBox5.Controls.Add(this.btnClaimOrgEdit);
            this.groupBox5.Controls.Add(this.btnClaimOrgAddrNew);
            this.groupBox5.Controls.Add(this.lbClaimOrg);
            this.groupBox5.Controls.Add(this.btnClaimOrgAddrDel);
            this.groupBox5.Controls.Add(this.btnClaimOrgAddrEdit);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.lbClaimOrgAddr);
            this.groupBox5.Location = new System.Drawing.Point(6, 20);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(204, 469);
            this.groupBox5.TabIndex = 41;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Organisations";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 335);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "3rd Party Vehicles";
            // 
            // btnClaimOrgVehNew
            // 
            this.btnClaimOrgVehNew.Location = new System.Drawing.Point(118, 439);
            this.btnClaimOrgVehNew.Name = "btnClaimOrgVehNew";
            this.btnClaimOrgVehNew.Size = new System.Drawing.Size(44, 23);
            this.btnClaimOrgVehNew.TabIndex = 36;
            this.btnClaimOrgVehNew.Text = "New";
            this.btnClaimOrgVehNew.UseVisualStyleBackColor = true;
            this.btnClaimOrgVehNew.Click += new System.EventHandler(this.btnClaimOrgVehNew_Click);
            // 
            // lbClaimOrgAddrVeh
            // 
            this.lbClaimOrgAddrVeh.DataSource = this.orgVehBindingSource1;
            this.lbClaimOrgAddrVeh.DisplayMember = "DisplayText";
            this.lbClaimOrgAddrVeh.FormattingEnabled = true;
            this.lbClaimOrgAddrVeh.Location = new System.Drawing.Point(9, 351);
            this.lbClaimOrgAddrVeh.Name = "lbClaimOrgAddrVeh";
            this.lbClaimOrgAddrVeh.Size = new System.Drawing.Size(188, 82);
            this.lbClaimOrgAddrVeh.TabIndex = 36;
            this.lbClaimOrgAddrVeh.DoubleClick += new System.EventHandler(this.btnClaimOrgVehicleEdit_Click);
            // 
            // orgVehBindingSource1
            // 
            this.orgVehBindingSource1.DataMember = "Vehicles";
            this.orgVehBindingSource1.DataSource = this.orgsClaimBindingSource;
            // 
            // btnClaimOrgVehDelete
            // 
            this.btnClaimOrgVehDelete.Location = new System.Drawing.Point(11, 439);
            this.btnClaimOrgVehDelete.Name = "btnClaimOrgVehDelete";
            this.btnClaimOrgVehDelete.Size = new System.Drawing.Size(51, 23);
            this.btnClaimOrgVehDelete.TabIndex = 37;
            this.btnClaimOrgVehDelete.Text = "Delete";
            this.btnClaimOrgVehDelete.UseVisualStyleBackColor = true;
            this.btnClaimOrgVehDelete.Click += new System.EventHandler(this.btnClaimOrgVehicleDel_Click);
            // 
            // btnClaimOrgNew
            // 
            this.btnClaimOrgNew.Location = new System.Drawing.Point(119, 143);
            this.btnClaimOrgNew.Name = "btnClaimOrgNew";
            this.btnClaimOrgNew.Size = new System.Drawing.Size(44, 23);
            this.btnClaimOrgNew.TabIndex = 40;
            this.btnClaimOrgNew.Text = "New";
            this.btnClaimOrgNew.UseVisualStyleBackColor = true;
            this.btnClaimOrgNew.Click += new System.EventHandler(this.btnClaimOrgNew_Click);
            // 
            // btnClaimOrgVehEdit
            // 
            this.btnClaimOrgVehEdit.Location = new System.Drawing.Point(68, 439);
            this.btnClaimOrgVehEdit.Name = "btnClaimOrgVehEdit";
            this.btnClaimOrgVehEdit.Size = new System.Drawing.Size(44, 23);
            this.btnClaimOrgVehEdit.TabIndex = 38;
            this.btnClaimOrgVehEdit.Text = "Edit";
            this.btnClaimOrgVehEdit.UseVisualStyleBackColor = true;
            this.btnClaimOrgVehEdit.Click += new System.EventHandler(this.btnClaimOrgVehicleEdit_Click);
            // 
            // btnClaimOrgDel
            // 
            this.btnClaimOrgDel.Location = new System.Drawing.Point(9, 143);
            this.btnClaimOrgDel.Name = "btnClaimOrgDel";
            this.btnClaimOrgDel.Size = new System.Drawing.Size(51, 23);
            this.btnClaimOrgDel.TabIndex = 41;
            this.btnClaimOrgDel.Text = "Delete";
            this.btnClaimOrgDel.UseVisualStyleBackColor = true;
            this.btnClaimOrgDel.Click += new System.EventHandler(this.btnClaimOrgDel_Click);
            // 
            // btnClaimOrgEdit
            // 
            this.btnClaimOrgEdit.Location = new System.Drawing.Point(68, 143);
            this.btnClaimOrgEdit.Name = "btnClaimOrgEdit";
            this.btnClaimOrgEdit.Size = new System.Drawing.Size(44, 23);
            this.btnClaimOrgEdit.TabIndex = 42;
            this.btnClaimOrgEdit.Text = "Edit";
            this.btnClaimOrgEdit.UseVisualStyleBackColor = true;
            this.btnClaimOrgEdit.Click += new System.EventHandler(this.btnClaimOrgEdit_Click);
            // 
            // btnClaimOrgAddrNew
            // 
            this.btnClaimOrgAddrNew.Location = new System.Drawing.Point(119, 295);
            this.btnClaimOrgAddrNew.Name = "btnClaimOrgAddrNew";
            this.btnClaimOrgAddrNew.Size = new System.Drawing.Size(44, 23);
            this.btnClaimOrgAddrNew.TabIndex = 36;
            this.btnClaimOrgAddrNew.Text = "New";
            this.btnClaimOrgAddrNew.UseVisualStyleBackColor = true;
            this.btnClaimOrgAddrNew.Click += new System.EventHandler(this.btnClaimOrgAddrNew_Click);
            // 
            // btnClaimOrgAddrDel
            // 
            this.btnClaimOrgAddrDel.Location = new System.Drawing.Point(9, 295);
            this.btnClaimOrgAddrDel.Name = "btnClaimOrgAddrDel";
            this.btnClaimOrgAddrDel.Size = new System.Drawing.Size(51, 23);
            this.btnClaimOrgAddrDel.TabIndex = 37;
            this.btnClaimOrgAddrDel.Text = "Delete";
            this.btnClaimOrgAddrDel.UseVisualStyleBackColor = true;
            this.btnClaimOrgAddrDel.Click += new System.EventHandler(this.btnClaimOrgAddrDel_Click);
            // 
            // btnClaimOrgAddrEdit
            // 
            this.btnClaimOrgAddrEdit.Location = new System.Drawing.Point(68, 295);
            this.btnClaimOrgAddrEdit.Name = "btnClaimOrgAddrEdit";
            this.btnClaimOrgAddrEdit.Size = new System.Drawing.Size(44, 23);
            this.btnClaimOrgAddrEdit.TabIndex = 38;
            this.btnClaimOrgAddrEdit.Text = "Edit";
            this.btnClaimOrgAddrEdit.UseVisualStyleBackColor = true;
            this.btnClaimOrgAddrEdit.Click += new System.EventHandler(this.btnClaimOrgAddrEdit_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.listBox1);
            this.groupBox4.Controls.Add(this.btnClaimNew);
            this.groupBox4.Controls.Add(this.btnClaimEdit);
            this.groupBox4.Controls.Add(this.btnClaimDel);
            this.groupBox4.Location = new System.Drawing.Point(4, 13);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(171, 498);
            this.groupBox4.TabIndex = 42;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Batch";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(170, 566);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(237, 23);
            this.button2.TabIndex = 43;
            this.button2.Text = "Upload File, Process and Score";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(622, 531);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 44;
            this.button3.Text = "Get DOCX";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(713, 531);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(88, 23);
            this.button6.TabIndex = 45;
            this.button6.Text = "View Batches";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(811, 531);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(88, 23);
            this.button7.TabIndex = 46;
            this.button7.Text = "Batch Report";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(117, 522);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 47;
            this.label8.Text = "Client ID";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(119, 538);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown1.TabIndex = 48;
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(170, 534);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(75, 23);
            this.btnUpload.TabIndex = 49;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Visible = false;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(251, 534);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 50;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Visible = false;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnScore
            // 
            this.btnScore.Location = new System.Drawing.Point(332, 534);
            this.btnScore.Name = "btnScore";
            this.btnScore.Size = new System.Drawing.Size(75, 23);
            this.btnScore.TabIndex = 51;
            this.btnScore.Text = "Score";
            this.btnScore.UseVisualStyleBackColor = true;
            this.btnScore.Visible = false;
            this.btnScore.Click += new System.EventHandler(this.btnScore_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(83, 566);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(67, 23);
            this.button8.TabIndex = 52;
            this.button8.Text = "Convert";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Visible = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 601);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.btnScore);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.button1);
            this.Name = "MainForm";
            this.Text = "Keoghs Claim Editor";
            ((System.ComponentModel.ISupportInitialize)(this.claimsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimBatchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehiclesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peopleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.organisationsBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orgVehBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orgsClaimBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesOrgBindingSource1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orgVehBindingSource1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource claimBatchBindingSource;
        private System.Windows.Forms.BindingSource claimsBindingSource;
        private System.Windows.Forms.BindingSource vehiclesBindingSource;
        private System.Windows.Forms.BindingSource peopleBindingSource;
        private System.Windows.Forms.BindingSource addressesBindingSource;
        private System.Windows.Forms.BindingSource addressesBindingSource1;
        private System.Windows.Forms.BindingSource organisationsBindingSource;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.ListBox listBox4;
        private System.Windows.Forms.ListBox listBox6;
        private System.Windows.Forms.ListBox listBox5;
        private System.Windows.Forms.Button btnClaimNew;
        private System.Windows.Forms.Button btnClaimDel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnClaimEdit;
        private System.Windows.Forms.Button btnVehicleEdit;
        private System.Windows.Forms.Button btnVehicleDel;
        private System.Windows.Forms.Button btnVehicleNew;
        private System.Windows.Forms.Button btnPeopleEdit;
        private System.Windows.Forms.Button btnPeopleDel;
        private System.Windows.Forms.Button btnPeopleNew;
        private System.Windows.Forms.Button btnOrgEdit;
        private System.Windows.Forms.Button btnOrgDel;
        private System.Windows.Forms.Button btnOrgNew;
        private System.Windows.Forms.Button btnPerAddrEdit;
        private System.Windows.Forms.Button btnPerAddrDel;
        private System.Windows.Forms.Button btnPerAddrNew;
        private System.Windows.Forms.Button btnOrgAddrEdit;
        private System.Windows.Forms.Button btnOrgAddrDel;
        private System.Windows.Forms.Button btnOrgAddrNew;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lbClaimOrg;
        private System.Windows.Forms.BindingSource orgsClaimBindingSource;
        private System.Windows.Forms.ListBox lbClaimOrgAddr;
        private System.Windows.Forms.BindingSource addressesOrgBindingSource1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnClaimOrgNew;
        private System.Windows.Forms.Button btnClaimOrgDel;
        private System.Windows.Forms.Button btnClaimOrgEdit;
        private System.Windows.Forms.Button btnClaimOrgAddrNew;
        private System.Windows.Forms.Button btnClaimOrgAddrDel;
        private System.Windows.Forms.Button btnClaimOrgAddrEdit;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClaimOrgVehNew;
        private System.Windows.Forms.ListBox lbClaimOrgAddrVeh;
        private System.Windows.Forms.Button btnClaimOrgVehDelete;
        private System.Windows.Forms.Button btnClaimOrgVehEdit;
        private System.Windows.Forms.BindingSource orgVehBindingSource1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnPersonOrgVehNew;
        private System.Windows.Forms.ListBox lbPersonOrgAddrVeh;
        private System.Windows.Forms.Button btnPersonOrgVehDelete;
        private System.Windows.Forms.Button btnPersonOrgVehEdit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource orgVehBindingSource2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Button btnScore;
        private System.Windows.Forms.Button button8;
    }
}

