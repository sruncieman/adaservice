﻿namespace MDA.FileEditor
{
    partial class OrgEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label organisationNameLabel;
            System.Windows.Forms.Label organisationPersonLinkTypeLabel;
            System.Windows.Forms.Label organisationTypeLabel;
            System.Windows.Forms.Label registeredNumberLabel;
            System.Windows.Forms.Label vatNumberLabel;
            System.Windows.Forms.Label sortCodeLabel1;
            System.Windows.Forms.Label paymentCardTypeLabel;
            System.Windows.Forms.Label paymentCardNumberLabel;
            System.Windows.Forms.Label datePaymentDetailsTakenLabel1;
            System.Windows.Forms.Label bankLabel;
            System.Windows.Forms.Label sortCodeLabel;
            System.Windows.Forms.Label policyPaymentTypeLabel;
            System.Windows.Forms.Label datePaymentDetailsTakenLabel;
            System.Windows.Forms.Label bankNameLabel;
            System.Windows.Forms.Label accountNumberLabel;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label11;
            this.organisationNameTextBox = new System.Windows.Forms.TextBox();
            this.organisationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.organisationPersonLinkTypeComboBox = new System.Windows.Forms.ComboBox();
            this.organisationTypeComboBox = new System.Windows.Forms.ComboBox();
            this.registeredNumberTextBox = new System.Windows.Forms.TextBox();
            this.vatNumberTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.grpCard = new System.Windows.Forms.GroupBox();
            this.chkCard = new System.Windows.Forms.CheckBox();
            this.pnlCard = new System.Windows.Forms.Panel();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.paymentCardTypeComboBox = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.datePaymentDetailsTakenDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.bankTextBox = new System.Windows.Forms.TextBox();
            this.sortCodeTextBox1 = new System.Windows.Forms.TextBox();
            this.paymentCardNumberTextBox = new System.Windows.Forms.TextBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.grpBank = new System.Windows.Forms.GroupBox();
            this.chkBank = new System.Windows.Forms.CheckBox();
            this.pnlBank = new System.Windows.Forms.Panel();
            this.bankNameTextBox = new System.Windows.Forms.TextBox();
            this.sortCodeTextBox = new System.Windows.Forms.TextBox();
            this.policyPaymentTypeComboBox = new System.Windows.Forms.ComboBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.accountNumberTextBox = new System.Windows.Forms.TextBox();
            this.datePaymentDetailsTakenDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            organisationNameLabel = new System.Windows.Forms.Label();
            organisationPersonLinkTypeLabel = new System.Windows.Forms.Label();
            organisationTypeLabel = new System.Windows.Forms.Label();
            registeredNumberLabel = new System.Windows.Forms.Label();
            vatNumberLabel = new System.Windows.Forms.Label();
            sortCodeLabel1 = new System.Windows.Forms.Label();
            paymentCardTypeLabel = new System.Windows.Forms.Label();
            paymentCardNumberLabel = new System.Windows.Forms.Label();
            datePaymentDetailsTakenLabel1 = new System.Windows.Forms.Label();
            bankLabel = new System.Windows.Forms.Label();
            sortCodeLabel = new System.Windows.Forms.Label();
            policyPaymentTypeLabel = new System.Windows.Forms.Label();
            datePaymentDetailsTakenLabel = new System.Windows.Forms.Label();
            bankNameLabel = new System.Windows.Forms.Label();
            accountNumberLabel = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.organisationsBindingSource)).BeginInit();
            this.grpCard.SuspendLayout();
            this.pnlCard.SuspendLayout();
            this.grpBank.SuspendLayout();
            this.pnlBank.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // organisationNameLabel
            // 
            organisationNameLabel.AutoSize = true;
            organisationNameLabel.Location = new System.Drawing.Point(17, 24);
            organisationNameLabel.Name = "organisationNameLabel";
            organisationNameLabel.Size = new System.Drawing.Size(100, 13);
            organisationNameLabel.TabIndex = 0;
            organisationNameLabel.Text = "Organisation Name:";
            // 
            // organisationPersonLinkTypeLabel
            // 
            organisationPersonLinkTypeLabel.AutoSize = true;
            organisationPersonLinkTypeLabel.Location = new System.Drawing.Point(17, 50);
            organisationPersonLinkTypeLabel.Name = "organisationPersonLinkTypeLabel";
            organisationPersonLinkTypeLabel.Size = new System.Drawing.Size(116, 13);
            organisationPersonLinkTypeLabel.TabIndex = 2;
            organisationPersonLinkTypeLabel.Text = "Relationship to Person:";
            // 
            // organisationTypeLabel
            // 
            organisationTypeLabel.AutoSize = true;
            organisationTypeLabel.Location = new System.Drawing.Point(17, 77);
            organisationTypeLabel.Name = "organisationTypeLabel";
            organisationTypeLabel.Size = new System.Drawing.Size(96, 13);
            organisationTypeLabel.TabIndex = 4;
            organisationTypeLabel.Text = "Organisation Type:";
            // 
            // registeredNumberLabel
            // 
            registeredNumberLabel.AutoSize = true;
            registeredNumberLabel.Location = new System.Drawing.Point(17, 104);
            registeredNumberLabel.Name = "registeredNumberLabel";
            registeredNumberLabel.Size = new System.Drawing.Size(101, 13);
            registeredNumberLabel.TabIndex = 6;
            registeredNumberLabel.Text = "Registered Number:";
            // 
            // vatNumberLabel
            // 
            vatNumberLabel.AutoSize = true;
            vatNumberLabel.Location = new System.Drawing.Point(17, 130);
            vatNumberLabel.Name = "vatNumberLabel";
            vatNumberLabel.Size = new System.Drawing.Size(66, 13);
            vatNumberLabel.TabIndex = 8;
            vatNumberLabel.Text = "Vat Number:";
            // 
            // sortCodeLabel1
            // 
            sortCodeLabel1.AutoSize = true;
            sortCodeLabel1.Location = new System.Drawing.Point(3, 120);
            sortCodeLabel1.Name = "sortCodeLabel1";
            sortCodeLabel1.Size = new System.Drawing.Size(108, 13);
            sortCodeLabel1.TabIndex = 48;
            sortCodeLabel1.Text = "Sort Code: (nn-nn-nn)";
            // 
            // paymentCardTypeLabel
            // 
            paymentCardTypeLabel.AutoSize = true;
            paymentCardTypeLabel.Location = new System.Drawing.Point(3, 37);
            paymentCardTypeLabel.Name = "paymentCardTypeLabel";
            paymentCardTypeLabel.Size = new System.Drawing.Size(103, 13);
            paymentCardTypeLabel.TabIndex = 44;
            paymentCardTypeLabel.Text = "Payment Card Type:";
            // 
            // paymentCardNumberLabel
            // 
            paymentCardNumberLabel.AutoSize = true;
            paymentCardNumberLabel.Location = new System.Drawing.Point(3, 63);
            paymentCardNumberLabel.Name = "paymentCardNumberLabel";
            paymentCardNumberLabel.Size = new System.Drawing.Size(116, 13);
            paymentCardNumberLabel.TabIndex = 46;
            paymentCardNumberLabel.Text = "Payment Card Number:";
            // 
            // datePaymentDetailsTakenLabel1
            // 
            datePaymentDetailsTakenLabel1.AutoSize = true;
            datePaymentDetailsTakenLabel1.Location = new System.Drawing.Point(3, 146);
            datePaymentDetailsTakenLabel1.Name = "datePaymentDetailsTakenLabel1";
            datePaymentDetailsTakenLabel1.Size = new System.Drawing.Size(146, 13);
            datePaymentDetailsTakenLabel1.TabIndex = 50;
            datePaymentDetailsTakenLabel1.Text = "Date Payment Details Taken:";
            // 
            // bankLabel
            // 
            bankLabel.AutoSize = true;
            bankLabel.Location = new System.Drawing.Point(3, 11);
            bankLabel.Name = "bankLabel";
            bankLabel.Size = new System.Drawing.Size(35, 13);
            bankLabel.TabIndex = 42;
            bankLabel.Text = "Bank:";
            // 
            // sortCodeLabel
            // 
            sortCodeLabel.AutoSize = true;
            sortCodeLabel.Location = new System.Drawing.Point(3, 52);
            sortCodeLabel.Name = "sortCodeLabel";
            sortCodeLabel.Size = new System.Drawing.Size(57, 13);
            sortCodeLabel.TabIndex = 35;
            sortCodeLabel.Text = "Sort Code:";
            // 
            // policyPaymentTypeLabel
            // 
            policyPaymentTypeLabel.AutoSize = true;
            policyPaymentTypeLabel.Location = new System.Drawing.Point(3, 83);
            policyPaymentTypeLabel.Name = "policyPaymentTypeLabel";
            policyPaymentTypeLabel.Size = new System.Drawing.Size(78, 13);
            policyPaymentTypeLabel.TabIndex = 37;
            policyPaymentTypeLabel.Text = "Payment Type:";
            // 
            // datePaymentDetailsTakenLabel
            // 
            datePaymentDetailsTakenLabel.AutoSize = true;
            datePaymentDetailsTakenLabel.Location = new System.Drawing.Point(3, 110);
            datePaymentDetailsTakenLabel.Name = "datePaymentDetailsTakenLabel";
            datePaymentDetailsTakenLabel.Size = new System.Drawing.Size(146, 13);
            datePaymentDetailsTakenLabel.TabIndex = 39;
            datePaymentDetailsTakenLabel.Text = "Date Payment Details Taken:";
            // 
            // bankNameLabel
            // 
            bankNameLabel.AutoSize = true;
            bankNameLabel.Location = new System.Drawing.Point(3, 5);
            bankNameLabel.Name = "bankNameLabel";
            bankNameLabel.Size = new System.Drawing.Size(66, 13);
            bankNameLabel.TabIndex = 31;
            bankNameLabel.Text = "Bank Name:";
            // 
            // accountNumberLabel
            // 
            accountNumberLabel.AutoSize = true;
            accountNumberLabel.Location = new System.Drawing.Point(3, 28);
            accountNumberLabel.Name = "accountNumberLabel";
            accountNumberLabel.Size = new System.Drawing.Size(90, 13);
            accountNumberLabel.TabIndex = 33;
            accountNumberLabel.Text = "Account Number:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(11, 69);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(73, 13);
            label4.TabIndex = 62;
            label4.Text = "Telephone 3 :";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(11, 46);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(73, 13);
            label3.TabIndex = 61;
            label3.Text = "Telephone 2 :";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(11, 20);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(73, 13);
            label2.TabIndex = 60;
            label2.Text = "Telephone 1 :";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(17, 153);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(91, 13);
            label1.TabIndex = 63;
            label1.Text = "Moj Crm Number :";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(17, 208);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(38, 13);
            label6.TabIndex = 65;
            label6.Text = "Email :";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(17, 230);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(52, 13);
            label7.TabIndex = 68;
            label7.Text = "Website :";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(17, 182);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(81, 13);
            label5.TabIndex = 34;
            label5.Text = "Moj Crm Status:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(3, 94);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(78, 13);
            label9.TabIndex = 53;
            label9.Text = "Payment Type:";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(217, 123);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(98, 13);
            label10.TabIndex = 55;
            label10.Text = "Exp. Date: (MMYY)";
            // 
            // organisationNameTextBox
            // 
            this.organisationNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "OrganisationName", true));
            this.organisationNameTextBox.Location = new System.Drawing.Point(178, 21);
            this.organisationNameTextBox.Name = "organisationNameTextBox";
            this.organisationNameTextBox.Size = new System.Drawing.Size(121, 20);
            this.organisationNameTextBox.TabIndex = 1;
            // 
            // organisationsBindingSource
            // 
            this.organisationsBindingSource.DataSource = typeof(MDA.Common.FileModel.Organisation);
            this.organisationsBindingSource.CurrentChanged += new System.EventHandler(this.organisationsBindingSource_CurrentChanged);
            // 
            // organisationPersonLinkTypeComboBox
            // 
            this.organisationPersonLinkTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "Person2OrganisationLinkType", true));
            this.organisationPersonLinkTypeComboBox.FormattingEnabled = true;
            this.organisationPersonLinkTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Director",
            "Secretary",
            "Employee",
            "PreviousDirector",
            "PreviousSecretary",
            "PreviousEmployee",
            "Manager",
            "Owner",
            "Partner",
            "Broker",
            "ReferralSource",
            "Solicitor",
            "Engineer",
            "Recovery",
            "Storage",
            "Repairer",
            "Hire",
            "AccidentManagement",
            "MedicalExaminer"});
            this.organisationPersonLinkTypeComboBox.Location = new System.Drawing.Point(178, 47);
            this.organisationPersonLinkTypeComboBox.Name = "organisationPersonLinkTypeComboBox";
            this.organisationPersonLinkTypeComboBox.Size = new System.Drawing.Size(121, 21);
            this.organisationPersonLinkTypeComboBox.TabIndex = 3;
            // 
            // organisationTypeComboBox
            // 
            this.organisationTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "OrganisationType", true));
            this.organisationTypeComboBox.FormattingEnabled = true;
            this.organisationTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "PolicyHolder",
            "UndefinedSupplier",
            "AccidentManagement",
            "CreditHire",
            "Solicitor",
            "VehicleEngineer",
            "Recovery",
            "Storage",
            "Broker",
            "MedicalExaminer",
            "Repairer",
            "Insurer"});
            this.organisationTypeComboBox.Location = new System.Drawing.Point(178, 74);
            this.organisationTypeComboBox.Name = "organisationTypeComboBox";
            this.organisationTypeComboBox.Size = new System.Drawing.Size(121, 21);
            this.organisationTypeComboBox.TabIndex = 5;
            // 
            // registeredNumberTextBox
            // 
            this.registeredNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "RegisteredNumber", true));
            this.registeredNumberTextBox.Location = new System.Drawing.Point(178, 101);
            this.registeredNumberTextBox.Name = "registeredNumberTextBox";
            this.registeredNumberTextBox.Size = new System.Drawing.Size(121, 20);
            this.registeredNumberTextBox.TabIndex = 7;
            // 
            // vatNumberTextBox
            // 
            this.vatNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "VatNumber", true));
            this.vatNumberTextBox.Location = new System.Drawing.Point(178, 127);
            this.vatNumberTextBox.Name = "vatNumberTextBox";
            this.vatNumberTextBox.Size = new System.Drawing.Size(121, 20);
            this.vatNumberTextBox.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(540, 393);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(621, 393);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // grpCard
            // 
            this.grpCard.Controls.Add(this.chkCard);
            this.grpCard.Controls.Add(this.pnlCard);
            this.grpCard.Location = new System.Drawing.Point(320, 179);
            this.grpCard.Name = "grpCard";
            this.grpCard.Size = new System.Drawing.Size(391, 208);
            this.grpCard.TabIndex = 58;
            this.grpCard.TabStop = false;
            this.grpCard.Text = "Payment Card";
            // 
            // chkCard
            // 
            this.chkCard.AutoSize = true;
            this.chkCard.Location = new System.Drawing.Point(4, 19);
            this.chkCard.Name = "chkCard";
            this.chkCard.Size = new System.Drawing.Size(15, 14);
            this.chkCard.TabIndex = 70;
            this.chkCard.UseVisualStyleBackColor = true;
            this.chkCard.CheckedChanged += new System.EventHandler(this.chkCard_CheckedChanged);
            // 
            // pnlCard
            // 
            this.pnlCard.Controls.Add(this.textBox7);
            this.pnlCard.Controls.Add(bankLabel);
            this.pnlCard.Controls.Add(label10);
            this.pnlCard.Controls.Add(this.paymentCardTypeComboBox);
            this.pnlCard.Controls.Add(label9);
            this.pnlCard.Controls.Add(datePaymentDetailsTakenLabel1);
            this.pnlCard.Controls.Add(this.comboBox2);
            this.pnlCard.Controls.Add(this.datePaymentDetailsTakenDateTimePicker1);
            this.pnlCard.Controls.Add(this.bankTextBox);
            this.pnlCard.Controls.Add(paymentCardNumberLabel);
            this.pnlCard.Controls.Add(this.sortCodeTextBox1);
            this.pnlCard.Controls.Add(this.paymentCardNumberTextBox);
            this.pnlCard.Controls.Add(sortCodeLabel1);
            this.pnlCard.Controls.Add(paymentCardTypeLabel);
            this.pnlCard.Controls.Add(this.checkBox5);
            this.pnlCard.Enabled = false;
            this.pnlCard.Location = new System.Drawing.Point(21, 19);
            this.pnlCard.Name = "pnlCard";
            this.pnlCard.Size = new System.Drawing.Size(361, 183);
            this.pnlCard.TabIndex = 69;
            // 
            // textBox7
            // 
            this.textBox7.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "PaymentCard.ExpiryDate", true));
            this.textBox7.Location = new System.Drawing.Point(321, 116);
            this.textBox7.MaxLength = 4;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(34, 20);
            this.textBox7.TabIndex = 56;
            // 
            // paymentCardTypeComboBox
            // 
            this.paymentCardTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "PaymentCard.PaymentCardType", true));
            this.paymentCardTypeComboBox.FormattingEnabled = true;
            this.paymentCardTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "CreditCard",
            "DebitCard"});
            this.paymentCardTypeComboBox.Location = new System.Drawing.Point(155, 36);
            this.paymentCardTypeComboBox.Name = "paymentCardTypeComboBox";
            this.paymentCardTypeComboBox.Size = new System.Drawing.Size(200, 21);
            this.paymentCardTypeComboBox.TabIndex = 45;
            // 
            // comboBox2
            // 
            this.comboBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "PaymentCard.PolicyPaymentType", true));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Unknown",
            "Deposit",
            "DirectDebit",
            "SinglePayment"});
            this.comboBox2.Location = new System.Drawing.Point(155, 89);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(200, 21);
            this.comboBox2.TabIndex = 54;
            // 
            // datePaymentDetailsTakenDateTimePicker1
            // 
            this.datePaymentDetailsTakenDateTimePicker1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.organisationsBindingSource, "PaymentCard.DatePaymentDetailsTaken", true));
            this.datePaymentDetailsTakenDateTimePicker1.Enabled = false;
            this.datePaymentDetailsTakenDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePaymentDetailsTakenDateTimePicker1.Location = new System.Drawing.Point(176, 146);
            this.datePaymentDetailsTakenDateTimePicker1.Name = "datePaymentDetailsTakenDateTimePicker1";
            this.datePaymentDetailsTakenDateTimePicker1.Size = new System.Drawing.Size(179, 20);
            this.datePaymentDetailsTakenDateTimePicker1.TabIndex = 52;
            // 
            // bankTextBox
            // 
            this.bankTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "PaymentCard.BankName", true));
            this.bankTextBox.Location = new System.Drawing.Point(155, 11);
            this.bankTextBox.Name = "bankTextBox";
            this.bankTextBox.Size = new System.Drawing.Size(200, 20);
            this.bankTextBox.TabIndex = 43;
            // 
            // sortCodeTextBox1
            // 
            this.sortCodeTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "PaymentCard.SortCode", true));
            this.sortCodeTextBox1.Location = new System.Drawing.Point(155, 120);
            this.sortCodeTextBox1.MaxLength = 8;
            this.sortCodeTextBox1.Name = "sortCodeTextBox1";
            this.sortCodeTextBox1.Size = new System.Drawing.Size(56, 20);
            this.sortCodeTextBox1.TabIndex = 49;
            // 
            // paymentCardNumberTextBox
            // 
            this.paymentCardNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "PaymentCard.PaymentCardNumber", true));
            this.paymentCardNumberTextBox.Location = new System.Drawing.Point(155, 63);
            this.paymentCardNumberTextBox.Name = "paymentCardNumberTextBox";
            this.paymentCardNumberTextBox.Size = new System.Drawing.Size(200, 20);
            this.paymentCardNumberTextBox.TabIndex = 47;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(159, 148);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 51;
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // grpBank
            // 
            this.grpBank.Controls.Add(this.chkBank);
            this.grpBank.Controls.Add(this.pnlBank);
            this.grpBank.Location = new System.Drawing.Point(320, 9);
            this.grpBank.Name = "grpBank";
            this.grpBank.Size = new System.Drawing.Size(391, 163);
            this.grpBank.TabIndex = 57;
            this.grpBank.TabStop = false;
            this.grpBank.Text = "Bank Account";
            // 
            // chkBank
            // 
            this.chkBank.AutoSize = true;
            this.chkBank.Location = new System.Drawing.Point(4, 18);
            this.chkBank.Name = "chkBank";
            this.chkBank.Size = new System.Drawing.Size(15, 14);
            this.chkBank.TabIndex = 57;
            this.chkBank.UseVisualStyleBackColor = true;
            this.chkBank.CheckedChanged += new System.EventHandler(this.chkBank_CheckedChanged);
            // 
            // pnlBank
            // 
            this.pnlBank.Controls.Add(this.bankNameTextBox);
            this.pnlBank.Controls.Add(bankNameLabel);
            this.pnlBank.Controls.Add(this.sortCodeTextBox);
            this.pnlBank.Controls.Add(this.policyPaymentTypeComboBox);
            this.pnlBank.Controls.Add(sortCodeLabel);
            this.pnlBank.Controls.Add(accountNumberLabel);
            this.pnlBank.Controls.Add(this.checkBox4);
            this.pnlBank.Controls.Add(this.accountNumberTextBox);
            this.pnlBank.Controls.Add(policyPaymentTypeLabel);
            this.pnlBank.Controls.Add(datePaymentDetailsTakenLabel);
            this.pnlBank.Controls.Add(this.datePaymentDetailsTakenDateTimePicker);
            this.pnlBank.Enabled = false;
            this.pnlBank.Location = new System.Drawing.Point(21, 21);
            this.pnlBank.Name = "pnlBank";
            this.pnlBank.Size = new System.Drawing.Size(361, 139);
            this.pnlBank.TabIndex = 69;
            // 
            // bankNameTextBox
            // 
            this.bankNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "BankAccount.BankName", true));
            this.bankNameTextBox.Location = new System.Drawing.Point(155, 2);
            this.bankNameTextBox.Name = "bankNameTextBox";
            this.bankNameTextBox.Size = new System.Drawing.Size(200, 20);
            this.bankNameTextBox.TabIndex = 32;
            // 
            // sortCodeTextBox
            // 
            this.sortCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "BankAccount.SortCode", true));
            this.sortCodeTextBox.Location = new System.Drawing.Point(155, 49);
            this.sortCodeTextBox.Name = "sortCodeTextBox";
            this.sortCodeTextBox.Size = new System.Drawing.Size(200, 20);
            this.sortCodeTextBox.TabIndex = 36;
            // 
            // policyPaymentTypeComboBox
            // 
            this.policyPaymentTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "BankAccount.PolicyPaymentType", true));
            this.policyPaymentTypeComboBox.FormattingEnabled = true;
            this.policyPaymentTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Deposit",
            "DirectDebit",
            "SinglePayment"});
            this.policyPaymentTypeComboBox.Location = new System.Drawing.Point(155, 78);
            this.policyPaymentTypeComboBox.Name = "policyPaymentTypeComboBox";
            this.policyPaymentTypeComboBox.Size = new System.Drawing.Size(200, 21);
            this.policyPaymentTypeComboBox.TabIndex = 38;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(155, 110);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 40;
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // accountNumberTextBox
            // 
            this.accountNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "BankAccount.AccountNumber", true));
            this.accountNumberTextBox.Location = new System.Drawing.Point(155, 25);
            this.accountNumberTextBox.Name = "accountNumberTextBox";
            this.accountNumberTextBox.Size = new System.Drawing.Size(200, 20);
            this.accountNumberTextBox.TabIndex = 34;
            // 
            // datePaymentDetailsTakenDateTimePicker
            // 
            this.datePaymentDetailsTakenDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.organisationsBindingSource, "BankAccount.DatePaymentDetailsTaken", true));
            this.datePaymentDetailsTakenDateTimePicker.Enabled = false;
            this.datePaymentDetailsTakenDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePaymentDetailsTakenDateTimePicker.Location = new System.Drawing.Point(176, 106);
            this.datePaymentDetailsTakenDateTimePicker.Name = "datePaymentDetailsTakenDateTimePicker";
            this.datePaymentDetailsTakenDateTimePicker.Size = new System.Drawing.Size(179, 20);
            this.datePaymentDetailsTakenDateTimePicker.TabIndex = 41;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox4);
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Controls.Add(label4);
            this.groupBox3.Controls.Add(label3);
            this.groupBox3.Controls.Add(label2);
            this.groupBox3.Location = new System.Drawing.Point(6, 264);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(293, 104);
            this.groupBox3.TabIndex = 62;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Phone";
            // 
            // textBox4
            // 
            this.textBox4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "Telephone3", true));
            this.textBox4.Location = new System.Drawing.Point(116, 66);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(156, 20);
            this.textBox4.TabIndex = 64;
            // 
            // textBox3
            // 
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "Telephone2", true));
            this.textBox3.Location = new System.Drawing.Point(116, 42);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(156, 20);
            this.textBox3.TabIndex = 63;
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "Telephone1", true));
            this.textBox2.Location = new System.Drawing.Point(116, 17);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(156, 20);
            this.textBox2.TabIndex = 60;
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "MojCrmNumber", true));
            this.textBox1.Location = new System.Drawing.Point(178, 153);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 20);
            this.textBox1.TabIndex = 64;
            // 
            // textBox5
            // 
            this.textBox5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "Email", true));
            this.textBox5.Location = new System.Drawing.Point(178, 205);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(121, 20);
            this.textBox5.TabIndex = 66;
            // 
            // textBox6
            // 
            this.textBox6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "WebSite", true));
            this.textBox6.Location = new System.Drawing.Point(178, 227);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(121, 20);
            this.textBox6.TabIndex = 67;
            // 
            // comboBox1
            // 
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "MojCrmStatus", true));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Unknown",
            "Authorised",
            "Suspended",
            "Cancelled"});
            this.comboBox1.Location = new System.Drawing.Point(178, 179);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(118, 21);
            this.comboBox1.TabIndex = 35;
            // 
            // comboBox3
            // 
            this.comboBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.organisationsBindingSource, "Operation", true));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Normal",
            "Delete",
            "Withdraw"});
            this.comboBox3.Location = new System.Drawing.Point(122, 383);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(144, 21);
            this.comboBox3.TabIndex = 77;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(13, 386);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(56, 13);
            label11.TabIndex = 76;
            label11.Text = "Operation:";
            // 
            // OrgEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 430);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(label11);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(label5);
            this.Controls.Add(label7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(label6);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(label1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.grpCard);
            this.Controls.Add(this.grpBank);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(organisationNameLabel);
            this.Controls.Add(this.organisationNameTextBox);
            this.Controls.Add(organisationPersonLinkTypeLabel);
            this.Controls.Add(this.organisationPersonLinkTypeComboBox);
            this.Controls.Add(organisationTypeLabel);
            this.Controls.Add(this.organisationTypeComboBox);
            this.Controls.Add(registeredNumberLabel);
            this.Controls.Add(this.registeredNumberTextBox);
            this.Controls.Add(vatNumberLabel);
            this.Controls.Add(this.vatNumberTextBox);
            this.Name = "OrgEdit";
            this.Text = "Edit Organisation Details";
            ((System.ComponentModel.ISupportInitialize)(this.organisationsBindingSource)).EndInit();
            this.grpCard.ResumeLayout(false);
            this.grpCard.PerformLayout();
            this.pnlCard.ResumeLayout(false);
            this.pnlCard.PerformLayout();
            this.grpBank.ResumeLayout(false);
            this.grpBank.PerformLayout();
            this.pnlBank.ResumeLayout(false);
            this.pnlBank.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox organisationNameTextBox;
        private System.Windows.Forms.ComboBox organisationPersonLinkTypeComboBox;
        private System.Windows.Forms.ComboBox organisationTypeComboBox;
        private System.Windows.Forms.TextBox registeredNumberTextBox;
        private System.Windows.Forms.TextBox vatNumberTextBox;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.BindingSource organisationsBindingSource;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox grpCard;
        private System.Windows.Forms.TextBox bankTextBox;
        private System.Windows.Forms.TextBox sortCodeTextBox1;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.TextBox paymentCardNumberTextBox;
        private System.Windows.Forms.DateTimePicker datePaymentDetailsTakenDateTimePicker1;
        private System.Windows.Forms.ComboBox paymentCardTypeComboBox;
        private System.Windows.Forms.GroupBox grpBank;
        private System.Windows.Forms.TextBox bankNameTextBox;
        private System.Windows.Forms.TextBox sortCodeTextBox;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.DateTimePicker datePaymentDetailsTakenDateTimePicker;
        private System.Windows.Forms.TextBox accountNumberTextBox;
        private System.Windows.Forms.ComboBox policyPaymentTypeComboBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.CheckBox chkCard;
        private System.Windows.Forms.Panel pnlCard;
        private System.Windows.Forms.CheckBox chkBank;
        private System.Windows.Forms.Panel pnlBank;
        private System.Windows.Forms.ComboBox comboBox3;
    }
}