﻿namespace MDA.FileEditor
{
    partial class IncidentEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label claimNumberLabel;
            System.Windows.Forms.Label incidentDateLabel;
            System.Windows.Forms.Label brokerLabel;
            System.Windows.Forms.Label tradingNameLabel;
            System.Windows.Forms.Label coverTypeLabel;
            System.Windows.Forms.Label previousNoFaultClaimsCountLabel;
            System.Windows.Forms.Label policyEndDateLabel;
            System.Windows.Forms.Label previousFaultClaimsCountLabel;
            System.Windows.Forms.Label policyNumberLabel;
            System.Windows.Forms.Label premiumLabel;
            System.Windows.Forms.Label policyStartDateLabel;
            System.Windows.Forms.Label policyTypeLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label ambulanceAttendedLabel;
            System.Windows.Forms.Label claimCodeLabel;
            System.Windows.Forms.Label claimNotificationDateLabel;
            System.Windows.Forms.Label claimStatusLabel;
            System.Windows.Forms.Label incidentCircumstancesLabel;
            System.Windows.Forms.Label incidentLocationLabel;
            System.Windows.Forms.Label policeAttendedLabel;
            System.Windows.Forms.Label policeForceLabel;
            System.Windows.Forms.Label policeReferenceLabel;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label4;
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.incidentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.claimTypeComboBox = new System.Windows.Forms.ComboBox();
            this.claimNumberTextBox = new System.Windows.Forms.TextBox();
            this.incidentDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.policyTypeComboBox = new System.Windows.Forms.ComboBox();
            this.coverTypeComboBox = new System.Windows.Forms.ComboBox();
            this.policyEndDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.tradingNameTextBox = new System.Windows.Forms.TextBox();
            this.brokerTextBox = new System.Windows.Forms.TextBox();
            this.previousNoFaultClaimsCountTextBox = new System.Windows.Forms.TextBox();
            this.previousFaultClaimsCountTextBox = new System.Windows.Forms.TextBox();
            this.premiumTextBox = new System.Windows.Forms.TextBox();
            this.policyNumberTextBox = new System.Windows.Forms.TextBox();
            this.policyStartDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.claimStatusComboBox = new System.Windows.Forms.ComboBox();
            this.claimCodeTextBox = new System.Windows.Forms.TextBox();
            this.ambulanceAttendedCheckBox = new System.Windows.Forms.CheckBox();
            this.claimNotificationDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.incidentCircumstancesTextBox = new System.Windows.Forms.TextBox();
            this.incidentLocationTextBox = new System.Windows.Forms.TextBox();
            this.policeAttendedCheckBox = new System.Windows.Forms.CheckBox();
            this.policeForceTextBox = new System.Windows.Forms.TextBox();
            this.policeReferenceTextBox = new System.Windows.Forms.TextBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            claimNumberLabel = new System.Windows.Forms.Label();
            incidentDateLabel = new System.Windows.Forms.Label();
            brokerLabel = new System.Windows.Forms.Label();
            tradingNameLabel = new System.Windows.Forms.Label();
            coverTypeLabel = new System.Windows.Forms.Label();
            previousNoFaultClaimsCountLabel = new System.Windows.Forms.Label();
            policyEndDateLabel = new System.Windows.Forms.Label();
            previousFaultClaimsCountLabel = new System.Windows.Forms.Label();
            policyNumberLabel = new System.Windows.Forms.Label();
            premiumLabel = new System.Windows.Forms.Label();
            policyStartDateLabel = new System.Windows.Forms.Label();
            policyTypeLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            ambulanceAttendedLabel = new System.Windows.Forms.Label();
            claimCodeLabel = new System.Windows.Forms.Label();
            claimNotificationDateLabel = new System.Windows.Forms.Label();
            claimStatusLabel = new System.Windows.Forms.Label();
            incidentCircumstancesLabel = new System.Windows.Forms.Label();
            incidentLocationLabel = new System.Windows.Forms.Label();
            policeAttendedLabel = new System.Windows.Forms.Label();
            policeForceLabel = new System.Windows.Forms.Label();
            policeReferenceLabel = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.incidentsBindingSource)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // claimNumberLabel
            // 
            claimNumberLabel.AutoSize = true;
            claimNumberLabel.Location = new System.Drawing.Point(11, 31);
            claimNumberLabel.Name = "claimNumberLabel";
            claimNumberLabel.Size = new System.Drawing.Size(75, 13);
            claimNumberLabel.TabIndex = 0;
            claimNumberLabel.Text = "Claim Number:";
            // 
            // incidentDateLabel
            // 
            incidentDateLabel.AutoSize = true;
            incidentDateLabel.Location = new System.Drawing.Point(637, 31);
            incidentDateLabel.Name = "incidentDateLabel";
            incidentDateLabel.Size = new System.Drawing.Size(74, 13);
            incidentDateLabel.TabIndex = 7;
            incidentDateLabel.Text = "Incident Date:";
            // 
            // brokerLabel
            // 
            brokerLabel.AutoSize = true;
            brokerLabel.Location = new System.Drawing.Point(24, 36);
            brokerLabel.Name = "brokerLabel";
            brokerLabel.Size = new System.Drawing.Size(41, 13);
            brokerLabel.TabIndex = 0;
            brokerLabel.Text = "Broker:";
            // 
            // tradingNameLabel
            // 
            tradingNameLabel.AutoSize = true;
            tradingNameLabel.Location = new System.Drawing.Point(602, 84);
            tradingNameLabel.Name = "tradingNameLabel";
            tradingNameLabel.Size = new System.Drawing.Size(77, 13);
            tradingNameLabel.TabIndex = 20;
            tradingNameLabel.Text = "Trading Name:";
            // 
            // coverTypeLabel
            // 
            coverTypeLabel.AutoSize = true;
            coverTypeLabel.Location = new System.Drawing.Point(24, 88);
            coverTypeLabel.Name = "coverTypeLabel";
            coverTypeLabel.Size = new System.Drawing.Size(65, 13);
            coverTypeLabel.TabIndex = 4;
            coverTypeLabel.Text = "Cover Type:";
            // 
            // previousNoFaultClaimsCountLabel
            // 
            previousNoFaultClaimsCountLabel.AutoSize = true;
            previousNoFaultClaimsCountLabel.Location = new System.Drawing.Point(602, 58);
            previousNoFaultClaimsCountLabel.Name = "previousNoFaultClaimsCountLabel";
            previousNoFaultClaimsCountLabel.Size = new System.Drawing.Size(158, 13);
            previousNoFaultClaimsCountLabel.TabIndex = 18;
            previousNoFaultClaimsCountLabel.Text = "Previous No Fault Claims Count:";
            // 
            // policyEndDateLabel
            // 
            policyEndDateLabel.AutoSize = true;
            policyEndDateLabel.Location = new System.Drawing.Point(290, 115);
            policyEndDateLabel.Name = "policyEndDateLabel";
            policyEndDateLabel.Size = new System.Drawing.Size(86, 13);
            policyEndDateLabel.TabIndex = 9;
            policyEndDateLabel.Text = "Policy End Date:";
            // 
            // previousFaultClaimsCountLabel
            // 
            previousFaultClaimsCountLabel.AutoSize = true;
            previousFaultClaimsCountLabel.Location = new System.Drawing.Point(602, 32);
            previousFaultClaimsCountLabel.Name = "previousFaultClaimsCountLabel";
            previousFaultClaimsCountLabel.Size = new System.Drawing.Size(141, 13);
            previousFaultClaimsCountLabel.TabIndex = 16;
            previousFaultClaimsCountLabel.Text = "Previous Fault Claims Count:";
            // 
            // policyNumberLabel
            // 
            policyNumberLabel.AutoSize = true;
            policyNumberLabel.Location = new System.Drawing.Point(24, 62);
            policyNumberLabel.Name = "policyNumberLabel";
            policyNumberLabel.Size = new System.Drawing.Size(78, 13);
            policyNumberLabel.TabIndex = 2;
            policyNumberLabel.Text = "Policy Number:";
            // 
            // premiumLabel
            // 
            premiumLabel.AutoSize = true;
            premiumLabel.Location = new System.Drawing.Point(290, 62);
            premiumLabel.Name = "premiumLabel";
            premiumLabel.Size = new System.Drawing.Size(50, 13);
            premiumLabel.TabIndex = 14;
            premiumLabel.Text = "Premium:";
            // 
            // policyStartDateLabel
            // 
            policyStartDateLabel.AutoSize = true;
            policyStartDateLabel.Location = new System.Drawing.Point(290, 89);
            policyStartDateLabel.Name = "policyStartDateLabel";
            policyStartDateLabel.Size = new System.Drawing.Size(89, 13);
            policyStartDateLabel.TabIndex = 6;
            policyStartDateLabel.Text = "Policy Start Date:";
            // 
            // policyTypeLabel
            // 
            policyTypeLabel.AutoSize = true;
            policyTypeLabel.Location = new System.Drawing.Point(290, 36);
            policyTypeLabel.Name = "policyTypeLabel";
            policyTypeLabel.Size = new System.Drawing.Size(65, 13);
            policyTypeLabel.TabIndex = 12;
            policyTypeLabel.Text = "Policy Type:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(11, 57);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(62, 13);
            label1.TabIndex = 28;
            label1.Text = "Claim Type:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(314, 31);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(94, 13);
            label2.TabIndex = 30;
            label2.Text = "Payments to Date:";
            label2.Visible = false;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(314, 57);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(50, 13);
            label3.TabIndex = 32;
            label3.Text = "Reserve:";
            label3.Visible = false;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(314, 20);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(94, 13);
            label8.TabIndex = 36;
            label8.Text = "Payments to Date:";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(602, 126);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(50, 13);
            label11.TabIndex = 32;
            label11.Text = "Reserve:";
            // 
            // ambulanceAttendedLabel
            // 
            ambulanceAttendedLabel.AutoSize = true;
            ambulanceAttendedLabel.Location = new System.Drawing.Point(602, 21);
            ambulanceAttendedLabel.Name = "ambulanceAttendedLabel";
            ambulanceAttendedLabel.Size = new System.Drawing.Size(109, 13);
            ambulanceAttendedLabel.TabIndex = 20;
            ambulanceAttendedLabel.Text = "Ambulance Attended:";
            // 
            // claimCodeLabel
            // 
            claimCodeLabel.AutoSize = true;
            claimCodeLabel.Location = new System.Drawing.Point(16, 22);
            claimCodeLabel.Name = "claimCodeLabel";
            claimCodeLabel.Size = new System.Drawing.Size(63, 13);
            claimCodeLabel.TabIndex = 2;
            claimCodeLabel.Text = "Claim Code:";
            // 
            // claimNotificationDateLabel
            // 
            claimNotificationDateLabel.AutoSize = true;
            claimNotificationDateLabel.Location = new System.Drawing.Point(16, 49);
            claimNotificationDateLabel.Name = "claimNotificationDateLabel";
            claimNotificationDateLabel.Size = new System.Drawing.Size(117, 13);
            claimNotificationDateLabel.TabIndex = 4;
            claimNotificationDateLabel.Text = "Claim Notification Date:";
            // 
            // claimStatusLabel
            // 
            claimStatusLabel.AutoSize = true;
            claimStatusLabel.Location = new System.Drawing.Point(16, 100);
            claimStatusLabel.Name = "claimStatusLabel";
            claimStatusLabel.Size = new System.Drawing.Size(68, 13);
            claimStatusLabel.TabIndex = 10;
            claimStatusLabel.Text = "Claim Status:";
            // 
            // incidentCircumstancesLabel
            // 
            incidentCircumstancesLabel.AutoSize = true;
            incidentCircumstancesLabel.Location = new System.Drawing.Point(314, 97);
            incidentCircumstancesLabel.Name = "incidentCircumstancesLabel";
            incidentCircumstancesLabel.Size = new System.Drawing.Size(120, 13);
            incidentCircumstancesLabel.TabIndex = 14;
            incidentCircumstancesLabel.Text = "Incident Circumstances:";
            // 
            // incidentLocationLabel
            // 
            incidentLocationLabel.AutoSize = true;
            incidentLocationLabel.Location = new System.Drawing.Point(314, 43);
            incidentLocationLabel.Name = "incidentLocationLabel";
            incidentLocationLabel.Size = new System.Drawing.Size(92, 13);
            incidentLocationLabel.TabIndex = 12;
            incidentLocationLabel.Text = "Incident Location:";
            // 
            // policeAttendedLabel
            // 
            policeAttendedLabel.AutoSize = true;
            policeAttendedLabel.Location = new System.Drawing.Point(602, 47);
            policeAttendedLabel.Name = "policeAttendedLabel";
            policeAttendedLabel.Size = new System.Drawing.Size(85, 13);
            policeAttendedLabel.TabIndex = 22;
            policeAttendedLabel.Text = "Police Attended:";
            // 
            // policeForceLabel
            // 
            policeForceLabel.AutoSize = true;
            policeForceLabel.Location = new System.Drawing.Point(602, 75);
            policeForceLabel.Name = "policeForceLabel";
            policeForceLabel.Size = new System.Drawing.Size(69, 13);
            policeForceLabel.TabIndex = 24;
            policeForceLabel.Text = "Police Force:";
            // 
            // policeReferenceLabel
            // 
            policeReferenceLabel.AutoSize = true;
            policeReferenceLabel.Location = new System.Drawing.Point(602, 101);
            policeReferenceLabel.Name = "policeReferenceLabel";
            policeReferenceLabel.Size = new System.Drawing.Size(92, 13);
            policeReferenceLabel.TabIndex = 26;
            policeReferenceLabel.Text = "Police Reference:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(602, 111);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(42, 13);
            label5.TabIndex = 22;
            label5.Text = "Insurer:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(637, 57);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(74, 13);
            label6.TabIndex = 35;
            label6.Text = "Incident Time:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(label6);
            this.groupBox5.Controls.Add(this.dateTimePicker2);
            this.groupBox5.Controls.Add(label3);
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(label2);
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.claimTypeComboBox);
            this.groupBox5.Controls.Add(label1);
            this.groupBox5.Controls.Add(claimNumberLabel);
            this.groupBox5.Controls.Add(this.claimNumberTextBox);
            this.groupBox5.Controls.Add(incidentDateLabel);
            this.groupBox5.Controls.Add(this.incidentDateDateTimePicker);
            this.groupBox5.Location = new System.Drawing.Point(12, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(919, 92);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Incident";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "";
            this.dateTimePicker2.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.incidentsBindingSource, "IncidentDate", true));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker2.Location = new System.Drawing.Point(723, 54);
            this.dateTimePicker2.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.dateTimePicker2.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.ShowUpDown = true;
            this.dateTimePicker2.Size = new System.Drawing.Size(151, 20);
            this.dateTimePicker2.TabIndex = 34;
            // 
            // incidentsBindingSource
            // 
            this.incidentsBindingSource.DataSource = typeof(MDA.Common.FileModel.MotorClaim);
            this.incidentsBindingSource.CurrentChanged += new System.EventHandler(this.incidentsBindingSource_CurrentChanged);
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "Reserve", true));
            this.textBox2.Location = new System.Drawing.Point(440, 54);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(151, 20);
            this.textBox2.TabIndex = 33;
            this.textBox2.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "PaymentsToDate", true));
            this.textBox1.Location = new System.Drawing.Point(440, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(151, 20);
            this.textBox1.TabIndex = 31;
            this.textBox1.Visible = false;
            // 
            // claimTypeComboBox
            // 
            this.claimTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "ClaimType", true));
            this.claimTypeComboBox.FormattingEnabled = true;
            this.claimTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Motor",
            "Commercial",
            "Employment",
            "PublicLiability",
            "Travel",
            "HomeContents",
            "HomeBuilding"});
            this.claimTypeComboBox.Location = new System.Drawing.Point(137, 54);
            this.claimTypeComboBox.Name = "claimTypeComboBox";
            this.claimTypeComboBox.Size = new System.Drawing.Size(151, 21);
            this.claimTypeComboBox.TabIndex = 29;
            // 
            // claimNumberTextBox
            // 
            this.claimNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "ClaimNumber", true));
            this.claimNumberTextBox.Location = new System.Drawing.Point(137, 28);
            this.claimNumberTextBox.Name = "claimNumberTextBox";
            this.claimNumberTextBox.Size = new System.Drawing.Size(151, 20);
            this.claimNumberTextBox.TabIndex = 1;
            // 
            // incidentDateDateTimePicker
            // 
            this.incidentDateDateTimePicker.CustomFormat = "";
            this.incidentDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.incidentsBindingSource, "IncidentDate", true));
            this.incidentDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.incidentDateDateTimePicker.Location = new System.Drawing.Point(723, 28);
            this.incidentDateDateTimePicker.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.incidentDateDateTimePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.incidentDateDateTimePicker.Name = "incidentDateDateTimePicker";
            this.incidentDateDateTimePicker.Size = new System.Drawing.Size(151, 20);
            this.incidentDateDateTimePicker.TabIndex = 9;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox3);
            this.groupBox4.Controls.Add(label5);
            this.groupBox4.Controls.Add(this.checkBox4);
            this.groupBox4.Controls.Add(this.checkBox3);
            this.groupBox4.Controls.Add(this.policyTypeComboBox);
            this.groupBox4.Controls.Add(this.coverTypeComboBox);
            this.groupBox4.Controls.Add(brokerLabel);
            this.groupBox4.Controls.Add(this.policyEndDateDateTimePicker);
            this.groupBox4.Controls.Add(policyEndDateLabel);
            this.groupBox4.Controls.Add(this.tradingNameTextBox);
            this.groupBox4.Controls.Add(this.brokerTextBox);
            this.groupBox4.Controls.Add(tradingNameLabel);
            this.groupBox4.Controls.Add(coverTypeLabel);
            this.groupBox4.Controls.Add(this.previousNoFaultClaimsCountTextBox);
            this.groupBox4.Controls.Add(previousNoFaultClaimsCountLabel);
            this.groupBox4.Controls.Add(this.previousFaultClaimsCountTextBox);
            this.groupBox4.Controls.Add(previousFaultClaimsCountLabel);
            this.groupBox4.Controls.Add(policyNumberLabel);
            this.groupBox4.Controls.Add(this.premiumTextBox);
            this.groupBox4.Controls.Add(this.policyNumberTextBox);
            this.groupBox4.Controls.Add(premiumLabel);
            this.groupBox4.Controls.Add(policyStartDateLabel);
            this.groupBox4.Controls.Add(this.policyStartDateDateTimePicker);
            this.groupBox4.Controls.Add(policyTypeLabel);
            this.groupBox4.Location = new System.Drawing.Point(12, 279);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(919, 144);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Policy";
            // 
            // textBox3
            // 
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "Policy.Insurer", true));
            this.textBox3.Location = new System.Drawing.Point(766, 105);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(137, 20);
            this.textBox3.TabIndex = 23;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(416, 111);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 10;
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(416, 88);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 7;
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // policyTypeComboBox
            // 
            this.policyTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "Policy.PolicyType", true));
            this.policyTypeComboBox.FormattingEnabled = true;
            this.policyTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "PersonalMotor",
            "CommercialMotor",
            "HomeContents",
            "HomeBuilding",
            "EmployersLiability"});
            this.policyTypeComboBox.Location = new System.Drawing.Point(440, 32);
            this.policyTypeComboBox.Name = "policyTypeComboBox";
            this.policyTypeComboBox.Size = new System.Drawing.Size(137, 21);
            this.policyTypeComboBox.TabIndex = 13;
            // 
            // coverTypeComboBox
            // 
            this.coverTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "Policy.CoverType", true));
            this.coverTypeComboBox.FormattingEnabled = true;
            this.coverTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Comprehensive",
            "ThirdPartyFireAndTheft",
            "ThirdPartyOnly"});
            this.coverTypeComboBox.Location = new System.Drawing.Point(137, 84);
            this.coverTypeComboBox.Name = "coverTypeComboBox";
            this.coverTypeComboBox.Size = new System.Drawing.Size(137, 21);
            this.coverTypeComboBox.TabIndex = 5;
            // 
            // policyEndDateDateTimePicker
            // 
            this.policyEndDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.incidentsBindingSource, "Policy.PolicyEndDate", true));
            this.policyEndDateDateTimePicker.Enabled = false;
            this.policyEndDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.policyEndDateDateTimePicker.Location = new System.Drawing.Point(440, 111);
            this.policyEndDateDateTimePicker.Name = "policyEndDateDateTimePicker";
            this.policyEndDateDateTimePicker.Size = new System.Drawing.Size(113, 20);
            this.policyEndDateDateTimePicker.TabIndex = 11;
            // 
            // tradingNameTextBox
            // 
            this.tradingNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "Policy.TradingName", true));
            this.tradingNameTextBox.Location = new System.Drawing.Point(766, 81);
            this.tradingNameTextBox.Name = "tradingNameTextBox";
            this.tradingNameTextBox.Size = new System.Drawing.Size(137, 20);
            this.tradingNameTextBox.TabIndex = 21;
            // 
            // brokerTextBox
            // 
            this.brokerTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "Policy.Broker", true));
            this.brokerTextBox.Location = new System.Drawing.Point(137, 33);
            this.brokerTextBox.Name = "brokerTextBox";
            this.brokerTextBox.Size = new System.Drawing.Size(137, 20);
            this.brokerTextBox.TabIndex = 1;
            // 
            // previousNoFaultClaimsCountTextBox
            // 
            this.previousNoFaultClaimsCountTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "Policy.PreviousNoFaultClaimsCount", true));
            this.previousNoFaultClaimsCountTextBox.Location = new System.Drawing.Point(766, 55);
            this.previousNoFaultClaimsCountTextBox.Name = "previousNoFaultClaimsCountTextBox";
            this.previousNoFaultClaimsCountTextBox.Size = new System.Drawing.Size(137, 20);
            this.previousNoFaultClaimsCountTextBox.TabIndex = 19;
            // 
            // previousFaultClaimsCountTextBox
            // 
            this.previousFaultClaimsCountTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "Policy.PreviousFaultClaimsCount", true));
            this.previousFaultClaimsCountTextBox.Location = new System.Drawing.Point(766, 29);
            this.previousFaultClaimsCountTextBox.Name = "previousFaultClaimsCountTextBox";
            this.previousFaultClaimsCountTextBox.Size = new System.Drawing.Size(137, 20);
            this.previousFaultClaimsCountTextBox.TabIndex = 17;
            // 
            // premiumTextBox
            // 
            this.premiumTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "Policy.Premium", true));
            this.premiumTextBox.Location = new System.Drawing.Point(440, 59);
            this.premiumTextBox.Name = "premiumTextBox";
            this.premiumTextBox.Size = new System.Drawing.Size(137, 20);
            this.premiumTextBox.TabIndex = 15;
            // 
            // policyNumberTextBox
            // 
            this.policyNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "Policy.PolicyNumber", true));
            this.policyNumberTextBox.Location = new System.Drawing.Point(137, 59);
            this.policyNumberTextBox.Name = "policyNumberTextBox";
            this.policyNumberTextBox.Size = new System.Drawing.Size(137, 20);
            this.policyNumberTextBox.TabIndex = 3;
            // 
            // policyStartDateDateTimePicker
            // 
            this.policyStartDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.incidentsBindingSource, "Policy.PolicyStartDate", true));
            this.policyStartDateDateTimePicker.Enabled = false;
            this.policyStartDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.policyStartDateDateTimePicker.Location = new System.Drawing.Point(440, 85);
            this.policyStartDateDateTimePicker.Name = "policyStartDateDateTimePicker";
            this.policyStartDateDateTimePicker.Size = new System.Drawing.Size(113, 20);
            this.policyStartDateDateTimePicker.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(775, 429);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(856, 429);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(label8);
            this.groupBox1.Controls.Add(this.textBox11);
            this.groupBox1.Controls.Add(this.textBox9);
            this.groupBox1.Controls.Add(label11);
            this.groupBox1.Controls.Add(this.checkBox5);
            this.groupBox1.Controls.Add(this.claimStatusComboBox);
            this.groupBox1.Controls.Add(ambulanceAttendedLabel);
            this.groupBox1.Controls.Add(claimCodeLabel);
            this.groupBox1.Controls.Add(this.claimCodeTextBox);
            this.groupBox1.Controls.Add(this.ambulanceAttendedCheckBox);
            this.groupBox1.Controls.Add(claimNotificationDateLabel);
            this.groupBox1.Controls.Add(this.claimNotificationDateDateTimePicker);
            this.groupBox1.Controls.Add(claimStatusLabel);
            this.groupBox1.Controls.Add(incidentCircumstancesLabel);
            this.groupBox1.Controls.Add(this.incidentCircumstancesTextBox);
            this.groupBox1.Controls.Add(incidentLocationLabel);
            this.groupBox1.Controls.Add(this.incidentLocationTextBox);
            this.groupBox1.Controls.Add(policeAttendedLabel);
            this.groupBox1.Controls.Add(this.policeAttendedCheckBox);
            this.groupBox1.Controls.Add(policeForceLabel);
            this.groupBox1.Controls.Add(this.policeForceTextBox);
            this.groupBox1.Controls.Add(policeReferenceLabel);
            this.groupBox1.Controls.Add(this.policeReferenceTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 110);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(919, 163);
            this.groupBox1.TabIndex = 62;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Incident Details (ClaimInfo)";
            // 
            // textBox11
            // 
            this.textBox11.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "MotorClaimInfo.PaymentsToDate", true));
            this.textBox11.Location = new System.Drawing.Point(440, 17);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(151, 20);
            this.textBox11.TabIndex = 35;
            // 
            // textBox9
            // 
            this.textBox9.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "MotorClaimInfo.Reserve", true));
            this.textBox9.Location = new System.Drawing.Point(728, 123);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(118, 20);
            this.textBox9.TabIndex = 33;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(142, 48);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 5;
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // claimStatusComboBox
            // 
            this.claimStatusComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "MotorClaimInfo.ClaimStatus", true));
            this.claimStatusComboBox.FormattingEnabled = true;
            this.claimStatusComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Open",
            "Settled",
            "Withdrawn",
            "Reopened",
            "Delete"});
            this.claimStatusComboBox.Location = new System.Drawing.Point(142, 97);
            this.claimStatusComboBox.Name = "claimStatusComboBox";
            this.claimStatusComboBox.Size = new System.Drawing.Size(151, 21);
            this.claimStatusComboBox.TabIndex = 11;
            // 
            // claimCodeTextBox
            // 
            this.claimCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "MotorClaimInfo.ClaimCode", true));
            this.claimCodeTextBox.Location = new System.Drawing.Point(142, 19);
            this.claimCodeTextBox.Name = "claimCodeTextBox";
            this.claimCodeTextBox.Size = new System.Drawing.Size(151, 20);
            this.claimCodeTextBox.TabIndex = 3;
            // 
            // ambulanceAttendedCheckBox
            // 
            this.ambulanceAttendedCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.incidentsBindingSource, "MotorClaimInfo.AmbulanceAttended", true));
            this.ambulanceAttendedCheckBox.Location = new System.Drawing.Point(728, 16);
            this.ambulanceAttendedCheckBox.Name = "ambulanceAttendedCheckBox";
            this.ambulanceAttendedCheckBox.Size = new System.Drawing.Size(24, 24);
            this.ambulanceAttendedCheckBox.TabIndex = 21;
            this.ambulanceAttendedCheckBox.UseVisualStyleBackColor = true;
            // 
            // claimNotificationDateDateTimePicker
            // 
            this.claimNotificationDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.incidentsBindingSource, "MotorClaimInfo.ClaimNotificationDate", true));
            this.claimNotificationDateDateTimePicker.Enabled = false;
            this.claimNotificationDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.claimNotificationDateDateTimePicker.Location = new System.Drawing.Point(175, 45);
            this.claimNotificationDateDateTimePicker.Name = "claimNotificationDateDateTimePicker";
            this.claimNotificationDateDateTimePicker.Size = new System.Drawing.Size(118, 20);
            this.claimNotificationDateDateTimePicker.TabIndex = 6;
            // 
            // incidentCircumstancesTextBox
            // 
            this.incidentCircumstancesTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "MotorClaimInfo.IncidentCircumstances", true));
            this.incidentCircumstancesTextBox.Location = new System.Drawing.Point(440, 66);
            this.incidentCircumstancesTextBox.Multiline = true;
            this.incidentCircumstancesTextBox.Name = "incidentCircumstancesTextBox";
            this.incidentCircumstancesTextBox.Size = new System.Drawing.Size(151, 78);
            this.incidentCircumstancesTextBox.TabIndex = 15;
            // 
            // incidentLocationTextBox
            // 
            this.incidentLocationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "MotorClaimInfo.IncidentLocation", true));
            this.incidentLocationTextBox.Location = new System.Drawing.Point(440, 40);
            this.incidentLocationTextBox.Name = "incidentLocationTextBox";
            this.incidentLocationTextBox.Size = new System.Drawing.Size(151, 20);
            this.incidentLocationTextBox.TabIndex = 13;
            // 
            // policeAttendedCheckBox
            // 
            this.policeAttendedCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.incidentsBindingSource, "MotorClaimInfo.PoliceAttended", true));
            this.policeAttendedCheckBox.Location = new System.Drawing.Point(728, 42);
            this.policeAttendedCheckBox.Name = "policeAttendedCheckBox";
            this.policeAttendedCheckBox.Size = new System.Drawing.Size(24, 24);
            this.policeAttendedCheckBox.TabIndex = 23;
            this.policeAttendedCheckBox.UseVisualStyleBackColor = true;
            // 
            // policeForceTextBox
            // 
            this.policeForceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "MotorClaimInfo.PoliceForce", true));
            this.policeForceTextBox.Location = new System.Drawing.Point(728, 72);
            this.policeForceTextBox.Name = "policeForceTextBox";
            this.policeForceTextBox.Size = new System.Drawing.Size(118, 20);
            this.policeForceTextBox.TabIndex = 25;
            // 
            // policeReferenceTextBox
            // 
            this.policeReferenceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "MotorClaimInfo.PoliceReference", true));
            this.policeReferenceTextBox.Location = new System.Drawing.Point(728, 98);
            this.policeReferenceTextBox.Name = "policeReferenceTextBox";
            this.policeReferenceTextBox.Size = new System.Drawing.Size(118, 20);
            this.policeReferenceTextBox.TabIndex = 27;
            // 
            // comboBox3
            // 
            this.comboBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.incidentsBindingSource, "Operation", true));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Normal",
            "Delete",
            "Withdraw"});
            this.comboBox3.Location = new System.Drawing.Point(125, 433);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(144, 21);
            this.comboBox3.TabIndex = 79;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(16, 436);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(56, 13);
            label4.TabIndex = 78;
            label4.Text = "Operation:";
            // 
            // IncidentEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 466);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Name = "IncidentEdit";
            this.Text = "Edit Incident Details";
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.incidentsBindingSource)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox claimNumberTextBox;
        private System.Windows.Forms.DateTimePicker incidentDateDateTimePicker;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tradingNameTextBox;
        private System.Windows.Forms.TextBox brokerTextBox;
        private System.Windows.Forms.TextBox previousNoFaultClaimsCountTextBox;
        private System.Windows.Forms.TextBox previousFaultClaimsCountTextBox;
        private System.Windows.Forms.DateTimePicker policyEndDateDateTimePicker;
        private System.Windows.Forms.TextBox premiumTextBox;
        private System.Windows.Forms.TextBox policyNumberTextBox;
        private System.Windows.Forms.DateTimePicker policyStartDateDateTimePicker;
        public System.Windows.Forms.BindingSource incidentsBindingSource;
        private System.Windows.Forms.ComboBox coverTypeComboBox;
        private System.Windows.Forms.ComboBox policyTypeComboBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox claimTypeComboBox;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.ComboBox claimStatusComboBox;
        private System.Windows.Forms.TextBox claimCodeTextBox;
        private System.Windows.Forms.CheckBox ambulanceAttendedCheckBox;
        private System.Windows.Forms.DateTimePicker claimNotificationDateDateTimePicker;
        private System.Windows.Forms.TextBox incidentCircumstancesTextBox;
        private System.Windows.Forms.TextBox incidentLocationTextBox;
        private System.Windows.Forms.CheckBox policeAttendedCheckBox;
        private System.Windows.Forms.TextBox policeForceTextBox;
        private System.Windows.Forms.TextBox policeReferenceTextBox;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.ComboBox comboBox3;

    }
}