﻿namespace MDA.FileEditor
{
    partial class PeopleEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label dateOfBirthLabel;
            System.Windows.Forms.Label firstNameLabel;
            System.Windows.Forms.Label genderLabel;
            System.Windows.Forms.Label lastNameLabel;
            System.Windows.Forms.Label middleNameLabel;
            System.Windows.Forms.Label nationalityLabel;
            System.Windows.Forms.Label occupationLabel;
            System.Windows.Forms.Label partyTypeLabel;
            System.Windows.Forms.Label salutationLabel;
            System.Windows.Forms.Label subPartyTypeLabel;
            System.Windows.Forms.Label accountNumberLabel;
            System.Windows.Forms.Label bankNameLabel;
            System.Windows.Forms.Label datePaymentDetailsTakenLabel;
            System.Windows.Forms.Label policyPaymentTypeLabel;
            System.Windows.Forms.Label sortCodeLabel;
            System.Windows.Forms.Label bankLabel;
            System.Windows.Forms.Label datePaymentDetailsTakenLabel1;
            System.Windows.Forms.Label paymentCardNumberLabel;
            System.Windows.Forms.Label paymentCardTypeLabel;
            System.Windows.Forms.Label sortCodeLabel1;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label11;
            this.dateOfBirthDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.peopleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.middleNameTextBox = new System.Windows.Forms.TextBox();
            this.occupationTextBox = new System.Windows.Forms.TextBox();
            this.accountNumberTextBox = new System.Windows.Forms.TextBox();
            this.bankNameTextBox = new System.Windows.Forms.TextBox();
            this.datePaymentDetailsTakenDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.sortCodeTextBox = new System.Windows.Forms.TextBox();
            this.bankTextBox = new System.Windows.Forms.TextBox();
            this.datePaymentDetailsTakenDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.paymentCardNumberTextBox = new System.Windows.Forms.TextBox();
            this.sortCodeTextBox1 = new System.Windows.Forms.TextBox();
            this.salutationComboBox = new System.Windows.Forms.ComboBox();
            this.genderComboBox = new System.Windows.Forms.ComboBox();
            this.nationalityComboBox = new System.Windows.Forms.ComboBox();
            this.partyTypeComboBox = new System.Windows.Forms.ComboBox();
            this.subPartyTypeComboBox = new System.Windows.Forms.ComboBox();
            this.policyPaymentTypeComboBox = new System.Windows.Forms.ComboBox();
            this.paymentCardTypeComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlBank = new System.Windows.Forms.Panel();
            this.chkBank = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkCard = new System.Windows.Forms.CheckBox();
            this.pnlCard = new System.Windows.Forms.Panel();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            dateOfBirthLabel = new System.Windows.Forms.Label();
            firstNameLabel = new System.Windows.Forms.Label();
            genderLabel = new System.Windows.Forms.Label();
            lastNameLabel = new System.Windows.Forms.Label();
            middleNameLabel = new System.Windows.Forms.Label();
            nationalityLabel = new System.Windows.Forms.Label();
            occupationLabel = new System.Windows.Forms.Label();
            partyTypeLabel = new System.Windows.Forms.Label();
            salutationLabel = new System.Windows.Forms.Label();
            subPartyTypeLabel = new System.Windows.Forms.Label();
            accountNumberLabel = new System.Windows.Forms.Label();
            bankNameLabel = new System.Windows.Forms.Label();
            datePaymentDetailsTakenLabel = new System.Windows.Forms.Label();
            policyPaymentTypeLabel = new System.Windows.Forms.Label();
            sortCodeLabel = new System.Windows.Forms.Label();
            bankLabel = new System.Windows.Forms.Label();
            datePaymentDetailsTakenLabel1 = new System.Windows.Forms.Label();
            paymentCardNumberLabel = new System.Windows.Forms.Label();
            paymentCardTypeLabel = new System.Windows.Forms.Label();
            sortCodeLabel1 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.peopleBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.pnlBank.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pnlCard.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dateOfBirthLabel
            // 
            dateOfBirthLabel.AutoSize = true;
            dateOfBirthLabel.Location = new System.Drawing.Point(11, 148);
            dateOfBirthLabel.Name = "dateOfBirthLabel";
            dateOfBirthLabel.Size = new System.Drawing.Size(71, 13);
            dateOfBirthLabel.TabIndex = 10;
            dateOfBirthLabel.Text = "Date Of Birth:";
            // 
            // firstNameLabel
            // 
            firstNameLabel.AutoSize = true;
            firstNameLabel.Location = new System.Drawing.Point(11, 45);
            firstNameLabel.Name = "firstNameLabel";
            firstNameLabel.Size = new System.Drawing.Size(60, 13);
            firstNameLabel.TabIndex = 2;
            firstNameLabel.Text = "First Name:";
            // 
            // genderLabel
            // 
            genderLabel.AutoSize = true;
            genderLabel.Location = new System.Drawing.Point(11, 120);
            genderLabel.Name = "genderLabel";
            genderLabel.Size = new System.Drawing.Size(45, 13);
            genderLabel.TabIndex = 8;
            genderLabel.Text = "Gender:";
            // 
            // lastNameLabel
            // 
            lastNameLabel.AutoSize = true;
            lastNameLabel.Location = new System.Drawing.Point(11, 95);
            lastNameLabel.Name = "lastNameLabel";
            lastNameLabel.Size = new System.Drawing.Size(61, 13);
            lastNameLabel.TabIndex = 6;
            lastNameLabel.Text = "Last Name:";
            // 
            // middleNameLabel
            // 
            middleNameLabel.AutoSize = true;
            middleNameLabel.Location = new System.Drawing.Point(11, 69);
            middleNameLabel.Name = "middleNameLabel";
            middleNameLabel.Size = new System.Drawing.Size(72, 13);
            middleNameLabel.TabIndex = 4;
            middleNameLabel.Text = "Middle Name:";
            // 
            // nationalityLabel
            // 
            nationalityLabel.AutoSize = true;
            nationalityLabel.Location = new System.Drawing.Point(11, 172);
            nationalityLabel.Name = "nationalityLabel";
            nationalityLabel.Size = new System.Drawing.Size(59, 13);
            nationalityLabel.TabIndex = 13;
            nationalityLabel.Text = "Nationality:";
            // 
            // occupationLabel
            // 
            occupationLabel.AutoSize = true;
            occupationLabel.Location = new System.Drawing.Point(11, 198);
            occupationLabel.Name = "occupationLabel";
            occupationLabel.Size = new System.Drawing.Size(65, 13);
            occupationLabel.TabIndex = 15;
            occupationLabel.Text = "Occupation:";
            // 
            // partyTypeLabel
            // 
            partyTypeLabel.AutoSize = true;
            partyTypeLabel.Location = new System.Drawing.Point(11, 224);
            partyTypeLabel.Name = "partyTypeLabel";
            partyTypeLabel.Size = new System.Drawing.Size(61, 13);
            partyTypeLabel.TabIndex = 17;
            partyTypeLabel.Text = "Party Type:";
            // 
            // salutationLabel
            // 
            salutationLabel.AutoSize = true;
            salutationLabel.Location = new System.Drawing.Point(11, 19);
            salutationLabel.Name = "salutationLabel";
            salutationLabel.Size = new System.Drawing.Size(57, 13);
            salutationLabel.TabIndex = 0;
            salutationLabel.Text = "Salutation:";
            // 
            // subPartyTypeLabel
            // 
            subPartyTypeLabel.AutoSize = true;
            subPartyTypeLabel.Location = new System.Drawing.Point(11, 251);
            subPartyTypeLabel.Name = "subPartyTypeLabel";
            subPartyTypeLabel.Size = new System.Drawing.Size(83, 13);
            subPartyTypeLabel.TabIndex = 19;
            subPartyTypeLabel.Text = "Sub Party Type:";
            // 
            // accountNumberLabel
            // 
            accountNumberLabel.AutoSize = true;
            accountNumberLabel.Location = new System.Drawing.Point(3, 37);
            accountNumberLabel.Name = "accountNumberLabel";
            accountNumberLabel.Size = new System.Drawing.Size(90, 13);
            accountNumberLabel.TabIndex = 33;
            accountNumberLabel.Text = "Account Number:";
            // 
            // bankNameLabel
            // 
            bankNameLabel.AutoSize = true;
            bankNameLabel.Location = new System.Drawing.Point(3, 14);
            bankNameLabel.Name = "bankNameLabel";
            bankNameLabel.Size = new System.Drawing.Size(66, 13);
            bankNameLabel.TabIndex = 31;
            bankNameLabel.Text = "Bank Name:";
            // 
            // datePaymentDetailsTakenLabel
            // 
            datePaymentDetailsTakenLabel.AutoSize = true;
            datePaymentDetailsTakenLabel.Location = new System.Drawing.Point(3, 119);
            datePaymentDetailsTakenLabel.Name = "datePaymentDetailsTakenLabel";
            datePaymentDetailsTakenLabel.Size = new System.Drawing.Size(146, 13);
            datePaymentDetailsTakenLabel.TabIndex = 39;
            datePaymentDetailsTakenLabel.Text = "Date Payment Details Taken:";
            // 
            // policyPaymentTypeLabel
            // 
            policyPaymentTypeLabel.AutoSize = true;
            policyPaymentTypeLabel.Location = new System.Drawing.Point(3, 92);
            policyPaymentTypeLabel.Name = "policyPaymentTypeLabel";
            policyPaymentTypeLabel.Size = new System.Drawing.Size(78, 13);
            policyPaymentTypeLabel.TabIndex = 37;
            policyPaymentTypeLabel.Text = "Payment Type:";
            // 
            // sortCodeLabel
            // 
            sortCodeLabel.AutoSize = true;
            sortCodeLabel.Location = new System.Drawing.Point(3, 61);
            sortCodeLabel.Name = "sortCodeLabel";
            sortCodeLabel.Size = new System.Drawing.Size(57, 13);
            sortCodeLabel.TabIndex = 35;
            sortCodeLabel.Text = "Sort Code:";
            // 
            // bankLabel
            // 
            bankLabel.AutoSize = true;
            bankLabel.Location = new System.Drawing.Point(3, 5);
            bankLabel.Name = "bankLabel";
            bankLabel.Size = new System.Drawing.Size(35, 13);
            bankLabel.TabIndex = 42;
            bankLabel.Text = "Bank:";
            // 
            // datePaymentDetailsTakenLabel1
            // 
            datePaymentDetailsTakenLabel1.AutoSize = true;
            datePaymentDetailsTakenLabel1.Location = new System.Drawing.Point(3, 133);
            datePaymentDetailsTakenLabel1.Name = "datePaymentDetailsTakenLabel1";
            datePaymentDetailsTakenLabel1.Size = new System.Drawing.Size(146, 13);
            datePaymentDetailsTakenLabel1.TabIndex = 50;
            datePaymentDetailsTakenLabel1.Text = "Date Payment Details Taken:";
            // 
            // paymentCardNumberLabel
            // 
            paymentCardNumberLabel.AutoSize = true;
            paymentCardNumberLabel.Location = new System.Drawing.Point(3, 57);
            paymentCardNumberLabel.Name = "paymentCardNumberLabel";
            paymentCardNumberLabel.Size = new System.Drawing.Size(116, 13);
            paymentCardNumberLabel.TabIndex = 46;
            paymentCardNumberLabel.Text = "Payment Card Number:";
            // 
            // paymentCardTypeLabel
            // 
            paymentCardTypeLabel.AutoSize = true;
            paymentCardTypeLabel.Location = new System.Drawing.Point(3, 31);
            paymentCardTypeLabel.Name = "paymentCardTypeLabel";
            paymentCardTypeLabel.Size = new System.Drawing.Size(103, 13);
            paymentCardTypeLabel.TabIndex = 44;
            paymentCardTypeLabel.Text = "Payment Card Type:";
            // 
            // sortCodeLabel1
            // 
            sortCodeLabel1.AutoSize = true;
            sortCodeLabel1.Location = new System.Drawing.Point(3, 110);
            sortCodeLabel1.Name = "sortCodeLabel1";
            sortCodeLabel1.Size = new System.Drawing.Size(108, 13);
            sortCodeLabel1.TabIndex = 48;
            sortCodeLabel1.Text = "Sort Code: (nn-nn-nn)";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(11, 275);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(38, 13);
            label1.TabIndex = 57;
            label1.Text = "Email :";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(18, 21);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(53, 13);
            label2.TabIndex = 60;
            label2.Text = "Landline :";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(18, 45);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(73, 13);
            label3.TabIndex = 61;
            label3.Text = "Work Phone :";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(18, 69);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(78, 13);
            label4.TabIndex = 62;
            label4.Text = "Mobile Phone :";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(18, 91);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(73, 13);
            label6.TabIndex = 65;
            label6.Text = "Other Phone :";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(11, 300);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(64, 13);
            label7.TabIndex = 61;
            label7.Text = "NI Number :";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(11, 326);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(66, 13);
            label8.TabIndex = 63;
            label8.Text = "Driving Lic. :";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(11, 356);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(74, 13);
            label10.TabIndex = 66;
            label10.Text = "Passport No. :";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(3, 86);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(78, 13);
            label12.TabIndex = 53;
            label12.Text = "Payment Type:";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(216, 109);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(89, 13);
            label13.TabIndex = 55;
            label13.Text = "Exp Date MMYY:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(11, 382);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(94, 13);
            label5.TabIndex = 68;
            label5.Text = "Attended Hospital:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(12, 415);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(65, 13);
            label9.TabIndex = 70;
            label9.Text = "MOJ Status:";
            // 
            // dateOfBirthDateTimePicker
            // 
            this.dateOfBirthDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.peopleBindingSource, "DateOfBirth", true));
            this.dateOfBirthDateTimePicker.Enabled = false;
            this.dateOfBirthDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateOfBirthDateTimePicker.Location = new System.Drawing.Point(142, 144);
            this.dateOfBirthDateTimePicker.Name = "dateOfBirthDateTimePicker";
            this.dateOfBirthDateTimePicker.Size = new System.Drawing.Size(125, 20);
            this.dateOfBirthDateTimePicker.TabIndex = 12;
            // 
            // peopleBindingSource
            // 
            this.peopleBindingSource.DataSource = typeof(MDA.Common.FileModel.Person);
            this.peopleBindingSource.CurrentChanged += new System.EventHandler(this.peopleBindingSource_CurrentChanged);
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "FirstName", true));
            this.firstNameTextBox.Location = new System.Drawing.Point(121, 42);
            this.firstNameTextBox.MaxLength = 50;
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(146, 20);
            this.firstNameTextBox.TabIndex = 3;
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "LastName", true));
            this.lastNameTextBox.Location = new System.Drawing.Point(121, 92);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(146, 20);
            this.lastNameTextBox.TabIndex = 7;
            // 
            // middleNameTextBox
            // 
            this.middleNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "MiddleName", true));
            this.middleNameTextBox.Location = new System.Drawing.Point(121, 66);
            this.middleNameTextBox.Name = "middleNameTextBox";
            this.middleNameTextBox.Size = new System.Drawing.Size(146, 20);
            this.middleNameTextBox.TabIndex = 5;
            // 
            // occupationTextBox
            // 
            this.occupationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "Occupation", true));
            this.occupationTextBox.Location = new System.Drawing.Point(121, 195);
            this.occupationTextBox.Name = "occupationTextBox";
            this.occupationTextBox.Size = new System.Drawing.Size(146, 20);
            this.occupationTextBox.TabIndex = 16;
            // 
            // accountNumberTextBox
            // 
            this.accountNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "BankAccount.AccountNumber", true));
            this.accountNumberTextBox.Location = new System.Drawing.Point(155, 34);
            this.accountNumberTextBox.Name = "accountNumberTextBox";
            this.accountNumberTextBox.Size = new System.Drawing.Size(200, 20);
            this.accountNumberTextBox.TabIndex = 34;
            // 
            // bankNameTextBox
            // 
            this.bankNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "BankAccount.BankName", true));
            this.bankNameTextBox.Location = new System.Drawing.Point(155, 11);
            this.bankNameTextBox.Name = "bankNameTextBox";
            this.bankNameTextBox.Size = new System.Drawing.Size(200, 20);
            this.bankNameTextBox.TabIndex = 32;
            // 
            // datePaymentDetailsTakenDateTimePicker
            // 
            this.datePaymentDetailsTakenDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.peopleBindingSource, "BankAccount.DatePaymentDetailsTaken", true));
            this.datePaymentDetailsTakenDateTimePicker.Enabled = false;
            this.datePaymentDetailsTakenDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePaymentDetailsTakenDateTimePicker.Location = new System.Drawing.Point(176, 115);
            this.datePaymentDetailsTakenDateTimePicker.Name = "datePaymentDetailsTakenDateTimePicker";
            this.datePaymentDetailsTakenDateTimePicker.Size = new System.Drawing.Size(179, 20);
            this.datePaymentDetailsTakenDateTimePicker.TabIndex = 41;
            // 
            // sortCodeTextBox
            // 
            this.sortCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "BankAccount.SortCode", true));
            this.sortCodeTextBox.Location = new System.Drawing.Point(155, 58);
            this.sortCodeTextBox.Name = "sortCodeTextBox";
            this.sortCodeTextBox.Size = new System.Drawing.Size(200, 20);
            this.sortCodeTextBox.TabIndex = 36;
            // 
            // bankTextBox
            // 
            this.bankTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "PaymentCard.BankName", true));
            this.bankTextBox.Location = new System.Drawing.Point(155, 5);
            this.bankTextBox.Name = "bankTextBox";
            this.bankTextBox.Size = new System.Drawing.Size(200, 20);
            this.bankTextBox.TabIndex = 43;
            // 
            // datePaymentDetailsTakenDateTimePicker1
            // 
            this.datePaymentDetailsTakenDateTimePicker1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.peopleBindingSource, "PaymentCard.DatePaymentDetailsTaken", true));
            this.datePaymentDetailsTakenDateTimePicker1.Enabled = false;
            this.datePaymentDetailsTakenDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePaymentDetailsTakenDateTimePicker1.Location = new System.Drawing.Point(176, 133);
            this.datePaymentDetailsTakenDateTimePicker1.Name = "datePaymentDetailsTakenDateTimePicker1";
            this.datePaymentDetailsTakenDateTimePicker1.Size = new System.Drawing.Size(179, 20);
            this.datePaymentDetailsTakenDateTimePicker1.TabIndex = 52;
            // 
            // paymentCardNumberTextBox
            // 
            this.paymentCardNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "PaymentCard.PaymentCardNumber", true));
            this.paymentCardNumberTextBox.Location = new System.Drawing.Point(155, 57);
            this.paymentCardNumberTextBox.Name = "paymentCardNumberTextBox";
            this.paymentCardNumberTextBox.Size = new System.Drawing.Size(200, 20);
            this.paymentCardNumberTextBox.TabIndex = 47;
            // 
            // sortCodeTextBox1
            // 
            this.sortCodeTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "PaymentCard.SortCode", true));
            this.sortCodeTextBox1.Location = new System.Drawing.Point(155, 107);
            this.sortCodeTextBox1.MaxLength = 8;
            this.sortCodeTextBox1.Name = "sortCodeTextBox1";
            this.sortCodeTextBox1.Size = new System.Drawing.Size(55, 20);
            this.sortCodeTextBox1.TabIndex = 49;
            // 
            // salutationComboBox
            // 
            this.salutationComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "Salutation", true));
            this.salutationComboBox.FormattingEnabled = true;
            this.salutationComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Mr",
            "Mrs",
            "Ms",
            "Miss",
            "Master",
            "Dr",
            "Professor",
            "Reverend",
            "Sir",
            "Lord",
            "Baroness",
            "RightHonourable"});
            this.salutationComboBox.Location = new System.Drawing.Point(121, 16);
            this.salutationComboBox.Name = "salutationComboBox";
            this.salutationComboBox.Size = new System.Drawing.Size(121, 21);
            this.salutationComboBox.TabIndex = 1;
            // 
            // genderComboBox
            // 
            this.genderComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "Gender", true));
            this.genderComboBox.FormattingEnabled = true;
            this.genderComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Male",
            "Female"});
            this.genderComboBox.Location = new System.Drawing.Point(121, 117);
            this.genderComboBox.Name = "genderComboBox";
            this.genderComboBox.Size = new System.Drawing.Size(121, 21);
            this.genderComboBox.TabIndex = 9;
            // 
            // nationalityComboBox
            // 
            this.nationalityComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "Nationality", true));
            this.nationalityComboBox.FormattingEnabled = true;
            this.nationalityComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Afghanistan",
            "Albania",
            "Algeria",
            "Andorra",
            "Angola",
            "Anguilla",
            "Antigua",
            "Antilles",
            "Argentina",
            "Armenia",
            "Aruba",
            "Ascension Island",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Azores",
            "Bahamas",
            "Bahrain",
            "Balarus",
            "Bangladesh",
            "Barbados",
            "Belgium",
            "Belize",
            "Benin",
            "Bermuda",
            "Bhutan",
            "Bolivia",
            "Bosnia Hercegovina",
            "Botswana",
            "Brazil",
            "Brunei",
            "Bulgaria",
            "Burkina Faso",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Canary Islands",
            "CapeVerde Islands",
            "Cayman Islands",
            "Central African Republic",
            "Chad",
            "Chile",
            "China",
            "Christmas Island",
            "Cocos Islands",
            "Colombia",
            "Comoros",
            "Congo",
            "Cook Islands",
            "Costa Rica",
            "Cote dIvoire",
            "Croatia",
            "Cuba",
            "Cyprus",
            "CzechRepublic",
            "Denmark",
            "Djbouti",
            "Dominica",
            "Ecuador",
            "Egypt",
            "ElSalvador",
            "England",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Falkland Islands",
            "Faroe Islands",
            "Fiji",
            "Finland",
            "France",
            "French Guiana",
            "French Polynesia",
            "Gabon",
            "Gambia",
            "Georgia",
            "Germany",
            "Ghana",
            "Gibraltar",
            "Great Britain",
            "Greece",
            "Greenland",
            "Grenada",
            "Guadeloupe",
            "Guam",
            "Guatemala",
            "Guinea",
            "Guyana",
            "Haiti",
            "Honduras",
            "Hong Kong",
            "Hungary",
            "Ibiza",
            "Iceland",
            "India",
            "Indonesia",
            "Inmarsat",
            "Iran",
            "Iraq",
            "Republic Of Ireland",
            "Israel",
            "Italy",
            "Ivory Coast",
            "Jamaica",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kirghizstan",
            "Kiribati",
            "Korea (North)",
            "Korea (South)",
            "Kosovo",
            "Kurdistan",
            "Kuwait",
            "Laos",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Macao",
            "Macedonia",
            "Madagascar",
            "Madeira",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Marshall Islands",
            "Martinique",
            "Mauritania",
            "Mauritius",
            "Mayotte",
            "Mexico",
            "Micronesia",
            "Midway Island",
            "Minorca",
            "Moldova",
            "Monaco",
            "Mongolia",
            "Montserrat",
            "Morocco",
            "Mozambique",
            "Myanmar",
            "Namibia",
            "Nauru",
            "Nepal",
            "Netherlands",
            "New Caledonia",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "Niue",
            "Norfolk Island",
            "Northern Ireland",
            "Northern Marianas",
            "Norway",
            "Oman",
            "Pakistan",
            "Palau",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Pitcairn Island",
            "Poland",
            "Portugal",
            "Puerto Rico",
            "Qatar",
            "Romania",
            "Russia",
            "Rwanda",
            "Samoa",
            "San Marino",
            "Sao Tome Principe",
            "Saudi Arabia",
            "Scotland",
            "Senegal",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovakia",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "Spain",
            "Sri Lanka",
            "St Helena",
            "St Kitts Nevis",
            "St Lucia",
            "St Pierre Miquelon",
            "St Vince Grenadines",
            "Sudan",
            "Suriname",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Syria",
            "Tadzhikistan",
            "Taiwan",
            "Tanzania",
            "Thailand",
            "Togo",
            "Tonga",
            "Trinidad Tobago",
            "Tristan Da Cunha",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Turks Caicos Islands",
            "Tuvalu",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "Uruguay",
            "USA",
            "Uzbekistan",
            "Vanuatu",
            "Venezuela",
            "Vietnam",
            "Virgin Islands",
            "Wake Island",
            "Wales",
            "Wallis Futuna",
            "Yemen",
            "Yugoslavia",
            "Zaire",
            "Zambia",
            "Zimbabwe"});
            this.nationalityComboBox.Location = new System.Drawing.Point(121, 169);
            this.nationalityComboBox.Name = "nationalityComboBox";
            this.nationalityComboBox.Size = new System.Drawing.Size(146, 21);
            this.nationalityComboBox.TabIndex = 14;
            // 
            // partyTypeComboBox
            // 
            this.partyTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "PartyType", true));
            this.partyTypeComboBox.FormattingEnabled = true;
            this.partyTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Claimant",
            "Hirer",
            "Insured",
            "LitigationFriend",
            "Policyholder",
            "ThirdParty",
            "Witness",
            "VehicleOwner",
            "PaidPolicy"});
            this.partyTypeComboBox.Location = new System.Drawing.Point(121, 221);
            this.partyTypeComboBox.Name = "partyTypeComboBox";
            this.partyTypeComboBox.Size = new System.Drawing.Size(146, 21);
            this.partyTypeComboBox.TabIndex = 18;
            // 
            // subPartyTypeComboBox
            // 
            this.subPartyTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "SubPartyType", true));
            this.subPartyTypeComboBox.FormattingEnabled = true;
            this.subPartyTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Driver",
            "Passenger",
            "Witness"});
            this.subPartyTypeComboBox.Location = new System.Drawing.Point(121, 248);
            this.subPartyTypeComboBox.Name = "subPartyTypeComboBox";
            this.subPartyTypeComboBox.Size = new System.Drawing.Size(146, 21);
            this.subPartyTypeComboBox.TabIndex = 20;
            // 
            // policyPaymentTypeComboBox
            // 
            this.policyPaymentTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "BankAccount.PolicyPaymentType", true));
            this.policyPaymentTypeComboBox.FormattingEnabled = true;
            this.policyPaymentTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Deposit",
            "DirectDebit",
            "SinglePayment"});
            this.policyPaymentTypeComboBox.Location = new System.Drawing.Point(155, 87);
            this.policyPaymentTypeComboBox.Name = "policyPaymentTypeComboBox";
            this.policyPaymentTypeComboBox.Size = new System.Drawing.Size(200, 21);
            this.policyPaymentTypeComboBox.TabIndex = 38;
            // 
            // paymentCardTypeComboBox
            // 
            this.paymentCardTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "PaymentCard.PaymentCardType", true));
            this.paymentCardTypeComboBox.FormattingEnabled = true;
            this.paymentCardTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "CreditCard",
            "DebitCard"});
            this.paymentCardTypeComboBox.Location = new System.Drawing.Point(155, 30);
            this.paymentCardTypeComboBox.Name = "paymentCardTypeComboBox";
            this.paymentCardTypeComboBox.Size = new System.Drawing.Size(200, 21);
            this.paymentCardTypeComboBox.TabIndex = 45;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(61, 470);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 53;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(121, 148);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(155, 119);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 40;
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(159, 135);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 51;
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // button2
            // 
            this.button2.CausesValidation = false;
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(142, 470);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 54;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pnlBank);
            this.groupBox1.Controls.Add(this.chkBank);
            this.groupBox1.Location = new System.Drawing.Point(281, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(394, 171);
            this.groupBox1.TabIndex = 55;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bank Account";
            // 
            // pnlBank
            // 
            this.pnlBank.Controls.Add(bankNameLabel);
            this.pnlBank.Controls.Add(this.bankNameTextBox);
            this.pnlBank.Controls.Add(this.policyPaymentTypeComboBox);
            this.pnlBank.Controls.Add(this.sortCodeTextBox);
            this.pnlBank.Controls.Add(accountNumberLabel);
            this.pnlBank.Controls.Add(sortCodeLabel);
            this.pnlBank.Controls.Add(this.accountNumberTextBox);
            this.pnlBank.Controls.Add(this.checkBox4);
            this.pnlBank.Controls.Add(datePaymentDetailsTakenLabel);
            this.pnlBank.Controls.Add(policyPaymentTypeLabel);
            this.pnlBank.Controls.Add(this.datePaymentDetailsTakenDateTimePicker);
            this.pnlBank.Enabled = false;
            this.pnlBank.Location = new System.Drawing.Point(21, 22);
            this.pnlBank.Name = "pnlBank";
            this.pnlBank.Size = new System.Drawing.Size(365, 143);
            this.pnlBank.TabIndex = 43;
            // 
            // chkBank
            // 
            this.chkBank.AutoSize = true;
            this.chkBank.Location = new System.Drawing.Point(6, 19);
            this.chkBank.Name = "chkBank";
            this.chkBank.Size = new System.Drawing.Size(15, 14);
            this.chkBank.TabIndex = 42;
            this.chkBank.UseVisualStyleBackColor = true;
            this.chkBank.CheckedChanged += new System.EventHandler(this.chkBank_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkCard);
            this.groupBox2.Controls.Add(this.pnlCard);
            this.groupBox2.Location = new System.Drawing.Point(281, 187);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(394, 180);
            this.groupBox2.TabIndex = 56;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Payment Card";
            // 
            // chkCard
            // 
            this.chkCard.AutoSize = true;
            this.chkCard.Location = new System.Drawing.Point(6, 15);
            this.chkCard.Name = "chkCard";
            this.chkCard.Size = new System.Drawing.Size(15, 14);
            this.chkCard.TabIndex = 44;
            this.chkCard.UseVisualStyleBackColor = true;
            this.chkCard.CheckedChanged += new System.EventHandler(this.chkCard_CheckedChanged);
            // 
            // pnlCard
            // 
            this.pnlCard.Controls.Add(this.textBox10);
            this.pnlCard.Controls.Add(bankLabel);
            this.pnlCard.Controls.Add(label13);
            this.pnlCard.Controls.Add(this.paymentCardTypeComboBox);
            this.pnlCard.Controls.Add(label12);
            this.pnlCard.Controls.Add(datePaymentDetailsTakenLabel1);
            this.pnlCard.Controls.Add(this.comboBox1);
            this.pnlCard.Controls.Add(this.datePaymentDetailsTakenDateTimePicker1);
            this.pnlCard.Controls.Add(this.bankTextBox);
            this.pnlCard.Controls.Add(paymentCardNumberLabel);
            this.pnlCard.Controls.Add(this.sortCodeTextBox1);
            this.pnlCard.Controls.Add(this.paymentCardNumberTextBox);
            this.pnlCard.Controls.Add(sortCodeLabel1);
            this.pnlCard.Controls.Add(paymentCardTypeLabel);
            this.pnlCard.Controls.Add(this.checkBox5);
            this.pnlCard.Enabled = false;
            this.pnlCard.Location = new System.Drawing.Point(21, 21);
            this.pnlCard.Name = "pnlCard";
            this.pnlCard.Size = new System.Drawing.Size(368, 156);
            this.pnlCard.TabIndex = 68;
            // 
            // textBox10
            // 
            this.textBox10.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "PaymentCard.ExpiryDate", true));
            this.textBox10.Location = new System.Drawing.Point(311, 105);
            this.textBox10.MaxLength = 4;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(44, 20);
            this.textBox10.TabIndex = 56;
            // 
            // comboBox1
            // 
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "PaymentCard.PolicyPaymentType", true));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Unknown",
            "Deposit",
            "DirectDebit",
            "SinglePayment"});
            this.comboBox1.Location = new System.Drawing.Point(155, 81);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(200, 21);
            this.comboBox1.TabIndex = 54;
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "EmailAddress", true));
            this.textBox1.Location = new System.Drawing.Point(121, 275);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(146, 20);
            this.textBox1.TabIndex = 58;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox5);
            this.groupBox3.Controls.Add(label6);
            this.groupBox3.Controls.Add(this.textBox4);
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Controls.Add(label4);
            this.groupBox3.Controls.Add(label3);
            this.groupBox3.Controls.Add(label2);
            this.groupBox3.Location = new System.Drawing.Point(282, 373);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(393, 120);
            this.groupBox3.TabIndex = 59;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Phone";
            // 
            // textBox5
            // 
            this.textBox5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "OtherTelephone", true));
            this.textBox5.Location = new System.Drawing.Point(170, 88);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(200, 20);
            this.textBox5.TabIndex = 66;
            // 
            // textBox4
            // 
            this.textBox4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "MobileTelephone", true));
            this.textBox4.Location = new System.Drawing.Point(170, 66);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(200, 20);
            this.textBox4.TabIndex = 64;
            // 
            // textBox3
            // 
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "WorkTelephone", true));
            this.textBox3.Location = new System.Drawing.Point(170, 42);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(200, 20);
            this.textBox3.TabIndex = 63;
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "LandlineTelephone", true));
            this.textBox2.Location = new System.Drawing.Point(170, 18);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(200, 20);
            this.textBox2.TabIndex = 60;
            // 
            // textBox6
            // 
            this.textBox6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "NINumber", true));
            this.textBox6.Location = new System.Drawing.Point(121, 300);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(146, 20);
            this.textBox6.TabIndex = 62;
            // 
            // textBox7
            // 
            this.textBox7.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "DrivingLicenseNumber", true));
            this.textBox7.Location = new System.Drawing.Point(121, 326);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(146, 20);
            this.textBox7.TabIndex = 64;
            // 
            // textBox8
            // 
            this.textBox8.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "PassportNumber", true));
            this.textBox8.Location = new System.Drawing.Point(121, 349);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(146, 20);
            this.textBox8.TabIndex = 67;
            // 
            // checkBox2
            // 
            this.checkBox2.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.peopleBindingSource, "AttendedHospital", true));
            this.checkBox2.Location = new System.Drawing.Point(121, 377);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(24, 24);
            this.checkBox2.TabIndex = 69;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // comboBox2
            // 
            this.comboBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "MojStatus", true));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Unknown",
            "Stage1",
            "Stage2",
            "Stage3",
            "OutOfProcess"});
            this.comboBox2.Location = new System.Drawing.Point(121, 405);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(146, 21);
            this.comboBox2.TabIndex = 71;
            // 
            // comboBox3
            // 
            this.comboBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.peopleBindingSource, "Operation", true));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Normal",
            "Delete",
            "Withdraw"});
            this.comboBox3.Location = new System.Drawing.Point(121, 438);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(144, 21);
            this.comboBox3.TabIndex = 73;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(12, 441);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(56, 13);
            label11.TabIndex = 72;
            label11.Text = "Operation:";
            // 
            // PeopleEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 505);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(label11);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(label9);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(label5);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(label10);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(label8);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(label7);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.subPartyTypeComboBox);
            this.Controls.Add(this.partyTypeComboBox);
            this.Controls.Add(this.nationalityComboBox);
            this.Controls.Add(this.genderComboBox);
            this.Controls.Add(this.salutationComboBox);
            this.Controls.Add(dateOfBirthLabel);
            this.Controls.Add(this.dateOfBirthDateTimePicker);
            this.Controls.Add(firstNameLabel);
            this.Controls.Add(this.firstNameTextBox);
            this.Controls.Add(genderLabel);
            this.Controls.Add(lastNameLabel);
            this.Controls.Add(this.lastNameTextBox);
            this.Controls.Add(middleNameLabel);
            this.Controls.Add(this.middleNameTextBox);
            this.Controls.Add(nationalityLabel);
            this.Controls.Add(occupationLabel);
            this.Controls.Add(this.occupationTextBox);
            this.Controls.Add(partyTypeLabel);
            this.Controls.Add(salutationLabel);
            this.Controls.Add(subPartyTypeLabel);
            this.Name = "PeopleEdit";
            this.Text = "Edit People Details";
            ((System.ComponentModel.ISupportInitialize)(this.peopleBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlBank.ResumeLayout(false);
            this.pnlBank.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pnlCard.ResumeLayout(false);
            this.pnlCard.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateOfBirthDateTimePicker;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox middleNameTextBox;
        private System.Windows.Forms.TextBox occupationTextBox;
        private System.Windows.Forms.TextBox accountNumberTextBox;
        private System.Windows.Forms.TextBox bankNameTextBox;
        private System.Windows.Forms.DateTimePicker datePaymentDetailsTakenDateTimePicker;
        private System.Windows.Forms.TextBox sortCodeTextBox;
        private System.Windows.Forms.TextBox bankTextBox;
        private System.Windows.Forms.DateTimePicker datePaymentDetailsTakenDateTimePicker1;
        private System.Windows.Forms.TextBox paymentCardNumberTextBox;
        private System.Windows.Forms.TextBox sortCodeTextBox1;
        private System.Windows.Forms.ComboBox salutationComboBox;
        private System.Windows.Forms.ComboBox genderComboBox;
        private System.Windows.Forms.ComboBox nationalityComboBox;
        private System.Windows.Forms.ComboBox partyTypeComboBox;
        private System.Windows.Forms.ComboBox subPartyTypeComboBox;
        private System.Windows.Forms.ComboBox policyPaymentTypeComboBox;
        private System.Windows.Forms.ComboBox paymentCardTypeComboBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.BindingSource peopleBindingSource;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Panel pnlBank;
        private System.Windows.Forms.CheckBox chkBank;
        private System.Windows.Forms.CheckBox chkCard;
        private System.Windows.Forms.Panel pnlCard;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox3;

    }
}