﻿using System;
using System.Windows.Forms;
//using MDA.WCF.WebServices.Messages;
//using MDA.RiskService.Model;
using MDA.FileEditor.ADAWebServiceReference;
using MDA.Common;

namespace MDA.FileEditor
{
    public partial class ClientInterface : Form
    {
        public ClientInterface()
        {
            InitializeComponent();

            ADAWebServiceReference.ADAWebServicesClient p = new ADAWebServiceReference.ADAWebServicesClient();

            var batch = p.GetClientBatchList( new BatchListRequest() { ClientId = 1, StatusFilter = 1 } );

            batchListBindingSource.DataSource = batch.BatchList;
        }

        public ClientInterface(int clientId)
        {
            InitializeComponent();

            ADAWebServiceReference.ADAWebServicesClient p = new ADAWebServiceReference.ADAWebServicesClient();

            var batch = p.GetClientBatchList(new BatchListRequest() { ClientId = clientId, StatusFilter = 1 });

            batchListBindingSource.DataSource = batch.BatchList;
        }

        private void batchListBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            ADAWebServiceReference.ADAWebServicesClient p = new ADAWebServiceReference.ADAWebServicesClient();

            ExBatchDetails bd = (ExBatchDetails)batchListBindingSource.Current;

            var claims = p.GetClaimsInBatch(new ClaimsInBatchRequest() {  BatchId = bd.BatchId });

            claimsListBindingSource.DataSource = claims.ClaimsList;

        }

        private TreeNode AssignMyNodeToTreeNode( MessageNode root )
        {
            TreeNode node = new TreeNode(root.Text);

            foreach (MessageNode n in root.Nodes)
                node.Nodes.Add(AssignMyNodeToTreeNode(n));

            return node;
        }

        private void claimsListBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            ADAWebServiceReference.ADAWebServicesClient p = new ADAWebServiceReference.ADAWebServicesClient();

            var cd = (ExRiskClaim)claimsListBindingSource.Current;

            if (cd != null)
            {
                var aa = p.GetClaimScoreData(new GetClaimScoreDataRequest() { RiskClaimId = cd.Id });

                propertyGrid1.SelectedObject = aa.RiskClaimRun;

                //richTextBox1.Text = aa.RiskClaimRun.ScoreEntityXml;

                treeView1.Nodes.Clear();

                if (aa.RiskClaimRun != null)
                {
                    ExRiskClaimRun rcr = aa.RiskClaimRun;

                    treeView1.Nodes.Add(AssignMyNodeToTreeNode(aa.ScoreNodes));

                    treeView1.Nodes[0].Expand();
                }
            }
            else
            {
                propertyGrid1.SelectedObject = null;

                //richTextBox1.Text = "";

                treeView1.Nodes.Clear();
            }

        }
    }
}
