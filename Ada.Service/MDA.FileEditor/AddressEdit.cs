﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MDA.Common.FileModel;

namespace MDA.FileEditor
{
    public partial class AddressEdit : Form
    {
        public AddressEdit()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Address a = (Address)addressesBindingSource.Current;

            if (!checkBox1.Checked) a.StartOfResidency = null;
            if (!checkBox2.Checked) a.EndOfResidency = null;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Address a = (Address)addressesBindingSource.Current;

            startOfResidencyDateTimePicker.Enabled = checkBox1.Checked;
            if (checkBox1.Checked && a.StartOfResidency == null)
                a.StartOfResidency = DateTime.Now;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            Address a = (Address)addressesBindingSource.Current;

            endOfResidencyDateTimePicker.Enabled = checkBox2.Checked;
            if (checkBox2.Checked && a.EndOfResidency == null)
                a.EndOfResidency = DateTime.Now;
        }

        private void addressesBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            Address a = (Address)addressesBindingSource.Current;

            checkBox1.Checked = a.StartOfResidency != null;
            checkBox2.Checked = a.EndOfResidency != null;
        }
    }
}
