﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MDA.Common.FileModel;

namespace MDA.FileEditor
{
    public partial class IncidentEdit : Form
    {
        public IncidentEdit()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool ok = true;

            if (claimNumberTextBox.Text.Length == 0)
            {
                MessageBox.Show("Must provide claim number");
                ok = false;
            }

            if (incidentDateDateTimePicker.Value == null)
            {
                MessageBox.Show("Must provide incident date");
                ok = false;
            }

            //if (policyNumberTextBox.Text.Length == 0)
            //{
            //    MessageBox.Show("Must provide policy number");
            //    ok = false;
            //}

            if ( ok )
            {
                MotorClaim v = (MotorClaim)incidentsBindingSource.Current;

                //if (!checkBox2.Checked) v.IncidentDate = null;
                if (!checkBox3.Checked) v.Policy.PolicyStartDate = null;
                if (!checkBox4.Checked) v.Policy.PolicyEndDate = null;

                //if (!checkBox2.Checked) v.MotorClaimInfo.IncidentTime = null;
                if (!checkBox5.Checked) v.MotorClaimInfo.ClaimNotificationDate = null;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        private void incidentsBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            MotorClaim v = (MotorClaim)incidentsBindingSource.Current;

            //checkBox2.Checked = v.IncidentDate != null;
            checkBox3.Checked = v.Policy.PolicyStartDate != null;
            checkBox4.Checked = v.Policy.PolicyEndDate != null;

            //checkBox2.Checked = v.MotorClaimInfo.IncidentTime != null;
            checkBox5.Checked = v.MotorClaimInfo.ClaimNotificationDate != null;
        }

        //private void checkBox2_CheckedChanged(object sender, EventArgs e)
        //{
        //    MotorClaim v = (MotorClaim)incidentsBindingSource.Current;

        //    incidentDateDateTimePicker.Enabled = checkBox2.Checked;
        //    if (checkBox2.Checked && v.IncidentDate == null)
        //        v.IncidentDate = DateTime.Now;
        //}

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            MotorClaim v = (MotorClaim)incidentsBindingSource.Current;

            policyStartDateDateTimePicker.Enabled = checkBox3.Checked;
            if (checkBox3.Checked && v.Policy.PolicyStartDate == null)
                v.Policy.PolicyStartDate = DateTime.Now;
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            MotorClaim v = (MotorClaim)incidentsBindingSource.Current;

            policyEndDateDateTimePicker.Enabled = checkBox4.Checked;
            if (checkBox4.Checked && v.Policy.PolicyEndDate == null)
                v.Policy.PolicyEndDate = DateTime.Now;
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            MotorClaim i = (MotorClaim)incidentsBindingSource.Current;

            ClaimInfo v = i.MotorClaimInfo;

            claimNotificationDateDateTimePicker.Enabled = checkBox5.Checked;
            if (checkBox5.Checked && v.ClaimNotificationDate == null)
                v.ClaimNotificationDate = DateTime.Now;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            //MotorClaim i = (MotorClaim)incidentsBindingSource.Current;

            //ClaimInfo v = i.MotorClaimInfo;

            //incidentDateDateTimePicker.Enabled = checkBox2.Checked;
            //if (checkBox2.Checked && v.IncidentTime == null)
            //    v.IncidentTime = DateTime.Now.TimeOfDay;
        }
    }
}
