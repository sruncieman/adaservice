﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.AddressService.Model;

namespace MDA.AddressService.Interface
{
    public interface IAddressService
    {
        Address CleanseAddress(Address address);
    }
}
