﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.AddressService.Model
{
    public class Address
    {
        public string SubBuilding { get; set; }
        public string BuildingNumber { get; set; }
        public string Building { get; set; }
        public string Street { get; set; }
        public string Locality { get; set; }
        public string County { get; set; }
        public string Town { get; set; }
        public string PostCode { get; set; }
        public string PafUPRN { get; set; }
        public int PafValidation { get; set; }
    }
}
