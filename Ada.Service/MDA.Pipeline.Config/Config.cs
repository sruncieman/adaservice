﻿using System.Configuration;

namespace MDA.Pipeline.Config
{
    public class PipelineConfig
    {
        private static PipelineConfiguration _config;

        public static PipelineConfiguration GetConfig
        {
            get
            {
                if (_config == null)
                    _config = ConfigurationManager.GetSection("pipelineConfiguration") as PipelineConfiguration;

                return _config;
            }
        }
    }
}
