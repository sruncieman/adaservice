﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="d0ed9acb-0435-4532-afdd-b5115bc4d562" namespace="MDA.Pipeline.Config" xmlSchemaNamespace="MDA.Pipeline.Config" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
  </typeDefinitions>
  <configurationElements>
    <configurationSection name="PipelineConfiguration" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="pipelineConfiguration">
      <elementProperties>
        <elementProperty name="PolicyPipelines" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="policyPipelines" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/PolicyPipelines" />
          </type>
        </elementProperty>
        <elementProperty name="PipelineAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="pipelineAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="TraceScore" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="traceScore" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/BoolValue" />
          </type>
        </elementProperty>
        <elementProperty name="TraceScoring" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="traceScoring" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/BoolValue" />
          </type>
        </elementProperty>
        <elementProperty name="TraceLoading" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="traceLoading" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/BoolValue" />
          </type>
        </elementProperty>
        <elementProperty name="ScoreDirectFromPipeline" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="scoreDirectFromPipeline" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/BoolValue" />
          </type>
        </elementProperty>
        <elementProperty name="SubmitDirectFromPipeline" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="submitDirectFromPipeline" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/BoolValue" />
          </type>
        </elementProperty>
        <elementProperty name="ScoreClaimsWithLiveRules" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="scoreClaimsWithLiveRules" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/BoolValue" />
          </type>
        </elementProperty>
        <elementProperty name="DbConnections" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="dbConnections" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/DbConnections" />
          </type>
        </elementProperty>
        <elementProperty name="TraceDeDupe" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="traceDeDupe" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/BoolValue" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElement name="AssemblyDetails">
      <attributeProperties>
        <attributeProperty name="TypeName" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="typeName" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElement name="ClientModule">
      <attributeProperties>
        <attributeProperty name="ClientId" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="clientId" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="LoadAllBatchesBeforeScoring" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="loadAllBatchesBeforeScoring" isReadOnly="false" defaultValue="&quot;false&quot;">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Boolean" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <elementProperties>
        <elementProperty name="VerificationAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="verificationAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="RiskAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="riskAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="MappingAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="mappingAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="MatchingAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="matchingAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="ValidationAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="validationAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="CleansingAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="cleansingAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="ClaimAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="claimAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="AddressAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="addressAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="MonitoredFolder" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="monitoredFolder" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/MonitoredFolder" />
          </type>
        </elementProperty>
        <elementProperty name="ExternalServices" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="externalServices" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/ExternalServices" />
          </type>
        </elementProperty>
        <elementProperty name="ClientAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="clientAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationElement>
    <configurationElementCollection name="PolicyPipelines" xmlItemName="policyType" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/PolicyPipeline" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="PolicyPipeline">
      <attributeProperties>
        <attributeProperty name="PolicyType" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="policyType" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <elementProperties>
        <elementProperty name="ClientModules" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="customClientModules" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/PolicyClients" />
          </type>
        </elementProperty>
        <elementProperty name="DefaultClientModules" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="defaultClientModules" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/DefaultClientModule" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationElement>
    <configurationElementCollection name="PolicyClients" xmlItemName="clientModule" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/ClientModule" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="BoolValue">
      <attributeProperties>
        <attributeProperty name="Value" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="value" isReadOnly="false" defaultValue="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Boolean" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElement name="IntValue">
      <attributeProperties>
        <attributeProperty name="Value" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="value" isReadOnly="false" defaultValue="0">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Int32" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElement name="DefaultClientModule">
      <elementProperties>
        <elementProperty name="VerificationAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="verificationAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="RiskAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="riskAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="MappingAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="mappingAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="MatchingAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="matchingAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="ValidationAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="validationAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="CleansingAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="cleansingAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="ClaimAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="claimAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="AddressAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="addressAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
        <elementProperty name="ExternalServices" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="externalServices" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/ExternalServices" />
          </type>
        </elementProperty>
        <elementProperty name="ClientAssembly" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="clientAssembly" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/AssemblyDetails" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationElement>
    <configurationElement name="DatabaseContext">
      <attributeProperties>
        <attributeProperty name="ConnectionString" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="connectionString" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="ProviderName" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="providerName" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="ClientId" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="clientId" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Int32" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElement name="MonitoredFolder">
      <attributeProperties>
        <attributeProperty name="SourceFolder" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="sourceFolder" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Wildcard" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="wildcard" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="PostOp" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="postOp" isReadOnly="false" defaultValue="&quot;Rename&quot;">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="ArchiveFolder" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="archiveFolder" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="BatchSize" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="batchSize" isReadOnly="false" defaultValue="500">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="SettleTime" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="settleTime" isReadOnly="false" defaultValue="2">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Int32" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElementCollection name="ExternalServices" xmlItemName="externalService" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/ExternalService" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="ExternalService">
      <attributeProperties>
        <attributeProperty name="Id" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="id" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="TypeName" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="typeName" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Name" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="name" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="ClientId" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="clientId" isReadOnly="false" defaultValue="0">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="SubmitAsBatch" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="submitAsBatch" isReadOnly="false" defaultValue="(bool)false">
          <type>
            <externalTypeMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/Boolean" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElementCollection name="DbConnections" xmlItemName="databaseContext" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementMoniker name="/d0ed9acb-0435-4532-afdd-b5115bc4d562/DatabaseContext" />
      </itemType>
    </configurationElementCollection>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>