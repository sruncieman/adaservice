﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Common.Debug;
using MDA.Common.Enum;
using MDA.DataService;
using MDA.WCF.WebServices;
using MDA.WCF.WebServices.Messages;
using MDA.Common.FileModel;

namespace RiskEngine.UnitTest
{
    public class Common
    {
        public const string PathToXmlFiles = @"..\..\..\RiskEngine.UnitTest\XMLInputFiles\";

        public static MotorClaimBatch SubmitFile(string filePath)
        {
            MDA.WCF.WebServices.ADAWebServices proxy = new MDA.WCF.WebServices.ADAWebServices();

            string xx = Directory.GetCurrentDirectory();

            FileStream fs = File.OpenRead(PathToXmlFiles + filePath);

            fs.Seek(0, SeekOrigin.Begin);

            
            XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            DataContractSerializer ser = new DataContractSerializer(typeof(MotorClaimBatch));

            MotorClaimBatch batch = (MDA.Common.FileModel.MotorClaimBatch)ser.ReadObject(reader, true);


            var x = proxy.ProcessClaimBatch(new ProcessClaimBatchRequest() { ClientId = 1, UserId = 0, Batch = batch, BatchEntityType = "Motor" });

            return batch;
        }
    }
}
