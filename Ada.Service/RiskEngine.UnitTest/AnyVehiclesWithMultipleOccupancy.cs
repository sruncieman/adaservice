﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RiskEngine.Scoring.Entities;
using RiskEngine.Model;
using MDA.DAL;
using MDA.Common.FileModel;
using End2EndTests;
using System.Data.Entity;
using System.Linq;
using MDA.DataService;
using MDA.Common.Server;
using RiskEngine.Scoring.Model;


namespace RiskEngine.UnitTest
{
    [TestClass]
    public class AnyVehiclesWithMultipleOccupancy
    {
        private const int userId = 1;

        [TestMethod]
        public void AnyVehiclesWithMultipleOccupancyTest()
        {
            Test1Step1();
            Test1Step2();
            Test1Step3();
            Test2Step1();
            Test2Step2();
            Test2Step3();
            Test2Step4();
            Test3Step1();
            Test3Step3();
            Test3Step4();
        }


        [TestMethod]
        public void Test1Step1()
        {
            //  1 vehicle with 2 occupants, plus a witness for that vehicle
            MotorClaimBatch batch = Common.SubmitFile("Test1Step1.xml");

            CurrentContext ctx = new CurrentContext(3, userId, "Fred");

            FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(1);

            MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

            Assert.AreEqual<bool?>(true, incident.v_AnyVehiclesWithMultipleOccupancy("key",2));
        }

        [TestMethod]
        public void Test1Step2()
        {
            // separate incident where the incident involved 1 vehicle with 3 occupants
            MotorClaimBatch batch = Common.SubmitFile("Test1Step2.xml");

            CurrentContext ctx = new CurrentContext(3, userId, "Fred");

            FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(2);

            MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

            Assert.AreEqual<bool?>(true, incident.v_AnyVehiclesWithMultipleOccupancy("key", 2));
        }

        [TestMethod]
        public void Test1Step3()
        {
            // separate incident where the incident involved 1 vehicle with 4 occupants
            MotorClaimBatch batch = Common.SubmitFile("Test1Step3.xml");

            CurrentContext ctx = new CurrentContext(3, userId, "Fred");

            FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(3);

            MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

            Assert.AreEqual<bool?>(true, incident.v_AnyVehiclesWithMultipleOccupancy("key", 2));
        }

        [TestMethod]
        public void Test2Step1()
        {
            //  add second vehicle (vehicle 2) with 1 occupant, using the same claim ID
            MotorClaimBatch batch = Common.SubmitFile("Test2Step1.xml");

            CurrentContext ctx = new CurrentContext(3, userId, "Fred");

            FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(1);

            MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

            Assert.AreEqual<bool?>(true, incident.v_AnyVehiclesWithMultipleOccupancy("key", 2));
        }

        [TestMethod]
        public void Test2Step2()
        {
            // add another person to vehicle 2
            MotorClaimBatch batch = Common.SubmitFile("Test2Step2.xml");

            CurrentContext ctx = new CurrentContext(3, userId, "Fred");

            FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(1);

            MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

            Assert.AreEqual<bool?>(true, incident.v_AnyVehiclesWithMultipleOccupancy("key", 2));
        }

        [TestMethod]
        public void Test2Step3()
        {
            // add another person to vehicle 1
            MotorClaimBatch batch = Common.SubmitFile("Test2Step3.xml");

            CurrentContext ctx = new CurrentContext(3, userId, "Fred");

            FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(1);

            MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

            Assert.AreEqual<bool?>(true, incident.v_AnyVehiclesWithMultipleOccupancy("key", 2));
        }

        [TestMethod]
        public void Test2Step4()
        {
            // add another person to vehicle 2 
            MotorClaimBatch batch = Common.SubmitFile("Test2Step4.xml");

            CurrentContext ctx = new CurrentContext(3, userId, "Fred");

            FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(1);

            MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

            Assert.AreEqual<bool?>(true, incident.v_AnyVehiclesWithMultipleOccupancy("key", 2));
        }

        [TestMethod]
        public void Test3Step1()
        {
            //  new claim with 2 vehicles where at least 1 vehicle has 2 occupants, and no vehicles have more than 2 occupants
            MotorClaimBatch batch = Common.SubmitFile("Test3Step1.xml");

            CurrentContext ctx = new CurrentContext(3, userId, "Fred");

            FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(8);

            MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

            Assert.AreEqual<bool?>(true, incident.v_AnyVehiclesWithMultipleOccupancy("key", 2));
        }

        [TestMethod]
        public void Test3Step3()
        {
            // new claim with the same Incident Date and at least 2 matching vehicles (1 must be a vehicle that has two occupants), but a different claim number. Have 1 person match one of the people in the vehicle that originally had 2, in the same vehicle. 
            MotorClaimBatch batch = Common.SubmitFile("Test3Step2.xml");

            CurrentContext ctx = new CurrentContext(3, userId, "Fred");

            FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(9);

            MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

            Assert.AreEqual<bool?>(true, incident.v_AnyVehiclesWithMultipleOccupancy("key", 2));
        }

        [TestMethod]
        public void Test3Step4()
        {
            //  add a new person to the same vehicle
            MotorClaimBatch batch = Common.SubmitFile("Test3Step4.xml");

            CurrentContext ctx = new CurrentContext(3, userId, "Fred");

            FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(9);

            MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

            Assert.AreEqual<bool?>(true, incident.v_AnyVehiclesWithMultipleOccupancy("key", 2));
        }
    }
}
