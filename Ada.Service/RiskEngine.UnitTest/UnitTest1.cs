﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RiskEngine.Scoring.Entities;
using RiskEngine.Model;
using MDA.DAL;
using MDA.Common.FileModel;
using End2EndTests;
using System.Data.Entity;
using System.Linq;
using MDA.Common.Server;
using RiskEngine.Scoring.Model;
//using MDA.RiskService.DataService;


namespace RiskEngine.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        const int userId = 1;

        [TestMethod]
        public void DataPopulation()
        {
            using (CurrentContext ctx = new CurrentContext(3, userId, "Test"))
            {
                MotorClaimBatch batch = Common.SubmitFile("Claim 1-0.xml");

                PropertiesTestPopulation();
                PropertiesTest();
            }
        }


        [TestMethod]
        public void PropertiesTestPopulation()
        {
            using (CurrentContext ctx = new CurrentContext(3, userId, "Test"))
            {

                var people = from ord in ctx.db.People
                             where ord.Id == 1
                             select ord;

                foreach (var person in people)
                {
                    person.KeyAttractor = "FirstFirstName FirstLastName is a key attractor";
                }

                var emails = from ord in ctx.db.Emails
                             where ord.Id == 1
                             select ord;

                foreach (var email in emails)
                {
                    email.KeyAttractor = "111@firstemail.com is a key attractor";
                }

                try
                {
                    ctx.db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }


        [TestMethod]
        public void PropertiesTest()
        {
            using (CurrentContext ctx = new CurrentContext(3, userId, "Test"))
            {

                FullClaimToScore x = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(1);

                #region Policy Properties

                MotorClaimIncidentRisk incident = (MotorClaimIncidentRisk)x.RiskEntityMap;

                Assert.AreEqual<bool?>(true, incident.v_AnyVehiclesWithMultipleOccupancy("key", 0));
                Assert.AreEqual<int?>(1, incident.v_NumberOfClaimants);
                Assert.AreEqual<int?>(1, incident.v_AgeRangeCount("key", 0, 99));
                Assert.AreEqual<int?>(300, incident.v_NumberOfDaysSincePolicyEnd);
                Assert.AreEqual<int?>(64, incident.v_NumberOfDaysSincePolicyInception);
                Assert.AreEqual<bool>(false, incident.v_MultiplePoliciesFromSameAddress("key", 1));

                #endregion

                #region Vehicle Properties
                foreach (var vehicle in incident.IncidentVehicles)
                {
                    Assert.AreEqual<int?>(1, vehicle.v_OccupancyCount);

                #endregion

                    #region People Properties
                    foreach (var person in vehicle.VehiclePeople)
                    {
                        if (person.Db_Id == 1)
                        {

                            //Assert.AreEqual<int>(0, person.v_NumberOfIncidents("key", "confirmed", "ADA", "RTC", "0m", "12m"));
                            Assert.AreEqual<int>(0, person.v_NumberOfCases("key", "confirmed", "ADA", true, "0m", "12m"));
                            //Assert.AreEqual<int>(0, person.v_BankAccountAtMultipleAddressCount("key", "confirmed"));
                            Assert.AreEqual<int>(0, person.v_BankAccountUsageCount("key", "confirmed"));
                            Assert.AreEqual<int>(0, person.v_EmailUsageCount("key", "confirmed"));
                            Assert.AreEqual<int>(0, person.v_SettledCaseCount("key", "confirmed", 1, true, "0m", "12m"));
                            Assert.AreEqual<int>(0, person.v_LinkedSettledCaseCount("key", "confirmed", 1, null, true, "0m", "24m"));
                            Assert.AreEqual<bool?>(false, person.v_IsKeyAttractor("key", "confirmed"));
                            Assert.AreEqual<string>(null, person.v_KeyAttractor);
                            //Assert.AreEqual<bool?>(false, person.v_IsEmailKeyAttractor);
                            Assert.AreEqual<bool?>(false, person.v_IsAnyEmailKeyAttractor);
                            Assert.AreEqual<string>(null, person.v_EmailKeyAttractor);
                            Assert.AreEqual<string>("", person.v_AllEmailKeyAttractor);
                            //Assert.AreEqual<bool?>(false, person.v_IsBankAccountKeyAttractor);
                            Assert.AreEqual<bool?>(false, person.v_IsAnyBankAccountKeyAttractor);
                            Assert.AreEqual<string>("", person.v_BankAccountKeyAttractor);
                            Assert.AreEqual<string>(null, person.v_AllBankAccountKeyAttractor);
                            //Assert.AreEqual<bool?>(false, person.v_IsPaymentCardKeyAttractor);
                            Assert.AreEqual<bool?>(false, person.v_IsAnyPaymentCardKeyAttractor);
                            Assert.AreEqual<string>("", person.v_PaymentCardKeyAttractor);
                            Assert.AreEqual<string>(null, person.v_AllPaymentCardKeyAttractor);

                            //Assert.AreEqual<bool?>(false, person.v_IsTelephoneKeyAttractor("key", "Unknown"));
                            Assert.AreEqual<bool?>(false, person.v_IsAnyTelephoneKeyAttractor("key", "Unknown"));
                            Assert.AreEqual<string>("", person.v_TelephoneKeyAttractor("key", "Unknown"));
                            Assert.AreEqual<string>("", person.v_AllTelephoneKeyAttractor("key", "Unknown"));

                            //Assert.AreEqual<bool?>(false, person.v_IsTelephoneKeyAttractor("key", "Landline"));
                            Assert.AreEqual<bool?>(false, person.v_IsAnyTelephoneKeyAttractor("key", "Landline"));
                            Assert.AreEqual<string>("", person.v_TelephoneKeyAttractor("key", "Landline"));
                            Assert.AreEqual<string>("", person.v_AllTelephoneKeyAttractor("key", "Landline"));

                            //Assert.AreEqual<bool?>(false, person.v_IsTelephoneKeyAttractor("key", "Mobile"));
                            Assert.AreEqual<bool?>(false, person.v_IsAnyTelephoneKeyAttractor("key", "Mobile"));
                            Assert.AreEqual<string>("", person.v_TelephoneKeyAttractor("key", "Mobile"));
                            Assert.AreEqual<string>("", person.v_AllTelephoneKeyAttractor("key", "Mobile"));

                            //Assert.AreEqual<bool?>(false, person.v_IsTelephoneKeyAttractor("key", "FAX"));
                            Assert.AreEqual<bool?>(false, person.v_IsAnyTelephoneKeyAttractor("key", "FAX"));
                            Assert.AreEqual<string>("", person.v_TelephoneKeyAttractor("key", "FAX"));
                            Assert.AreEqual<string>("", person.v_AllTelephoneKeyAttractor("key", "FAX"));

                            //Assert.AreEqual<int>(0, person.v_TelephoneNumberUsageCount("key", "confirmed", "Mobile"));
                        }
                    #endregion

                        #region Address Properties
                        foreach (var address in person.PersonsAddresses)
                        {
                            //Assert.AreEqual<int>(0, address.v_NumberOfCases("key",true,"12m", "1m"));
                            Assert.AreEqual<bool?>(false, address.v_PafValidation);
                            Assert.AreEqual<string>("", address.v_PostCodeKeyAttractor);

                        }
                        #endregion

                        #region Organisation Properties
                        foreach (var organisation in person.PersonsOrganisations)
                        {
                            //Assert.AreEqual<bool>(false, organisation.v_IsKeyAttractor);
                        #endregion

                        }
                    }
                    #region Organistion Properties
                    foreach (var incOrg in incident.IncidentOrganisations)
                    {
                        //Assert.AreEqual<bool>(false, incOrg.v_IsKeyAttractor);

                    #endregion

                        #region Address Properties
                        foreach (var orgAddress in incOrg.OrganisationsAddresses)
                        {
                            //Assert.AreEqual<bool>(false, incOrg.v_IsKeyAttractor);
                        }
                        #endregion

                        #region Vehicle Properties
                        foreach (var orgVehicle in incOrg.OrganisationsVehicles)
                        {
                            //Assert.AreEqual<bool>(false, incOrg.v_IsKeyAttractor);
                        }
                        #endregion
                    }
                }
            }
        }
    }
}
