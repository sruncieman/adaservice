﻿CREATE TABLE [dbo].[_Configuration_Binary] (
    [Item]       NVARCHAR (255) NOT NULL,
    [BinaryData] IMAGE          NULL,
    CONSTRAINT [PK__Configuration_Binary] PRIMARY KEY NONCLUSTERED ([Item] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_Configuration_Binary_Item] FOREIGN KEY ([Item]) REFERENCES [dbo].[_Configuration_Def] ([Item])
);


GO
ALTER TABLE [dbo].[_Configuration_Binary] NOCHECK CONSTRAINT [FK_Configuration_Binary_Item];


GO
CREATE NONCLUSTERED INDEX [FK_Configuration_Binary_Item]
    ON [dbo].[_Configuration_Binary]([Item] ASC) WITH (FILLFACTOR = 80);

