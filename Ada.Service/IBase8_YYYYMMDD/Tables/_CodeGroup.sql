﻿CREATE TABLE [dbo].[_CodeGroup] (
    [CodeGroup_ID] INT            NOT NULL,
    [Description]  NVARCHAR (255) NOT NULL,
    [Notes]        NTEXT          NULL,
    [Parent_ID]    INT            NULL,
    [SortOrder]    TINYINT        NULL,
    [Type]         TINYINT        NULL,
    CONSTRAINT [PK__CodeGroup] PRIMARY KEY NONCLUSTERED ([CodeGroup_ID] ASC) WITH (FILLFACTOR = 80)
);

