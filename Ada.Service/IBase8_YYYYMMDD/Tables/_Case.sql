﻿CREATE TABLE [dbo].[_Case] (
    [CaseID]      NVARCHAR (50)  NOT NULL,
    [Case_Status] INT            NOT NULL,
    [Date_Closed] DATETIME       NULL,
    [Date_Open]   DATETIME       NULL,
    [Description] NTEXT          NULL,
    [Name]        NVARCHAR (255) NOT NULL,
    [Notes]       NTEXT          NULL,
    CONSTRAINT [PK__Case] PRIMARY KEY NONCLUSTERED ([CaseID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [Name]
    ON [dbo].[_Case]([Name] ASC) WITH (FILLFACTOR = 80);

