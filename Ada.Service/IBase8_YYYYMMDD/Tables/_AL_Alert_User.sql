﻿CREATE TABLE [dbo].[_AL_Alert_User] (
    [alert_id]  INT             NOT NULL,
    [user_name] NVARCHAR (255)  NOT NULL,
    [email]     NVARCHAR (2000) NULL,
    [UID]       NVARCHAR (50)   NOT NULL,
    CONSTRAINT [PK__AL_Alert_User] PRIMARY KEY CLUSTERED ([alert_id] ASC, [user_name] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [AL_Alert_User__AL_Alert_FK] FOREIGN KEY ([alert_id]) REFERENCES [dbo].[_AL_Alert] ([alert_id])
);


GO
ALTER TABLE [dbo].[_AL_Alert_User] NOCHECK CONSTRAINT [AL_Alert_User__AL_Alert_FK];


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_User]
    ON [dbo].[_AL_Alert_User]([user_name] ASC) WITH (FILLFACTOR = 80);

