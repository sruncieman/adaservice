﻿CREATE TABLE [dbo].[_FormField] (
    [Form_ID]    INT      NOT NULL,
    [Field_ID]   INT      NOT NULL,
    [Column_No]  TINYINT  NULL,
    [FieldIndex] SMALLINT NOT NULL,
    [Page_No]    TINYINT  NULL,
    CONSTRAINT [PK__FormField] PRIMARY KEY NONCLUSTERED ([Form_ID] ASC, [Field_ID] ASC) WITH (FILLFACTOR = 80)
);

