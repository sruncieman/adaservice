﻿CREATE TABLE [dbo].[_AL_Alert_Change_Keys] (
    [alert_id]  INT            NOT NULL,
    [Unique_ID] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK__AL_Alert_Change_Keys] PRIMARY KEY CLUSTERED ([alert_id] ASC, [Unique_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK__AL_Alert_Change_Keys__AL_Alert] FOREIGN KEY ([alert_id]) REFERENCES [dbo].[_AL_Alert] ([alert_id])
);

