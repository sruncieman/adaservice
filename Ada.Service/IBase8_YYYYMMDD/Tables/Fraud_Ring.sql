﻿CREATE TABLE [dbo].[Fraud_Ring] (
    [Unique_ID]                    NVARCHAR (50)  NOT NULL,
    [AltEntity]                    NVARCHAR (50)  NULL,
    [Briefing_Document]            NVARCHAR (MAX) NULL,
    [Champion_]                    NVARCHAR (255) NULL,
    [Create_Date]                  DATETIME       NOT NULL,
    [Create_User]                  NVARCHAR (255) NOT NULL,
    [Do_Not_Disseminate_412284494] SMALLINT       NOT NULL,
    [Fraud_Ring_ID]                NVARCHAR (50)  NULL,
    [Fraud_Ring_Name]              NVARCHAR (255) NULL,
    [IconColour]                   INT            NULL,
    [Key_Attractor_412284410]      NVARCHAR (100) NULL,
    [Last_Upd_Date]                DATETIME       NULL,
    [Last_Upd_User]                NVARCHAR (255) NULL,
    [Lead_Analyst]                 NVARCHAR (255) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)  NULL,
    [Record_Status]                TINYINT        DEFAULT ((0)) NULL,
    [SCC]                          NVARCHAR (255) DEFAULT ('') NULL,
    [Source_411765484]             NVARCHAR (50)  NULL,
    [Source_Description_411765489] NVARCHAR (MAX) NULL,
    [Source_Reference_411765487]   NVARCHAR (50)  NULL,
    [Status_]                      NVARCHAR (255) NULL,
    [Status_Binding]               NVARCHAR (50)  DEFAULT ('') NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255) NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)  NULL,
    [MO_]                          NVARCHAR (255) NULL,
    [Owning_Insurer]               NVARCHAR (255) NULL,
    [Closed_Date]                  DATETIME       NULL,
    [Industry_Alternative_Names]   NVARCHAR (100) NULL,
    CONSTRAINT [PK_Fraud_Ring] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Fraud_Ring_Name]
    ON [dbo].[Fraud_Ring]([Fraud_Ring_Name] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);

