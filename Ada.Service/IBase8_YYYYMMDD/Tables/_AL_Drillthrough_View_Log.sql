﻿CREATE TABLE [dbo].[_AL_Drillthrough_View_Log] (
    [ID]          INT            IDENTITY (1, 1) NOT NULL,
    [Unique_ID]   NVARCHAR (255) NOT NULL,
    [date_viewed] DATETIME       CONSTRAINT [DF__AL_Drillthrough_View_Log_date_viewed] DEFAULT (getdate()) NOT NULL,
    [user_name]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK__AL_Drillthrough_View_Log] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

