﻿CREATE TABLE [dbo].[_AuditContext] (
    [Session_ID]      INT              NOT NULL,
    [ApplicationUser] NVARCHAR (255)   NOT NULL,
    [Reason]          NVARCHAR (255)   NULL,
    [MachineName]     NVARCHAR (255)   NOT NULL,
    [OS_User_Name]    NVARCHAR (255)   NOT NULL,
    [Location]        NVARCHAR (50)    NULL,
    [GIDS]            NVARCHAR (MAX)   NULL,
    [Created_On]      DATETIME         NULL,
    [Restricted]      NVARCHAR (MAX)   NULL,
    [PersistReason]   BIT              NULL,
    [LinkAuditID]     UNIQUEIDENTIFIER NULL,
    [BinaryFlag]      INT              NULL,
    CONSTRAINT [PK_AuditContext_1] PRIMARY KEY CLUSTERED ([Session_ID] ASC)
);

