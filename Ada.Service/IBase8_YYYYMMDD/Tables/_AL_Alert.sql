﻿CREATE TABLE [dbo].[_AL_Alert] (
    [alert_id]          INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Create_User]       NVARCHAR (255) NOT NULL,
    [Create_Date]       DATETIME       NOT NULL,
    [Last_Upd_Date]     DATETIME       NULL,
    [Last_Upd_User]     NVARCHAR (255) NULL,
    [alert_name]        NVARCHAR (255) NOT NULL,
    [alert_description] NVARCHAR (255) NULL,
    [expiry_date]       DATETIME       NULL,
    [alert_type]        SMALLINT       NOT NULL,
    [detection_options] INT            NOT NULL,
    [alert_status]      INT            NOT NULL,
    [execution_time]    FLOAT (53)     NOT NULL,
    [queue_no]          TINYINT        NOT NULL,
    [CaseID]            NVARCHAR (50)  NULL,
    [UID]               NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK__AL_Alert] PRIMARY KEY CLUSTERED ([alert_id] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert]
    ON [dbo].[_AL_Alert]([expiry_date] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_1]
    ON [dbo].[_AL_Alert]([alert_status] ASC) WITH (FILLFACTOR = 80);

