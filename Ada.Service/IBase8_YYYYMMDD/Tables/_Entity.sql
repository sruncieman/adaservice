﻿CREATE TABLE [dbo].[_Entity] (
    [Table_ID]    INT            NOT NULL,
    [ChartEntity] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK__Entity] PRIMARY KEY NONCLUSTERED ([Table_ID] ASC) WITH (FILLFACTOR = 80)
);

