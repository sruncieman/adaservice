﻿CREATE TABLE [dbo].[Incident_] (
    [Unique_ID]                    NVARCHAR (50)  NOT NULL,
    [AltEntity]                    NVARCHAR (50)  NULL,
    [Claim_Type]                   NVARCHAR (255) NULL,
    [Create_Date]                  DATETIME       NOT NULL,
    [Create_User]                  NVARCHAR (255) NOT NULL,
    [Do_Not_Disseminate_412284494] SMALLINT       NOT NULL,
    [Fraud_Ring_Name]              NVARCHAR (255) NULL,
    [IconColour]                   INT            NULL,
    [IFB_Reference]                NVARCHAR (50)  NULL,
    [Incident_Date]                DATETIME       NULL,
    [Incident_ID]                  NVARCHAR (50)  NULL,
    [Incident_Type]                NVARCHAR (255) NULL,
    [Keoghs_Elite_Reference]       NVARCHAR (50)  NULL,
    [Key_Attractor_412284410]      NVARCHAR (100) NULL,
    [Last_Upd_Date]                DATETIME       NULL,
    [Last_Upd_User]                NVARCHAR (255) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)  NULL,
    [Record_Status]                TINYINT        DEFAULT ((0)) NULL,
    [SCC]                          NVARCHAR (255) DEFAULT ('') NULL,
    [Source_411765484]             NVARCHAR (50)  NULL,
    [Source_Description_411765489] NVARCHAR (MAX) NULL,
    [Source_Reference_411765487]   NVARCHAR (50)  NULL,
    [Status_Binding]               NVARCHAR (50)  DEFAULT ('') NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255) NULL,
    [Reserve_]                     MONEY          NULL,
    [Payments_]                    MONEY          NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Incident_] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Fraud_Ring_Name]
    ON [dbo].[Incident_]([Fraud_Ring_Name] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Incident_Date]
    ON [dbo].[Incident_]([Incident_Date] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Incident_Type]
    ON [dbo].[Incident_]([Incident_Type] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Keoghs_Elite_Reference]
    ON [dbo].[Incident_]([Keoghs_Elite_Reference] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);

