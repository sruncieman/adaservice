﻿CREATE TABLE [dbo].[_AL_Alert_Notification] (
    [alert_id]           INT              NOT NULL,
    [delivery_method_id] INT              NOT NULL,
    [user_name]          NVARCHAR (255)   NOT NULL,
    [raised_on]          DATETIME         NOT NULL,
    [read_on]            DATETIME         NULL,
    [Notification_ID]    INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Batch_ID]           UNIQUEIDENTIFIER NULL,
    [PrevSnapshotDate]   DATETIME         NULL,
    [Deleted]            BIT              CONSTRAINT [DF__AL_Alert_Notification_Deleted] DEFAULT ((0)) NOT NULL,
    [FollowUp]           BIT              CONSTRAINT [DF__AL_Alert_Notification_FollowUp] DEFAULT ((0)) NOT NULL,
    [AlertRead]          BIT              CONSTRAINT [DF__AL_Alert_Notification_AlertRead] DEFAULT ((0)) NOT NULL,
    [Status]             INT              CONSTRAINT [DF__AL_Alert_Notification_Status] DEFAULT ((0)) NOT NULL,
    [IsMessage]          BIT              NULL,
    [Title]              NVARCHAR (255)   NULL,
    [Message]            NVARCHAR (1000)  NULL,
    [UID]                NVARCHAR (50)    NULL,
    CONSTRAINT [PK__AL_Alert_Notification] PRIMARY KEY CLUSTERED ([Notification_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK__AL_Alert_Notification__AL_Alert] FOREIGN KEY ([alert_id]) REFERENCES [dbo].[_AL_Alert] ([alert_id])
);


GO
CREATE NONCLUSTERED INDEX [AL_AN_Not1]
    ON [dbo].[_AL_Alert_Notification]([user_name] ASC)
    INCLUDE([AlertRead], [Deleted]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [AL_AN_Not2]
    ON [dbo].[_AL_Alert_Notification]([user_name] ASC, [raised_on] ASC, [Notification_ID] ASC)
    INCLUDE([AlertRead], [Deleted], [delivery_method_id]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [AL_AN_Not3]
    ON [dbo].[_AL_Alert_Notification]([user_name] ASC, [raised_on] ASC, [Notification_ID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_Notification]
    ON [dbo].[_AL_Alert_Notification]([alert_id] ASC) WITH (FILLFACTOR = 80);

