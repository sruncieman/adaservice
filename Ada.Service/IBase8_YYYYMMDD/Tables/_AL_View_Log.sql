﻿CREATE TABLE [dbo].[_AL_View_Log] (
    [Unique_ID]    NVARCHAR (255) NOT NULL,
    [date_viewed]  DATETIME       CONSTRAINT [DF__AL_View_Log_DateViewed] DEFAULT (getdate()) NOT NULL,
    [user_name]    NVARCHAR (255) NOT NULL,
    [machine_Name] NVARCHAR (255) NULL,
    [OS_user_name] NVARCHAR (255) NULL,
    [View_Log_ID]  INT            IDENTITY (1, 1) NOT NULL,
    [AccessDenied] BIT            NOT NULL,
    CONSTRAINT [PK__AL_View_Log] PRIMARY KEY CLUSTERED ([View_Log_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [_AL_VL_Date_Viewed]
    ON [dbo].[_AL_View_Log]([date_viewed] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_View_Log]
    ON [dbo].[_AL_View_Log]([Unique_ID] ASC) WITH (FILLFACTOR = 80);

