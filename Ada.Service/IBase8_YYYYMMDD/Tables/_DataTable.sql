﻿CREATE TABLE [dbo].[_DataTable] (
    [Table_ID]     INT            NOT NULL,
    [Description]  NVARCHAR (255) NULL,
    [Fixed]        SMALLINT       NOT NULL,
    [InExpandList] SMALLINT       NOT NULL,
    [LogicalName]  NVARCHAR (255) NOT NULL,
    [PhysicalName] NVARCHAR (255) NOT NULL,
    [TableCode]    NVARCHAR (3)   NOT NULL,
    [Type]         TINYINT        NOT NULL,
    CONSTRAINT [PK__DataTable] PRIMARY KEY NONCLUSTERED ([Table_ID] ASC) WITH (FILLFACTOR = 80)
);

