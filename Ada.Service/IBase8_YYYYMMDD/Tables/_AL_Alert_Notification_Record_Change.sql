﻿CREATE TABLE [dbo].[_AL_Alert_Notification_Record_Change] (
    [ID]                   INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Notification_ID]      INT              NULL,
    [alert_id]             INT              NOT NULL,
    [Unique_ID]            NVARCHAR (255)   NOT NULL,
    [date_changed]         DATETIME         NOT NULL,
    [edit_user_name]       NVARCHAR (255)   NOT NULL,
    [Database_Action_Type] TINYINT          NOT NULL,
    [Audit_ID]             UNIQUEIDENTIFIER NULL,
    [Record_Status]        TINYINT          NULL,
    [user_name]            NVARCHAR (255)   NOT NULL,
    [SCC_New]              NVARCHAR (255)   NULL,
    [SCC_Old]              NVARCHAR (255)   NULL,
    [parent_unique_id]     NVARCHAR (255)   NULL,
    [IsLink]               BIT              NULL,
    [Edit]                 INT              NULL,
    [New]                  INT              NULL,
    [Deleted]              INT              NULL,
    [Batch_ID]             UNIQUEIDENTIFIER NULL,
    [SoftDeleted]          BIT              NULL,
    [Restored]             BIT              NULL,
    [SCC_Deleted]          BIT              NULL,
    [SCC_Created]          BIT              NULL,
    CONSTRAINT [PK__AL_Alert_Notification_Record_Change] PRIMARY KEY NONCLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK__AL_Alert_Notification_Record_Change__AL_Alert] FOREIGN KEY ([alert_id]) REFERENCES [dbo].[_AL_Alert] ([alert_id]),
    CONSTRAINT [FK__AL_Alert_Notification_Record_Change__AL_Alert_Notification] FOREIGN KEY ([Notification_ID]) REFERENCES [dbo].[_AL_Alert_Notification] ([Notification_ID])
);


GO
CREATE CLUSTERED INDEX [IX__AL_Alert_Notification_Record_Change]
    ON [dbo].[_AL_Alert_Notification_Record_Change]([Notification_ID] ASC, [parent_unique_id] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_Notification_Record_Change_1]
    ON [dbo].[_AL_Alert_Notification_Record_Change]([alert_id] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_Notification_Record_Change_2]
    ON [dbo].[_AL_Alert_Notification_Record_Change]([Unique_ID] ASC) WITH (FILLFACTOR = 80);

