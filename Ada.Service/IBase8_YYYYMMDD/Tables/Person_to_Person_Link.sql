﻿CREATE TABLE [dbo].[Person_to_Person_Link] (
    [Unique_ID]                    NVARCHAR (50)  NOT NULL,
    [AltEntity]                    NVARCHAR (50)  NULL,
    [Create_Date]                  DATETIME       NOT NULL,
    [Create_User]                  NVARCHAR (255) NOT NULL,
    [Do_Not_Disseminate_412284494] SMALLINT       NOT NULL,
    [IconColour]                   INT            NULL,
    [Key_Attractor_412284410]      NVARCHAR (100) NULL,
    [Last_Upd_Date]                DATETIME       NULL,
    [Last_Upd_User]                NVARCHAR (255) NULL,
    [Link_Type]                    NVARCHAR (255) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)  NULL,
    [Person_to_Person_Link_ID]     NVARCHAR (50)  NULL,
    [Record_Status]                TINYINT        DEFAULT ((0)) NULL,
    [SCC]                          NVARCHAR (255) DEFAULT ('') NULL,
    [Source_411765484]             NVARCHAR (50)  NULL,
    [Source_Description_411765489] NVARCHAR (MAX) NULL,
    [Source_Reference_411765487]   NVARCHAR (50)  NULL,
    [Status_Binding]               NVARCHAR (50)  DEFAULT ('') NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255) NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Person_to_Person_Link] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);

