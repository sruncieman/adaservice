﻿CREATE TABLE [dbo].[_AL_Alert_Removed_Keys] (
    [ID]        INT            IDENTITY (1, 1) NOT NULL,
    [alert_id]  INT            NOT NULL,
    [Unique_ID] NVARCHAR (255) NOT NULL,
    [QueueNo]   INT            NOT NULL,
    CONSTRAINT [PK__AL_Alert_Removed_Keys] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

