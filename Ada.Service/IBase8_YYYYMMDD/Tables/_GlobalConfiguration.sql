﻿CREATE TABLE [dbo].[_GlobalConfiguration] (
    [Item] NVARCHAR (255) NOT NULL,
    [Data] NTEXT          NOT NULL,
    CONSTRAINT [PK__GlobalConfiguration] PRIMARY KEY NONCLUSTERED ([Item] ASC) WITH (FILLFACTOR = 80)
);

