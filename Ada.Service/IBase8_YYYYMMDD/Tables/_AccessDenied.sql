﻿CREATE TABLE [dbo].[_AccessDenied] (
    [GID]            NVARCHAR (50)  NOT NULL,
    [ItemType]       INT            NOT NULL,
    [ItemIdentifier] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK__AccessDenied] PRIMARY KEY NONCLUSTERED ([GID] ASC, [ItemType] ASC, [ItemIdentifier] ASC) WITH (FILLFACTOR = 80)
);

