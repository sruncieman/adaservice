﻿CREATE TABLE [dbo].[Address_] (
    [Unique_ID]                    NVARCHAR (50)   NOT NULL,
    [Address_ID]                   NVARCHAR (50)   NULL,
    [Address_Type]                 NVARCHAR (255)  NULL,
    [AltEntity]                    NVARCHAR (50)   NULL,
    [Building_]                    NVARCHAR (50)   NULL,
    [Building_Number]              NVARCHAR (50)   NULL,
    [County_]                      NVARCHAR (255)  NULL,
    [Create_Date]                  DATETIME        NOT NULL,
    [Create_User]                  NVARCHAR (255)  NOT NULL,
    [Do_Not_Disseminate_412284494] SMALLINT        NOT NULL,
    [Document_Link]                VARBINARY (MAX) NULL,
    [Document_Link_Binding]        NVARCHAR (50)   NULL,
    [DX_Exchange]                  NVARCHAR (150)  NULL,
    [DX_Number]                    NVARCHAR (50)   NULL,
    [Grid_X]                       NVARCHAR (50)   NULL,
    [Grid_Y]                       NVARCHAR (50)   NULL,
    [IconColour]                   INT             NULL,
    [Key_Attractor_412284410]      NVARCHAR (100)  NULL,
    [Last_Upd_Date]                DATETIME        NULL,
    [Last_Upd_User]                NVARCHAR (255)  NULL,
    [Locality_]                    NVARCHAR (50)   NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)   NULL,
    [PAF_Validation]               SMALLINT        NOT NULL,
    [Post_Code]                    NVARCHAR (50)   NULL,
    [Record_Status]                TINYINT         DEFAULT ((0)) NULL,
    [SCC]                          NVARCHAR (255)  DEFAULT ('') NULL,
    [Source_411765484]             NVARCHAR (50)   NULL,
    [Source_Description_411765489] NVARCHAR (MAX)  NULL,
    [Source_Reference_411765487]   NVARCHAR (50)   NULL,
    [Status_Binding]               NVARCHAR (50)   DEFAULT ('') NULL,
    [Street_]                      NVARCHAR (255)  NULL,
    [Sub_Building]                 NVARCHAR (50)   NULL,
    [Town_]                        NVARCHAR (50)   NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255)  NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)   NULL,
    CONSTRAINT [PK_Address_] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Building_Number]
    ON [dbo].[Address_]([Building_Number] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Post_Code]
    ON [dbo].[Address_]([Post_Code] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Street_]
    ON [dbo].[Address_]([Street_] ASC, [Unique_ID] ASC, [Record_Status] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Sub_Building]
    ON [dbo].[Address_]([Sub_Building] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Town_]
    ON [dbo].[Address_]([Town_] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);

