﻿CREATE TABLE [dbo].[_LinkEnd] (
    [Link_ID]        NVARCHAR (50)  NOT NULL,
    [Entity_ID1]     NVARCHAR (50)  NOT NULL,
    [Confidence]     TINYINT        NOT NULL,
    [Direction]      TINYINT        NOT NULL,
    [Entity_ID2]     NVARCHAR (50)  NOT NULL,
    [EntityType_ID1] INT            NOT NULL,
    [EntityType_ID2] INT            NOT NULL,
    [LinkType_ID]    INT            NOT NULL,
    [Record_Status]  TINYINT        DEFAULT ((0)) NULL,
    [Record_Type]    TINYINT        NOT NULL,
    [SCC]            NVARCHAR (255) DEFAULT ('') NULL,
    CONSTRAINT [PK__LinkEnd] PRIMARY KEY NONCLUSTERED ([Link_ID] ASC, [Entity_ID1] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_LinkEnd_Entity_ID2_Record_Status]
    ON [dbo].[_LinkEnd]([Entity_ID2] ASC, [Record_Status] ASC)
    INCLUDE([Link_ID], [Entity_ID1]);


GO
CREATE NONCLUSTERED INDEX [IX_LinkEnd_EntityType_ID1_EntityType_ID2]
    ON [dbo].[_LinkEnd]([EntityType_ID1] ASC, [EntityType_ID2] ASC)
    INCLUDE([Link_ID]);

