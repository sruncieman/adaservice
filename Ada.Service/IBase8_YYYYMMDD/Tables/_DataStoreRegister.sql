﻿CREATE TABLE [dbo].[_DataStoreRegister] (
    [RecordType]  INT            NOT NULL,
    [Icon]        NVARCHAR (255) NULL,
    [Name]        NVARCHAR (255) NOT NULL,
    [VisibleType] TINYINT        NULL,
    CONSTRAINT [PK__DataStoreRegister] PRIMARY KEY NONCLUSTERED ([RecordType] ASC) WITH (FILLFACTOR = 80)
);

