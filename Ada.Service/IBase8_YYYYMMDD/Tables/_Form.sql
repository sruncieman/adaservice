﻿CREATE TABLE [dbo].[_Form] (
    [Form_ID]       INT            NOT NULL,
    [MainEntity_ID] INT            NOT NULL,
    [Name]          NVARCHAR (255) NOT NULL,
    [Page_Style]    TINYINT        NULL,
    [UseForShow]    SMALLINT       NOT NULL,
    CONSTRAINT [PK__Form] PRIMARY KEY NONCLUSTERED ([Form_ID] ASC) WITH (FILLFACTOR = 80)
);

