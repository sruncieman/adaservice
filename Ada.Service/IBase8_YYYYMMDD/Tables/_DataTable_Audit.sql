﻿CREATE TABLE [dbo].[_DataTable_Audit] (
    [Table_ID]     INT            NOT NULL,
    [Description]  NVARCHAR (255) NULL,
    [Fixed]        SMALLINT       NOT NULL,
    [InExpandList] SMALLINT       NOT NULL,
    [LogicalName]  NVARCHAR (255) NOT NULL,
    [PhysicalName] NVARCHAR (255) NOT NULL,
    [TableCode]    NVARCHAR (3)   NOT NULL,
    [Type]         TINYINT        NOT NULL,
    [Date_Deleted] DATETIME       CONSTRAINT [DF__DataTable_Audit_Date_Deleted] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK__DataTable_Audit] PRIMARY KEY NONCLUSTERED ([Table_ID] ASC) WITH (FILLFACTOR = 80)
);

