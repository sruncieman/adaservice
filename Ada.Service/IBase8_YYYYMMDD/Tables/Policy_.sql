﻿CREATE TABLE [dbo].[Policy_] (
    [Unique_ID]                       NVARCHAR (50)  NOT NULL,
    [AltEntity]                       NVARCHAR (50)  NULL,
    [Broker_]                         NVARCHAR (50)  NULL,
    [Cover_Type]                      NVARCHAR (255) NULL,
    [Create_Date]                     DATETIME       NOT NULL,
    [Create_User]                     NVARCHAR (255) NOT NULL,
    [Do_Not_Disseminate_412284494]    SMALLINT       NOT NULL,
    [IconColour]                      INT            NULL,
    [Insurer_]                        NVARCHAR (50)  NULL,
    [Insurer_Trading_Name]            NVARCHAR (50)  NULL,
    [Key_Attractor_412284410]         NVARCHAR (100) NULL,
    [Last_Upd_Date]                   DATETIME       NULL,
    [Last_Upd_User]                   NVARCHAR (255) NULL,
    [MDA_Incident_ID_412284502]       NVARCHAR (50)  NULL,
    [Policy_End_Date]                 DATETIME       NULL,
    [Policy_ID]                       NVARCHAR (50)  NULL,
    [Policy_Number]                   NVARCHAR (50)  NULL,
    [Policy_Start_Date]               DATETIME       NULL,
    [Policy_Type]                     NVARCHAR (255) NULL,
    [Premium_]                        NVARCHAR (10)  NULL,
    [Previous_Claims_Disclosed]       NVARCHAR (10)  NULL,
    [Previous_Fault_Claims_Disclosed] NVARCHAR (10)  NULL,
    [Record_Status]                   TINYINT        DEFAULT ((0)) NULL,
    [SCC]                             NVARCHAR (255) DEFAULT ('') NULL,
    [Source_411765484]                NVARCHAR (50)  NULL,
    [Source_Description_411765489]    NVARCHAR (MAX) NULL,
    [Source_Reference_411765487]      NVARCHAR (50)  NULL,
    [Status_Binding]                  NVARCHAR (50)  DEFAULT ('') NULL,
    [x5x5x5_Grading_412284402]        NVARCHAR (255) NULL,
    [Risk_Claim_ID_414244883]         NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Policy_] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [IX_Policy_End_Date]
    ON [dbo].[Policy_]([Policy_End_Date] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Policy_Number]
    ON [dbo].[Policy_]([Policy_Number] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Policy_Start_Date]
    ON [dbo].[Policy_]([Policy_Start_Date] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);

