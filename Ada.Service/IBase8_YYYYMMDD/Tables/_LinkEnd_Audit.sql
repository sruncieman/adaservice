﻿CREATE TABLE [dbo].[_LinkEnd_Audit] (
    [Link_ID]        NVARCHAR (50)  NOT NULL,
    [Entity_ID1]     NVARCHAR (50)  NOT NULL,
    [Confidence]     TINYINT        NOT NULL,
    [Direction]      TINYINT        NOT NULL,
    [Entity_ID2]     NVARCHAR (50)  NOT NULL,
    [EntityType_ID1] INT            NOT NULL,
    [EntityType_ID2] INT            NOT NULL,
    [LinkType_ID]    INT            NOT NULL,
    [Record_Status]  TINYINT        NULL,
    [Record_Type]    TINYINT        NOT NULL,
    [SCC]            NVARCHAR (255) NULL,
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [date_deleted]   DATETIME       NULL,
    CONSTRAINT [PK__LinkEnd_Audit] PRIMARY KEY NONCLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [_AL_LEA_Date_Deleted]
    ON [dbo].[_LinkEnd_Audit]([date_deleted] ASC) WITH (FILLFACTOR = 80);

