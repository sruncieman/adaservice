﻿CREATE TABLE [dbo].[_Field_Audit] (
    [Field_ID]         INT            NOT NULL,
    [Characteristic]   SMALLINT       NOT NULL,
    [ChartAttributeID] INT            NULL,
    [CodeGroup_ID]     INT            NULL,
    [DefaultValue]     NVARCHAR (255) NULL,
    [Description]      NVARCHAR (255) NULL,
    [Discriminator]    SMALLINT       NOT NULL,
    [FieldIndex]       SMALLINT       NULL,
    [FieldSize]        TINYINT        NULL,
    [Fixed]            SMALLINT       NOT NULL,
    [Format]           NVARCHAR (255) NULL,
    [LogicalName]      NVARCHAR (255) NOT NULL,
    [LogicalType]      TINYINT        NOT NULL,
    [Mandatory]        SMALLINT       NOT NULL,
    [PhysicalName]     NVARCHAR (255) NOT NULL,
    [Search]           SMALLINT       NOT NULL,
    [Table_ID]         INT            NOT NULL,
    [Date_Deleted]     DATETIME       CONSTRAINT [DF__Field_Audit_Date_Deleted] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK__Field_Audit] PRIMARY KEY NONCLUSTERED ([Field_ID] ASC) WITH (FILLFACTOR = 80)
);

