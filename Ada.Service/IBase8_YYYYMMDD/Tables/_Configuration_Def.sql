﻿CREATE TABLE [dbo].[_Configuration_Def] (
    [Item]      NVARCHAR (255) NOT NULL,
    [Encrypted] SMALLINT       NOT NULL,
    CONSTRAINT [PK__Configuration_Def] PRIMARY KEY NONCLUSTERED ([Item] ASC) WITH (FILLFACTOR = 80)
);

