﻿CREATE TABLE [dbo].[_Codes] (
    [Unique_ID]    NVARCHAR (50)  NOT NULL,
    [Code]         NVARCHAR (255) NULL,
    [CodeGroup_ID] INT            NOT NULL,
    [Description]  NVARCHAR (255) NULL,
    [Expansion]    NVARCHAR (255) NULL,
    [Parent_ID]    NVARCHAR (50)  NULL,
    [SortIndex]    INT            NULL,
    CONSTRAINT [PK__Codes] PRIMARY KEY NONCLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [Codegroup_ID]
    ON [dbo].[_Codes]([CodeGroup_ID] ASC) WITH (FILLFACTOR = 80);

