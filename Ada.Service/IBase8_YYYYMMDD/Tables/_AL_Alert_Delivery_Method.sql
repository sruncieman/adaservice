﻿CREATE TABLE [dbo].[_AL_Alert_Delivery_Method] (
    [alert_id]           INT            NOT NULL,
    [delivery_method_id] INT            NOT NULL,
    [user_name]          NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK__AL_Alert_Delivery_Method] PRIMARY KEY CLUSTERED ([alert_id] ASC, [delivery_method_id] ASC, [user_name] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [AL_Alert_Delivery_Method__AL_Alert_User_FK] FOREIGN KEY ([alert_id], [user_name]) REFERENCES [dbo].[_AL_Alert_User] ([alert_id], [user_name]) ON UPDATE CASCADE
);


GO
ALTER TABLE [dbo].[_AL_Alert_Delivery_Method] NOCHECK CONSTRAINT [AL_Alert_Delivery_Method__AL_Alert_User_FK];

