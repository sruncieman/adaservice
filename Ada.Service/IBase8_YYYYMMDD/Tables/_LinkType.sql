﻿CREATE TABLE [dbo].[_LinkType] (
    [LinkType_ID] INT           NOT NULL,
    [Colour]      NVARCHAR (50) NOT NULL,
    [End1Types]   NTEXT         NULL,
    [End2Types]   NTEXT         NULL,
    CONSTRAINT [PK__LinkType] PRIMARY KEY NONCLUSTERED ([LinkType_ID] ASC) WITH (FILLFACTOR = 80)
);

