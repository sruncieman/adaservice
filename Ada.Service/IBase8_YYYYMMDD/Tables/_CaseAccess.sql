﻿CREATE TABLE [dbo].[_CaseAccess] (
    [CaseID]         NVARCHAR (50)  NOT NULL,
    [ItemType]       INT            NOT NULL,
    [ItemIdentifier] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK__CaseAccess] PRIMARY KEY NONCLUSTERED ([CaseID] ASC, [ItemType] ASC, [ItemIdentifier] ASC) WITH (FILLFACTOR = 80)
);

