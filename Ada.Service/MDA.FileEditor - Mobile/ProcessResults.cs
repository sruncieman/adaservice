﻿using MDA.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDA.FileEditor
{
    public partial class ProcessResults : Form
    {
        public ProcessResults()
        {
            InitializeComponent();
        }

        private TreeNode AssignMyNodeToTreeNode(MessageNode root)
        {
            TreeNode node = new TreeNode(root.Text);

            foreach (MessageNode n in root.Nodes)
                node.Nodes.Add(AssignMyNodeToTreeNode(n));

            return node;
        }

        public void SetNodes(MessageNode root)
        {
            treeView1.Nodes.Add(AssignMyNodeToTreeNode(root));
        }
    }
}
