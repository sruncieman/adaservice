﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using MDA.Common.FileModel;
//using MDA.WCF.TransferServices;
//using MDA.WCF.WebServices;
//using MDA.WCF.WebServices.Messages;
using MDA.FileEditor.ADAWebServiceReference;
using System.ServiceModel.Channels;
using System.ServiceModel;
using MDA.MappingService.Interface;
using MDA.VerificationService.Interface;
using MDA.Pipeline.Config;
using System.Reflection;
using MDA.Common;
using MDA.Common.Server;

namespace MDA.FileEditor
{
    public partial class MainForm : Form
    {
        public MobileClaimBatch Batch = new MobileClaimBatch();

        public MainForm()
        {
            InitializeComponent();

            claimBatchBindingSource.DataSource = Batch;

            SetButtons();
        }


        static public string SerialiseToXml(System.Object o, System.Type type)
        {
            StringWriter writer = new StringWriter();
            XmlSerializer ser = new XmlSerializer(type);
            ser.Serialize(writer, o);
            string s = writer.ToString();
            writer.Close();
            return s;
        }

        static public System.Object DeserialiseFromXmlString(string xmlString, System.Type type)
        {
            StringReader s = new StringReader(xmlString);

            XmlSerializer xml = new XmlSerializer(type);
            object o = xml.Deserialize(s);
            return o;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Batch.Claims = new ObservableCollection<MobileClaim>();

            claimBatchBindingSource.DataSource = Batch;
            
            SetButtons();

        }

        #region Claim
        private void ClaimEdit_Click(object sender, EventArgs e)
        {
            IncidentEdit ee = new IncidentEdit();

            MobileClaim i = (MobileClaim)this.claimsBindingSource.Current;
            if (i.Policy == null) i.Policy = new Policy();
            if (i.Handsets == null) i.Handsets = new ObservableCollection<Handset>();

            ee.incidentsBindingSource.DataSource = i;

            ee.ShowDialog();

            SetButtons();
        }

        private void ClaimNew_Click(object sender, EventArgs e)
        {
            IncidentEdit ee = new IncidentEdit();

            MobileClaim i = new MobileClaim();
            
            ee.incidentsBindingSource.DataSource = i;

            if (ee.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.claimsBindingSource.Add(i);

            SetButtons();
        }

        private void ClaimDel_Click(object sender, EventArgs e)
        {
            this.peopleBindingSource.Clear();
            this.organisationsBindingSource.Clear();
            this.addressesBindingSource.Clear();
            this.addressesBindingSource1.Clear();
            this.handsetsBindingSource.Clear();
            this.orgsClaimBindingSource.Clear();
            this.addressesOrgBindingSource1.Clear();
            this.orgVehBindingSource1.Clear();
            this.orgVehBindingSource2.Clear();

            this.claimsBindingSource.RemoveCurrent();

            SetButtons();
        }
        #endregion


        #region Claim Org
        private void btnClaimOrgNew_Click(object sender, EventArgs e)
        {
            OrgEdit ee = new OrgEdit();

            Organisation o = new Organisation();

            ee.organisationsBindingSource.DataSource = o;

            ee.SetGroupBoxes(true);

            if (ee.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.orgsClaimBindingSource.Add(o);

            SetButtons();
        }

        private void btnClaimOrgDel_Click(object sender, EventArgs e)
        {
            this.addressesOrgBindingSource1.Clear();
            this.orgsClaimBindingSource.RemoveCurrent();

            SetButtons();
        }

        private void btnClaimOrgEdit_Click(object sender, EventArgs e)
        {
            OrgEdit ee = new OrgEdit();

            Organisation o = (Organisation)this.orgsClaimBindingSource.Current;
            if (o.Addresses == null) o.Addresses = new ObservableCollection<Address>();

            ee.SetGroupBoxes(true);
            ee.organisationsBindingSource.DataSource = o;

            ee.ShowDialog();

            SetButtons();
        }
        #endregion

        #region Claim Org Address
        private void btnClaimOrgAddrNew_Click(object sender, EventArgs e)
        {
            AddressEdit ee = new AddressEdit();

            Address a = new Address();

            ee.addressesBindingSource.DataSource = a;

            if (ee.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.addressesOrgBindingSource1.Add(a);

            SetButtons();
        }

        private void btnClaimOrgAddrDel_Click(object sender, EventArgs e)
        {
            this.addressesOrgBindingSource1.RemoveCurrent();

            SetButtons();
        }

        private void btnClaimOrgAddrEdit_Click(object sender, EventArgs e)
        {
            AddressEdit ee = new AddressEdit();

            Address a = (Address)this.addressesOrgBindingSource1.Current;

            ee.addressesBindingSource.DataSource = a;

            ee.ShowDialog();

            SetButtons();
        }
        #endregion

        #region Claim Org Vehicle
        private void btnClaimOrgVehicleEdit_Click(object sender, EventArgs e)
        {
            OrgVehicleEdit ee = new OrgVehicleEdit();

            OrganisationVehicle v = (OrganisationVehicle)this.orgVehBindingSource1.Current;

            ee.vehiclesBindingSource.DataSource = v;

            ee.ShowDialog();

            SetButtons();
        }


        private void btnClaimOrgVehicleDel_Click(object sender, EventArgs e)
        {
            this.orgVehBindingSource1.RemoveCurrent();

            SetButtons();
        }


        private void btnClaimOrgVehNew_Click(object sender, EventArgs e)
        {
            OrgVehicleEdit ee = new OrgVehicleEdit();

            OrganisationVehicle v = new OrganisationVehicle();

            ee.vehiclesBindingSource.DataSource = v;

            if (ee.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.orgVehBindingSource1.Add(v);

            SetButtons();
        }

        #endregion


        #region Handset
        private void btnHandsetNew_Click(object sender, EventArgs e)
        {
            HandsetEdit ee = new HandsetEdit();

            Handset h = new Handset();

            ee.handsetBindingSource.DataSource = h;

            if (ee.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.handsetsBindingSource.Add(h);

            SetButtons();
        }

        private void btnHandsetEdit_Click(object sender, EventArgs e)
        {
            HandsetEdit ee = new HandsetEdit();

            Handset v = (Handset)this.handsetsBindingSource.Current;
            if (v.People == null) v.People = new ObservableCollection<Person>();

            ee.handsetBindingSource.DataSource = v;

            ee.ShowDialog();

            SetButtons();
        }

        private void btnHandsetDel_Click(object sender, EventArgs e)
        {
            this.peopleBindingSource.Clear();
            this.organisationsBindingSource.Clear();
            this.addressesBindingSource.Clear();
            this.addressesBindingSource1.Clear();
            this.orgVehBindingSource2.Clear();

            this.handsetsBindingSource.RemoveCurrent();

            SetButtons();
        }
        #endregion

        #region Person

        private void btnPeopleNew_Click(object sender, EventArgs e)
        {
            PeopleEdit ee = new PeopleEdit();

            Person p = new Person();

            ee.peopleBindingSource.DataSource = p;

            if (ee.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.peopleBindingSource.Add(p);

            SetButtons();
        }

        private void btnPeopleDel_Click(object sender, EventArgs e)
        {
            this.addressesBindingSource.Clear();
            this.peopleBindingSource.RemoveCurrent();

            SetButtons();
        }

        private void btnPeopleEdit_Click(object sender, EventArgs e)
        {
            PeopleEdit ee = new PeopleEdit();

            Person p = (Person)this.peopleBindingSource.Current;
            //if (p.BankAccount == null) p.BankAccount = new BankAccount();
            //if (p.PaymentCard == null) p.PaymentCard = new PaymentCard();
            if (p.Addresses == null) p.Addresses = new ObservableCollection<Address>();

            ee.peopleBindingSource.DataSource = p;

            ee.ShowDialog();

            SetButtons();
        }

        #endregion

        #region Person Address
        private void btnPerAddrNew_Click(object sender, EventArgs e)
        {
            AddressEdit ee = new AddressEdit();

            Address a = new Address();

            ee.addressesBindingSource.DataSource = a;

            if (ee.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.addressesBindingSource.Add(a);

            SetButtons();
        }

        private void btnPerAddrDel_Click(object sender, EventArgs e)
        {
            this.addressesBindingSource.RemoveCurrent();

            SetButtons();
        }

        private void btnPerAddrEdit_Click(object sender, EventArgs e)
        {
            AddressEdit ee = new AddressEdit();

            Address a = (Address)this.addressesBindingSource.Current;

            ee.addressesBindingSource.DataSource = a;

            ee.ShowDialog();

            SetButtons();
        }
        #endregion


        #region Organisation
        private void btnOrgNew_Click(object sender, EventArgs e)
        {
            OrgEdit ee = new OrgEdit();

            Organisation o = new Organisation();

            ee.organisationsBindingSource.DataSource = o;
            //if (o.BankAccount == null) o.BankAccount = new BankAccount();
            //if (o.PaymentCard == null) o.PaymentCard = new PaymentCard();
            if (o.Addresses == null) o.Addresses = new ObservableCollection<Address>();

            ee.SetGroupBoxes(false);

            if (ee.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.organisationsBindingSource.Add(o);

            SetButtons();
        }

        private void btnOrgDel_Click(object sender, EventArgs e)
        {
            this.addressesBindingSource1.Clear();
            this.organisationsBindingSource.RemoveCurrent();

            SetButtons();
        }

        private void btnOrgEdit_Click(object sender, EventArgs e)
        {
            OrgEdit ee = new OrgEdit();

            Organisation o = (Organisation)this.organisationsBindingSource.Current;
            if (o.Addresses == null) o.Addresses = new ObservableCollection<Address>();

            ee.organisationsBindingSource.DataSource = o;
            ee.SetGroupBoxes(false);

            ee.ShowDialog();

            SetButtons();
        }
        #endregion

        #region Organ Address
        private void btnOrgAddrNew_Click(object sender, EventArgs e)
        {
            AddressEdit ee = new AddressEdit();

            Address a = new Address();

            ee.addressesBindingSource.DataSource = a;

            if (ee.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.addressesBindingSource1.Add(a);

            SetButtons();
        }

        private void btnOrgAddrDel_Click(object sender, EventArgs e)
        {
            this.addressesBindingSource1.RemoveCurrent();

            SetButtons();
        }

        private void btnOrgAddrEdit_Click(object sender, EventArgs e)
        {
            AddressEdit ee = new AddressEdit();

            Address a = (Address)this.addressesBindingSource1.Current;

            ee.addressesBindingSource.DataSource = a;

            ee.ShowDialog();

            SetButtons();
        }
        #endregion

        #region Person Org Vehicle

        private void btnPersonOrgVehDelete_Click(object sender, EventArgs e)
        {
            this.orgVehBindingSource2.RemoveCurrent();

            SetButtons();
        }

        private void btnPersonOrgVehEdit_Click(object sender, EventArgs e)
        {
            OrgVehicleEdit ee = new OrgVehicleEdit();

            OrganisationVehicle v = (OrganisationVehicle)this.orgVehBindingSource2.Current;

            ee.vehiclesBindingSource.DataSource = v;

            ee.ShowDialog();

            SetButtons();
        }



        private void btnPersonOrgVehNew_Click(object sender, EventArgs e)
        {
            OrgVehicleEdit ee = new OrgVehicleEdit();

            OrganisationVehicle v = new OrganisationVehicle();

            ee.vehiclesBindingSource.DataSource = v;

            if (ee.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.orgVehBindingSource2.Add(v);

            SetButtons();
        }
        #endregion


        private void button4_Click(object sender, EventArgs e)
        {
            FileStream fs = null;
            XmlDictionaryReader reader = null;

            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    fs = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read);
                    reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                    DataContractSerializer ser = new DataContractSerializer(typeof(MobileClaimBatch));

                    // Deserialize the data and read it from the instance.
                    MobileClaimBatch c = (MobileClaimBatch)ser.ReadObject(reader, true);

                    fs.Close();
                    fs = null;

                    Batch = c;

                    claimBatchBindingSource.DataSource = c;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                if ( reader != null ) reader.Close();
                if ( fs != null ) fs.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            FileStream writer = null;

            try
            {
                //string s = SerialiseToXml(Batch, typeof(ClaimBatch));

                if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    writer = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    DataContractSerializer ser = new DataContractSerializer(typeof(MobileClaimBatch));
                    ser.WriteObject(writer, Batch);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                if (writer != null) writer.Close();
            }

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            try
            {
                ADAWebServiceReference.ADAWebServicesClient s = new ADAWebServiceReference.ADAWebServicesClient();

                 string clientBatchReference = Path.GetRandomFileName();

                ProcessMobileClaimBatchRequest req = new ProcessMobileClaimBatchRequest()
                {
                    ClientId = (int)numericUpDown1.Value,
                    Batch = Batch,
                    UserId = 0,
                    BatchEntityType = "Mobile",
                    Who = "FileEditor",
                    ClientBatchReference = clientBatchReference
                };


                ProcessMobileClaimBatchResponse res = s.ProcessMobileClaimBatch(req);

                MessageBox.Show("Success");
            }
            catch (System.ServiceModel.FaultException faultException)
            {
                var fault = faultException.CreateMessageFault();
                var doc = new XmlDocument();
                var nav = doc.CreateNavigator();
                string msg = "error";

                if (nav != null)
                {
                    using (var writer = nav.AppendChild())
                    {
                        fault.WriteTo(writer, EnvelopeVersion.Soap11);

                    }

                    //this is where you implement your API specific code 
                    //to handle the returned detail.
                    msg = doc.InnerXml; //do something with it
                }

                MessageBox.Show(msg);
            }


        }

        private void button5_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    ADAWebServiceReference.ADAWebServicesClient s = new ADAWebServiceReference.ADAWebServicesClient();

            //    foreach (MobileClaim claim in Batch.Claims)
            //    {

            //        ProcessClaimRequest req = new ProcessClaimRequest()
            //        {
            //            ClientId = 0,
            //            Claim = claim,
            //            UserId = 0
            //        };

            //        ProcessClaimResponse res = s.ProcessClaim(req);
            //    }
            //}
            //catch (System.ServiceModel.FaultException faultException)
            //{
            //    var fault = faultException.CreateMessageFault();
            //    var doc = new XmlDocument();
            //    var nav = doc.CreateNavigator();
            //    string msg = "error";

            //    if (nav != null)
            //    {
            //        using (var writer = nav.AppendChild())
            //        {
            //            fault.WriteTo(writer, EnvelopeVersion.Soap11);

            //        }

            //        //this is where you implement your API specific code 
            //        //to handle the returned detail.
            //        msg = doc.InnerXml; //do something with it
            //    }

            //    MessageBox.Show(msg);
            //}
        }

        private void SetButtons()
        {
            btnClaimDel.Enabled = listBox1.Items.Count > 0 && listBox1.SelectedItem != null;
            btnClaimEdit.Enabled = listBox1.Items.Count > 0 && listBox1.SelectedItem != null;

            btnClaimOrgNew.Enabled = btnClaimEdit.Enabled;
            btnClaimOrgDel.Enabled = lbClaimOrg.Items.Count > 0 && lbClaimOrg.SelectedItem != null;
            btnClaimOrgEdit.Enabled = lbClaimOrg.Items.Count > 0 && lbClaimOrg.SelectedItem != null;

            btnClaimOrgAddrNew.Enabled = btnClaimOrgEdit.Enabled;
            btnClaimOrgAddrDel.Enabled = lbClaimOrgAddr.Items.Count > 0 && lbClaimOrgAddr.SelectedItem != null;
            btnClaimOrgAddrEdit.Enabled = lbClaimOrgAddr.Items.Count > 0 && lbClaimOrgAddr.SelectedItem != null;


            btnVehicleNew.Enabled = btnClaimEdit.Enabled;
            btnVehicleDel.Enabled = listBox2.Items.Count > 0 && listBox2.SelectedItem != null;
            btnVehicleEdit.Enabled = listBox2.Items.Count > 0 && listBox2.SelectedItem != null;

            btnPeopleNew.Enabled = btnVehicleEdit.Enabled;
            btnPeopleDel.Enabled = listBox3.Items.Count > 0 && listBox3.SelectedItem != null;
            btnPeopleEdit.Enabled = listBox3.Items.Count > 0 && listBox3.SelectedItem != null;

            btnOrgNew.Enabled = btnPeopleEdit.Enabled;
            btnOrgDel.Enabled = listBox5.Items.Count > 0 && listBox5.SelectedItem != null;
            btnOrgEdit.Enabled = listBox5.Items.Count > 0 && listBox5.SelectedItem != null;

            btnPerAddrNew.Enabled = btnPeopleEdit.Enabled;
            btnPerAddrDel.Enabled = listBox4.Items.Count > 0 && listBox4.SelectedItem != null;
            btnPerAddrEdit.Enabled = listBox4.Items.Count > 0 && listBox4.SelectedItem != null;

            btnOrgAddrNew.Enabled = btnOrgEdit.Enabled;
            btnOrgAddrDel.Enabled = listBox6.Items.Count > 0 && listBox6.SelectedItem != null;
            btnOrgAddrEdit.Enabled = listBox6.Items.Count > 0 && listBox6.SelectedItem != null;

        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetButtons();
        }

        private int DummyRecordProcess(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim, object o)
        {
            MDA.Pipeline.Model.PipelineClaimBatch cb = o as MDA.Pipeline.Model.PipelineClaimBatch;

            cb.Claims.Add(claim);

            return 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(openFileDialog1.FileName);

                ADATransferServiceReference.ADATransferServicesClient s = new ADATransferServiceReference.ADATransferServicesClient();

                LoadAssemblies((int)numericUpDown1.Value, "Mobile");

                foreach (var fileName in openFileDialog1.FileNames)
                {

                    using (System.IO.FileStream stream =
                        new System.IO.FileStream(fileName, System.IO.FileMode.Open,
                                                 System.IO.FileAccess.Read))
                    {
                        bool somethingToShow = false;
                        ProcessResults pr = new ProcessResults();

                        if (openFileDialog1.FileNames.Count() < 5)
                        {
                            ProcessingResults processingResults1;
                            ProcessingResults processingResults2;
                            ProcessingResults processingResults3;

                            CurrentContext ctx = new CurrentContext((int)numericUpDown1.Value, 1, "Test");
                           

                            processingResults1 = _verificationService.QuickVerification(stream, ctx, "csv");

                            if (processingResults1.ErrorCount > 0 || processingResults1.WarningCount > 0)
                            {
                                somethingToShow = true;
                                pr.SetNodes(processingResults1.Message);
                            }

                            if (!processingResults1.IsValid)
                            {
                                MessageBox.Show("File failed verification");
                                continue;
                            }

                            MDA.Pipeline.Model.PipelineClaimBatch cb = new Pipeline.Model.PipelineClaimBatch() ;

                            //if (_mappingService is MDA.MappingService.Interface.ITableMappingService)
                            //{
                            //    var ms = _mappingService as MDA.MappingService.Interface.ITableMappingService;

                            //    cb = ms.ConvertToClaimBatch(ctx, stream, out processingResults2, null); //WriteClaimsToDb);
                            //}
                            //else
                            //{
                            //    var ms = _mappingService as MDA.MappingService.Interface.IMemoryMappingService;

                            //    cb = ms.ConvertToClaimBatch(ctx, stream, out processingResults2);

                            //    //ScanPipelineRecords(ctx, claimBatch, InitialisePipelineEntityRecord, InitialisePipelineLinkRecord);
                            //}

                            //MDA.Pipeline.Model.PipelineClaimBatch cb = _mappingService.ConvertToClaimBatch(ctx, stream, out processingResults2);


                            _mappingService.ConvertToClaimBatch(ctx, stream, cb, DummyRecordProcess, out processingResults2);

                            if (processingResults2.ErrorCount > 0 || processingResults2.WarningCount > 0)
                            {
                                somethingToShow = true;
                                pr.SetNodes(processingResults2.Message);
                            }

                            if (!processingResults2.IsValid)
                            {
                                MessageBox.Show("File failed mapping");
                                continue;
                            }

                            int errorCount = 0;

                            foreach (var c in cb.Claims)
                            {
                                processingResults3 = _validationService.ValidateClaim(c);
                                if (processingResults2.ErrorCount > 0 || processingResults2.WarningCount > 0)
                                {
                                    somethingToShow = true;
                                    if (processingResults2.ErrorCount > 0) errorCount += 1;
                                    pr.SetNodes(processingResults3.Message);
                                }
                            }

                            if (errorCount > 0)
                            {
                                MessageBox.Show(errorCount.ToString() + " claims failed validation");
                            }
                        }

                        if (!somethingToShow || pr.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            try
                            {
                                stream.Position = 0;

                                s.UploadBatchOfClaimsFile("Mobile", "#ref", (int)numericUpDown1.Value, fileInfo.Length, fileInfo.Name, fileInfo.Extension, 0,"fred", stream);

                            }
                            catch (System.ServiceModel.FaultException faultException)
                            {
                                var fault = faultException.CreateMessageFault();
                                var doc = new XmlDocument();
                                var nav = doc.CreateNavigator();
                                string msg = "error";

                                if (nav != null)
                                {
                                    using (var writer = nav.AppendChild())
                                    {
                                        fault.WriteTo(writer, EnvelopeVersion.Soap11);

                                    }

                                    //this is where you implement your API specific code 
                                    //to handle the returned detail.
                                    msg = doc.InnerXml; //do something with it
                                }

                                MessageBox.Show(msg);
                            }
                        }
                        
                    }
                }
                MessageBox.Show("The upload of your extremely interesting data is complete");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //ADATransferServiceReference.ADATransferServicesClient s = new ADATransferServiceReference.ADATransferServicesClient();

            //var ss = s.GetBatchReportStream( new GetBatchReportStreamRequest() { riskClientId=0,})

            // FileStream f = File.Create(@"c:\\temp\\testdoc.pdf");

            //byte[] buffer = new byte[2000];
            //int count  = 0;
            //int c = 0;

            //while ((c = ss.Read(buffer, 0, 1000)) > 0)
            //{
            //    count += c;
            //    f.Write(buffer, 0, (int)c);
            //}

            //MessageBox.Show("Downloaded file : " + count.ToString() + " bytes. Saved to c:\\temp\\testdoc.pdf");

            //f.Flush();
            //f.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ClientInterface d = new ClientInterface((int)numericUpDown1.Value);

            d.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            
            //ADATransferServiceReference.ADATransferServicesClient s = new ADATransferServiceReference.ADATransferServicesClient();

            //var ss = s.GetBatchReportStream(1, "pdf");

            //FileStream f = File.Create(@"c:\\temp\\BatchReport.pdf");

            //byte[] buffer = new byte[2000];
            //int count = 0;
            //int c = 0;

            //while ((c = ss.Read(buffer, 0, 1000)) > 0)
            //{
            //    count += c;
            //    f.Write(buffer, 0, (int)c);
            //}

            //MessageBox.Show("Downloaded file : " + count.ToString() + " bytes. Saved to c:\\temp\\BatchReport.pdf");

            //f.Flush();
            //f.Close();
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(openFileDialog1.FileName);

                ADATransferServiceReference.ADATransferServicesClient s = new ADATransferServiceReference.ADATransferServicesClient();

                using (System.IO.FileStream stream =
                       new System.IO.FileStream(openFileDialog1.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                {
                    try
                    {
                        s.UploadBatchOfClaimsFile("Mobile", "#ref", (int)numericUpDown1.Value, fileInfo.Length, fileInfo.Name, fileInfo.Extension, 0, "fred", stream);

                        MessageBox.Show("Success");
                    }
                    catch (System.ServiceModel.FaultException faultException)
                    {
                        var fault = faultException.CreateMessageFault();
                        var doc = new XmlDocument();
                        var nav = doc.CreateNavigator();
                        string msg = "error";

                        if (nav != null)
                        {
                            using (var writer = nav.AppendChild())
                            {
                                fault.WriteTo(writer, EnvelopeVersion.Soap11);

                            }

                            //this is where you implement your API specific code 
                            //to handle the returned detail.
                            msg = doc.InnerXml; //do something with it
                        }

                        MessageBox.Show(msg);
                    }
                }
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(openFileDialog1.FileName);

                ADATransferServiceReference.ADATransferServicesClient s = new ADATransferServiceReference.ADATransferServicesClient();

                using (System.IO.FileStream stream =
                       new System.IO.FileStream(openFileDialog1.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                {
                    try
                    {
                        s.UploadBatchOfClaimsFile("Mobile", "#ref", (int)numericUpDown1.Value, fileInfo.Length, fileInfo.Name, fileInfo.Extension, 0, "fred", stream);

                        MessageBox.Show("Success");
                    }
                    catch (System.ServiceModel.FaultException faultException)
                    {
                        var fault = faultException.CreateMessageFault();
                        var doc = new XmlDocument();
                        var nav = doc.CreateNavigator();
                        string msg = "error";

                        if (nav != null)
                        {
                            using (var writer = nav.AppendChild())
                            {
                                fault.WriteTo(writer, EnvelopeVersion.Soap11);

                            }

                            //this is where you implement your API specific code 
                            //to handle the returned detail.
                            msg = doc.InnerXml; //do something with it
                        }

                        MessageBox.Show(msg);
                    }
                }
            }
        }

        private void btnScore_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(openFileDialog1.FileName);

                ADATransferServiceReference.ADATransferServicesClient s = new ADATransferServiceReference.ADATransferServicesClient();

                using (System.IO.FileStream stream =
                       new System.IO.FileStream(openFileDialog1.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                {
                    try
                    {
                        s.UploadBatchOfClaimsFile("Mobile", "#ref", (int)numericUpDown1.Value, fileInfo.Length, fileInfo.Name, fileInfo.Extension, 0, "fred", stream);

                        MessageBox.Show("Success");
                    }
                    catch (System.ServiceModel.FaultException faultException)
                    {
                        var fault = faultException.CreateMessageFault();
                        var doc = new XmlDocument();
                        var nav = doc.CreateNavigator();
                        string msg = "error";

                        if (nav != null)
                        {
                            using (var writer = nav.AppendChild())
                            {
                                fault.WriteTo(writer, EnvelopeVersion.Soap11);

                            }

                            //this is where you implement your API specific code 
                            //to handle the returned detail.
                            msg = doc.InnerXml; //do something with it
                        }

                        MessageBox.Show(msg);
                    }
                }
            }
        }

        private static IMappingService _mappingService = null;
        private static IVerificationService _verificationService = null;
        private static IValidationService _validationService = null;

        public static void LoadAssemblies(int clientId, string policyType)
        {
            PolicyPipeline pType = PipelineConfig.GetConfig.PolicyPipelines[policyType];

            ClientModule cm = pType.ClientModules.GetItemByKey(clientId.ToString());

            _mappingService = (IMappingService)CreateInstance(cm.MappingAssembly.TypeName, "");
            _verificationService = (IVerificationService)CreateInstance(cm.VerificationAssembly.TypeName, "");
            _validationService = (IValidationService)CreateInstance(cm.ValidationAssembly.TypeName, "");
        }

        /// <summary>
        /// Creates an instance of the object identified by this class.
        /// </summary>
        /// <param name="args">Parameters required by the object's constructor.</param>
        /// <returns>An instance of the specified type</returns>
        private static object CreateInstance(string fullTypeName, string assemblyPath, object[] args = null)
        {
            object ans = null;
            string typeName;
            string assemblyName;
            string assemblyDll;

            if (fullTypeName == null || string.IsNullOrEmpty(fullTypeName)) return null;

            SplitType(fullTypeName, out typeName, out assemblyName, out assemblyDll);

            Assembly assemblyInstance = null;

            //try
            //{
            if (assemblyPath.Length > 0)
            {
                string path = assemblyPath;

                if (!path.EndsWith("\\")) path += "\\";

                assemblyInstance = Assembly.LoadFrom(path + assemblyDll);
            }
            else
                //  use full assembly name to get assembly instance
                assemblyInstance = Assembly.Load(assemblyName);
            //}
            //catch (Exception)
            //{
            //throw new YWApplicationException("error creating instance"); //string.Format(Resources.RES_ExceptionCantLoadAssembly, _assembly, e.Message), e);
            //}

            //  use type name to get type from assembly
            Type type = assemblyInstance.GetType(typeName, true, false);

            //try
            //{
            if (args != null)
            {
                ans = Activator.CreateInstance(type, args);
            }
            else
            {
                ans = Activator.CreateInstance(type);
            }
            //}
            //catch (Exception e)
            //{
            //    throw new Exception("error creating instance"); //throw new YWApplicationException(string.Format(Resources.RES_ExceptionCantCreateInstanceUsingActivate, _type.ToString(), e.Message), e);
            //}

            return ans;
        }

        /// <summary>
        /// Takes incoming full type string, and splits the type into two strings, the typeName and assemblyName.
        /// This routine also cleans up any extra whitespace, and throws an exception if the full type string
        /// does not have five comma-delimited parts. Sets the private variables behind the public properties.
        /// </summary>
        /// <param name="fullType">The string to be parsed.</param>
        /// <exception cref="ArgumentException">Raised if the type is not a full 5-part name.</exception>
        private static void SplitType(string fullType, out string typeName, out string assemblyName, out string assemblyDll)
        {
            if (fullType == null)
                throw new ArgumentException("Invalid Type specified in config :" + fullType);

            string[] parts = fullType.Split(',');

            if (parts.Length == 5)
            {
                //  set the object type name
                typeName = parts[0].Trim();

                //  set the object assembly name
                assemblyName = String.Concat(parts[1].Trim(), ",",
                    parts[2].Trim(), ",",
                    parts[3].Trim(), ",",
                    parts[4].Trim());

                assemblyDll = String.Concat(parts[1].Trim() + ".dll");
            }
            else
                throw new ArgumentException("Invalid Type specified in config :" + fullType);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            
        }

      
    }
}
