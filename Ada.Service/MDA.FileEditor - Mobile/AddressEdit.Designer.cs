﻿namespace MDA.FileEditor
{
    partial class AddressEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label addressLinkTypeLabel;
            System.Windows.Forms.Label buildingLabel;
            System.Windows.Forms.Label buildingNumberLabel;
            System.Windows.Forms.Label countyLabel;
            System.Windows.Forms.Label endOfResidencyLabel;
            System.Windows.Forms.Label localityLabel;
            System.Windows.Forms.Label postCodeLabel;
            System.Windows.Forms.Label startOfResidencyLabel;
            System.Windows.Forms.Label streetLabel;
            System.Windows.Forms.Label subBuildingLabel;
            System.Windows.Forms.Label townLabel;
            System.Windows.Forms.Label label4;
            this.addressLinkTypeComboBox = new System.Windows.Forms.ComboBox();
            this.addressesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buildingTextBox = new System.Windows.Forms.TextBox();
            this.buildingNumberTextBox = new System.Windows.Forms.TextBox();
            this.countyTextBox = new System.Windows.Forms.TextBox();
            this.endOfResidencyDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.localityTextBox = new System.Windows.Forms.TextBox();
            this.postCodeTextBox = new System.Windows.Forms.TextBox();
            this.startOfResidencyDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.streetTextBox = new System.Windows.Forms.TextBox();
            this.subBuildingTextBox = new System.Windows.Forms.TextBox();
            this.townTextBox = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            addressLinkTypeLabel = new System.Windows.Forms.Label();
            buildingLabel = new System.Windows.Forms.Label();
            buildingNumberLabel = new System.Windows.Forms.Label();
            countyLabel = new System.Windows.Forms.Label();
            endOfResidencyLabel = new System.Windows.Forms.Label();
            localityLabel = new System.Windows.Forms.Label();
            postCodeLabel = new System.Windows.Forms.Label();
            startOfResidencyLabel = new System.Windows.Forms.Label();
            streetLabel = new System.Windows.Forms.Label();
            subBuildingLabel = new System.Windows.Forms.Label();
            townLabel = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // addressLinkTypeLabel
            // 
            addressLinkTypeLabel.AutoSize = true;
            addressLinkTypeLabel.Location = new System.Drawing.Point(21, 25);
            addressLinkTypeLabel.Name = "addressLinkTypeLabel";
            addressLinkTypeLabel.Size = new System.Drawing.Size(81, 13);
            addressLinkTypeLabel.TabIndex = 0;
            addressLinkTypeLabel.Text = "Address Status:";
            // 
            // buildingLabel
            // 
            buildingLabel.AutoSize = true;
            buildingLabel.Location = new System.Drawing.Point(21, 52);
            buildingLabel.Name = "buildingLabel";
            buildingLabel.Size = new System.Drawing.Size(47, 13);
            buildingLabel.TabIndex = 2;
            buildingLabel.Text = "Building:";
            // 
            // buildingNumberLabel
            // 
            buildingNumberLabel.AutoSize = true;
            buildingNumberLabel.Location = new System.Drawing.Point(21, 104);
            buildingNumberLabel.Name = "buildingNumberLabel";
            buildingNumberLabel.Size = new System.Drawing.Size(87, 13);
            buildingNumberLabel.TabIndex = 6;
            buildingNumberLabel.Text = "Building Number:";
            // 
            // countyLabel
            // 
            countyLabel.AutoSize = true;
            countyLabel.Location = new System.Drawing.Point(21, 208);
            countyLabel.Name = "countyLabel";
            countyLabel.Size = new System.Drawing.Size(43, 13);
            countyLabel.TabIndex = 14;
            countyLabel.Text = "County:";
            // 
            // endOfResidencyLabel
            // 
            endOfResidencyLabel.AutoSize = true;
            endOfResidencyLabel.Location = new System.Drawing.Point(21, 296);
            endOfResidencyLabel.Name = "endOfResidencyLabel";
            endOfResidencyLabel.Size = new System.Drawing.Size(96, 13);
            endOfResidencyLabel.TabIndex = 21;
            endOfResidencyLabel.Text = "End Of Residency:";
            // 
            // localityLabel
            // 
            localityLabel.AutoSize = true;
            localityLabel.Location = new System.Drawing.Point(21, 156);
            localityLabel.Name = "localityLabel";
            localityLabel.Size = new System.Drawing.Size(46, 13);
            localityLabel.TabIndex = 10;
            localityLabel.Text = "Locality:";
            // 
            // postCodeLabel
            // 
            postCodeLabel.AutoSize = true;
            postCodeLabel.Location = new System.Drawing.Point(21, 234);
            postCodeLabel.Name = "postCodeLabel";
            postCodeLabel.Size = new System.Drawing.Size(59, 13);
            postCodeLabel.TabIndex = 16;
            postCodeLabel.Text = "Post Code:";
            // 
            // startOfResidencyLabel
            // 
            startOfResidencyLabel.AutoSize = true;
            startOfResidencyLabel.Location = new System.Drawing.Point(21, 270);
            startOfResidencyLabel.Name = "startOfResidencyLabel";
            startOfResidencyLabel.Size = new System.Drawing.Size(99, 13);
            startOfResidencyLabel.TabIndex = 18;
            startOfResidencyLabel.Text = "Start Of Residency:";
            // 
            // streetLabel
            // 
            streetLabel.AutoSize = true;
            streetLabel.Location = new System.Drawing.Point(21, 130);
            streetLabel.Name = "streetLabel";
            streetLabel.Size = new System.Drawing.Size(38, 13);
            streetLabel.TabIndex = 8;
            streetLabel.Text = "Street:";
            // 
            // subBuildingLabel
            // 
            subBuildingLabel.AutoSize = true;
            subBuildingLabel.Location = new System.Drawing.Point(21, 78);
            subBuildingLabel.Name = "subBuildingLabel";
            subBuildingLabel.Size = new System.Drawing.Size(69, 13);
            subBuildingLabel.TabIndex = 4;
            subBuildingLabel.Text = "Sub Building:";
            // 
            // townLabel
            // 
            townLabel.AutoSize = true;
            townLabel.Location = new System.Drawing.Point(21, 182);
            townLabel.Name = "townLabel";
            townLabel.Size = new System.Drawing.Size(37, 13);
            townLabel.TabIndex = 12;
            townLabel.Text = "Town:";
            // 
            // addressLinkTypeComboBox
            // 
            this.addressLinkTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "AddressLinkType", true));
            this.addressLinkTypeComboBox.FormattingEnabled = true;
            this.addressLinkTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "PreviousAddress",
            "LinkedAddress",
            "CurrentAddress",
            "RegisteredOffice",
            "TradingAddress",
            "StorageAddress"});
            this.addressLinkTypeComboBox.Location = new System.Drawing.Point(126, 22);
            this.addressLinkTypeComboBox.Name = "addressLinkTypeComboBox";
            this.addressLinkTypeComboBox.Size = new System.Drawing.Size(200, 21);
            this.addressLinkTypeComboBox.TabIndex = 1;
            // 
            // addressesBindingSource
            // 
            this.addressesBindingSource.DataSource = typeof(MDA.Common.FileModel.Address);
            this.addressesBindingSource.CurrentChanged += new System.EventHandler(this.addressesBindingSource_CurrentChanged);
            // 
            // buildingTextBox
            // 
            this.buildingTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "Building", true));
            this.buildingTextBox.Location = new System.Drawing.Point(126, 49);
            this.buildingTextBox.Name = "buildingTextBox";
            this.buildingTextBox.Size = new System.Drawing.Size(200, 20);
            this.buildingTextBox.TabIndex = 3;
            // 
            // buildingNumberTextBox
            // 
            this.buildingNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "BuildingNumber", true));
            this.buildingNumberTextBox.Location = new System.Drawing.Point(126, 101);
            this.buildingNumberTextBox.Name = "buildingNumberTextBox";
            this.buildingNumberTextBox.Size = new System.Drawing.Size(200, 20);
            this.buildingNumberTextBox.TabIndex = 7;
            // 
            // countyTextBox
            // 
            this.countyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "County", true));
            this.countyTextBox.Location = new System.Drawing.Point(126, 205);
            this.countyTextBox.Name = "countyTextBox";
            this.countyTextBox.Size = new System.Drawing.Size(200, 20);
            this.countyTextBox.TabIndex = 15;
            // 
            // endOfResidencyDateTimePicker
            // 
            this.endOfResidencyDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.addressesBindingSource, "EndOfResidency", true));
            this.endOfResidencyDateTimePicker.Enabled = false;
            this.endOfResidencyDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.endOfResidencyDateTimePicker.Location = new System.Drawing.Point(158, 292);
            this.endOfResidencyDateTimePicker.Name = "endOfResidencyDateTimePicker";
            this.endOfResidencyDateTimePicker.Size = new System.Drawing.Size(168, 20);
            this.endOfResidencyDateTimePicker.TabIndex = 23;
            // 
            // localityTextBox
            // 
            this.localityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "Locality", true));
            this.localityTextBox.Location = new System.Drawing.Point(126, 153);
            this.localityTextBox.Name = "localityTextBox";
            this.localityTextBox.Size = new System.Drawing.Size(200, 20);
            this.localityTextBox.TabIndex = 11;
            // 
            // postCodeTextBox
            // 
            this.postCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "PostCode", true));
            this.postCodeTextBox.Location = new System.Drawing.Point(126, 231);
            this.postCodeTextBox.Name = "postCodeTextBox";
            this.postCodeTextBox.Size = new System.Drawing.Size(200, 20);
            this.postCodeTextBox.TabIndex = 17;
            // 
            // startOfResidencyDateTimePicker
            // 
            this.startOfResidencyDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.addressesBindingSource, "StartOfResidency", true));
            this.startOfResidencyDateTimePicker.Enabled = false;
            this.startOfResidencyDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startOfResidencyDateTimePicker.Location = new System.Drawing.Point(158, 266);
            this.startOfResidencyDateTimePicker.Name = "startOfResidencyDateTimePicker";
            this.startOfResidencyDateTimePicker.Size = new System.Drawing.Size(168, 20);
            this.startOfResidencyDateTimePicker.TabIndex = 20;
            // 
            // streetTextBox
            // 
            this.streetTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "Street", true));
            this.streetTextBox.Location = new System.Drawing.Point(126, 127);
            this.streetTextBox.Name = "streetTextBox";
            this.streetTextBox.Size = new System.Drawing.Size(200, 20);
            this.streetTextBox.TabIndex = 9;
            // 
            // subBuildingTextBox
            // 
            this.subBuildingTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "SubBuilding", true));
            this.subBuildingTextBox.Location = new System.Drawing.Point(126, 75);
            this.subBuildingTextBox.Name = "subBuildingTextBox";
            this.subBuildingTextBox.Size = new System.Drawing.Size(200, 20);
            this.subBuildingTextBox.TabIndex = 5;
            // 
            // townTextBox
            // 
            this.townTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "Town", true));
            this.townTextBox.Location = new System.Drawing.Point(126, 179);
            this.townTextBox.Name = "townTextBox";
            this.townTextBox.Size = new System.Drawing.Size(200, 20);
            this.townTextBox.TabIndex = 13;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(126, 269);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 19;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(126, 295);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 22;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(170, 364);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(251, 364);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 25;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // comboBox3
            // 
            this.comboBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "Operation", true));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Normal",
            "Delete",
            "Withdraw"});
            this.comboBox3.Location = new System.Drawing.Point(126, 318);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(144, 21);
            this.comboBox3.TabIndex = 81;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(24, 321);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(56, 13);
            label4.TabIndex = 80;
            label4.Text = "Operation:";
            // 
            // AddressEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 399);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(label4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(addressLinkTypeLabel);
            this.Controls.Add(this.addressLinkTypeComboBox);
            this.Controls.Add(buildingLabel);
            this.Controls.Add(this.buildingTextBox);
            this.Controls.Add(buildingNumberLabel);
            this.Controls.Add(this.buildingNumberTextBox);
            this.Controls.Add(countyLabel);
            this.Controls.Add(this.countyTextBox);
            this.Controls.Add(endOfResidencyLabel);
            this.Controls.Add(this.endOfResidencyDateTimePicker);
            this.Controls.Add(localityLabel);
            this.Controls.Add(this.localityTextBox);
            this.Controls.Add(postCodeLabel);
            this.Controls.Add(this.postCodeTextBox);
            this.Controls.Add(startOfResidencyLabel);
            this.Controls.Add(this.startOfResidencyDateTimePicker);
            this.Controls.Add(streetLabel);
            this.Controls.Add(this.streetTextBox);
            this.Controls.Add(subBuildingLabel);
            this.Controls.Add(this.subBuildingTextBox);
            this.Controls.Add(townLabel);
            this.Controls.Add(this.townTextBox);
            this.Name = "AddressEdit";
            this.Text = "Edit Address Details";
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox addressLinkTypeComboBox;
        private System.Windows.Forms.TextBox buildingTextBox;
        private System.Windows.Forms.TextBox buildingNumberTextBox;
        private System.Windows.Forms.TextBox countyTextBox;
        private System.Windows.Forms.DateTimePicker endOfResidencyDateTimePicker;
        private System.Windows.Forms.TextBox localityTextBox;
        private System.Windows.Forms.TextBox postCodeTextBox;
        private System.Windows.Forms.DateTimePicker startOfResidencyDateTimePicker;
        private System.Windows.Forms.TextBox streetTextBox;
        private System.Windows.Forms.TextBox subBuildingTextBox;
        private System.Windows.Forms.TextBox townTextBox;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.BindingSource addressesBindingSource;
        private System.Windows.Forms.ComboBox comboBox3;
    }
}