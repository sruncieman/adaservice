﻿//using MDA.WCF.WebServices.Messages;

namespace MDA.FileEditor
{
    partial class ClientInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.batchListDataGridView = new System.Windows.Forms.DataGridView();
            this.claimsListDataGridView = new System.Windows.Forms.DataGridView();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.claimsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.batchListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.batchListDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimsListDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchListBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // batchListDataGridView
            // 
            this.batchListDataGridView.AllowUserToAddRows = false;
            this.batchListDataGridView.AllowUserToDeleteRows = false;
            this.batchListDataGridView.AutoGenerateColumns = false;
            this.batchListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.batchListDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.batchListDataGridView.DataSource = this.batchListBindingSource;
            this.batchListDataGridView.Location = new System.Drawing.Point(12, 12);
            this.batchListDataGridView.Name = "batchListDataGridView";
            this.batchListDataGridView.Size = new System.Drawing.Size(1058, 133);
            this.batchListDataGridView.TabIndex = 1;
            // 
            // claimsListDataGridView
            // 
            this.claimsListDataGridView.AllowUserToAddRows = false;
            this.claimsListDataGridView.AllowUserToDeleteRows = false;
            this.claimsListDataGridView.AutoGenerateColumns = false;
            this.claimsListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.claimsListDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.TotalScore,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15});
            this.claimsListDataGridView.DataSource = this.claimsListBindingSource;
            this.claimsListDataGridView.Location = new System.Drawing.Point(12, 151);
            this.claimsListDataGridView.Name = "claimsListDataGridView";
            this.claimsListDataGridView.Size = new System.Drawing.Size(1058, 230);
            this.claimsListDataGridView.TabIndex = 2;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(419, 387);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(651, 361);
            this.treeView1.TabIndex = 3;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.HelpVisible = false;
            this.propertyGrid1.Location = new System.Drawing.Point(12, 387);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.propertyGrid1.Size = new System.Drawing.Size(376, 361);
            this.propertyGrid1.TabIndex = 61;
            this.propertyGrid1.ToolbarVisible = false;
            // 
            // claimsListBindingSource
            // 
            this.claimsListBindingSource.DataSource = typeof(MDA.FileEditor.ADAWebServiceReference.ExRiskClaim);
            this.claimsListBindingSource.CurrentChanged += new System.EventHandler(this.claimsListBindingSource_CurrentChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "BatchId";
            this.dataGridViewTextBoxColumn1.HeaderText = "BatchId";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Reference";
            this.dataGridViewTextBoxColumn2.HeaderText = "Reference";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "UploadedDate";
            this.dataGridViewTextBoxColumn3.HeaderText = "UploadedDate";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "UploadedBy";
            this.dataGridViewTextBoxColumn4.HeaderText = "UploadedBy";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "BatchStatusId";
            this.dataGridViewTextBoxColumn5.HeaderText = "BatchStatusId";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Status";
            this.dataGridViewTextBoxColumn6.HeaderText = "Status";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "BatchStatusValue";
            this.dataGridViewTextBoxColumn7.HeaderText = "BatchStatusValue";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // batchListBindingSource
            // 
            this.batchListBindingSource.DataSource = typeof(MDA.FileEditor.ADAWebServiceReference.ExBatchDetails);
            this.batchListBindingSource.CurrentChanged += new System.EventHandler(this.batchListBindingSource_CurrentChanged);
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "RiskBatch_Id";
            this.dataGridViewTextBoxColumn9.HeaderText = "RiskBatch_Id";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // TotalScore
            // 
            this.TotalScore.DataPropertyName = "TotalScore";
            this.TotalScore.HeaderText = "TotalScore";
            this.TotalScore.Name = "TotalScore";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "MdaClaimRef";
            this.dataGridViewTextBoxColumn10.HeaderText = "MdaClaimRef";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "BaseRiskClaim_Id";
            this.dataGridViewTextBoxColumn11.HeaderText = "BaseRiskClaim_Id";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "CreatedDate";
            this.dataGridViewTextBoxColumn12.HeaderText = "CreatedDate";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "CreatedBy";
            this.dataGridViewTextBoxColumn13.HeaderText = "CreatedBy";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "ClientClaimRefNumber";
            this.dataGridViewTextBoxColumn14.HeaderText = "ClientClaimRefNumber";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Incident_Id";
            this.dataGridViewTextBoxColumn15.HeaderText = "Incident_Id";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // ClientInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 760);
            this.Controls.Add(this.propertyGrid1);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.claimsListDataGridView);
            this.Controls.Add(this.batchListDataGridView);
            this.Name = "ClientInterface";
            this.Text = "ClientInterface";
            ((System.ComponentModel.ISupportInitialize)(this.batchListDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimsListDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.batchListBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource batchListBindingSource;
        private System.Windows.Forms.DataGridView batchListDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.BindingSource claimsListBindingSource;
        private System.Windows.Forms.DataGridView claimsListDataGridView;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;

    }
}