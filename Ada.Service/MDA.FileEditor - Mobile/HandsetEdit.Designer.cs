﻿namespace MDA.FileEditor
{
    partial class HandsetEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label makeLabel;
            System.Windows.Forms.Label modelLabel;
            System.Windows.Forms.Label vINLabel;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            this.handsetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.makeTextBox = new System.Windows.Forms.TextBox();
            this.modelTextBox = new System.Windows.Forms.TextBox();
            this.vINTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.DeviceFaultTextBox = new System.Windows.Forms.TextBox();
            this.HandsetOperationComboBox = new System.Windows.Forms.ComboBox();
            this.HandsetTypeComboBox = new System.Windows.Forms.ComboBox();
            this.I2HLinkTypeComboBox = new System.Windows.Forms.ComboBox();
            this.ValueTextBox = new System.Windows.Forms.TextBox();
            this.ValueCategoryTextBox = new System.Windows.Forms.TextBox();
            this.HandsetColourComboBox = new System.Windows.Forms.ComboBox();
            makeLabel = new System.Windows.Forms.Label();
            modelLabel = new System.Windows.Forms.Label();
            vINLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.handsetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // makeLabel
            // 
            makeLabel.AutoSize = true;
            makeLabel.Location = new System.Drawing.Point(15, 49);
            makeLabel.Name = "makeLabel";
            makeLabel.Size = new System.Drawing.Size(77, 13);
            makeLabel.TabIndex = 14;
            makeLabel.Text = "HandsetMake:";
            // 
            // modelLabel
            // 
            modelLabel.AutoSize = true;
            modelLabel.Location = new System.Drawing.Point(15, 74);
            modelLabel.Name = "modelLabel";
            modelLabel.Size = new System.Drawing.Size(79, 13);
            modelLabel.TabIndex = 16;
            modelLabel.Text = "HandsetModel:";
            // 
            // vINLabel
            // 
            vINLabel.AutoSize = true;
            vINLabel.Location = new System.Drawing.Point(15, 23);
            vINLabel.Name = "vINLabel";
            vINLabel.Size = new System.Drawing.Size(72, 13);
            vINLabel.TabIndex = 6;
            vINLabel.Text = "HandsetIMEI:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(15, 201);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(67, 13);
            label2.TabIndex = 24;
            label2.Text = "DeviceFault:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(15, 316);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(56, 13);
            label3.TabIndex = 26;
            label3.Text = "Operation:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(15, 261);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(74, 13);
            label1.TabIndex = 29;
            label1.Text = "HandsetType:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(15, 289);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(138, 13);
            label4.TabIndex = 31;
            label4.Text = "Incident2HandsetLinkType:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(15, 100);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(80, 13);
            label5.TabIndex = 32;
            label5.Text = "HandsetColour:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(15, 130);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(77, 13);
            label6.TabIndex = 34;
            label6.Text = "HandsetValue:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(17, 155);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(119, 13);
            label7.TabIndex = 36;
            label7.Text = "HandsetValueCategory:";
            // 
            // handsetBindingSource
            // 
            this.handsetBindingSource.DataSource = typeof(MDA.Common.FileModel.Handset);
            // 
            // makeTextBox
            // 
            this.makeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.handsetBindingSource, "Make", true));
            this.makeTextBox.Location = new System.Drawing.Point(161, 45);
            this.makeTextBox.Name = "makeTextBox";
            this.makeTextBox.Size = new System.Drawing.Size(144, 20);
            this.makeTextBox.TabIndex = 15;
            // 
            // modelTextBox
            // 
            this.modelTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.handsetBindingSource, "Model", true));
            this.modelTextBox.Location = new System.Drawing.Point(161, 70);
            this.modelTextBox.Name = "modelTextBox";
            this.modelTextBox.Size = new System.Drawing.Size(144, 20);
            this.modelTextBox.TabIndex = 17;
            // 
            // vINTextBox
            // 
            this.vINTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.handsetBindingSource, "IMEI", true));
            this.vINTextBox.Location = new System.Drawing.Point(161, 19);
            this.vINTextBox.Name = "vINTextBox";
            this.vINTextBox.Size = new System.Drawing.Size(144, 20);
            this.vINTextBox.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(89, 361);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(170, 361);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 21;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // DeviceFaultTextBox
            // 
            this.DeviceFaultTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.handsetBindingSource, "DeviceFault", true));
            this.DeviceFaultTextBox.Location = new System.Drawing.Point(161, 200);
            this.DeviceFaultTextBox.Multiline = true;
            this.DeviceFaultTextBox.Name = "DeviceFaultTextBox";
            this.DeviceFaultTextBox.Size = new System.Drawing.Size(144, 50);
            this.DeviceFaultTextBox.TabIndex = 25;
            // 
            // HandsetOperationComboBox
            // 
            this.HandsetOperationComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.handsetBindingSource, "Operation", true));
            this.HandsetOperationComboBox.FormattingEnabled = true;
            this.HandsetOperationComboBox.Items.AddRange(new object[] {
            "Normal",
            "Delete",
            "Withdraw"});
            this.HandsetOperationComboBox.Location = new System.Drawing.Point(161, 315);
            this.HandsetOperationComboBox.Name = "HandsetOperationComboBox";
            this.HandsetOperationComboBox.Size = new System.Drawing.Size(144, 21);
            this.HandsetOperationComboBox.TabIndex = 27;
            // 
            // HandsetTypeComboBox
            // 
            this.HandsetTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.handsetBindingSource, "HandsetType", true));
            this.HandsetTypeComboBox.FormattingEnabled = true;
            this.HandsetTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Insured",
            "Updated",
            "Replacement",
            "Policy"});
            this.HandsetTypeComboBox.Location = new System.Drawing.Point(161, 257);
            this.HandsetTypeComboBox.Name = "HandsetTypeComboBox";
            this.HandsetTypeComboBox.Size = new System.Drawing.Size(144, 21);
            this.HandsetTypeComboBox.TabIndex = 28;
            // 
            // I2HLinkTypeComboBox
            // 
            this.I2HLinkTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.handsetBindingSource, "Incident2HandsetLinkType", true));
            this.I2HLinkTypeComboBox.FormattingEnabled = true;
            this.I2HLinkTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "PolicyHandset",
            "InsuredHandset",
            "PreviouslyInsuredHandset",
            "ReplacementHandset",
            "LoanHandset",
            "NoHandsetInvolved"});
            this.I2HLinkTypeComboBox.Location = new System.Drawing.Point(161, 285);
            this.I2HLinkTypeComboBox.Name = "I2HLinkTypeComboBox";
            this.I2HLinkTypeComboBox.Size = new System.Drawing.Size(144, 21);
            this.I2HLinkTypeComboBox.TabIndex = 30;
            // 
            // ValueTextBox
            // 
            this.ValueTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.handsetBindingSource, "Value", true));
            this.ValueTextBox.Location = new System.Drawing.Point(161, 124);
            this.ValueTextBox.Name = "ValueTextBox";
            this.ValueTextBox.Size = new System.Drawing.Size(144, 20);
            this.ValueTextBox.TabIndex = 35;
            // 
            // ValueCategoryTextBox
            // 
            this.ValueCategoryTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.handsetBindingSource, "ValueCategory", true));
            this.ValueCategoryTextBox.Location = new System.Drawing.Point(161, 154);
            this.ValueCategoryTextBox.Name = "ValueCategoryTextBox";
            this.ValueCategoryTextBox.Size = new System.Drawing.Size(144, 20);
            this.ValueCategoryTextBox.TabIndex = 37;
            // 
            // HandsetColourComboBox
            // 
            this.HandsetColourComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.handsetBindingSource, "Colour", true));
            this.HandsetColourComboBox.FormattingEnabled = true;
            this.HandsetColourComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Beige",
            "Black",
            "Blue",
            "Bronze",
            "Brown",
            "Cream",
            "Gold",
            "Graphite",
            "Green",
            "Grey",
            "Lilac",
            "Maroon",
            "Mauve",
            "Orange",
            "Pink",
            " Purple",
            "Red",
            "Silver",
            "Turquoise",
            "White",
            "Yellow"});
            this.HandsetColourComboBox.Location = new System.Drawing.Point(161, 97);
            this.HandsetColourComboBox.Name = "HandsetColourComboBox";
            this.HandsetColourComboBox.Size = new System.Drawing.Size(144, 21);
            this.HandsetColourComboBox.TabIndex = 38;
            // 
            // HandsetEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 404);
            this.Controls.Add(this.HandsetColourComboBox);
            this.Controls.Add(this.ValueCategoryTextBox);
            this.Controls.Add(label7);
            this.Controls.Add(this.ValueTextBox);
            this.Controls.Add(label6);
            this.Controls.Add(label5);
            this.Controls.Add(label4);
            this.Controls.Add(this.I2HLinkTypeComboBox);
            this.Controls.Add(label1);
            this.Controls.Add(this.HandsetTypeComboBox);
            this.Controls.Add(this.HandsetOperationComboBox);
            this.Controls.Add(label3);
            this.Controls.Add(this.DeviceFaultTextBox);
            this.Controls.Add(label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(makeLabel);
            this.Controls.Add(this.makeTextBox);
            this.Controls.Add(modelLabel);
            this.Controls.Add(this.modelTextBox);
            this.Controls.Add(vINLabel);
            this.Controls.Add(this.vINTextBox);
            this.Name = "HandsetEdit";
            this.Text = "Edit Handset Details";
            ((System.ComponentModel.ISupportInitialize)(this.handsetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox makeTextBox;
        private System.Windows.Forms.TextBox modelTextBox;
        private System.Windows.Forms.TextBox vINTextBox;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.BindingSource handsetBindingSource;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox DeviceFaultTextBox;
        private System.Windows.Forms.ComboBox HandsetOperationComboBox;
        private System.Windows.Forms.ComboBox HandsetTypeComboBox;
        private System.Windows.Forms.ComboBox I2HLinkTypeComboBox;
        private System.Windows.Forms.TextBox ValueTextBox;
        private System.Windows.Forms.TextBox ValueCategoryTextBox;
        private System.Windows.Forms.ComboBox HandsetColourComboBox;
    }
}