﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MDA.Common.FileModel;

namespace MDA.FileEditor
{
    public partial class IncidentEdit : Form
    {
        public IncidentEdit()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool ok = true;

            if (claimNumberTextBox.Text.Length == 0)
            {
                MessageBox.Show("Must provide claim number");
                ok = false;
            }

            if (incidentDateDateTimePicker.Value == null)
            {
                MessageBox.Show("Must provide incident date");
                ok = false;
            }


            if ( ok )
            {
                MobileClaim v = (MobileClaim)incidentsBindingSource.Current;

                if (!checkBox3.Checked) v.Policy.PolicyStartDate = null;
                if (!checkBox4.Checked) v.Policy.PolicyEndDate = null;

                if (!checkBox5.Checked) v.MobileClaimInfo.ClaimNotificationDate = null;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        private void incidentsBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            MobileClaim v = (MobileClaim)incidentsBindingSource.Current;

            checkBox3.Checked = v.Policy.PolicyStartDate != null;
            checkBox4.Checked = v.Policy.PolicyEndDate != null;

            checkBox5.Checked = v.MobileClaimInfo.ClaimNotificationDate != null;
        }


        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            MobileClaim v = (MobileClaim)incidentsBindingSource.Current;

            policyStartDateDateTimePicker.Enabled = checkBox3.Checked;
            if (checkBox3.Checked && v.Policy.PolicyStartDate == null)
                v.Policy.PolicyStartDate = DateTime.Now;
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            MobileClaim v = (MobileClaim)incidentsBindingSource.Current;

            policyEndDateDateTimePicker.Enabled = checkBox4.Checked;
            if (checkBox4.Checked && v.Policy.PolicyEndDate == null)
                v.Policy.PolicyEndDate = DateTime.Now;
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            MobileClaim i = (MobileClaim)incidentsBindingSource.Current;

            ClaimInfo v = i.MobileClaimInfo;

            claimNotificationDateDateTimePicker.Enabled = checkBox5.Checked;
            if (checkBox5.Checked && v.ClaimNotificationDate == null)
                v.ClaimNotificationDate = DateTime.Now;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
