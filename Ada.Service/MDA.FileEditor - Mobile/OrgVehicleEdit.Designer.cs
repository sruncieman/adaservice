﻿namespace MDA.FileEditor
{
    partial class OrgVehicleEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label colourLabel;
            System.Windows.Forms.Label engineCapacityLabel;
            System.Windows.Forms.Label fuelLabel;
            System.Windows.Forms.Label makeLabel;
            System.Windows.Forms.Label modelLabel;
            System.Windows.Forms.Label transmissionLabel;
            System.Windows.Forms.Label vehicleIncidentLinkLabel;
            System.Windows.Forms.Label vehicleRegistrationLabel;
            System.Windows.Forms.Label vehicleTypeLabel;
            System.Windows.Forms.Label vINLabel;
            System.Windows.Forms.Label hireEndDateLabel;
            System.Windows.Forms.Label hireStartDateLabel;
            System.Windows.Forms.Label label11;
            this.engineCapacityTextBox = new System.Windows.Forms.TextBox();
            this.vehiclesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.makeTextBox = new System.Windows.Forms.TextBox();
            this.modelTextBox = new System.Windows.Forms.TextBox();
            this.vehicleRegistrationTextBox = new System.Windows.Forms.TextBox();
            this.vINTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.colourComboBox = new System.Windows.Forms.ComboBox();
            this.fuelComboBox = new System.Windows.Forms.ComboBox();
            this.transmissionComboBox = new System.Windows.Forms.ComboBox();
            this.vehicleIncidentLinkComboBox = new System.Windows.Forms.ComboBox();
            this.vehicleTypeComboBox = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.hireEndDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.hireStartDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            colourLabel = new System.Windows.Forms.Label();
            engineCapacityLabel = new System.Windows.Forms.Label();
            fuelLabel = new System.Windows.Forms.Label();
            makeLabel = new System.Windows.Forms.Label();
            modelLabel = new System.Windows.Forms.Label();
            transmissionLabel = new System.Windows.Forms.Label();
            vehicleIncidentLinkLabel = new System.Windows.Forms.Label();
            vehicleRegistrationLabel = new System.Windows.Forms.Label();
            vehicleTypeLabel = new System.Windows.Forms.Label();
            vINLabel = new System.Windows.Forms.Label();
            hireEndDateLabel = new System.Windows.Forms.Label();
            hireStartDateLabel = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.vehiclesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // colourLabel
            // 
            colourLabel.AutoSize = true;
            colourLabel.Location = new System.Drawing.Point(26, 126);
            colourLabel.Name = "colourLabel";
            colourLabel.Size = new System.Drawing.Size(40, 13);
            colourLabel.TabIndex = 8;
            colourLabel.Text = "Colour:";
            // 
            // engineCapacityLabel
            // 
            engineCapacityLabel.AutoSize = true;
            engineCapacityLabel.Location = new System.Drawing.Point(26, 152);
            engineCapacityLabel.Name = "engineCapacityLabel";
            engineCapacityLabel.Size = new System.Drawing.Size(87, 13);
            engineCapacityLabel.TabIndex = 10;
            engineCapacityLabel.Text = "Engine Capacity:";
            // 
            // fuelLabel
            // 
            fuelLabel.AutoSize = true;
            fuelLabel.Location = new System.Drawing.Point(26, 178);
            fuelLabel.Name = "fuelLabel";
            fuelLabel.Size = new System.Drawing.Size(30, 13);
            fuelLabel.TabIndex = 12;
            fuelLabel.Text = "Fuel:";
            // 
            // makeLabel
            // 
            makeLabel.AutoSize = true;
            makeLabel.Location = new System.Drawing.Point(26, 204);
            makeLabel.Name = "makeLabel";
            makeLabel.Size = new System.Drawing.Size(37, 13);
            makeLabel.TabIndex = 14;
            makeLabel.Text = "Make:";
            // 
            // modelLabel
            // 
            modelLabel.AutoSize = true;
            modelLabel.Location = new System.Drawing.Point(26, 230);
            modelLabel.Name = "modelLabel";
            modelLabel.Size = new System.Drawing.Size(39, 13);
            modelLabel.TabIndex = 16;
            modelLabel.Text = "Model:";
            // 
            // transmissionLabel
            // 
            transmissionLabel.AutoSize = true;
            transmissionLabel.Location = new System.Drawing.Point(26, 256);
            transmissionLabel.Name = "transmissionLabel";
            transmissionLabel.Size = new System.Drawing.Size(71, 13);
            transmissionLabel.TabIndex = 18;
            transmissionLabel.Text = "Transmission:";
            // 
            // vehicleIncidentLinkLabel
            // 
            vehicleIncidentLinkLabel.AutoSize = true;
            vehicleIncidentLinkLabel.Location = new System.Drawing.Point(26, 17);
            vehicleIncidentLinkLabel.Name = "vehicleIncidentLinkLabel";
            vehicleIncidentLinkLabel.Size = new System.Drawing.Size(95, 13);
            vehicleIncidentLinkLabel.TabIndex = 0;
            vehicleIncidentLinkLabel.Text = "Vehicle Inv Group:";
            // 
            // vehicleRegistrationLabel
            // 
            vehicleRegistrationLabel.AutoSize = true;
            vehicleRegistrationLabel.Location = new System.Drawing.Point(26, 43);
            vehicleRegistrationLabel.Name = "vehicleRegistrationLabel";
            vehicleRegistrationLabel.Size = new System.Drawing.Size(104, 13);
            vehicleRegistrationLabel.TabIndex = 2;
            vehicleRegistrationLabel.Text = "Vehicle Registration:";
            // 
            // vehicleTypeLabel
            // 
            vehicleTypeLabel.AutoSize = true;
            vehicleTypeLabel.Location = new System.Drawing.Point(26, 69);
            vehicleTypeLabel.Name = "vehicleTypeLabel";
            vehicleTypeLabel.Size = new System.Drawing.Size(72, 13);
            vehicleTypeLabel.TabIndex = 4;
            vehicleTypeLabel.Text = "Vehicle Type:";
            // 
            // vINLabel
            // 
            vINLabel.AutoSize = true;
            vINLabel.Location = new System.Drawing.Point(26, 95);
            vINLabel.Name = "vINLabel";
            vINLabel.Size = new System.Drawing.Size(28, 13);
            vINLabel.TabIndex = 6;
            vINLabel.Text = "VIN:";
            // 
            // hireEndDateLabel
            // 
            hireEndDateLabel.AutoSize = true;
            hireEndDateLabel.Location = new System.Drawing.Point(28, 309);
            hireEndDateLabel.Name = "hireEndDateLabel";
            hireEndDateLabel.Size = new System.Drawing.Size(77, 13);
            hireEndDateLabel.TabIndex = 34;
            hireEndDateLabel.Text = "Hire End Date:";
            // 
            // hireStartDateLabel
            // 
            hireStartDateLabel.AutoSize = true;
            hireStartDateLabel.Location = new System.Drawing.Point(28, 283);
            hireStartDateLabel.Name = "hireStartDateLabel";
            hireStartDateLabel.Size = new System.Drawing.Size(80, 13);
            hireStartDateLabel.TabIndex = 31;
            hireStartDateLabel.Text = "Hire Start Date:";
            // 
            // engineCapacityTextBox
            // 
            this.engineCapacityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vehiclesBindingSource, "EngineCapacity", true));
            this.engineCapacityTextBox.Location = new System.Drawing.Point(141, 149);
            this.engineCapacityTextBox.Name = "engineCapacityTextBox";
            this.engineCapacityTextBox.Size = new System.Drawing.Size(144, 20);
            this.engineCapacityTextBox.TabIndex = 11;
            // 
            // vehiclesBindingSource
            // 
            this.vehiclesBindingSource.DataSource = typeof(MDA.Common.FileModel.OrganisationVehicle);
            this.vehiclesBindingSource.CurrentChanged += new System.EventHandler(this.vehiclesBindingSource_CurrentChanged);
            // 
            // makeTextBox
            // 
            this.makeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vehiclesBindingSource, "Make", true));
            this.makeTextBox.Location = new System.Drawing.Point(141, 201);
            this.makeTextBox.Name = "makeTextBox";
            this.makeTextBox.Size = new System.Drawing.Size(144, 20);
            this.makeTextBox.TabIndex = 15;
            // 
            // modelTextBox
            // 
            this.modelTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vehiclesBindingSource, "Model", true));
            this.modelTextBox.Location = new System.Drawing.Point(141, 227);
            this.modelTextBox.Name = "modelTextBox";
            this.modelTextBox.Size = new System.Drawing.Size(144, 20);
            this.modelTextBox.TabIndex = 17;
            // 
            // vehicleRegistrationTextBox
            // 
            this.vehicleRegistrationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vehiclesBindingSource, "VehicleRegistration", true));
            this.vehicleRegistrationTextBox.Location = new System.Drawing.Point(141, 40);
            this.vehicleRegistrationTextBox.Name = "vehicleRegistrationTextBox";
            this.vehicleRegistrationTextBox.Size = new System.Drawing.Size(144, 20);
            this.vehicleRegistrationTextBox.TabIndex = 3;
            // 
            // vINTextBox
            // 
            this.vINTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vehiclesBindingSource, "VIN", true));
            this.vINTextBox.Location = new System.Drawing.Point(141, 92);
            this.vINTextBox.Name = "vINTextBox";
            this.vINTextBox.Size = new System.Drawing.Size(144, 20);
            this.vINTextBox.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(81, 374);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // colourComboBox
            // 
            this.colourComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vehiclesBindingSource, "Colour", true));
            this.colourComboBox.FormattingEnabled = true;
            this.colourComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Beige",
            "Black",
            "Blue",
            "Bronze",
            "Brown",
            "Cream",
            "Gold",
            "Graphite",
            "Green",
            "Grey",
            "Lilac",
            "Maroon",
            "Mauve",
            "Orange",
            "Pink",
            " Purple",
            "Red",
            "Silver",
            "Turquoise",
            "White",
            "Yellow"});
            this.colourComboBox.Location = new System.Drawing.Point(141, 118);
            this.colourComboBox.Name = "colourComboBox";
            this.colourComboBox.Size = new System.Drawing.Size(144, 21);
            this.colourComboBox.TabIndex = 9;
            // 
            // fuelComboBox
            // 
            this.fuelComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vehiclesBindingSource, "Fuel", true));
            this.fuelComboBox.FormattingEnabled = true;
            this.fuelComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Petrol ",
            "Diesel",
            "Electric",
            "Hybrid",
            "LPG "});
            this.fuelComboBox.Location = new System.Drawing.Point(141, 174);
            this.fuelComboBox.Name = "fuelComboBox";
            this.fuelComboBox.Size = new System.Drawing.Size(144, 21);
            this.fuelComboBox.TabIndex = 13;
            // 
            // transmissionComboBox
            // 
            this.transmissionComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vehiclesBindingSource, "Transmission", true));
            this.transmissionComboBox.FormattingEnabled = true;
            this.transmissionComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Manual",
            "Automatic"});
            this.transmissionComboBox.Location = new System.Drawing.Point(141, 252);
            this.transmissionComboBox.Name = "transmissionComboBox";
            this.transmissionComboBox.Size = new System.Drawing.Size(144, 21);
            this.transmissionComboBox.TabIndex = 19;
            // 
            // vehicleIncidentLinkComboBox
            // 
            this.vehicleIncidentLinkComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vehiclesBindingSource, "Incident2VehicleLinkType", true));
            this.vehicleIncidentLinkComboBox.FormattingEnabled = true;
            this.vehicleIncidentLinkComboBox.Items.AddRange(new object[] {
            "Unknown",
            "InsuredVehicle",
            "ThirdPartyVehicle",
            "InsuredHireVehicle",
            "ThirdPartyHireVehicle",
            "WitnessVehicle",
            "NoVehicleInvolved"});
            this.vehicleIncidentLinkComboBox.Location = new System.Drawing.Point(141, 14);
            this.vehicleIncidentLinkComboBox.Name = "vehicleIncidentLinkComboBox";
            this.vehicleIncidentLinkComboBox.Size = new System.Drawing.Size(144, 21);
            this.vehicleIncidentLinkComboBox.TabIndex = 1;
            // 
            // vehicleTypeComboBox
            // 
            this.vehicleTypeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vehiclesBindingSource, "VehicleType", true));
            this.vehicleTypeComboBox.FormattingEnabled = true;
            this.vehicleTypeComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Bicycle",
            "Car",
            "Coach",
            "Lorry",
            "Minibus",
            "Motorcycle",
            "Pickup",
            "Van",
            "Taxi"});
            this.vehicleTypeComboBox.Location = new System.Drawing.Point(141, 66);
            this.vehicleTypeComboBox.Name = "vehicleTypeComboBox";
            this.vehicleTypeComboBox.Size = new System.Drawing.Size(144, 21);
            this.vehicleTypeComboBox.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(162, 374);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 21;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(141, 282);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 32;
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(141, 308);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 35;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // hireEndDateDateTimePicker
            // 
            this.hireEndDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.vehiclesBindingSource, "HireEndDate", true));
            this.hireEndDateDateTimePicker.Enabled = false;
            this.hireEndDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.hireEndDateDateTimePicker.Location = new System.Drawing.Point(159, 305);
            this.hireEndDateDateTimePicker.Name = "hireEndDateDateTimePicker";
            this.hireEndDateDateTimePicker.Size = new System.Drawing.Size(126, 20);
            this.hireEndDateDateTimePicker.TabIndex = 36;
            // 
            // hireStartDateDateTimePicker
            // 
            this.hireStartDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.vehiclesBindingSource, "HireStartDate", true));
            this.hireStartDateDateTimePicker.Enabled = false;
            this.hireStartDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.hireStartDateDateTimePicker.Location = new System.Drawing.Point(159, 279);
            this.hireStartDateDateTimePicker.Name = "hireStartDateDateTimePicker";
            this.hireStartDateDateTimePicker.Size = new System.Drawing.Size(126, 20);
            this.hireStartDateDateTimePicker.TabIndex = 33;
            // 
            // comboBox3
            // 
            this.comboBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vehiclesBindingSource, "Operation", true));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Normal",
            "Delete",
            "Withdraw"});
            this.comboBox3.Location = new System.Drawing.Point(141, 331);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(144, 21);
            this.comboBox3.TabIndex = 75;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(32, 334);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(56, 13);
            label11.TabIndex = 74;
            label11.Text = "Operation:";
            // 
            // OrgVehicleEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 409);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(label11);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(hireEndDateLabel);
            this.Controls.Add(this.hireEndDateDateTimePicker);
            this.Controls.Add(hireStartDateLabel);
            this.Controls.Add(this.hireStartDateDateTimePicker);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.vehicleTypeComboBox);
            this.Controls.Add(this.vehicleIncidentLinkComboBox);
            this.Controls.Add(this.transmissionComboBox);
            this.Controls.Add(this.fuelComboBox);
            this.Controls.Add(this.colourComboBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(colourLabel);
            this.Controls.Add(engineCapacityLabel);
            this.Controls.Add(this.engineCapacityTextBox);
            this.Controls.Add(fuelLabel);
            this.Controls.Add(makeLabel);
            this.Controls.Add(this.makeTextBox);
            this.Controls.Add(modelLabel);
            this.Controls.Add(this.modelTextBox);
            this.Controls.Add(transmissionLabel);
            this.Controls.Add(vehicleIncidentLinkLabel);
            this.Controls.Add(vehicleRegistrationLabel);
            this.Controls.Add(this.vehicleRegistrationTextBox);
            this.Controls.Add(vehicleTypeLabel);
            this.Controls.Add(vINLabel);
            this.Controls.Add(this.vINTextBox);
            this.Name = "OrgVehicleEdit";
            this.Text = "Edit Vehicle Details";
            ((System.ComponentModel.ISupportInitialize)(this.vehiclesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox engineCapacityTextBox;
        private System.Windows.Forms.TextBox makeTextBox;
        private System.Windows.Forms.TextBox modelTextBox;
        private System.Windows.Forms.TextBox vehicleRegistrationTextBox;
        private System.Windows.Forms.TextBox vINTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox colourComboBox;
        private System.Windows.Forms.ComboBox fuelComboBox;
        private System.Windows.Forms.ComboBox transmissionComboBox;
        private System.Windows.Forms.ComboBox vehicleIncidentLinkComboBox;
        private System.Windows.Forms.ComboBox vehicleTypeComboBox;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.BindingSource vehiclesBindingSource;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.DateTimePicker hireEndDateDateTimePicker;
        private System.Windows.Forms.DateTimePicker hireStartDateDateTimePicker;
        private System.Windows.Forms.ComboBox comboBox3;
    }
}