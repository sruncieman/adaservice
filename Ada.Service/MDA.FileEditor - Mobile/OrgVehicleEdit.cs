﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MDA.Common.FileModel;

namespace MDA.FileEditor
{
    public partial class OrgVehicleEdit : Form
    {
        public OrgVehicleEdit()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //if (vehicleRegistrationTextBox.Text.Length == 0)
            //    MessageBox.Show("Must provide reg number");
            //else
            //{
                OrganisationVehicle p = (OrganisationVehicle)vehiclesBindingSource.Current;

                if (!checkBox2.Checked) p.HireEndDate = null;
                if (!checkBox3.Checked) p.HireStartDate = null;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            //}
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            OrganisationVehicle p = (OrganisationVehicle)vehiclesBindingSource.Current;

            hireEndDateDateTimePicker.Enabled = checkBox2.Checked;
            if (checkBox2.Checked && p.HireEndDate == null)
                p.HireEndDate = DateTime.Now;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            OrganisationVehicle p = (OrganisationVehicle)vehiclesBindingSource.Current;

            hireStartDateDateTimePicker.Enabled = checkBox3.Checked;
            if (checkBox3.Checked && p.HireStartDate == null)
                p.HireStartDate = DateTime.Now;
        }

        private void vehiclesBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            OrganisationVehicle p = (OrganisationVehicle)vehiclesBindingSource.Current;

            checkBox2.Checked = p.HireEndDate != null;
            checkBox3.Checked = p.HireStartDate != null;
        }

    }
}
