﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MDA.Common.FileModel;

namespace MDA.FileEditor
{
    public partial class PeopleEdit : Form
    {
        public PeopleEdit()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Person p = (Person)peopleBindingSource.Current;

            dateOfBirthDateTimePicker.Enabled = checkBox1.Checked;
            if (checkBox1.Checked && p.DateOfBirth == null)
                p.DateOfBirth = DateTime.Now;
        }

        //private void checkBox2_CheckedChanged(object sender, EventArgs e)
        //{
        //    Person p = (Person)peopleBindingSource.Current;

        //    ClaimInfo v = p.ClaimInfo;

        //    incidentDateDateTimePicker.Enabled = checkBox2.Checked;
        //    if (checkBox2.Checked && v.IncidentTime == null)
        //        v.IncidentTime = DateTime.Now.TimeOfDay;
        //}


        //private void checkBox3_CheckedChanged(object sender, EventArgs e)
        //{
        //    Person p = (Person)peopleBindingSource.Current;

        //    ClaimInfo v = p.ClaimInfo;

        //    claimNotificationDateDateTimePicker.Enabled = checkBox3.Checked;
        //    if (checkBox3.Checked && v.ClaimNotificationDate == null)
        //        v.ClaimNotificationDate = DateTime.Now;
        //}

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            Person p = (Person)peopleBindingSource.Current;

            datePaymentDetailsTakenDateTimePicker.Enabled = checkBox4.Checked;
            if (checkBox4.Checked && p.BankAccount != null && p.BankAccount.DatePaymentDetailsTaken == null)
                p.BankAccount.DatePaymentDetailsTaken = DateTime.Now;
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            Person p = (Person)peopleBindingSource.Current;

            datePaymentDetailsTakenDateTimePicker1.Enabled = checkBox5.Checked;
            if (checkBox5.Checked && p.PaymentCard != null && p.PaymentCard.DatePaymentDetailsTaken == null)
                p.PaymentCard.DatePaymentDetailsTaken = DateTime.Now;
        }

        private void peopleBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            Person p = (Person)peopleBindingSource.Current;

            checkBox1.Checked = p.DateOfBirth != null;
            //checkBox2.Checked = p.ClaimInfo.IncidentTime != null;
            //checkBox3.Checked = p.ClaimInfo.ClaimNotificationDate != null;

            checkBox4.Checked = p.BankAccount != null && p.BankAccount.DatePaymentDetailsTaken != null;
            checkBox5.Checked = p.PaymentCard != null && p.PaymentCard.DatePaymentDetailsTaken != null;

            chkBank.Checked = p.BankAccount != null;
            chkCard.Checked = p.PaymentCard != null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //if (lastNameTextBox.Text.Length == 0)
            //    MessageBox.Show("Must provide Last Name");
            //else
            //{
                Person p = (Person)peopleBindingSource.Current;

                if (!checkBox1.Checked) p.DateOfBirth = null;
                //if (!checkBox2.Checked) p.ClaimInfo.IncidentTime = null;
                //if (!checkBox3.Checked) p.ClaimInfo.ClaimNotificationDate = null;

                if (!checkBox4.Checked && p.BankAccount != null) p.BankAccount.DatePaymentDetailsTaken = null;
                if (!checkBox5.Checked && p.PaymentCard != null) p.PaymentCard.DatePaymentDetailsTaken = null;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            //}
        }

        private void chkBank_CheckedChanged(object sender, EventArgs e)
        {
            pnlBank.Enabled = chkBank.Checked;

            Person p = (Person)peopleBindingSource.Current;

            if (chkBank.Checked)
            {
                if (p.BankAccount == null)
                    p.BankAccount = new BankAccount();

                
            }
            else
            {
                p.BankAccount = null;
            }
            peopleBindingSource.ResetBindings(false);
        }

        private void chkCard_CheckedChanged(object sender, EventArgs e)
        {
            pnlCard.Enabled = chkCard.Checked;

            Person p = (Person)peopleBindingSource.Current;

            if (chkCard.Checked)
            {
                if (p.PaymentCard == null)
                    p.PaymentCard = new PaymentCard();


            }
            else
            {
                p.PaymentCard = null;
            }
            peopleBindingSource.ResetBindings(false);
        }
    }
}
