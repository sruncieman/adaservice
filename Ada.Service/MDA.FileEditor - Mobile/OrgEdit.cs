﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MDA.Common.FileModel;

namespace MDA.FileEditor
{
    public partial class OrgEdit : Form
    {
        public OrgEdit()
        {
            InitializeComponent();
        }

        public void SetGroupBoxes(bool on)
        {
            grpBank.Visible = on;
            grpCard.Visible = on;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //if (organisationNameTextBox.Text.Length == 0)
            //    MessageBox.Show("Must provide organisation name");
            //else
            //{
                Organisation o = (Organisation)organisationsBindingSource.Current;

                //if (!checkBox2.Checked) o.ClaimInfo.IncidentTime = null;
                //if (!checkBox3.Checked) o.ClaimInfo.ClaimNotificationDate = null;

                if (!checkBox4.Checked && o.BankAccount != null) o.BankAccount.DatePaymentDetailsTaken = null;
                if (!checkBox5.Checked && o.PaymentCard != null) o.PaymentCard.DatePaymentDetailsTaken = null;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            //}
        }


        //private void checkBox2_CheckedChanged(object sender, EventArgs e)
        //{
        //    Organisation o = (Organisation)organisationsBindingSource.Current;

        //    ClaimInfo v = o.ClaimInfo;

        //    incidentDateDateTimePicker.Enabled = checkBox2.Checked;
        //    if (checkBox2.Checked && v.IncidentTime == null)
        //        v.IncidentTime = DateTime.Now.TimeOfDay;
        //}


        //private void checkBox3_CheckedChanged(object sender, EventArgs e)
        //{
        //    Organisation o = (Organisation)organisationsBindingSource.Current;

        //    ClaimInfo v = o.ClaimInfo;

        //    claimNotificationDateDateTimePicker.Enabled = checkBox3.Checked;
        //    if (checkBox3.Checked && v.ClaimNotificationDate == null)
        //        v.ClaimNotificationDate = DateTime.Now;
        //}

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            Organisation o = (Organisation)organisationsBindingSource.Current;

            datePaymentDetailsTakenDateTimePicker.Enabled = checkBox4.Checked;
            if (checkBox4.Checked && o.BankAccount != null && o.BankAccount.DatePaymentDetailsTaken == null)
                o.BankAccount.DatePaymentDetailsTaken = DateTime.Now;
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            Organisation o = (Organisation)organisationsBindingSource.Current;

            datePaymentDetailsTakenDateTimePicker1.Enabled = checkBox5.Checked;
            if (checkBox5.Checked && o.PaymentCard != null && o.PaymentCard.DatePaymentDetailsTaken == null)
                o.PaymentCard.DatePaymentDetailsTaken = DateTime.Now;
        }

        private void organisationsBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            Organisation o = (Organisation)organisationsBindingSource.Current;

            //checkBox2.Checked = o.ClaimInfo.IncidentTime != null;
            //checkBox3.Checked = o.ClaimInfo.ClaimNotificationDate != null;

            checkBox5.Checked = o.PaymentCard != null && o.PaymentCard.DatePaymentDetailsTaken != null;
            checkBox4.Checked = o.BankAccount != null && o.BankAccount.DatePaymentDetailsTaken != null;

            chkBank.Checked = o.BankAccount != null;
            chkCard.Checked = o.PaymentCard != null;
        }

        private void chkBank_CheckedChanged(object sender, EventArgs e)
        {
            pnlBank.Enabled = chkBank.Checked;

            Organisation o = (Organisation)organisationsBindingSource.Current;

            if (chkBank.Checked)
            {
                if (o.BankAccount == null)
                    o.BankAccount = new BankAccount();


            }
            else
            {
                o.BankAccount = null;
            }
            organisationsBindingSource.ResetBindings(false);
        }

        private void chkCard_CheckedChanged(object sender, EventArgs e)
        {
            pnlCard.Enabled = chkCard.Checked;

            Organisation o = (Organisation)organisationsBindingSource.Current;

            if (chkCard.Checked)
            {
                if (o.PaymentCard == null)
                    o.PaymentCard = new PaymentCard();


            }
            else
            {
               o.PaymentCard = null;
            }
            organisationsBindingSource.ResetBindings(false);
        }
    }
}
