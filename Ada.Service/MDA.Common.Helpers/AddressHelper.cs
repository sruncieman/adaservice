﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.DAL;
using MDA.Pipeline.Model;

namespace MDA.Common.Helpers
{
    public class AddressHelper
    {
        public static string CreateRawAddress(PipelineAddress a)
        {
            return string.Format("SB=[{0}],BN=[{1}],BLD=[{2}],ST=[{3}],LOC=[{4}],TWN=[{5}],CNTY=[{6}],PC=[{7}]",
                a.SubBuilding, a.BuildingNumber, a.Building, a.Street, a.Locality, a.Town, a.County, a.PostCode);
        }

        

        public static string FormatAddress( Address address )
        {
            StringBuilder s = new StringBuilder();

            if (address != null)
            {

                if (!string.IsNullOrEmpty(address.SubBuilding))
                {
                    address.SubBuilding.ToString().Trim();
                    s.Append(address.SubBuilding + " ");
                }

                if (!string.IsNullOrEmpty(address.BuildingNumber))
                {
                    address.BuildingNumber.ToString().Trim();
                    s.Append(address.BuildingNumber + " ");
                }

                if (!string.IsNullOrEmpty(address.Building))
                {
                    address.Building.ToString().Trim();
                    s.Append(address.Building + " ");
                }

                if (!string.IsNullOrEmpty(address.Street))
                {
                    address.Street.ToString().Trim();
                    s.Append(address.Street + ", ");
                }

                if (!string.IsNullOrEmpty(address.Locality))
                {
                    address.Locality.ToString().Trim();
                    s.Append(address.Locality + ", ");
                }

                if (!string.IsNullOrEmpty(address.Town))
                {
                    address.Town.ToString().Trim();
                    s.Append(address.Town + ", ");
                }

                if (!string.IsNullOrEmpty(address.County))
                {
                    address.County.ToString().Trim();
                    s.Append(address.County + ", ");
                }

                if (!string.IsNullOrEmpty(address.PostCode))
                {
                    address.PostCode.ToString().Trim();
                    s.Append(address.PostCode + " ");
                }

            }

            return s.ToString();
        }
    }
}
