﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Pipeline.Model;
using MDA.Common.Enum;
using System.Runtime.Serialization;

namespace MDA.Common.Helpers
{


    public class LinqHelper
    {

        public static List<int> GetClaimantPartyTypes()
        {
            return new List<int>()
            {
                (int)PartyType.Claimant,
                (int)PartyType.Hirer,
                (int)PartyType.Insured,
                (int)PartyType.Policyholder,
                (int)PartyType.ThirdParty,
                (int)PartyType.Unknown,
            };
        }

        public static List<int> GetVehicleOccupancyPartyTypes()
        {
            return new List<int>()
            {
                (int)PartyType.Claimant,
                (int)PartyType.Hirer,
                (int)PartyType.ThirdParty,
                (int)PartyType.Unknown,
                (int)PartyType.VehicleOwner,
                (int)PartyType.Insured
            };
        }

        public static List<int> GetVehicleOccupancySubPartyTypes()
        {
            return new List<int>()
            {              
                (int)SubPartyType.Driver,
                (int)SubPartyType.Passenger,
                //(int)SubPartyType.Unknown,
            };
        }

        public static List<int> GetPolicyHolderPartyTypes()
        {
            return new List<int>()
            {              
                (int)PartyType.Policyholder
            };
        }

        public static List<string> GetSettlementCategoryStrings(int category)
        {
            List<string> catStrings = new List<string>();

            switch (category)
            {
                case 1:   // all FRAUD outcomes
                    catStrings.Add("Admissions of Fraud");
                    catStrings.Add("Fraud Proven");
                    catStrings.Add("Repudiated (pre proc only)");
                    catStrings.Add("Repudiated and not heard for 3 months");
                    catStrings.Add("Settled Best Terms - Suspect Fraud but Not Proven");
                    catStrings.Add("Settled Best Terms - Using Fraud Arguments");
                    catStrings.Add("Won at Trial");
                    break;

                case 2:  // all discontinued outcomes
                    catStrings.Add("Discontinued");
                    catStrings.Add("Discontinued (Litigated)");
                    break;

                case 3:  // all struck out outcomes
                    catStrings.Add("Claim Struck-out");
                    break;
            }

            return catStrings;
        }
    }

}
