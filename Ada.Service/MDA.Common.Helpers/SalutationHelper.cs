﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common.Enum;

namespace MDA.Common.Helpers
{
    public static class SalutationHelper
    {
        public static Salutation GetSalutation(string salutation)
        {
            switch (salutation.ToUpper())
            {
                case "MR":
                    return Salutation.Mr;
                case "MRS":
                    return Salutation.Mrs;
                case "MS":
                    return Salutation.Ms;
                case "MISS":
                    return Salutation.Miss;
                case "MASTER":
                    return Salutation.Master;
                case "DR":
                case "DOCTOR":
                    return Salutation.Dr;
                case "PROFESSOR":
                case "PROF":
                    return Salutation.Professor;
                case "REVEREND":
                case "REV":
                    return Salutation.Reverend;
                case "SIR":
                    return Salutation.Sir;
                case "LORD":
                    return Salutation.Lord;
                case "BARONESS":
                    return Salutation.Baroness;
                case "RIGHT HONOURABLE":
                    return Salutation.RightHonourable;
                default:
                    return Salutation.Unknown;
            }
        }
    }
}
