﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.Common.Helpers
{
    public class LookupHelper
    {
        private static string[] VehicleIncidentLinkAsString = new string[]  {
            "Vehicle Involvement Unknown", 
            "Insured Vehicle", 
            "Third Party Vehicle",
            "Insured Hire Vehicle", 
            "Third Party Hire Vehicle",
            "Witness Vehicle", 
            "No Vehicle Involved" 
        };

        public static string VehicleIncidentLinkToString(int vehicleIncidentLink)
        {
            return VehicleIncidentLinkAsString[vehicleIncidentLink];
        }

        private static string[] AddressLinkTypeAsString = new string[]  {
            "Unknown", 
            "Previous Address", 
            "Linked Address",
            "Current Address", 
            "Registered Office",
            "Trading Address", 
            "Storage Address", 
            "",
            "Inspected at",
            "Interviewed at",
            "Lives at",
            "Recovered to",
            "Seen at",
            "Stolen from",
            "Repaired at",
            "Owner",
            "Delivery Address",
            "Pickup Address",
            "Jiffy Bag Address",
        };

        public static string AddressLinkTypeToString(int addressLinkTypeId)
        {
            return AddressLinkTypeAsString[addressLinkTypeId];
        }

        private static string[] OrganisationLinkTypeAsString = new string[]  {
            "Unknown", 
            "Director", 
            "Secretary",
            "Previous Director", 
            "Previous Secretary",
            "Formerly Known As", 
            "Trading Name Of", 
            "Also Known As", 
        };

        public static string OrganisationLinkTypeToString(int organisationLinkTypeId)
        {
            return OrganisationLinkTypeAsString[organisationLinkTypeId];
        }

        private static string[] OrganisationTypeAsString = new string[]  {
            "Unknown", 
            "Policy Holder", 
            "Undefined Supplier",
            "Accident Management", 
            "Credit Hire",
            "Solicitor", 
            "Vehicle Engineer", 
            "Recovery", 
            "Storage", 
            "Broker", 
            "Medical Examiner", 
            "Repairer", 
            "Insurer" 
        };

        public static string OrganisationTypeToString(int organisationTypeId)
        {
            return OrganisationTypeAsString[organisationTypeId];
        }

        private static string[,] PartyTypeReportStrings = new string[,]
        {
            { "Involvement Unknown", "Driver", "Passenger", "Witness" },
            { "Claimant", "Claimant Driver", "Claimant Passenger", "Witness" },
            { "Hirer", "Driver", "Passenger", "Witness" },
            { "Insured", "Insured Driver", "Insured Vehicle Passenger", "Insured's Witness" },
            { "Litigation Friend", "Litigation Friend", "Litigation Friend", "Litigation Friend" },
            { "PolicyHolder", "Insured Driver", "Insured Vehicle Passenger", "Insured's Witness" },
            { "Third Party", "Third Party Driver", "Third Party Vehicle Passenger", "Third Party Witness" },
            { "Witness", "Witness", "Witness", "Witness" },
            { "Vehicle Owner", "Vehicle Owner", "Vehicle Owner", "Vehicle Owner" },
            { "Paid Policy", "Paid Policy", "Paid Policy", "Paid Policy" },
        };

        public static string PartyTypeToString(int partyType, int subPartyType)
        {
            return PartyTypeReportStrings[partyType, subPartyType];
        }

    }
}
