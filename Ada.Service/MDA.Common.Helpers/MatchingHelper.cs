﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Pipeline.Model;
using MDA.Common.Enum;
using System.Runtime.Serialization;

namespace MDA.Common.Helpers
{
    public class MatchingHelper
    {
        public static bool IncludeOrganisationInPersonMatch(Person2OrganisationLinkType linkType)
        {
            switch (linkType)
            {
                case Person2OrganisationLinkType.Director:
                case Person2OrganisationLinkType.Secretary:
                case Person2OrganisationLinkType.Employee:
                case Person2OrganisationLinkType.PreviousDirector:
                case Person2OrganisationLinkType.PreviousSecretary:
                case Person2OrganisationLinkType.PreviousEmployee:
                case Person2OrganisationLinkType.Manager:
                case Person2OrganisationLinkType.Owner:
                case Person2OrganisationLinkType.Partner:
                    return true;

            }

            return false;
        }
    }
}
