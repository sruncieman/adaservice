﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.Common.Helpers
{
    public class DateHelper
    {

        public static string DateOfBirthMessage(DateTime? DateOfBirth)
        {

            string message;

            if (DateOfBirth.HasValue)
            {
                message = "DOB " + string.Format("{0:d/M/yyyy}", DateOfBirth);
            }
            else
            {
                message = "(DOB unknown)";
            }

            return message;
        }

        public static DateTime? ConvertToSqlSmallDateTime(DateTime? dateTime)
        {
            if ( dateTime == null )  return null;

            DateTime t = dateTime.Value;

            if (t.Year < 1900 || t.Year > 2078) return null;

            try
            { 
                return new DateTime(t.Year, t.Month, t.Day, t.Hour, t.Minute, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
        }

        public static DateTime? ConvertToSqlSmallDate(DateTime? dateTime)
        {
            if (dateTime == null) return null;

            DateTime t = dateTime.Value;

            if (t.Year < 1900 || t.Year > 2078) return null;

            try
            {
                return new DateTime(t.Year, t.Month, t.Day, t.Hour, t.Minute, 0).Date;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
        }

        public static int ConvertTimeOffsetToDays( string offset )
        {
            // offset should be number followed by letter D,M,Y

            int num = 0;
            int i = 0;

            if (!char.IsNumber(offset[0]))
                throw new System.ArgumentException("Property time offset argument invalid : [" + offset + "] : Should be number[D|M|Y] eg 23D");

            for (i = 0; i < offset.Length; i++)
            {
                if (char.IsNumber(offset[i]))
                    num = (num * 10) + Convert.ToInt16(offset[i] - '0');
                else
                    break;
            }

            switch( offset[i] )
            {
                case 'd' :
                case 'D' :
                    return num;

                case 'm' :
                case 'M' :
                    return (int)(DateTime.Now - DateTime.Now.AddMonths(-num)).TotalDays;

                case 'y':
                case 'Y':
                    return (int)(DateTime.Now - DateTime.Now.AddYears(-num)).TotalDays;

                default:
                    throw new System.ArgumentException("Property time offset argument invalid : [" + offset + "] : Should be number[D|M|Y] eg 23D" );
            }
        }
    }

    public class PropertyDateRange
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public PropertyDateRange( DateTime? incidentDate, string periodSkip, string periodCount )
        {
            DateTime refDate = (incidentDate != null && periodSkip.ToUpper().EndsWith("I")) ? (DateTime)incidentDate : DateTime.Now;

            EndDate = refDate.AddDays(ConvertTimeOffsetToDays(refDate, periodSkip)).Date;

            EndDate = EndDate.AddHours(23);
            EndDate = EndDate.AddMinutes(59);
            EndDate = EndDate.AddSeconds(59);

            StartDate = EndDate.AddDays(ConvertTimeOffsetToDays(refDate, periodCount)).Date;
        }

        private int ConvertTimeOffsetToDays(DateTime refDate, string offset)
        {
            // offset should be number followed by letter D,M,Y

            int num = 0;
            int i = 0;
            bool negative = true;

            if (offset[i] == '+')
            {
                negative = false;
                i++;
            }
            else if (offset[i] == '-')
            {
                i++;
            }

            if (!char.IsNumber(offset[i]))
                throw new System.ArgumentException("Property time offset argument invalid : [" + offset + "] : Should be number[D|M|Y] eg 23D");

            while (i < offset.Length)
            {
                if (char.IsNumber(offset[i]))
                    num = (num * 10) + Convert.ToInt16(offset[i] - '0');
                else
                    break;

                i++;
            }

            //if (negative)
            //    num = -num;

            switch (offset[i])
            {
                case 'd':
                case 'D':
                    break;

                case 'm':
                case 'M':
                    if ( negative )
                        num = (int)(refDate - refDate.AddMonths(-num)).TotalDays;
                    else
                        num = (int)(refDate.AddMonths(num) - refDate).TotalDays;
                    break;

                case 'y':
                case 'Y':
                    if ( negative )
                        num = (int)(refDate - refDate.AddYears(-num)).TotalDays;
                    else
                        num = (int)(refDate.AddYears(num) - refDate).TotalDays;
                    break;

                default:
                    throw new System.ArgumentException("Property time offset argument invalid : [" + offset + "] : Should be number[D|M|Y] eg 23D");
            }

            return (negative) ? -num : num;
        }

    }

}
