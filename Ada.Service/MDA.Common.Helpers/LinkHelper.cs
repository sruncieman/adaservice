﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Pipeline.Model;
using MDA.Common.Enum;
using System.Runtime.Serialization;

namespace MDA.Common.Helpers
{


    public class LinkHelper
    {
        public static VehicleLinkType GetVehicle2PersonLinkType(PartyType partyType, SubPartyType subPartyType)
        {
            if (subPartyType == SubPartyType.Driver)
                return VehicleLinkType.Driver;

            if (subPartyType == SubPartyType.Passenger)
                return VehicleLinkType.Passenger;

            if (subPartyType == SubPartyType.Witness)
                return VehicleLinkType.Witness;

            if (subPartyType == SubPartyType.Unknown)
            {
                if (partyType == PartyType.Unknown)
                    return VehicleLinkType.Unknown;

                if (partyType == PartyType.Hirer)
                    return VehicleLinkType.Hirer;

                if (partyType == PartyType.Insured)
                    return VehicleLinkType.Insured;

                if (partyType == PartyType.Witness)
                    return VehicleLinkType.Witness;

                if (partyType == PartyType.VehicleOwner)
                    return VehicleLinkType.Owner;
            }

            return VehicleLinkType.Unknown;
        }

        public static PolicyLinkType GetPerson2PolicyLinkType(PartyType partyType, SubPartyType subPartyType, PolicyLinkType policyLinkType)
        {
            if (policyLinkType == PolicyLinkType.Unknown)
            {
                if (partyType == PartyType.Insured && subPartyType == SubPartyType.Driver)
                    return PolicyLinkType.NamedDriver;

                if (partyType == PartyType.Policyholder)
                    return PolicyLinkType.Policyholder;

                if (partyType == PartyType.PaidPolicy)
                    return PolicyLinkType.PaidPolicy;
            }
            else
            {
                return policyLinkType;
            }

            return PolicyLinkType.Unknown;
        }


        /// <summary>
        /// Do we create an Handset2Policy link for given claim and Handset.
        /// 
        /// </summary>
        /// <param name="h"></param>
        /// <returns></returns>
        public static bool CreateHandset2PolicyLink(PipelineHandset h, PipelinePolicy p)
        {
            if (h.I2H_LinkData.Incident2HandsetLinkType_Id == (int)Incident2HandsetLinkType.NoHandsetInvolved) return false;

            return true;
        }

        /// <summary>
        /// Do we create an Vehicle2Policy link for given claim and vehicle.
        /// 
        /// Current rule says "only for insured vehicle"
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static bool CreateVehicle2PolicyLink(PipelineVehicle v, PipelinePolicy p)
        {
            if (v.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.NoVehicleInvolved) return false;

            return v.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle;
        }

        /// <summary>
        /// Do we create an Incident2Vehicle link for given claim and vehicle
        /// 
        /// Current Rule says "always"
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static bool CreateIncident2VehicleLink(PipelineMotorClaim c, PipelineVehicle v)
        {
            //if (v.Incident2VehicleLinkType == Incident2VehicleLinkType.NoVehicleInvolved) return false;

            return true;
        }

        /// <summary>
        /// Do we create an Incident2Handset link for given claim and handset
        /// 
        /// Current Rule says "always"
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static bool CreateIncident2HandsetLink(PipelineMobileClaim c, PipelineHandset h)
        {
            //if (v.Incident2VehicleLinkType == Incident2VehicleLinkType.NoVehicleInvolved) return false;

            return true;
        }
       

        public static int CalculateAge(DateTime? DoB, DateTime? incidentDate)
        {
            if (incidentDate != null && DoB != null)
            {
                int age = incidentDate.Value.Year - DoB.Value.Year;
                if (incidentDate.Value.Month < DoB.Value.Month || (incidentDate.Value.Month == DoB.Value.Month && incidentDate.Value.Day < DoB.Value.Day)) age--;
                return age;
            }

            return -1;
        }
        /// <summary>
        /// Do we create an Vehicle2Address link for given claim and vehicle
        /// 
        /// Current Rule says "always"
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static bool CreateVehicle2AddressLink(PipelineVehicle v, PipelineAddress a)
        {
            if (v.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.NoVehicleInvolved) return false;

            return a.PO2A_LinkData.AddressLinkType_Id == (int)AddressLinkType.StorageAddress;
        }

        /// <summary>
        /// Do we create an Incident2Person link for given person and claim
        /// 
        /// Current Rule says "always"        
        /// </summary>
        /// <param name="c"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public static bool CreateIncident2PersonLink(IPipelineClaim c, PipelinePerson p)
        {
            //if (p.PartyType == PartyType.PaidPolicy) return false;

            return true;
        }

        /// <summary>
        /// Do we create an Person2Policy link for given person and policy
        /// 
        /// Current Rule says "only if party type is policyholder OR party type is insured and sub party is driver"        
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="person"></param>
        /// <returns></returns>
        public static bool CreatePerson2PolicyLink(PipelinePolicy policy, PipelinePerson person)
        {
            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Policyholder)
                return true;

            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.PaidPolicy)
                return true;

            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                return true;

            return false;
        }

        /// <summary>
        /// Do we create an Vehicle2Person link for given person and vehicle
        /// 
        /// Current Rule says "always"        
        /// </summary>
        /// <param name="v"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public static bool CreateVehicle2PersonLink(PipelineVehicle v, PipelinePerson p)
        {
            //if (v.Incident2VehicleLinkType == Incident2VehicleLinkType.NoVehicleInvolved) return false;

            //if (p.PartyType == PartyType.PaidPolicy)
            //    return false;

            ////if (p.PartyType == PartyType.Claimant && p.SubPartyType == SubPartyType.Unknown)
            ////    return false;

            //if (p.PartyType == PartyType.LitigationFriend && p.SubPartyType == SubPartyType.Unknown)
            //    return false;

            ////if (p.PartyType == PartyType.Policyholder && p.SubPartyType == SubPartyType.Unknown)
            ////    return false;
            
            ////if (p.PartyType == PartyType.ThirdParty && p.SubPartyType == SubPartyType.Unknown)
            ////    return false;

            return true;
        }

        public static bool CreateHandset2PersonLink(PipelineHandset h, PipelinePerson p)
        {
            //if (v.Incident2VehicleLinkType == Incident2VehicleLinkType.NoVehicleInvolved) return false;

            //if (p.PartyType == PartyType.PaidPolicy)
            //    return false;

            ////if (p.PartyType == PartyType.Claimant && p.SubPartyType == SubPartyType.Unknown)
            ////    return false;

            //if (p.PartyType == PartyType.LitigationFriend && p.SubPartyType == SubPartyType.Unknown)
            //    return false;

            ////if (p.PartyType == PartyType.Policyholder && p.SubPartyType == SubPartyType.Unknown)
            ////    return false;

            ////if (p.PartyType == PartyType.ThirdParty && p.SubPartyType == SubPartyType.Unknown)
            ////    return false;

            return true;
        }

        public static bool CreateHandset2TelephoneLink(PipelineHandset h, PipelineTelephone t)
        {
            return true;
        }

        public static bool CreateHireVehicle2PersonLink(PipelineOrganisationVehicle v, PipelineOrganisation o)
        {
            return (o.OrganisationType_Id == (int)OrganisationType.CreditHire || o.OrganisationType_Id == (int)OrganisationType.AccidentManagement &&
                                 (v.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Common.Enum.Incident2VehicleLinkType.InsuredHireVehicle ||
                                  v.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Common.Enum.Incident2VehicleLinkType.ThirdPartyHireVehicle));
        }

        public static bool CreatePrimaryVehicle2OrganisationLink(PipelineVehicle v, PipelineOrganisation o)
        {
            if (v.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.NoVehicleInvolved) return false;

            return (o.P2O_LinkData.Person2OrganisationLinkType_Id == (int)Person2OrganisationLinkType.Recovery ||
                    o.P2O_LinkData.Person2OrganisationLinkType_Id == (int)Person2OrganisationLinkType.Storage ||
                    o.P2O_LinkData.Person2OrganisationLinkType_Id == (int)Person2OrganisationLinkType.Engineer ||
                    o.P2O_LinkData.Person2OrganisationLinkType_Id == (int)Person2OrganisationLinkType.Repairer);

        }

        public static bool CreatePrimaryHandset2OrganisationLink(PipelineHandset h, PipelineOrganisation o)
        {
            if (h.I2H_LinkData.Incident2HandsetLinkType_Id == (int)Incident2HandsetLinkType.NoHandsetInvolved) return false;

            return (o.P2O_LinkData.Person2OrganisationLinkType_Id == (int)Person2OrganisationLinkType.Recovery ||
                    o.P2O_LinkData.Person2OrganisationLinkType_Id == (int)Person2OrganisationLinkType.Storage ||
                    o.P2O_LinkData.Person2OrganisationLinkType_Id == (int)Person2OrganisationLinkType.Engineer ||
                    o.P2O_LinkData.Person2OrganisationLinkType_Id == (int)Person2OrganisationLinkType.Repairer);

        }

        public static bool CreatePrimaryVehicle2OrgAddressLink(PipelineVehicle v, PipelineOrganisation o, PipelineAddress a)
        {
            if (v.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.NoVehicleInvolved) return false;

            return (o.OrganisationType_Id == (int)OrganisationType.Storage && a.PO2A_LinkData.AddressLinkType_Id == (int)AddressLinkType.StorageAddress);
        }

        public static bool CreatePrimaryHandset2OrgAddressLink(PipelineHandset h, PipelineOrganisation o, PipelineAddress a)
        {
            if (h.I2H_LinkData.Incident2HandsetLinkType_Id == (int)Incident2HandsetLinkType.NoHandsetInvolved) return false;

            return (o.OrganisationType_Id == (int)OrganisationType.Storage && a.PO2A_LinkData.AddressLinkType_Id == (int)AddressLinkType.StorageAddress);
        }


        /// <summary>
        /// Do we create an Person2Organisation link for given person and organisation
        /// 
        /// Current Rule says "always"        
        /// </summary>
        /// <param name="p"></param>
        /// <param name="o"></param>
        /// <returns></returns>
        public static bool CreatePerson2OrganisationLink(PipelinePerson p, PipelineOrganisation o)
        {
            return true;
        }

        /// <summary>
        /// Do we create an Incident2Organisation link for given claim and organisation and person
        /// 
        /// Current Rule says "always"        
        /// </summary>
        /// <param name="c"></param>
        /// <param name="o"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public static bool CreateIncident2OrganisationLink(IPipelineClaim c, PipelineOrganisation o, PipelinePerson p)
        {
            return true;
        }

        /// <summary>
        /// Do we create an Incident2Organisation link for given claim and organisation
        /// 
        /// Current Rule says "always"        
        /// </summary>
        /// <param name="c"></param>
        /// <param name="o"></param>
        /// <returns></returns>
        public static bool CreateIncident2OrganisationLink(IPipelineClaim c, PipelineOrganisation o)
        {
            return true;
        }

        /// <summary>
        /// Do we create an Policy2BankAccount link for given person and policy and account
        /// 
        /// Current Rule says "only if party type is policy holder or they paid the policy"        
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="person"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public static bool CreatePolicy2BankAccountLink(PipelinePolicy policy, PipelinePerson person, PipelineBankAccount account)
        {
            // account will never come through as NULL

            return (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Policyholder || person.I2Pe_LinkData.PartyType_Id == (int)PartyType.PaidPolicy);
        }

        /// <summary>
        /// Do we create an Policy2PaymentCard link for given person and policy and card
        /// 
        /// Current Rule says "only if party type is policy holder or they paid the policy"        
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="person"></param>
        /// <param name="card"></param>
        /// <returns></returns>
        public static bool CreatePolicy2PaymentCardLink(PipelinePolicy policy, PipelinePerson person, PipelinePaymentCard card)
        {
            // card will never come through as NULL

            return (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Policyholder || person.I2Pe_LinkData.PartyType_Id == (int)PartyType.PaidPolicy);
        }

        /// <summary>
        /// Do we create an Policy2BankAccount link for given organisation and policy and account
        /// 
        /// Current Rule says "always"        
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="org"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public static bool CreatePolicy2BankAccountLink(PipelinePolicy policy, PipelineOrganisation org, PipelineBankAccount account)
        {
            // account will never come through as NULL

            return org.OrganisationType_Id == (int)OrganisationType.PolicyHolder;
        }

        /// <summary>
        /// Do we create an Policy2PaymentCard link for given organisation and policy and card
        /// 
        /// Current Rule says "always"        
        /// </summary>
        /// <param name="policy"></param>
        /// <param name="org"></param>
        /// <param name="card"></param>
        /// <returns></returns>
        public static bool CreatePolicy2PaymentCardLink(PipelinePolicy policy, PipelineOrganisation org, PipelinePaymentCard card)
        {
            // card will never come through as NULL

            return org.OrganisationType_Id == (int)OrganisationType.PolicyHolder;
        }

        /// <summary>
        /// Do we create an Organisaton2Policy link for given organisation and policy
        /// 
        /// Current Rule says "only if party type is policy holder"        
        /// </summary>
        /// <param name="org"></param>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static bool CreateOrganisation2PolicyLink(PipelineOrganisation org, PipelinePolicy policy)
        {
            return org.OrganisationType_Id == (int)OrganisationType.PolicyHolder;
        }
    }
}
