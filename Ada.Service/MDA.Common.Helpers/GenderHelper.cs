﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Common.Enum;

namespace MDA.Common.Helpers
{
    public class GenderHelper
    {
        public static Gender Salutation2Gender(string salutation)
        {
            if ( string.Compare(salutation,"mr",true) == 0 )
                return Gender.Male;

            if ( string.Compare(salutation,"master",true) == 0 )
                return Gender.Male;

            if ( string.Compare(salutation,"sir",true) == 0 )
                return Gender.Male;

            if ( string.Compare(salutation,"lord",true) == 0 )
                return Gender.Male;

            if ( string.Compare(salutation,"mrs",true) == 0 )
                return Gender.Female;

            if ( string.Compare(salutation,"ms",true) == 0 )
                return Gender.Female;

            if ( string.Compare(salutation,"miss",true) == 0 )
                return Gender.Female;

            if ( string.Compare(salutation,"baroness",true) == 0 )
                return Gender.Female;

            return Gender.Unknown;
        }

        public static Gender GetGender(string gender)
        {
            switch (gender.ToUpper())
            {
                case "M":
                    return Gender.Male;
                case "F":
                    return Gender.Female;
                default:
                    return Gender.Unknown;
            }
        }

        public static Gender Salutation2Gender(Salutation salutation)
        {
            switch( salutation )
            {
                case Salutation.Mr :
                case Salutation.Master:
                case Salutation.Sir:
                case Salutation.Lord:
                    return Gender.Male;

                case Salutation.Mrs:
                case Salutation.Miss:
                case Salutation.Ms:
                case Salutation.Baroness:
                    return Gender.Female;

            }
            return Gender.Unknown;
        }

    }
}
