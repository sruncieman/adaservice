﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.Common.Helpers
{
    public class FileNameHelper
    {
        public static string CreateCleanFileName( string clientBatchReference )
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));

            string invalidReStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(clientBatchReference, invalidReStr, "_");
        }
    }
}
