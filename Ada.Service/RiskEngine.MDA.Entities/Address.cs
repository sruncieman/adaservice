﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MDA.Common.Helpers;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using System.Diagnostics;
using MDA.Common.Debug;
using System.Text.RegularExpressions;
using System.Text;
using System.Linq;

namespace RiskEngine.Scoring.Entities
{
    #region Testing Classes
    public class AddressRiskEditExtras
    {
        public int v_NumberOfCases { get; set; }
        public int v_NumberOpenCases { get; set; }
        public int v_SettledCaseCount { get; set; }
        public int v_LinkedSettledCaseCount { get; set; }
        public string v_PropertyType { get; set; }
        public string v_MosaicCode { get; set; }

    }

    public class AddressRiskEdit : AddressRisk
    {


        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();

        public AddressRiskEdit()
        {
            v_AddressLinkType = (int)AddressLinkType.Unknown;
            v_AddressLinkType_Text = v_AddressLinkType.ToString();

            Extras = new AddressRiskEditExtras();
        }

        #region Extras
        public AddressRiskEditExtras Extras;

        //public override int v_NumberOfCasesCount
        //{
        //get { return Extras.v_NumberOfCasesCount; }
        //set { Extras.v_NumberOfCasesCount = value; }
        //}

        public override int v_NumberOfCases(string cacheKey, bool? isPotentialClaimant, string linkType, string incidentType, string claimType, string periodSkip, string periodCount)
        {
            return Extras.v_NumberOfCases;
        }

        public override int v_NumberOpenCases(string cacheKey, bool? isPotentialClaimant, string linkType, string keoghsCaseLinkType, bool stagedContrived, string periodSkip, string periodCount)
        {
            return Extras.v_NumberOpenCases;
        }

        public override int v_SettledCaseCount(string cacheKey, bool? isPotentialClaimant, string linkType, int category, string periodSkip, string periodCount)
        {
            return Extras.v_SettledCaseCount;
        }

        public override int v_LinkedSettledCaseCount(string cacheKey, bool? isPotentialClaimant, string linkType, int category, int claimTypeId, string periodSkip, string periodCount)
        {
            return Extras.v_LinkedSettledCaseCount;
        }

        public override string v_PropertyType
        {
            get { return Extras.v_PropertyType; }
            set { Extras.v_PropertyType = value; }
        }

        public override string v_MosaicCode
        {
            get { return Extras.v_MosaicCode; }
            set { Extras.v_MosaicCode = value; }
        }


        #endregion

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, int KeyAttCount, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
                ClientScores[orgName].RuleGroupKeyAttractorTotalCount = KeyAttCount;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }


        public AddressRiskEdit CloneEntity(bool cloneChildren)
        {
            AddressRiskEdit c = new AddressRiskEdit();

            c.v_AddressLinkType = v_AddressLinkType;
            c.v_AddressLinkType_Text = v_AddressLinkType_Text;
            c.v_StartOfResidency = v_StartOfResidency;
            c.v_EndOfResidency = v_EndOfResidency;
            c.v_SubBuilding = v_SubBuilding;
            c.v_Building = v_Building;
            c.v_BuildingNumber = v_BuildingNumber;
            c.v_Street = v_Street;
            c.v_Locality = v_Locality;
            c.v_Town = v_Town;
            c.v_County = v_County;
            c.v_Postcode = v_Postcode;
            //c.v_IsKeyAttractor           = v_IsKeyAttractor;
            c.v_IsPostCodeKeyAttractor = v_IsPostCodeKeyAttractor;
            //c.v_NumberOfCases          = v_NumberOfCases;
            c.v_PafValidation = v_PafValidation;
            c.v_PropertyType = v_PropertyType;
            c.v_MosaicCode = v_MosaicCode;
            c.v_IncidentDate = v_IncidentDate;

            return c;
        }

    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class AddressRisk : RiskEntityBase, IRiskEntity
    {
        [DataMember(Name = "Addr_DbId")]
        public int Db_Id { get; set; }

        [DataMember(Name = "Addr_DbIncId")]
        public int Db_Incident_Id { get; set; }

        [DataMember(Name = "v_IsConf")]
        public bool v_IsConfirmed { get; set; }

        [DataMember(Name = "v_ALT")]
        public int v_AddressLinkType { get; set; }
        [DataMember(Name = "v_ALTt")]
        public string v_AddressLinkType_Text { get; set; }

        [DataMember(Name = "v_SoR")]
        public DateTime? v_StartOfResidency { get; set; }
        [DataMember(Name = "v_EoR")]
        public DateTime? v_EndOfResidency { get; set; }

        [DataMember(Name = "v_SB")]
        public string v_SubBuilding { get; set; }
        [DataMember(Name = "v_B")]
        public string v_Building { get; set; }
        [DataMember(Name = "v_BN")]
        public string v_BuildingNumber { get; set; }
        [DataMember(Name = "v_S")]
        public string v_Street { get; set; }
        [DataMember(Name = "v_L")]
        public string v_Locality { get; set; }
        [DataMember(Name = "v_T")]
        public string v_Town { get; set; }
        [DataMember(Name = "v_C")]
        public string v_County { get; set; }
        [DataMember(Name = "v_PC")]
        public string v_Postcode { get; set; }
        //[DataMember(Name = "v_IsKA")]
        //public bool v_IsKeyAttractor { get; set; }

        public bool? v_IsKeyAttractor(string cacheKey)
        {
            string key = string.Format("IsKeyAttractor: P1:{0}", this.Db_Id.ToString());

            bool result = false;

            if (_trace) ADATrace.WriteLine("Address." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                //EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Address_IsKeyAttractor(this.Db_Id);

                result = (recs != null && recs.Count > 0);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);


                foreach (var row in recs)
                {
                    try
                    {

                        string s = string.Format(formatString, (MDA.Common.Helpers.AddressHelper.FormatAddress(row.Address)), row.KeyAttractor);

                        MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                    }
                    catch (System.FormatException)
                    {
                        MessageCache.AddMediumOutputMessage(cacheKey, "IsKeyAttractor Format Error", null);
                    }
                }


                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Address." + key + " = " + result.ToString());

            return result;
        }

        [DataMember(Name = "v_PafV")]
        public bool? v_PafValidation { get; set; }
        [DataMember(Name = "v_PT")]
        public virtual string v_PropertyType { get; set; }
        [DataMember(Name = "v_MC")]
        public virtual string v_MosaicCode { get; set; }

        [DataMember(Name = "v_Id")]
        public virtual DateTime v_IncidentDate { get; set; }

        [DataMember(Name = "v_POCATA")]
        public ListOfInts PeopleOnClaimAndTheirAliasIds { get; set; }

        private string _v_PostCodeKeyAttractor;

        [DataMember(Name = "v_PCKA")]
        public virtual string v_PostCodeKeyAttractor
        {
            get
            {
                if (Locked)
                {
                    string x = (_v_PostCodeKeyAttractor == null) ? "NA" : (string)_v_PostCodeKeyAttractor;

                    if (_trace)
                        ADATrace.WriteLine("Person.v_PostCodeKeyAttractor. Serializing [" + x + "]");

                    return x;
                }

                if (_trace) ADATrace.WriteLine("Address.v_PostCodeKeyAttractor");

                if (_v_PostCodeKeyAttractor == null)
                {
                    _v_PostCodeKeyAttractor = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Address_PostCodeKeyAttractor(this.Db_Id);
                }

                if (_trace) ADATrace.WriteLine("Address.v_PostCodeKeyAttractor = " + _v_PostCodeKeyAttractor);

                return _v_PostCodeKeyAttractor;
            }
            set
            {
                _v_PostCodeKeyAttractor = value;
            }
        }

        private bool? _v_IsPostCodeKeyAttractor;

        [DataMember(Name = "v_IsPCKA")]
        public virtual bool? v_IsPostCodeKeyAttractor
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_IsPostCodeKeyAttractor == null) ? "NA" : _v_IsPostCodeKeyAttractor.ToString();

                        ADATrace.WriteLine("Person.v_IsPostCodeKeyAttractor. Serializing [" + x.ToString() + "]");
                    }

                    return _v_IsPostCodeKeyAttractor;
                }

                if (_trace) ADATrace.WriteLine("Address.v_IsPostCodeKeyAttractor");

                if (_v_PostCodeKeyAttractor == null)
                {
                    _v_IsPostCodeKeyAttractor = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Address_IsPostCodeKeyAttractor(this.Db_Id);
                }

                if (_trace) ADATrace.WriteLine("Address.v_IsPostCodeKeyAttractor = " + _v_IsPostCodeKeyAttractor.ToString());

                return (bool)_v_IsPostCodeKeyAttractor;
            }
            set
            {
                _v_IsPostCodeKeyAttractor = value;
            }
        }

        public string v_InvolvementType(string PartyType, string SubPartyType)
        {

            string Message = PartyType += SubPartyType;

            switch (Message)
            {
                case "UnknownUnknown":
                    return "Unknown";
                case "UnknownDriver":
                    return "Driver";
                case "UnknownPassenger":
                    return "Passenger";
                case "UnknownWitness":
                    return "Witness";
                case "ClaimantUnknown":
                    return "Claimant";
                case "ClaimantDriver":
                    return "Claimant Driver";
                case "ClaimantPassenger":
                    return "Claimant Passenger";
                case "ClaimantWitness":
                    return "Witness";
                case "HirerUnknown":
                    return "Vehicle Hirer";
                case "HirerDriver":
                    return "Driver";
                case "HirerPassenger":
                    return "Passenger";
                case "HirerWitness":
                    return "Witness";
                case "InsuredUnknown":
                    return "Insured";
                case "InsuredDriver":
                    return "Insured Driver";
                case "InsuredPassenger":
                    return "Insured Passenger";
                case "InsuredWitness":
                    return "Witness";
                case "Litigation FriendUnknown":
                    return "Litigation Friend";
                case "Litigation FriendDriver":
                    return "Litigation Friend";
                case "Litigation FriendPassenger":
                    return "Litigation Friend";
                case "Litigation FriendWitness":
                    return "Litigation Friend";
                case "PolicyholderUnknown":
                    return "Policyholder";
                case "PolicyholderDriver":
                    return "Insured Driver";
                case "PolicyholderPassenger":
                    return "Insured Passenger";
                case "PolicyholderWitness":
                    return "Witness";
                case "Third PartyUnknown":
                    return "Third Party";
                case "Third PartyDriver":
                    return "Third Party Driver";
                case "Third PartyPassenger":
                    return "Third Party Passenger";
                case "Third PartyWitness":
                    return "Witness";
                case "Vehicle OwnerUnknown":
                    return "Vehicle Owner";
                case "Vehicle OwnerDriver":
                    return "Vehicle Owner";
                case "Vehicle OwnerPassenger":
                    return "Vehicle Owner";
                case "Vehicle OwnerWitness":
                    return "Vehicle Owner";
                case "Paid PolicyUnknown":
                    return "Paid the policy of the insured driver";
                case "Paid PolicyDriver":
                    return "Paid the policy of the insured driver";
                case "Paid PolicyPassenger":
                    return "Paid the policy of the insured driver";
                case "Paid PolicyWitness":
                    return "Paid the policy of the insured driver";

                default:
                    return "Unknown";
            }
        }

        public string v_PartyTypeMessage2(string PartyType, string SubPartyType)
        {

            string Message = PartyType += SubPartyType;

            switch (Message)
            {
                case "UnknownUnknown":
                    return "involved";
                case "UnknownDriver":
                    return "a driver";
                case "UnknownPassenger":
                    return "a passenger";
                case "UnknownWitness":
                    return "a witness";
                case "ClaimantUnknown":
                    return "a claimant";
                case "ClaimantDriver":
                    return "a claimant driver";
                case "ClaimantPassenger":
                    return "a claimant passenger";
                case "ClaimantWitness":
                    return "a witness";
                case "HirerUnknown":
                    return "a vehicle hirer";
                case "HirerDriver":
                    return "a driver";
                case "HirerPassenger":
                    return "a passenger";
                case "HirerWitness":
                    return "a witness";
                case "InsuredUnknown":
                    return "the insured";
                case "InsuredDriver":
                    return "the insured driver";
                case "InsuredPassenger":
                    return "a passenger in the insured vehicle";
                case "InsuredWitness":
                    return "a witness";
                case "Litigation FriendUnknown":
                    return "a litigation friend";
                case "Litigation FriendDriver":
                    return "a litigation friend";
                case "Litigation FriendPassenger":
                    return "a litigation friend";
                case "Litigation FriendWitness":
                    return "a litigation friend";
                case "PolicyholderUnknown":
                    return "the policyholder";
                case "PolicyholderDriver":
                    return "the insured driver";
                case "PolicyholderPassenger":
                    return "a passenger in the insured vehicle";
                case "PolicyholderWitness":
                    return "a witness";
                case "Third PartyUnknown":
                    return "a third party";
                case "Third PartyDriver":
                    return "a third party driver";
                case "Third PartyPassenger":
                    return "a passenger in the third party vehicle";
                case "Third PartyWitness":
                    return "a witness";
                case "Vehicle OwnerUnknown":
                    return "the owner of a vehicle that involved";
                case "Vehicle OwnerDriver":
                    return "the owner of a vehicle that involved";
                case "Vehicle OwnerPassenger":
                    return "the owner of a vehicle that involved";
                case "Vehicle OwnerWitness":
                    return "the owner of a vehicle that involved";
                case "Paid PolicyUnknown":
                    return "paid the policy of the insured driver";
                case "Paid PolicyDriver":
                    return "paid the policy of the insured driver";
                case "Paid PolicyPassenger":
                    return "paid the policy of the insured driver";
                case "Paid PolicyWitness":
                    return "paid the policy of the insured driver";

                default:
                    return "Unknown";
            }
        }

        public string v_ClaimTypeMessage(string ClaimType)
        {
            switch (ClaimType)
            {
                case "NA":
                    return "fraudulent claim";
                case "Commercial":
                    return "fraudulent commercial claim";
                case "Employer's Liability":
                    return "fraudulent employer's liability claim";
                case "Financial":
                    return "fraudulent financial claim";
                case "Holiday":
                    return "fraudulent travel claim";
                case "Household":
                    return "fraudulent household claim";
                case "Occupiers Liability":
                    return "fraudulent occupiers liability claim";
                case "Public Liability":
                    return "fraudulent public liability claim";
                case "Bogus":
                    return "bogus motor collision";
                case "Credit Hire":
                    return "fraudulent credit hire claim";
                case "Exaggerated Loss":
                    return "exaggerated loss claim";
                case "Fire / Theft":
                    return "fraudulent vehicle fire / theft claim";
                case "Fire/Theft":
                    return "fraudulent vehicle fire / theft claim";
                case "General":
                    return "fraudulent motor claim";
                case "Induced Collision":
                    return "induced motor collision";
                case "Intel + Report":
                    return "fraudulent motor claim";
                case "Intel Only":
                    return "fraudulent motor claim";
                case "Low Speed Impact":
                    return "LSI collision";
                case "Staged/Contrived":
                    return "staged/contrived collision";
                case "RTA - Bogus":
                    return "bogus motor collision";
                case "RTA - Credit Hire":
                    return "fraudulent credit hire claim";
                case "RTA - Exaggerated Loss":
                    return "exaggerated loss claim";
                case "RTA - Fire / Theft":
                    return "fraudulent vehicle fire / theft claim";
                case "RTA - Fire/Theft":
                    return "fraudulent vehicle fire / theft claim";
                case "RTA - General":
                    return "fraudulent motor claim";
                case "RTA - Induced Collision":
                    return "induced motor collision";
                case "RTA - Intel + Report":
                    return "fraudulent motor claim";
                case "RTA - Intel Only":
                    return "fraudulent motor claim";
                case "RTA - Low Speed Impact":
                    return "LSI collision";
                case "RTA - Staged/Contrived":
                    return "staged/contrived collision";
                default:
                    return "fraudulent claim";
            }
        }

        //public Dictionary<string, object> MethodResultHistory;

        public AddressRisk()
        {
            // MethodResultHistory = new Dictionary<string, object>();
        }


        public virtual int v_NumberOfCases(string cacheKey, bool? isPotentialClaimant, string linkType, string incidentType, string claimType, string periodSkip, string periodCount)
        {
            string key = string.Format("NOfCases: {0}, {1}, {2}, {3}, {4}, {5}, {6}", this.Db_Id, isPotentialClaimant.ToString(), linkType, incidentType, claimType, periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Address." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                string caseLinkType = "Instruction";
                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Address_NumberOfCases(this.Db_Id, this.RiskClaimId, linkType, isPotentialClaimant, this.v_IncidentDate, this.PeopleOnClaimAndTheirAliasIds, incidentType, claimType, periodSkip, periodCount, cacheKey, MessageCache, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {
                    result = recs.Count;


                    foreach (var row in recs)
                    {

                        try
                        {

                            MDA.DAL.Address address = new MDA.DAL.Address();

                            address.Building = this.v_Building;
                            address.BuildingNumber = this.v_BuildingNumber;
                            address.County = this.v_County;
                            address.Locality = this.v_Locality;
                            address.PostCode = this.v_Postcode;
                            address.Street = this.v_Street;
                            address.SubBuilding = this.v_SubBuilding;


                            string s = string.Format(formatString, MDA.Common.Helpers.AddressHelper.FormatAddress(address), row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth), v_PartyTypeMessage2(row.PartyTypeText, row.SubPartyText), string.Format("{0:d/M/yyyy}", row.IncidentDate.Date), v_ClaimTypeMessage(row.LinkedClaimType), row.EliteReference, row.FiveGrading);


                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "Address_NumberOfCases Format Error", null);
                        }

                    }


                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Address." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_NumberOfIncidents(string cacheKey, string linkType, string incidentType, string claimType, string periodSkip, string periodCount)
        {
            string key = string.Format("NOfIncidents: {0}, {1}, {2}, {3}, {4}, {5}, {6}", cacheKey, this.Db_Id, linkType, incidentType, claimType, periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Address." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Address_NumberOfIncidents(this.Db_Id, this.RiskClaimId, linkType, this.v_IncidentDate, this.PeopleOnClaimAndTheirAliasIds, incidentType, claimType, periodSkip, periodCount, cacheKey, MessageCache, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {

                    result = (from x in recs select x.Id).Distinct().Count();

                    foreach (var row in recs)
                    {

                        try
                        {

                            ListOfStringData newRow = new ListOfStringData();

                            newRow.Add(row.LinkedClaimType);
                            newRow.Add(string.Format("{0:d/M/yyyy}", row.IncidentDate.Date));
                            newRow.Add(row.Insurer);
                            newRow.Add(row.InsurerClaimRef);
                            newRow.Add(row.FirstName + " " + row.LastName + " " + MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth));
                            newRow.Add(reverseStringFormat(row.RawAddress));
                            newRow.Add(v_InvolvementType(row.PartyTypeText, row.SubPartyText));

                            MessageCache.AddMediumOutputData(cacheKey, newRow, null);


                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "Address_NumberOfIncidents Format Error", null);
                        }

                    }

                    PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Address." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_NumberOfCUEIncidents(string cacheKey, string periodSkip, string periodCount)
        {
            string key = string.Format("NOfIncidents: {0}, {1}, {2}, {3}", cacheKey, this.Db_Id, periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Address." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Address_NumberOfCUEIncidents(this.Db_Id, this.RiskClaimId, this.v_IncidentDate, periodSkip, periodCount, MessageCache, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {

                    result = (from x in recs select x.Id).Distinct().Count();

                    foreach (var row in recs)
                    {

                        try
                        {

                            ListOfStringData newRow = new ListOfStringData();

                            newRow.Add(row.LinkedClaimType);
                            newRow.Add(string.Format("{0:d/M/yyyy}", row.IncidentDate.Date));
                            newRow.Add(row.Insurer);
                            newRow.Add(row.InsurerClaimRef);
                            newRow.Add(row.FirstName + " " + row.LastName + " " + MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth));
                            newRow.Add(reverseStringFormat(row.RawAddress));
                            newRow.Add(v_InvolvementType(row.PartyTypeText, row.SubPartyText));

                            MessageCache.AddMediumOutputData(cacheKey, newRow, null);


                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "Address_NumberOfCUEIncidents Format Error", null);
                        }

                    }

                    PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Address." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_NumberOpenCases(string cacheKey, bool? isPotentialClaimant, string linkType, string keoghsCaseLinkType, bool stagedContrived, string periodSkip, string periodCount)
        {
            string key = string.Format("NOfOpenCases: {0}, {1}, {2}, {3}, {4}, {5}, {6}", this.Db_Id, isPotentialClaimant.ToString(), linkType, keoghsCaseLinkType, stagedContrived, periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Address." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Address_NumberOpenCases(this.Db_Id, this.RiskClaimId, linkType, keoghsCaseLinkType, stagedContrived, isPotentialClaimant, this.v_IncidentDate, this.PeopleOnClaimAndTheirAliasIds, periodSkip, periodCount, cacheKey, MessageCache, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {
                    result = recs.Count;


                    foreach (var row in recs)
                    {

                        try
                        {

                            MDA.DAL.Address address = new MDA.DAL.Address();

                            address.Building = this.v_Building;
                            address.BuildingNumber = this.v_BuildingNumber;
                            address.County = this.v_County;
                            address.Locality = this.v_Locality;
                            address.PostCode = this.v_Postcode;
                            address.Street = this.v_Street;
                            address.SubBuilding = this.v_SubBuilding;


                            string s = string.Format(formatString, MDA.Common.Helpers.AddressHelper.FormatAddress(address), row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth), v_PartyTypeMessage2(row.PartyTypeText, row.SubPartyText), string.Format("{0:d/M/yyyy}", row.IncidentDate.Date), row.FiveGrading);


                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "Address_NumberOpenCases Format Error", null);
                        }

                    }


                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Address." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_SettledCaseCount(string cacheKey, bool? isPotentialClaimant, string linkType, int category, string periodSkip, string periodCount)
        {
            string key = string.Format("SettledCaseCount: {0}, {1}, {2}, {3}, {4}, {5}", this.Db_Id, isPotentialClaimant.ToString(), linkType, category, periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Address." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Address_SettledCaseCount(this.Db_Id, this.Db_Incident_Id, this.RiskClaimId, linkType, category, isPotentialClaimant, this.v_IncidentDate, this.PeopleOnClaimAndTheirAliasIds, periodSkip, periodCount, cacheKey, MessageCache, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {
                    result = recs.Count;


                    foreach (var row in recs)
                    {

                        try
                        {

                            MDA.DAL.Address address = new MDA.DAL.Address();

                            address.Building = this.v_Building;
                            address.BuildingNumber = this.v_BuildingNumber;
                            address.County = this.v_County;
                            address.Locality = this.v_Locality;
                            address.PostCode = this.v_Postcode;
                            address.Street = this.v_Street;
                            address.SubBuilding = this.v_SubBuilding;


                            string s = string.Format(formatString, MDA.Common.Helpers.AddressHelper.FormatAddress(address), row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth), v_PartyTypeMessage2(row.PartyTypeText, row.SubPartyText), string.Format("{0:d/M/yyyy}", row.IncidentDate.Date));


                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "Address_NumberOpenCases Format Error", null);
                        }

                    }


                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Address." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_LinkedSettledCaseCount(string cacheKey, bool? isPotentialClaimant, string linkType, int category, int claimTypeId, string periodSkip, string periodCount)
        {
            string key = string.Format("LinkedSettledCaseCount: {0}, {1}, {2}, {3}, {4}, {5}", this.Db_Id, isPotentialClaimant.ToString(), linkType, category, periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Address." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Address_LinkedSettledCaseCount(this.Db_Id, this.RiskClaimId, linkType, category, claimTypeId, isPotentialClaimant, this.v_IncidentDate, this.PeopleOnClaimAndTheirAliasIds, periodSkip, periodCount, cacheKey, MessageCache, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {
                    result = recs.Count;


                    foreach (var row in recs)
                    {

                        try
                        {

                            MDA.DAL.Address address = new MDA.DAL.Address();

                            address.Building = this.v_Building;
                            address.BuildingNumber = this.v_BuildingNumber;
                            address.County = this.v_County;
                            address.Locality = this.v_Locality;
                            address.PostCode = this.v_Postcode;
                            address.Street = this.v_Street;
                            address.SubBuilding = this.v_SubBuilding;


                            string s = string.Format(formatString, MDA.Common.Helpers.AddressHelper.FormatAddress(address), row.LinkedFirstName, row.LinkedLastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.LinkedDateOfBirth), v_PartyTypeMessage2(row.LinkedPartyType, row.LinkedSubPartyType), string.Format("{0:d/M/yyyy}", row.LinkedIncidentDate));


                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "Address_LinkedSettledCaseCount Format Error", null);
                        }

                    }


                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Address." + key + " = " + result.ToString());

            return result;
        }

        private string reverseStringFormat(string str)
        {

            string value = "";
            string ret = "";

            if (!string.IsNullOrEmpty(str))
            {

                MatchCollection match = Regex.Matches(str, @"\[(.*?)\]");

                foreach (var m in match)
                {
                    value = m.ToString();
                    value = value.Replace("[", string.Empty).Replace("]", string.Empty);
                    value = value.Trim();
                    if (!string.IsNullOrEmpty(value))
                    {
                        ret += value + ", ";
                    }
                }

                ret = ret.Trim();
                ret = ret.TrimEnd(',');

            }

            return ret;
        }

        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        public override void AssignUsedProperties(IRiskEntity dest)
        {
            AddressRisk d = dest as AddressRisk;

            if (_v_IsPostCodeKeyAttractor != null)
                d.v_IsPostCodeKeyAttractor = v_IsPostCodeKeyAttractor;

            if (_v_PostCodeKeyAttractor != null)
                d.v_PostCodeKeyAttractor = v_PostCodeKeyAttractor;
        }

        protected AddressRisk ShallowCopyThis(AddressRisk c)
        {
            c.db = db;

            c.Db_Id = Db_Id;
            c.Db_Incident_Id = Db_Incident_Id;
            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;

            c.v_AddressLinkType = v_AddressLinkType;
            c.v_AddressLinkType_Text = v_AddressLinkType_Text;
            c.v_StartOfResidency = v_StartOfResidency;
            c.v_EndOfResidency = v_EndOfResidency;
            c.v_SubBuilding = v_SubBuilding;
            c.v_Building = v_Building;
            c.v_BuildingNumber = v_BuildingNumber;
            c.v_Street = v_Street;
            c.v_Locality = v_Locality;
            c.v_Town = v_Town;
            c.v_County = v_County;
            c.v_Postcode = v_Postcode;
            //c.v_IsPostCodeKeyAttractor   = v_IsPostCodeKeyAttractor;
            //c.v_IsKeyAttractor           = v_IsKeyAttractor;
            //c.v_NumberOfCases          = v_NumberOfCases;
            c.v_PafValidation = v_PafValidation;
            c.v_PropertyType = v_PropertyType;
            c.v_MosaicCode = v_MosaicCode;
            c.v_IncidentDate = v_IncidentDate;

            c.PeopleOnClaimAndTheirAliasIds = new ListOfInts();
            if (PeopleOnClaimAndTheirAliasIds != null)
                c.PeopleOnClaimAndTheirAliasIds.AddRange(PeopleOnClaimAndTheirAliasIds);

            //foreach (var x in PeopleOnClaimAndTheirAliasIds)
            //    c.PeopleOnClaimAndTheirAliasIds.Add(x);

            return c;
        }

        public override object Clone()
        {
            if (_trace)
            {
                ADATrace.WriteLine("Cloning Address [" + Db_Id.ToString() + "]");
            }

            AddressRisk c = new AddressRisk();

            ShallowCopyThis(c);

            if (_trace)
            {
                ADATrace.WriteLine("Cloning Address Complete");
            }

            return c;
        }

        //[DataMember]
        //public override int? AutoKACount
        //{
        //    get 
        //    {
        //        int total = 0;
        //        if (this.v_IsKeyAttractor) total += 1;
        //        if (this.v_IsPostCodeKeyAttractor) total += 1;

        //        return total;
        //    }
        //    //set;
        //}
        [DataMember]
        public override string DisplayText
        {
            get
            {
                StringBuilder r = new StringBuilder("Address:");


                if (!string.IsNullOrEmpty(v_SubBuilding))
                    r.Append(" " + v_SubBuilding);

                if (!string.IsNullOrEmpty(v_Building))
                    r.Append(" " + v_Building);

                if (!string.IsNullOrEmpty(v_BuildingNumber))
                    r.Append(" " + v_BuildingNumber);

                if (!string.IsNullOrEmpty(v_Street))
                    r.Append(" " + v_Street);

                if (!string.IsNullOrEmpty(v_Town))
                    r.Append(" " + v_Town);

                if (!string.IsNullOrEmpty(v_Locality))
                    r.Append(" " + v_Locality);

                if (!string.IsNullOrEmpty(v_County))
                    r.Append(" " + v_County);

                if (!string.IsNullOrEmpty(v_Postcode))
                    r.Append(" " + v_Postcode);

                r.Append(" [" + LookupHelper.AddressLinkTypeToString(v_AddressLinkType) + "]");

                return r.ToString();

            }
            //set;
        }

        [DataMember]
        public override string ReportHeading1
        {
            get
            {
                return DisplayText;

                //StringBuilder r = "Address:";

                //if (!string.IsNullOrEmpty(v_BuildingNumber))
                //    r.Append(" " + v_BuildingNumber);

                //if (!string.IsNullOrEmpty(v_Street))
                //    r.Append(" " + v_Street);

                //if (!string.IsNullOrEmpty(v_Postcode))
                //    r.Append(" " + v_Postcode);

                //r.Append(" [" + LookupHelper.AddressLinkTypeToString(v_AddressLinkType) + "]");

                //return r.ToString();

            }
            //set;
        }

        [DataMember]
        public override string ReportHeading2
        {
            get
            {
                return DisplayText;

                //StringBuilder r = "Address:";

                //if (!string.IsNullOrEmpty(v_BuildingNumber))
                //    r.Append(" " + v_BuildingNumber);

                //if (!string.IsNullOrEmpty(v_Street))
                //    r.Append(" " + v_Street);

                //if (!string.IsNullOrEmpty(v_Postcode))
                //    r.Append(" " + v_Postcode);

                //r.Append(" [" + LookupHelper.AddressLinkTypeToString(v_AddressLinkType) + "]");

                //return r.ToString();

            }
            //set;
        }
    }


}