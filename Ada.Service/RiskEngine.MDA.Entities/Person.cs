﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using System.Diagnostics;
using MDA.Common.Debug;
using MDA.DataService;
using System.Text;
using System.Globalization;
using System.Threading;
using MDA.DataService.PropertyMethods.Model;

namespace RiskEngine.Scoring.Entities
{
    #region Testing Classes
    public class PersonRiskEditExtras
    {
        public int v_SettledCaseCount { get; set; }
        public int v_TelephoneSettledCaseCount { get; set; }
        public int v_LinkedSettledCaseCount { get; set; }
        public int v_TelephoneLinkedSettledCaseCount { get; set; }
        public int v_NumberOfCases { get; set; }
        public int v_EmailUsageCount { get; set; }
        public int v_TelephoneNumberUsageCount { get; set; }
        //public bool? v_IsTelephoneKeyAttractor { get; set; }
        public int v_TelephoneAtMultipleAddressCount { get; set; }
        public bool v_PersonFraudRulesTriggered { get; set; }
        public int v_BankAccountUsageCount { get; set; }
        //public bool? v_IsBankAccountKeyAttractor { get; set; }
        public int v_BankAccountAtMultipleAddressCount { get; set; }
        public int v_NIToPersonCount { get; set; }
        public int v_PassportToPersonCount { get; set; }
        public DateTime v_IncidentDate { get; set; }
        public int? v_Age { get; set; }
        //public bool? v_IsEmailKeyAttractor { get; set; }
        public DateTime? v_SanctionDate { get; set; }
        public string v_SanctionSource { get; set; }

        public string v_TSIDUAMLResult { get; set; }
        public int? v_TracemartDOB { get; set; }
        public int? v_ExperianDOB { get; set; }
        public string v_GoneAway { get; set; }
        public string v_Insolvency { get; set; }
        public string v_InsolvencyHistory { get; set; }
        //public string v_InsolvencyStatus { get; set; }
        public string v_CCJHistory { get; set; }
        //public string v_CCJType { get; set; }
        //public decimal v_CCJAmount { get; set; }
        //public DateTime? v_CCJEndDate { get; set; }

        public string v_DeathScreenMatchType { get; set; }
        public string v_DeathScreenDoD { get; set; }
        public string v_DeathScreenRegNo { get; set; }

        public bool? v_CredivaCheck { get; set; }
        public bool? v_BankAccountValidated { get; set; }
        public bool? v_DrivingLicenseValidated { get; set; }
        public bool? v_NINumberValidated { get; set; }
        public bool? v_PassportMRZValid { get; set; }
        public bool? v_PassportDOBValid { get; set; }
        public bool? v_PassportGenderValid { get; set; }
        public bool? v_PaymentCardNumberValid { get; set; }
        public bool? v_LandlineValidated { get; set; }
        public string v_LandlineStatus { get; set; }
        public string v_MobileCurrentLocation { get; set; }
        public string v_MobileStatus { get; set; }
        public int v_TracesmartServicesCalled { get; set; }

    }

    public class PersonRiskEdit : PersonRisk
    {
        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();


        public PersonRiskEdit()
        {
            Addresses = new ObservableCollection<AddressRiskEdit>();
            Organisations = new ObservableCollection<OrganisationRiskEdit>();

            Extras = new PersonRiskEditExtras();

            //v_PersonPolicyLink = PersonPolicyLink.Insured;
            //v_PersonPolicyLink_Text = v_PersonPolicyLink.ToString();

            //v_PersonVehicleLink = PersonVehicleLink.Owner;
            //v_PersonVehicleLink_Text = v_PersonVehicleLink.ToString();

            v_PartyType = (int)PartyType.Unknown;
            v_PartyType_Text = v_PartyType.ToString();

            v_SubPartyType = (int)SubPartyType.Unknown;
            v_SubPartyType_Text = v_SubPartyType.ToString();

            v_Salutation = 0;
            v_Salutation_Text = v_Salutation.ToString();

            v_Gender = 0;
            v_Gender_Text = v_Gender.ToString();
        }

        public ObservableCollection<AddressRiskEdit> Addresses { get; set; }

        //public BankAccountRiskEdit BankAccount { get; set; }

        //public PaymentCardRiskEdit PaymentCard { get; set; }

        public ObservableCollection<OrganisationRiskEdit> Organisations { get; set; }

        #region Extras
        public PersonRiskEditExtras Extras;

        //public override int v_TelephoneNumberUsageCount
        //{
        //    get { return Extras.v_TelephoneNumberUsageCount; }
        //    set { Extras.v_TelephoneNumberUsageCount = value; }
        //}

        //public override bool v_IsTelephoneKeyAttractor
        //{
        //    get { return Extras.v_IsTelephoneKeyAttractor; }
        //    set { Extras.v_IsTelephoneKeyAttractor = value; }
        //}

        public override int v_TelephoneAtMultipleAddressCount
        {
            get { return Extras.v_TelephoneAtMultipleAddressCount; }
            set { Extras.v_TelephoneAtMultipleAddressCount = value; }
        }

        public override bool v_PersonFraudRulesTriggered
        {
            get { return Extras.v_PersonFraudRulesTriggered; }
            set { Extras.v_PersonFraudRulesTriggered = value; }

        }

        //public override int v_BankAccountUsageCount
        //{
        //    get { return Extras.v_BankAccountUsageCount; }
        //    set { Extras.v_BankAccountUsageCount = value; }
        //}

        //public override bool? v_IsBankAccountKeyAttractor
        //{
        //    get { return Extras.v_IsBankAccountKeyAttractor; }
        //    set { Extras.v_IsBankAccountKeyAttractor = value; }
        //}

        //public override int v_BankAccountAtMultipleAddressCount
        //{
        //    get { return Extras.v_BankAccountAtMultipleAddressCount; }
        //    set { Extras.v_BankAccountAtMultipleAddressCount = value; }
        //}

        public override int v_NIToPersonCount
        {
            get { return Extras.v_NIToPersonCount; }
            set { Extras.v_NIToPersonCount = value; }
        }

        public override int v_PassportToPersonCount
        {
            get { return Extras.v_PassportToPersonCount; }
            set { Extras.v_PassportToPersonCount = value; }
        }

        public override DateTime v_IncidentDate
        {
            get { return Extras.v_IncidentDate; }
            set { Extras.v_IncidentDate = value; }
        }

        public override int? v_Age
        {
            get { return Extras.v_Age; }
            set { Extras.v_Age = value; }
        }

        //public override bool? v_IsEmailKeyAttractor
        //{
        //    get { return Extras.v_IsEmailKeyAttractor; }
        //    set { Extras.v_IsEmailKeyAttractor = value; }
        //}

        public override DateTime? v_SanctionDate
        {
            get { return Extras.v_SanctionDate; }
            set { Extras.v_SanctionDate = value; }
        }

        public override string v_SanctionSource
        {
            get { return Extras.v_SanctionSource; }
            set { Extras.v_SanctionSource = value; }
        }

        public override string v_TSIDUAMLResult
        {
            get { return Extras.v_TSIDUAMLResult; }
            set { Extras.v_TSIDUAMLResult = value; }
        }

        public override int? v_TracemartDOB
        {
            get { return Extras.v_TracemartDOB; }
            set { Extras.v_TracemartDOB = value; }
        }

        public override int? v_ExperianDOB
        {
            get { return Extras.v_ExperianDOB; }
            set { Extras.v_ExperianDOB = value; }
        }

        public override string v_GoneAway
        {
            get { return Extras.v_GoneAway; }
            set { Extras.v_GoneAway = value; }
        }

        public override string v_Insolvency
        {
            get { return Extras.v_Insolvency; }
            set { Extras.v_Insolvency = value; }
        }

        public override string v_InsolvencyHistory
        {
            get { return Extras.v_InsolvencyHistory; }
            set { Extras.v_InsolvencyHistory = value; }
        }

        //public override string v_InsolvencyStatus
        //{
        //    get { return Extras.v_InsolvencyStatus; }
        //    set { Extras.v_InsolvencyStatus = value; }
        //}

        public override string v_CCJHistory
        {
            get { return Extras.v_CCJHistory; }
            set { Extras.v_CCJHistory = value; }
        }

        public override string v_DeathScreenMatchType
        {
            get { return Extras.v_DeathScreenMatchType; }
            set { Extras.v_DeathScreenMatchType = value; }
        }

        public override string v_DeathScreenDoD
        {
            get { return Extras.v_DeathScreenDoD; }
            set { Extras.v_DeathScreenDoD = value; }
        }

        public override string v_DeathScreenRegNo
        {
            get { return Extras.v_DeathScreenRegNo; }
            set { Extras.v_DeathScreenRegNo = value; }
        }

        public override bool? v_CredivaCheck
        {
            get { return Extras.v_CredivaCheck; }
            set { Extras.v_CredivaCheck = value; }
        }

        public override bool? v_BankAccountValidated
        {
            get { return Extras.v_BankAccountValidated; }
            set { Extras.v_BankAccountValidated = value; }
        }

        public override bool? v_DrivingLicenseValidated
        {
            get { return Extras.v_DrivingLicenseValidated; }
            set { Extras.v_DrivingLicenseValidated = value; }
        }

        public override bool? v_NINumberValidated
        {
            get { return Extras.v_NINumberValidated; }
            set { Extras.v_NINumberValidated = value; }
        }

        public override bool? v_PassportMRZValid
        {
            get { return Extras.v_PassportMRZValid; }
            set { Extras.v_PassportMRZValid = value; }
        }

        public override bool? v_PassportDOBValid
        {
            get { return Extras.v_PassportDOBValid; }
            set { Extras.v_PassportDOBValid = value; }
        }

        public override bool? v_PassportGenderValid
        {
            get { return Extras.v_PassportGenderValid; }
            set { Extras.v_PassportGenderValid = value; }
        }

        public override bool? v_PaymentCardNumberValid
        {
            get { return Extras.v_PaymentCardNumberValid; }
            set { Extras.v_PaymentCardNumberValid = value; }
        }

        //public override bool? v_LandlineValidated
        //{
        //    get { return Extras.v_LandlineValidated; }
        //    set { Extras.v_LandlineValidated = value; }
        //}

        //public override string v_LandlineStatus
        //{
        //    get { return Extras.v_LandlineStatus; }
        //    set { Extras.v_LandlineStatus = value; }
        //}

        //public override string v_MobileCurrentLocation
        //{
        //    get { return Extras.v_MobileCurrentLocation; }
        //    set { Extras.v_MobileCurrentLocation = value; }
        //}

        //public override string v_MobileStatus
        //{
        //    get { return Extras.v_MobileStatus; }
        //    set { Extras.v_MobileStatus = value; }
        //}

        public override int v_TracesmartServicesCalled
        {
            get { return Extras.v_TracesmartServicesCalled; }
            set { Extras.v_TracesmartServicesCalled = value; }
        }
        #endregion

        public override int v_SettledCaseCount(string cachekey, string matchType, int category, bool? isPotentialClaimant, string periodSkip, string periodCount)
        {
            return Extras.v_SettledCaseCount;
        }

        public override int v_TelephoneSettledCaseCount(string cachekey, int category, bool? isPotentialClaimant, string periodSkip, string periodCount)
        {
            return Extras.v_TelephoneSettledCaseCount;
        }

        public override int v_LinkedSettledCaseCount(string cacheKey, string matchType, int category, int? claimTypeId, bool? isPotentialClaimant, string periodSkip, string periodCount)
        {
            return Extras.v_LinkedSettledCaseCount;
        }

        public override int v_TelephoneLinkedSettledCaseCount(string cacheKey, int category, int? claimTypeId, bool? isPotentialClaimant, string periodSkip, string periodCount)
        {
            return Extras.v_TelephoneLinkedSettledCaseCount;
        }

        public override int v_NumberOfCases(string cacheKey, string matchType, string caseSource, bool? isPotentialClaimant, string periodSkip, string periodCount)
        {
            return Extras.v_NumberOfCases;
        }

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, int KeyAttCount, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
                ClientScores[orgName].RuleGroupKeyAttractorTotalCount = KeyAttCount;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }



        public override object Clone()
        {
            return CloneEntity(false);
        }

        public PersonRiskEdit CloneEntity(bool cloneChildren)
        {
            PersonRiskEdit c = new PersonRiskEdit();

            //c.v_PersonPolicyLink       = v_PersonPolicyLink;
            //c.v_PersonPolicyLink_Text  = v_PersonPolicyLink_Text;
            //c.v_PersonVehicleLink      = v_PersonVehicleLink;
            //c.v_PersonVehicleLink_Text = v_PersonVehicleLink_Text;
            c.v_PartyType = v_PartyType;
            c.v_PartyType_Text = v_PartyType_Text;
            c.v_SubPartyType = v_SubPartyType;
            c.v_SubPartyType_Text = v_SubPartyType_Text;
            c.v_Salutation = v_Salutation;
            c.v_Salutation_Text = v_Salutation_Text;
            c.v_Gender = v_Gender;
            c.v_Gender_Text = v_Gender_Text;
            c.v_FirstName = v_FirstName;
            c.v_MiddleName = v_MiddleName;
            c.v_LastName = v_LastName;
            c.v_DateOfBirth = v_DateOfBirth;
            c.v_Nationality = v_Nationality;
            c.v_Occupation = v_Occupation;
            c.v_NINumber = v_NINumber;
            c.v_PassportNumber = v_PassportNumber;
            //c.v_LandlineNumber = v_LandlineNumber;
            //c.v_MobileNumber = v_MobileNumber;
            //c.v_EmailAddress = v_EmailAddress;
            //c.v_EMailAddress           = v_EMailAddress;
            //c.v_LandlineTelephone      = v_LandlineTelephone;
            //c.v_WorkTelephone          = v_WorkTelephone;
            //c.v_MobileTelephone        = v_MobileTelephone;
            //c.v_OtherTelephone         = v_OtherTelephone;
            c.v_AttendedHospital = v_AttendedHospital;
            c.v_DrivingLicenseNumber = v_DrivingLicenseNumber;

            c.v_ClaimStatus = v_ClaimStatus;
            c.v_ClaimStatus_Text = v_ClaimStatus_Text;
            c.v_IncidentLocation = v_IncidentLocation;
            c.v_IncidentCircumstances = v_IncidentCircumstances;
            c.v_IncidentDateTime = v_IncidentDateTime;
            c.v_PoliceAttended = v_PoliceAttended;
            c.v_PoliceForce = v_PoliceForce;
            c.v_PoliceReference = v_PoliceReference;
            c.v_AmbulanceAttended = v_AmbulanceAttended;
            c.v_MojStatus = v_MojStatus;
            c.v_MojStatus_Text = v_MojStatus_Text;
            c.v_ClaimNotificationDate = v_ClaimNotificationDate;
            c.v_PaymentsToDate = v_PaymentsToDate;
            c.v_Reserve = v_Reserve;
            c.v_AttendedHospital = v_AttendedHospital;
            //c.v_IsKeyAttractor        = v_IsKeyAttractor;
            c.v_KeyAttractor = v_KeyAttractor;

            c.v_SanctionDate = v_SanctionDate;
            c.v_SanctionSource = v_SanctionSource;
            c.v_TSIDUAMLResult = v_TSIDUAMLResult;
            c.v_TracemartDOB = v_TracemartDOB;
            c.v_ExperianDOB = v_ExperianDOB;
            c.v_GoneAway = v_GoneAway;
            c.v_Insolvency = v_Insolvency;
            c.v_InsolvencyHistory = v_InsolvencyHistory;
            //c.v_InsolvencyStatus = v_InsolvencyStatus;
            c.v_CCJHistory = v_CCJHistory;

            c.v_DeathScreenMatchType = v_DeathScreenRegNo;
            c.v_DeathScreenDoD = v_DeathScreenDoD;
            c.v_DeathScreenRegNo = v_DeathScreenRegNo;

            c.v_BankAccountValidated = v_BankAccountValidated;
            c.v_DrivingLicenseValidated = v_DrivingLicenseValidated;
            c.v_NINumberValidated = v_NINumberValidated;
            c.v_PassportMRZValid = v_PassportMRZValid;
            c.v_PassportDOBValid = v_PassportDOBValid;
            c.v_PassportGenderValid = v_PassportGenderValid;
            c.v_PaymentCardNumberValid = v_PaymentCardNumberValid;
            //c.v_LandlineValidated = v_LandlineValidated;
            //c.v_LandlineStatus = v_LandlineStatus;
            //c.v_MobileCurrentLocation = v_MobileCurrentLocation;
            //c.v_MobileStatus = v_MobileStatus;
            c.v_TracesmartServicesCalled = v_TracesmartServicesCalled;

            c.Extras.v_SettledCaseCount = Extras.v_SettledCaseCount;
            c.Extras.v_TelephoneSettledCaseCount = Extras.v_TelephoneSettledCaseCount;
            c.Extras.v_LinkedSettledCaseCount = Extras.v_LinkedSettledCaseCount;
            c.Extras.v_TelephoneLinkedSettledCaseCount = Extras.v_TelephoneLinkedSettledCaseCount;
            c.Extras.v_NumberOfCases = Extras.v_NumberOfCases;
            //c.v_EmailUsageCount                   = v_EmailUsageCount;
            //c.v_IsTelephoneKeyAttractor           = v_IsTelephoneKeyAttractor;
            //c.v_TelephoneNumberUsageCount         = v_TelephoneNumberUsageCount;
            c.v_TelephoneAtMultipleAddressCount = v_TelephoneAtMultipleAddressCount;
            //c.v_BankAccountUsageCount             = v_BankAccountUsageCount;
            //c.v_IsBankAccountKeyAttractor = v_IsBankAccountKeyAttractor;
            //c.v_BankAccountAtMultipleAddressCount = v_BankAccountAtMultipleAddressCount;
            c.v_NIToPersonCount = v_NIToPersonCount;
            c.v_PassportToPersonCount = v_PassportToPersonCount;
            c.v_IncidentDate = v_IncidentDate;
            c.v_Age = v_Age;
            //c.v_IsEmailKeyAttractor = v_IsEmailKeyAttractor;

            if (cloneChildren)
            {
                foreach (AddressRiskEdit a in this.Addresses)
                    c.Addresses.Add(a.CloneEntity(true));

                foreach (OrganisationRiskEdit o in this.Organisations)
                    c.Organisations.Add(o.CloneEntity(true));

                foreach (TelephoneRiskEdit t in this.PersonsTelephones)
                    c.PersonsTelephones.Add(t.CloneEntity(true));
            }

            return c;
        }
    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class PersonRisk : RiskEntityBase, IRiskEntity
    {
        public PersonRisk()
        {
        }

        #region Normal Properties

        [DataMember(Name = "Per_DbId")]
        public int Db_Id { get; set; }

        [DataMember(Name = "Inc_DbId")]
        public int Db_Incident_Id { get; set; }

        [DataMember(Name = "v_PFRT")]
        public virtual bool v_PersonFraudRulesTriggered { get; set; }


        [DataMember(Name = "v_PPL")]
        public int v_PersonPolicyLink { get; set; }  //PolicyLinkType

        [DataMember(Name = "v_PPLt")]
        public string v_PersonPolicyLink_Text { get; set; }

        [DataMember(Name = "v_PVL")]
        public int v_PersonVehicleLink { get; set; } //VehicleLinkType
        [DataMember(Name = "v_PVLt")]
        public string v_PersonVehicleLink_Text { get; set; }

        [DataMember(Name = "v_PT")]
        public int v_PartyType { get; set; }  //PartyType

        [DataMember(Name = "v_PTt")]
        public string v_PartyType_Text { get; set; }

        [DataMember(Name = "v_SPT")]
        public int v_SubPartyType { get; set; }  //SubPartyType

        [DataMember(Name = "v_SPTt")]
        public string v_SubPartyType_Text { get; set; }

        [DataMember(Name = "v_SAL")]
        public int v_Salutation { get; set; }

        [DataMember(Name = "v_SALt")]
        public string v_Salutation_Text { get; set; }

        [DataMember(Name = "v_GEN")]
        public int v_Gender { get; set; }

        [DataMember(Name = "v_GENt")]
        public string v_Gender_Text { get; set; }

        [DataMember(Name = "v_FN")]
        public string v_FirstName { get; set; }

        [DataMember(Name = "v_MN")]
        public string v_MiddleName { get; set; }

        [DataMember(Name = "v_LN")]
        public string v_LastName { get; set; }

        [DataMember(Name = "v_DoB")]
        public DateTime? v_DateOfBirth { get; set; }

        [DataMember(Name = "v_NAT")]
        public string v_Nationality { get; set; }

        [DataMember(Name = "v_OCC")]
        public string v_Occupation { get; set; }

        [DataMember(Name = "v_DLN")]
        public string v_DrivingLicenseNumber { get; set; }

        [DataMember(Name = "v_NI")]
        public string v_NINumber { get; set; }

        [DataMember(Name = "v_PASS")]
        public string v_PassportNumber { get; set; }

        [DataMember(Name = "v_TELE")]
        //[ScoreableItem(IsCollection = true, RulePath = "Telephones")]
        public List<TelephoneRisk> PersonsTelephones { get; set; }
        //[DataMember(Name = "v_LAND")]
        //public string v_LandlineNumber { get; set; }
        //[DataMember(Name = "v_MOB")]
        //public string v_MobileNumber { get; set; }
        //[DataMember(Name = "v_TELEO")]
        //public string v_OtherTelephoneNumber { get; set; }
        //[DataMember(Name = "v_FAX")]
        //public string v_FaxNumber { get; set; }
        [DataMember(Name = "v_EMAIL")]
        public string v_EmailAddress { get; set; }

        [DataMember(Name = "v_ConA")]
        public ListOfInts ConfirmedAliases { get; set; }

        [DataMember(Name = "v_UConA")]
        public ListOfInts UnconfirmedAliases { get; set; }

        [DataMember(Name = "v_TenA")]
        public ListOfInts TentativeAliases { get; set; }

        [DataMember(Name = "v_POCATA")]
        public ListOfInts PeopleOnClaimAndTheirAliasIds { get; set; }

        [DataMember(Name = "v_AH")]
        public bool? v_AttendedHospital { get; set; }

        [DataMember(Name = "v_CS")]
        public int v_ClaimStatus { get; set; }  // ClaimStatus
        [DataMember(Name = "v_CSt")]
        public string v_ClaimStatus_Text { get; set; }

        [DataMember(Name = "v_CC")]
        public string v_ClaimCode { get; set; }
        [DataMember(Name = "v_IDT")]
        public DateTime? v_IncidentDateTime { get; set; }
        [DataMember(Name = "v_IL")]
        public string v_IncidentLocation { get; set; }
        [DataMember(Name = "v_IC")]
        public string v_IncidentCircumstances { get; set; }
        [DataMember(Name = "v_PA")]
        public bool? v_PoliceAttended { get; set; }
        [DataMember(Name = "v_PF")]
        public string v_PoliceForce { get; set; }
        [DataMember(Name = "v_PR")]
        public string v_PoliceReference { get; set; }
        [DataMember(Name = "v_AA")]
        public bool? v_AmbulanceAttended { get; set; }

        [DataMember(Name = "v_SanD")]
        public virtual DateTime? v_SanctionDate { get; set; }     // virtual 'cos they are in prop grid, not own textboxes
        [DataMember(Name = "v_SanS")]
        public virtual string v_SanctionSource { get; set; }
        [DataMember(Name = "v_TSIRes")]
        public virtual string v_TSIDUAMLResult { get; set; }
        [DataMember(Name = "v_TDoB")]
        public virtual int? v_TracemartDOB { get; set; }
        [DataMember(Name = "v_EDob")]
        public virtual int? v_ExperianDOB { get; set; }
        [DataMember(Name = "v_GonA")]
        public virtual string v_GoneAway { get; set; }
        [DataMember(Name = "v_Insol")]
        public virtual string v_Insolvency { get; set; }

        [DataMember(Name = "v_InsolH")]
        public virtual string v_InsolvencyHistory { get; set; }
        //[DataMember(Name = "v_InsolS")]
        //public virtual string v_InsolvencyStatus { get; set; }

        [DataMember(Name = "v_CCJH")]
        public virtual string v_CCJHistory { get; set; }
        //[DataMember(Name = "v_CCJT")]
        //public virtual string v_CCJType { get; set; }
        //[DataMember(Name = "v_CCJA")]
        //public virtual decimal v_CCJAmount { get; set; }
        //[DataMember(Name = "v_CCJED")]
        //public virtual DateTime? v_CCJEndDate { get; set; }

        [DataMember(Name = "v_CCDSMT")]
        public virtual string v_DeathScreenMatchType { get; set; }
        [DataMember(Name = "v_CCDSDoD")]
        public virtual string v_DeathScreenDoD { get; set; }
        [DataMember(Name = "v_CCDSRNo")]
        public virtual string v_DeathScreenRegNo { get; set; }

        [DataMember(Name = "v_CredC")]
        public virtual bool? v_CredivaCheck { get; set; }
        [DataMember(Name = "v_BAV")]
        public virtual bool? v_BankAccountValidated { get; set; }
        [DataMember(Name = "v_DLV")]
        public virtual bool? v_DrivingLicenseValidated { get; set; }
        [DataMember(Name = "v_NIV")]
        public virtual bool? v_NINumberValidated { get; set; }
        [DataMember(Name = "v_PassMV")]
        public virtual bool? v_PassportMRZValid { get; set; }
        [DataMember(Name = "v_PassDoBV")]
        public virtual bool? v_PassportDOBValid { get; set; }
        [DataMember(Name = "v_PGV")]
        public virtual bool? v_PassportGenderValid { get; set; }
        [DataMember(Name = "v_PCNV")]
        public virtual bool? v_PaymentCardNumberValid { get; set; }


        [DataMember(Name = "v_TraceSC")]
        public virtual int v_TracesmartServicesCalled { get; set; }

        [DataMember(Name = "v_MojS")]
        public virtual int v_MojStatus { get; set; }  // MojStatus
        [DataMember(Name = "v_MojSt")]
        public virtual string v_MojStatus_Text { get; set; }

        [DataMember(Name = "v_CND")]
        public virtual DateTime? v_ClaimNotificationDate { get; set; }
        [DataMember(Name = "v_PTD")]
        public virtual decimal? v_PaymentsToDate { get; set; }
        [DataMember(Name = "v_Res")]
        public virtual decimal? v_Reserve { get; set; }

        [DataMember(Name = "v_BADbId")]
        public virtual int v_BankAccountDb_Id { get; set; }
        [DataMember(Name = "v_BASC")]
        public virtual string v_BankAccountSortCode { get; set; }
        [DataMember(Name = "v_BAN")]
        public virtual string v_BankAccountName { get; set; }
        [DataMember(Name = "v_BAAN")]
        public virtual string v_BankAccountAccountNumber { get; set; }
        //[DataMember(Name = "v_IsBAKA")]
        //public virtual bool? v_IsBankAccountKeyAttractor { get; set; }
        [DataMember(Name = "v_BAKA")]
        public virtual string v_BankAccountKeyAttractor { get; set; }
        [DataMember(Name = "v_BAFRS")]
        public virtual string v_BankAccountFraudRingStatus { get; set; }

        [DataMember(Name = "vPCDbId")]
        public virtual int v_PaymentCardDb_Id { get; set; }
        [DataMember(Name = "v_PCSC")]
        public virtual string v_PaymentCardSortCode { get; set; }
        [DataMember(Name = "v_PCBN")]
        public virtual string v_PaymentCardBankName { get; set; }
        [DataMember(Name = "v_PCAN")]
        public virtual string v_PaymentCardAccountNumber { get; set; }
        //[DataMember(Name = "v_IsPCKA")]
        //public virtual bool? v_IsPaymentCardKeyAttractor { get; set; }
        [DataMember(Name = "v_PCKA")]
        public virtual string v_PaymentCardKeyAttractor { get; set; }
        [DataMember(Name = "v_PCFRS")]
        public virtual string v_PaymentCardFraudRingStatus { get; set; }

        [DataMember(Name = "v_LkAL")]
        public List<Person_AliasInformation_Result> v_LinkedAlias { get; set; }
        [DataMember(Name = "v_LkADD")]
        public List<Person_LinkedAddresses_Result> v_LinkedAddress { get; set; }
        [DataMember(Name = "v_LkT")]
        public List<Person_LinkedTelephones> v_LinkedTelephone { get; set; }
        [DataMember(Name = "v_LkEM")]
        public List<Person_LinkedEmails> v_LinkedEmails { get; set; }
        [DataMember(Name = "v_LkNI")]
        public List<Person_LinkedNINumbers> v_LinkedNINumbers { get; set; }
        [DataMember(Name = "v_LkDL")]
        public List<Person_LinkedDrivingLicenceNumbers> v_LinkedDrivingLicenceNumbers { get; set; }
        [DataMember(Name = "v_LkPN")]
        public List<Person_LinkedPassportNumbers> v_LinkedPassportNumbers { get; set; }
        [DataMember(Name = "v_LkBA")]
        public List<Person_LinkedBankAccounts> v_LinkedBankAccounts { get; set; }


        [DataMember(Name = "v_KeyA")]
        public string v_KeyAttractor { get; set; }


        [DataMember(Name = "Addresses")]
        [ScoreableItem(IsCollection = true, RulePath = "Address")]
        public List<AddressRisk> PersonsAddresses { get; set; }

        [DataMember(Name = "UAddresses")]
        public List<AddressRisk> PersonsUnconfirmedAddresses { get; set; }
        //[DataMember]
        //public List<BankAccountRisk> PersonsBankAccounts { get; set; }
        //[DataMember]
        //public List<PaymentCardRisk> PersonsPaymentCards { get; set; }
        [DataMember(Name = "Orgs")]
        [ScoreableItem(IsCollection = true, RulePath = "Organisations")]
        public List<OrganisationRisk> PersonsOrganisations { get; set; }


        [DataMember(Name = "v_TAMAC")]
        public virtual int v_TelephoneAtMultipleAddressCount { get; set; }

        [DataMember(Name = "v_NIPerCnt")]
        public virtual int v_NIToPersonCount { get; set; }

        [DataMember(Name = "v_PassPerCnt")]
        public virtual int v_PassportToPersonCount { get; set; }

        [DataMember(Name = "v_IDate")]
        public virtual DateTime v_IncidentDate { get; set; }
        #endregion

        private bool? _v_LandlineValidated;

        [DataMember(Name = "v_LLV")]
        public virtual bool? v_LandlineValidated
        {
            get
            {
                foreach (var t in this.PersonsTelephones)
                    if (t.v_TelephoneType == (int)MDA.Common.Enum.TelephoneType.Landline && t.v_Validated != null && t.v_Validated == false)
                        return false;

                return true;
            }
            set
            {
                _v_LandlineValidated = value;
            }
        }

        private string _v_LandlineStatus;

        [DataMember(Name = "v_LLS")]
        public virtual string v_LandlineStatus
        {
            get
            {
                return (from x in this.PersonsTelephones
                        where x.v_TelephoneType == (int)MDA.Common.Enum.TelephoneType.Landline && x.v_Status != null
                        select x.v_Status).FirstOrDefault();
            }
            set
            {
                _v_LandlineStatus = value;
            }
        }

        private string _v_MobileCurrentLocation;

        [DataMember(Name = "v_MobCL")]
        public virtual string v_MobileCurrentLocation
        {
            get
            {
                return (from x in this.PersonsTelephones
                        where x.v_Status != null
                        select x.v_Status).FirstOrDefault();
            }
            set
            {
                _v_MobileCurrentLocation = value;
            }
        }

        private string _v_MobileStatus;

        [DataMember(Name = "v_MobS")]
        public virtual string v_MobileStatus
        {
            get
            {
                return (from x in this.PersonsTelephones
                        where x.v_TelephoneType == (int)MDA.Common.Enum.TelephoneType.Mobile && x.v_Status != null
                        select x.v_Status).FirstOrDefault();
            }

            set
            {
                _v_MobileStatus = value;
            }
        }

        private int? _v_NumberOfDaysBetweenIncidentDateAndNotificationDate;

        [DataMember(Name = "v_NoDBIDaND")]
        public virtual int? v_NumberOfDaysBetweenIncidentDateAndNotificationDate
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_NumberOfDaysBetweenIncidentDateAndNotificationDate == null) ? "NA" : _v_NumberOfDaysBetweenIncidentDateAndNotificationDate.ToString();

                        ADATrace.WriteLine("Person.v_NumberOfDaysBetweenIncidentDateAndNotificationDate. Serializing [" + x.ToString() + "]");
                    }

                    return _v_NumberOfDaysBetweenIncidentDateAndNotificationDate;
                }

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfDaysBetweenIncidentDateAndNotificationDate");

                if (_v_NumberOfDaysBetweenIncidentDateAndNotificationDate != null)
                    return (int)_v_NumberOfDaysBetweenIncidentDateAndNotificationDate;

                if (v_IncidentDateTime != null && v_ClaimNotificationDate != null)
                {
                    DateTime dtOne = (DateTime)v_IncidentDateTime;
                    DateTime dtTwo = v_ClaimNotificationDate.Value;

                    _v_NumberOfDaysBetweenIncidentDateAndNotificationDate = (dtTwo - dtOne).Days + 1;


                    if (_trace) ADATrace.WriteLine("Incident.v_NumberOfDaysBetweenIncidentDateAndNotificationDate = " + _v_NumberOfDaysBetweenIncidentDateAndNotificationDate);

                    return (int)_v_NumberOfDaysBetweenIncidentDateAndNotificationDate;
                }

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfDaysBetweenIncidentDateAndNotificationDate = 0");

                return 0;
            }
            set
            {
                _v_NumberOfDaysBetweenIncidentDateAndNotificationDate = value;
            }
        }

        #region Payment Card Key Attractor Properties
        //private string _v_PaymentCardKeyAttractor;

        //[DataMember(Name = "v_PCKA")]
        //public virtual string v_PaymentCardKeyAttractor
        //{
        //    get
        //    {
        //        if (Locked)
        //        {
        //            string x = (_v_PaymentCardKeyAttractor == null) ? "NA" : (string)_v_PaymentCardKeyAttractor;

        //            if (_trace)
        //                ADATrace.WriteLine("Person.v_PaymentCardKeyAttractor. Serializing [" + x + "]");

        //            return x;
        //        }

        //        if (_trace) ADATrace.WriteLine("Person.v_PaymentCardKeyAttractor");

        //        if (_v_PaymentCardKeyAttractor == null)
        //        {
        //             _v_PaymentCardKeyAttractor = new MDA.DataService.DataServices(ctx).Person_PaymentCardKeyAttractor(this.Db_Id, this.RiskClaimId);
        //        }

        //        if (_trace) ADATrace.WriteLine("Person.v_PaymentCardKeyAttractor = " + _v_PaymentCardKeyAttractor);

        //        return _v_PaymentCardKeyAttractor;
        //    }
        //    set
        //    {
        //        _v_PaymentCardKeyAttractor = value;
        //    }
        //}

        private string _v_AllPaymentCardKeyAttractor;

        [DataMember(Name = "v_APCKA")]
        public virtual string v_AllPaymentCardKeyAttractor
        {
            get
            {
                if (Locked)
                {
                    string x = (_v_AllPaymentCardKeyAttractor == null) ? "NA" : (string)_v_AllPaymentCardKeyAttractor;

                    if (_trace)
                        ADATrace.WriteLine("Person.v_AllPaymentCardKeyAttractor. Serializing [" + x + "]");

                    return x;
                }

                if (_trace) ADATrace.WriteLine("Person.v_AllPaymentCardKeyAttractor");

                if (_v_AllPaymentCardKeyAttractor == null)
                {
                    _v_AllPaymentCardKeyAttractor = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_AllPaymentCardKeyAttractor(this.Db_Id);
                }

                if (_trace) ADATrace.WriteLine("Person.v_AllPaymentCardKeyAttractor = " + _v_AllPaymentCardKeyAttractor);

                return _v_AllPaymentCardKeyAttractor;
            }
            set
            {
                _v_AllPaymentCardKeyAttractor = value;
            }
        }

        //private bool? _v_IsPaymentCardKeyAttractor;

        //[DataMember(Name = "v_IsPCKA")]
        public bool? v_IsPaymentCardKeyAttractor(string cacheKey)
        {
            string key = string.Format("IsPCKeyA: Id:{0}", this.Db_Id.ToString());

            bool result = false;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_IsPaymentCardKeyAttractor(this.Db_Id, this.RiskClaimId);

                result = (recs != null && recs.Count > 0);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);


                foreach (var row in recs)
                {
                    try
                    {
                        string s = string.Format(formatString, row.PaymentCard, row.KeyAttractor);

                        MessageCache.AddMediumOutputMessage(cacheKey, s, null);

                    }
                    catch (System.FormatException)
                    {
                        MessageCache.AddMediumOutputMessage(cacheKey, "IsTelephoneKeyAttractor Format Error", null);
                    }
                }


                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        private bool? _v_IsAnyPaymentCardKeyAttractor;

        [DataMember(Name = "v_IsAPCKA")]
        public virtual bool? v_IsAnyPaymentCardKeyAttractor
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_IsAnyPaymentCardKeyAttractor == null) ? "NA" : _v_IsAnyPaymentCardKeyAttractor.ToString();

                        ADATrace.WriteLine("Person.v_IsAnyPaymentCardKeyAttractor. Serializing [" + x.ToString() + "]");
                    }

                    return _v_IsAnyPaymentCardKeyAttractor;
                }

                if (_trace) ADATrace.WriteLine("Person.v_IsAnyPaymentCardKeyAttractor");

                if (_v_IsAnyPaymentCardKeyAttractor == null)
                {
                    _v_IsAnyPaymentCardKeyAttractor = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_IsAnyPaymentCardKeyAttractor(this.Db_Id);
                }

                if (_trace) ADATrace.WriteLine("Person.v_IsAnyPaymentCardKeyAttractor = " + _v_IsAnyPaymentCardKeyAttractor.ToString());

                return (_v_IsAnyPaymentCardKeyAttractor == null) ? false : (bool)_v_IsAnyPaymentCardKeyAttractor;
            }
            set
            {
                _v_IsAnyPaymentCardKeyAttractor = value;
            }
        }

        #endregion

        #region Bank Account Key Attractor  Properties

        private string _v_BankAccountKeyAttractor;

        //[DataMember(Name = "v_BAKA")]
        //public virtual string v_BankAccountKeyAttractor 
        //{
        //    get
        //    {
        //        if (Locked)
        //        {
        //            string x = (_v_BankAccountKeyAttractor == null) ? "NA" : (string)_v_BankAccountKeyAttractor;

        //            if (_trace)
        //                ADATrace.WriteLine("Person.v_BankAccountKeyAttractor. Serializing [" + x + "]");

        //            return x;
        //        }

        //        if (_trace) ADATrace.WriteLine("Person.v_BankAccountKeyAttractor");

        //        if (_v_BankAccountKeyAttractor == null)
        //        {
        //                _v_BankAccountKeyAttractor = new MDA.DataService.DataServices(ctx).Person_BankAccountKeyAttractor(this.Db_Id, this.RiskClaimId);
        //        }

        //        if (_trace) ADATrace.WriteLine("Person.v_BankAccountKeyAttractor = " + _v_BankAccountKeyAttractor);

        //        return _v_BankAccountKeyAttractor;
        //    }
        //    set
        //    {
        //        _v_BankAccountKeyAttractor = value;
        //    }
        //}

        private string _v_AllBankAccountKeyAttractor;

        [DataMember(Name = "v_ABAKA")]
        public virtual string v_AllBankAccountKeyAttractor
        {
            get
            {
                if (Locked)
                {
                    string x = (_v_AllBankAccountKeyAttractor == null) ? "NA" : (string)_v_AllBankAccountKeyAttractor;

                    if (_trace)
                        ADATrace.WriteLine("Person.v_AllBankAccountKeyAttractor. Serializing [" + x + "]");

                    return x;
                }

                if (_trace) ADATrace.WriteLine("Person.v_AllBankAccountKeyAttractor");

                if (_v_AllBankAccountKeyAttractor == null)
                {
                    _v_BankAccountKeyAttractor = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_AllBankAccountKeyAttractor(this.Db_Id);
                }

                if (_trace) ADATrace.WriteLine("Person.v_AllBankAccountKeyAttractor = " + _v_AllBankAccountKeyAttractor);

                return _v_AllBankAccountKeyAttractor;
            }
            set
            {
                _v_AllBankAccountKeyAttractor = value;
            }
        }

        //private bool? _v_IsBankAccountKeyAttractor;

        //[DataMember(Name = "v_IsBAKA")]
        public virtual bool? v_IsBankAccountKeyAttractor(string cacheKey)
        {
            string key = string.Format("IsBAKeyA: Id:{0}", this.Db_Id.ToString());

            bool result = false;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_IsBankAccountKeyAttractor(this.Db_Id, this.RiskClaimId);

                result = (recs != null && recs.Count > 0);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);


                foreach (var row in recs)
                {
                    try
                    {
                        string s = string.Format(formatString, row.SortCode, row.AccountNumber, row.KeyAttractor);

                        MessageCache.AddMediumOutputMessage(cacheKey, s, null);

                    }
                    catch (System.FormatException)
                    {
                        MessageCache.AddMediumOutputMessage(cacheKey, "IsTelephoneKeyAttractor Format Error", null);
                    }
                }


                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        private bool? _v_IsAnyBankAccountKeyAttractor;

        [DataMember(Name = "v_IsABAKA")]
        public virtual bool? v_IsAnyBankAccountKeyAttractor
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_IsAnyBankAccountKeyAttractor == null) ? "NA" : _v_IsAnyBankAccountKeyAttractor.ToString();

                        ADATrace.WriteLine("Person.v_IsAnyBankAccountKeyAttractor. Serializing [" + x.ToString() + "]");
                    }

                    return _v_IsAnyBankAccountKeyAttractor;
                }

                if (_trace) ADATrace.WriteLine("Person.v_IsAnyBankAccountKeyAttractor");

                if (_v_IsAnyBankAccountKeyAttractor == null)
                {
                    _v_IsAnyBankAccountKeyAttractor = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_IsAnyBankAccountKeyAttractor(this.Db_Id);
                }

                if (_trace) ADATrace.WriteLine("Person.v_IsAnyBankAccountKeyAttractor = " + _v_IsAnyBankAccountKeyAttractor.ToString());

                return (_v_IsAnyBankAccountKeyAttractor == null) ? false : (bool)_v_IsAnyBankAccountKeyAttractor;
            }
            set
            {
                _v_IsAnyBankAccountKeyAttractor = value;
            }
        }
        #endregion

        #region Email Key Attractor  Properties
        private string _v_EmailKeyAttractor;

        [DataMember(Name = "v_EKA")]
        public virtual string v_EmailKeyAttractor
        {
            get
            {
                if (Locked)
                {
                    string x = (_v_EmailKeyAttractor == null) ? "NA" : (string)_v_EmailKeyAttractor;

                    if (_trace)
                        ADATrace.WriteLine("Person.v_EmailKeyAttractor. Serializing [" + x + "]");

                    return x;
                }

                if (_trace) ADATrace.WriteLine("Person.v_EmailKeyAttractor");

                if (_v_EmailKeyAttractor == null)
                {
                    _v_BankAccountKeyAttractor = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_EmailKeyAttractor(this.Db_Id, this.RiskClaimId);
                }

                if (_trace) ADATrace.WriteLine("Person.v_EmailKeyAttractor = " + _v_EmailKeyAttractor);

                return _v_EmailKeyAttractor;
            }
            set
            {
                _v_EmailKeyAttractor = value;
            }
        }


        private string _v_AllEmailKeyAttractor;

        [DataMember(Name = "v_AEKA")]
        public virtual string v_AllEmailKeyAttractor
        {
            get
            {
                if (Locked)
                {
                    string x = (_v_AllEmailKeyAttractor == null) ? "NA" : (string)_v_AllEmailKeyAttractor;

                    if (_trace)
                        ADATrace.WriteLine("Person.v_AllEmailKeyAttractor. Serializing [" + x + "]");

                    return x;
                }

                if (_trace) ADATrace.WriteLine("Person.v_AllEmailKeyAttractor");

                if (_v_AllEmailKeyAttractor == null)
                {
                    _v_AllEmailKeyAttractor = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_AllEmailKeyAttractor(this.Db_Id);
                }

                if (_trace) ADATrace.WriteLine("Person.v_AllEmailKeyAttractor = " + _v_AllEmailKeyAttractor);

                return _v_AllEmailKeyAttractor;
            }
            set
            {
                _v_AllEmailKeyAttractor = value;
            }
        }

        //private bool? _v_IsEmailKeyAttractor;

        //[DataMember(Name = "v_IsEKA")]
        public virtual bool? v_IsEmailKeyAttractor(string cacheKey)
        {
            string key = string.Format("IsEmailKeyA: Id:{0}", this.Db_Id.ToString());

            bool result = false;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_IsEmailKeyAttractor(this.Db_Id, this.RiskClaimId);

                result = (recs != null && recs.Count > 0);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);


                foreach (var row in recs)
                {
                    try
                    {
                        string s = string.Format(formatString, row.Email, row.KeyAttractor);

                        MessageCache.AddMediumOutputMessage(cacheKey, s, null);

                    }
                    catch (System.FormatException)
                    {
                        MessageCache.AddMediumOutputMessage(cacheKey, "IsEmailKeyAttractor Format Error", null);
                    }
                }


                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        private bool? _v_IsAnyEmailKeyAttractor;

        [DataMember(Name = "v_IsAEKA")]
        public virtual bool? v_IsAnyEmailKeyAttractor
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_IsAnyEmailKeyAttractor == null) ? "NA" : _v_IsAnyEmailKeyAttractor.ToString();

                        ADATrace.WriteLine("Person.v_IsAnyEmailKeyAttractor. Serializing [" + x.ToString() + "]");
                    }

                    return _v_IsAnyEmailKeyAttractor;
                }

                if (_trace) ADATrace.WriteLine("Person.v_IsAnyEmailKeyAttractor");

                if (_v_IsAnyEmailKeyAttractor == null)
                {
                    _v_IsAnyEmailKeyAttractor = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_IsAnyEmailKeyAttractor(this.Db_Id);
                }

                if (_trace) ADATrace.WriteLine("Person.v_IsAnyEmailKeyAttractor = " + _v_IsAnyEmailKeyAttractor.ToString());

                return (_v_IsAnyEmailKeyAttractor == null) ? false : (bool)_v_IsAnyEmailKeyAttractor;
            }
            set
            {
                _v_IsAnyEmailKeyAttractor = value;
            }
        }
        #endregion

        private int? _v_Age;

        [DataMember]
        public virtual int? v_Age
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_Age == null) ? "NA" : _v_Age.ToString();

                        ADATrace.WriteLine("Person.v_Age. Serializing [" + x.ToString() + "]");
                    }

                    return _v_Age;
                }

                if (_trace) ADATrace.WriteLine("Person.v_Age");

                if (_v_Age == null)
                    _v_Age = LinkHelper.CalculateAge(v_DateOfBirth, v_IncidentDate);

                if (_trace) ADATrace.WriteLine("Person.v_Age = " + _v_Age.ToString());

                return (int)_v_Age;
            }
            set
            {
                _v_Age = value;
            }
        }


        #region Telephone Key Attractor Methods

        public virtual string v_TelephoneKeyAttractor(string cacheKey, string telephoneType)
        {
            string key = string.Format("TeleKA: Id:{0}, P1:{1}", this.Db_Id.ToString(), telephoneType.ToString());
            string result = "";

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToString(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                result = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_TelephoneKeyAttractor(this.Db_Id, telephoneType, this.RiskClaimId, cacheKey, this.MessageCache, _trace);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey);

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual string v_AllTelephoneKeyAttractor(string cacheKey, string telephoneType)
        {
            string key = string.Format("AllTeleKA: Id:{0}, P1:{1}", this.Db_Id.ToString(), telephoneType.ToString());
            string result = "";

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToString(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                result = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_AllTelephoneKeyAttractor(this.Db_Id, telephoneType, cacheKey, this.MessageCache, _trace);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey);

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual bool v_IsAnyTelephoneKeyAttractor(string cacheKey, string telephoneType)
        {
            string key = string.Format("IsAnyTeleKA: Id:{0}, P1:{1}", this.Db_Id.ToString(), telephoneType.ToString());
            bool result = false;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                result = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_IsAnyTelephoneKeyAttractor(this.Db_Id, telephoneType, cacheKey, this.MessageCache, _trace);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey);

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }


        public virtual bool? v_IsTelephoneKeyAttractor(string cacheKey)
        {
            string key = string.Format("IsTeleKeyA: Id:{0}", this.Db_Id.ToString());

            bool result = false;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_IsTelephoneKeyAttractor(this.Db_Id, this.RiskClaimId);

                result = (recs != null && recs.Count > 0);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);


                foreach (var row in recs)
                {
                    try
                    {

                        string teleType = "";

                        switch (row.TelephoneType)
                        {
                            case (int)TelephoneType.FAX:
                                teleType = "Fax";
                                break;
                            case (int)TelephoneType.Landline:
                                teleType = "landline telephone";
                                break;
                            case (int)TelephoneType.Mobile:
                                teleType = "mobile telephone";
                                break;
                            case (int)TelephoneType.Unknown:
                                teleType = "telephone";
                                break;
                        }

                        string s = string.Format(formatString, teleType, row.Telephone, row.KeyAttractor);

                        MessageCache.AddMediumOutputMessage(cacheKey, s, null);

                    }
                    catch (System.FormatException)
                    {
                        MessageCache.AddMediumOutputMessage(cacheKey, "IsTelephoneKeyAttractor Format Error", null);
                    }
                }


                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        #endregion

        public bool? v_IsKeyAttractor(string cacheKey, string matchType)
        {
            string key = string.Format("IsKeyA: Id:{0}, P1:{1}", this.Db_Id.ToString(), matchType);

            bool result = false;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_IsKeyAttractor(personAliases, this.Db_Id, cacheKey, this.MessageCache, _trace);

                result = (recs != null && recs.Count > 0);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (personAliases.IncludeThis || personAliases.IncludeConfirmed)
                {
                    foreach (var row in recs)
                    {
                        try
                        {
                            // Needs 7 parameters
                            string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text),
                                                            this.v_FirstName, this.v_LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(this.v_DateOfBirth), row.KeyAttractor, "");

                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);

                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "IsKeyAttractor Format Error", null);
                        }
                    }
                }
                else if (personAliases.IncludeUnconfirmed)
                {
                    foreach (var row in recs)
                    {
                        try
                        {
                            string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text),
                                                        row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth), row.KeyAttractor, "possible ");

                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "IsKeyAttractor Format Error", null);
                        }
                    }
                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public bool? v_IsSanctionsAttractor(string cacheKey, string matchType)
        {
            string key = string.Format("IsSancAtt: Id:{0}, P1:{1}", this.Db_Id.ToString(), matchType);

            bool result = false;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_IsSanctionsAttractor(personAliases, this.Db_Id, cacheKey, this.MessageCache, _trace);

                result = (recs != null && recs.Count > 0);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (personAliases.IncludeThis || personAliases.IncludeConfirmed)
                {
                    foreach (var row in recs)
                    {
                        try
                        {
                            // Needs 7 parameters
                            string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text),
                                                            this.v_FirstName, this.v_LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(this.v_DateOfBirth), row.KeyAttractor, "");

                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);

                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "IsSancAtt Format Error", null);
                        }
                    }
                }
                else if (personAliases.IncludeUnconfirmed)
                {
                    foreach (var row in recs)
                    {
                        try
                        {
                            string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text),
                                                        row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth), row.KeyAttractor, "possible ");

                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "IsSancAtt Format Error", null);
                        }
                    }
                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        //public bool? v_IsTOGAttractor(string cacheKey, string matchType)
        //{
        //    string key = string.Format("IsTOGAttractor: P1:{0}, P2:{1}", this.Db_Id.ToString(), matchType);

        //    bool result = false;

        //    if (_trace) ADATrace.WriteLine("Person." + key);

        //    if (MethodResultHistory.ContainsKey(key))
        //    {
        //        if (_trace) ADATrace.WriteLine("Accessing cached result");

        //        result = Convert.ToBoolean(MethodResultHistory[key].Result);

        //        this.MessageCache = MethodResultHistory[key].MessageCache;
        //    }
        //    else
        //    {
        //        EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

        //        PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

        //        var recs = new MDA.DataService.DataServices(ctx).Person_IsTOGAttractor(personAliases, this.Db_Id, cacheKey, this.MessageCache, _trace);

        //        result = (recs != null && recs.Count > 0);

        //        PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

        //        string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

        //        if (personAliases.IncludeThis || personAliases.IncludeConfirmed)
        //        {
        //            foreach (var row in recs)
        //            {
        //                try
        //                {
        //                    string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text),
        //                                                    this.v_FirstName, this.v_LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(this.v_DateOfBirth));

        //                    MessageCache.AddMediumOutputMessage(cacheKey, s, null);

        //                }
        //                catch (System.FormatException)
        //                {
        //                    MessageCache.AddMediumOutputMessage(cacheKey, "IsTOGAttractor Format Error", null);
        //                }
        //            }
        //        }
        //        else if (personAliases.IncludeUnconfirmed)
        //        {
        //            foreach (var row in recs)
        //            {
        //                try
        //                {
        //                    string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text),
        //                                                row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth));

        //                    MessageCache.AddMediumOutputMessage(cacheKey, s, null);
        //                }
        //                catch (System.FormatException)
        //                {
        //                    MessageCache.AddMediumOutputMessage(cacheKey, "IsTOGAttractor Format Error", null);
        //                }
        //            }
        //        }

        //        MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
        //    }
        //    if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

        //    return result;
        //}

        private string v_PartyTypeMessage(string PartyType, string SubPartyType)
        {

            string Message = PartyType += SubPartyType;

            switch (Message)
            {
                case "UnknownUnknown":
                    return "this person";
                case "UnknownDriver":
                    return "the driver";
                case "UnknownPassenger":
                    return "the passenger";
                case "UnknownWitness":
                    return "the witness";
                case "ClaimantUnknown":
                    return "the claimant";
                case "ClaimantDriver":
                    return "the claimant driver";
                case "ClaimantPassenger":
                    return "the claimant passenger";
                case "ClaimantWitness":
                    return "the witness";
                case "HirerUnknown":
                    return "the hirer";
                case "HirerDriver":
                    return "the driver";
                case "HirerPassenger":
                    return "the passenger";
                case "HirerWitness":
                    return "the witness";
                case "InsuredUnknown":
                    return "the insured";
                case "InsuredDriver":
                    return "the insured driver";
                case "InsuredPassenger":
                    return "the passenger in the insured vehicle";
                case "InsuredWitness":
                    return "the insured's witness";
                case "Litigation FriendUnknown":
                    return "the litigation friend";
                case "Litigation FriendDriver":
                    return "the litigation friend";
                case "Litigation FriendPassenger":
                    return "the litigation friend";
                case "Litigation FriendWitness":
                    return "the litigation friend";
                case "PolicyholderUnknown":
                    return "the policyholder";
                case "PolicyholderDriver":
                    return "the insured driver";
                case "PolicyholderPassenger":
                    return "the passenger in the insured vehicle";
                case "PolicyholderWitness":
                    return "the insured's witness";
                case "Third PartyUnknown":
                    return "the third party";
                case "Third PartyDriver":
                    return "the third party driver";
                case "Third PartyPassenger":
                    return "the passenger in the third party vehicle";
                case "Third PartyWitness":
                    return "the third party's witness";
                case "Vehicle OwnerUnknown":
                    return "the vehicle owner";
                case "Vehicle OwnerDriver":
                    return "the vehicle owner";
                case "Vehicle OwnerPassenger":
                    return "the vehicle owner";
                case "Vehicle OwnerWitness":
                    return "the vehicle owner";
                case "Paid PolicyUnknown":
                    return "this person";
                case "Paid PolicyDriver":
                    return "this person";
                case "Paid PolicyPassenger":
                    return "this person";
                case "Paid PolicyWitness":
                    return "this person";

                default:
                    return "this person";
            }
        }

        private string v_PartyTypeMessage2(string PartyType, string SubPartyType)
        {

            string Message = PartyType += SubPartyType;

            switch (Message)
            {
                case "UnknownUnknown":
                    return "involved";
                case "UnknownDriver":
                    return "a driver";
                case "UnknownPassenger":
                    return "a passenger";
                case "UnknownWitness":
                    return "a witness";
                case "ClaimantUnknown":
                    return "a claimant";
                case "ClaimantDriver":
                    return "a claimant driver";
                case "ClaimantPassenger":
                    return "a claimant passenger";
                case "ClaimantWitness":
                    return "a witness";
                case "HirerUnknown":
                    return "a vehicle hirer";
                case "HirerDriver":
                    return "a driver";
                case "HirerPassenger":
                    return "a passenger";
                case "HirerWitness":
                    return "a witness";
                case "InsuredUnknown":
                    return "the insured";
                case "InsuredDriver":
                    return "the insured driver";
                case "InsuredPassenger":
                    return "a passenger in the insured vehicle";
                case "InsuredWitness":
                    return "a witness";
                case "Litigation FriendUnknown":
                    return "a litigation friend";
                case "Litigation FriendDriver":
                    return "a litigation friend";
                case "Litigation FriendPassenger":
                    return "a litigation friend";
                case "Litigation FriendWitness":
                    return "a litigation friend";
                case "PolicyholderUnknown":
                    return "the policyholder";
                case "PolicyholderDriver":
                    return "the insured driver";
                case "PolicyholderPassenger":
                    return "a passenger in the insured vehicle";
                case "PolicyholderWitness":
                    return "a witness";
                case "Third PartyUnknown":
                    return "a third party";
                case "Third PartyDriver":
                    return "a third party driver";
                case "Third PartyPassenger":
                    return "a passenger in the third party vehicle";
                case "Third PartyWitness":
                    return "a witness";
                case "Vehicle OwnerUnknown":
                    return "the owner of a vehicle that involved";
                case "Vehicle OwnerDriver":
                    return "the owner of a vehicle that involved";
                case "Vehicle OwnerPassenger":
                    return "the owner of a vehicle that involved";
                case "Vehicle OwnerWitness":
                    return "the owner of a vehicle that involved";
                case "Paid PolicyUnknown":
                    return "paid the policy of the insured driver";
                case "Paid PolicyDriver":
                    return "paid the policy of the insured driver";
                case "Paid PolicyPassenger":
                    return "paid the policy of the insured driver";
                case "Paid PolicyWitness":
                    return "paid the policy of the insured driver";

                default:
                    return "Unknown";
            }
        }

        private string v_PartyTypeMessage3(string PartyType, string SubPartyType)
        {

            string Message = PartyType += SubPartyType;

            switch (Message)
            {
                case "UnknownUnknown":
                    return "Unknown";
                case "UnknownDriver":
                    return "Driver";
                case "UnknownPassenger":
                    return "Passenger";
                case "UnknownWitness":
                    return "Witness";
                case "ClaimantUnknown":
                    return "Claimant";
                case "ClaimantDriver":
                    return "Claimant Driver";
                case "ClaimantPassenger":
                    return "Claimant Passenger";
                case "ClaimantWitness":
                    return "Witness";
                case "HirerUnknown":
                    return "Vehicle Hirer";
                case "HirerDriver":
                    return "Driver";
                case "HirerPassenger":
                    return "Passenger";
                case "HirerWitness":
                    return "Witness";
                case "InsuredUnknown":
                    return "Insured";
                case "InsuredDriver":
                    return "Insured Driver";
                case "InsuredPassenger":
                    return "Insured Passenger";
                case "InsuredWitness":
                    return "Witness";
                case "Litigation FriendUnknown":
                    return "Litigation Friend";
                case "Litigation FriendDriver":
                    return "Litigation Friend";
                case "Litigation FriendPassenger":
                    return "Litigation Friend";
                case "Litigation FriendWitness":
                    return "Litigation Friend";
                case "PolicyholderUnknown":
                    return "Policyholder";
                case "PolicyholderDriver":
                    return "Insured Driver";
                case "PolicyholderPassenger":
                    return "Insured Passenger";
                case "PolicyholderWitness":
                    return "Witness";
                case "Third PartyUnknown":
                    return "Third Party";
                case "Third PartyDriver":
                    return "Third Party Driver";
                case "Third PartyPassenger":
                    return "Third Party Passenger";
                case "Third PartyWitness":
                    return "Witness";
                case "Vehicle OwnerUnknown":
                    return "Vehicle Owner";
                case "Vehicle OwnerDriver":
                    return "Vehicle Owner";
                case "Vehicle OwnerPassenger":
                    return "Vehicle Owner";
                case "Vehicle OwnerWitness":
                    return "Vehicle Owner";
                case "Paid PolicyUnknown":
                    return "Paid the policy of the insured driver";
                case "Paid PolicyDriver":
                    return "Paid the policy of the insured driver";
                case "Paid PolicyPassenger":
                    return "Paid the policy of the insured driver";
                case "Paid PolicyWitness":
                    return "Paid the policy of the insured driver";

                default:
                    return "Unknown";
            }
        }

        private string v_ClaimTypeMessage(string ClaimType)
        {
            switch (ClaimType)
            {
                case "NA":
                    return "fraudulent claim";
                case "Commercial":
                    return "fraudulent commercial claim";
                case "Employer's Liability":
                    return "fraudulent employer's liability claim";
                case "Financial":
                    return "fraudulent financial claim";
                case "Holiday":
                    return "fraudulent travel claim";
                case "Household":
                    return "fraudulent household claim";
                case "Occupiers Liability":
                    return "fraudulent occupiers liability claim";
                case "Public Liability":
                    return "fraudulent public liability claim";
                case "Bogus":
                    return "bogus motor collision";
                case "Credit Hire":
                    return "fraudulent credit hire claim";
                case "Exaggerated Loss":
                    return "exaggerated loss claim";
                case "Fire / Theft":
                    return "fraudulent vehicle fire / theft claim";
                case "Fire/Theft":
                    return "fraudulent vehicle fire / theft claim";
                case "General":
                    return "fraudulent motor claim";
                case "Induced Collision":
                    return "induced motor collision";
                case "Intel + Report":
                    return "fraudulent motor claim";
                case "Intel Only":
                    return "fraudulent motor claim";
                case "Low Speed Impact":
                    return "LSI collision";
                case "Staged/Contrived":
                    return "staged/contrived collision";
                case "RTA - Bogus":
                    return "bogus motor collision";
                case "RTA - Credit Hire":
                    return "fraudulent credit hire claim";
                case "RTA - Exaggerated Loss":
                    return "exaggerated loss claim";
                case "RTA - Fire / Theft":
                    return "fraudulent vehicle fire / theft claim";
                case "RTA - Fire/Theft":
                    return "fraudulent vehicle fire / theft claim";
                case "RTA - General":
                    return "fraudulent motor claim";
                case "RTA - Induced Collision":
                    return "induced motor collision";
                case "RTA - Intel + Report":
                    return "fraudulent motor claim";
                case "RTA - Intel Only":
                    return "fraudulent motor claim";
                case "RTA - Low Speed Impact":
                    return "LSI collision";
                case "RTA - Staged/Contrived":
                    return "staged/contrived collision";
                default:
                    return "fraudulent claim";
            }
        }

        private string v_IncidentTypeMessage(int IncidentType)
        {
            switch (IncidentType)
            {
                case 0: //Unknown
                    return "an incident";
                case 1: //RTC
                    return "a motor vehicle claim";
                case 2: //Commercial
                    return "a commercial claim";
                case 3: //Employment
                    return "an employment liability claim";
                case 4: // Public Liability
                    return "a public liability claim";
                case 5: //Travel
                    return "a travel claim";
                case 6: //Home Contents
                    return "a household claim";
                case 7: //Home Building
                    return "a household claim";
                case 8: //Keoghs CFS
                    return "a Keoghs CFS case";
                case 9: //Keoghs CMS
                    return "a Keoghs CMS case";
                case 10: // Theft
                    return "a theft claim";
                case 11: // Pet
                    return "a pet insurance claim";
                case 12: // Mobile
                    return "a mobile phone claim";
                case 13: // CUE Home
                    return "a household claim";
                case 14: // CUE Motor
                    return "a motor vehicle claim";
                case 15: // CUE PI
                    return "a personal injury claim";
                case 16: // MIAFTR
                    return "an incident ";
                default:
                    return "";
            }
        }

        public virtual int v_SettledCaseCount(string cacheKey, string matchType, int category, bool? isPotentialClaimant, string periodSkip, string periodCount)
        {
            string key = string.Format("SettledCC: Id:{0}, P1:{1}, P2:{2}, P3:{3}, P4:{4}, P5:{5}",
                                            this.Db_Id, matchType, category.ToString(), isPotentialClaimant.ToString(), periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_SettledCaseCount(this.Db_Incident_Id, this.Db_Id, category, isPotentialClaimant, periodSkip, periodCount, personAliases, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {
                    result = recs.Count;

                    if (personAliases.IncludeThis || personAliases.IncludeConfirmed)
                    {
                        foreach (var row in recs)
                        {
                            try
                            {
                                string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text), this.v_FirstName, this.v_LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(this.v_DateOfBirth),
                                    v_PartyTypeMessage2(row.PartyTypeText, row.SubPartyText), string.Format("{0:d/M/yyyy}", row.IncidentDate), v_ClaimTypeMessage(row.ClaimType), row.KeoghsEliteReference,
                                    row.I2P_FiveGrading, row.MannerOfResolution, row.MOR_FiveGrading);

                                MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                            }
                            catch (System.FormatException)
                            {
                                MessageCache.AddMediumOutputMessage(cacheKey, "Person_SettledCaseCount Format Error", null);
                            }
                        }
                    }
                    else if (personAliases.IncludeUnconfirmed)
                    {
                        foreach (var row in recs)
                        {
                            try
                            {
                                string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text), row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth),
                                    v_PartyTypeMessage2(row.PartyTypeText, row.SubPartyText), string.Format("{0:d/M/yyyy}", row.IncidentDate), v_ClaimTypeMessage(row.ClaimType), row.KeoghsEliteReference,
                                    row.I2P_FiveGrading, row.MannerOfResolution, row.MOR_FiveGrading);

                                MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                            }
                            catch (System.FormatException)
                            {
                                MessageCache.AddMediumOutputMessage(cacheKey, "Person_SettledCaseCount Format Error", null);
                            }
                        }
                    }
                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_TelephoneSettledCaseCount(string cacheKey, int category, bool? isPotentialClaimant, string periodSkip, string periodCount)
        {
            string key = string.Format("TelephoneSettledCC: Id:{0}, P1:{1}, P2:{2}, P3:{3}, P4:{4}",
                                            this.Db_Id, category.ToString(), isPotentialClaimant.ToString(), periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                //EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_TelephoneSettledCaseCount(this.Db_Id, this.Db_Incident_Id, this.RiskClaimId, category, isPotentialClaimant, this.v_IncidentDate, this.PeopleOnClaimAndTheirAliasIds, periodSkip, periodCount, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {
                    result = recs.Count;

                    foreach (var row in recs)
                    {
                        try
                        {
                            //string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text), this.v_FirstName, this.v_LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(this.v_DateOfBirth),
                            //    v_PartyTypeMessage2(row.PartyTypeText, row.SubPartyText), string.Format("{0:d/M/yyyy}", row.IncidentDate), v_ClaimTypeMessage(row.ClaimType), row.KeoghsEliteReference,
                            //    row.I2P_FiveGrading, row.MannerOfResolution, row.MOR_FiveGrading);

                            MessageCache.AddMediumOutputMessage(cacheKey, null, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "Person_TelephoneSettledCaseCount Format Error", null);
                        }
                    }

                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_LinkedSettledCaseCount(string cacheKey, string matchType, int category, int? claimTypeId, bool? isPotentialClaimant, string periodSkip, string periodCount)
        {
            string key = string.Format("LSettledCC: Id:{0}, P1:{1}, P2:{2}, P3:{3}, P4:{4}, P5:{5}, P6:{6}",
                                                this.Db_Id, matchType, category.ToString(), claimTypeId.ToString(), isPotentialClaimant.ToString(), periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_LinkedSettledCaseCount(this.Db_Incident_Id, this.Db_Id,
                    category, claimTypeId, isPotentialClaimant, periodSkip, periodCount, personAliases, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {
                    result = recs.Count;

                    if (personAliases.IncludeThis || personAliases.IncludeConfirmed)
                    {
                        foreach (var row in recs)
                        {
                            try
                            {
                                string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text), this.v_FirstName, this.v_LastName,
                                    MDA.Common.Helpers.DateHelper.DateOfBirthMessage(this.v_DateOfBirth), v_PartyTypeMessage2(row.LinkedPartyType, row.LinkedSubPartyType), string.Format("{0:d/M/yyyy}", row.LinkedIncidentDate), v_ClaimTypeMessage(row.LinkedClaimType), row.LinkedEliteReference,
                                    row.LinkedFiveGrading, v_PartyTypeMessage(row.OtherPartyType, row.OtherSubPartyType), row.OtherFirstname, row.OtherLastName,
                                    MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.OtherDoB), row.OtherMoR, row.OtherFiveGrading);

                                MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                            }
                            catch (System.FormatException)
                            {
                                MessageCache.AddMediumOutputMessage(cacheKey, "Person_LinkedSettledCaseCount Format Error", null);
                            }

                        }
                    }
                    else if (personAliases.IncludeUnconfirmed)
                    {
                        foreach (var row in recs)
                        {
                            try
                            {
                                string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text), this.v_FirstName, this.v_LastName,
                                    MDA.Common.Helpers.DateHelper.DateOfBirthMessage(this.v_DateOfBirth), v_PartyTypeMessage2(row.LinkedPartyType, row.LinkedSubPartyType), string.Format("{0:d/M/yyyy}", row.LinkedIncidentDate), v_ClaimTypeMessage(row.LinkedClaimType), row.LinkedEliteReference,
                                    row.LinkedFiveGrading, v_PartyTypeMessage(row.OtherPartyType, row.OtherSubPartyType), row.OtherFirstname, row.OtherLastName,
                                    MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.OtherDoB), row.OtherMoR, row.OtherFiveGrading);

                                MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                            }
                            catch (System.FormatException)
                            {
                                MessageCache.AddMediumOutputMessage(cacheKey, "Person_LinkedSettledCaseCount Format Error", null);
                            }
                        }
                    }
                }               

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_TelephoneLinkedSettledCaseCount(string cacheKey, int category, int? claimTypeId, bool? isPotentialClaimant, string periodSkip, string periodCount)
        {
            string key = string.Format("TelLSettledCC: Id:{0}, P1:{1}, P2:{2}, P3:{3}, P4:{4}, P5:{5}",
                                                this.Db_Id, category.ToString(), claimTypeId.ToString(), isPotentialClaimant.ToString(), periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                //EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_TelephoneLinkedSettledCaseCount(this.Db_Id, this.RiskClaimId, category, claimTypeId, isPotentialClaimant, this.v_IncidentDate, this.PeopleOnClaimAndTheirAliasIds, periodSkip, periodCount, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {
                    result = recs.Count;


                    foreach (var row in recs)
                    {
                        try
                        {
                            //string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text), this.v_FirstName, this.v_LastName,
                            //    MDA.Common.Helpers.DateHelper.DateOfBirthMessage(this.v_DateOfBirth), v_PartyTypeMessage2(row.LinkedPartyType, row.LinkedSubPartyType), string.Format("{0:d/M/yyyy}", row.LinkedIncidentDate), v_ClaimTypeMessage(row.LinkedClaimType), row.LinkedEliteReference,
                            //    row.LinkedFiveGrading, v_PartyTypeMessage(row.OtherPartyType, row.OtherSubPartyType), row.OtherFirstname, row.OtherLastName,
                            //    MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.OtherDoB), row.OtherMoR, row.OtherFiveGrading);

                            MessageCache.AddMediumOutputMessage(cacheKey, null, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "Person_TelephoneLinkedSettledCaseCount Format Error", null);
                        }

                    }

                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_EmailUsageCount(string cacheKey, string matchType)
        {
            string key = string.Format("EmailUC: Id:{0}, P1:{1}", this.Db_Id.ToString(), matchType);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_EmailUsageCount(personAliases, this.Db_Id, this.RiskClaimId, _trace);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                result = recs.Count();

                if (result > 0)
                {

                    string s = "A match has been found in the Keoghs Intelligence Database for the email address " + this.v_EmailAddress + " - It is linked to " + recs.Count() + " other " + ((result <= 2) ? "person" : "people") + " :";

                    foreach (var row in recs)
                    {
                        ListOfStringData newRow = new ListOfStringData();

                        newRow.Add(row.Salutation);
                        newRow.Add(row.FirstName);
                        newRow.Add(row.LastName);
                        newRow.Add(row.DateOfBirth.ToString());
                        newRow.Add(row.IncidentDateWhereEmailUsedByThisPerson.ToString());
                        newRow.Add(row.Incident2PersonFiveGrading);

                        MessageCache.AddMediumOutputData(cacheKey, newRow, null);

                        string source = "";

                        if (row.Source == "Keoghs CFS")
                        {
                            source = "a Keoghs CFS case";
                        }
                        else
                        {
                            source = v_IncidentTypeMessage(row.IncidentType) + " dated " + string.Format("{0:d/M/yyyy}", row.IncidentDateWhereEmailUsedByThisPerson);
                        }

                        s += "~n" + string.Format(MessageCache.GetMediumThenInMessage(cacheKey), row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth), source, row.Incident2PersonFiveGrading);

                    }

                    MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_CCJAmountCount(string cacheKey, int? lowerValue, int? upperValue)
        {
            string key = string.Format("CCJAmountCheck: Id:{0}, p1:{0}, p2:{1}", this.Db_Id.ToString(), lowerValue.ToString(), upperValue.ToString());
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                if (v_CCJHistory != null)
                {
                    try
                    {
                        string[] recs = v_CCJHistory.Split(':');
                        if (_trace)
                            ADATrace.IndentLevel += 1;

                        foreach (string r in recs)
                        {
                            string[] fields = r.Substring(1, r.Length - 2).Split(',');

                            //string caseNumber    = fields[0];
                            string judgementType = fields[1];
                            //string courtName     = fields[2];
                            //string judgementDate = fields[3];
                            string amount = fields[4];

                            if (_trace)
                                ADATrace.WriteLine(string.Format("CaseNumber[{0}],JudgementType[{1}],CourtName[{2}],JudgementDate[{3}], Amount[{4}]",
                                                    fields[0], fields[1], fields[2], fields[3], fields[4]));

                            if (string.Compare(judgementType, "SS", true) != 0 &&
                                string.Compare(judgementType, "AS", true) != 0 &&
                                string.Compare(judgementType, "C", true) != 0)
                            {
                                try
                                {
                                    decimal amnt = Convert.ToDecimal(amount);

                                    if (lowerValue != null)
                                    {
                                        if (upperValue != null)
                                        {
                                            if (amnt >= lowerValue && amnt <= upperValue)
                                                result++;
                                        }
                                        else
                                        {
                                            if (amnt >= lowerValue)
                                                result++;
                                        }
                                    }
                                    else
                                    {
                                        if (upperValue != null)
                                        {
                                            if (amnt <= upperValue)
                                                result++;
                                        }
                                        else
                                        {
                                            if (amnt != 0)
                                                result++;
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                    if (_trace) ADATrace.WriteLine("Person." + key + "ERROR. CCJ Amount not an integer is ignored : [" + amount + "]");
                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (_trace) ADATrace.WriteLine("Person." + key + " = CCJ parsing Exception Absorbed: " + ex.Message);
                    }
                    finally
                    {
                        if (_trace)
                            ADATrace.IndentLevel -= 1;
                    }
                }
                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_CCJSatisfiedCount(string cacheKey)
        {
            string key = string.Format("CCJSatisfiedCount: Id:{0}", this.Db_Id.ToString());
            int cnt = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                cnt = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                if (v_CCJHistory != null)
                {
                    try
                    {
                        string[] recs = v_CCJHistory.Split(':');
                        if (_trace)
                            ADATrace.IndentLevel += 1;

                        foreach (string r in recs)
                        {
                            string[] fields = r.Substring(1, r.Length - 2).Split(',');

                            //string caseNumber = fields[0];
                            string judgementType = fields[1];
                            //string courtName = fields[2];
                            //string judgementDate = fields[3];
                            //string amount = fields[4];

                            if (_trace)
                                ADATrace.WriteLine(string.Format("CaseNumber[{0}],JudgementType[{1}],CourtName[{2}],JudgementDate[{3}], Amount[{4}]",
                                                    fields[0], fields[1], fields[2], fields[3], fields[4]));

                            if (string.Compare(judgementType, "SS", true) == 0 ||
                                string.Compare(judgementType, "AS", true) == 0 ||
                                string.Compare(judgementType, "C", true) == 0)
                            {
                                cnt++;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (_trace) ADATrace.WriteLine("Person." + key + " = CCJ parsing Exception Absorbed: " + ex.Message);
                    }
                    finally
                    {
                        if (_trace)
                            ADATrace.IndentLevel -= 1;
                    }
                }
                MethodResultHistory.Add(key, new MethodResultRecord() { Result = cnt, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + cnt.ToString());

            return cnt;
        }

        public virtual bool v_CCJSatisfied(string cacheKey)
        {
            string key = string.Format("CCJSatisfied: Id:{0}", this.Db_Id.ToString());
            bool result = false;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                result = v_CCJSatisfiedCount(cacheKey) > 0;

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        private MDA.Common.Enum.TelephoneType TelephoneTypeToEnum(string telephoneType)
        {
            switch (telephoneType.ToUpper())
            {
                case "UNKNOWN":
                    return MDA.Common.Enum.TelephoneType.Unknown;

                case "LANDLINE":
                    return MDA.Common.Enum.TelephoneType.Landline;

                case "MOBILE":
                    return MDA.Common.Enum.TelephoneType.Mobile;

                case "FAX":
                    return MDA.Common.Enum.TelephoneType.FAX;
            }

            return MDA.Common.Enum.TelephoneType.Unknown;
        }

        private List<int> TelephoneTypesToList(string telephoneTypes)
        {
            string[] typesSource = telephoneTypes.Split('|');

            List<int> types = new List<int>();

            foreach (var s in typesSource)
            {
                int newType = (int)TelephoneTypeToEnum(s);

                if (!types.Contains(newType))
                    types.Add(newType);
            }

            return types;
        }

        /// <summary>
        /// Return the number of people using this telephone number
        /// </summary>
        /// <param name="matchType">A string either CONFIRMED or UNCONFIRMED</param>
        /// <param name="telephoneTypes">A string either LANDLINE, MOBILE, FAX or UNKNOWN (or | separated list).  If "" UNKNOWN assumed</param>
        /// <returns></returns>
        public virtual int v_TelephoneNumberUsageCount(string cacheKey, string telephoneTypes, bool sameAddress)
        {
            string key = string.Format("TeleNumberUC: Id:{0}, P1:{1}, P2:{2}, P3:{3}", this.Db_Id.ToString(), sameAddress, telephoneTypes, this.RiskClaimId.ToString());
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;

            }
            else
            {
                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                if (this.PersonsTelephones != null && this.PersonsTelephones.Count > 0)
                {
                    var teleTypes = TelephoneTypesToList(telephoneTypes);

                    var teleIds = from x in this.PersonsTelephones where teleTypes.Contains(x.v_TelephoneType) select x.Db_Id;

                    StringBuilder MedMessage = new StringBuilder();

                    foreach (var teleId in teleIds)
                    {
                        var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_TelephoneNumberUsageCount(sameAddress, teleId, this.Db_Id, this.RiskClaimId, this.PeopleOnClaimAndTheirAliasIds, _trace);

                        PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                        result = recs.Count();

                        if (result > 0)
                        {

                            string teleType = recs[0].TelephoneTypeText;
                            string teleNumber = recs[0].TelephoneNumber;

                            MedMessage.Append("A match has been found in the Keoghs Intelligence Database for the " + teleType + " number, " + teleNumber + " - It is linked to " + recs.Count() + " other " + ((result <= 2) ? "person" : "people") + " : ");

                            foreach (var row in recs)
                            {
                                ListOfStringData newRow = new ListOfStringData();

                                string address;

                                if (row.FormattedAddress != null)
                                {
                                    address = "of " + row.FormattedAddress;
                                }
                                else
                                {
                                    if (row.Address != null)
                                    {
                                        address = "of " + (MDA.Common.Helpers.AddressHelper.FormatAddress(row.Address));
                                    }
                                    else
                                    {
                                        address = null;
                                    }
                                }


                                newRow.Add(row.Salutation);
                                newRow.Add(row.FirstName);
                                newRow.Add(row.LastName);
                                newRow.Add(row.DateOfBirth.ToString());
                                newRow.Add(address);
                                newRow.Add(row.IncidentDateWhereTeleNumberUsedByThisPerson.ToString());
                                newRow.Add(row.Incident2PersonFiveGrading);
                                newRow.Add(row.TelephoneNumber);
                                newRow.Add(row.TelephoneTypeText);

                                MessageCache.AddMediumOutputData(cacheKey, newRow, null);

                                string source = "";

                                if (row.Source == "Keoghs CFS")
                                {
                                    source = "a Keoghs CFS case";
                                }
                                else
                                {
                                    source = v_ClaimTypeMessage(row.IncidentType) + " dated " + string.Format("{0:d/M/yyyy}", row.IncidentDateWhereTeleNumberUsedByThisPerson);
                                }

                                MedMessage.Append("~n" + string.Format(MessageCache.GetMediumThenInMessage(cacheKey), row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth), address, source, row.Incident2PersonFiveGrading));
                            }
                        }

                        MessageCache.AddMediumOutputMessage(cacheKey, MedMessage.ToString(), null);
                    }
                }
                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_LinkedAddressCaseCount(string cacheKey, string matchType, bool? isPotentialClaimant, string linkType, string incidentType, string claimType, string periodSkip, string periodCount)
        {
            string key = string.Format("LAddCC: Id:{0}, P1:{1}, P2:{2}, P3:{3}, P4:{4}, P5:{5}, P6:{6}, P7:{7}", this.Db_Id, matchType, isPotentialClaimant.ToString(), linkType, incidentType, claimType, periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_LinkedAddressCaseCount(this.Db_Id, this.RiskClaimId, linkType, isPotentialClaimant, this.v_IncidentDate, this.PeopleOnClaimAndTheirAliasIds, personAliases, incidentType, claimType, periodSkip, periodCount, cacheKey, MessageCache, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {
                    result = recs.Count;


                    foreach (var row in recs)
                    {

                        try
                        {

                            string s = string.Format(formatString, row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth), v_PartyTypeMessage2(row.PartyTypeText, row.SubPartyText), string.Format("{0:d/M/yyyy}", row.IncidentDate.Date), v_ClaimTypeMessage(row.LinkedClaimType), row.EliteReference, row.FiveGrading);


                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "Person_LinkedAddressCaseCount Format Error", null);
                        }

                    }


                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_BankAccountUsageCount(string cacheKey, string matchType)
        {
            string key = string.Format("BankUC: Id:{0}, P1:{1}", this.Db_Id.ToString(), matchType);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                result = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_BankAccountUsageCount(personAliases, this.Db_Id, this.RiskClaimId, cacheKey, this.MessageCache, _trace);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey);

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_BankAccountAtMultipleAddressCount(string cacheKey)
        {
            string key = string.Format("BankAtMultiAC: Id:{0}", this.Db_Id.ToString());

            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_BankAccountAtMultipleAddressCount(this.Db_Id, this.RiskClaimId, this.PeopleOnClaimAndTheirAliasIds, _trace);

                result = recs.Count();

                if (result > 0)
                {

                    string s = "A match has been found in the Keoghs Intelligence Database for the bank account " + this.v_BankAccountSortCode + " " + this.v_BankAccountAccountNumber + ".  It is linked to" + ((result <= 2) ? " a person who lives " : " people that live ") + "at a different address to " + v_PartyTypeMessage(this.v_PartyType.ToString(), this.v_SubPartyType.ToString()) + ".";

                    foreach (var row in recs)
                    {
                        ListOfStringData newRow = new ListOfStringData();

                        newRow.Add(row.Salutation);
                        newRow.Add(row.FirstName);
                        newRow.Add(row.LastName);
                        newRow.Add(row.DateOfBirth.ToString());
                        newRow.Add(MDA.Common.Helpers.AddressHelper.FormatAddress(row.FullAddress));
                        newRow.Add(row.IncidentDateWhereBankAccountUsedByThisPerson.ToString());
                        newRow.Add(row.Incident2PersonFiveGrading);

                        MessageCache.AddMediumOutputData(cacheKey, newRow, null);


                        PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                        string source = "";

                        if (row.Source == "Keoghs CFS")
                        {
                            source = "a Keoghs CFS case";
                        }
                        else
                        {
                            source = v_ClaimTypeMessage(row.IncidentType) + " dated " + string.Format("{0:d/M/yyyy}", row.IncidentDateWhereBankAccountUsedByThisPerson);
                        }

                        s += "~n" + string.Format(MessageCache.GetMediumThenInMessage(cacheKey), row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth), (MDA.Common.Helpers.AddressHelper.FormatAddress(row.FullAddress)), source, row.Incident2PersonFiveGrading);

                    }

                    MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="matchType">This+Confirmed or Unconfirmed</param>
        /// <param name="linkType">Keoghs Case 2 Incident Link Type - 1 Instruction or 2 Info Only</param>
        /// <param name="isPotentialClaimant"></param>
        /// <param name="periodSkip"></param>
        /// <param name="periodCount"></param>
        /// <returns></returns>
        public virtual int v_NumberOfCases(string cacheKey, string matchType, string linkType, bool? isPotentialClaimant, string periodSkip, string periodCount)
        {
            string key = string.Format("NOfCases: Id:{0}, P1:{1}, P2:{2}, P3:{3}, P4:{4}, P5:{5}", this.Db_Id, matchType, linkType, isPotentialClaimant.ToString(), periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_NumberOfCases(this.Db_Id, personAliases, linkType, isPotentialClaimant, periodSkip, periodCount, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {
                    result = recs.Count;

                    if (personAliases.IncludeThis || personAliases.IncludeConfirmed)
                    {
                        foreach (var row in recs)
                        {
                            try
                            {
                                string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text), this.v_FirstName, this.v_LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(this.v_DateOfBirth),
                                    v_PartyTypeMessage2(row.PartyTypeText, row.SubPartyText), string.Format("{0:d/M/yyyy}", row.IncidentDate), v_ClaimTypeMessage(row.ClaimType), row.KeoghsEliteReference, row.FiveGrading, "");

                                MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                            }
                            catch (System.FormatException)
                            {
                                MessageCache.AddMediumOutputMessage(cacheKey, "Person_LinkedSettledCaseCount Format Error", null);
                            }

                        }
                    }
                    else if (personAliases.IncludeUnconfirmed)
                    {
                        foreach (var row in recs)
                        {
                            try
                            {
                                string s = string.Format(formatString, v_PartyTypeMessage(this.v_PartyType_Text, this.v_SubPartyType_Text), row.FirstName, row.LastName, MDA.Common.Helpers.DateHelper.DateOfBirthMessage(row.DateOfBirth),
                                    v_PartyTypeMessage2(row.PartyTypeText, row.SubPartyText), string.Format("{0:d/M/yyyy}", row.IncidentDate), v_ClaimTypeMessage(row.ClaimType), row.KeoghsEliteReference, row.FiveGrading, "");

                                MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                            }
                            catch (System.FormatException)
                            {
                                MessageCache.AddMediumOutputMessage(cacheKey, "Person_LinkedSettledCaseCount Format Error", null);
                            }
                        }
                    }
                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_NumberOfIncidents(string cacheKey, string matchType, string incidentSource, string incidentType, bool? isPotentialClaimant, string periodSkip, string periodCount) //, string searchDate)
        {
            string key = string.Format("NOfIncidents: Id:{0}, P1:{1}, P2:{2}, P3:{3}, P4:{4}, P5:{5}, P6:{6}", this.Db_Id, matchType, incidentSource, incidentType, isPotentialClaimant, periodSkip, periodCount); //, searchDate);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                result = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_NumberOfIncidents(this.Db_Incident_Id, this.RiskClaimId, this.Db_Id, this.v_IncidentDate, this.v_PartyType, this.v_SubPartyType, personAliases, incidentSource, incidentType, periodSkip, periodCount, isPotentialClaimant, /*searchDate,*/ _trace);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey);

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result);

            return result;
        }

        public virtual int v_TelephoneNumberOfOpenCases(string cacheKey, string matchType, string linkType, bool? isPotentialClaimant, bool? stagedContrived, string periodSkip, string periodCount)
        {
            string key = string.Format("TelephoneNOfCases: Id:{0}, P1:{1}, P2:{2}, P3:{3}, P4:{4}, P5:{5}", this.Db_Id, matchType, linkType, isPotentialClaimant.ToString(), periodSkip, periodCount);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Person." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                EntityAliases personAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Person_TelephoneNumberOfOpenCases(this.Db_Id, this.RiskClaimId, this.PeopleOnClaimAndTheirAliasIds, personAliases, linkType, isPotentialClaimant, stagedContrived, periodSkip, periodCount, _trace);

                // We are populating the medium here so copy Low and High Only
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);

                if (recs != null && recs.Count > 0)
                {
                    result = recs.Count;

                    if (personAliases.IncludeThis || personAliases.IncludeConfirmed)
                    {
                        foreach (var row in recs)
                        {
                            try
                            {
                                MessageCache.AddMediumOutputMessage(cacheKey, null, null);
                            }
                            catch (System.FormatException)
                            {
                                MessageCache.AddMediumOutputMessage(cacheKey, "Person_TelephoneNumberOfOpenCases Format Error", null);
                            }

                        }
                    }
                    else if (personAliases.IncludeUnconfirmed)
                    {
                        foreach (var row in recs)
                        {
                            try
                            {
                                MessageCache.AddMediumOutputMessage(cacheKey, null, null);
                            }
                            catch (System.FormatException)
                            {
                                MessageCache.AddMediumOutputMessage(cacheKey, "Person_TelephoneNumberOfOpenCases", null);
                            }
                        }
                    }
                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

            return result;
        }


        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        public override void AssignUsedProperties(IRiskEntity dest)
        {
            PersonRisk d = dest as PersonRisk;

            if (_v_Age != null)
                d.v_Age = v_Age;

            //if (_v_PaymentCardKeyAttractor != null)
            //    d.v_PaymentCardKeyAttractor = v_PaymentCardKeyAttractor;

            if (_v_AllPaymentCardKeyAttractor != null)
                d.v_AllPaymentCardKeyAttractor = v_AllPaymentCardKeyAttractor;

            //if (_v_IsPaymentCardKeyAttractor != null)
            //    d.v_IsPaymentCardKeyAttractor = v_IsPaymentCardKeyAttractor;

            if (_v_IsAnyPaymentCardKeyAttractor != null)
                d.v_IsAnyPaymentCardKeyAttractor = v_IsAnyPaymentCardKeyAttractor;

            if (_v_BankAccountKeyAttractor != null)
                d.v_BankAccountKeyAttractor = v_BankAccountKeyAttractor;

            if (_v_AllBankAccountKeyAttractor != null)
                d.v_AllBankAccountKeyAttractor = v_AllBankAccountKeyAttractor;

            //if (_v_IsBankAccountKeyAttractor != null)
            //    d.v_IsBankAccountKeyAttractor = v_IsBankAccountKeyAttractor;

            if (_v_IsAnyBankAccountKeyAttractor != null)
                d.v_IsAnyBankAccountKeyAttractor = v_IsAnyBankAccountKeyAttractor;

            if (_v_EmailKeyAttractor != null)
                d.v_EmailKeyAttractor = v_EmailKeyAttractor;

            if (_v_AllEmailKeyAttractor != null)
                d.v_AllEmailKeyAttractor = v_AllEmailKeyAttractor;

            //if (_v_IsEmailKeyAttractor != null)
            //    d.v_IsEmailKeyAttractor = v_IsEmailKeyAttractor;

            if (_v_IsAnyEmailKeyAttractor != null)
                d.v_IsAnyEmailKeyAttractor = v_IsAnyEmailKeyAttractor;

            d.v_PersonFraudRulesTriggered = v_PersonFraudRulesTriggered;

        }

        protected PersonRisk ShallowCopyThis(PersonRisk c)
        {
            c.db = db;

            c.Db_Id = Db_Id;
            c.Db_Incident_Id = Db_Incident_Id;

            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;

            c.v_PersonPolicyLink = v_PersonPolicyLink;
            c.v_PersonPolicyLink_Text = v_PersonPolicyLink_Text;

            c.v_PersonVehicleLink = v_PersonVehicleLink;
            c.v_PersonVehicleLink_Text = v_PersonVehicleLink_Text;

            c.v_PartyType = v_PartyType;
            c.v_PartyType_Text = v_PartyType_Text;

            c.v_SubPartyType = v_SubPartyType;
            c.v_SubPartyType_Text = v_SubPartyType_Text;

            c.v_Salutation = v_Salutation;
            c.v_Salutation_Text = v_Salutation_Text;

            c.v_PersonFraudRulesTriggered = v_PersonFraudRulesTriggered;

            c.v_Gender = v_Gender;
            c.v_Gender_Text = v_Gender_Text;

            c.v_FirstName = v_FirstName;
            c.v_MiddleName = v_MiddleName;
            c.v_LastName = v_LastName;
            c.v_DateOfBirth = v_DateOfBirth;
            c.v_Nationality = v_Nationality;
            c.v_Occupation = v_Occupation;

            c.v_NINumber = v_NINumber;
            c.v_PassportNumber = v_PassportNumber;
            c.v_EmailAddress = v_EmailAddress;

            c.v_AttendedHospital = v_AttendedHospital;

            c.v_ClaimStatus = v_ClaimStatus;
            c.v_ClaimStatus_Text = v_ClaimStatus_Text;

            c.v_IncidentLocation = v_IncidentLocation;
            c.v_IncidentCircumstances = v_IncidentCircumstances;
            c.v_IncidentDateTime = v_IncidentDateTime;

            c.v_PoliceAttended = v_PoliceAttended;
            c.v_PoliceForce = v_PoliceForce;
            c.v_PoliceReference = v_PoliceReference;
            c.v_AmbulanceAttended = v_AmbulanceAttended;

            c.v_DrivingLicenseNumber = v_DrivingLicenseNumber;

            c.v_SanctionDate = v_SanctionDate;
            c.v_SanctionSource = v_SanctionSource;
            c.v_TSIDUAMLResult = v_TSIDUAMLResult;
            c.v_TracemartDOB = v_TracemartDOB;
            c.v_ExperianDOB = v_ExperianDOB;
            c.v_GoneAway = v_GoneAway;
            c.v_Insolvency = v_Insolvency;
            c.v_InsolvencyHistory = v_InsolvencyHistory;
            //c.v_InsolvencyStatus       = v_InsolvencyStatus;
            c.v_CCJHistory = v_CCJHistory;
            c.v_DeathScreenMatchType = v_DeathScreenRegNo;
            c.v_DeathScreenDoD = v_DeathScreenDoD;
            c.v_DeathScreenRegNo = v_DeathScreenRegNo;
            c.v_BankAccountValidated = v_BankAccountValidated;
            c.v_DrivingLicenseValidated = v_DrivingLicenseValidated;
            c.v_NINumberValidated = v_NINumberValidated;
            c.v_PassportMRZValid = v_PassportMRZValid;
            c.v_PassportDOBValid = v_PassportDOBValid;
            c.v_PassportGenderValid = v_PassportGenderValid;
            c.v_PaymentCardNumberValid = v_PaymentCardNumberValid;
            c.v_TracesmartServicesCalled = v_TracesmartServicesCalled;

            c.v_MojStatus = v_MojStatus;
            c.v_MojStatus_Text = v_MojStatus_Text;

            c.v_ClaimNotificationDate = v_ClaimNotificationDate;
            c.v_PaymentsToDate = v_PaymentsToDate;
            c.v_Reserve = v_Reserve;

            c.v_AttendedHospital = v_AttendedHospital;

            c.v_BankAccountDb_Id = v_BankAccountDb_Id;
            c.v_BankAccountName = v_BankAccountName;
            c.v_BankAccountSortCode = v_BankAccountSortCode;
            c.v_BankAccountAccountNumber = v_BankAccountAccountNumber;
            //c.v_IsBankAccountKeyAttractor  = v_IsBankAccountKeyAttractor;
            c.v_BankAccountKeyAttractor = v_BankAccountKeyAttractor;
            c.v_BankAccountFraudRingStatus = v_BankAccountFraudRingStatus;

            c.v_PaymentCardDb_Id = v_PaymentCardDb_Id;
            c.v_PaymentCardSortCode = v_PaymentCardSortCode;
            c.v_PaymentCardBankName = v_PaymentCardBankName;
            c.v_PaymentCardAccountNumber = v_PaymentCardAccountNumber;
            //c.v_IsPaymentCardKeyAttractor  = v_IsPaymentCardKeyAttractor;
            c.v_PaymentCardKeyAttractor = v_PaymentCardKeyAttractor;
            c.v_PaymentCardFraudRingStatus = v_PaymentCardFraudRingStatus;

            c.PersonsTelephones = new List<TelephoneRisk>();
            if (PersonsTelephones != null)
            {
                foreach (var x in PersonsTelephones)
                    c.PersonsTelephones.Add((TelephoneRisk)x.Clone());
            }

            c.v_LinkedAlias = new List<Person_AliasInformation_Result>();
            if (v_LinkedAlias != null)
            {
                foreach (var x in v_LinkedAlias)
                    c.v_LinkedAlias.Add(x.Clone());
            }


            c.v_LinkedAddress = new List<Person_LinkedAddresses_Result>();
            if (v_LinkedAddress != null)
            {
                foreach (var x in v_LinkedAddress)
                    c.v_LinkedAddress.Add(x.Clone());
            }


            c.v_LinkedTelephone = new List<Person_LinkedTelephones>();
            if (v_LinkedTelephone != null)
            {
                foreach (var x in v_LinkedTelephone)
                    c.v_LinkedTelephone.Add(x.Clone());
            }

            c.v_LinkedEmails = new List<Person_LinkedEmails>();
            if (v_LinkedEmails != null)
            {
                foreach (var x in v_LinkedEmails)
                    c.v_LinkedEmails.Add(x.Clone());
            }

            c.v_LinkedNINumbers = new List<Person_LinkedNINumbers>();
            if (v_LinkedNINumbers != null)
            {
                foreach (var x in v_LinkedNINumbers)
                    c.v_LinkedNINumbers.Add(x.Clone());
            }

            c.v_LinkedDrivingLicenceNumbers = new List<Person_LinkedDrivingLicenceNumbers>();
            if (v_LinkedDrivingLicenceNumbers != null)
            {
                foreach (var x in v_LinkedDrivingLicenceNumbers)
                    c.v_LinkedDrivingLicenceNumbers.Add(x.Clone());
            }

            c.v_LinkedPassportNumbers = new List<Person_LinkedPassportNumbers>();
            if (v_LinkedPassportNumbers != null)
            {
                foreach (var x in v_LinkedPassportNumbers)
                    c.v_LinkedPassportNumbers.Add(x.Clone());
            }

            c.v_LinkedBankAccounts = new List<Person_LinkedBankAccounts>();
            if (v_LinkedBankAccounts != null)
            {
                foreach (var x in v_LinkedBankAccounts)
                    c.v_LinkedBankAccounts.Add(x.Clone());
            }


            //c.v_IsKeyAttractor = v_IsKeyAttractor;
            c.v_KeyAttractor = v_KeyAttractor;

            c.v_TelephoneAtMultipleAddressCount = v_TelephoneAtMultipleAddressCount;

            c.v_NIToPersonCount = v_NIToPersonCount;
            c.v_PassportToPersonCount = v_PassportToPersonCount;
            c.v_IncidentDate = v_IncidentDate;

            c.ConfirmedAliases = new ListOfInts();
            if (ConfirmedAliases != null)
                c.ConfirmedAliases.AddRange(ConfirmedAliases);

            c.UnconfirmedAliases = new ListOfInts();
            if (UnconfirmedAliases != null)
                c.UnconfirmedAliases.AddRange(UnconfirmedAliases);

            c.TentativeAliases = new ListOfInts();
            if (TentativeAliases != null)
                c.TentativeAliases.AddRange(TentativeAliases);

            c.PeopleOnClaimAndTheirAliasIds = new ListOfInts();
            if (PeopleOnClaimAndTheirAliasIds != null)
                c.PeopleOnClaimAndTheirAliasIds.AddRange(PeopleOnClaimAndTheirAliasIds);

            //Custom properties
            //AssignUsedProperties(c);

            return c;
        }

        public override object Clone()
        {
            if (_trace)
            {
                ADATrace.WriteLine("Cloning Person [" + Db_Id.ToString() + "]");
            }
            PersonRisk c = new PersonRisk();

            ShallowCopyThis(c);

            if (_trace)
            {
                ADATrace.WriteLine("Cloning Person Complete");
            }

            return c;
        }

        //[DataMember]
        //public override int? AutoKACount
        //{
        //    get
        //    {
        //        if (Locked)
        //        {
        //            if (_trace)
        //            {
        //                string x = (_AutoKACount == null) ? "NA" : _AutoKACount.ToString();

        //                ADATrace.WriteLine("Person.AutoKACount. Serializing [" + x.ToString() + "]");
        //            }

        //            return _AutoKACount;
        //        }

        //        if (_AutoKACount != null)
        //            return (int)_AutoKACount;

        //        _AutoKACount = 0;

        //        if (this.v_IsKeyAttractor) _AutoKACount += 1;
        //        if ((bool)this.v_IsAnyBankAccountKeyAttractor) _AutoKACount += 1;
        //        if ((bool)this.v_IsAnyEmailKeyAttractor) _AutoKACount += 1;
        //        if ((bool)this.v_IsAnyPaymentCardKeyAttractor) _AutoKACount += 1;
        //        if (this.v_IsAnyTelephoneKeyAttractor("")) _AutoKACount += 1;

        //        return (int)_AutoKACount;
        //    }
        //}

        //private string[,] ReportStrings = new string[,]
        //{
        //    { "Involvement Unknown", "Driver", "Passenger", "Witness" },
        //    { "Claimant", "Claimant Driver", "Claimant Passenger", "Witness" },
        //    { "Hirer", "Driver", "Passenger", "Witness" },
        //    { "Insured", "Insured Driver", "Insured Vehicle Passenger", "Insured's Witness" },
        //    { "Litigation Friend", "Litigation Friend", "Litigation Friend", "Litigation Friend" },
        //    { "PolicyHolder", "Insured Driver", "Insured Vehicle Passenger", "Insured's Witness" },
        //    { "Third Party", "Third Party Driver", "Third Party Vehicle Passenger", "Third Party Witness" },
        //    { "Witness", "Witness", "Witness", "Witness" },
        //    { "Vehicle Owner", "Vehicle Owner", "Vehicle Owner", "Vehicle Owner" },
        //    { "Paid Policy", "Paid Policy", "Paid Policy", "Paid Policy" },
        //};

        public override string ToString()
        {
            return DisplayText;
        }

        public override string DisplayText
        {
            get
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;

                StringBuilder s = new StringBuilder("Person: ");

                if (v_FirstName != null)
                    s.Append(textInfo.ToTitleCase(v_FirstName));

                if (!string.IsNullOrEmpty(v_MiddleName))
                {
                    if (s.Length > 0)
                        s.Append(" ");

                    s.Append(textInfo.ToTitleCase(v_MiddleName));
                }

                if (!string.IsNullOrEmpty(v_LastName))
                {
                    if (s.Length > 0)
                        s.Append(" ");

                    s.Append(v_LastName.ToUpper());
                }

                return s.ToString() + " [" + MDA.Common.Helpers.LookupHelper.PartyTypeToString((int)v_PartyType, (int)v_SubPartyType) + "]";
            }
        }

        [DataMember]
        public override string ReportHeading1
        {
            get
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;

                StringBuilder s = new StringBuilder("Person: ");

                if (v_FirstName != null)
                    s.Append(textInfo.ToTitleCase(v_FirstName));

                if (!string.IsNullOrEmpty(v_MiddleName))
                {
                    if (s.Length > 0)
                        s.Append(" ");

                    s.Append(textInfo.ToTitleCase(v_MiddleName));
                }

                if (!string.IsNullOrEmpty(v_LastName))
                {
                    if (s.Length > 0)
                        s.Append(" ");

                    s.Append(v_LastName.ToUpper());
                }

                return s.ToString() + " [" + MDA.Common.Helpers.LookupHelper.PartyTypeToString((int)v_PartyType, (int)v_SubPartyType) + "]";
            }
        }

        [DataMember]
        public override string ReportHeading2
        {
            get
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;

                StringBuilder s = new StringBuilder();

                if (v_FirstName != null)
                    s.Append(textInfo.ToTitleCase(v_FirstName));

                if (!string.IsNullOrEmpty(v_MiddleName))
                {
                    if (s.Length > 0)
                        s.Append(" ");

                    s.Append(textInfo.ToTitleCase(v_MiddleName));
                }

                if (!string.IsNullOrEmpty(v_LastName))
                {
                    if (s.Length > 0)
                        s.Append(" ");

                    s.Append(v_LastName.ToUpper());
                }

                return MDA.Common.Helpers.LookupHelper.PartyTypeToString((int)v_PartyType, (int)v_SubPartyType) + " - " + s.ToString();
            }
        }
    }
}