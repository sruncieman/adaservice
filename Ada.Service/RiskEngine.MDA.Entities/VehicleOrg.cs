﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using MDA.Common.Debug;

namespace RiskEngine.Scoring.Entities
{
    #region Testing Classes
    public class OrganisationVehicleRiskEditExtras
    {
        //public bool v_VehLinkHighRiskOrgDef { get; set; }
    }

    public class OrganisationVehicleRiskEdit : OrganisationVehicleRisk
    {
        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();


        public OrganisationVehicleRiskEdit()
        {
            v_VehicleType = (int)VehicleType.Car;
            v_VehicleType_Text = v_VehicleType.ToString();

            v_Colour = (int)VehicleColour.Unknown;
            v_Colour_Text = v_Colour.ToString();

            v_VehicleIncidentLink = (int)Incident2VehicleLinkType.Unknown;
            v_VehicleIncidentLink_Text = v_VehicleIncidentLink.ToString();

            v_VehicleType = (int)VehicleType.Unknown;
            v_VehicleType_Text = v_VehicleType.ToString();

            v_Fuel = (int)VehicleFuelType.Unknown;
            v_Fuel_Text = v_Fuel.ToString();

            v_Transmission = (int)VehicleTransmission.Unknown;
            v_Transmission_Text = v_Transmission.ToString();
        }

        #region Extras
        public OrganisationVehicleRiskEditExtras Extras;

        //public override bool v_VehLinkHighRiskOrgDef
        //{
        //    get { return Extras.v_VehLinkHighRiskOrgDef; }
        //    set { Extras.v_VehLinkHighRiskOrgDef = value; }
        //}
        #endregion

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, int KeyAttCount, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
                ClientScores[orgName].RuleGroupKeyAttractorTotalCount = KeyAttCount;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }


        public OrganisationVehicleRiskEdit CloneEntity(bool cloneChildren)
        {
            OrganisationVehicleRiskEdit c = new OrganisationVehicleRiskEdit();

            c.v_VehicleType              = v_VehicleType;
            c.v_VehicleType_Text         = v_VehicleType_Text;
            c.v_VehicleIncidentLink      = v_VehicleIncidentLink;
            c.v_VehicleIncidentLink_Text = v_VehicleIncidentLink_Text;
            c.v_VehicleRegistration      = v_VehicleRegistration;
            c.v_Colour                   = v_Colour;
            c.v_Colour_Text              = v_Colour_Text;
            c.v_Make                     = v_Make;
            c.v_Model                    = v_Model;
            c.v_EngineCapacity           = v_EngineCapacity;
            c.v_VIN                      = v_VIN;
            c.v_Fuel                     = v_Fuel;
            c.v_Fuel_Text                = v_Fuel_Text;
            c.v_Transmission             = v_Transmission;
            c.v_Transmission_Text        = v_Transmission_Text;
            c.v_HireStartDate            = v_HireStartDate;
            c.v_HireEndDate              = v_HireEndDate;

            //c.v_VehLinkHighRiskOrgDef = v_VehLinkHighRiskOrgDef;
            return c;
        }
    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class OrganisationVehicleRisk : RiskEntityBase, IRiskEntity
    {
        public OrganisationVehicleRisk()
        {
        }

        #region Public Scoring Properties
        [DataMember(Name = "Veh_DbId")]
        public int Db_Id { get; set; }

        [DataMember(Name = "v_VT")]
        public int v_VehicleType { get; set; }  //VehicleType
        [DataMember(Name = "v_VTt")]
        public string v_VehicleType_Text { get; set; }

        [DataMember(Name = "v_VIL")]
        public int v_VehicleIncidentLink { get; set; }  //Incident2VehicleLinkType
        [DataMember(Name = "v_VILt")]
        public string v_VehicleIncidentLink_Text { get; set; }

        [DataMember(Name = "v_VR")]
        public string v_VehicleRegistration { get; set; }

        [DataMember(Name = "v_C")]
        public int v_Colour { get; set; }  //VehicleColour
        [DataMember(Name = "v_Ct")]
        public string v_Colour_Text { get; set; }

        [DataMember(Name = "v_Ma")]
        public string v_Make { get; set; }
        [DataMember(Name = "v_Mo")]
        public string v_Model { get; set; }
        [DataMember(Name = "v_EC")]
        public string v_EngineCapacity { get; set; }
        [DataMember(Name = "v_VIN")]
        public string v_VIN { get; set; }

        [DataMember(Name = "v_F")]
        public int v_Fuel { get; set; }  //VehicleFuelType
        [DataMember(Name = "v_Ft")]
        public string v_Fuel_Text { get; set; }

        [DataMember(Name = "v_T")]
        public int v_Transmission { get; set; }  //VehicleTransmission
        [DataMember(Name = "v_Tt")]
        public string v_Transmission_Text { get; set; }

        [DataMember(Name = "v_HSD")]
        public DateTime? v_HireStartDate { get; set; }
        [DataMember(Name = "v_HED")]
        public DateTime? v_HireEndDate { get; set; }

        //[DataMember(Name = "OrgAddr")]
        //public AddressRisk OrganisationsAddress { get; set; }

        #endregion Public Scoring Properties

        #region Public Alias Properties

        [DataMember(Name = "ConA")]
        public ListOfInts ConfirmedAliases { get; set; }
        [DataMember(Name = "UConA")]
        public ListOfInts UnconfirmedAliases { get; set; }
        [DataMember(Name = "TenA")]
        public ListOfInts TentativeAliases { get; set; }

        #endregion Public Alias Properties

        #region Scoring Methods

        public virtual int v_NumberOfHirePeriods(string cacheKey, string matchType)
        {
            string key = string.Format("NOfHirePeriods: Id:{0}, P1:{1}", this.Db_Id, matchType); 
            int result = 0;

            if (_trace) ADATrace.WriteLine("Vehicle." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                EntityAliases vehicleAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Vehicle_ConcurrentHirePeriodCount(this.RiskClaimId, this.Db_Id,vehicleAliases, this.v_HireStartDate, this.v_HireEndDate);

                result = recs;

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });


            }
            if (_trace) ADATrace.WriteLine("Vehicle." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_NumberOfHireOverlappedDays(string cacheKey, string matchType)
        {
            string key = string.Format("NOfHireOverlappedDays: Id:{0}, P1:{1}", this.Db_Id, matchType);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Vehicle." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                EntityAliases vehicleAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Vehicle_ConcurrentHireDayCount(this.RiskClaimId, this.Db_Id, vehicleAliases, this.v_HireStartDate, this.v_HireEndDate);

                result = recs;

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });


            }
            if (_trace) ADATrace.WriteLine("Vehicle." + key + " = " + result.ToString());

            return result;
        }

        #endregion Scoring Methods

        protected OrganisationVehicleRisk ShallowCopyThis(OrganisationVehicleRisk c)
        {
            c.db = db;

            c.Db_Id = Db_Id;
            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;

            c.v_VehicleType = v_VehicleType;
            c.v_VehicleType_Text = v_VehicleType_Text;
            c.v_VehicleIncidentLink = v_VehicleIncidentLink;
            c.v_VehicleIncidentLink_Text = v_VehicleIncidentLink_Text;
            c.v_VehicleRegistration = v_VehicleRegistration;
            c.v_Colour = v_Colour;
            c.v_Colour_Text = v_Colour_Text;
            c.v_Make = v_Make;
            c.v_Model = v_Model;
            c.v_EngineCapacity = v_EngineCapacity;
            c.v_VIN = v_VIN;
            c.v_Fuel = v_Fuel;
            c.v_Fuel_Text = v_Fuel_Text;
            c.v_Transmission = v_Transmission;
            c.v_Transmission_Text = v_Transmission_Text;
            c.v_HireStartDate = v_HireStartDate;
            c.v_HireEndDate = v_HireEndDate;

            c.ConfirmedAliases = new ListOfInts();
            if (ConfirmedAliases != null)
                c.ConfirmedAliases.AddRange(ConfirmedAliases);

            c.UnconfirmedAliases = new ListOfInts();
            if (UnconfirmedAliases != null)
                c.UnconfirmedAliases.AddRange(UnconfirmedAliases);

            c.TentativeAliases = new ListOfInts();
            if (TentativeAliases != null)
                c.TentativeAliases.AddRange(TentativeAliases);

            return c;
        }

        public override object Clone()
        {
            if (_trace)
            {
                ADATrace.WriteLine("Cloning Vehicle [" + Db_Id.ToString() + "]");
            }

            OrganisationVehicleRisk c = new OrganisationVehicleRisk();

            ShallowCopyThis(c);

            if (_trace)
            {
                ADATrace.WriteLine("Cloning Vehicle Complete");
            }

            return c;
        }

        //public override int AutoKACount
        //{
        //    get
        //    {
        //        int total = 0;
        //        if (this.v_IsKeyAttractor) total += 1;

        //        return total;
        //    }
        //}

        private string[] ReportStrings = new string[]
        {
             "Vehicle Involvement Unknown" ,
             "Insured Vehicle" ,
             "Third Party Vehicle" ,
             "Insured Hire Vehicle" ,
             "Third Party Hire Vehicle" ,
             "Witness Vehicle" ,
             "No Vehicle Involved" ,

        };

        public override string DisplayText
        {
            get { return "Vehicle: " + v_VehicleRegistration + "  [" + ReportStrings[(int)v_VehicleIncidentLink] + "]"; }
        }

        [DataMember]
        public override string ReportHeading1
        {
            get { return "Vehicle: " + v_VehicleRegistration + "  [" + ReportStrings[(int)v_VehicleIncidentLink] + "]"; }
        }

        [DataMember]
        public override string ReportHeading2
        {
            get { return "Vehicle: " + v_VehicleRegistration + "  [" + ReportStrings[(int)v_VehicleIncidentLink] + "]"; }
        }
    }
}
