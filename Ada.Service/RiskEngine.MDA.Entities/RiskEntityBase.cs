﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using System.Runtime.Serialization;
using System.Diagnostics;
using MDA.Common.Debug;
using System.Configuration;
using MDA.Common.Helpers;
using RiskEngine.Model;
using MDA.Common.Server;
using System.Collections;
using MDA.Common;


namespace RiskEngine.Scoring.Entities
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public abstract class RiskEntityBase
    {
        protected bool _trace;
        private CurrentContext _ctx;

        [Browsable(false)]
        public DbContext db { get; set; }

        [Browsable(false)]
        public CurrentContext ctx 
        {
            get
            {
                return _ctx;
            }

            set
            {
                _ctx = value;
                db = (DbContext)_ctx.db;
                Trace = ctx.TraceScoring;
            }
        }

        [Browsable(false)]
        public bool Locked { get; set; }

        #region Properties common to risk entites
        
        [Browsable(false)]
        public List<RuleScoreData> RuleScoreData { get; set; }

        [Browsable(false)]
        public List<RuleVariableData> RuleVariableData { get; set; }

        [Browsable(false)]
        public string RuleSetName { get; set; }

        [Browsable(false)]
        public string ExecutingRuleName { get; set; }

        [Browsable(false)]
        public int RuleSetId { get; set; }

        [Browsable(false)]
        [DataMember(Name = "RCId")]
        public int RiskClaimId { get; set; }

        [Browsable(false)]
        [DataMember(Name = "BRCId")]
        public int BaseRiskClaimId { get; set; }

        [Browsable(false)]
        public DictionaryOfRiskResults Results { get; set; }

        [Browsable(false)]
        public MessageCache MessageCache { get; set; }

        [Browsable(false)]
        public int UserKACount { get; set; }

        //[Browsable(false)]
        //public int TotalAutoKACount { get; set; }

        public int AddUpEntityRuleSetScores()
        {
            int _Score = 0;

            if (Results != null)
            {
                foreach (KeyValuePair<string, RiskResult> key in Results)
                {
                    _Score += key.Value.Score;
                }
            }

            return _Score;
        }


        //private int _TotalEntityScore = 0;

        [Browsable(false)]
        [DataMember(Name="TES")]
        public int TotalEntityScore { get; set; }

        //private int _TotalChildScore = 0;

        [Browsable(false)]
        [DataMember(Name = "TCS")]
        public int TotalChildScore { get; set; }

        [Browsable(false)]
        [DataMember]
        public ListOfStrings Errors { get; set; }
        #endregion

        [DataMember]
        public CalculateRiskResponse _ScoreResults { get; set; }

        [Browsable(false)]
        [DataMember(Name = "MRH")]
        public DictionaryOfMethodResultRecords MethodResultHistory { get; set; }

        [Browsable(false)]
        public MyVars MyVars { get; set; }

        public bool Trace 
        {
            get { return _trace; }
            set { _trace = value; }
        }

        public RiskEntityBase()
        {
            Results = new DictionaryOfRiskResults(); // new Dictionary<string, RiskResult>();
            MethodResultHistory = new DictionaryOfMethodResultRecords(); // new Dictionary<string, MethodResultRecord>();
            Errors = new ListOfStrings();
            MyVars = new MyVars();
            MessageCache = new MessageCache();
 
            TotalEntityScore = 0;
            UserKACount = 0;
            //TotalAutoKACount = 0;

            Trace = false;

            Locked = true;

            _traceMyString = _trace;
        }

        public virtual object Clone()
        {
            return this;
        }

        //[Browsable(false)]
        //[DataMember]
        public virtual string DisplayText
        {
            get { return "RiskEntityBase: method need overriding in entity"; }
            set { _DisplayText = value;  }
        }
        protected string _DisplayText;

        //[Browsable(false)]
        [DataMember(Name = "RH1")]
        public virtual string ReportHeading1
        {
            get { return "RiskEntityBase: method need overriding in entity"; }
            set { _ReportHeading1 = value; }
        }
        protected string _ReportHeading1;

        //[Browsable(false)]
        [DataMember(Name = "RH2")]
        public virtual string ReportHeading2
        {
            get { return "RiskEntityBase: method need overriding in entity"; }
            set { _ReportHeading2 = value; }
        }
        protected string _ReportHeading2;


        public MessageNode GetScoresAsNodeTree(bool includeChildren)
        {
            string name = GetType().Name;

            MessageNode node = (_ScoreResults == null) ? new MessageNode("No " + name)
                       : _ScoreResults.ScoresAsNodes(DisplayText + " SCORE=[" + TotalEntityScore + ", " + TotalChildScore + "]", true);

            if (!includeChildren) return node;

            // Now find all the properties with the SCOREABLE attribute on THIS entity
            var propList = from p in GetType().GetProperties()
                           let attr = p.GetCustomAttributes(typeof(ScoreableItemAttribute), true)
                           where attr.Length == 1
                           select new { Property = p, Attribute = attr.First() as ScoreableItemAttribute };

            // Now cycle through each property
            foreach (var p in propList)
            {
                // Now check if the property is a collection.  If so score each item in the collection otherwise score the single property value
                if (p.Attribute.IsCollection)
                {
                    IEnumerable items = (IEnumerable)p.Property.GetValue(this);   // Get the list to cycle through

                    foreach (var item in items)  // Score each list item
                    {
                        if (item != null)
                            node.AddNode(((IRiskEntity)item).GetScoresAsNodeTree(includeChildren));
                    }
                }
                else
                {
                    var item = (IRiskEntity)p.Property.GetValue(this);  // Get the single value

                    if (item != null)
                        node.AddNode(((IRiskEntity)item).GetScoresAsNodeTree(includeChildren));
                }
            }

            return node;
        }

        //[Browsable(false)]
        //[DataMember]
        //public virtual int? AutoKACount
        //{
        //    get { return 0; }
        //    set { _AutoKACount = value; }
        //}
        //protected int? _AutoKACount;

        public bool SetRuleName(string ruleName)
        {
            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine(string.Format("<<<Current Rule being executed>>>> : {0}.{1}", this.RuleSetName, ruleName));
            }

            ExecutingRuleName = ruleName;
            return true;
        }

        public void LogError(string error)
        {
            Errors.Add(string.Format("[{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, error));

            if (_trace) ADATrace.WriteLine(string.Format("[{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, error));
        }

        public void WriteTrace(string traceMsg)
        {
            if (_trace)
                ADATrace.WriteLine(traceMsg);
        }

        private RiskResult GetResultKey(string ruleName)
        {
            RiskResult res;

            if (!Results.ContainsKey(ruleName))
            {
                res = new RiskResult();
                res.RuleSetId = this.RuleSetId;

                Results.Add(ruleName, res);
            }
            else
                res = Results[ruleName];

            return res;
        }

        #region Get and Set KA Count

        public void UpdateKeyAttractorCount(int increment)
        {
            UserKACount += increment;
        }

        public void SetKeyAttractorCount(int value)
        {
            UserKACount = value;
        }

        public int GetKeyAttractorCount()
        {
            return UserKACount;
        }
        #endregion

        #region Get and Set Score

        public void UpdateScore(int increment)
        {
            //if (increment != 0)
            //{
                GetResultKey(ExecutingRuleName).Score += increment;

                if (_trace)
                {
                    ADATrace.WriteLine("");
                    ADATrace.WriteLine("-----------------------------------------------------------------------");
                    ADATrace.WriteLine(string.Format("|UpdateScore: [{0}].[{1}] : Adding [{2}] : RuleScore[{3}]", this.RuleSetName, ExecutingRuleName, increment.ToString(), GetResultKey(ExecutingRuleName).Score.ToString()));
                    ADATrace.WriteLine("-----------------------------------------------------------------------");
                    ADATrace.WriteLine("");
                }
            //}
        }

        public void SetScore(int score)
        {
            GetResultKey(ExecutingRuleName).Score = score;

            if (_trace)
            {
                ADATrace.WriteLine("");
                ADATrace.WriteLine("-----------------------------------------------------------------------");
                ADATrace.WriteLine(string.Format("|SetScore: [{0}].[{1}] : SetScore to [{2}] : RuleScore[{3}]", this.RuleSetName, ExecutingRuleName, score.ToString(), GetResultKey(ExecutingRuleName).Score.ToString()));
                ADATrace.WriteLine("-----------------------------------------------------------------------");
                ADATrace.WriteLine("");
            }
        }

        public int GetScore()
        {
            if (!Results.ContainsKey(ExecutingRuleName))
                return 0;

            return GetResultKey(ExecutingRuleName).Score;
        }

        public int GetTotalScore()
        {
            return TotalEntityScore;
        }
        #endregion


        #region Output DATA table


        private string DumpData(ListOfListOfStringData data)
        {
            StringBuilder ss = new StringBuilder();

            foreach (var l in data)
            {
                ss.Append("[" + string.Join(",", l) + "]");
            }

            return ss.ToString();
        }

        /// <summary>
        /// Add row of data to output table for LOW message data
        /// </summary>
        /// <param name="data">Lists of strings that make up a row</param>
        public void AddLowMessageData(ListOfStringData data)
        {
            if (data.Count > 0)
            {
                RiskResult res = GetResultKey(ExecutingRuleName);

                res.DataLow.Add(data);

                if (_trace)
                    ADATrace.WriteLine(string.Format("AddLowMessageData: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, DumpData(res.DataLow)));
            }
        }

        /// <summary>
        /// Set output table for LOW message data
        /// </summary>
        /// <param name="data">List of Lists of strings that make up the table</param>
        public void SetLowMessageData(ListOfListOfStringData data)
        {
            if (data.Count > 0)
            {
                RiskResult res = GetResultKey(ExecutingRuleName);

                res.DataLow = data;

                if (_trace)
                    ADATrace.WriteLine(string.Format("SetLowMessageData: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, DumpData(res.DataLow)));
            }
        }

        /// <summary>
        /// Add row of data to output table for MED message data
        /// </summary>
        /// <param name="data">Lists of strings that make up a row</param>
        public void AddMediumMessageData(ListOfStringData data)
        {
            if (data.Count > 0)
            {
                RiskResult res = GetResultKey(ExecutingRuleName);

                res.DataMedium.Add(data);

                if (_trace)
                    ADATrace.WriteLine(string.Format("AddMediumMessageData: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, DumpData(res.DataMedium)));
            }
        }

        /// <summary>
        /// Set output table for MED message data
        /// </summary>
        /// <param name="data">List of Lists of strings that make up the table</param>
        public void SetMediumMessageData(ListOfListOfStringData data)
        {
            if (data.Count > 0)
            {
                RiskResult res = GetResultKey(ExecutingRuleName);

                res.DataMedium = data;

                if (_trace)
                    ADATrace.WriteLine(string.Format("SetMediumMessageData: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, DumpData(res.DataMedium)));
            }
        }

        /// <summary>
        /// Add row of data to output table for HIGH message data
        /// </summary>
        /// <param name="data">Lists of strings that make up a row</param>
        public void AddHighMessageData(ListOfStringData data)
        {
            if (data.Count > 0)
            {
                RiskResult res = GetResultKey(ExecutingRuleName);

                res.DataHigh.Add(data);

                if (_trace)
                    ADATrace.WriteLine(string.Format("AddHighMessageData: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, DumpData(res.DataHigh)));
            }
        }

        /// <summary>
        /// Set output table for HIGH message data
        /// </summary>
        /// <param name="data">List of Lists of strings that make up the table</param>
        public void SetHighMessageData(ListOfListOfStringData data)
        {
            if (data.Count > 0)
            {
                RiskResult res = GetResultKey(ExecutingRuleName);

                res.DataHigh = data;

                if (_trace)
                    ADATrace.WriteLine(string.Format("SetHighMessageData: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, DumpData(res.DataHigh)));
            }
        }
        #endregion

        #region Output Messages

        /// <summary>
        /// Assign a message to the output for LOW messages
        /// </summary>
        /// <param name="messageList">Message</param>
        public void AddLowMessage(string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                RiskResult res = GetResultKey(ExecutingRuleName);

                res.MessageLow.Add(message);

                if (_trace) ADATrace.WriteLine(string.Format("AddLowMessage: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, string.Join(",", res.MessageLow)));

            }
            else if (_trace) ADATrace.WriteLine("AddLowMessage was BLANK");
        }

        /// <summary>
        /// Assign a message list of type ListOfMsgs to the output for LOW messages
        /// </summary>
        /// <param name="messageList">Message List</param>
        public void AddLowMessages(ListOfMsgs messageList)
        {
            if (_trace) ADATrace.WriteLine("AddLowMessages Count[" + messageList.Count.ToString() + "]");

            if (messageList.Count == 0) return;

            foreach (var m in messageList)
            {
                AddLowMessage(m);
            }
        }

        /// <summary>
        /// Assign a message to the output for MED messages
        /// </summary>
        /// <param name="messageList">Message</param>
        public void AddMediumMessage(string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                RiskResult res = GetResultKey(ExecutingRuleName);

                res.MessageMedium.Add(message);

                if (_trace) ADATrace.WriteLine(string.Format("AddMediumMessage: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, string.Join(",", res.MessageMedium)));

            }
            else if (_trace) ADATrace.WriteLine("AddMediumMessage was BLANK");
        }

        /// <summary>
        /// Assign a message list of type ListOfMsgs to the output for MED messages
        /// </summary>
        /// <param name="messageList">Message List</param>
        public void AddMediumMessages(ListOfMsgs messageList)
        {
            if (_trace) ADATrace.WriteLine("AddMediumMessages Count[" + messageList.Count.ToString() + "]");

            if (messageList.Count == 0) return;

            foreach (var m in messageList)
            {
                AddMediumMessage(m);
            }
        }

        /// <summary>
        /// Assign a message to the output for HIGH messages
        /// </summary>
        /// <param name="messageList">Message</param>
        public void AddHighMessage(string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                RiskResult res = GetResultKey(ExecutingRuleName);

                res.MessageHigh.Add(message);

                if (_trace) ADATrace.WriteLine(string.Format("AddHighMessage: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, string.Join(",", res.MessageHigh)));

            }
            else if (_trace) ADATrace.WriteLine("AddHighMessage was BLANK");

        }

        /// <summary>
        /// Assign a message list of type ListOfMsgs to the output for HIGH messages
        /// </summary>
        /// <param name="messageList">Message List</param>
        public void AddHighMessages(ListOfMsgs messageList)
        {
            if (_trace) ADATrace.WriteLine("AddHighMessages Count[" + messageList.Count.ToString() + "]");

            if (messageList.Count == 0) return;

            foreach (var m in messageList)
            {
                AddHighMessage(m);
            }
        }
        #endregion

        #region Simple Score and Message Assign

        /// <summary>
        /// Assign THEN score for current rule. This increments/decrements the current score
        /// </summary>
        public void AssignThenScore()
        {
            UpdateScore( GetThenScore());
        }

        /// <summary>
        /// Assign THEN LOW message to output
        /// </summary>
        public void AssignThenLow()
        {
            AddLowMessage( GetThenMessageLow());
        }

        /// <summary>
        /// Assign THEN MED message to output
        /// </summary>
        public void AssignThenMedium()
        {
            AddMediumMessage(GetThenMessageMedium());
        }

        /// <summary>
        /// Assign THEN HIGH message to output
        /// </summary>
        public void AssignThenHigh()
        {
            AddHighMessage(GetThenMessageHigh());
        }

        /// <summary>
        /// Assign ELSE score for current rule. This increments/decrements the current score
        /// </summary>
        public void AssignElseScore()
        {
            UpdateScore(GetElseScore());
        }

        /// <summary>
        /// Assign ELSE LOW message to output
        /// </summary>
        public void AssignElseLow()
        {
            AddLowMessage(GetElseMessageLow());
        }

        /// <summary>
        /// Assign ELSE MED message to output
        /// </summary>
        public void AssignElseMedium()
        {
            AddMediumMessage(GetElseMessageMedium());
        }

        /// <summary>
        /// Assign ELSE HIGH message to output
        /// </summary>
        public void AssignElseHigh()
        {
            AddHighMessage(GetElseMessageHigh());
        }

        /// <summary>
        /// Assign THEN score and LOW, MED, HIGH messages for current rule. This increments/decrements the current score
        /// </summary>
        public void AssignThenAll()
        {
            AssignThenScore();
            AssignThenLow();
            AssignThenMedium();
            AssignThenHigh();
        }

        /// <summary>
        /// Assign ELSE score and LOW, MED, HIGH messages for current rule. This increments/decrements the current score
        /// </summary>
        public void AssignElseAll()
        {
            AssignElseScore();
            AssignElseLow();
            AssignElseMedium();
            AssignElseHigh();
        }

        /// <summary>
        /// Assign THEN LOW, MED, HIGH messages for current rule. Does not change score
        /// </summary>
        public void AssignThenMessages()
        {
            AssignThenLow();
            AssignThenMedium();
            AssignThenHigh();
        }

        /// <summary>
        /// Assign ELSE LOW, MED, HIGH messages for current rule. Does not change score
        /// </summary>
        public void AssignElseMessages()
        {
            AssignElseLow();
            AssignElseMedium();
            AssignElseHigh();
        }
        #endregion

        #region Get and Assign to output the lists of Messages from Cache

        /// <summary>
        /// Get the THEN LOW messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>List of Messages</returns>
        public ListOfMsgs GetThenLowMessagesFromCache(string cacheKey)
        {
            return this.MessageCache.GetLowThenMessages(cacheKey);
        }

        /// <summary>
        /// Get the THEN MED messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>List of Messages</returns>
        public ListOfMsgs GetThenMediumMessagesFromCache(string cacheKey)
        {
            return this.MessageCache.GetMediumThenMessages(cacheKey);
        }

        /// <summary>
        /// Get the THEN HIGH messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>List of Messages</returns>
        public ListOfMsgs GetThenHighMessagesFromCache(string cacheKey)
        {
            return this.MessageCache.GetHighThenMessages(cacheKey);
        }

        /// <summary>
        /// Get the ELSE LOW messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>List of Messages</returns>
        public ListOfMsgs GetElseLowMessagesFromCache(string cacheKey)
        {
            return this.MessageCache.GetLowElseMessages(cacheKey);
        }

        /// <summary>
        /// Get the ELSE MED messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>List of Messages</returns>
        public ListOfMsgs GetElseMediumMessagesFromCache(string cacheKey)
        {
            return this.MessageCache.GetMediumElseMessages(cacheKey);
        }

        /// <summary>
        /// Get the ELSE HIGH messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>List of Messages</returns>
        public ListOfMsgs GetElseHighMessagesFromCache(string cacheKey)
        {
            return this.MessageCache.GetHighElseMessages(cacheKey);
        }

        /// <summary>
        /// Get the THEN LOW input messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>String message</returns>
        public string GetThenLowInMessageFromCache(string cacheKey)
        {
            return this.MessageCache.GetLowThenInMessage(cacheKey); 
        }

        /// <summary>
        /// Get the THEN MED input messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>String message</returns>
        public string GetThenMediumInMessageFromCache(string cacheKey)
        {
            return this.MessageCache.GetMediumThenInMessage(cacheKey);
        }

        /// <summary>
        /// Get the THEN HIGH input messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>String message</returns>
        public string GetThenHighInMessageFromCache(string cacheKey)
        {
            return this.MessageCache.GetHighThenInMessage(cacheKey);
        }

        /// <summary>
        /// Get the ELSE LOW input messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>String message</returns>
        public string GetElseLowInMessageFromCache(string cacheKey)
        {
            return this.MessageCache.GetLowElseInMessage(cacheKey);
        }

        /// <summary>
        /// Get the ELSE MED input messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>String message</returns>
        public string GetElseMediumInMessageFromCache(string cacheKey)
        {
            return this.MessageCache.GetMediumElseInMessage(cacheKey);
        }

        /// <summary>
        /// Get the ELSE HIGH input messages from the cache
        /// </summary>
        /// <param name="key">Cache Key</param>
        /// <returns>String message</returns>
        public string GetElseHighInMessageFromCache(string cacheKey)
        {
            return this.MessageCache.GetHighElseInMessage(cacheKey);
        }

        /// <summary>
        /// Assign THEN LOW messages from the cache to output
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignThenCacheLowMessages(string cacheKey)
        {
            AddLowMessages(GetThenLowMessagesFromCache(cacheKey));
        }

        /// <summary>
        /// Assign THEN MED messages from the cache to output
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignThenCacheMediumMessages(string cacheKey)
        {
            AddMediumMessages(GetThenMediumMessagesFromCache(cacheKey));
        }

        /// <summary>
        /// Assign THEN HIGH messages from the cache to output
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignThenCacheHighMessages(string cacheKey)
        {
            AddHighMessages(GetThenHighMessagesFromCache(cacheKey));
        }

        /// <summary>
        /// Assign THEN LOW cached data into formatted messages to output. Assumes THEN LOW message contains format {} characters
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignThenLowCacheDataIntoFormattedMessages(string cacheKey)
        {
            string format = GetThenLowInMessageFromCache(cacheKey);

            foreach (var l in this.MessageCache.GetThenLowDataFromCache(cacheKey))
            {
                foreach( var s in l)
                    AddLowMessage(string.Format(format, s.ToArray()));
            }
        }

        /// <summary>
        /// Assign THEN MED cached data into formatted messages to output. Assumes THEN MED message contains format {} characters
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignThenMediumCacheDataIntoFormattedMessages(string cacheKey)
        {
            string format = GetThenMediumInMessageFromCache(cacheKey);

            foreach (var l in this.MessageCache.GetThenMediumDataFromCache(cacheKey))
            {
                foreach (var s in l)
                    AddMediumMessage(string.Format(format, s.ToArray()));
            }
        }

        /// <summary>
        /// Assign THEN HIGH cached data into formatted messages to output. Assumes THEN HIGH message contains format {} characters
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignThenHighCacheDataIntoFormattedMessages(string cacheKey)
        {
            string format = GetThenHighInMessageFromCache(cacheKey);

            foreach (var l in this.MessageCache.GetThenHighDataFromCache(cacheKey))
            {
                foreach (var s in l)
                    AddHighMessage(string.Format(format, s.ToArray()));
            }
        }

        /// <summary>
        /// Assign ELSE LOW, MED and HIGH messages from the cache to output
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignThenCacheMessages(string cacheKey)
        {
            AddLowMessages(this.MessageCache.GetLowThenMessages(cacheKey));
            AddMediumMessages(this.MessageCache.GetMediumThenMessages(cacheKey));
            AddHighMessages(this.MessageCache.GetHighThenMessages(cacheKey));
        }

        /// <summary>
        /// Assign ELSE LOW messages from the cache to output
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignElseCacheLowMessages(string cacheKey)
        {
            AddLowMessages(GetElseLowMessagesFromCache(cacheKey));
        }

        /// <summary>
        /// Assign ELSE MED messages from the cache to output
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignElseCacheMediumMessages(string cacheKey)
        {
            AddMediumMessages(GetElseMediumMessagesFromCache(cacheKey));
        }

        /// <summary>
        /// Assign ELSE HIGH messages from the cache to output
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignElseCacheHighMessages(string cacheKey)
        {
            AddHighMessages(GetElseHighMessagesFromCache(cacheKey));
        }

        /// <summary>
        /// Assign ELSE LOW cached data into formatted messages to output. Assumes ELSE LOW message contains format {} characters
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignElseLowCacheDataIntoFormattedMessages(string cacheKey)
        {
            string format = GetElseLowInMessageFromCache(cacheKey);

            foreach (var l in this.MessageCache.GetElseLowDataFromCache(cacheKey))
            {
                foreach (var s in l)
                    AddLowMessage(string.Format(format, s.ToArray()));
            }
        }

        /// <summary>
        /// Assign ELSE MED cached data into formatted messages to output. Assumes ELSE MED message contains format {} characters
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignElseMediumCacheDataIntoFormattedMessages(string cacheKey)
        {
            string format = GetElseMediumInMessageFromCache(cacheKey);

            foreach (var l in this.MessageCache.GetElseMediumDataFromCache(cacheKey))
            {
                foreach (var s in l)
                    AddMediumMessage(string.Format(format, s.ToArray()));
            }
        }

        /// <summary>
        /// Assign ELSE HIGH cached data into formatted messages to output. Assumes ELSE HIGH message contains format {} characters
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignElseHighCacheDataIntoFormattedMessages(string cacheKey)
        {
            string format = GetElseHighInMessageFromCache(cacheKey);

            foreach (var l in this.MessageCache.GetElseHighDataFromCache(cacheKey))
            {
                foreach (var s in l)
                    AddHighMessage(string.Format(format, s.ToArray()));
            }
        }

        /// <summary>
        /// Assign all ELSE cache messages to output
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignElseCacheMessages(string cacheKey)
        {
            if (_trace) ADATrace.WriteLine("AssignElseCacheMessages[" + cacheKey + "]");

            AddLowMessages(this.MessageCache.GetLowElseMessages(cacheKey));
            AddMediumMessages(this.MessageCache.GetMediumElseMessages(cacheKey));
            AddHighMessages(this.MessageCache.GetHighElseMessages(cacheKey));
        }

        /// <summary>
        /// Assign THEN Score and all THEN cache messages to output
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignScoreAndThenCacheMessages(string cacheKey)
        {
            if (_trace) ADATrace.WriteLine("AssignScoreAndThenCacheMessages[" + cacheKey + "]");

            AssignThenScore();
            AssignThenCacheMessages(cacheKey);
        }

        /// <summary>
        /// Assign ELSE Score and all ELSE cache messages to output
        /// </summary>
        /// <param name="key">Cache Key</param>
        public void AssignScoreAndElseCacheMessages(string cacheKey)
        {
            if (_trace) ADATrace.WriteLine("AssignScoreAndElseCacheMessages[" + cacheKey + "]");

            AssignElseScore();
            AssignElseCacheMessages(cacheKey);
        }
        #endregion

        #region Get Cache Data table

        /// <summary>
        /// Get THEN LOW data from the cache
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        /// <returns>A list containing Lists of Strings holding data</returns>
        public ListOfListOfStringData GetThenLowDataFromCache(string cacheKey)
        {
            return this.MessageCache.GetThenLowDataFromCache(cacheKey);
        }

        /// <summary>
        /// Get THEN MED data from the cache
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        /// <returns>A list containing Lists of Strings holding data</returns>
        public ListOfListOfStringData GetThenMediumDataFromCache(string cacheKey)
        {
            return this.MessageCache.GetThenMediumDataFromCache(cacheKey);
        }

        /// <summary>
        /// Get THEN HIGH data from the cache
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        /// <returns>A list containing Lists of Strings holding data</returns>
        public ListOfListOfStringData GetThenHighDataFromCache(string cacheKey)
        {
            return this.MessageCache.GetThenHighDataFromCache(cacheKey);
        }

        /// <summary>
        /// Get ELSE LOW data from the cache
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        /// <returns>A list containing Lists of Strings holding data</returns>
        public ListOfListOfStringData GetElseLowDataFromCache(string cacheKey)
        {
            return this.MessageCache.GetElseLowDataFromCache(cacheKey);
        }

        /// <summary>
        /// Get ELSE MED data from the cache
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        /// <returns>A list containing Lists of Strings holding data</returns>
        public ListOfListOfStringData GetElseMediumDataFromCache(string cacheKey)
        {
            return this.MessageCache.GetElseMediumDataFromCache(cacheKey);
        }

        /// <summary>
        /// Get ELSE HIGH data from the cache
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        /// <returns>A list containing Lists of Strings holding data</returns>
        public ListOfListOfStringData GetElseHighDataFromCache(string cacheKey)
        {
            return this.MessageCache.GetElseHighDataFromCache(cacheKey);
        }

        #endregion

        #region Assign Cache Data to output 

        /// <summary>
        /// Assign LOW THEN data to the output
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        public void AssignThenCacheLowData(string cacheKey)
        {
            SetLowMessageData(GetThenLowDataFromCache(cacheKey));
        }

        /// <summary>
        /// Assign MED THEN data to the output
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        public void AssignThenCacheMediumData(string cacheKey)
        {
            SetMediumMessageData(GetThenMediumDataFromCache(cacheKey));
        }

        /// <summary>
        /// Assign HIGH THEN data to the output
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        public void AssignThenCacheHighData(string cacheKey)
        {
            SetHighMessageData(GetThenHighDataFromCache(cacheKey));
        }

        /// <summary>
        /// Assign LOW, MED and HIGH THEN data to the output
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        public void AssignThenCacheData(string cacheKey)
        {
            AssignThenCacheLowData(cacheKey);
            AssignThenCacheMediumData(cacheKey);
            AssignThenCacheHighData(cacheKey);
        }

        /// <summary>
        /// Assign LOW ELSE data to the output
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        public void AssignElseCacheLowData(string cacheKey)
        {
            SetLowMessageData(GetElseLowDataFromCache(cacheKey));
        }

        /// <summary>
        /// Assign MED ELSE data to the output
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        public void AssignElseCacheMediumData(string cacheKey)
        {
            SetMediumMessageData(GetElseMediumDataFromCache(cacheKey));
        }

        /// <summary>
        /// Assign HIGH ELSE data to the output
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        public void AssignElseCacheHighData(string cacheKey)
        {
            SetHighMessageData(GetElseHighDataFromCache(cacheKey));
        }

        /// <summary>
        /// Assign LOW, MED and HIGH ELSE data to the output
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        public void AssignElseCacheData(string cacheKey)
        {
            AssignElseCacheLowData(cacheKey);
            AssignElseCacheMediumData(cacheKey);
            AssignElseCacheHighData(cacheKey);
        }

        #endregion

        #region MY Methods - Methods to read variables from Rules Db

        private bool _traceMyString = false;

        private void WriteExceptionError(string error)
        {
            Errors.Add("<<EXCEPTION>>: " + error);

            if (_trace)
            {
                int ind = ADATrace.IndentLevel;
                ADATrace.IndentLevel = 0;

                ADATrace.WriteLine("===========================================================");
                ADATrace.WriteLine("EXCEPTION : " + error);
                ADATrace.WriteLine("===========================================================");

                ADATrace.IndentLevel = ind;
            }

        }

        private void WriteNotFoundWarning(string fnName, string ruleSetName, string ruleName)
        {
            string error = "<<Warning>>: " + fnName + ": [" + ruleSetName + "].[" + ruleName + "] Value accessed but not found";

            Errors.Add(error);

            if (_trace) ADATrace.WriteLine(error);

        }

        /// <summary>
        /// Read a Rules Variable and return as a String. 
        /// </summary>
        /// <param name="varName">Variable Name</param>
        /// <returns>Variable value as a String. "" returned if not found and exception written to trace file</returns>
        public string MyString(string varName)
        {
            RuleVariableData rvd = (from x in RuleVariableData where x.VariableName == varName select x).FirstOrDefault();

            if (rvd == null)
            {
                string error = string.Format("MyString: [{0}]..[{1}] NOT FOUND. Empty STRING assumed", this.RuleSetName, varName);

                WriteExceptionError(error);
                return string.Empty;
            }

            if (_traceMyString) ADATrace.WriteLine(string.Format("MyString: [{0}]..[{1}] = [{2}]", this.RuleSetName, varName, rvd.VariableValue));

            return rvd.VariableValue;
        }

        /// <summary>
        /// Read a Rules Variable and return as an Int. 
        /// </summary>
        /// <param name="varName">Variable Name</param>
        /// <returns>Variable value as a Int. Zero returned if not found or unable to cast and exception written to trace file</returns>
        public int MyInteger(string varName)
        {
            try
            {
                _traceMyString = false;
                int ret = Convert.ToInt32(MyString(varName));
                _traceMyString = _trace;

                if (_trace) ADATrace.WriteLine(string.Format("MyInteger: [{0}]..[{1}] = [{2}]", this.RuleSetName, varName, ret.ToString()));

                return ret;
            }
            catch (InvalidCastException)
            {
                string error = string.Format("MyInteger: [{0}]..[{1}] = [{2}] cannot be converted to a integer. Invalid Cast. Zero assumed", this.RuleSetName, varName, MyString(varName));

                WriteExceptionError(error);
                return 0;
            }
            catch (OverflowException)
            {
                string error = string.Format("MyInteger: [{0}]..[{1}] = [{2}] cannot be converted to a integer. Overflow. Zero assumed", this.RuleSetName, varName, MyString(varName));

                WriteExceptionError(error);
                return 0;
            }
            catch (Exception ex)
            {
                string error = string.Format("MyInteger: [{0}]..[{1}] = [{2}] cannot be converted to a integer. {3}. Zero assumed", this.RuleSetName, varName, MyString(varName), ex.Message);

                WriteExceptionError(error);
                return 0;
            }
        }

        /// <summary>
        /// Read a Rules Variable and return as an Double. 
        /// </summary>
        /// <param name="varName">Variable Name</param>
        /// <returns>Variable value as a Double. Zero returned if not found or unable to cast and exception written to trace file</returns>
        public double MyDouble(string varName)
        {
            try
            {
                _traceMyString = false;
                double ret = Convert.ToDouble(MyString(varName));
                _traceMyString = _trace;

                if (_trace) ADATrace.WriteLine(string.Format("MyDouble: [{0}]..[{1}] = [{2}]", this.RuleSetName, varName, ret.ToString()));

                return ret;
            }
            catch (InvalidCastException)
            {
                string error = string.Format("MyDouble: [{0}]..[{1}] = [{2}] cannot be converted to a double. Invalid Cast. Zero assumed", this.RuleSetName, varName, MyString(varName));

                WriteExceptionError(error);
                return 0;
            }
            catch (OverflowException)
            {
                string error = string.Format("MyDouble: [{0}]..[{1}] = [{2}] cannot be converted to a double. Overflow. Zero assumed", this.RuleSetName, varName, MyString(varName));

                WriteExceptionError(error);
                return 0;
            }
            catch (Exception ex)
            {
                string error = string.Format("MyDouble: [{0}]..[{1}] = [{2}] cannot be converted to a double. {3}. Zero assumed", this.RuleSetName, varName, MyString(varName), ex.Message);

                WriteExceptionError(error);
                return 0;
            }
        }

        /// <summary>
        /// Read a Rules Variable and return as an DateTime. 
        /// </summary>
        /// <param name="varName">Variable Name</param>
        /// <returns>Variable value as a DateTime. NOW returned if not found or unable to cast and exception written to trace file</returns>
        public DateTime MyDateTime(string varName)
        {
            try
            {
                _traceMyString = false;
                DateTime ret = DateTime.Parse(MyString(varName));
                _traceMyString = _trace;

                if (_trace) ADATrace.WriteLine(string.Format("MyDateTime: [{0}]..[{1}] = [{2}]", this.RuleSetName, varName, ret.ToString()));

                return ret;
            }
            catch (InvalidCastException)
            {
                string error = string.Format("MyDateTime: [{0}]..[{1}] = [{2}] cannot be converted to a DateTime. Invalid Cast. NOW assumed", this.RuleSetName, varName, MyString(varName));

                WriteExceptionError(error);
                return DateTime.Now;
            }
            catch (OverflowException)
            {
                string error = string.Format("MyDateTime: [{0}]..[{1}] = [{2}] cannot be converted to a DateTime. Overflow. NOW assumed", this.RuleSetName, varName, MyString(varName));

                WriteExceptionError(error);
                return DateTime.Now;
            }
            catch (Exception ex)
            {
                string error = string.Format("MyDateTime: [{0}]..[{1}] = [{2}] cannot be converted to a DateTime. {3}. NOW assumed", this.RuleSetName, varName, MyString(varName), ex.Message);

                WriteExceptionError(error);
                return DateTime.Now;
            }
        }

        /// <summary>
        /// Read a Rules Variable and return as an Bool. 
        /// </summary>
        /// <param name="varName">Variable Name</param>
        /// <returns>Variable value as a Bool. FALSE returned if not found or unable to cast and exception written to trace file</returns>
        public bool MyBoolean(string varName)
        {
            try
            {
                _traceMyString = false;
                string a = MyString(varName).ToUpper();
                _traceMyString = _trace;
                
                if (_trace) ADATrace.WriteLine(string.Format("MyBoolean: [{0}]..[{1}] = [{2}]", this.RuleSetName, varName, a.ToString()));

                return (a == "TRUE" ? true : false);
            }
            catch (Exception ex)
            {
                string error = string.Format("MyBoolean: [{0}]..[{1}] = [{2}] cannot be converted to a boolean. {3}. FALSE assumed", this.RuleSetName, varName, MyString(varName), ex.Message);

                WriteExceptionError(error);
                return false;
            }
        }

        /// <summary>
        /// Read a Rules Variable and return as an Decimal. 
        /// </summary>
        /// <param name="varName">Variable Name</param>
        /// <returns>Variable value as a Decimal. Zero returned if not found or unable to cast and exception written to trace file</returns>
        public decimal MyDecimal(string varName)
        {
            try
            {
                _traceMyString = false;
                decimal ret = Convert.ToDecimal(MyString(varName));
                _traceMyString = _trace;

                if (_trace) ADATrace.WriteLine(string.Format("MyDecimal: [{0}]..[{1}] = [{2}]", this.RuleSetName, varName, ret.ToString()));

                return ret;
            }
            catch (InvalidCastException)
            {
                string error = string.Format("MyDecimal: [{0}]..[{1}] = [{2}] cannot be converted to a decimal. Invalid Cast. Zero assumed", this.RuleSetName, varName, MyString(varName));

                WriteExceptionError(error);
                return 0;
            }
            catch (OverflowException)
            {
                string error = string.Format("MyDecimal: [{0}]..[{1}] = [{2}] cannot be converted to a decimal. Overflow. Zero assumed", this.RuleSetName, varName, MyString(varName));

                WriteExceptionError(error);
                return 0;
            }
            catch (Exception ex)
            {
                string error = string.Format("MyDecimal: [{0}]..[{1}] = [{2}] cannot be converted to a decimal. {3}. Zero assumed", this.RuleSetName, varName, MyString(varName), ex.Message);

                WriteExceptionError(error);
                return 0;
            }

        }

        /// <summary>
        /// Read a Rules Variable and return as an Char. 
        /// </summary>
        /// <param name="varName">Variable Name</param>
        /// <returns>Variable value as a Char. Space char returned if not found or unable to cast and exception written to trace file</returns>
        public char MyChar(string varName)
        {
            try
            {
                _traceMyString = false;
                char ret = Convert.ToChar(MyString(varName)[0]);
                _traceMyString = _trace;

                if (_trace) ADATrace.WriteLine(string.Format("MyChar: [{0}]..[{1}] = [{2}]", this.RuleSetName, varName, ret.ToString()));

                return ret;
            }
            catch (InvalidCastException)
            {
                string error = string.Format("MyChar: [{0}]..[{1}] = [{2}] cannot be converted to a character. Invalid Cast. Space assumed", this.RuleSetName, varName, MyString(varName));

                WriteExceptionError(error);
                return ' ';
            }
            catch (OverflowException)
            {
                string error = string.Format("MyChar: [{0}]..[{1}] = [{2}] cannot be converted to a character. Overflow. Space assumed", this.RuleSetName, varName, MyString(varName));

                WriteExceptionError(error);
                return ' ';
            }
            catch (Exception ex)
            {
                string error = string.Format("MyChar: [{0}]..[{1}] = [{2}] cannot be converted to a character. {3}. Space assumed", this.RuleSetName, varName, MyString(varName), ex.Message);

                WriteExceptionError(error);
                return ' ';
            }
        }

        #endregion

        #region GET Scores from Rules database

        /// <summary>
        /// Read the THEN SCORE message for the Executing RULE from the Rules Db
        /// </summary>
        /// <returns>The THEN SCORE message (trimmed). If not found "" returned</returns>
        public int GetThenScore()
        {
            if (RuleScoreData == null) return 0;

            RuleScoreData rsd = (from x in RuleScoreData where x.RuleName == ExecutingRuleName && x.RootRuleSetId == this.RuleSetId select x).FirstOrDefault();

            if (rsd == null)
            {
                WriteNotFoundWarning("GetThenScore", this.RuleSetName, ExecutingRuleName);
                return 0;
            }


            if (_trace) ADATrace.WriteLine(string.Format("GetThenScore: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, rsd.ThenScore.ToString()));

            return rsd.ThenScore;
        }

        /// <summary>
        /// Read the ELSE SCORE message for the Executing RULE from the Rules Db
        /// </summary>
        /// <returns>The ELSE SCORE message (trimmed). If not found "" returned</returns>
        public int GetElseScore()
        {
            if (RuleScoreData == null) return 0;

            RuleScoreData rsd = (from x in RuleScoreData where x.RuleName == ExecutingRuleName && x.RootRuleSetId == this.RuleSetId select x).FirstOrDefault();

            if (rsd == null)
            {
                WriteNotFoundWarning("GetElseScore", this.RuleSetName, ExecutingRuleName);
                return 0;
            }


            if (_trace) ADATrace.WriteLine(string.Format("GetElseScore: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, rsd.ElseScore.ToString()));

            return rsd.ElseScore;
        }
        #endregion

        #region GET Error Messages from Rules database

        /// <summary>
        /// Read the ERROR THEN message for the Executing RULE from the Rules Db
        /// </summary>
        /// <returns>The ERROR THEN message (trimmed). If not found "" returned</returns>
        public string GetThenError()
        {
            if (RuleScoreData == null) return string.Empty;

            RuleScoreData rsd = (from x in RuleScoreData where x.RuleName == ExecutingRuleName && x.RootRuleSetId == this.RuleSetId select x).FirstOrDefault();

            if (rsd == null)
            {
                WriteNotFoundWarning("GetThenError", this.RuleSetName, ExecutingRuleName);
                return string.Empty;
            }


            if (_trace) ADATrace.WriteLine(string.Format("GetThenError: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, rsd.ThenError.ToString()));

            return rsd.ThenError.Trim();
        }

        /// <summary>
        /// Read the ERROR ELSE message for the Executing RULE from the Rules Db
        /// </summary>
        /// <returns>The ERROR ELSE message (trimmed). If not found "" returned</returns>
        public string GetElseError()
        {
            if (RuleScoreData == null) return string.Empty;

            RuleScoreData rsd = (from x in RuleScoreData where x.RuleName == ExecutingRuleName && x.RootRuleSetId == this.RuleSetId select x).FirstOrDefault();

            if (rsd == null)
            {
                WriteNotFoundWarning("GetElseError", this.RuleSetName, ExecutingRuleName);
                return string.Empty;
            }


            if (_trace) ADATrace.WriteLine(string.Format("GetElseError: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, rsd.ElseError.ToString()));

            return rsd.ElseError.Trim();
        }
        #endregion

        #region Get Messages for the executing rule from Rules database

        /// <summary>
        /// Read the LOW THEN message for the Executing RULE from the Rules Db
        /// </summary>
        /// <returns>The LOW THEN message (trimmed). If not found "" returned</returns>
        public string GetThenMessageLow()
        {
            if (RuleScoreData == null) return string.Empty;

            RuleScoreData rsd = (from x in RuleScoreData where x.RuleName == ExecutingRuleName && x.RootRuleSetId == this.RuleSetId select x).FirstOrDefault();

            if (rsd == null)
            {
                WriteNotFoundWarning("GetThenMessageLow", this.RuleSetName, ExecutingRuleName);
                return string.Empty;
            }

            if (_trace) ADATrace.WriteLine(string.Format("GetThenMessageLow:    [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, rsd.ThenMessageLow.ToString()));

            return rsd.ThenMessageLow.Trim();
        }

        /// <summary>
        /// Read the LOW ELSE message for the Executing RULE from the Rules Db
        /// </summary>
        /// <returns>The LOW ELSE message (trimmed). If not found "" returned</returns>
        public string GetElseMessageLow()
        {
            if (RuleScoreData == null) return string.Empty;

            RuleScoreData rsd = (from x in RuleScoreData where x.RuleName == ExecutingRuleName && x.RootRuleSetId == this.RuleSetId select x).FirstOrDefault();

            if (rsd == null)
            {
                WriteNotFoundWarning("GetElseMessageLow", this.RuleSetName, ExecutingRuleName);
                return string.Empty;
            }

            if (_trace) ADATrace.WriteLine(string.Format("GetElseMessageLow:    [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, rsd.ElseMessageLow.ToString()));

            return rsd.ElseMessageLow.Trim();
        }

        /// <summary>
        /// Read the MEDIUM THEN message for the Executing RULE from the Rules Db
        /// </summary>
        /// <returns>The MEDIUM THEN message (trimmed). If not found "" returned</returns>
        public string GetThenMessageMedium()
        {
            if (RuleScoreData == null) return string.Empty;

            RuleScoreData rsd = (from x in RuleScoreData where x.RuleName == ExecutingRuleName && x.RootRuleSetId == this.RuleSetId select x).FirstOrDefault();

            if (rsd == null)
            {
                WriteNotFoundWarning("GetThenMessageMedium", this.RuleSetName, ExecutingRuleName);
                return string.Empty;
            }

            if (_trace) ADATrace.WriteLine(string.Format("GetThenMessageMedium: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, rsd.ThenMessageMedium.ToString()));

            return rsd.ThenMessageMedium.Trim();
        }

        /// <summary>
        /// Read the MEDIUM ELSE message for the Executing RULE from the Rules Db
        /// </summary>
        /// <returns>The MEDIUM ELSE message (trimmed). If not found "" returned</returns>
        public string GetElseMessageMedium()
        {
            if (RuleScoreData == null) return string.Empty;

            RuleScoreData rsd = (from x in RuleScoreData where x.RuleName == ExecutingRuleName && x.RootRuleSetId == this.RuleSetId select x).FirstOrDefault();

            if (rsd == null)
            {
                WriteNotFoundWarning("GetElseMessageMedium", this.RuleSetName, ExecutingRuleName);
                return string.Empty;
            }

            if (_trace) ADATrace.WriteLine(string.Format("GetElseMessageMedium: [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, rsd.ElseMessageMedium.ToString()));

            return rsd.ElseMessageMedium.Trim();
        }

        /// <summary>
        /// Read the HIGH THEN message for the Executing RULE from the Rules Db
        /// </summary>
        /// <returns>The HIGH THEN message (trimmed). If not found "" returned</returns>
        public string GetThenMessageHigh()
        {
            if (RuleScoreData == null) return string.Empty;

            RuleScoreData rsd = (from x in RuleScoreData where x.RuleName == ExecutingRuleName && x.RootRuleSetId == this.RuleSetId select x).FirstOrDefault();

            if (rsd == null)
            {
                WriteNotFoundWarning("GetThenMessageHigh", this.RuleSetName, ExecutingRuleName);
                return string.Empty;
            }

            if (_trace) ADATrace.WriteLine(string.Format("GetThenMessageHigh:   [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, rsd.ThenMessageHigh.ToString()));

            return rsd.ThenMessageHigh.Trim();
        }

        /// <summary>
        /// Read the HIGH ELSE message for the Executing RULE from the Rules Db
        /// </summary>
        /// <returns>The HIGH ELSE message (trimmed). If not found "" returned</returns>
        public string GetElseMessageHigh()
        {
            if (RuleScoreData == null) return string.Empty;

            RuleScoreData rsd = (from x in RuleScoreData where x.RuleName == ExecutingRuleName && x.RootRuleSetId == this.RuleSetId select x).FirstOrDefault();

            if (rsd == null)
            {
                WriteNotFoundWarning("GetElseMessageHigh", this.RuleSetName, ExecutingRuleName);
                return string.Empty;
            }

            if (_trace) ADATrace.WriteLine(string.Format("GetElseMessageHigh:   [{0}].[{1}] : [{2}]", this.RuleSetName, ExecutingRuleName, rsd.ElseMessageHigh.ToString()));

            return rsd.ElseMessageHigh.Trim();
        }
        #endregion

        /// <summary>
        /// Populate the INPUT messages held in the cache by reading the message string held in the Rules Db for this rule.
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        public void PopulateMessageCacheInputMessagesFromRulesDb(string cacheKey)
        {
            if (_trace)
            {
                ADATrace.WriteLine("Populating Message cache with input messages from Rules Db");
                ADATrace.IndentLevel += 1;
            }
            this.MessageCache.SetLowThenInMessage(cacheKey, this.GetThenMessageLow());
            this.MessageCache.SetMediumThenInMessage(cacheKey, this.GetThenMessageMedium());
            this.MessageCache.SetHighThenInMessage(cacheKey, this.GetThenMessageHigh());

            this.MessageCache.SetLowElseInMessage(cacheKey, this.GetElseMessageLow());
            this.MessageCache.SetMediumElseInMessage(cacheKey, this.GetElseMessageMedium());
            this.MessageCache.SetHighElseInMessage(cacheKey, this.GetElseMessageHigh());

            if (_trace)
                ADATrace.IndentLevel -= 1;
        }

        /// <summary>
        /// Add the Input Messages stored in the cache to the output messages in the cache. Developer can choose Low, Med, High messages
        /// </summary>
        /// <param name="cacheKey">Cache Key</param>
        /// <param name="popLow">Copy Low?</param>
        /// <param name="popMed">Copy Med?</param>
        /// <param name="popHigh">Copy High?</param>
        public void PopulateMessageCacheOutputMessagesFromInputMessages(string cacheKey, bool popLow = true, bool popMed = true, bool popHigh = true)
        {
            if (_trace)
            {
                ADATrace.WriteLine(string.Format("Populating Message cache output messages with cached input messages L[{0}] M[{1}] H[{2}]", popLow, popMed, popHigh) );
                ADATrace.IndentLevel += 1;
            }
            if (popLow)
            {
                if (_trace) ADATrace.WriteLine(string.Format("Add to Cache Low : Then[{0}], Else[{1}]", this.MessageCache.GetLowThenInMessage(cacheKey), this.MessageCache.GetLowElseInMessage(cacheKey)));
                this.MessageCache.AddLowOutputMessage(cacheKey, this.MessageCache.GetLowThenInMessage(cacheKey), this.MessageCache.GetLowElseInMessage(cacheKey));
            }
            if (popMed)
            {
                if (_trace) ADATrace.WriteLine(string.Format("Add to Cache Med : Then[{0}], Else[{1}]", this.MessageCache.GetMediumThenInMessage(cacheKey), this.MessageCache.GetMediumElseInMessage(cacheKey)));
                this.MessageCache.AddMediumOutputMessage(cacheKey, this.MessageCache.GetMediumThenInMessage(cacheKey), this.MessageCache.GetMediumElseInMessage(cacheKey));
            }
            if (popHigh)
            {
                if (_trace) ADATrace.WriteLine(string.Format("Add to Cache High : Then[{0}], Else[{1}]", this.MessageCache.GetHighThenInMessage(cacheKey), this.MessageCache.GetHighElseInMessage(cacheKey)));
                this.MessageCache.AddHighOutputMessage(cacheKey, this.MessageCache.GetHighThenInMessage(cacheKey), this.MessageCache.GetHighElseInMessage(cacheKey));
            }
            if (_trace)
                ADATrace.IndentLevel -= 1;
        }

        /// <summary>
        /// Custom method to allow developer to exit scoring through an exception
        /// </summary>
        /// <param name="message">Message in th exception</param>
        /// <param name="returnCode">Numeric return code</param>
        public void ThrowNewScoringException(string message, int returnCode)
        {
            if (_trace)
                ADATrace.WriteLine(string.Format("User Exception thrown [{0}] Code[{1}]", message, returnCode.ToString()));

            throw new ScoringException(message, returnCode);
        }

        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        public virtual void AssignUsedProperties(IRiskEntity dest)
        {
        }
    }



}
