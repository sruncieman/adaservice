﻿using System;
using System.Collections.Generic;


namespace RiskEngine.Scoring.Entities
{

    /// <summary>
    /// This class is used to store variables used by the developer whilst scoring.  A dictionary holds a name=object
    /// pair and several access method retrieve and CAST the objects.
    /// </summary>
    public class MyVars
    {
        public Dictionary<string, object> _MyVars { get; set; }

        public MyVars()
        {
            _MyVars = new Dictionary<string, object>();
        }

        /// <summary>
        /// Save a value. It it exists overwrite the value already stored
        /// </summary>
        /// <param name="name">Name of Variable</param>
        /// <param name="value">Value to store</param>
        public void Save(string name, object value)
        {
            if (_MyVars.ContainsKey(name))
                _MyVars[name] = value;
            else
                _MyVars.Add(name, value);
        }

        /// <summary>
        /// Cast and return value as Boolean
        /// </summary>
        /// <param name="name">Name of variable to get</param>
        /// <returns>object stored cast to Bool</returns>
        public bool GetBoolean(string name)
        {
            return (bool)_MyVars[name];
        }

        /// <summary>
        /// Cast and return value as DateTime
        /// </summary>
        /// <param name="name">Name of variable to get</param>
        /// <returns>object stored cast to DateTime</returns>
        public DateTime GetDateTime(string name)
        {
            return (DateTime)_MyVars[name];
        }

        /// <summary>
        /// Cast and return value as String
        /// </summary>
        /// <param name="name">Name of variable to get</param>
        /// <returns>object stored cast to String</returns>
        public string GetString(string name)
        {
            return (string)_MyVars[name];
        }

        /// <summary>
        /// Cast and return value as Int
        /// </summary>
        /// <param name="name">Name of variable to get</param>
        /// <returns>object stored cast to Int</returns>
        public int GetInt(string name)
        {
            return (int)_MyVars[name];
        }

        /// <summary>
        /// return object stored as Object
        /// </summary>
        /// <param name="name">Name of variable to get</param>
        /// <returns>object stored</returns>
        public object GetObject(string name)
        {
            return _MyVars[name];
        }
    }
}
