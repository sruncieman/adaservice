﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using MDA.Common.Debug;

namespace RiskEngine.Scoring.Entities
{
    // PLACEHOLDER : This class in NOT USED

    #region Testing Classes

    public class DrivingLicenseRiskEditExtras
    {
        //public int v_DrivingLicense_Integer1 { get; set; }

    }

    public class DrivingLicenseRiskEdit : DrivingLicenseRisk
    {
        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();

        public DrivingLicenseRiskEdit()
        {
            Extras = new DrivingLicenseRiskEditExtras();
        }

        #region Extras
        public DrivingLicenseRiskEditExtras Extras;

        #endregion

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, int KeyAttCount, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
                ClientScores[orgName].RuleGroupKeyAttractorTotalCount = KeyAttCount;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }


        public DrivingLicenseRiskEdit CloneEntity()
        {
            DrivingLicenseRiskEdit c = new DrivingLicenseRiskEdit();

            c.v_DriverNumber = v_DriverNumber;

            //c.v_DrivingLicense_Integer1 = v_DrivingLicense_Integer1;


            return c;
        }
    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class DrivingLicenseRisk : RiskEntityBase, IRiskEntity
    {
        [DataMember]
        public int Db_Id { get; set; }

        [DataMember]
        public string v_DriverNumber { get; set; }

        public DrivingLicenseRisk()
        {
        }

        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        //public override void AssignUsedProperties(IRiskEntity dest)
        //{
        //}

        protected DrivingLicenseRisk ShallowCopyThis(DrivingLicenseRisk c)
        {
            c.db = db;

            c.Db_Id = Db_Id;
            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;

            c.v_DriverNumber = v_DriverNumber;

            return c;
        }

        public override object Clone()
        {
            if (_trace)
            {
                ADATrace.WriteLine("Cloning License [" + Db_Id.ToString() + "]");
            }

            DrivingLicenseRisk c = new DrivingLicenseRisk();

            ShallowCopyThis(c);

            if (_trace)
            {
                ADATrace.WriteLine("Cloning License Complete");
            }

            return c;
        }


        public override string DisplayText
        {
            get { return "License: " + v_DriverNumber; } //  + "  [" + this.RuleGroupRiskTotalScore.ToString() + "]"; }
            //set;
        }

        [DataMember]
        public override string ReportHeading1
        {
            get { return "License: " + v_DriverNumber; } //  + "  [" + this.RuleGroupRiskTotalScore.ToString() + "]"; }
            //set;
        }

        [DataMember]
        public override string ReportHeading2
        {
            get { return "License: " + v_DriverNumber; } //  + "  [" + this.RuleGroupRiskTotalScore.ToString() + "]"; }
            //set;
        }
    }

}