﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using MDA.Common.Debug;

namespace RiskEngine.Scoring.Entities
{
    // PLACEHOLDER : This class in NOT USED

    #region Testing Classes
    public class PaymentRiskEditExtras
    {
        //public int v_PaymentCard_Integer1 { get; set; }

    }

    public class PaymentCardRiskEdit : PaymentCardRisk
    {
        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();


        public PaymentCardRiskEdit()
        {
            Extras = new PaymentRiskEditExtras();

            v_PaymentCardType = PaymentCardType.Unknown;
            v_PaymentCardType_Text = v_PaymentCardType.ToString();
        }

        #region Extras
        public PaymentRiskEditExtras Extras;

        //public override int v_PaymentCard_Integer1
        //{
        //    get { return Extras.v_PaymentCard_Integer1; }
        //    set { Extras.v_PaymentCard_Integer1 = value; }
        //}


        #endregion

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, int KeyAttCount, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
                ClientScores[orgName].RuleGroupKeyAttractorTotalCount = KeyAttCount;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }


        public PaymentCardRiskEdit CloneEntity()
        {
            PaymentCardRiskEdit c = new PaymentCardRiskEdit();

            c.v_PaymentCardNumber       = v_PaymentCardNumber;
            c.v_SortCode                = v_SortCode;
            c.v_BankName                = v_BankName;
            c.v_ExpiryDate              = v_ExpiryDate;
            c.v_DatePaymentDetailsTaken = v_DatePaymentDetailsTaken;
            c.v_PaymentCardType         = v_PaymentCardType;
            c.v_PaymentCardType_Text    = v_PaymentCardType_Text;
            //c.v_PaymentCard_Integer1  = v_PaymentCard_Integer1;

            return c;
        }
    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class PaymentCardRisk : RiskEntityBase, IRiskEntity, ICloneable
    {
        [DataMember(Name = "PCard_DbId")]
        public int Db_Id { get; set; }

        [DataMember(Name = "v_PCN")]
        public string v_PaymentCardNumber { get; set; }
        [DataMember(Name = "v_SC")]
        public string v_SortCode { get; set; }
        [DataMember(Name = "v_BN")]
        public string v_BankName { get; set; }
        [DataMember(Name = "v_ED")]
        public string v_ExpiryDate { get; set; }
        [DataMember(Name = "v_DPDT")]
        public DateTime? v_DatePaymentDetailsTaken { get; set; }

        [DataMember(Name = "v_PCT")]
        public PaymentCardType v_PaymentCardType { get; set; }
        [DataMember(Name = "v_PCTt")]
        public string v_PaymentCardType_Text { get; set; }

        public PaymentCardRisk()
        {
        }

        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        //public override void AssignUsedProperties(IRiskEntity dest)
        //{
        //}

        protected PaymentCardRisk ShallowCopyThis(PaymentCardRisk c)
        {
            c.db = db;

            c.Db_Id = Db_Id;
            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;

            c.v_PaymentCardNumber = v_PaymentCardNumber;
            c.v_SortCode = v_SortCode;
            c.v_BankName = v_BankName;
            c.v_ExpiryDate = v_ExpiryDate;
            c.v_DatePaymentDetailsTaken = v_DatePaymentDetailsTaken;
            c.v_PaymentCardType = v_PaymentCardType;
            c.v_PaymentCardType_Text = v_PaymentCardType_Text;

            return c;
        }

        public override object Clone()
        {
            if (_trace)
            {
                ADATrace.WriteLine("Cloning Payment Card [" + Db_Id.ToString() + "]");
            }
            PaymentCardRisk c = new PaymentCardRisk();

            ShallowCopyThis(c);

            if (_trace)
            {
                ADATrace.WriteLine("Cloning Payment Card Complete");
            }

            return c;
        }


        public override string DisplayText
        {
            get { return "Card: " + v_PaymentCardNumber; } 
            //set;
        }

        [DataMember]
        public override string ReportHeading1
        {
            get { return "Card: " + v_PaymentCardNumber; } 
            //set;
        }

        [DataMember]
        public override string ReportHeading2
        {
            get { return "Card: " + v_PaymentCardNumber; } 
            //set;
        }
    }

}