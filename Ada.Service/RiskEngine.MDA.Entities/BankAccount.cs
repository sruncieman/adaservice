﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using MDA.Common.Debug;

namespace RiskEngine.Scoring.Entities
{
    // PLACEHOLDER : This class in NOT USED

    #region Testing Classes

    public class BankAccountRiskEditExtras
    {
    }

    public class BankAccountRiskEdit : BankAccountRisk
    {
        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();

        public BankAccountRiskEdit()
        {
            //v_PolicyPaymentType = PolicyPaymentType.Unknown;

            Extras = new BankAccountRiskEditExtras();
        }

        #region Extras
        public BankAccountRiskEditExtras Extras;

        //public override int v_BankAccount_Integer1
        //{
        //    get { return Extras.v_BankAccount_Integer1; }
        //    set { Extras.v_BankAccount_Integer1 = value; }
        //}


        #endregion

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }


        public BankAccountRiskEdit CloneEntity()
        {
            BankAccountRiskEdit c = new BankAccountRiskEdit();

            c.v_AccountNumber = v_AccountNumber;
            c.v_SortCode      = v_SortCode;
            c.v_BankName      = v_BankName;

            c.v_IsKeyAttractor = v_IsKeyAttractor;
            c.v_KeyAttractor = v_KeyAttractor;

            //c.v_BankAccount_Integer1 = v_BankAccount_Integer1;


            return c;
        }
    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class BankAccountRisk : RiskEntityBase, IRiskEntity
    {
        [DataMember(Name = "Bank_DbId")]
        public int Db_Id { get; set; }

        [DataMember(Name = "v_AN")]
        public string v_AccountNumber { get; set; }
        [DataMember(Name = "v_SC")]
        public string v_SortCode { get; set; }
        [DataMember(Name = "v_BN")]
        public string v_BankName { get; set; }

        [DataMember(Name = "v_KA")]
        public string v_KeyAttractor { get; set; }
        [DataMember(Name = "v_IsKA")]
        public bool v_IsKeyAttractor { get; set; }

        //public virtual int v_BankAccount_Integer1 { get; set; }


        public BankAccountRisk()
        {
        }

        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        //public override void AssignUsedProperties(IRiskEntity dest)
        //{
        //}

        protected BankAccountRisk ShallowCopyThis(BankAccountRisk c)
        {
            c.db = db;

            c.Db_Id = Db_Id;
            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;

            c.v_AccountNumber = v_AccountNumber;
            c.v_SortCode = v_SortCode;
            c.v_BankName = v_BankName;

            c.v_IsKeyAttractor = v_IsKeyAttractor;
            c.v_KeyAttractor = v_KeyAttractor;

            //c.v_BankAccount_Integer1 = v_BankAccount_Integer1;

            return c;
        }

        public override object Clone()
        {
            if (_trace)
            {
                ADATrace.WriteLine("Cloning Bank Account [" + Db_Id.ToString() + "]");
            }

            BankAccountRisk c = new BankAccountRisk();

            ShallowCopyThis(c);

            if (_trace)
            {
                ADATrace.WriteLine("Cloning Bank Account Complete");
            }

            return c;
        }


        public override string DisplayText
        {
            get { return "Account: " + v_AccountNumber; } 
            //set;
        }

        [DataMember]
        public override string ReportHeading1
        {
            get { return "Account: " + v_AccountNumber; } 
            //set;
        }

        [DataMember]
        public override string ReportHeading2
        {
            get { return "Account: " + v_AccountNumber; }
            //set;
        }
    }
}