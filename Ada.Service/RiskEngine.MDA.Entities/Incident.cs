﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using MDA.Common.Debug;

namespace RiskEngine.Scoring.Entities
{
    #region Testing Classes
    public class IncidentRiskEditExtras
    {
        public int? v_NumberOfClaimants { get; set; }
        public int? v_NumberOfPolicies { get; set; }
        public int? v_NumberOfDaysSincePolicyInception { get; set; }
        public int? v_NumberOfDaysSincePolicyEnd { get; set; }
        public int v_AgeRangeCount { get; set; }
        public bool? v_PoliceAttended { get; set; }
        public bool? v_AmbulanceAttended { get; set; }
        public int v_NumberOfPassengers { get; set; }
        public bool v_AnyVehiclesWithMultipleOccupancy { get; set; }
        public bool v_MultiplePoliciesFromSameAddress { get; set; }
        public int v_VehiclesOccupancyCount { get; set; }
        public string v_ClaimCode { get; set; }
        public string v_IncidentLocation { get; set; }
        public string v_IncidentCircumstances { get; set; }
    }

    public class IncidentRiskEdit : MotorClaimIncidentRisk
    {
        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();


        public IncidentRiskEdit()
        {
            Vehicles = new ObservableCollection<VehicleRiskEdit>();
            Organisations = new ObservableCollection<OrganisationRiskEdit>();
            Handsets = new ObservableCollection<HandsetRiskEdit>();

            Policy = new PolicyRiskEdit();
            Extras = new IncidentRiskEditExtras();

            v_IncidentType = (int)IncidentType.Unknown;
            v_IncidentType_Text = "Unknown";
        }

        #region Extras
        public IncidentRiskEditExtras Extras;


        public override string v_ClaimCode
        {
            get { return Extras.v_ClaimCode; }
            set { Extras.v_ClaimCode = value; }
        }
        public override string v_IncidentLocation
        {
            get { return Extras.v_IncidentLocation; }
            set { Extras.v_IncidentLocation = value; }
        }
        public override string v_IncidentCircumstances
        {
            get { return Extras.v_IncidentCircumstances; }
            set { Extras.v_IncidentCircumstances = value; }
        }

        public override bool? v_PoliceAttended
        {
            get { return Extras.v_PoliceAttended; }
            set { Extras.v_PoliceAttended = value; }
        }

        public override bool? v_AmbulanceAttended
        {
            get { return Extras.v_AmbulanceAttended; }
            set { Extras.v_AmbulanceAttended = value; }
        }

        public override int? v_NumberOfClaimants
        {
            get { return Extras.v_NumberOfClaimants; }
            set { Extras.v_NumberOfClaimants = value; }
        }

        public override bool v_AnyVehiclesWithMultipleOccupancy(string cacheKey, int limit)
        {
              //get { return Extras.v_AnyVehiclesWithMultipleOccupancy; }
              //set { Extras.v_AnyVehiclesWithMultipleOccupancy = value; }
            return Extras.v_AnyVehiclesWithMultipleOccupancy;
        }

        public override bool v_MultiplePoliciesFromSameAddress(string cacheKey, int limit)
        {

            return Extras.v_MultiplePoliciesFromSameAddress;
        }

        //public override int v_NumberOfPassengers
        //{
        //    get { return Extras.v_NumberOfPassengers; }
        //    set { Extras.v_NumberOfPassengers = value; }
        //}

        public override int? v_NumberOfDaysSincePolicyInception
        {
            get { return Extras.v_NumberOfDaysSincePolicyInception; }
            set { Extras.v_NumberOfDaysSincePolicyInception = value; }
        }

        public override int? v_NumberOfDaysSincePolicyEnd
        {
            get { return Extras.v_NumberOfDaysSincePolicyEnd; }
            set { Extras.v_NumberOfDaysSincePolicyEnd = value; }
        }

        public override int v_AgeRangeCount(string cacheKey, int ageFrom, int ageTo)
        {
            //get { return Extras.v_AgeRangeCount; }
            //set { Extras.v_AgeRangeCount = value; }
            return Extras.v_AgeRangeCount;
        }
        
        
        #endregion

        #region Children
        public PolicyRiskEdit Policy { get; set; }
        public ObservableCollection<VehicleRiskEdit> Vehicles { get; set; }
        public ObservableCollection<OrganisationRiskEdit> Organisations { get; set; }
        public ObservableCollection<HandsetRiskEdit> Handsets { get; set; }
        #endregion

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, int KeyAttCount, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
                ClientScores[orgName].RuleGroupKeyAttractorTotalCount = KeyAttCount;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }

        public List<ClientScore_TestingOnly> PolicyExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in Policy.ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }

        public new string DisplayText
        {
            get { return v_ClaimNumber; } // + "  [" + this.RuleGroupRiskTotalScore.ToString() + "]"; }
        }

        public new string DisplayTextPolicy
        {
            get { return Policy.DisplayText; }
        }

        public IncidentRiskEdit CloneEntity(bool cloneChildren)
        {
            IncidentRiskEdit c = new IncidentRiskEdit();

            c.v_IncidentType   = v_IncidentType;
            c.v_IncidentDateTime   = v_IncidentDateTime;
            //c.v_IncidentTime = v_IncidentTime;
            c.v_ClaimNumber    = v_ClaimNumber;
            c.v_PaymentsToDate = v_PaymentsToDate;
            c.v_Reserve        = v_Reserve;
            c.v_PolicyStartDate = v_PolicyStartDate;
            c.v_PolicyEndDate   = v_PolicyEndDate;

            c.Policy = this.Policy.CloneEntity(true);

            c.v_NumberOfClaimants                = v_NumberOfClaimants;
            c.v_NumberOfPolicies                 = v_NumberOfPolicies;
            //c.v_AnyVehiclesWithMultipleOccupancy = v_AnyVehiclesWithMultipleOccupancy;
            //c.v_NumberOfPassengers         = v_NumberOfPassengers;
            c.v_AmbulanceAttended                = v_AmbulanceAttended;
            c.v_PoliceAttended                   = v_PoliceAttended;
            c.v_NumberOfDaysSincePolicyInception = v_NumberOfDaysSincePolicyInception;
            c.v_NumberOfDaysSincePolicyEnd       = v_NumberOfDaysSincePolicyEnd;
            //c.v_AgeRangeCount                    = v_AgeRangeCount;

            c.v_ClaimCode = v_ClaimCode;
            c.v_IncidentLocation = v_IncidentLocation;
            c.v_IncidentCircumstances = v_IncidentCircumstances;

            if (cloneChildren)
            {
                foreach (var v in this.Vehicles)
                    c.Vehicles.Add(v.CloneEntity(true));

                foreach (var o in this.Organisations)
                    c.Organisations.Add(o.CloneEntity(true));
            }

            return c;
        }
    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class IncidentRisk : RiskEntityBase, IRiskEntity
    {

        public IncidentRisk()
        {
            IncidentPolicy = new PolicyRisk();
        }

        [DataMember(Name = "Inc_DbId")]
        public int Db_Id { get; set; }

        [DataMember(Name = "v_IT")]
        public int v_IncidentType { get; set; }
        [DataMember(Name = "v_ITt")]
        public string v_IncidentType_Text { get; set; }

        [DataMember(Name = "v_IDT")]
        public DateTime? v_IncidentDateTime { get; set; }

        [DataMember(Name = "v_CN")]
        public string v_ClaimNumber { get; set; }

        [DataMember(Name = "v_PTt")]
        public decimal? v_PaymentsToDate { get; set; }

        [DataMember(Name = "v_PSD")]
        public DateTime? v_PolicyStartDate { get; set; }

        [DataMember(Name = "v_PED")]
        public DateTime? v_PolicyEndDate { get; set; }

        [DataMember(Name = "v_Res")]
        public decimal? v_Reserve { get; set; }

        [DataMember(Name = "v_CCode")]
        public virtual string v_ClaimCode { get; set; }
        [DataMember(Name = "v_Loc")]
        public virtual string v_IncidentLocation { get; set; }
        [DataMember(Name = "v_Circ")]
        public virtual string v_IncidentCircumstances { get; set; }

        [DataMember(Name = "Policy")]
        [ScoreableItem( IsCollection=false, RulePath="Policy")]
        public PolicyRisk IncidentPolicy { get; set; }



        private int? _v_NumberOfClaimants;

        [DataMember(Name = "v_NoC")]
        public virtual int? v_NumberOfClaimants 
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_NumberOfClaimants == null) ? "NA" : _v_NumberOfClaimants.ToString();

                        ADATrace.WriteLine("Person.v_NumberOfClaimants. Serializing [" + x.ToString() + "]");
                    }

                    return _v_NumberOfClaimants;
                }

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfClaimants");

                if (_v_NumberOfClaimants != null)
                    return (int)_v_NumberOfClaimants;

                //using (MDADAL.MdaDbContext db = new MDADAL.MdaDbContext())
                //{
                    _v_NumberOfClaimants = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Incident_NumberOfClaimants( this.Db_Id, this.RiskClaimId);
                //}

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfClaimants = " + _v_NumberOfClaimants);

                return (int)_v_NumberOfClaimants;
            }

            set 
            {
                _v_NumberOfClaimants = value;
            } 
        }

        private int? _v_NumberOfPolicies;

        [DataMember(Name = "v_NPs")]
        public virtual int? v_NumberOfPolicies
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_NumberOfPolicies == null) ? "NA" : _v_NumberOfPolicies.ToString();

                        ADATrace.WriteLine("Incident.v_NumberOfPolicies. Serializing [" + x.ToString() + "]");
                    }

                    return _v_NumberOfPolicies;
                }

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfPolicies");

                if (_v_NumberOfPolicies != null)
                    return (int)_v_NumberOfPolicies;

                _v_NumberOfPolicies = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Incident_NumberOfPolicies(this.Db_Id);

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfPolicies = " + _v_NumberOfPolicies);

                return (int)_v_NumberOfPolicies;
            }

            set
            {
                _v_NumberOfPolicies = value;
            }
        }

        //v_VehiclesOccupancyCount

        private int? _v_VehiclesOccupancyCount;

        [DataMember(Name = "v_VOC")]
        public virtual int? v_VehiclesOccupancyCount
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_VehiclesOccupancyCount == null) ? "NA" : _v_VehiclesOccupancyCount.ToString();

                        ADATrace.WriteLine("Person.v_VehiclesOccupancyCount. Serializing [" + x.ToString() + "]");
                    }

                    return _v_VehiclesOccupancyCount;
                }

                if (_trace) ADATrace.WriteLine("Incident.v_VehiclesOccupancyCount");

                if (_v_VehiclesOccupancyCount != null)
                    return (int)_v_VehiclesOccupancyCount;

                _v_VehiclesOccupancyCount = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Incident_VehiclesOccupancyCount(this.Db_Id, this.RiskClaimId);

                if (_trace) ADATrace.WriteLine("Incident.v_VehiclesOccupancyCount = " + _v_VehiclesOccupancyCount);

                return (int)_v_VehiclesOccupancyCount;
            }

            set
            {
                _v_VehiclesOccupancyCount = value;
            }
        }

        private int? _v_NumberOfPassengers;

        [DataMember(Name = "v_NoP")]
        public virtual int? v_NumberOfPassengers 
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_NumberOfPassengers == null) ? "NA" : _v_NumberOfPassengers.ToString();

                        ADATrace.WriteLine("Person.v_NumberOfPassengers. Serializing [" + x.ToString() + "]");
                    }

                    return _v_NumberOfPassengers;
                }

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfPassengers");

                if (_v_NumberOfPassengers != null)
                    return (int)_v_NumberOfPassengers;

                _v_NumberOfPassengers = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Incident_VehiclesPassengerCount(this.Db_Id, this.RiskClaimId);

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfPassengers = " + _v_NumberOfPassengers);

                return (int)_v_NumberOfPassengers;
            }

            set
            {
                _v_NumberOfPassengers = value;
            }
        }

        [DataMember(Name = "v_PA")]
        public virtual bool? v_PoliceAttended  { get; set; }

        [DataMember(Name = "v_AA")]
        public virtual bool? v_AmbulanceAttended  { get; set; }

        private int? _v_NumberOfDaysSincePolicyInception;

        [DataMember(Name = "v_NoDSPI")]
        public virtual int? v_NumberOfDaysSincePolicyInception 
        { 
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_NumberOfDaysSincePolicyInception == null) ? "NA" : _v_NumberOfDaysSincePolicyInception.ToString();

                        ADATrace.WriteLine("Person.v_NumberOfDaysSincePolicyInception. Serializing [" + x.ToString() + "]");
                    }

                    return _v_NumberOfDaysSincePolicyInception;
                }

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfDaysSincePolicyInception");

                if (_v_NumberOfDaysSincePolicyInception != null)
                    return (int)_v_NumberOfDaysSincePolicyInception;

                if (v_IncidentDateTime != null && v_PolicyStartDate != null)
                {
                    DateTime dtOne = (DateTime)v_IncidentDateTime;
                    DateTime dtTwo = v_PolicyStartDate.Value;
                               
                    _v_NumberOfDaysSincePolicyInception = (dtOne - dtTwo).Days + 1;


                    if (_trace) ADATrace.WriteLine("Incident.v_NumberOfDaysSincePolicyInception = " + _v_NumberOfDaysSincePolicyInception);

                    return (int)_v_NumberOfDaysSincePolicyInception;
                }

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfDaysSincePolicyInception = 999");

                return 999;
            }
            set
            {
                _v_NumberOfDaysSincePolicyInception = value;
            }
        }

        private int? _v_NumberOfDaysSincePolicyEnd;

        [DataMember(Name = "v_NoDSPE")]
        public virtual int? v_NumberOfDaysSincePolicyEnd
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_NumberOfDaysSincePolicyEnd == null) ? "NA" : _v_NumberOfDaysSincePolicyEnd.ToString();

                        ADATrace.WriteLine("Person.v_NumberOfDaysSincePolicyEnd. Serializing [" + x.ToString() + "]");
                    }

                    return _v_NumberOfDaysSincePolicyEnd;
                }

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfDaysSincePolicyEnd");

                if (_v_NumberOfDaysSincePolicyEnd != null)
                    return (int)_v_NumberOfDaysSincePolicyEnd;

                if (v_IncidentDateTime != null && v_PolicyEndDate != null)
                {
                    DateTime dtOne = (DateTime)v_IncidentDateTime;
                    DateTime dtTwo = v_PolicyEndDate.Value;

                    _v_NumberOfDaysSincePolicyEnd = (dtTwo - dtOne).Days;

                    if (_trace) ADATrace.WriteLine("Incident.v_NumberOfDaysSincePolicyEnd = " + _v_NumberOfDaysSincePolicyEnd);

                    return (int)_v_NumberOfDaysSincePolicyEnd;
                }

                if (_trace) ADATrace.WriteLine("Incident.v_NumberOfDaysSincePolicyEnd = 999");

                return 999;
            }
            set
            {
                _v_NumberOfDaysSincePolicyEnd = value;
            }
        }

        public virtual bool v_AnyVehiclesWithMultipleOccupancy(string cacheKey, int limit)
        {
            string key = string.Format("AnyVehWithMO: {0}, {1}", this.Db_Id.ToString(), limit.ToString());
            bool result = false;

            if (_trace) ADATrace.WriteLine("Incident." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                result = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Incident_AnyVehiclesWithMultipleOccupancy(this.Db_Id, this.RiskClaimId, limit, cacheKey, this.MessageCache, _trace);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey);

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Incident." + key + " = " + result.ToString());

            return result;
        }

        public virtual bool v_MultiplePoliciesFromSameAddress(string cacheKey, int limit)
        {
            string key = string.Format("MultiPolFSA: {0}, {1}", this.Db_Id.ToString(), limit.ToString());
            bool result = false;

            if (_trace) ADATrace.WriteLine("Incident." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                result = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Incident_MultiplePoliciesFromSameAddress(this.Db_Id, limit, cacheKey, this.MessageCache, _trace);
                
                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey);

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Incident." + key + " = " + result.ToString());

            return result;
        }

        public virtual int v_AgeRangeCount(string cacheKey, int ageFrom, int ageTo)
        {
            string key = string.Format("AgeRC: {0}, P1:{1}, P2:{2}", this.Db_Id.ToString(), ageFrom.ToString(), ageTo.ToString());
            int result = 0;

            if (_trace) ADATrace.WriteLine("Incident." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                result = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Incident_AgeRangeCount(this.Db_Id, this.RiskClaimId, ageFrom, ageTo, cacheKey, this.MessageCache, _trace);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey);

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Incident." + key + " = " + result.ToString());

            return result;
        }

        


        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        public override void AssignUsedProperties(IRiskEntity dest)
        {

            IncidentRisk d = dest as IncidentRisk;

            //if (_v_IncidentDateTime != null)
            //    d.v_IncidentDateTime = v_IncidentDateTime;

            if (_v_NumberOfClaimants != null)
                d.v_NumberOfClaimants = v_NumberOfClaimants;

            if (_v_NumberOfDaysSincePolicyInception != null)
                d.v_NumberOfDaysSincePolicyInception = v_NumberOfDaysSincePolicyInception;

            if (_v_NumberOfDaysSincePolicyEnd != null)
                d.v_NumberOfDaysSincePolicyEnd = v_NumberOfDaysSincePolicyEnd;

            if (_v_NumberOfPolicies != null)
                d._v_NumberOfPolicies = v_NumberOfPolicies;
        }

        protected IncidentRisk ShallowCopyThis(IncidentRisk c)
        {
            c.db = db;

            c.Db_Id = Db_Id;
            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;

            c.v_IncidentType = v_IncidentType;
            c.v_IncidentType_Text = v_IncidentType_Text;

            c.v_IncidentDateTime = v_IncidentDateTime;
            //c.v_IncidentTime = v_IncidentTime;

            c.v_ClaimNumber = v_ClaimNumber;
            c.v_PaymentsToDate = v_PaymentsToDate;
            c.v_PolicyStartDate = v_PolicyStartDate;
            c.v_PolicyEndDate = v_PolicyEndDate;
            c.v_Reserve = v_Reserve;

            c.v_NumberOfPassengers = v_NumberOfPassengers;
            c.v_PoliceAttended = v_PoliceAttended;
            c.v_AmbulanceAttended = v_AmbulanceAttended;

            c.v_ClaimCode = v_ClaimCode;
            c.v_IncidentLocation = v_IncidentLocation;
            c.v_IncidentCircumstances = v_IncidentCircumstances;

            return c;
        }

        public override object Clone()
        {
            if (_trace)
                ADATrace.WriteLine("Cloning Incident [" + Db_Id.ToString() + "]");

            IncidentRisk c = new IncidentRisk();

            ShallowCopyThis(c);

            if (_trace)
                ADATrace.WriteLine("Cloning Incident Complete");

            return c;
        }


        public override string DisplayText
        {
            get { return "Incident: " + v_ClaimNumber; } // + "  [" + this.RuleGroupRiskTotalScore.ToString() + "]"; }
            //set { DisplayText = value; }
        }

        public string DisplayTextPolicy
        {
            get { return ((IncidentPolicy == null) ? "None" : IncidentPolicy.DisplayText); }
            //set
        }

        [DataMember]
        public override string ReportHeading1
        {
            get { return "Incident: " + v_ClaimNumber; } // + "  [" + this.RuleGroupRiskTotalScore.ToString() + "]"; }
            //set
        }

        [DataMember]
        public override string ReportHeading2
        {
            get { return "Incident: " + v_ClaimNumber; } // + "  [" + this.RuleGroupRiskTotalScore.ToString() + "]"; }
            //set
        }
    }

    [DataContract(Namespace = XmlScores.Namespace, Name = "IncidentRisk")]   // Force the NAME to be the name if the inherited item so XML stays the same
    public class MotorClaimIncidentRisk : IncidentRisk
    {
        [DataMember(Name = "Orgs")]
        [ScoreableItem(IsCollection = true, RulePath = "Organisations")]
        public List<OrganisationRisk> IncidentOrganisations { get; set; }


        [DataMember(Name = "Vehicles")]
        [ScoreableItem(IsCollection = true, RulePath = "Vehicles")]
        public List<MotorClaimVehicleRisk> IncidentVehicles { get; set; }

        protected MotorClaimIncidentRisk ShallowCopyThis(MotorClaimIncidentRisk c)
        {
            base.ShallowCopyThis(c);

            // Add any fields in MotorClaimIncidentRisk that need cloning that are additional to the ones in IncidentRisk
            // At the moment there are none. We don't clone children

            return c;
        }

        public override object Clone()
        {
            MotorClaimIncidentRisk c = new MotorClaimIncidentRisk();

            return ShallowCopyThis(c);
        }
    }

    [DataContract(Namespace = XmlScores.Namespace, Name = "PIClaimIncidentRisk")]   // Force the NAME to be the name if the inherited item so XML stays the same
    public class PIClaimIncidentRisk : IncidentRisk
    {
        [DataMember(Name = "Orgs")]
        [ScoreableItem(IsCollection = true, RulePath = "Organisations")]
        public List<OrganisationRisk> IncidentOrganisations { get; set; }


        [DataMember(Name = "Person")]
        [ScoreableItem(IsCollection = false, RulePath = "Person")]
        public PersonRisk Person { get; set; }

        protected PIClaimIncidentRisk ShallowCopyThis(PIClaimIncidentRisk c)
        {
            base.ShallowCopyThis(c);

            // Add any fields in PIIncidentRisk that need cloning that are additional to the ones in IncidentRisk
            // At the moment there are none. We don't clone children

            return c;
        }

        public override object Clone()
        {
            PIClaimIncidentRisk c = new PIClaimIncidentRisk();

            return ShallowCopyThis(c);
        }
    }

    [DataContract(Namespace = XmlScores.Namespace, Name = "MobileCliamIncidentRisk")]   // Force the NAME to be the name if the inherited item so XML stays the same
    public class MobileClaimIncidentRisk : IncidentRisk
    {
        [DataMember(Name = "Orgs")]
        [ScoreableItem(IsCollection = true, RulePath = "Organisations")]
        public List<OrganisationRisk> IncidentOrganisations { get; set; }


        [DataMember(Name = "Handsets")]
        [ScoreableItem(IsCollection = true, RulePath = "Handsets")]
        public List<MobileClaimHandsetRisk> IncidentHandsets { get; set; }

        protected MotorClaimIncidentRisk ShallowCopyThis(MotorClaimIncidentRisk c)
        {
            base.ShallowCopyThis(c);

            // Add any fields in MotorClaimIncidentRisk that need cloning that are additional to the ones in IncidentRisk
            // At the moment there are none. We don't clone children

            return c;
        }

        public override object Clone()
        {
            MotorClaimIncidentRisk c = new MotorClaimIncidentRisk();

            return ShallowCopyThis(c);
        }
    }
}
