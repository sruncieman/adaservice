﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using MDA.Common.Debug;

namespace RiskEngine.Scoring.Entities
{
    #region Testing Classes

    public class PolicyRiskEditExtras
    {
        public DateTime v_IncidentDate { get; set; }

    }

    public class PolicyRiskEdit : PolicyRisk
    {
        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();


        public PolicyRiskEdit()
        {
            Extras = new PolicyRiskEditExtras();

            v_PolicyType = (int)PolicyType.PersonalMotor;
            v_PolicyType_Text = v_PolicyType.ToString();

            v_CoverType = (int)PolicyCoverType.Unknown;
            v_CoverType_Text = v_CoverType.ToString();
        }

        #region Extras
        public PolicyRiskEditExtras Extras = new PolicyRiskEditExtras();



        public override DateTime v_IncidentDate
        {
            get { return Extras.v_IncidentDate; }
            set { Extras.v_IncidentDate = value; }
        }


        #endregion

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, int KeyAttCount, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
                ClientScores[orgName].RuleGroupKeyAttractorTotalCount = KeyAttCount;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }


        public PolicyRiskEdit CloneEntity(bool cloneChildren)
        {
            PolicyRiskEdit c = new PolicyRiskEdit();

            c.v_PolicyType_Text            = v_PolicyType_Text;
            c.v_PolicyType                 = v_PolicyType;
            c.v_PolicyNumber               = v_PolicyNumber;
            c.v_TradingName                = v_TradingName;
            c.v_Broker                     = v_Broker;
            c.v_CoverType_Text             = v_CoverType_Text;
            c.v_CoverType                  = v_CoverType;
            c.v_PolicyStartDate            = v_PolicyStartDate;
            c.v_PolicyEndDate              = v_PolicyEndDate;
            c.v_PreviousNoFaultClaimsCount = v_PreviousNoFaultClaimsCount;
            c.v_PreviousFaultClaimsCount   = v_PreviousFaultClaimsCount;
            c.v_Premium                    = v_Premium;
            c.v_IncidentDate               = v_IncidentDate;

            //c.v_Policy_Integer1 = v_Policy_Integer1;


            return c;
        }
    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class PolicyRisk : RiskEntityBase, IRiskEntity
    {
        [DataMember(Name = "Pol_DbId")]
        public int Db_Id { get; set; }

        [DataMember(Name = "v_PTt")]
        public string v_PolicyType_Text { get; set; }
        [DataMember(Name = "v_PT")]
        public int v_PolicyType { get; set; } //PolicyType

        [DataMember(Name = "v_PN")]
        public string v_PolicyNumber { get; set; }
        [DataMember(Name = "v_TN")]
        public string v_TradingName { get; set; }
        [DataMember(Name = "v_B")]
        public string v_Broker { get; set; }

        [DataMember(Name = "v_CTt")]
        public string v_CoverType_Text { get; set; }
        [DataMember(Name = "v_CT")]
        public int v_CoverType { get; set; }  //PolicyCoverType

        [DataMember(Name = "v_PSD")]
        public DateTime? v_PolicyStartDate { get; set; }
        [DataMember(Name = "v_PED")]
        public DateTime? v_PolicyEndDate { get; set; }
        [DataMember(Name = "v_PNFCC")]
        public int? v_PreviousNoFaultClaimsCount { get; set; }
        [DataMember(Name = "v_PFCC")]
        public int? v_PreviousFaultClaimsCount { get; set; }

        [DataMember(Name = "v_Prem")]
        public decimal? v_Premium { get; set; }

        [DataMember(Name = "v_ID")]
        public virtual DateTime v_IncidentDate{ get; set; }

        public PolicyRisk()
        {
        }

        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        //public override void AssignUsedProperties(IRiskEntity dest)
        //{
        //}

        protected PolicyRisk ShallowCopyThis(PolicyRisk c)
        {
            c.db = db;


            c.Db_Id = Db_Id;
            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;

            c.v_PolicyType_Text = v_PolicyType_Text;
            c.v_PolicyType = v_PolicyType;

            c.v_PolicyNumber = v_PolicyNumber;
            c.v_TradingName = v_TradingName;
            c.v_Broker = v_Broker;

            c.v_CoverType_Text = v_CoverType_Text;
            c.v_CoverType = v_CoverType;

            c.v_PolicyStartDate = v_PolicyStartDate;
            c.v_PolicyEndDate = v_PolicyEndDate;
            c.v_PreviousNoFaultClaimsCount = v_PreviousNoFaultClaimsCount;
            c.v_PreviousFaultClaimsCount = v_PreviousFaultClaimsCount;
            c.v_Premium = v_Premium;
            c.v_IncidentDate = v_IncidentDate;

            //c.v_MyValue = v_MyValue;
            //c.v_Policy_Integer1 = v_Policy_Integer1;

            return c;
        }

        public override object Clone()
        {
            if (_trace)
            {
                ADATrace.WriteLine("Cloning Payment Card [" + Db_Id.ToString() + "]");
            }
            PolicyRisk c = new PolicyRisk();

            ShallowCopyThis(c);

            if (_trace)
            {
                ADATrace.WriteLine("Cloning Payment Card Complete");
            }

            return c;
        }


        public override string DisplayText
        {
            get { return "Policy: " + v_PolicyNumber; } // + "  [" + this.RuleGroupRiskTotalScore.ToString() + "]"; }
            //set;
        }

        [DataMember]
        public override string ReportHeading1
        {
            get { return "Policy: " + v_PolicyNumber; } // + "  [" + this.RuleGroupRiskTotalScore.ToString() + "]"; }
            //set;
        }

        [DataMember]
        public override string ReportHeading2
        {
            get { return "Policy: " + v_PolicyNumber; } // + "  [" + this.RuleGroupRiskTotalScore.ToString() + "]"; }
            //set;
        }
    }

}