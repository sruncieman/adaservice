﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using MDA.Common.Debug;

namespace RiskEngine.Scoring.Entities
{
    // PLACEHOLDER : This class is used in PERSON and ORG but it is not actually used as a scoring entity

    #region Testing Classes
    public class TelephoneRiskEditExtras
    {
        //public int v_BankAccount_Integer1 { get; set; }
    }

    public class TelephoneRiskEdit : TelephoneRisk
    {
        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();

        public TelephoneRiskEdit()
        {
            //v_PolicyPaymentType = PolicyPaymentType.Unknown;

            Extras = new TelephoneRiskEditExtras();
        }

        #region Extras
        public TelephoneRiskEditExtras Extras;

        //public override int v_BankAccount_Integer1
        //{
        //    get { return Extras.v_BankAccount_Integer1; }
        //    set { Extras.v_BankAccount_Integer1 = value; }
        //}


        #endregion

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }


        public override object Clone()
        {
            return CloneEntity(false);
        }

        public TelephoneRiskEdit CloneEntity(bool cloneChildren)
        {
            return new TelephoneRiskEdit()
            {
                v_TelephoneNumber = v_TelephoneNumber,
                v_Validated = v_Validated,
                v_CurrentLocation = v_CurrentLocation,
                v_Status = v_Status,
                v_TelephoneType = v_TelephoneType,
                v_TelephoneTypeText = v_TelephoneTypeText,
            };
        }
    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class TelephoneRisk : RiskEntityBase, IRiskEntity
    {
        [DataMember(Name = "DbId")]
        public int Db_Id { get; set; }

        [DataMember(Name = "v_TN")]
        public string v_TelephoneNumber { get; set; }

        [DataMember(Name = "v_VAL")]
        public bool? v_Validated { get; set; }

        [DataMember(Name = "v_TT")]
        public int v_TelephoneType { get; set; }

        [DataMember(Name = "v_TTt")]
        public string v_TelephoneTypeText { get; set; }

        [DataMember(Name = "v_St")]
        public string v_Status { get; set; }

        [DataMember(Name = "v_CL")]
        public string v_CurrentLocation { get; set; }

        public TelephoneRisk()
        {
        }

        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        //public override void AssignUsedProperties(IRiskEntity dest)
        //{
        //}

        protected TelephoneRisk ShallowCopyThis(TelephoneRisk c)
        {
            c.db = db;

            c.Db_Id = Db_Id;
            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;

            c.v_TelephoneNumber = v_TelephoneNumber;
            c.v_Validated = v_Validated;
            c.v_CurrentLocation = v_CurrentLocation;
            c.v_Status = v_Status;
            c.v_TelephoneType = v_TelephoneType;
            c.v_TelephoneTypeText = v_TelephoneTypeText;

            return c;
        }

        public override object Clone()
        {
            if (_trace)
            {
                ADATrace.WriteLine("Cloning Telephone [" + Db_Id.ToString() + "]");
            }

            TelephoneRisk c = new TelephoneRisk();

            ShallowCopyThis(c);

            if (_trace)
            {
                ADATrace.WriteLine("Cloning Telephone Complete");
            }

            return c;
        }


        public override string DisplayText
        {
            get { return "Tele: " + v_TelephoneNumber + "[" + v_TelephoneTypeText + "]"; } 
            //set;
        }

        [DataMember]
        public override string ReportHeading1
        {
            get { return "Tele: " + v_TelephoneNumber + "[" + v_TelephoneTypeText + "]"; } 
            //set;
        }

        [DataMember]
        public override string ReportHeading2
        {
            get { return "Tele: " + v_TelephoneNumber + "[" + v_TelephoneTypeText + "]"; } 
            //set;
        }
    }
}