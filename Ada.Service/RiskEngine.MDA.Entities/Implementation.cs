﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;

namespace RiskEngine.Scoring.Entities
{
    public class RiskEntitySetup
    {
        public const string PathToIncident                            = "Incident";
        public const string PathToIncidentPolicy                      = "Incident\\Policy";
        public const string PathToIncidentVehicles                    = "Incident\\Vehicles";
        public const string PathToIncidentPeople                      = "Incident\\People";
        public const string PathToIncidentPeopleAddress               = "Incident\\People\\Address";
        public const string PathToIncidentPeopleOrganisations         = "Incident\\People\\Organisations";
        public const string PathToIncidentPeopleOrganisationsAddress  = "Incident\\People\\Organisations\\Address";
        public const string PathToIncidentPeopleOrganisationsVehicles = "Incident\\People\\Organisations\\Vehicles";
        public const string PathToIncidentOrganisations               = "Incident\\Organisations";
        public const string PathToIncidentOrganisationsAddress        = "Incident\\Organisations\\Address";
        public const string PathToIncidentOrganisationsVehicles       = "Incident\\Organisations\\Vehicles";

        public static Dictionary<string, string> GetEntityStructure()
        {
            return new Dictionary<string, string>()
            {
                { PathToIncident,                                "RiskEngine.Scoring.Entities.IncidentRisk"   },
               
                { PathToIncidentPolicy,                          "RiskEngine.Scoring.Entities.PolicyRisk"  },
                
                { PathToIncidentVehicles,                        "RiskEngine.Scoring.Entities.VehicleRisk" },

                { PathToIncidentPeople,                          "RiskEngine.Scoring.Entities.PersonRisk"  },
                { PathToIncidentPeopleAddress,                   "RiskEngine.Scoring.Entities.AddressRisk" },
                { PathToIncidentPeopleOrganisations,             "RiskEngine.Scoring.Entities.OrganisationRisk" },
                { PathToIncidentPeopleOrganisationsAddress,      "RiskEngine.Scoring.Entities.AddressRisk" },
                { PathToIncidentPeopleOrganisationsVehicles,     "RiskEngine.Scoring.Entities.OrganisationVehicleRisk" },
               
                { PathToIncidentOrganisations,                   "RiskEngine.Scoring.Entities.OrganisationRisk" },
                { PathToIncidentOrganisationsAddress,            "RiskEngine.Scoring.Entities.AddressRisk" },
                { PathToIncidentOrganisationsVehicles,           "RiskEngine.Scoring.Entities.OrganisationVehicleRisk" }
            };
        }
    }

    public class MobileRiskEntitySetup
    {
        public const string PathToIncident = "Incident";
        public const string PathToIncidentPolicy = "Incident\\Policy";
        public const string PathToIncidentHandsets = "Incident\\Handsets";
        public const string PathToIncidentPeople = "Incident\\People";
        public const string PathToIncidentPeopleAddress = "Incident\\People\\Address";
        public const string PathToIncidentPeopleOrganisations = "Incident\\People\\Organisations";
        public const string PathToIncidentPeopleOrganisationsAddress = "Incident\\People\\Organisations\\Address";
        public const string PathToIncidentOrganisations = "Incident\\Organisations";
        public const string PathToIncidentOrganisationsAddress = "Incident\\Organisations\\Address";

        public static Dictionary<string, string> GetEntityStructure()
        {
            return new Dictionary<string, string>()
            {
                { PathToIncident,                                "RiskEngine.Scoring.Entities.IncidentRisk"   },
               
                { PathToIncidentPolicy,                          "RiskEngine.Scoring.Entities.PolicyRisk"  },
                
                { PathToIncidentHandsets,                        "RiskEngine.Scoring.Entities.HandsetRisk" },

                { PathToIncidentPeople,                          "RiskEngine.Scoring.Entities.PersonRisk"  },
                { PathToIncidentPeopleAddress,                   "RiskEngine.Scoring.Entities.AddressRisk" },
                { PathToIncidentPeopleOrganisations,             "RiskEngine.Scoring.Entities.OrganisationRisk" },
                { PathToIncidentPeopleOrganisationsAddress,      "RiskEngine.Scoring.Entities.AddressRisk" },
               
                { PathToIncidentOrganisations,                   "RiskEngine.Scoring.Entities.OrganisationRisk" },
                { PathToIncidentOrganisationsAddress,            "RiskEngine.Scoring.Entities.AddressRisk" },
            };
        }
    }

    //

    [DataContract(Namespace = XmlScores.Namespace)]
    public class ClaimBatchEdit
    {
        public ClaimBatchEdit()
        {
            Incidents = new ObservableCollection<IncidentRiskEdit>();
        }

        public ObservableCollection<IncidentRiskEdit> Incidents { get; set; }
    }

    [DataContract(Namespace = XmlScores.Namespace)]
    public class ClaimBatchTest
    {
        public ClaimBatchTest()
        {
            Incidents = new List<MotorClaimIncidentRisk>();
        }

        [DataMember]
        public List<MotorClaimIncidentRisk> Incidents { get; set; }
    }
}
