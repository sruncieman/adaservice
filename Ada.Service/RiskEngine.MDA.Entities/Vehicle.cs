﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using MDA.Common.Debug;
using MDA.DataService;


namespace RiskEngine.Scoring.Entities
{
    #region Testing Classes
    public class VehicleRiskEditExtras
    {
        public int? v_OccupancyCount { get; set; }
        public DateTime v_IncidentDate { get; set; }      // just the date
        public DateTime? v_IncidentDateTime { get; set; }  // date and time

    }

    public class VehicleRiskEdit : MotorClaimVehicleRisk
    {
        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();


        public VehicleRiskEdit()
        {
            People = new ObservableCollection<PersonRiskEdit>();

            Extras = new VehicleRiskEditExtras();

            v_VehicleType = (int)VehicleType.Car;
            v_VehicleType_Text = v_VehicleType.ToString();

            v_Colour = 0;
            v_Colour_Text = v_Colour.ToString();

            v_VehicleIncidentLink = (int)Incident2VehicleLinkType.Unknown;
            v_VehicleIncidentLink_Text = v_VehicleIncidentLink.ToString();

            v_VehicleType = (int)VehicleType.Unknown;
            v_VehicleType_Text = v_VehicleType.ToString();

            v_Fuel = (int)VehicleFuelType.Unknown;
            v_Fuel_Text = v_Fuel.ToString();

            v_Transmission = (int)VehicleTransmission.Unknown;
            v_Transmission_Text = v_Transmission.ToString();

            v_CategoryOfLoss = (int)VehicleCategoryOfLoss.Unknown;
            v_CategoryOfLoss_Text = v_CategoryOfLoss.ToString();

        }

        public ObservableCollection<PersonRiskEdit> People { get; set; }

        #region Extras
        public VehicleRiskEditExtras Extras;

        public override int? v_OccupancyCount
        {
            get { return Extras.v_OccupancyCount; }
            set { Extras.v_OccupancyCount = value; }
        }

        public override DateTime v_IncidentDate
        {
            get { return Extras.v_IncidentDate; }
            set { Extras.v_IncidentDate = value; }
        }

        public override DateTime? v_IncidentDateTime
        {
            get { return Extras.v_IncidentDateTime; }
            set { Extras.v_IncidentDateTime = value; }
        }
        #endregion

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, int KeyAttCount, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
                ClientScores[orgName].RuleGroupKeyAttractorTotalCount = KeyAttCount;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }


        public VehicleRiskEdit CloneEntity(bool cloneChildren)
        {
            VehicleRiskEdit c = new VehicleRiskEdit();

            c.v_VehicleType              = v_VehicleType;
            c.v_VehicleType_Text         = v_VehicleType_Text;
            c.v_VehicleIncidentLink      = v_VehicleIncidentLink;
            c.v_VehicleIncidentLink_Text = v_VehicleIncidentLink_Text;
            c.v_VehicleRegistration      = v_VehicleRegistration;
            c.v_Colour                   = v_Colour;
            c.v_Colour_Text              = v_Colour_Text;
            c.v_Make                     = v_Make;
            c.v_Model                    = v_Model;
            c.v_EngineCapacity           = v_EngineCapacity;
            c.v_VIN                      = v_VIN;
            c.v_Fuel                     = v_Fuel;
            c.v_Fuel_Text                = v_Fuel_Text;
            c.v_CategoryOfLoss           = v_CategoryOfLoss;
            c.v_CategoryOfLoss_Text      = v_CategoryOfLoss_Text;
            c.v_Transmission             = v_Transmission;
            c.v_Transmission_Text        = v_Transmission_Text;
            c.v_OccupancyCount           = v_OccupancyCount;
            c.v_IncidentDate = v_IncidentDate;
            c.v_IncidentDateTime = v_IncidentDateTime;

            if (cloneChildren)
            {
                foreach (PersonRiskEdit p in this.People)
                    c.People.Add(p.CloneEntity(true));
            }

            return c;
        }
    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class VehicleRisk : RiskEntityBase, IRiskEntity
    {
        public VehicleRisk()
        {
        }

        #region Public Scoring Properties
        [DataMember(Name = "Veh_DbId")]
        public int Db_Id { get; set; }

        [DataMember(Name = "Inc_DbId")]
        public int Db_Incident_Id { get; set; }

        [DataMember(Name="v_VT")]
        public int v_VehicleType { get; set; }  //VehicleType

        [DataMember(Name = "v_VTt")]
        public string v_VehicleType_Text { get; set; }

        [DataMember(Name = "v_VIL")]
        public int v_VehicleIncidentLink { get; set; }  // Incident2VehicleLinkType

        [DataMember(Name = "v_VILt")]
        public string v_VehicleIncidentLink_Text { get; set; }

        [DataMember(Name = "v_VR")]
        public string v_VehicleRegistration { get; set; }

        [DataMember(Name = "v_C")]
        public int v_Colour { get; set; }

        [DataMember(Name = "v_Ct")]
        public string v_Colour_Text { get; set; }

        [DataMember(Name = "v_Ma")]
        public string v_Make { get; set; }

        [DataMember(Name = "v_Mo")]
        public string v_Model { get; set; }

        [DataMember(Name = "v_EC")]
        public string v_EngineCapacity { get; set; }

        [DataMember(Name = "v_VIN")]
        public string v_VIN { get; set; }

        [DataMember(Name = "v_IsKA")]
        public bool v_IsKeyAttractor { get; set; }

        [DataMember(Name = "v_COL")]
        public int v_CategoryOfLoss { get; set; } //VehicleCategoryOfLoss

        [DataMember(Name = "v_COLt")]
        public string v_CategoryOfLoss_Text { get; set; }

        [DataMember(Name = "v_F")]
        public int v_Fuel { get; set; } //VehicleFuelType

        [DataMember(Name = "v_Ft")]
        public string v_Fuel_Text { get; set; }

        [DataMember(Name = "v_T")]
        public int v_Transmission { get; set; } //VehicleTransmission

        [DataMember(Name = "v_Tt")]
        public string v_Transmission_Text { get; set; }

        [DataMember(Name = "v_IDate")]
        public virtual DateTime v_IncidentDate { get; set; }

        [DataMember(Name = "v_IDT")]
        public virtual DateTime? v_IncidentDateTime { get; set; }

        #endregion Public Scoring Properties

        #region Public Alias Properties

        [DataMember(Name = "ConA")]
        public ListOfInts ConfirmedAliases { get; set; }
        [DataMember(Name = "UConA")]
        public ListOfInts UnconfirmedAliases { get; set; }
        [DataMember(Name = "TenA")]
        public ListOfInts TentativeAliases { get; set; }

        #endregion Public Alias Properties

        #region Scoring Methods
        private int? _v_OccupancyCount;


        [DataMember(Name = "v_OC")]
        public virtual int? v_OccupancyCount
        {
            get
            {
                if (Locked)
                {
                    if (_trace)
                    {
                        string x = (_v_OccupancyCount == null) ? "NA" : _v_OccupancyCount.ToString();

                        ADATrace.WriteLine("Vehicle.v_OccupancyCount. Serializing [" + x.ToString() + "]");
                    }

                    return _v_OccupancyCount;
                }

                if (_trace) ADATrace.WriteLine("Vehicle.v_OccupancyCount: [RiskClaim_Id=" + RiskClaimId.ToString() + " Vehicle_Id=" + this.Db_Id.ToString() + "]");

                if (_v_OccupancyCount == null)
                {
                    _v_OccupancyCount = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Vehicle_OccupancyCount(this.RiskClaimId, this.Db_Id);
                }

                if (_trace) ADATrace.WriteLine("Vehicle.v_OccupancyCount = " + _v_OccupancyCount.ToString());

                return (_v_OccupancyCount == null) ? 0 : (int)_v_OccupancyCount;

            }
            set 
            {
                _v_OccupancyCount = value; 
            }
        }

        public virtual int v_ExistsInKeoghsCfs(string cacheKey)
        {
            try
            {
                string key = string.Format("v_ExistsInKeoghsCfs");
                int result = 0;

                if (_trace) ADATrace.WriteLine("Vehicle." + key);

                if (MethodResultHistory.ContainsKey(key))
                {
                    if (_trace) ADATrace.WriteLine("Accessing cached result");

                    result = Convert.ToInt32(MethodResultHistory[key].Result);

                    this.MessageCache = MethodResultHistory[key].MessageCache;
                }
                else
                {
                    result = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Vehicle_IsInKeoghsCfs(this.Db_Incident_Id, this.v_IncidentDate, this.BaseRiskClaimId, this.Db_Id, this.v_VehicleIncidentLink_Text, _trace);
                }


                if (_trace) ADATrace.WriteLine("Vehicle.v_ExistsInKeoghsCfs = " + result.ToString());

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
                ADATrace.WriteLine("Vehicle.v_ExistsInKeoghsCfs = " + ex.InnerException);
            }

            return 0;
        }

        public virtual int v_NumberOfIncidents(string cacheKey, string matchType, string incidentSource, string incidentType, string periodSkip, string periodCount, string vehicleLinkType) //, string searchDate)
        {
            string key = string.Format("NOfIncidents: Id:{0}, P1:{1}, P2:{2}, P3:{3}, P4:{4}, P5:{5}, P6:{6}", this.Db_Id, matchType, incidentSource, incidentType, periodSkip, periodCount, vehicleLinkType); //, searchDate);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Vehicle." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                //this.v_VehicleIncidentLink_Text

                EntityAliases vehicleAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Vehicle_NumberOfIncidents(this.Db_Incident_Id, this.v_IncidentDate, this.BaseRiskClaimId, this.Db_Id, vehicleAliases, incidentSource, incidentType, periodSkip, periodCount, vehicleLinkType, this.v_VehicleIncidentLink_Text, _trace);

                result = (recs != null) ? recs.Count : 0;
                
                //result = (recs != null) ? recs.Count : 0;

                if (result > 0)
                {
                    foreach (var row in recs)
                    {
                        ListOfStringData newRow = new ListOfStringData();

                        newRow.Add(row.IncidentDate.ToString());
                        newRow.Add(row.Insurer);
                        newRow.Add(row.ClaimRef);
                        newRow.Add(row.DriverDetails);
                        newRow.Add(row.Involvement);

                        MessageCache.AddMediumOutputData(cacheKey, newRow, null);
                    }

                    PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);


                    string s = string.Format(MessageCache.GetMediumThenInMessage(cacheKey), result.ToString(),
                                                    ((result <= 2) ? "person" : "people"));

                    MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                }

                //PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey);

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });


            }
            if (_trace) ADATrace.WriteLine("Vehicle." + key + " = " + result.ToString());

            return result;
        }
        #endregion Scoring Methods


        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        public override void AssignUsedProperties(IRiskEntity dest)
        {
            VehicleRisk d = dest as VehicleRisk;

            if (_v_OccupancyCount != null)
                d.v_OccupancyCount = v_OccupancyCount;
        }

        protected VehicleRisk ShallowCopyThis(VehicleRisk c)
        {
            c.db = db;

            c.Db_Id = Db_Id;
            c.Db_Incident_Id = Db_Incident_Id;

            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;

            c.v_VehicleType = v_VehicleType;
            c.v_VehicleType_Text = v_VehicleType_Text;

            c.v_VehicleIncidentLink = v_VehicleIncidentLink;
            c.v_VehicleIncidentLink_Text = v_VehicleIncidentLink_Text;

            c.v_VehicleRegistration = v_VehicleRegistration;

            c.v_Colour = v_Colour;
            c.v_Colour_Text = v_Colour_Text;

            c.v_Make = v_Make;
            c.v_Model = v_Model;
            c.v_EngineCapacity = v_EngineCapacity;
            c.v_VIN = v_VIN;

            c.v_IsKeyAttractor = v_IsKeyAttractor;

            c.v_CategoryOfLoss = v_CategoryOfLoss;
            c.v_CategoryOfLoss_Text = v_CategoryOfLoss_Text;

            c.v_Fuel = v_Fuel;
            c.v_Fuel_Text = v_Fuel_Text;

            c.v_Transmission = v_Transmission;
            c.v_Transmission_Text = v_Transmission_Text;
            c.v_IncidentDate = v_IncidentDate;
            c.v_IncidentDateTime = v_IncidentDateTime;

            c.ConfirmedAliases = new ListOfInts();
            if (ConfirmedAliases != null)
                c.ConfirmedAliases.AddRange(ConfirmedAliases);

            c.UnconfirmedAliases = new ListOfInts();
            if (UnconfirmedAliases != null)
                c.UnconfirmedAliases.AddRange(UnconfirmedAliases);

            c.TentativeAliases = new ListOfInts();
            if (TentativeAliases != null)
                c.TentativeAliases.AddRange(TentativeAliases);

            return c;
        }

        /// <summary>
        /// We clone the object BEFORE scoring it so that no side-effects of scoring are passed between rulesets
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            if (_trace)
            {
                ADATrace.WriteLine("Cloning Vehicle [" + Db_Id.ToString() + "]");
            }

            VehicleRisk c = new VehicleRisk();

            ShallowCopyThis(c);

            if (_trace)
            {
                ADATrace.WriteLine("Cloning Vehicle Complete");
            }

            return c;
        }

        public string vehicleDescription(string vehicleRegistration, string vehicleVin, string vehicleMake, string vehicleModel)
        {
            if (!string.IsNullOrEmpty(vehicleRegistration))
            {
                return vehicleRegistration;
            }
            else if (!string.IsNullOrEmpty(vehicleVin))
            {
                return vehicleVin;
            }
            else
            {
                return vehicleMake + " " + vehicleModel;
            }
            
        }


        public override string DisplayText
        {
            get { return "Vehicle: " + vehicleDescription(v_VehicleRegistration,v_VIN,v_Make,v_Model) + "  [" + MDA.Common.Helpers.LookupHelper.VehicleIncidentLinkToString((int)v_VehicleIncidentLink) + "]"; }
        }

        [DataMember]
        public override string ReportHeading1
        {
            get { return "Vehicle: " + vehicleDescription(v_VehicleRegistration, v_VIN, v_Make, v_Model) + "  [" + MDA.Common.Helpers.LookupHelper.VehicleIncidentLinkToString((int)v_VehicleIncidentLink) + "]"; }
        }

        [DataMember]
        public override string ReportHeading2
        {
            get { return MDA.Common.Helpers.LookupHelper.VehicleIncidentLinkToString((int)v_VehicleIncidentLink) + " - " + ((v_VehicleRegistration == null) ? "Details Unknown" : v_VehicleRegistration); }
        }
    }

    /// <summary>
    /// This is a version of vehicle that contains a collection of People.  Used for MotorClaims
    /// </summary>
    [DataContract(Namespace = XmlScores.Namespace, Name="VehicleRisk")]   // Force the NAME to be the name if the inherited item so XML stays the same
    public class MotorClaimVehicleRisk : VehicleRisk
    {
        public ListOfInts PeopleOnClaimAndTheirAliasIds { get; set; }

        [DataMember(Name = "People")]
        [ScoreableItem(IsCollection = true, RulePath = "\\Incident\\People")] // Start path with \ because this entity "should really be" \Incident\vehicle\people
        public List<PersonRisk> VehiclePeople { get; set; }

        protected MotorClaimVehicleRisk ShallowCopyThis(MotorClaimVehicleRisk c)
        {
            base.ShallowCopyThis(c);

            c.PeopleOnClaimAndTheirAliasIds = new ListOfInts();
            if (PeopleOnClaimAndTheirAliasIds != null)
                c.PeopleOnClaimAndTheirAliasIds.AddRange(PeopleOnClaimAndTheirAliasIds);

            return c;
        }

        public override object Clone()
        {
            MotorClaimVehicleRisk c = new MotorClaimVehicleRisk();

            return ShallowCopyThis(c);
        }

    }
}
