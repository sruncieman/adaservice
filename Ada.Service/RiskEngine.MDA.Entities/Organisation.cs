﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using MDA.Common.Debug;
using MDA.DataService.PropertyMethods.Model;
using MDA.Common.Helpers;
using System.Text;

namespace RiskEngine.Scoring.Entities
{
    #region Testing Classes
    public class OrganisationRiskEditExtras
    {
        //public bool v_OrgKeyAttFraudRingDef { get; set; }

    }

    public class OrganisationRiskEdit : OrganisationRisk
    {
        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();


        public OrganisationRiskEdit()
        {
            Extras = new OrganisationRiskEditExtras();

            Addresses = new ObservableCollection<AddressRiskEdit>();
            Vehicles = new ObservableCollection<OrganisationVehicleRiskEdit>();

            v_OrganisationType = (int)OrganisationType.Unknown;
            v_OrganisationType_Text = v_OrganisationType.ToString();

            v_OrganisationPersonLinkType = (int)Person2OrganisationLinkType.Unknown;
            v_OrganisationPersonLinkType_Text = v_OrganisationPersonLinkType.ToString();
        }

        public ObservableCollection<AddressRiskEdit> Addresses { get; set; }
        public ObservableCollection<OrganisationVehicleRiskEdit> Vehicles { get; set; }

        #region Extras
        public OrganisationRiskEditExtras Extras;

        //public override bool v_OrgKeyAttFraudRingDef
        //{
        //    get { return Extras.v_OrgKeyAttFraudRingDef; }
        //    set { Extras.v_OrgKeyAttFraudRingDef = value; }
        //}


        #endregion

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, int KeyAttCount, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
                ClientScores[orgName].RuleGroupKeyAttractorTotalCount = KeyAttCount;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }


        public OrganisationRiskEdit CloneEntity(bool cloneChildren)
        {
            OrganisationRiskEdit c = new OrganisationRiskEdit();

            c.v_OrganisationType                = v_OrganisationType;
            c.v_OrganisationType_Text           = v_OrganisationType_Text;
            c.v_OrganisationPersonLinkType      = v_OrganisationPersonLinkType;
            c.v_OrganisationPersonLinkType_Text = v_OrganisationPersonLinkType_Text;
            c.v_OrganisationName                = v_OrganisationName;
            c.v_RegisteredNumber                = v_RegisteredNumber;
            c.v_VatNumber                       = v_VatNumber;
            //c.v_IsKeyAttractor                  = v_IsKeyAttractor;
            
            //c.v_OrgKeyAttFraudRingDef         = v_OrgKeyAttFraudRingDef;

            c.v_PartyType = v_PartyType;
            c.v_PartyType_Text = v_PartyType_Text;
            c.v_IsClaimLevel = v_IsClaimLevel;

            if (cloneChildren)
            {
                foreach (var a in this.Addresses)
                    c.Addresses.Add(a.CloneEntity(true));

                foreach (var v in this.Vehicles)
                    c.Vehicles.Add(v.CloneEntity(true));
            }

            return c;
        }

    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class OrganisationRisk : RiskEntityBase, IRiskEntity
    {
        [DataMember(Name = "Org_DbId")]
        public int Db_Id { get; set; }

        [DataMember(Name = "v_IsCL")]
        public bool v_IsClaimLevel { get; set; }

        [DataMember(Name = "v_OT")]
        public int v_OrganisationType { get; set; } //OrganisationType
        [DataMember(Name = "v_OTt")]
        public string v_OrganisationType_Text { get; set; }

        [DataMember(Name = "v_OPLT")]
        public int v_OrganisationPersonLinkType { get; set; } //Person2OrganisationLinkType
        [DataMember(Name = "v_OPLTt")]
        public string v_OrganisationPersonLinkType_Text { get; set; }

        [DataMember(Name = "v_PT")]
        public int v_PartyType { get; set; } //PartyType
        [DataMember(Name = "v_PTt")]
        public string v_PartyType_Text { get; set; }

        [DataMember(Name = "v_ON")]
        public string v_OrganisationName { get; set; }
        [DataMember(Name = "v_RN")]
        public string v_RegisteredNumber { get; set; }
        [DataMember(Name = "v_VN")]
        public string v_VatNumber { get; set; }

        [DataMember(Name = "v_ConA")]
        public ListOfInts ConfirmedAliases { get; set; }
        [DataMember(Name = "v_UConA")]
        public ListOfInts UnconfirmedAliases { get; set; }
        [DataMember(Name = "v_TenA")]
        public ListOfInts TentativeAliases { get; set; }

        [DataMember(Name = "v_EMAIL")]
        public string v_EmailAddress { get; set; }
        [DataMember(Name = "v_WSITE")]
        public string v_WebsiteAddress { get; set; }

        [DataMember(Name = "v_PTD")]
        public decimal? v_PaymentsToDate { get; set; }
        [DataMember(Name = "v_Res")]
        public decimal? v_Reserve { get; set; }

        [DataMember(Name = "v_LkADD")]
        public List<Organisation_LinkedAddresses> v_LinkedAddresses { get; set; }
        [DataMember(Name = "v_LkEM")]
        public List<Organisation_LinkedEmails> v_LinkedEmails { get; set; }
        [DataMember(Name = "v_LkWS")]
        public List<Organisation_LinkedWebSites> v_LinkedWebSites { get; set; }
        [DataMember(Name = "v_LkPH")]
        public List<Organisation_LinkedTelephones> v_LinkedTelephones { get; set; }
        [DataMember(Name = "v_TN")]
        public List<Organisation_TradingNames> v_TradingNames { get; set; }
        [DataMember(Name = "v_FN")]
        public List<Organisation_FormallyKnownAs> v_FormerNames { get; set; }

        [DataMember(Name = "v_OrgAddrs")]
        [ScoreableItem(IsCollection = true, RulePath = "Address")]
        public List<AddressRisk> OrganisationsAddresses { get; set; }

        [DataMember(Name = "v_OrgVehs")]
        [ScoreableItem(IsCollection = true, RulePath = "Vehicles")]
        public List<OrganisationVehicleRisk> OrganisationsVehicles { get; set; }

        [DataMember(Name = "v_TELE")]
        //[ScoreableItem(IsCollection = true, RulePath = "Telephones")]  // We don't score telephones separately yet
        public List<TelephoneRisk> OrganisationsTelephones { get; set; }

        public OrganisationRisk()
        {
        }

        #region Public Scoring Methods

        public virtual int v_OrganisationAliasesInfo(string cacheKey, string matchType)
        {

            string key = string.Format("OrgAliasesInfo: Id:{0}, P1:{1}", this.Db_Id.ToString(), matchType );

            int result = 0;

            if (_trace) ADATrace.WriteLine("Organisation." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                EntityAliases organisationAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Organisation_AliasInformation(organisationAliases, this.Db_Id, _trace);

                result = (recs != null) ? recs.Count : 0;

                //result = recs.Count();

                if (result > 0)
                {
                    foreach (var row in recs)
                    {
                        ListOfStringData newRow = new ListOfStringData();

                        newRow.Add(row.OrganisationId.ToString());
                        newRow.Add(row.OrganisationName);


                        MessageCache.AddMediumOutputData(cacheKey, newRow, null);
                    }

                    PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                    string s = string.Format(MessageCache.GetMediumThenInMessage(cacheKey));

                    MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Organisation." + key + " = " + result.ToString());

            return result;
        }

        //public virtual int v_LinkedAddresses(string cacheKey, string matchType)
        //{

        //    string key = string.Format("OrganisationLinkedAddresses: {0}, {1}", matchType, this.Db_Id.ToString());

        //    int result = 0;

        //    if (_trace) ADATrace.WriteLine("Organisation." + key);

        //    if (MethodResultHistory.ContainsKey(key))
        //    {
        //        if (_trace) ADATrace.WriteLine("Accessing cached result");

        //        result = Convert.ToInt32(MethodResultHistory[key].Result);

        //        this.MessageCache = MethodResultHistory[key].MessageCache;
        //    }
        //    else
        //    {
        //        PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

        //        EntityAliases organisationAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

        //        var recs = MDA.DataService.DataServices.Organisation_LinkedAddresses(db as MDADAL.MdaDbContext, organisationAliases, this.Db_Id, this.RiskClaimId, _trace);

        //        result = (recs != null) ? recs.Count : 0;

        //        //result = recs.Count();

        //        if (result > 0)
        //        {
        //            foreach (var row in recs)
        //            {
        //                ListOfStringData newRow = new ListOfStringData();

        //                newRow.Add(MDA.Common.Helpers.AddressHelper.FormatAddress(row.FullAddress));

        //                MessageCache.AddMediumOutputData(cacheKey, newRow, null);
        //            }

        //            PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

        //            string s = string.Format(MessageCache.GetMediumThenInMessage(cacheKey));

        //            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
        //        }

        //        MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
        //    }
        //    if (_trace) ADATrace.WriteLine("Organisation." + key + " = " + result.ToString());

        //    return result;
        //}

        public bool? v_IsKeyAttractor(string cacheKey, string matchType)
        {
            string key = string.Format("IsKeyAttractor: Id:{0}, P1:{1}", this.Db_Id.ToString(), matchType);

            bool result = false;

            if (_trace) ADATrace.WriteLine("Organisation." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                
                EntityAliases organisationAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Organisation_IsKeyAttractor(organisationAliases, this.Db_Id, cacheKey, this.MessageCache, _trace);

                result = (recs != null && recs.Count > 0);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);


                if (organisationAliases.IncludeThis || organisationAliases.IncludeConfirmed)
                {
                    foreach (var row in recs)
                    {
                        try
                        {
                            string s = string.Format(formatString, this.v_OrganisationName, row.KeyAttractor);

                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);

                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "IsKeyAttractor Format Error", null);
                        }
                    }
                }
                else if (organisationAliases.IncludeUnconfirmed)
                {
                    foreach (var row in recs)
                    {
                        try
                        {
                            string s = string.Format(formatString, this.v_OrganisationName, row.KeyAttractor);

                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "IsKeyAttractor Format Error", null);
                        }
                    }
                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Organisation." + key + " = " + result.ToString());

            return result;
        }

        public bool? v_IsSanctionsAttractor(string cacheKey, string matchType)
        {
            string key = string.Format("IsSanctionsAttractor: Id:{0}, P1:{1}", this.Db_Id.ToString(), matchType);

            bool result = false;

            if (_trace) ADATrace.WriteLine("Organisation." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                EntityAliases organisationAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Organisation_IsSanctionsAttractor(organisationAliases, this.Db_Id, cacheKey, this.MessageCache, _trace);

                result = (recs != null && recs.Count > 0);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);


                if (organisationAliases.IncludeThis || organisationAliases.IncludeConfirmed)
                {
                    foreach (var row in recs)
                    {
                        try
                        {
                            string s = string.Format(formatString, this.v_OrganisationName, row.KeyAttractor);

                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);

                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "IsSanctionsAttractor Format Error", null);
                        }
                    }
                }
                else if (organisationAliases.IncludeUnconfirmed)
                {
                    foreach (var row in recs)
                    {
                        try
                        {
                            string s = string.Format(formatString, this.v_OrganisationName, row.KeyAttractor);

                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "IsSanctionsAttractor Format Error", null);
                        }
                    }
                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Organisation." + key + " = " + result.ToString());

            return result;
        }

        public bool? v_IsTogAttractor(string cacheKey, string matchType)
        {
            string key = string.Format("IsTOGAttractor: Id:{0}, P1:{1}", this.Db_Id.ToString(), matchType);

            bool result = false;

            if (_trace) ADATrace.WriteLine("Organisation." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToBoolean(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {

                EntityAliases organisationAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Organisation_IsTOGAttractor(organisationAliases, this.Db_Id, cacheKey, this.MessageCache, _trace);

                result = (recs != null && recs.Count > 0);

                PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

                string formatString = MessageCache.GetMediumThenInMessage(cacheKey);


                if (organisationAliases.IncludeThis || organisationAliases.IncludeConfirmed)
                {
                    foreach (var row in recs)
                    {
                        try
                        {
                            string s = string.Format(formatString, this.v_OrganisationName);

                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);

                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "IsTOGAttractor Format Error", null);
                        }
                    }
                }
                else if (organisationAliases.IncludeUnconfirmed)
                {
                    foreach (var row in recs)
                    {
                        try
                        {
                            string s = string.Format(formatString, this.v_OrganisationName);

                            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                        }
                        catch (System.FormatException)
                        {
                            MessageCache.AddMediumOutputMessage(cacheKey, "IsTOGAttractor Format Error", null);
                        }
                    }
                }

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
            }
            if (_trace) ADATrace.WriteLine("Organisation." + key + " = " + result.ToString());

            return result;
        }

        //public virtual int v_LinkedTelephones(string cacheKey, string matchType)
        //{

        //    string key = string.Format("OrganisationLinkedTelephones: {0}, {1}", matchType, this.Db_Id.ToString());

        //    int result = 0;

        //    if (_trace) ADATrace.WriteLine("Organisation." + key);

        //    if (MethodResultHistory.ContainsKey(key))
        //    {
        //        if (_trace) ADATrace.WriteLine("Accessing cached result");

        //        result = Convert.ToInt32(MethodResultHistory[key].Result);

        //        this.MessageCache = MethodResultHistory[key].MessageCache;
        //    }
        //    else
        //    {
        //        PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

        //        EntityAliases organisationAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

        //        var recs = MDA.DataService.DataServices.Organisation_LinkedTelephones(db as MDADAL.MdaDbContext, organisationAliases, this.Db_Id, this.RiskClaimId, _trace);

        //        result = (recs != null) ? recs.Count : 0;

        //        //result = recs.Count();

        //        if (result > 0)
        //        {
        //            foreach (var row in recs)
        //            {
        //                ListOfStringData newRow = new ListOfStringData();

        //                newRow.Add(row.Telephone);

        //                MessageCache.AddMediumOutputData(cacheKey, newRow, null);
        //            }

        //            PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

        //            string s = string.Format(MessageCache.GetMediumThenInMessage(cacheKey));

        //            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
        //        }

        //        MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
        //    }
        //    if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

        //    return result;
        //}

        //public virtual int v_LinkedWebSites(string cacheKey, string matchType)
        //{

        //    string key = string.Format("OrganisationLinkedWebSites: {0}, {1}", matchType, this.Db_Id.ToString());

        //    int result = 0;

        //    if (_trace) ADATrace.WriteLine("Organisation." + key);

        //    if (MethodResultHistory.ContainsKey(key))
        //    {
        //        if (_trace) ADATrace.WriteLine("Accessing cached result");

        //        result = Convert.ToInt32(MethodResultHistory[key].Result);

        //        this.MessageCache = MethodResultHistory[key].MessageCache;
        //    }
        //    else
        //    {
        //        PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

        //        EntityAliases organisationAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

        //        var recs = MDA.DataService.DataServices.Organisation_LinkedWebSites(db as MDADAL.MdaDbContext, organisationAliases, this.Db_Id, this.RiskClaimId, _trace);

        //        result = (recs != null) ? recs.Count : 0;

        //        //result = recs.Count();

        //        if (result > 0)
        //        {
        //            foreach (var row in recs)
        //            {
        //                ListOfStringData newRow = new ListOfStringData();

        //                newRow.Add(row.WebSiteURL);

        //                MessageCache.AddMediumOutputData(cacheKey, newRow, null);
        //            }

        //            PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

        //            string s = string.Format(MessageCache.GetMediumThenInMessage(cacheKey));

        //            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
        //        }

        //        MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
        //    }
        //    if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

        //    return result;
        //}

        //public virtual int v_LinkedEmails(string cacheKey, string matchType)
        //{

        //    string key = string.Format("OrganisationLinkedEmails: {0}, {1}", matchType, this.Db_Id.ToString());

        //    int result = 0;

        //    if (_trace) ADATrace.WriteLine("Organisation." + key);

        //    if (MethodResultHistory.ContainsKey(key))
        //    {
        //        if (_trace) ADATrace.WriteLine("Accessing cached result");

        //        result = Convert.ToInt32(MethodResultHistory[key].Result);

        //        this.MessageCache = MethodResultHistory[key].MessageCache;
        //    }
        //    else
        //    {
        //        PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

        //        EntityAliases organisationAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

        //        var recs = MDA.DataService.DataServices.Organisation_LinkedEmails(db as MDADAL.MdaDbContext, organisationAliases, this.Db_Id, this.RiskClaimId, _trace);

        //        result = (recs != null) ? recs.Count : 0;

        //        //result = recs.Count();

        //        if (result > 0)
        //        {
        //            foreach (var row in recs)
        //            {
        //                ListOfStringData newRow = new ListOfStringData();

        //                newRow.Add(row.EmailAddress);

        //                MessageCache.AddMediumOutputData(cacheKey, newRow, null);
        //            }

        //            PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);

        //            string s = string.Format(MessageCache.GetMediumThenInMessage(cacheKey));

        //            MessageCache.AddMediumOutputMessage(cacheKey, s, null);
        //        }

        //        MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });
        //    }
        //    if (_trace) ADATrace.WriteLine("Person." + key + " = " + result.ToString());

        //    return result;
        //}

        #endregion Public Methods

        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        //public override void AssignUsedProperties(IRiskEntity dest)
        //{
        //}

        protected OrganisationRisk ShallowCopyThis(OrganisationRisk c)
        {
            c.db = db;

            c.Db_Id = Db_Id;
            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;
            c.v_OrganisationType = v_OrganisationType;
            c.v_OrganisationType_Text = v_OrganisationType_Text;
            c.v_OrganisationPersonLinkType = v_OrganisationPersonLinkType;
            c.v_OrganisationPersonLinkType_Text = v_OrganisationPersonLinkType_Text;
            c.v_OrganisationName = v_OrganisationName;
            c.v_RegisteredNumber = v_RegisteredNumber;
            c.v_VatNumber = v_VatNumber;
            //c.v_IsKeyAttractor                  = v_IsKeyAttractor;
            c.v_PaymentsToDate = v_PaymentsToDate;
            c.v_Reserve = v_Reserve;
            c.v_PartyType = v_PartyType;
            c.v_PartyType_Text = v_PartyType_Text;
            c.v_IsClaimLevel = v_IsClaimLevel;
            c.v_EmailAddress = v_EmailAddress;
            c.v_WebsiteAddress = v_WebsiteAddress;
            c.v_LinkedEmails = v_LinkedEmails;
            c.v_LinkedWebSites = v_LinkedWebSites;
            c.v_LinkedTelephones = v_LinkedTelephones;
            c.v_TradingNames = v_TradingNames;
            c.v_FormerNames = v_FormerNames;

            c.OrganisationsTelephones = new List<TelephoneRisk>();
            if (OrganisationsTelephones != null)
            {
                foreach (var x in OrganisationsTelephones)
                    c.OrganisationsTelephones.Add((TelephoneRisk)x.Clone());
            }

            c.v_LinkedEmails = new List<Organisation_LinkedEmails>();
            foreach (var x in v_LinkedEmails)
                c.v_LinkedEmails.Add(x.Clone());

            c.v_LinkedWebSites = new List<Organisation_LinkedWebSites>();
            foreach (var x in v_LinkedWebSites)
                c.v_LinkedWebSites.Add(x);

            c.v_LinkedTelephones = new List<Organisation_LinkedTelephones>();
            foreach (var x in v_LinkedTelephones)
                c.v_LinkedTelephones.Add(x);

            c.v_TradingNames = new List<Organisation_TradingNames>();
            foreach (var x in v_TradingNames)
                c.v_TradingNames.Add(x);

            c.v_FormerNames = new List<Organisation_FormallyKnownAs>();
            foreach (var x in v_FormerNames)
                c.v_FormerNames.Add(x);

            c.ConfirmedAliases = new ListOfInts();
            foreach (var x in ConfirmedAliases)
                c.ConfirmedAliases.Add(x);

            c.UnconfirmedAliases = new ListOfInts();
            foreach (var x in UnconfirmedAliases)
                c.UnconfirmedAliases.Add(x);


            c.TentativeAliases = new ListOfInts();
            foreach (var x in TentativeAliases)
                c.TentativeAliases.Add(x);

            //c.v_OrgKeyAttFraudRingDef         = v_OrgKeyAttFraudRingDef;

            return c;
        }

        public override object Clone()
        {
            if (_trace)
            {
                ADATrace.WriteLine("Cloning Organisation [" + Db_Id.ToString() + "]");
            }


            OrganisationRisk c = new OrganisationRisk();

            ShallowCopyThis(c);

            if (_trace)
            {
                ADATrace.WriteLine("Cloning Organisation Complete");
            }

            return c;
        }

        public override string DisplayText
        {
            get
            {
                StringBuilder r = new StringBuilder("Organisation:");

                if (!string.IsNullOrEmpty(v_OrganisationName))
                    r.Append(" " + v_OrganisationName);

                r.Append(" [" + LookupHelper.OrganisationTypeToString(v_OrganisationType) + ", " + v_OrganisationPersonLinkType_Text + "]");

                return r.ToString();
            }
            //set;
        }

        [DataMember]
        public override string ReportHeading1
        {
            get 
            {
                return DisplayText;
            } 

        }

        [DataMember]
        public override string ReportHeading2
        {
            get 
            {
                return v_OrganisationPersonLinkType_Text + " - " + v_OrganisationName; 
            } 

        }
    }

}
