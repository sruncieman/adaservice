﻿using RiskEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskEngine.Scoring.Entities
{
    /// <summary>
    /// Used ONLY by the testing classes in each risk entity
    /// </summary>
    public class ClientScore_TestingOnly
    {
        public ClientScore_TestingOnly()
        {
            ClearValues();
        }

        public DictionaryOfDictionaryOfRiskResults RuleGroupRiskResults { get; set; }
        public int RuleGroupRiskTotalScore { get; set; }
        public int RuleGroupKeyAttractorTotalCount { get; set; }
        public int ExpectedTotalScore { get; set; }
        public List<string> RuleGroupRiskErrors { get; set; }

        public int orgId { get; set; }
        public string orgName { get; set; }

        public void ClearValues()
        {
            RuleGroupRiskTotalScore         = -1;
            RuleGroupKeyAttractorTotalCount = -1;
            RuleGroupRiskResults            = new DictionaryOfDictionaryOfRiskResults();
            RuleGroupRiskErrors             = new List<string>();
        }

        public string DisplayText
        {
            get { return orgName + " :\t" + ExpectedTotalScore.ToString() + " = " + RuleGroupRiskTotalScore.ToString(); }
        }
    }
}
