﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RiskEngine.Model;
using System.Runtime.Serialization;
using MDA.Common.Enum;
using MDA.Common.Debug;
using MDA.DataService;


namespace RiskEngine.Scoring.Entities
{
    #region Testing Classes
    public class HandsetRiskEditExtras
    {
        public DateTime v_IncidentDate { get; set; }      // just the date
        public DateTime? v_IncidentDateTime { get; set; }  // date and time

    }

    public class HandsetRiskEdit : MobileClaimHandsetRisk
    {
        public Dictionary<string, ClientScore_TestingOnly> ClientScores = new Dictionary<string, ClientScore_TestingOnly>();


        public HandsetRiskEdit()
        {
            People = new ObservableCollection<PersonRiskEdit>();

            Extras = new HandsetRiskEditExtras();

            //v_HandsetType = (int)HandsetType.Unknown;
            v_HandsetType_Text = v_HandsetType.ToString();

            v_HandsetIncidentLink = (int)Incident2HandsetLinkType.Unknown;
            v_HandsetIncidentLink_Text = v_HandsetIncidentLink.ToString();

            v_HandsetType = (int)HandsetType.Unknown;
            v_HandsetType_Text = v_HandsetType.ToString();

        }

        public ObservableCollection<PersonRiskEdit> People { get; set; }

        #region Extras
        public HandsetRiskEditExtras Extras;

        public override DateTime v_IncidentDate
        {
            get { return Extras.v_IncidentDate; }
            set { Extras.v_IncidentDate = value; }
        }

        public override DateTime? v_IncidentDateTime
        {
            get { return Extras.v_IncidentDateTime; }
            set { Extras.v_IncidentDateTime = value; }
        }
        #endregion

        public void ClearClientScores(string orgName, int orgId)
        {
            if (ClientScores.ContainsKey(orgName))
                ClientScores[orgName].ClearValues();
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgId = orgId, orgName = orgName });
        }

        public void SaveClientScore(string orgName, int orgId, int score, int KeyAttCount, DictionaryOfDictionaryOfRiskResults results, List<string> errors)
        {
            if (ClientScores.ContainsKey(orgName))
            {
                ClientScores[orgName].RuleGroupRiskTotalScore = score;
                ClientScores[orgName].RuleGroupRiskResults = results;
                ClientScores[orgName].RuleGroupRiskErrors = errors;
                ClientScores[orgName].RuleGroupKeyAttractorTotalCount = KeyAttCount;
            }
            else
                ClientScores.Add(orgName, new ClientScore_TestingOnly() { orgName = orgName, orgId = orgId, RuleGroupRiskTotalScore = score, RuleGroupRiskResults = results, RuleGroupRiskErrors = errors });
        }

        public List<ClientScore_TestingOnly> ExpectedResults
        {
            get
            {
                List<ClientScore_TestingOnly> res = new List<ClientScore_TestingOnly>();

                foreach (var s in ClientScores)
                {
                    var ss = s.Value;

                    res.Add(ss); //.Key + " " + ss.ExpectedTotalScore.ToString() + " = " + ss.RuleGroupRiskTotalScore.ToString());
                }

                return res;
            }
        }


        public HandsetRiskEdit CloneEntity(bool cloneChildren)
        {
            HandsetRiskEdit c = new HandsetRiskEdit();

            c.v_HandsetType = v_HandsetType;
            c.v_HandsetType_Text = v_HandsetType_Text;
            c.v_HandsetIncidentLink = v_HandsetIncidentLink;
            c.v_HandsetIncidentLink_Text = v_HandsetIncidentLink_Text;
            c.v_Make = v_Make;
            c.v_Model = v_Model;
            c.v_IMEI = v_IMEI;
            c.v_IncidentDate = v_IncidentDate;
            c.v_IncidentDateTime = v_IncidentDateTime;

            if (cloneChildren)
            {
                foreach (PersonRiskEdit p in this.People)
                    c.People.Add(p.CloneEntity(true));
            }

            return c;
        }
    }
    #endregion Testing Classes

    [DataContract(Namespace = XmlScores.Namespace)]
    public class HandsetRisk : RiskEntityBase, IRiskEntity
    {
        public HandsetRisk()
        {
        }

        #region Public Scoring Properties
        [DataMember(Name = "Han_DbId")]
        public int Db_Id { get; set; }

        [DataMember(Name = "Inc_DbId")]
        public int Db_Incident_Id { get; set; }

        [DataMember(Name = "h_VT")]
        public int v_HandsetType { get; set; }  //HandsetType

        [DataMember(Name = "h_VTt")]
        public string v_HandsetType_Text { get; set; }

        [DataMember(Name = "h_VIL")]
        public int v_HandsetIncidentLink { get; set; }  // Incident2HandsetLinkType

        [DataMember(Name = "h_VILt")]
        public string v_HandsetIncidentLink_Text { get; set; }

        [DataMember(Name = "h_Ma")]
        public string v_Make { get; set; }

        [DataMember(Name = "h_Mo")]
        public string v_Model { get; set; }

        [DataMember(Name = "h_IMEI")]
        public string v_IMEI { get; set; }

        [DataMember(Name = "h_IDate")]
        public virtual DateTime v_IncidentDate { get; set; }

        [DataMember(Name = "h_IDT")]
        public virtual DateTime? v_IncidentDateTime { get; set; }

        #endregion Public Scoring Properties

        #region Public Alias Properties

        [DataMember(Name = "ConA")]
        public ListOfInts ConfirmedAliases { get; set; }
        [DataMember(Name = "UConA")]
        public ListOfInts UnconfirmedAliases { get; set; }
        [DataMember(Name = "TenA")]
        public ListOfInts TentativeAliases { get; set; }

        #endregion Public Alias Properties

        #region Scoring Methods

        public virtual int v_NumberOfIncidents(string cacheKey, string matchType, string incidentSource, string incidentType, string periodSkip, string periodCount, string handsetLinkType) //, string searchDate)
        {
            string key = string.Format("NOfIncidents: Id:{0}, P1:{1}, P2:{2}, P3:{3}, P4:{4}, P5:{5}, P6:{6}", this.Db_Id, matchType, incidentSource, incidentType, periodSkip, periodCount, handsetLinkType); //, searchDate);
            int result = 0;

            if (_trace) ADATrace.WriteLine("Handset." + key);

            if (MethodResultHistory.ContainsKey(key))
            {
                if (_trace) ADATrace.WriteLine("Accessing cached result");

                result = Convert.ToInt32(MethodResultHistory[key].Result);

                this.MessageCache = MethodResultHistory[key].MessageCache;
            }
            else
            {
                //this.v_HandsetIncidentLink_Text

                EntityAliases handsetAliases = new EntityAliases(matchType, this.ConfirmedAliases, this.UnconfirmedAliases, this.TentativeAliases);

                PopulateMessageCacheInputMessagesFromRulesDb(cacheKey);

                var recs = new MDA.DataService.PropertyMethods.PropertyDataServices(ctx).Handset_NumberOfIncidents(this.Db_Incident_Id, this.v_IncidentDate, this.BaseRiskClaimId, this.Db_Id, handsetAliases, incidentSource, incidentType, periodSkip, periodCount, _trace);

                result = (recs != null) ? recs.Count : 0;

                //result = (recs != null) ? recs.Count : 0;

                if (result > 0)
                {
                    foreach (var row in recs)
                    {
                        ListOfStringData newRow = new ListOfStringData();

                        newRow.Add(row.IncidentDate.ToString());
                        newRow.Add(row.Insurer);
                        newRow.Add(row.ClaimRef);
                        newRow.Add(row.DriverDetails);
                        newRow.Add(row.Involvement);

                        MessageCache.AddMediumOutputData(cacheKey, newRow, null);
                    }

                    PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey, true, false, true);


                    string s = string.Format(MessageCache.GetMediumThenInMessage(cacheKey), result.ToString(),
                                                    ((result <= 2) ? "person" : "people"));

                    MessageCache.AddMediumOutputMessage(cacheKey, s, null);
                }

                //PopulateMessageCacheOutputMessagesFromInputMessages(cacheKey);

                MethodResultHistory.Add(key, new MethodResultRecord() { Result = result, MessageCache = this.MessageCache });


            }
            if (_trace) ADATrace.WriteLine("Handset." + key + " = " + result.ToString());

            return result;
        }
        #endregion Scoring Methods


        /// <summary>
        /// This method is called during scoring so that you can optimise the code. If you have called a property in one ruleset and you want to pass
        /// this to another ruleset (to avoid it getting called again) assign its value in here. You are in effect assigning THIS.property to a 
        /// DESTination entity before scoring starts on this new dest entity
        /// </summary>
        /// <param name="dest"></param>
        public override void AssignUsedProperties(IRiskEntity dest)
        {
            HandsetRisk d = dest as HandsetRisk;

        }

        protected HandsetRisk ShallowCopyThis(HandsetRisk c)
        {
            c.db = db;

            c.Db_Id = Db_Id;
            c.Db_Incident_Id = Db_Incident_Id;

            c.BaseRiskClaimId = BaseRiskClaimId;
            c.RiskClaimId = RiskClaimId;
            c.RuleSetId = RuleSetId;
            c.RuleSetName = RuleSetName;

            c.v_HandsetType = v_HandsetType;
            c.v_HandsetType_Text = v_HandsetType_Text;

            c.v_HandsetIncidentLink = v_HandsetIncidentLink;
            c.v_HandsetIncidentLink_Text = v_HandsetIncidentLink_Text;

            c.v_Make = v_Make;
            c.v_Model = v_Model;
            c.v_IMEI = v_IMEI;

            c.v_IncidentDate = v_IncidentDate;
            c.v_IncidentDateTime = v_IncidentDateTime;

            c.ConfirmedAliases = new ListOfInts();
            if (ConfirmedAliases != null)
                c.ConfirmedAliases.AddRange(ConfirmedAliases);

            c.UnconfirmedAliases = new ListOfInts();
            if (UnconfirmedAliases != null)
                c.UnconfirmedAliases.AddRange(UnconfirmedAliases);

            c.TentativeAliases = new ListOfInts();
            if (TentativeAliases != null)
                c.TentativeAliases.AddRange(TentativeAliases);

            return c;
        }

        /// <summary>
        /// We clone the object BEFORE scoring it so that no side-effects of scoring are passed between rulesets
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            if (_trace)
            {
                ADATrace.WriteLine("Cloning Handset [" + Db_Id.ToString() + "]");
            }

            HandsetRisk c = new HandsetRisk();

            ShallowCopyThis(c);

            if (_trace)
            {
                ADATrace.WriteLine("Cloning Handset Complete");
            }

            return c;
        }

        public string handsetDescription( string handsetIMEI, string handsetMake, string handsetModel)
        {
            if (!string.IsNullOrEmpty(handsetIMEI))
            {
                return handsetIMEI;
            }
            else
            {
                return handsetMake + " " + handsetModel;
            }

        }


        public override string DisplayText
        {
            get { return "Handset: " + handsetDescription(v_IMEI, v_Make, v_Model); }
        }

        [DataMember]
        public override string ReportHeading1
        {
            get { return "Handset: " + handsetDescription(v_IMEI, v_Make, v_Model); }
        }

        [DataMember]
        public override string ReportHeading2
        {
            get { return "Handset: " + handsetDescription(v_IMEI, v_Make, v_Model); }
        }
    }

    /// <summary>
    /// This is a version of Handset that contains a collection of People.  Used for MobileClaims
    /// </summary>
    [DataContract(Namespace = XmlScores.Namespace, Name = "HandsetRisk")]   // Force the NAME to be the name if the inherited item so XML stays the same
    public class MobileClaimHandsetRisk : HandsetRisk
    {
        public ListOfInts PeopleOnClaimAndTheirAliasIds { get; set; }

        [DataMember(Name = "People")]
        [ScoreableItem(IsCollection = true, RulePath = "\\Incident\\People")] // Start path with \ because this entity "should really be" \Incident\Handset\people
        public List<PersonRisk> HandsetPeople { get; set; }

        protected MobileClaimHandsetRisk ShallowCopyThis(MobileClaimHandsetRisk c)
        {
            base.ShallowCopyThis(c);

            c.PeopleOnClaimAndTheirAliasIds = new ListOfInts();
            if (PeopleOnClaimAndTheirAliasIds != null)
                c.PeopleOnClaimAndTheirAliasIds.AddRange(PeopleOnClaimAndTheirAliasIds);

            return c;
        }

        public override object Clone()
        {
            MobileClaimHandsetRisk c = new MobileClaimHandsetRisk();

            return ShallowCopyThis(c);
        }

    }
}
