﻿
INSERT [dbo].[_Account__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Account_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Address__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Address_to_Address_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_BB_Pin_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_BBPin__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Case_to_Incident_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (1721, 1, NULL, N'Redtri', -1, 11, N'Claim Type', 1, N'', 0, 0, 0, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (2009, 1, NULL, N'Redsq', -1, 11, N'Fraud Ring', 1, N'', 0, 0, 0, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (2011, 1, NULL, N'_confrm1', -1, 11, N'Keoghs Elite Reference', 1, N'', 0, 0, 0, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (2012, 1, NULL, N'Reddot', -1, 11, N'Client', 1, N'', 0, 0, 0, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (2013, 1, NULL, N'Blackdot', -1, 11, N'Date of Birth', 1, N'', 0, 0, 0, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (2282, 1, NULL, N'Bluedot', -1, 11, N'Policy Type', 1, N'', 0, 0, 0, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (2289, 1, NULL, N'Bluetri', -1, 11, N'Policy Cover Type', 1, N'', 0, 0, 0, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (2899, 1, NULL, N'Ac-deflt', -1, 11, N'Vehicle Catery of Loss', 1, N'', 0, 0, 0, 0, -1, -1, 0, -1, N' Loss', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (3126, 1, NULL, N'Address5', -1, 11, N'Residency Start', 1, N'Residency Start ', 0, 0, -1, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (3127, 1, NULL, N'Address5', -1, 11, N'Residency End', 1, N'Residency End ', 0, 0, -1, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (3151, 1, NULL, N'Address3', -1, 11, N'Reg Keeper Start', 1, N'Reg Keeper Start ', 0, 0, -1, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (3152, 1, NULL, N'Address3', -1, 11, N'Reg Keeper End', 1, N'Reg Keeper End ', 0, 0, -1, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (3153, 1, NULL, N'Address3', -1, 11, N'Hire Company', 1, N'Hired from ', 0, 0, -1, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (3154, 1, NULL, N'Address3', -1, 11, N'Hire Start', 1, N'Hire Start ', 0, 0, -1, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (3155, 1, NULL, N'Address3', -1, 11, N'Hire End', 1, N'Hire End ', 0, 0, -1, 0, 0, -1, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (4021, 0, NULL, N'_confrm3', -1, 3, N'PAF', 1, N'PAF Validated', 0, -1, -1, 0, 0, -1, 0, 0, N'', -1, -1, -1)

INSERT [dbo].[_ChartAttribute] ([ChartAttributeID], [AttributeType], [DecimalPlaces], [Icon], [IsUser], [MergeBehaviour], [Name], [PasteBehaviour], [Prefix], [ShowDate], [ShowIfSet], [ShowPrefix], [ShowSeconds], [ShowSuffix], [ShowSymbol], [ShowTime], [ShowValue], [Suffix], [UserCanAdd], [UserCanRemove], [Visible]) VALUES (4029, 1, NULL, N'', -1, 11, N'VIN Number', 1, N'', 0, 0, 0, 0, 0, 0, 0, -1, N'', -1, -1, -1)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1007, N'Bank name', N'Provides the name of the bank, building society or card issuer', NULL, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1081, N'County', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1165, N'Address Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1218, N'Fraud Ring (Name)', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1680, N'Keoghs Employee', N'Employee Name & Team Name', 1761, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1681, N'Fraud Ring Status', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1699, N'Incident Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1713, N'Incident Icon', N'', 0, NULL, 6)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1722, N'Claim Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1761, N'Keoghs Office', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1767, N'Insurers/Clients', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1877, N'Keoghs Case Status', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1904, N'Organisation Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1915, N'Organisation Icon', N'', 0, NULL, 6)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1922, N'Organisation Status', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1934, N'MoJ CRM Status', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1965, N'Payment Card Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1979, N'Salutation', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (1998, N'Gender', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2003, N'Person Icon', N'', 0, NULL, 6)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2014, N'Nationality', N'', NULL, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2275, N'Policy Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2284, N'Policy Cover Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2311, N'Telephone Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2317, N'Telephone Icon', N'', 0, NULL, 6)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2333, N'Vehicle Make', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2405, N'Vehicle Colour', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2432, N'Vehicle Fuel', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2437, N'Vehicle Transmission', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2442, N'Vehicle Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2450, N'Vehicle Icon', N'', 0, NULL, 6)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2494, N'Address Link Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2503, N'Case Incident Link Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2575, N'Party Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2605, N'Organisation to Organisation Link Type', N'', NULL, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2713, N'Sub Party Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2766, N'Policy Link Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2791, N'Person to Organisation Link Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2843, N'Person to Person Link Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2887, N'Vehicle Incident Link Type', N'', NULL, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2894, N'Vehicle Catery of Loss', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2911, N'Vehicle Link Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (2953, N'Intelligence Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (3171, N'Grading', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (3633, N'Telephone Link Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (4041, N'Handset Colour', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (4084, N'Handset Incident Link Type', N'', 0, NULL, 3)

INSERT [dbo].[_CodeGroup] ([CodeGroup_ID], [Description], [Notes], [Parent_ID], [SortOrder], [Type]) VALUES (4110, N'Handset Link Type', N'', 0, NULL, 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1008', NULL, 1007, N'', N'Abbey', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1009', NULL, 1007, N'', N'Alliance & Leicester', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1010', NULL, 1007, N'', N'American Express', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1011', NULL, 1007, N'', N'Bank of America', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1012', NULL, 1007, N'', N'Bank of England', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1013', NULL, 1007, N'', N'Bank Of Ireland', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1014', NULL, 1007, N'', N'Bank of Scotland', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1015', NULL, 1007, N'', N'Barclaycard', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1016', NULL, 1007, N'', N'Barclays', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1017', NULL, 1007, N'', N'Birmingham Midshires', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1018', NULL, 1007, N'', N'Capital One', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1019', NULL, 1007, N'', N'Chelsea Building Society', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1020', NULL, 1007, N'', N'Clydesdale Bank', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1021', NULL, 1007, N'', N'Co Operative Bank', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1022', NULL, 1007, N'', N'Coventry Building Society', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1023', NULL, 1007, N'', N'Dresdner Bank AG', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1024', NULL, 1007, N'', N'Egg Banking Plc', N'', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1025', NULL, 1007, N'', N'First National Bank', N'', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1026', NULL, 1007, N'', N'GE Capital Bank Ltd', N'', 19)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1027', NULL, 1007, N'', N'Habib Bank', N'', 20)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1028', NULL, 1007, N'', N'Halifax Bank', N'', 21)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1029', NULL, 1007, N'', N'HFC', N'', 22)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1030', NULL, 1007, N'', N'HSBC', N'', 23)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1031', NULL, 1007, N'', N'ING', N'', 24)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1032', NULL, 1007, N'', N'Islamic Bank of Britain', N'', 25)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1033', NULL, 1007, N'', N'Lloyds TSB', N'', 26)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1034', NULL, 1007, N'', N'Mastercard', N'', 27)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1035', NULL, 1007, N'', N'MBNA', N'', 28)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1036', NULL, 1007, N'', N'Morgan Stanley', N'', 29)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1037', NULL, 1007, N'', N'Nationwide', N'', 30)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1038', NULL, 1007, N'', N'Natwest', N'', 31)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1039', NULL, 1007, N'', N'Northern Rock', N'', 32)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1040', NULL, 1007, N'', N'Portman Building Society', N'', 33)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1041', NULL, 1007, N'', N'Royal Bank Of Scotland', N'', 34)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1042', NULL, 1007, N'', N'Sainsbury''s Bank Plc', N'', 35)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1043', NULL, 1007, N'', N'Santander UK Plc', N'', 36)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1044', NULL, 1007, N'', N'Tesco Personal Finance Ltd', N'', 37)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1045', NULL, 1007, N'', N'The Post Office', N'', 38)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1046', NULL, 1007, N'', N'The Woolwich', N'', 39)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1047', NULL, 1007, N'', N'Yorkshire Bank PLC', N'', 40)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1082', NULL, 1081, N'', N'Aberdeenshire', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1083', NULL, 1081, N'', N'Angus', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1084', NULL, 1081, N'', N'Avon', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1085', NULL, 1081, N'', N'Ayrshire', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1086', NULL, 1081, N'', N'Bedfordshire', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1087', NULL, 1081, N'', N'Berkshire', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1088', NULL, 1081, N'', N'Borders', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1089', NULL, 1081, N'', N'Buckinghamshire', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1090', NULL, 1081, N'', N'Cambridgeshire', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1091', NULL, 1081, N'', N'Cheshire', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1092', NULL, 1081, N'', N'Cleveland', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1093', NULL, 1081, N'', N'Clwyd', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1094', NULL, 1081, N'', N'Cornwall', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1095', NULL, 1081, N'', N'County Antrim', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1096', NULL, 1081, N'', N'County Armagh', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1097', NULL, 1081, N'', N'County Down', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1098', NULL, 1081, N'', N'County Durham', N'', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1099', NULL, 1081, N'', N'Cumbria', N'', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1100', NULL, 1081, N'', N'Denbighshire', N'', 19)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1101', NULL, 1081, N'', N'Derbyshire', N'', 20)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1102', NULL, 1081, N'', N'Devon', N'', 21)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1103', NULL, 1081, N'', N'Dorset', N'', 22)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1104', NULL, 1081, N'', N'Dumfries & Galloway', N'', 23)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1105', NULL, 1081, N'', N'Dyfed', N'', 24)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1106', NULL, 1081, N'', N'East Sussex', N'', 25)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1107', NULL, 1081, N'', N'East Yorkshire', N'', 26)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1108', NULL, 1081, N'', N'Essex', N'', 27)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1109', NULL, 1081, N'', N'Fermanagh', N'', 28)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1110', NULL, 1081, N'', N'Fife', N'', 29)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1111', NULL, 1081, N'', N'Flintshire', N'', 30)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1112', NULL, 1081, N'', N'Gloucestershire', N'', 31)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1113', NULL, 1081, N'', N'Grampian', N'', 32)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1114', NULL, 1081, N'', N'Greater London', N'', 33)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1115', NULL, 1081, N'', N'Greater Manchester', N'', 34)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1116', NULL, 1081, N'', N'Gwent', N'', 35)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1117', NULL, 1081, N'', N'Gwynedd', N'', 36)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1118', NULL, 1081, N'', N'Hampshire', N'', 37)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1119', NULL, 1081, N'', N'Hereford & Worcester', N'', 38)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1120', NULL, 1081, N'', N'Hertfordshire', N'', 39)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1121', NULL, 1081, N'', N'Highlands', N'', 40)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1122', NULL, 1081, N'', N'Humberside', N'', 41)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1123', NULL, 1081, N'', N'Isle of Wight', N'', 42)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1124', NULL, 1081, N'', N'Kent', N'', 43)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1125', NULL, 1081, N'', N'Lancashire', N'', 44)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1126', NULL, 1081, N'', N'Leicestershire', N'', 45)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1127', NULL, 1081, N'', N'Lincolnshire', N'', 46)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1128', NULL, 1081, N'', N'Londonderry', N'', 47)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1129', NULL, 1081, N'', N'Lothian', N'', 48)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1130', NULL, 1081, N'', N'Merseyside', N'', 49)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1131', NULL, 1081, N'', N'Mid Glamorgan', N'', 50)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1132', NULL, 1081, N'', N'Middlesex', N'', 51)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1133', NULL, 1081, N'', N'Norfolk', N'', 52)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1134', NULL, 1081, N'', N'North Yorkshire', N'', 53)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1135', NULL, 1081, N'', N'Northamptonshire', N'', 54)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1136', NULL, 1081, N'', N'Northumberland', N'', 55)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1137', NULL, 1081, N'', N'Nottinghamshire', N'', 56)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1138', NULL, 1081, N'', N'Oxfordshire', N'', 57)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1139', NULL, 1081, N'', N'Pembrokeshire', N'', 58)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1140', NULL, 1081, N'', N'Powys', N'', 59)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1141', NULL, 1081, N'', N'Rutland', N'', 60)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1142', NULL, 1081, N'', N'Shetland', N'', 61)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1143', NULL, 1081, N'', N'Shropshire', N'', 62)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1144', NULL, 1081, N'', N'Somerset', N'', 63)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1145', NULL, 1081, N'', N'South Glamorgan', N'', 64)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1146', NULL, 1081, N'', N'South Yorkshire', N'', 65)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1147', NULL, 1081, N'', N'Staffordshire', N'', 66)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1148', NULL, 1081, N'', N'Strathclyde', N'', 67)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1149', NULL, 1081, N'', N'Suffolk', N'', 68)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1150', NULL, 1081, N'', N'Surrey', N'', 69)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1151', NULL, 1081, N'', N'Tayside', N'', 70)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1152', NULL, 1081, N'', N'Tyne & Wear', N'', 71)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1153', NULL, 1081, N'', N'Tyrone', N'', 72)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1154', NULL, 1081, N'', N'Warwickshire', N'', 73)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1155', NULL, 1081, N'', N'West Glamorgan', N'', 74)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1156', NULL, 1081, N'', N'West Midlands', N'', 75)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1157', NULL, 1081, N'', N'West Sussex', N'', 76)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1158', NULL, 1081, N'', N'West Yorkshire', N'', 77)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1159', NULL, 1081, N'', N'Wiltshire', N'', 78)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1160', NULL, 1081, N'', N'Worcestershire', N'', 79)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1166', NULL, 1165, N'Home Address', N'Residential', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1167', NULL, 1165, N'', N'Commercial', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1219', NULL, 1218, N'', N'4 Ever', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1220', NULL, 1218, N'', N'43', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1221', NULL, 1218, N'', N'91', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1222', NULL, 1218, N'', N'Aaron', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1223', NULL, 1218, N'', N'Abbott', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1224', NULL, 1218, N'', N'Abraham', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1225', NULL, 1218, N'', N'Acorn', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1226', NULL, 1218, N'', N'Acromas', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1227', NULL, 1218, N'', N'Adonis', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1228', NULL, 1218, N'', N'Adriano', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1229', NULL, 1218, N'', N'Aeolus', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1230', NULL, 1218, N'', N'Ageas', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1231', NULL, 1218, N'', N'Air', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1232', NULL, 1218, N'', N'Aire', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1233', NULL, 1218, N'', N'Ajax', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1234', NULL, 1218, N'', N'Ajax NFU Mutual', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1235', NULL, 1218, N'', N'Alf', N'', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1236', NULL, 1218, N'', N'Alice', N'', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1237', NULL, 1218, N'', N'Alithea', N'', 19)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1238', NULL, 1218, N'', N'Allen', N'', 20)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1239', NULL, 1218, N'', N'Alligator', N'', 21)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1240', NULL, 1218, N'', N'Alonso', N'', 22)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1241', NULL, 1218, N'', N'Amazon', N'', 23)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1242', NULL, 1218, N'', N'Amber', N'', 24)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1243', NULL, 1218, N'', N'Amethyst', N'', 25)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1244', NULL, 1218, N'', N'Amstel', N'', 26)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1245', NULL, 1218, N'', N'Andromeda', N'', 27)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1246', NULL, 1218, N'', N'Andronicus', N'', 28)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1247', NULL, 1218, N'', N'Angelo', N'', 29)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1248', NULL, 1218, N'', N'Anne', N'', 30)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1249', NULL, 1218, N'', N'Aphrodite', N'', 31)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1250', NULL, 1218, N'', N'Apollo', N'', 32)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1251', NULL, 1218, N'', N'Arctic', N'', 33)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1252', NULL, 1218, N'', N'Ares', N'', 34)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1253', NULL, 1218, N'', N'Ariel', N'', 35)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1254', NULL, 1218, N'', N'Arizona', N'', 36)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1255', NULL, 1218, N'', N'Arkansas', N'', 37)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1256', NULL, 1218, N'', N'Arrow', N'', 38)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1257', NULL, 1218, N'', N'Aruana', N'', 39)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1258', NULL, 1218, N'', N'Aspen', N'', 40)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1259', NULL, 1218, N'', N'Astute', N'', 41)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1260', NULL, 1218, N'', N'Atlanta', N'', 42)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1261', NULL, 1218, N'', N'Audrey', N'', 43)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1262', NULL, 1218, N'', N'Avon', N'', 44)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1263', NULL, 1218, N'', N'Barrow', N'', 45)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1264', NULL, 1218, N'', N'Bartok', N'', 46)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1265', NULL, 1218, N'', N'Bates', N'', 47)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1266', NULL, 1218, N'', N'Bear', N'', 48)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1267', NULL, 1218, N'', N'Beatrice', N'', 49)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1268', NULL, 1218, N'', N'Bellerophon', N'', 50)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1269', NULL, 1218, N'', N'Bernard', N'', 51)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1270', NULL, 1218, N'', N'Best', N'', 52)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1271', NULL, 1218, N'', N'Betano', N'', 53)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1272', NULL, 1218, N'', N'Black 1', N'', 54)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1273', NULL, 1218, N'', N'Black 12', N'', 55)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1274', NULL, 1218, N'', N'Black 3', N'', 56)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1275', NULL, 1218, N'', N'Black 4', N'', 57)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1276', NULL, 1218, N'', N'Black 7', N'', 58)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1277', NULL, 1218, N'', N'Black 8', N'', 59)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1278', NULL, 1218, N'', N'Black 9', N'', 60)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1279', NULL, 1218, N'', N'Black Iris', N'', 61)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1280', NULL, 1218, N'', N'Blackfriars', N'', 62)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1281', NULL, 1218, N'', N'Bladderwort', N'', 63)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1282', NULL, 1218, N'', N'Blue 2', N'', 64)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1283', NULL, 1218, N'', N'Blue 4', N'', 65)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1284', NULL, 1218, N'', N'Blythe', N'', 66)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1285', NULL, 1218, N'', N'Bobcat', N'', 67)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1286', NULL, 1218, N'', N'Bontekoe', N'', 68)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1287', NULL, 1218, N'', N'Boomerang', N'', 69)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1288', NULL, 1218, N'', N'Boyle', N'', 70)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1289', NULL, 1218, N'', N'Brent', N'', 71)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1290', NULL, 1218, N'', N'Bronze 3', N'', 72)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1291', NULL, 1218, N'', N'Bronze 4', N'', 73)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1292', NULL, 1218, N'', N'Brown 1', N'', 74)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1293', NULL, 1218, N'', N'Brown 4', N'', 75)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1294', NULL, 1218, N'', N'Brutus', N'', 76)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1295', NULL, 1218, N'', N'Bug', N'', 77)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1296', NULL, 1218, N'', N'Bull', N'', 78)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1297', NULL, 1218, N'', N'Bushey', N'', 79)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1298', NULL, 1218, N'', N'Buzzard', N'', 80)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1299', NULL, 1218, N'', N'Caesar', N'', 81)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1300', NULL, 1218, N'', N'Calder', N'', 82)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1301', NULL, 1218, N'', N'Camillo', N'', 83)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1302', NULL, 1218, N'', N'Capricorn', N'', 84)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1303', NULL, 1218, N'', N'Carousel', N'', 85)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1304', NULL, 1218, N'', N'Catcher', N'', 86)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1305', NULL, 1218, N'', N'Cavern', N'', 87)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1306', NULL, 1218, N'', N'Centaur', N'', 88)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1307', NULL, 1218, N'', N'Chalke', N'', 89)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1308', NULL, 1218, N'', N'Chalmers', N'', 90)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1309', NULL, 1218, N'', N'Charles', N'', 91)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1310', NULL, 1218, N'', N'Cherry', N'', 92)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1311', NULL, 1218, N'', N'Cherrypie', N'', 93)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1312', NULL, 1218, N'', N'Chestnut', N'', 94)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1313', NULL, 1218, N'', N'Claudius', N'', 95)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1314', NULL, 1218, N'', N'Clover', N'', 96)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1315', NULL, 1218, N'', N'Clyde', N'', 97)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1316', NULL, 1218, N'', N'Clyde Aviva', N'', 98)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1317', NULL, 1218, N'', N'Coldplay', N'', 99)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1318', NULL, 1218, N'', N'Cole', N'', 100)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1319', NULL, 1218, N'', N'Colne', N'', 101)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1320', NULL, 1218, N'', N'Coluden', N'', 102)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1321', NULL, 1218, N'', N'Contact', N'', 103)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1322', NULL, 1218, N'', N'Cordelia', N'', 104)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1323', NULL, 1218, N'', N'Cosmopolitan', N'', 105)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1324', NULL, 1218, N'', N'Cradle', N'', 106)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1325', NULL, 1218, N'', N'Crenshaw', N'', 107)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1326', NULL, 1218, N'', N'Cross Harbour', N'', 108)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1327', NULL, 1218, N'', N'Culloden', N'', 109)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1328', NULL, 1218, N'', N'Cupid', N'', 110)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1329', NULL, 1218, N'', N'Cyclops', N'', 111)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1330', NULL, 1218, N'', N'Dace', N'', 112)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1331', NULL, 1218, N'', N'Dahl', N'', 113)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1332', NULL, 1218, N'', N'Dahl 4', N'', 114)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1333', NULL, 1218, N'', N'Danno', N'', 115)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1334', NULL, 1218, N'', N'Danube', N'', 116)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1335', NULL, 1218, N'', N'Delaware', N'', 117)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1336', NULL, 1218, N'', N'Delight', N'', 118)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1337', NULL, 1218, N'', N'Demeter', N'', 119)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1338', NULL, 1218, N'', N'Deptford', N'', 120)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1339', NULL, 1218, N'', N'Derwent', N'', 121)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1340', NULL, 1218, N'', N'Diamond', N'', 122)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1341', NULL, 1218, N'', N'Dicentra', N'', 123)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1342', NULL, 1218, N'', N'Dido', N'', 124)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1343', NULL, 1218, N'', N'Dino', N'', 125)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1344', NULL, 1218, N'', N'Discus', N'', 126)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1345', NULL, 1218, N'', N'Dory', N'', 127)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1346', NULL, 1218, N'', N'Dove', N'', 128)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1347', NULL, 1218, N'', N'Duck', N'', 129)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1348', NULL, 1218, N'', N'Dulcimer', N'', 130)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1349', NULL, 1218, N'', N'Earth', N'', 131)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1350', NULL, 1218, N'', N'Echo', N'', 132)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1351', NULL, 1218, N'', N'Eclipse', N'', 133)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1352', NULL, 1218, N'', N'Edelweiss', N'', 134)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1353', NULL, 1218, N'', N'Edmund', N'', 135)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1354', NULL, 1218, N'', N'Edward', N'', 136)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1355', NULL, 1218, N'', N'Er', N'', 137)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1356', NULL, 1218, N'', N'Electra', N'', 138)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1357', NULL, 1218, N'', N'Elle', N'', 139)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1358', NULL, 1218, N'', N'Elm', N'', 140)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1359', NULL, 1218, N'', N'Emerald', N'', 141)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1360', NULL, 1218, N'', N'Emerald DLG', N'', 142)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1361', NULL, 1218, N'', N'Emilia', N'', 143)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1362', NULL, 1218, N'', N'Enfiler', N'', 144)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1363', NULL, 1218, N'', N'Enyo', N'', 145)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1364', NULL, 1218, N'', N'Eos', N'', 146)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1365', NULL, 1218, N'', N'Eros', N'', 147)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1366', NULL, 1218, N'', N'Eton', N'', 148)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1367', NULL, 1218, N'', N'Euphorbia', N'', 149)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1368', NULL, 1218, N'', N'Europa', N'', 150)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1369', NULL, 1218, N'', N'Excalibur', N'', 151)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1370', NULL, 1218, N'', N'Exhort', N'', 152)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1371', NULL, 1218, N'', N'Fabio', N'', 153)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1372', NULL, 1218, N'', N'Ferry', N'', 154)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1373', NULL, 1218, N'', N'Filmore', N'', 155)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1374', NULL, 1218, N'', N'Fire', N'', 156)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1375', NULL, 1218, N'', N'Fire Opal', N'', 157)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1376', NULL, 1218, N'', N'Floodgate', N'', 158)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1377', NULL, 1218, N'', N'Fly Fish', N'', 159)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1378', NULL, 1218, N'', N'Forde', N'', 160)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1379', NULL, 1218, N'', N'Fox', N'', 161)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1380', NULL, 1218, N'', N'Foxglove', N'', 162)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1381', NULL, 1218, N'', N'Foyle', N'', 163)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1382', NULL, 1218, N'', N'Francis', N'', 164)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1383', NULL, 1218, N'', N'Fuji', N'', 165)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1384', NULL, 1218, N'', N'Gala', N'', 166)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1385', NULL, 1218, N'', N'Galax', N'', 167)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1386', NULL, 1218, N'', N'Gallions Reach', N'', 168)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1387', NULL, 1218, N'', N'Garnet', N'', 169)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1388', NULL, 1218, N'', N'Gemmill', N'', 170)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1389', NULL, 1218, N'', N'Georgia', N'', 171)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1390', NULL, 1218, N'', N'Gerald', N'', 172)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1391', NULL, 1218, N'', N'Glenstorm', N'', 173)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1392', NULL, 1218, N'', N'lf', N'', 174)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1393', NULL, 1218, N'', N'pher', N'', 175)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1394', NULL, 1218, N'', N'urami', N'', 176)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1395', NULL, 1218, N'', N'Green 3', N'', 177)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1396', NULL, 1218, N'', N'Green 7', N'', 178)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1397', NULL, 1218, N'', N'Green 8', N'', 179)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1398', NULL, 1218, N'', N'Guppy', N'', 180)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1399', NULL, 1218, N'', N'Haberdasher', N'', 181)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1400', NULL, 1218, N'', N'Hades', N'', 182)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1401', NULL, 1218, N'', N'Hagfish', N'', 183)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1402', NULL, 1218, N'', N'Hamburg', N'', 184)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1403', NULL, 1218, N'', N'Hammond', N'', 185)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1404', NULL, 1218, N'', N'Hampermill', N'', 186)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1405', NULL, 1218, N'', N'Harpy', N'', 187)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1406', NULL, 1218, N'', N'Hatchet', N'', 188)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1407', NULL, 1218, N'', N'Hawaii', N'', 189)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1408', NULL, 1218, N'', N'Hebe', N'', 190)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1409', NULL, 1218, N'', N'Helen', N'', 191)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1410', NULL, 1218, N'', N'Helena', N'', 192)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1411', NULL, 1218, N'', N'Helios', N'', 193)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1412', NULL, 1218, N'', N'Helium', N'', 194)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1413', NULL, 1218, N'', N'Hera', N'', 195)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1414', NULL, 1218, N'', N'Hero', N'', 196)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1415', NULL, 1218, N'', N'Hogsmill', N'', 197)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1416', NULL, 1218, N'', N'Holly', N'', 198)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1417', NULL, 1218, N'', N'Holme', N'', 199)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1418', NULL, 1218, N'', N'Hope', N'', 200)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1419', NULL, 1218, N'', N'Hordeum', N'', 201)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1420', NULL, 1218, N'', N'Hound', N'', 202)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1421', NULL, 1218, N'', N'Housemartin', N'', 203)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1422', NULL, 1218, N'', N'Hudson', N'', 204)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1423', NULL, 1218, N'', N'Hydra', N'', 205)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1424', NULL, 1218, N'', N'Hydrogen', N'', 206)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1425', NULL, 1218, N'', N'Icarus', N'', 207)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1426', NULL, 1218, N'', N'Ilex', N'', 208)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1427', NULL, 1218, N'', N'Io', N'', 209)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1428', NULL, 1218, N'', N'Iowa', N'', 210)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1429', NULL, 1218, N'', N'Iris', N'', 211)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1430', NULL, 1218, N'', N'Ivory', N'', 212)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1431', NULL, 1218, N'', N'Jackson', N'', 213)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1432', NULL, 1218, N'', N'Jade', N'', 214)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1433', NULL, 1218, N'', N'Javelin', N'', 215)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1434', NULL, 1218, N'', N'Jersey', N'', 216)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1435', NULL, 1218, N'', N'Jet', N'', 217)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1436', NULL, 1218, N'', N'Julius', N'', 218)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1437', NULL, 1218, N'', N'Juniper', N'', 219)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1438', NULL, 1218, N'', N'Juno', N'', 220)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1439', NULL, 1218, N'', N'Jupiter', N'', 221)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1440', NULL, 1218, N'', N'Kentucky', N'', 222)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1441', NULL, 1218, N'', N'King', N'', 223)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1442', NULL, 1218, N'', N'Kipper', N'', 224)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1443', NULL, 1218, N'', N'Knightsbridge', N'', 225)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1444', NULL, 1218, N'', N'Kniphofia', N'', 226)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1445', NULL, 1218, N'', N'Koda', N'', 227)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1446', NULL, 1218, N'', N'Koi', N'', 228)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1447', NULL, 1218, N'', N'Laveratus', N'', 229)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1448', NULL, 1218, N'', N'Lavinia', N'', 230)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1449', NULL, 1218, N'', N'Leeds', N'', 231)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1450', NULL, 1218, N'', N'Lenny', N'', 232)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1451', NULL, 1218, N'', N'Leopard', N'', 233)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1452', NULL, 1218, N'', N'Lethe', N'', 234)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1453', NULL, 1218, N'', N'Lincoln', N'', 235)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1454', NULL, 1218, N'', N'Lithium', N'', 236)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1455', NULL, 1218, N'', N'Liverpool', N'', 237)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1456', NULL, 1218, N'', N'Loki', N'', 238)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1457', NULL, 1218, N'', N'Lollipop', N'', 239)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1458', NULL, 1218, N'', N'London', N'', 240)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1459', NULL, 1218, N'', N'Lorenzo', N'', 241)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1460', NULL, 1218, N'', N'Lupin', N'', 242)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1461', NULL, 1218, N'', N'Luther', N'', 243)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1462', NULL, 1218, N'', N'Luxembourg', N'', 244)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1463', NULL, 1218, N'', N'Macduff', N'', 245)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1464', NULL, 1218, N'', N'Mackerel', N'', 246)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1465', NULL, 1218, N'', N'Magnolia', N'', 247)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1466', NULL, 1218, N'', N'Mainwaring', N'', 248)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1467', NULL, 1218, N'', N'Mandarin', N'', 249)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1468', NULL, 1218, N'', N'Marcellus', N'', 250)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1469', NULL, 1218, N'', N'Mavros', N'', 251)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1470', NULL, 1218, N'', N'Michigan', N'', 252)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1471', NULL, 1218, N'', N'Midas', N'', 253)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1472', NULL, 1218, N'', N'Mint', N'', 254)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1473', NULL, 1218, N'', N'Mississippi', N'', 255)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1474', NULL, 1218, N'', N'Mole', N'', 256)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1475', NULL, 1218, N'', N'Monkfish', N'', 257)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1476', NULL, 1218, N'', N'Montague', N'', 258)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1477', NULL, 1218, N'', N'Montana', N'', 259)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1478', NULL, 1218, N'', N'Monty', N'', 260)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1479', NULL, 1218, N'', N'Moonstone', N'', 261)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1480', NULL, 1218, N'', N'Moonstone DLG', N'', 262)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1481', NULL, 1218, N'', N'Moselle', N'', 263)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1482', NULL, 1218, N'', N'Mullet', N'', 264)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1483', NULL, 1218, N'', N'Mustard', N'', 265)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1484', NULL, 1218, N'', N'Nacho', N'', 266)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1485', NULL, 1218, N'', N'Neasden', N'', 267)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1486', NULL, 1218, N'', N'Nemo', N'', 268)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1487', NULL, 1218, N'', N'Neptune', N'', 269)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1488', NULL, 1218, N'', N'Nero', N'', 270)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1489', NULL, 1218, N'', N'Nevada', N'', 271)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1490', NULL, 1218, N'', N'Nile', N'', 272)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1491', NULL, 1218, N'', N'Noire', N'', 273)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1492', NULL, 1218, N'', N'Nymphaea', N'', 274)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1493', NULL, 1218, N'', N'Ohio', N'', 275)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1494', NULL, 1218, N'', N'Oklahoma', N'', 276)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1495', NULL, 1218, N'', N'Omele', N'', 277)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1496', NULL, 1218, N'', N'Oracle', N'', 278)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1497', NULL, 1218, N'', N'Orange 1', N'', 279)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1498', NULL, 1218, N'', N'Orange 10', N'', 280)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1499', NULL, 1218, N'', N'Orange 11', N'', 281)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1500', NULL, 1218, N'', N'Orange 14', N'', 282)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1501', NULL, 1218, N'', N'Orange 20', N'', 283)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1502', NULL, 1218, N'', N'Orange 21', N'', 284)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1503', NULL, 1218, N'', N'Orange 22', N'', 285)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1504', NULL, 1218, N'', N'Orange 5', N'', 286)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1505', NULL, 1218, N'', N'Orange 6', N'', 287)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1506', NULL, 1218, N'', N'Orion', N'', 288)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1507', NULL, 1218, N'', N'Orlando', N'', 289)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1508', NULL, 1218, N'', N'Ouse', N'', 290)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1509', NULL, 1218, N'', N'Owl', N'', 291)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1510', NULL, 1218, N'', N'Oxhide', N'', 292)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1511', NULL, 1218, N'', N'Pan', N'', 293)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1512', NULL, 1218, N'', N'Panda', N'', 294)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1513', NULL, 1218, N'', N'Paris', N'', 295)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1514', NULL, 1218, N'', N'Pavelow', N'', 296)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1515', NULL, 1218, N'', N'Pearl', N'', 297)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1516', NULL, 1218, N'', N'Pegasus', N'', 298)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1517', NULL, 1218, N'', N'Pelican', N'', 299)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1518', NULL, 1218, N'', N'Penguin', N'', 300)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1519', NULL, 1218, N'', N'Percutor', N'', 301)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1520', NULL, 1218, N'', N'Percy', N'', 302)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1521', NULL, 1218, N'', N'Peridot', N'', 303)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1522', NULL, 1218, N'', N'Phobo', N'', 304)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1523', NULL, 1218, N'', N'Pike', N'', 305)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1524', NULL, 1218, N'', N'Pink 1', N'', 306)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1525', NULL, 1218, N'', N'Pink 6', N'', 307)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1526', NULL, 1218, N'', N'Piper', N'', 308)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1527', NULL, 1218, N'', N'Piranha', N'', 309)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1528', NULL, 1218, N'', N'Pison', N'', 310)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1529', NULL, 1218, N'', N'Pitstop', N'', 311)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1530', NULL, 1218, N'', N'Polaris', N'', 312)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1531', NULL, 1218, N'', N'Poppy', N'', 313)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1532', NULL, 1218, N'', N'Poseidon', N'', 314)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1533', NULL, 1218, N'', N'Posh', N'', 315)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1534', NULL, 1218, N'', N'Prague', N'', 316)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1535', NULL, 1218, N'', N'Price', N'', 317)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1536', NULL, 1218, N'', N'Proteus', N'', 318)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1537', NULL, 1218, N'', N'Puck', N'', 319)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1538', NULL, 1218, N'', N'Purple 10', N'', 320)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1539', NULL, 1218, N'', N'Purple 12', N'', 321)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1540', NULL, 1218, N'', N'Purple 4', N'', 322)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1541', NULL, 1218, N'', N'Purple 5', N'', 323)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1542', NULL, 1218, N'', N'Purple 7', N'', 324)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1543', NULL, 1218, N'', N'Purple 8', N'', 325)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1544', NULL, 1218, N'', N'Quena', N'', 326)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1545', NULL, 1218, N'', N'Quercus', N'', 327)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1546', NULL, 1218, N'', N'Rangpur', N'', 328)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1547', NULL, 1218, N'', N'Recycle', N'', 329)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1548', NULL, 1218, N'', N'Red 3', N'', 330)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1549', NULL, 1218, N'', N'Red 4', N'', 331)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1550', NULL, 1218, N'', N'Red 5', N'', 332)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1551', NULL, 1218, N'', N'Red 7', N'', 333)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1552', NULL, 1218, N'', N'Redwood', N'', 334)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1553', NULL, 1218, N'', N'Ribble', N'', 335)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1554', NULL, 1218, N'', N'Richmond', N'', 336)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1555', NULL, 1218, N'', N'Rickshaw', N'', 337)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1556', NULL, 1218, N'', N'Robin', N'', 338)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1557', NULL, 1218, N'', N'Rom', N'', 339)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1558', NULL, 1218, N'', N'Rosaline', N'', 340)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1559', NULL, 1218, N'', N'Roskhill', N'', 341)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1560', NULL, 1218, N'', N'Rother', N'', 342)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1561', NULL, 1218, N'', N'Roy', N'', 343)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1562', NULL, 1218, N'', N'Ruff', N'', 344)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1563', NULL, 1218, N'', N'Saint Louis', N'', 345)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1564', NULL, 1218, N'', N'Saisir', N'', 346)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1565', NULL, 1218, N'', N'Salmon', N'', 347)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1566', NULL, 1218, N'', N'Sapphire', N'', 348)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1567', NULL, 1218, N'', N'Saxifrage', N'', 349)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1568', NULL, 1218, N'', N'Scallop', N'', 350)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1569', NULL, 1218, N'', N'Schwarz', N'', 351)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1570', NULL, 1218, N'', N'Scorpa', N'', 352)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1571', NULL, 1218, N'', N'Sebastian', N'', 353)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1572', NULL, 1218, N'', N'Serval', N'', 354)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1573', NULL, 1218, N'', N'Shankley', N'', 355)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1574', NULL, 1218, N'', N'Shelob', N'', 356)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1575', NULL, 1218, N'', N'Sherwood', N'', 357)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1576', NULL, 1218, N'', N'Shoreditch', N'', 358)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1577', NULL, 1218, N'', N'Sim', N'', 359)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1578', NULL, 1218, N'', N'Skate', N'', 360)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1579', NULL, 1218, N'', N'Slough', N'', 361)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1580', NULL, 1218, N'', N'Smoothound', N'', 362)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1581', NULL, 1218, N'', N'Snake', N'', 363)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1582', NULL, 1218, N'', N'Snapper', N'', 364)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1583', NULL, 1218, N'', N'Sodonia', N'', 365)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1584', NULL, 1218, N'', N'Solanio', N'', 366)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1585', NULL, 1218, N'', N'Spear', N'', 367)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1586', NULL, 1218, N'', N'Spearhead', N'', 368)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1587', NULL, 1218, N'', N'Sphinx', N'', 369)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1588', NULL, 1218, N'', N'Spitfire', N'', 370)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1589', NULL, 1218, N'', N'Spongeweed', N'', 371)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1590', NULL, 1218, N'', N'Spring', N'', 372)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1591', NULL, 1218, N'', N'Squirrel', N'', 373)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1592', NULL, 1218, N'', N'St Helens', N'', 374)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1593', NULL, 1218, N'', N'Stanley', N'', 375)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1594', NULL, 1218, N'', N'Steel', N'', 376)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1595', NULL, 1218, N'', N'Stephano', N'', 377)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1596', NULL, 1218, N'', N'Stile', N'', 378)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1597', NULL, 1218, N'', N'Stoat', N'', 379)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1598', NULL, 1218, N'', N'Stockholm', N'', 380)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1599', NULL, 1218, N'', N'Stone', N'', 381)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1600', NULL, 1218, N'', N'Style', N'', 382)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1601', NULL, 1218, N'', N'Styx', N'', 383)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1602', NULL, 1218, N'', N'Sunflower', N'', 384)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1603', NULL, 1218, N'', N'Swan', N'', 385)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1604', NULL, 1218, N'', N'Swipe', N'', 386)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1605', NULL, 1218, N'', N'Tame', N'', 387)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1606', NULL, 1218, N'', N'Tarsus', N'', 388)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1607', NULL, 1218, N'', N'TBC', N'', 389)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1608', NULL, 1218, N'', N'Tench', N'', 390)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1609', NULL, 1218, N'', N'Tetley', N'', 391)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1610', NULL, 1218, N'', N'Texas', N'', 392)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1611', NULL, 1218, N'', N'Theseus', N'', 393)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1612', NULL, 1218, N'', N'Thistle', N'', 394)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1613', NULL, 1218, N'', N'Thresh', N'', 395)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1614', NULL, 1218, N'', N'Tiber', N'', 396)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1615', NULL, 1218, N'', N'Tierney', N'', 397)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1616', NULL, 1218, N'', N'Tiffey', N'', 398)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1617', NULL, 1218, N'', N'Tiger', N'', 399)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1618', NULL, 1218, N'', N'Tigers Eye', N'', 400)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1619', NULL, 1218, N'', N'Tinos', N'', 401)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1620', NULL, 1218, N'', N'Titan', N'', 402)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1621', NULL, 1218, N'', N'Titanium', N'', 403)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1622', NULL, 1218, N'', N'Tourmaline', N'', 404)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1623', NULL, 1218, N'', N'Trent', N'', 405)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1624', NULL, 1218, N'', N'Trident', N'', 406)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1625', NULL, 1218, N'', N'Trillium', N'', 407)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1626', NULL, 1218, N'', N'Triton', N'', 408)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1627', NULL, 1218, N'', N'Trout', N'', 409)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1628', NULL, 1218, N'', N'Troy', N'', 410)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1629', NULL, 1218, N'', N'Tuna', N'', 411)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1630', NULL, 1218, N'', N'Turbot', N'', 412)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1631', NULL, 1218, N'', N'Turquoise 1', N'', 413)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1632', NULL, 1218, N'', N'Turquoise 12', N'', 414)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1633', NULL, 1218, N'', N'Turquoise 13', N'', 415)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1634', NULL, 1218, N'', N'Turquoise 15', N'', 416)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1635', NULL, 1218, N'', N'Turquoise 17', N'', 417)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1636', NULL, 1218, N'', N'Tusk', N'', 418)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1637', NULL, 1218, N'', N'Tweed', N'', 419)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1638', NULL, 1218, N'', N'Tybalt', N'', 420)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1639', NULL, 1218, N'', N'Tyger', N'', 421)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1640', NULL, 1218, N'', N'Ukelin', N'', 422)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1641', NULL, 1218, N'', N'Under', N'', 423)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1642', NULL, 1218, N'', N'Unicorn', N'', 424)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1643', NULL, 1218, N'', N'Unique', N'', 425)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1644', NULL, 1218, N'', N'Uranium', N'', 426)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1645', NULL, 1218, N'', N'Uriel', N'', 427)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1646', NULL, 1218, N'', N'Ursula', N'', 428)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1647', NULL, 1218, N'', N'Usak', N'', 429)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1648', NULL, 1218, N'', N'Venus', N'', 430)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1649', NULL, 1218, N'', N'Viola', N'', 431)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1650', NULL, 1218, N'', N'Vorina', N'', 432)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1651', NULL, 1218, N'', N'Vulcan', N'', 433)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1652', NULL, 1218, N'', N'Wandle', N'', 434)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1653', NULL, 1218, N'', N'Warrior', N'', 435)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1654', NULL, 1218, N'', N'Wasat', N'', 436)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1655', NULL, 1218, N'', N'Washington', N'', 437)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1656', NULL, 1218, N'', N'Waterfall', N'', 438)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1657', NULL, 1218, N'', N'Wey', N'', 439)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1658', NULL, 1218, N'', N'White 3', N'', 440)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1659', NULL, 1218, N'', N'White 6', N'', 441)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1660', NULL, 1218, N'', N'Wilson', N'', 442)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1661', NULL, 1218, N'', N'Wind', N'', 443)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1662', NULL, 1218, N'', N'Wisdom', N'', 444)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1663', NULL, 1218, N'', N'Worth', N'', 445)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1664', NULL, 1218, N'', N'Xigia', N'', 446)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1665', NULL, 1218, N'', N'Yellow 1', N'', 447)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1666', NULL, 1218, N'', N'Yellow 10', N'', 448)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1667', NULL, 1218, N'', N'Yellow 11', N'', 449)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1668', NULL, 1218, N'', N'Yellow 12', N'', 450)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1669', NULL, 1218, N'', N'Yellow 13', N'', 451)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1670', NULL, 1218, N'', N'Yellow 2', N'', 452)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1671', NULL, 1218, N'', N'Yellow 3', N'', 453)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1672', NULL, 1218, N'', N'Yellow 4', N'', 454)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1673', NULL, 1218, N'', N'Yellow 7', N'', 455)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1674', NULL, 1218, N'', N'Yellow 8', N'', 456)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1675', NULL, 1218, N'', N'Zagreus', N'', 457)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1676', NULL, 1218, N'', N'Zambezi', N'', 458)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1677', NULL, 1218, N'', N'Zander', N'', 459)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1678', NULL, 1218, N'', N'Zircon', N'', 460)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1682', NULL, 1681, N'', N'Open', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1683', NULL, 1681, N'', N'Closed', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1700', NULL, 1699, N'', N'RTC', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1701', NULL, 1699, N'', N'Commercial', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1702', NULL, 1699, N'', N'Employment', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1703', NULL, 1699, N'', N'Public Liability', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1704', NULL, 1699, N'', N'Travel', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1705', NULL, 1699, N'', N'Home Contents', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1706', NULL, 1699, N'', N'Home Building', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1707', NULL, 1699, N'', N'Keoghs CFS', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1708', NULL, 1699, N'', N'Keoghs CMS', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1709', NULL, 1699, N'', N'Keoghs TCS', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1710', NULL, 1699, N'', N'Theft', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1711', NULL, 1699, N'', N'Pet', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1712', NULL, 1699, N'', N'Mobile Phone', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1714', NULL, 1713, N'', N'Incident', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1715', NULL, 1713, N'', N'Claim', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1723', NULL, 1722, N'', N'Commercial', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1724', NULL, 1722, N'', N'Employer''s Liability', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1725', NULL, 1722, N'', N'Financial', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1726', NULL, 1722, N'', N'Holiday', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1727', NULL, 1722, N'', N'Home', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1728', NULL, 1722, N'', N'Public Liability', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1729', NULL, 1722, N'', N'RTA - Bogus', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1730', NULL, 1722, N'', N'RTA - Credit Hire', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1731', NULL, 1722, N'', N'RTA - Exaggerated Loss', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1732', NULL, 1722, N'', N'RTA - Fire/Theft', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1733', NULL, 1722, N'', N'RTA - Induced Collision', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1734', NULL, 1722, N'', N'RTA - Intel + Report', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1735', NULL, 1722, N'', N'RTA - Low Speed Impact', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1736', NULL, 1722, N'', N'RTA - Staged/Contrived', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1738', NULL, 1722, N'', N'N/A', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1762', NULL, 1761, N'', N'Bolton', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1763', NULL, 1761, N'', N'Coventry', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1764', NULL, 1761, N'', N'Manchester', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1768', NULL, 1767, N'', N'1st Central', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1769', NULL, 1767, N'', N'ABC Insurance', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1770', NULL, 1767, N'', N'Acromas', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1771', NULL, 1767, N'', N'Admiral', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1772', NULL, 1767, N'', N'Admiral - Diamond', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1773', NULL, 1767, N'', N'Ageas', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1774', NULL, 1767, N'', N'AGF', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1775', NULL, 1767, N'', N'Allianz Cornhill', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1776', NULL, 1767, N'', N'American Re', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1777', NULL, 1767, N'', N'Amlin Insurance', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1778', NULL, 1767, N'', N'Aviva', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1779', NULL, 1767, N'', N'AXA', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1780', NULL, 1767, N'', N'Brit Insurance', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1781', NULL, 1767, N'', N'Broadspire', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1782', NULL, 1767, N'', N'Broker Direct', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1783', NULL, 1767, N'', N'Budget', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1784', NULL, 1767, N'', N'Capita', N'', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1785', NULL, 1767, N'', N'CGU', N'', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1786', NULL, 1767, N'', N'Chartis', N'', 19)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1787', NULL, 1767, N'', N'Chaucer Insurance', N'', 20)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1788', NULL, 1767, N'', N'CMGL', N'', 21)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1789', NULL, 1767, N'', N'Collingwood Claims', N'', 22)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1790', NULL, 1767, N'', N'Co-operative Insurance', N'', 23)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1791', NULL, 1767, N'', N'Cornhill', N'', 24)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1792', NULL, 1767, N'', N'Crowe', N'', 25)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1793', NULL, 1767, N'', N'DLG - (CUE only)', N'', 26)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1794', NULL, 1767, N'', N'DLG - Barclays', N'', 27)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1795', NULL, 1767, N'', N'DLG - BMW Insurance', N'', 28)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1796', NULL, 1767, N'', N'DLG - Churchill', N'', 29)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1797', NULL, 1767, N'', N'DLG - Citroen', N'', 30)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1798', NULL, 1767, N'', N'DLG - Direct Line', N'', 31)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1799', NULL, 1767, N'', N'DLG - Egg', N'', 32)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1800', NULL, 1767, N'', N'DLG - Halifax', N'', 33)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1801', NULL, 1767, N'', N'DLG - Lloyds TSB', N'', 34)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1802', NULL, 1767, N'', N'DLG - Natwest', N'', 35)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1803', NULL, 1767, N'', N'DLG - NIG', N'', 36)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1804', NULL, 1767, N'', N'DLG - Nissan', N'', 37)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1805', NULL, 1767, N'', N'DLG - Pearl Assurance', N'', 38)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1806', NULL, 1767, N'', N'DLG - Peugeot', N'', 39)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1807', NULL, 1767, N'', N'DLG - Privilege', N'', 40)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1808', NULL, 1767, N'', N'DLG - Prudential', N'', 41)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1809', NULL, 1767, N'', N'DLG - Tesco', N'', 42)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1810', NULL, 1767, N'', N'DLG - UKI', N'', 43)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1811', NULL, 1767, N'', N'DLG - Virgin Insurance', N'', 44)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1812', NULL, 1767, N'', N'Ecclesiastical', N'', 45)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1813', NULL, 1767, N'', N'Eldon', N'', 46)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1814', NULL, 1767, N'', N'Elephant', N'', 47)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1815', NULL, 1767, N'', N'Endsleigh', N'', 48)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1816', NULL, 1767, N'', N'Ensign Motor Policies', N'', 49)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1817', NULL, 1767, N'', N'Equity Red Star', N'', 50)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1818', NULL, 1767, N'', N'Esure', N'', 51)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1819', NULL, 1767, N'', N'Esure - Sainsbury', N'', 52)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1820', NULL, 1767, N'', N'First Group', N'', 53)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1821', NULL, 1767, N'', N'Ford Insure', N'', 54)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1822', NULL, 1767, N'', N'Fortis', N'', 55)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1823', NULL, 1767, N'', N'GHL Insurance Services Ltd', N'', 56)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1824', NULL, 1767, N'', N'Green Flag', N'', 57)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1825', NULL, 1767, N'', N'Groupama', N'', 58)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1826', NULL, 1767, N'', N'Hamilton Insurance', N'', 59)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1827', NULL, 1767, N'', N'Hastings Direct', N'', 60)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1828', NULL, 1767, N'', N'Haven Insurance', N'', 61)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1829', NULL, 1767, N'', N'Hertz Claims Management', N'', 62)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1830', NULL, 1767, N'', N'Highway Motor Policies', N'', 63)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1831', NULL, 1767, N'', N'Hill House Hammond', N'', 64)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1832', NULL, 1767, N'', N'HSBC', N'', 65)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1833', NULL, 1767, N'', N'Inceptum Insurance', N'', 66)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1834', NULL, 1767, N'', N'Inter Europe AG', N'', 67)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1835', NULL, 1767, N'', N'Interiura', N'', 68)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1836', NULL, 1767, N'', N'John Menzies Plc', N'', 69)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1837', NULL, 1767, N'', N'Jubilee Insurance', N'', 70)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1838', NULL, 1767, N'', N'KGM Insurance', N'', 71)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1839', NULL, 1767, N'', N'Legal & General', N'', 72)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1840', NULL, 1767, N'', N'Liberty Syndicates', N'', 73)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1841', NULL, 1767, N'', N'Link', N'', 74)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1842', NULL, 1767, N'', N'Liverpool Victoria', N'', 75)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1843', NULL, 1767, N'', N'Markerstudy', N'', 76)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1844', NULL, 1767, N'', N'MarketBalance', N'', 77)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1845', NULL, 1767, N'', N'Marsh Ltd', N'', 78)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1846', NULL, 1767, N'', N'McLarens Topliss', N'', 79)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1847', NULL, 1767, N'', N'MIB', N'', 80)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1848', NULL, 1767, N'', N'Millenium Insurance', N'', 81)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1849', NULL, 1767, N'', N'MMA', N'', 82)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1850', NULL, 1767, N'', N'NFU Mutual', N'', 83)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1851', NULL, 1767, N'', N'Nobilas UK', N'', 84)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1852', NULL, 1767, N'', N'Premier Insurance', N'', 85)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1853', NULL, 1767, N'', N'Prospect Legal Ltd', N'', 86)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1854', NULL, 1767, N'', N'Provident', N'', 87)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1855', NULL, 1767, N'', N'QBE Insurance', N'', 88)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1856', NULL, 1767, N'', N'Quinn Insurance', N'', 89)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1857', NULL, 1767, N'', N'Royal Sun Alliance', N'', 90)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1858', NULL, 1767, N'', N'Ryder Plc', N'', 91)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1859', NULL, 1767, N'', N'Sabre', N'', 92)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1860', NULL, 1767, N'', N'Service Claims', N'', 93)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1861', NULL, 1767, N'', N'Southern Rock', N'', 94)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1862', NULL, 1767, N'', N'Summit', N'', 95)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1863', NULL, 1767, N'', N'Swiftcover', N'', 96)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1864', NULL, 1767, N'', N'Tesco Underwriting Ltd', N'', 97)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1865', NULL, 1767, N'', N'Tradewise Insurance', N'', 98)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1866', NULL, 1767, N'', N'Tradex', N'', 99)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1867', NULL, 1767, N'', N'Transcare Solutions', N'', 100)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1868', NULL, 1767, N'', N'Transportation Claims Ltd', N'', 101)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1869', NULL, 1767, N'', N'Travelers Insurance', N'', 102)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1870', NULL, 1767, N'', N'Wellington', N'', 103)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1871', NULL, 1767, N'', N'Westminster', N'', 104)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1872', NULL, 1767, N'', N'Zenith', N'', 105)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1873', NULL, 1767, N'', N'Zurich', N'', 106)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1878', NULL, 1877, N'', N'Open', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1879', NULL, 1877, N'', N'Closed', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1905', NULL, 1904, N'', N'Accident or Claims Management', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1906', NULL, 1904, N'', N'Credit Hire', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1907', NULL, 1904, N'', N'Solicitor', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1908', NULL, 1904, N'', N'Vehicle Engineer', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1909', NULL, 1904, N'', N'Recovery and / or Storage', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1910', NULL, 1904, N'', N'Broker', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1911', NULL, 1904, N'', N'Insurer', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1912', NULL, 1904, N'', N'Self Insured', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1913', NULL, 1904, N'', N'Medical Examiner', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1916', NULL, 1915, N'', N'Office', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1917', NULL, 1915, N'', N'Garage', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1918', NULL, 1915, N'', N'Hospital', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1919', NULL, 1915, N'', N'Warehouse', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1923', NULL, 1922, N'', N'Active', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1924', NULL, 1922, N'', N'Dissolved', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1925', NULL, 1922, N'', N'In Liquidation', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1926', NULL, 1922, N'', N'Receivership Action', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1935', NULL, 1934, N'', N'Authorised', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1936', NULL, 1934, N'', N'Suspended', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1937', NULL, 1934, N'', N'Cancelled', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1966', NULL, 1965, N'', N'Credit Card', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1967', NULL, 1965, N'', N'Debit Card', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1980', NULL, 1979, N'', N'Mr', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1981', NULL, 1979, N'', N'Mrs', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1982', NULL, 1979, N'', N'Ms', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1983', NULL, 1979, N'', N'Miss', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1984', NULL, 1979, N'', N'Master', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1985', NULL, 1979, N'', N'Dr', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1986', NULL, 1979, N'', N'Professor', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1987', NULL, 1979, N'', N'Reverend', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1988', NULL, 1979, N'', N'Sir', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1989', NULL, 1979, N'', N'Lord', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1990', NULL, 1979, N'', N'Baroness', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1991', NULL, 1979, N'', N'Right Honourable', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1992', NULL, 1979, N'', N'Unknown', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'1999', NULL, 1998, N'', N'Male', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2000', NULL, 1998, N'', N'Female', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2001', NULL, 1998, N'', N'Unknown', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2004', NULL, 2003, N'', N'Person', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2005', NULL, 2003, N'', N'Female', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2006', NULL, 2003, N'', N'Other name', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2007', NULL, 2003, N'', N'Anonymous', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2015', NULL, 2014, N'', N'Afghanistan', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2016', NULL, 2014, N'', N'Albania', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2017', NULL, 2014, N'', N'Algeria', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2018', NULL, 2014, N'', N'Andorra', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2019', NULL, 2014, N'', N'Anla', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2020', NULL, 2014, N'', N'Anguilla', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2021', NULL, 2014, N'', N'Antigua', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2022', NULL, 2014, N'', N'Antilles', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2023', NULL, 2014, N'', N'Argentina', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2024', NULL, 2014, N'', N'Armenia', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2025', NULL, 2014, N'', N'Aruba', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2026', NULL, 2014, N'', N'Ascension Island', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2027', NULL, 2014, N'', N'Australia', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2028', NULL, 2014, N'', N'Austria', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2029', NULL, 2014, N'', N'Azerbaijan', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2030', NULL, 2014, N'', N'Azores', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2031', NULL, 2014, N'', N'Bahamas', N'', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2032', NULL, 2014, N'', N'Bahrain', N'', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2033', NULL, 2014, N'', N'Balarus', N'', 19)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2034', NULL, 2014, N'', N'Bangladesh', N'', 20)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2035', NULL, 2014, N'', N'Barbados', N'', 21)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2036', NULL, 2014, N'', N'Belgium', N'', 22)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2037', NULL, 2014, N'', N'Belize', N'', 23)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2038', NULL, 2014, N'', N'Benin', N'', 24)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2039', NULL, 2014, N'', N'Bermuda', N'', 25)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2040', NULL, 2014, N'', N'Bhutan', N'', 26)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2041', NULL, 2014, N'', N'Bolivia', N'', 27)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2042', NULL, 2014, N'', N'Bosnia-Hercevina', N'', 28)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2043', NULL, 2014, N'', N'Botswana', N'', 29)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2044', NULL, 2014, N'', N'Brazil', N'', 30)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2045', NULL, 2014, N'', N'Brunei', N'', 31)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2046', NULL, 2014, N'', N'Bulgaria', N'', 32)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2047', NULL, 2014, N'', N'Burkina Faso', N'', 33)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2048', NULL, 2014, N'', N'Burundi', N'', 34)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2049', NULL, 2014, N'', N'Cambodia', N'', 35)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2050', NULL, 2014, N'', N'Cameroon', N'', 36)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2051', NULL, 2014, N'', N'Canada', N'', 37)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2052', NULL, 2014, N'', N'Canary Islands', N'', 38)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2053', NULL, 2014, N'', N'Cape Verde Islands', N'', 39)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2054', NULL, 2014, N'', N'Cayman Islands', N'', 40)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2055', NULL, 2014, N'', N'Central African Republic', N'', 41)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2056', NULL, 2014, N'', N'Chad', N'', 42)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2057', NULL, 2014, N'', N'Chile', N'', 43)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2058', NULL, 2014, N'', N'China', N'', 44)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2059', NULL, 2014, N'', N'Christmas Island', N'', 45)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2060', NULL, 2014, N'', N'Cocos Island', N'', 46)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2061', NULL, 2014, N'', N'Colombia', N'', 47)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2062', NULL, 2014, N'', N'Comoros', N'', 48)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2063', NULL, 2014, N'', N'Con', N'', 49)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2064', NULL, 2014, N'', N'Cook Islands', N'', 50)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2065', NULL, 2014, N'', N'Costa Rica', N'', 51)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2066', NULL, 2014, N'', N'Cote d''Ivoire', N'', 52)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2067', NULL, 2014, N'', N'Croatia', N'', 53)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2068', NULL, 2014, N'', N'Cuba', N'', 54)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2069', NULL, 2014, N'', N'Cyprus', N'', 55)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2070', NULL, 2014, N'', N'Czech Republic', N'', 56)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2071', NULL, 2014, N'', N'Denmark', N'', 57)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2072', NULL, 2014, N'', N'Djbouti', N'', 58)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2073', NULL, 2014, N'', N'Dominica', N'', 59)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2074', NULL, 2014, N'', N'Ecuador', N'', 60)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2075', NULL, 2014, N'', N'Egypt', N'', 61)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2076', NULL, 2014, N'', N'El Salvador', N'', 62)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2077', NULL, 2014, N'', N'England', N'', 63)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2078', NULL, 2014, N'', N'Equatorial Guinea', N'', 64)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2079', NULL, 2014, N'', N'Eritrea', N'', 65)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2080', NULL, 2014, N'', N'Estonia', N'', 66)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2081', NULL, 2014, N'', N'Ethiopia', N'', 67)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2082', NULL, 2014, N'', N'Falkland Islands', N'', 68)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2083', NULL, 2014, N'', N'Faroe Islands', N'', 69)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2084', NULL, 2014, N'', N'Fiji', N'', 70)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2085', NULL, 2014, N'', N'Finland', N'', 71)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2086', NULL, 2014, N'', N'France', N'', 72)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2087', NULL, 2014, N'', N'French Guiana', N'', 73)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2088', NULL, 2014, N'', N'French Polynesia', N'', 74)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2089', NULL, 2014, N'', N'Gabon', N'', 75)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2090', NULL, 2014, N'', N'Gambia', N'', 76)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2091', NULL, 2014, N'', N'Georgia', N'', 77)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2092', NULL, 2014, N'', N'Germany', N'', 78)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2093', NULL, 2014, N'', N'Ghana', N'', 79)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2094', NULL, 2014, N'', N'Gibraltar', N'', 80)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2095', NULL, 2014, N'', N'Great Britain', N'', 81)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2096', NULL, 2014, N'', N'Greece', N'', 82)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2097', NULL, 2014, N'', N'Greenland', N'', 83)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2098', NULL, 2014, N'', N'Grenada', N'', 84)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2099', NULL, 2014, N'', N'Guadeloupe', N'', 85)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2100', NULL, 2014, N'', N'Guam', N'', 86)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2101', NULL, 2014, N'', N'Guatemala', N'', 87)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2102', NULL, 2014, N'', N'Guinea', N'', 88)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2103', NULL, 2014, N'', N'Guyana', N'', 89)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2104', NULL, 2014, N'', N'Haiti', N'', 90)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2105', NULL, 2014, N'', N'Honduras', N'', 91)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2106', NULL, 2014, N'', N'Hong Kong', N'', 92)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2107', NULL, 2014, N'', N'Hungary', N'', 93)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2108', NULL, 2014, N'', N'Ibiza', N'', 94)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2109', NULL, 2014, N'', N'Iceland', N'', 95)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2110', NULL, 2014, N'', N'India', N'', 96)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2111', NULL, 2014, N'', N'Indonesia', N'', 97)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2112', NULL, 2014, N'', N'Inmarsat', N'', 98)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2113', NULL, 2014, N'', N'Iran', N'', 99)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2114', NULL, 2014, N'', N'Iraq', N'', 100)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2115', NULL, 2014, N'', N'Ireland, Republic of', N'', 101)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2116', NULL, 2014, N'', N'Israel', N'', 102)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2117', NULL, 2014, N'', N'Italy', N'', 103)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2118', NULL, 2014, N'', N'Ivory Coast', N'', 104)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2119', NULL, 2014, N'', N'Jamaica', N'', 105)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2120', NULL, 2014, N'', N'Japan', N'', 106)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2121', NULL, 2014, N'', N'Jordan', N'', 107)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2122', NULL, 2014, N'', N'Kazakhstan', N'', 108)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2123', NULL, 2014, N'', N'Kenya', N'', 109)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2124', NULL, 2014, N'', N'Kirghizstan', N'', 110)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2125', NULL, 2014, N'', N'Kiribati', N'', 111)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2126', NULL, 2014, N'', N'Korea (North)', N'', 112)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2127', NULL, 2014, N'', N'Korea (South)', N'', 113)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2128', NULL, 2014, N'', N'Kosovo', N'', 114)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2129', NULL, 2014, N'', N'Kurdistan', N'', 115)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2130', NULL, 2014, N'', N'Kuwait', N'', 116)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2131', NULL, 2014, N'', N'Laos', N'', 117)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2132', NULL, 2014, N'', N'Latvia', N'', 118)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2133', NULL, 2014, N'', N'Lebanon', N'', 119)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2134', NULL, 2014, N'', N'Lesotho', N'', 120)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2135', NULL, 2014, N'', N'Liberia', N'', 121)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2136', NULL, 2014, N'', N'Libya', N'', 122)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2137', NULL, 2014, N'', N'Liechtenstein', N'', 123)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2138', NULL, 2014, N'', N'Lithuania', N'', 124)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2139', NULL, 2014, N'', N'Luxembourg', N'', 125)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2140', NULL, 2014, N'', N'Macao', N'', 126)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2141', NULL, 2014, N'', N'Macedonia', N'', 127)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2142', NULL, 2014, N'', N'Madagascar', N'', 128)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2143', NULL, 2014, N'', N'Madeira', N'', 129)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2144', NULL, 2014, N'', N'Malawi', N'', 130)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2145', NULL, 2014, N'', N'Malaysia', N'', 131)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2146', NULL, 2014, N'', N'Maldives', N'', 132)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2147', NULL, 2014, N'', N'Mali', N'', 133)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2148', NULL, 2014, N'', N'Malta', N'', 134)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2149', NULL, 2014, N'', N'Marshall Islands', N'', 135)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2150', NULL, 2014, N'', N'Martinique', N'', 136)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2151', NULL, 2014, N'', N'Mauritania', N'', 137)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2152', NULL, 2014, N'', N'Mauritius', N'', 138)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2153', NULL, 2014, N'', N'Mayotte', N'', 139)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2154', NULL, 2014, N'', N'Mexico', N'', 140)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2155', NULL, 2014, N'', N'Micronesia', N'', 141)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2156', NULL, 2014, N'', N'Midway Island', N'', 142)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2157', NULL, 2014, N'', N'Minorca', N'', 143)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2158', NULL, 2014, N'', N'Moldova', N'', 144)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2159', NULL, 2014, N'', N'Monaco', N'', 145)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2160', NULL, 2014, N'', N'Monlia', N'', 146)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2161', NULL, 2014, N'', N'Montserrat', N'', 147)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2162', NULL, 2014, N'', N'Morocco', N'', 148)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2163', NULL, 2014, N'', N'Mozambique', N'', 149)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2164', NULL, 2014, N'', N'Myanmar', N'', 150)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2165', NULL, 2014, N'', N'Namibia', N'', 151)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2166', NULL, 2014, N'', N'Nauru', N'', 152)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2167', NULL, 2014, N'', N'Nepal', N'', 153)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2168', NULL, 2014, N'', N'Netherlands', N'', 154)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2169', NULL, 2014, N'', N'New Caledonia', N'', 155)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2170', NULL, 2014, N'', N'New Zealand', N'', 156)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2171', NULL, 2014, N'', N'Nicaragua', N'', 157)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2172', NULL, 2014, N'', N'Niger', N'', 158)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2173', NULL, 2014, N'', N'Nigeria', N'', 159)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2174', NULL, 2014, N'', N'Niue', N'', 160)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2175', NULL, 2014, N'', N'Norfolk Island', N'', 161)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2176', NULL, 2014, N'', N'Northern Ireland', N'', 162)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2177', NULL, 2014, N'', N'Northern Marianas', N'', 163)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2178', NULL, 2014, N'', N'Norway', N'', 164)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2179', NULL, 2014, N'', N'Oman', N'', 165)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2180', NULL, 2014, N'', N'Pakistan', N'', 166)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2181', NULL, 2014, N'', N'Palau', N'', 167)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2182', NULL, 2014, N'', N'Panama', N'', 168)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2183', NULL, 2014, N'', N'Papua New Guinea', N'', 169)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2184', NULL, 2014, N'', N'Paraguay', N'', 170)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2185', NULL, 2014, N'', N'Peru', N'', 171)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2186', NULL, 2014, N'', N'Philippines', N'', 172)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2187', NULL, 2014, N'', N'Pitcairn Island', N'', 173)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2188', NULL, 2014, N'', N'Poland', N'', 174)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2189', NULL, 2014, N'', N'Portugal', N'', 175)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2190', NULL, 2014, N'', N'Puerto Rico', N'', 176)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2191', NULL, 2014, N'', N'Qatar', N'', 177)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2192', NULL, 2014, N'', N'Romania', N'', 178)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2193', NULL, 2014, N'', N'Russia', N'', 179)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2194', NULL, 2014, N'', N'Rwanda', N'', 180)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2195', NULL, 2014, N'', N'Samoa', N'', 181)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2196', NULL, 2014, N'', N'San Marino', N'', 182)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2197', NULL, 2014, N'', N'Sao Tome & Principe', N'', 183)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2198', NULL, 2014, N'', N'Saudi Arabia', N'', 184)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2199', NULL, 2014, N'', N'Scotland', N'', 185)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2200', NULL, 2014, N'', N'Senegal', N'', 186)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2201', NULL, 2014, N'', N'Seychelles', N'', 187)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2202', NULL, 2014, N'', N'Sierra Leone', N'', 188)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2203', NULL, 2014, N'', N'Singapore', N'', 189)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2204', NULL, 2014, N'', N'Slovakia', N'', 190)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2205', NULL, 2014, N'', N'Slovenia', N'', 191)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2206', NULL, 2014, N'', N'Solomon Islands', N'', 192)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2207', NULL, 2014, N'', N'Somalia', N'', 193)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2208', NULL, 2014, N'', N'South Africa', N'', 194)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2209', NULL, 2014, N'', N'Spain', N'', 195)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2210', NULL, 2014, N'', N'Sri Lanka', N'', 196)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2211', NULL, 2014, N'', N'St. Helena', N'', 197)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2212', NULL, 2014, N'', N'St. Kitts & Nevis', N'', 198)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2213', NULL, 2014, N'', N'St. Lucia', N'', 199)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2214', NULL, 2014, N'', N'St. Pierre & Miquelon', N'', 200)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2215', NULL, 2014, N'', N'St. Vincent & Grenadines', N'', 201)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2216', NULL, 2014, N'', N'Sudan', N'', 202)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2217', NULL, 2014, N'', N'Suriname', N'', 203)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2218', NULL, 2014, N'', N'Swaziland', N'', 204)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2219', NULL, 2014, N'', N'Sweden', N'', 205)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2220', NULL, 2014, N'', N'Switzerland', N'', 206)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2221', NULL, 2014, N'', N'Syria', N'', 207)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2222', NULL, 2014, N'', N'Tadzhikistan', N'', 208)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2223', NULL, 2014, N'', N'Taiwan', N'', 209)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2224', NULL, 2014, N'', N'Tanzania', N'', 210)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2225', NULL, 2014, N'', N'Thailand', N'', 211)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2226', NULL, 2014, N'', N'To', N'', 212)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2227', NULL, 2014, N'', N'Tonga', N'', 213)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2228', NULL, 2014, N'', N'Trinidad & Toba', N'', 214)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2229', NULL, 2014, N'', N'Tristan Da Cunha', N'', 215)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2230', NULL, 2014, N'', N'Tunisia', N'', 216)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2231', NULL, 2014, N'', N'Turkey', N'', 217)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2232', NULL, 2014, N'', N'Turkmenistan', N'', 218)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2233', NULL, 2014, N'', N'Turks & Caicos Islands', N'', 219)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2234', NULL, 2014, N'', N'Tuvalu', N'', 220)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2235', NULL, 2014, N'', N'Uganda', N'', 221)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2236', NULL, 2014, N'', N'Ukraine', N'', 222)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2237', NULL, 2014, N'', N'United Arab Emirates', N'', 223)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2238', NULL, 2014, N'', N'Uruguay', N'', 224)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2239', NULL, 2014, N'', N'USA', N'', 225)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2240', NULL, 2014, N'', N'Uzbekistan', N'', 226)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2241', NULL, 2014, N'', N'Vanuatu', N'', 227)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2242', NULL, 2014, N'', N'Venezuela', N'', 228)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2243', NULL, 2014, N'', N'Vietnam', N'', 229)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2244', NULL, 2014, N'', N'Virgin Islands', N'', 230)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2245', NULL, 2014, N'', N'Wake Island', N'', 231)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2246', NULL, 2014, N'', N'Wales', N'', 232)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2247', NULL, 2014, N'', N'Wallis & Futuna', N'', 233)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2248', NULL, 2014, N'', N'Yemen', N'', 234)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2249', NULL, 2014, N'', N'Yuslavia', N'', 235)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2250', NULL, 2014, N'', N'Zaire', N'', 236)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2251', NULL, 2014, N'', N'Zambia', N'', 237)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2252', NULL, 2014, N'', N'Zimbabwe', N'', 238)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2276', NULL, 2275, N'', N'Personal Motor', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2277', NULL, 2275, N'', N'Commercial Motor', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2278', NULL, 2275, N'', N'Home', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2279', NULL, 2275, N'', N'Travel', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2280', NULL, 2275, N'', N'Employers Liability', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2283', NULL, 2275, N'', N'Unknown', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2285', NULL, 2284, N'', N'Comprehensive', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2286', NULL, 2284, N'', N'Third Party, Fire & Theft', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2287', NULL, 2284, N'', N'Third Party Only', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2288', NULL, 2284, N'', N'Unknown', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2293', NULL, 2275, N'', N'Commercial', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2294', NULL, 2275, N'', N'Health', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2295', NULL, 2275, N'', N'Payment Protection', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2312', NULL, 2311, N'', N'Landline', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2313', NULL, 2311, N'', N'Mobile', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2314', NULL, 2311, N'', N'FAX', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2315', NULL, 2311, N'', N'Unknown', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2318', NULL, 2317, N'', N'Landline', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2319', NULL, 2317, N'', N'Fax', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2334', NULL, 2333, N'', N'Alfa Romeo', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2335', NULL, 2333, N'', N'Aston Martin', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2336', NULL, 2333, N'', N'Audi', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2337', NULL, 2333, N'', N'Austin', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2338', NULL, 2333, N'', N'Bentley', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2339', NULL, 2333, N'', N'BMW', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2340', NULL, 2333, N'', N'Cadillac', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2341', NULL, 2333, N'', N'Carbodies', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2342', NULL, 2333, N'', N'Chevrolet', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2343', NULL, 2333, N'', N'Chrysler', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2344', NULL, 2333, N'', N'Citroen', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2345', NULL, 2333, N'', N'Daewoo', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2346', NULL, 2333, N'', N'DAF', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2347', NULL, 2333, N'', N'Daihatsu', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2348', NULL, 2333, N'', N'Daimler', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2349', NULL, 2333, N'', N'Dennis', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2350', NULL, 2333, N'', N'Dinli', N'', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2351', NULL, 2333, N'', N'Dodge', N'', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2352', NULL, 2333, N'', N'ERF', N'', 19)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2353', NULL, 2333, N'', N'Ferrari', N'', 20)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2354', NULL, 2333, N'', N'Fiat', N'', 21)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2355', NULL, 2333, N'', N'Ford', N'', 22)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2356', NULL, 2333, N'', N'Grand Jeep', N'', 23)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2357', NULL, 2333, N'', N'Honda', N'', 24)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2358', NULL, 2333, N'', N'Hummer', N'', 25)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2359', NULL, 2333, N'', N'Hyundai', N'', 26)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2360', NULL, 2333, N'', N'Isuzu', N'', 27)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2361', NULL, 2333, N'', N'Iveco', N'', 28)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2362', NULL, 2333, N'', N'Jaguar', N'', 29)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2363', NULL, 2333, N'', N'Jeep', N'', 30)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2364', NULL, 2333, N'', N'Kia', N'', 31)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2365', NULL, 2333, N'', N'Lamborghini', N'', 32)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2366', NULL, 2333, N'', N'Lancia', N'', 33)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2367', NULL, 2333, N'', N'Land Rover', N'', 34)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2368', NULL, 2333, N'', N'Landcruiser', N'', 35)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2369', NULL, 2333, N'', N'LDV', N'', 36)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2370', NULL, 2333, N'', N'Lexus', N'', 37)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2371', NULL, 2333, N'', N'Ligier', N'', 38)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2372', NULL, 2333, N'', N'Lincoln', N'', 39)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2373', NULL, 2333, N'', N'London Taxi', N'', 40)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2374', NULL, 2333, N'', N'Lotus', N'', 41)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2375', NULL, 2333, N'', N'Man', N'', 42)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2376', NULL, 2333, N'', N'Mazda', N'', 43)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2377', NULL, 2333, N'', N'Mercedes Benz', N'', 44)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2378', NULL, 2333, N'', N'Metrocab', N'', 45)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2379', NULL, 2333, N'', N'MG', N'', 46)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2380', NULL, 2333, N'', N'Mini', N'', 47)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2381', NULL, 2333, N'', N'Mitsubishi', N'', 48)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2382', NULL, 2333, N'', N'Morris', N'', 49)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2383', NULL, 2333, N'', N'Nissan', N'', 50)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2384', NULL, 2333, N'', N'Peugeot', N'', 51)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2385', NULL, 2333, N'', N'Piaggio', N'', 52)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2386', NULL, 2333, N'', N'Porsche', N'', 53)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2387', NULL, 2333, N'', N'Proton', N'', 54)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2388', NULL, 2333, N'', N'Range Rover', N'', 55)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2389', NULL, 2333, N'', N'Renault', N'', 56)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2390', NULL, 2333, N'', N'Rolls Royce', N'', 57)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2391', NULL, 2333, N'', N'Rover', N'', 58)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2392', NULL, 2333, N'', N'Saab', N'', 59)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2393', NULL, 2333, N'', N'Scania ', N'', 60)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2394', NULL, 2333, N'', N'Seat', N'', 61)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2395', NULL, 2333, N'', N'Skoda', N'', 62)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2396', NULL, 2333, N'', N'Smart', N'', 63)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2397', NULL, 2333, N'', N'SSangyong', N'', 64)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2398', NULL, 2333, N'', N'Subaru', N'', 65)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2399', NULL, 2333, N'', N'Suzuki', N'', 66)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2400', NULL, 2333, N'', N'Toyota', N'', 67)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2401', NULL, 2333, N'', N'Vauxhall', N'', 68)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2402', NULL, 2333, N'', N'Volkswagen', N'', 69)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2403', NULL, 2333, N'', N'Volvo', N'', 70)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2404', NULL, 2333, N'', N'Yamaha', N'', 71)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2406', NULL, 2405, N'', N'Beige', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2407', NULL, 2405, N'', N'Black', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2408', NULL, 2405, N'', N'Blue', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2409', NULL, 2405, N'', N'Bronze', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2410', NULL, 2405, N'', N'Brown', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2411', NULL, 2405, N'', N'Cream', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2412', NULL, 2405, N'', N'ld', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2413', NULL, 2405, N'', N'Graphite', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2414', NULL, 2405, N'', N'Green', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2415', NULL, 2405, N'', N'Grey', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2416', NULL, 2405, N'', N'Lilac', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2417', NULL, 2405, N'', N'Maroon', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2418', NULL, 2405, N'', N'Mauve', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2419', NULL, 2405, N'', N'Orange', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2420', NULL, 2405, N'', N'Pink', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2421', NULL, 2405, N'', N'Purple', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2422', NULL, 2405, N'', N'Red', N'', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2423', NULL, 2405, N'', N'Silver', N'', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2424', NULL, 2405, N'', N'Turquoise', N'', 19)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2425', NULL, 2405, N'', N'White', N'', 20)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2426', NULL, 2405, N'', N'Yellow', N'', 21)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2433', NULL, 2432, N'', N'Petrol', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2434', NULL, 2432, N'', N'Diesel', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2435', NULL, 2432, N'', N'Unknown', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2438', NULL, 2437, N'', N'Manual', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2439', NULL, 2437, N'', N'Automatic', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2440', NULL, 2437, N'', N'Unknown', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2443', NULL, 2442, N'', N'Car', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2444', NULL, 2442, N'', N'Van', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2445', NULL, 2442, N'', N'Lorry', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2446', NULL, 2442, N'', N'Pickup', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2447', NULL, 2442, N'', N'Motorcycle', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2448', NULL, 2442, N'', N'Bicycle', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2451', NULL, 2450, N'', N'Car', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2452', NULL, 2450, N'', N'Van', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2453', NULL, 2450, N'', N'Lorry', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2454', NULL, 2450, N'', N'Motorcycle', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2455', NULL, 2450, N'', N'Pickup Van', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2456', NULL, 2450, N'', N'Cycle', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2457', NULL, 2442, N'', N'Minibus', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2458', NULL, 2450, N'', N'Minibus', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2459', NULL, 2450, N'', N'Coach', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2460', NULL, 2442, N'', N'Coach', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2463', NULL, 2317, N'', N'Mobile', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2495', NULL, 2494, N'', N'Previous Address', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2496', NULL, 2494, N'', N'Linked Address', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2497', NULL, 2494, N'', N'Lives At', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2504', NULL, 2503, N'', N'Instruction', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2505', NULL, 2503, N'', N'Info Only', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2577', NULL, 2575, N'', N'Hirer', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2578', NULL, 2575, N'', N'Insured', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2579', NULL, 2575, N'', N'Litigation Friend', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2580', NULL, 2575, N'', N'Policyholder', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2581', NULL, 2575, N'', N'Third Party', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2582', NULL, 2575, N'', N'Unknown', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2583', NULL, 2575, N'', N'Witness', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2606', NULL, 2605, N'', N'Director', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2607', NULL, 2605, N'', N'Secretary', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2608', NULL, 2605, N'', N'Previous Director', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2609', NULL, 2605, N'', N'Previous Secretary', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2610', NULL, 2605, N'', N'Formerly known as', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2611', NULL, 2605, N'', N'Trading Name of', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2714', NULL, 2713, N'', N'Driver', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2715', NULL, 2713, N'', N'Passenger', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2716', NULL, 2713, N'', N'Witness', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2717', NULL, 2713, N'', N'Unknown', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2767', NULL, 2766, N'', N'Policyholder', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2768', NULL, 2766, N'', N'Named Driver', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2769', NULL, 2766, N'', N'Paid Policy', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2792', NULL, 2791, N'', N'Director', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2793', NULL, 2791, N'', N'Secretary', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2794', NULL, 2791, N'', N'Employee', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2795', NULL, 2791, N'', N'Previous Director', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2796', NULL, 2791, N'', N'Pervious Secretary', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2797', NULL, 2791, N'', N'Previous Employee', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2798', NULL, 2791, N'', N'Manager', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2799', NULL, 2791, N'', N'Owner', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2800', NULL, 2791, N'', N'Partner', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2813', NULL, 2791, N'', N'Broker', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2814', NULL, 2791, N'', N'Referral Source', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2815', NULL, 2791, N'', N'Solicitor', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2816', NULL, 2791, N'', N'Engineer', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2817', NULL, 2791, N'', N'Recovery', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2818', NULL, 2791, N'', N'Storage', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2819', NULL, 2791, N'', N'Repairer', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2820', NULL, 2791, N'', N'Hire', N'', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2821', NULL, 2791, N'', N'Accident Management', N'', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2822', NULL, 2791, N'', N'Medical Examiner', N'', 19)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2844', NULL, 2843, N'', N'Also known as', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2845', NULL, 2843, N'', N'Associate', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2846', NULL, 2843, N'', N'Aunt', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2847', NULL, 2843, N'', N'Brother', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2848', NULL, 2843, N'', N'Brother in Law', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2849', NULL, 2843, N'', N'Father', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2850', NULL, 2843, N'', N'Father in Law', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2851', NULL, 2843, N'', N'Friend', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2852', NULL, 2843, N'', N'Suspected Identity Match', N'', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2853', NULL, 2843, N'', N'Interpreter', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2854', NULL, 2843, N'', N'Mother', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2855', NULL, 2843, N'', N'Mother in Law', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2856', NULL, 2843, N'', N'Partner', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2857', NULL, 2843, N'', N'Sibling', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2858', NULL, 2843, N'', N'Sister', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2859', NULL, 2843, N'', N'Sister in Law', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2860', NULL, 2843, N'', N'Spouse', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2861', NULL, 2843, N'', N'Uncle', N'', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2863', NULL, 2494, N'', N'Registered Office', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2864', NULL, 2494, N'', N'Trading Address', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2888', NULL, 2887, N'', N'Insured Vehicle', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2889', NULL, 2887, N'', N'Third Party Vehicle', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2890', NULL, 2887, N'', N'Hire Vehicle', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2891', NULL, 2887, N'', N'Witness Vehicle', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2892', NULL, 2887, N'', N'Unknown', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2895', NULL, 2894, N'', N'Cat A', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2896', NULL, 2894, N'', N'Cat B', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2897', NULL, 2894, N'', N'Cat C', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2898', NULL, 2894, N'', N'Cat D', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2912', NULL, 2911, N'', N'Insured', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2913', NULL, 2911, N'', N'Examined By', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2914', NULL, 2911, N'', N'Hirer', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2915', NULL, 2911, N'', N'Insured', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2916', NULL, 2911, N'', N'Owner', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2917', NULL, 2911, N'', N'Passenger', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2918', NULL, 2911, N'', N'Previous Keeper', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2919', NULL, 2911, N'', N'Registered Keeper', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2920', NULL, 2911, N'', N'Unknown', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2921', NULL, 2911, N'', N'Witness', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2929', NULL, 2766, N'', N'Insured', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2954', NULL, 2953, N'', N'CIFAS', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2955', NULL, 2953, N'', N'Comment', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2956', NULL, 2953, N'', N'IFIG Circular', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2957', NULL, 2953, N'', N'Intelligence', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2958', NULL, 2953, N'', N'Recovery', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2959', NULL, 2953, N'', N'Report', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2978', NULL, 2911, N'', N'Hired from', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2979', NULL, 2911, N'', N'Inspected By', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2980', NULL, 2911, N'', N'Recovered by', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2981', NULL, 2911, N'', N'Recovered to', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2982', NULL, 2911, N'', N'Repairer', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'2983', NULL, 2911, N'', N'Stored at', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3170', NULL, 2275, N'', N'Public Liability', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3172', NULL, 3171, N'', N'A11', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3173', NULL, 3171, N'', N'A12', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3174', NULL, 3171, N'', N'A13', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3175', NULL, 3171, N'', N'A14', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3176', NULL, 3171, N'', N'A15', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3177', NULL, 3171, N'', N'A21', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3178', NULL, 3171, N'', N'A22', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3179', NULL, 3171, N'', N'A23', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3180', NULL, 3171, N'', N'A24', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3181', NULL, 3171, N'', N'A25', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3182', NULL, 3171, N'', N'A31', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3183', NULL, 3171, N'', N'A32', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3184', NULL, 3171, N'', N'A33', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3185', NULL, 3171, N'', N'A34', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3186', NULL, 3171, N'', N'A35', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3187', NULL, 3171, N'', N'A41', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3188', NULL, 3171, N'', N'A42', N'', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3189', NULL, 3171, N'', N'A43', N'', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3190', NULL, 3171, N'', N'A44', N'', 19)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3191', NULL, 3171, N'', N'A45', N'', 20)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3192', NULL, 3171, N'', N'A51', N'', 21)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3193', NULL, 3171, N'', N'A52', N'', 22)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3194', NULL, 3171, N'', N'A53', N'', 23)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3195', NULL, 3171, N'', N'A54', N'', 24)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3196', NULL, 3171, N'', N'A55', N'', 25)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3197', NULL, 3171, N'', N'B11', N'', 26)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3198', NULL, 3171, N'', N'B12', N'', 27)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3199', NULL, 3171, N'', N'B13', N'', 28)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3200', NULL, 3171, N'', N'B14', N'', 29)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3201', NULL, 3171, N'', N'B15', N'', 30)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3202', NULL, 3171, N'', N'B21', N'', 31)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3203', NULL, 3171, N'', N'B22', N'', 32)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3204', NULL, 3171, N'', N'B23', N'', 33)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3205', NULL, 3171, N'', N'B24', N'', 34)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3206', NULL, 3171, N'', N'B25', N'', 35)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3207', NULL, 3171, N'', N'B31', N'', 36)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3208', NULL, 3171, N'', N'B32', N'', 37)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3209', NULL, 3171, N'', N'B33', N'', 38)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3210', NULL, 3171, N'', N'B34', N'', 39)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3211', NULL, 3171, N'', N'B35', N'', 40)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3212', NULL, 3171, N'', N'B41', N'', 41)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3213', NULL, 3171, N'', N'B42', N'', 42)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3214', NULL, 3171, N'', N'B43', N'', 43)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3215', NULL, 3171, N'', N'B44', N'', 44)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3216', NULL, 3171, N'', N'B45', N'', 45)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3217', NULL, 3171, N'', N'B51', N'', 46)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3218', NULL, 3171, N'', N'B52', N'', 47)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3219', NULL, 3171, N'', N'B53', N'', 48)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3220', NULL, 3171, N'', N'B54', N'', 49)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3221', NULL, 3171, N'', N'B55', N'', 50)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3222', NULL, 3171, N'', N'C11', N'', 51)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3223', NULL, 3171, N'', N'C12', N'', 52)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3224', NULL, 3171, N'', N'C13', N'', 53)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3225', NULL, 3171, N'', N'C14', N'', 54)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3226', NULL, 3171, N'', N'C15', N'', 55)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3227', NULL, 3171, N'', N'C21', N'', 56)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3228', NULL, 3171, N'', N'C22', N'', 57)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3229', NULL, 3171, N'', N'C23', N'', 58)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3230', NULL, 3171, N'', N'C24', N'', 59)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3231', NULL, 3171, N'', N'C25', N'', 60)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3232', NULL, 3171, N'', N'C31', N'', 61)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3233', NULL, 3171, N'', N'C32', N'', 62)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3234', NULL, 3171, N'', N'C33', N'', 63)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3235', NULL, 3171, N'', N'C34', N'', 64)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3236', NULL, 3171, N'', N'C35', N'', 65)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3237', NULL, 3171, N'', N'C41', N'', 66)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3238', NULL, 3171, N'', N'C42', N'', 67)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3239', NULL, 3171, N'', N'C43', N'', 68)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3240', NULL, 3171, N'', N'C44', N'', 69)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3241', NULL, 3171, N'', N'C45', N'', 70)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3242', NULL, 3171, N'', N'C51', N'', 71)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3243', NULL, 3171, N'', N'C52', N'', 72)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3244', NULL, 3171, N'', N'C53', N'', 73)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3245', NULL, 3171, N'', N'C54', N'', 74)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3246', NULL, 3171, N'', N'C55', N'', 75)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3247', NULL, 3171, N'', N'D11', N'', 76)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3248', NULL, 3171, N'', N'D12', N'', 77)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3249', NULL, 3171, N'', N'D13', N'', 78)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3250', NULL, 3171, N'', N'D14', N'', 79)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3251', NULL, 3171, N'', N'D15', N'', 80)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3252', NULL, 3171, N'', N'D21', N'', 81)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3253', NULL, 3171, N'', N'D22', N'', 82)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3254', NULL, 3171, N'', N'D23', N'', 83)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3255', NULL, 3171, N'', N'D24', N'', 84)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3256', NULL, 3171, N'', N'D25', N'', 85)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3257', NULL, 3171, N'', N'D31', N'', 86)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3258', NULL, 3171, N'', N'D32', N'', 87)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3259', NULL, 3171, N'', N'D33', N'', 88)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3260', NULL, 3171, N'', N'D34', N'', 89)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3261', NULL, 3171, N'', N'D35', N'', 90)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3262', NULL, 3171, N'', N'D41', N'', 91)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3263', NULL, 3171, N'', N'D42', N'', 92)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3264', NULL, 3171, N'', N'D43', N'', 93)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3265', NULL, 3171, N'', N'D44', N'', 94)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3266', NULL, 3171, N'', N'D45', N'', 95)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3267', NULL, 3171, N'', N'D51', N'', 96)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3268', NULL, 3171, N'', N'D52', N'', 97)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3269', NULL, 3171, N'', N'D53', N'', 98)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3270', NULL, 3171, N'', N'D54', N'', 99)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3271', NULL, 3171, N'', N'D55', N'', 100)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3272', NULL, 3171, N'', N'E11', N'', 101)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3273', NULL, 3171, N'', N'E12', N'', 102)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3274', NULL, 3171, N'', N'E13', N'', 103)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3275', NULL, 3171, N'', N'E14', N'', 104)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3276', NULL, 3171, N'', N'E15', N'', 105)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3277', NULL, 3171, N'', N'E21', N'', 106)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3278', NULL, 3171, N'', N'E22', N'', 107)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3279', NULL, 3171, N'', N'E23', N'', 108)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3280', NULL, 3171, N'', N'E24', N'', 109)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3281', NULL, 3171, N'', N'E25', N'', 110)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3282', NULL, 3171, N'', N'E31', N'', 111)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3283', NULL, 3171, N'', N'E32', N'', 112)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3284', NULL, 3171, N'', N'E33', N'', 113)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3285', NULL, 3171, N'', N'E34', N'', 114)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3286', NULL, 3171, N'', N'E35', N'', 115)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3287', NULL, 3171, N'', N'E41', N'', 116)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3288', NULL, 3171, N'', N'E42', N'', 117)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3289', NULL, 3171, N'', N'E43', N'', 118)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3290', NULL, 3171, N'', N'E44', N'', 119)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3291', NULL, 3171, N'', N'E45', N'', 120)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3292', NULL, 3171, N'', N'E51', N'', 121)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3293', NULL, 3171, N'', N'E52', N'', 122)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3294', NULL, 3171, N'', N'E53', N'', 123)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3295', NULL, 3171, N'', N'E54', N'', 124)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3296', NULL, 3171, N'', N'E55', N'', 125)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3297', NULL, 3171, N'', N'Ungraded', N'', 126)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3330', NULL, 1904, N'', N'Repairer', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3331', NULL, 1680, N'KIS', N'Jon Glover', N'1763', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3332', NULL, 1680, N'CFS - AXA CIS', N'Robert Rees', N'1762', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3333', NULL, 1680, N'CFS - AXA CIS', N'Helen Cutler', N'1762', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3334', NULL, 1680, N'CFS - AXA CIS', N'Elise Henderson-Renshaw', N'1762', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3335', NULL, 1680, N'CFS - AXA CIS', N'Judith Dooner', N'1762', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3336', NULL, 1680, N'CFS - AXA CIS', N'Nicola Walker', N'1762', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3337', NULL, 1680, N'CFS - AXA CIS', N'Emma Jane Simm', N'1762', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3338', NULL, 1680, N'CFS - AXA CIS', N'Matthew Ruck', N'1762', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3339', NULL, 1680, N'CFS - AXA CIS', N'Michelle Scully', N'1762', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3340', NULL, 1680, N'CFS - AXA CIS', N'Rachael Stirling', N'1762', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3341', NULL, 1680, N'CFS - AXA CIS', N'Janine Hadfield', N'1762', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3342', NULL, 1680, N'CFS - AXA CIS', N'Daniella nzalez', N'1762', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3343', NULL, 1680, N'CFS - AXA CIS', N'Ansa Rashid', N'1762', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3344', NULL, 1680, N'CFS - AXA CIS', N'David Riley', N'1762', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3345', NULL, 1680, N'CFS - AXA CIS', N'Anita Warhurst', N'1762', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3346', NULL, 1680, N'CFS - AXA CIS', N'Julia Morran', N'1762', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3347', NULL, 1680, N'KIS', N'Sarah Graber', N'1762', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3348', NULL, 1680, N'KIS', N'Steph Worthington', N'1762', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3349', NULL, 1680, N'KIS', N'Stephanie Young', N'1762', 19)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3350', NULL, 1680, N'KIS', N'Katy Koleniak', N'1762', 20)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3351', NULL, 1680, N'KIS', N'Anna Smethurst', N'1762', 21)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3352', NULL, 1680, N'KIS', N'Mark Weston', N'1762', 22)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3353', NULL, 1680, N'KIS', N'Nick Skelly', N'1762', 23)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3354', NULL, 1680, N'KIS', N'Andrea Flynn', N'1762', 24)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3355', NULL, 1680, N'KIS', N'Chris Bailey', N'1762', 25)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3356', NULL, 1680, N'KIS', N'Laura Sabin', N'1762', 26)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3357', NULL, 1680, N'KIS', N'Nicholas Berry', N'1762', 27)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3358', NULL, 1680, N'KIS', N'Anne Hoerty', N'1762', 28)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3359', NULL, 1680, N'KIS', N'Diane Whittle', N'1762', 29)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3360', NULL, 1680, N'KIS', N'Alice Martland', N'1762', 30)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3361', NULL, 1680, N'KIS', N'Tori Chambers', N'1762', 31)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3362', NULL, 1680, N'KIS', N'Jackie Burton', N'1762', 32)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3363', NULL, 1680, N'KIS', N'Gavin Walsh', N'1762', 33)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3364', NULL, 1680, N'KIS', N'Andrew Counsell', N'1762', 34)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3365', NULL, 1680, N'KIS', N'Stephen Holmes', N'1762', 35)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3366', NULL, 1680, N'KIS', N'Becky Wardle', N'1762', 36)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3367', NULL, 1680, N'KIS', N'Tracie Holland', N'1762', 37)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3368', NULL, 1680, N'KIS', N'Claire Brown', N'1762', 38)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3369', NULL, 1680, N'KIS', N'Lynda Dolan', N'1762', 39)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3370', NULL, 1680, N'KIS', N'Chevi Hochhauser', N'1762', 40)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3371', NULL, 1680, N'KIS', N'Alex Edwards', N'1762', 41)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3372', NULL, 1680, N'KIS', N'Rachel Stephenson', N'1762', 42)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3373', NULL, 1680, N'KIS', N'Amy Watson', N'1762', 43)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3374', NULL, 1680, N'KIS', N'Shauna Carbery', N'1762', 44)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3375', NULL, 1680, N'KIS', N'Steve Birt', N'1762', 45)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3376', NULL, 1680, N'KIS', N'Dominique Taylor', N'1762', 46)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3377', NULL, 1680, N'CFS - Fraud Ring 1', N'Andria Moorcroft', N'1762', 47)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3378', NULL, 1680, N'CFS - Fraud Ring 1', N'Cara Spendlove', N'1762', 48)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3379', NULL, 1680, N'CFS - Fraud Ring 1', N'Jonathan Cawley', N'1762', 49)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3380', NULL, 1680, N'CFS - Fraud Ring 1', N'Oliver Bingle', N'1762', 50)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3381', NULL, 1680, N'CFS - Fraud Ring 1', N'Fiona Snow', N'1762', 51)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3382', NULL, 1680, N'CFS - Fraud Ring 1', N'Natasha Tottey-Gee', N'1762', 52)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3383', NULL, 1680, N'CFS - Fraud Ring 1', N'Sam Lethbridge', N'1762', 53)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3384', NULL, 1680, N'CFS - Fraud Ring 1', N'Amy Masters', N'1762', 54)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3385', NULL, 1680, N'CFS - Fraud Ring 1', N'Marc Chadwick', N'1762', 55)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3386', NULL, 1680, N'CFS - Fraud Ring 1', N'Rachel Greenway', N'1762', 56)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3387', NULL, 1680, N'CFS - Fraud Ring 1', N'Hayley Thornhill', N'1762', 57)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3388', NULL, 1680, N'CFS - Fraud Ring 1', N'Pippa Judd', N'1762', 58)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3389', NULL, 1680, N'CFS - Fraud Ring 1', N'Zara Schofield', N'1762', 59)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3390', NULL, 1680, N'CFS - Fraud Ring 1', N'Sarah Lloyd', N'1762', 60)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3391', NULL, 1680, N'CFS - Fraud Ring 1', N'Shelley Galluccio', N'1762', 61)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3392', NULL, 1680, N'CFS - Fraud Ring 1', N'Thomas Mullin', N'1762', 62)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3393', NULL, 1680, N'CFS - Fraud Ring 1', N'Katie Graw', N'1762', 63)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3394', NULL, 1680, N'KIS', N'Emma Hayes', N'1763', 64)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3395', NULL, 1680, N'KIS', N'Paul Brown', N'1763', 65)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3396', NULL, 1680, N'KIS', N'Laura Langford', N'1763', 66)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3397', NULL, 1680, N'KIS', N'Kerrie Caven', N'1763', 67)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3398', NULL, 1680, N'KIS', N'Rita Lewis', N'1763', 68)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3399', NULL, 1680, N'KIS', N'Jon Gill', N'1763', 69)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3400', NULL, 1680, N'KIS', N'Joe Leneghan', N'1763', 70)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3401', NULL, 1680, N'KIS', N'Mandeep Gill', N'1763', 71)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3402', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Daniel Roberts', N'1762', 72)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3403', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Amy Collison', N'1762', 73)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3404', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Ayesha Roked', N'1762', 74)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3405', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Chris Scott', N'1762', 75)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3406', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Ian Barrans', N'1762', 76)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3407', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Deborah Conway', N'1762', 77)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3408', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Shantell Frudd', N'1762', 78)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3409', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Manisha Mistry', N'1762', 79)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3410', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Natalie Walker', N'1762', 80)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3411', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Natalie Hart', N'1762', 81)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3412', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Olivia Need-Tupman', N'1762', 82)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3413', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Anisha Ainscough', N'1762', 83)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3414', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Rehana Ali', N'1762', 84)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3415', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Qudsia Rana', N'1762', 85)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3416', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Kurt Edwards', N'1762', 86)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3417', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Shelley Butterworth', N'1762', 87)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3418', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Suzanne Stickland', N'1762', 88)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3419', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Laura Kelly', N'1762', 89)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3420', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Hannah Walling', N'1762', 90)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3421', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Maria Franchetti', N'1762', 91)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3422', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Marie Emberton', N'1762', 92)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3423', NULL, 1680, N'CFS - Esure/Zurich Bolton', N'Catherine Banks', N'1762', 93)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3424', NULL, 1680, N'Coventry CFS - Zurich', N'Susan Jones', N'1763', 94)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3425', NULL, 1680, N'Coventry CFS - Zurich', N'Roly Groom', N'1763', 95)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3426', NULL, 1680, N'Coventry CFS - Zurich', N'Neville Sampson', N'1763', 96)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3427', NULL, 1680, N'Coventry CFS - Zurich', N'Harminder Kang', N'1763', 97)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3428', NULL, 1680, N'Coventry CFS - Zurich', N'Pierre Smith', N'1763', 98)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3429', NULL, 1680, N'Coventry CFS - Zurich', N'Barny Overton', N'1763', 99)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3430', NULL, 1680, N'Coventry CFS - Zurich', N'Amy Edwards', N'1763', 100)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3431', NULL, 1680, N'Coventry CFS - Zurich', N'Abraham Khan', N'1763', 101)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3432', NULL, 1680, N'Coventry CFS - Zurich', N'Dee Poonia', N'1763', 102)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3433', NULL, 1680, N'Coventry CFS - Zurich', N'Don Bryson', N'1763', 103)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3434', NULL, 1680, N'Coventry CFS - Zurich', N'Alasdair Irving', N'1763', 104)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3435', NULL, 1680, N'Coventry CFS - Zurich', N'Sangeet Sanghera', N'1763', 105)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3436', NULL, 1680, N'Coventry CFS - Zurich', N'Milly McCabe', N'1763', 106)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3437', NULL, 1680, N'Coventry CFS - Zurich', N'Tino Nyatanga', N'1763', 107)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3438', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'James Heath', N'1763', 108)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3439', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Gupi Burchu', N'1763', 109)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3440', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Howard Chater', N'1763', 110)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3441', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Joanne Mulvenna', N'1763', 111)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3442', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Satvinder Boparan', N'1763', 112)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3443', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Amy odwin', N'1763', 113)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3444', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Nadine Grant', N'1763', 114)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3445', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Tom Beynon', N'1763', 115)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3446', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Miles Hepworth', N'1763', 116)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3447', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Bradley Roberts', N'1763', 117)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3448', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Emma Richmond', N'1763', 118)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3449', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Freya Feghali', N'1763', 119)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3450', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Sangeeta Matharu', N'1763', 120)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3451', NULL, 1680, N'Coventry CFS - Ryder Sabre Collingwood', N'Amrit Rama', N'1763', 121)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3452', NULL, 1680, N'CFS - Fraud Ring 2', N'Kirsty Heyes', N'1762', 122)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3453', NULL, 1680, N'CFS - Fraud Ring 2', N'Greg Cave', N'1762', 123)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3454', NULL, 1680, N'CFS - Fraud Ring 2', N'Courtney Bury', N'1762', 124)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3455', NULL, 1680, N'CFS - Fraud Ring 2', N'Danielle Elliott', N'1762', 125)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3456', NULL, 1680, N'CFS - Fraud Ring 2', N'Ruth Hill', N'1762', 126)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3457', NULL, 1680, N'CFS - Fraud Ring 2', N'Claire Simms', N'1762', 127)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3458', NULL, 1680, N'CFS - Fraud Ring 2', N'Kirsty Rawlings', N'1762', 128)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3459', NULL, 1680, N'CFS - Fraud Ring 2', N'Craig Glover', N'1762', 129)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3460', NULL, 1680, N'CFS - Fraud Ring 2', N'Idris Maka', N'1762', 130)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3461', NULL, 1680, N'CFS - Fraud Ring 2', N'Rebecca Martin', N'1762', 131)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3462', NULL, 1680, N'CFS - Fraud Ring 2', N'Tyrone Forrest', N'1762', 132)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3463', NULL, 1680, N'CFS - Fraud Ring 2', N'Laura Sales', N'1762', 133)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3464', NULL, 1680, N'CFS - Fraud Ring 2', N'Lee Hewitt', N'1762', 134)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3465', NULL, 1680, N'CFS - Fraud Ring 2', N'Adam Mayer', N'1762', 135)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3466', NULL, 1680, N'CFS - Fraud Ring 2', N'Deirdre McGeown', N'1762', 136)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3467', NULL, 1680, N'CFS - Fraud Ring 2', N'Lisa Burton', N'1762', 137)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3468', NULL, 1680, N'CFS - Fraud Ring 2', N'Andy Byrne', N'1762', 138)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3469', NULL, 1680, N'CFS - Fraud Ring 2', N'Allan Kornbluth', N'1762', 139)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3470', NULL, 1680, N'CFS - NFUM Broker Direct', N'Sarah Booth', N'1762', 140)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3471', NULL, 1680, N'CFS - NFUM Broker Direct', N'Jo Horner', N'1762', 141)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3472', NULL, 1680, N'CFS - NFUM Broker Direct', N'Julia Draper', N'1762', 142)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3473', NULL, 1680, N'CFS - NFUM Broker Direct', N'Jemma Brindle', N'1762', 143)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3474', NULL, 1680, N'CFS - NFUM Broker Direct', N'Kirsty Taylor', N'1762', 144)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3475', NULL, 1680, N'CFS - NFUM Broker Direct', N'Nicola Round', N'1762', 145)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3476', NULL, 1680, N'CFS - NFUM Broker Direct', N'Matthew Wheeler', N'1762', 146)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3477', NULL, 1680, N'CFS - NFUM Broker Direct', N'Gemma White', N'1762', 147)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3478', NULL, 1680, N'CFS - NFUM Broker Direct', N'Amy Fowler', N'1762', 148)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3479', NULL, 1680, N'CFS - NFUM Broker Direct', N'Sarah Hargreaves', N'1762', 149)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3480', NULL, 1680, N'CFS - NFUM Broker Direct', N'Rob lds', N'1762', 150)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3481', NULL, 1680, N'CFS - NFUM Broker Direct', N'Rachael Tyrer', N'1762', 151)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3482', NULL, 1680, N'CFS - NFUM Broker Direct', N'Jasmin Jalil', N'1762', 152)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3483', NULL, 1680, N'CFS - NFUM Broker Direct', N'Libby Ashton', N'1762', 153)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3484', NULL, 1680, N'CFS - NFUM Broker Direct', N'Zachary Hilton', N'1762', 154)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3485', NULL, 1680, N'CFS Sales & Management Team', N'Debra Brookes', N'1762', 155)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3486', NULL, 1680, N'CFS Sales & Management Team', N'Tina Smith', N'1762', 156)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3487', NULL, 1680, N'CFS Sales & Management Team', N'Nichola Ashworth', N'1762', 157)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3488', NULL, 1680, N'CFS Sales & Management Team', N'Andrew Auchterlounie', N'1762', 158)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3489', NULL, 1680, N'CFS Sales & Management Team', N'Lucy Walker', N'1762', 159)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3490', NULL, 1680, N'CFS Sales & Management Team', N'Sharon Weeks', N'1762', 160)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3491', NULL, 1680, N'CFS Sales & Management Team', N'Avrom Topperman', N'1762', 161)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3492', NULL, 1680, N'CFS Sales & Management Team', N'Anthony Dale', N'1762', 162)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3493', NULL, 1680, N'CFS Sales & Management Team', N'Martin Gill', N'1763', 163)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3494', NULL, 1680, N'CFS - Delegated', N'Amy Kirwan', N'1762', 164)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3495', NULL, 1680, N'CFS - Delegated', N'Richard Iddon', N'1762', 165)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3496', NULL, 1680, N'CFS - Delegated', N'Hannah Jones', N'1762', 166)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3497', NULL, 1680, N'CFS - Delegated', N'Rob Milner', N'1762', 167)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3498', NULL, 1680, N'CFS - Delegated', N'Waheeda Bagasi', N'1762', 168)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3499', NULL, 1680, N'CFS - Delegated', N'Pamela Kennedy', N'1762', 169)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3500', NULL, 1680, N'CFS - Delegated', N'Nicola O`Neill', N'1762', 170)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3501', NULL, 1680, N'CFS - Delegated', N'Chris Heaton', N'1762', 171)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3502', NULL, 1680, N'CFS - Delegated', N'Karen Carrington', N'1762', 172)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3503', NULL, 1680, N'CFS - Delegated', N'Deborah Hazlewood', N'1762', 173)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3504', NULL, 1680, N'CFS - Delegated', N'Michelle Hayley Lewis', N'1762', 174)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3505', NULL, 1680, N'CFS - Delegated', N'Charlotte Richards', N'1762', 175)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3506', NULL, 1680, N'CFS - Delegated', N'Kate Watmough', N'1762', 176)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3507', NULL, 1680, N'CFS - Delegated', N'Anna Churchill', N'1762', 177)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3508', NULL, 1680, N'CFS - Delegated', N'Laura Hardiman', N'1762', 178)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3509', NULL, 1680, N'CFS - Enforcement', N'Karen Berry', N'1762', 179)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3510', NULL, 1680, N'CFS - Enforcement', N'Leanne Nuttall', N'1762', 180)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3511', NULL, 1680, N'CFS - Enforcement', N'Sharon Walker', N'1762', 181)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3512', NULL, 1680, N'CFS - Enforcement', N'Christine Collins', N'1762', 182)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3513', NULL, 1680, N'CFS - Enforcement', N'Annara Rodger', N'1762', 183)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3514', NULL, 1680, N'CFS - Enforcement', N'Rebecca Hogg', N'1762', 184)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3515', NULL, 1680, N'CFS Resolution Team', N'Louise Jeffries', N'1762', 185)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3516', NULL, 1680, N'CFS Resolution Team', N'Angela Molloy', N'1762', 186)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3517', NULL, 1680, N'CFS Resolution Team', N'Gary Adey', N'1762', 187)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3518', NULL, 1680, N'CFS Resolution Team', N'Nicola McGrath', N'1762', 188)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3519', NULL, 1680, N'CFS Resolution Team', N'Johanna Tomlinson', N'1762', 189)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3520', NULL, 1680, N'CFS Resolution Team', N'John Boylan', N'1762', 190)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3521', NULL, 1680, N'CFS Resolution Team', N'Emma Wilkinson', N'1762', 191)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3522', NULL, 1680, N'CFS Resolution Team', N'Raadiah Habeeb', N'1762', 192)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3523', NULL, 1680, N'CFS Complex Fraud', N'Stratos Gatzouris', N'1762', 193)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3524', NULL, 1680, N'CFS Complex Fraud', N'Pamela Davies', N'1762', 194)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3525', NULL, 1680, N'CFS Complex Fraud', N'Kirstie Smith', N'1762', 195)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3526', NULL, 1680, N'CFS Complex Fraud', N'Hamida Khatun', N'1762', 196)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3527', NULL, 1680, N'CFS Complex Fraud', N'Elaine Ibbotson', N'1762', 197)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3528', NULL, 1680, N'CFS Complex Fraud', N'Ted Sheils', N'1762', 198)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3529', NULL, 1680, N'CFS Complex Fraud', N'Victoria Yates', N'1762', 199)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3530', NULL, 1680, N'CFS Complex Fraud', N'Richard Masters', N'1762', 200)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3531', NULL, 1680, N'CFS Complex Fraud', N'Aysha Ahmed', N'1762', 201)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3532', NULL, 1680, N'CFS Complex Fraud', N'Emma Wilson', N'1762', 202)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3533', NULL, 1680, N'CFS Complex Fraud', N'Paul Smith', N'1762', 203)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3534', NULL, 1680, N'CFS Complex Fraud', N'Jennifer Brooks', N'1762', 204)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3535', NULL, 1680, N'CFS Complex Fraud', N'Rekha Sharma', N'1762', 205)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3536', NULL, 1680, N'CFS - Credit Hire Fraud Team', N'Claire Duncan', N'1762', 206)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3537', NULL, 1680, N'CFS - Credit Hire Fraud Team', N'Naeema Riazat', N'1762', 207)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3538', NULL, 1680, N'CFS - Credit Hire Fraud Team', N'Cheryl Baines', N'1762', 208)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3539', NULL, 1680, N'CFS - Credit Hire Fraud Team', N'Christine Wilson-Collins', N'1762', 209)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3540', NULL, 1680, N'CFS - Credit Hire Fraud Team', N'Andrew Smethurst', N'1762', 210)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3541', NULL, 1680, N'CFS - Ageas Tradex', N'Lauren Hunt', N'1762', 211)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3542', NULL, 1680, N'CFS - Ageas Tradex', N'Lynsey Penk', N'1762', 212)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3543', NULL, 1680, N'CFS - Ageas Tradex', N'Lindsey Clarke', N'1762', 213)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3544', NULL, 1680, N'CFS - Ageas Tradex', N'Alex RuauxKeyho', N'1762', 214)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3545', NULL, 1680, N'CFS - Ageas Tradex', N'Leanne Hamblett', N'1762', 215)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3546', NULL, 1680, N'CFS - Ageas Tradex', N'Daniel King', N'1762', 216)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3547', NULL, 1680, N'CFS - Ageas Tradex', N'Jeremy Rosenberg', N'1762', 217)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3548', NULL, 1680, N'CFS - Ageas Tradex', N'Christopher Hearle', N'1762', 218)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3549', NULL, 1680, N'CFS - Ageas Tradex', N'Elizabeth Poole', N'1762', 219)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3550', NULL, 1680, N'CFS - Ageas Tradex', N'Wendy Catterall', N'1762', 220)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3551', NULL, 1680, N'CFS - Ageas Tradex', N'Stuart Farrell', N'1762', 221)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3552', NULL, 1680, N'CFS - Ageas Tradex', N'Victoria Andrews', N'1762', 222)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3553', NULL, 1680, N'CFS - Ageas Tradex', N'Gareth Poole', N'1762', 223)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3554', NULL, 1680, N'CFS - Ageas Tradex', N'Gemma Rolstone', N'1762', 224)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3555', NULL, 1680, N'CFS - Ageas Tradex', N'Becky Duffus', N'1762', 225)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3556', NULL, 1680, N'CFS - Ageas Tradex', N'Lian Whitty', N'1762', 226)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3557', NULL, 1680, N'CFS - Fraud Ring 3', N'Jo Boardman', N'1762', 227)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3558', NULL, 1680, N'CFS - Fraud Ring 3', N'Anna Shea', N'1762', 228)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3559', NULL, 1680, N'CFS - Fraud Ring 3', N'Isaac Taylor', N'1762', 229)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3560', NULL, 1680, N'CFS - Fraud Ring 3', N'Marian JamesAuthe', N'1762', 230)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3561', NULL, 1680, N'CFS - Fraud Ring 3', N'Andrew Clarke', N'1762', 231)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3562', NULL, 1680, N'CFS - Fraud Ring 3', N'Gemma Wilkinson', N'1762', 232)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3563', NULL, 1680, N'CFS - Fraud Ring 3', N'Chanda Andrews', N'1762', 233)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3564', NULL, 1680, N'CFS - Fraud Ring 3', N'Nick Ingham', N'1762', 234)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3565', NULL, 1680, N'CFS - Fraud Ring 3', N'Nicola Walsh', N'1762', 235)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3566', NULL, 1680, N'CFS - Fraud Ring 3', N'Rachel Fulda', N'1762', 236)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3567', NULL, 1680, N'CFS - Fraud Ring 3', N'Rehana Ahmed', N'1762', 237)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3568', NULL, 1680, N'CFS - Fraud Ring 3', N'Kathryn Walmsley', N'1762', 238)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3569', NULL, 1680, N'CFS - Fraud Ring 3', N'Mark Lewis', N'1762', 239)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3570', NULL, 1680, N'CFS - Fraud Ring 3', N'John Jackson', N'1762', 240)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3571', NULL, 1680, N'CFS - Fraud Ring 3', N'Joanna Knapman', N'1762', 241)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3572', NULL, 1680, N'CFS - Fraud Ring 3', N'Philip Armitage', N'1762', 242)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3573', NULL, 1680, N'CFS - Fraud Ring 3', N'Ian Toft', N'1762', 243)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3574', NULL, 1680, N'CFS - LV Team 1', N'Billie-Jean Whitaker', N'1762', 244)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3575', NULL, 1680, N'CFS - LV Team 1', N'Aaron Reynard', N'1762', 245)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3576', NULL, 1680, N'CFS - LV Team 1', N'Matthew Carroll', N'1762', 246)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3577', NULL, 1680, N'CFS - LV Team 1', N'Rachel Harrison', N'1762', 247)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3578', NULL, 1680, N'CFS - LV Team 1', N'Holly Hardman', N'1762', 248)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3579', NULL, 1680, N'CFS - LV Team 1', N'Dharmishta Pither', N'1762', 249)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3580', NULL, 1680, N'CFS - LV Team 1', N'Rebecca Gilbert', N'1762', 250)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3581', NULL, 1680, N'CFS - LV Team 1', N'Stacey Croft', N'1762', 251)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3582', NULL, 1680, N'CFS - LV Team 1', N'Nicky Perrott', N'1762', 252)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3583', NULL, 1680, N'CFS - LV Team 1', N'Danny Newton', N'1762', 253)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3584', NULL, 1680, N'CFS - LV Team 1', N'David Parkinson', N'1762', 254)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3585', NULL, 1680, N'CFS - LV Team 1', N'Katherine Richardson', N'1762', 255)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3586', NULL, 1680, N'CFS - LV Team 2', N'Deborah Kelly', N'1762', 256)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3587', NULL, 1680, N'CFS - LV Team 2', N'Donna Brindle', N'1762', 257)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3588', NULL, 1680, N'CFS - LV Team 2', N'Gareth Berry', N'1762', 258)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3589', NULL, 1680, N'CFS - LV Team 2', N'Graham Dillon', N'1762', 259)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3590', NULL, 1680, N'CFS - LV Team 2', N'Kimberley Tinker', N'1762', 260)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3591', NULL, 1680, N'CFS - LV Team 2', N'Anna Farrimond-Ackers', N'1762', 261)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3592', NULL, 1680, N'CFS - LV Team 2', N'Nicola Wise', N'1762', 262)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3593', NULL, 1680, N'CFS - LV Team 2', N'Catherine Hanson', N'1762', 263)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3594', NULL, 1680, N'CFS - LV Team 2', N'Shazia Munshi', N'1762', 264)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3595', NULL, 1680, N'CFS - LV Team 2', N'Jessica Swire', N'1762', 265)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3596', NULL, 1680, N'CFS - LV Team 2', N'Monica McKay', N'1762', 266)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3597', NULL, 1680, N'CFS - LV Team 2', N'Louise Dempsey', N'1762', 267)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3598', NULL, 1680, N'CFS - LV Team 2', N'Caroline Melody', N'1762', 268)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3599', NULL, 1680, N'CFS - Fraud Rings Management Team', N'Mags Singleton', N'1762', 269)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3600', NULL, 1680, N'CFS - Fraud Rings Management Team', N'Amanda Nuttall', N'1762', 270)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3601', NULL, 1680, N'CFS - Fraud Rings Management Team', N'Jacqueline Impey', N'1762', 271)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3602', NULL, 1680, N'CFS - Fraud Rings Management Team', N'Ruth Needham', N'1762', 272)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3603', NULL, 1680, N'CFS - Fraud Rings Management Team', N'Elise Ellson', N'1762', 273)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3604', NULL, 1680, N'CFS - Fraud Rings Management Team', N'Nina Dayal', N'1762', 274)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3605', NULL, 1680, N'CFS - Fraud Rings Management Team', N'Mark Beales', N'1762', 275)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3606', NULL, 1680, N'CFS - RBSI', N'Maeve Spillane', N'1762', 276)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3607', NULL, 1680, N'CFS - RBSI', N'Christina Stell', N'1762', 277)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3608', NULL, 1680, N'CFS - RBSI', N'Sarah Whewell', N'1762', 278)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3609', NULL, 1680, N'CFS - RBSI', N'Eli Wieder', N'1762', 279)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3610', NULL, 1680, N'CFS - RBSI', N'Natalie Hadley', N'1762', 280)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3611', NULL, 1680, N'CFS - RBSI', N'Lauren Poole', N'1762', 281)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3612', NULL, 1680, N'CFS - RBSI', N'Karen Flemmings-Jordan', N'1762', 282)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3613', NULL, 1680, N'CFS - RBSI', N'Alison Chilvers', N'1762', 283)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3614', NULL, 1680, N'CFS - RBSI', N'Misbah Koli', N'1762', 284)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3615', NULL, 1680, N'CFS - RBSI', N'Louise Smiley', N'1762', 285)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3616', NULL, 1680, N'CFS - RBSI', N'Claire Carroll', N'1762', 286)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3617', NULL, 1680, N'CFS - RBSI', N'Neil Shepard', N'1762', 287)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3618', NULL, 1680, N'CFS - RBSI', N'Clare Senior', N'1762', 288)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3619', NULL, 1680, N'CFS - RBSI', N'Alexandra Kenyon', N'1762', 289)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3620', NULL, 1680, N'CFS - RBSI', N'Tracy Crompton', N'1762', 290)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3621', NULL, 1680, N'CFS - Quality and Auditing', N'Helen Webster', N'1762', 291)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3622', NULL, 1680, N'CFS - Quality and Auditing', N'Sabiha Kolia', N'1762', 292)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3623', NULL, 1680, N'CFS - Quality and Auditing', N'Imogen Martinez', N'1762', 293)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3624', NULL, 1680, N'CFS - Quality and Auditing', N'Nicola Bayes', N'1762', 294)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3625', NULL, 1680, N'CFS - Quality and Auditing', N'Sarah Bell', N'1762', 295)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3626', NULL, 1680, N'Coventry CFS Pre Proceedings', N'Gaveeta Parmar', N'1763', 296)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3627', NULL, 1680, N'Coventry CFS Pre Proceedings', N'Samantha Akehurst', N'1763', 297)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3628', NULL, 1680, N'Coventry CFS Pre Proceedings', N'Jagrutee Patel', N'1763', 298)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3629', NULL, 1680, N'Coventry CFS Pre Proceedings', N'Kayleigh Mullarkey', N'1763', 299)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3634', NULL, 3633, N'', N'Linked Address', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3635', NULL, 3633, N'', N'Registered Office', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3898', NULL, 2003, N'', N'Male', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3899', NULL, 2003, N'', N'Alias', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3901', NULL, 2450, N'', N'Bicycle', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3948', NULL, 3633, N'', N'Insured', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3949', NULL, 2432, N'', N'Electric', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3950', NULL, 2432, N'', N'Hybrid', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'3951', NULL, 2432, N'', N'LPG', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4004', NULL, 1713, N'', N'MIAFTR', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4005', NULL, 1713, N'', N'CUE PI', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4006', NULL, 2317, N'', N'Phone Unknown', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4007', NULL, 1713, N'', N'Motor', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4008', NULL, 1713, N'', N'Commercial Claim', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4009', NULL, 1713, N'', N'EL Claim', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4010', NULL, 1713, N'', N'PL Claim', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4011', NULL, 1713, N'', N'Travel Claim', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4012', NULL, 1713, N'', N'Home Contents', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4013', NULL, 1713, N'', N'Home Building', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4014', NULL, 1713, N'', N'CMS', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4015', NULL, 1713, N'', N'TCS', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4016', NULL, 1713, N'', N'Theft Claim', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4017', NULL, 1713, N'', N'Pet Claim', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4018', NULL, 1713, N'', N'Mobile Claim', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4019', NULL, 1713, N'', N'Keoghs CFS', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4020', NULL, 1713, N'', N'Keoghs CFS Fraud Ring', N'', 0)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4042', NULL, 4041, N'', N'Beige', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4043', NULL, 4041, N'', N'Black', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4044', NULL, 4041, N'', N'Blue', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4045', NULL, 4041, N'', N'Bronze', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4046', NULL, 4041, N'', N'Brown', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4047', NULL, 4041, N'', N'Cream', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4048', NULL, 4041, N'', N'ld', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4049', NULL, 4041, N'', N'Graphite', N'', 8)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4050', NULL, 4041, N'', N'Green', N'', 9)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4051', NULL, 4041, N'', N'Grey', N'', 10)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4052', NULL, 4041, N'', N'Lilac', N'', 11)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4053', NULL, 4041, N'', N'Maroon', N'', 12)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4054', NULL, 4041, N'', N'Mauve', N'', 13)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4055', NULL, 4041, N'', N'Orange', N'', 14)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4056', NULL, 4041, N'', N'Pink', N'', 15)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4057', NULL, 4041, N'', N'Purple', N'', 16)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4058', NULL, 4041, N'', N'Red', N'', 17)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4059', NULL, 4041, N'', N'Silver', N'', 18)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4060', NULL, 4041, N'', N'Turquoise', N'', 19)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4061', NULL, 4041, N'', N'White', N'', 20)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4062', NULL, 4041, N'', N'Yellow', N'', 21)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4085', NULL, 4084, N'Unknown', N'Unknown', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4086', NULL, 4084, N'Policy Handset', N'Policy Handset', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4087', NULL, 4084, N'Insured Handset', N'Insured Handset', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4088', NULL, 4084, N'Previously Insured Handset', N'Previously Insured Handset', N'', 4)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4089', NULL, 4084, N'Replacement Handset', N'Replacement Handset', N'', 5)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4090', NULL, 4084, N'Loan Handset', N'Loan Handset', N'', 6)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4091', NULL, 4084, N'No Handset Involved', N'No Handset Involved', N'', 7)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4111', NULL, 4110, N'Unknown', N'Unknown', N'', 1)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4112', NULL, 4110, N'Claimant', N'Claimant', N'', 2)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4113', NULL, 4110, N'Owner', N'Owner', N'', 3)

INSERT [dbo].[_Codes] ([Unique_ID], [Code], [CodeGroup_ID], [Description], [Expansion], [Parent_ID], [SortIndex]) VALUES (N'4114', NULL, 4110, N'Uses', N'Uses', N'', 4)

INSERT [dbo].[_DataStoreRegister] ([RecordType], [Icon], [Name], [VisibleType]) VALUES (12, N'Set', N'Sets', 1)

INSERT [dbo].[_DataStoreRegister] ([RecordType], [Icon], [Name], [VisibleType]) VALUES (13, N'Query', N'Queries', 1)

INSERT [dbo].[_DataStoreRegister] ([RecordType], [Icon], [Name], [VisibleType]) VALUES (19, N'Report', N'Report Definitions', 1)

INSERT [dbo].[_DataStoreRegister] ([RecordType], [Icon], [Name], [VisibleType]) VALUES (20, N'Browse', N'Browse Definitions', 1)

INSERT [dbo].[_DataStoreRegister] ([RecordType], [Icon], [Name], [VisibleType]) VALUES (21, N'ScoredMatching', N'Scored Matching', 1)

INSERT [dbo].[_DataStoreRegister] ([RecordType], [Icon], [Name], [VisibleType]) VALUES (30, N'ImportSpec', N'Import Specifications', 1)

INSERT [dbo].[_DataStoreRegister] ([RecordType], [Icon], [Name], [VisibleType]) VALUES (31, N'ExportSpec', N'Export Specifications', 1)

INSERT [dbo].[_DataStoreRegister] ([RecordType], [Icon], [Name], [VisibleType]) VALUES (32, N'BatchImport', N'Import Batch Specifications', 1)

INSERT [dbo].[_DataStoreRegister] ([RecordType], [Icon], [Name], [VisibleType]) VALUES (33, N'BatchExport', N'Export Batch Specifications', 1)

INSERT [dbo].[_DataStoreRegister] ([RecordType], [Icon], [Name], [VisibleType]) VALUES (40, N'ChartScheme', N'Charting Schemes', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1000, N'', 0, -1, N'Account', N'Account_', N'ACC', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1065, N'', 0, -1, N'Address', N'Address_', N'ADD', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1174, N'', 0, -1, N'BB Pin', N'BBPin_', N'BBP', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1186, N'', 0, -1, N'Driving Licence', N'Driving_Licence', N'DRI', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1197, N'', 0, -1, N'Email', N'Email_', N'EMA', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1208, N'', 0, -1, N'Fraud Ring', N'Fraud_Ring', N'FRA', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1688, N'', 0, -1, N'Incident', N'Incident_', N'INC', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1739, N'', 0, -1, N'IP Address', N'IP_Address', N'IP_', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1750, N'', 0, -1, N'Keoghs Case', N'Keoghs_Case', N'KEO', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1882, N'', 0, -1, N'NI Number', N'NI_Number', N'NI_', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1893, N'', 0, -1, N'Organisation', N'Organisation_', N'ORG', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1940, N'', 0, -1, N'Passport', N'Passport_', N'PAS', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1951, N'', 0, -1, N'Payment Card', N'Payment_Card', N'PAY', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (1969, N'', 0, -1, N'Person', N'Person_', N'PER', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2261, N'', 0, -1, N'Policy', N'Policy_', N'POL', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2298, N'', 0, -1, N'Telephone', N'Telephone_', N'TEL', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2321, N'', 0, -1, N'Vehicle', N'Vehicle_', N'VEH', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2464, N'', 0, -1, N'Website', N'Website_', N'WEB', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2475, N'', 0, -1, N'Address Link', N'Address_to_Address_Link', N'AD0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2485, N'', 0, -1, N'Case Incident Link', N'Case_to_Incident_Link', N'CAS', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2508, N'', 0, -1, N'Fraud Ring Incident Link', N'Fraud_Ring_Incident_Link', N'FR0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2519, N'', 0, -1, N'Incident Match Link', N'Incident_Match_Link', N'IN0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2595, N'', 0, -1, N'Organisation to Organisation Link', N'Organisation_Match_Link', N'OR5', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2632, N'', 0, -1, N'Account Link', N'Account_Link', N'AC0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2642, N'', 0, -1, N'Email Link', N'Email_Link', N'EM0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2652, N'', 0, -1, N'IP Address Link', N'IP_Address_Link', N'IP0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2662, N'', 0, -1, N'Payment Card Link', N'Payment_Card_Link', N'PA0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2672, N'', 0, -1, N'Telephone Link', N'Telephone_Link', N'TE0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2682, N'', 0, -1, N'BB Pin Link', N'BB_Pin_Link', N'BB_', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2692, N'', 0, -1, N'Driving Licence Link', N'Driving_Licence_Link', N'DR0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2702, N'', 0, -1, N'Incident Link', N'Incident_Link', N'IN1', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2746, N'', 0, -1, N'NI Number Link', N'NI_Number_Link', N'NI0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2756, N'', 0, -1, N'Policy Link', N'Policy_Link', N'PO0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2771, N'', 0, -1, N'Website Link', N'Website_Link', N'WE0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2781, N'', 0, -1, N'Person to Organisation Link', N'Person_to_Organisation_Link', N'PE0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2823, N'', 0, -1, N'Passport Link', N'Passport_Link', N'PA1', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2833, N'', 0, -1, N'Person to Person Link', N'Person_to_Person_Link', N'PE1', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2865, N'', 0, -1, N'Policy Payment Link', N'Policy_Payment_Link', N'PO1', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2877, N'', 0, -1, N'Vehicle Incident Link', N'Vehicle_Incident_Link', N'VE0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2901, N'', 0, -1, N'Vehicle Link', N'Vehicle_Link', N'VE1', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2930, N'', 0, -1, N'Vehicle to Vehicle Link', N'Vehicle_to_Vehicle_Link', N'VE2', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2942, N'', 0, -1, N'Intelligence', N'Intelligence_', N'INT', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (2967, N'', 0, -1, N'Intelligence Link', N'Intelligence_Link', N'IN2', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (3836, N'', 0, -1, N'TOG', N'TOG_', N'TOG', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (3858, N'', 0, -1, N'Outcome Link', N'Outcome_Link', N'OUT', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (3883, N'', 0, -1, N'TOG Link', N'TOG_Link', N'TO0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (4027, N'', 0, -1, N'Handset', N'Handset_', N'HAN', 0)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (4070, N'', 0, -1, N'Handset Incident Link', N'Handset_Incident_Link', N'HA0', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (4095, N'', 0, -1, N'Handset Link', N'Handset_Link', N'HA1', 1)

INSERT [dbo].[_DataTable] ([Table_ID], [Description], [Fixed], [InExpandList], [LogicalName], [PhysicalName], [TableCode], [Type]) VALUES (4117, N'', 0, -1, N'Handset to Handset Link', N'Handset_to_Handset_Link', N'HA2', 1)

INSERT [dbo].[_Driving_Licence_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Driving_Licence_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Email__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Email_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1000, N'Account')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1065, N'House')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1174, N'Pager')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1186, N'Driving License')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1197, N'E-mail')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1208, N'Search')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1688, N'Incident')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1739, N'IP Address')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1750, N'Filing Cabinet')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1882, N'NI')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1893, N'Office')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1940, N'Passport')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1951, N'Credit Card')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (1969, N'Person')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (2261, N'Insurance Policy')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (2298, N'Telephone')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (2321, N'Car')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (2464, N'WWW')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (2942, N'Document')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (3836, N'TOG')

INSERT [dbo].[_Entity] ([Table_ID], [ChartEntity]) VALUES (4027, N'Handset')

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-20, 0, NULL, NULL, NULL, N'Security Classification Code - can be used to ''hide'' records from certain users.', 0, NULL, 255, -1, NULL, N'SCC', 6, -1, N'SCC', -1, -102)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-19, 0, NULL, NULL, NULL, N'Record status = 0 or 1 same end record flipped around', 0, NULL, 0, -1, NULL, N'Record_Type', 8, -1, N'Record_Type', -1, -102)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-18, 0, NULL, NULL, N'0', N'The status of the record: 0=open;2=Chart via set;200=Soft Merged;254=Soft Deleted;(+1)=Read-only', 0, NULL, 0, -1, NULL, N'Record_Status', 8, -1, N'Record_Status', -1, -102)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-17, 0, NULL, NULL, NULL, N'Type of link i.e. tableid of link', 0, NULL, 0, -1, NULL, N'LinkType_ID', 1, -1, N'LinkType_ID', -1, -102)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-16, 0, NULL, NULL, NULL, N'Entity Type ID for end 2', 0, NULL, 0, -1, NULL, N'EntityType_ID2', 1, -1, N'EntityType_ID2', -1, -102)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-15, 0, NULL, NULL, NULL, N'Entity Type ID for end 1', 0, NULL, 0, -1, NULL, N'EntityType_ID1', 1, -1, N'EntityType_ID1', -1, -102)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-14, 0, NULL, NULL, NULL, N'Link Direction', 0, NULL, 0, -1, NULL, N'Direction', 8, -1, N'Direction', -1, -102)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-13, 0, NULL, NULL, NULL, N'Link confidence (strength)', 0, NULL, 0, -1, NULL, N'Confidence', 8, -1, N'Confidence', -1, -102)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-12, 0, NULL, NULL, NULL, N'Database-unique record identifier for End 2 of link', 0, NULL, 50, -1, NULL, N'Entity_ID2', 6, -1, N'Entity_ID2', -1, -102)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-11, 0, NULL, NULL, NULL, N'Database-unique record identifier for End 1 of link', 0, NULL, 50, -1, NULL, N'Entity_ID1', 6, -1, N'Entity_ID1', -1, -102)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-10, 0, NULL, NULL, NULL, N'Icon shading colour. (Only valid for entity types.)', 0, NULL, 0, -1, NULL, N'IconColour', 1, 0, N'IconColour', 0, -100)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-9, 0, NULL, NULL, NULL, N'Alternate icon representation. (Only valid for entity types.)', 0, NULL, 50, -1, NULL, N'AltEntity', 6, 0, N'AltEntity', 0, -100)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-8, 0, NULL, NULL, N'''''', N'Set_ID, Record_ID or User_ID as appropriate to the current ''Record_Status'' value.', 0, NULL, 50, -1, NULL, N'Status_Binding', 6, 0, N'Status_Binding', 0, -100)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-7, 0, NULL, NULL, N'0', N'The status of the record: 0=open;2=Chart via set;200=Soft Merged;254=Soft Deleted;(+1)=Read-only', 0, NULL, 0, -1, NULL, N'Record_Status', 8, 0, N'Record_Status', 0, -100)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-6, 0, NULL, NULL, N'''''', N'Security Classification Code - can be used to ''hide'' records from certain users.', 0, NULL, 255, -1, NULL, N'SCC', 6, 0, N'SCC', 0, -100)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-5, 0, NULL, NULL, NULL, N'The date the record was last updated, if applicable.', 0, NULL, 0, -1, NULL, N'Last_Upd_Date', 5, 0, N'Last_Upd_Date', 0, -100)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-4, 0, NULL, NULL, NULL, N'The user who last updated the record, if applicable.', 0, NULL, 255, -1, NULL, N'Last_Upd_User', 6, 0, N'Last_Upd_User', 0, -100)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-3, 0, NULL, NULL, NULL, N'The date the record was created.', 0, NULL, 0, -1, NULL, N'Create_Date', 5, -1, N'Create_Date', 0, -100)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-2, 0, NULL, NULL, NULL, N'The user who created the record.', 0, NULL, 255, -1, NULL, N'Create_User', 6, -1, N'Create_User', 0, -100)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (-1, 0, NULL, NULL, NULL, N'Database-unique record identifier.', 0, NULL, 50, -1, NULL, N'Unique_ID', 6, -1, N'Unique_ID', -1, -100)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1004, 0, 0, 0, N'', N'', 0, 16, 50, 0, N'', N'Previous iBase ID', 0, 0, N'AccountID_', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1005, 0, 0, 0, N'', N'', -1, 0, 50, 0, N'', N'Account Number', 0, 0, N'Account_Number', -1, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1006, 0, 0, 0, N'', N'', -1, 1, 10, 0, N'', N'Sort Code', 0, 0, N'Sort_Code', -1, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1048, 0, 0, 1007, N'', N'', 0, 2, 255, 0, N'', N'Bank', 7, 0, N'Bank_', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1049, 0, 0, 0, N'', N'', 0, 0, 50, 0, N'', N'Source', 0, 0, N'Source_411765484', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1050, 0, 0, 0, N'', N'', 0, 4, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1051, 0, 0, 0, N'', N'', 0, 1, 50, 0, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1052, 0, 0, 0, N'', N'', 0, 5, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1053, 0, 0, 0, N'', N'', 0, 2, 0, 0, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1054, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1055, 0, 0, 0, N'', N'', 0, 3, 0, 0, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1056, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1057, 0, 0, 0, N'', N'', 0, 4, 255, 0, N'', N'Created User', 13, 0, N'Create_User', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1058, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1059, 0, 0, 0, N'', N'', 0, 5, 0, 0, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1060, 0, 0, 0, N'', N'', 0, 10, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1061, 0, 0, 0, N'', N'', 0, 6, 255, 0, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1062, 0, 0, 0, N'', N'', 0, 11, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1063, 0, 0, 0, N'', N'', 0, 7, 255, 0, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1064, 0, 0, 0, N'', N'', 0, 15, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1066, 0, 0, 0, N'', N'', 0, 16, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1067, 0, 0, 0, N'', N'', 0, 17, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1068, 0, 0, 0, N'', N'', 0, 18, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1069, 0, 0, 0, N'', N'', 0, 20, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1070, 0, 0, 0, N'', N'', 0, 21, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1071, 0, 0, 0, N'', N'', 0, 22, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1072, 0, 0, 0, N'', N'', 0, 23, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1073, 0, 0, 0, N'', N'', 0, 27, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1074, 0, 0, 0, N'', N'', 0, 28, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Address_ID', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1075, 0, 0, 0, N'', N'', -1, 0, 50, 0, N'', N'Sub Building', 0, 0, N'Sub_Building', -1, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1076, 0, 0, 0, N'', N'', 0, 1, 50, 0, N'', N'Building', 0, 0, N'Building_', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1077, 0, 0, 0, N'', N'', -1, 2, 50, 0, N'', N'Building Number', 0, 0, N'Building_Number', -1, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1078, 0, 0, 0, N'', N'', -1, 3, 255, 0, N'', N'Street', 0, 0, N'Street_', -1, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1079, 0, 0, 0, N'', N'', 0, 4, 50, 0, N'', N'Locality', 0, 0, N'Locality_', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1080, 0, 0, 0, N'', N'', -1, 5, 50, 0, N'', N'Town', 0, 0, N'Town_', -1, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1161, 0, 0, 1081, N'', N'', 0, 6, 255, 0, N'', N'County', 7, 0, N'County_', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1162, 0, 0, 0, N'', N'', -1, 7, 50, 0, N'', N'Post Code', 0, 0, N'Post_Code', -1, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1163, 0, 0, 0, N'', N'', 0, 8, 50, 0, N'', N'DX Number', 0, 0, N'DX_Number', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1164, 0, 0, 0, N'', N'', 0, 9, 150, 0, N'', N'DX Exchange', 0, 0, N'DX_Exchange', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1168, 0, 0, 1165, N'', N'', 0, 10, 255, 0, N'', N'Address Type', 7, 0, N'Address_Type', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1169, 0, 0, 0, N'', N'', 0, 11, 50, 0, N'', N'Grid X', 0, 0, N'Grid_X', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1170, 0, 0, 0, N'', N'', 0, 12, 50, 0, N'', N'Grid Y', 0, 0, N'Grid_Y', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1171, 0, 4021, 0, N'', N'', 0, 13, 0, 0, N'', N'PAF Validation', 6, 0, N'PAF_Validation', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1175, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1176, 0, 0, 0, N'', N'', 0, 4, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1177, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1178, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1179, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1180, 0, 0, 0, N'', N'', 0, 9, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1181, 0, 0, 0, N'', N'', 0, 10, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1182, 0, 0, 0, N'', N'', 0, 14, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1183, 0, 0, 0, N'', N'', 0, 15, 50, 0, N'', N'Previous iBase ID', 0, 0, N'BB_Pin_ID', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1184, 0, 0, 0, N'', N'', -1, 0, 50, 0, N'', N'BB Pin', 0, 0, N'BB_Pin', -1, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1185, 0, 0, 0, N'', N'', -1, 1, 50, 0, N'', N'Display Name', 0, 0, N'Display_Name', -1, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1187, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1188, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1189, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1190, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1191, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1192, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1193, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1194, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1195, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Driving_Licence_ID', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1196, 0, 0, 0, N'', N'', -1, 0, 50, 0, N'Upper Case', N'Driver Number', 0, 0, N'Driving_Licence_Number', -1, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1198, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1199, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1200, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1201, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1202, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1203, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1204, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1205, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1206, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Email_ID', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1207, 0, 0, 0, N'', N'', -1, 0, 255, 0, N'', N'Email Address', 0, 0, N'Email_Address', -1, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1209, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1210, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1211, 0, 0, 0, N'', N'', 0, 12, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1212, 0, 0, 0, N'', N'', 0, 14, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1213, 0, 0, 0, N'', N'', 0, 15, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1214, 0, 0, 0, N'', N'', 0, 16, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1215, 0, 0, 0, N'', N'', 0, 17, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1216, 0, 0, 0, N'', N'', 0, 21, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1217, 0, 0, 0, N'', N'', 0, 22, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Fraud_Ring_ID', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1679, 0, 0, 1218, N'', N'', 0, 0, 255, 0, N'', N'Fraud Ring Name', 7, 0, N'Fraud_Ring_Name', -1, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1684, 0, 0, 1681, N'', N'', 0, 1, 255, 0, N'', N'Status', 7, 0, N'Status_', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1685, 0, 0, 1680, N'', N'', 0, 2, 255, 0, N'', N'Lead Analyst', 7, 0, N'Lead_Analyst', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1686, 0, 0, 1680, N'', N'', 0, 3, 255, 0, N'', N'Champion', 7, 0, N'Champion_', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1689, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1690, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1691, 0, 0, 0, N'', N'', 0, 12, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1692, 0, 0, 0, N'', N'', 0, 14, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1693, 0, 0, 0, N'', N'', 0, 15, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1694, 0, 0, 0, N'', N'', 0, 16, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1695, 0, 0, 0, N'', N'', 0, 17, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1696, 0, 0, 0, N'', N'', 0, 21, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1697, 0, 0, 0, N'', N'', 0, 22, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Incident_ID', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1698, 0, 2011, 0, N'', N'', -1, 0, 50, 0, N'', N'Keoghs Elite Reference', 0, 0, N'Keoghs_Elite_Reference', -1, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1716, 0, 0, 1699, N'', N'', 0, 1, 255, 0, N'', N'Incident Type', 7, 0, N'Incident_Type', -1, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1717, 0, 0, 1713, N'Incident', N'', 0, 2, 255, 0, N'', N'Incident Icon', 22, 0, N'AltEntity', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1718, 0, 0, 0, N'', N'', 0, 4, 50, 0, N'', N'IFB Reference', 0, 0, N'IFB_Reference', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1719, 0, 0, 0, N'', N'', -1, 5, 0, 0, N'Short Date', N'Incident Date', 2, 0, N'Incident_Date', -1, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1737, 0, 1721, 1722, N'', N'', 0, 3, 255, 0, N'', N'Claim Type', 7, 0, N'Claim_Type', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1740, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1741, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1742, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1743, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1744, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1745, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1746, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1747, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1748, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'IP_Address_ID', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1749, 0, 0, 0, N'', N'', -1, 0, 20, 0, N'', N'IP Address', 0, 0, N'IP_Address', -1, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1751, 0, 0, 0, N'', N'', 0, 16, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1752, 0, 0, 0, N'', N'', 0, 17, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1753, 0, 0, 0, N'', N'', 0, 18, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1754, 0, 0, 0, N'', N'', 0, 20, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1755, 0, 0, 0, N'', N'', 0, 21, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1756, 0, 0, 0, N'', N'', 0, 22, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1757, 0, 0, 0, N'', N'', 0, 23, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1758, 0, 0, 0, N'', N'', 0, 27, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1759, 0, 0, 0, N'', N'', 0, 28, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Keoghs_Case_ID', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1760, 0, 0, 0, N'', N'', -1, 1, 50, 0, N'', N'Keoghs Elite Reference', 0, 0, N'Keoghs_Elite_Reference', -1, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1765, 0, 0, 1761, N'', N'', 0, 2, 255, 0, N'', N'Keoghs Office', 7, 0, N'Keoghs_Office', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1766, 0, 0, 1680, N'', N'', 0, 3, 255, 0, N'', N'Fee Earner', 7, 0, N'Fee_Earner', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1874, 0, 0, 1767, N'', N'', 0, 4, 255, 0, N'', N'Client', 7, 0, N'Clients_', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1875, 0, 0, 0, N'', N'', 0, 5, 50, 0, N'', N'Client Reference', 0, 0, N'Client_Reference', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1876, 0, 0, 0, N'', N'', 0, 6, 50, 0, N'', N'Client Claims Handler', 0, 0, N'Client_Claims_Handler', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1880, 0, 0, 1877, N'', N'', 0, 7, 255, 0, N'', N'Case Status', 7, 0, N'Case_Status', -1, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1881, 0, 0, 0, N'', N'', 0, 9, 0, 0, N'Short Date', N'Closure Date', 2, 0, N'Closure_Date', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1883, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1884, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1885, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1886, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1887, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1888, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1889, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1890, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1891, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'NI_Number_ID', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1892, 0, 0, 0, N'', N'', -1, 0, 10, 0, N'Upper Case', N'NI Number', 0, 0, N'NI_Number', -1, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1894, 0, 0, 0, N'', N'', 0, 18, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1895, 0, 0, 0, N'', N'', 0, 19, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1896, 0, 0, 0, N'', N'', 0, 20, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1897, 0, 0, 0, N'', N'', 0, 22, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1898, 0, 0, 0, N'', N'', 0, 23, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1899, 0, 0, 0, N'', N'', 0, 24, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1900, 0, 0, 0, N'', N'', 0, 25, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1901, 0, 0, 0, N'', N'', 0, 30, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1902, 0, 0, 0, N'', N'', -1, 0, 255, 0, N'', N'Organisation Name', 0, 0, N'Organisation_Name', -1, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1903, 0, 0, 0, N'', N'', 0, 31, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Organisation_ID', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1914, 0, 0, 1904, N'', N'', 0, 1, 255, 0, N'', N'Organisation Type', 7, 0, N'Organisation_Type', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1920, 0, 0, 1915, N'Office', N'', 0, 2, 255, 0, N'', N'Organisation Icon', 22, 0, N'AltEntity', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1921, 0, 0, 0, N'', N'', 0, 3, 0, 0, N'Short Date', N'Incorporated Date', 2, 0, N'Incorporated_Date', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1927, 0, 0, 1922, N'', N'', 0, 5, 255, 0, N'', N'Organisation Status', 7, 0, N'Organisation_Status', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1928, 0, 0, 0, N'', N'', 0, 6, 0, 0, N'Short Date', N'Effective Date of Status', 2, 0, N'Effective_Date_of_Status', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1929, 0, 0, 0, N'', N'', -1, 7, 50, 0, N'', N'Registered Number', 0, 0, N'Registered_Number', -1, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1930, 0, 0, 0, N'', N'', 0, 8, 50, 0, N'', N'SIC', 0, 0, N'SIC_', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1931, 0, 0, 0, N'', N'', 0, 9, 50, 0, N'', N'VAT Number', 0, 0, N'VAT_Number', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1932, 0, 0, 0, N'', N'', 0, 10, 0, 0, N'Short Date', N'VAT Number Validated Date', 2, 0, N'VAT_Number_Validated_Date', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1933, 0, 0, 0, N'', N'', 0, 11, 10, 0, N'', N'MoJ CRM Number', 0, 0, N'MoJ_CRM_Number', -1, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1938, 0, 0, 1934, N'', N'', 0, 12, 255, 0, N'', N'MoJ CRM Status', 7, 0, N'MoJ_CRM_Status', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1939, 0, 0, 0, N'', N'', 0, 13, 50, 0, N'', N'Credit Licence', 0, 0, N'Credit_Licence', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1941, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1942, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1943, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1944, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1945, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1946, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1947, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1948, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1949, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Passport_ID', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1950, 0, 0, 0, N'', N'', -1, 0, 50, 0, N'Upper Case', N'Passport Number', 0, 0, N'Passport_Number', -1, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1952, 0, 0, 0, N'', N'', 0, 5, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1953, 0, 0, 0, N'', N'', 0, 6, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1954, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1955, 0, 0, 0, N'', N'', 0, 9, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1956, 0, 0, 0, N'', N'', 0, 10, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1957, 0, 0, 0, N'', N'', 0, 11, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1958, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1959, 0, 0, 0, N'', N'', 0, 16, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1960, 0, 0, 0, N'', N'', 0, 17, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Payment_Card_ID', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1961, 0, 0, 0, N'', N'', -1, 0, 50, 0, N'', N'Payment Card Number', 0, 0, N'Payment_Card_Number', -1, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1963, 0, 0, 0, N'', N'', 0, 1, 8, 0, N'', N'Sort Code', 0, 0, N'Sort_Code', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1964, 0, 0, 1007, N'', N'', 0, 2, 255, 0, N'', N'Bank', 7, 0, N'Bank_', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1968, 0, 0, 1965, N'', N'', 0, 3, 255, 0, N'', N'Payment Card Type', 7, 0, N'Payment_Card_Type', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1970, 0, 0, 0, N'', N'', 0, 19, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1971, 0, 0, 0, N'', N'', 0, 20, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1972, 0, 0, 0, N'', N'', 0, 21, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1973, 0, 0, 0, N'', N'', 0, 23, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1974, 0, 0, 0, N'', N'', 0, 24, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1975, 0, 0, 0, N'', N'', 0, 25, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1976, 0, 0, 0, N'', N'', 0, 26, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1977, 0, 0, 0, N'', N'', 0, 30, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1978, 0, 0, 0, N'', N'', 0, 31, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Person_ID', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1993, 0, 0, 1979, N'', N'', 0, 0, 255, 0, N'', N'Salutation', 7, 0, N'Salutation_', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1994, 0, 0, 0, N'', N'', -1, 1, 100, 0, N'', N'First Name', 0, 0, N'First_Name', -1, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1995, 0, 0, 0, N'', N'', 0, 2, 100, 0, N'', N'Middle Name', 0, 0, N'Middle_Name', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1996, 0, 0, 0, N'', N'', -1, 3, 100, 0, N'Upper Case', N'Last Name', 0, 0, N'Last_Name', -1, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (1997, 0, 2013, 0, N'', N'', -1, 4, 0, 0, N'Short Date', N'Date of Birth', 2, 0, N'Date_of_Birth', -1, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2002, 0, 0, 1998, N'', N'', 0, 5, 255, 0, N'', N'Gender', 7, 0, N'Gender_', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2008, 0, 0, 2003, N'Person', N'', 0, 6, 255, 0, N'', N'Person Icon', 22, 0, N'AltEntity', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2010, 0, 2009, 1218, N'', N'', 0, 8, 255, 0, N'', N'Fraud Ring Name', 7, 0, N'Fraud_Ring_Name', -1, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2253, 0, 0, 2014, N'', N'', 0, 7, 255, 0, N'', N'Nationality', 7, 0, N'Nationality_', -1, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2254, 0, 0, 0, N'', N'', 0, 8, 255, 0, N'', N'Occupation', 0, 0, N'Occupation_', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2255, 0, 0, 0, N'', N'', 0, 9, 20, 0, N'', N'Taxi Driver Licence', 0, 0, N'Taxi_Driver_Licence', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2256, 0, 0, 0, N'', N'', 0, 11, 150, 0, N'', N'Schools', 0, 0, N'Schools_', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2257, 0, 0, 0, N'', N'', 0, 12, 150, 0, N'', N'Hobbies', 0, 0, N'Hobbies_', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2258, 0, 0, 0, N'', N'', 0, 13, 0, 0, N'', N'Notes', 10, 0, N'Notes_', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2259, 0, 0, 0, N'', N'', 0, 14, 0, 0, N'', N'Picture', 25, 0, N'Picture_', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2262, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2263, 0, 0, 0, N'', N'', 0, 13, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2264, 0, 0, 0, N'', N'', 0, 14, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2265, 0, 0, 0, N'', N'', 0, 16, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2266, 0, 0, 0, N'', N'', 0, 17, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2267, 0, 0, 0, N'', N'', 0, 18, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2268, 0, 0, 0, N'', N'', 0, 19, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2269, 0, 0, 0, N'', N'', 0, 23, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2270, 0, 0, 0, N'', N'', 0, 24, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Policy_ID', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2271, 0, 0, 0, N'', N'', -1, 0, 50, 0, N'', N'Policy Number', 0, 0, N'Policy_Number', -1, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2272, 0, 0, 0, N'', N'', 0, 1, 50, 0, N'', N'Insurer', 0, 0, N'Insurer_', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2273, 0, 0, 0, N'', N'', 0, 2, 50, 0, N'', N'Insurer Trading Name', 0, 0, N'Insurer_Trading_Name', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2274, 0, 0, 0, N'', N'', 0, 3, 50, 0, N'', N'Broker', 0, 0, N'Broker_', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2281, 0, 2282, 2275, N'', N'', 0, 4, 255, 0, N'', N'Policy Type', 7, 0, N'Policy_Type', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2290, 0, 2289, 2284, N'', N'', 0, 5, 255, 0, N'', N'Cover Type', 7, 0, N'Cover_Type', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2291, 0, 0, 0, N'', N'', 0, 6, 0, 0, N'Short Date', N'Policy Start Date', 2, 0, N'Policy_Start_Date', -1, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2292, 0, 0, 0, N'', N'', 0, 7, 0, 0, N'Short Date', N'Policy End Date', 2, 0, N'Policy_End_Date', -1, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2296, 0, 0, 0, N'', N'', 0, 9, 10, 0, N'', N'Previous Non-Fault Claims Disclosed', 0, 0, N'Previous_Claims_Disclosed', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2297, 0, 0, 0, N'', N'', 0, 10, 10, 0, N'', N'Premium', 0, 0, N'Premium_', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2299, 0, 0, 0, N'', N'', 0, 6, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2300, 0, 0, 0, N'', N'', 0, 7, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2301, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2302, 0, 0, 0, N'', N'', 0, 10, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2303, 0, 0, 0, N'', N'', 0, 11, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2304, 0, 0, 0, N'', N'', 0, 12, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2305, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2306, 0, 0, 0, N'', N'', 0, 17, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2307, 0, 0, 0, N'', N'', 0, 18, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Telephone_ID', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2308, 0, 0, 0, N'', N'', -1, 0, 50, 0, N'', N'Telephone Number', 0, 0, N'Telephone_Number', -1, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2309, 0, 0, 0, N'', N'', 0, 1, 10, 0, N'', N'Country Code', 0, 0, N'Country_Code', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2310, 0, 0, 0, N'', N'', 0, 2, 10, 0, N'', N'STD Code', 0, 0, N'STD_Code', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2316, 0, 0, 2311, N'', N'', 0, 3, 255, 0, N'', N'Telephone Type', 7, 0, N'Telephone_Type', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2320, 0, 0, 2317, N'Telephone', N'', 0, 4, 255, 0, N'', N'Telephone Icon', 22, 0, N'AltEntity', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2322, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2323, 0, 0, 0, N'', N'', 0, 13, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2324, 0, 0, 0, N'', N'', 0, 14, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2325, 0, 0, 0, N'', N'', 0, 16, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2326, 0, 0, 0, N'', N'', 0, 17, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2327, 0, 0, 0, N'', N'', 0, 18, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2328, 0, 0, 0, N'', N'', 0, 19, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2329, 0, 0, 0, N'', N'', 0, 23, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2330, 0, 0, 0, N'', N'', 0, 24, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Vehicle_ID', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2332, 0, 0, 0, N'', N'', -1, 0, 20, 0, N'Upper Case', N'Vehicle Registration', 0, 0, N'Vehicle_Registration', -1, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2427, 0, 0, 2405, N'', N'', 0, 3, 255, 0, N'', N'Colour', 7, 0, N'Colour_', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2428, 0, 0, 2333, N'', N'', 0, 1, 255, 0, N'', N'Make', 7, 0, N'Make_', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2429, 0, 0, 0, N'', N'', 0, 2, 50, 0, N'', N'Model', 0, 0, N'Model_', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2430, 0, 0, 0, N'', N'', 0, 4, 50, 0, N'', N'Engine Capacity', 0, 0, N'Engine_Capacity', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2431, 0, 0, 0, N'', N'', 0, 5, 50, 0, N'', N'VIN', 0, 0, N'VIN_', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2436, 0, 0, 2432, N'', N'', 0, 6, 255, 0, N'', N'Fuel', 7, 0, N'Fuel_', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2441, 0, 0, 2437, N'', N'', 0, 7, 255, 0, N'', N'Transmission', 7, 0, N'Transmission_', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2449, 0, 0, 2442, N'', N'', 0, 8, 255, 0, N'', N'Vehicle Type', 7, 0, N'Vehicle_Type', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2461, 0, 0, 2450, N'Car', N'', 0, 9, 255, 0, N'', N'Vehicle Icon', 22, 0, N'AltEntity', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2462, 0, 0, 0, N'', N'', 0, 10, 0, 0, N'', N'Notes', 10, 0, N'Notes_', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2465, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2466, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2467, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2468, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2469, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2470, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2471, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2472, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2473, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Website_ID', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2474, 0, 0, 0, N'', N'', -1, 0, 255, 0, N'', N'URL', 0, 0, N'Website_', -1, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2476, 0, 0, 0, N'', N'', 0, 5, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2477, 0, 0, 0, N'', N'', 0, 6, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2478, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2479, 0, 0, 0, N'', N'', 0, 9, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2480, 0, 0, 0, N'', N'', 0, 10, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2481, 0, 0, 0, N'', N'', 0, 11, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2482, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2483, 0, 0, 0, N'', N'', 0, 16, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2484, 0, 0, 0, N'', N'', 0, 17, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Address_to_Address_Link_ID', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2486, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2487, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2488, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2489, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2490, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2491, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2492, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2493, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2499, 0, 0, 2494, N'', N'', 0, 0, 255, 0, N'', N'Address Link Type', 7, 0, N'Address_Link_Type', -1, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2500, 0, 3126, 0, N'', N'', 0, 1, 0, 0, N'Short Date', N'Start of Residency', 2, 0, N'Start_of_Residency', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2501, 0, 3127, 0, N'', N'', 0, 2, 0, 0, N'Short Date', N'End of Residency', 2, 0, N'End_of_Residency', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2502, 0, 0, 0, N'', N'', 0, 3, 0, 0, N'', N'Notes', 10, 0, N'Notes_', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2506, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Case_Incident_Link_ID', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2507, 0, 0, 2503, N'', N'', 0, 0, 255, 0, N'', N'Link Type', 7, 0, N'Link_Type', -1, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2509, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2510, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2511, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2512, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2513, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2514, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2515, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2516, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2517, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Fraud_Ring_Incident_Link_ID', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2518, 0, 0, 2503, N'', N'', 0, 0, 255, 0, N'', N'Link Type', 7, 0, N'Link_Type', -1, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2520, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2521, 0, 0, 0, N'', N'', 0, 4, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2522, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2523, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2524, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2525, 0, 0, 0, N'', N'', 0, 9, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2526, 0, 0, 0, N'', N'', 0, 10, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2527, 0, 0, 0, N'', N'', 0, 14, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2528, 0, 0, 0, N'', N'', 0, 15, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Incident_Match_Link_ID', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2596, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2597, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2598, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2599, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2600, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2601, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2602, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2603, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2604, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Organisation_Match_Link_ID', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2612, 0, 0, 2605, N'', N'', 0, 0, 255, 0, N'', N'Link Type', 7, 0, N'Link_Type', -1, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2633, 0, 0, 0, N'', N'', 0, 1, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2634, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2635, 0, 0, 0, N'', N'', 0, 3, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2636, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2637, 0, 0, 0, N'', N'', 0, 6, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2638, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2639, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2640, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2641, 0, 0, 0, N'', N'', 0, 13, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Account_Link_ID', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2643, 0, 0, 0, N'', N'', 0, 1, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2644, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2645, 0, 0, 0, N'', N'', 0, 3, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2646, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2647, 0, 0, 0, N'', N'', 0, 6, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2648, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2649, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2650, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2651, 0, 0, 0, N'', N'', 0, 13, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Email_Link_ID', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2653, 0, 0, 0, N'', N'', 0, 1, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2654, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2655, 0, 0, 0, N'', N'', 0, 3, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2656, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2657, 0, 0, 0, N'', N'', 0, 6, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2658, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2659, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2660, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2661, 0, 0, 0, N'', N'', 0, 13, 50, 0, N'', N'Previous iBase ID', 0, 0, N'IP_Address_Link_ID', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2663, 0, 0, 0, N'', N'', 0, 1, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2664, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2665, 0, 0, 0, N'', N'', 0, 3, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2666, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2667, 0, 0, 0, N'', N'', 0, 6, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2668, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2669, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2670, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2671, 0, 0, 0, N'', N'', 0, 13, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Payment_Card_Link_ID', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2673, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2674, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2675, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2676, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2677, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2678, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2679, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2680, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2681, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Telephone_Link_ID', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2683, 0, 0, 0, N'', N'', 0, 1, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2684, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2685, 0, 0, 0, N'', N'', 0, 3, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2686, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2687, 0, 0, 0, N'', N'', 0, 6, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2688, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2689, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2690, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2691, 0, 0, 0, N'', N'', 0, 13, 50, 0, N'', N'Previous iBase ID', 0, 0, N'BB_Pin_Link_ID', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2693, 0, 0, 0, N'', N'', 0, 1, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2694, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2695, 0, 0, 0, N'', N'', 0, 3, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2696, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2697, 0, 0, 0, N'', N'', 0, 6, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2698, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2699, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2700, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2701, 0, 0, 0, N'', N'', 0, 13, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Driving_Licence_Link_ID', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2703, 0, 0, 0, N'', N'', 0, 40, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2704, 0, 0, 0, N'', N'', 0, 41, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2705, 0, 0, 0, N'', N'', 0, 42, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2706, 0, 0, 0, N'', N'', 0, 44, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2707, 0, 0, 0, N'', N'', 0, 45, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2708, 0, 0, 0, N'', N'', 0, 46, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2709, 0, 0, 0, N'', N'', 0, 47, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2710, 0, 0, 0, N'', N'', 0, 51, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2711, 0, 0, 0, N'', N'', 0, 52, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Incident_Link_ID', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2712, 0, 0, 2575, N'', N'', 0, 0, 255, 0, N'', N'Party Type', 7, 0, N'Party_Type', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2718, 0, 0, 2713, N'', N'', 0, 1, 255, 0, N'', N'Sub Party Type', 7, 0, N'Sub_Party_Type', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2720, 0, 0, 0, N'', N'', 0, 3, 10, 0, N'', N'Incident Time', 0, 0, N'Incident_Time', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2721, 0, 0, 0, N'', N'', 0, 2, 0, 0, N'', N'Incident Circs', 10, 0, N'Incident_Circs', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2722, 0, 0, 0, N'', N'', 0, 5, 255, 0, N'', N'Insurer', 0, 0, N'Insurer_', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2723, 0, 0, 0, N'', N'', 0, 6, 50, 0, N'', N'Claim Number', 0, 0, N'Claim_Number', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2724, 0, 0, 0, N'', N'', 0, 7, 50, 0, N'', N'Claim Status', 0, 0, N'Claim_Status', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2725, 0, 0, 0, N'', N'', 0, 8, 50, 0, N'', N'Claim Type', 0, 0, N'Claim_Type', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2726, 0, 0, 0, N'', N'', 0, 9, 50, 0, N'', N'Claim Code', 0, 0, N'Claim_Code', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2727, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'MoJ Status', 0, 0, N'MoJ_Status', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2728, 0, 0, 0, N'', N'', 0, 15, 0, 0, N'Short Date', N'Claim Notification Date', 2, 0, N'Claim_Notification_Date', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2729, 0, 0, 0, N'', N'', 0, 16, 255, 0, N'', N'Broker', 0, 0, N'Broker_', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2730, 0, 0, 0, N'', N'', 0, 17, 255, 0, N'', N'Referral Source', 0, 0, N'Referral_Source', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2731, 0, 0, 0, N'', N'', 0, 18, 255, 0, N'', N'Solicitors', 0, 0, N'Solicitors_', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2732, 0, 0, 0, N'', N'', 0, 19, 255, 0, N'', N'Engineer', 0, 0, N'Engineer_', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2733, 0, 0, 0, N'', N'', 0, 20, 255, 0, N'', N'Recovery', 0, 0, N'Recovery_', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2734, 0, 0, 0, N'', N'', 0, 22, 255, 0, N'', N'Storage', 0, 0, N'Storage_', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2735, 0, 0, 0, N'', N'', 0, 23, 255, 0, N'', N'Storage Address', 0, 0, N'Storage_Address', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2736, 0, 0, 0, N'', N'', 0, 24, 255, 0, N'', N'Repairer', 0, 0, N'Repairer_', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2737, 0, 0, 0, N'', N'', 0, 26, 255, 0, N'', N'Hire', 0, 0, N'Hire_', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2738, 0, 0, 0, N'', N'', 0, 27, 255, 0, N'', N'Accident Management', 0, 0, N'Accident_Management', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2739, 0, 0, 0, N'', N'', 0, 28, 255, 0, N'', N'Medical Examiner', 0, 0, N'Medical_Examiner', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2740, 0, 0, 0, N'', N'', 0, 32, 0, 0, N'', N'Police Attended', 6, 0, N'Police_Attended', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2741, 0, 0, 0, N'', N'', 0, 33, 50, 0, N'', N'Police Force', 0, 0, N'Police_Force', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2742, 0, 0, 0, N'', N'', 0, 34, 50, 0, N'', N'Police Reference', 0, 0, N'Police_Reference', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2743, 0, 0, 0, N'', N'', 0, 35, 0, 0, N'', N'Ambulance Attended', 6, 0, N'Ambulance_Attended', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2747, 0, 0, 0, N'', N'', 0, 1, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2748, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2749, 0, 0, 0, N'', N'', 0, 3, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2750, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2751, 0, 0, 0, N'', N'', 0, 6, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2752, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2753, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2754, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2755, 0, 0, 0, N'', N'', 0, 13, 50, 0, N'', N'Previous iBase ID', 0, 0, N'NI_Number_Link_ID', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2757, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2758, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2759, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2760, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2761, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2762, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2763, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2764, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2765, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Policy_Link_ID', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2770, 0, 0, 2766, N'', N'', 0, 0, 255, 0, N'', N'Link Type', 7, 0, N'Link_Type', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2772, 0, 0, 0, N'', N'', 0, 1, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2773, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2774, 0, 0, 0, N'', N'', 0, 3, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2775, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2776, 0, 0, 0, N'', N'', 0, 6, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2777, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2778, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2779, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2780, 0, 0, 0, N'', N'', 0, 13, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Website_Link_ID', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2782, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2783, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2784, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2785, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2786, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2787, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2788, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2789, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2790, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Person_to_Organisation_Link_ID', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2812, 0, 0, 2791, N'', N'', 0, 0, 255, 0, N'', N'Link Type', 7, 0, N'Link_Type', -1, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2824, 0, 0, 0, N'', N'', 0, 1, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2825, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2826, 0, 0, 0, N'', N'', 0, 3, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2827, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2828, 0, 0, 0, N'', N'', 0, 6, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2829, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2830, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2831, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2832, 0, 0, 0, N'', N'', 0, 13, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Passport_Link_ID', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2834, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2835, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2836, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2837, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2838, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2839, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2840, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2841, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2842, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Person_to_Person_Link_ID', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2862, 0, 0, 2843, N'', N'', 0, 0, 255, 0, N'', N'Link Type', 7, 0, N'Link_Type', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2866, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2867, 0, 0, 0, N'', N'', 0, 4, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2868, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2869, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2870, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2871, 0, 0, 0, N'', N'', 0, 9, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2872, 0, 0, 0, N'', N'', 0, 10, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2873, 0, 0, 0, N'', N'', 0, 14, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2874, 0, 0, 0, N'', N'', 0, 15, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Policy_Payment_Link_ID', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2875, 0, 0, 0, N'', N'', 0, 0, 50, 0, N'', N'Payment Type', 0, 0, N'Payment_Type', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2876, 0, 0, 0, N'', N'', 0, 1, 0, 0, N'Short Date', N'Date Payment Details Taken', 2, 0, N'Date_payment_details_taken', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2878, 0, 0, 0, N'', N'', 0, 5, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2879, 0, 0, 0, N'', N'', 0, 6, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2880, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2881, 0, 0, 0, N'', N'', 0, 9, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2882, 0, 0, 0, N'', N'', 0, 10, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2883, 0, 0, 0, N'', N'', 0, 11, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2884, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2885, 0, 0, 0, N'', N'', 0, 16, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2886, 0, 0, 0, N'', N'', 0, 17, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Vehicle_Incident_Link_ID', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2893, 0, 0, 2887, N'', N'', 0, 2, 255, 0, N'', N'Link Type', 7, 0, N'Link_Type', -1, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2900, 0, 2899, 2894, N'', N'', 0, 3, 255, 0, N'', N'Catery of Loss', 7, 0, N'Catery_of_Loss', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2902, 0, 0, 0, N'', N'', 0, 8, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2903, 0, 0, 0, N'', N'', 0, 9, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2904, 0, 0, 0, N'', N'', 0, 10, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2905, 0, 0, 0, N'', N'', 0, 12, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2906, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2907, 0, 0, 0, N'', N'', 0, 14, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2908, 0, 0, 0, N'', N'', 0, 15, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2909, 0, 0, 0, N'', N'', 0, 19, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2910, 0, 0, 0, N'', N'', 0, 20, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Vehicle_Link_ID', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2922, 0, 0, 2911, N'', N'', 0, 0, 255, 0, N'', N'Link Type', 7, 0, N'Link_Type', -1, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2923, 0, 3151, 0, N'', N'', 0, 1, 0, 0, N'Short Date', N'Reg Keeper Start Date', 2, 0, N'Reg_Keeper_Start_Date', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2924, 0, 3152, 0, N'', N'', 0, 2, 0, 0, N'Short Date', N'Reg Keeper End Date', 2, 0, N'Reg_Keeper_End_Date', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2925, 0, 3153, 0, N'', N'', 0, 3, 255, 0, N'', N'Hire Company', 0, 0, N'Hire_Company', -1, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2926, 0, 0, 0, N'', N'', 0, 4, 255, 0, N'', N'Cross-Hire Company', 0, 0, N'CrossHire_Company', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2927, 0, 3154, 0, N'', N'', 0, 5, 0, 0, N'Short Date', N'Hire Start Date', 2, 0, N'Hire_Start_Date', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2928, 0, 3155, 0, N'', N'', 0, 6, 0, 0, N'Short Date', N'Hire End Date', 2, 0, N'Hire_End_Date', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2931, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2932, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2933, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2934, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2935, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2936, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2937, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2938, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2939, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Vehicle_to_Vehicle_Link_ID', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2941, 0, 0, 0, N'', N'', 0, 0, 0, 0, N'Short Date', N'Date of Reg Change', 2, 0, N'Date_fo_Reg_Change', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2943, 0, 0, 0, N'', N'', 0, 9, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2944, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2945, 0, 0, 0, N'', N'', 0, 11, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2946, 0, 0, 0, N'', N'', 0, 13, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2947, 0, 0, 0, N'', N'', 0, 14, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2948, 0, 0, 0, N'', N'', 0, 15, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2949, 0, 0, 0, N'', N'', 0, 16, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2950, 0, 0, 0, N'', N'', 0, 20, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2951, 0, 0, 0, N'', N'', 0, 21, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Intelligence_ID', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2960, 0, 0, 2953, N'', N'', 0, 1, 255, 0, N'', N'Type', 7, 0, N'Type_', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2961, 0, 0, 0, N'', N'', 0, 3, 0, 0, N'', N'Picture', 25, 0, N'Picture_', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2962, 0, 0, 0, N'', N'', 0, 5, 0, 0, N'', N'Report', 10, 0, N'Report_', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2963, 0, 0, 0, N'', N'', 0, 6, 0, 0, N'', N'Web Page', 24, 0, N'Web_Page', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2964, 0, 0, 0, N'', N'', 0, 7, 0, 0, N'', N'Key Attractors', 10, 0, N'Key_Attractors', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2966, 0, 0, 0, N'', N'', 0, 0, 50, 0, N'', N'Description', 0, 0, N'Description_', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2968, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2969, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2970, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2971, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2972, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2973, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2974, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2975, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2976, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Intelligence_Link_ID', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (2977, 0, 0, 0, N'', N'', 0, 0, 0, 0, N'', N'Notes', 10, 0, N'Notes_', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3156, 0, 0, 0, N'', N'', 0, 4, 255, 0, N'', N'Incident Location', 0, 0, N'Incident_Location', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3162, 0, 0, 0, N'', N'', 0, 14, 0, 0, N'', N'Document Link', 26, 0, N'Document_Link', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3164, 0, 0, 0, N'', N'', 0, 4, 0, 0, N'', N'Briefing Document', 24, 0, N'Briefing_Document', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3165, 0, 0, 0, N'', N'', 0, 2, 0, 0, N'', N'Document', 26, 0, N'Document_', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3166, 0, 0, 0, N'', N'', 0, 15, 0, 0, N'', N'Document', 26, 0, N'Document_', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3167, 0, 0, 0, N'', N'', 0, 4, 0, 0, N'', N'Picture 2', 25, 0, N'Picture_2', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3168, 0, 0, 0, N'', N'', 0, 8, 50, 0, N'', N'Current Reserve', 0, 0, N'Current_Reserve', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3169, 0, 0, 0, N'', N'', 0, 10, 0, 0, N'Short Date', N'Weed Date', 2, 0, N'Weed_Date', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3630, 0, 0, 0, N'', N'', 0, 8, 10, 0, N'', N'Previous Fault Claims Disclosed', 0, 0, N'Previous_Fault_Claims_Disclosed', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3631, 0, 0, 0, N'', N'', 0, 37, 15, 0, N'', N'Reserve', 0, 0, N'Reserve_', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3632, 0, 0, 0, N'', N'', 0, 38, 15, 0, N'', N'Payments', 0, 0, N'Payments_', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3636, 0, 0, 3633, N'', N'', 0, 0, 255, 0, N'', N'Link Type', 7, 0, N'Link_Type', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3637, 0, 0, 0, N'', N'', 0, 16, 50, 0, N'', N'VF ID', 0, 0, N'VF_ID', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3638, 0, 0, 3171, N'', N'', 0, 8, 255, 0, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3639, 0, 0, 3171, N'', N'', 0, 7, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3640, 0, 0, 3171, N'', N'', 0, 19, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3641, 0, 0, 3171, N'', N'', 0, 6, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3642, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3643, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3644, 0, 0, 3171, N'', N'', 0, 13, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3645, 0, 0, 3171, N'', N'', 0, 13, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3646, 0, 0, 3171, N'', N'', 0, 12, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3647, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3648, 0, 0, 3171, N'', N'', 0, 19, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3649, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3650, 0, 0, 3171, N'', N'', 0, 21, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3651, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3652, 0, 0, 3171, N'', N'', 0, 8, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3653, 0, 0, 3171, N'', N'', 0, 22, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3654, 0, 0, 3171, N'', N'', 0, 15, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3655, 0, 0, 3171, N'', N'', 0, 9, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3656, 0, 0, 3171, N'', N'', 0, 15, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3657, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3658, 0, 0, 3171, N'', N'', 0, 4, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3659, 0, 0, 3171, N'', N'', 0, 8, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3660, 0, 0, 3171, N'', N'', 0, 4, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3661, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3662, 0, 0, 3171, N'', N'', 0, 4, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3663, 0, 0, 3171, N'', N'', 0, 4, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3664, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3665, 0, 0, 3171, N'', N'', 0, 43, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3666, 0, 0, 3171, N'', N'', 0, 6, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3667, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3668, 0, 0, 3171, N'', N'', 0, 4, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3669, 0, 0, 3171, N'', N'', 0, 4, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3670, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3671, 0, 0, 3171, N'', N'', 0, 4, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3672, 0, 0, 3171, N'', N'', 0, 4, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3673, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3674, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3675, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3676, 0, 0, 3171, N'', N'', 0, 6, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3677, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3678, 0, 0, 3171, N'', N'', 0, 8, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3679, 0, 0, 3171, N'', N'', 0, 11, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3680, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3681, 0, 0, 3171, N'', N'', 0, 4, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3682, 0, 0, 0, N'', N'', 0, 9, 100, 0, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3683, 0, 0, 0, N'', N'', 0, 3, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3684, 0, 0, 0, N'', N'', 0, 15, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3685, 0, 0, 0, N'', N'', 0, 2, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3686, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3687, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3688, 0, 0, 0, N'', N'', 0, 9, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3689, 0, 0, 0, N'', N'', 0, 9, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3690, 0, 0, 0, N'', N'', 0, 8, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3691, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3692, 0, 0, 0, N'', N'', 0, 15, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3693, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3694, 0, 0, 0, N'', N'', 0, 14, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3695, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3696, 0, 0, 0, N'', N'', 0, 4, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3697, 0, 0, 0, N'', N'', 0, 17, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3698, 0, 0, 0, N'', N'', 0, 11, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3699, 0, 0, 0, N'', N'', 0, 5, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3700, 0, 0, 0, N'', N'', 0, 11, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3701, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3702, 0, 0, 0, N'', N'', 0, 0, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3703, 0, 0, 0, N'', N'', 0, 4, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3704, 0, 0, 0, N'', N'', 0, 0, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3705, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3706, 0, 0, 0, N'', N'', 0, 0, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3707, 0, 0, 0, N'', N'', 0, 0, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3708, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3709, 0, 0, 0, N'', N'', 0, 39, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3710, 0, 0, 0, N'', N'', 0, 2, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3711, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3712, 0, 0, 0, N'', N'', 0, 0, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3713, 0, 0, 0, N'', N'', 0, 0, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3714, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3715, 0, 0, 0, N'', N'', 0, 0, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3716, 0, 0, 0, N'', N'', 0, 0, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3717, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3718, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3719, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3720, 0, 0, 0, N'', N'', 0, 2, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3721, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3722, 0, 0, 0, N'', N'', 0, 4, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3723, 0, 0, 0, N'', N'', 0, 7, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3724, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3725, 0, 0, 0, N'', N'', 0, 0, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3748, 0, 0, 0, N'No', N'', 0, 10, 0, 0, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3749, 0, 0, 0, N'No', N'', 0, 12, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3750, 0, 0, 0, N'No', N'', 0, 24, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3751, 0, 0, 0, N'No', N'', 0, 11, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3752, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3753, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3754, 0, 0, 0, N'No', N'', 0, 18, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3755, 0, 0, 0, N'No', N'', 0, 18, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3756, 0, 0, 0, N'No', N'', 0, 17, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3757, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3758, 0, 0, 0, N'No', N'', 0, 24, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3759, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3760, 0, 0, 0, N'No', N'', 0, 26, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3761, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3762, 0, 0, 0, N'No', N'', 0, 13, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3763, 0, 0, 0, N'No', N'', 0, 27, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3764, 0, 0, 0, N'No', N'', 0, 20, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3765, 0, 0, 0, N'No', N'', 0, 14, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3766, 0, 0, 0, N'No', N'', 0, 20, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3767, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3768, 0, 0, 0, N'No', N'', 0, 9, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3769, 0, 0, 0, N'No', N'', 0, 13, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3770, 0, 0, 0, N'No', N'', 0, 9, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3771, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3772, 0, 0, 0, N'No', N'', 0, 9, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3773, 0, 0, 0, N'No', N'', 0, 9, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3774, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3775, 0, 0, 0, N'No', N'', 0, 48, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3776, 0, 0, 0, N'No', N'', 0, 11, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3777, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3778, 0, 0, 0, N'No', N'', 0, 9, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3779, 0, 0, 0, N'No', N'', 0, 9, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3780, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3781, 0, 0, 0, N'No', N'', 0, 9, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3782, 0, 0, 0, N'No', N'', 0, 9, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3783, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3784, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3785, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3786, 0, 0, 0, N'No', N'', 0, 11, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3787, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3788, 0, 0, 0, N'No', N'', 0, 13, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3789, 0, 0, 0, N'No', N'', 0, 16, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3790, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3791, 0, 0, 0, N'No', N'', 0, 9, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3792, 0, 0, 0, N'', N'', 0, 11, 50, 0, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3793, 0, 0, 0, N'', N'', 0, 13, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3794, 0, 0, 0, N'', N'', 0, 25, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3795, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3796, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3797, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3798, 0, 0, 0, N'', N'', 0, 19, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3799, 0, 0, 0, N'', N'', 0, 19, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3800, 0, 0, 0, N'', N'', 0, 18, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3801, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3802, 0, 0, 0, N'', N'', 0, 25, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3803, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3804, 0, 0, 0, N'', N'', 0, 28, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3805, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3806, 0, 0, 0, N'', N'', 0, 14, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3807, 0, 0, 0, N'', N'', 0, 28, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3808, 0, 0, 0, N'', N'', 0, 21, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3809, 0, 0, 0, N'', N'', 0, 15, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3810, 0, 0, 0, N'', N'', 0, 21, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3811, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3812, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3813, 0, 0, 0, N'', N'', 0, 14, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3814, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3815, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3816, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3817, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3818, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3819, 0, 0, 0, N'', N'', 0, 49, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3820, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3821, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3822, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3823, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3824, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3825, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3826, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3827, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3828, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3829, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3830, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3831, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3832, 0, 0, 0, N'', N'', 0, 14, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3833, 0, 0, 0, N'', N'', 0, 17, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3834, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3835, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3837, 0, 0, 0, N'', N'', 0, 9, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3838, 0, 0, 0, N'', N'', 0, 10, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3839, 0, 0, 0, N'', N'', 0, 11, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3840, 0, 0, 0, N'', N'', 0, 13, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3841, 0, 0, 0, N'', N'', 0, 14, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3842, 0, 0, 0, N'', N'', 0, 15, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3843, 0, 0, 0, N'', N'', 0, 16, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3844, 0, 0, 0, N'', N'', 0, 20, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3845, 0, 0, 3171, N'', N'', 0, 12, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3846, 0, 0, 0, N'', N'', 0, 8, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3847, 0, 0, 0, N'No', N'', 0, 17, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3848, 0, 0, 0, N'', N'', 0, 18, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3849, 0, 0, 0, N'', N'', 0, 21, 50, 0, N'', N'Previous iBase ID', 0, 0, N'TOG_ID', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3850, 0, 0, 0, N'', N'', 0, 0, 50, 0, N'', N'TOG Reference', 0, 0, N'TOG_Reference', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3851, 0, 0, 1680, N'', N'', 0, 1, 255, 0, N'', N'Referred by', 7, 0, N'Referred_by', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3852, 0, 0, 0, N'', N'', 0, 2, 0, 0, N'Short Date', N'Date of Review', 2, 0, N'Date_of_Review', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3853, 0, 0, 0, N'', N'', 0, 3, 20, 0, N'', N'Decision', 0, 0, N'Decision_', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3854, 0, 0, 1680, N'', N'', 0, 4, 255, 0, N'', N'Allocated to', 7, 0, N'Allocated_to', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3855, 0, 0, 0, N'', N'', 0, 5, 0, 0, N'Short Date', N'Date Allocated', 2, 0, N'Date_Allocated', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3856, 0, 0, 0, N'', N'', 0, 6, 0, 0, N'Short Date', N'Date Completed', 2, 0, N'Date_Completed', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3857, 0, 0, 0, N'', N'', 0, 7, 0, 0, N'', N'Report', 24, 0, N'Report_', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3859, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3860, 0, 0, 0, N'', N'', 0, 13, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3861, 0, 0, 0, N'', N'', 0, 14, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3862, 0, 0, 0, N'', N'', 0, 16, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3863, 0, 0, 0, N'', N'', 0, 17, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3864, 0, 0, 0, N'', N'', 0, 18, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3865, 0, 0, 0, N'', N'', 0, 19, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3866, 0, 0, 0, N'', N'', 0, 23, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3867, 0, 0, 3171, N'', N'', 0, 15, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3868, 0, 0, 0, N'', N'', 0, 11, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3869, 0, 0, 0, N'No', N'', 0, 20, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3870, 0, 0, 0, N'', N'', 0, 21, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3871, 0, 0, 0, N'', N'', 0, 24, 50, 0, N'', N'Previous iBase ID', 0, 0, N'Outcome_Link_ID', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3872, 0, 0, 0, N'', N'', 0, 0, 0, 0, N'Short Date', N'Settlement Date', 2, 0, N'Settlement_Date', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3873, 0, 0, 0, N'', N'', 0, 1, 100, 0, N'', N'Manner of Resolution', 0, 0, N'Manner_of_Resolution', -1, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3874, 0, 0, 0, N'', N'', 0, 2, 100, 0, N'', N'Enforcement Role', 0, 0, N'Enforcement_Role', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3875, 0, 0, 0, N'', N'', 0, 3, 20, 0, N'', N'Property Owned', 0, 0, N'Property_Owned', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3876, 0, 0, 0, N'', N'', 0, 4, 100, 0, N'', N'Method of Enforcement', 0, 0, N'Method_of_Enforcement', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3877, 0, 0, 0, N'', N'', 0, 5, 0, 0, N'Short Date', N'Date Settlement Agreed', 2, 0, N'Date_Settlement_Agreed', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3878, 0, 0, 0, N'', N'', 0, 6, 20, 0, N'', N'Settlement Amount Agreed', 0, 0, N'Settlement_Amount_Agreed', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3879, 0, 0, 0, N'', N'', 0, 7, 20, 0, N'', N'Amount Recovered to Date', 0, 0, N'Amount_Recovered_to_Date', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3880, 0, 0, 0, N'', N'', 0, 8, 20, 0, N'', N'Amount Still Outstanding', 0, 0, N'Amount_Still_Outstanding', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3881, 0, 0, 0, N'', N'', 0, 9, 20, 0, N'', N'Was Enforcement Successful', 0, 0, N'Was_Enforcement_Successful', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3882, 0, 0, 0, N'', N'', 0, 10, 100, 0, N'', N'Type of Success', 0, 0, N'Type_of_Success', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3884, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3885, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3886, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3887, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3888, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3889, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3890, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3891, 0, 0, 0, N'', N'', 0, 13, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3892, 0, 0, 3171, N'', N'', 0, 5, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3893, 0, 0, 0, N'', N'', 0, 1, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3894, 0, 0, 0, N'No', N'', 0, 10, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3895, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3896, 0, 0, 0, N'', N'', 0, 14, 50, 0, N'', N'Previous iBase ID', 0, 0, N'TOG_Link_ID', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3897, 0, 0, 0, N'', N'', 0, 0, 0, 0, N'', N'Notes', 10, 0, N'Notes_', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3944, 0, 0, 0, N'', N'', 0, 31, 255, 0, N'', N'Undefined Supplier', 0, 0, N'Undefined_Supplier', -1, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3945, 0, 0, 0, N'No', N'', 0, 36, 0, 0, N'', N'Attended Hospital', 6, 0, N'Attended_Hospital', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3946, 0, 0, 0, N'', N'', 0, 6, 0, 0, N'Currency', N'Reserve', 5, 0, N'Reserve_', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3947, 0, 0, 0, N'', N'', 0, 7, 0, 0, N'Currency', N'Payments to date', 5, 0, N'Payments_', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3952, 0, 0, 0, N'', N'', 0, 12, 50, 0, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, -101)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3953, 0, 0, 0, N'', N'', 0, 14, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1000)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3954, 0, 0, 0, N'', N'', 0, 26, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1065)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3955, 0, 0, 0, N'', N'', 0, 13, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1174)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3956, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1186)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3957, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1197)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3958, 0, 0, 0, N'', N'', 0, 20, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3959, 0, 0, 0, N'', N'', 0, 20, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1688)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3960, 0, 0, 0, N'', N'', 0, 19, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2942)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3961, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1739)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3962, 0, 0, 0, N'', N'', 0, 26, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3963, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1882)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3964, 0, 0, 0, N'', N'', 0, 29, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3965, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1940)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3966, 0, 0, 0, N'', N'', 0, 15, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1951)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3967, 0, 0, 0, N'', N'', 0, 29, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3968, 0, 0, 0, N'', N'', 0, 22, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2261)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3969, 0, 0, 0, N'', N'', 0, 16, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2298)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3970, 0, 0, 0, N'', N'', 0, 19, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 3836)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3971, 0, 0, 0, N'', N'', 0, 22, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2321)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3972, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2464)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3973, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2632)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3974, 0, 0, 0, N'', N'', 0, 15, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2475)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3975, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2682)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3976, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2485)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3977, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2692)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3978, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2642)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3979, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2508)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3980, 0, 0, 0, N'', N'', 0, 50, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3981, 0, 0, 0, N'', N'', 0, 13, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3982, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2967)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3983, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2652)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3984, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2746)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3985, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2595)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3986, 0, 0, 0, N'', N'', 0, 22, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 3858)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3987, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2823)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3988, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2662)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3989, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2781)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3990, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2833)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3991, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2756)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3992, 0, 0, 0, N'', N'', 0, 13, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2865)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3993, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2672)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3994, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 3883)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3995, 0, 0, 0, N'', N'', 0, 15, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3996, 0, 0, 0, N'', N'', 0, 18, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2901)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3997, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2930)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (3998, 0, 0, 0, N'', N'', 0, 11, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 2771)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4000, 0, 0, 0, N'No', N'', 0, 15, 0, 0, N'', N'CHFKeyAttractor', 6, 0, N'CHFKeyAttractor_', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4001, 0, 0, 0, N'', N'', 0, 16, 0, 0, N'Short Date', N'CHFKeyAttractorRemoveDate', 2, 0, N'CHFKeyAttractorRemoveDate_', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4002, 0, 0, 0, N'', N'', 0, 29, 255, 0, N'', N'Medical Legal', 0, 0, N'Medical_Legal', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4003, 0, 0, 0, N'', N'', 0, 21, 255, 0, N'', N'Recovery Address', 0, 0, N'Recovery_Address', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4004, 0, 0, 0, N'', N'', 0, 25, 255, 0, N'', N'Repairer Address', 0, 0, N'Repairer_Address', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4005, 0, 0, 0, N'', N'', 0, 30, 255, 0, N'', N'Inspection Address', 0, 0, N'Inspection_Address', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4006, 0, 0, 0, N'', N'', 0, 0, 255, 0, N'', N'Data Type', 0, 0, N'Data_Type', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4007, 0, 0, 0, N'', N'', 0, 11, 0, 0, N'Short Date', N'Primary Weed Date', 2, 0, N'Primary_Weed_Date', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4008, 0, 0, 0, N'', N'', 0, 12, 100, 0, N'', N'Primary Weed Details', 0, 0, N'Primary_Weed_Details', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4009, 0, 0, 0, N'', N'', 0, 13, 0, 0, N'Short Date', N'Secondary Weed Date', 2, 0, N'Secondary_Weed_Date', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4010, 0, 0, 0, N'', N'', 0, 14, 100, 0, N'', N'Secondary Weed Details', 0, 0, N'Secondary_Weed_Details', 0, 1750)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4011, 0, 0, 0, N'', N'', 0, 5, 255, 0, N'', N'MO', 0, 0, N'MO_', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4012, 0, 0, 1767, N'', N'', 0, 6, 255, 0, N'', N'Owning Insurer', 7, 0, N'Owning_Insurer', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4013, 0, 0, 0, N'', N'', 0, 7, 0, 0, N'Short Date', N'Closed Date', 2, 0, N'Closed_Date', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4014, 0, 0, 0, N'', N'', 0, 8, 100, 0, N'', N'Industry Alternative Names', 0, 0, N'Industry_Alternative_Names', 0, 1208)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4015, 0, 0, 0, N'', N'', 0, 4, 0, 0, N'Short Date', N'Dissolved Date', 2, 0, N'Dissolved_Date', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4016, 0, 0, 0, N'', N'', 0, 27, 0, 0, N'', N'Document', 26, 0, N'Document_', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4020, 0, 0, 0, N'', N'', 0, 0, 255, 0, N'', N'Insurer', 0, 0, N'Insurer_', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4021, 0, 0, 0, N'', N'', 0, 1, 50, 0, N'', N'Insurer Ref', 0, 0, N'Insurer_Ref', 0, 2519)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4022, 0, 0, 0, N'', N'', 0, 0, 255, 0, N'', N'Insurer', 0, 0, N'Insurer_', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4023, 0, 0, 0, N'', N'', 0, 1, 50, 0, N'', N'Insurer Ref', 0, 0, N'Insurer_Ref', 0, 2877)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4024, 0, 0, 0, N'', N'', 0, 10, 50, 0, N'', N'NHS Number', 0, 0, N'NHS_Number', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4025, 0, 0, 0, N'', N'', 0, 18, 255, 0, N'', N'Sanction List', 0, 0, N'Sanction_List', 0, 1969)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4026, 0, 0, 0, N'', N'', 0, 17, 255, 0, N'', N'Sanction List', 0, 0, N'Sanction_List', 0, 1893)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4028, 0, 0, 0, N'', N'', 0, 4, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4029, 0, 0, 0, N'', N'', 0, 9, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4030, 0, 0, 0, N'', N'', 0, 10, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4031, 0, 0, 0, N'', N'', 0, 11, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4032, 0, 0, 0, N'', N'', 0, 12, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4033, 0, 0, 0, N'', N'', 0, 13, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4034, 0, 0, 0, N'', N'', 0, 14, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4035, 0, 0, 0, N'', N'', 0, 0, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4036, 0, 0, 3171, N'', N'', 0, 15, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4037, 0, 0, 0, N'', N'', 0, 8, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4038, 0, 0, 0, N'No', N'', 0, 16, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4039, 0, 0, 0, N'', N'', 0, 17, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4040, 0, 0, 0, N'', N'', 0, 18, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4063, 0, 0, 4041, N'', N'', 0, 5, 255, 0, N'', N'Handset Colour', 7, 0, N'Handset_Colour', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4064, 0, 0, 0, N'', N'', 0, 2, 50, 0, N'', N'Handset Make', 0, 0, N'Handset_Make', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4065, 0, 0, 0, N'', N'', 0, 3, 50, 0, N'', N'Handset Model', 0, 0, N'Handset_Model', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4066, 0, 0, 0, N'', N'', 0, 19, 50, 0, N'', N'Handset ID', 0, 0, N'Handset_ID', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4067, 0, 0, 0, N'', N'', 0, 1, 255, 0, N'', N'Handset IMEI', 0, 0, N'Handset_IMEI', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4068, 0, 0, 0, N'', N'', 0, 6, 0, 0, N'Currency', N'Handset Value', 5, 0, N'Handset_Value', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4069, 0, 0, 0, N'', N'', 0, 7, 0, 0, N'', N'Notes', 10, 0, N'Notes_', 0, 4027)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4071, 0, 0, 0, N'', N'', 0, 5, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4072, 0, 0, 0, N'', N'', 0, 6, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4073, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4074, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4075, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4076, 0, 0, 0, N'', N'', 0, 10, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4077, 0, 0, 0, N'', N'', 0, 11, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4078, 0, 0, 0, N'', N'', 0, 0, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4079, 0, 0, 3171, N'', N'', 0, 12, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4080, 0, 0, 0, N'', N'', 0, 13, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4081, 0, 0, 0, N'No', N'', 0, 14, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4082, 0, 0, 0, N'', N'', 0, 15, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4083, 0, 0, 0, N'', N'', 0, 16, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4092, 0, 0, 0, N'', N'', 0, 2, 50, 0, N'', N'Handset Value Catery', 0, 0, N'Handset_Value_Catery', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4093, 0, 0, 0, N'', N'', 0, 3, 50, 0, N'', N'Device Fault', 0, 0, N'Device_Fault', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4094, 0, 0, 4084, N'', N'', 0, 1, 255, 0, N'', N'Link Type', 8, 0, N'Link_Type', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4096, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4097, 0, 0, 0, N'', N'', 0, 4, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4098, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4099, 0, 0, 0, N'', N'', 0, 6, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4100, 0, 0, 0, N'', N'', 0, 7, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4101, 0, 0, 0, N'', N'', 0, 8, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4102, 0, 0, 0, N'', N'', 0, 9, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4103, 0, 0, 0, N'', N'', 0, 0, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4104, 0, 0, 3171, N'', N'', 0, 10, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4105, 0, 0, 0, N'', N'', 0, 11, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4106, 0, 0, 0, N'No', N'', 0, 12, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4107, 0, 0, 0, N'', N'', 0, 13, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4108, 0, 0, 0, N'', N'', 0, 14, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4109, 0, 0, 0, N'', N'', 0, 4, 50, 0, N'', N'Handset Link ID', 0, 0, N'Handset_Link_ID', 0, 4070)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4115, 0, 0, 4110, N'', N'', 0, 1, 255, 0, N'', N'Link Type', 8, 0, N'Link_Type', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4116, 0, 0, 0, N'', N'', 0, 2, 50, 0, N'', N'Handset Link Id', 0, 0, N'Handset_Link_Id', 0, 4095)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4118, 0, 0, 0, N'', N'', 0, 2, 50, -1, N'', N'Source', 0, 0, N'Source_411765484', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4119, 0, 0, 0, N'', N'', 0, 3, 50, -1, N'', N'Source Reference', 0, 0, N'Source_Reference_411765487', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4120, 0, 0, 0, N'', N'', 0, 4, 0, -1, N'', N'Source Description', 10, 0, N'Source_Description_411765489', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4121, 0, 0, 0, N'', N'', 0, 5, 0, -1, N'', N'Created Date & Time', 12, 0, N'Create_Date', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4122, 0, 0, 0, N'', N'', 0, 6, 255, -1, N'', N'Created User', 13, 0, N'Create_User', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4123, 0, 0, 0, N'', N'', 0, 7, 0, -1, N'', N'Updated Date & Time', 15, 0, N'Last_Upd_Date', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4124, 0, 0, 0, N'', N'', 0, 8, 255, -1, N'', N'Updated User', 14, 0, N'Last_Upd_User', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4125, 0, 0, 0, N'', N'', 0, 0, 255, -1, N'', N'iBase ID', 16, 0, N'Unique_ID', -1, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4126, 0, 0, 3171, N'', N'', 0, 9, 255, -1, N'', N'5x5x5 Grading', 7, 0, N'x5x5x5_Grading_412284402', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4127, 0, 0, 0, N'', N'', 0, 10, 100, -1, N'', N'Key Attractor', 0, 0, N'Key_Attractor_412284410', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4128, 0, 0, 0, N'No', N'', 0, 11, 0, -1, N'', N'Do Not Disseminate', 6, 0, N'Do_Not_Disseminate_412284494', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4129, 0, 0, 0, N'', N'', 0, 12, 50, -1, N'', N'ADA ID', 0, 0, N'MDA_Incident_ID_412284502', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4130, 0, 0, 0, N'', N'', 0, 13, 50, -1, N'', N'Risk Claim ID', 0, 0, N'Risk_Claim_ID_414244883', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4131, 0, 0, 0, N'', N'', 0, 1, 50, 0, N'', N'Handset to Handset Link ID', 0, 0, N'Handset_to_Handset_Link_ID', 0, 4117)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4132, 0, 0, 0, N'', N'', 0, 10, 50, 0, N'', N'Source Claim Status', 0, 0, N'Source_Claim_Status', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4133, 0, 0, 0, N'', N'', 0, 11, 0, 0, N'Currency', N'Total Claim Cost', 5, 0, N'Total_Claim_Cost', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4134, 0, 0, 0, N'', N'', 0, 12, 0, 0, N'Currency', N'Total Claim Cost Less Excess', 5, 0, N'Total_Claim_Cost_Less_Excess', 0, 2702)

INSERT [dbo].[_Field] ([Field_ID], [Characteristic], [ChartAttributeID], [CodeGroup_ID], [DefaultValue], [Description], [Discriminator], [FieldIndex], [FieldSize], [Fixed], [Format], [LogicalName], [LogicalType], [Mandatory], [PhysicalName], [Search], [Table_ID]) VALUES (4135, 0, 0, 0, N'No', N'', 0, 13, 0, 0, N'', N'Bypass Fraud', 6, 0, N'Bypass_Fraud', 0, 2702)

INSERT [dbo].[_Fraud_Ring_Incident_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Fraud_Ring_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Handset__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Handset_Incident_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Handset_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Handset_to_Handset_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Incident__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Incident_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Incident_Match_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Intelligence__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Intelligence_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_IP_Address_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_IP_Address_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Keoghs_Case_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2985, 1005, 0, 2984, 0, 1000, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2986, 0, 1, 2984, 0, 1000, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2987, 1006, 2, 2984, 0, 1000, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2988, 0, 3, 2984, 0, 1000, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2989, 1048, 4, 2984, 0, 1000, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2990, 1005, 0, 2984, -1, 1000, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2991, 0, 1, 2984, -1, 1000, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2992, 1006, 2, 2984, -1, 1000, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2993, 0, 3, 2984, -1, 1000, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2994, 1048, 4, 2984, -1, 1000, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2995, 1075, 0, 2984, 0, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2996, 0, 1, 2984, 0, 1065, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2997, 1076, 2, 2984, 0, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2998, 0, 3, 2984, 0, 1065, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (2999, 1077, 4, 2984, 0, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3000, 0, 5, 2984, 0, 1065, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3001, 1078, 6, 2984, 0, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3002, 0, 7, 2984, 0, 1065, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3003, 1079, 8, 2984, 0, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3004, 0, 9, 2984, 0, 1065, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3005, 1080, 10, 2984, 0, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3006, 0, 11, 2984, 0, 1065, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3007, 1162, 12, 2984, 0, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3008, 1075, 0, 2984, -1, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3009, 0, 1, 2984, -1, 1065, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3010, 1076, 2, 2984, -1, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3011, 0, 3, 2984, -1, 1065, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3012, 1077, 4, 2984, -1, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3013, 0, 5, 2984, -1, 1065, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3014, 1078, 6, 2984, -1, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3015, 0, 7, 2984, -1, 1065, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3016, 1079, 8, 2984, -1, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3017, 0, 9, 2984, -1, 1065, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3018, 1080, 10, 2984, -1, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3019, 0, 11, 2984, -1, 1065, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3020, 1162, 12, 2984, -1, 1065, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3021, 1184, 0, 2984, -1, 1174, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3022, 0, 1, 2984, -1, 1174, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3023, 1185, 2, 2984, -1, 1174, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3024, 1184, 0, 2984, 0, 1174, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3025, 1185, 2, 2984, 0, 1174, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3026, 0, 1, 2984, 0, 1174, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3027, 1196, 0, 2984, -1, 1186, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3028, 1196, 0, 2984, 0, 1186, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3029, 1207, 0, 2984, -1, 1197, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3030, 1207, 0, 2984, 0, 1197, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3031, 1679, 0, 2984, -1, 1208, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3032, 1684, 2, 2984, -1, 1208, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3033, 0, 1, 2984, -1, 1208, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3034, 1679, 0, 2984, 0, 1208, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3035, 0, 1, 2984, 0, 1208, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3036, 1684, 2, 2984, 0, 1208, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3037, 1698, 4, 2984, -1, 1688, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3038, 1716, 2, 2984, -1, 1688, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3039, 0, 3, 2984, -1, 1688, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3040, 0, 1, 2984, -1, 1688, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3041, 1719, 0, 2984, -1, 1688, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3044, 1716, 2, 2984, 0, 1688, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3045, 1719, 0, 2984, 0, 1688, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3046, 0, 1, 2984, 0, 1688, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3047, 2960, 0, 2984, -1, 2942, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3048, 0, 1, 2984, -1, 2942, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3051, 0, 1, 2984, 0, 2942, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3052, 2960, 0, 2984, 0, 2942, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3055, 1760, 0, 2984, -1, 1750, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3056, 0, 1, 2984, -1, 1750, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3057, 1880, 2, 2984, -1, 1750, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3058, 0, 3, 2984, -1, 1750, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3059, 1874, 4, 2984, -1, 1750, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3060, 1760, 2, 2984, 0, 1750, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3061, 0, 1, 2984, 0, 1750, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3062, 1880, 0, 2984, 0, 1750, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3063, 0, 3, 2984, 0, 1750, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3064, 1874, 4, 2984, 0, 1750, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3065, 1892, 0, 2984, -1, 1882, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3066, 1892, 0, 2984, 0, 1882, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3067, 1902, 0, 2984, -1, 1893, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3068, 0, 1, 2984, -1, 1893, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3069, 1929, 2, 2984, -1, 1893, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3070, 1902, 0, 2984, 0, 1893, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3071, 0, 1, 2984, 0, 1893, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3072, 1929, 2, 2984, 0, 1893, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3073, 0, 3, 2984, 0, 1893, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3074, 1927, 4, 2984, 0, 1893, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3075, 1950, 0, 2984, -1, 1940, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3076, 1950, 0, 2984, 0, 1940, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3077, 1961, 0, 2984, -1, 1951, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3078, 0, 1, 2984, -1, 1951, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3079, 1964, 2, 2984, -1, 1951, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3080, 1961, 0, 2984, 0, 1951, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3081, 0, 1, 2984, 0, 1951, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3082, 1964, 2, 2984, 0, 1951, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3083, 1994, 0, 2984, -1, 1969, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3084, 0, 1, 2984, -1, 1969, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3085, 1995, 2, 2984, -1, 1969, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3086, 0, 3, 2984, -1, 1969, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3087, 1996, 4, 2984, -1, 1969, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3088, 1994, 0, 2984, 0, 1969, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3089, 0, 1, 2984, 0, 1969, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3090, 1995, 2, 2984, 0, 1969, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3091, 0, 3, 2984, 0, 1969, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3092, 1996, 4, 2984, 0, 1969, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3093, 2272, 2, 2984, -1, 2261, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3094, 2271, 0, 2984, -1, 2261, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3095, 0, 1, 2984, -1, 2261, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3096, 2271, 0, 2984, 0, 2261, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3097, 0, 1, 2984, 0, 2261, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3098, 2272, 2, 2984, 0, 2261, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3099, 0, 3, 2984, 0, 2261, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3100, 2291, 4, 2984, 0, 2261, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3101, 0, 5, 2984, 0, 2261, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3102, 0, 6, 2984, 0, 2261, 1, N'-')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3103, 0, 7, 2984, 0, 2261, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3104, 2292, 8, 2984, 0, 2261, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3105, 2308, 0, 2984, -1, 2298, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3106, 2308, 0, 2984, 0, 2298, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3107, 2332, 0, 2984, 0, 2321, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3108, 0, 1, 2984, 0, 2321, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3109, 2427, 2, 2984, 0, 2321, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3110, 0, 3, 2984, 0, 2321, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3111, 2428, 4, 2984, 0, 2321, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3112, 0, 5, 2984, 0, 2321, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3113, 2429, 6, 2984, 0, 2321, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3114, 2332, 0, 2984, -1, 2321, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3115, 0, 1, 2984, -1, 2321, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3116, 2427, 2, 2984, -1, 2321, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3117, 0, 3, 2984, -1, 2321, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3118, 2428, 4, 2984, -1, 2321, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3119, 0, 5, 2984, -1, 2321, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3120, 2429, 6, 2984, -1, 2321, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3121, 2474, 0, 2984, -1, 2464, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3122, 2474, 0, 2984, 0, 2464, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3123, 2499, 0, 2984, 0, 2475, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3124, 2499, 0, 2984, -1, 2475, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3125, 2507, 0, 2984, 0, 2485, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3131, 0, 0, 2984, 0, 2508, 1, N'Fraud Ring')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3132, 2712, 0, 2984, 0, 2702, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3133, 0, 1, 2984, 0, 2702, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3134, 2718, 2, 2984, 0, 2702, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3136, 2612, 0, 2984, 0, 2595, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3137, 2612, 0, 2984, -1, 2595, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3138, 2812, 0, 2984, 0, 2781, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3139, 2812, 0, 2984, -1, 2781, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3140, 2862, 0, 2984, -1, 2833, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3141, 2862, 0, 2984, 0, 2833, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3142, 2770, 0, 2984, -1, 2756, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3143, 2770, 0, 2984, 0, 2756, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3144, 2875, 0, 2984, -1, 2865, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3145, 2875, 0, 2984, 0, 2865, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3146, 0, 0, 2984, 0, 2672, 1, N'Uses')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3147, 2893, 0, 2984, -1, 2877, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3148, 2893, 0, 2984, 0, 2877, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3149, 2922, 0, 2984, -1, 2901, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3150, 2922, 0, 2984, 0, 2901, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3157, 1749, 0, 2984, 0, 1739, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3158, 1749, 0, 2984, -1, 1739, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3159, 0, 5, 2984, 0, 1750, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3160, 1875, 6, 2984, 0, 1750, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3299, 0, 0, 2984, -1, 2632, 1, N'Account Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3301, 2507, 0, 2984, -1, 2485, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3303, 0, 0, 2984, -1, 2692, 1, N'Driving Licence Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3304, 0, 0, 2984, -1, 2642, 1, N'Email Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3306, 0, 0, 2984, -1, 2508, 1, N'Fraud Ring Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3307, 0, 0, 2984, -1, 2702, 1, N'Incident Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3308, 0, 0, 2984, -1, 2519, 1, N'Incident Match Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3309, 0, 0, 2984, 0, 2519, 1, N'Incident Match')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3310, 0, 0, 2984, 0, 2632, 1, N'Account')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3311, 0, 0, 2984, -1, 2682, 1, N'BB Pin Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3312, 0, 0, 2984, 0, 2682, 1, N'BB Pin')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3313, 0, 0, 2984, 0, 2692, 1, N'Driving Licence')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3314, 0, 0, 2984, 0, 2642, 1, N'Email')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3315, 0, 0, 2984, -1, 2967, 1, N'Intelligence Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3316, 0, 0, 2984, 0, 2967, 1, N'Intelligence')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3317, 0, 0, 2984, -1, 2652, 1, N'IP Address Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3318, 0, 0, 2984, 0, 2652, 1, N'IP Address')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3319, 0, 0, 2984, -1, 2746, 1, N'NI Number Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3320, 0, 0, 2984, 0, 2746, 1, N'NI Number')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3321, 0, 0, 2984, -1, 2823, 1, N'Passport Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3322, 0, 0, 2984, 0, 2823, 1, N'Passport')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3323, 0, 0, 2984, -1, 2662, 1, N'Payment Card Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3324, 0, 0, 2984, 0, 2662, 1, N'Payment Card')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3325, 0, 0, 2984, -1, 2672, 1, N'Telephone Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3326, 0, 0, 2984, -1, 2930, 1, N'Vehicle to Vehicle Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3327, 0, 0, 2984, 0, 2930, 1, N'Vehicle to Vehicle Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3328, 0, 0, 2984, -1, 2771, 1, N'Website Link')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (3329, 0, 0, 2984, 0, 2771, 1, N'Website')

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4027, 4094, 0, 2984, 0, 4070, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4028, 0, 1, 2984, 0, 4070, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4029, 4072, 2, 2984, 0, 4070, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4030, 4094, 0, 2984, -1, 4070, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4031, 4072, 2, 2984, -1, 4070, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4032, 0, 1, 2984, -1, 4070, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4033, 4115, 0, 2984, 0, 4095, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4034, 0, 1, 2984, 0, 4095, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4035, 4097, 2, 2984, 0, 4095, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4036, 4115, 0, 2984, -1, 4095, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4037, 0, 1, 2984, -1, 4095, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4038, 4097, 2, 2984, -1, 4095, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4039, 4067, 0, 2984, 0, 4027, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4040, 0, 1, 2984, 0, 4027, 3, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4041, 4064, 2, 2984, 0, 4027, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4042, 0, 3, 2984, 0, 4027, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4043, 4065, 4, 2984, 0, 4027, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4044, 4067, 0, 2984, -1, 4027, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4045, 0, 3, 2984, -1, 4027, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4046, 4064, 2, 2984, -1, 4027, 0, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4047, 0, 1, 2984, -1, 4027, 2, NULL)

INSERT [dbo].[_LabelPart] ([Part_ID], [Field_ID], [PartIndex], [Scheme_ID], [Screen], [Table_ID], [Type], [UserText]) VALUES (4048, 4065, 4, 2984, -1, 4027, 0, NULL)

INSERT [dbo].[_LabelScheme] ([Scheme_ID], [DefaultScheme], [Name]) VALUES (2984, -1, N'Keoghs Internal')

INSERT [dbo].[_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_NI_Number_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_NI_Number_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Organisation__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Organisation_Match_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Outcome_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Passport__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Passport_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Payment_Card_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Payment_Card_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Person__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Person_to_Organisation_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Person_to_Person_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Policy__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Policy_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Policy_Payment_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Telephone__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Telephone_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_TOG__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_TOG_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Vehicle__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Vehicle_Incident_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Vehicle_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Vehicle_to_Vehicle_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Website__NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_Website_Link_NextID] ([NextID]) VALUES (1)

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2475, N'Green', N'1065', N'1065,1893,1969,2321,4027')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2485, N'Aqua', N'1750', N'1688')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2508, N'Purple', N'1208', N'1688')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2519, N'Grey', N'1688', N'1688,1065')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2632, N'Yellow', N'1000', N'1893,1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2865, N'Yellow', N'2261', N'1000,1951')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2642, N'Grey', N'1197', N'1893,1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2877, N'Black', N'2321', N'1688')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2652, N'Grey', N'1739', N'1893,1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2595, N'Grey', N'1893', N'1893')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2662, N'Yellow', N'1951', N'1893,1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2672, N'Grey', N'2298', N'1065,1893,1969,4027')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2682, N'Grey', N'1174', N'1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2692, N'Grey', N'1186', N'1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2702, N'Black', N'1688', N'1969,1893')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2746, N'Grey', N'1882', N'1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2756, N'Grey', N'2261', N'1893,1969,2321,4027')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2771, N'Grey', N'2464', N'1893,1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2781, N'Grey', N'1969', N'1893')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2823, N'Grey', N'1940', N'1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2833, N'Grey', N'1969', N'1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2901, N'Grey', N'2321', N'1893,1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2930, N'Grey', N'2321', N'2321')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (2967, N'Red', N'', N'')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (3858, N'Grey', N'1688', N'1893,1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (3883, N'Red', N'3836', N'')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (4070, N'Black', N'4027', N'1688')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (4095, N'Grey', N'4027', N'1893,1969')

INSERT [dbo].[_LinkType] ([LinkType_ID], [Colour], [End1Types], [End2Types]) VALUES (4117, N'Grey', N'4027', N'4027')

