﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
--------------------------
--LoadTheDate
--------------------------
IF (SELECT MAX(Rows) FROM sys.partitions WHERE OBJECT_SCHEMA_NAME(OBJECT_ID)  = 'dbo') = 0
BEGIN
	:r .\MDA_IBaseConfigData.sql
END


