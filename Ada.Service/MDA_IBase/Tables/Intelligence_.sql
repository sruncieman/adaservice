﻿CREATE TABLE [dbo].[Intelligence_] (
    [Unique_ID]                    NVARCHAR (50)   NOT NULL,
    [AltEntity]                    NVARCHAR (50)   NULL,
    [Create_Date]                  DATETIME        NOT NULL,
    [Create_User]                  NVARCHAR (255)  NOT NULL,
    [Description_]                 NVARCHAR (50)   NULL,
    [Do_Not_Disseminate_412284494] SMALLINT        NOT NULL,
    [Document_]                    VARBINARY (MAX) NULL,
    [Document__Binding]            NVARCHAR (50)   NULL,
    [IconColour]                   INT             NULL,
    [Intelligence_ID]              NVARCHAR (50)   NULL,
    [Key_Attractor_412284410]      NVARCHAR (100)  NULL,
    [Key_Attractors]               NVARCHAR (MAX)  NULL,
    [Last_Upd_Date]                DATETIME        NULL,
    [Last_Upd_User]                NVARCHAR (255)  NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)   NULL,
    [Picture_]                     VARBINARY (MAX) NULL,
    [Picture__Binding]             NVARCHAR (50)   NULL,
    [Picture_2]                    VARBINARY (MAX) NULL,
    [Picture_2_Binding]            NVARCHAR (50)   NULL,
    [Record_Status]                TINYINT         CONSTRAINT [DF__Intellige__Recor__03F0984C] DEFAULT ((0)) NULL,
    [Report_]                      NVARCHAR (MAX)  NULL,
    [SCC]                          NVARCHAR (255)  CONSTRAINT [DF__Intelligenc__SCC__04E4BC85] DEFAULT ('') NULL,
    [Source_411765484]             NVARCHAR (50)   NULL,
    [Source_Description_411765489] NVARCHAR (MAX)  NULL,
    [Source_Reference_411765487]   NVARCHAR (50)   NULL,
    [Status_Binding]               NVARCHAR (50)   CONSTRAINT [DF__Intellige__Statu__05D8E0BE] DEFAULT ('') NULL,
    [Type_]                        NVARCHAR (255)  NULL,
    [Web_Page]                     NVARCHAR (MAX)  NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255)  NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)   NULL,
    CONSTRAINT [PK_Intelligence_] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
ALTER TABLE [dbo].[Intelligence_] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

