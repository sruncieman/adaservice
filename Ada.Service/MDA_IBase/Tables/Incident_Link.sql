﻿CREATE TABLE [dbo].[Incident_Link] (
    [Unique_ID]                    NVARCHAR (50)  NOT NULL,
    [Accident_Management]          NVARCHAR (500) NULL,
    [AltEntity]                    NVARCHAR (50)  NULL,
    [Ambulance_Attended]           SMALLINT       NOT NULL,
    [Broker_]                      NVARCHAR (500) NULL,
    [Claim_Code]                   NVARCHAR (50)  NULL,
    [Claim_Notification_Date]      DATETIME       NULL,
    [Claim_Number]                 NVARCHAR (50)  NULL,
    [Claim_Status]                 NVARCHAR (50)  NULL,
    [Claim_Type]                   NVARCHAR (50)  NULL,
    [Create_Date]                  DATETIME       NOT NULL,
    [Create_User]                  NVARCHAR (255) NOT NULL,
    [Do_Not_Disseminate_412284494] SMALLINT       NOT NULL,
    [Engineer_]                    NVARCHAR (500) NULL,
    [Hire_]                        NVARCHAR (500) NULL,
    [IconColour]                   INT            NULL,
    [Incident_Circs]               NVARCHAR (MAX) NULL,
    [Incident_Link_ID]             NVARCHAR (50)  NULL,
    [Incident_Location]            NVARCHAR (500) NULL,
    [Incident_Time]                NVARCHAR (10)  NULL,
    [Insurer_]                     NVARCHAR (500) NULL,
    [Key_Attractor_412284410]      NVARCHAR (100) NULL,
    [Last_Upd_Date]                DATETIME       NULL,
    [Last_Upd_User]                NVARCHAR (255) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)  NULL,
    [Medical_Examiner]             NVARCHAR (500) NULL,
    [MoJ_Status]                   NVARCHAR (50)  NULL,
    [Party_Type]                   NVARCHAR (500) NULL,
    [Payments_]                    NVARCHAR (10)  NULL,
    [Police_Attended]              SMALLINT       NOT NULL,
    [Police_Force]                 NVARCHAR (50)  NULL,
    [Police_Reference]             NVARCHAR (50)  NULL,
    [Record_Status]                TINYINT        CONSTRAINT [DF__Incident___Recor__7C4F7684] DEFAULT ((0)) NULL,
    [Recovery_]                    NVARCHAR (500) NULL,
    [Referral_Source]              NVARCHAR (500) NULL,
    [Repairer_]                    NVARCHAR (500) NULL,
    [Reserve_]                     NVARCHAR (10)  NULL,
    [SCC]                          NVARCHAR (255) CONSTRAINT [DF__Incident_Li__SCC__7D439ABD] DEFAULT ('') NULL,
    [Solicitors_]                  NVARCHAR (500) NULL,
    [Source_411765484]             NVARCHAR (50)  NULL,
    [Source_Description_411765489] NVARCHAR (MAX) NULL,
    [Source_Reference_411765487]   NVARCHAR (50)  NULL,
    [Status_Binding]               NVARCHAR (50)  CONSTRAINT [DF__Incident___Statu__7E37BEF6] DEFAULT ('') NULL,
    [Storage_]                     NVARCHAR (500) NULL,
    [Storage_Address]              NVARCHAR (500) NULL,
    [Sub_Party_Type]               NVARCHAR (255) NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255) NULL,
    [Undefined_Supplier]           NVARCHAR (255) NULL,
    [Attended_Hospital]            SMALLINT       NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)  NULL,
    [Medical_Legal]                NVARCHAR (255) NULL,
    [Recovery_Address]             NVARCHAR (255) NULL,
    [Repairer_Address]             NVARCHAR (255) NULL,
    [Inspection_Address]           NVARCHAR (255) NULL,
    [Source_Claim_Status]          NVARCHAR (50)  NULL,
    [Total_Claim_Cost]             MONEY          NULL,
    [Total_Claim_Cost_Less_Excess] MONEY          NULL,
    [Bypass_Fraud]                 SMALLINT       NULL,
    CONSTRAINT [PK_Incident_Link] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);





GO
ALTER TABLE [dbo].[Incident_Link] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Ambulance_Attended]
    ON [dbo].[Incident_Link]([Ambulance_Attended] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Claim_Number]
    ON [dbo].[Incident_Link]([Claim_Number] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Claim_Status]
    ON [dbo].[Incident_Link]([Claim_Status] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Claim_Type]
    ON [dbo].[Incident_Link]([Claim_Type] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Police_Attended]
    ON [dbo].[Incident_Link]([Police_Attended] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Sub_Party_Type]
    ON [dbo].[Incident_Link]([Sub_Party_Type] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Undefined_Supplier]
    ON [dbo].[Incident_Link]([Undefined_Supplier] ASC, [Unique_ID] ASC, [Record_Status] ASC);

