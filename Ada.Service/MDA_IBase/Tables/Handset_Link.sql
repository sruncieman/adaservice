﻿CREATE TABLE [dbo].[Handset_Link] (
    [Unique_ID]                    NVARCHAR (50)  NOT NULL,
    [Create_User]                  NVARCHAR (255) NOT NULL,
    [Create_Date]                  DATETIME       NOT NULL,
    [Last_Upd_User]                NVARCHAR (255) NULL,
    [Last_Upd_Date]                DATETIME       NULL,
    [SCC]                          NVARCHAR (255) DEFAULT (N'') NULL,
    [Record_Status]                TINYINT        DEFAULT ((0)) NULL,
    [Status_Binding]               NVARCHAR (50)  DEFAULT (N'') NULL,
    [AltEntity]                    NVARCHAR (50)  NULL,
    [IconColour]                   INT            NULL,
    [Source_411765484]             NVARCHAR (50)  NULL,
    [Source_Reference_411765487]   NVARCHAR (50)  NULL,
    [Source_Description_411765489] NVARCHAR (MAX) NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255) NULL,
    [Key_Attractor_412284410]      NVARCHAR (100) NULL,
    [Do_Not_Disseminate_412284494] SMALLINT       NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)  NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)  NULL,
    [Link_Type]                    NVARCHAR (255) NULL,
    [Handset_Link_Id]              NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Handset_Link] PRIMARY KEY CLUSTERED ([Unique_ID] ASC)
);


GO
ALTER TABLE [dbo].[Handset_Link] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

