﻿CREATE TABLE [dbo].[_AL_Queue] (
    [QueueNo]       INT        NOT NULL,
    [ExecutionTime] FLOAT (53) NULL,
    [LastRun]       DATETIME   NULL,
    CONSTRAINT [PK__AL_Queue] PRIMARY KEY CLUSTERED ([QueueNo] ASC) WITH (FILLFACTOR = 80)
);

