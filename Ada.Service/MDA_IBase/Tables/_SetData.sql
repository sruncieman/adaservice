﻿CREATE TABLE [dbo].[_SetData] (
    [Set_ID]       NVARCHAR (50) NOT NULL,
    [Record_ID]    NVARCHAR (50) NOT NULL,
    [Record_Index] INT           NOT NULL,
    CONSTRAINT [PK__SetData] PRIMARY KEY CLUSTERED ([Set_ID] ASC, [Record_ID] ASC, [Record_Index] ASC) WITH (FILLFACTOR = 80)
);

