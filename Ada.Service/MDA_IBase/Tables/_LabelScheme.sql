﻿CREATE TABLE [dbo].[_LabelScheme] (
    [Scheme_ID]     INT            NOT NULL,
    [DefaultScheme] SMALLINT       NOT NULL,
    [Name]          NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK__LabelScheme] PRIMARY KEY NONCLUSTERED ([Scheme_ID] ASC) WITH (FILLFACTOR = 80)
);

