﻿CREATE TABLE [dbo].[_AL_Alert_Datastore_Keys] (
    [alert_id]      INT            NOT NULL,
    [Record_ID]     NVARCHAR (50)  NOT NULL,
    [Unique_ID]     NVARCHAR (255) NOT NULL,
    [Record_Status] TINYINT        NULL,
    [SCC]           NVARCHAR (255) NULL,
    CONSTRAINT [PK__AL_Alert_Datastore_Keys] PRIMARY KEY CLUSTERED ([alert_id] ASC, [Record_ID] ASC, [Unique_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK__AL_Alert_Datastore_Keys__AL_Alert_Datastore] FOREIGN KEY ([alert_id], [Record_ID]) REFERENCES [dbo].[_AL_Alert_Datastore] ([alert_id], [Record_ID])
);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_Datastore_Keys]
    ON [dbo].[_AL_Alert_Datastore_Keys]([Unique_ID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_Datastore_Keys_1]
    ON [dbo].[_AL_Alert_Datastore_Keys]([alert_id] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);

