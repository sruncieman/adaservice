﻿CREATE TABLE [dbo].[Keoghs_Case] (
    [Unique_ID]                    NVARCHAR (50)  NOT NULL,
    [AltEntity]                    NVARCHAR (50)  NULL,
    [Case_Status]                  NVARCHAR (255) NULL,
    [Client_Claims_Handler]        NVARCHAR (50)  NULL,
    [Client_Reference]             NVARCHAR (50)  NULL,
    [Clients_]                     NVARCHAR (255) NULL,
    [Closure_Date]                 DATETIME       NULL,
    [Create_Date]                  DATETIME       NOT NULL,
    [Create_User]                  NVARCHAR (255) NOT NULL,
    [Current_Reserve]              NVARCHAR (50)  NULL,
    [Do_Not_Disseminate_412284494] SMALLINT       NOT NULL,
    [Fee_Earner]                   NVARCHAR (255) NULL,
    [IconColour]                   INT            NULL,
    [Keoghs_Case_ID]               NVARCHAR (50)  NULL,
    [Keoghs_Elite_Reference]       NVARCHAR (50)  NULL,
    [Keoghs_Office]                NVARCHAR (255) NULL,
    [Key_Attractor_412284410]      NVARCHAR (100) NULL,
    [Last_Upd_Date]                DATETIME       NULL,
    [Last_Upd_User]                NVARCHAR (255) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)  NULL,
    [Record_Status]                TINYINT        DEFAULT ((0)) NULL,
    [SCC]                          NVARCHAR (255) DEFAULT ('') NULL,
    [Source_411765484]             NVARCHAR (50)  NULL,
    [Source_Description_411765489] NVARCHAR (MAX) NULL,
    [Source_Reference_411765487]   NVARCHAR (50)  NULL,
    [Status_Binding]               NVARCHAR (50)  DEFAULT ('') NULL,
    [Weed_Date]                    DATETIME       NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255) NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)  NULL,
    [Data_Type]                    NVARCHAR (255) NULL,
    [Primary_Weed_Date]            DATETIME       NULL,
    [Primary_Weed_Details]         NVARCHAR (100) NULL,
    [Secondary_Weed_Date]          DATETIME       NULL,
    [Secondary_Weed_Details]       NVARCHAR (100) NULL,
    CONSTRAINT [PK_Keoghs_Case] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
ALTER TABLE [dbo].[Keoghs_Case] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Case_Status]
    ON [dbo].[Keoghs_Case]([Case_Status] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Keoghs_Elite_Reference]
    ON [dbo].[Keoghs_Case]([Keoghs_Elite_Reference] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);

