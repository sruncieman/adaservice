﻿CREATE TABLE [dbo].[_DataTable] (
    [Table_ID]     INT            NOT NULL,
    [Description]  NVARCHAR (255) NULL,
    [Fixed]        SMALLINT       NOT NULL,
    [InExpandList] SMALLINT       NOT NULL,
    [LogicalName]  NVARCHAR (255) NOT NULL,
    [PhysicalName] NVARCHAR (255) NOT NULL,
    [TableCode]    NVARCHAR (3)   NOT NULL,
    [Type]         TINYINT        NOT NULL,
    CONSTRAINT [PK__DataTable] PRIMARY KEY NONCLUSTERED ([Table_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE TRIGGER  [dbo].[i2DataTable_AFTERDELETE] ON [dbo].[_DataTable]  FOR DELETE  NOT FOR REPLICATION  AS  
 BEGIN  SET NOCOUNT ON; 
 
 
 INSERT INTO _DataTable_Audit (Table_ID, Description, Fixed, InExpandList, LogicalName, PhysicalName, TableCode, Type) 
 SELECT     Table_ID, Description, Fixed, InExpandList, LogicalName, PhysicalName, TableCode, Type  FROM deleted 
 WHERE Table_ID NOT IN (SELECT Table_ID FROM _DataTable_Audit) END
