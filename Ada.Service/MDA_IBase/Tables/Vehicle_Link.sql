﻿CREATE TABLE [dbo].[Vehicle_Link] (
    [Unique_ID]                    NVARCHAR (50)  NOT NULL,
    [AltEntity]                    NVARCHAR (50)  NULL,
    [Create_Date]                  DATETIME       NOT NULL,
    [Create_User]                  NVARCHAR (255) NOT NULL,
    [CrossHire_Company]            NVARCHAR (255) NULL,
    [Do_Not_Disseminate_412284494] SMALLINT       NOT NULL,
    [Hire_Company]                 NVARCHAR (255) NULL,
    [Hire_End_Date]                DATETIME       NULL,
    [Hire_Start_Date]              DATETIME       NULL,
    [IconColour]                   INT            NULL,
    [Key_Attractor_412284410]      NVARCHAR (100) NULL,
    [Last_Upd_Date]                DATETIME       NULL,
    [Last_Upd_User]                NVARCHAR (255) NULL,
    [Link_Type]                    NVARCHAR (255) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)  NULL,
    [Record_Status]                TINYINT        DEFAULT ((0)) NULL,
    [Reg_Keeper_End_Date]          DATETIME       NULL,
    [Reg_Keeper_Start_Date]        DATETIME       NULL,
    [SCC]                          NVARCHAR (255) DEFAULT ('') NULL,
    [Source_411765484]             NVARCHAR (50)  NULL,
    [Source_Description_411765489] NVARCHAR (MAX) NULL,
    [Source_Reference_411765487]   NVARCHAR (50)  NULL,
    [Status_Binding]               NVARCHAR (50)  DEFAULT ('') NULL,
    [Vehicle_Link_ID]              NVARCHAR (50)  NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255) NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Vehicle_Link] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
ALTER TABLE [dbo].[Vehicle_Link] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Hire_Company]
    ON [dbo].[Vehicle_Link]([Hire_Company] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Link_Type]
    ON [dbo].[Vehicle_Link]([Link_Type] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);

