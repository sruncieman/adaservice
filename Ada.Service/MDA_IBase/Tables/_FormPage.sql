﻿CREATE TABLE [dbo].[_FormPage] (
    [Form_ID]          INT           NOT NULL,
    [Page_Description] NVARCHAR (50) NOT NULL,
    [Page_No]          TINYINT       NOT NULL
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [PrimaryKey]
    ON [dbo].[_FormPage]([Form_ID] ASC, [Page_No] ASC) WITH (FILLFACTOR = 80);

