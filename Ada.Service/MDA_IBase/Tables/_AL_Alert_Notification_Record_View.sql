﻿CREATE TABLE [dbo].[_AL_Alert_Notification_Record_View] (
    [ID]              INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Notification_ID] INT            NULL,
    [alert_id]        INT            NOT NULL,
    [Unique_ID]       NVARCHAR (255) NOT NULL,
    [date_viewed]     DATETIME       NOT NULL,
    [View_user_name]  NVARCHAR (255) NOT NULL,
    [machine_Name]    NVARCHAR (255) NOT NULL,
    [OS_user_name]    NVARCHAR (255) NOT NULL,
    [SCC_New]         NVARCHAR (255) NULL,
    [SCC_Old]         NVARCHAR (255) NULL,
    [user_name]       NVARCHAR (255) NULL,
    [AccessDenied]    BIT            NOT NULL,
    CONSTRAINT [PK__AL_Alert_Notification_Record_View] PRIMARY KEY NONCLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK__AL_Alert_Notification_Record_View__AL_Alert] FOREIGN KEY ([alert_id]) REFERENCES [dbo].[_AL_Alert] ([alert_id]),
    CONSTRAINT [FK__AL_Alert_Notification_Record_View__AL_Alert_Notification] FOREIGN KEY ([Notification_ID]) REFERENCES [dbo].[_AL_Alert_Notification] ([Notification_ID])
);


GO
CREATE CLUSTERED INDEX [IX__AL_Alert_Notification_Record_View]
    ON [dbo].[_AL_Alert_Notification_Record_View]([Notification_ID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_Notification_Record_View_3]
    ON [dbo].[_AL_Alert_Notification_Record_View]([ID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_Notification_Record_View_1]
    ON [dbo].[_AL_Alert_Notification_Record_View]([alert_id] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_Notification_Record_View_2]
    ON [dbo].[_AL_Alert_Notification_Record_View]([user_name] ASC) WITH (FILLFACTOR = 80);

