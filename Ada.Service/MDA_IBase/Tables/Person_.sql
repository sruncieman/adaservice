﻿CREATE TABLE [dbo].[Person_] (
    [Unique_ID]                    NVARCHAR (50)   NOT NULL,
    [AltEntity]                    NVARCHAR (50)   NULL,
    [Create_Date]                  DATETIME        NOT NULL,
    [Create_User]                  NVARCHAR (255)  NOT NULL,
    [Date_of_Birth]                DATETIME        NULL,
    [Do_Not_Disseminate_412284494] SMALLINT        NOT NULL,
    [Document_]                    VARBINARY (MAX) NULL,
    [Document__Binding]            NVARCHAR (50)   NULL,
    [First_Name]                   NVARCHAR (100)  NULL,
    [Gender_]                      NVARCHAR (255)  NULL,
    [Hobbies_]                     NVARCHAR (150)  NULL,
    [IconColour]                   INT             NULL,
    [Key_Attractor_412284410]      NVARCHAR (100)  NULL,
    [Last_Name]                    NVARCHAR (100)  NULL,
    [Last_Upd_Date]                DATETIME        NULL,
    [Last_Upd_User]                NVARCHAR (255)  NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)   NULL,
    [Middle_Name]                  NVARCHAR (100)  NULL,
    [Nationality_]                 NVARCHAR (255)  NULL,
    [Notes_]                       NVARCHAR (MAX)  NULL,
    [Occupation_]                  NVARCHAR (255)  NULL,
    [Person_ID]                    NVARCHAR (50)   NULL,
    [Picture_]                     VARBINARY (MAX) NULL,
    [Picture__Binding]             NVARCHAR (50)   NULL,
    [Record_Status]                TINYINT         DEFAULT ((0)) NULL,
    [Salutation_]                  NVARCHAR (255)  NULL,
    [SCC]                          NVARCHAR (255)  DEFAULT ('') NULL,
    [Schools_]                     NVARCHAR (150)  NULL,
    [Source_411765484]             NVARCHAR (50)   NULL,
    [Source_Description_411765489] NVARCHAR (MAX)  NULL,
    [Source_Reference_411765487]   NVARCHAR (50)   NULL,
    [Status_Binding]               NVARCHAR (50)   DEFAULT ('') NULL,
    [Taxi_Driver_Licence]          NVARCHAR (20)   NULL,
    [VF_ID]                        NVARCHAR (50)   NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255)  NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)   NULL,
    [NHS_Number]                   NVARCHAR (50)   NULL,
    [Sanction_List]                NVARCHAR (255)  NULL,
    CONSTRAINT [PK_Person_] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);





GO
ALTER TABLE [dbo].[Person_] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Date_of_Birth]
    ON [dbo].[Person_]([Date_of_Birth] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_First_Name]
    ON [dbo].[Person_]([First_Name] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Last_Name]
    ON [dbo].[Person_]([Last_Name] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Nationality_]
    ON [dbo].[Person_]([Nationality_] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);

