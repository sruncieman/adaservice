﻿CREATE TABLE [dbo].[Outcome_Link] (
    [Unique_ID]                    NVARCHAR (50)  NOT NULL,
    [AltEntity]                    NVARCHAR (50)  NULL,
    [Amount_Recovered_to_Date]     NVARCHAR (20)  NULL,
    [Amount_Still_Outstanding]     NVARCHAR (20)  NULL,
    [Create_Date]                  DATETIME       NOT NULL,
    [Create_User]                  NVARCHAR (255) NOT NULL,
    [Date_Settlement_Agreed]       DATETIME       NULL,
    [Do_Not_Disseminate_412284494] SMALLINT       NOT NULL,
    [Enforcement_Role]             NVARCHAR (100) NULL,
    [IconColour]                   INT            NULL,
    [Key_Attractor_412284410]      NVARCHAR (100) NULL,
    [Last_Upd_Date]                DATETIME       NULL,
    [Last_Upd_User]                NVARCHAR (255) NULL,
    [Manner_of_Resolution]         NVARCHAR (100) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)  NULL,
    [Method_of_Enforcement]        NVARCHAR (100) NULL,
    [Outcome_Link_ID]              NVARCHAR (50)  NULL,
    [Property_Owned]               NVARCHAR (20)  NULL,
    [Record_Status]                TINYINT        DEFAULT ((0)) NULL,
    [SCC]                          NVARCHAR (255) DEFAULT ('') NULL,
    [Settlement_Amount_Agreed]     NVARCHAR (20)  NULL,
    [Settlement_Date]              DATETIME       NULL,
    [Source_411765484]             NVARCHAR (50)  NULL,
    [Source_Description_411765489] NVARCHAR (MAX) NULL,
    [Source_Reference_411765487]   NVARCHAR (50)  NULL,
    [Status_Binding]               NVARCHAR (50)  DEFAULT ('') NULL,
    [Type_of_Success]              NVARCHAR (100) NULL,
    [Was_Enforcement_Successful]   NVARCHAR (20)  NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255) NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Outcome_Link] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
ALTER TABLE [dbo].[Outcome_Link] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_Manner_of_Resolution]
    ON [dbo].[Outcome_Link]([Manner_of_Resolution] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);

