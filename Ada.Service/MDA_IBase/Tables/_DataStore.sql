﻿CREATE TABLE [dbo].[_DataStore] (
    [Record_ID]   NVARCHAR (50)  NOT NULL,
    [Category]    NVARCHAR (255) NOT NULL,
    [CreateDate]  DATETIME       NOT NULL,
    [CreateUser]  NVARCHAR (255) NOT NULL,
    [Data]        NTEXT          NULL,
    [Description] NVARCHAR (255) NULL,
    [GIDs]        NVARCHAR (255) NULL,
    [Name]        NVARCHAR (255) NOT NULL,
    [RecordType]  INT            NOT NULL,
    [SCC]         NVARCHAR (50)  NULL,
    [Unique_ID]   NVARCHAR (38)  NULL,
    [UpdateDate]  DATETIME       NULL,
    [UpdateUser]  NVARCHAR (255) NULL,
    CONSTRAINT [PK__DataStore] PRIMARY KEY NONCLUSTERED ([Record_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [Category]
    ON [dbo].[_DataStore]([Category] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [Name]
    ON [dbo].[_DataStore]([Name] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [RecordType]
    ON [dbo].[_DataStore]([RecordType] ASC) WITH (FILLFACTOR = 80);

