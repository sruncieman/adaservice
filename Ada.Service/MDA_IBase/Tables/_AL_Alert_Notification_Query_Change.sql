﻿CREATE TABLE [dbo].[_AL_Alert_Notification_Query_Change] (
    [ID]              INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Notification_ID] INT            NULL,
    [alert_id]        INT            NOT NULL,
    [Record_ID]       NVARCHAR (50)  NOT NULL,
    [Unique_ID]       NVARCHAR (255) NOT NULL,
    [change_type]     TINYINT        NOT NULL,
    CONSTRAINT [PK__AL_Alert_Notification_Query_Change] PRIMARY KEY NONCLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK__AL_Alert_Notification_Query_Change__AL_Alert] FOREIGN KEY ([alert_id]) REFERENCES [dbo].[_AL_Alert] ([alert_id]),
    CONSTRAINT [FK__AL_Alert_Notification_Query_Change__AL_Alert_Notification] FOREIGN KEY ([Notification_ID]) REFERENCES [dbo].[_AL_Alert_Notification] ([Notification_ID])
);


GO
CREATE CLUSTERED INDEX [IX__AL_Alert_Notification_Query_Change_2]
    ON [dbo].[_AL_Alert_Notification_Query_Change]([Notification_ID] ASC, [Unique_ID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_Notification_Query_Change_99]
    ON [dbo].[_AL_Alert_Notification_Query_Change]([Notification_ID] ASC, [Unique_ID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_Notification_Query_Change]
    ON [dbo].[_AL_Alert_Notification_Query_Change]([alert_id] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Alert_Notification_Query_Change_1]
    ON [dbo].[_AL_Alert_Notification_Query_Change]([Unique_ID] ASC) WITH (FILLFACTOR = 80);

