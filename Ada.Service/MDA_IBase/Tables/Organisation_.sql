﻿CREATE TABLE [dbo].[Organisation_] (
    [Unique_ID]                    NVARCHAR (50)   NOT NULL,
    [AltEntity]                    NVARCHAR (50)   NULL,
    [Create_Date]                  DATETIME        NOT NULL,
    [Create_User]                  NVARCHAR (255)  NOT NULL,
    [Credit_Licence]               NVARCHAR (50)   NULL,
    [Do_Not_Disseminate_412284494] SMALLINT        NOT NULL,
    [Effective_Date_of_Status]     DATETIME        NULL,
    [IconColour]                   INT             NULL,
    [Incorporated_Date]            DATETIME        NULL,
    [Key_Attractor_412284410]      NVARCHAR (100)  NULL,
    [Last_Upd_Date]                DATETIME        NULL,
    [Last_Upd_User]                NVARCHAR (255)  NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (50)   NULL,
    [MoJ_CRM_Number]               NVARCHAR (10)   NULL,
    [MoJ_CRM_Status]               NVARCHAR (255)  NULL,
    [Organisation_ID]              NVARCHAR (50)   NULL,
    [Organisation_Name]            NVARCHAR (255)  NULL,
    [Organisation_Status]          NVARCHAR (255)  NULL,
    [Organisation_Type]            NVARCHAR (255)  NULL,
    [Record_Status]                TINYINT         DEFAULT ((0)) NULL,
    [Registered_Number]            NVARCHAR (50)   NULL,
    [SCC]                          NVARCHAR (255)  DEFAULT ('') NULL,
    [SIC_]                         NVARCHAR (50)   NULL,
    [Source_411765484]             NVARCHAR (50)   NULL,
    [Source_Description_411765489] NVARCHAR (MAX)  NULL,
    [Source_Reference_411765487]   NVARCHAR (50)   NULL,
    [Status_Binding]               NVARCHAR (50)   DEFAULT ('') NULL,
    [VAT_Number]                   NVARCHAR (50)   NULL,
    [VAT_Number_Validated_Date]    DATETIME        NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (255)  NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (50)   NULL,
    [CHFKeyAttractor_]             SMALLINT        NULL,
    [CHFKeyAttractorRemoveDate_]   DATETIME        NULL,
    [Dissolved_Date]               DATETIME        NULL,
    [Document_]                    VARBINARY (MAX) NULL,
    [Document__Binding]            NVARCHAR (50)   NULL,
	[Sanction_List]				   NVARCHAR (255)  NULL,
    CONSTRAINT [PK_Organisation_] PRIMARY KEY CLUSTERED ([Unique_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
ALTER TABLE [dbo].[Organisation_] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_MoJ_CRM_Number]
    ON [dbo].[Organisation_]([MoJ_CRM_Number] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Organisation_Name]
    ON [dbo].[Organisation_]([Organisation_Name] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_Registered_Number]
    ON [dbo].[Organisation_]([Registered_Number] ASC, [Unique_ID] ASC, [Record_Status] ASC) WITH (FILLFACTOR = 80);

