﻿CREATE TABLE [dbo].[_Field] (
    [Field_ID]         INT            NOT NULL,
    [Characteristic]   SMALLINT       NOT NULL,
    [ChartAttributeID] INT            NULL,
    [CodeGroup_ID]     INT            NULL,
    [DefaultValue]     NVARCHAR (255) NULL,
    [Description]      NVARCHAR (255) NULL,
    [Discriminator]    SMALLINT       NOT NULL,
    [FieldIndex]       SMALLINT       NULL,
    [FieldSize]        TINYINT        NULL,
    [Fixed]            SMALLINT       NOT NULL,
    [Format]           NVARCHAR (255) NULL,
    [LogicalName]      NVARCHAR (255) NOT NULL,
    [LogicalType]      TINYINT        NOT NULL,
    [Mandatory]        SMALLINT       NOT NULL,
    [PhysicalName]     NVARCHAR (255) NOT NULL,
    [Search]           SMALLINT       NOT NULL,
    [Table_ID]         INT            NOT NULL,
    CONSTRAINT [PK__Field] PRIMARY KEY NONCLUSTERED ([Field_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE TRIGGER  [dbo].[i2Field_AFTERDELETE] ON [dbo].[_Field]  FOR DELETE  NOT FOR REPLICATION AS  
 BEGIN 
 
 SET NOCOUNT ON; 
 
 INSERT INTO _Field_Audit  (Field_ID, Characteristic, ChartAttributeID, CodeGroup_ID, DefaultValue, Description, Discriminator, FieldIndex, FieldSize, Fixed, Format, LogicalName, LogicalType, Mandatory, PhysicalName, Search, Table_ID) 
 SELECT     Field_ID, Characteristic, ChartAttributeID, CodeGroup_ID, DefaultValue, Description, Discriminator, FieldIndex, FieldSize, Fixed, Format, LogicalName, LogicalType,           Mandatory , PhysicalName, Search, Table_ID 
 From deleted 
 WHERE Field_ID NOT IN (SELECT Field_ID FROM _Field_Audit) 
 END 
