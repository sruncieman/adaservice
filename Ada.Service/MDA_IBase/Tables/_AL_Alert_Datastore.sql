﻿CREATE TABLE [dbo].[_AL_Alert_Datastore] (
    [alert_id]         INT             NOT NULL,
    [Record_ID]        NVARCHAR (50)   NOT NULL,
    [CommandText]      NVARCHAR (1000) NOT NULL,
    [QueryDefinition]  NVARCHAR (MAX)  NULL,
    [SnapshotDate]     DATETIME        NULL,
    [PrevSnapshotDate] DATETIME        NULL,
    CONSTRAINT [PK__AL_Alert_Datastore] PRIMARY KEY CLUSTERED ([alert_id] ASC, [Record_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK__AL_Alert_Datastore__AL_Alert] FOREIGN KEY ([alert_id]) REFERENCES [dbo].[_AL_Alert] ([alert_id])
);

