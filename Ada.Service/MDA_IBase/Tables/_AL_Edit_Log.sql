﻿CREATE TABLE [dbo].[_AL_Edit_Log] (
    [Unique_ID]            NVARCHAR (255)   NOT NULL,
    [date_changed]         DATETIME         CONSTRAINT [DF__AL_Edit_Log_date_viewed] DEFAULT (getdate()) NOT NULL,
    [user_name]            NVARCHAR (255)   NOT NULL,
    [View_Log_ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Database_Action_Type] TINYINT          NOT NULL,
    [Audit_ID]             UNIQUEIDENTIFIER NULL,
    [Record_Status]        TINYINT          NULL,
    [Batch_ID]             UNIQUEIDENTIFIER NULL,
    [SCC_Old]              NVARCHAR (255)   NULL,
    [SCC_New]              NVARCHAR (255)   NULL,
    [Audit_Batch_ID]       UNIQUEIDENTIFIER NULL,
    [Create_Date]          DATETIME         NULL,
    [Last_Upd_Date]        DATETIME         NULL,
    [Delete_Date]          DATETIME         NULL,
    [Record_Status_Old]    TINYINT          NULL,
    CONSTRAINT [PK__AL_Edit_Log] PRIMARY KEY CLUSTERED ([View_Log_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [_AL_VL_Date_Changed]
    ON [dbo].[_AL_Edit_Log]([date_changed] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Edit_Log]
    ON [dbo].[_AL_Edit_Log]([Unique_ID] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX__AL_Edit_Log_Batch_ID]
    ON [dbo].[_AL_Edit_Log]([Audit_Batch_ID] ASC, [Unique_ID] ASC) WITH (FILLFACTOR = 80);

