﻿CREATE TABLE [dbo].[_Configuration_Text] (
    [Item] NVARCHAR (255) NOT NULL,
    [Data] NTEXT          NULL,
    CONSTRAINT [PK__Configuration_Text] PRIMARY KEY NONCLUSTERED ([Item] ASC),
    CONSTRAINT [FK_Configuration_Text_Item] FOREIGN KEY ([Item]) REFERENCES [dbo].[_Configuration_Def] ([Item])
);




GO
CREATE NONCLUSTERED INDEX [FK_Configuration_Text_Item]
    ON [dbo].[_Configuration_Text]([Item] ASC);

