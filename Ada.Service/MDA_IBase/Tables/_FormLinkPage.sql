﻿CREATE TABLE [dbo].[_FormLinkPage] (
    [Entity_ID]        INT           NOT NULL,
    [Form_ID]          INT           NOT NULL,
    [Link_ID]          INT           NOT NULL,
    [LinkDirection]    TINYINT       NOT NULL,
    [Page_Description] NVARCHAR (50) NOT NULL,
    [Page_No]          TINYINT       NOT NULL
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [PrimaryKey]
    ON [dbo].[_FormLinkPage]([Form_ID] ASC, [Entity_ID] ASC, [Link_ID] ASC, [LinkDirection] ASC, [Page_No] ASC) WITH (FILLFACTOR = 80);

