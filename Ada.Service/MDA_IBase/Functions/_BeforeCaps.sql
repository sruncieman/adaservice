﻿create function dbo._BeforeCaps(@s varchar(100)) returns varchar(100)
as
begin
    declare @return varchar(100);
    set @return = left(@s,1);
    declare @i int;
    set @i = 2;
    while @i <= len(@s)
    begin
        if ASCII(substring(@s,@i,1)) between ASCII('A') and ASCII('Z')
            set @return = @return + '_' + substring(@s,@i,1)
        else
            set @return = @return + substring(@s,@i,1)
        set @i = @i + 1;
    end;
    return @return;
end;
