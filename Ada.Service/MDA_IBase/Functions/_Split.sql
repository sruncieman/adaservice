﻿CREATE FUNCTION [dbo].[_Split] (@csv nvarchar(4000))
RETURNS table
AS
RETURN (
    WITH Pieces(pn, start, stop) AS (
      SELECT 1, 1, CHARINDEX(',', @csv)
      UNION ALL
      SELECT pn + 1, stop + 1, CHARINDEX(',', @csv, stop + 1)
      FROM Pieces
      WHERE stop > 0
    )
    SELECT pn as csv_index,
      SUBSTRING(@csv, start, CASE WHEN stop > 0 THEN stop - start ELSE 4000 END) AS csv_value
    FROM Pieces
  )