﻿CREATE FUNCTION [dbo].[GetTextFromContextInfo] () 
 RETURNS VarChar(128) AS 
 BEGIN 
 DECLARE @text varchar(128) 
 DECLARE @pos int 
 --Initialize variables 
 SET @pos = 1 
 -- would be better as context_info() sql2005 but not supported sql2000 so read from sysprocesses 
 SET @text = (select  isnull(CAST(context_info As varchar(128)),'')  from master.dbo.sysprocesses WHERE spid = @@SPID) 
 --Remove all 0x0 (keep only text) 
       WHILE @pos <LEN(@text) 
       BEGIN 
            IF ASCII(SUBSTRING(@text, @pos, 1))= 0 
               BREAK 
               SET @pos = @pos + 1 
       END 
       SET @text =SUBSTRING(@text, 1, @pos - 1) 
       RETURN @text 
       END 