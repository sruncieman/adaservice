﻿ CREATE VIEW [dbo].[_AL_Notification_Summary] 
 AS 
 SELECT     ID, parent_unique_id, Unique_ID, Edit, New, Deleted, Audit_ID, EditDate, date_viewed, Notification_ID, Type, IsLink, Batch_ID, SoftDeleted, 
                       Restored , QueryChangeType, SCC_Deleted, SCC_Created , IsParent
 FROM         dbo._AL_NotificationEdits 
 Union 
 SELECT     ID, parent_unique_id, Unique_ID, Edit, New, Deleted, Audit_ID, EditDate, date_viewed, Notification_ID, Type, ISLink, Batch_ID, SoftDeleted, 
                       Restored , QueryChangeType, SCC_Deleted, SCC_Created, IsParent 
 FROM         dbo._AL_NotificationViews 
