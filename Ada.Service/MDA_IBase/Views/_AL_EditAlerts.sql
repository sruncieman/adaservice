﻿ CREATE VIEW [dbo].[_AL_EditAlerts] 
 AS 
 SELECT     dbo._AL_ActiveAlertKeysUnion.alert_id, dbo._AL_ActiveAlertKeysUnion.Unique_ID, dbo._AL_Alert_Datastore.PrevSnapshotDate, 
                       dbo._AL_Edit_Log.date_changed, dbo._AL_Edit_Log.user_name, dbo._AL_Edit_Log.Database_Action_Type, dbo._AL_Edit_Log.Audit_ID, 
                       dbo._AL_Edit_Log.Record_Status, dbo._AL_Edit_Log.SCC_Old, dbo._AL_Edit_Log.SCC_New, dbo._AL_Edit_Log.Record_Status_Old, 
                       dbo._AL_Alert.queue_no, dbo._AL_ActiveAlertKeysUnion.Parent_ID, 
                       CASE WHEN ((dbo._AL_Edit_Log.Create_Date >= dbo._AL_Alert_Datastore.PrevSnapshotDate)) THEN 1 ELSE 0 END AS New, 
                       CASE WHEN (_AL_Edit_Log.Last_Upd_Date >= _AL_Alert_Datastore.PrevSnapshotDate) AND 
                       (_AL_Edit_Log.Last_Upd_Date <= _AL_Alert_Datastore.SnapshotDate) THEN 1 ELSE 0 END AS Edit, 
                       CASE WHEN ((_AL_Edit_Log.Delete_Date >= _AL_Alert_Datastore.PrevSnapshotDate) AND 
                       (_AL_Edit_Log.Delete_Date <= _AL_Alert_Datastore.SnapshotDate)) THEN 1 ELSE 0 END AS Deleted, dbo._AL_Edit_Log.Audit_Batch_ID, 
                       CASE WHEN (ISNULL(_AL_Edit_Log.Record_Status, 0) >= 200 AND ISNULL(_AL_Edit_Log.Record_Status_Old, 0) = 0) 
                       THEN 1 ELSE 0 END AS SoftDeleted, CASE WHEN (ISNULL(_AL_Edit_Log.Record_Status, 0) = 0 AND ISNULL(_AL_Edit_Log.Record_Status_Old, 0) 
                       >= 200) THEN 1 ELSE 0 END AS Restored 
 FROM         dbo._AL_ActiveAlertKeysUnion INNER JOIN 
                       dbo._AL_Edit_Log ON dbo._AL_ActiveAlertKeysUnion.Unique_ID = dbo._AL_Edit_Log.Unique_ID INNER JOIN 
                       dbo._AL_Alert_Datastore ON dbo._AL_ActiveAlertKeysUnion.alert_id = dbo._AL_Alert_Datastore.alert_id INNER JOIN 
                       dbo._AL_Alert ON dbo._AL_Alert_Datastore.alert_id = dbo._AL_Alert.alert_id 
 WHERE     (dbo._AL_Edit_Log.Create_Date >= dbo._AL_Alert_Datastore.PrevSnapshotDate) AND 
                       (dbo._AL_Edit_Log.Create_Date <= dbo._AL_Alert_Datastore.SnapshotDate) OR 
                       (dbo._AL_Edit_Log.Last_Upd_Date >= dbo._AL_Alert_Datastore.PrevSnapshotDate) AND 
                       (dbo._AL_Edit_Log.Last_Upd_Date <= dbo._AL_Alert_Datastore.SnapshotDate) OR 
                       (dbo._AL_Edit_Log.Delete_Date >= dbo._AL_Alert_Datastore.PrevSnapshotDate) AND 
                       (dbo._AL_Edit_Log.Delete_Date <= dbo._AL_Alert_Datastore.SnapshotDate) OR 
                       (CASE WHEN (ISNULL(_AL_Edit_Log.Record_Status, 0) >= 200 AND ISNULL(_AL_Edit_Log.Record_Status_Old, 0) = 0) THEN 1 ELSE 0 END = 1) OR  
                       (CASE WHEN (ISNULL(_AL_Edit_Log.Record_Status, 0) = 0 AND ISNULL(_AL_Edit_Log.Record_Status_Old, 0) >= 200) THEN 1 ELSE 0 END = 1) 
