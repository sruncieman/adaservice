﻿ CREATE VIEW [dbo].[_AL_ActiveAlertKeysLinks5] 
 AS 
 SELECT     dbo._AL_ActiveAlertKeys.alert_id, dbo._LinkEnd.Entity_ID2 AS Unique_ID, dbo._AL_ActiveAlertKeys.Unique_ID AS Parent_ID 
 FROM         dbo._AL_ActiveAlertKeys INNER JOIN 
                       dbo._LinkEnd ON dbo._AL_ActiveAlertKeys.Unique_ID = dbo._LinkEnd.Entity_ID1 
 Union 
 SELECT     dbo._AL_ActiveAlertKeys.alert_id, dbo._LinkEnd_Audit.Entity_ID2 AS Unique_ID, dbo._AL_ActiveAlertKeys.Unique_ID AS Parent_ID 
 FROM         dbo._AL_ActiveAlertKeys INNER JOIN 
                       dbo._LinkEnd_Audit ON dbo._AL_ActiveAlertKeys.Unique_ID = dbo._LinkEnd_Audit.Entity_ID1 
