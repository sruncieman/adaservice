﻿CREATE VIEW [dbo].[_AL_AlertDeniedSCC]
 AS
 SELECT     dbo._AccessDenied.ItemIdentifier, dbo._AL_Alert.alert_id
 FROM         dbo._UserGroup INNER JOIN
                       dbo._AccessDenied ON dbo._UserGroup.GID = dbo._AccessDenied.GID INNER JOIN
                       dbo._AL_Alert ON dbo._UserGroup.UID = dbo._AL_Alert.UID
 WHERE     (dbo._AccessDenied.ItemType = 3)
