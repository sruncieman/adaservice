﻿    CREATE VIEW [dbo].[_AL_ActiveAlertKeys] 
    AS 
    SELECT     dbo._AL_Alert_Datastore_Keys.alert_id, dbo._AL_Alert_Datastore_Keys.Unique_ID, dbo._AL_Alert_Datastore_Keys.Unique_ID AS Parent_ID 
    FROM         dbo._AL_Alert_Datastore_Keys INNER JOIN 
                          dbo._AL_Alert ON dbo._AL_Alert_Datastore_Keys.alert_id = dbo._AL_Alert.alert_id 
    WHERE     (dbo._AL_Alert.expiry_date >= GETDATE() OR 
                          dbo._AL_Alert.expiry_date IS NULL) AND (dbo._AL_Alert.alert_status < 2) AND (dbo._AL_Alert.detection_options & 14 > 0) 
    Union 
    SELECT     dbo._AL_Alert_Removed_Keys.alert_id, dbo._AL_Alert_Removed_Keys.Unique_ID, dbo._AL_Alert_Removed_Keys.Unique_ID AS Parent_ID 
    FROM         dbo._AL_Alert INNER JOIN 
                          dbo._AL_Alert_Removed_Keys ON dbo._AL_Alert.alert_id = dbo._AL_Alert_Removed_Keys.alert_id 
    WHERE     (dbo._AL_Alert.expiry_date >= GETDATE() OR 
                          dbo._AL_Alert.expiry_date IS NULL) AND (dbo._AL_Alert.alert_status < 2) AND (dbo._AL_Alert.detection_options & 14 > 0) 
