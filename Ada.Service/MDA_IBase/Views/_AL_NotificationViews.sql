﻿ CREATE VIEW [dbo].[_AL_NotificationViews] 
 AS 
 SELECT     ID, Notification_ID, Unique_ID AS parent_unique_id, 0 AS Edit, 0 AS New, 0 AS Deleted, CAST(NULL AS UniqueIdentifier) AS Audit_ID, NULL AS EditDate, 
                       date_viewed, Unique_ID, NULL AS ISLink, 0 AS Type, CAST(NULL AS UniqueIdentifier) AS Batch_ID, 0 AS SoftDeleted, 0 AS Restored, NULL 
                       AS QueryChangeType , NULL AS SCC_Deleted, NULL AS SCC_Created, 1 AS IsParent
 FROM         dbo._AL_Alert_Notification_Record_View 
