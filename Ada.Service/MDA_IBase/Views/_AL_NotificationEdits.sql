﻿ CREATE VIEW [dbo].[_AL_NotificationEdits] 
 AS 
 SELECT  dbo._AL_Alert_Notification_Record_Change.ID, dbo._AL_Alert_Notification_Record_Change.parent_unique_id, 
                       dbo._AL_Alert_Notification_Record_Change.Unique_ID, dbo._AL_Alert_Notification_Record_Change.Edit, 
                       dbo._AL_Alert_Notification_Record_Change.New, dbo._AL_Alert_Notification_Record_Change.Deleted, 
                       dbo._AL_Alert_Notification_Record_Change.Audit_ID, CASE WHEN Edit = 1 THEN date_changed ELSE NULL END AS EditDate, NULL AS date_viewed, 
                       dbo._AL_Alert_Notification_Record_Change.Notification_ID, dbo._AL_Alert_Notification_Record_Change.IsLink, 1 AS Type, 
                       dbo._AL_Alert_Notification_Record_Change.Batch_ID, dbo._AL_Alert_Notification_Record_Change.SoftDeleted, 
                       dbo._AL_Alert_Notification_Record_Change.Restored, dbo._AL_Alert_Notification_Query_Change.change_type AS QueryChangeType, dbo._AL_Alert_Notification_Record_Change.SCC_Deleted, dbo._AL_Alert_Notification_Record_Change.SCC_Created, CASE WHEN dbo._AL_Alert_Notification_Record_Change.parent_unique_id = dbo._AL_Alert_Notification_Record_Change.Unique_ID THEN 1 ELSE 0 END AS IsParent
 FROM         dbo._AL_Alert_Notification_Record_Change LEFT OUTER JOIN 
                       dbo._AL_Alert_Notification_Query_Change ON 
                       dbo._AL_Alert_Notification_Record_Change.Notification_ID = dbo._AL_Alert_Notification_Query_Change.Notification_ID AND 
                       dbo._AL_Alert_Notification_Record_Change.parent_unique_id = dbo._AL_Alert_Notification_Query_Change.Unique_ID 
