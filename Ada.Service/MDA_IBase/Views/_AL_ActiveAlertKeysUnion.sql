﻿ CREATE VIEW [dbo].[_AL_ActiveAlertKeysUnion] 
 AS 
 SELECT     alert_id, Unique_ID, Parent_ID 
 FROM         _AL_ActiveAlertKeys 
 Union 
 SELECT     alert_id, Unique_ID, Parent_ID 
 FROM         _AL_ActiveAlertKeysLinks 
 Union 
 SELECT     alert_id, Unique_ID, Parent_ID 
 FROM         _AL_ActiveAlertKeysLinks2 
 Union 
 SELECT     alert_id, Unique_ID, Parent_ID 
 FROM         _AL_ActiveAlertKeysLinks3 
 Union 
 SELECT     alert_id, Unique_ID, Parent_ID 
 FROM         _AL_ActiveAlertKeysLinks4 
 Union 
 SELECT     alert_id, Unique_ID, Parent_ID 
 FROM         _AL_ActiveAlertKeysLinks5 
 Union 
 SELECT     alert_id, Unique_ID, Parent_ID 
 FROM         _AL_ActiveAlertKeysLinks6 
