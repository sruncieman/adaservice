﻿



CREATE VIEW [dbo].[VW_StagingEntities]
AS

SELECT AccountID_ IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM  dbo.Account_
UNION ALL
SELECT Address_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Address_
UNION ALL
SELECT BB_Pin_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.BBPin_
UNION ALL
SELECT Driving_Licence_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Driving_Licence
UNION ALL
SELECT Email_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Email_
UNION ALL
SELECT Fraud_ring_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Fraud_Ring
UNION ALL
SELECT Incident_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Incident_
UNION ALL
SELECT IP_Address_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.IP_Address
UNION ALL
SELECT Intelligence_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate  FROM dbo.Intelligence_
UNION ALL
SELECT NI_Number_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate  FROM dbo.NI_Number
UNION ALL
SELECT Organisation_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate  FROM dbo.Organisation_
UNION ALL
SELECT Passport_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.passport_
UNION ALL
SELECT Payment_Card_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Payment_Card
UNION ALL
SELECT Person_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Person_
UNION ALL
SELECT Policy_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Policy_
UNION ALL
SELECT Vehicle_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Vehicle_
UNION ALL
SELECT Website_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Website_
UNION ALL
SELECT Keoghs_Case_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Keoghs_Case
UNION ALL
SELECT Telephone_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.Telephone_
UNION ALL
SELECT TOG_ID IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM dbo.TOG_
UNION ALL
SELECT [Handset_ID] IBase5EntityID, Unique_ID IBase8EntityID, Last_Upd_Date ModifiedDate FROM [dbo].[Handset_]





