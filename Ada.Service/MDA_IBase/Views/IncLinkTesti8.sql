﻿  Create view IncLinkTesti8
  AS
  select LE.Link_ID, LE.Entity_ID1, LE.Entity_ID2, INC.Incident_ID
  from dbo._LinkEnd AS LE 
  JOIN dbo.Incident_ AS INC
	ON LE.Entity_ID1 = INC.Unique_ID
    where Link_ID like 'in1%' and Entity_ID1 like 'INC%' and Entity_ID2 not like 'VEH%' and Entity_ID2 not like 'ins%' and INC.Claim_Type not like 'RTA%'  
 