﻿ CREATE VIEW [dbo].[_AL_ActiveAlertKeysLinks2] 
 AS 
 SELECT     dbo._AL_ActiveAlertKeys.alert_id, dbo._LinkEnd.Link_ID AS Unique_ID, dbo._AL_ActiveAlertKeys.Unique_ID AS Parent_ID 
 FROM         dbo._LinkEnd INNER JOIN 
                       dbo._AL_ActiveAlertKeys ON dbo._LinkEnd.Entity_ID2 = dbo._AL_ActiveAlertKeys.Unique_ID 
 Union 
 SELECT     dbo._AL_ActiveAlertKeys.alert_id, dbo._LinkEnd_Audit.Link_ID AS Unique_ID, dbo._AL_ActiveAlertKeys.Unique_ID AS Parent_ID 
 FROM         dbo._LinkEnd_Audit INNER JOIN 
                       dbo._AL_ActiveAlertKeys ON dbo._LinkEnd_Audit.Entity_ID2 = dbo._AL_ActiveAlertKeys.Unique_ID 
