﻿CREATE PROCEDURE [dbo].[_AL_CleanViews]
  AS 
  SET NOCOUNT ON

   
   DELETE FROM _AL_View_Log
   WHERE     (View_Log_ID IN
                             (SELECT     _AL_View_Log_1.View_Log_ID
                               FROM          _AL_View_Log AS _AL_View_Log_1 INNER JOIN
                                                      _AL_Drillthrough_View_Log ON _AL_View_Log_1.Unique_ID = _AL_Drillthrough_View_Log.Unique_ID AND
                                                      _AL_View_Log_1.user_name = _AL_Drillthrough_View_Log.user_name
                               WHERE      (DATEDIFF(ms, _AL_Drillthrough_View_Log.date_viewed, _AL_View_Log_1.date_viewed) < 1000)))

   DELETE FROM _AL_Drillthrough_View_Log
   Where (date_viewed < DateAdd(Second, -2, GETDATE()))

   
 Return
