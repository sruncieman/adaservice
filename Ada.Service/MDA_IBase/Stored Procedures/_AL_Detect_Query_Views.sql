﻿CREATE PROCEDURE [dbo].[_AL_Detect_Query_Views]
 (
    @QueueNo int, @BatchID UniqueIdentifier
 )
 AS
 BEGIN

 SET NOCOUNT ON;

 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
 

 INSERT INTO _AL_Alert_Notification
                       (delivery_method_id, user_name, raised_on, alert_id, Batch_ID, PrevSnapshotDate, UID)
 SELECT DISTINCT
                       _AL_Alert_Delivery_Method.delivery_method_id, _AL_Alert_User.user_name, GETDATE() AS raised_on, _AL_EditAlerts.alert_id, @BatchID AS Expr1,
                       _AL_EditAlerts.PrevSnapshotDate, _AL_Alert_User.UID
 FROM         _AL_EditAlerts INNER JOIN
                       _AL_Alert_User INNER JOIN
                       _AL_Alert_Delivery_Method ON _AL_Alert_User.alert_id = _AL_Alert_Delivery_Method.alert_id AND
                       _AL_Alert_User.user_name = _AL_Alert_Delivery_Method.user_name ON _AL_EditAlerts.alert_id = _AL_Alert_User.alert_id INNER JOIN
                       _AL_Alert ON _AL_EditAlerts.alert_id = _AL_Alert.alert_id
 WHERE     (NOT (_AL_EditAlerts.alert_id IN
                           (SELECT     alert_id
                             FROM          _AL_Alert_Notification AS _AL_Alert_Notification_1
                             WHERE      (Batch_ID = @batchID)))) AND (_AL_EditAlerts.queue_no = @QueueNo) AND (_AL_EditAlerts.New = 0 OR
                       _AL_EditAlerts.New IS NULL) AND (_AL_Alert.detection_options & 2 = 2) OR
                       (NOT (_AL_EditAlerts.alert_id IN
                           (SELECT     alert_id
                             FROM          _AL_Alert_Notification AS _AL_Alert_Notification_1
                             WHERE      (Batch_ID = @batchID)))) AND (_AL_EditAlerts.queue_no = @QueueNo) AND (_AL_EditAlerts.New = 1) AND
                       (_AL_EditAlerts.Parent_ID <> _AL_EditAlerts.Unique_ID) AND (_AL_Alert.detection_options & 2 = 2)

 INSERT INTO _AL_Alert_Notification_Record_Change
                        (alert_id, Unique_ID, date_changed, edit_user_name, Database_Action_Type, Audit_ID, Record_Status, user_name, SCC_New, SCC_Old,
                        Notification_ID, parent_unique_id, New, Edit, Deleted, Batch_ID, SoftDeleted, Restored)
  SELECT DISTINCT
                        _AL_EditAlerts.alert_id, _AL_EditAlerts.Unique_ID, _AL_EditAlerts.date_changed, _AL_EditAlerts.user_name, _AL_EditAlerts.Database_Action_Type,
                        _AL_EditAlerts.Audit_ID, _AL_EditAlerts.Record_Status, _AL_Alert_User.user_name AS Expr1, _AL_EditAlerts.SCC_New, _AL_EditAlerts.SCC_Old,
                        _AL_Alert_Notification.Notification_ID, _AL_EditAlerts.Parent_ID, _AL_EditAlerts.New, _AL_EditAlerts.Edit, _AL_EditAlerts.Deleted,
                        _AL_EditAlerts.Audit_Batch_ID, CAST(_AL_EditAlerts.SoftDeleted AS Bit) AS Expr2, CAST(_AL_EditAlerts.Restored AS Bit) AS Expr3
  FROM         _AL_EditAlerts INNER JOIN
                        _AL_Alert_User INNER JOIN
                        _AL_Alert_Notification ON _AL_Alert_User.alert_id = _AL_Alert_Notification.alert_id AND
                        _AL_Alert_User.user_name = _AL_Alert_Notification.user_name ON _AL_EditAlerts.alert_id = _AL_Alert_User.alert_id INNER JOIN
                        _AL_Alert ON _AL_EditAlerts.alert_id = _AL_Alert.alert_id
  WHERE     (_AL_Alert_Notification.Batch_ID = @BatchID) AND (_AL_EditAlerts.queue_no = @QueueNo) AND (_AL_EditAlerts.New = 0 OR
                        _AL_EditAlerts.New IS NULL) AND (_AL_Alert.detection_options & 14 > 0) OR
                        (_AL_Alert_Notification.Batch_ID = @BatchID) AND (_AL_EditAlerts.queue_no = @QueueNo) AND (_AL_EditAlerts.New = 1) AND
                        (_AL_EditAlerts.Parent_ID <> _AL_EditAlerts.Unique_ID) AND (_AL_Alert.detection_options & 14 > 0)

 UPDATE    _AL_Alert_Notification_Record_Change
 SET              IsLink = _DataTable.Type
 FROM         _AL_Alert_Notification_Record_Change INNER JOIN
                       _DataTable ON LEFT(_AL_Alert_Notification_Record_Change.Unique_ID, 3) = _DataTable.TableCode INNER JOIN
                       _AL_Alert_Notification ON _AL_Alert_Notification_Record_Change.Notification_ID = _AL_Alert_Notification.Notification_ID
 WHERE     (_AL_Alert_Notification.Batch_ID = @BatchID)

 UPDATE    _AL_Alert_Notification_Record_Change
  Set SCC_Deleted = 1
  FROM         _AL_Alert_Notification_Record_Change INNER JOIN
                        _AL_Alert_Notification ON _AL_Alert_Notification_Record_Change.Notification_ID = _AL_Alert_Notification.Notification_ID
  WHERE     (_AL_Alert_Notification_Record_Change.ID IN
                            (SELECT     A.ID
                              FROM          _AL_Alert_Notification_Record_Change AS A INNER JOIN
                                                     _AL_AlertDeniedSCC ON A.alert_id = _AL_AlertDeniedSCC.alert_id AND A.SCC_New = _AL_AlertDeniedSCC.ItemIdentifier
                              WHERE      (ISNULL(A.SCC_New, N'') <> ISNULL(A.SCC_Old, N'')) AND (NOT (ISNULL(A.SCC_Old, N'') IN
                                                         (SELECT     ItemIdentifier AS SCC
                                                           FROM          _AL_AlertDeniedSCC AS B
                                                           WHERE      (A.alert_id = alert_id)))))) AND (_AL_Alert_Notification.Batch_ID = @BatchID)

  UPDATE    _AL_Alert_Notification_Record_Change
  Set SCC_Created = 1
  FROM         _AL_Alert_Notification_Record_Change INNER JOIN
                        _AL_Alert_Notification ON _AL_Alert_Notification_Record_Change.Notification_ID = _AL_Alert_Notification.Notification_ID
  WHERE     (_AL_Alert_Notification_Record_Change.ID IN
                            (SELECT     A.ID
                              FROM          _AL_Alert_Notification_Record_Change AS A INNER JOIN
                                                     _AL_AlertDeniedSCC ON A.alert_id = _AL_AlertDeniedSCC.alert_id AND A.SCC_Old = _AL_AlertDeniedSCC.ItemIdentifier
                              WHERE      (ISNULL(A.SCC_New, N'') <> ISNULL(A.SCC_Old, N'')) AND (NOT (ISNULL(A.SCC_New, N'') IN
                                                         (SELECT     ItemIdentifier AS SCC
                                                           FROM          _AL_AlertDeniedSCC AS B
                                                           WHERE      (A.alert_id = alert_id)))))) AND (_AL_Alert_Notification.Batch_ID = @BatchID)
INSERT INTO _AL_Alert_Notification
                      (delivery_method_id, user_name, raised_on, alert_id, Batch_ID, PrevSnapshotDate, UID)
 SELECT DISTINCT
                       _AL_Alert_Delivery_Method.delivery_method_id, _AL_Alert_User.user_name, GETDATE() AS raised_on, _AL_Alert.alert_id, @BatchID AS Expr1,
                       _AL_Alert_Datastore.PrevSnapshotDate, _AL_Alert_User.UID
 FROM         _AL_View_Log INNER JOIN
                       _AL_Alert_Datastore_Keys ON _AL_View_Log.Unique_ID = _AL_Alert_Datastore_Keys.Unique_ID INNER JOIN
                       _AL_Alert_Datastore ON _AL_Alert_Datastore_Keys.alert_id = _AL_Alert_Datastore.alert_id AND
                       _AL_Alert_Datastore_Keys.Record_ID = _AL_Alert_Datastore.Record_ID AND _AL_View_Log.date_viewed <= _AL_Alert_Datastore.SnapshotDate AND
                       _AL_View_Log.date_viewed >= _AL_Alert_Datastore.PrevSnapshotDate AND
                       _AL_View_Log.date_viewed <= _AL_Alert_Datastore.SnapshotDate INNER JOIN
                       _AL_Alert_User INNER JOIN
                       _AL_Alert ON _AL_Alert_User.alert_id = _AL_Alert.alert_id INNER JOIN
                       _AL_Alert_Delivery_Method ON _AL_Alert_User.alert_id = _AL_Alert_Delivery_Method.alert_id AND
                       _AL_Alert_User.user_name = _AL_Alert_Delivery_Method.user_name ON _AL_Alert_Datastore.alert_id = _AL_Alert.alert_id
 WHERE     (_AL_Alert.expiry_date >= GETDATE() OR
                       _AL_Alert.expiry_date IS NULL) AND (_AL_Alert.detection_options & 1 = 1) AND (_AL_Alert.alert_id NOT IN
                           (SELECT     alert_id
                             FROM          _AL_Alert_Notification AS _AL_Alert_Notification_1
                             WHERE      (Batch_ID = @batchID))) AND (_AL_Alert.queue_no = @QueueNo)

 INSERT INTO _AL_Alert_Notification_Record_View
                       (user_name, alert_id, Unique_ID, date_viewed, View_user_name, 
                        machine_Name, OS_user_name, 
                        Notification_ID,AccessDenied)
 SELECT DISTINCT
                       _AL_Alert_User.user_name, _AL_Alert.alert_id, _AL_View_Log.Unique_ID, _AL_View_Log.date_viewed, _AL_View_Log.user_name AS Expr1,
                       _AL_View_Log.machine_Name, _AL_View_Log.OS_user_name, 
                       _AL_Alert_Notification.Notification_ID, _AL_View_Log.AccessDenied
 FROM         _AL_Alert_Datastore INNER JOIN
                       _AL_View_Log INNER JOIN
                       _AL_Alert_Datastore_Keys ON _AL_View_Log.Unique_ID = _AL_Alert_Datastore_Keys.Unique_ID ON
                       _AL_Alert_Datastore.alert_id = _AL_Alert_Datastore_Keys.alert_id AND _AL_Alert_Datastore.Record_ID = _AL_Alert_Datastore_Keys.Record_ID INNER JOIN
                       _AL_Alert_Notification INNER JOIN
                       _AL_Alert_User INNER JOIN
                       _AL_Alert ON _AL_Alert_User.alert_id = _AL_Alert.alert_id ON _AL_Alert_Notification.alert_id = _AL_Alert_User.alert_id AND
                       _AL_Alert_Notification.user_name = _AL_Alert_User.user_name ON _AL_Alert_Datastore.alert_id = _AL_Alert.alert_id AND
                       _AL_View_Log.date_viewed > _AL_Alert.Create_Date
 WHERE     (_AL_Alert.expiry_date >= GETDATE() OR
                       _AL_Alert.expiry_date IS NULL) AND (_AL_Alert.detection_options & 1 = 1) AND (_AL_Alert_Notification.Batch_ID = @BatchID) AND
                       (_AL_Alert.queue_no = @QueueNo) AND (_AL_View_Log.date_viewed >= _AL_Alert_Datastore.PrevSnapshotDate AND
                       _AL_View_Log.date_viewed <= _AL_Alert_Datastore.SnapshotDate)

 
End
