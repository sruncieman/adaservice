﻿CREATE PROCEDURE [dbo].[_AL_DeleteAlertNotification]
 (@NotificationID Int)
 AS
 SET NOCOUNT ON

 BEGIN TRAN

 DELETE FROM _AL_Alert_Notification_Record_View
 WHERE     (Notification_ID = @NotificationID)

 DELETE FROM _AL_Alert_Notification_Record_Change
 WHERE     (Notification_ID = @NotificationID)

 DELETE FROM _AL_Alert_Notification_Query_Change
 WHERE     (Notification_ID = @NotificationID)

 DELETE FROM _AL_Alert_Notification
 WHERE     (Notification_ID = @NotificationID)

 Commit TRAN
 Return
