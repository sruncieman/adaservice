﻿CREATE Procedure [dbo].[_AL_NotificationRecord]
 (
 @NotificationID int
 )
 AS
 BEGIN
 SET NOCOUNT ON


 SELECT     AN.alert_id, _AL_Alert.alert_name, AN.delivery_method_id, AN.user_name, AN.raised_on, AN.read_on, AN.Notification_ID, _AL_Alert.alert_type,
                     _AL_Alert.Create_User, AN.AlertRead, AN.Deleted, AN.FollowUp, _AL_Alert.detection_options, ISNULL(AN.Status, 0) AS Status, AN.PrevSnapshotDate,ISNULL(AN.IsMessage, 0) AS IsMessage, AN.Title, AN.Message
 FROM         _AL_Alert_Notification AS AN INNER JOIN
                     _AL_Alert ON _AL_Alert.alert_id = AN.alert_id
 WHERE     (AN.Notification_ID = @NotificationID) 
 END 
