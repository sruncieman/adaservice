﻿CREATE PROCEDURE dbo._AL_CheckDuplicateName
 (
 @Name nvarchar(255),
 @Owner nvarchar(255),
 @Present bit OUTPUT 
 )
 
 AS
 SET NOCOUNT ON
  
 IF EXISTS(SELECT        Create_User, alert_name
  FROM  _AL_Alert
  WHERE        (Create_User = @Owner) AND (alert_name = @Name) AND alert_status =0) 
 BEGIN 
 SET @Present = 1
 END 
 ELSE 
 BEGIN 
 SET @Present = 0 
 END 
 
 RETURN 
