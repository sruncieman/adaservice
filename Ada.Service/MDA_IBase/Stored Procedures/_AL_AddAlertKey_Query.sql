﻿CREATE PROCEDURE [dbo].[_AL_AddAlertKey_Query]
( @alert_id int, @record_id nvarchar(255), @commandText nvarchar(1000), @QueryDefinition nvarchar(MAX), @SnapshotDate DateTime )

 AS
 SET NOCOUNT ON
 INSERT INTO _AL_Alert_Datastore
     (alert_id, Record_ID,CommandText,QueryDefinition,SnapshotDate ,PrevSnapshotDate)
 VALUES        (@alert_id,@record_id, @commandText, @QueryDefinition,GETDATE(),GETDATE()  )
 RETURN
