﻿CREATE PROCEDURE _Next_Handset_Incident_Link @NEXTID int OUTPUT AS SET NOCOUNT ON BEGIN TRANSACTION;SELECT @NEXTID = NextID FROM dbo._Handset_Incident_Link_NextID WITH (TABLOCKX, HOLDLOCK); UPDATE dbo._Handset_Incident_Link_NextID SET NextID = NextID + 1;COMMIT TRANSACTION
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[_Next_Handset_Incident_Link] TO PUBLIC
    AS [dbo];

