﻿CREATE PROCEDURE [dbo].[_AL_NotificationUpdate]
 (@NotificationID Int, @read_on DateTime, @Deleted Bit, @FollowUp Bit, @AlertRead Bit, @Status Int )
 AS 
 SET NOCOUNT ON 

 UPDATE    _AL_Alert_Notification
      SET  read_on = @read_on, Deleted = @Deleted, AlertRead = @AlertRead, Status = @Status, FollowUp = @FollowUp 
 WHERE     (Notification_ID = @NotificationID) 
 
 Return 
