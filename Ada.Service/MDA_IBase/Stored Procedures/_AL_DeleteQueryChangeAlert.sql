﻿CREATE PROCEDURE [dbo].[_AL_DeleteQueryChangeAlert]
 (@alert_id int, @alert_status int)
 AS
 SET NOCOUNT ON

 UPDATE    _AL_Alert
 Set alert_status = @alert_status
 WHERE     (alert_id = @alert_id)

Return
