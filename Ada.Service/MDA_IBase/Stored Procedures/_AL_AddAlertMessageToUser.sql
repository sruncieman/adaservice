﻿CREATE PROCEDURE [dbo].[_AL_AddAlertMessageToUser]
 (
  @alert_id int ,
  @title nvarchar(255),
  @message nvarchar(1000),
  @user nvarchar(255)
 )
 AS
 SET NOCOUNT ON

 INSERT INTO _AL_Alert_Notification
                      (alert_id, delivery_method_id, user_name, raised_on, IsMessage, Title, Message)
VALUES     (@alert_id, 0, @user, GETDATE(), 1,@title,@message)

  Return
