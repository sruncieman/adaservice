﻿CREATE PROCEDURE dbo.[_AL_AddAlertUser]
 ( @alert_id int, @username nvarchar(255), @email nvarchar(2000), @iBaseNotify Bit, @emailNotify Bit, @UID nvarchar(50))
 
 AS
 SET NOCOUNT ON
 INSERT INTO _AL_Alert_User
                 (alert_id, user_name, email, UID)
 VALUES        (@alert_id,@username,@email, @UID)
 
 IF @iBaseNotify = 1
 BEGIN
        INSERT INTO _AL_Alert_Delivery_Method
          (alert_id, delivery_method_id, user_name)
         VALUES        (@alert_id, 0,@username)
 END
 
 IF @emailNotify = 1
 BEGIN
   INSERT INTO _AL_Alert_Delivery_Method
          (alert_id, delivery_method_id, user_name)
 VALUES        (@alert_id, 1,@username)
 END
 RETURN