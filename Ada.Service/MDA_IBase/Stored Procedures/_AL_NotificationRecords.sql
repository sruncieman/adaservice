﻿CREATE Procedure [dbo].[_AL_NotificationRecords] 
 ( 
  @NotificationID Int, @StartTime DateTime OUTPUT, @EndTime DateTime OUTPUT 
  ) 
  AS 
  BEGIN 
    SET NOCOUNT ON 
 
    SET @StartTime = (SELECT  PrevSnapshotDate FROM _AL_Alert_Notification WHERE (Notification_ID = @NotificationID)) 
    SET @EndTime = (SELECT raised_on   FROM _AL_Alert_Notification WHERE (Notification_ID = @NotificationID)) 
 
 SELECT     ID, parent_unique_id, Unique_ID, Edit, New, Deleted, Audit_ID, EditDate, IsLink, Type, date_viewed, Batch_ID,Notification_ID ,SoftDeleted, Restored, QueryChangeType,SCC_Deleted, SCC_Created, IsParent 
 FROM         _AL_Notification_Summary 
 WHERE Notification_ID = @NotificationID 
 ORDER BY Type DESC, parent_unique_id, IsParent DESC
  End 
