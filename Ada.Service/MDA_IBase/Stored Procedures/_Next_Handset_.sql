﻿CREATE PROCEDURE _Next_Handset_ @NEXTID int OUTPUT AS SET NOCOUNT ON BEGIN TRANSACTION;SELECT @NEXTID = NextID FROM dbo._Handset__NextID WITH (TABLOCKX, HOLDLOCK); UPDATE dbo._Handset__NextID SET NextID = NextID + 1;COMMIT TRANSACTION
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[_Next_Handset_] TO PUBLIC
    AS [dbo];

