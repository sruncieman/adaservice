﻿CREATE PROCEDURE [dbo].[_AL_AlertOwnedBy] 
 (     @owner nvarchar(255), @CaseID nvarchar(50) = '' ) 
 
 AS 
 SET NOCOUNT ON 
 
 IF @CaseID = '' 
 BEGIN 
     SELECT     alert_id 
     FROM         _AL_Alert 
     WHERE     (Create_User = @owner)  AND alert_status = 0 
     ORDER BY alert_name, alert_description 
 End 
 Else 
 BEGIN 
     SELECT     alert_id 
     FROM         _AL_Alert 
     WHERE     (Create_User = @owner) AND (alert_status = 0) AND (CaseID = @CaseID) 
     ORDER BY alert_name, alert_description 
 End 
 
RETURN
