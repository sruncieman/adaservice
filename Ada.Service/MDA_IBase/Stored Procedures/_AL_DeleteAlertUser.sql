﻿CREATE PROCEDURE [dbo].[_AL_DeleteAlertUser] 
 (@alert_id int, @user nvarchar(255))

 AS
 SET NOCOUNT ON
 DELETE FROM _AL_Alert_Delivery_Method
 WHERE        (alert_id = @alert_id AND user_name = @user)

 DELETE FROM _AL_Alert_User
 WHERE        (alert_id = @alert_id AND user_name = @user )
 Return
