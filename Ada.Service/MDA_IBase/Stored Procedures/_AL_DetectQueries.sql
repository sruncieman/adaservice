﻿CREATE PROCEDURE [dbo].[_AL_DetectQueries] 
  (@QueueNo Int = 0) 
  AS 
  SET NOCOUNT ON 
  EXEC _AL_CleanViews 
 
  DECLARE @Delay varchar(10) 
   SET @Delay = '00:00:' + RIGHT('0' + CAST(@QueueNo * 3 As Varchar(2)),2) 
   WAITFOR DELAY @Delay 
  BEGIN TRAN 
  DECLARE @BatchID UniqueIdentifier 
  DECLARE @StartTime DateTime 
 
  SET @BatchID = NEWID() 
  SET @StartTime = GETDATE() 
  IF NOT EXISTS( SELECT QueueNo FROM  _AL_Queue WHERE     (QueueNo = @QueueNo)) 
  BEGIN 
     INSERT INTO _AL_Queue 
                        (QueueNo, LastRun) 
   SELECT     @QueueNo AS QueueNo, GETDATE() AS LastRun 
  End 
  Else 
  BEGIN 
     UPDATE    _AL_Queue 
     Set LastRun = GETDATE() 
     WHERE     (QueueNo = @QueueNo) 
  End 
 
  IF Object_ID('tempdb.dbo.#QueryResults') IS NULL 
  CREATE TABLE #QueryResults (Unique_ID nvarchar(255)  COLLATE Latin1_General_CI_AS  NOT NULL) 
 
  IF Object_ID('tempdb.dbo.#QueryChanges') IS NULL 
  CREATE TABLE #QueryChanges (Unique_ID nvarchar(255)  COLLATE Latin1_General_CI_AS  NOT NULL, Change_Type TinyInt NOT NULL) 
  DECLARE @View nvarchar(1000) 
  DECLARE @SQL nvarchar(3000) 
  DECLARE @alert_id int 
  DECLARE @record_id nvarchar(50) 
  DECLARE @type int 
 
  DECLARE query_cursor CURSOR FAST_FORWARD  FOR 
  SELECT     _AL_Alert_Datastore.CommandText, _AL_Alert_Datastore.alert_id, _AL_Alert_Datastore.Record_ID, _AL_Alert.alert_type 
   FROM         _AL_Alert INNER JOIN 
                            _AL_Alert_Datastore ON _AL_Alert.alert_id = _AL_Alert_Datastore.alert_id 
      WHERE    (_AL_Alert.queue_no = @QueueNo) AND (_AL_Alert.expiry_date >= GETDATE()  OR _AL_Alert.expiry_date IS NULL) AND (_AL_Alert.alert_status < 2) 
 
 
  OPEN query_cursor 
  FETCH NEXT FROM query_cursor 
  INTO @View, @alert_id, @record_id, @type 
  WHILE @@FETCH_STATUS = 0 
  BEGIN 
     SET @SQL = 'INSERT INTO #QueryResults (Unique_ID) SELECT ' +  @View + '.* FROM ' + @View 
     EXEC _AL_Detect_Query_Changes  @alert_id , @record_id , @SQL, @BatchID, @QueueNo, @Type 
 
 
  FETCH NEXT FROM query_cursor 
  INTO @View, @alert_id, @record_id, @type 
  End 
 
  Close query_cursor 
  DEALLOCATE query_cursor 
  EXEC _AL_Detect_Query_Views  @QueueNo, @BatchID 
 
  UPDATE    _AL_Queue 
  SET              ExecutionTime = CAST(CAST(DATEDIFF(ms, LastRun, GETDATE()) As float)/1000 As Float), LastRun = @StartTime 
  WHERE     (QueueNo = @QueueNo) 
 
  EXEC _AL_ClearLogs 
 
  DELETE _AL_Alert_Removed_Keys WHERE QueueNo = @QueueNo 
 
  COMMIT TRAN 
   UPDATE    _AL_Alert 
   SET              alert_status = 2, expiry_date = GETDATE() 
   WHERE     (alert_status = 1) AND (queue_no = @QueueNo) 
  Return 
