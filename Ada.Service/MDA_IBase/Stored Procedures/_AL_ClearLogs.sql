﻿CREATE PROCEDURE [dbo].[_AL_ClearLogs]
 AS
 SET NOCOUNT ON
 
 DECLARE @LastDate DateTime
 
 SET @LastDate = (SELECT MIN(LastRun) AS Expr1 FROM _AL_Queue)
 
 DELETE FROM _AL_Edit_Log
WHERE     (date_changed < @LastDate)

 DELETE FROM _AL_View_Log
WHERE     (date_viewed  < @LastDate)
 DELETE FROM _LinkEnd_Audit WHERE date_deleted < @LastDate 
 RETURN
