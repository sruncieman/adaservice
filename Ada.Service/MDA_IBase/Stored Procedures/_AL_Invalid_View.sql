﻿CREATE PROCEDURE [dbo].[_AL_Invalid_View] 
 (  @alert_id int ) 
 AS 
 SET NOCOUNT ON 
 
    DECLARE @Message nvarchar(255) 
    DECLARE @Title nvarchar(255) 
    DECLARE @Owner nvarchar(255) 
    DECLARE @UID nvarchar(50) 
    DECLARE @View nvarchar(1000) 
    DECLARE @SQL nvarchar(4000) 
 
    SET @Message = 'It was detected that this alert is invalid, it has been deleted' 
    SET @Title = 'Alert Deleted' 
 
    SET @Owner = (SELECT Create_User  FROM _AL_Alert WHERE alert_id = @alert_id ) 
    SET @UID = (SELECT UID  FROM _AL_Alert WHERE alert_id = @alert_id ) 
    SET @View = (SELECT TOP 1 CommandText  FROM _AL_Alert_Datastore WHERE alert_id = @alert_id ) 
 
    INSERT INTO _AL_Alert_Notification 
                      (alert_id, delivery_method_id, user_name, raised_on, Batch_ID, IsMessage, Title, Message, UID) 
    VALUES     (@alert_id, 0,@Owner, GETDATE(), NEWID(), 1,@Title,@Message,@UID) 
 
    UPDATE _AL_Alert SET alert_status = 2 WHERE alert_id = @alert_id 
 
    SET @SQL = 'DROP VIEW [dbo].[' + @View + ']' 
    EXECUTE(@SQL) 
 
 Return 
