﻿CREATE PROCEDURE [dbo].[_AL_SetNotificationStatus]
 (
    @NotificationID Int, @Status Int
 )
 AS
 SET NOCOUNT ON

 UPDATE    _AL_Alert_Notification
 SET              Status = @Status
 WHERE     (Notification_ID = @NotificationID)

 Return
