﻿CREATE PROCEDURE [dbo].[_AL_ChangeNotificationFlag]
 (@NotificationID Int, @Flag Int, @Set Bit)
 AS
 SET NOCOUNT ON
 IF @Set = 1
 BEGIN
   UPDATE    _AL_Alert_Notification
   SET    Status = Status | @Flag
   WHERE     (Notification_ID = @NotificationID)

 End
 Else
 BEGIN
   UPDATE    _AL_Alert_Notification
   SET    Status = Status & ~@Flag
   WHERE     (Notification_ID = @NotificationID)
 End

 Return
