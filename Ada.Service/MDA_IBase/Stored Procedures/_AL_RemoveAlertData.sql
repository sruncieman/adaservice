﻿CREATE PROCEDURE [dbo].[_AL_RemoveAlertData]
 AS
SET NOCOUNT ON

 TRUNCATE TABLE _AL_Alert_Notification_Record_View
 TRUNCATE TABLE _AL_Alert_Notification_Record_Change
 TRUNCATE TABLE _AL_Alert_Notification_Query_Change
 TRUNCATE TABLE _AL_Alert_Notification
 TRUNCATE TABLE _AL_Alert_Delivery_Method
 TRUNCATE TABLE _AL_Alert_User
 TRUNCATE TABLE _AL_Alert_Datastore_Keys
 TRUNCATE TABLE _AL_Alert_Datastore
 TRUNCATE TABLE _AL_Alert
 TRUNCATE TABLE _AL_Drillthrough_View_Log
 TRUNCATE TABLE _AL_Edit_Log
 TRUNCATE TABLE _AL_View_Log
 TRUNCATE TABLE _AL_Queue

  Return
