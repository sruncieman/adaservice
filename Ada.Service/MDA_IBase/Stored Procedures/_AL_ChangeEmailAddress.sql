﻿CREATE PROCEDURE [dbo].[_AL_ChangeEmailAddress] 
 ( 
 @uid nvarchar(50) , 
 @email nvarchar(2000) 
 ) 
 AS 
 SET NOCOUNT ON 
 
 UPDATE    _AL_Alert_User 
 SET              email = @email 
 WHERE     (UID = @uid) 
 
 
  Return 
