﻿CREATE PROCEDURE dbo._AL_LoadAlertChangedKeys
(@AlertID int )

 AS
 SET NOCOUNT ON
 SELECT     Record_ID, Unique_ID , '' AS CommandText,  '' AS QueryDefinition
 FROM    _AL_Alert_Datastore_Keys
 WHERE        (alert_id = @AlertID)
 RETURN