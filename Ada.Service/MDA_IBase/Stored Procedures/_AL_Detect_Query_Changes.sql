﻿CREATE PROCEDURE [dbo].[_AL_Detect_Query_Changes] 
 ( 
 @alert_id int, 
 @record_id nvarchar(50), 
 @SQL nvarchar(4000), 
 @BatchID UniqueIdentifier, 
 @QueueNo int,  @type int 
 ) 
 AS 
 BEGIN 
 
 SET NOCOUNT ON; 
 
 DECLARE @deleted_count int 
 DECLARE @inserted_count int 
 
 TRUNCATE TABLE #QueryResults 
 TRUNCATE TABLE #QueryChanges 
 EXECUTE (@SQL) 
 IF @@ERROR <> 0 
  BEGIN 
       EXEC _AL_Invalid_View  @alert_id 
       Return 
  End 
 UPDATE    _AL_Alert_Datastore 
 SET        PrevSnapshotDate = SnapshotDate ,  SnapshotDate = GETDATE() 
 WHERE     (alert_id = @alert_id) AND (Record_ID = @record_id) 
 
 IF @type = 1 
 BEGIN 
     INSERT INTO #QueryChanges (Unique_ID, Change_Type) 
     SELECT     _AL_Alert_Datastore_Keys.Unique_ID AS Deleted_ID, 0 As change_type 
     FROM         _AL_Alert_Datastore_Keys LEFT OUTER JOIN 
               #QueryResults ON _AL_Alert_Datastore_Keys.Unique_ID = #QueryResults.Unique_ID 
     WHERE     (_AL_Alert_Datastore_Keys.alert_id = @alert_id) AND (_AL_Alert_Datastore_Keys.Record_ID = @record_id) AND (#QueryResults.Unique_ID IS NULL) 
     End 
 Else 
 BEGIN 
    INSERT INTO [#QueryChanges] 
                      (Unique_ID, Change_Type) 
    SELECT     _AL_Edit_Log.Unique_ID, 0 AS Change_Type 
    FROM         _AL_Edit_Log INNER JOIN 
                          _AL_Alert_Datastore_Keys ON _AL_Edit_Log.Unique_ID = _AL_Alert_Datastore_Keys.Unique_ID 
    WHERE     (_AL_Edit_Log.Delete_Date IS NOT NULL) AND (_AL_Alert_Datastore_Keys.alert_id = @alert_id) OR 
                          (_AL_Alert_Datastore_Keys.alert_id = @alert_id) AND (_AL_Alert_Datastore_Keys.Record_Status = 254) 
 End 
 
 SET @deleted_count = (SELECT @@ROWCOUNT) 
 INSERT INTO #QueryChanges (Unique_ID, Change_Type) 
 SELECT     Unique_ID As Inserted_ID, 1 As change_type 
 FROM       #QueryResults 
 WHERE     (NOT (Unique_ID IN 
               (SELECT     Unique_ID 
                 FROM          _AL_Alert_Datastore_Keys 
                 WHERE      (alert_id = @alert_id) AND (Record_ID = @record_id )))) 
 
 SET @inserted_count = (SELECT @@ROWCOUNT) 
 
 IF @inserted_count > 0 OR @deleted_count > 0
 BEGIN 
    EXEC _AL_AddQuery_Notification @alert_id, @record_id, @BatchID , @QueueNo, @deleted_count, @inserted_count 
 End 
 End 
