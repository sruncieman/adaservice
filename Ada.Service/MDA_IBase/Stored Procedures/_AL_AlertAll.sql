﻿CREATE PROCEDURE [dbo].[_AL_AlertAll] 
 ( @user nvarchar(255) = '', @CaseID nvarchar(50) = '' ) 
  AS 
  SET NOCOUNT ON 
 
 IF @CaseID = '' 
 BEGIN 
       SELECT     alert_id, CASE Create_User WHEN @user THEN 0 ELSE 1 END AS Creator 
       FROM         _AL_Alert 
       Where (alert_status = 0) 
       ORDER BY Creator ,Create_User, alert_name, alert_description 
  End 
  Else 
  BEGIN 
     SELECT     alert_id, CASE Create_User WHEN @user THEN 0 ELSE 1 END AS Creator 
     FROM         _AL_Alert 
     WHERE     (alert_status = 0) AND (CaseID = @CaseID) 
     ORDER BY Creator, Create_User, alert_name, alert_description 
  End 
 
  Return 
