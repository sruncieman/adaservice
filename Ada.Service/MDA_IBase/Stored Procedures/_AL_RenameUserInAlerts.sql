﻿CREATE PROCEDURE [dbo].[_AL_RenameUserInAlerts] 
 ( 
 @uid nvarchar(50) , 
 @user nvarchar(255) 
 ) 
 AS 
 SET NOCOUNT ON 
 
 UPDATE    _AL_Alert 
 SET              Create_User = @user 
 WHERE     (UID = @uid) 
 
 UPDATE    _AL_Alert_User 
 SET              user_name = @user 
 WHERE     (UID = @uid) 
 
UPDATE    _AL_Alert_Delivery_Method 
  SET              user_name = @user 
  FROM         _AL_Alert_Delivery_Method INNER JOIN 
                        _AL_Alert_User ON _AL_Alert_Delivery_Method.alert_id = _AL_Alert_User.alert_id AND 
                        _AL_Alert_Delivery_Method.user_name = _AL_Alert_User.user_name 
  WHERE     (_AL_Alert_User.UID = @uid)
 UPDATE    _AL_Alert_Notification 
 SET              user_name = @user 
 WHERE     (UID = @uid) 
 
 Return 