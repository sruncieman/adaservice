﻿CREATE Procedure [dbo].[_AL_LoadAlertNotificationsQueryChanges]
(@NotificationID int)
AS
BEGIN
SET NOCOUNT ON

SELECT     alert_id, Record_ID, Unique_ID
FROM         _AL_Alert_Notification_Query_Change
WHERE     (change_type = 0) AND (Notification_ID = @NotificationID)

SELECT     alert_id, Record_ID, Unique_ID
FROM         _AL_Alert_Notification_Query_Change
WHERE     (change_type = 1) AND (Notification_ID = @NotificationID)

 SELECT     alert_id, Unique_ID
 FROM         _AL_Alert_Notification_Record_Change
 WHERE     (Notification_ID = @NotificationID)

 SELECT     alert_id, Unique_ID
 FROM         _AL_Alert_Notification_Record_View 
 WHERE     (Notification_ID = @NotificationID)
END

