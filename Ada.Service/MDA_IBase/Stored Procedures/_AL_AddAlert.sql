﻿CREATE PROCEDURE dbo.[_AL_AddAlert]
 (
 @alert_id int OUTPUT,
 @create_user nvarchar(255),
 @create_date datetime,
 @name nvarchar(255),
 @description nvarchar(255),
 @expiry_date datetime,
 @alert_type int,
 @detection_options int,
 @execution_time float=0,
 @queue_no int = 0, @case_id nvarchar(50), @uid nvarchar(50))
 AS
 SET NOCOUNT ON
 

IF EXISTS( SELECT     TOP 1 ISNULL(_AL_Queue.QueueNo, 0) AS QueueNo
 FROM         _AL_Queue LEFT OUTER JOIN
                       _AL_Alert ON _AL_Queue.QueueNo = _AL_Alert.queue_no
 GROUP BY ISNULL(_AL_Queue.QueueNo, 0), _AL_Queue.ExecutionTime, _AL_Queue.QueueNo, _AL_Alert.alert_status 
 HAVING      (_AL_Alert.alert_status = 0) OR _AL_Alert.alert_status IS NULL ORDER BY _AL_Queue.ExecutionTime DESC, COUNT(_AL_Alert.alert_id), QueueNo)
 SET @queue_no  = ( SELECT     TOP 1 ISNULL(_AL_Queue.QueueNo, 0) AS QueueNo
        FROM         _AL_Queue LEFT OUTER JOIN 
                              _AL_Alert ON _AL_Queue.QueueNo = _AL_Alert.queue_no
        GROUP BY ISNULL(_AL_Queue.QueueNo, 0), _AL_Queue.ExecutionTime, _AL_Queue.QueueNo, _AL_Alert.alert_status
        HAVING      (_AL_Alert.alert_status = 0) OR _AL_Alert.alert_status IS NULL ORDER BY _AL_Queue.ExecutionTime DESC, COUNT(_AL_Alert.alert_id), QueueNo)

  
 INSERT INTO _AL_Alert
                    (UID, CaseID ,Create_User, Create_Date, alert_name, expiry_date,alert_type,alert_description,detection_options,execution_time,queue_no,alert_status )
 VALUES     (@uid, @case_id,@create_user,GETDATE(),@name,@expiry_date,@alert_type,@description,@detection_options,@execution_time,@queue_no,0 )

 
 SET @alert_id = SCOPE_IDENTITY()
 RETURN