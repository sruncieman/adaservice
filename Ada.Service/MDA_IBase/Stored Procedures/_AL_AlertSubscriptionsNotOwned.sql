﻿CREATE PROCEDURE [dbo].[_AL_AlertSubscriptionsNotOwned] 
 ( @owner nvarchar(255), @CaseID nvarchar(50) = '' ) 
 AS 
 SET NOCOUNT ON 
 
 IF @CaseID = '' 
 BEGIN 
 SELECT DISTINCT _AL_Alert_User.alert_id, _AL_Alert.alert_name 
 FROM         _AL_Alert_User INNER JOIN 
                       _AL_Alert ON _AL_Alert_User.alert_id = _AL_Alert.alert_id 
 WHERE     (_AL_Alert_User.user_name = @owner) AND (_AL_Alert.Create_User <> @owner) AND (_AL_Alert.expiry_date >= GETDATE() OR 
                       _AL_Alert.expiry_date IS NULL) AND (_AL_Alert.alert_status = 0) 
 ORDER BY _AL_Alert.alert_name 
 End 
 Else 
 BEGIN 
 SELECT DISTINCT _AL_Alert_User.alert_id, _AL_Alert.alert_name 
 FROM         _AL_Alert_User INNER JOIN 
                       _AL_Alert ON _AL_Alert_User.alert_id = _AL_Alert.alert_id 
 WHERE     (_AL_Alert_User.user_name = @owner) AND (_AL_Alert.Create_User <> @owner) AND (_AL_Alert.expiry_date >= GETDATE() OR 
                       _AL_Alert.expiry_date IS NULL) AND (_AL_Alert.alert_status = 0) AND (_AL_Alert.CaseID = @CaseID) 
 ORDER BY _AL_Alert.alert_name 
 End 
 
 Return 