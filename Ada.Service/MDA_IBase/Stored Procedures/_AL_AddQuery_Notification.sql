﻿CREATE PROCEDURE [dbo].[_AL_AddQuery_Notification]  
 ( 
 @alert_id int, 
 @record_id nvarchar(50), 
 @BatchID UniqueIdentifier, 
 @QueueNo Int, @DeletedCount Int, @InsertedCount Int 
 ) 
 AS 
 BEGIN 
 -- #QueryChanges contains relevant changes 
 SET NOCOUNT ON; 
 
 DECLARE @ChangeOption int 
 
  SET @ChangeOption = (SELECT detection_options  AS ViewAlert FROM  _AL_Alert WHERE (alert_id = @alert_id)) 
     IF (@ChangeOption & 4 = 4 AND @DeletedCount > 0) OR (@ChangeOption & 8 = 8 AND @InsertedCount > 0) 
 BEGIN 
 
INSERT INTO _AL_Alert_Notification
                       (alert_id, delivery_method_id, user_name, raised_on, Batch_ID, PrevSnapshotDate, UID)
 SELECT     _AL_Alert_Delivery_Method.alert_id, _AL_Alert_Delivery_Method.delivery_method_id, _AL_Alert_Delivery_Method.user_name, GETDATE()
                       AS currentDate, @BatchID AS Expr1, _AL_Alert_Datastore.PrevSnapshotDate, _AL_Alert_User.UID
 FROM         _AL_Alert_Delivery_Method INNER JOIN
                       _AL_Alert_Datastore ON _AL_Alert_Delivery_Method.alert_id = _AL_Alert_Datastore.alert_id INNER JOIN
                       _AL_Alert_User ON _AL_Alert_Delivery_Method.alert_id = _AL_Alert_User.alert_id AND
                       _AL_Alert_Delivery_Method.user_name = _AL_Alert_User.user_name
 WHERE     (_AL_Alert_Delivery_Method.alert_id = @alert_id) AND (_AL_Alert_Datastore.Record_ID = @record_id)
 
        INSERT INTO _AL_Alert_Notification_Query_Change 
                  (Unique_ID, change_type, alert_id, Record_ID, Notification_ID) 
        SELECT     Unique_ID, Change_Type, @alert_id AS alert_id, @record_id AS Record_ID, _AL_Alert_Notification.Notification_ID 
        FROM       #QueryChanges, _AL_Alert_Notification, _AL_Alert 
        WHERE _AL_Alert_Notification.Batch_ID = @BatchID AND _AL_Alert.alert_id = _AL_Alert_Notification.alert_id AND _AL_Alert.detection_options & 8 = 8 AND #QueryChanges.Change_Type = 1 
 
        INSERT INTO _AL_Alert_Notification_Query_Change 
                  (Unique_ID, change_type, alert_id, Record_ID, Notification_ID) 
        SELECT     Unique_ID, Change_Type, @alert_id AS alert_id, @record_id AS Record_ID, _AL_Alert_Notification.Notification_ID 
      FROM       #QueryChanges, _AL_Alert_Notification, _AL_Alert 
        WHERE _AL_Alert_Notification.Batch_ID = @BatchID AND _AL_Alert.alert_id = _AL_Alert_Notification.alert_id AND _AL_Alert.detection_options & 4 = 4 AND #QueryChanges.Change_Type = 0 
 
 End 
 
 INSERT INTO _AL_Alert_Removed_Keys 
                       (alert_id, Unique_ID,QueueNo) 
 SELECT @alert_id, Unique_ID, @QueueNo FROM #QueryChanges WHERE Change_Type = 0 
 
 DELETE FROM _AL_Alert_Datastore_Keys 
 WHERE     (alert_id = @alert_id)  AND (Record_ID = @record_id) AND (Unique_ID IN (SELECT Unique_ID FROM #QueryChanges WHERE Change_Type = 0)) 
 
 
 INSERT INTO _AL_Alert_Datastore_Keys 
           (Unique_ID,alert_id,Record_ID) 
 SELECT     Unique_ID, @alert_id AS alert_id, @record_id AS Record_ID 
 FROM         #QueryChanges WHERE Change_type = 1 
 IF (@ChangeOption & 4 = 4 AND @DeletedCount > 0) OR (@ChangeOption & 8 = 8 AND @InsertedCount > 0) 
 BEGIN  
        INSERT INTO _AL_Alert_Notification_Record_Change  
                              (alert_id, Unique_ID, date_changed, edit_user_name, Database_Action_Type, Audit_ID, Record_Status, user_name, SCC_New, SCC_Old,  
                              Notification_ID, parent_unique_id, New, Edit, Deleted, Batch_ID, SoftDeleted, Restored) 
        SELECT DISTINCT 
                              _AL_EditAlerts.alert_id, _AL_EditAlerts.Unique_ID, _AL_EditAlerts.date_changed, _AL_EditAlerts.user_name, _AL_EditAlerts.Database_Action_Type, 
                              _AL_EditAlerts.Audit_ID, _AL_EditAlerts.Record_Status, _AL_Alert_User.user_name AS Expr1, _AL_EditAlerts.SCC_New, _AL_EditAlerts.SCC_Old, 
                              _AL_Alert_Notification.Notification_ID, _AL_EditAlerts.Parent_ID, _AL_EditAlerts.New, _AL_EditAlerts.Edit, _AL_EditAlerts.Deleted, 
                              _AL_EditAlerts.Audit_Batch_ID, CAST(_AL_EditAlerts.SoftDeleted AS Bit) AS Expr2, CAST(_AL_EditAlerts.Restored AS Bit) AS Expr3 
        FROM         _AL_EditAlerts INNER JOIN 
                              _AL_Alert_User INNER JOIN 
                              _AL_Alert_Notification ON _AL_Alert_User.alert_id = _AL_Alert_Notification.alert_id AND 
                              _AL_Alert_User.user_name = _AL_Alert_Notification.user_name ON _AL_EditAlerts.alert_id = _AL_Alert_User.alert_id 
        WHERE     (_AL_Alert_Notification.Batch_ID = @BatchID) AND (_AL_EditAlerts.queue_no = @QueueNo) AND (_AL_EditAlerts.New = 1) AND 
                              (_AL_EditAlerts.Parent_ID IN 
                                  (SELECT     _AL_EditAlerts.Unique_ID 
                                    From [#QueryChanges] 
                     WHERE      (Change_type = 1))) OR 
                              (_AL_EditAlerts.New = 1) AND (_AL_EditAlerts.Unique_ID IN 
                                  (SELECT     _AL_EditAlerts.Unique_ID 
                                    FROM          [#QueryChanges] AS [#QueryChanges_1] 
                                    WHERE      (Change_type = 1))) 
 END
 End 
