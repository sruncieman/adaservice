﻿CREATE PROCEDURE [dbo].[_AL_send_alert_emails]
(
    @subject nvarchar(255),
    @bodyHeader nvarchar(255)
)
AS
SET NOCOUNT ON

DECLARE @email nvarchar(255)
DECLARE @alert nvarchar(255)
DECLARE @text nvarchar(4000)
DECLARE @last_email nvarchar(255)
DECLARE @body nvarchar(4000)
DECLARE @last_alert nvarchar(255)
DECLARE @ID int

CREATE TABLE #TempIDs(ID INT )
SET @bodyHeader = @bodyHeader + CONVERT(varchar(50),GETDATE(),100) + ':'

BEGIN TRAN
DECLARE email_cursor CURSOR FAST_FORWARD  FOR
SELECT     _AL_Alert_User.email, _AL_Alert.alert_name, _AL_Alert_Notification.Notification_ID
FROM         _AL_Alert_Notification INNER JOIN
                  _AL_Alert_User ON _AL_Alert_Notification.alert_id = _AL_Alert_User.alert_id AND _AL_Alert_Notification.user_name = _AL_Alert_User.user_name INNER JOIN
                  _AL_Alert ON _AL_Alert_User.alert_id = _AL_Alert.alert_id
WHERE     (_AL_Alert_Notification.delivery_method_id = 1)
ORDER BY _AL_Alert_User.email, _AL_Alert.alert_name

OPEN email_cursor

FETCH NEXT FROM email_cursor
INTO @email, @alert, @ID

SET @last_email = @email
SET @last_alert = ''
SET @text = ''
WHILE @@FETCH_STATUS = 0
BEGIN
INSERT INTO #TempIDs VALUES (@ID)

IF @email = @last_email
BEGIN
    IF @alert <> @last_alert
    BEGIN
        SET @text = @text + char(13) + char(10) + @alert
    END
END
ELSE
BEGIN

    SET @body = @bodyHeader + char(13) + char(10) + char(13) + char(10) + @text + char(13) + char(10)
    EXECUTE  _AL_send_alert_email @last_email, @subject, @body

    DELETE #TempIDs
    SET @text = ''
    SET @last_email = @email
    SET @text = @text + char(13) + char(10) + @alert
END

SET @last_alert = @alert
FETCH NEXT FROM email_cursor
INTO @email, @alert, @ID
END

SET @body = @bodyHeader + char(13) + char(10) + char(13) + char(10) + @text + char(13) + char(10)
EXECUTE _AL_send_alert_email @last_email, @subject, @body

DELETE #TempIDs

Close email_cursor
DEALLOCATE email_cursor

COMMIT TRAN

RETURN
