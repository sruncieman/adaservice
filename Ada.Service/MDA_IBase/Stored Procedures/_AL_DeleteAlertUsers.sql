﻿CREATE PROCEDURE dbo.[_AL_DeleteAlertUsers]
 (@alert_id int)
 
 AS
 SET NOCOUNT ON
 DELETE FROM _AL_Alert_Delivery_Method
 WHERE        (alert_id = @alert_id)
 
 DELETE FROM _AL_Alert_User
 WHERE        (alert_id = @alert_id)
 RETURN