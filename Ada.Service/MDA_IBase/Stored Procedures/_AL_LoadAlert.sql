﻿CREATE PROCEDURE dbo._AL_LoadAlert
 (@AlertID int )
 AS
 SET NOCOUNT ON
 -- Load alert common data
 SELECT     Create_User, Create_Date, Last_Upd_Date, Last_Upd_User, alert_name, expiry_date, alert_type, alert_description, detection_options, alert_status,
          execution_time, queue_no, CaseID 
 FROM            _AL_Alert
 WHERE   (alert_id = @AlertID)
 
 SELECT         user_name, email, UID
 FROM            _AL_Alert_User
 WHERE        (alert_id = @AlertID)
 
SELECT         delivery_method_id, user_name
 FROM            _AL_Alert_Delivery_Method
 WHERE        (alert_id = @AlertID)
 RETURN