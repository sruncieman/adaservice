﻿CREATE PROCEDURE dbo.[_AL_EditAlert]
 (
 @alert_id int,
 @update_user nvarchar(255),
 @name nvarchar(255),
 @expiry_date datetime,
 @description nvarchar(255),
 @detection_options int,
 @alert_status int,
 @execution_time float,
 @queue_no tinyint
 )
 AS
 SET NOCOUNT ON
 UPDATE       _AL_Alert
 SET          Last_Upd_Date = GETDATE(), Last_Upd_User =@update_user, alert_name =@name, expiry_date =@expiry_date, alert_description=@description, detection_options=@detection_options, alert_status = @alert_status, execution_time = @execution_time, queue_no = @queue_no 
 WHERE        (alert_id = @alert_id)
 Return