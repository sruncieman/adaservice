﻿CREATE PROCEDURE [dbo].[_AL_send_alert_email]
(
@To nvarchar(Max),
@SubjectText nvarchar(255),
@BodyText nvarchar(MAX)
)
AS
SET NOCOUNT ON
DECLARE @Success int
DECLARE @LogTExt nvarchar(4000)
IF @To IS NULL
BEGIN
    RETURN 0
END
SET @LogText = 'Email sent to: ' + @To + char(13) + char(10) + @BodyText

EXECUTE @Success = msdb.dbo.sp_send_dbmail  @recipients = @To, @subject = @SubjectText, @body = @BodyText
IF @@ERROR <> 0
BEGIN
    RETURN 1
END

IF @Success = 0
BEGIN
    -- email sent so archive ID's in #TempIDs
    INSERT INTO [$(MDA_IBase_Log)].dbo._AuditLog
                          (Action_Type, User_Name, Network_Login, Detail, Date_Time)
    VALUES     (98, 'Email Alert', 'Email Alert',@LogText, GETDATE())
 
DELETE FROM _AL_Alert_Notification_Record_View 
 WHERE     (Notification_ID IN 
                           (SELECT ID FROM [#TempIDs])) 
 
 DELETE FROM _AL_Alert_Notification_Record_Change 
 WHERE     (Notification_ID IN 
                           (SELECT ID FROM [#TempIDs])) 
DELETE FROM _AL_Alert_Notification_Query_Change 
WHERE     (Notification_ID IN
                          (SELECT ID FROM [#TempIDs]))

DELETE FROM _AL_Alert_Notification
WHERE     (Notification_ID IN
                          (SELECT ID FROM [#TempIDs]))
END

RETURN @Success
