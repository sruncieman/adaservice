﻿CREATE PROCEDURE [dbo].[_AL_AddAlertMessageToOwner]
 (
  @alert_id int ,
  @title nvarchar(255),
  @message nvarchar(1000)
 )
 AS
 SET NOCOUNT ON

 DECLARE @user nvarchar(255)

 SET @user = (SELECT     Create_User
        FROM         _AL_Alert
        WHERE     (alert_id = @alert_id))

 INSERT INTO _AL_Alert_Notification
                      (alert_id, delivery_method_id, user_name, raised_on, IsMessage, Title, Message)
 VALUES     (@alert_id, 0, @user, GETDATE(), 1,@title,@message)

 Return 
