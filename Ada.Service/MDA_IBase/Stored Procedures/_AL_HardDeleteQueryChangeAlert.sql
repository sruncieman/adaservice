﻿CREATE PROCEDURE [dbo].[_AL_HardDeleteQueryChangeAlert]
 (@alert_id int)
 AS
SET NOCOUNT ON

 EXEC _AL_DeleteAlertUsers @alert_id

 DELETE FROM _AL_Alert_Datastore_Keys
 WHERE     (alert_id = @alert_id)
 DELETE FROM _AL_Alert_Notification_Record_View
 WHERE     (alert_id = @alert_id)

  DELETE FROM _AL_Alert_Notification_Record_Change
  WHERE     (alert_id = @alert_id)

  DELETE FROM _AL_Alert_Notification_Query_Change
  WHERE     (alert_id = @alert_id)
  DELETE FROM _AL_Alert_Notification WHERE (alert_id = @alert_id)

 DELETE FROM _AL_Alert_Datastore
 WHERE     (alert_id = @alert_id)

 DELETE FROM _AL_Alert
 WHERE        (alert_id = @alert_id)
 RETURN
