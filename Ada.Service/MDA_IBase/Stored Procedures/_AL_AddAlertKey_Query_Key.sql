﻿CREATE PROCEDURE  [dbo].[_AL_AddAlertKey_Query_Key]
(@alert_id int, @record_id nvarchar(255), @Unique_ID nvarchar(255), @Record_Status tinyint = 0, @SCC nvarchar(255) = NULL  )

AS
SET NOCOUNT ON
INSERT INTO _AL_Alert_Datastore_Keys
                 (alert_id, Record_ID, Unique_ID, Record_Status, SCC)
VALUES        (@alert_id,@record_id,@Unique_ID, @Record_Status, @SCC)
RETURN
