﻿CREATE Procedure [dbo].[_AL_LoadAlertNotificationsRecordChanges]
 (@NotificationID int)
 AS
 BEGIN
 SET NOCOUNT ON
 
 SELECT DISTINCT alert_id, Unique_ID
 FROM         _AL_Alert_Notification_Record_Change
 WHERE     (Notification_ID = @NotificationID)

 SELECT DISTINCT alert_id, Unique_ID
 FROM         _AL_Alert_Notification_Record_View
 WHERE     (Notification_ID = @NotificationID)

 End
