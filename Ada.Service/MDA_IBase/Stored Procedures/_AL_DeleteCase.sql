﻿CREATE PROCEDURE [dbo].[_AL_DeleteCase] 
 ( 
 @case_id nvarchar(50) 
 ) 
 AS 
 SET NOCOUNT ON 
 
 DELETE FROM _AL_Alert_Notification_Record_View 
 FROM         _AL_Alert_Notification_Record_View INNER JOIN 
                      _AL_Alert ON _AL_Alert_Notification_Record_View.alert_id = _AL_Alert.alert_id 
 WHERE     (_AL_Alert.CaseID = @case_id) 
 
 DELETE FROM _AL_Alert_Notification_Record_Change 
 FROM         _AL_Alert_Notification_Record_Change INNER JOIN 
                       _AL_Alert ON _AL_Alert_Notification_Record_Change.alert_id = _AL_Alert.alert_id 
 WHERE     (_AL_Alert.CaseID = @case_id) 
 
 DELETE FROM _AL_Alert_Notification_Query_Change 
 FROM         _AL_Alert_Notification_Query_Change INNER JOIN 
                       _AL_Alert ON _AL_Alert_Notification_Query_Change.alert_id = _AL_Alert.alert_id 
 WHERE     (_AL_Alert.CaseID = @case_id) 
 
 DELETE FROM _AL_Alert_Notification 
 FROM         _AL_Alert_Notification INNER JOIN 
                       _AL_Alert ON _AL_Alert_Notification.alert_id = _AL_Alert.alert_id 
 WHERE     (_AL_Alert.CaseID = @case_id) 
 
 DELETE FROM _AL_Alert_Delivery_Method 
 FROM         _AL_Alert_Delivery_Method INNER JOIN 
                       _AL_Alert ON _AL_Alert_Delivery_Method.alert_id = _AL_Alert.alert_id 
 WHERE     (_AL_Alert.CaseID = @case_id) 
 
  DELETE FROM _AL_Alert_User 
 FROM         _AL_Alert_User INNER JOIN 
                       _AL_Alert ON _AL_Alert_User.alert_id = _AL_Alert.alert_id 
 WHERE     (_AL_Alert.CaseID = @case_id) 
 
  DELETE FROM _AL_Alert_Datastore_Keys 
 FROM         _AL_Alert_Datastore_Keys INNER JOIN 
                       _AL_Alert ON _AL_Alert_Datastore_Keys.alert_id = _AL_Alert.alert_id 
 WHERE     (_AL_Alert.CaseID = @case_id) 
 
  DELETE FROM _AL_Alert_Datastore 
 FROM         _AL_Alert_Datastore INNER JOIN 
                       _AL_Alert ON _AL_Alert_Datastore.alert_id = _AL_Alert.alert_id 
 WHERE     (_AL_Alert.CaseID = @case_id) 
 
 DELETE FROM _AL_Alert 
 FROM         _AL_Alert INNER JOIN 
                       _AL_Alert AS _AL_Alert_1 ON _AL_Alert.alert_id = _AL_Alert_1.alert_id 
 WHERE     (_AL_Alert.CaseID = @case_id) 
 
 
 
  Return 
