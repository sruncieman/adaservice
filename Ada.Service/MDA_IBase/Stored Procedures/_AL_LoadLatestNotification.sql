﻿CREATE Procedure [dbo].[_AL_LoadLatestNotification]
 (
 @DeliveryMethodId Int, @Username nvarchar(255), @LastID int , @NumberUnread Int OUTPUT
 )
 AS
 BEGIN
  SET NOCOUNT ON

--, @Username nvarchar(255), @LastID int = -1, @NumberUnread Int

 SELECT     TOP 1 AN.alert_id, _AL_Alert.alert_name, AN.delivery_method_id, AN.user_name, AN.raised_on, AN.read_on, AN.Notification_ID, _AL_Alert.alert_type,
                      _AL_Alert.Create_User, AlertRead ,Deleted , FollowUp , _AL_Alert.detection_options,  ISNULL(AN.Status ,0) As Status, AN.PrevSnapshotDate,ISNULL(AN.IsMessage,0) AS IsMessage, AN.Title, AN.Message
 FROM         _AL_Alert_Notification AS AN INNER JOIN
                      _AL_Alert ON _AL_Alert.alert_id = AN.alert_id
 WHERE     (AN.delivery_method_id = @DeliveryMethodId) AND (AN.user_name = @Username) AND (AN.Notification_ID > @LastID)  AND ISNULL(AN.Status,0) = 0 
 ORDER BY AN.Notification_ID DESC

 SET @NumberUnread = (SELECT     ISNULL(COUNT(Notification_ID), 0) AS NoUnread
        FROM         _AL_Alert_Notification
        WHERE     AlertRead = 0 AND Deleted = 0  AND (user_name = @UserName)) 

 END
