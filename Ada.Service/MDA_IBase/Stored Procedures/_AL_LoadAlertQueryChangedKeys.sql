﻿CREATE PROCEDURE [dbo].[_AL_LoadAlertQueryChangedKeys]
(@AlertID int )

AS
SET NOCOUNT ON

SELECT     alert_id, Record_ID AS Unique_ID, CommandText,  QueryDefinition 
FROM         _AL_Alert_Datastore
WHERE     (alert_id = @AlertID)

Return