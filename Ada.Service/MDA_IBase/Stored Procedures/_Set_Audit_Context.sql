﻿CREATE PROCEDURE [dbo].[_Set_Audit_Context] 
  @ApplicationUser nvarchar(128), 
 @Reason nvarchar(255), @MachineName nvarchar(255), @OS_User_Name nvarchar(255), @Location nvarchar(50), @GIDS nvarchar(MAX) = '', @Restrict nvarchar(MAX) = '' 
 AS BEGIN   
 SET NOCOUNT ON; 
 DECLARE @Session_ID Int 
 DECLARE @Context varchar(30) 
 DECLARE @ContextBinary as varbinary(128) 
 SET @Session_ID = @@SPID 
 DELETE FROM _AuditContext WHERE (Session_ID = @Session_ID) 
 INSERT INTO _AuditContext 
       (Session_ID, ApplicationUser, Reason, MachineName, OS_User_Name, Location, GIDS, Created_On, Restricted) 
       VALUES  (@Session_ID,@ApplicationUser,@Reason,@MachineName,@OS_User_Name, @Location, @GIDS, GetDate(),@Restrict) 
 SET @Context = 'i2:' + CAST(@Session_ID As Varchar(30)) 
 SET @ContextBinary = CAST(@Context As Varbinary(128)) 
 SET Context_Info  @ContextBinary 
 END 