﻿using System;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.TracesmartService.Interface;
using MDA.TracesmartService.Model;
using MDA.TracesmartService.Impl;
using MDA.TracesmartService.Impl.TMServiceReference;
using System.Configuration;
using MDA.Common.Server;

namespace MDA.TracesmartService.Test
{
    [TestClass]
    public class TracemartTest
    {
        [TestMethod]
        public void TestBasicConnection()
        {
            CurrentContext ctx = new CurrentContext(0, 0, "Test");

            ITracesmartService tm = new TracesmartService.Impl.TracesmartService();

            PersonInfo p = new PersonInfo()
            {
                Forename = "Colin",
                Middle   = "Graham",
                Surname  = "Gowan",
                Gender   = "M",
                DOB      = "1962-01-01",
                Address1 = "85 Julius Road",
                Address2 = "Bristol",
                Address3 = "",
                Address4 = "",
                Address5 = "",
                Address6 = "",
                Postcode = "BS7 8EU"
            };


           // p.Cardavs = new CardInfo();

            //var v = tm.CallService(p, true);

            //Assert.AreEqual<string>(v.Summary.ResultText, "PASS");
            //Assert.AreEqual<bool>(v.Summary.Status, true);
        }

        [TestMethod]
        public void TestFullPopulation()
        {

            PersonInfo p = new PersonInfo()
            {
                Forename = "Colin",
                Middle   = "Graham",
                Surname  = "Gowan",
                Gender   = "M",
                DOB      = "1962-01-01",
                Address1 = "85 Julius Road",
                Address2 = "Bristol",
                Address3 = "",
                Address4 = "",
                Address5 = "",
                Address6 = "",
                Postcode = "BS7 8EU"
            };

            var login = new LoginDetails()
            {
                username = ConfigurationManager.AppSettings["TracemartUserName"].ToString(),
                password = ConfigurationManager.AppSettings["TracemartPassword"].ToString()
            };


            var serviceDetails = new ServiceDetails()
            {
                // must call these
                address     = true,
                deathscreen = true,
                dob         = true,
                sanction    = true,
                insolvency  = true,
                ccj         = true,


                // disable for now
                passport = false,     // requires gender and dob which are already reqd

                //optional, reqs landline number
                telephone = false, //!string.IsNullOrEmpty(personInfo.LandlineTelephoneField),

                //optional, reqs defaults + middlename??
                driving = false, //!string.IsNullOrEmpty(personInfo.DrivinglicenceField),

                //optional, never have maiden name
                birth = false,

                //optional, defaults + optional fields
                smartlink = false,

                // call is we have NI number
                ni = false, //!string.IsNullOrEmpty(personInfo.NIField),

                // call is we have card number
                cardnumber = false, //!string.IsNullOrEmpty(personInfo.CardnumberField),

                // needs sort number and account number
                bankmatch = false, //!string.IsNullOrEmpty(personInfo.SortcodeField) && !string.IsNullOrEmpty(personInfo.AccountnumberField),

                // needs mobile number field
                mobile = false, //!string.IsNullOrEmpty(personInfo.MobileTelephoneField),

                // Explicitly disable non-required services
                crediva      = false,
                creditactive = false,
                nhs          = false,
                cardavs      = false,
                mpan         = false,
            };


            // Optional, to aid request/response tracking
            //var iduDetails = new IDUDetails()
            //{
            //    ID = "your-id",
            //    IKey = "your-ikey",
            //    Reference = "your-reference",
            //    Scorecard = "IDU Default",
            //    equifaxUsername = ""
            //};

            // Create Request
            var request = new Request()
            {
                Login    = login,
                Person   = Translator.Translate(p),
                Services = serviceDetails,
                //IDU    = iduDetails
            };

            // Make synchronous call to IduService 
            //service.Timeout = 20000;

            iduPortClient proxy = new iduPortClient();

            try
            {
                var a = proxy.IDUProcess(request);

                ResultInfo results = Translator.Translate(a);

                FilteredResults r = Translator.Translate(results);

                //Type eType = a.GetType().ToString();

                MemoryStream ms = new MemoryStream();

                DataContractSerializer ser = new DataContractSerializer(a.GetType());
                ser.WriteObject(ms, a);
                ms.Position = 0;
                var sr = new StreamReader(ms);
                string xml = sr.ReadToEnd();
                //int aa = 2;

            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
        }
    }
}
