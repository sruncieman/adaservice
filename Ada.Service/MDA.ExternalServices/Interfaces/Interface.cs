﻿using MDA.Common.Server;
using MDA.DAL;
using MDA.ExternalServices.Model;


namespace MDA.ExternalService.Interface
{
    public interface IExternalService
    {
        /// <summary>
        /// Very simple interface. This method accepts the 3-field KEY in the request object that identifies the UNQIUE record in the
        /// RiskExternalServicesRequests table. From that information it will read data direct from ADA, make an external call, update ADA
        /// directly and return a populated Response object.
        /// return a populated Response object.
        /// </summary>
        /// <param name="ctx">The current context</param>
        /// <param name="resr">An object holding the 3-key index into the RiskExternalServicesRequests table</param>
        /// <returns>A Response object that contains the call status, any errors and the data sent and received via the service</returns>
        ExternalServiceCallResponse ProcessPipelineClaim(CurrentContext ctx, ExternalServiceCallRequest resr);

        /// <summary>
        /// Take the riskClaimId and uniqueId and work out which sub-services of the external service can be called.  Typically you would fetch all the data
        /// taking the uniqueId as the starting point. You would then work out which fields are available to call the sub-service.  You need to create an
        /// inityial mask of 0 and then or (|) into the mask the bit the spcifies the services that can be called. eg  mask |= 0x0001 sets the first bit
        /// </summary>
        /// <param name="ctx">Current Context</param>
        /// <param name="riskClaimId">RiskClaim_Id of this claim</param>
        /// <param name="uniqueId">The unique ID specific to this service</param>
        /// <returns>The bit mask</returns>
        ExternalServicesMaskResponse BuildMaskOfServicesThatCanBeCalled(CurrentContext ctx, int riskClaimId, int uniqueId);
    }
}
