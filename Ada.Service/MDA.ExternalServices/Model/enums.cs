﻿
namespace MDA.ExternalServices.Model
{
    /// <summary>
    /// The various status values a call can hold. Negative values are errors. Both 2 and 3 signify incomplete, needing retries.
    /// </summary>
    public enum RiskExternalServiceCallStatus
    {
        /// <summary>
        /// A RiskExternalService ID has been used but a call to that service has not been coded in C#
        /// </summary>
        ServiceCallNotCoded   = -6,

        /// <summary>
        /// The service returned FALSE, it could not process the data
        /// </summary>
        ResultsFalse          = -5,

        /// <summary>
        /// No results were returned from the service
        /// </summary>
        NoResults             = -4,

        /// <summary>
        /// The call was aborted. Usually a MUSTCALL service did not have sufficient data
        /// </summary>
        Aborted               = -3,

        /// <summary>
        /// The Unique data ID in the RiskExternalServiceRequests table could not be used to find data
        /// </summary>
        UniqueIdDataNotFound  = -2,

        /// <summary>
        /// The main entity searched for using the Unique ID could not be found
        /// </summary>
        PrimaryEntityNotFound = -1,

        /// <summary>
        /// The RiskExternalServiceRequest record has just been created and not processed. It needs to be processed
        /// </summary>
        Created               = 0,

        /// <summary>
        /// The call was completed successfully
        /// </summary>
        Success               = 1,

        /// <summary>
        /// The call failed but this was due to service availability. It should be retried later
        /// </summary>
        FailedNeedsRetry      = 2,

        /// <summary>
        /// The call failed but this was due to pre-existing service unavailability. It should be retried later
        /// </summary>
        UnavailableNeedsRetry = 3
    }

    /// <summary>
    /// Should a client Always, Never or Only if specified in the XML, call a particular External service.
    /// </summary>
    public enum RiskExternalServiceUsage
    {
        /// <summary>
        /// NEVER call this service
        /// </summary>
        NeverCall            = 0,

        /// <summary>
        /// ALWAYS Call this service
        /// </summary>
        AlwaysCall           = 1,

        /// <summary>
        /// ONLY Call this service if its name is listed as an External Service in the source XML
        /// </summary>
        OnlyIfSpecifiedInXml = 2
    }

    /// <summary>
    /// Is the external service enabled or has ADA disabled it due to it not responding
    /// </summary>
    public enum RiskExternalServiceCurrentStatus
    {
        /// <summary>
        /// The service is available to be called
        /// </summary>
        Enabled         = 0,

        /// <summary>
        /// ADA has tried (and retried) to call this service and it is not responding. It is disabled for a defined period.
        /// </summary>
        AdaAutoDisabled = 1
    }

    /// <summary>
    /// Specific to Tracesmart.  To set the availability and usage of each sub-service.
    /// </summary>
    public enum RiskExternalServiceTracesmartUse
    {
        /// <summary>
        /// Never call this sub-service even if pre-req data available
        /// </summary>
        NeverCall = 0,
 
        /// <summary>
        /// Can call this sub-service is sufficient pre-req data is available
        /// </summary>
        CanCall   = 1,

        /// <summary>
        /// If ADA cannot call this service due to lack of pre-req data then NO SERVICES should be called, the whole service call is aborted.
        /// </summary>
        MustCall  = 2, 
    }
}
