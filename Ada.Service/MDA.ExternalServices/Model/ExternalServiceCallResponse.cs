﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.ExternalServices.Model
{
    public class ExternalServiceCallResponse
    {
        public ExternalServiceCallResponse()
        {
            UseJsonSerialisation = true;
        }

        /// <summary>
        /// The Call Result Status
        /// </summary>
        public RiskExternalServiceCallStatus CallStatus { get; set; }

        /// <summary>
        /// A Mask of the services that were called/attempted. Can be zero
        /// </summary>
        public int ServicesCalledMask { get; set; }

        /// <summary>
        /// A simple error message that will be written to the Db record. Can be NULL
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// An object containing all the data sent to the service. It is JSON serialised into the DB for audit. Can be NULL
        /// </summary>
        public object RequestData { get; set; }

        /// <summary>
        /// An object containing all the data returned from the service. It is JSON serialised into the DB for audit. Can be NULL.
        /// If your service just returns a dataset that you want serialised "as-is" then add to Dict<> as ("Results", object)
        /// If your service returns data to be fed into pipeline add data as ("Motor", object) or ("PI", object).  So for CUE you may 
        /// want to add 3 DICT<> entries holding different claim types.  The name you use are the ones from the pipeline CONFIG.   Each
        /// DICT<> entry will result in a new pipeline of the correct type being created and you data passed "as-is" to it.
        /// </summary>
        public Dictionary<string, object> ReturnedData { get; set; }

        /// <summary>
        /// Tell the calling code (pipeline) that the return data can be used to feed a new batch
        /// </summary>
        public bool CanCreateBatch { get; set; }

        /// <summary>
        /// Specify whether data should be serialized in json (or xml)
        /// </summary>
        public bool UseJsonSerialisation { get; set; }
    }
}
