﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.ExternalServices.Model
{
    public class ExternalServiceCallRequest
    {
        /// <summary>
        /// Part 1 of the Unique KEY to identify the External Service Record. The ID of the RiskClaim we are processing
        /// </summary>
        public int RiskClaim_Id { get; set; }

        /// <summary>
        /// Part 2 of the Unique KEY to identify the External Service Record. The ID os the service in the RiskExternalService table
        /// </summary>
        public int RiskExternalService_Id { get; set; }

        /// <summary>
        /// Part 3 of the Unique KEY to identify the External Service Record. This is typically the ID of a LINK specific to the service being called.
        /// For example from Tracesmart it is the Person2Address link ID.  This isused to find and update all the data relevant to this call.
        /// </summary>
        public int Unique_Id { get; set; }

    }
}
