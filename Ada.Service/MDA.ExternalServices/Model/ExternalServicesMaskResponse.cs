﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.ExternalServices.Model
{
    public class ExternalServicesMaskResponse
    {
        /// <summary>
        /// A Mask of the services that were called/attempted. Can be zero
        /// </summary>
        public int ServicesThatCanBeCalledMask { get; set; }

        /// <summary>
        /// A simple error message that will be written to the Db record. Can be NULL
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
