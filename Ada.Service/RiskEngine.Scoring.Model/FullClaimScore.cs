﻿using RiskEngine.Scoring.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiskEngine.Model;

namespace RiskEngine.Scoring.Model
{
    /// <summary>
    /// A simple container class to hold all the data required to score a claim for the scoring engine
    /// </summary>
    public class FullClaimToScore
    {
        public IRiskEntity RiskEntityMap { get; set; }

        public int TotalScore { get; set; }
        public int TotalKeyAttractorCount { get; set; }
    }

}
