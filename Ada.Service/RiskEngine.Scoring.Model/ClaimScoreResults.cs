﻿using RiskEngine.Scoring.Entities;
using System.Runtime.Serialization;
using RiskEngine.Model;

namespace RiskEngine.Scoring.Model
{
    public enum ClaimScoreResultsStatusReturnCode
    {
        DuplicateIncident = -99,
        Error = -1,
        OK = 0,
    }
    //public interface IClaimScoreResults
    //{
    //    IRiskEntity CompleteScoreResults { get; set; }

    //    int TotalScore { get; set; }

    //    int TotalKeyAttractorCount { get; set; }

    //    int StatusReturnCode { get; set; }
    //}


    [DataContract(Namespace = XmlScores.Namespace)]
    [KnownType(typeof(RiskEngine.Model.DictionaryOfRiskResults))]
    [KnownType(typeof(RiskEngine.Model.DictionaryOfStringString))]
    [KnownType(typeof(RiskEngine.Model.DictionaryOfMethodResultRecords))]
    [KnownType(typeof(RiskEngine.Model.DictionaryOfDictionaryOfRiskResults))]
    [KnownType(typeof(MotorClaimIncidentRisk))]
    [KnownType(typeof(MobileClaimIncidentRisk))]
    //[KnownType(typeof(IncidentRisk))]   Do not add this, always create a specific ClaimIncident type
    [KnownType(typeof(VehicleRisk))]
    [KnownType(typeof(OrganisationVehicleRisk))]
    [KnownType(typeof(PersonRisk))]
    [KnownType(typeof(OrganisationRisk))]
    public class ClaimScoreResults
    {
        [DataMember(Name = "Incident")]
        public IRiskEntity CompleteScoreResults { get; set; }

        [DataMember]
        public int TotalScore { get; set; }

        [DataMember(Name = "TotalKAC")]
        public int TotalKeyAttractorCount { get; set; }

        [DataMember(Name = "StatRetCode")]
        public int StatusReturnCode { get; set; }

        public ClaimScoreResults()
        {
            StatusReturnCode = (int)ClaimScoreResultsStatusReturnCode.OK;  // zero
            TotalScore = 0;
            TotalKeyAttractorCount = 0;
        }
    }

    //[DataContract(Namespace = XmlScores.Namespace)]
    //[KnownType(typeof(RiskEngine.Model.DictionaryOfRiskResults))]
    //[KnownType(typeof(RiskEngine.Model.DictionaryOfStringString))]
    //[KnownType(typeof(RiskEngine.Model.DictionaryOfMethodResultRecords))]
    //[KnownType(typeof(RiskEngine.Model.DictionaryOfDictionaryOfRiskResults))]
    //public class MotorClaimScoreResults : ClaimScoreResultsBase, IClaimScoreResults
    //{
    //    private MotorClaimIncidentRisk _riskRootEntity;

    //    [DataMember(Name = "Incident")]
    //    public override IRiskEntity CompleteScoreResults 
    //    {
    //        get { return _riskRootEntity; }
    //        set { _riskRootEntity = (MotorClaimIncidentRisk)value; } 
    //    }

    //    public MotorClaimScoreResults() : base()
    //    {
    //    }
    //}
}
