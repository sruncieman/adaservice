﻿CREATE TABLE [dbo].[RuleGroup] (
    [RuleGroupId]     INT            IDENTITY (1, 1) NOT NULL,
    [RootRuleGroupId] INT            NOT NULL,
    [RuleClientId]    INT            NOT NULL,
    [RuleGroupName]   NVARCHAR (120) NULL,
    [EntityClassType] NVARCHAR (100) NULL,
    [Status]          INT            CONSTRAINT [DF_RuleGroup_ExecuteOrder] DEFAULT ((0)) NOT NULL,
    [Version]         INT            CONSTRAINT [DF_RuleGroup_Version] DEFAULT ((0)) NOT NULL,
    [CreatedDate]     SMALLDATETIME  NOT NULL,
    [ModifiedDate]    SMALLDATETIME  NULL,
    [CreatedBy]       NVARCHAR (50)  NOT NULL,
    [ModifiedBy]      NVARCHAR (50)  NULL,
    CONSTRAINT [PK_RuleGroup] PRIMARY KEY CLUSTERED ([RuleGroupId] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [FK_RuleGroup_RuleClient] FOREIGN KEY ([RuleClientId]) REFERENCES [dbo].[RuleClient] ([RuleClientId])
);


GO
ALTER TABLE [dbo].[RuleGroup] NOCHECK CONSTRAINT [FK_RuleGroup_RuleClient];



