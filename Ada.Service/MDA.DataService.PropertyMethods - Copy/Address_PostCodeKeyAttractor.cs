﻿using System.Linq;
using System.Text;


namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public string Address_PostCodeKeyAttractor(int addressId)
        {
            var keyStrings = from x in _db.Address2Address
                             where x.Address1_Id == addressId
                             && x.RecordStatus == (byte)ADARecordStatus.Current
                             select new
                             {
                                 KeyString = x.Address.KeyAttractor,
                                 PostCode = x.Address.PostCode
                             };

            StringBuilder s = new StringBuilder();

            string comma = "";

            foreach (var k in keyStrings)
            {
                s.Append(string.Format("{0} [{1}] {2}", comma, k.PostCode, k.KeyString));
                comma = ", ";
            }

            return s.ToString();
        }
 
    }
}
