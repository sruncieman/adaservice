﻿using System.Linq;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {

        /// <summary>
        /// Does this person have any connection to ANY email address with a key attractor
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public bool Person_IsAnyEmailKeyAttractor(int personId)
        {
            return (from p2e in _db.Person2Email.AsNoTracking()
                    where p2e.Person_Id == personId
                    && p2e.RecordStatus == (byte)ADARecordStatus.Current
                    && p2e.Email.KeyAttractor != null && p2e.Email.KeyAttractor.Length > 0
                    select p2e).Count() > 0;

        }

	}
}
