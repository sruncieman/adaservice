﻿using System.Linq;
using System.Text;
using MDA.DAL;
using LinqKit;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {

        /// <summary>
        /// Build a CSV string of all key attractors on telephone types provided for all telephones connected to this person
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId"></param>
        /// <param name="telephoneType"></param>
        /// <returns></returns>
        public string Person_AllTelephoneKeyAttractor(int personId, string telephoneType, string cacheKey, MessageCache messageCache, bool _trace)
        {
            var phones = (from p2t in _db.Person2Telephone.AsNoTracking()
                          where p2t.Person_Id == personId
                          && p2t.RecordStatus == (byte)ADARecordStatus.Current
                          && p2t.Telephone.KeyAttractor != null
                          && p2t.Telephone.KeyAttractor.Length > 0
                          select p2t);

            string[] typesSource = telephoneType.Split('|');

            var predicate = PredicateBuilder.False<Person2Telephone>();
            bool predUsed = false;

            foreach (string s in typesSource)
            {
                if (string.Compare(s, "Unknown", true) == 0)
                {
                    predicate = predicate.Or(p => p.Telephone.TelephoneType_Id == (int)MDA.Common.Enum.TelephoneType.Unknown);
                    predUsed = true;
                }
                else if (string.Compare(s, "Landline", true) == 0)
                {
                    predicate = predicate.Or(p => p.Telephone.TelephoneType_Id == (int)MDA.Common.Enum.TelephoneType.Landline);
                    predUsed = true;
                }
                else if (string.Compare(s, "Mobile", true) == 0)
                {
                    predicate = predicate.Or(p => p.Telephone.TelephoneType_Id == (int)MDA.Common.Enum.TelephoneType.Mobile);
                    predUsed = true;
                }
                else if (string.Compare(s, "FAX", true) == 0)
                {
                    predicate = predicate.Or(p => p.Telephone.TelephoneType_Id == (int)MDA.Common.Enum.TelephoneType.FAX);
                    predUsed = true;
                }
            }

            if (predUsed)
                phones = phones.AsExpandable().Where(predicate);

            StringBuilder sb = new StringBuilder();

            string comma = "";

            foreach (var k in phones)
            {
                sb.Append(string.Format("{0} [{1}] {2}", comma, k.Telephone.TelephoneNumber, k.Telephone.KeyAttractor));
                comma = ", ";
            }

            return sb.ToString();
        }

	}
}
