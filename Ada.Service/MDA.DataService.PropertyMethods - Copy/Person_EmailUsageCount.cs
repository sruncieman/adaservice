﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        /// <summary>
        /// return a count of the number of people associated with this email address who are not this person (or any of their aliases)
        /// </summary>
        /// <param name="db"></param>
        /// <param name="matchType">take into account CONFIRMED or UNCONFIRMED aliases for this person ID</param>
        /// <param name="personId"></param>
        /// <param name="riskClaimId"></param>
        /// <returns></returns>
        public List<Person_EmailUsageCount_Result> Person_EmailUsageCount(EntityAliases personAliases, int personId, int riskClaimId, bool _trace)
        {
            // Get all this persons email accounts specified on this claim
            var emailIds = (from p2e in _db.Person2Email.AsNoTracking()
                            where p2e.Person_Id == personId
                              && p2e.RiskClaim_Id == riskClaimId
                              && p2e.LinkConfidence == (int)LinkConfidence.Confirmed
                              && p2e.ADARecordStatus == (byte)ADARecordStatus.Current
                            select p2e.Email.Id).ToList();

            if (emailIds.Count > 0)
            {
                // Get all this persons aliases as ID's
                List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

                // Get all the people who are NOT aliases who use any of these emails
                var personIds = (from p2e in _db.Person2Email.AsNoTracking()
                                 where emailIds.Contains(p2e.Email_Id)
                                    && !peopleId.Contains(p2e.Person_Id)
                                    && (p2e.RiskClaim_Id != riskClaimId || p2e.RiskClaim_Id == null)
                                    && p2e.LinkConfidence == (int)LinkConfidence.Confirmed
                                    && p2e.ADARecordStatus == (byte)ADARecordStatus.Current
                                 select p2e.Person_Id).ToList();

                if (personIds.Count() > 0)
                {
                    string personIdsAsCommaList = string.Join(",", personIds);

                    int linkStrength = 0;
                    if (personAliases.IncludeTentative) linkStrength = (int)LinkConfidence.Tentative;
                    if (personAliases.IncludeUnconfirmed) linkStrength = (int)LinkConfidence.Unconfirmed;
                    if (personAliases.IncludeConfirmed) linkStrength = (int)LinkConfidence.Confirmed;

                    var uniqueIds = _db.uspUniquePerson(personIdsAsCommaList, ",", linkStrength).ToList();

                    // We have a list of people and email ID's.
                    // get a join of the people and emails and join in the incident on the same riskClaim as the email was used
                    var ret = (from person in _db.People.AsNoTracking()
                               join p2e in _db.Person2Email.AsNoTracking() on person.Id equals p2e.Person_Id
                               join i2p in _db.Incident2Person.AsNoTracking() on person.Id equals i2p.Person_Id
                               where uniqueIds.Contains(person.Id)
                               && emailIds.Contains(p2e.Email_Id)
                               && p2e.RiskClaim_Id == i2p.RiskClaim_Id
                               select new Person_EmailUsageCount_Result()
                               {
                                   Person_Id = person.Id,
                                   FirstName = person.FirstName,
                                   LastName = person.LastName,
                                   DateOfBirth = person.DateOfBirth,
                                   Salutation = person.Salutation.Text,
                                   IncidentDateWhereEmailUsedByThisPerson = i2p.Incident.IncidentDate,
                                   Incident2PersonFiveGrading = p2e.FiveGrading,
                                   Source = p2e.Source,
                                   IncidentType = i2p.Incident.IncidentType_Id
                               }).ToList();

                    return ret;
                }
            }

            return new List<Person_EmailUsageCount_Result>();

        }

        public int Person_EmailUsageCount(EntityAliases personAliases, int personId, string emailAddress, bool _trace)
        {
            int count = 0;


            List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

            //foreach person
            //  emailid = Get the link from that person to email that is current
            //  count += select all person2email links with emailid = emailid && link.personid not in list<peopleId>
            //
            foreach (var pId in peopleId)
            {

                int emailId = _db.Person2Email.Where(p2e => p2e.Person_Id == pId
                                                           && p2e.ADARecordStatus == (byte)ADARecordStatus.Current)
                                .Select(p2e => p2e.Email_Id).FirstOrDefault();

                count += (from p2e in _db.Person2Email
                          where p2e.Email_Id == emailId
                          && !peopleId.Contains(p2e.Person_Id)
                          select p2e).Count();

            }

            return count;
        }
	}
}
