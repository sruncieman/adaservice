﻿using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
  
        public List<Person_BankAccountAtMultipleAddressCount_Result> Person_BankAccountAtMultipleAddressCount(int personId, int riskClaimId, ListOfInts peopleAliasIds, bool _trace)
        {

            // Get all this persons bank accounts specified on this claim
            var accounts = (from p2b in _db.Person2BankAccount.AsNoTracking()
                            where p2b.Person_Id == personId
                            && p2b.RiskClaim_Id == riskClaimId
                            && p2b.LinkConfidence == (int)LinkConfidence.Confirmed
                            && p2b.ADARecordStatus == (byte)ADARecordStatus.Current
                            select p2b.BankAccount_Id).ToList();

            // Get list of incidents index person has previously been involved in
            List<int> incidents = (from i2p in _db.Incident2Person.AsNoTracking()
                                   where i2p.Person_Id == personId
                                   && i2p.RiskClaim_Id != riskClaimId
                                   && i2p.LinkConfidence == (int)LinkConfidence.Confirmed
                                   && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                                   select i2p.Incident_Id).ToList();

            ListOfInts otherPeopleLinkedToIncidentAndTheirAliasIds = new ListOfInts();

            if (incidents.Count() > 0)
            {

                // Get list of other people attached to incidents 
                List<int> otherPeopleLinkedToIncidents = (from i2p in _db.Incident2Person.AsNoTracking()
                                                          where incidents.Contains(i2p.Incident_Id)
                                                          && i2p.Person_Id != personId
                                                          && i2p.RiskClaim_Id != riskClaimId
                                                          && i2p.LinkConfidence == (int)LinkConfidence.Confirmed
                                                          && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                                                          select i2p.Person_Id).ToList();


                // Get the confirmed + unconfirmed aliases of otherPeopleLinkedToIncidents
                List<int> otherPeopleLinkedToIncidentsAliases = new List<int>();
                foreach (var op in otherPeopleLinkedToIncidents)
                {
                    otherPeopleLinkedToIncidentsAliases.AddRange(_dataServices.FindAllPersonAliasesFromDb(op, true, false, false, false));
                    otherPeopleLinkedToIncidentsAliases.AddRange(_dataServices.FindAllPersonAliasesFromDb(op, false, true, false, false));
                }

                // Build list of people and their aliases
                otherPeopleLinkedToIncidentAndTheirAliasIds.AddRange(otherPeopleLinkedToIncidents);
                otherPeopleLinkedToIncidentAndTheirAliasIds.AddRange(otherPeopleLinkedToIncidentsAliases);

            }

            // Get id of everyone that is linked to account that is NOT an alias of this person or on any claim involing index person
            List<int> otherPeopleLinkedToBankAccount = (from p2b in _db.Person2BankAccount.AsNoTracking()
                                                        where accounts.Contains(p2b.BankAccount_Id)
                                                             && !peopleAliasIds.Contains(p2b.Person_Id)
                                                             && !otherPeopleLinkedToIncidentAndTheirAliasIds.Contains(p2b.Person_Id)
                                                             && p2b.RiskClaim_Id != riskClaimId
                                                             && p2b.LinkConfidence == (int)LinkConfidence.Confirmed
                                                             && p2b.ADARecordStatus == (byte)ADARecordStatus.Current
                                                        select p2b.Person_Id).ToList();

            if (otherPeopleLinkedToBankAccount.Count() > 0)
            {

                List<int> otherPeopleLinkedToBankAccountAtSameAddress = new List<int>();
                List<int> otherPeopleLinkedToBankAccountAtDifferentAddress = new List<int>();

                string formattedAddress = "";

                foreach (var p in otherPeopleLinkedToBankAccount)
                {

                    // Get the confirmed aliases of all people
                    List<int> confirmedAliases = (_dataServices.FindAllPersonAliasesFromDb(p, true, false, false, false));
                    List<int> unconfirmedAliases = (_dataServices.FindAllPersonAliasesFromDb(p, false, true, false, false));


                    // Build list of people and their aliases
                    ListOfInts otherPeopleLinkedToBankAccountAndTheirAliasIds = new ListOfInts();
                    otherPeopleLinkedToBankAccountAndTheirAliasIds.Add(p);
                    otherPeopleLinkedToBankAccountAndTheirAliasIds.AddRange(confirmedAliases);
                    otherPeopleLinkedToBankAccountAndTheirAliasIds.AddRange(unconfirmedAliases);

                    // Get list of otherPeopleLinkedToTelephoneAndTheirAliasIds addresses
                    var otherPeopleLinkedToBankAccountAndTheirAliasIdsAddresses = (from p2a in _db.Person2Address.AsNoTracking()
                                                                                   where otherPeopleLinkedToBankAccountAndTheirAliasIds.Contains(p2a.Person_Id)
                                                                                   && p2a.RiskClaim_Id != riskClaimId
                                                                                   select p2a.Address_Id).Distinct().ToList();

                    // Get the address provided on the claim
                    var personAddress = (from p2a in _db.Person2Address.AsNoTracking()
                                         where p2a.Person_Id == personId
                                         && p2a.RiskClaim_Id == riskClaimId
                                         select p2a.Address_Id).ToList();

                    // Get list of common addresses
                    List<int> commonAddresses = otherPeopleLinkedToBankAccountAndTheirAliasIdsAddresses.Intersect(personAddress).ToList();

                    Address address = new Address();

                    if (commonAddresses.Count() > 0)
                    {
                        foreach (var add in commonAddresses)
                        {
                            address = (from a in _db.Addresses
                                       where a.Id == add
                                       select a).FirstOrDefault();
                        }

                        otherPeopleLinkedToBankAccountAtSameAddress.Add(p);
                    }
                    else
                    {
                        otherPeopleLinkedToBankAccountAtDifferentAddress.Add(p);
                    }

                    formattedAddress = MDA.Common.Helpers.AddressHelper.FormatAddress(address);

                }

                if (otherPeopleLinkedToBankAccountAtDifferentAddress.Count() > 0)
                {
                    var ret = (from p2b in _db.Person2BankAccount.AsNoTracking()
                               join p2a in _db.Person2Address.AsNoTracking() on p2b.Person_Id equals p2a.Person_Id into add
                               from p2a in add.DefaultIfEmpty()
                               join i2p in _db.Incident2Person.AsNoTracking() on p2b.Person_Id equals i2p.Person_Id into per
                               from i2p in per.DefaultIfEmpty()
                               join i in _db.Incidents.AsNoTracking() on i2p.Incident_Id equals i.Id into inc
                               from i in inc.DefaultIfEmpty()
                               where accounts.Contains(p2b.BankAccount_Id)
                               && i2p.RiskClaim_Id == p2b.RiskClaim_Id
                               && !peopleAliasIds.Contains(p2b.Person_Id)
                               && otherPeopleLinkedToBankAccountAtDifferentAddress.Contains(p2b.Person_Id)
                               select new Person_BankAccountAtMultipleAddressCount_Result()
                               {
                                   Person_Id = p2b.Person.Id,
                                   FirstName = p2b.Person.FirstName,
                                   LastName = p2b.Person.LastName,
                                   DateOfBirth = p2b.Person.DateOfBirth,
                                   Salutation = p2b.Person.Salutation.Text,
                                   FullAddress = p2a.Address,
                                   IncidentDateWhereBankAccountUsedByThisPerson = i.IncidentDate,
                                   Incident2PersonFiveGrading = i2p.FiveGrading,
                                   Source = i2p.Source,
                                   IncidentType = i2p.ClaimType
                               }).Distinct().ToList();

                    if (ret.Count() > 0)
                    {
                        return ret;
                    }
                }



            }

            return new List<Person_BankAccountAtMultipleAddressCount_Result>();
        }


        //public List<Person_BankAccountAtMultipleAddressCount_Result> Person_BankAccountAtMultipleAddressCount(string matchType, int personId, bool _trace)
        //{
        //    //int count = 0;

        //    //For this person (ignore aliases) get their bank account record and their Address record. 
        //    //Now find all Bank2Person links for that bank that are not this person and the person does 
        //    //not live at the address.
        //    var personBankAccount = (from p2b in _db.Person2BankAccount.AsNoTracking()
        //                       where p2b.Person_Id == personId
        //                       select p2b).FirstOrDefault();

        //    var personAddress = (from p2a in _db.Person2Address.AsNoTracking()
        //                         where p2a.Person_Id == personId
        //                         select p2a).FirstOrDefault();

        //    if (personBankAccount != null && personAddress != null)
        //    {
        //        //var r = (from p2b in _db.Person2BankAccount
        //        //         join p2a in _db.Person2Address on p2b.Person_Id equals p2a.Person_Id
        //        //         join i in _db.Incidents on p2a.RiskClaim.Incident_Id equals i.Id
        //        //         join i2p in _db.Incident2Person on i.Id equals i2p.Incident_Id 
        //        //         where p2b.BankAccount_Id == personBankAccount.BankAccount_Id
        //        //         && p2b.Person_Id != personId
        //        //         && p2a.Address_Id != personAddress.Address_Id

        //        var r = (from p2b in _db.Person2BankAccount.AsNoTracking()
        //                 join p2a in _db.Person2Address.AsNoTracking() on p2b.Person_Id equals p2a.Person_Id into add
        //                 from p2a in add.DefaultIfEmpty()
        //                 join i2p in _db.Incident2Person.AsNoTracking() on p2b.Person_Id equals i2p.Person_Id into per
        //                 from i2p in per.DefaultIfEmpty()
        //                 join i in _db.Incidents.AsNoTracking() on i2p.Incident_Id equals i.Id into inc
        //                 from i in inc.DefaultIfEmpty()
        //                 where p2b.BankAccount_Id == personBankAccount.BankAccount_Id
        //                 && p2b.Person_Id != personId
        //                 && p2a.Address_Id == personAddress.Address_Id
        //                 select new Person_BankAccountAtMultipleAddressCount_Result()
        //                 {
        //                     Person_Id                                    = p2b.Person.Id,
        //                     FirstName                                    = p2b.Person.FirstName,
        //                     LastName                                     = p2b.Person.LastName,
        //                     DateOfBirth                                  = p2b.Person.DateOfBirth,
        //                     Salutation                                   = p2b.Person.Salutation.Text,
        //                     FullAddress                                  = p2a.Address,
        //                     IncidentDateWhereBankAccountUsedByThisPerson = i. IncidentDate,
        //                     Incident2PersonFiveGrading                   = i2p.FiveGrading,
        //                     Source                                       = i2p.Source,
        //                     IncidentType                                 = i2p.ClaimType
        //                 }).Distinct().ToList();

        //        return r;
        //    }

        //    return new List<Person_BankAccountAtMultipleAddressCount_Result>();
        //}

        //public List<Person_AliasInformation_Result> Person_AliasInformation(EntityAliases personAliases, int personId, bool _trace)
        //{

        //    List<int> peopleId = FindAllPersonAliases(personId, personAliases, _trace);

        //    foreach (var pId in peopleId)
        //    {

        //        return (from p in _db.People
        //                     where p.Id == pId
        //                     select new Person_AliasInformation_Result()
        //                     {
        //                         Salutation = p.Salutation.Text,
        //                         FirstName = p.FirstName,
        //                         LastName = p.LastName,
        //                         DateOfBirth = p.DateOfBirth,
        //                         Occupation = p.Occupation,
        //                     }).ToList();
        //    }

        //    return null;

        //}

        //public List<Person_LinkedAddresses> Person_LinkedAddresses(EntityAliases personAliases, int personId, int riskClaimId ,bool _trace)
        //{

        //    List<int> peopleId = FindAllPersonAliases(personId, personAliases, _trace);

        //    foreach (var pId in peopleId)
        //    {

        //        return (from p2a in _db.Person2Address
        //                join p in _db.People on p2a.Person_Id equals p.Id
        //                where p2a.Person_Id == pId
        //                && (p2a.RiskClaim_Id != riskClaimId || p2a.RiskClaim_Id == null)
        //                && p2a.LinkConfidence == (int)LinkConfidence.Confirmed
        //                && p2a.ADARecordStatus == (byte)ADARecordStatus.Current
        //                select new Person_LinkedAddresses()
        //                {
        //                    FullAddress = p2a.Address,
        //                    Salutation = p.Salutation.Text,
        //                    FirstName = p.FirstName,
        //                    LastName = p.LastName,
        //                    Person_Id = p.Id,
        //                    Occupation = p.Occupation,

        //                }).ToList();
        //    }

        //    return null;

        //}

        //public List<Person_LinkedTelephones> Person_LinkedTelephones(EntityAliases personAliases, int personId, int riskClaimId, bool _trace)
        //{

        //    List<int> peopleId = FindAllPersonAliases(personId, personAliases, _trace);

        //    foreach (var pId in peopleId)
        //    {

        //        return (from p in _db.Person2Telephone
        //                where p.Person_Id == pId
        //                && (p.RiskClaim_Id != riskClaimId || p.RiskClaim_Id == null)
        //                && p.LinkConfidence == (int)LinkConfidence.Confirmed
        //                && p.ADARecordStatus == (byte)ADARecordStatus.Current
        //                select new Person_LinkedTelephones()
        //                {
        //                   Telephone = p.Telephone.TelephoneNumber,
        //                }).ToList();
        //    }

        //    return null;

        //}
	}
}
