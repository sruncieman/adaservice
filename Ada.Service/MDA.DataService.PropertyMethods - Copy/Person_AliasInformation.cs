﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {

        public List<Person_AliasInformation_Result> Person_AliasInformation(EntityAliases personAliases, int personId, bool _trace)
        {

            List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

            foreach (var pId in peopleId)
            {

                return (from p in _db.People
                        where p.Id == pId
                        select new Person_AliasInformation_Result()
                        {
                            Person_Id = p.Id,
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            DateOfBirth = p.DateOfBirth,
                            Salutation = p.Salutation.Text,
                            Occupation = p.Occupation
                        }).ToList();
            }

            return null;

        }

	}
}
