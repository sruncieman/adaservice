﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        /// <summary>
        /// Returns true if any of the telephones provided have a key attractor for this person in THIS claim
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId">Person ID</param>
        /// <param name="riskClaimId">The riskclaim ID</param>
        /// <returns></returns>
        public List<Person_IsTelephoneKeyAttractor> Person_IsTelephoneKeyAttractor(int personId, int riskClaimId)
        {

            return (from p2t in _db.Person2Telephone.AsNoTracking()
                    where p2t.Person_Id == personId
                    && p2t.RiskClaim_Id == riskClaimId
                    && p2t.RecordStatus == (byte)ADARecordStatus.Current
                    && p2t.Telephone.KeyAttractor != null
                    && p2t.Telephone.KeyAttractor.Length > 0
                    && (p2t.Telephone.FraudRingClosed_ == null || p2t.Telephone.FraudRingClosed_ == "No")
                    select new Person_IsTelephoneKeyAttractor()
                    {
                        TelephoneType = p2t.Telephone.TelephoneType_Id,
                        Telephone = p2t.Telephone.TelephoneNumber,
                        KeyAttractor = p2t.Telephone.KeyAttractor
                    }).ToList();
        }


	}
}
