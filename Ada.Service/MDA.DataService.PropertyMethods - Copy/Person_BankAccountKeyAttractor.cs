﻿using System.Linq;
using System.Text;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        /// <summary>
        /// Build a CSV string of all Key Attractor values for all the Bank Accounts connected with this person provided with this claim
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId"></param>
        /// <param name="riskClaimId"></param>
        /// <returns></returns>
        public string Person_BankAccountKeyAttractor(int personId, int riskClaimId)
        {
            var keyStrings = from p2a in _db.Person2BankAccount.AsNoTracking()
                             where p2a.Person_Id == personId
                             && p2a.RiskClaim_Id == riskClaimId
                             && p2a.RecordStatus == (byte)ADARecordStatus.Current
                             select new
                             {
                                 KeyString = p2a.BankAccount.KeyAttractor,
                                 AccNumber = p2a.BankAccount.AccountNumber
                             };

            StringBuilder s = new StringBuilder();

            string comma = "";

            foreach (var k in keyStrings)
            {
                s.Append(string.Format("{0} [{1}] {2}", comma, k.AccNumber, k.KeyString));
                comma = ", ";
            }

            return s.ToString();
        }

	}
}
