﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public List<Person_LinkedAddresses_Result> Person_LinkedAddresses(EntityAliases personAliases, int personId, int riskClaimId, bool _trace)
        {

            List<int> addresses = (from a in _db.Person2Address
                                   where a.Person_Id == personId
                                   && a.RiskClaim_Id == riskClaimId
                                   select a.Address.Id).ToList();


            List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

            foreach (var pId in peopleId)
            {
                var addressList = (from p2a in _db.Person2Address
                                   where peopleId.Contains(p2a.Person_Id)
                                   && !addresses.Contains(p2a.Address.Id)
                                   && (p2a.RiskClaim_Id != riskClaimId || p2a.RiskClaim_Id == null)
                                   && p2a.LinkConfidence == (int)LinkConfidence.Confirmed
                                   && p2a.ADARecordStatus == (byte)ADARecordStatus.Current
                                   select p2a).ToList();

                List<Person_LinkedAddresses_Result> res = (from x in addressList
                                                           select new Person_LinkedAddresses_Result()
                                                              {
                                                                  Confirmed = x.LinkConfidence == (int)LinkConfidence.Confirmed,
                                                                  FullAddress = MDA.Common.Helpers.AddressHelper.FormatAddress(x.Address), // x.Address.BuildingNumber + " " + x.Address.SubBuilding + " " + x.Address.Building + " " + x.Address.Street + " " + x.Address.Locality + " " + x.Address.Town + " " + x.Address.County + " " + x.Address.PostCode
                                                              }).ToList();
                return res;
            }

            return null;

        }
    }
}

