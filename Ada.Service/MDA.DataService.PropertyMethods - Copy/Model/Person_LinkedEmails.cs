﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_LinkedEmails
    {
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Person_LinkedEmails Clone()
        {
            return new Person_LinkedEmails()
            {
                EmailAddress = this.EmailAddress,
                Confirmed = this.Confirmed
            };
        }
    }


}
