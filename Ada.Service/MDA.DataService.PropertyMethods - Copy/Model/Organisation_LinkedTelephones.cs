﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Organisation_LinkedTelephones
    {
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Organisation_LinkedTelephones Clone()
        {
            return new Organisation_LinkedTelephones()
            {
                Telephone = this.Telephone,
                Confirmed = this.Confirmed
            };
        }
    }
}
