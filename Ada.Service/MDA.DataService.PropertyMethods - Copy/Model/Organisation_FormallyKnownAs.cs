﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Organisation_FormallyKnownAs
    {
        [DataMember]
        public string FormerName { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Organisation_FormallyKnownAs Clone()
        {
            return new Organisation_FormallyKnownAs()
            {
                FormerName = this.FormerName,
                Confirmed = this.Confirmed
            };
        }
    }
}
