﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_LinkedPassportNumbers
    {
        [DataMember]
        public string PassportNumber { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Person_LinkedPassportNumbers Clone()
        {
            return new Person_LinkedPassportNumbers()
            {
                PassportNumber = this.PassportNumber,
                Confirmed = this.Confirmed
            };
        }
    }
}
