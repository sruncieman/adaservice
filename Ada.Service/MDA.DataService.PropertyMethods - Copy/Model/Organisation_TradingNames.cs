﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Organisation_TradingNames
    {
        [DataMember]
        public string TradingName { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Organisation_TradingNames Clone()
        {
            return new Organisation_TradingNames()
            {
                TradingName = this.TradingName,
                Confirmed = this.Confirmed
            };
        }
    }
}
