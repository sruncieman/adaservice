﻿using MDA.DAL;
using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_NumberOfIncidents_Query
    {
        [DataMember]
        public Incident Incident { get; set; }
        [DataMember]
        public Incident2Person Incident2Person { get; set; }
        [DataMember]
        public Incident2Vehicle Incident2Vehicle { get; set; }
        [DataMember]
        public RiskClaim RiskClaim { get; set; }
        [DataMember]
        public Person Person { get; set; }
        [DataMember]
        public string PartyType { get; set; }
        [DataMember]
        public string SubPartyType { get; set; }
        [DataMember]
        public string IncidentType { get; set; }
        [DataMember]
        public string Address { get; set; }
    }

    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_NumberOfIncidents_Result
    {
        [DataMember]
        public string IncidentType { get; set; }
        [DataMember]
        public string IncidentDate { get; set; }
        [DataMember]
        public string Insurer { get; set; }
        [DataMember]
        public string ClaimRef { get; set; }
        [DataMember]
        public string Involvement { get; set; }
        [DataMember]
        public string PersonDetails { get; set; }

        public Person_NumberOfIncidents_Result Clone()
        {
            return new Person_NumberOfIncidents_Result()
            {
                IncidentType = this.IncidentType,
                IncidentDate = this.IncidentDate,
                Insurer = this.Insurer,
                ClaimRef = this.ClaimRef,
                Involvement = this.Involvement,
                PersonDetails = this.PersonDetails
            };
        }
    }
}
