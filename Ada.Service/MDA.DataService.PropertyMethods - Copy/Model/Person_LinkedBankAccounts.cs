﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_LinkedBankAccounts
    {
        [DataMember]
        public string BankAccounts { get; set; }
        [DataMember]
        public bool Confirmed { get; set; }

        public Person_LinkedBankAccounts Clone()
        {
            return new Person_LinkedBankAccounts()
            {
                BankAccounts = this.BankAccounts,
                Confirmed = this.Confirmed
            };
        }
    }
}
