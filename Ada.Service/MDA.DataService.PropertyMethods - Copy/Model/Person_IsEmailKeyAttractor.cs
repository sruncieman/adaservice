﻿using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Person_IsEmailKeyAttractor
    {
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string KeyAttractor { get; set; }

        public Person_IsEmailKeyAttractor Clone()
        {
            return new Person_IsEmailKeyAttractor()
            {
                Email = this.Email,
                KeyAttractor = this.KeyAttractor
            };
        }
    }
}
