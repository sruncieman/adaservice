﻿using System;
using MDA.DAL;
using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Vehicle_NumberOfIncidents_Query
    {
        [DataMember]
        public Vehicle Vehicle { get; set; }
        [DataMember]
        public Incident Incident { get; set; }
        [DataMember]
        public Incident2Person Incident2Person { get; set; }
        [DataMember]
        public Incident2Vehicle Incident2Vehicle { get; set; }
        [DataMember]
        public RiskClaim RiskClaim { get; set; }
        [DataMember]
        public Person Person { get; set; }
        [DataMember]
        public string Involvement { get; set; }

    }

    [DataContract(Namespace = XmlScores.Namespace)]
    public class Vehicle_NumberOfIncidents_Result
    {
        [DataMember]
        public DateTime? IncidentDate { get; set; }
        [DataMember]
        public string Insurer { get; set; }
        [DataMember]
        public string ClaimRef { get; set; }
        [DataMember]
        public string DriverDetails { get; set; }
        [DataMember]
        public string Involvement { get; set; }
        [DataMember]
        public string VehicleDetails { get; set; }

        public Vehicle_NumberOfIncidents_Result Clone()
        {
            return new Vehicle_NumberOfIncidents_Result()
            {
                IncidentDate = this.IncidentDate,
                Insurer = this.Insurer,
                ClaimRef = this.ClaimRef,
                DriverDetails = this.DriverDetails,
                Involvement = this.Involvement,
                VehicleDetails = this.VehicleDetails
            };
        }
    }
}
