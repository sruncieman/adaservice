﻿using System;
using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Address_NumberOfCUEIncidents_Result
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public DateTime? DateOfBirth { get; set; }
        [DataMember]
        public DateTime IncidentDate { get; set; }
        [DataMember]
        public string PartyTypeText { get; set; }
        [DataMember]
        public string SubPartyText { get; set; }
        [DataMember]
        public string LinkedClaimType { get; set; }
        [DataMember]
        public string Insurer { get; set; }
        [DataMember]
        public string InsurerClaimRef { get; set; }
        [DataMember]
        public string RawAddress { get; set; }


        public Address_NumberOfCUEIncidents_Result Clone()
        {
            return new Address_NumberOfCUEIncidents_Result()
            {
                Id = this.Id,
                FirstName = this.FirstName,
                LastName = this.LastName,
                DateOfBirth = this.DateOfBirth,
                IncidentDate = this.IncidentDate,
                PartyTypeText = this.PartyTypeText,
                SubPartyText = this.SubPartyText,
                LinkedClaimType = this.LinkedClaimType,
                Insurer = this.Insurer,
                InsurerClaimRef = this.InsurerClaimRef,
                RawAddress = this.RawAddress
            };
        }
    }
}
