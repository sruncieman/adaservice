﻿using System;
using MDA.DAL;
using System.Runtime.Serialization;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods.Model
{
    [DataContract(Namespace = XmlScores.Namespace)]
    public class Handset_NumberOfIncidents_Query
    {
        [DataMember]
        public Handset Handset { get; set; }
        [DataMember]
        public Incident Incident { get; set; }
        [DataMember]
        public Incident2Person Incident2Person { get; set; }
        [DataMember]
        public Incident2Handset Incident2Handset { get; set; }
        [DataMember]
        public RiskClaim RiskClaim { get; set; }
        [DataMember]
        public Person Person { get; set; }
        [DataMember]
        public string Involvement { get; set; }

    }

    [DataContract(Namespace = XmlScores.Namespace)]
    public class Handset_NumberOfIncidents_Result
    {
        [DataMember]
        public DateTime? IncidentDate { get; set; }
        [DataMember]
        public string Insurer { get; set; }
        [DataMember]
        public string ClaimRef { get; set; }
        [DataMember]
        public string DriverDetails { get; set; }
        [DataMember]
        public string Involvement { get; set; }
        [DataMember]
        public string HandsetDetails { get; set; }

        public Handset_NumberOfIncidents_Result Clone()
        {
            return new Handset_NumberOfIncidents_Result()
            {
                IncidentDate = this.IncidentDate,
                Insurer = this.Insurer,
                ClaimRef = this.ClaimRef,
                DriverDetails = this.DriverDetails,
                Involvement = this.Involvement,
                HandsetDetails = this.HandsetDetails
            };
        }
    }
}
