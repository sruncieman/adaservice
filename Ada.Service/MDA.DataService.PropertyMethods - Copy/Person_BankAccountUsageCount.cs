﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        public int Person_BankAccountUsageCount(EntityAliases personAliases, int personId, int riskClaimId, string cacheKey, MessageCache messageCache, bool _trace)
        {
            //string people = null;

            // Get all this persons bank accounts specified on this claim
            var accounts = (from p2b in _db.Person2BankAccount.AsNoTracking()
                            where p2b.Person_Id == personId
                            && p2b.RiskClaim_Id == riskClaimId
                            && p2b.LinkConfidence == (int)LinkConfidence.Confirmed
                            && p2b.ADARecordStatus == (byte)ADARecordStatus.Current
                            select p2b.BankAccount).ToList();

            List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

            int count = 0;
            StringBuilder PeopleIds = new StringBuilder();
            string comma = "";

            // for each account count the number of other people associated with it
            foreach (var account in accounts)
            {
                var ids = string.Join(",", (from p2b in _db.Person2BankAccount.AsNoTracking()
                                            where p2b.BankAccount_Id == account.Id
                                            && !peopleId.Contains(p2b.Person_Id)
                                            && p2b.LinkConfidence == (int)LinkConfidence.Confirmed
                                            && p2b.ADARecordStatus == (byte)ADARecordStatus.Current
                                            select p2b.Person_Id).ToArray());

                if (ids.Length > 0)
                {
                    PeopleIds.Append(comma + ids);

                    comma = ",";
                }
            }

            if (PeopleIds.Length > 0)
            {
                int linkStrength = 0;
                if (personAliases.IncludeTentative) linkStrength = (int)LinkConfidence.Tentative;
                if (personAliases.IncludeUnconfirmed) linkStrength = (int)LinkConfidence.Unconfirmed;
                if (personAliases.IncludeConfirmed) linkStrength = (int)LinkConfidence.Confirmed;

                var x = _db.uspUniquePerson(PeopleIds.ToString(), ",", linkStrength).FirstOrDefault();

                if (x != null)
                    count += (int)x;
            }

            //var count = db.uspUniquePerson(people, ",").FirstOrDefault();

            //return (count == null) ? 0 : (int)count;

            return count;
        }

	}
}
