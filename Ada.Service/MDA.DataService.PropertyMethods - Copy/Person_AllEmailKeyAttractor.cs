﻿using System.Linq;
using System.Text;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        /// <summary>
        /// Build a CSV string of all key attractors on ANY email address connected to this person
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public string Person_AllEmailKeyAttractor(int personId)
        {
            var keyStrings = from p2e in _db.Person2Email.AsNoTracking()
                             where p2e.Person_Id == personId
                             && p2e.RecordStatus == (byte)ADARecordStatus.Current
                             && p2e.Email.KeyAttractor != null && p2e.Email.KeyAttractor.Length > 0
                             && (p2e.Email.FraudRingClosed_ == null || p2e.Email.FraudRingClosed_ == "No")
                             select new
                             {
                                 KeyString = p2e.Email.KeyAttractor,
                                 EmAddress = p2e.Email.EmailAddress
                             };

            StringBuilder s = new StringBuilder();

            string comma = "";

            foreach (var k in keyStrings)
            {
                s.Append(string.Format("{0} [{1}] {2}", comma, k.EmAddress, k.KeyString));
                comma = ", ";
            }

            return s.ToString();
        }

	}
}
