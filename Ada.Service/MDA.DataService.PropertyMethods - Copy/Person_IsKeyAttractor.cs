﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        public List<Person_IsKeyAttractor_Result> Person_IsKeyAttractor(EntityAliases personAliases, int personId, string cacheKey, MessageCache messageCache, bool _trace)
        {
            List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);

            return (from p in _db.People.AsNoTracking()
                    where peopleId.Contains(p.Id)
                      && p.KeyAttractor != null
                      && p.KeyAttractor.Length > 0
                      && (p.FraudRingClosed_ == null || p.FraudRingClosed_ == "No")
                    select new Person_IsKeyAttractor_Result()
                    {
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        DateOfBirth = p.DateOfBirth,
                        KeyAttractor = p.KeyAttractor
                    }).ToList();
        }

	}
}
