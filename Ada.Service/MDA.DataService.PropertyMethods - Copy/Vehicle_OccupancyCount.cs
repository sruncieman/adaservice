﻿using System.Collections.Generic;
using System.Linq;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public int Vehicle_OccupancyCount(int riskClaimId, int vehicleId)
        {
            List<int> claimantTypes = MDA.Common.Helpers.LinqHelper.GetVehicleOccupancySubPartyTypes();

            var a = (from x in _db.Vehicle2Person
                     join y in _db.Incident2Person on x.Person_Id equals y.Person_Id
                     join z in _db.Incident2Vehicle on x.Vehicle_Id equals z.Vehicle_Id
                     where x.RiskClaim_Id == riskClaimId
                     && x.Vehicle_Id == vehicleId
                     && z.Incident2VehicleLinkType_Id != (int)MDA.Common.Enum.Incident2VehicleLinkType.NoVehicleInvolved // Added to fix Bug 8701:FP 157: Vehicle occupancy rules score when no vehicle is involved
                     && claimantTypes.Contains(y.SubPartyType_Id)
                     && x.ADARecordStatus == (byte)ADARecordStatus.Current
                     select x.Id).Distinct();

            return a.Count();

        }

    }
}
