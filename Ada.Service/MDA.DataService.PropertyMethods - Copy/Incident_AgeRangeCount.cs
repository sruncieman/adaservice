﻿using System.Linq;
using MDA.Common.Helpers;
using RiskEngine.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        public int Incident_AgeRangeCount(int incidentId, int riskClaimId ,int ageFrom, int ageTo, string cacheKey, MessageCache messageCache, bool _trace)
        {
            var people = (from i2p in _db.Incident2Person
                          where i2p.Incident_Id == incidentId
                          && i2p.RiskClaim_Id == riskClaimId
                          && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                          select new 
                          {
                              i2p.Person.DateOfBirth,
                              i2p.Incident.IncidentDate
                          });

            if (people == null) return 0;

            int count = 0;
            foreach (var x in people)
            {
                if (x.DateOfBirth.HasValue && x.IncidentDate != null)
                {
                    if (LinkHelper.CalculateAge(x.DateOfBirth.Value.Date, x.IncidentDate.Date) >= ageFrom
                         && LinkHelper.CalculateAge(x.DateOfBirth.Value.Date, x.IncidentDate.Date) <= ageTo)
                    {
                        count++;
                    }
                }
            }

            messageCache.AddLowOutputMessage(cacheKey, null, null);
            messageCache.AddMediumOutputMessage(cacheKey, null, null);
            messageCache.AddHighOutputMessage(cacheKey, null, null);

            return count;
        }

    }

}
