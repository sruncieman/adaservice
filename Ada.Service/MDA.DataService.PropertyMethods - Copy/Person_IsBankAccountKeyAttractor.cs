﻿using System.Collections.Generic;
using System.Linq;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        /// <summary>
        /// Has the Bank Account provided by this person for this claim got a key attractor value
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId"></param>
        /// <param name="riskClaimId"></param>
        /// <returns></returns>
        public List<Person_IsBankAccountKeyAttractor> Person_IsBankAccountKeyAttractor(int personId, int riskClaimId)
        {
            return (from p2a in _db.Person2BankAccount.AsNoTracking()
                    where p2a.Person_Id == personId
                    && p2a.RiskClaim_Id == riskClaimId
                    && p2a.RecordStatus == (byte)ADARecordStatus.Current
                    && p2a.BankAccount.KeyAttractor != null && p2a.BankAccount.KeyAttractor.Length > 0
                    select new Person_IsBankAccountKeyAttractor()
                    {
                        AccountNumber = p2a.BankAccount.AccountNumber,
                        SortCode = p2a.BankAccount.SortCode,
                        KeyAttractor = p2a.BankAccount.KeyAttractor
                    }).ToList();

        }

	}
}
