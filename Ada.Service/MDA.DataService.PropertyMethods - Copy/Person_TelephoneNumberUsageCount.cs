﻿using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
	public partial class PropertyDataServices
	{
 
		public List<Person_TelephoneNumberUsageCount_Result> Person_TelephoneNumberUsageCount(bool sameAddress, int telephoneId, int personId, int riskClaimId, ListOfInts peopleAliasIds, bool _trace)
		{

			// Get list of incidents index person has previously been involved in
			List<int> incidents = (from i2p in _db.Incident2Person.AsNoTracking()
								   where i2p.Person_Id == personId
								   && i2p.RiskClaim_Id != riskClaimId
								   && i2p.LinkConfidence == (int)LinkConfidence.Confirmed
								   && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
								   select i2p.Incident_Id).ToList();

			ListOfInts otherPeopleLinkedToIncidentAndTheirAliasIds = new ListOfInts();

			if (incidents.Count() > 0)
			{

				// Get list of other people attached to incidents 
				List<int> otherPeopleLinkedToIncidents = (from i2p in _db.Incident2Person.AsNoTracking()
														  where incidents.Contains(i2p.Incident_Id)
														  && i2p.Person_Id != personId
														  && i2p.RiskClaim_Id != riskClaimId
														  && i2p.LinkConfidence == (int)LinkConfidence.Confirmed
														  && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
														  select i2p.Person_Id).ToList();


				// Get the confirmed + unconfirmed aliases of otherPeopleLinkedToIncidents
				List<int> otherPeopleLinkedToIncidentsAliases = new List<int>();
				foreach (var op in otherPeopleLinkedToIncidents)
				{
					otherPeopleLinkedToIncidentsAliases.AddRange(_dataServices.FindAllPersonAliasesFromDb(op, true, false, false, false));
					otherPeopleLinkedToIncidentsAliases.AddRange(_dataServices.FindAllPersonAliasesFromDb(op, false, true, false, false));
				}

				// Build list of people and their aliases
				otherPeopleLinkedToIncidentAndTheirAliasIds.AddRange(otherPeopleLinkedToIncidents);
				otherPeopleLinkedToIncidentAndTheirAliasIds.AddRange(otherPeopleLinkedToIncidentsAliases);

			}

			// Get id of everyone that is linked to telephone that is NOT an alias of this person or on the any claim involing index person
			List<int> otherPeopleLinkedToTelephone = (from p2t in _db.Person2Telephone.AsNoTracking()
													  where p2t.Telephone_Id == telephoneId
														   && !peopleAliasIds.Contains(p2t.Person_Id)
														   && !otherPeopleLinkedToIncidentAndTheirAliasIds.Contains(p2t.Person_Id)
														   && p2t.RiskClaim_Id != riskClaimId
														   && p2t.LinkConfidence == (int)LinkConfidence.Confirmed
														   && p2t.ADARecordStatus == (byte)ADARecordStatus.Current
													  select p2t.Person_Id).ToList();

			if (otherPeopleLinkedToTelephone.Count() > 0)
			{
				string personIdsAsCommaList = string.Join(",", otherPeopleLinkedToTelephone);

				List<int?> uniqueIds = _db.uspUniquePerson(personIdsAsCommaList, ",", 0).ToList();

				var newList = uniqueIds.Select(i => (int)i).ToList();

				if (newList.Count() > 0)
				{

					List<int> otherPeopleLinkedToTelephoneAtSameAddress = new List<int>();
					List<int> otherPeopleLinkedToTelephoneAtDifferentAddress = new List<int>();

					string formattedAddress = "";

					foreach (var p in newList)
					{

						// Get the confirmed aliases of all people
						List<int> confirmedAliases = (_dataServices.FindAllPersonAliasesFromDb(p, true, false, false, false));
						List<int> unconfirmedAliases = (_dataServices.FindAllPersonAliasesFromDb(p, false, true, false, false));


						// Build list of people and their aliases
						ListOfInts otherPeopleLinkedToTelephoneAndTheirAliasIds = new ListOfInts();
						otherPeopleLinkedToTelephoneAndTheirAliasIds.Add(p);
						otherPeopleLinkedToTelephoneAndTheirAliasIds.AddRange(confirmedAliases);
						otherPeopleLinkedToTelephoneAndTheirAliasIds.AddRange(unconfirmedAliases);

						// Get list of otherPeopleLinkedToTelephoneAndTheirAliasIds addresses
						var otherPeopleLinkedToTelephoneAndTheirAliasIdsAddresses = (from p2a in _db.Person2Address.AsNoTracking()
																					 where otherPeopleLinkedToTelephoneAndTheirAliasIds.Contains(p2a.Person_Id)
																					 && p2a.RiskClaim_Id != riskClaimId
																					 select p2a.Address_Id).Distinct().ToList();

						// Get the address provided on the claim
						var personAddress = (from p2a in _db.Person2Address.AsNoTracking()
											 where p2a.Person_Id == personId
											 && p2a.RiskClaim_Id == riskClaimId
											 select p2a.Address_Id).ToList();

						// Get list of common addresses
						List<int> commonAddresses = otherPeopleLinkedToTelephoneAndTheirAliasIdsAddresses.Intersect(personAddress).ToList();

						Address address = new Address();

						if (commonAddresses.Count() > 0)
						{
							foreach (var add in commonAddresses)
							{
								address = (from a in _db.Addresses
										   where a.Id == add
										   select a).FirstOrDefault();
							}

							otherPeopleLinkedToTelephoneAtSameAddress.Add(p);
						}
						else
						{
							otherPeopleLinkedToTelephoneAtDifferentAddress.Add(p);
						}

						formattedAddress = MDA.Common.Helpers.AddressHelper.FormatAddress(address);

					}

					if (sameAddress == true && otherPeopleLinkedToTelephoneAtSameAddress.Count() > 0)
					{

						var ret = (from p2t in _db.Person2Telephone.AsNoTracking()
								   join i2p in _db.Incident2Person.AsNoTracking() on p2t.Person_Id equals i2p.Person_Id into per
								   from i2p in per.DefaultIfEmpty()
								   join i in _db.Incidents.AsNoTracking() on i2p.Incident_Id equals i.Id into inc
								   from i in inc.DefaultIfEmpty()
								   where p2t.Telephone_Id == telephoneId
								   && i2p.RiskClaim_Id == p2t.RiskClaim_Id
								   && !peopleAliasIds.Contains(p2t.Person_Id)
								   && otherPeopleLinkedToTelephoneAtSameAddress.Contains(p2t.Person_Id)
								   select new Person_TelephoneNumberUsageCount_Result()
								   {
									   Person_Id = p2t.Person.Id,
									   FirstName = p2t.Person.FirstName,
									   LastName = p2t.Person.LastName,
									   DateOfBirth = p2t.Person.DateOfBirth,
									   Salutation = p2t.Person.Salutation.Text,
									   IncidentDateWhereTeleNumberUsedByThisPerson = i2p.Incident.IncidentDate,
									   Incident2PersonFiveGrading = i2p.FiveGrading,
									   Source = i2p.Source,
									   IncidentType = i2p.ClaimType,
									   TelephoneNumber = p2t.Telephone.TelephoneNumber,
									   TelephoneTypeText = p2t.Telephone.TelephoneType.PhoneText,
									   FormattedAddress = formattedAddress
								   }).Distinct().ToList();

						if (ret.Count() > 0)
						{
							return ret;
						}
					}
					else if (sameAddress == false && otherPeopleLinkedToTelephoneAtDifferentAddress.Count() > 0)
					{
						var ret = (from p2t in _db.Person2Telephone.AsNoTracking()
								   join p2a in _db.Person2Address.AsNoTracking() on p2t.Person_Id equals p2a.Person_Id into add
								   from p2a in add.DefaultIfEmpty()
								   join i2p in _db.Incident2Person.AsNoTracking() on p2t.Person_Id equals i2p.Person_Id into per
								   from i2p in per.DefaultIfEmpty()
								   join i in _db.Incidents.AsNoTracking() on i2p.Incident_Id equals i.Id into inc
								   from i in inc.DefaultIfEmpty()
								   where p2t.Telephone_Id == telephoneId
								   && i2p.RiskClaim_Id == p2t.RiskClaim_Id
								   && !peopleAliasIds.Contains(p2t.Person_Id)
								   && otherPeopleLinkedToTelephoneAtDifferentAddress.Contains(p2t.Person_Id)
								   select new Person_TelephoneNumberUsageCount_Result()
								   {
									   Person_Id = p2t.Person.Id,
									   FirstName = p2t.Person.FirstName,
									   LastName = p2t.Person.LastName,
									   DateOfBirth = p2t.Person.DateOfBirth,
									   Salutation = p2t.Person.Salutation.Text,
									   IncidentDateWhereTeleNumberUsedByThisPerson = i2p.Incident.IncidentDate,
									   Incident2PersonFiveGrading = i2p.FiveGrading,
									   Source = i2p.Source,
									   IncidentType = i2p.ClaimType,
									   TelephoneNumber = p2t.Telephone.TelephoneNumber,
									   TelephoneTypeText = p2t.Telephone.TelephoneType.PhoneText,
									   Address = p2a.Address
								   }).Distinct().ToList();

						if (ret.Count() > 0)
						{
							return ret;
						}
					}



				}

			}

			return new List<Person_TelephoneNumberUsageCount_Result>();
		}

	}
}
