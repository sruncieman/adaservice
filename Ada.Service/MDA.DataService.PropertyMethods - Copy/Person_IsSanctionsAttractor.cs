﻿using System.Collections.Generic;
using System.Linq;
using RiskEngine.Model;
using MDA.DataService.PropertyMethods.Model;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {

        public List<Person_IsSanctionsAttractor_Result> Person_IsSanctionsAttractor(EntityAliases personAliases, int personId, string cacheKey, MessageCache messageCache, bool _trace)
        {

            List<int> peopleId = _dataServices.FindAllPersonAliases(personId, personAliases, _trace);


            return (from p in _db.People.AsNoTracking()
                    where peopleId.Contains(p.Id)
                      && p.SanctionList.Length > 0 
                      && p.RecordStatus == (byte)ADARecordStatus.Current
                    select new Person_IsSanctionsAttractor_Result()
                    {
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        DateOfBirth = p.DateOfBirth,
                        KeyAttractor = p.SanctionList
                    }).ToList();



        }

    }
}
