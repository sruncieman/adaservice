﻿using System.Linq;
using System.Text;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
 
        /// <summary>
        /// Build a CSV string of all Key Attractor values for all the Bank Accounts connected with this person across all their claims claims
        /// </summary>
        /// <param name="db"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public string Person_AllBankAccountKeyAttractor(int personId)
        {
            var keyStrings = from p2a in _db.Person2BankAccount.AsNoTracking()
                             where p2a.Person_Id == personId
                             && p2a.RecordStatus == (byte)ADARecordStatus.Current
                             select new
                             {
                                 KeyString = p2a.BankAccount.KeyAttractor,
                                 AccNumber = p2a.BankAccount.AccountNumber
                             };

            StringBuilder s = new StringBuilder();

            string comma = "";

            foreach (var k in keyStrings)
            {
                s.Append(string.Format("{0} [{1}] {2}", comma, k.AccNumber, k.KeyString));
                comma = ", ";
            }

            return s.ToString();
        }

	}
}
