﻿using System.Linq;

namespace MDA.DataService.PropertyMethods
{
    public partial class PropertyDataServices
    {
        public int Incident_NumberOfPolicies(int incidentId)
        {

            return (from i2p in _db.Incident2Policy
                    join i in _db.Incidents on i2p.Incident_Id equals i.Id
                    where i2p.Incident_Id == incidentId
                    && i.IncidentType_Id == 8
                    && i2p.ADARecordStatus == (byte)ADARecordStatus.Current
                    select i2p).Distinct().Count();
        }

    }

}
