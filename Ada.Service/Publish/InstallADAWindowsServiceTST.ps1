

function Get-Service(
    [string]$serviceName = $(throw "serviceName is required"), 
    [string]$targetServer = $(throw "targetServer is required"))
{
    $securePassword = ConvertTo-SecureString "Arsenal9" -AsPlainText -force
    $credential = New-Object System.Management.Automation.PsCredential("FOXTEST.PROG\martinl-admin",$securePassword)

    $Service = Get-WmiObject -Class Win32_Service -ComputerName $targetServer -Credential $Credential -Filter "Name='$serviceName'" 
    
    return $service
}

function Uninstall-Service(
    [string]$serviceName = $(throw "serviceName is required"), 
    [string]$targetServer = $(throw "targetServer is required"))
{
    $service = Get-Service $serviceName $targetServer
     
    if (!($service))
    { 
        Write-Warning "Failed to find service $serviceName on $targetServer. Nothing to uninstall."
        return
    }
     
    "Found service $serviceName on $targetServer; checking status"
             
    if ($service.Started)
    {
        "Stopping service $serviceName on $targetServer"
        #could also use Set-Service, net stop, SC, psservice, psexec etc.
        $result = $service.StopService()
        Test-ServiceResult -operation "Stop service $serviceName on $targetServer" -result $result
    }
     
    "Attempting to uninstall service $serviceName on $targetServer"
    $result = $service.Delete()
    Test-ServiceResult -operation "Delete service $serviceName on $targetServer" -result $result   
}

function Test-ServiceResult(
    [string]$operation = $(throw "operation is required"), 
    [object]$result = $(throw "result is required"), 
    [switch]$continueOnError = $false)
{
    $retVal = -1
    if ($result.GetType().Name -eq "UInt32") { $retVal = $result } else {$retVal = $result.ReturnValue}
         
    if ($retVal -eq 0) {return}
     
    $errorcode = 'Success,Not Supported,Access Denied,Dependent Services Running,Invalid Service Control'
    $errorcode += ',Service Cannot Accept Control, Service Not Active, Service Request Timeout'
    $errorcode += ',Unknown Failure, Path Not Found, Service Already Running, Service Database Locked'
    $errorcode += ',Service Dependency Deleted, Service Dependency Failure, Service Disabled'
    $errorcode += ',Service Logon Failure, Service Marked for Deletion, Service No Thread'
    $errorcode += ',Status Circular Dependency, Status Duplicate Name, Status Invalid Name'
    $errorcode += ',Status Invalid Parameter, Status Invalid Service Account, Status Service Exists'
    $errorcode += ',Service Already Paused'
    $desc = $errorcode.Split(',')[$retVal]
     
    $msg = ("{0} failed with code {1}:{2}" -f $operation, $retVal, $desc)
     
    if (!$continueOnError) { Write-Error $msg } else { Write-Warning $msg }        
}


    $targetServerIP = "192.168.181.31"
    $serviceName = "ADA Service"    

    Uninstall-Service $serviceName $targetServerIP
 
    "Pausing to avoid potential temporary access denied"
    Start-Sleep -s 5 # Yeah I know, don't beat me up over this
             

    $targetServer = "INTELTST-DB1.FOXTEST.PROG"           
    $securePassword = ConvertTo-SecureString "Arsenal9" -AsPlainText -force
    $credential = New-Object System.Management.Automation.PsCredential("FOXTEST.PROG\martinl-admin",$securePassword)
    
    $dir = "D:\Drops\MDAFozzieBearSolution"
    $latest = Get-ChildItem -Path $dir | Sort-Object LastAccessTime -Descending | Select-Object -First 1
    
    $sourcedirectory = "D:\Drops\MDAFozzieBearSolution\"+$latest.name+"\MDASolution\MDA.Pipeline.WindowsService" 
    
    $targetdirectory = "\\" + $targetServer + "\D$\temp\deploy\WinServ"

    "Remove old files from $targetdirectory"
    Invoke-Command -ComputerName $targetServer -ScriptBlock { Param($targetdirectory) Get-ChildItem -Path $targetdirectory -Recurse -exclude '*.config' | Remove-Item -force -recurse } -Credential $credential -ArgumentList $targetdirectory
    
    net use $targetdirectory "Arsenal9" /USER:"FOXTEST.PROG\martinl-admin"

    "Copy new files from $sourcedirectory to $targetdirectory"
    Get-ChildItem $sourcedirectory -Recurse -Exclude '*.config' | Copy-Item -Destination {Join-Path $targetdirectory $_.FullName.Substring($sourcedirectory.length)}
    
    $path = "D:\temp\deploy\WinServ\MDA.Pipeline.WindowsService.exe"  
    $secpasswd = ConvertTo-SecureString "Arsenal9" -AsPlainText -Force
    $mycreds = New-Object System.Management.Automation.PSCredential ("FOXTEST\martinl-admin", $secpasswd)                
 
    "Install new service"
    Invoke-Command -ComputerName $targetServer -ScriptBlock { Param($path,$mycreds)
    New-Service -name "ADA Service" -binaryPathName $path -displayName "ADA Service" -startupType Automatic -credential $mycreds    
    } -Credential $credential -ArgumentList $path,$mycreds

    $service = Get-Service $serviceName $targetServerIP 

    if (!$service.Started)
    {
        "-------------------------------------------------------------------------------------------"
        "Starting service $serviceName on $targetServer"
        $result = $service.StartService()
    }

             
                          
