﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    using System;
    using System.Collections.Generic;




    [DataContract]
    [Serializable]
    public class Person : IXmlEntity
    {
        public Person()
        {
            Addresses = new ObservableCollection<Address>();
            Organisations = new ObservableCollection<Organisation>();

            //ClaimInfo = new xClaimInfo();
            //PaymentCard = new PaymentCard();
            //BankAccount = new BankAccount();

            PartyType = PartyType.Unknown;
            SubPartyType = SubPartyType.Unknown;
            Salutation = Salutation.Unknown;
            Gender = Gender.Unknown;

            Operation = Operation.Normal;
        }

        [DataMember]
        public PartyType PartyType { get; set; }

        [DataMember]
        public SubPartyType SubPartyType { get; set; }


        [DataMember]
        public Salutation Salutation { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public DateTime? DateOfBirth { get; set; }

        [DataMember]
        public Gender Gender { get; set; }

        [DataMember]
        public string Nationality { get; set; }

        [DataMember]
        public string NINumber { get; set; }

        [DataMember]
        public string DrivingLicenseNumber { get; set; }

        [DataMember]
        public string PassportNumber { get; set; }

        [DataMember]
        public string MRZPassportNumber { get; set; }

        [DataMember]
        public string Occupation { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string LandlineTelephone { get; set; }

        [DataMember]
        public string WorkTelephone { get; set; }

        [DataMember]
        public string MobileTelephone { get; set; }

        [DataMember]
        public string OtherTelephone { get; set; }

        [DataMember]
        public bool? AttendedHospital { get; set; }

        [DataMember]
        public MojStatus MojStatus { get; set; }

        //[DataMember]
        //public xClaimInfo ClaimInfo { get; set; }

        [DataMember]
        public Operation Operation { get; set; }

        [DataMember]
        public ObservableCollection<Address> Addresses { get; set; }

        [DataMember]
        public BankAccount BankAccount { get; set; }

        [DataMember]
        public PaymentCard PaymentCard { get; set; }

        [DataMember]
        public ObservableCollection<Organisation> Organisations { get; set; }

        public string DisplayText
        {
            get { return FirstName + " " + MiddleName + " " + LastName; } // +"  [" + PersonPolicyLink.ToString() + "]"; }
        }
    }
}