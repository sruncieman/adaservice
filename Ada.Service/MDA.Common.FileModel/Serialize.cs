﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MDA.Common.FileModel
{
    class Serialize
    {
        public static T Deserialize<T>(string rawXml)
        {
            using (XmlReader reader = XmlReader.Create(new StringReader(rawXml)))
            {
                DataContractSerializer formatter = new DataContractSerializer(typeof(T));
                return (T)formatter.ReadObject(reader);
            }
        }

        public static MDA.Common.FileModel.IClaim DeserializeClaim(string rawXml, bool useJson)
        {
            if (!useJson)
            {
                return Deserialize<MDA.Common.FileModel.IClaim>(rawXml);
            }
            else
            {
                return JsonConvert.DeserializeObject<MDA.Common.FileModel.IClaim>(rawXml);
            }
        }

        //public static string SerializeClaim(MDA.Common.FileModel.Claim claimXml, bool useJson)
        //{
        //    string xml = null;
        //    string eType = null;

        //    if (!useJson)
        //    {
        //        eType = claimXml.GetType().ToString();

        //        MemoryStream ms = new MemoryStream();

        //        DataContractSerializer ser = new DataContractSerializer(claimXml.GetType());
        //        ser.WriteObject(ms, claimXml);
        //        ms.Position = 0;
        //        var sr = new StreamReader(ms);
        //        xml = sr.ReadToEnd();
        //    }
        //    else
        //    {
        //        xml = JsonConvert.SerializeObject(claimXml, Newtonsoft.Json.Formatting.None);
        //    }

        //    return xml;
        //}

        //public static string SerializeClaimBatch(MDA.Common.FileModel.ClaimBatch batch)
        //{
        //    string xml = null;
        //    string eType = null;

        //    eType = batch.GetType().ToString();

        //    MemoryStream ms = new MemoryStream();

        //    DataContractSerializer ser = new DataContractSerializer(batch.GetType());
        //    ser.WriteObject(ms, batch);
        //    ms.Position = 0;
        //    var sr = new StreamReader(ms);
        //    xml = sr.ReadToEnd();

        //    return xml;
        //}
    }
}
