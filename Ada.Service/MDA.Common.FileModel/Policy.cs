﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    using System;
    using System.Collections.Generic;


    [DataContract]
    [Serializable]
    public class Policy
    {
        public Policy()
        {
            PolicyType = PolicyType.Unknown;
            CoverType = PolicyCoverType.Unknown;
            //Operation = Operation.Normal;
        }

        [DataMember]
        public PolicyType PolicyType { get; set; }

        [DataMember]
        public string PolicyNumber { get; set; }

        [DataMember]
        public string TradingName { get; set; }

        [DataMember]
        public string Insurer { get; set; }

        [DataMember]
        public string Broker { get; set; }

        [DataMember]
        public PolicyCoverType CoverType { get; set; }

        [DataMember]
        public DateTime? PolicyStartDate { get; set; }

        [DataMember]
        public DateTime? PolicyEndDate { get; set; }

        [DataMember]
        public int? PreviousNoFaultClaimsCount { get; set; }

        [DataMember]
        public int? PreviousFaultClaimsCount { get; set; }

        [DataMember]
        public Operation Operation { get; set; }

        [DataMember]
        public decimal? Premium { get; set; }
    }
}
