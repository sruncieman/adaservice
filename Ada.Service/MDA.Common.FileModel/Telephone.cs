﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    /// <summary>
    /// Class NOT IN XML but used when cleansing and reformatting a telephone number
    /// </summary>
    public class Telephone
    {
        public string AreaCode { get; set; }
        public string InternationalCode { get; set; }
        public string FullNumber { get; set; }
        public string ClientSuppliedNumber { get; set; }
        public Common.Enum.TelephoneType TelephoneType { get; set; }

        [DataMember]
        public Operation Operation { get; set; }
    }
}
