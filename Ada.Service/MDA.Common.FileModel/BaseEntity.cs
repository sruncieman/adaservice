﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    //using System;
    //using System.Collections.Generic;
    //using System.Collections.ObjectModel;

    //[DataContract]
    //[Serializable]
    //public class BaseXmlEntity
    //{
    //    public BaseXmlEntity()
    //    {
    //        Operation = Operation.Normal;
    //    }

    //    [DataMember]
    //    public Operation Operation { get; set; }
    //}

    public interface IClaim
    {
        ClaimType ClaimType { get; set; }

        string ClaimNumber { get; set; }

        DateTime IncidentDate { get; set; }
    }

    public interface IXmlEntity
    {
        string DisplayText { get; }
    }

}
