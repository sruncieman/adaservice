﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    using System;
    using System.Collections.Generic;



    [DataContract(Name = "ClaimBatch", Namespace = "http://www.keoghs.com/ClaimBatchV1")]
    [Serializable]
    public class MotorClaimBatch
    {
        public MotorClaimBatch()
        {
            Claims = new ObservableCollection<MotorClaim>();
        }

        [DataMember(IsRequired = true)]
        public ObservableCollection<MotorClaim> Claims { get; set; }
    }



    [DataContract(Name = "PIClaimBatch", Namespace = "http://www.keoghs.com/ClaimBatchV1")]
    [Serializable]
    public class PIClaimBatch
    {
        public PIClaimBatch()
        {
            Claims = new ObservableCollection<PIClaim>();
        }

        [DataMember(IsRequired = true)]
        public ObservableCollection<PIClaim> Claims { get; set; }
    }

    [DataContract(Name = "MobileClaimBatch", Namespace = "http://www.keoghs.com/ClaimBatchV1")]
    [Serializable]
    public class MobileClaimBatch
    {
        public MobileClaimBatch()
        {
            Claims = new ObservableCollection<MobileClaim>();
        }

        [DataMember(IsRequired = true)]
        public ObservableCollection<MobileClaim> Claims { get; set; }
    }
}
