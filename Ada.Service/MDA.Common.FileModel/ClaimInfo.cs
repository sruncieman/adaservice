﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    using System;
    using System.Collections.Generic;

    [DataContract]
    [Serializable]
    public class ClaimInfo
    {
        public ClaimInfo()
        {
            ClaimStatus = ClaimStatus.Unknown;
        }

        [DataMember]
        public ClaimStatus ClaimStatus { get; set; }

        [DataMember]
        public string ClaimCode { get; set; }

        //[DataMember]
        //public TimeSpan? IncidentTime { get; set; }

        [DataMember]
        public string IncidentLocation { get; set; }

        [DataMember]
        public string IncidentCircumstances { get; set; }

        [DataMember]
        public bool? PoliceAttended { get; set; }

        [DataMember]
        public string PoliceForce { get; set; }

        [DataMember]
        public string PoliceReference { get; set; }

        [DataMember]
        public bool? AmbulanceAttended { get; set; }

        [DataMember]
        public DateTime? ClaimNotificationDate { get; set; }

        [DataMember]
        public decimal? PaymentsToDate { get; set; }

        [DataMember]
        public decimal? Reserve { get; set; }

        [DataMember]
        public string SourceClaimStatus { get; set; }

        [DataMember]
        public decimal? TotalClaimCost { get; set; }

        [DataMember]
        public decimal? TotalClaimCostLessExcess { get; set; }

        [DataMember]
        public bool? BypassFraud { get; set; }

    }

    //[DataContract]
    //[Serializable]
    //public class xClaimInfo
    //{
    //    public xClaimInfo()
    //    {
    //        ClaimStatus = ClaimStatus.Unknown;
    //    }

    //    [DataMember]
    //    public ClaimStatus ClaimStatus { get; set; }

    //    [DataMember]
    //    public string ClaimCode { get; set; }

    //    [DataMember]
    //    public DateTime? IncidentTime { get; set; }

    //    [DataMember]
    //    public string IncidentLocation { get; set; }

    //    [DataMember]
    //    public string IncidentCircumstances { get; set; }

    //    [DataMember]
    //    public bool PoliceAttended { get; set; }

    //    [DataMember]
    //    public string PoliceForce { get; set; }

    //    [DataMember]
    //    public string PoliceReference { get; set; }

    //    [DataMember]
    //    public bool AmbulanceAttended { get; set; }

    //    [DataMember]
    //    public bool AttendedHospital { get; set; }

    //    [DataMember]
    //    public MojStatus MojStatus { get; set; }

    //    [DataMember]
    //    public DateTime? ClaimNotificationDate { get; set; }

    //    [DataMember]
    //    public decimal? PaymentsToDate { get; set; }

    //    [DataMember]
    //    public decimal? Reserve { get; set; }

    //}
}
