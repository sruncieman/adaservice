﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    using System;
    using System.Collections.Generic;

    [DataContract(Name = "Claim")]
    [Serializable]
    public class MotorClaim : IXmlEntity, IClaim
    {
        public MotorClaim() : base()
        {
            Policy = new Policy();


            Operation = Operation.Normal;

            IncidentDate = DateTime.Now;

            Vehicles = new ObservableCollection<Vehicle>();
            Organisations = new ObservableCollection<Organisation>();


            MotorClaimInfo = new ClaimInfo();  //??

            ClaimType = ClaimType.Motor;
        }


        [DataMember(IsRequired = true)]
        public string ClaimNumber { get; set; }

        [DataMember(IsRequired = true)]
        public ClaimType ClaimType { get; set; }

        [DataMember(IsRequired = true)]
        public DateTime IncidentDate { get; set; }

        [DataMember]
        public string ExternalServices { get; set; }


        [DataMember]
        public Policy Policy { get; set; }

        [DataMember]
        public Operation Operation { get; set; }

        [DataMember]
        public ClaimInfo MotorClaimInfo { get; set; }

        [DataMember(IsRequired = true)]
        public ObservableCollection<Vehicle> Vehicles { get; set; }

        [DataMember]
        public ObservableCollection<Organisation> Organisations { get; set; }

        public string DisplayText
        {
            get { return ClaimNumber; }
        }
    }

    [DataContract(Name = "PIClaim")]
    [Serializable]
    public class PIClaim : IXmlEntity, IClaim
    {
        public PIClaim() : base()
        {
            Policy = new Policy();


            Operation = Operation.Normal;

            IncidentDate = DateTime.Now;

            Person = new FileModel.Person();
            Organisations = new ObservableCollection<Organisation>();


            PIClaimInfo = new ClaimInfo();  //??

            ClaimType = ClaimType.PersonalInjury;
        }


        [DataMember(IsRequired = true)]
        public string ClaimNumber { get; set; }

        [DataMember(IsRequired = true)]
        public ClaimType ClaimType { get; set; }

        [DataMember(IsRequired = true)]
        public DateTime IncidentDate { get; set; }

        [DataMember]
        public string ExternalServices { get; set; }


        [DataMember]
        public Policy Policy { get; set; }

        [DataMember]
        public Operation Operation { get; set; }

        [DataMember]
        public ClaimInfo PIClaimInfo { get; set; }

        [DataMember(IsRequired = true)]
        public Person Person { get; set; }

        [DataMember]
        public ObservableCollection<Organisation> Organisations { get; set; }

        public string DisplayText
        {
            get { return ClaimNumber; }
        }
    }


    [DataContract(Name = "MobileClaim")]
    [Serializable]
    public class MobileClaim : IXmlEntity, IClaim
    {
        public MobileClaim()
            : base()
        {
            Policy = new Policy();

            Operation = Operation.Normal;

            IncidentDate = DateTime.Now;

            Handsets = new ObservableCollection<Handset>();

            Organisations = new ObservableCollection<Organisation>();

            MobileClaimInfo = new ClaimInfo();  //??

            ClaimType = ClaimType.MobilePhone;
        }

        [DataMember(IsRequired = true)]
        public string ClaimNumber { get; set; }

        [DataMember(IsRequired = true)]
        public ClaimType ClaimType { get; set; }

        [DataMember(IsRequired = true)]
        public DateTime IncidentDate { get; set; }

        [DataMember]
        public string ExternalServices { get; set; }


        [DataMember]
        public Policy Policy { get; set; }

        [DataMember]
        public Operation Operation { get; set; }

        [DataMember]
        public ClaimInfo MobileClaimInfo { get; set; }

        [DataMember(IsRequired = true)]
        public ObservableCollection<Handset> Handsets { get; set; }

        [DataMember]
        public ObservableCollection<Organisation> Organisations { get; set; }

        public string DisplayText
        {
            get { return ClaimNumber; }
        }
    }
}
