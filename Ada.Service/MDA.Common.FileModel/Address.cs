﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    public class Address : IXmlEntity
    {
        public Address()
        {
            AddressLinkType = AddressLinkType.CurrentAddress;

            Operation = Operation.Normal;
        }

        [DataMember(Name = "AddressStatus")]
        public AddressLinkType AddressLinkType { get; set; }

        [DataMember]
        public DateTime? StartOfResidency { get; set; }
        [DataMember]
        public DateTime? EndOfResidency { get; set; }

        [DataMember]
        public string SubBuilding { get; set; }
        [DataMember]
        public string Building { get; set; }
        [DataMember]
        public string BuildingNumber { get; set; }
        [DataMember]
        public string Street { get; set; }
        [DataMember]
        public string Locality { get; set; }
        [DataMember]
        public string Town { get; set; }
        [DataMember]
        public string County { get; set; }  
        [DataMember]
        public string PostCode { get; set; }
        [DataMember]
        public string PAFUPRN { get; set; }

        [DataMember]
        public Operation Operation { get; set; }

        public string DisplayText
        {
            get { return BuildingNumber + " " + Street + " " + PostCode + "  [" + AddressLinkType.ToString() + "]"; }
        }

    }
}