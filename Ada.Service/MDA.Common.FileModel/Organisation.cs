﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    using System;
    using System.Collections.Generic;


    [DataContract]
    [Serializable]
    public class Organisation : IXmlEntity
    {
        public Organisation()
        {
            Addresses = new ObservableCollection<Address>();
            Vehicles = new ObservableCollection<OrganisationVehicle>();

            //??ClaimInfo = new ClaimInfo();
            //PaymentCard = new PaymentCard();
            //BankAccount = new BankAccount();

            OrganisationType = OrganisationType.Unknown;
            Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;

            Operation = Operation.Normal;
        }

        [DataMember(Name = "OrganisationPersonRelationship")]
        public Person2OrganisationLinkType Person2OrganisationLinkType { get; set; }

        [DataMember]
        public OrganisationType OrganisationType { get; set; }

        [DataMember]
        public string OrganisationName { get; set; }

        [DataMember]
        public string RegisteredNumber { get; set; }

        [DataMember]
        public string VatNumber { get; set; }

        [DataMember]
        public string MojCrmNumber { get; set; }

        [DataMember]
        public MojCRMStatus MojCrmStatus { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string WebSite { get; set; }

        [DataMember]
        public string Telephone1 { get; set; }

        [DataMember]
        public string Telephone2 { get; set; }

        [DataMember]
        public string Telephone3 { get; set; }

        //??[DataMember]
        //??public ClaimInfo ClaimInfo { get; set; }

        [DataMember]
        public BankAccount BankAccount { get; set; }   // Only populated when Org under Claim (not person)

        [DataMember]
        public PaymentCard PaymentCard { get; set; }   // Only populated when Org under Claim (not person)

        [DataMember]
        public Operation Operation { get; set; }

        [DataMember]
        public ObservableCollection<Address> Addresses { get; set; }

        [DataMember]
        public ObservableCollection<OrganisationVehicle> Vehicles { get; set; }

        public string DisplayText
        {
            get { return OrganisationName + "  [" + OrganisationType.ToString() + "]"; } // +OrganisationPersonLinkType.ToString() + "]"; }
        }
    }
}