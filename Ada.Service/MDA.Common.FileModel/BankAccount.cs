﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    using System;
    using System.Collections.Generic;


    public class BankAccount : IXmlEntity
    {
        public BankAccount()
        {
            PolicyPaymentType = PolicyPaymentType.Unknown;

            Operation = Operation.Normal;
        }

        [DataMember(IsRequired = true)]
        public string AccountNumber { get; set; }
        [DataMember]
        public string SortCode { get; set; }
        [DataMember]
        public string BankName { get; set; }
        [DataMember]
        public PolicyPaymentType PolicyPaymentType { get; set; }
        [DataMember]
        public DateTime? DatePaymentDetailsTaken { get; set; }

        [DataMember]
        public Operation Operation { get; set; }

        public string DisplayText
        {
            get { return BankName + " " + SortCode + " " + AccountNumber; }
        }
    }
}