﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    using System;
    using System.Collections.Generic;


    public class PaymentCard : IXmlEntity
    {
        private string _paymentCardNumber;
        private string _hashedCardNumber;

        public PaymentCard()
        {
            PaymentCardType = PaymentCardType.Unknown;

            Operation = Operation.Normal;
        }

        [DataMember(IsRequired = true)]
        public string PaymentCardNumber
        {
            get { return _paymentCardNumber; }
            set 
            {
                if ( value.Length == 16 && !value.Contains('#') && !value.Contains('-'))
                    _hashedCardNumber = HashPaymentCard(value);

                _paymentCardNumber = MaskPaymentCard(value); 
            }
        }

        [DataMember]
        public string HashedCardNumber
        {
            get { return _hashedCardNumber; }
            set { _hashedCardNumber = value; }
        }

        [DataMember]
        public string SortCode { get; set; }
        [DataMember]
        public string BankName { get; set; }
        [DataMember]
        public string ExpiryDate { get; set; }
        [DataMember]
        public PaymentCardType PaymentCardType { get; set; }
        [DataMember]
        public PolicyPaymentType PolicyPaymentType { get; set; }
        [DataMember]
        public DateTime? DatePaymentDetailsTaken { get; set; }

        [DataMember]
        public Operation Operation { get; set; }

        //[DataMember]
        //public bool Action { get; set; }

        public string HashPaymentCard(string cardNumber)
        {
            return Hashing.sha256(cardNumber);
        }



        public string MaskPaymentCard(string cardNumber)
        {
            string toReturn = string.Empty;

            if (cardNumber != null)
            {
                int length = cardNumber.Length;
                if (length == 16)
                {
                    toReturn = cardNumber.Substring(0, 4);
                    toReturn += "########";
                    toReturn += cardNumber.Substring(12, 4);
                }
            }
            return toReturn;
        }

        public string DisplayText
        {
            get { return BankName + " " + SortCode + " " + PaymentCardNumber; }
        }
    }
}