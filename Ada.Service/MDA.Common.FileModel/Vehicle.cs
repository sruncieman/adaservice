﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MDA.Common.Enum;

namespace MDA.Common.FileModel
{
    //using System;
    //using System.Collections.Generic;


    [DataContract]
    [Serializable]
    public class Vehicle : IXmlEntity
    {
        public Vehicle()
        {
            People = new ObservableCollection<Person>();

            VehicleType = VehicleType.Car;
            Colour = VehicleColour.Unknown;
            VehicleType = VehicleType.Unknown;
            Fuel = VehicleFuelType.Unknown;
            Transmission = VehicleTransmission.Unknown;
            CategoryOfLoss = VehicleCategoryOfLoss.Unknown;

            Operation = Operation.Normal;
        }

        [DataMember(IsRequired = true)]
        public VehicleType VehicleType { get; set; }

        [DataMember(Name = "VehicleInvolvementGroup")]
        public Incident2VehicleLinkType Incident2VehicleLinkType { get; set; }

        [DataMember]
        public string VehicleRegistration { get; set; }

        [DataMember]
        public VehicleColour Colour { get; set; }

        [DataMember]
        public string Make { get; set; }

        [DataMember]
        public string Model { get; set; }

        [DataMember]
        public string EngineCapacity { get; set; }

        [DataMember]
        public string VIN { get; set; }

        [DataMember]
        public VehicleFuelType Fuel { get; set; }

        [DataMember]
        public VehicleTransmission Transmission { get; set; }

        [DataMember]
        public string DamageDescription { get; set; }

        [DataMember]
        public VehicleCategoryOfLoss CategoryOfLoss { get; set; }

        [DataMember]
        public Operation Operation { get; set; }

        [DataMember(IsRequired = true)]
        public ObservableCollection<Person> People { get; set; }

        public string DisplayText
        {
            get { return VehicleRegistration + "  [" + Incident2VehicleLinkType.ToString() + "]"; }
        }

    }

    [DataContract]
    [Serializable]
    public class OrganisationVehicle : IXmlEntity
    {
        public OrganisationVehicle()
        {
            Colour = VehicleColour.Unknown;
            Incident2VehicleLinkType = Incident2VehicleLinkType.Unknown;
            VehicleType = VehicleType.Unknown;
            Fuel = VehicleFuelType.Unknown;
            Transmission = VehicleTransmission.Unknown;
            Operation = Operation.Normal;

        }

        [DataMember(IsRequired = true)]
        public VehicleType VehicleType { get; set; }

        [DataMember(Name = "VehicleInvolvementGroup")]
        public Incident2VehicleLinkType Incident2VehicleLinkType { get; set; }

        [DataMember]
        public string VehicleRegistration { get; set; }

        [DataMember]
        public VehicleColour Colour { get; set; }

        [DataMember]
        public string Make { get; set; }

        [DataMember]
        public string Model { get; set; }

        [DataMember]
        public string EngineCapacity { get; set; }

        [DataMember]
        public string VIN { get; set; }

        [DataMember]
        public VehicleFuelType Fuel { get; set; }

        [DataMember]
        public VehicleTransmission Transmission { get; set; }

        //[DataMember]
        //public string DamageDescription { get; set; }

        [DataMember]
        public DateTime? HireStartDate { get; set; }

        [DataMember]
        public DateTime? HireEndDate { get; set; }

        [DataMember]
        public Operation Operation { get; set; }

        public string DisplayText
        {
            get { return VehicleRegistration + "  [" + Incident2VehicleLinkType.ToString() + "]"; }
        }

    }
}

