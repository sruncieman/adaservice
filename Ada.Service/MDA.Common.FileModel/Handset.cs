﻿using MDA.Common.Enum;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MDA.Common.FileModel
{
    public class Handset : IXmlEntity
    {
        public Handset()
	    {
            People = new ObservableCollection<Person>();
            HandsetType = HandsetType.Unknown;
            Operation = Operation.Normal;
	    }

        [DataMember]
        public Operation Operation { get; set; }

        [DataMember(IsRequired = true)]
        public HandsetType HandsetType { get; set; }

        [DataMember(Name = "HandsetInvolvementGroup")]
        public Incident2HandsetLinkType Incident2HandsetLinkType { get; set; }

        [DataMember]
        public string IMEI { get; set; }

        [DataMember]
        public string Make { get; set; }

        [DataMember]
        public string Model { get; set; }
         
        [DataMember]
        public HandsetColour Colour { get; set; }

        [DataMember]
        public decimal? Value { get; set; }

        [DataMember]
        public string ValueCategory { get; set; }

        [DataMember]
        public string DeviceFault { get; set; }

        [DataMember(IsRequired = true)]
        public ObservableCollection<Person> People { get; set; }

        public string DisplayText
        {
            get { return IMEI + "  [" + Incident2HandsetLinkType.ToString() + "]"; }
        }
    }

  
}
