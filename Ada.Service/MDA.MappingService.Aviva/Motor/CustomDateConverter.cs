﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.Aviva.Motor
{
    public class CustomDateConverter : ConverterBase
    {
        public override object StringToField(string value)
        {

            value = value.Replace("Z", "");
            value = value.Replace("+01:00", "");

            DateTime dt;
            value = value.TrimEnd();
            if (DateTime.TryParseExact(value, "dd/MM/yyyy", null, DateTimeStyles.None, out dt))
                return dt;

            return null;


        }
    }
}
