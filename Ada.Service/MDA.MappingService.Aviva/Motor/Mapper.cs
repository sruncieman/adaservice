﻿using FileHelpers;
using MDA.Common.Server;
using MDA.MappingService.Aviva.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;
using System.Configuration;
using MDA.Common.Enum;

namespace MDA.MappingService.Aviva.Motor
{
    public class Mapper
    {
        private Stream _fs;
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        Dictionary<string, List<MotorClaimData>> DictionaryMotorClaimData = new Dictionary<string, List<MotorClaimData>>();
        //private List<MotorClaimData> _lstMotorClaim;
        Dictionary<string, List<LiabilityClaimData>> DictionaryLiabilityClaimData = new Dictionary<string, List<LiabilityClaimData>>();
        //private List<LiabilityClaimData> _lstLiabilityClaim;
        private object statusTracking;
        private static bool _boolDebug = true;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        public bool BoolDebug { get; set; }
        public string FilePath { get; set; }
        public string FolderPath { get; set; }
        public string MotorClaimFile { get; set; }
        public string LiabilityClaimFile { get; set; }
        public MotorClaimData[] MotorClaim { get; set; }
        public LiabilityClaimData[] LiabilityClaim { get; set; }
        public FileHelperEngine MotorClaimEngine { get; set; }
        public FileHelperEngine LiabilityClaimEngine { get; set; }

        private string _coreClaimDataFile;
        private string _vehicleClaimDataFile;
        private bool _debug;
        private FileHelperEngine _coreClaimDataEngine;
        private MotorClaimData[] _coreClaimData;
        private List<string> _uniqueClaimNumberList;
        private string _claimCode;
        private string _filePath = null;
        private bool _deleteExtractedFiles;
        private int _claimCount = 0;
        private string _clientFolder;

        private static bool boolDebug = false;
        private static string folderPath;
        string fileDateString = string.Empty;


        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking)
        {
            this._fs = fs;
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;

        }

        public void AttemptToExtractFilesFromZip()
        {
            try
            {
                if (_fs != null)
                {
                    _boolDebug = false;

                    using (ZipFile zip1 = ZipFile.Read(_fs)) //_filePath))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            var extractTmpPath = string.Concat(ClientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);


                            string unpackDirectory = ClientFolder;
                            e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in extraction of files: " + ex);
            }
        }

        public void AssignFiles()
        {
            try
            {
                if (_boolDebug)
                {
                    FolderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Aviva\Data\";
                    MotorClaimFile = FolderPath + @"\MotorBI_MOJ_data.csv";
                    LiabilityClaimFile = FolderPath + @"\ELPL_MOJ_data.csv";
                }
                else
                {
                    MotorClaimFile = ClientFolder + @"\MotorBI_MOJ_data.csv";
                    LiabilityClaimFile = ClientFolder + @"\ELPL_MOJ_data.csv";
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in assigning extracted files: " + ex);
            }
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                MotorClaimEngine = new FileHelperEngine(typeof(MotorClaimData));
                LiabilityClaimEngine = new FileHelperEngine(typeof(LiabilityClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {
                MotorClaim = MotorClaimEngine.ReadFile(MotorClaimFile) as MotorClaimData[];
                LiabilityClaim = LiabilityClaimEngine.ReadFile(LiabilityClaimFile) as LiabilityClaimData[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Motor Claim Numbers 
                DictionaryMotorClaimData = MotorClaim.GroupBy(i => new { i.AccidentDate, i.ClaimSentDate, i.Defendant_PolicyNumber }).ToDictionary(x => x.Key.ToString(), x => x.ToList());
                
                // Get Distinct Liability Claim Numbers 
                DictionaryLiabilityClaimData = LiabilityClaim.GroupBy(i => new { i.Defendant_PolicyNumber, i.AccidentDate, i.ClaimSentDate }).ToDictionary(x => x.Key.ToString(), x => x.ToList());
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }
        

        }

        public void Translate()
        {
            int _skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["AvivaSkipNum"]);
            int _takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["AvivaTakeNum"]);

            #region Motor Claims

            foreach (var claim in DictionaryMotorClaimData.Skip(_skipValue).Take(_takeValue))
            {

                PipelineMotorClaim motorClaim = new PipelineMotorClaim();
                List<string> insuredVehicles = new List<string>();
                List<string> thirdPartyVehicles = new List<string>();

                foreach (var c in claim.Value)
                {
                    #region Claim
                    try
                    {
                        motorClaim.ClaimNumber = c.Internal_Ref.TrimStart(new Char[] { '0' });
                        motorClaim.ClaimNumber = motorClaim.ClaimNumber.TrimEnd(',');
                        motorClaim.IncidentDate =  Convert.ToDateTime(c.AccidentDate);
                        if (motorClaim.IncidentDate == DateTime.MinValue)
                        {
                            motorClaim.IncidentDate = DateTime.Now;
                        }
                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                    }

                    catch (Exception ex)
                    {
                        throw new CustomException("Error in translating general claim data: " + ex);
                    }
                    #endregion

                    #region Claim Info
                    try
                    {
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                        motorClaim.ExtraClaimInfo.ClaimNotificationDate = c.ClaimSentDate;
                        if (motorClaim.ExtraClaimInfo.ClaimCode == null)
                        {
                            motorClaim.ExtraClaimInfo.ClaimCode = "Motor";
                        }
                        motorClaim.ExtraClaimInfo.ClaimCode = motorClaim.ExtraClaimInfo.ClaimCode + "," + c.Internal_Ref.TrimStart(new Char[] { '0' });
                        motorClaim.ExtraClaimInfo.ClaimCode = motorClaim.ExtraClaimInfo.ClaimCode.TrimEnd(',');
                        motorClaim.ExtraClaimInfo.IncidentLocation = c.AccidentLocation;
                    }

                    catch (Exception ex)
                    {
                        throw new CustomException("Error in translating general claim data: " + ex);
                    }
                    #endregion

                    #region Policy
                    try
                    {
                        motorClaim.Policy.Insurer = "Aviva";
                        motorClaim.Policy.PolicyNumber = c.Defendant_PolicyNumber;
                    }

                    catch (Exception ex)
                    {
                        throw new CustomException("Error in translating general claim data: " + ex);
                    }
                    #endregion

                    #region Insured Vehicle

                    PipelineVehicle insuredVehicle = new PipelineVehicle();

                    if (!insuredVehicles.Contains(c.DefendantVehicleReg))
                    {

                        try
                        {
                            if (!string.IsNullOrEmpty(c.DefendantVehicleReg))
                            {

                                bool insuredVehicleExists = false;

                                foreach (var vehicle in motorClaim.Vehicles)
                                {
                                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                    {
                                        insuredVehicleExists = true;
                                    }
                                }

                                if (insuredVehicleExists == false)
                                {
                                    insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                                }
                                else
                                {
                                    insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.Unknown;
                                }


                                insuredVehicle.VehicleRegistration = c.DefendantVehicleReg;
                                motorClaim.Vehicles.Add(insuredVehicle);
                                insuredVehicles.Add(c.DefendantVehicleReg);
                            }
                        }

                        catch (Exception ex)
                        {
                            throw new CustomException("Error in translating general claim data: " + ex);
                        }

                    }

                    #endregion

                    #region Insured Person
                    try
                    {
                        PipelinePerson insuredPerson = new PipelinePerson();

                        insuredPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        insuredPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        if (!string.IsNullOrEmpty(c.DefendantName))
                        {
                            insuredPerson.FirstName = c.DefendantName;
                        }

                        if (!string.IsNullOrEmpty(c.DefendantSurname))
                        {
                            insuredPerson.LastName = c.DefendantSurname;
                        }

                        if (!string.IsNullOrEmpty(c.Defendant_HouseName) || !string.IsNullOrEmpty(c.Defendant_Street) || !string.IsNullOrEmpty(c.Defendant_City) || !string.IsNullOrEmpty(c.Defendant_PostCode))
                        {
                            PipelineAddress insuredAddress = new PipelineAddress();

                            insuredAddress.AddressType_Id = (int)AddressType.Residential;
                            insuredAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                            insuredAddress.Building = c.Defendant_HouseName;
                            insuredAddress.Street = c.Defendant_Street;
                            insuredAddress.Town = c.Defendant_City;
                            insuredAddress.PostCode = c.Defendant_PostCode;

                            insuredPerson.Addresses.Add(insuredAddress);
                        }

                        if (!string.IsNullOrEmpty(c.DefendantVehicleReg))
                        {
                            insuredVehicle.People.Add(insuredPerson);
                        }
                        else
                        {
                            PipelineVehicle unknownInsuredVehicle = new PipelineVehicle();
                            unknownInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                            unknownInsuredVehicle.People.Add(insuredPerson);
                            motorClaim.Vehicles.Add(unknownInsuredVehicle);
                        }

                    }

                    catch (Exception ex)
                    {
                        throw new CustomException("Error in translating general claim data: " + ex);
                    }
                    #endregion

                    #region Claimant Vehicle

                    if (!thirdPartyVehicles.Contains(c.ClaimantVehicleReg))
                    {

                        try
                        {
                            if (!string.IsNullOrEmpty(c.ClaimantVehicleReg))
                            {
                                PipelineVehicle claimantVehicle = new PipelineVehicle();

                                claimantVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                                claimantVehicle.VehicleRegistration = c.ClaimantVehicleReg;
                                motorClaim.Vehicles.Add(claimantVehicle);
                                thirdPartyVehicles.Add(c.ClaimantVehicleReg);
                            }
                        }

                        catch (Exception ex)
                        {
                            throw new CustomException("Error in translating general claim data: " + ex);
                        }

                    }
                    #endregion

                    #region Claimant Person
                    try
                    {
                        if (!string.IsNullOrEmpty(c.Claimant_Forename) || !string.IsNullOrEmpty(c.Claimant_Surname))
                        {
                            PipelinePerson claimantPerson = new PipelinePerson();

                            claimantPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                            //claimantPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType;

                            claimantPerson.FirstName = c.Claimant_Forename;
                            claimantPerson.LastName = c.Claimant_Surname;
                            claimantPerson.DateOfBirth = c.ClaimantDOB;

                            if (!string.IsNullOrEmpty(c.Claimant_Occupation))
                            {
                                claimantPerson.Occupation = c.Claimant_Occupation;
                            }

                            if (!string.IsNullOrEmpty(c.ClaimantNatInsNumber))
                            {
                                PipelineNINumber claimantNINumber = new PipelineNINumber();

                                claimantNINumber.NINumber1 = c.ClaimantNatInsNumber;

                                claimantPerson.NINumbers.Add(claimantNINumber);
                            }

                            if (!string.IsNullOrEmpty(c.Claimant_HouseNumber) || !string.IsNullOrEmpty(c.Claimant_Street) || !string.IsNullOrEmpty(c.Claimant_City) || !string.IsNullOrEmpty(c.Claimant_PostCode))
                            {
                                PipelineAddress claimantAddress = new PipelineAddress();

                                claimantAddress.AddressType_Id = (int)AddressType.Residential;
                                claimantAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                                claimantAddress.BuildingNumber = c.Claimant_HouseNumber;
                                claimantAddress.Street = c.Claimant_Street;
                                claimantAddress.Town = c.Claimant_City;
                                claimantAddress.PostCode = c.Claimant_PostCode;

                                claimantPerson.Addresses.Add(claimantAddress);
                            }

                            #region Claimant Organisation

                            if (!string.IsNullOrEmpty(c.CR_CompanyName) || !string.IsNullOrEmpty(c.CR_EmailAddress) || !string.IsNullOrEmpty(c.CR_TelNumber))
                            {
                                PipelineOrganisation claimantSolicitor = new PipelineOrganisation();
                                claimantSolicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                claimantSolicitor.OrganisationName = c.CR_CompanyName;

                                if (!string.IsNullOrEmpty(c.CR_EmailAddress))
                                {
                                    PipelineEmail email = new PipelineEmail();
                                    email.EmailAddress = c.CR_EmailAddress;
                                    claimantSolicitor.EmailAddresses.Add(email);
                                }

                                if (!string.IsNullOrEmpty(c.CR_TelNumber))
                                {
                                    PipelineTelephone telephone = new PipelineTelephone();
                                    telephone.ClientSuppliedNumber = c.CR_TelNumber;
                                    claimantSolicitor.Telephones.Add(telephone);
                                }

                                if (!string.IsNullOrEmpty(c.CR_HouseName) || !string.IsNullOrEmpty(c.CR_HouseNumber) || !string.IsNullOrEmpty(c.CR_Street) || !string.IsNullOrEmpty(c.CR_City) || !string.IsNullOrEmpty(c.CR_PostCode))
                                {
                                    PipelineAddress claimantSolicitorAddress = new PipelineAddress();

                                    claimantSolicitorAddress.AddressType_Id = (int)AddressType.Commercial;
                                    claimantSolicitorAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;

                                    claimantSolicitorAddress.Building = c.CR_HouseName;
                                    claimantSolicitorAddress.BuildingNumber = c.CR_HouseNumber;
                                    claimantSolicitorAddress.Street = c.CR_Street;
                                    claimantSolicitorAddress.Town = c.CR_City;
                                    claimantSolicitorAddress.PostCode = c.CR_PostCode;

                                    claimantSolicitor.Addresses.Add(claimantSolicitorAddress);
                                }

                                claimantPerson.Organisations.Add(claimantSolicitor);

                            }

                            if (!string.IsNullOrEmpty(c.RepresentativeName))
                            {
                                PipelineOrganisation claimantOrg = new PipelineOrganisation();
                                claimantOrg.OrganisationName = c.RepresentativeName;

                                claimantPerson.Organisations.Add(claimantOrg);
                            }

                            #endregion

                            bool claimantVehicleExists = false;

                            foreach (var v in motorClaim.Vehicles)
                            {
                                if (v.VehicleRegistration == c.ClaimantVehicleReg)
                                {
                                    v.People.Add(claimantPerson);
                                    claimantVehicleExists = true;
                                }
                            }

                            if (claimantVehicleExists == false)
                            {
                                PipelineVehicle thirdPartyVehicle = new PipelineVehicle();
                                thirdPartyVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                                thirdPartyVehicle.People.Add(claimantPerson);
                                motorClaim.Vehicles.Add(thirdPartyVehicle);
                            }
                        }

                    }

                    catch (Exception ex)
                    {
                        throw new CustomException("Error in translating general claim data: " + ex);
                    }
                    #endregion

                }

                #region Insured Vehicle and Driver Check

                bool insuredVehicleCheck = false;
                bool insuredDriverCheck = false;

                foreach (var vehicle in motorClaim.Vehicles)
                {
                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                    {
                        insuredVehicleCheck = true;
                    }

                    foreach (var person in vehicle.People)
                    {
                        if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                        {
                            insuredDriverCheck = true;
                        }
                    }
                }

                if (insuredVehicleCheck == false)
                {

                    if (insuredDriverCheck == false)
                    {
                        PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                        defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;


                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                        motorClaim.Vehicles.Add(defaultInsuredVehicle);

                    }

                }
                else
                {
                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        foreach (var vehicle in motorClaim.Vehicles)
                        {
                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                            {

                                vehicle.People.Add(defaultInsuredDriver);

                            }

                        }

                    }
                }

                #endregion


                if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;


            }
            #endregion

            #region Employment Liability (EL) and Public Liability (PL) Claims

            foreach (var claim in DictionaryLiabilityClaimData.Skip(_skipValue).Take(_takeValue))
            {

                PipelineMotorClaim motorClaim = new PipelineMotorClaim();
                List<string> insuredVehicles = new List<string>();
                List<string> thirdPartyVehicles = new List<string>();

                foreach (var c in claim.Value)
                {

                    #region Claim
                    try
                    {
                        motorClaim.ClaimNumber = c.Internal_Ref.TrimStart(new Char[] { '0' });
                        motorClaim.IncidentDate =  Convert.ToDateTime(c.AccidentDate);
                        if (motorClaim.IncidentDate == DateTime.MinValue)
                        {
                            motorClaim.IncidentDate = DateTime.Now;
                        }
                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                    }

                    catch (Exception ex)
                    {
                        throw new CustomException("Error in translating general claim data: " + ex);
                    }
                    #endregion

                    #region Claim Info
                    try
                    {
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                        motorClaim.ExtraClaimInfo.ClaimNotificationDate = c.ClaimSentDate;
                        if (motorClaim.ExtraClaimInfo.ClaimCode == null)
                        {
                            motorClaim.ExtraClaimInfo.ClaimCode = "EL/PL";
                        }
                        motorClaim.ExtraClaimInfo.ClaimCode = motorClaim.ExtraClaimInfo.ClaimCode + "," + c.Internal_Ref.TrimStart(new Char[] { '0' });
                        motorClaim.ExtraClaimInfo.ClaimCode = motorClaim.ExtraClaimInfo.ClaimCode.TrimEnd(',');
                        motorClaim.ExtraClaimInfo.IncidentLocation = c.AccidentLocation;
                    }

                    catch (Exception ex)
                    {
                        throw new CustomException("Error in translating general claim data: " + ex);
                    }
                    #endregion

                    #region Policy
                    try
                    {
                        motorClaim.Policy.Insurer = "Aviva";
                        motorClaim.Policy.PolicyNumber = c.Defendant_PolicyNumber;
                    }

                    catch (Exception ex)
                    {
                        throw new CustomException("Error in translating general claim data: " + ex);
                    }
                    #endregion

                    #region Insured Person
                    try
                    {

                        PipelinePerson insuredPerson = new PipelinePerson();
                        insuredPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        insuredPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        if (!string.IsNullOrEmpty(c.Defendant_Company))
                        {
                            PipelineOrganisation insuredOrg = new PipelineOrganisation();
                            insuredOrg.OrganisationName = c.Defendant_Company;

                            insuredPerson.Organisations.Add(insuredOrg);

                            if (!string.IsNullOrEmpty(c.Defendant_HouseName) || !string.IsNullOrEmpty(c.Defendant_Street) || !string.IsNullOrEmpty(c.Defendant_City) || !string.IsNullOrEmpty(c.Defendant_PostCode))
                            {
                                PipelineAddress insuredAddress = new PipelineAddress();

                                insuredAddress.AddressType_Id = (int)AddressType.Commercial;
                                insuredAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;

                                insuredAddress.Building = c.Defendant_HouseName;
                                insuredAddress.Street = c.Defendant_Street;
                                insuredAddress.Town = c.Defendant_City;
                                insuredAddress.PostCode = c.Defendant_PostCode;

                                insuredOrg.Addresses.Add(insuredAddress);
                            }

                            PipelineVehicle unknownInsuredVehicle = new PipelineVehicle();
                            unknownInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                            unknownInsuredVehicle.People.Add(insuredPerson);

                            bool insuredVehiclesCheck = false;

                            foreach (var vehicle in motorClaim.Vehicles)
                            {
                                if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                {
                                    insuredVehiclesCheck = true;
                                }
                            }

                            if (insuredVehiclesCheck == false)
                            {
                                motorClaim.Vehicles.Add(unknownInsuredVehicle);
                            }

                        }
                        else
                        {

                            if (!string.IsNullOrEmpty(c.DefendantName))
                            {
                                insuredPerson.FirstName = c.DefendantName;
                            }

                            if (!string.IsNullOrEmpty(c.DefendantSurname))
                            {
                                insuredPerson.LastName = c.DefendantSurname;
                            }

                            if (!string.IsNullOrEmpty(c.Defendant_HouseName) || !string.IsNullOrEmpty(c.Defendant_Street) || !string.IsNullOrEmpty(c.Defendant_City) || !string.IsNullOrEmpty(c.Defendant_PostCode))
                            {
                                PipelineAddress insuredAddress = new PipelineAddress();

                                insuredAddress.AddressType_Id = (int)AddressType.Residential;
                                insuredAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                                insuredAddress.Building = c.Defendant_HouseName;
                                insuredAddress.Street = c.Defendant_Street;
                                insuredAddress.Town = c.Defendant_City;
                                insuredAddress.PostCode = c.Defendant_PostCode;

                                insuredPerson.Addresses.Add(insuredAddress);
                            }

                            PipelineVehicle unknownInsuredVehicle = new PipelineVehicle();
                            unknownInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                            unknownInsuredVehicle.People.Add(insuredPerson);

                            bool insuredVehiclesCheck = false;

                            foreach (var vehicle in motorClaim.Vehicles)
                            {
                                if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                {
                                    insuredVehiclesCheck = true;
                                }
                            }

                            if (insuredVehiclesCheck == false)
                            {
                                motorClaim.Vehicles.Add(unknownInsuredVehicle);
                            }

                        }


                    }

                    catch (Exception ex)
                    {
                        throw new CustomException("Error in translating general claim data: " + ex);
                    }
                    #endregion

                    #region Claimant Person
                    try
                    {
                        if (!string.IsNullOrEmpty(c.ClaimantName) || !string.IsNullOrEmpty(c.ClaimantSurname))
                        {
                            PipelinePerson claimantPerson = new PipelinePerson();

                            claimantPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                            //claimantPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType;

                            claimantPerson.FirstName = c.ClaimantName;
                            claimantPerson.LastName = c.ClaimantSurname;
                            claimantPerson.DateOfBirth = c.ClaimantDOB;

                            if (!string.IsNullOrEmpty(c.Claimant_Occupation))
                            {
                                claimantPerson.Occupation = c.Claimant_Occupation;
                            }

                            if (!string.IsNullOrEmpty(c.ClaimantNatInsNumber))
                            {
                                PipelineNINumber claimantNINumber = new PipelineNINumber();

                                claimantNINumber.NINumber1 = c.ClaimantNatInsNumber;

                                claimantPerson.NINumbers.Add(claimantNINumber);
                            }

                            if (!string.IsNullOrEmpty(c.Claimant_HouseNumber) || !string.IsNullOrEmpty(c.Claimant_Street) || !string.IsNullOrEmpty(c.Claimant_City) || !string.IsNullOrEmpty(c.Claimant_PostCode))
                            {
                                PipelineAddress claimantAddress = new PipelineAddress();

                                claimantAddress.AddressType_Id = (int)AddressType.Residential;
                                claimantAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                                claimantAddress.BuildingNumber = c.Claimant_HouseNumber;
                                claimantAddress.Street = c.Claimant_Street;
                                claimantAddress.Town = c.Claimant_City;
                                claimantAddress.PostCode = c.Claimant_PostCode;

                                claimantPerson.Addresses.Add(claimantAddress);
                            }

                            #region Claimant Organisation

                            if (!string.IsNullOrEmpty(c.CR_CompanyName) || !string.IsNullOrEmpty(c.CR_EmailAddress) || !string.IsNullOrEmpty(c.CR_TelNumber))
                            {
                                PipelineOrganisation claimantSolicitor = new PipelineOrganisation();
                                claimantSolicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                claimantSolicitor.OrganisationName = c.CR_CompanyName;

                                if (!string.IsNullOrEmpty(c.CR_EmailAddress))
                                {
                                    PipelineEmail email = new PipelineEmail();
                                    email.EmailAddress = c.CR_EmailAddress;
                                    claimantSolicitor.EmailAddresses.Add(email);
                                }

                                if (!string.IsNullOrEmpty(c.CR_TelNumber))
                                {
                                    PipelineTelephone telephone = new PipelineTelephone();
                                    telephone.ClientSuppliedNumber = c.CR_TelNumber;
                                    claimantSolicitor.Telephones.Add(telephone);
                                }

                                if (!string.IsNullOrEmpty(c.CR_HouseName) || !string.IsNullOrEmpty(c.CR_HouseNumber) || !string.IsNullOrEmpty(c.CR_Street) || !string.IsNullOrEmpty(c.CR_City) || !string.IsNullOrEmpty(c.CR_PostCode))
                                {
                                    PipelineAddress claimantSolicitorAddress = new PipelineAddress();

                                    claimantSolicitorAddress.AddressType_Id = (int)AddressType.Commercial;
                                    claimantSolicitorAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;

                                    claimantSolicitorAddress.Building = c.CR_HouseName;
                                    claimantSolicitorAddress.BuildingNumber = c.CR_HouseNumber;
                                    claimantSolicitorAddress.Street = c.CR_Street;
                                    claimantSolicitorAddress.Town = c.CR_City;
                                    claimantSolicitorAddress.PostCode = c.CR_PostCode;

                                    claimantSolicitor.Addresses.Add(claimantSolicitorAddress);
                                }

                                claimantPerson.Organisations.Add(claimantSolicitor);

                            }

                            if (!string.IsNullOrEmpty(c.RepresentativeName))
                            {
                                PipelineOrganisation claimantOrg = new PipelineOrganisation();
                                claimantOrg.OrganisationName = c.RepresentativeName;

                                claimantPerson.Organisations.Add(claimantOrg);
                            }

                            #endregion

                            PipelineVehicle thirdPartyVehicle = new PipelineVehicle();
                            thirdPartyVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;
                            thirdPartyVehicle.People.Add(claimantPerson);
                            motorClaim.Vehicles.Add(thirdPartyVehicle);

                        }

                    }

                    catch (Exception ex)
                    {
                        throw new CustomException("Error in translating general claim data: " + ex);
                    }
                    #endregion

                }

                #region Insured Vehicle and Driver Check

                bool insuredVehicleCheck = false;
                bool insuredDriverCheck = false;

                foreach (var vehicle in motorClaim.Vehicles)
                {
                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                    {
                        insuredVehicleCheck = true;
                    }

                    foreach (var person in vehicle.People)
                    {
                        if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                        {
                            insuredDriverCheck = true;
                        }
                    }
                }

                if (insuredVehicleCheck == false)
                {

                    if (insuredDriverCheck == false)
                    {
                        PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                        defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;


                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                        motorClaim.Vehicles.Add(defaultInsuredVehicle);

                    }

                }
                else
                {
                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        foreach (var vehicle in motorClaim.Vehicles)
                        {
                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                            {

                                vehicle.People.Add(defaultInsuredDriver);

                            }

                        }

                    }
                }

                #endregion

                if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;
            }

            #endregion


        }
    }
}
