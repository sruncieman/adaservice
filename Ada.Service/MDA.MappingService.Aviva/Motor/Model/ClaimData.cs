﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MDA.MappingService.Aviva.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class LiabilityClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime ?ClaimSentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RepresentativeBranchID;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RepresentativeBranchName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RepresentativeID;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RepresentativeName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? AccidentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AccidentLocation;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? AccidentTime;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_CompanyName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_ContactName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_EmailAddress;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_RefNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_TelNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_HouseNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_HouseName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_Street;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_City;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_Country;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_PostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantIsChild;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantNatInsNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Occupation;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantSurname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? ClaimantDOB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_HouseNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Street;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_City;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Country;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_PostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomStringConverter))]
        public string Defendant_PolicyNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DefendantName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DefendantSurname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Defendant_HouseName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Defendant_Street;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Defendant_City;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Defendant_Country;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Defendant_PostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Defendant_Company;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Internal_Ref;

    }

    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class MotorClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? ClaimSentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RepresentativeBranchID;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RepresentativeBranchName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RepresentativeID;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RepresentativeName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? AccidentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AccidentLocation;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? AccidentTime;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_CompanyName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_ContactName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_EmailAddress;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_RefNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_TelNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_HouseNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_HouseName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_Street;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_City;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_Country;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CR_PostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantIsChild;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Forename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Surname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantNatInsNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? ClaimantDOB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomStringConverter))]
        public string ClaimantVehicleReg;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_HouseNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Street;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_City;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Country;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_PostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant_Occupation;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomStringConverter))]
        public string Defendant_PolicyNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DefendantName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DefendantSurname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomStringConverter))]
        public string DefendantVehicleReg;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Defendant_HouseName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Defendant_Street;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Defendant_City;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Defendant_Country;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Defendant_PostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Internal_Ref;
    }
}
