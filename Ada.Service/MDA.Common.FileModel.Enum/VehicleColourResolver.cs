﻿using System.Collections.Generic;
using System.Linq;

namespace MDA.Common.Enum
{
    public static class VehicleColourResolver
    {
        public static VehicleColour DeriveVehicleColour(this string colourToMatch)
        {
            if (string.IsNullOrEmpty(colourToMatch))
            {
                return Enum.VehicleColour.Unknown;
            }

            var matchingColour = VehicleColours.FirstOrDefault(c => c.ToLower() == colourToMatch.ToLower());

            if (matchingColour == null)
            {
                return Enum.VehicleColour.Unknown;
            }

            VehicleColour parsedVehicleColour;

            if (System.Enum.TryParse(colourToMatch, true, out parsedVehicleColour))
            {
                return parsedVehicleColour;
            }

            return Enum.VehicleColour.Unknown;
        }

        private static string[] _vehicleColours;

        private static IEnumerable<string> VehicleColours
        {
            get
            {
                if (_vehicleColours == null)
                {
                    _vehicleColours = System.Enum.GetNames(typeof(VehicleColour));
                }
                return _vehicleColours;
            }
        }
    }
}
