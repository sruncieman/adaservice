﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace MDA.Common.Enum
{
    [DataContract]
    //[Serializable]
    public enum Operation
    {
        [EnumMember]
        Normal = 0,
        [EnumMember]
        Delete,
        [EnumMember]
        Withdrawn
    }

    [DataContract]
    //[Serializable]
    public enum CaseIncidentLinkType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Instruction = 1,
        [EnumMember]
        InfoOnly = 2
    }

    [DataContract]
    //[Serializable]
    public enum AddressType  // not used in XML
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember(Value = "Residential")]
        Residential = 1,
        [EnumMember(Value = "Commercial")]
        Commercial = 2
    }

    [DataContract(Name="AddressStatus")]
    //[Serializable]
    public enum AddressLinkType   // AddressLinkType -- Used in Address.AddressStatus    DB:AddressLinkType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember(Value = "Previous Address")]
        PreviousAddress = 1,
        [EnumMember(Value = "Linked Address")]
        LinkedAddress,
        [EnumMember(Value = "Current Address")]
        CurrentAddress,
        [EnumMember(Value = "Registered Office")]
        RegisteredOffice,
        [EnumMember(Value = "Trading Address")]
        TradingAddress,
        [EnumMember(Value = "Storage Address")]
        StorageAddress,
        [EnumMember(Value = "Inspected at")]
        InspectedAt,
        [EnumMember(Value = "Blank")]
        Blank,
        [EnumMember(Value = "Interviewed at")]
        InterviewedAt,
        [EnumMember(Value = "Lives at")]
        LivesAt,
        [EnumMember(Value = "Recovered to")]
        RecoveredTo,
        [EnumMember(Value = "Seen at")]
        SeenAt,
        [EnumMember(Value = "Stolen from")]
        StolenFrom,
        [EnumMember(Value = "RepairedAt")]
        RepairedAt,
        [EnumMember(Value = "Owner")]
        Owner,
        [EnumMember(Value = "Delivery Address")]
        DeliveryAddress,
        [EnumMember(Value = "Pickup Address")]
        PickupAddress,
        [EnumMember(Value = "Jiffy Bag Address")]
        JiffyBagAddress,

    };


    [DataContract]
    //[Serializable]
    public enum ClaimType  // pushed into IncidentType
    {
        [EnumMember]
        NA = 0,
        [EnumMember]
        Motor = 1,
        [EnumMember]
        Commercial = 2,
        [EnumMember]
        Employment = 3,
        [EnumMember(Value = "Public Liability")]
        PublicLiability = 4,
        [EnumMember]
        Travel = 5,
        [EnumMember(Value = "Home Contents")]
        HomeContents = 6,
        [EnumMember(Value = "Home Building")]
        HomeBuilding = 7,
        [EnumMember]
        KeoghsCFS = 8,
        [EnumMember]
        KeoghsCMS = 9,
        [EnumMember]
        Theft = 10,
        [EnumMember]
        Pet = 11,
        [EnumMember(Value = "Mobile Phone")]
        MobilePhone = 12,
        [EnumMember(Value = "Person Injury")]
        PersonalInjury = 13,
        [EnumMember(Value = "Mobile Phone - Accidental Damage")]
        MobilePhoneAccidentalDamage = 14,
        [EnumMember(Value = "Mobile Phone - Loss")]
        MobilePhoneLoss = 15,
        [EnumMember(Value = "Mobile Phone – Theft")]
        MobilePhoneTheft = 16,

    };

    [DataContract]
    //[Serializable]
    public enum MojCRMStatus  // used in Organisation.MojCrmStatus   DB:MojCrmStatus
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Authorised = 1,
        [EnumMember]
        Suspended = 2,
        [EnumMember]
        Cancelled = 3
    };

    [DataContract]
    //[Serializable]
    public enum MojStatus  
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Stage1 = 1,
        [EnumMember]
        Stage2 = 2,
        [EnumMember]
        Stage3 = 3,
        [EnumMember(Value = "Out Of Process")]
        OutOfProcess = 4
    };

    [DataContract]
    //[Serializable]
    public enum ClaimStatus  // used in Organisation.ClaimInfo.ClaimStatus and Person.ClaimInfo.ClaimStatus  DB:ClaimStatus
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Open = 1,
        [EnumMember]
        Settled = 2,
        [EnumMember]
        Withdrawn = 3,
        [EnumMember]
        Reopened = 4,
        [EnumMember]
        Delete = 5
    };

    [DataContract]
    //[Serializable]
    public enum PolicyPaymentType   // used in Organisation.BankAccount.PolicyPaymentType and Person.BankAccount.PolicyPaymentType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember(Value = "Deposit")]
        Deposit = 1,
        [EnumMember(Value = "Direct Debit")]
        DirectDebit,
        [EnumMember(Value = "Single Payment")]
        SinglePayment
    };

    [DataContract]
    //[Serializable]
    public enum OrganisationStatus   // not used in XML  InsertOrganisation hardcodes to ACTIVE.  DB:OrganisationStatus
    {
        [EnumMember(Value = "Unknown")]
        Unknown = 0,
        [EnumMember(Value = "Active")]
        Active = 1,
        [EnumMember(Value = "Dissolved")]
        Dissolved = 2,
        [EnumMember(Value = "In Liquidation")]
        InLiquidation = 3,
        [EnumMember(Value = "Receivership Action")]
        ReceivershipAction = 4,
    }

    [DataContract(Name = "Organisation2OrganisationRelationship")]
    //[Serializable]
    public enum OrganisationLinkType // not used in XML  InsertLink hardcodes to UNKNOWN DB:OrganisationLinkType
    {
        [EnumMember(Value = "Unknown")]
        Unknown = 0,
        [EnumMember(Value = "Director")]
        Director = 1,
        [EnumMember(Value = "Secretary")]
        Secretary = 2,
        [EnumMember(Value = "Previous Director")]
        PreviousDirector = 3,
        [EnumMember(Value = "Previous Secretary")]
        PreviousSecretary = 4,
        [EnumMember(Value = "Formerly Known As")]
        FormerlyKnownAs = 5,
        [EnumMember(Value = "Trading Name Of")]
        TradingNameOf = 6,
        [EnumMember(Value = "Also Known As")]
        AlsoKnownAs = 7,
    }

    [DataContract]
    //[Serializable]
    public enum OrganisationType // used in Organisation.OrganisationType   DB:OrganisationType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember(Value = "Policy Holder")]
        PolicyHolder,
        [EnumMember(Value = "Undefined Supplier")]
        UndefinedSupplier,
        [EnumMember(Value = "Accident Management")]
        AccidentManagement,
        [EnumMember(Value = "Credit Hire")]
        CreditHire,
        [EnumMember(Value = "Solicitor")]
        Solicitor,
        [EnumMember(Value = "Vehicle Engineer")]
        VehicleEngineer,
        [EnumMember(Value = "Recovery")]
        Recovery,
        [EnumMember(Value = "Storage")]
        Storage,
        [EnumMember(Value = "Broker")]
        Broker,
        [EnumMember(Value = "Medical Examiner")]
        MedicalExaminer,
        [EnumMember(Value = "Repairer")]
        Repairer,
        [EnumMember(Value = "Insurer")]
        Insurer
    };


    [DataContract(Name="Person2OrganisationRelationship")]
    //[Serializable]
    public enum Person2OrganisationLinkType // used in Organisation.OrganisationPersonRelationship  DB:Person2OrganisationLinkType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Director = 1,
        [EnumMember]
        Secretary,
        [EnumMember]
        Employee,
        [EnumMember(Value = "Previous Director")]
        PreviousDirector,
        [EnumMember(Value = "Previous Secretary")]
        PreviousSecretary,
        [EnumMember(Value = "Previous Employee")]
        PreviousEmployee,
        [EnumMember]
        Manager,
        [EnumMember]
        Owner,
        [EnumMember]
        Partner,
        [EnumMember]
        Broker,
        [EnumMember(Value = "Referral Source")]
        ReferralSource,
        [EnumMember]
        Solicitor,
        [EnumMember]
        Engineer,
        [EnumMember]
        Recovery,
        [EnumMember]
        Storage,
        [EnumMember]
        Repairer,
        [EnumMember]
        Hire,
        [EnumMember(Value = "Accident Management")]
        AccidentManagement,
        [EnumMember(Value = "Medical Examiner")]
        MedicalExaminer,
        [EnumMember(Value = "Medical Legal")]
        MedicalLegal,
        [EnumMember(Value = "Undefined Supplier")]
        UndefinedSupplier
    };


    [DataContract]
    //[Serializable]
    public enum PaymentCardType// used in Organisation.PaymentCard.PaymentCardType and Person.PaymentCard.PaymentCardType  DB:PaymentCardType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember(Value = "Credit Card")]
        CreditCard,
        [EnumMember(Value = "Debit Card")]
        DebitCard
    };

    [DataContract(Name = "Person2PolicyRelationship")]
    //[Serializable]
    public enum PolicyLinkType   // not used in XML   DB:PolicyLinkType  used to link Person and Org and vehicle to Policy
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Policyholder = 1,
        [EnumMember(Value = "Named Driver")]
        NamedDriver,
        [EnumMember(Value = "Paid Policy")]
        PaidPolicy,
        [EnumMember]
        Insured
    };


    [DataContract(Name = "Person2PersonRelationship")]
    //[Serializable]
    public enum Person2PersonLinkType  // not used in XML  DB:Person2PersonLinkType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember(Value = "Also Known As")]
        AlsoKnownAs = 1,
        [EnumMember]
        Associate = 2,
        [EnumMember]
        Aunt = 3,
        [EnumMember]
        Brother = 4,
        [EnumMember(Value = "Brother In Law")]
        BrotherInLaw = 5,
        [EnumMember]
        Father = 6,
        [EnumMember(Value = "Father In Law")]
        FatherInLaw = 7,
        [EnumMember]
        Friend = 8,
        [EnumMember]
        Interpreter = 9,
        [EnumMember]
        Mother = 10,
        [EnumMember(Value = "Mother In Law")]
        MotherInLaw = 11,
        [EnumMember]
        Partner = 12,
        [EnumMember]
        Sibling = 13,
        [EnumMember]
        Sister = 14,
        [EnumMember(Value = "Sister In Law")]
        SisterInLaw = 15,
        [EnumMember]
        Spouce = 16,
        [EnumMember(Value = "Suspected Identity Match")]
        SuspectedIdentityMatch = 17,
        [EnumMember]
        Uncle = 18
    };

    [DataContract(Name = "Person2VehicleRelationship")]
    //[Serializable]
    public enum VehicleLinkType  // not used in XML  DB:VehicleLinkType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Driver = 1,
        [EnumMember(Value = "Examined By")]
        ExaminedBy,
        [EnumMember(Value = "Hired From")]
        HiredFrom,
        [EnumMember]
        Hirer,
        [EnumMember(Value = "Inspected By")]
        InspectedBy,
        [EnumMember]
        Insured,
        [EnumMember]
        Owner,
        [EnumMember]
        Passenger,
        [EnumMember(Value = "Previous Keeper")]
        PreviousKeeper,
        [EnumMember(Value = "Recovered By")]
        RecoveredBy,
        [EnumMember(Value = "Recovered To")]
        RecoveredTo,
        [EnumMember(Value = "Registered Keeper")]
        RegisteredKeeper,
        [EnumMember]
        Repairer,
        [EnumMember(Value = "Stored At")]
        StoredAt,
        [EnumMember]
        Witness
    };


    [DataContract(Name = "Person2HandsetRelationship")]
    //[Serializable]
    public enum HandsetLinkType  // not used in XML  DB:HandsetLinkType
    {
        [EnumMember]
        Unknown, 
        [EnumMember]
        Claimant,
        [EnumMember]
        Owner,
        [EnumMember]
        Uses
    };

    [DataContract]
    //[Serializable]
    public enum PartyType  // used in Person.PartyType  DB:PartyType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Claimant = 1,
        [EnumMember]
        Hirer,
        [EnumMember]
        Insured,
        [EnumMember(Value = "Litigation Friend")]
        LitigationFriend,
        [EnumMember]
        Policyholder,
        [EnumMember(Value = "Third Party")]
        ThirdParty,
        [EnumMember]
        Witness,
        [EnumMember(Value = "Vehicle Owner")]
        VehicleOwner,
        [EnumMember(Value = "Paid Policy")]
        PaidPolicy
    };

    [DataContract]
    //[Serializable]
    public enum SubPartyType  // used in Person.SubPartyType  DB:SubPartyType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Driver = 1,
        [EnumMember]
        Passenger,
        [EnumMember]
        Witness
    };

    [DataContract]
    //[Serializable]
    public enum Salutation   // used in Person  DB:Salutation
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Mr = 1,
        [EnumMember]
        Mrs,
        [EnumMember]
        Ms,
        [EnumMember]
        Miss,
        [EnumMember]
        Master,
        [EnumMember]
        Dr,
        [EnumMember]
        Professor,
        [EnumMember]
        Reverend,
        [EnumMember]
        Sir,
        [EnumMember]
        Lord,
        [EnumMember]
        Baroness,
        [EnumMember(Value = "Right Honourable")]
        RightHonourable
    };

    [DataContract]
    //[Serializable]
    public enum Gender   // used in Person   DB:Gender
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Male = 1,
        [EnumMember]
        Female = 2,
    };


    [DataContract]
    //[Serializable]
    public enum PolicyType   // used in Policy.PolicyType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember(Value = "Personal Motor")]
        PersonalMotor = 1,
        [EnumMember(Value = "Commercial Motor")]
        CommercialMotor = 2,
        [EnumMember(Value = "Home Contents")]
        HomeContents = 3,
        [EnumMember(Value = "Home Building")]
        HomeBuilding = 4,
        [EnumMember(Value = "Employers Liability")]
        EmployersLiability = 5,
        [EnumMember(Value = "Public Liability")]
        PublicLiability = 7,
        [EnumMember(Value = "Payment Protection")]
        PaymentProtection = 8,
        [EnumMember(Value = "Permanent Health")]
        PermanentHealth = 9,
        [EnumMember(Value = "Home")]
        Home = 10,
        [EnumMember(Value = "Commercial")]
        Commercial = 11,
        [EnumMember(Value = "Mobile")]
        Mobile = 12
    };

    [DataContract]
    //[Serializable]
    public enum PolicyCoverType   // used in Policy.CoverType    DB:PolicyCoverType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember(Value = "Comprehensive")]
        Comprehensive = 1,
        [EnumMember(Value = "Third Party, Fire and Theft")]
        ThirdPartyFireAndTheft = 2,
        [EnumMember(Value = "Third Party Only")]
        ThirdPartyOnly = 3
    };

    [DataContract]
    public enum HandsetType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Insured = 1,
        [EnumMember]
        Updated = 2,
        [EnumMember]
        Replacement = 3,
        [EnumMember]
        Policy = 4,
        [EnumMember(Value = "Previously Insured Handset")]
        PreviouslyInsuredHandset = 5
    }

    [DataContract(Name = "HandsetInvolvementGroup")]
    //[Serializable]
    public enum Incident2HandsetLinkType   // used in Handset   DB:Incident2HandsetLinkType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember(Value = "Policy Handset")]
        PolicyHandset = 1,
        [EnumMember(Value = "Insured Handset")]
        InsuredHandset = 2,
        [EnumMember(Value = "Previously Insured Handset")]
        PreviouslyInsuredHandset = 3,
        [EnumMember(Value = "Replacement Handset")]
        ReplacementHandset = 4,
        [EnumMember(Value = "Loan Handset")]
        LoanHandset = 5,
        [EnumMember(Value = "No Handset Involved")]
        NoHandsetInvolved = 6
    }

    [DataContract]
    //[Serializable]
    public enum VehicleType    // used in Vehicle   DB:VehicleType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Bicycle = 1,
        [EnumMember]
        Car = 2,
        [EnumMember]
        Coach = 3,
        [EnumMember]
        Lorry = 4,
        [EnumMember]
        Minibus = 5,
        [EnumMember]
        Motorcycle = 6,
        [EnumMember]
        Pickup = 7,
        [EnumMember]
        Van = 8,
        [EnumMember]
        Taxi = 9,
    };

    [DataContract(Name = "VehicleInvolvementGroup")]
    //[Serializable]
    public enum Incident2VehicleLinkType   // used in Vehicle   DB:Incident2VehicleLinkType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember(Value = "Insured Vehicle")]
        InsuredVehicle = 1,
        [EnumMember(Value = "Third Party Vehicle")]
        ThirdPartyVehicle = 2,
        [EnumMember(Value = "Insured Hire Vehicle")]
        InsuredHireVehicle = 3,
        [EnumMember(Value = "Third Party Hire Vehicle")]
        ThirdPartyHireVehicle = 4,
        [EnumMember(Value = "Witness Vehicle")]
        WitnessVehicle = 5,
        [EnumMember(Value = "No Vehicle Involved")]
        NoVehicleInvolved = 6
    };


    [DataContract]
    //[Serializable]
    public enum VehicleFuelType   // used in Vehicle   DB:VehicleFuel
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Petrol = 1,
        [EnumMember]
        Diesel = 2,
        [EnumMember]
        Electric = 3,
        [EnumMember]
        Hybrid = 4,
        [EnumMember]
        LPG = 5
    };

    [DataContract]
    //[Serializable]
    public enum VehicleTransmission   // used in Vehicle   DB:VehicleTransmission
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Manual = 1,
        [EnumMember]
        Automatic = 2
    };

    [DataContract]
    //[Serializable]
    public enum HandsetColour   // used in Handset   DB:HandsetColour
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Beige,
        [EnumMember]
        Black,
        [EnumMember]
        Blue,
        [EnumMember]
        Bronze,
        [EnumMember]
        Brown,
        [EnumMember]
        Cream,
        [EnumMember]
        Gold,
        [EnumMember]
        Graphite,
        [EnumMember]
        Green,
        [EnumMember]
        Grey,
        [EnumMember]
        Lilac,
        [EnumMember]
        Maroon,
        [EnumMember]
        Mauve,
        [EnumMember]
        Orange,
        [EnumMember]
        Pink,
        [EnumMember]
        Purple,
        [EnumMember]
        Red,
        [EnumMember]
        Silver,
        [EnumMember]
        Turquoise,
        [EnumMember]
        White,
        [EnumMember]
        Yellow
    };

    [DataContract]
    //[Serializable]
    public enum VehicleColour   // used in Vehicle   DB:VehicleColour
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Beige,
        [EnumMember]
        Black,
        [EnumMember]
        Blue,
        [EnumMember]
        Bronze,
        [EnumMember]
        Brown,
        [EnumMember]
        Cream,
        [EnumMember]
        Gold,
        [EnumMember]
        Graphite,
        [EnumMember]
        Green,
        [EnumMember]
        Grey,
        [EnumMember]
        Lilac,
        [EnumMember]
        Maroon,
        [EnumMember]
        Mauve,
        [EnumMember]
        Orange,
        [EnumMember]
        Pink,
        [EnumMember]
        Purple,
        [EnumMember]
        Red,
        [EnumMember]
        Silver,
        [EnumMember]
        Turquoise,
        [EnumMember]
        White,
        [EnumMember]
        Yellow
    };

    [DataContract]
    //[Serializable]
    public enum VehicleCategoryOfLoss   // used in Vehicle  DB:VehicleCategoryOfLoss
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        CatA = 1,
        [EnumMember]
        CatB = 2,
        [EnumMember]
        CatC = 3,
        [EnumMember]
        CatD = 4
    };

    [DataContract]
    //[Serializable]
    public enum TelephoneType
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Landline = 1,
        [EnumMember]
        Mobile = 2,
        [EnumMember]
        FAX = 3
    };

    [DataContract]
    //[Serializable]
    public enum TelephoneLinkType
    {
        [EnumMember]
        Uses = 0,
        [EnumMember(Value = "Registered at")]
        RegisteredAt = 1,
        [EnumMember(Value = "Linked Address")]
        LinkedAddress = 2,
    };

    [DataContract]
    //[Serializable]
    public enum IncidentType   // when read from database
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        RTC = 1,
        [EnumMember]
        Commercial = 2,
        [EnumMember]
        Employment = 3,
        [EnumMember(Value = "Public Liability")]
        PublicLiability = 4,
        [EnumMember]
        Travel = 5,
        [EnumMember(Value = "Home Contents")]
        HomeContents = 6,
        [EnumMember(Value = "Home Building")]
        HomeBuilding = 7,
        [EnumMember(Value = "Keoghs CFS")]
        KeoghsCFS = 8,
        [EnumMember(Value = "Keoghs CMS")]
        KeoghsCMS = 9,
        [EnumMember]
        Theft = 10,
        [EnumMember]
        Pet = 11,
        [EnumMember(Value = "Mobile Phone")]
        MobilePhone = 12,
        [EnumMember(Value = "Person Injury")]
        PersonInjury = 13,
        [EnumMember(Value = "Mobile Phone - Accidental Damage")]
        MobilePhoneAccidentalDamage = 14,
        [EnumMember(Value = "Mobile Phone - Loss")]
        MobilePhoneLoss = 15,
        [EnumMember(Value = "Mobile Phone – Theft")]
        MobilePhoneTheft = 16,
        [EnumMember(Value = "CUE Home")]
        CUEHome = 17,
        [EnumMember(Value = "CUE Motor")]
        CUEMotor = 18,
        [EnumMember(Value = "CUE PI")]
        CUEPI = 19,
        [EnumMember]
        MIAFTR = 20
    }

    [DataContract]
    //[Serializable]
    public enum Incident2IncidentLinkType
    {
        [EnumMember]
        ADA_Alias = 0,
        [EnumMember]
        CUE_Reference = 1
    }

    [DataContract]
    //[Serializable]
    public enum Incident2AddressLinkType
    {
        [EnumMember]
        CUE_Match = 0,
    }

   
    public enum SyncOrDedupeType
    {
        Recovery = 0,
        Pipeline = 1,
        Overnight = 2,
    }

    public enum BackupType
    {
        InititalFullBackup = 1,
        DiffBeforeCreate = 2,
        LogAfterLoading = 3,
        LogAfterExternal = 4,
        LogAfterScoring = 5,
        BeforeSyncLog = 6,
        AfterSyncLog = 7
    }

    public enum TimingType
    {
        Matching = 1,
        Scoring = 2
    }

#if false
    


    [DataContract]
    //[Serializable]
    public enum BankName
    {
        Unknown = 0,
        [EnumMember(Value = "Abbey")]
        Abbey,
        [EnumMember(Value = "Alliance and Leicester")]
        AllianceLeicester,
        [EnumMember(Value = "American Express")]
        AmericanExpress,
        [EnumMember(Value = "Bank of America")]
        BankOfAmerica,
        [EnumMember(Value = "Bank of England")]
        BankOfEngland,
        [EnumMember(Value = "Bank of Ireland")]
        BankOfIreland,
        [EnumMember(Value = "Bank of Scotland")]
        BankOfScotland,
        [EnumMember(Value = "Barclaycard")]
        Barclaycard,
        [EnumMember(Value = "Barclays")]
        Barclays,
        [EnumMember(Value = "Birmingham Midshires")]
        BirminghamMidshires,
        [EnumMember(Value = "Capital One")]
        CapitalOne,
        [EnumMember(Value = "Chelsea Building Society")]
        ChelseaBuildingSociety,
        [EnumMember(Value = "Clydesdale Bank")]
        ClydesdaleBank,
        [EnumMember(Value = "Co-Operative Bank")]
        CoOperativeBank,
        [EnumMember(Value = "Coventry Building Society")]
        CoventryBuildingSociety,
        [EnumMember(Value = "Dresdner Bank AG")]
        DresdnerBankAG,
        [EnumMember(Value = "Egg Banking Plc")]
        EggBankingPlc,
        [EnumMember(Value = "First National Bank")]
        FirstNationalBank,
        [EnumMember(Value = "GE CapitalBank Ltd")]
        GECapitalBankLtd,
        [EnumMember(Value = "Habib Bank")]
        HabibBank,
        [EnumMember(Value = "Halifax Bank")]
        HalifaxBank,
        [EnumMember(Value = "HFC")]
        HFC,
        [EnumMember(Value = "HSBC")]
        HSBC,
        [EnumMember(Value = "ING")]
        ING,
        [EnumMember(Value = "Islamic Bank of Britain")]
        IslamicBankOfBritain,
        [EnumMember(Value = "Lloyds TSB")]
        LloydsTSB,
        [EnumMember(Value = "Mastercard")]
        Mastercard,
        [EnumMember(Value = "MBNA")]
        MBNA,
        [EnumMember(Value = "Morgan Stanley")]
        MorganStanley,
        [EnumMember(Value = "Nationwide")]
        Nationwide,
        [EnumMember(Value = "Natwest")]
        Natwest,
        [EnumMember(Value = "Northern Rock")]
        NorthernRock,
        [EnumMember(Value = "Portman Building Society")]
        PortmanBuildingSociety,
        [EnumMember(Value = "Royal Bank of Scotland")]
        RoyalBankOfScotland,
        [EnumMember(Value = "Sainsburys Bank Plc")]
        SainsburysBankPlc,
        [EnumMember(Value = "Santander UK Plc")]
        SantanderUKPlc,
        [EnumMember(Value = "Tesco Personal Finance Ltd")]
        TescoPersonalFinanceLtd,
        [EnumMember(Value = "The Post Office")]
        ThePostOffice,
        [EnumMember(Value = "The Woolwich")]
        TheWoolwich,
        [EnumMember(Value = "YorkshireBank PLC")]
        YorkshireBankPLC
    };

    [DataContract]
    //[Serializable]
    public enum VehicleMake
    {
        Unknown = 0,
        [EnumMember(Value = "Alfa Romeo")]
        AlfaRomeo,
        [EnumMember(Value = "Aston Martin")]
        AstonMartin,
        [EnumMember]
        Audi,
        [EnumMember]
        Austin,
        [EnumMember]
        Bentley,
        [EnumMember]
        BMW,
        [EnumMember]
        Cadillac,
        [EnumMember]
        Carbodies,
        [EnumMember]
        Chevrolet,
        [EnumMember]
        Chrysler,
        [EnumMember]
        Citroen,
        [EnumMember]
        Daewoo,
        [EnumMember]
        DAF,
        [EnumMember]
        Daihatsu,
        [EnumMember]
        Daimler,
        [EnumMember]
        Dennis,
        [EnumMember]
        Dinli,
        [EnumMember]
        Dodge,
        [EnumMember]
        ERF,
        [EnumMember]
        Ferrari,
        [EnumMember]
        Fiat,
        [EnumMember]
        Ford,
        [EnumMember(Value = "Grand Jeep")]
        GrandJeep,
        [EnumMember]
        Honda,
        [EnumMember]
        Hummer,
        [EnumMember]
        Hyundai,
        [EnumMember]
        Isuzu,
        [EnumMember]
        Iveco,
        [EnumMember]
        Jaguar,
        [EnumMember]
        Jeep,
        [EnumMember]
        Kia,
        [EnumMember]
        Lamborghini,
        [EnumMember]
        Lancia,
        [EnumMember(Value = "Land Rover")]
        LandRover,
        [EnumMember]
        Landcruiser,
        [EnumMember]
        LDV,
        [EnumMember]
        Lexus,
        [EnumMember]
        Ligier,
        [EnumMember]
        Lincoln,
        [EnumMember(Value = "London Taxi")]
        LondonTaxi,
        [EnumMember]
        Lotus,
        [EnumMember]
        Man,
        [EnumMember]
        Mazda,
        [EnumMember(Value = "Mercedes Benz")]
        MercedesBenz,
        [EnumMember]
        Metrocab,
        [EnumMember]
        MG,
        [EnumMember]
        Mini,
        [EnumMember]
        Mitsubishi,
        [EnumMember]
        Morris,
        [EnumMember]
        Nissan,
        [EnumMember]
        Peugeot,
        [EnumMember]
        Piaggio,
        [EnumMember]
        Porsche,
        [EnumMember]
        Proton,
        [EnumMember(Value = "Range Rover")]
        RangeRover,
        [EnumMember]
        Renault,
        [EnumMember(Value = "Rolls Royce")]
        RollsRoyce,
        [EnumMember]
        Rover,
        [EnumMember]
        Saab,
        [EnumMember]
        Scania,
        [EnumMember]
        Seat,
        [EnumMember]
        Skoda,
        [EnumMember]
        Smart,
        [EnumMember]
        SSangyong,
        [EnumMember]
        Subaru,
        [EnumMember]
        Suzuki,
        [EnumMember]
        Toyota,
        [EnumMember]
        Vauxhall,
        [EnumMember]
        Volkswagen,
        [EnumMember]
        Volvo,
        [EnumMember]
        Yamaha
    };

    [DataContract]
    //[Serializable]
    public enum Nationality
    {
        Unknown = 0,
        [EnumMember]
        Afghanistan,
        [EnumMember]
        Albania,
        [EnumMember]
        Algeria,
        [EnumMember]
        Andorra,
        [EnumMember]
        Angola,
        [EnumMember]
        Anguilla,
        [EnumMember]
        Antigua,
        [EnumMember]
        Antilles,
        [EnumMember]
        Argentina,
        [EnumMember]
        Armenia,
        [EnumMember]
        Aruba,
        [EnumMember(Value = "Ascension Island")]
        AscensionIsland,
        [EnumMember]
        Australia,
        [EnumMember]
        Austria,
        [EnumMember]
        Azerbaijan,
        [EnumMember]
        Azores,
        [EnumMember]
        Bahamas,
        [EnumMember]
        Bahrain,
        [EnumMember]
        Balarus,
        [EnumMember]
        Bangladesh,
        [EnumMember]
        Barbados,
        [EnumMember]
        Belgium,
        [EnumMember]
        Belize,
        [EnumMember]
        Benin,
        [EnumMember]
        Bermuda,
        [EnumMember]
        Bhutan,
        [EnumMember]
        Bolivia,
        [EnumMember(Value = "Bosnia Hercegovina")]
        BosniaHercegovina,
        [EnumMember]
        Botswana,
        [EnumMember]
        Brazil,
        [EnumMember]
        Brunei,
        [EnumMember]
        Bulgaria,
        [EnumMember(Value = "Burkina Faso")]
        BurkinaFaso,
        [EnumMember]
        Burundi,
        [EnumMember]
        Cambodia,
        [EnumMember]
        Cameroon,
        [EnumMember]
        Canada,
        [EnumMember(Value = "Canary Islands")]
        CanaryIslands,
        [EnumMember(Value = "Cape Verde Islands")]
        CapeVerdeIslands,
        [EnumMember(Value = "Cayman Islands")]
        CaymanIslands,
        [EnumMember(Value = "Central African Republic")]
        CentralAfricanRepublic,
        [EnumMember]
        Chad,
        [EnumMember]
        Chile,
        [EnumMember]
        China,
        [EnumMember(Value = "Christmas Island")]
        ChristmasIsland,
        [EnumMember(Value = "Cocos Islands")]
        CocosIslands,
        [EnumMember]
        Colombia,
        [EnumMember]
        Comoros,
        [EnumMember]
        Congo,
        [EnumMember(Value = "Cook Islands")]
        CookIslands,
        [EnumMember(Value = "Costa Rica")]
        CostaRica,
        [EnumMember(Value = "Coted Ivoire")]
        CotedIvoire,
        [EnumMember]
        Croatia,
        [EnumMember]
        Cuba,
        [EnumMember]
        Cyprus,
        [EnumMember(Value = "Czech Republic")]
        CzechRepublic,
        [EnumMember]
        Denmark,
        [EnumMember]
        Djbouti,
        [EnumMember]
        Dominica,
        [EnumMember]
        Ecuador,
        [EnumMember]
        Egypt,
        [EnumMember]
        ElSalvador,
        [EnumMember]
        England,
        [EnumMember(Value = "Equatorial Guinea")]
        EquatorialGuinea,
        [EnumMember]
        Eritrea,
        [EnumMember]
        Estonia,
        [EnumMember]
        Ethiopia,
        [EnumMember(Value = "Falkland Islands")]
        FalklandIslands,
        [EnumMember(Value = "Faroe Islands")]
        FaroeIslands,
        [EnumMember]
        Fiji,
        [EnumMember]
        Finland,
        [EnumMember]
        France,
        [EnumMember(Value = "French Guiana")]
        FrenchGuiana,
        [EnumMember(Value = "French Polynesia")]
        FrenchPolynesia,
        [EnumMember]
        Gabon,
        [EnumMember]
        Gambia,
        [EnumMember]
        Georgia,
        [EnumMember]
        Germany,
        [EnumMember]
        Ghana,
        [EnumMember]
        Gibraltar,
        [EnumMember(Value = "Great Britain")]
        GreatBritain,
        [EnumMember]
        Greece,
        [EnumMember]
        Greenland,
        [EnumMember]
        Grenada,
        [EnumMember]
        Guadeloupe,
        [EnumMember]
        Guam,
        [EnumMember]
        Guatemala,
        [EnumMember]
        Guinea,
        [EnumMember]
        Guyana,
        [EnumMember]
        Haiti,
        [EnumMember]
        Honduras,
        [EnumMember(Value = "Hong Kong")]
        HongKong,
        [EnumMember]
        Hungary,
        [EnumMember]
        Ibiza,
        [EnumMember]
        Iceland,
        [EnumMember]
        India,
        [EnumMember]
        Indonesia,
        [EnumMember]
        Inmarsat,
        [EnumMember]
        Iran,
        [EnumMember]
        Iraq,
        [EnumMember(Value = "Republic of Ireland")]
        RepublicOfIreland,
        [EnumMember]
        Israel,
        [EnumMember]
        Italy,
        [EnumMember(Value = "Ivory Coast")]
        IvoryCoast,
        [EnumMember]
        Jamaica,
        [EnumMember]
        Japan,
        [EnumMember]
        Jordan,
        [EnumMember]
        Kazakhstan,
        [EnumMember]
        Kenya,
        [EnumMember]
        Kirghizstan,
        [EnumMember]
        Kiribati,
        [EnumMember(Value = "Korea (North)")]
        KoreaNorth,
        [EnumMember(Value = "Korea (South)")]
        KoreaSouth,
        [EnumMember]
        Kosovo,
        [EnumMember]
        Kurdistan,
        [EnumMember]
        Kuwait,
        [EnumMember]
        Laos,
        [EnumMember]
        Latvia,
        [EnumMember]
        Lebanon,
        [EnumMember]
        Lesotho,
        [EnumMember]
        Liberia,
        [EnumMember]
        Libya,
        [EnumMember]
        Liechtenstein,
        [EnumMember]
        Lithuania,
        [EnumMember]
        Luxembourg,
        [EnumMember]
        Macao,
        [EnumMember]
        Macedonia,
        [EnumMember]
        Madagascar,
        [EnumMember]
        Madeira,
        [EnumMember]
        Malawi,
        [EnumMember]
        Malaysia,
        [EnumMember]
        Maldives,
        [EnumMember]
        Mali,
        [EnumMember]
        Malta,
        [EnumMember(Value = "Marshall Islands")]
        MarshallIslands,
        [EnumMember]
        Martinique,
        [EnumMember]
        Mauritania,
        [EnumMember]
        Mauritius,
        [EnumMember]
        Mayotte,
        [EnumMember]
        Mexico,
        [EnumMember]
        Micronesia,
        [EnumMember(Value = "Midway Island")]
        MidwayIsland,
        [EnumMember]
        Minorca,
        [EnumMember]
        Moldova,
        [EnumMember]
        Monaco,
        [EnumMember]
        Mongolia,
        [EnumMember]
        Montserrat,
        [EnumMember]
        Morocco,
        [EnumMember]
        Mozambique,
        [EnumMember]
        Myanmar,
        [EnumMember]
        Namibia,
        [EnumMember]
        Nauru,
        [EnumMember]
        Nepal,
        [EnumMember]
        Netherlands,
        [EnumMember(Value = "New Caledonia")]
        NewCaledonia,
        [EnumMember(Value = "New Zealand")]
        NewZealand,
        [EnumMember]
        Nicaragua,
        [EnumMember]
        Niger,
        [EnumMember]
        Nigeria,
        [EnumMember]
        Niue,
        [EnumMember(Value = "Norfolk Island")]
        NorfolkIsland,
        [EnumMember(Value = "Northern Ireland")]
        NorthernIreland,
        [EnumMember(Value = "Northern Marianas")]
        NorthernMarianas,
        [EnumMember]
        Norway,
        [EnumMember]
        Oman,
        [EnumMember]
        Pakistan,
        [EnumMember]
        Palau,
        [EnumMember]
        Panama,
        [EnumMember(Value = "Papua New Guinea")]
        PapuaNewGuinea,
        [EnumMember]
        Paraguay,
        [EnumMember]
        Peru,
        [EnumMember]
        Philippines,
        [EnumMember(Value = "Pitcairn Island")]
        PitcairnIsland,
        [EnumMember]
        Poland,
        [EnumMember]
        Portugal,
        [EnumMember(Value = "Puerto Rico")]
        PuertoRico,
        [EnumMember]
        Qatar,
        [EnumMember]
        Romania,
        [EnumMember]
        Russia,
        [EnumMember]
        Rwanda,
        [EnumMember]
        Samoa,
        [EnumMember(Value = "San Marino")]
        SanMarino,
        [EnumMember(Value = "Sao Tome Principe")]
        SaoTomePrincipe,
        [EnumMember(Value = "Saudi Arabia")]
        SaudiArabia,
        [EnumMember]
        Scotland,
        [EnumMember]
        Senegal,
        [EnumMember]
        Seychelles,
        [EnumMember(Value = "Sierra Leone")]
        SierraLeone,
        [EnumMember]
        Singapore,
        [EnumMember]
        Slovakia,
        [EnumMember]
        Slovenia,
        [EnumMember(Value = "Solomon Islands")]
        SolomonIslands,
        [EnumMember]
        Somalia,
        [EnumMember(Value = "South Africa")]
        SouthAfrica,
        [EnumMember]
        Spain,
        [EnumMember(Value = "Sri Lanka")]
        SriLanka,
        [EnumMember(Value = "St Helena")]
        StHelena,
        [EnumMember(Value = "St Kitts Nevis")]
        StKittsNevis,
        [EnumMember(Value = "St Lucia")]
        StLucia,
        [EnumMember(Value = "St Pierre Miquelon")]
        StPierreMiquelon,
        [EnumMember(Value = "St Vincent Grenadines")]
        StVincentGrenadines,
        [EnumMember]
        Sudan,
        [EnumMember]
        Suriname,
        [EnumMember]
        Swaziland,
        [EnumMember]
        Sweden,
        [EnumMember]
        Switzerland,
        [EnumMember]
        Syria,
        [EnumMember]
        Tadzhikistan,
        [EnumMember]
        Taiwan,
        [EnumMember]
        Tanzania,
        [EnumMember]
        Thailand,
        [EnumMember]
        Togo,
        [EnumMember]
        Tonga,
        [EnumMember(Value = "Trinidad Tobago")]
        TrinidadTobago,
        [EnumMember(Value = "Tristan Da Cunha")]
        TristanDaCunha,
        [EnumMember]
        Tunisia,
        [EnumMember]
        Turkey,
        [EnumMember]
        Turkmenistan,
        [EnumMember(Value = "Turks Caicos Islands")]
        TurksCaicosIslands,
        [EnumMember]
        Tuvalu,
        [EnumMember]
        Uganda,
        [EnumMember]
        Ukraine,
        [EnumMember(Value = "United Arab Emirates")]
        UnitedArabEmirates,
        [EnumMember]
        Uruguay,
        [EnumMember]
        USA,
        [EnumMember]
        Uzbekistan,
        [EnumMember]
        Vanuatu,
        [EnumMember]
        Venezuela,
        [EnumMember]
        Vietnam,
        [EnumMember(Value = "Virgin Islands")]
        VirginIslands,
        [EnumMember(Value = "Wake Island")]
        WakeIsland,
        [EnumMember]
        Wales,
        [EnumMember(Value = "Wallis Futuna")]
        WallisFutuna,
        [EnumMember]
        Yemen,
        [EnumMember]
        Yugoslavia,
        [EnumMember]
        Zaire,
        [EnumMember]
        Zambia,
        [EnumMember]
        Zimbabwe
    };
#endif
   
}