﻿using MDA.Common;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    [DataContract]
    [Serializable]
    public class PipelineWebSite : PipeEntityBase, IPipelineEntity
    {
        public PipelineWebSite()
            : base()
        {
            O2Ws_LinkData = new PipeLinkBase();
        }

        [DataMember(Name = "wsId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string WebSiteId { get; set; }

        [DataMember(Name = "url")]
        public string URL { get; set; }

        [DataMember(Name = "O2Ws")]
        [Browsable(false)] 
        public PipeLinkBase O2Ws_LinkData { get; set; }

        public override string DisplayText
        {
            get { return "URL[" + URL + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Organisation-2-Website ", O2Ws_LinkData));

            return node;
        }
    }
}