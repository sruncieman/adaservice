﻿using System.Runtime.Serialization;

namespace MDA.Pipeline.Model
{
    using MDA.Common;
    using System;
    using System.ComponentModel;

    [DataContract]
    [Serializable]
    public class PipelineClaimInfo : PipeEntityBase, IPipelineEntity
    {
        public PipelineClaimInfo() : base()
        {
            ClaimStatus_Id = (int)MDA.Common.Enum.ClaimStatus.Unknown;
        }

        [DataMember(Name = "csId")]
        [Description("0=Unknown, 1=Open, 2=Settled, 3=Withdrawn, 4=Reopened, 5=Delete")]
        public int ClaimStatus_Id { get; set; }

        [DataMember(Name = "cc")]
        public string ClaimCode { get; set; }

        [DataMember(Name = "il")]
        public string IncidentLocation { get; set; }

        [DataMember(Name = "ic")]
        public string IncidentCircumstances { get; set; }

        [DataMember(Name = "pa")]
        public bool? PoliceAttended { get; set; }

        [DataMember(Name = "pf")]
        public string PoliceForce { get; set; }

        [DataMember(Name = "pr")]
        public string PoliceReference { get; set; }

        [DataMember(Name = "aa")]
        public bool? AmbulanceAttended { get; set; }

        [DataMember(Name = "cnd")]
        public DateTime? ClaimNotificationDate { get; set; }

        [DataMember(Name = "ptd")]
        public decimal? PaymentsToDate { get; set; }

        [DataMember(Name = "res")]
        public decimal? Reserve { get; set; }

        [DataMember(Name = "scs")]
        public string SourceClaimStatus { get; set; }

        [DataMember(Name = "tcc")]
        public decimal? TotalClaimCost { get; set; }

        [DataMember(Name = "tle")]
        public decimal? TotalClaimCostLessExcess { get; set; }

        [DataMember(Name = "bf")]
        public bool? BypassFraud { get; set; }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            return new PipelineNodeDetails("MotorClaimInfo:", this);
        }

    }
}
