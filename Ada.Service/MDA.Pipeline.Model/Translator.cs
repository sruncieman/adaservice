﻿using System;

namespace MDA.Pipeline.Model
{
    public class Translator
    {
        private DateTime? ConvertToSqlSmallDateTime(DateTime? dateTime)
        {
            if (dateTime == null) return null;

            DateTime t = dateTime.Value;

            if (t.Year < 1900 || t.Year > 2078) return null;

            try
            {
                return new DateTime(t.Year, t.Month, t.Day, t.Hour, t.Minute, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
        }

        private DateTime? ConvertToSqlSmallDate(DateTime? dateTime)
        {
            if (dateTime == null) return null;

            DateTime t = dateTime.Value;

            if (t.Year < 1900 || t.Year > 2078) return null;

            try
            {
                return new DateTime(t.Year, t.Month, t.Day, t.Hour, t.Minute, 0).Date;
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
        }

        private bool AddressEmpty(MDA.Common.FileModel.Address address)
        {
            return (
                     string.IsNullOrEmpty(address.Building) &&
                     string.IsNullOrEmpty(address.BuildingNumber) &&
                     string.IsNullOrEmpty(address.County) &&
                     string.IsNullOrEmpty(address.Locality) &&
                     string.IsNullOrEmpty(address.PostCode) &&
                     string.IsNullOrEmpty(address.Street) &&
                     string.IsNullOrEmpty(address.SubBuilding) &&
                     string.IsNullOrEmpty(address.Town)
                   );
        }

        private PipelineAddress Translate(MDA.Common.FileModel.Address address)
        {
            if (address == null) return null;

            PipelineAddress r = new PipelineAddress()
            {
                //AddressLinkType_Id  = (int)address.AddressLinkType,
                Building            = address.Building,
                BuildingNumber      = address.BuildingNumber,
                County              = address.County,
                //EndOfResidency      = address.EndOfResidency,
                Locality            = address.Locality,
                Operation           = (int)address.Operation,
                //PafValidation       = address.PAFAddress,
                PostCode            = address.PostCode,
                //StartOfResidency    = address.StartOfResidency,
                Street              = address.Street,
                SubBuilding         = address.SubBuilding,
                Town                = address.Town,
                 
            };

            r.PO2A_LinkData.AddressLinkType_Id = (int)address.AddressLinkType;
            r.PO2A_LinkData.EndOfResidency     = address.EndOfResidency;
            r.PO2A_LinkData.StartOfResidency   = address.StartOfResidency;

            r.V2A_LinkData.AddressLinkType_Id = (int)Common.Enum.AddressLinkType.StorageAddress;
            r.V2A_LinkData.EndOfResidency     = address.EndOfResidency;
            r.V2A_LinkData.StartOfResidency   = address.StartOfResidency;

            return r;
        }

        private PipelineBankAccount Translate(MDA.Common.FileModel.BankAccount account)
        {
            if (account == null) return null;

            var r = new PipelineBankAccount()
            {
                AccountNumber              = account.AccountNumber,
                BankName                   = account.BankName,
                //DatePaymentDetailsTaken    = account.DatePaymentDetailsTaken,
                Operation                  = (int)account.Operation,
                //PolicyPaymentType_Id       = (int)account.PolicyPaymentType,
                SortCode                   = account.SortCode,
       
            };

            r.Po2Ba_LinkData.DatePaymentDetailsTaken = account.DatePaymentDetailsTaken;
            r.Po2Ba_LinkData.PolicyPaymentType_Id = (int)account.PolicyPaymentType;

            return r;
        }

        private PipelineTelephone Translate(MDA.Common.FileModel.Telephone telephone)
        {
            if (telephone == null) return null;

            return new PipelineTelephone()
            {
                AreaCode             = telephone.AreaCode,
                ClientSuppliedNumber = telephone.ClientSuppliedNumber,
                FullNumber           = telephone.FullNumber,
                InternationalCode    = telephone.InternationalCode,
                TelephoneType_Id     = (int)telephone.TelephoneType,
                Operation            = (int)telephone.Operation
            };
        }

        private PipelinePaymentCard Translate(MDA.Common.FileModel.PaymentCard card)
        {
            if (card == null) return null;

            var r = new PipelinePaymentCard()
            {
                PaymentCardNumber       = card.PaymentCardNumber,
                BankName                = card.BankName,
                //DatePaymentDetailsTaken = card.DatePaymentDetailsTaken,
                Operation               = (int)card.Operation,
                //PolicyPaymentType_Id    = (int)card.PolicyPaymentType,
                SortCode                = card.SortCode,
                ExpiryDate              = card.ExpiryDate,
                PaymentCardType_Id      = (int)card.PaymentCardType
            };

            r.Po2Pc_LinkData.DatePaymentDetailsTaken = card.DatePaymentDetailsTaken;
            r.Po2Pc_LinkData.PolicyPaymentType_Id    = (int)card.PolicyPaymentType;

            return r;
        }

        private PipelineClaimInfo Translate(MDA.Common.FileModel.ClaimInfo info)
        {
            if (info == null) return null;

            return new PipelineClaimInfo()
            {
                AmbulanceAttended        = info.AmbulanceAttended,
                ClaimCode                = info.ClaimCode,
                ClaimNotificationDate    = info.ClaimNotificationDate,
                ClaimStatus_Id           = (int)info.ClaimStatus,
                IncidentCircumstances    = info.IncidentCircumstances,
                IncidentLocation         = info.IncidentLocation,
                PaymentsToDate           = info.PaymentsToDate,
                PoliceAttended           = info.PoliceAttended,
                PoliceForce              = info.PoliceForce,
                PoliceReference          = info.PoliceReference,
                Reserve                  = info.Reserve,
                SourceClaimStatus        = info.SourceClaimStatus,
                TotalClaimCost           = info.TotalClaimCost,
                TotalClaimCostLessExcess = info.TotalClaimCostLessExcess,
                BypassFraud              = info.BypassFraud

            };
        }

        private PipelineOrganisation Translate(MDA.Common.FileModel.Organisation org)
        {
            if (org == null) return null;

            PipelineOrganisation o = new PipelineOrganisation()
            {
                MojCrmNumber                   = org.MojCrmNumber,
                Operation                      = (int)org.Operation,
                MojCrmStatus_Id                = (int)org.MojCrmStatus,
                OrganisationName               = org.OrganisationName,
                OrganisationType_Id            = (int)org.OrganisationType,
                //Person2OrganisationLinkType_Id = (int)org.Person2OrganisationLinkType,
                RegisteredNumber               = org.RegisteredNumber,
                VatNumber                      = org.VatNumber              
            };

            o.P2O_LinkData.Person2OrganisationLinkType_Id = (int)org.Person2OrganisationLinkType;

            if (org.BankAccount != null)
                o.BankAccounts.Add(Translate(org.BankAccount));

            if (org.PaymentCard != null)
                o.PaymentCards.Add(Translate(org.PaymentCard));

            if (!string.IsNullOrEmpty(org.Email))
                o.EmailAddresses.Add(new MDA.Pipeline.Model.PipelineEmail() { EmailAddress = org.Email });

            if (!string.IsNullOrEmpty(org.WebSite))
                o.WebSites.Add(new MDA.Pipeline.Model.PipelineWebSite() { URL = org.WebSite });

            if (!string.IsNullOrEmpty(org.Telephone1)) 
                o.Telephones.Add(new MDA.Pipeline.Model.PipelineTelephone() { ClientSuppliedNumber = org.Telephone1, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
            if (!string.IsNullOrEmpty(org.Telephone2)) 
                o.Telephones.Add(new MDA.Pipeline.Model.PipelineTelephone() { ClientSuppliedNumber = org.Telephone2, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
            if (!string.IsNullOrEmpty(org.Telephone3)) 
                o.Telephones.Add(new MDA.Pipeline.Model.PipelineTelephone() { ClientSuppliedNumber = org.Telephone3, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });

            foreach (var a in org.Addresses)
            {
                if( a != null && !AddressEmpty(a))
                    o.Addresses.Add(Translate(a));
            }

            foreach (var v in org.Vehicles)
            {
                if( v != null )
                    o.Vehicles.Add(Translate(v));
            }

            return o;
        }

        private PipelinePerson Translate(MDA.Common.FileModel.Person person)
        {
            if (person == null) return null;

            PipelinePerson p = new PipelinePerson()
            {
                //AttendedHospital     = person.AttendedHospital,
                DateOfBirth          = ConvertToSqlSmallDate(person.DateOfBirth),
                FirstName            = person.FirstName,
                Gender_Id            = (int)person.Gender,
                LastName             = person.LastName,
                MiddleName           = person.MiddleName,
                //MojStatus_Id         = (int)person.MojStatus,
                MRZPassportNumber    = person.MRZPassportNumber,
                Nationality          = person.Nationality,
                Occupation           = person.Occupation,
                Operation            = (int)person.Operation,
                //PartyType_Id         = (int)person.PartyType,
                Salutation_Id        = (int)person.Salutation,
                //SubPartyType_Id      = (int)person.SubPartyType,
            };

            p.I2Pe_LinkData.AttendedHospital = person.AttendedHospital;
            p.I2Pe_LinkData.MojStatus_Id = (int)person.MojStatus;
            p.I2Pe_LinkData.PartyType_Id = (int)person.PartyType;
            p.I2Pe_LinkData.SubPartyType_Id = (int)person.SubPartyType;

            if (person.BankAccount != null)
                p.BankAccounts.Add(Translate(person.BankAccount));
            if (person.PaymentCard != null)
                p.PaymentCards.Add(Translate(person.PaymentCard));

            if (!string.IsNullOrEmpty(person.EmailAddress)) 
                p.EmailAddresses.Add(new MDA.Pipeline.Model.PipelineEmail() { EmailAddress = person.EmailAddress });

            if (!string.IsNullOrEmpty(person.LandlineTelephone)) 
                p.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = person.LandlineTelephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
            if (!string.IsNullOrEmpty(person.MobileTelephone)) 
                p.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = person.MobileTelephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
            if (!string.IsNullOrEmpty(person.WorkTelephone)) 
                p.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = person.WorkTelephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
            if (!string.IsNullOrEmpty(person.OtherTelephone)) 
                p.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = person.OtherTelephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });

            if (!string.IsNullOrEmpty(person.PassportNumber)) 
                p.PassportNumbers.Add(new PipelinePassport() { PassportNumber = person.PassportNumber });
            if (!string.IsNullOrEmpty(person.NINumber)) 
                p.NINumbers.Add(new PipelineNINumber() { NINumber1 = person.NINumber });
            if (!string.IsNullOrEmpty(person.DrivingLicenseNumber)) 
                p.DrivingLicenseNumbers.Add(new PipelineDrivingLicense() { DriverNumber = person.DrivingLicenseNumber });

            foreach (var a in person.Addresses)
            {
                if (a != null && !AddressEmpty(a))
                    p.Addresses.Add(Translate(a));
            }

            foreach (var o in person.Organisations)
            {
                if (o != null)
                    p.Organisations.Add(Translate(o));
            }
                

            return p;
        }

        private PipelinePolicy Translate(MDA.Common.FileModel.Policy policy)
        {
            if (policy == null) return null;

            return new PipelinePolicy()
            {
                Broker                              = policy.Broker,
                PolicyCoverType_Id                  = (int)policy.CoverType,
                Insurer                             = policy.Insurer,
                Operation                           = (int)policy.Operation,
                PolicyEndDate                       = ConvertToSqlSmallDateTime(policy.PolicyEndDate),
                PolicyNumber                        = policy.PolicyNumber,
                PolicyStartDate                     = ConvertToSqlSmallDateTime(policy.PolicyStartDate),
                PolicyType_Id                       = (int)policy.PolicyType,
                Premium                             = policy.Premium,
                PreviousFaultClaimsCount            = policy.PreviousFaultClaimsCount,
                PreviousNoFaultClaimsCount          = policy.PreviousNoFaultClaimsCount,
                InsurerTradingName                  = policy.TradingName,
            };
        }

        private PipelineVehicle Translate(MDA.Common.FileModel.Vehicle vehicle)
        {
            if (vehicle == null) return null;

            PipelineVehicle v = new PipelineVehicle()
            {
                //VehicleCategoryOfLoss_Id           = (int)vehicle.CategoryOfLoss,
                VehicleColour_Id                   = (int)vehicle.Colour,
                //DamageDescription                  = vehicle.DamageDescription,
                EngineCapacity                     = vehicle.EngineCapacity,
                VehicleFuel_Id                     = (int)vehicle.Fuel,
                //Incident2VehicleLinkType_Id        = (int)vehicle.Incident2VehicleLinkType,
                VehicleMake                        = vehicle.Make,
                VehicleModel                       = vehicle.Model,
                Operation                          = (int)vehicle.Operation,
                VehicleTransmission_Id             = (int)vehicle.Transmission,
                VehicleRegistration                = vehicle.VehicleRegistration,
                VehicleType_Id                     = (int)vehicle.VehicleType,
                VIN                                = vehicle.VIN
            };

            v.I2V_LinkData.Incident2VehicleLinkType_Id = (int)vehicle.Incident2VehicleLinkType;
            v.I2V_LinkData.VehicleCategoryOfLoss_Id = (int)vehicle.CategoryOfLoss;
            v.I2V_LinkData.DamageDescription = vehicle.DamageDescription;

            foreach (var p in vehicle.People)
            {
                if ( p != null )
                    v.People.Add(Translate(p));
            }
            return v;
        }

        private PipelineHandset Translate(MDA.Common.FileModel.Handset handset)
        {
            if (handset == null) return null;

            PipelineHandset h = new PipelineHandset()
            {
                HandsetIMEI = handset.IMEI,
                HandsetMake = handset.Make,
                HandsetModel = handset.Model,
                HandsetValue = handset.Value,
                HandsetValueCategory = handset.ValueCategory,
                HandsetColour_Id = (int)handset.Colour
            };

           
            h.I2H_LinkData.Incident2HandsetLinkType_Id = (int)handset.Incident2HandsetLinkType;
            h.I2H_LinkData.DeviceFault = handset.DeviceFault;
            h.I2H_LinkData.HandsetValueCategory = handset.ValueCategory;
           

            foreach (var p in handset.People)
            {
                if (p != null)
                    h.People.Add(Translate(p));
            }
            return h;
        }

        private PipelineOrganisationVehicle Translate(MDA.Common.FileModel.OrganisationVehicle vehicle)
        {
            if (vehicle == null) return null;

            PipelineOrganisationVehicle v = new PipelineOrganisationVehicle()
            {
                VehicleColour_Id            = (int)vehicle.Colour,
                DamageDescription           = string.Empty, //vehicle.DamageDescription,
                EngineCapacity              = vehicle.EngineCapacity,
                VehicleFuel_Id              = (int)vehicle.Fuel,
                //Incident2VehicleLinkType_Id = (int)vehicle.Incident2VehicleLinkType,
                VehicleMake                 = vehicle.Make,
                VehicleModel                = vehicle.Model,
                Operation                   = (int)vehicle.Operation,
                VehicleTransmission_Id      = (int)vehicle.Transmission,
                VehicleRegistration         = vehicle.VehicleRegistration,
                VehicleType_Id              = (int)vehicle.VehicleType,
                VIN                         = vehicle.VIN,
                //HireEndDate                 = vehicle.HireEndDate,
                //HireStartDate               = vehicle.HireStartDate,
            };

            v.V2O_LinkData.HireEndDate = vehicle.HireEndDate;
            v.V2O_LinkData.HireStartDate = vehicle.HireStartDate;

            v.I2V_LinkData.Incident2VehicleLinkType_Id = (int)vehicle.Incident2VehicleLinkType;

            return v;
        }

        public IPipelineClaim Translate(MDA.Common.FileModel.MotorClaim claim)
        {
            if (claim == null) return null;

            PipelineMotorClaim c = new PipelineMotorClaim()
            {
                ClaimNumber         = claim.ClaimNumber,
                ClaimType_Id        = (int)claim.ClaimType,
                ExternalServices    = claim.ExternalServices,
                IncidentDate        = claim.IncidentDate,
                OriginalIncidentDateTime = claim.IncidentDate,
                ExtraClaimInfo      = Translate(claim.MotorClaimInfo),
                Operation           = (int)claim.Operation,
                Policy              = Translate(claim.Policy)
            };

            foreach (var o in claim.Organisations)
            {
                if ( o != null )
                    c.Organisations.Add(Translate(o));
            }

            foreach (var v in claim.Vehicles)
            {
                if ( v != null )
                    c.Vehicles.Add(Translate(v));
            }

            return c;
        }

        public IPipelineClaim Translate(MDA.Common.FileModel.PIClaim claim)
        {
            if (claim == null) return null;

            PipelinePIClaim c = new PipelinePIClaim()
            {
                ClaimNumber              = claim.ClaimNumber,
                ClaimType_Id             = (int)claim.ClaimType,
                ExternalServices         = claim.ExternalServices,
                IncidentDate             = claim.IncidentDate,
                OriginalIncidentDateTime = claim.IncidentDate,
                ExtraClaimInfo           = Translate(claim.PIClaimInfo),
                Operation                = (int)claim.Operation,
                Policy                   = Translate(claim.Policy),
                Person                   = Translate(claim.Person)
            };

            foreach (var o in claim.Organisations)
            {
                if (o != null)
                    c.Organisations.Add(Translate(o));
            }

            return c;
        }

        public IPipelineClaim Translate(Common.FileModel.MobileClaim claim)
        {
            if (claim == null) return null;

            PipelineMobileClaim c = new PipelineMobileClaim()
            {
                ClaimNumber = claim.ClaimNumber,
                ClaimType_Id = (int)claim.ClaimType,
                ExternalServices = claim.ExternalServices,
                IncidentDate = claim.IncidentDate,
                OriginalIncidentDateTime = claim.IncidentDate,
                ExtraClaimInfo = Translate(claim.MobileClaimInfo),
                Operation = (int)claim.Operation,
                Policy = Translate(claim.Policy)
              
            };

            foreach (var h in claim.Handsets)
            {
                if (h != null)
                    c.Handsets.Add(Translate(h));
            }

            foreach (var o in claim.Organisations)
            {
                if (o != null)
                    c.Organisations.Add(Translate(o));
            }

            return c;
        }

       

        public PipelineClaimBatch Translate(MDA.Common.FileModel.MotorClaimBatch batch)
        {
            if (batch == null) return null;

            PipelineClaimBatch b = new PipelineClaimBatch();

            foreach (var c in batch.Claims)
            {
                b.Claims.Add(Translate(c));
            }

            return b;
        }

        public PipelineClaimBatch Translate(MDA.Common.FileModel.PIClaimBatch batch)
        {
            if (batch == null) return null;

            PipelineClaimBatch b = new PipelineClaimBatch();

            foreach (var c in batch.Claims)
            {
                b.Claims.Add(Translate(c));
            }

            return b;
        }

       
    }
}
