﻿using MDA.Common;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    [DataContract]
    [Serializable]
    public class PipelinePolicy : PipeEntityBase, IPipelineEntity
    {
        public PipelinePolicy()
            : base()
        {
            PolicyType_Id = (int)MDA.Common.Enum.PolicyType.Unknown;
            PolicyCoverType_Id = (int)MDA.Common.Enum.PolicyCoverType.Unknown;
            Operation = (int)MDA.Common.Enum.Operation.Normal;

            I2Po_LinkData = new PipelineIncident2PolicyLink();
        }

        [DataMember(Name = "ptId")]
        [Description("0=Unknown, 1=Personal Motor, 2=Commercial Motor, 3=Home Contents, 4=Home Building, 5=Employers Liability")]
        public int PolicyType_Id { get; set; }

        [DataMember(Name = "pn")]
        public string PolicyNumber { get; set; }

        [DataMember(Name = "itn")]
        public string InsurerTradingName { get; set; }

        [DataMember(Name = "ins")]
        public string Insurer { get; set; }

        [DataMember(Name = "brk")]
        public string Broker { get; set; }

        [DataMember(Name = "pctId")]
        [Description("0=Unknown, 1=Comprehensive, 2=Third Party, Fire and Theft, 3=Third Party Only")]
        public int PolicyCoverType_Id { get; set; }

        [DataMember(Name = "psd")]
        public DateTime? PolicyStartDate { get; set; }

        [DataMember(Name = "ped")]
        public DateTime? PolicyEndDate { get; set; }

        [DataMember(Name = "nfcc")]
        public int? PreviousNoFaultClaimsCount { get; set; }

        [DataMember(Name = "pfcc")]
        public int? PreviousFaultClaimsCount { get; set; }

        [DataMember(Name = "prm")]
        public decimal? Premium { get; set; }

        [DataMember(Name = "pId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string PolicyId { get; set; }

        [DataMember(Name = "I2Po")]
        [Browsable(false)]
        public PipelineIncident2PolicyLink I2Po_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "POL: [" + PolicyNumber + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Incident-2-Policy: ", I2Po_LinkData));

            return node;
        }
    }
}
