﻿using MDA.Common;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    public class PipelinePaymentCard : PipeEntityBase, IPipelineEntity
    {
        public PipelinePaymentCard() : base()
        {
            PaymentCardType_Id = (int)MDA.Common.Enum.PaymentCardType.Unknown;

            PO2Pc_LinkData = new PipeLinkBase();
            Po2Pc_LinkData = new PipelinePolicy2PaymentCardLink();
        }


        [DataMember(Name = "pcId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string PaymentCardId { get; set; }

        [DataMember(Name = "sc")]
        public string SortCode { get; set; }

        [DataMember(Name = "bn")]
        public string BankName { get; set; }
        [DataMember(Name = "ed")]
        public string ExpiryDate { get; set; }

        [DataMember(Name = "pctId")]
        [Description("0=Unknown, 1=Credit Card, 2=Debit Card")]
        public int PaymentCardType_Id { get; set; }

        [DataMember(Name = "cn")]
        public string PaymentCardNumber { get; set; }

        [DataMember(Name = "PO2Pc")]
        [Browsable(false)] 
        public PipeLinkBase PO2Pc_LinkData { get; set; }

        [DataMember(Name = "Po2Pc")]
        [Browsable(false)] 
        public PipelinePolicy2PaymentCardLink Po2Pc_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "BN[" + BankName + "] SORT[" + SortCode + "] CNUM[" + PaymentCardNumber + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Policy-2-PaymentCard: " , Po2Pc_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Person/Org-2-PaymentCard: " , PO2Pc_LinkData));

            return node;
        }
    }
}