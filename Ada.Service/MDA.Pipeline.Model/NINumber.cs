﻿using MDA.Common;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    [DataContract]
    [Serializable]
    public class PipelineNINumber : PipeEntityBase, IPipelineEntity
    {
        public PipelineNINumber() : base()
        {
            Pe2Ni_LinkData = new PipeLinkBase();
        }

        [DataMember(Name = "niId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string NINumberId { get; set; }

        [DataMember(Name = "ni")]
        public string NINumber1 { get; set; }

        [DataMember]
        [Browsable(false)] 
        public PipeLinkBase Pe2Ni_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "NI[" + NINumber1 + "]"; }
        }
        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Person-2-NiNumber: ", Pe2Ni_LinkData));

            return node;
        }
    }
}