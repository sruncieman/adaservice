﻿using MDA.Common;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    public class PipelineAddress : PipeEntityBase, IPipelineEntity
    {
        public PipelineAddress() : base()
        {
            PO2A_LinkData = new PipelineAddressLink() { AddressLinkType_Id = (int)MDA.Common.Enum.AddressLinkType.CurrentAddress };
            V2A_LinkData = new PipelineAddressLink() { AddressLinkType_Id = (int)MDA.Common.Enum.AddressLinkType.StorageAddress };
            A2A_LinkData = new PipelineAddressLink() { AddressLinkType_Id = (int)MDA.Common.Enum.AddressLinkType.LinkedAddress };
        }

        [DataMember(Name="sb")]
        public string SubBuilding { get; set; }

        [DataMember(Name = "b")]
        public string Building { get; set; }

        [DataMember(Name = "bn")]
        public string BuildingNumber { get; set; }

        [DataMember(Name = "st")]
        public string Street { get; set; }

        [DataMember(Name = "lo")]
        public string Locality { get; set; }

        [DataMember(Name = "tn")]
        public string Town { get; set; }

        [DataMember(Name = "cn")]
        public string County { get; set; }  

        [DataMember(Name="pc")]
        public string PostCode { get; set; }

        [DataMember(Name="aid")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string AddressId { get; set; }

        [DataMember(Name="dxn")]
        public string DxNumber { get; set; }

        [DataMember(Name="dxe")]
        public string DxExchange { get; set; }

        [DataMember(Name="gx")]
        public string GridX { get; set; }

        [DataMember(Name="gy")]
        public string GridY { get; set; }

        [DataMember(Name="pv")]
        [XmlIgnore]
        public int PafValidation { get; set; }

        [DataMember(Name="pu")]
        [XmlIgnore]
        public string PafUPRN { get; set; }

        [DataMember(Name="dl")]
        public string DocumentLink { get; set; }

        [DataMember(Name="pt")]
        public string PropertyType { get; set; }

        [DataMember(Name="mc")]
        public string MosaicCode { get; set; }

        [DataMember(Name="atId")]
        [Description("0=Unknown 1=Residential, 2=Commercial")]
        public int AddressType_Id { get; set; }

        [DataMember(Name="po2a")]
        [Browsable(false)] 
        public PipelineAddressLink PO2A_LinkData { get; set; }   // Person and Org link to address

        [DataMember(Name="v2a")]
        [Browsable(false)] 
        public PipelineAddressLink V2A_LinkData { get; set; } // Storage Address info

        [DataMember(Name = "h2a")]
        [Browsable(false)]
        public PipelineAddressLink H2A_LinkData { get; set; } // Storage Address info


        [DataMember(Name="a2a")]
        [Browsable(false)] 
        public PipelineAddressLink A2A_LinkData { get; set; }

        [Browsable(false)] 
        public override string DisplayText
        {
            get { return "UPRN[" + PafUPRN + "] BN[" + BuildingNumber + "] ST[" + Street + "] PC[" + PostCode + "] TYPE[" + ((MDA.Common.Enum.AddressLinkType)PO2A_LinkData.AddressLinkType_Id).ToString() + "]"; }
        }

        /// <summary>
        /// A property used in the Matching Handlers in the ClaimService to decide if THIS entity should be discarded (along with it's children)
        /// In this case we do not want to insert addresses that are empty. Addresses do not have children
        /// </summary>
        [Browsable(false)]
        public override bool ShouldBeDiscarded
        {
            get
            {
                return string.IsNullOrEmpty(this.Building) &&
                       string.IsNullOrEmpty(this.BuildingNumber) &&
                       string.IsNullOrEmpty(this.County) &&
                       string.IsNullOrEmpty(this.Locality) &&
                       string.IsNullOrEmpty(this.PostCode) &&
                       string.IsNullOrEmpty(this.Street) &&
                       string.IsNullOrEmpty(this.SubBuilding) &&
                       string.IsNullOrEmpty(this.Town);
            }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Person/Org-2-Address: ", PO2A_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Vehicle-2-Address: ", V2A_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Address-2-Address: ", A2A_LinkData));

            return node;
        }

    }

}