﻿using MDA.Common;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    [DataContract]
    [Serializable]
    public class PipelineBankAccount : PipeEntityBase, IPipelineEntity
    {
        public PipelineBankAccount() : base()
        {
            PO2Ba_LinkData = new PipeLinkBase();
            Po2Ba_LinkData = new PipelinePolicy2BankAccountLink();
        }

        [DataMember(Name = "aId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string AccountId { get; set; }

        [DataMember(Name = "an", IsRequired = true)]
        public string AccountNumber { get; set; }
        [DataMember(Name = "sc")]
        public string SortCode { get; set; }
        [DataMember(Name = "bn")]
        public string BankName { get; set; }

        [DataMember(Name = "PO2ba")]
        [Browsable(false)] 
        public PipeLinkBase PO2Ba_LinkData { get; set; }

        [DataMember(Name = "Po2Ba")]
        public PipelinePolicy2BankAccountLink Po2Ba_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "NAME[" + BankName + "] SORT[" + SortCode + "] ACC[" + AccountNumber + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Person/Org-2-Account: ", PO2Ba_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Policy-2-Account: ", Po2Ba_LinkData));

            return node;
        }
    }


}