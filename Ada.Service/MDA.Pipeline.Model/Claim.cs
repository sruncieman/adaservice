﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using MDA.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MDA.Pipeline.Model
{
    [DataContract(Name = "Claim")]
    [Serializable]
    public class PipelineMotorClaim : PipeEntityBase, IPipelineClaim
    {
        public PipelineMotorClaim() : base()
        {
            Vehicles = new List<PipelineVehicle>();
            Organisations = new List<PipelineOrganisation>();
            Policy = new PipelinePolicy();

            ExtraClaimInfo = new PipelineClaimInfo();  //??

            ClaimType_Id = (int)MDA.Common.Enum.ClaimType.Motor;

            IncidentDate = new DateTime();
        }

        [XmlIgnore]
        public int StagingID { get; set; }

        [DataMember(Name = "sb")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string IncidentId { get; set; }

        [DataMember(Name = "kef")]
        [XmlIgnore]
        public string KeoghsEliteReference { get; set; }

        [DataMember(Name="cn", IsRequired = true)]
        public string ClaimNumber { get; set; }

        [DataMember(Name = "itId")]
        [Description("0=Unknown, 1=RTC, 2=Commercial, 3=Employment, 4=Public Liability, 5=Travel, 6=Home Contents, 7=Home Building, 8=Keoghs CFS, 9=Keoghs CMS, 10=Theft, 11=Pet, 12=Mobile Phone")]
        public int IncidentType_Id { get; set; }

        [DataMember(Name="ctId", IsRequired = true)]
        [Description("0=NA, 1=Motor, 2=Commercial, 3=Employment, 4=Public Liability, 5=Travel, 6=Home Contents, 7=Home Building")]
        public int ClaimType_Id { get; set; }

        [DataMember(Name = "ifb")]
        public string IfbReference { get; set; }

        [DataMember(Name="idte", IsRequired = true)]
        public DateTime IncidentDate { get; set; }

        [DataMember(Name = "oidt")]
        public DateTime OriginalIncidentDateTime { get; set; }

        [DataMember(Name = "es")]
        public string ExternalServices { get; set; }

        [DataMember(Name = "frn")]
        [XmlIgnore]
        public string FraudRingName { get; set; }

        [DataMember(Name = "pol")]
        [Browsable(false)] 
        public PipelinePolicy Policy { get; set; }

        [DataMember(Name = "mci")]   //??
        [Browsable(false)] 
        public PipelineClaimInfo ExtraClaimInfo { get; set; }  //??

        [DataMember(Name="vehs", IsRequired=true)]
        [Browsable(false)] 
        public List<PipelineVehicle> Vehicles { get; set; }

        [DataMember(Name = "orgs")]
        [Browsable(false)] 
        public List<PipelineOrganisation> Organisations { get; set; }

       // [DataMember(Name = "CR")] DO NOT SERIALISE THIS
        [Browsable(false)]
        [XmlIgnore]
        public ProcessingResults CleanseResults { get; set; }

        // [DataMember(Name = "VR")] DO NOT SERIALISE THIS
        [Browsable(false)]
        [XmlIgnore]
        public ProcessingResults ValidationResults { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "CN[" + ClaimNumber + "]"; } 
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            if (ExtraClaimInfo != null)
            {
                var n = new PipelineNodeDetails("ExtraClaimInfo", null);
                n.Nodes.Add(ExtraClaimInfo.ToPipelineNodeDetails());
                node.Nodes.Add(n);
            }

            if (Policy != null)
            {
                var n = new PipelineNodeDetails("Policy", null);
                n.Nodes.Add(Policy.ToPipelineNodeDetails());
                node.Nodes.Add(n);
            }

            if (Vehicles != null && Vehicles.Count > 0)
            {
                var n = new PipelineNodeDetails("Vehicles", null);
                foreach (var v in Vehicles)
                {
                    n.Nodes.Add(v.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if ( Organisations != null && Organisations.Count > 0)
            {
                var n = new PipelineNodeDetails("Organisations", null);
                foreach (var o in Organisations)
                {
                    n.Nodes.Add(o.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            return node;
        }
    }


    [DataContract(Name = "Claim")]
    [Serializable]
    public class PipelinePIClaim : PipeEntityBase, IPipelineClaim
    {
        public PipelinePIClaim()
            : base()
        {
            Policy = new PipelinePolicy();

            ExtraClaimInfo = new PipelineClaimInfo();  //??

            ClaimType_Id = (int)MDA.Common.Enum.ClaimType.NA;

            IncidentDate = DateTime.Now;
        }

        [XmlIgnore]
        public int StagingID { get; set; }

        [DataMember(Name = "sb")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string IncidentId { get; set; }

        [DataMember(Name = "kef")]
        [XmlIgnore]
        public string KeoghsEliteReference { get; set; }

        [DataMember(Name = "cn", IsRequired = true)]
        public string ClaimNumber { get; set; }

        [DataMember(Name = "itId")]
        [Description("0=Unknown, 1=RTC, 2=Commercial, 3=Employment, 4=Public Liability, 5=Travel, 6=Home Contents, 7=Home Building, 8=Keoghs CFS, 9=Keoghs CMS, 10=Theft, 11=Pet, 12=Mobile Phone")]
        public int IncidentType_Id { get; set; }

        [DataMember(Name = "ctId", IsRequired = true)]
        [Description("0=NA, 1=Motor, 2=Commercial, 3=Employment, 4=Public Liability, 5=Travel, 6=Home Contents, 7=Home Building")]
        public int ClaimType_Id { get; set; }

        [DataMember(Name = "ifb")]
        public string IfbReference { get; set; }

        [DataMember(Name = "idte", IsRequired = true)]
        public DateTime IncidentDate { get; set; }

        [DataMember(Name = "oidt")]
        public DateTime OriginalIncidentDateTime { get; set; }

        [DataMember(Name = "es")]
        public string ExternalServices { get; set; }

        [DataMember(Name = "frn")]
        [XmlIgnore]
        public string FraudRingName { get; set; }

        [DataMember(Name = "pol")]
        [Browsable(false)]
        public PipelinePolicy Policy { get; set; }

        [DataMember(Name = "mci")]   //??
        [Browsable(false)]
        public PipelineClaimInfo ExtraClaimInfo { get; set; }  //??

        [DataMember(Name = "person", IsRequired = true)]
        [Browsable(false)]
        public PipelinePerson Person { get; set; }

        [DataMember(Name = "orgs")]
        [Browsable(false)]
        public List<PipelineOrganisation> Organisations { get; set; }

        // [DataMember(Name = "CR")] DO NOT SERIALISE THIS
        [Browsable(false)]
        [XmlIgnore]
        public ProcessingResults CleanseResults { get; set; }

        // [DataMember(Name = "VR")] DO NOT SERIALISE THIS
        [Browsable(false)]
        [XmlIgnore]
        public ProcessingResults ValidationResults { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "CN[" + ClaimNumber + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            if (ExtraClaimInfo != null)
            {
                var n = new PipelineNodeDetails("ExtraClaimInfo", null);
                n.Nodes.Add(ExtraClaimInfo.ToPipelineNodeDetails());
                node.Nodes.Add(n);
            }

            if (Policy != null)
            {
                var n = new PipelineNodeDetails("Policy", null);
                n.Nodes.Add(Policy.ToPipelineNodeDetails());
                node.Nodes.Add(n);
            }

            if (Organisations != null && Organisations.Count > 0)
            {
                var n = new PipelineNodeDetails("Organisations", null);
                foreach (var o in Organisations)
                {
                    n.Nodes.Add(o.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            return node;
        }
    }


    [DataContract(Name = "Claim")]
    [Serializable]
    public class PipelineMobileClaim : PipeEntityBase, IPipelineClaim
    {
        public PipelineMobileClaim()
            : base()
        {
            Handsets = new List<PipelineHandset>();
            Organisations = new List<PipelineOrganisation>();
            Policy = new PipelinePolicy();

            ExtraClaimInfo = new PipelineClaimInfo();  //??

            ClaimType_Id = (int)MDA.Common.Enum.ClaimType.MobilePhone;
            IncidentDate = DateTime.Now;
        }
        [XmlIgnore]
        public int StagingID { get; set; }

        [DataMember(Name = "sb")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string IncidentId { get; set; }

        [DataMember(Name = "kef")]
        [XmlIgnore]
        public string KeoghsEliteReference { get; set; }

        [DataMember(Name = "cn", IsRequired = true)]
        public string ClaimNumber { get; set; }

        [DataMember(Name = "itId")]
        [Description("0=Unknown, 1=RTC, 2=Commercial, 3=Employment, 4=Public Liability, 5=Travel, 6=Home Contents, 7=Home Building, 8=Keoghs CFS, 9=Keoghs CMS, 10=Theft, 11=Pet, 12=Mobile Phone")]
        public int IncidentType_Id { get; set; }

        [DataMember(Name = "ctId", IsRequired = true)]
        [Description("0=NA, 1=Motor, 2=Commercial, 3=Employment, 4=Public Liability, 5=Travel, 6=Home Contents, 7=Home Building")]
        public int ClaimType_Id { get; set; }

        [DataMember(Name = "ifb")]
        public string IfbReference { get; set; }

        [DataMember(Name = "idte", IsRequired = true)]
        public DateTime IncidentDate { get; set; }

        [DataMember(Name = "oidt")]
        public DateTime OriginalIncidentDateTime { get; set; }

        [DataMember(Name = "es")]
        public string ExternalServices { get; set; }

        [DataMember(Name = "frn")]
        [XmlIgnore]
        public string FraudRingName { get; set; }

        [DataMember(Name = "pol")]
        [Browsable(false)]
        public PipelinePolicy Policy { get; set; }

        [DataMember(Name = "mci")]   //??
        [Browsable(false)]
        public PipelineClaimInfo ExtraClaimInfo { get; set; }  //??

        [DataMember(Name = "hans", IsRequired = true)]
        [Browsable(false)]
        public List<PipelineHandset> Handsets { get; set; }

        [DataMember(Name = "person", IsRequired = true)]
        [Browsable(false)]
        public PipelinePerson Person { get; set; }

        [DataMember(Name = "orgs")]
        [Browsable(false)]
        public List<PipelineOrganisation> Organisations { get; set; }

        // [DataMember(Name = "CR")] DO NOT SERIALISE THIS
        [Browsable(false)]
        [XmlIgnore]
        public ProcessingResults CleanseResults { get; set; }

        // [DataMember(Name = "VR")] DO NOT SERIALISE THIS
        [Browsable(false)]
        [XmlIgnore]
        public ProcessingResults ValidationResults { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "CN[" + ClaimNumber + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            if (ExtraClaimInfo != null)
            {
                var n = new PipelineNodeDetails("ExtraClaimInfo", null);
                n.Nodes.Add(ExtraClaimInfo.ToPipelineNodeDetails());
                node.Nodes.Add(n);
            }

            if (Policy != null)
            {
                var n = new PipelineNodeDetails("Policy", null);
                n.Nodes.Add(Policy.ToPipelineNodeDetails());
                node.Nodes.Add(n);
            }

            if (Handsets != null && Handsets.Count > 0)
            {
                var n = new PipelineNodeDetails("Mobiles", null);
                foreach (var h in Handsets)
                {
                    n.Nodes.Add(h.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (Organisations != null && Organisations.Count > 0)
            {
                var n = new PipelineNodeDetails("Organisations", null);
                foreach (var o in Organisations)
                {
                    n.Nodes.Add(o.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            return node;
        }
    }
    
}
