﻿using MDA.Common;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    [DataContract]
    [Serializable]
    public class PipelineIPAddress : PipeEntityBase, IPipelineEntity
    {
        public PipelineIPAddress() : base()
        {
            PO2Ip_LinkData = new PipeLinkBase();
        }

        [DataMember(Name="iaId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string IPAddressId { get; set; }

        [DataMember(Name = "ia")]
        public string IPAddress1 { get; set; }

        [DataMember(Name = "PO2Ip")]
        [Browsable(false)] 
        public PipeLinkBase PO2Ip_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "IP[" + IPAddress1 + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Person/Org-2-IpAddress: ", PO2Ip_LinkData));

            return node;
        }
    }
}