﻿using MDA.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    [DataContract]
    [Serializable]
    public class PipelinePerson : PipeEntityBase, IPipelineEntity
    {
        public PipelinePerson() : base()
        {
            Addresses = new List<PipelineAddress>();
            Organisations = new List<PipelineOrganisation>();
            EmailAddresses = new List<PipelineEmail>();
            NINumbers = new List<PipelineNINumber>();
            PassportNumbers = new List<PipelinePassport>();
            DrivingLicenseNumbers = new List<PipelineDrivingLicense>();
            BankAccounts = new List<PipelineBankAccount>();
            PaymentCards = new List<PipelinePaymentCard>();
            Telephones = new List<PipelineTelephone>();

            //PartyType_Id = (int)MDA.Common.Enum.PartyType.Unknown;
            //SubPartyType_Id = (int)MDA.Common.Enum.SubPartyType.Unknown;
            Salutation_Id = (int)MDA.Common.Enum.Salutation.Unknown;
            Gender_Id = (int)MDA.Common.Enum.Gender.Unknown;
            //MojStatus_Id = (int)MDA.Common.Enum.MojStatus.Unknown;

            I2Pe_LinkData = new PipelineIncident2PersonLink();
            V2Pe_LinkData = new PipelineVehicle2PersonLink();
            H2Pe_LinkData = new PipelineHandset2PersonLink();
            Pe2Po_LinkData = new PipelinePolicyLink();
            P2P_LinkData = new PipelinePerson2PersonLink();
        }

        //[DataMember]
        //public int PartyType_Id { get; set; }

        //[DataMember]
        //public int SubPartyType_Id { get; set; }

        [DataMember(Name = "pId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string PersonId { get; set; }

        [DataMember(Name = "sal")]
        [Description("0=Unknown, 1=Mr, 2=Mrs, 3=Ms, 4=Miss, 5=Master, 6=Dr, 7=Professor, 8=Reverend, 9=Sir, 10=Lord, 11=Baroness, 12=Right Honourable")]
        public int Salutation_Id { get; set; }

        [DataMember(Name = "fn")]
        public string FirstName { get; set; }

        [DataMember(Name = "mn")]
        public string MiddleName { get; set; }

        [DataMember(Name = "ln")]
        public string LastName { get; set; }

        [DataMember(Name = "dob")]
        public DateTime? DateOfBirth { get; set; }

        [DataMember(Name = "sd")]
        public DateTime? SanctionDate { get; set; }

        [DataMember(Name = "gen")]
        [Description("0=Unknown, 1=Male, 2=Female")]
        public int Gender_Id { get; set; }

        [DataMember(Name = "nat")]
        public string Nationality { get; set; }

        [DataMember(Name = "doc")]
        public string Documents { get; set; }
        [DataMember(Name = "hob")]
        public string Hobbies { get; set; }
        [DataMember(Name = "note")]
        public string Notes { get; set; }
        [DataMember(Name = "ss")]
        public string SanctionSource { get; set; }
        [DataMember(Name = "sch")]
        public string Schools { get; set; }
        [DataMember(Name = "tdl")]
        public string TaxiDriverLicense { get; set; }


        [DataMember(Name = "ni")]
        [Browsable(false)] 
        public List<PipelineNINumber> NINumbers { get; set; }

        [DataMember(Name = "dln")]
        [Browsable(false)] 
        public List<PipelineDrivingLicense> DrivingLicenseNumbers { get; set; }

        [DataMember(Name = "pn")]
        [Browsable(false)] 
        public List<PipelinePassport> PassportNumbers { get; set; }

        [DataMember(Name = "mrz")]
        public string MRZPassportNumber { get; set; }

        [DataMember(Name = "occ")]
        public string Occupation { get; set; }

        [DataMember(Name = "ea")]
        [Browsable(false)] 
        public List<PipelineEmail> EmailAddresses{ get; set; }

        [DataMember(Name = "tel")]
        [Browsable(false)] 
        public List<PipelineTelephone> Telephones { get; set; }

        //[DataMember]
        //public bool? AttendedHospital { get; set; }

        //[DataMember]
        //public int MojStatus_Id { get; set; }

        [DataMember(Name = "addr")]
        [Browsable(false)] 
        public List<PipelineAddress> Addresses { get; set; }

        [DataMember(Name = "ba")]
        [Browsable(false)] 
        public List<PipelineBankAccount> BankAccounts { get; set; }

        [DataMember(Name = "pc")]
        [Browsable(false)] 
        public List<PipelinePaymentCard> PaymentCards { get; set; }

        [DataMember(Name = "org")]
        [Browsable(false)] 
        public List<PipelineOrganisation> Organisations { get; set; }

        [DataMember(Name = "I2Pe")]
        [Browsable(false)] 
        public PipelineIncident2PersonLink I2Pe_LinkData { get; set; }

        [DataMember(Name = "V2Pe")]
        [Browsable(false)] 
        public PipelineVehicle2PersonLink V2Pe_LinkData { get; set; }

        [DataMember(Name = "H2Pe")]
        [Browsable(false)]
        public PipelineHandset2PersonLink H2Pe_LinkData { get; set; }
        
        [DataMember(Name = "Pe2Po")]
        [Browsable(false)] 
        public PipelinePolicyLink Pe2Po_LinkData { get; set; }

        [DataMember(Name = "P2P")]
        [Browsable(false)] 
        public PipelinePerson2PersonLink P2P_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "FN[" + FirstName + "] MN[" + MiddleName + "] LN [" + LastName + "] Policy[" + ((MDA.Common.Enum.PolicyLinkType)Pe2Po_LinkData.PolicyLinkType_Id).ToString() + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails("Person: " + DisplayText, this);

            node.Nodes.Add(I2Pe_LinkData.ToPipelineNodeDetails());
            node.Nodes.Add(new PipelineNodeDetails("Vehicle-2-Person", V2Pe_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Person-2-Policy", Pe2Po_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Person-2-Person", P2P_LinkData));

            if (Addresses != null && Addresses.Count > 0)
            {
                var n = new PipelineNodeDetails("Addresses", null);
                foreach (var a in Addresses)
                {
                    n.Nodes.Add(a.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (BankAccounts != null && BankAccounts.Count > 0)
            {
                var n = new PipelineNodeDetails("Bank Accounts", null);
                foreach (var ba in BankAccounts)
                {
                    n.Nodes.Add(ba.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (DrivingLicenseNumbers != null && DrivingLicenseNumbers.Count > 0)
            {
                var n = new PipelineNodeDetails("Driving License Numbers", null);
                foreach (var dl in DrivingLicenseNumbers)
                {
                    n.Nodes.Add(dl.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (EmailAddresses != null && EmailAddresses.Count > 0)
            {
                var n = new PipelineNodeDetails("Email Addresses", null);
                foreach (var ea in EmailAddresses)
                {
                    n.Nodes.Add(ea.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (NINumbers != null && NINumbers.Count > 0)
            {
                var n = new PipelineNodeDetails("NI Numbers", null);
                foreach (var ni in NINumbers)
                {
                    node.Nodes.Add(ni.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (PassportNumbers != null && PassportNumbers.Count > 0)
            {
                var n = new PipelineNodeDetails("PassportNumbers", null);
                foreach (var pn in PassportNumbers)
                {
                    n.Nodes.Add(pn.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (PaymentCards != null && PaymentCards.Count > 0)
            {
                var n = new PipelineNodeDetails("Payment Cards", null);
                foreach (var pc in PaymentCards)
                {
                    node.Nodes.Add(pc.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (Telephones != null && Telephones.Count > 0)
            {
                var n = new PipelineNodeDetails("Telephones", null);
                foreach (var tn in Telephones)
                {
                    n.Nodes.Add(tn.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (Organisations != null && Organisations.Count > 0)
            {
                var n = new PipelineNodeDetails("Organisations", null);
                foreach (var o in Organisations)
                {
                    n.Nodes.Add(o.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            return node;
        }
    }
}