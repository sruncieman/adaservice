﻿using MDA.Common;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    [DataContract]
    [Serializable]
    public class PipelineEmail : PipeEntityBase, IPipelineEntity
    {
        public PipelineEmail() : base()
        {
            PO2E_LinkData = new PipelineEmailLink();
        }

 
        [DataMember(Name = "eId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string EmailId { get; set; }

        [DataMember(Name = "ea")]
        public string EmailAddress { get; set; }

        [DataMember(Name = "PO2E")]
        [Browsable(false)] 
        public PipelineEmailLink PO2E_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "Email[" + EmailAddress + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Person/Org-2-Email: ", PO2E_LinkData));

            return node;
        }
    }
}