﻿using MDA.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{

    [DataContract]
    [Serializable]
    public class PipelineVehicle : PipeEntityBase, IPipelineEntity
    {
        public PipelineVehicle() : base()
        {
            People = new List<PipelinePerson>();

            VehicleType_Id           = (int)MDA.Common.Enum.VehicleType.Car;
            VehicleColour_Id         = (int)MDA.Common.Enum.VehicleColour.Unknown;
            VehicleType_Id           = (int)MDA.Common.Enum.VehicleType.Unknown;
            VehicleFuel_Id           = (int)MDA.Common.Enum.VehicleFuelType.Unknown;
            VehicleTransmission_Id   = (int)MDA.Common.Enum.VehicleTransmission.Unknown;
            //VehicleCategoryOfLoss_Id = (int)MDA.Common.Enum.VehicleCategoryOfLoss.Unknown;

            I2V_LinkData = new PipelineIncident2VehicleLink();
            V2Po_LinkData = new PipelineVehicle2PolicyLink();
            V2A_LinkData = new PipelineVehicle2AddressLink();
            V2V_LinkData = new PipelineVehicle2VehicleLink();
        }

        [DataMember(Name = "vtId", IsRequired = true)]
        [Description("0=Unknown, 1=Bicycle, 2=Car, 3=Coach, 4=Lorry, 5=Minibus, 6=MotorCycle, 7=Pickup, 8=Van, 9=Taxi")]
        public int VehicleType_Id { get; set; }

        [DataMember(Name = "vId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string VehicleId { get; set; }

        [DataMember(Name = "vr")]
        public string VehicleRegistration { get; set; }

        [DataMember(Name = "vc")]
        [Description("0=Unknown, 1=Beige, 2=Black, 3=Blue, 4=Bronze, 5=Brown, 6=Cream, 7=Gold, 8=Graphite, 9=Green, 10=Grey, 11=Lilac, 12=Maroon, 13=Mauve, 14=Orange, 15=Pink, 16=Purple, 17=Red, 18=Silver, 19=Turquoise, 20=White, 21=Yellow")]
        public int VehicleColour_Id { get; set; }

        [DataMember(Name = "vmk")]
        public string VehicleMake { get; set; }

        [DataMember(Name = "vm")]
        public string VehicleModel { get; set; }

        [DataMember(Name = "note")]
        public string Notes { get; set; }

        [DataMember(Name = "ec")]
        public string EngineCapacity { get; set; }

        [DataMember(Name = "vin")]
        public string VIN { get; set; }

        [DataMember(Name = "vf")]
        [Description("0=Unknown, 1=Petrol, 2=Diesel, 3=LPG")]
        public int VehicleFuel_Id { get; set; }

        [DataMember(Name = "vt")]
        [Description("0=Unknown, 1=Manual, 2=Automatic")]
        public int VehicleTransmission_Id { get; set; }

        [DataMember(Name = "dd")]
        public string DamageDescription { get; set; }

        [DataMember(Name = "peo",IsRequired = true)]
        [Browsable(false)] 
        public List<PipelinePerson> People { get; set; }

        [DataMember(Name = "I2V")]
        [Browsable(false)] 
        public PipelineIncident2VehicleLink I2V_LinkData { get; set; }

        [DataMember(Name = "V2Po")]
        [Browsable(false)] 
        public PipelineVehicle2PolicyLink V2Po_LinkData { get; set; }

        [DataMember(Name = "V2A")]
        [Browsable(false)]
        public PipelineVehicle2AddressLink V2A_LinkData { get; set; }

        [DataMember(Name = "V2V")]
        [Browsable(false)] 
        public PipelineVehicle2VehicleLink V2V_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "REG[" + VehicleRegistration + "] ILINK[" + ((MDA.Common.Enum.Incident2VehicleLinkType)I2V_LinkData.Incident2VehicleLinkType_Id).ToString() + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails("Vehicle: " + DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Incident-2-Vehicle", I2V_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Vehicle-2-Policy", V2Po_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Vehicle-2-Address", V2A_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Vehicle-2-Vehicle", V2V_LinkData));

            if (People != null && People.Count > 0)
            {
                var n = new PipelineNodeDetails("People", null);
                foreach (var p in People)
                {
                    n.Nodes.Add(p.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            return node;
        }
    }

    [DataContract]
    [Serializable]
    public class PipelineOrganisationVehicle : PipeEntityBase 
    {
        public PipelineOrganisationVehicle() : base()
        {
            VehicleType_Id           = (int)MDA.Common.Enum.VehicleType.Car;
            VehicleColour_Id         = (int)MDA.Common.Enum.VehicleColour.Unknown;
            VehicleType_Id           = (int)MDA.Common.Enum.VehicleType.Unknown;
            VehicleFuel_Id           = (int)MDA.Common.Enum.VehicleFuelType.Unknown;
            VehicleTransmission_Id   = (int)MDA.Common.Enum.VehicleTransmission.Unknown;

            I2V_LinkData   = new PipelineIncident2VehicleLink();
            V2Po_LinkData  = new PipelineVehicle2PolicyLink();
            V2O_LinkData   = new PipelineOrgVehicleLink();
            V2Pe_LinkData  = null;
            V2V_LinkData = new PipelineVehicle2VehicleLink();

        }

        [DataMember(Name = "vtId",IsRequired = true)]
        [Description("0=Unknown, 1=Bicycle, 2=Car, 3=Coach, 4=Lorry, 5=Minibus, 6=MotorCycle, 7=Pickup, 8=Van, 9=Taxi")]
        public int VehicleType_Id { get; set; }

        [DataMember(Name = "vId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string VehicleId { get; set; }

        [DataMember(Name = "vr")]
        public string VehicleRegistration { get; set; }

        [DataMember(Name = "vc")]
        [Description("0=Unknown, 1=Beige, 2=Black, 3=Blue, 4=Bronze, 5=Brown, 6=Cream, 7=Gold, 8=Graphite, 9=Green, 10=Grey, 11=Lilac, 12=Maroon, 13=Mauve, 14=Orange, 15=Pink, 16=Purple, 17=Red, 18=Silver, 19=Turquoise, 20=White, 21=Yellow")]
        public int VehicleColour_Id { get; set; }

        [DataMember(Name = "vmk")]
        public string VehicleMake { get; set; }

        [DataMember(Name = "vm")]
        public string VehicleModel { get; set; }

        [DataMember(Name = "ec")]
        public string EngineCapacity { get; set; }

        [DataMember(Name = "vin")]
        public string VIN { get; set; }

        [DataMember(Name = "vf")]
        [Description("0=Unknown, 1=Petrol, 2=Diesel, 3=LPG")]
        public int VehicleFuel_Id { get; set; }

        [DataMember(Name = "vt")]
        [Description("0=Unknown, 1=Manual, 2=Automatic")]
        public int VehicleTransmission_Id { get; set; }

        [DataMember(Name = "dd")]
        public string DamageDescription { get; set; }

        [DataMember(Name = "I2V")]
        [Browsable(false)] 
        public PipelineIncident2VehicleLink I2V_LinkData { get; set; }

        [DataMember(Name = "V2Po")]
        [Browsable(false)] 
        public PipelineVehicle2PolicyLink V2Po_LinkData { get; set; }

        [DataMember(Name = "V2O")]
        [Browsable(false)] 
        public PipelineOrgVehicleLink V2O_LinkData { get; set; }

        [DataMember(Name = "V2Pe")]
        [Browsable(false)] 
        public PipelineVehicle2PersonLink V2Pe_LinkData { get; set; }

        [DataMember(Name = "V2V")]
        [Browsable(false)] 
        public PipelineVehicle2VehicleLink V2V_LinkData { get; set; }

        [Browsable(false)] 
        public override string DisplayText
        {
            get { return "REG[" + VehicleRegistration + "] ILINK[" + ((MDA.Common.Enum.VehicleLinkType)I2V_LinkData.Incident2VehicleLinkType_Id).ToString() + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails("Vehicle: " + DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Incident-2-oVehicle", I2V_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("oVehicle-2-Policy", V2Po_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("oVehicle-2-Organisation", V2O_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("oVehicle-2-Person", V2Pe_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("oVehicle-2-Vehicle", V2V_LinkData));

            return node;
        }
    }
}

