﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{

    [DataContract]
    [Serializable]
    public class PipeEntityBase
    {
        public PipeEntityBase()
        {
            Operation = (int)MDA.Common.Enum.Operation.Normal;
        }

        [DataMember(Name = "ka")]
        [XmlIgnore]
        //[Browsable(false)] 
        public string KeyAttractor { get; set; }
        [DataMember(Name = "src")]
        [XmlIgnore]
        //[Browsable(false)] 
        public string Source { get; set; }
        [DataMember(Name = "sref")]
        [XmlIgnore]
        //[Browsable(false)] 
        public string SourceReference { get; set; }
        [DataMember(Name = "sdes")]
        [XmlIgnore]
        //[Browsable(false)] 
        public string SourceDescription { get; set; }
        [DataMember(Name = "crb")]
        [XmlIgnore]
        //[Browsable(false)] 
        public string CreatedBy { get; set; }
        [DataMember(Name = "crd")]
        [XmlIgnore]
        //[Browsable(false)] 
        public DateTime CreatedDate { get; set; }
        [DataMember(Name = "mob")]
        [XmlIgnore]
        //[Browsable(false)] 
        public string ModifiedBy { get; set; }

        [DataMember(Name = "mod")]
        [XmlIgnore]
        //[Browsable(false)] 
        public DateTime? ModifiedDate { get; set; }

        [DataMember(Name = "ibId")]
        [XmlIgnore]
        [Browsable(false)] 
        [Description("IBASE 8 Id")]
        public string IBaseId { get; set; }

        [DataMember(Name = "rstat")]
        [XmlIgnore]
        [Browsable(false)]
        [Description("IBASE Record Status 0=Normal, 254=Deleted")]
        public byte RecordStatus { get; set; }

        [DataMember(Name = "arstat")]
        [XmlIgnore]
        //[Browsable(false)] 
        [Description("0=Current, 1=Client Deleted, 2=Client Withdrawn, 3=Update Removed, 4=Analyst Deleted, 5=Dedupe Deleted")]
        public byte ADARecordStatus { get; set; }

        [DataMember(Name = "oper")]
        [Description("0=Normal, 1=Delete, 2=Withdrawn")]
        public int Operation { get; set; }

        [Description("User Defined")]
        [Browsable(false)] 
        [XmlIgnore]
        public object Tag { get; set; }

        [Browsable(false)]
        [XmlIgnore]
        public virtual string DisplayText
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// A property used in the Matching Handlers in the ClaimService to decide if THIS entity should be discarded and skipped (along with it's children)
        /// The default, if not overriden, is FALSE meaning that ALL entities should be saved in ADA database. Override in derived class if you want to
        /// discard some kinds of entities if they are empty (or for some other reason)
        /// </summary>
        [Browsable(false)]
        [XmlIgnore]
        public virtual bool ShouldBeDiscarded
        {
            get { return false; }
        }
    }

    public class PipeLinkBase : PipeEntityBase
    {
        [DataMember(Name = "lcf")]
        [XmlIgnore]
        [Description("-1=Unset, 0=Confirmed, 1=Unconfirmed, 2=Tentative")]
        public int LinkConfidence { get; set; }

        [DataMember(Name = "fgr")]
        [XmlIgnore]
        public int FiveGrading { get; set; }
    }

    public class PipelineTagCreateDummy
    {
        private bool _createDummy;
        public bool CreateDummy 
        {
            get { return _createDummy;  } 
        }
        public PipelineTagCreateDummy()
        {
            _createDummy = true;
        }
    }

}