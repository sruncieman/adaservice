﻿using MDA.Common;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace MDA.Pipeline.Model
{
    public class PipelineAddressLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "eor")]
        public DateTime? EndOfResidency { get; set; }

        [DataMember(Name = "sor")]
        public DateTime? StartOfResidency { get; set; }

        [DataMember(Name = "altId")]
        [Description("0=Unknown, 1=Previous Address, 2=Linked Address, 3=Current Address, 4=Registered Office, 5=Trading Address, 6=Storage Address")]
        public int AddressLinkType_Id { get; set; }

        [DataMember(Name = "ra")]
        public string RawAddress { get; set; }

        public PipelineAddressLink()
        {
            AddressLinkType_Id = (int)MDA.Common.Enum.AddressLinkType.CurrentAddress;
        }
    }

    public class PipelinePolicy2BankAccountLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "pptId")]
        [Description("0=Unknown, 1=Deposit, 2=Direct Debit, 3=Single Payment")]
        public int PolicyPaymentType_Id { get; set; }
        [DataMember(Name = "dpdt")]
        public DateTime? DatePaymentDetailsTaken { get; set; }

        public PipelinePolicy2BankAccountLink()
        {
            PolicyPaymentType_Id = (int)MDA.Common.Enum.PolicyPaymentType.Unknown;
        }
    }

    public class PipelinePolicy2PaymentCardLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "pptId")]
        [Description("0=Unknown, 1=Deposit, 2=Direct Debit, 3=Single Payment")]
        public int PolicyPaymentType_Id { get; set; }
        [DataMember(Name = "dpdt")]
        public DateTime? DatePaymentDetailsTaken { get; set; }

        public PipelinePolicy2PaymentCardLink()
        {
            PolicyPaymentType_Id = (int)MDA.Common.Enum.PolicyPaymentType.Unknown;
        }
    }

    public class PipelineIncident2VehicleLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "i2vltId")]
        [Description("0=Unknown, 1=Insured Vehicle, 2=Third Party Vehicle, 3=Insured Hire Vehicle, 4=Third Party Hire Vehicl, 5=Witness Vehicl, 6=No Vehicle Involved")]
        public int Incident2VehicleLinkType_Id { get; set; }

        [DataMember(Name = "dd")]
        public string DamageDescription { get; set; }

        [DataMember(Name = "vclId")]
        [Description("0=Unknown, 1=CatA, 2=CatB, 3=CatC, 4=CatD")]
        public int VehicleCategoryOfLoss_Id { get; set; }

        public PipelineIncident2VehicleLink()
        {
            Incident2VehicleLinkType_Id = (int)MDA.Common.Enum.Incident2VehicleLinkType.Unknown;
        }
    }

    public class PipelineIncident2HandsetLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "i2hltId")]
        [Description("0=Unknown, 1=Policy Handset, 2=Insured Handset, 3=Previously Insured Handset, 4=Loan Handset, 5=No Hanset Involved")]
        public int Incident2HandsetLinkType_Id { get; set; }

        [DataMember(Name = "df")]
        public string DeviceFault { get; set; }

        [DataMember(Name = "vc")]
        public string HandsetValueCategory { get; set; }

        public PipelineIncident2HandsetLink()
        {
            Incident2HandsetLinkType_Id = (int)MDA.Common.Enum.Incident2HandsetLinkType.Unknown;
        }
    }


    public class PipelineOrganisation2OrganisationLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "oltId")]
        [Description("0=Unknown, 1=Director, 2=Secretary, 3=Previous Director, 4=Previous Secretary, 5=Formerly Known As, 6=Trading Name Of, 7=Also Known As")]
        public int OrganisationLinkType_Id { get; set; }

        public PipelineOrganisation2OrganisationLink()
        {
            OrganisationLinkType_Id = (int)MDA.Common.Enum.OrganisationLinkType.AlsoKnownAs;
        }
    }

    public class PipelineOrgVehicleLink : PipeLinkBase, IPipelineEntity // pV2O in Org, V2O in Vehicle
    {
        [DataMember(Name = "vltId")]
        [Description("0=Unknown, 1=Driver, 2=Examined By, 3=Hired From, 4=Hirer, 5=Inspected By, 6=Insured, 7=Owner, 8=Passenger, 9=Previous Keeper, 10=Recovered By, 11=Recovered To, 12=Registered Keeper, 13=Repairer, 14=Stored At, 15=Witness")]
        public int VehicleLinkType_Id { get; set; }

        [DataMember(Name = "hsd")]
        public DateTime? HireStartDate { get; set; }

        [DataMember(Name = "hed")]
        public DateTime? HireEndDate { get; set; }

        [DataMember(Name = "hc")]
        public string HireCompany { get; set; }

        public PipelineOrgVehicleLink()
        {
        }
    }

    public class PipelineOrgHandsetLink : PipeLinkBase, IPipelineEntity // pH2O in Org, H2O in Handset
    {
        [DataMember(Name = "hltId")]
        [Description("0=Unknown")]
        public int HandsetLinkType_Id { get; set; }

        public PipelineOrgHandsetLink()
        {
        }
    }

    public class PipelineVehicle2PersonLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "vltId")]
        [Description("0=Unknown, 1=Driver, 2=Examined By, 3=Hired From, 4=Hirer, 5=Inspected By, 6=Insured, 7=Owner, 8=Passenger, 9=Previous Keeper, 10=Recovered By, 11=Recovered To, 12=Registered Keeper, 13=Repairer, 14=Stored At, 15=Witness")]
        public int VehicleLinkType_Id { get; set; }

        [DataMember(Name = "hsd")]
        public DateTime? HireStartDate { get; set; }

        [DataMember(Name = "hed")]
        public DateTime? HireEndDate { get; set; }

        [DataMember(Name = "hc")]
        public string HireCompany { get; set; }

        public PipelineVehicle2PersonLink()
        {
        }
    }

    public class PipelineHandset2PersonLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "hltId")]
        [Description("0=Unknown")]
        public int HandsetLinkType_Id { get; set; }
        //[DataMember(Name = "vltId")]
        //[Description("0=Unknown")]
        //public int VehicleLinkType_Id { get; set; }

        //[DataMember(Name = "hsd")]
        //public DateTime? HireStartDate { get; set; }

        //[DataMember(Name = "hed")]
        //public DateTime? HireEndDate { get; set; }

        //[DataMember(Name = "hc")]
        //public string HireCompany { get; set; }

        public PipelineHandset2PersonLink()
        {
        }
    }


    //public class PipelineOrgVehicleLink : PipeLinkBase, IPipelineEntity
    //{
    //    [DataMember]
    //    [Description("0=Unknown, 1=Driver, 2=Examined By, 3=Hired From, 4=Hirer, 5=Inspected By, 6=Insured, 7=Owner, 8=Passenger, 9=Previous Keeper, 10=Recovered By, 11=Recovered To, 12=Registered Keeper, 13=Repairer, 14=Stored At, 15=Witness")]

    //    public int VehicleLinkType_Id { get; set; }

    //    //[DataMember]
    //    //public DateTime? HireStartDate { get; set; }

    //    //[DataMember]
    //    //public DateTime? HireEndDate { get; set; }

    //    //[DataMember]
    //    //public string HireCompany { get; set; }

    //    public PipelineOrgVehicleLink()
    //    {
    //    }
    //}

    //public class PipelineOrgVehicle2HirerPersonLink : PipeLinkBase, IPipelineEntity
    //{
    //    [DataMember]
    //    [Description("0=Unknown, 1=Driver, 2=Examined By, 3=Hired From, 4=Hirer, 5=Inspected By, 6=Insured, 7=Owner, 8=Passenger, 9=Previous Keeper, 10=Recovered By, 11=Recovered To, 12=Registered Keeper, 13=Repairer, 14=Stored At, 15=Witness")]

    //    public int VehicleLinkType_Id { get; set; }

    //    [DataMember]
    //    public string OrganisationName { get; set; }


    //    public PipelineOrgVehicle2HirerPersonLink()
    //    {
    //    }
    //}

    public class PipelineVehicle2PolicyLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "pltId")]
        [Description("0=Unknown, 1=Policyholder, 2=Named Driver, 3=Paid Policy, 4=Insured")]
        public int PolicyLinkType_Id { get; set; }

        public PipelineVehicle2PolicyLink()
        {
            PolicyLinkType_Id = (int)MDA.Common.Enum.PolicyLinkType.Insured;
        }
    }

    public class PipelineHandset2PolicyLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "pltId")]
        [Description("0=Unknown")]
        public int PolicyLinkType_Id { get; set; }

        public PipelineHandset2PolicyLink()
        {
            PolicyLinkType_Id = (int)MDA.Common.Enum.PolicyLinkType.Unknown;
        }
    }

    public class PipelineVehicle2VehicleLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "drc")]
        public DateTime? DateOfRegChange { get; set; }

        public PipelineVehicle2VehicleLink()
        {
        }
    }

    public class PipelineHandset2HandsetLink : PipeLinkBase, IPipelineEntity
    {
        public PipelineHandset2HandsetLink()
        {
        }
    }


    public class PipelineHandset2TelephoneLink : PipeLinkBase, IPipelineEntity
    {
        public PipelineHandset2TelephoneLink()
        {

        }
    }

    public class PipelinePolicyLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "pltId")]
        [Description("0=Unknown, 1=Policyholder, 2=Named Driver, 3=Paid Policy, 4=Insured")]
        public int PolicyLinkType_Id { get; set; }

        public PipelinePolicyLink()
        {
            PolicyLinkType_Id = (int)MDA.Common.Enum.PolicyLinkType.Unknown;
        }
    }

    public class PipelinePerson2PersonLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "p2pltId")]
        [Description("0=Unknown, 1=Also Known As, 2=Associate, 3=Aunt, 4=Brother, 5=Brother In Law, 6=Father, 7=Father In Law, 8=Friend, 9=Interpreter, 10=Mother, 11=Mother In Law, 12=Partner, 13=Sibling, 14=Sister, 15=ister In Law, 16=Spouce, 17=Suspected Identity Match, 18=Uncle")]
        public int Person2PersonLinkType_Id { get; set; }

        public PipelinePerson2PersonLink()
        {
            Person2PersonLinkType_Id = (int)MDA.Common.Enum.Person2PersonLinkType.AlsoKnownAs;
        }
    }

    public class PipelineIncident2PolicyLink : PipeLinkBase, IPipelineEntity
    {
        public PipelineIncident2PolicyLink()
        {
        }
    }

    public class PipelineEmailLink : PipeLinkBase, IPipelineEntity
    {
        public PipelineEmailLink()
        {
        }
    }

    public class PipelineVehicle2AddressLink : PipeLinkBase, IPipelineEntity
    {
        public PipelineVehicle2AddressLink()
        {
        }
    }

    public class PipelineHandset2AddressLink : PipeLinkBase, IPipelineEntity
    {
        public PipelineHandset2AddressLink()
        {
        }
    }


    public class PipelinePerson2OrganisationLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "p2oltId")]
        [Description("0=Unknown, 1=Director, 2=Secretary, 3=Employee, 4=Previous Director, 5=Previous Secretary, 6=Previous Employee, 7=Manager, 8=Owner, 9=Partner, 10=Broker, 11=Referral Source, 12=Solicitor, 13=Engineer, 14=Recovery, 15=Storage, 16=Repairer, 17=Hire, 18=Accident Management, 19=Medical Examiner")]
        public int Person2OrganisationLinkType_Id { get; set; }

        public PipelinePerson2OrganisationLink()
        {
            Person2OrganisationLinkType_Id = (int)MDA.Common.Enum.Person2OrganisationLinkType.Unknown;
        }
    }

    public class PipelineTelephoneLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "tltId")]
        [Description("0=Uses, 1=Registered at, 2=Linked Address")]
        public int TelephoneLinkType_Id { get; set; }

        public PipelineTelephoneLink()
        {
            TelephoneLinkType_Id = (int)MDA.Common.Enum.TelephoneLinkType.Uses;
        }
    }

    public class PipelineIncident2PersonLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "ci")]
        [Browsable(false)] 
        public PipelineClaimInfo ClaimInfo { get; set; }

        [DataMember(Name = "pt")]
        [Description("0=Unknown, 1=Claimant, 2=Hirer, 3=Insured, 4=Litigation Friend, 5=Policyholder, 6=Third Party, 7=Witness, 8=Vehicle Owner, 9=Paid Policy")]
        public int PartyType_Id { get; set; }

        [DataMember(Name = "spt")]
        [Description("0=Unknown, 1=Driver, 2=Passenger, 3=Witness")]
        public int SubPartyType_Id { get; set; }

        [DataMember(Name = "moj")]
        [Description("0=Unknown, 1=Stage1, 2=Stage2, 3=Stage3, 4=Out Of Process")]
        public int MojStatus_Id { get; set; }

        [DataMember(Name = "aa")]
        public bool? AttendedHospital { get; set; }

        [DataMember(Name = "inr")]
        public string Insurer { get; set; }

        [DataMember(Name = "brk")]
        public string Broker { get; set; }

        [DataMember(Name = "cn")]
        public string ClaimNumber { get; set; }

        [DataMember(Name = "it")]
        public TimeSpan? IncidentTime { get; set; }

        public PipelineIncident2PersonLink()
        {
            PartyType_Id = (int)MDA.Common.Enum.PartyType.Unknown;
            SubPartyType_Id = (int)MDA.Common.Enum.SubPartyType.Unknown;
            MojStatus_Id = (int)MDA.Common.Enum.MojStatus.Unknown;

            ClaimInfo = new PipelineClaimInfo();
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails("Incident-2-Person: ", this);

            node.Nodes.Add(new PipelineNodeDetails("ClaimInfo: ", this.ClaimInfo));

            return node;
        }
    }

    public class PipelineIncident2OrganisationLink : PipeLinkBase, IPipelineEntity
    {
        [DataMember(Name = "ci")]
        [Browsable(false)] 
        public PipelineClaimInfo ClaimInfo { get; set; }

        [DataMember(Name = "oId")]
        [Description("0=Unknown, 1=Policy Holder, 2=Undefined Supplier, 3=Accident Management, 4=Credit Hire,  5=Solicitor, 6=Vehicle Engineer, 7=Recovery, 8=Storage, 9=Broker, 10=Medical Examiner, 11=Repairer, 12=Insurer")]
         public int OrganisationType_Id { get; set; }

        [DataMember(Name = "pt")]
        [Description("0=Unknown, 1=Claimant, 2=Hirer, 3=Insured, 4=Litigation Friend, 5=Policyholder, 6=Third Party, 7=Witness, 8=Vehicle Owner, 9=Paid Policy")]
        public int PartyType_Id { get; set; }

        [DataMember(Name = "spt")]
        [Description("0=Unknown, 1=Driver, 2=Passenger, 3=Witness")]
        public int SubPartyType_Id { get; set; }

        [DataMember(Name = "moj")]
        [Description("0=Unknown, 1=Stage1, 2=Stage2, 3=Stage3, 4=Out Of Process")]
        public int MojStatus_Id { get; set; }

        [DataMember(Name = "aa")]
        public bool? AttendedHospital { get; set; }

        [DataMember(Name = "ins")]
        public string Insurer { get; set; }

        [DataMember(Name = "brk")]
        public string Broker { get; set; }

        [DataMember(Name = "cn")]
        public string ClaimNumber { get; set; }

        [DataMember(Name = "it")]
        public TimeSpan? IncidentTime { get; set; }

        public PipelineIncident2OrganisationLink()
        {
            PartyType_Id = (int)MDA.Common.Enum.PartyType.Unknown;
            SubPartyType_Id = (int)MDA.Common.Enum.SubPartyType.Unknown;
            MojStatus_Id = (int)MDA.Common.Enum.MojStatus.Unknown;

            ClaimInfo = new PipelineClaimInfo();
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails("Incident-2-Org: ", this);

            node.Nodes.Add(new PipelineNodeDetails("ClaimInfo: ", this.ClaimInfo));

            return node;
        }
    }

}


