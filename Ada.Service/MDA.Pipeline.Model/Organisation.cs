﻿using MDA.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace MDA.Pipeline.Model
{
    [DataContract]
    [Serializable]
    public class PipelineOrganisation : PipeEntityBase, IPipelineEntity
    {
        public PipelineOrganisation() : base()
        {
            Addresses      = new List<PipelineAddress>();
            Vehicles       = new List<PipelineOrganisationVehicle>();
            Telephones     = new List<PipelineTelephone>();
            BankAccounts   = new List<PipelineBankAccount>();
            PaymentCards   = new List<PipelinePaymentCard>();
            EmailAddresses = new List<PipelineEmail>();
            WebSites       = new List<PipelineWebSite>();

            OrganisationType_Id = (int)MDA.Common.Enum.OrganisationType.Unknown;        
            MojCrmStatus_Id     = (int)MDA.Common.Enum.MojCRMStatus.Unknown;
            OrganisationStatus_Id = (int)MDA.Common.Enum.OrganisationStatus.Unknown;

            P2O_LinkData = new PipelinePerson2OrganisationLink();
            I2O_LinkData = new PipelineIncident2OrganisationLink();
            pV2O_LinkData = new PipelineOrgVehicleLink();
            //oV2O2_LinkData = new PipelineVehicle2OrganisationLink();
            O2Po_LinkData = new PipelinePolicyLink() { PolicyLinkType_Id = (int)Common.Enum.PolicyLinkType.Policyholder };
            O2O_LinkData = new PipelineOrganisation2OrganisationLink() { OrganisationLinkType_Id = (int)MDA.Common.Enum.OrganisationLinkType.AlsoKnownAs };
        }

        [DataMember(Name = "otId")]
        [Description("0=Unknown, 1=Policy Holder, 2=Undefined Supplier, 3=Accident Management, 4=Credit Hire,  5=Solicitor, 6=Vehicle Engineer, 7=Recovery, 8=Storage, 9=Broker, 10=Medical Examiner, 11=Repairer, 12=Insurer")]
        public int OrganisationType_Id { get; set; }

        [DataMember(Name = "on")]
        public string OrganisationName { get; set; }

        [DataMember(Name = "rn")]
        public string RegisteredNumber { get; set; }

        [DataMember(Name = "vat")]
        public string VatNumber { get; set; }

        [DataMember(Name = "mojN")]
        public string MojCrmNumber { get; set; }

        [DataMember(Name = "mojS")]
        [Description("0=Unknown, 1=Authorised, 2=Suspended, 3=Cancelled")]
        public int MojCrmStatus_Id { get; set; }

        [DataMember(Name = "ea")]
        [Browsable(false)] 
        public List<PipelineEmail> EmailAddresses { get; set; }

        [DataMember(Name = "ws")]
        [Browsable(false)] 
        public List<PipelineWebSite> WebSites { get; set; }

        [DataMember(Name = "tel")]
        [Browsable(false)] 
        public List<PipelineTelephone> Telephones { get; set; }

        [DataMember(Name = "oId")]
        [Description("IBASE 5 Id")]
        public string OrganisationId { get; set; }

        [DataMember(Name = "id")]
        public Nullable<System.DateTime> IncorporatedDate { get; set; }

        [DataMember(Name = "osId")]
        [Description("0=Unknown, 1=Active, 2=Dissolved, 3=In Liquidation, 4=Receivership Action")]
        public int OrganisationStatus_Id { get; set; }

        [DataMember(Name = "edos")]
        public DateTime? EffectiveDateOfStatus { get; set; }

        [DataMember(Name = "sic")]
        public string SIC { get; set; }

        [DataMember(Name = "vvd")]
        public System.DateTime? VATNumberValidationDate { get; set; }

        [DataMember(Name = "cl")]
        public string CreditLicense { get; set; }


        [DataMember(Name = "ba")]
        [Browsable(false)] 
        public List<PipelineBankAccount> BankAccounts { get; set; }   // Only populated when Org under Claim (not person)

        [DataMember(Name = "pc")]
        [Browsable(false)] 
        public List<PipelinePaymentCard> PaymentCards { get; set; }   // Only populated when Org under Claim (not person)

        [DataMember(Name = "addr")]
        [Browsable(false)] 
        public List<PipelineAddress> Addresses { get; set; }

        [DataMember(Name = "veh")]
        [Browsable(false)] 
        public List<PipelineOrganisationVehicle> Vehicles { get; set; }

        [DataMember(Name = "han")]
        [Browsable(false)]
        public List<PipelineOrganisationHandset> Handsets { get; set; }

        [DataMember(Name = "P2O")]
        [Browsable(false)]
        public PipelinePerson2OrganisationLink P2O_LinkData { get; set; }
        //[DataMember]
        //public PipeLinkBase O2A_LinkData { get; set; }
        [DataMember(Name = "I2O")]
        [Browsable(false)] 
        public PipelineIncident2OrganisationLink I2O_LinkData { get; set; }
        [DataMember(Name = "pV2O")]
        [Browsable(false)] 
        public PipelineOrgVehicleLink pV2O_LinkData { get; set; }   // Primary vehicle to org when org under person

        [DataMember(Name = "pH2O")]
        [Browsable(false)]
        public PipelineOrgHandsetLink pH2O_LinkData { get; set; }   // Primary handset to org when org under person

        //[DataMember]
        //public PipelineVehicle2OrganisationLink oV2O2_LinkData { get; set; }     
        [DataMember(Name = "o2Po")]
        [Browsable(false)] 
        public PipelinePolicyLink O2Po_LinkData { get; set; }

        [DataMember(Name = "O2O")]
        [Browsable(false)] 
        public PipelineOrganisation2OrganisationLink O2O_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "ONAME[" + OrganisationName + "] OTYPE[" + ((MDA.Common.Enum.OrganisationType)OrganisationType_Id).ToString() + "] LTYPE[" + ((MDA.Common.Enum.OrganisationLinkType)O2O_LinkData.OrganisationLinkType_Id).ToString() + "]"; } 
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails("Org: " + DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Person-2-Org", P2O_LinkData));
            node.Nodes.Add(I2O_LinkData.ToPipelineNodeDetails());
            node.Nodes.Add(new PipelineNodeDetails("orgVehicle-2-Org", pV2O_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Org-2-Policy", O2Po_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Org-2-Org", O2O_LinkData));

            if (Addresses != null && Addresses.Count > 0)
            {
                var n = new PipelineNodeDetails("Addresses", null);
                foreach (var a in Addresses)
                {
                    n.Nodes.Add(a.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (BankAccounts != null && BankAccounts.Count > 0)
            {
                var n = new PipelineNodeDetails("Organisations", null);
                foreach (var ba in BankAccounts)
                {
                    n.Nodes.Add(ba.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (EmailAddresses != null && EmailAddresses.Count > 0)
            {
                var n = new PipelineNodeDetails("Email Addresses", null);
                foreach (var ea in EmailAddresses)
                {
                    n.Nodes.Add(ea.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (PaymentCards != null && PaymentCards.Count > 0)
            {
                var n = new PipelineNodeDetails("Payment Cards", null);
                foreach (var pc in PaymentCards)
                {
                    n.Nodes.Add(pc.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (Telephones != null && Telephones.Count > 0)
            {
                var n = new PipelineNodeDetails("Telephones", null);
                foreach (var tn in Telephones)
                {
                    node.Nodes.Add(tn.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (WebSites != null && WebSites.Count > 0)
            {
                var n = new PipelineNodeDetails("WebSites", null);
                foreach (var ws in WebSites)
                {
                    n.Nodes.Add(ws.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            if (Vehicles != null && Vehicles.Count > 0)
            {
                var n = new PipelineNodeDetails("Vehicles", null);
                foreach (var ov in Vehicles)
                {
                    n.Nodes.Add(ov.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            return node;
        }
    }

}