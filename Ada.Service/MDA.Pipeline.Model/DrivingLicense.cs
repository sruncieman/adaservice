﻿using MDA.Common;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    [DataContract]
    [Serializable]
    public class PipelineDrivingLicense : PipeEntityBase, IPipelineEntity
    {
        public PipelineDrivingLicense() : base()
        {
            Pe2Dl_LinkData = new PipeLinkBase();
        }

        [DataMember(Name = "dlId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string DrivingLicenseId { get; set; }

        [DataMember(Name = "dn")]
        public string DriverNumber { get; set; }

        [DataMember(Name = "Pe2Dl")]
        [Browsable(false)] 
        public PipeLinkBase Pe2Dl_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "DNum[" + DriverNumber + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Person-2-License: ", Pe2Dl_LinkData));

            return node;
        }
    }
}