﻿using MDA.Common;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    [DataContract]
    [Serializable]
    public class PipelinePassport : PipeEntityBase, IPipelineEntity
    {
        public PipelinePassport() : base()
        {
            Pe2Pa_LinkData = new PipeLinkBase();
        }

        [DataMember(Name = "paId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string PassportId { get; set; }

        [DataMember(Name = "pn")]
        public string PassportNumber { get; set; }

        [DataMember(Name = "Pe2Pa")]
        [Browsable(false)] 
        public PipeLinkBase Pe2Pa_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "PassNo[" + PassportNumber + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Person-2-Passport", Pe2Pa_LinkData));

            return node;
        }
    }
}