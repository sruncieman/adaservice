﻿using MDA.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    [DataContract]
    [Serializable]
    public class PipelineHandset : PipeEntityBase, IPipelineEntity
    {
        public PipelineHandset() : base()
        {
            People = new List<PipelinePerson>();

            //HandsetType_Id = (int)MDA.Common.Enum.HandsetType.Unknown;
            HandsetColour_Id = (int)MDA.Common.Enum.HandsetColour.Unknown;

            I2H_LinkData = new PipelineIncident2HandsetLink();
            H2Po_LinkData = new PipelineHandset2PolicyLink();
            H2A_LinkData = new PipelineHandset2AddressLink();
            H2H_LinkData = new PipelineHandset2HandsetLink();
            H2T_LinkData = new PipelineHandset2TelephoneLink();
        }

        [DataMember(Name = "I2H")]
        [Browsable(false)]
        public PipelineIncident2HandsetLink I2H_LinkData { get; set; }

        [DataMember(Name = "H2Po")]
        [Browsable(false)]
        public PipelineHandset2PolicyLink H2Po_LinkData { get; set; }

        [DataMember(Name = "H2A")]
        [Browsable(false)]
        public PipelineHandset2AddressLink H2A_LinkData { get; set; }

        [DataMember(Name = "H2H")]
        [Browsable(false)]
        public PipelineHandset2HandsetLink H2H_LinkData { get; set; }

        [DataMember(Name = "H2T")]
        [Browsable(false)]
        public PipelineHandset2TelephoneLink H2T_LinkData { get; set; }


        [DataMember(Name = "peo", IsRequired = true)]
        [Browsable(false)]
        public List<PipelinePerson> People { get; set; }

        //[DataMember(Name = "htId", IsRequired = true)]
        //[Description("0=Unknown, 1=Insured, 2=Updated, 3=Replacement, 4=Policy")]
        //public int HandsetType_Id { get; set; }


        [DataMember(Name = "imei")]
        public string HandsetIMEI { get; set; }

        [DataMember(Name = "hmke")]
        public string HandsetMake { get; set; }

        [DataMember(Name = "hmod")]
        public string HandsetModel { get; set; }

        [DataMember(Name = "hc")]
        [Description("0=Unknown, 1=Beige, 2=Black, 3=Blue, 4=Bronze, 5=Brown, 6=Cream, 7=Gold, 8=Graphite, 9=Green, 10=Grey, 11=Lilac, 12=Maroon, 13=Mauve, 14=Orange, 15=Pink, 16=Purple, 17=Red, 18=Silver, 19=Turquoise, 20=White, 21=Yellow")]
        public int HandsetColour_Id { get; set; }

        [DataMember(Name = "hval")]
        public decimal? HandsetValue { get; set; }

        [DataMember(Name = "hvca")]
        public string HandsetValueCategory { get; set; }
     

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "IMEI[" + HandsetIMEI + "] HANDSETTYPE[" + ((MDA.Common.Enum.Incident2HandsetLinkType)I2H_LinkData.Incident2HandsetLinkType_Id).ToString() + "]"; }
        }
        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails("Handset: " + DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Incident-2-Handset", I2H_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Handset-2-Policy", H2Po_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Handset-2-Address", H2A_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Handset-2-Handset", H2H_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Handset-2-Telephone", H2T_LinkData));

            if (People != null && People.Count > 0)
            {
                var n = new PipelineNodeDetails("People", null);
                foreach (var p in People)
                {
                    n.Nodes.Add(p.ToPipelineNodeDetails());
                }
                node.Nodes.Add(n);
            }

            return node;
        }
    }

    [DataContract]
    [Serializable]
    public class PipelineOrganisationHandset : PipeEntityBase
    {
        public PipelineOrganisationHandset()
            : base()
        {

            HandsetType_Id = (int)MDA.Common.Enum.HandsetType.Unknown;
            HandsetColour_Id = (int)MDA.Common.Enum.HandsetColour.Unknown;

            I2H_LinkData = new PipelineIncident2HandsetLink();
            H2Po_LinkData = new PipelineHandset2PolicyLink();
            H2O_LinkData = new PipelineOrgHandsetLink();
            H2Pe_LinkData = null;
            H2H_LinkData = new PipelineHandset2HandsetLink();
            H2T_LinkData = new PipelineHandset2TelephoneLink();

        }

       

        [DataMember(Name = "htId", IsRequired = true)]
        [Description("0=Unknown, 1=Insured, 2=Updated, 3=Replacement, 4=Policy")]
        public int HandsetType_Id { get; set; }


        [DataMember(Name = "hId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string HandsetId { get; set; }

        [DataMember(Name = "imei")]
        public string HandsetIMEI { get; set; }

        [DataMember(Name = "hmke")]
        public string HandsetMake { get; set; }

        [DataMember(Name = "hmod")]
        public string HandsetModel { get; set; }

        [DataMember(Name = "hcol")]
        [Description("0=Unknown, 1=Beige, 2=Black, 3=Blue, 4=Bronze, 5=Brown, 6=Cream, 7=Gold, 8=Graphite, 9=Green, 10=Grey, 11=Lilac, 12=Maroon, 13=Mauve, 14=Orange, 15=Pink, 16=Purple, 17=Red, 18=Silver, 19=Turquoise, 20=White, 21=Yellow")]
        public int HandsetColour_Id { get; set; }

        [DataMember(Name = "hval")]
        public decimal? HandsetValue { get; set; }

        [DataMember(Name = "hvca")]
        public string HandsetValueCategory { get; set; }


        [DataMember(Name = "H2T")]
        [Browsable(false)]
        public PipelineHandset2TelephoneLink H2T_LinkData { get; set; }

        [DataMember(Name = "I2H")]
        [Browsable(false)]
        public PipelineIncident2HandsetLink I2H_LinkData { get; set; }

        [DataMember(Name = "H2Po")]
        [Browsable(false)]
        public PipelineHandset2PolicyLink H2Po_LinkData { get; set; }

        [DataMember(Name = "H2O")]
        [Browsable(false)]
        public PipelineOrgHandsetLink H2O_LinkData { get; set; }

        [DataMember(Name = "H2Pe")]
        [Browsable(false)]
        public PipelineHandset2PersonLink H2Pe_LinkData { get; set; }

        [DataMember(Name = "V2V")]
        [Browsable(false)]
        public PipelineHandset2HandsetLink H2H_LinkData { get; set; }

        [Browsable(false)]
        public override string DisplayText
        {
            get { return "IMEI[" + HandsetIMEI + "] HANDSETTYPE[" + ((MDA.Common.Enum.HandsetType)HandsetType_Id).ToString() + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails("Handset: " + DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Incident-2-oHandset", I2H_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("oHandset-2-Policy", H2Po_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("oHandset-2-Organisation", H2O_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("oHandset-2-Person", H2Pe_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("oHandset-2-Handset", H2H_LinkData));
            node.Nodes.Add(new PipelineNodeDetails("Handset-2-Telephone", H2T_LinkData));

            return node;
        }
    }
    
}
