﻿using MDA.Common;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MDA.Pipeline.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class PipelineTelephone : PipeEntityBase, IPipelineEntity
    {
        public PipelineTelephone() : base()
        {
            TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown;

            PO2T_LinkData = new PipelineTelephoneLink();
        }

        [DataMember(Name = "tId")]
        [Description("IBASE 5 Id")]
        [XmlIgnore]
        public string TelephoneId { get; set; }

        [DataMember(Name = "ac")]
        public string AreaCode { get; set; }
        [DataMember(Name = "ic")]
        public string InternationalCode { get; set; }
        [DataMember(Name = "fn")]
        public string FullNumber { get; set; }
        [DataMember(Name = "csn")]
        public string ClientSuppliedNumber { get; set; }

        [DataMember(Name = "ttId")]
        [Description("0=Unknown, 1=Landline, 2=Mobile, 3=FAX")]
        public int TelephoneType_Id { get; set; }

        [DataMember(Name = "PO2T")]
        [Browsable(false)] 
        public PipelineTelephoneLink PO2T_LinkData { get; set; }

        public override string DisplayText
        {
            get { return "CNUM[" + ClientSuppliedNumber + "] FNUM[" + FullNumber + "] LTYPE[" + ((MDA.Common.Enum.TelephoneLinkType)PO2T_LinkData.TelephoneLinkType_Id).ToString() + "] TYPE[" + ((MDA.Common.Enum.TelephoneType)TelephoneType_Id).ToString() + "]"; }
        }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails(DisplayText, this);

            node.Nodes.Add(new PipelineNodeDetails("Person/Org-2-Telephone ", PO2T_LinkData));

            return node;
        }
    }
}
