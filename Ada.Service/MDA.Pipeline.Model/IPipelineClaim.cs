﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using MDA.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MDA.Pipeline.Model
{
    public interface IPipelineClaim
    {
        string ClaimNumber { get; set; }

        DateTime IncidentDate { get; set; }

        PipelineClaimInfo ExtraClaimInfo { get; set; } 

        ProcessingResults CleanseResults { get; set; }
        ProcessingResults ValidationResults { get; set; }

        PipelineNodeDetails ToPipelineNodeDetails();

        PipelinePolicy Policy { get; set; }
 
        int ClaimType_Id { get; set; }
        string FraudRingName { get; set; }
        string IfbReference { get; set; }
        string IncidentId { get; set; }
        string KeoghsEliteReference { get; set; }
        //DateTime OriginalIncidentDateTime { get; set; }

        string ExternalServices { get; set; }

        string KeyAttractor { get; set; }
        string Source { get; set; }
        string SourceReference { get; set; }
        string SourceDescription { get; set; }
        string CreatedBy { get; set; }
        DateTime CreatedDate { get; set; }
        string ModifiedBy { get; set; }
        DateTime? ModifiedDate { get; set; }
        string IBaseId { get; set; }

        byte RecordStatus { get; set; }

        byte ADARecordStatus { get; set; }

        int Operation { get; set; }

        object Tag { get; set; }

        bool ShouldBeDiscarded { get; }

        string DisplayText { get; }
    }

    public interface IPipelineEntity
    {
        string KeyAttractor { get; set; }
        string Source { get; set; }
        string SourceReference { get; set; }
        string SourceDescription { get; set; }
        string CreatedBy { get; set; }
        DateTime CreatedDate { get; set; }
        string ModifiedBy { get; set; }
        DateTime? ModifiedDate { get; set; }
        string IBaseId { get; set; }

        byte RecordStatus { get; set; }

        byte ADARecordStatus { get; set; }

        int Operation { get; set; }

        object Tag { get; set; }

        bool ShouldBeDiscarded { get; }

        string DisplayText { get; }
    }
}
