﻿using System.Runtime.Serialization;

namespace MDA.Pipeline.Model
{
    using MDA.Common;
    using System;
    using System.Collections.Generic;

    [DataContract(Name = "PipelineClaimBatch", Namespace = "http://www.keoghs.com/PipelineClaimBatchV1")]
    [Serializable]
    public class PipelineClaimBatch
    {
        public PipelineClaimBatch()
        {
            Claims = new List<IPipelineClaim>();
        }

        [DataMember(IsRequired = true)]
        public List<IPipelineClaim> Claims { get; set; }

        public PipelineNodeDetails ToPipelineNodeDetails()
        {
            var node = new PipelineNodeDetails("Batch", null);

            if (Claims != null)
            {
                foreach (var c in Claims)
                {
                    node.Nodes.Add(c.ToPipelineNodeDetails());
                }
            }

            return node;
        }

    }

}
