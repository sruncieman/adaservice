﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MDA.CueService.Model
{
    /// <summary>   Information about the result from CUE API </summary>
    ///
    /// <remarks>   Jonathanwalker, 29/07/2014. </remarks>

    [DataContract]
    public class Results
    {
        [DataMember(Name = "lstRI")]
        public List<Dictionary<string,Result>> ListResultInfo { get; set; }
        [DataMember(Name = "cn")]
        public string ClaimNumber { get; set; }
        [DataMember(Name = "sd")]
        public DateTime SearchDate { get; set; }
        
    }

    [DataContract]
    [KnownType(typeof(MDA.CueService.Model.Result))]
    public class Result
    {
       [DataMember(Name = "lstSS")]
        public List<CUESearchSummary> SearchSummary { get; set; }
        [DataMember(Name = "lstRS")]
        public List<CUEResultsSummary> ResultsSummary { get; set; }
        [DataMember(Name = "SC")]
        public CueSearchCriteria SearchCriteria { get; set; }
        [DataMember(Name = "lstIS")]
        public List<CUEIncidentSummary> IncidentSummary { get; set; }
        [DataMember(Name = "lstCI")]
        public List<CUEIncident> CUE_Incidents { get; set; }
    }

    [DataContract]
    public class CueSearchCriteria
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "dob")]
        public string Dob { get; set; }
        [DataMember(Name = "ni")]
        public string NI_Number { get; set; }
        [DataMember(Name = "d_no")]
        public string DrivingLicenceNum { get; set; }
        [DataMember(Name = "vin")]
        public string VIN { get; set; }
        [DataMember(Name = "reg")]
        public string VehicleReg { get; set; }
        [DataMember(Name = "houseN")]
        public string AddressHouseName { get; set; }
        [DataMember(Name = "num")]
        public string AddressNumber { get; set; }
        [DataMember(Name = "st")]
        public string AddressStreet { get; set; }
        [DataMember(Name = "loc")]
        public string AddressLocality { get; set; }
        [DataMember(Name = "town")]
        public string AddressTown { get; set; }
        [DataMember(Name = "city")]
        public string AddressCity { get; set; }
        [DataMember(Name = "post")]
        public string AddressPostCode { get; set; }
    }

    [DataContract]
    public class CUEIncident
    {
        [DataMember(Name = "I_Id")]
        public int IncidentId { get; set; }
        [DataMember(Name = "I_Type")]
        public string IncidentType { get; set; }
        [DataMember(Name = "c_data")]
        public CUEClaimData ClaimData { get; set; }
        [DataMember(Name = "g_data")]
        public CUEGeneralData GeneralData { get; set; }
        [DataMember(Name = "lstCII")]
        public List<CUEIncidentInvolvement> CUEIncidentInvolvements { get; set; }
        [DataMember(Name = "lstCIIVD")]
        public List<CUEIncidentInvolvementVehicleDetails> CUEIncidentInvolvementVehicleDetails { get; set; }
        [DataMember(Name = "lstSup")]
        public List<CUESupplier> Suppliers { get; set; }
        [DataMember(Name = "i_details")]
        public CUEIncidentDetails IncidentDetails { get; set; }
    }

    [DataContract]
    public class CUESupplier
    {
        [DataMember(Name = "I_Id")]
        public int IncidentId { get; set; }
        [DataMember(Name = "compN")]
        public string CompanyName { get; set; }
        [DataMember(Name = "ss")]
        public string SupplierStatus { get; set; }
        [DataMember(Name = "tel")]
        public string Telephone { get; set; }
        [DataMember(Name = "houseN")]
        public string AddressHouseName { get; set; }
        [DataMember(Name = "num")]
        public string AddressNumber { get; set; }
        [DataMember(Name = "st")]
        public string AddressStreet { get; set; }
        [DataMember(Name = "loc")]
        public string AddressLocality { get; set; }
        [DataMember(Name = "town")]
        public string AddressTown { get; set; }
        [DataMember(Name = "city")]
        public string AddressCity { get; set; }
        [DataMember(Name = "post")]
        public string AddressPostCode { get; set; }
        [DataMember(Name = "a_ind")]
        public string AddressIndicator { get; set; }
        [DataMember(Name = "vat")]
        public int VAT { get; set; }
        [DataMember(Name = "p_amount")]
        public decimal PaymentAmount { get; set; }
    }

    [DataContract]
    public class CUEIncidentInvolvement
    {
        [DataMember(Name = "I_Id")]
        public int Incident_Id { get; set; }
        [DataMember(Name = "type")]
        public string Type { get; set; }
        [DataMember(Name = "stat")]
        public string Status { get; set; }
        [DataMember(Name = "tit")]
        public string Title { get; set; }
        [DataMember(Name = "fname")]
        public string Forename { get; set; }
        [DataMember(Name = "mname")]
        public string MiddleName { get; set; }
        [DataMember(Name = "lname")]
        public string LastName { get; set; }
        [DataMember(Name = "ni")]
        public string NI_Number { get; set; }
        [DataMember(Name = "hname")]
        public string AddressHouseName { get; set; }
        [DataMember(Name = "num")]
        public string AddressNumber { get; set; }
        [DataMember(Name = "st")]
        public string AddressStreet { get; set; }
        [DataMember(Name = "local")]
        public string AddressLocality { get; set; }
        [DataMember(Name = "town")]
        public string AddressTown { get; set; }
        [DataMember(Name = "city")]
        public string AddressCity { get; set; }
        [DataMember(Name = "post")]
        public string AddressPostCode { get; set; }
         [DataMember(Name = "a_ind")]
        public string AddressIndicator { get; set; }
        [DataMember(Name = "gen")]
        public string Sex { get; set; }
        [DataMember(Name = "dob")]
        public DateTime Dob { get; set; }
        [DataMember(Name = "vat")]
        public int VAT { get; set; }
        [DataMember(Name = "p_amount")]
        public decimal PaymentAmount { get; set; }
        [DataMember(Name = "tel")]
        public string Telephone { get; set; }
        [DataMember(Name = "occ")]
        public string Occupation { get; set; }
    }

    [DataContract]
    public class CUEIncidentInvolvementVehicleDetails : CUEIncidentInvolvement
    {
        [DataMember(Name = "reg")]
        public string VehicleRegistration { get; set; }
        [DataMember(Name = "vvin")]
        public string VehicleIdentificationNumber { get; set; }
        [DataMember(Name = "mak")]
        public string Make { get; set; }
        [DataMember(Name = "mod")]
        public string Model { get; set; }
        [DataMember(Name = "col")]
        public string Colour { get; set; }
        [DataMember(Name = "cov")]
        public string CoverType { get; set; }
        [DataMember(Name = "vin")]
        public string VIN { get; set; }
        [DataMember(Name = "dStat")]
        public string DamageStatus { get; set; }
        [DataMember(Name = "recStatus")]
        public string RecoveredStatus { get; set; }
        [DataMember(Name = "regStatus")]
        public string RegistrationStatus { get; set; }
        [DataMember(Name = "nForOInd")]
        public string NewForOldIndicator { get; set; }
    }

    [DataContract]
    public class CUEGeneralData
    {
        [DataMember(Name = "cStat")]
        public string ClaimStatus { get; set; }
        [DataMember(Name = "iDesc")]
        public string IncidentDescription { get; set; }
        [DataMember(Name = "polType")]
        public string PolicyType { get; set; }
        [DataMember(Name = "polNum")]
        public string PolicyNumber { get; set; }
        [DataMember(Name = "polId")]
        public string PolicyNumberID { get; set; }
        [DataMember(Name = "colPolInd")]
        public string CollectivePolicyIndicator { get; set; }
        [DataMember(Name = "lossDt")]
        public DateTime LossSetupDate { get; set; }
        [DataMember(Name = "insCNam")]
        public string InsurerContactName { get; set; }
        [DataMember(Name = "insConTel")]
        public string InsurerContactTel { get; set; }
        [DataMember(Name = "incDt")]
        public DateTime IncidentDate { get; set; }
        [DataMember(Name = "polIncDt")]
        public DateTime PolicyInceptionDate { get; set; }
        [DataMember(Name = "polPEndDt")]
        public DateTime PolicyPeriodEndDate { get; set; }
        [DataMember(Name = "causeLoss")]
        public string CauseOfLoss { get; set; }
        [DataMember(Name = "closedDt")]
        public DateTime ClosedDate { get; set; }
        [DataMember(Name = "catRel")]
        public string CatastropheRelated { get; set; }
        [DataMember(Name = "houseN")]
        public string RiskAddressHouseName { get; set; }
        [DataMember(Name = "num")]
        public string RiskAddressNumber { get; set; }
        [DataMember(Name = "st")]
        public string RiskAddressStreet { get; set; }
        [DataMember(Name = "loc")]
        public string RiskAddressLocality { get; set; }
        [DataMember(Name = "town")]
        public string RiskAddressTown { get; set; }
        [DataMember(Name = "city")]
        public string RiskAddressCity { get; set; }
        [DataMember(Name = "post")]
        public string RiskAddressPostCode { get; set; }
        [DataMember(Name = "paymnts")]
        public List<CUEPayment> CUEPayments { get; set; }
    }

    [DataContract]
    public class CUEPayment
    {
        [DataMember(Name = "i_Id")]
        public int IncidentId { get; set; }
        [DataMember(Name = "PType")]
        public string PaymentType { get; set; }
        [DataMember(Name = "PAmnt")]
        public decimal PaymentAmount { get; set; }
    }

    [DataContract]
    public class CUEClaimData
    {
        [DataMember(Name = "Ins")]
        public string Insurer { get; set; }
        [DataMember(Name = "cNum")]
        public string ClaimNumber { get; set; }
        [DataMember(Name = "lstCCDPH")]
        public List<CUEClaimDataPolicyHolder> PolicyHolders { get; set; }

    }

    [DataContract]
    public class CUEClaimDataPolicyHolder
    {
        [DataMember(Name = "i_Id")]
        public int IncidentId { get; set; }
        [DataMember(Name = "fullN")]
        public string FullName { get; set; }
        [DataMember(Name = "addr")]
        public string Address { get; set; }
    }

    [DataContract]
    public class CUESearchSummary
    {
        [DataMember(Name = "subj")]
        public string Subject { get; set; }
        [DataMember(Name = "involv")]
        public string Involvement { get; set; }
        [DataMember(Name = "mot")]
        public bool Motor { get; set; }
        [DataMember(Name = "pi")]
        public bool PI { get; set; }
        [DataMember(Name = "hh")]
        public bool Household { get; set; }
        [DataMember(Name = "crossQ")]
        public bool Cross_Enquiry { get; set; }
        [DataMember(Name = "searchDt")]
        public DateTime SearchDate { get; set; }
    }

    [DataContract]
    public class CUEResultsSummary
    {
        [DataMember(Name = "incnts")]
        public string Incidents { get; set; }
        [DataMember(Name = "invPIClaims")]
        public string Involved_PI_Claims { get; set; }
        [DataMember(Name = "invMClaims")]
        public string Involved_Motor_Claims { get; set; }
       [DataMember(Name = "invHHClaims")]
        public string Involved_Household_Claims { get; set; }
    }

    [DataContract]
    public class CUEIncidentSummary
    {
        [DataMember(Name = "i_Id")]
        public int IncidentId { get; set; }
        [DataMember(Name = "frm")]
        public DateTime From { get; set; }
        [DataMember(Name = "to")]
        public DateTime To { get; set; }
        [DataMember(Name = "piClaims")]
        public int PI_Claims { get; set; }
        [DataMember(Name = "moClaims")]
        public int MO_Claims { get; set; }
        [DataMember(Name = "hhClaims")]
        public int HH_Claims { get; set; }
        [DataMember(Name = "lstCSI")]
        public List<CUESummaryIncident> CUE_Incidents { get; set; }
    }

    [DataContract]
    public class CUESummaryIncident
    {
        [DataMember(Name = "iId")]
        public int IncidentId { get; set; }
        [DataMember(Name = "iType")]
        public string IncidentType { get; set; }
        [DataMember(Name = "subj")]
        public string Subject { get; set; }
        [DataMember(Name = "invType")]
        public string InvolvementType { get; set; }
        [DataMember(Name = "incidentDt")]
        public DateTime IncidentDate { get; set; }
        [DataMember(Name = "claimStat")]
        public string ClaimStatus { get; set; }
        [DataMember(Name = "ins")]
        public string Insurer { get; set; }
        [DataMember(Name = "desc")]
        public string Description { get; set; }
        [DataMember(Name = "matchCode")]
        public string MatchCode { get; set; }
    }


    [DataContract]
    public class CUERequestInfo
    {
        [DataMember(Name = "claimNum")]
        public string ClaimNumber { get; set; }
        [DataMember(Name = "incDt")]
        public DateTime IncidentDate { get; set; }
        [DataMember(Name = "uploadDt")]
        public DateTime UploadedDate { get; set; }
        [DataMember(Name = "riskId")]
        public int RiskClaim_Id { get; set; }
        [DataMember(Name = "lstCI")]
        public List<CueInvolvement> CueInvolvements { get; set; }
    }

    [DataContract]
    public class CueInvolvement
    {
        [DataMember(Name = "personId")]
        public int PersonId { get; set; }
        [DataMember(Name = "fName")]
        public string FirstName { get; set; }
        [DataMember(Name = "lName")]
        public string LastName { get; set; }
        [DataMember(Name = "inv")]
        public string Involvement { get; set; }
        [DataMember(Name = "dob")]
        public DateTime Dob { get; set; }
        [DataMember(Name = "ni")]
        public string NI_Number { get; set; }
        [DataMember(Name = "d_num")]
        public string DrivingLicenceNumber { get; set; }
        [DataMember(Name = "hName")]
        public string HouseName { get; set; }
        [DataMember(Name = "num")]
        public string Number { get; set; }
        [DataMember(Name = "st")]
        public string Street { get; set; }
        [DataMember(Name = "loc")]
        public string Locality { get; set; }
        [DataMember(Name = "town")]
        public string Town { get; set; }
        [DataMember(Name = "post")]
        public string PostCode { get; set; }
        [DataMember(Name = "reg")]
        public string VehicleReg { get; set; }
        [DataMember(Name = "vin")]
        public string VIN { get; set; }
        [DataMember(Name = "cueDbs")]
        public IEnumerable<CUE_Databases> CUE_Databases { get; set; }
    }

    [DataContract]
    public class CUE_Databases
    {
        [DataMember(Name = "db")]
        public string Database { get; set; }
        [DataMember(Name = "checked")]
        public bool IsChecked { get; set; }
        [DataMember(Name = "readOnly")]
        public bool ReadOnly { get; set; }
    }

     [DataContract]
    public class CUEIncidentDetails
    {
        [DataMember(Name = "injDesc")]
        public string InjuryDescription { get; set; }
        [DataMember(Name = "hospAttended")]
        public string HospitalAttended { get; set; }
        [DataMember(Name = "illnessDesc")]
        public string IllnessDiseaseDescription { get; set; }
    }
}
