﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.TracesmartService.Model
{
    /// <summary>
    /// MASK values to use in the Services Called mask
    /// </summary>
    public class TracesmartServicesMask
    {
        public const int Tracesmart_Use_Passport     = 0x00000001;       // 0000 0000 0000 0000 0000 0000 0000 0001
        public const int Tracesmart_Use_Telephone    = 0x00000002;       // 0000 0000 0000 0000 0000 0000 0000 0010
        public const int Tracesmart_Use_Driving      = 0x00000004;       // 0000 0000 0000 0000 0000 0000 0000 0100
        public const int Tracesmart_Use_Birth        = 0x00000008;       // 0000 0000 0000 0000 0000 0000 0000 1000
        public const int Tracesmart_Use_SmartLink    = 0x00000010;       // 0000 0000 0000 0000 0000 0000 0001 0000
        public const int Tracesmart_Use_NI           = 0x00000020;       // 0000 0000 0000 0000 0000 0000 0010 0000
        public const int Tracesmart_Use_CardNumber   = 0x00000040;       // 0000 0000 0000 0000 0000 0000 0100 0000
        public const int Tracesmart_Use_BankAccount  = 0x00000080;       // 0000 0000 0000 0000 0000 0000 1000 0000
        public const int Tracesmart_Use_Mobile       = 0x00000100;       // 0000 0000 0000 0000 0000 0001 0000 0000
        public const int Tracesmart_Use_Crediva      = 0x00000200;       // 0000 0000 0000 0000 0000 0010 0000 0000
        public const int Tracesmart_Use_CreditActive = 0x00000400;       // 0000 0000 0000 0000 0000 0100 0000 0000
        public const int Tracesmart_Use_NHS          = 0x00000800;       // 0000 0000 0000 0000 0000 1000 0000 0000
        public const int Tracesmart_Use_Cardavs      = 0x00001000;       // 0000 0000 0000 0000 0001 0000 0000 0000
        public const int Tracesmart_Use_MPan         = 0x00002000;       // 0000 0000 0000 0000 0010 0000 0000 0000
        public const int Tracesmart_Use_Address      = 0x00004000;       // 0000 0000 0000 0000 0100 0000 0000 0000
        public const int Tracesmart_Use_DeathScreen  = 0x00008000;       // 0000 0000 0000 0000 1000 0000 0000 0000
        public const int Tracesmart_Use_DoB          = 0x00010000;       // 0000 0000 0000 0001 0000 0000 0000 0000
        public const int Tracesmart_Use_Sanction     = 0x00020000;       // 0000 0000 0000 0010 0000 0000 0000 0000
        public const int Tracesmart_Use_Insolvency   = 0x00040000;       // 0000 0000 0000 0100 0000 0000 0000 0000
        public const int Tracesmart_Use_CCJ          = 0x00080000;       // 0000 0000 0000 1000 0000 0000 0000 0000
    }

    public class ErrorPartInfo
    {
        public string Service { get; set; }
        public string Details { get; set; }
    }

    public class SummaryPartInfo
    {
        public bool Status { get; set; }
        public string Id { get; set; }
        public string IKey { get; set; }
        public string EquifaxUsername { get; set; }
        public string Reference { get; set; }
        public string Scorecard { get; set; }
        public int Smartscore{ get; set; }
        public string ResultText{ get; set; }
        public string ProfileURL{ get; set; }
        public int Credits{ get; set; }
        public ErrorPartInfo[] Errors{ get; set; }
        
    }

    public class OccupantPartInfo
    {
        public string Name { get; set; }
        public string DOB { get; set; }
        public string Recency { get; set; }
        public string Residency { get; set; }
        public string Telephone { get; set; }
        public string TelephoneName { get; set; }
        public string Source { get; set; }
    }

    public class PropertyPartInfo
    {
        public string Type { get; set; }
        public string Tenure { get; set; }
        public string Date { get; set; }
        public string Price { get; set; }

    }
    public class AddressPartInfo
    {
        public string Forename { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public string MatchType { get; set; }
        public string DOB { get; set; }
        public bool ForenameAppended { get; set; }
        public bool MiddleNameAppended { get; set; }
        public bool DOBAppended { get; set; }
        public string Telephone { get; set; }
        public string Telephonename { get; set; }
        public string GoneAway { get; set; }
        public string Mosaic { get; set; }
        public string Source { get; set; }
        public string Recency { get; set; }
        public List<OccupantPartInfo> Occupants { get; set; }
        public List<PropertyPartInfo> Property { get; set; }
        public bool AddressValidated { get; set; }
    }

    public class SmartlinkPartInfo
    {
        public string Title { get; set; }
        public string Forename { get; set; }
        public string Middle { get; set; }
        public string Surname { get; set; }
        public string DOB { get; set; }
        public AddressLabelInfo Address { get; set; }
        public string Recency { get; set; }
        public string Residency { get; set; }
        public string LinkSource { get; set; }
    }

    public class DeathscreenResultInfo
    {
        public string Forename{ get; set; }
        public string SecondName { get; set; }
        public string ThirdName { get; set; }
        public string FourthName { get; set; }
        public string Surname { get; set; }
        public string District { get; set; }
        public string DOB { get; set; }
        public string DOD { get; set; }
        public string DOR { get; set; }
        public string VolumeNo { get; set; }
        public string DistNo { get; set; }
        public string PageNo { get; set; }
        public string RegNo { get; set; }
        public string EntryNo { get; set; }
        public string PlaceOfBirth { get; set; }
        public string MaidenName { get; set; }
        public string GROReference { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Address5 { get; set; }
        public string Postcode { get; set; }
        public string MatchType { get; set; }
    }

    public class TelephonePartInfo
    {
        public bool ResultFlag { get; set; }
        public string Areaname { get; set; }
        public string Locality { get; set; }
        public string Postcode { get; set; }
        public string Operator { get; set; }
        public string Status { get; set; }
    }

    public class DrivingLicensePartInfo
    {
        public bool ResultFlag { get; set; }
        public bool MailSortFlag { get; set; }
        public bool MiddleNameWarning { get; set; }
    }

    public class DOBPartInfo
    {
        public int TracesmartDOB { get; set; }
        public int ExperianDOB { get; set; }
        public int EquifaxDOB { get; set; }
        public bool EquifaxDOBFieldSpecified { get; set; }
        public string EquifaxDOBStatusField { get; set; }
    }

    public class NHSPartInfo
    {
        public bool ResultFlag { get; set; }
    }

    public class NIPartInfo
    {
        public bool ResultFlag { get; set; }
    }

    public class AliasPartInfo
    {
        public string Name { get; set; }
    }

    public class SanctionPartInfo
    {
        public string Name { get; set; }
        public string Recency { get; set; }
        public string Source { get; set; }
        public List<AddressLabelInfo> Addresses { get; set; }
        public List<AliasPartInfo> Aliases { get; set; }
        public string[] Positions { get; set; }
    }

    public class PassportPartInfo
    {
        public bool MRZValid { get; set; }
        public bool DOBValid { get; set; }
        public bool GenderValid { get; set; }
    }

    public class BirthPartInfo
    {
        public string ResultIndicator { get; set; }
        public string Name { get; set; }
        public string Maiden { get; set; }
        public string RegDate { get; set; }
        public string RegDistrict { get; set; }
    }

    public class CardPartInfo
    {
        public string Authorisation { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string CV2 { get; set; }
    }

    public class CardNumberPartInfo
    {
        public bool CardNumberValid { get; set; }
        public bool CardTypeValid { get; set; }
        public string CardTypeConfirm { get; set; }
    }

    public class MpanPartInfo
    {
        public bool ResultFlag { get; set; }
    }

    public class InsolvencyResultInfo
    {
        //public string Type { get; set; }         // Not sure which of these 2 to use???
        public string CaseType { get; set; }

        public string Name { get; set; }
        public string CaseNo { get; set; }
        public AddressLabelInfo Address { get; set; }
        public string DOB { get; set; }
        public string Court { get; set; }
        public string StartDate { get; set; }
        public string Status { get; set; }
    }

    public class BankAccountPartInfo
    {
        public bool BankAccountValid { get; set; }
        public string BankName { get; set; }
        public string BranchDetails { get; set; }
        public bool BACSPayments { get; set; }
        public bool CHAPSPayments { get; set; }
        public bool FasterPayments { get; set; }
   }

    public class CredivaPartInfo
    {
        public bool FullER { get; set; }
    }

    public class CcjResultInfo
    {
        public string Name { get; set; }
        public string DOB { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Address5 { get; set; }
        public string Postcode { get; set; }
        public string JudgementDate { get; set; }
        public string JudgementType { get; set; }
        public string Amount { get; set; }
        public string CaseNumber { get; set; }
        public string CourtName { get; set; }
        public string DateEnd { get; set; }
    }

    public class MobilePartInfo
    {
        public string Status { get; set; }
        public string Provider { get; set; }
        public string CurrentLocation { get; set; }
    }

    public class CreditActivePartInfo
    {
        public int InsightAccounts { get; set; }
        public int InsightLenders { get; set; }
        public string CIFAS { get; set; }
    }

    public class ResultInfo
    {
        public SummaryPartInfo Summary { get; set; }
        public AddressPartInfo Address { get; set; }      
        public List<SmartlinkPartInfo> Smartlink { get; set; }   
        public List<DeathscreenResultInfo> Deathscreen { get; set; }
        public TelephonePartInfo Telephone { get; set; }
        public DrivingLicensePartInfo DrivingLicence { get; set; }
        public DOBPartInfo DOB { get; set; }
        public NHSPartInfo NHS { get; set; }
        public NIPartInfo NI { get; set; }
        public List<SanctionPartInfo> Sanction { get; set; }
        public PassportPartInfo Passport { get; set; }
        public BirthPartInfo Birth { get; set; }
        public CardPartInfo CardAVS { get; set; }
        public CardNumberPartInfo CardNumber { get; set; }
        public MpanPartInfo Mpan { get; set; }
        public List<InsolvencyResultInfo> Insolvency { get; set; }
        public BankAccountPartInfo BankAccount { get; set; }
        public CredivaPartInfo Crediva { get; set; }
        public List<CcjResultInfo> CCJ { get; set; }
        public MobilePartInfo Mobile { get; set; }
        public CreditActivePartInfo CreditActive { get; set; }
    }

    public class PersonInfo
    {
        public string IKey { get; set; }
        //public int ADA_PersonId { get; set; }

        public string Forename{ get; set; }
        public string Middle{ get; set; }
        public string Surname{ get; set; }
        public string Gender{ get; set; }
        public string DOB{ get; set; }
        public string Address1{ get; set; }
        public string Address2{ get; set; }
        public string Address3{ get; set; }
        public string Address4{ get; set; }
        public string Address5{ get; set; }
        public string Address6{ get; set; }
        public string Postcode{ get; set; }
        public string Passport1 { get; set; }
        public string Passport2 { get; set; }
        public string Passport3 { get; set; }
        public string Passport4 { get; set; }
        public string Passport5 { get; set; }
        public string Passport6 { get; set; }
        public string Passport7 { get; set; }
        public string Passport8 { get; set; }
        public string Drivinglicence1{ get; set; }
        public string Drivinglicence2{ get; set; }
        public string Drivinglicence3{ get; set; }
        public string DrivingPostcode{ get; set; }
        public string DrivingMailsort{ get; set; }
        public string CardNumber{ get; set; }
        public string NI{ get; set; }
        public string Nhs{ get; set; }
        public string Cardtype{ get; set; }
        public string BForename{ get; set; }
        public string BMiddle{ get; set; }
        public string BSurname{ get; set; }
        public string Maiden{ get; set; }
        public string LandlineTelephone{ get; set; }
        public string MobileTelephone{ get; set; }
        public string MpanNumber1{ get; set; }
        public string MpanNumber2{ get; set; }
        public string MpanNumber3{ get; set; }
        public string MpanNumber4{ get; set; }
        public CardInfo Cardavs{ get; set; }
        public string Sortcode{ get; set; }
        public string AccountNumber{ get; set; }
    }

    public class CardInfo
    {
        public string CardType { get; set; }
        public string CardHolder { get; set; }
        public string CardNumber { get; set; }
        public string CardStart { get; set; }
        public string CardExpire { get; set; }
        public AddressLabelInfo CardAddress { get; set; }
        public string CV2 { get; set; }
        public string IssueNumber { get; set; }
    }

    public class AddressLabelInfo
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Address5 { get; set; }
        public string Postcode { get; set; }
        public string DPS { get; set; }
    }


    public class FilteredResults
    {
        public string IKey { get; set; }

        public DateTime Person_SanctionDate { get; set; }           // TS.Sanction.Match[].Recency
        public string Person_SanctionSource { get; set; }           // TS.Sanction.Match[].Source

        public string P2A_TSIDUAMLResult { get; set; }              // Ts.Summary.ResultText
        public int? P2A_TracesmartDOB { get; set; }                  // TS.DOB.TracemartDOB
        public int? P2A_ExperianDOB { get; set; }                   // TS.DOB.ExperianDOB
        public string P2A_GoneAway { get; set; }                    // TS.Address.GoneAway
        //public string P2A_Insolvency { get; set; }                  // TS.Insolvency.InsResult[].CaseType
        //public DateTime? P2A_InsolvencyStartDate { get; set; }      // 
        //public string P2A_InsolvencyStatus { get; set; }            // TS.Insolvency.InsResult[].Status
        //public DateTime? P2A_CCJDate { get; set; }                     // TS.CCJ.CCJResult[].JudgementDate
        //public string P2A_CCJType { get; set; }                     // TS.CCJ.CCJResult[].JudgementType
        //public string P2A_CCJAmount { get; set; }                   // TS.CCJ.CCJResult[].Amount
        //public DateTime? P2A_CCJEndDate { get; set; }               // TS.CCJ.CCJResult[].dateEnd

        public string P2A_DeathScreenMatchType { get; set; }
        public string P2A_DeathScreenDoD { get; set; } 
        public string P2A_DeathScreenRegNo  { get; set; } 

        public string P2A_CCJHistory { get; set; }
        public string P2A_InsolvencyHistory { get; set; }             // TS.Insolvency.InsResult[].CaseType

        public bool? P2A_CredivaCheck { get; set; }                  // TS.Crediva.FullER
        public string P2A_AddressMatchType { get; set; }              // TS.Address.MatchType

        public bool? P2BA_Validated { get; set; }                    // TS.Bankaccount.BankAccountValid

        public bool? P2DL_Validated { get; set; }                    // TS.DrivingLicence.ResultFlag

        public bool? P2NI_Validated { get; set; }                    // TS.NI.ResultFlag

        public bool? P2PP_MRZValid { get; set; }                     // TS.Passport.MRZValid
        public bool? P2PP_DOBValid { get; set; }                     // TS.Passport.DOBValid
        public bool? P2PP_GenderValid { get; set; }                  // TS.Passport.GenderValid

        public bool? P2PC_CardNumberValid { get; set; }              // TS.CardNumber.CardNumberValid

        public bool? P2T_Validated { get; set; }                     // TS.Telephone.ResultFlag
        public string P2T_TelephoneStatus { get; set; }             // TS.Telephone.Status
        public string P2T_MobileStatus { get; set; }                // TS.Mobile.Status
        public string P2T_CurrentLocation { get; set; }             // TS.Mobile.CurrentLocation

        public string ADR_PropertyType { get; set; }                // TS.Address.Property
        public string ADR_MosaicCode { get; set; }                  // TS.Address.Mosaic
        public bool ADR_Validated { get; set; }                     // TS.Address.AddressValidated
        public string ADR_TracesmartSource { get; set; }            // TS.Address.Source

       
    }  
}
