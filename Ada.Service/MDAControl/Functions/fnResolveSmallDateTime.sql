﻿
CREATE FUNCTION [Sync].[fnResolveSmallDateTime](@DateTime DateTime)
RETURNS smalldateTime
AS
BEGIN

	DECLARE @sDatetime smalldatetime
	
	

	SELECT  @sdatetime =CASE WHEN @DateTime BETWEEN '19000101 00:00:00.000' AND '20790606 23:59:29.997'
							 THEN CAST(@DateTime AS SMALLDATETIME)
							 ELSE '19000101 00:00:00.111' END

	RETURN ISNULL(@sDatetime,'19000101 00:00:00.111')

END

