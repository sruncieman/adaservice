﻿CREATE FUNCTION IBaseM.Fn_GetEliteRefFromDbSource
/**************************************************************************************************/
-- ObjectName:			MDAControl.IBaseM.Fn_GetEliteRefFromDbSource
--
-- Description:			Extract the reference from the description using rules table
--
-- Usage:				Called within:
--							IBaseM.uspMigrationStep03
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
/**************************************************************************************************/
(
@Val VARCHAR(500)
)
RETURNS  VARCHAR(255)
AS
BEGIN

	DECLARE  @Ret			VARCHAR(50)
			,@Start			INT
			,@Expresion		VARCHAR(100)
			,@StringLength	INT
	
	/*CleanTheTheString*/	
	DECLARE C_CleanString CURSOR FOR
	SELECT Expression FROM IBaseM.SourceReferenceMatching WHERE ActionId = 1 ORDER BY PriorityGroup
	
	SET @Val = UPPER(' ' + REPLACE(REPLACE(@Val,'.','/'),'-',' ') + ' ')
	
	OPEN C_CleanString 
	FETCH NEXT FROM C_CleanString INTO @Expresion
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @Val = REPLACE(@Val,@Expresion,'')
			FETCH NEXT FROM C_CleanString INTO @Expresion
		END
	CLOSE C_CleanString
	DEALLOCATE C_CleanString
		
	/*IdentifyTheString*/
	DECLARE C_IdentifyTheString CURSOR FOR
	SELECT Expression, StringLength FROM IBaseM.SourceReferenceMatching WHERE ActionId = 2 ORDER BY PriorityGroup, StringLength DESC
	
	OPEN C_IdentifyTheString
	FETCH NEXT FROM C_IdentifyTheString INTO @Expresion, @StringLength
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @Start = PATINDEX(@Expresion, @Val)
			IF @Start > 0
			BEGIN
				SET @Ret =  SUBSTRING(@Val, @Start+1, @StringLength) 
				RETURN @Ret
			END
			FETCH NEXT FROM C_IdentifyTheString INTO @Expresion, @StringLength
		END
	CLOSE C_IdentifyTheString
	DEALLOCATE C_IdentifyTheString
	
	/*AllowAnyOtherPermitedValues*/ 
	DECLARE C_AllowAnyOtherPermitedValues CURSOR FOR
	SELECT Expression FROM IBaseM.SourceReferenceMatching WHERE ActionId = 3 ORDER BY PriorityGroup, StringLength DESC
	
	OPEN C_AllowAnyOtherPermitedValues
	FETCH NEXT FROM C_AllowAnyOtherPermitedValues INTO @Expresion
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @Start  = PATINDEX(@Expresion, @Val)
			IF @Start > 0
			BEGIN
				SET @Ret = LTRIM(RTRIM(@Val))
				RETURN @Ret
			END	 
			FETCH NEXT FROM C_AllowAnyOtherPermitedValues INTO @Expresion
		END
	CLOSE C_AllowAnyOtherPermitedValues
	DEALLOCATE C_AllowAnyOtherPermitedValues

	RETURN @Ret
END
