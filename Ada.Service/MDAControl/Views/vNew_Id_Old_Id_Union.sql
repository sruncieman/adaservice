﻿CREATE VIEW [IBaseM].[vNew_Id_Old_Id_Union]

AS

SELECT * FROM IBaseM.vNew_Id_Old_Id_Vehicle_
UNION ALL
SELECT * FROM IBaseM.vNew_Id_Old_Id_Incident_
UNION ALL
SELECT * FROM IBaseM.vNew_Id_Old_Id_Address_
UNION ALL
SELECT * FROM IBaseM.vNew_Id_Old_Id_Policy_
UNION ALL
SELECT * FROM IBaseM.vNew_Id_Old_Id_Fraud_Ring
UNION ALL
SELECT * FROM IBaseM.vNew_Id_Old_Id_Telephone_
UNION ALL
SELECT * FROM IBaseM.vNew_Id_Old_Id_Intelligence_
UNION ALL
SELECT * FROM IBaseM.vNew_Id_Old_Id_Person_
UNION ALL
SELECT * FROM IBaseM.vNew_Id_Old_Id_Account_
UNION ALL
SELECT * FROM IBaseM.vNew_Id_Old_Id_Payment_Card
UNION ALL
SELECT * FROM IBaseM.vNew_Id_Old_Id_Organisation_
