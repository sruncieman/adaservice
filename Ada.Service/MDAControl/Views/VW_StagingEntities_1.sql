﻿CREATE VIEW [IBaseM8Cur].[VW_StagingEntities]
AS

SELECT AccountID_ IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM  IBaseM8Cur.Account_
UNION ALL
SELECT Address_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Address_
UNION ALL
SELECT BB_Pin_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.BBPin_
UNION ALL
SELECT Driving_Licence_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Driving_Licence
UNION ALL
SELECT Email_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Email_
UNION ALL
SELECT Fraud_ring_Id IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Fraud_Ring
UNION ALL
SELECT Incident_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Incident_
UNION ALL
SELECT IP_Address_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.IP_Address
UNION ALL
SELECT Intelligence_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate  FROM IBaseM8Cur.Intelligence_
UNION ALL
SELECT NI_Number_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate  FROM IBaseM8Cur.NI_Number
UNION ALL
SELECT Organisation_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate  FROM IBaseM8Cur.Organisation_
UNION ALL
SELECT Passport_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Passport_
UNION ALL
SELECT Payment_Card_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Payment_Card
UNION ALL
SELECT Person_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Person_
UNION ALL
SELECT Policy_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Policy_
UNION ALL
SELECT Vehicle_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Vehicle_
UNION ALL
SELECT Website_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Website_
UNION ALL
SELECT Keoghs_Case_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Keoghs_Case
UNION ALL
SELECT Telephone_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.Telephone_
UNION ALL
SELECT TOG_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Cur.TOG_
