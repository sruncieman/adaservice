﻿CREATE VIEW [IBaseM].[Incident_Link]
AS
SELECT    LE.Entity_ID1, LE.Entity_ID2, L.Incident_Link_ID, L.Party_Type, L.Sub_Party_Type, CONVERT(TIME, L.Incident_Time) AS IncidentTime, L.Incident_Circs, 
          L.Incident_Location, L.Insurer_, L.Claim_Number, L.Claim_Status, L.Claim_Type, L.Claim_Code, L.MoJ_Status, L.Claim_Notification_Date, L.Broker_, 
          L.Referral_Source, L.Solicitors_, L.Engineer_, L.Recovery_, L.Storage_, L.Storage_Address, L.Repairer_, L.Hire_, L.Accident_Management, L.Medical_Examiner, 
          L.Police_Attended, L.Police_Force, L.Police_Reference, L.Ambulance_Attended, L.Source_411765484, L.Source_Reference_411765487, 
          L.Source_Description_411765489, L.Create_User, L.Create_Date, L.Last_Upd_User, L.Last_Upd_Date, LE.Link_ID AS IBaseId, L.x5x5x5_Grading_412284402
FROM IBaseM8Cur._LinkEnd AS LE 
INNER JOIN IBaseM8Cur.Incident_Link AS L ON LE.Link_ID = L.Unique_ID
