﻿
CREATE VIEW [IBaseM].[vPolicy_Plus_LinkEnd]
AS
SELECT     'PO0' + CONVERT(VARCHAR(20), DENSE_RANK() OVER (ORDER BY Unique_ID)) AS New_Unique_ID, *
FROM         (SELECT     N.Unique_ID, N.AltEntity, N.Create_Date, N.Create_User, N.DB_Source_387004609, N.Description_, N.Last_Upd_Date, N.Last_Upd_User, N.Record_Status, N.SCC, N.Status_Binding, N.IconColour, N.Forwarded_Record_Column , e.Link_ID, REPLACE(e.Entity_ID1, 'INS', 'POL') AS Entity_ID1, e.Confidence, e.Direction, REPLACE(e.Entity_ID2, 'INS', 'POL') AS Entity_ID2, 
                                              CASE e.EntityType_ID1 WHEN 1670 THEN 2261 /*POL*/ WHEN 1005 THEN 1893 /*ORG*/ END AS EntityType_ID1, 
                                              CASE e.EntityType_ID2 WHEN 1670 THEN 2261 /*POL*/ WHEN 1005 THEN 1893 /*ORG*/ END AS EntityType_ID2, 2756 AS LinkType_ID, 
                                              e.Record_Status AS EL_Record_Status, e.Record_Type, e.SCC AS EL_SCC
                       FROM          IBaseM5Cur.Nominal_Link AS n WITH (nolock) INNER JOIN
                                              IBaseM5Cur._LinkEnd AS e WITH (nolock) ON n.Unique_ID = e.Link_ID AND (e.Entity_ID1 LIKE 'INS%' AND e.Entity_ID2 LIKE 'ORG%' OR
                                              e.Entity_ID1 LIKE 'ORG%' AND e.Entity_ID2 LIKE 'INS%')
                       UNION ALL
                       SELECT     N.Unique_ID, N.AltEntity, N.Create_Date, N.Create_User, N.DB_Source_387004609, N.Description_, N.Last_Upd_Date, N.Last_Upd_User, N.Record_Status, N.SCC, N.Status_Binding, N.IconColour, N.Forwarded_Record_Column, e.Link_ID, REPLACE(e.Entity_ID1, 'INS', 'POL') AS Entity_ID1, e.Confidence, e.Direction, REPLACE(e.Entity_ID2, 'INS', 'POL') AS Entity_ID2, 
                                             CASE e.EntityType_ID1 WHEN 1670 THEN 2261 /*POL*/ WHEN 1001 THEN 1969 /*PER*/ END AS EntityType_ID1, 
                                             CASE e.EntityType_ID2 WHEN 1670 THEN 2261 /*POL*/ WHEN 1001 THEN 1969 /*PER*/ END AS EntityType_ID2, 2756 AS LinkType_ID, 
                                             e.Record_Status AS EL_Record_Status, e.Record_Type, e.SCC AS EL_SCC
                       FROM         IBaseM5Cur.Nominal_Link n WITH (nolock) INNER JOIN
                                             IBaseM5Cur._LinkEnd e WITH (nolock) ON n.Unique_ID = e.Link_ID AND (e.Entity_ID1 LIKE 'INS%' AND e.Entity_ID2 LIKE 'PER%' OR
                                             e.Entity_ID1 LIKE 'PER%' AND e.Entity_ID2 LIKE 'INS%')
                       UNION ALL
                       SELECT     N.Unique_ID, N.AltEntity, N.Create_Date, N.Create_User, N.DB_Source_387004609, N.Description_, N.Last_Upd_Date, N.Last_Upd_User, N.Record_Status, N.SCC, N.Status_Binding, N.IconColour, N.Forwarded_Record_Column, e.Link_ID, REPLACE(e.Entity_ID1, 'INS', 'POL') AS Entity_ID1, e.Confidence, e.Direction, REPLACE(e.Entity_ID2, 'INS', 'POL') AS Entity_ID2, 
                                             CASE e.EntityType_ID1 WHEN 1670 THEN 2261 /*POL*/ WHEN 1001 THEN 1969 /*PER*/ END AS EntityType_ID1, 
                                             CASE e.EntityType_ID2 WHEN 1670 THEN 2261 /*POL*/ WHEN 1001 THEN 1969 /*PER*/ END AS EntityType_ID2, 2756 AS LinkType_ID, 
                                             e.Record_Status AS EL_Record_Status, e.Record_Type, e.SCC AS EL_SCC
                       FROM         IBaseM5Cur.Finance_Link n WITH (nolock) INNER JOIN
                                             IBaseM5Cur._LinkEnd e WITH (nolock) ON n.Unique_ID = e.Link_ID AND (e.Entity_ID1 LIKE 'INS%' AND e.Entity_ID2 LIKE 'PER%' OR
                                             e.Entity_ID1 LIKE 'PER%' AND e.Entity_ID2 LIKE 'INS%')
                       UNION ALL
                       SELECT     N.Unique_ID, N.AltEntity, N.Create_Date, N.Create_User, N.DB_Source_387004609, N.Description_, N.Last_Upd_Date, N.Last_Upd_User, N.Record_Status, N.SCC, N.Status_Binding, N.IconColour, N.Forwarded_Record_Column, e.Link_ID, REPLACE(e.Entity_ID1, 'INS', 'POL') AS Entity_ID1, e.Confidence, e.Direction, REPLACE(e.Entity_ID2, 'INS', 'POL') AS Entity_ID2, 
                                             CASE e.EntityType_ID1 WHEN 1670 THEN 2261 /*POL*/ WHEN 1005 THEN 1893 /*ORG*/ END AS EntityType_ID1, 
                                             CASE e.EntityType_ID2 WHEN 1670 THEN 2261 /*POL*/ WHEN 1005 THEN 1893 /*ORG*/ END AS EntityType_ID2, 2756 AS LinkType_ID, 
                                             e.Record_Status AS EL_Record_Status, e.Record_Type, e.SCC AS EL_SCC
                       FROM         IBaseM5Cur.Finance_Link n WITH (nolock) INNER JOIN
                                             IBaseM5Cur._LinkEnd e WITH (nolock) ON n.Unique_ID = e.Link_ID AND (e.Entity_ID1 LIKE 'INS%' AND e.Entity_ID2 LIKE 'ORG%' OR
                                             e.Entity_ID1 LIKE 'ORG%' AND e.Entity_ID2 LIKE 'INS%')
                       UNION ALL
                       SELECT     N.Unique_ID, N.AltEntity, N.Create_Date, N.Create_User, N.DB_Source_387004609, N.Description_, N.Last_Upd_Date, N.Last_Upd_User, N.Record_Status, N.SCC, N.Status_Binding, N.IconColour, N.Forwarded_Record_Column, e.Link_ID, REPLACE(e.Entity_ID1, 'INS', 'POL') AS Entity_ID1, e.Confidence, e.Direction, REPLACE(e.Entity_ID2, 'INS', 'POL') AS Entity_ID2, 
                                             CASE e.EntityType_ID1 WHEN 1670 THEN 2261 /*POL*/ WHEN 1002 THEN 2321 /*VEH*/ END AS EntityType_ID1, 
                                             CASE e.EntityType_ID2 WHEN 1670 THEN 2261 /*POL*/ WHEN 1002 THEN 2321 /*VEH*/ END AS EntityType_ID2, 2756 AS LinkType_ID, 
                                             e.Record_Status AS EL_Record_Status, e.Record_Type, e.SCC AS EL_SCC
                       FROM         IBaseM5Cur.Vehicle_Link n WITH (nolock) INNER JOIN
                                             IBaseM5Cur._LinkEnd e WITH (nolock) ON n.Unique_ID = e.Link_ID AND (e.Entity_ID1 LIKE 'INS%' AND e.Entity_ID2 LIKE 'VEH%' OR
                                             e.Entity_ID1 LIKE 'VEH%' AND e.Entity_ID2 LIKE 'INS%')) X

