﻿CREATE VIEW [IBaseM8Cur].[VW_StagingLinks]

AS

SELECT Address_to_Address_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Address_to_Address_Link
UNION ALL
SELECT Case_Incident_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Case_to_Incident_Link
UNION ALL
SELECT Fraud_Ring_Incident_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Fraud_Ring_Incident_Link
UNION ALL
SELECT Incident_Match_Link_ID IBase5LinkID ,Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Incident_Match_Link
UNION ALL
SELECT Account_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Account_Link
UNION ALL
SELECT Policy_Payment_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Policy_Payment_Link
UNION ALL
SELECT Email_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Email_Link
UNION ALL
SELECT Vehicle_Incident_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Vehicle_Incident_Link
UNION ALL
SELECT IP_Address_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.IP_Address_Link
UNION ALL
SELECT Organisation_Match_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Organisation_Match_Link
UNION ALL
SELECT Payment_Card_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Payment_Card_Link
UNION ALL
SELECT Telephone_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Telephone_Link
UNION ALL
SELECT BB_Pin_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.BB_Pin_Link
UNION ALL
SELECT Driving_Licence_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Driving_Licence_Link
UNION ALL
SELECT Incident_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Incident_Link
UNION ALL
SELECT NI_Number_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.NI_Number_Link
UNION ALL
SELECT Policy_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Policy_Link
UNION ALL
SELECT Website_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Website_Link
UNION ALL
SELECT Person_to_Organisation_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Person_to_Organisation_Link
UNION ALL
SELECT Passport_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Passport_Link
UNION ALL
SELECT Person_to_Person_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Person_to_Person_Link
UNION ALL
SELECT Vehicle_Link_ID IBase5LinkID, Unique_ID IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Vehicle_Link
UNION ALL
SELECT Vehicle_to_Vehicle_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Vehicle_to_Vehicle_Link
UNION ALL
SELECT Intelligence_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Intelligence_Link
--UNION ALL
--SELECT Outcome_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.Outcome_Link
UNION ALL
SELECT TOG_Link_ID IBase5LinkID, Unique_Id IBase8LinkIDStaging, Source_Reference_411765487 Keoghs_Elite_Reference, Source_Description_411765489 Source_Description, Last_Upd_Date ModifiedDate, Risk_Claim_ID_414244883 RiskClaim_Id FROM IBaseM8Cur.TOG_Link
