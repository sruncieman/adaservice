﻿CREATE VIEW [IBaseM8Pre].[VW_StagingEntities]
AS

SELECT AccountID_ IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM  IBaseM8Pre.Account_
UNION ALL
SELECT Address_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Address_
UNION ALL
SELECT BB_Pin_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.BBPin_
UNION ALL
SELECT Driving_Licence_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Driving_Licence
UNION ALL
SELECT Email_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Email_
UNION ALL
SELECT Fraud_ring_Id IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Fraud_Ring
UNION ALL
SELECT Incident_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Incident_
UNION ALL
SELECT IP_Address_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.IP_Address
UNION ALL
SELECT Intelligence_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate  FROM IBaseM8Pre.Intelligence_
UNION ALL
SELECT NI_Number_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate  FROM IBaseM8Pre.NI_Number
UNION ALL
SELECT Organisation_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate  FROM IBaseM8Pre.Organisation_
UNION ALL
SELECT Passport_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Passport_
UNION ALL
SELECT Payment_Card_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Payment_Card
UNION ALL
SELECT Person_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Person_
UNION ALL
SELECT Policy_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Policy_
UNION ALL
SELECT Vehicle_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Vehicle_
UNION ALL
SELECT Website_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Website_
UNION ALL
SELECT Keoghs_Case_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Keoghs_Case
UNION ALL
SELECT Telephone_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.Telephone_
UNION ALL
SELECT TOG_ID IBase5EntityID, Unique_ID IBase8EntityIDStaging, Last_Upd_Date ModifiedDate FROM IBaseM8Pre.TOG_
