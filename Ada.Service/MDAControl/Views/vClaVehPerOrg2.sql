﻿CREATE VIEW [IBaseM].[vClaVehPerOrg2]

AS

SELECT
        KEO,
        Entity_ID1 AS CLA,
        Entity_ID1_VP AS VEH,
        Entity_ID1_PO AS PER,
        Entity_ID2_PO AS ORG,
        --C.DB_Source_387004609 AS CLAIM_DB_Source_,
        PO.DB_Source_387004609 AS Person_Organisation_DB_Source_,
        Keoghs_ELITE_Ref,
        Keoghs_AXIA_Ref,
        VP.Description_ AS VP_Description_,
        PO.Description_ AS PO_Description_,
        CV.Link_ID AS CV_Link_ID,
        VP.Link_ID AS VP_Link_ID,
        PO.Link_ID AS PO_Link_ID,

        PATINDEX('%' + Keoghs_ELITE_Ref + '%' , PO.DB_Source_387004609) AS ELITE_Match1,
        PATINDEX('%' + PO.DB_Source_387004609 + '%' , Keoghs_ELITE_Ref) AS ELITE_Match2,
        PATINDEX( '%' + Keoghs_AXIA_Ref  + '%', PO.DB_Source_387004609) AS AXIA_Match1,
        PATINDEX('%' + PO.DB_Source_387004609 + '%' , Keoghs_AXIA_Ref) AS AXIA_Match2,
        CASE 
                        WHEN
                                        PATINDEX('%' + Keoghs_ELITE_Ref + '%' , PO.DB_Source_387004609) > 0 THEN 1
                        WHEN
                                        PATINDEX('%' + PO.DB_Source_387004609 + '%' , Keoghs_ELITE_Ref) > 0 THEN 2
                        WHEN
                                        PATINDEX( '%' + Keoghs_AXIA_Ref  + '%', PO.DB_Source_387004609)  > 0 THEN  3
                        WHEN
                                        PATINDEX('%' + PO.DB_Source_387004609 + '%' , Keoghs_AXIA_Ref)   > 0 THEN 4
        END AS MatchType,
        
        CASE 
                        WHEN
                                        PATINDEX('%' + Keoghs_ELITE_Ref + '%' , PO.DB_Source_387004609) > 0 THEN Keoghs_ELITE_Ref
                        WHEN
                                        PATINDEX('%' + PO.DB_Source_387004609 + '%' , Keoghs_ELITE_Ref) > 0 THEN PO.DB_Source_387004609
                        WHEN
                                        PATINDEX( '%' + Keoghs_AXIA_Ref  + '%', PO.DB_Source_387004609)  > 0 THEN  Keoghs_AXIA_Ref
                        WHEN
                                        PATINDEX('%' + PO.DB_Source_387004609 + '%' , Keoghs_AXIA_Ref)   > 0 THEN  PO.DB_Source_387004609
        END AS USE_REF
                           
FROM  
                IBaseM.KIO_LinkEnd AS CV  WITH (NOLOCK)
--INNER JOIN
--             Claim_ C   WITH (NOLOCK)
--ON CV.Link_ID = C.Unique_ID
                INNER JOIN
                
(SELECT 
                Link_ID,
                KEO        AS KEO_VP,
                Entity_ID1 AS Entity_ID1_VP,
                Entity_ID2 AS Entity_ID2_VP,
                N.Description_
FROM  
                IBaseM.KIO_LinkEnd AS CV  WITH (NOLOCK)
INNER JOIN 
                IBaseM5Cur.Nominal_Link N   WITH (NOLOCK)
ON CV.Link_ID = N.Unique_ID
WHERE 
                Entity_ID1 LIKE 'VEH%'
                AND
                Entity_ID2 LIKE 'PER%') AS VP
                
                ON CV.Entity_ID2 = VP.Entity_ID1_VP
                AND CV.KEO = VP.KEO_VP
                
                INNER JOIN
                
(SELECT
                CV.Link_ID,
                KEO        AS KEO_PO,
                Entity_ID1 AS Entity_ID1_PO,
                Entity_ID2 AS Entity_ID2_PO,
                DB_Source_387004609,
                N.Description_
FROM  
                IBaseM.KIO_LinkEnd AS CV  WITH (NOLOCK)
INNER JOIN 
                IBaseM5Cur.Nominal_Link N   WITH (NOLOCK)
ON CV.Link_ID = N.Unique_ID
WHERE 
                Entity_ID1 LIKE 'PER%'
                AND
                Entity_ID2 LIKE 'ORG%') AS PO
                
ON VP.Entity_ID2_VP = PO.Entity_ID1_PO
AND CV.KEO = PO.KEO_PO

WHERE 
                Entity_ID1 LIKE 'CLA%'
                AND
                Entity_ID2 LIKE 'VEH%'
