﻿CREATE VIEW 

	[IBaseM].[v_LinkEnd_Keo2]

AS

SELECT   
	B.Link_ID AS Entity_ID1_Link_ID, 
	B.Entity_ID2  AS Entity_ID1_Keo, 
	A.*, C.Link_ID AS Entity_ID2_Link_ID,  
	C.Entity_ID2 AS Entity_ID2_Keo,
	B.Record_Status Entity_ID1_Record_Status,
	C.Record_Status Entity_ID2_Record_Status,
	D.Record_Status Reference_Link_Record_Status
FROM IBaseM5Cur._LinkEnd A WITH (NOLOCK) 
INNER JOIN IBaseM5Cur._LinkEnd B  WITH (NOLOCK)
		ON A.Entity_ID1 = B.Entity_ID1
		AND B.Entity_ID2 LIKE 'KEO%'
INNER JOIN IBaseM5Cur._LinkEnd C  WITH (NOLOCK)
		ON A.Entity_ID2 = C.Entity_ID1
		AND C.Entity_ID2 LIKE 'KEO%'
		AND B.Entity_ID2 = C.Entity_ID2 			
LEFT JOIN IBaseM5Cur.Reference_Link D  WITH (NOLOCK)
		ON B.Link_ID = D.Unique_ID
