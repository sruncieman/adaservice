﻿CREATE VIEW [IBaseM].[vGetClaimAddress]


AS

SELECT  DISTINCT 
	Ent2 Id,
	REPLACE('#' + CASE WHEN Property_No IS NOT NULL THEN   ', ' + Property_No + ' ' ELSE ''  END + CASE WHEN Property_Name IS NOT NULL THEN  ', ' + Property_Name ELSE ''  END + CASE WHEN Town_ IS NOT NULL THEN   ', ' + Town_  ELSE ''  END + CASE WHEN Post_Code IS NOT NULL THEN   ', ' + Post_Code     ELSE ''  END, '#, ', '') AS _ADDRESS
	FROM         
		(SELECT DISTINCT KEO.Entity_ID1 KRef, LE.Link_ID Link, LE.Entity_ID1 Ent1, LE.Entity_ID2 Ent2
			FROM 							
				IBaseM5Cur._LinkEnd KEO  WITH (NOLOCK)
			INNER JOIN 
				IBaseM5Cur._LinkEnd LE  WITH (NOLOCK)
				 ON KEO.Entity_ID2 = LE.Entity_ID1 
				 AND LEFT(LE.Entity_ID1, 3) = 'LOC'
				 AND LEFT(LE.Entity_ID2, 3) = 'CLA'
			LEFT JOIN 
				IBaseM5Cur.Nominal_Link N  WITH (NOLOCK)
				 ON LE.Link_ID = N.Unique_ID
			WHERE 
				LEFT(KEO.Entity_ID1, 3) = 'KEO' 
				AND (KEO.Record_Status !=  254)  
				AND (LE.Record_Status !=   254)) AS E
	INNER JOIN
		IBaseM5Cur.Location_Link AS L WITH (NOLOCK) 
		 ON E.Link = L.Unique_ID COLLATE DATABASE_DEFAULT 
	INNER JOIN
		 IBaseM5Cur.Location_ AS L1 WITH (NOLOCK)  
		  ON L1.Unique_ID = E.Ent1 COLLATE DATABASE_DEFAULT
	WHERE 
		L.Record_Status != 254 AND  L1.Record_Status != 254
