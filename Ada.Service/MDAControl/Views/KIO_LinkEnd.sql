﻿CREATE VIEW [IBaseM].[KIO_LinkEnd]

AS


SELECT K1.Entity_ID2 KEO,
       LE.*, KR.Keoghs_ELITE_Ref, KR.Keoghs_AXIA_Ref
                
FROM IBaseM5Cur._LinkEnd LE WITH (NOLOCK)
INNER JOIN
 IBaseM5Cur._LinkEnd K1  WITH (NOLOCK)
 ON LE.Entity_ID1 = K1.Entity_ID1
 AND K1.Entity_ID2 LIKE 'KEO%'
INNER JOIN
 IBaseM5Cur.Keoghs_Ref KR  WITH (NOLOCK)
 ON KR.Unique_ID = K1.Entity_ID2
 
INNER JOIN
 IBaseM5Cur._LinkEnd K2 WITH (NOLOCK)
 ON LE.Entity_ID2 = K2.Entity_ID1
 AND K2.Entity_ID2 LIKE 'KEO%'
 AND K1.Entity_ID2 = K2.Entity_ID2
 AND LE.Record_Status = 0
 AND K1.Record_Status = 0
 AND K1.Record_Status = 0
 AND KR.Record_Status = 0
