﻿CREATE TABLE [dbo].[DBControlRun] (
    [RunId]            INT            IdENTITY (1, 1) NOT NULL,
    [RunNameId]        INT            NOT NULL,
    [RunStartTime]     DATETIME       CONSTRAINT [DF_DBControlRun_RunStartTime] DEFAULT (getdate()) NOT NULL,
    [RunEndTime]       DATETIME       NULL,
    [RunDuration]      AS             (datediff(second,[RunStartTime],[RunEndTime])),
    [Error]            INT            NULL,
    [ErrorDescription] VARCHAR (1000) NULL,
    [Details]          VARCHAR (1000) NULL,
    CONSTRAINT [PK_DBControlRun_RunId_C] PRIMARY KEY CLUSTERED ([RunId] ASC),
    CONSTRAINT [FK_DBControlRun_DBControlName_NameId] FOREIGN KEY ([RunNameId]) REFERENCES [dbo].[DBControlName] ([NameId])
);

