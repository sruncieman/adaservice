﻿CREATE TABLE [Rpt].[ReportCreationOutputDeliveryConfiguration] (
    [Id]                      INT            IDENTITY (1, 1) NOT NULL,
    [ReportName_Id]           INT            NOT NULL,
    [RiskClient_Id]           INT            NOT NULL,
    [IsActive]                BIT            NOT NULL,
    [RiskRunExceedScore]      INT            NULL,
    [DestinationFileLocation] VARCHAR (200)  NULL,
    [DestinationFileName]     VARCHAR (200)  NULL,
    [DestinationFileFormat]   VARCHAR (10)   NULL,
    [EmailSubject]            VARCHAR (500)  NULL,
    [EmailBody_WithData]      VARCHAR (5000) NULL,
    [EmailBody_WithoutData]   VARCHAR (5000) NULL,
    [EmailFromProfile]        VARCHAR (50)   NULL,
    [EmailTo]                 VARCHAR (200)  NULL,
    [EmailCC]                 VARCHAR (200)  NULL,
    [EMailBCC]                VARCHAR (200)  NULL,
    [IgnoreBatchIdsTo]        INT            NULL,
    [FtpToUrl]                VARCHAR (200)  NULL,
    [FtpUsername]             VARCHAR (50)   NULL,
    [FtpPassword]             VARCHAR (50)   NULL,
    CONSTRAINT [PK_ReportCreationOutputDeliveryConfiguration_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ReportCreationOutputDeliveryConfiguration_ReportName_Id] FOREIGN KEY ([ReportName_Id]) REFERENCES [Rpt].[ReportName] ([Id])
);



