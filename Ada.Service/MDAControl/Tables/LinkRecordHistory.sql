﻿CREATE TABLE [Rep].[LinkRecordHistory] (
    [Id]                   INT          IdENTITY (1, 1) NOT NULL,
    [CompareDate]          DATETIME     NOT NULL,
    [DatabaseFromName]     VARCHAR (50) NULL,
    [DatabaseToName]       VARCHAR (50) NULL,
    [TableCode]            VARCHAR (3)  NULL,
    [IBase5LinkID]         VARCHAR (50) NULL,
    [IBase8LinkID_Staging] VARCHAR (50) NULL,
    [IBase8LinkID]         VARCHAR (50) NULL,
    [EliteReference]       VARCHAR (50) NULL,
    [RiskClaim_Id]         INT          NULL,
    [RecordAction]         VARCHAR (1)  NULL
);

