﻿CREATE TABLE [SyncDML].[MDAIBase_Address_] (
    [DMLId]                        BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]                       INT             NULL,
    [DMLAction]                    VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]             DATETIME        CONSTRAINT [DF_MDAIBase_Address__DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Unique_ID]                    NVARCHAR (2000) NULL,
    [Address_ID]                   NVARCHAR (2000) NULL,
    [Address_Type]                 NVARCHAR (2000) NULL,
    [AltEntity]                    NVARCHAR (2000) NULL,
    [Building_]                    NVARCHAR (2000) NULL,
    [Building_Number]              NVARCHAR (2000) NULL,
    [County_]                      NVARCHAR (2000) NULL,
    [Create_Date]                  NVARCHAR (2000) NULL,
    [Create_User]                  NVARCHAR (2000) NULL,
    [Do_Not_Disseminate_412284494] NVARCHAR (2000) NULL,
    [Document_Link]                VARBINARY (MAX) NULL,
    [Document_Link_Binding]        NVARCHAR (2000) NULL,
    [DX_Exchange]                  NVARCHAR (2000) NULL,
    [DX_Number]                    NVARCHAR (2000) NULL,
    [Grid_X]                       NVARCHAR (2000) NULL,
    [Grid_Y]                       NVARCHAR (2000) NULL,
    [IconColour]                   NVARCHAR (2000) NULL,
    [Key_Attractor_412284410]      NVARCHAR (2000) NULL,
    [Last_Upd_Date]                NVARCHAR (2000) NULL,
    [Last_Upd_User]                NVARCHAR (2000) NULL,
    [Locality_]                    NVARCHAR (2000) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (2000) NULL,
    [PAF_Validation]               NVARCHAR (2000) NULL,
    [Post_Code]                    NVARCHAR (2000) NULL,
    [Record_Status]                NVARCHAR (2000) NULL,
    [SCC]                          NVARCHAR (2000) NULL,
    [Source_411765484]             NVARCHAR (2000) NULL,
    [Source_Description_411765489] NVARCHAR (MAX)  NULL,
    [Source_Reference_411765487]   NVARCHAR (2000) NULL,
    [Status_Binding]               NVARCHAR (2000) NULL,
    [Street_]                      NVARCHAR (2000) NULL,
    [Sub_Building]                 NVARCHAR (2000) NULL,
    [Town_]                        NVARCHAR (2000) NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (2000) NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDAIBase_Address__DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Address__TaskId_DMLAction_NC]
    ON [SyncDML].[MDAIBase_Address_]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Address_DMLDateTimeStamp_NC]
    ON [SyncDML].[MDAIBase_Address_]([DMLDateTimeStamp] ASC);

