﻿CREATE TABLE [SyncDML].[MDAIBase_Organisation_] (
    [DMLId]                        BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]                       INT             NULL,
    [DMLAction]                    VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]             DATETIME        CONSTRAINT [DF_MDAIBase_Organisation__DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Unique_ID]                    NVARCHAR (2000) NULL,
    [AltEntity]                    NVARCHAR (2000) NULL,
    [Create_Date]                  NVARCHAR (2000) NULL,
    [Create_User]                  NVARCHAR (2000) NULL,
    [Credit_Licence]               NVARCHAR (2000) NULL,
    [Do_Not_Disseminate_412284494] NVARCHAR (2000) NULL,
    [Effective_Date_of_Status]     NVARCHAR (2000) NULL,
    [IconColour]                   NVARCHAR (2000) NULL,
    [Incorporated_Date]            NVARCHAR (2000) NULL,
    [Key_Attractor_412284410]      NVARCHAR (2000) NULL,
    [Last_Upd_Date]                NVARCHAR (2000) NULL,
    [Last_Upd_User]                NVARCHAR (2000) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (2000) NULL,
    [MoJ_CRM_Number]               NVARCHAR (2000) NULL,
    [MoJ_CRM_Status]               NVARCHAR (2000) NULL,
    [Organisation_ID]              NVARCHAR (2000) NULL,
    [Organisation_Name]            NVARCHAR (2000) NULL,
    [Organisation_Status]          NVARCHAR (2000) NULL,
    [Organisation_Type]            NVARCHAR (2000) NULL,
    [Record_Status]                NVARCHAR (2000) NULL,
    [Registered_Number]            NVARCHAR (2000) NULL,
    [SCC]                          NVARCHAR (2000) NULL,
    [SIC_]                         NVARCHAR (2000) NULL,
    [Source_411765484]             NVARCHAR (2000) NULL,
    [Source_Description_411765489] NVARCHAR (MAX)  NULL,
    [Source_Reference_411765487]   NVARCHAR (2000) NULL,
    [Status_Binding]               NVARCHAR (2000) NULL,
    [VAT_Number]                   NVARCHAR (2000) NULL,
    [VAT_Number_Validated_Date]    NVARCHAR (2000) NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (2000) NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDAIBase_Organisation__DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Organisation__TaskId_DMLAction_NC]
    ON [SyncDML].[MDAIBase_Organisation_]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Organisation_DMLDateTimeStamp_NC]
    ON [SyncDML].[MDAIBase_Organisation_]([DMLDateTimeStamp] ASC);

