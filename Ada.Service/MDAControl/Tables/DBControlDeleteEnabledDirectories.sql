﻿CREATE TABLE [dbo].[DBControlDeleteEnabledDirectories] (
    [Id]                           INT           IdENTITY (1, 1) NOT NULL,
    [DirectoryPath]                VARCHAR (200) NOT NULL,
    [MinimumRetentionPeriodInDays] INT           NOT NULL,
    CONSTRAINT [PK_DBControlDeleteEnabledDirectories] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [CK_DirectoryPath] CHECK (len([DirectoryPath])>(10)),
    CONSTRAINT [CK_MinimumRetentionPeriodInDays] CHECK ([MinimumRetentionPeriodInDays]>(0))
);

