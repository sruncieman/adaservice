﻿CREATE TABLE [IBaseM].[BackupRestoreConfig] (
    [ID]                        INT           IDENTITY (1, 1) NOT NULL,
    [BackupDatabaseName_ID]     INT           NOT NULL,
    [BackupLocation]            VARCHAR (255) NOT NULL,
    [DataFileLocation]          VARCHAR (255) NOT NULL,
    [SecondaryDataFileLocation] VARCHAR (255) NULL,
    [LogFileLocation]           VARCHAR (255) NOT NULL,
    [LiveBackupDirectory]       VARCHAR (255) NOT NULL,
    [LiveBackupPath]            VARCHAR (255) NOT NULL,
    [LiveBackupName]            VARCHAR (255) NOT NULL,
    [LiveDataFileName]          VARCHAR (255) NOT NULL,
    [LiveSecondaryDataFileName] VARCHAR (255) NULL,
    [LiveLogFileName]           VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_BackupRestoreConfig_ID_C] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_BackupRestoreConfig_BackupDatabaseName_ID] FOREIGN KEY ([BackupDatabaseName_ID]) REFERENCES [IBaseM].[BackupDatabaseName] ([ID])
);



