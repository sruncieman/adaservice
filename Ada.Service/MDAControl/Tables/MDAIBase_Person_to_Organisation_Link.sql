﻿CREATE TABLE [SyncDML].[MDAIBase_Person_to_Organisation_Link] (
    [DMLId]                          BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]                         INT             NULL,
    [DMLAction]                      VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]               DATETIME        CONSTRAINT [DF_MDAIBase_Person_to_Organisation_Link_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Unique_ID]                      NVARCHAR (2000) NULL,
    [AltEntity]                      NVARCHAR (2000) NULL,
    [Create_Date]                    NVARCHAR (2000) NULL,
    [Create_User]                    NVARCHAR (2000) NULL,
    [Do_Not_Disseminate_412284494]   NVARCHAR (2000) NULL,
    [IconColour]                     NVARCHAR (2000) NULL,
    [Key_Attractor_412284410]        NVARCHAR (2000) NULL,
    [Last_Upd_Date]                  NVARCHAR (2000) NULL,
    [Last_Upd_User]                  NVARCHAR (2000) NULL,
    [Link_Type]                      NVARCHAR (2000) NULL,
    [MDA_Incident_ID_412284502]      NVARCHAR (2000) NULL,
    [Person_to_Organisation_Link_ID] NVARCHAR (2000) NULL,
    [Record_Status]                  NVARCHAR (2000) NULL,
    [SCC]                            NVARCHAR (2000) NULL,
    [Source_411765484]               NVARCHAR (2000) NULL,
    [Source_Description_411765489]   NVARCHAR (MAX)  NULL,
    [Source_Reference_411765487]     NVARCHAR (2000) NULL,
    [Status_Binding]                 NVARCHAR (2000) NULL,
    [x5x5x5_Grading_412284402]       NVARCHAR (2000) NULL,
    [Risk_Claim_ID_414244883]        NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDAIBase_Person_to_Organisation_Link_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Person_to_Organisation_Link_TaskId_DMLAction_NC]
    ON [SyncDML].[MDAIBase_Person_to_Organisation_Link]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Person_to_Organisation_LinkDMLDateTimeStamp_NC]
    ON [SyncDML].[MDAIBase_Person_to_Organisation_Link]([DMLDateTimeStamp] ASC);

