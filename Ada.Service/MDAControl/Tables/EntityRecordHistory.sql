﻿CREATE TABLE [Rep].[EntityRecordHistory] (
    [Id]                     INT          IdENTITY (1, 1) NOT NULL,
    [CompareDate]            DATETIME     NOT NULL,
    [DatabaseFromName]       VARCHAR (50) NULL,
    [DatabaseToName]         VARCHAR (50) NULL,
    [TableCode]              VARCHAR (3)  NULL,
    [IBase5EntityID]         VARCHAR (50) NULL,
    [IBase8EntityID_Staging] VARCHAR (50) NULL,
    [IBase8EntityID]         VARCHAR (50) NULL,
    [RecordAction]           VARCHAR (1)  NULL
);

