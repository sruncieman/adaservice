﻿CREATE TABLE [IBaseM].[BackupDatabaseName] (
    [Id]                   INT           IdENTITY (1, 1) NOT NULL,
    [DatabaseName]         VARCHAR (100) NOT NULL,
    [DatabasePhysicalName] VARCHAR (100) NULL,
    CONSTRAINT [PK_BackupDatabaseName_Id_C] PRIMARY KEY CLUSTERED ([Id] ASC)
);

