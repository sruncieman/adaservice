﻿CREATE TABLE [SyncDML].[MDA_FraudRing] (
    [DMLId]             BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]            INT             NULL,
    [DMLAction]         VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]  DATETIME        CONSTRAINT [DF_MDA_FraudRing_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                NVARCHAR (2000) NULL,
    [FraudRingId]       NVARCHAR (2000) NULL,
    [FraudRingName]     NVARCHAR (2000) NULL,
    [Status]            NVARCHAR (2000) NULL,
    [LeadAnalyst]       NVARCHAR (2000) NULL,
    [Champion]          NVARCHAR (2000) NULL,
    [BriefingDocument]  NVARCHAR (2000) NULL,
    [KeyAttractor]      NVARCHAR (2000) NULL,
    [Source]            NVARCHAR (2000) NULL,
    [SourceReference]   NVARCHAR (2000) NULL,
    [SourceDescription] NVARCHAR (2000) NULL,
    [CreatedBy]         NVARCHAR (2000) NULL,
    [CreatedDate]       NVARCHAR (2000) NULL,
    [ModifiedBy]        NVARCHAR (2000) NULL,
    [ModifiedDate]      NVARCHAR (2000) NULL,
    [IBaseId]           NVARCHAR (2000) NULL,
    [RecordStatus]      NVARCHAR (2000) NULL,
    [ADARecordStatus]   NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_FraudRing_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_FraudRing_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_FraudRing]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_FraudRingDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_FraudRing]([DMLDateTimeStamp] ASC);

