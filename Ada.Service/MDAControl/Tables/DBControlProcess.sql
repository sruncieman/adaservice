﻿CREATE TABLE [dbo].[DBControlProcess] (
    [ProcessID]        INT            IDENTITY (1, 1) NOT NULL,
    [RunID]            INT            CONSTRAINT [DF_DBControlProcess_RunID] DEFAULT ((0)) NOT NULL,
    [ProcessNameID]    INT            NOT NULL,
    [DatabaseID]       INT            NOT NULL,
    [ObjectID]         INT            NOT NULL,
    [ProcessStartTime] DATETIME       CONSTRAINT [DF_DBControlProcess_ProcessStartTime] DEFAULT (getdate()) NOT NULL,
    [ProcessEndTime]   DATETIME       NULL,
    [ProcessDuration]  AS             (datediff(second,[ProcessStartTime],[ProcessEndTime])),
    [Error]            INT            NULL,
    [ErrorDescription] VARCHAR (1000) NULL,
    [Details]          VARCHAR (1000) NULL,
    CONSTRAINT [PK_DBControlProcess_ProcessID_C] PRIMARY KEY CLUSTERED ([ProcessID] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [FK_DBControlProcess_DBControlName_NameID] FOREIGN KEY ([ProcessNameID]) REFERENCES [dbo].[DBControlName] ([NameID]),
    CONSTRAINT [FK_DBControlProcess_DBControlRun_RunID] FOREIGN KEY ([RunID]) REFERENCES [dbo].[DBControlRun] ([RunID])
);




GO
CREATE NONCLUSTERED INDEX [IX_DBControlProcess_RunID]
    ON [dbo].[DBControlProcess]([RunID] ASC);

