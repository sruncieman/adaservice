﻿CREATE TABLE [IBaseM].[SourceReferenceMatching] (
    [Id]            INT           IdENTITY (1, 1) NOT NULL,
    [ActionId]      TINYINT       NOT NULL,
    [PriorityGroup] INT           NULL,
    [Expression]    VARCHAR (100) NOT NULL,
    [StringLength]  INT           NULL,
    CONSTRAINT [PK_SourceReferenceMatching] PRIMARY KEY CLUSTERED ([Id] ASC, [ActionId] ASC)
);

