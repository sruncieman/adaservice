﻿CREATE TABLE [Rep].[EntityRecord] (
    [Id]                     INT          IdENTITY (1, 1) NOT NULL,
    [TableCode]              VARCHAR (3)  NULL,
    [IBase5EntityID]         VARCHAR (50) NULL,
    [IBase8EntityID_Staging] VARCHAR (50) NULL,
    [IBase8EntityID]         VARCHAR (50) NULL,
    [RecordAction]           VARCHAR (1)  NULL
);

