﻿CREATE TABLE [dbo].[DBControlTask] (
    [TaskID]           INT            IDENTITY (0, 1) NOT NULL,
    [ParentTaskID]     INT            NULL,
    [ProcessID]        INT            CONSTRAINT [DF_DBControlTask_ProcessID] DEFAULT ((0)) NOT NULL,
    [TaskNameID]       INT            NOT NULL,
    [DatabaseID]       INT            NULL,
    [ObjectID]         INT            NULL,
    [TableID]          INT            NULL,
    [TaskStartTime]    DATETIME       CONSTRAINT [DF_DBControlTask_TaskStartTime] DEFAULT (getdate()) NOT NULL,
    [TaskEndTime]      DATETIME       NULL,
    [TaskDuration]     AS             (datediff(second,[TaskStartTime],[TaskEndTime])),
    [InsertRowCount]   INT            NULL,
    [UpdateRowCount]   INT            NULL,
    [DeleteRowCount]   INT            NULL,
    [Error]            INT            NULL,
    [ErrorDescription] VARCHAR (1000) NULL,
    [Details]          VARCHAR (1000) NULL,
    CONSTRAINT [PK_DBControlTask_TaskID_C] PRIMARY KEY CLUSTERED ([TaskID] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [FK_DBControlTask_DBControlName_NameID] FOREIGN KEY ([TaskNameID]) REFERENCES [dbo].[DBControlName] ([NameID]),
    CONSTRAINT [FK_DBControlTask_DBControlProcess_ProcessID] FOREIGN KEY ([ProcessID]) REFERENCES [dbo].[DBControlProcess] ([ProcessID])
);




GO
CREATE NONCLUSTERED INDEX [IX_DBControlTask_ProcessID]
    ON [dbo].[DBControlTask]([ProcessID] ASC);

