﻿CREATE TABLE [SyncDML].[MDAIBase_Person_] (
    [DMLId]                        BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]                       INT             NULL,
    [DMLAction]                    VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]             DATETIME        CONSTRAINT [DF_MDAIBase_Person__DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Unique_ID]                    NVARCHAR (2000) NULL,
    [AltEntity]                    NVARCHAR (2000) NULL,
    [Create_Date]                  NVARCHAR (2000) NULL,
    [Create_User]                  NVARCHAR (2000) NULL,
    [Date_of_Birth]                NVARCHAR (2000) NULL,
    [Do_Not_Disseminate_412284494] NVARCHAR (2000) NULL,
    [Document_]                    NVARCHAR (MAX)  NULL,
    [Document__Binding]            NVARCHAR (2000) NULL,
    [First_Name]                   NVARCHAR (2000) NULL,
    [Gender_]                      NVARCHAR (2000) NULL,
    [Hobbies_]                     NVARCHAR (2000) NULL,
    [IconColour]                   NVARCHAR (2000) NULL,
    [Key_Attractor_412284410]      NVARCHAR (2000) NULL,
    [Last_Name]                    NVARCHAR (2000) NULL,
    [Last_Upd_Date]                NVARCHAR (2000) NULL,
    [Last_Upd_User]                NVARCHAR (2000) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (2000) NULL,
    [Middle_Name]                  NVARCHAR (2000) NULL,
    [Nationality_]                 NVARCHAR (2000) NULL,
    [Notes_]                       NVARCHAR (MAX)  NULL,
    [Occupation_]                  NVARCHAR (2000) NULL,
    [Person_ID]                    NVARCHAR (2000) NULL,
    [Picture_]                     NVARCHAR (MAX)  NULL,
    [Picture__Binding]             NVARCHAR (2000) NULL,
    [Record_Status]                NVARCHAR (2000) NULL,
    [Salutation_]                  NVARCHAR (2000) NULL,
    [SCC]                          NVARCHAR (2000) NULL,
    [Schools_]                     NVARCHAR (2000) NULL,
    [Source_411765484]             NVARCHAR (2000) NULL,
    [Source_Description_411765489] NVARCHAR (MAX)  NULL,
    [Source_Reference_411765487]   NVARCHAR (2000) NULL,
    [Status_Binding]               NVARCHAR (2000) NULL,
    [Taxi_Driver_Licence]          NVARCHAR (2000) NULL,
    [VF_ID]                        NVARCHAR (2000) NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (2000) NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDAIBase_Person__DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Person__TaskId_DMLAction_NC]
    ON [SyncDML].[MDAIBase_Person_]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Person_DMLDateTimeStamp_NC]
    ON [SyncDML].[MDAIBase_Person_]([DMLDateTimeStamp] ASC);

