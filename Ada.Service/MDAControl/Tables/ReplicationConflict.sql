﻿CREATE TABLE [Sync].[ReplicationConflict] (
    [MDA_Id]              INT          NULL,
    [MDA_IBase_Unique_ID] VARCHAR (20) NULL,
    [ObjectId]            INT          NULL,
    [AlertSent]           INT          NOT NULL,
    [ConflictDateTime]    DATETIME     NOT NULL
);

