﻿CREATE TABLE [IBaseM].[BackupFileLog] (
    [LoadId]                INT           IdENTITY (1, 1) NOT NULL,
    [BackupDatabaseName_Id] INT           NOT NULL,
    [FileDate]              DATETIME      NOT NULL,
    [FileSize]              VARCHAR (50)  NOT NULL,
    [FileName]              VARCHAR (50)  NOT NULL,
    [SystemUser]            VARCHAR (100) NOT NULL,
    [BackupValId]           INT           NOT NULL,
    CONSTRAINT [PK_BackupFileLog_C] PRIMARY KEY CLUSTERED ([LoadId] ASC),
    CONSTRAINT [FK_BackupFileLog_BackupDatabaseName_Id] FOREIGN KEY ([BackupDatabaseName_Id]) REFERENCES [IBaseM].[BackupDatabaseName] ([Id])
);

