﻿CREATE TABLE [SyncDML].[MDA_Policy] (
    [DMLId]                      BIGINT         IdENTITY (1, 1) NOT NULL,
    [TaskId]                     INT            NULL,
    [DMLAction]                  VARCHAR (2000) NULL,
    [DMLDateTimeStamp]           DATETIME       CONSTRAINT [DF_MDA_Policy_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                         VARCHAR (2000) NULL,
    [PolicyId]                   VARCHAR (2000) NULL,
    [PolicyNumber]               VARCHAR (2000) NULL,
    [Insurer]                    VARCHAR (2000) NULL,
    [InsurerTradingName]         VARCHAR (2000) NULL,
    [Broker]                     VARCHAR (2000) NULL,
    [PolicyType_Id]              VARCHAR (2000) NULL,
    [PolicyCoverType_Id]         VARCHAR (2000) NULL,
    [PolicyStartDate]            VARCHAR (2000) NULL,
    [PolicyEndDate]              VARCHAR (2000) NULL,
    [PreviousNoFaultClaimsCount] VARCHAR (2000) NULL,
    [PreviousFaultClaimsCount]   VARCHAR (2000) NULL,
    [Premium]                    VARCHAR (2000) NULL,
    [KeyAttractor]               VARCHAR (2000) NULL,
    [Source]                     VARCHAR (2000) NULL,
    [SourceReference]            VARCHAR (2000) NULL,
    [SourceDescription]          VARCHAR (2000) NULL,
    [CreatedBy]                  VARCHAR (2000) NULL,
    [CreatedDate]                VARCHAR (2000) NULL,
    [ModifiedBy]                 VARCHAR (2000) NULL,
    [ModifiedDate]               VARCHAR (2000) NULL,
    [IBaseId]                    VARCHAR (2000) NULL,
    [RecordStatus]               VARCHAR (2000) NULL,
    [ADARecordStatus]            VARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Policy_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Policy_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Policy]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_PolicyDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Policy]([DMLDateTimeStamp] ASC);

