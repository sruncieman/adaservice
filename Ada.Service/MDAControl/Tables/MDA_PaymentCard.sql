﻿CREATE TABLE [SyncDML].[MDA_PaymentCard] (
    [DMLId]              BIGINT         IdENTITY (1, 1) NOT NULL,
    [TaskId]             INT            NULL,
    [DMLAction]          VARCHAR (2000) NULL,
    [DMLDateTimeStamp]   DATETIME       CONSTRAINT [DF_MDA_PaymentCard_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                 VARCHAR (2000) NULL,
    [PaymentCardId]      VARCHAR (2000) NULL,
    [PaymentCardNumber]  VARCHAR (2000) NULL,
    [HashedCardNumber]   VARCHAR (2000) NULL,
    [SortCode]           VARCHAR (2000) NULL,
    [ExpiryDate]         VARCHAR (2000) NULL,
    [BankName]           VARCHAR (2000) NULL,
    [PaymentCardType_Id] VARCHAR (2000) NULL,
    [KeyAttractor]       VARCHAR (2000) NULL,
    [Source]             VARCHAR (2000) NULL,
    [SourceReference]    VARCHAR (2000) NULL,
    [SourceDescription]  VARCHAR (2000) NULL,
    [CreatedBy]          VARCHAR (2000) NULL,
    [CreatedDate]        VARCHAR (2000) NULL,
    [ModifiedBy]         VARCHAR (2000) NULL,
    [ModifiedDate]       VARCHAR (2000) NULL,
    [IBaseId]            VARCHAR (2000) NULL,
    [RecordStatus]       VARCHAR (2000) NULL,
    [ADARecordStatus]    VARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_PaymentCard_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_PaymentCard_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_PaymentCard]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_PaymentCardDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_PaymentCard]([DMLDateTimeStamp] ASC);

