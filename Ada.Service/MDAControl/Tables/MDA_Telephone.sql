﻿CREATE TABLE [SyncDML].[MDA_Telephone] (
    [DMLId]             BIGINT         IdENTITY (1, 1) NOT NULL,
    [TaskId]            INT            NULL,
    [DMLAction]         VARCHAR (2000) NULL,
    [DMLDateTimeStamp]  DATETIME       CONSTRAINT [DF_MDA_Telephone_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                VARCHAR (2000) NULL,
    [TelephoneId]       VARCHAR (2000) NULL,
    [TelephoneNumber]   VARCHAR (2000) NULL,
    [CountryCode]       VARCHAR (2000) NULL,
    [STDCode]           VARCHAR (2000) NULL,
    [TelephoneType_Id]  VARCHAR (2000) NULL,
    [KeyAttractor]      VARCHAR (2000) NULL,
    [Source]            VARCHAR (2000) NULL,
    [SourceReference]   VARCHAR (2000) NULL,
    [SourceDescription] VARCHAR (2000) NULL,
    [CreatedBy]         VARCHAR (2000) NULL,
    [CreatedDate]       VARCHAR (2000) NULL,
    [ModifiedBy]        VARCHAR (2000) NULL,
    [ModifiedDate]      VARCHAR (2000) NULL,
    [IBaseId]           VARCHAR (2000) NULL,
    [RecordStatus]      VARCHAR (2000) NULL,
    [ADARecordStatus]   VARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Telephone_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Telephone_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Telephone]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_TelephoneDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Telephone]([DMLDateTimeStamp] ASC);

