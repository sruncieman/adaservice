﻿CREATE TABLE [Rep].[IBaseLinkEnd] (
    [LinkId]          VARCHAR (50) NOT NULL,
    [EntityId1]       VARCHAR (50) NOT NULL,
    [EntityId2]       VARCHAR (50) NOT NULL,
    [IBase5LinkID]    VARCHAR (50) NOT NULL,
    [IBase5EntityID1] VARCHAR (50) NOT NULL,
    [IBase5EntityID2] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_IBaseLinkEnd_LinkId_EntityId1_C] PRIMARY KEY CLUSTERED ([LinkId] ASC, [EntityId1] ASC)
);

