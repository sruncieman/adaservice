﻿CREATE TABLE [IBaseM].[OutcomeDatafromDWH] (
    [Id]                      INT           IDENTITY (1, 1) NOT NULL,
    [Division]                VARCHAR (100) NULL,
    [Claimant_Name]           VARCHAR (256) NULL,
    [Claimant_Address1]       VARCHAR (250) NULL,
    [Claimant_Address2]       VARCHAR (250) NULL,
    [Claimant_DOB]            VARCHAR (30)  NULL,
    [claimId]                 VARCHAR (200) NULL,
    [Matter_Number]           VARCHAR (100) NULL,
    [VF_Manner_Of_Resolution] VARCHAR (500) NULL,
    [Matter_Opening_Date]     VARCHAR (30)  NULL,
    [Matter_Status_Desc]      VARCHAR (240) NULL,
    [Settlement_Date]         VARCHAR (30)  NULL,
    [Claimant_Title]          VARCHAR (30)  NULL,
    [Claimant_First_Name]     VARCHAR (100) NULL,
    [Claimant_Last_Name]      VARCHAR (100) NULL,
    [Claimant_Post_Code]      VARCHAR (50)  NULL,
    [Claimant_Key]            VARCHAR (50)  NULL,
    CONSTRAINT [PK_Key_Attractor_Staging] PRIMARY KEY CLUSTERED ([Id] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_OutcomeDatafromDWH_Claimant_First_Name_Claimant_Last_Name_Claimant_Post_Code_Matter_Number_Claimant_DOB]
    ON [IBaseM].[OutcomeDatafromDWH]([Claimant_First_Name] ASC, [Claimant_Last_Name] ASC, [Claimant_Post_Code] ASC, [Matter_Number] ASC, [Claimant_DOB] ASC);

