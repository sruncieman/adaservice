﻿CREATE TABLE [dbo].[DBControlSynchronisation] (
    [SynchronisationID] INT          IDENTITY (1, 1) NOT NULL,
    [StartDateTime]     DATETIME     CONSTRAINT [DF_DBControlSynchronisation_StartDateTime] DEFAULT (getdate()) NOT NULL,
    [EndDateTime]       DATETIME     NULL,
    [DurationMS]        AS           (datediff(millisecond,[StartDateTime],[EndDateTime])),
    [DurationHHMMSS]    VARCHAR (50) NULL,
    CONSTRAINT [PK_DBControlSynchronisation] PRIMARY KEY CLUSTERED ([SynchronisationID] ASC)
);

