﻿CREATE TABLE [SyncDML].[MDAIBase_Policy_Link] (
    [DMLId]                        BIGINT         IdENTITY (1, 1) NOT NULL,
    [TaskId]                       INT            NULL,
    [DMLAction]                    VARCHAR (2000) NULL,
    [DMLDateTimeStamp]             DATETIME       CONSTRAINT [DF_MDAIBase_Policy_Link_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Unique_ID]                    VARCHAR (2000) NULL,
    [AltEntity]                    VARCHAR (2000) NULL,
    [Create_Date]                  VARCHAR (2000) NULL,
    [Create_User]                  VARCHAR (2000) NULL,
    [Do_Not_Disseminate_412284494] VARCHAR (2000) NULL,
    [IconColour]                   VARCHAR (2000) NULL,
    [Key_Attractor_412284410]      VARCHAR (2000) NULL,
    [Last_Upd_Date]                VARCHAR (2000) NULL,
    [Last_Upd_User]                VARCHAR (2000) NULL,
    [Link_Type]                    VARCHAR (2000) NULL,
    [MDA_Incident_ID_412284502]    VARCHAR (2000) NULL,
    [Policy_Link_ID]               VARCHAR (2000) NULL,
    [Record_Status]                VARCHAR (2000) NULL,
    [SCC]                          VARCHAR (2000) NULL,
    [Source_411765484]             VARCHAR (2000) NULL,
    [Source_Description_411765489] VARCHAR (2000) NULL,
    [Source_Reference_411765487]   VARCHAR (2000) NULL,
    [Status_Binding]               VARCHAR (2000) NULL,
    [x5x5x5_Grading_412284402]     VARCHAR (2000) NULL,
    [Risk_Claim_ID_414244883]      VARCHAR (2000) NULL,
    CONSTRAINT [PK_MDAIBase_Policy_Link_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Policy_Link_TaskId_DMLAction_NC]
    ON [SyncDML].[MDAIBase_Policy_Link]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Policy_LinkDMLDateTimeStamp_NC]
    ON [SyncDML].[MDAIBase_Policy_Link]([DMLDateTimeStamp] ASC);

