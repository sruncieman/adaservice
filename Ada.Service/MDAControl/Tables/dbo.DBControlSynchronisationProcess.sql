﻿CREATE TABLE [dbo].[DBControlSynchronisationProcess] (
    [SynchronisationDetailID] INT           IDENTITY (1, 1) NOT NULL,
    [SynchronisationID]       INT           NOT NULL,
    [ProcessName]             VARCHAR (100) NOT NULL,
    [StartDateTime]           DATETIME      CONSTRAINT [DF_DBControlSynchronisationProcess_StartDateTime] DEFAULT (getdate()) NOT NULL,
    [EndDateTime]             DATETIME      NULL,
    [DurationMS]              AS            (datediff(millisecond,[StartDateTime],[EndDateTime])),
    [DurationHHMMSS]          VARCHAR (50)  NULL,
    [Details]                 VARCHAR (200) NULL,
    CONSTRAINT [PK_DBControlSynchronisationProcess] PRIMARY KEY CLUSTERED ([SynchronisationDetailID] ASC),
    CONSTRAINT [FK_DBControlSynchronisationProcess_DBControlSynchronisation] FOREIGN KEY ([SynchronisationID]) REFERENCES [dbo].[DBControlSynchronisation] ([SynchronisationID])
);

