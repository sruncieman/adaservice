﻿CREATE TABLE [SyncDML].[MDA_Organisation] (
    [DMLId]                   BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]                  INT             NULL,
    [DMLAction]               VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]        DATETIME        CONSTRAINT [DF_MDA_Organisation_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                      NVARCHAR (2000) NULL,
    [OrganisationId]          NVARCHAR (2000) NULL,
    [OrganisationName]        NVARCHAR (2000) NULL,
    [OrganisationType_Id]     NVARCHAR (2000) NULL,
    [IncorporatedDate]        NVARCHAR (2000) NULL,
    [OrganisationStatus_Id]   NVARCHAR (2000) NULL,
    [EffectiveDateOfStatus]   NVARCHAR (2000) NULL,
    [RegisteredNumber]        NVARCHAR (2000) NULL,
    [SIC]                     NVARCHAR (2000) NULL,
    [VATNumber]               NVARCHAR (2000) NULL,
    [VATNumberValidationDate] NVARCHAR (2000) NULL,
    [MojCRMNumber]            NVARCHAR (2000) NULL,
    [MojCRMStatus_Id]         NVARCHAR (2000) NULL,
    [CreditLicense]           NVARCHAR (2000) NULL,
    [KeyAttractor]            NVARCHAR (2000) NULL,
    [Source]                  NVARCHAR (2000) NULL,
    [SourceReference]         NVARCHAR (2000) NULL,
    [SourceDescription]       NVARCHAR (2000) NULL,
    [CreatedBy]               NVARCHAR (2000) NULL,
    [CreatedDate]             NVARCHAR (2000) NULL,
    [ModifiedBy]              NVARCHAR (2000) NULL,
    [ModifiedDate]            NVARCHAR (2000) NULL,
    [IBaseId]                 NVARCHAR (2000) NULL,
    [RecordStatus]            NVARCHAR (2000) NULL,
    [ADARecordStatus]         NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Organisation_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Organisation_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Organisation]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_OrganisationDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Organisation]([DMLDateTimeStamp] ASC);

