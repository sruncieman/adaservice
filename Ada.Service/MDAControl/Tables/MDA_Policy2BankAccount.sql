﻿CREATE TABLE [SyncDML].[MDA_Policy2BankAccount] (
    [DMLId]                   BIGINT         IdENTITY (1, 1) NOT NULL,
    [TaskId]                  INT            NULL,
    [DMLAction]               VARCHAR (2000) NULL,
    [DMLDateTimeStamp]        DATETIME       CONSTRAINT [DF_MDA_Policy2BankAccount_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                      VARCHAR (2000) NULL,
    [Policy_ID]               VARCHAR (2000) NULL,
    [BankAccount_Id]          VARCHAR (2000) NULL,
    [PolicyPaymentLinkId]     VARCHAR (2000) NULL,
    [PolicyPaymentType_Id]    VARCHAR (2000) NULL,
    [DatePaymentDetailsTaken] VARCHAR (2000) NULL,
    [FiveGrading]             VARCHAR (2000) NULL,
    [LinkConfidence]          VARCHAR (2000) NULL,
    [Source]                  VARCHAR (2000) NULL,
    [SourceReference]         VARCHAR (2000) NULL,
    [SourceDescription]       VARCHAR (2000) NULL,
    [CreatedBy]               VARCHAR (2000) NULL,
    [CreatedDate]             VARCHAR (2000) NULL,
    [ModifiedBy]              VARCHAR (2000) NULL,
    [ModifiedDate]            VARCHAR (2000) NULL,
    [RiskClaim_Id]            VARCHAR (2000) NULL,
    [BaseRiskClaim_Id]        VARCHAR (2000) NULL,
    [IBaseId]                 VARCHAR (2000) NULL,
    [RecordStatus]            VARCHAR (2000) NULL,
    [ADARecordStatus]         VARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Policy2BankAccount_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Policy2BankAccount_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Policy2BankAccount]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Policy2BankAccountDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Policy2BankAccount]([DMLDateTimeStamp] ASC);

