﻿CREATE TABLE [SyncDML].[MDA_Incident2OrganisationOutcome] (
    [DMLId]                    BIGINT         IdENTITY (1, 1) NOT NULL,
    [TaskId]                   INT            NULL,
    [DMLAction]                VARCHAR (2000) NULL,
    [DMLDateTimeStamp]         DATETIME       CONSTRAINT [DF_MDA_Incident2OrganisationOutcome_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                       VARCHAR (2000) NULL,
    [Incident_ID]              VARCHAR (2000) NULL,
    [Organisation_ID]          VARCHAR (2000) NULL,
    [OutcomeLinkId]            VARCHAR (2000) NULL,
    [SettlementDate]           VARCHAR (2000) NULL,
    [MannerOfResolution]       VARCHAR (2000) NULL,
    [EnforcementRole]          VARCHAR (2000) NULL,
    [PropertyOwned]            VARCHAR (2000) NULL,
    [MethodOfEnforcement]      VARCHAR (2000) NULL,
    [DateOfSettlementAgreed]   VARCHAR (2000) NULL,
    [SettlementAmountAgreed]   VARCHAR (2000) NULL,
    [AmountRecoveredToDate]    VARCHAR (2000) NULL,
    [AmountStillOutstanding]   VARCHAR (2000) NULL,
    [WasEnforcementSuccessful] VARCHAR (2000) NULL,
    [TypeOfSuccess]            VARCHAR (2000) NULL,
    [FiveGrading]              VARCHAR (2000) NULL,
    [LinkConfidence]           VARCHAR (2000) NULL,
    [Source]                   VARCHAR (2000) NULL,
    [SourceReference]          VARCHAR (2000) NULL,
    [SourceDescription]        VARCHAR (2000) NULL,
    [CreatedBy]                VARCHAR (2000) NULL,
    [CreatedDate]              VARCHAR (2000) NULL,
    [ModifiedBy]               VARCHAR (2000) NULL,
    [ModifiedDate]             VARCHAR (2000) NULL,
    [RiskClaim_Id]             VARCHAR (2000) NULL,
    [BaseRiskClaim_Id]         VARCHAR (2000) NULL,
    [IBaseId]                  VARCHAR (2000) NULL,
    [RecordStatus]             VARCHAR (2000) NULL,
    [ADARecordStatus]          VARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Incident2OrganisationOutcome_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Incident2OrganisationOutcome_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Incident2OrganisationOutcome]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Incident2OrganisationOutcomeDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Incident2OrganisationOutcome]([DMLDateTimeStamp] ASC);

