﻿CREATE TABLE [IBaseM].[PickLists] (
    [Ref]                       FLOAT (53)     NULL,
    [CodeList]                  NVARCHAR (255) NULL,
    [NewiBaseCodeListName]      NVARCHAR (255) NULL,
    [Item]                      NVARCHAR (255) NULL,
    [PreviousiBaseCodeListName] NVARCHAR (255) NULL,
    [Item1]                     NVARCHAR (255) NULL,
    [Notes]                     NVARCHAR (255) NULL
);

