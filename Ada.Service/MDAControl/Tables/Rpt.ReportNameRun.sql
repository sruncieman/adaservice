﻿CREATE TABLE [Rpt].[ReportNameRun] (
    [Id]                                           INT IDENTITY (1, 1) NOT NULL,
    [ReportCreationOutputDeliveryConfiguration_Id] INT NOT NULL,
    [DBControlRun_Id]                              INT NOT NULL,
    [BatchID]                                      INT NOT NULL,
    [Resend]                                       BIT CONSTRAINT [DF_ReportNameRun_Resend] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ReportNameRun_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ReportNameRun_DBControlRun_RunID] FOREIGN KEY ([DBControlRun_Id]) REFERENCES [dbo].[DBControlRun] ([RunID]),
    CONSTRAINT [FK_ReportNameRun_ReportCreationOutputDeliveryConfiguration_Id] FOREIGN KEY ([ReportCreationOutputDeliveryConfiguration_Id]) REFERENCES [Rpt].[ReportCreationOutputDeliveryConfiguration] ([Id])
);

