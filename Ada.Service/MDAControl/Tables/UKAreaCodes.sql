﻿CREATE TABLE [IBaseM].[UKAreaCodes] (
    [Code]                    VARCHAR (50)  NULL,
    [OfcomDefinition]         VARCHAR (500) NULL,
    [TraditionalBTDefinition] VARCHAR (500) NULL
);

