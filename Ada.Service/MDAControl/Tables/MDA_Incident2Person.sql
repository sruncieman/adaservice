﻿CREATE TABLE [SyncDML].[MDA_Incident2Person] (
    [DMLId]                    BIGINT          IDENTITY (1, 1) NOT NULL,
    [TaskId]                   INT             NULL,
    [DMLAction]                VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]         DATETIME        CONSTRAINT [DF_MDA_Incident2Person_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                       NVARCHAR (2000) NULL,
    [Incident_ID]              NVARCHAR (2000) NULL,
    [Person_ID]                NVARCHAR (2000) NULL,
    [IncidentLinkId]           NVARCHAR (2000) NULL,
    [PartyType_Id]             NVARCHAR (2000) NULL,
    [SubPartyType_Id]          NVARCHAR (2000) NULL,
    [MojStatus_Id]             NVARCHAR (2000) NULL,
    [IncidentTime]             NVARCHAR (2000) NULL,
    [IncidentCircs]            NVARCHAR (MAX)  NULL,
    [IncidentLocation]         NVARCHAR (2000) NULL,
    [Insurer]                  NVARCHAR (2000) NULL,
    [ClaimNumber]              NVARCHAR (2000) NULL,
    [ClaimStatus_Id]           NVARCHAR (2000) NULL,
    [ClaimType]                NVARCHAR (2000) NULL,
    [ClaimCode]                NVARCHAR (2000) NULL,
    [ClaimNotificationDate]    NVARCHAR (2000) NULL,
    [PaymentsToDate]           NVARCHAR (2000) NULL,
    [Reserve]                  NVARCHAR (2000) NULL,
    [Broker]                   NVARCHAR (2000) NULL,
    [ReferralSource]           NVARCHAR (2000) NULL,
    [Solicitors]               NVARCHAR (2000) NULL,
    [Engineer]                 NVARCHAR (2000) NULL,
    [Recovery]                 NVARCHAR (2000) NULL,
    [Storage]                  NVARCHAR (2000) NULL,
    [StorageAddress]           NVARCHAR (2000) NULL,
    [Repairer]                 NVARCHAR (2000) NULL,
    [Hire]                     NVARCHAR (2000) NULL,
    [AccidentManagement]       NVARCHAR (2000) NULL,
    [MedicalExaminer]          NVARCHAR (2000) NULL,
    [UndefinedSupplier]        NVARCHAR (2000) NULL,
    [PoliceAttended]           NVARCHAR (2000) NULL,
    [PoliceForce]              NVARCHAR (2000) NULL,
    [PoliceReference]          NVARCHAR (2000) NULL,
    [AmbulanceAttended]        NVARCHAR (2000) NULL,
    [AttendedHospital]         NVARCHAR (2000) NULL,
    [FiveGrading]              NVARCHAR (2000) NULL,
    [LinkConfidence]           NVARCHAR (2000) NULL,
    [Source]                   NVARCHAR (2000) NULL,
    [SourceReference]          NVARCHAR (2000) NULL,
    [SourceDescription]        NVARCHAR (2000) NULL,
    [CreatedBy]                NVARCHAR (2000) NULL,
    [CreatedDate]              NVARCHAR (2000) NULL,
    [ModifiedBy]               NVARCHAR (2000) NULL,
    [ModifiedDate]             NVARCHAR (2000) NULL,
    [RiskClaim_Id]             NVARCHAR (2000) NULL,
    [BaseRiskClaim_Id]         NVARCHAR (2000) NULL,
    [IBaseId]                  NVARCHAR (2000) NULL,
    [RecordStatus]             NVARCHAR (2000) NULL,
    [ADARecordStatus]          NVARCHAR (2000) NULL,
    [SourceClaimStatus]        NVARCHAR (2000) NULL,
    [TotalClaimCost]           NVARCHAR (2000) NULL,
    [TotalClaimCostLessExcess] NVARCHAR (2000) NULL,
    [BypassFraud]              NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Incident2Person_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_MDA_Incident2Person_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Incident2Person]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Incident2PersonDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Incident2Person]([DMLDateTimeStamp] ASC);

