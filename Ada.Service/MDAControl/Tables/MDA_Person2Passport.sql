﻿CREATE TABLE [SyncDML].[MDA_Person2Passport] (
    [DMLId]             BIGINT         IdENTITY (1, 1) NOT NULL,
    [TaskId]            INT            NULL,
    [DMLAction]         VARCHAR (2000) NULL,
    [DMLDateTimeStamp]  DATETIME       CONSTRAINT [DF_MDA_Person2Passport_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                VARCHAR (2000) NULL,
    [Person_ID]         VARCHAR (2000) NULL,
    [Passport_ID]       VARCHAR (2000) NULL,
    [PassportLinkId]    VARCHAR (2000) NULL,
    [MRZValId]          VARCHAR (2000) NULL,
    [DOBValId]          VARCHAR (2000) NULL,
    [GenderValId]       VARCHAR (2000) NULL,
    [FiveGrading]       VARCHAR (2000) NULL,
    [LinkConfidence]    VARCHAR (2000) NULL,
    [Source]            VARCHAR (2000) NULL,
    [SourceReference]   VARCHAR (2000) NULL,
    [SourceDescription] VARCHAR (2000) NULL,
    [CreatedBy]         VARCHAR (2000) NULL,
    [CreatedDate]       VARCHAR (2000) NULL,
    [ModifiedBy]        VARCHAR (2000) NULL,
    [ModifiedDate]      VARCHAR (2000) NULL,
    [RiskClaim_Id]      VARCHAR (2000) NULL,
    [BaseRiskClaim_Id]  VARCHAR (2000) NULL,
    [IBaseId]           VARCHAR (2000) NULL,
    [RecordStatus]      VARCHAR (2000) NULL,
    [ADARecordStatus]   VARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Person2Passport_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Person2Passport_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Person2Passport]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Person2PassportDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Person2Passport]([DMLDateTimeStamp] ASC);

