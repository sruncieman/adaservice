﻿CREATE TABLE [dbo].[DataTable] (
    [Table_ID]     INT            NOT NULL,
    [Description]  NVARCHAR (255) NULL,
    [Fixed]        SMALLINT       NOT NULL,
    [InExpandList] SMALLINT       NOT NULL,
    [LogicalName]  NVARCHAR (255) NOT NULL,
    [PhysicalName] NVARCHAR (255) NOT NULL,
    [TableCode]    NVARCHAR (3)   NOT NULL,
    [Type]         TINYINT        NOT NULL,
    CONSTRAINT [PK_DataTable] PRIMARY KEY CLUSTERED ([Table_ID] ASC)
);



