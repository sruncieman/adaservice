﻿CREATE TABLE [SyncDML].[MDAIBase_Incident_Link] (
    [DMLId]                        BIGINT          IDENTITY (1, 1) NOT NULL,
    [TaskId]                       INT             NULL,
    [DMLAction]                    VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]             DATETIME        CONSTRAINT [DF_MDAIBase_Incident_Link_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Unique_ID]                    NVARCHAR (2000) NULL,
    [Accident_Management]          NVARCHAR (2000) NULL,
    [AltEntity]                    NVARCHAR (2000) NULL,
    [Ambulance_Attended]           NVARCHAR (2000) NULL,
    [Broker_]                      NVARCHAR (2000) NULL,
    [Claim_Code]                   NVARCHAR (2000) NULL,
    [Claim_Notification_Date]      NVARCHAR (2000) NULL,
    [Claim_Number]                 NVARCHAR (2000) NULL,
    [Claim_Status]                 NVARCHAR (2000) NULL,
    [Claim_Type]                   NVARCHAR (2000) NULL,
    [Create_Date]                  NVARCHAR (2000) NULL,
    [Create_User]                  NVARCHAR (2000) NULL,
    [Do_Not_Disseminate_412284494] NVARCHAR (2000) NULL,
    [Engineer_]                    NVARCHAR (2000) NULL,
    [Hire_]                        NVARCHAR (2000) NULL,
    [IconColour]                   NVARCHAR (2000) NULL,
    [Incident_Circs]               NVARCHAR (MAX)  NULL,
    [Incident_Link_ID]             NVARCHAR (2000) NULL,
    [Incident_Location]            NVARCHAR (2000) NULL,
    [Incident_Time]                NVARCHAR (2000) NULL,
    [Insurer_]                     NVARCHAR (2000) NULL,
    [Key_Attractor_412284410]      NVARCHAR (2000) NULL,
    [Last_Upd_Date]                NVARCHAR (2000) NULL,
    [Last_Upd_User]                NVARCHAR (2000) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (2000) NULL,
    [Medical_Examiner]             NVARCHAR (2000) NULL,
    [MoJ_Status]                   NVARCHAR (2000) NULL,
    [Party_Type]                   NVARCHAR (2000) NULL,
    [Payments_]                    NVARCHAR (2000) NULL,
    [Police_Attended]              NVARCHAR (2000) NULL,
    [Police_Force]                 NVARCHAR (2000) NULL,
    [Police_Reference]             NVARCHAR (2000) NULL,
    [Record_Status]                NVARCHAR (2000) NULL,
    [Recovery_]                    NVARCHAR (2000) NULL,
    [Referral_Source]              NVARCHAR (2000) NULL,
    [Repairer_]                    NVARCHAR (2000) NULL,
    [Reserve_]                     NVARCHAR (2000) NULL,
    [SCC]                          NVARCHAR (2000) NULL,
    [Solicitors_]                  NVARCHAR (2000) NULL,
    [Source_411765484]             NVARCHAR (2000) NULL,
    [Source_Description_411765489] NVARCHAR (MAX)  NULL,
    [Source_Reference_411765487]   NVARCHAR (2000) NULL,
    [Status_Binding]               NVARCHAR (2000) NULL,
    [Storage_]                     NVARCHAR (2000) NULL,
    [Storage_Address]              NVARCHAR (2000) NULL,
    [Sub_Party_Type]               NVARCHAR (2000) NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (2000) NULL,
    [Undefined_Supplier]           NVARCHAR (2000) NULL,
    [Attended_Hospital]            NVARCHAR (2000) NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (2000) NULL,
    [Source_Claim_Status]          NVARCHAR (2000) NULL,
    [Total_Claim_Cost]             NVARCHAR (2000) NULL,
    [Total_Claim_Cost_Less_Excess] NVARCHAR (2000) NULL,
    [Bypass_Fraud]                 NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDAIBase_Incident_Link_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Incident_Link_TaskId_DMLAction_NC]
    ON [SyncDML].[MDAIBase_Incident_Link]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Incident_LinkDMLDateTimeStamp_NC]
    ON [SyncDML].[MDAIBase_Incident_Link]([DMLDateTimeStamp] ASC);

