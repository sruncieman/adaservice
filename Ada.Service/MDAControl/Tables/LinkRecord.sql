﻿CREATE TABLE [Rep].[LinkRecord] (
    [Id]                   INT          IdENTITY (1, 1) NOT NULL,
    [TableCode]            VARCHAR (3)  NULL,
    [IBase5LinkID]         VARCHAR (50) NULL,
    [IBase8LinkID_Staging] VARCHAR (50) NULL,
    [IBase8LinkID]         VARCHAR (50) NULL,
    [EliteReference]       VARCHAR (50) NULL,
    [RiskClaim_Id]         INT          CONSTRAINT [DF_LinkRecord_RiskClaim_Id] DEFAULT ((0)) NULL,
    [RecordAction]         VARCHAR (1)  NULL
);

