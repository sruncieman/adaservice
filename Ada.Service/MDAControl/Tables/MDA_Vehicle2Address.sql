﻿CREATE TABLE [SyncDML].[MDA_Vehicle2Address] (
    [DMLId]              BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]             INT             NULL,
    [DMLAction]          VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]   DATETIME        CONSTRAINT [DF_MDA_Vehicle2Address_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                 NVARCHAR (2000) NULL,
    [Vehicle_ID]         NVARCHAR (2000) NULL,
    [Address_ID]         NVARCHAR (2000) NULL,
    [AddressLinkId]      NVARCHAR (2000) NULL,
    [AddressLinkType_ID] NVARCHAR (2000) NULL,
    [StartOfResidency]   NVARCHAR (2000) NULL,
    [EndOfResidency]     NVARCHAR (2000) NULL,
    [RawAddress]         NVARCHAR (2000) NULL,
    [Notes]              NVARCHAR (2000) NULL,
    [FiveGrading]        NVARCHAR (2000) NULL,
    [LinkConfidence]     NVARCHAR (2000) NULL,
    [Source]             NVARCHAR (2000) NULL,
    [SourceReference]    NVARCHAR (2000) NULL,
    [SourceDescription]  NVARCHAR (2000) NULL,
    [CreatedBy]          NVARCHAR (2000) NULL,
    [CreatedDate]        NVARCHAR (2000) NULL,
    [ModifiedBy]         NVARCHAR (2000) NULL,
    [ModifiedDate]       NVARCHAR (2000) NULL,
    [RiskClaim_Id]       NVARCHAR (2000) NULL,
    [BaseRiskClaim_Id]   NVARCHAR (2000) NULL,
    [IBaseId]            NVARCHAR (2000) NULL,
    [RecordStatus]       NVARCHAR (2000) NULL,
    [ADARecordStatus]    NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Vehicle2Address_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Vehicle2Address_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Vehicle2Address]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Vehicle2AddressDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Vehicle2Address]([DMLDateTimeStamp] ASC);

