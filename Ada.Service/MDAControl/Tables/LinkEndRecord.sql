﻿CREATE TABLE [Rep].[LinkEndRecord] (
    [Id]                     INT          IdENTITY (1, 1) NOT NULL,
    [IBase5LinkID]           VARCHAR (50) NULL,
    [IBase5EntityID1]        VARCHAR (50) NULL,
    [IBase5EntityID2]        VARCHAR (50) NULL,
    [IBase8LinkIDStaging]    VARCHAR (50) NULL,
    [IBase8EntityID1Staging] VARCHAR (50) NULL,
    [IBase8EntityID2Staging] VARCHAR (50) NULL,
    [IBase8LinkID]           VARCHAR (50) NULL,
    [IBase8EntityID1]        VARCHAR (50) NULL,
    [IBase8EntityID2]        VARCHAR (50) NULL,
    [RecordAction]           VARCHAR (1)  NULL
);

