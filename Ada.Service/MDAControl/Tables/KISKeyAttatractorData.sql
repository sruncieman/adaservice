﻿CREATE TABLE [Rep].[KISKeyAttatractorData] (
    [Id]                     INT          IdENTITY (1, 1) NOT NULL,
    [IBase5UniqueId]         VARCHAR (50) NULL,
    [KeyAttractorRemoveDate] DATETIME     NULL,
    CONSTRAINT [PK_KISKeyAttatractorData_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

