﻿CREATE TABLE [SyncDML].[MDA_Person2Address] (
    [DMLId]                BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]               INT             NULL,
    [DMLAction]            VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]     DATETIME        CONSTRAINT [DF_MDA_Person2Address_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                   NVARCHAR (2000) NULL,
    [Person_ID]            NVARCHAR (2000) NULL,
    [Address_ID]           NVARCHAR (2000) NULL,
    [AddressLinkId]        NVARCHAR (2000) NULL,
    [AddressLinkType_ID]   NVARCHAR (2000) NULL,
    [StartOfResidency]     NVARCHAR (2000) NULL,
    [EndOfResidency]       NVARCHAR (2000) NULL,
    [RawAddress]           NVARCHAR (2000) NULL,
    [Notes]                NVARCHAR (2000) NULL,
    [TracesmartIKey]       VARCHAR (2000)  NULL,
    [TracesmartAddrSource] NVARCHAR (2000) NULL,
    [TSIdUAMLResult]       NVARCHAR (2000) NULL,
    [TracesmartDOB]        NVARCHAR (2000) NULL,
    [ExperianDOB]          NVARCHAR (2000) NULL,
    [GoneAway]             NVARCHAR (2000) NULL,
    [Insolvency]           NVARCHAR (2000) NULL,
    [CCJHistory]           NVARCHAR (2000) NULL,
    [InsolvencyHistory]    NVARCHAR (2000) NULL,
    [DeathScreenMatchType] NVARCHAR (2000) NULL,
    [DeathScreenDoD]       NVARCHAR (2000) NULL,
    [DeathScreenRegNo]     NVARCHAR (2000) NULL,
    [CredivaCheck]         NVARCHAR (2000) NULL,
    [FiveGrading]          NVARCHAR (2000) NULL,
    [LinkConfidence]       NVARCHAR (2000) NULL,
    [Source]               NVARCHAR (2000) NULL,
    [SourceReference]      NVARCHAR (2000) NULL,
    [SourceDescription]    NVARCHAR (2000) NULL,
    [CreatedBy]            NVARCHAR (2000) NULL,
    [CreatedDate]          NVARCHAR (2000) NULL,
    [ModifiedBy]           NVARCHAR (2000) NULL,
    [ModifiedDate]         NVARCHAR (2000) NULL,
    [RiskClaim_Id]         NVARCHAR (2000) NULL,
    [BaseRiskClaim_Id]     NVARCHAR (2000) NULL,
    [IBaseId]              NVARCHAR (2000) NULL,
    [RecordStatus]         NVARCHAR (2000) NULL,
    [ADARecordStatus]      NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Person2Address_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Person2Address_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Person2Address]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Person2AddressDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Person2Address]([DMLDateTimeStamp] ASC);

