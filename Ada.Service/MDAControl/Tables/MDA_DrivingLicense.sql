﻿CREATE TABLE [SyncDML].[MDA_DrivingLicense] (
    [DMLId]             BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]            INT             NULL,
    [DMLAction]         VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]  DATETIME        CONSTRAINT [DF_MDA_DrivingLicense_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                NVARCHAR (2000) NULL,
    [DrivingLicenseId]  NVARCHAR (2000) NULL,
    [DriverNumber]      NVARCHAR (2000) NULL,
    [KeyAttractor]      NVARCHAR (2000) NULL,
    [Source]            NVARCHAR (2000) NULL,
    [SourceReference]   NVARCHAR (2000) NULL,
    [SourceDescription] NVARCHAR (2000) NULL,
    [CreatedBy]         NVARCHAR (2000) NULL,
    [CreatedDate]       NVARCHAR (2000) NULL,
    [ModifiedBy]        NVARCHAR (2000) NULL,
    [ModifiedDate]      NVARCHAR (2000) NULL,
    [IBaseId]           NVARCHAR (2000) NULL,
    [RecordStatus]      NVARCHAR (2000) NULL,
    [ADARecordStatus]   NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_DrivingLicense_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_DrivingLicense_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_DrivingLicense]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_DrivingLicenseDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_DrivingLicense]([DMLDateTimeStamp] ASC);

