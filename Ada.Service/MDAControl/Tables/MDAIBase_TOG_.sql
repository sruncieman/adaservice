﻿CREATE TABLE [SyncDML].[MDAIBase_TOG_] (
    [DMLId]                        BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]                       INT             NULL,
    [DMLAction]                    VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]             DATETIME        CONSTRAINT [DF_MDAIBase_TOG__DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Unique_ID]                    NVARCHAR (2000) NULL,
    [Allocated_to]                 NVARCHAR (2000) NULL,
    [AltEntity]                    NVARCHAR (2000) NULL,
    [Create_Date]                  NVARCHAR (2000) NULL,
    [Create_User]                  NVARCHAR (2000) NULL,
    [Date_Allocated]               NVARCHAR (2000) NULL,
    [Date_Completed]               NVARCHAR (2000) NULL,
    [Date_of_Review]               NVARCHAR (2000) NULL,
    [Decision_]                    NVARCHAR (2000) NULL,
    [Do_Not_Disseminate_412284494] NVARCHAR (2000) NULL,
    [IconColour]                   NVARCHAR (2000) NULL,
    [Key_Attractor_412284410]      NVARCHAR (2000) NULL,
    [Last_Upd_Date]                NVARCHAR (2000) NULL,
    [Last_Upd_User]                NVARCHAR (2000) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (2000) NULL,
    [Record_Status]                NVARCHAR (2000) NULL,
    [Referred_by]                  NVARCHAR (2000) NULL,
    [Report_]                      NVARCHAR (MAX)  NULL,
    [SCC]                          NVARCHAR (2000) NULL,
    [Source_411765484]             NVARCHAR (2000) NULL,
    [Source_Description_411765489] NVARCHAR (MAX)  NULL,
    [Source_Reference_411765487]   NVARCHAR (2000) NULL,
    [Status_Binding]               NVARCHAR (2000) NULL,
    [TOG_ID]                       NVARCHAR (2000) NULL,
    [TOG_Reference]                NVARCHAR (2000) NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (2000) NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDAIBase_TOG__DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_TOG__TaskId_DMLAction_NC]
    ON [SyncDML].[MDAIBase_TOG_]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_TOG_DMLDateTimeStamp_NC]
    ON [SyncDML].[MDAIBase_TOG_]([DMLDateTimeStamp] ASC);

