﻿CREATE TABLE [Rpt].[ReportName] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [ReportName] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ReportName_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

