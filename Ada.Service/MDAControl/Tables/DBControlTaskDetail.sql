﻿CREATE TABLE [dbo].[DBControlTaskDetail] (
    [TaskDetailId]     INT            IdENTITY (0, 1) NOT NULL,
    [TaskId]           INT            NOT NULL,
    [UniqueId]         VARCHAR (100)  NOT NULL,
    [ActionId]         VARCHAR (20)   NOT NULL,
    [Error]            INT            NULL,
    [ErrorDescription] VARCHAR (1000) NULL,
    [Details]          VARCHAR (1000) NULL,
    CONSTRAINT [PK_DBControlTaskDetail_DetailId_C] PRIMARY KEY CLUSTERED ([TaskDetailId] ASC)
);

