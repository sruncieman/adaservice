﻿CREATE TABLE [SyncDML].[MDA_Incident2FraudRing] (
    [DMLId]             BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]            INT             NULL,
    [DMLAction]         VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]  DATETIME        CONSTRAINT [DF_MDA_Incident2FraudRing_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                NVARCHAR (2000) NULL,
    [Incident_ID]       NVARCHAR (2000) NULL,
    [FraudRing_Id]      NVARCHAR (2000) NULL,
    [FraudRingLinkId]   NVARCHAR (2000) NULL,
    [LinkType]          NVARCHAR (2000) NULL,
    [FiveGrading]       NVARCHAR (2000) NULL,
    [LinkConfidence]    NVARCHAR (2000) NULL,
    [Source]            NVARCHAR (2000) NULL,
    [SourceReference]   NVARCHAR (2000) NULL,
    [SourceDescription] NVARCHAR (2000) NULL,
    [CreatedBy]         NVARCHAR (2000) NULL,
    [CreatedDate]       NVARCHAR (2000) NULL,
    [ModifiedBy]        NVARCHAR (2000) NULL,
    [ModifiedDate]      NVARCHAR (2000) NULL,
    [RiskClaim_Id]      NVARCHAR (2000) NULL,
    [BaseRiskClaim_Id]  NVARCHAR (2000) NULL,
    [IBaseId]           NVARCHAR (2000) NULL,
    [RecordStatus]      NVARCHAR (2000) NULL,
    [ADARecordStatus]   NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Incident2FraudRing_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Incident2FraudRing_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Incident2FraudRing]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Incident2FraudRingDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Incident2FraudRing]([DMLDateTimeStamp] ASC);

