﻿CREATE TABLE [SyncDML].[MDA_Vehicle2Organisation] (
    [DMLId]              BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]             INT             NULL,
    [DMLAction]          VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]   DATETIME        CONSTRAINT [DF_MDA_Vehicle2Organisation_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                 NVARCHAR (2000) NULL,
    [Vehicle_ID]         NVARCHAR (2000) NULL,
    [Organisation_ID]    NVARCHAR (2000) NULL,
    [VehicleLinkId]      NVARCHAR (2000) NULL,
    [VehicleLinkType_ID] NVARCHAR (2000) NULL,
    [RegKeeperStartDate] NVARCHAR (2000) NULL,
    [RegKeeperEndDate]   NVARCHAR (2000) NULL,
    [HireCompany]        NVARCHAR (2000) NULL,
    [CrossHireCompany]   NVARCHAR (2000) NULL,
    [HireStartDate]      NVARCHAR (2000) NULL,
    [HireEndDate]        NVARCHAR (2000) NULL,
    [FiveGrading]        NVARCHAR (2000) NULL,
    [LinkConfidence]     NVARCHAR (2000) NULL,
    [Source]             NVARCHAR (2000) NULL,
    [SourceReference]    NVARCHAR (2000) NULL,
    [SourceDescription]  NVARCHAR (2000) NULL,
    [CreatedBy]          NVARCHAR (2000) NULL,
    [CreatedDate]        NVARCHAR (2000) NULL,
    [ModifiedBy]         NVARCHAR (2000) NULL,
    [ModifiedDate]       NVARCHAR (2000) NULL,
    [RiskClaim_Id]       NVARCHAR (2000) NULL,
    [BaseRiskClaim_Id]   NVARCHAR (2000) NULL,
    [IBaseId]            NVARCHAR (2000) NULL,
    [RecordStatus]       NVARCHAR (2000) NULL,
    [ADARecordStatus]    NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Vehicle2Organisation_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Vehicle2Organisation_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Vehicle2Organisation]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Vehicle2OrganisationDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Vehicle2Organisation]([DMLDateTimeStamp] ASC);

