﻿CREATE TABLE [SyncDML].[MDA_NINumber] (
    [DMLId]             BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]            INT             NULL,
    [DMLAction]         VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]  DATETIME        CONSTRAINT [DF_MDA_NINumber_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                NVARCHAR (2000) NULL,
    [NINumberId]        NVARCHAR (2000) NULL,
    [NINumber]          NVARCHAR (2000) NULL,
    [KeyAttractor]      NVARCHAR (2000) NULL,
    [Source]            NVARCHAR (2000) NULL,
    [SourceReference]   NVARCHAR (2000) NULL,
    [SourceDescription] NVARCHAR (2000) NULL,
    [CreatedBy]         NVARCHAR (2000) NULL,
    [CreatedDate]       NVARCHAR (2000) NULL,
    [ModifiedBy]        NVARCHAR (2000) NULL,
    [ModifiedDate]      NVARCHAR (2000) NULL,
    [IBaseId]           NVARCHAR (2000) NULL,
    [RecordStatus]      NVARCHAR (2000) NULL,
    [ADARecordStatus]   NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_NINumber_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_NINumber_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_NINumber]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_NINumberDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_NINumber]([DMLDateTimeStamp] ASC);

