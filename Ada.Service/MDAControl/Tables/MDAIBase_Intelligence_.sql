﻿CREATE TABLE [SyncDML].[MDAIBase_Intelligence_] (
    [DMLId]                        BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]                       INT             NULL,
    [DMLAction]                    VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]             DATETIME        CONSTRAINT [DF_MDAIBase_Intelligence__DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Unique_ID]                    NVARCHAR (2000) NULL,
    [AltEntity]                    NVARCHAR (2000) NULL,
    [Create_Date]                  NVARCHAR (2000) NULL,
    [Create_User]                  NVARCHAR (2000) NULL,
    [Description_]                 NVARCHAR (2000) NULL,
    [Do_Not_Disseminate_412284494] NVARCHAR (2000) NULL,
    [Document_]                    NVARCHAR (MAX)  NULL,
    [Document__Binding]            NVARCHAR (2000) NULL,
    [IconColour]                   NVARCHAR (2000) NULL,
    [Intelligence_ID]              NVARCHAR (2000) NULL,
    [Key_Attractor_412284410]      NVARCHAR (2000) NULL,
    [Key_Attractors]               NVARCHAR (MAX)  NULL,
    [Last_Upd_Date]                NVARCHAR (2000) NULL,
    [Last_Upd_User]                NVARCHAR (2000) NULL,
    [MDA_Incident_ID_412284502]    NVARCHAR (2000) NULL,
    [Picture_]                     NVARCHAR (MAX)  NULL,
    [Picture__Binding]             NVARCHAR (2000) NULL,
    [Picture_2]                    NVARCHAR (MAX)  NULL,
    [Picture_2_Binding]            NVARCHAR (2000) NULL,
    [Record_Status]                NVARCHAR (2000) NULL,
    [Report_]                      NVARCHAR (MAX)  NULL,
    [SCC]                          NVARCHAR (2000) NULL,
    [Source_411765484]             NVARCHAR (2000) NULL,
    [Source_Description_411765489] NVARCHAR (MAX)  NULL,
    [Source_Reference_411765487]   NVARCHAR (2000) NULL,
    [Status_Binding]               NVARCHAR (2000) NULL,
    [Type_]                        NVARCHAR (2000) NULL,
    [Web_Page]                     NVARCHAR (MAX)  NULL,
    [x5x5x5_Grading_412284402]     NVARCHAR (2000) NULL,
    [Risk_Claim_ID_414244883]      NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDAIBase_Intelligence__DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Intelligence__TaskId_DMLAction_NC]
    ON [SyncDML].[MDAIBase_Intelligence_]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase_Intelligence_DMLDateTimeStamp_NC]
    ON [SyncDML].[MDAIBase_Intelligence_]([DMLDateTimeStamp] ASC);

