﻿CREATE TABLE [SyncDML].[MDA_KeoghsCase] (
    [DMLId]                BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]               INT             NULL,
    [DMLAction]            VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]     DATETIME        CONSTRAINT [DF_MDA_KeoghsCase_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                   NVARCHAR (2000) NULL,
    [KeoghsCaseId]         NVARCHAR (2000) NULL,
    [KeoghsEliteReference] NVARCHAR (2000) NULL,
    [KeoghsOffice_Id]      NVARCHAR (2000) NULL,
    [FeeEarner]            NVARCHAR (2000) NULL,
    [Client_Id]            NVARCHAR (2000) NULL,
    [ClientReference]      NVARCHAR (2000) NULL,
    [ClientClaimsHandler]  NVARCHAR (2000) NULL,
    [CaseStatus_Id]        NVARCHAR (2000) NULL,
    [CurrentReserve]       NVARCHAR (2000) NULL,
    [ClosureDate]          NVARCHAR (2000) NULL,
    [WeedDate]             NVARCHAR (2000) NULL,
    [KeyAttractor]         NVARCHAR (2000) NULL,
    [Source]               NVARCHAR (2000) NULL,
    [SourceReference]      NVARCHAR (2000) NULL,
    [SourceDescription]    NVARCHAR (2000) NULL,
    [CreatedBy]            NVARCHAR (2000) NULL,
    [CreatedDate]          NVARCHAR (2000) NULL,
    [ModifiedBy]           NVARCHAR (2000) NULL,
    [ModifiedDate]         NVARCHAR (2000) NULL,
    [IBaseId]              NVARCHAR (2000) NULL,
    [RecordStatus]         NVARCHAR (2000) NULL,
    [ADARecordStatus]      NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_KeoghsCase_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_KeoghsCase_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_KeoghsCase]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_KeoghsCaseDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_KeoghsCase]([DMLDateTimeStamp] ASC);

