﻿CREATE TABLE [SyncDML].[MDA_Person2BankAccount] (
    [DMLId]             BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]            INT             NULL,
    [DMLAction]         VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]  DATETIME        CONSTRAINT [DF_MDA_Person2BankAccount_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                NVARCHAR (2000) NULL,
    [Person_ID]         NVARCHAR (2000) NULL,
    [BankAccount_Id]    NVARCHAR (2000) NULL,
    [AccountLinkId]     NVARCHAR (2000) NULL,
    [Validated]         NVARCHAR (2000) NULL,
    [FiveGrading]       NVARCHAR (2000) NULL,
    [LinkConfidence]    NVARCHAR (2000) NULL,
    [Source]            NVARCHAR (2000) NULL,
    [SourceReference]   NVARCHAR (2000) NULL,
    [SourceDescription] NVARCHAR (2000) NULL,
    [CreatedBy]         NVARCHAR (2000) NULL,
    [CreatedDate]       NVARCHAR (2000) NULL,
    [ModifiedBy]        NVARCHAR (2000) NULL,
    [ModifiedDate]      NVARCHAR (2000) NULL,
    [RiskClaim_Id]      NVARCHAR (2000) NULL,
    [BaseRiskClaim_Id]  NVARCHAR (2000) NULL,
    [IBaseId]           NVARCHAR (2000) NULL,
    [RecordStatus]      NVARCHAR (2000) NULL,
    [ADARecordStatus]   NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Person2BankAccount_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Person2BankAccount_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Person2BankAccount]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Person2BankAccountDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Person2BankAccount]([DMLDateTimeStamp] ASC);

