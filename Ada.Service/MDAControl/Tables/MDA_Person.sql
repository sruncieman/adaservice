﻿CREATE TABLE [SyncDML].[MDA_Person] (
    [DMLId]             BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]            INT             NULL,
    [DMLAction]         VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]  DATETIME        CONSTRAINT [DF_MDA_Person_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                NVARCHAR (2000) NULL,
    [PersonId]          NVARCHAR (2000) NULL,
    [Gender_Id]         NVARCHAR (2000) NULL,
    [Salutation_Id]     NVARCHAR (2000) NULL,
    [FirstName]         NVARCHAR (2000) NULL,
    [MiddleName]        NVARCHAR (2000) NULL,
    [LastName]          NVARCHAR (2000) NULL,
    [DateOfBirth]       NVARCHAR (2000) NULL,
    [Nationality]       NVARCHAR (2000) NULL,
    [Occupation]        NVARCHAR (2000) NULL,
    [TaxiDriverLicense] NVARCHAR (2000) NULL,
    [Schools]           NVARCHAR (2000) NULL,
    [Hobbies]           NVARCHAR (2000) NULL,
    [Notes]             NVARCHAR (MAX)  NULL,
    [Documents]         NVARCHAR (MAX)  NULL,
    [KeyAttractor]      NVARCHAR (2000) NULL,
    [SanctionDate]      NVARCHAR (2000) NULL,
    [SanctionSource]    NVARCHAR (2000) NULL,
    [Source]            NVARCHAR (2000) NULL,
    [SourceReference]   NVARCHAR (2000) NULL,
    [SourceDescription] NVARCHAR (2000) NULL,
    [CreatedBy]         NVARCHAR (2000) NULL,
    [CreatedDate]       NVARCHAR (2000) NULL,
    [ModifiedBy]        NVARCHAR (2000) NULL,
    [ModifiedDate]      NVARCHAR (2000) NULL,
    [IBaseId]           NVARCHAR (2000) NULL,
    [RecordStatus]      NVARCHAR (2000) NULL,
    [ADARecordStatus]   NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Person_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Person_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Person]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_PersonDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Person]([DMLDateTimeStamp] ASC);

