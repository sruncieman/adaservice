﻿CREATE TABLE [dbo].[DBControlImportFiles] (
    [PkId]         INT                        IDENTITY (1, 1) NOT NULL,
    [Id]           UNIQUEIDENTIFIER           DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [ControlRunID] INT                        NULL,
    [FileName]     VARCHAR (1000)             NULL,
    [File]         VARBINARY (MAX) FILESTREAM NULL,
    PRIMARY KEY CLUSTERED ([PkId] ASC),
    UNIQUE NONCLUSTERED ([Id] ASC)
) FILESTREAM_ON [Filestream];

