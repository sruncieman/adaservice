﻿CREATE TABLE [SyncDML].[MDA_Address] (
    [DMLId]             BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]            INT             NULL,
    [DMLAction]         VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]  DATETIME        CONSTRAINT [DF_MDA_Address_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                NVARCHAR (2000) NULL,
    [AddressId]         NVARCHAR (2000) NULL,
    [SubBuilding]       NVARCHAR (2000) NULL,
    [Building]          NVARCHAR (2000) NULL,
    [BuildingNumber]    NVARCHAR (2000) NULL,
    [Street]            NVARCHAR (2000) NULL,
    [Locality]          NVARCHAR (2000) NULL,
    [Town]              NVARCHAR (2000) NULL,
    [County]            NVARCHAR (2000) NULL,
    [PostCode]          NVARCHAR (2000) NULL,
    [DxNumber]          NVARCHAR (2000) NULL,
    [DxExchange]        NVARCHAR (2000) NULL,
    [GridX]             NVARCHAR (2000) NULL,
    [GridY]             NVARCHAR (2000) NULL,
    [PafValidation]     NVARCHAR (2000) NULL,
    [PafUPRN]           NVARCHAR (2000) NULL,
    [DocumentLink]      NVARCHAR (2000) NULL,
    [KeyAttractor]      NVARCHAR (2000) NULL,
    [PropertyType]      NVARCHAR (2000) NULL,
    [MosaicCode]        NVARCHAR (2000) NULL,
    [Source]            NVARCHAR (2000) NULL,
    [SourceReference]   NVARCHAR (2000) NULL,
    [SourceDescription] NVARCHAR (2000) NULL,
    [CreatedBy]         NVARCHAR (2000) NULL,
    [CreatedDate]       NVARCHAR (2000) NULL,
    [ModifiedBy]        NVARCHAR (2000) NULL,
    [ModifiedDate]      NVARCHAR (2000) NULL,
    [AddressType_Id]    NVARCHAR (2000) NULL,
    [IBaseId]           NVARCHAR (2000) NULL,
    [RecordStatus]      NVARCHAR (2000) NULL,
    [ADARecordStatus]   NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Address_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Address_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Address]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_AddressDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Address]([DMLDateTimeStamp] ASC);

