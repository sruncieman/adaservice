﻿CREATE TABLE [dbo].[DBControlName] (
    [NameId]          INT           IdENTITY (1, 1) NOT NULL,
    [NameType]        VARCHAR (100) NOT NULL,
    [NameDescription] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_DBControlName_NameId_C] PRIMARY KEY CLUSTERED ([NameId] ASC)
);

