﻿CREATE TABLE [dbo].[DBControlPurgeHistory] (
    [PurgeHistory_Id] INT      IDENTITY (1, 1) NOT NULL,
    [RunID]           INT      NOT NULL,
    [RiskBatch_Id]    INT      NOT NULL,
    [PurgeDate]       DATETIME CONSTRAINT [DF_DBControlPurgeHistory_PurgeDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_DBControlPurgeHistory] PRIMARY KEY CLUSTERED ([PurgeHistory_Id] ASC),
    CONSTRAINT [FK_DBControlPurgeHistory_DBControlRun_RunID] FOREIGN KEY ([RunID]) REFERENCES [dbo].[DBControlRun] ([RunId])
);

