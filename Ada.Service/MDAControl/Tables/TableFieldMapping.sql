﻿CREATE TABLE [Sync].[TableFieldMapping] (
    [Id]                     INT             NOT NULL,
    [IBaseTable_Id]          INT             NOT NULL,
    [IBaseTableType]         TINYINT         NOT NULL,
    [TableLogicalName]       NVARCHAR (255)  NOT NULL,
    [IBaseTablePhysicalName] NVARCHAR (255)  NOT NULL,
    [MdaTablePhysicalName]   NVARCHAR (255)  NULL,
    [FieldLogicalName]       NVARCHAR (255)  NOT NULL,
    [IBaseFieldPhysicalName] NVARCHAR (255)  NOT NULL,
    [MdaFieldPhysicalName]   VARCHAR (100)   NULL,
    [ExcludeBitmask]         INT             NULL,
    [IBaseSql]               NVARCHAR (1000) NULL,
    [testdata1]              NVARCHAR (1000) NULL,
    [TestData2]              NVARCHAR (1000) NULL,
    [TestData3]              NVARCHAR (1000) NULL,
    [Testdata4]              NVARCHAR (1000) NULL,
    CONSTRAINT [PK_TableFieldMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);

