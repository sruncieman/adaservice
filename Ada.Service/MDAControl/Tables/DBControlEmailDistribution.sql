﻿CREATE TABLE [dbo].[DBControlEmailDistribution] (
    [EmailDistributionId] INT            IdENTITY (1, 1) NOT NULL,
    [EmailDistribution]   VARCHAR (1000) NULL,
    CONSTRAINT [PK_DBControlEmailDistribution] PRIMARY KEY CLUSTERED ([EmailDistributionId] ASC)
);

