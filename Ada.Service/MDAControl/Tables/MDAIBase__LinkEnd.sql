﻿CREATE TABLE [SyncDML].[MDAIBase__LinkEnd] (
    [DMLId]            BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]           INT             NULL,
    [DMLAction]        VARCHAR (2000)  NULL,
    [DMLDateTimeStamp] DATETIME        CONSTRAINT [DF_MDAIBase__LinkEnd_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Link_ID]          NVARCHAR (2000) NULL,
    [Entity_ID1]       NVARCHAR (2000) NULL,
    [Confidence]       NVARCHAR (2000) NULL,
    [Direction]        NVARCHAR (2000) NULL,
    [Entity_ID2]       NVARCHAR (2000) NULL,
    [EntityType_ID1]   NVARCHAR (2000) NULL,
    [EntityType_ID2]   NVARCHAR (2000) NULL,
    [LinkType_ID]      NVARCHAR (2000) NULL,
    [Record_Status]    NVARCHAR (2000) NULL,
    [Record_Type]      NVARCHAR (2000) NULL,
    [SCC]              NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDAIBase__LinkEnd_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase__LinkEnd_TaskId_DMLAction_NC]
    ON [SyncDML].[MDAIBase__LinkEnd]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDAIBase__LinkEndDMLDateTimeStamp_NC]
    ON [SyncDML].[MDAIBase__LinkEnd]([DMLDateTimeStamp] ASC);

