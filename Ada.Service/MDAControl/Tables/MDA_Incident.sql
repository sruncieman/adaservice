﻿CREATE TABLE [SyncDML].[MDA_Incident] (
    [DMLId]                BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]               INT             NULL,
    [DMLAction]            VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]     DATETIME        CONSTRAINT [DF_MDA_Incident_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                   NVARCHAR (2000) NULL,
    [IncidentId]           NVARCHAR (2000) NULL,
    [KeoghsEliteReference] NVARCHAR (2000) NULL,
    [IncidentType_Id]      NVARCHAR (2000) NULL,
    [ClaimType_Id]         NVARCHAR (50)   NULL,
    [IfbReference]         NVARCHAR (2000) NULL,
    [IncidentDate]         NVARCHAR (2000) NULL,
    [FraudRingName]        NVARCHAR (2000) NULL,
    [PaymentsToDate]       NVARCHAR (2000) NULL,
    [Reserve]              NVARCHAR (2000) NULL,
    [KeyAttractor]         NVARCHAR (2000) NULL,
    [Source]               NVARCHAR (2000) NULL,
    [SourceReference]      NVARCHAR (2000) NULL,
    [SourceDescription]    NVARCHAR (2000) NULL,
    [CreatedBy]            NVARCHAR (2000) NULL,
    [CreatedDate]          NVARCHAR (2000) NULL,
    [ModifiedBy]           NVARCHAR (2000) NULL,
    [ModifiedDate]         NVARCHAR (2000) NULL,
    [IBaseId]              NVARCHAR (2000) NULL,
    [RecordStatus]         NVARCHAR (2000) NULL,
    [ADARecordStatus]      NVARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Incident_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Incident_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Incident]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_IncidentDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Incident]([DMLDateTimeStamp] ASC);

