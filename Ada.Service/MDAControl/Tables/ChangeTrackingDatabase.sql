﻿CREATE TABLE [Sync].[ChangeTrackingDatabase] (
    [ID]                   INT      IDENTITY (1, 1) NOT NULL,
    [RunID]                INT      NULL,
    [DatabaseID]           SMALLINT NOT NULL,
    [DatabaseName]         AS       (db_name([DatabaseID])),
    [ChangeVersionFrom]    INT      NOT NULL,
    [ChangeVersionTo]      INT      NULL,
    [ProcessStartDateTime] DATETIME NULL,
    CONSTRAINT [PK_ChangeTrackingDatabase] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 100)
);




GO
CREATE NONCLUSTERED INDEX [IX_ChangeTrackingDatabase_DatabaseID]
    ON [Sync].[ChangeTrackingDatabase]([DatabaseID] ASC)
    INCLUDE([ChangeVersionTo]);

