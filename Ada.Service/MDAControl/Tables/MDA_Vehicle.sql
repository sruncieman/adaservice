﻿CREATE TABLE [SyncDML].[MDA_Vehicle] (
    [DMLId]                    BIGINT          IdENTITY (1, 1) NOT NULL,
    [TaskId]                   INT             NULL,
    [DMLAction]                VARCHAR (2000)  NULL,
    [DMLDateTimeStamp]         DATETIME        CONSTRAINT [DF_MDA_Vehicle_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                       NVARCHAR (2000) NULL,
    [VehicleId]                VARCHAR (2000)  NULL,
    [VehicleColour_Id]         VARCHAR (2000)  NULL,
    [VehicleRegistration]      VARCHAR (2000)  NULL,
    [VehicleMake]              VARCHAR (2000)  NULL,
    [Model]                    VARCHAR (2000)  NULL,
    [EngineCapacity]           VARCHAR (2000)  NULL,
    [VIN]                      VARCHAR (2000)  NULL,
    [VehicleFuel_Id]           VARCHAR (2000)  NULL,
    [VehicleTransmission_Id]   VARCHAR (2000)  NULL,
    [VehicleType_Id]           VARCHAR (2000)  NULL,
    [VehicleCategoryOfLoss_Id] VARCHAR (2000)  NULL,
    [Notes]                    VARCHAR (2000)  NULL,
    [KeyAttractor]             VARCHAR (2000)  NULL,
    [Source]                   VARCHAR (2000)  NULL,
    [SourceReference]          VARCHAR (2000)  NULL,
    [SourceDescription]        VARCHAR (2000)  NULL,
    [CreatedBy]                VARCHAR (2000)  NULL,
    [CreatedDate]              VARCHAR (2000)  NULL,
    [ModifiedBy]               VARCHAR (2000)  NULL,
    [ModifiedDate]             VARCHAR (2000)  NULL,
    [IBaseId]                  VARCHAR (2000)  NULL,
    [RecordStatus]             VARCHAR (2000)  NULL,
    [ADARecordStatus]          VARCHAR (2000)  NULL,
    CONSTRAINT [PK_MDA_Vehicle_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Vehicle_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Vehicle]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_VehicleDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Vehicle]([DMLDateTimeStamp] ASC);

