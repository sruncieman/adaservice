﻿CREATE TABLE [SyncDML].[MDA_Person2WebSite] (
    [DMLId]             BIGINT         IdENTITY (1, 1) NOT NULL,
    [TaskId]            INT            NULL,
    [DMLAction]         VARCHAR (2000) NULL,
    [DMLDateTimeStamp]  DATETIME       CONSTRAINT [DF_MDA_Person2Website_DMLDateTimeStamp] DEFAULT (getdate()) NOT NULL,
    [Id]                VARCHAR (2000) NULL,
    [Person_ID]         VARCHAR (2000) NULL,
    [Website_ID]        VARCHAR (2000) NULL,
    [WebSiteLinkId]     VARCHAR (2000) NULL,
    [FiveGrading]       VARCHAR (2000) NULL,
    [LinkConfidence]    VARCHAR (2000) NULL,
    [Source]            VARCHAR (2000) NULL,
    [SourceReference]   VARCHAR (2000) NULL,
    [SourceDescription] VARCHAR (2000) NULL,
    [CreatedBy]         VARCHAR (2000) NULL,
    [CreatedDate]       VARCHAR (2000) NULL,
    [ModifiedBy]        VARCHAR (2000) NULL,
    [ModifiedDate]      VARCHAR (2000) NULL,
    [RiskClaim_Id]      VARCHAR (2000) NULL,
    [BaseRiskClaim_Id]  VARCHAR (2000) NULL,
    [IBaseId]           VARCHAR (2000) NULL,
    [RecordStatus]      VARCHAR (2000) NULL,
    [ADARecordStatus]   VARCHAR (2000) NULL,
    CONSTRAINT [PK_MDA_Person2Website_DMLId] PRIMARY KEY CLUSTERED ([DMLId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Person2Website_TaskId_DMLAction_NC]
    ON [SyncDML].[MDA_Person2WebSite]([TaskId] ASC, [DMLAction] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MDA_Person2WebSiteDMLDateTimeStamp_NC]
    ON [SyncDML].[MDA_Person2WebSite]([DMLDateTimeStamp] ASC);

