﻿
CREATE PROCEDURE[Sync].[uspSynchroniseLinkIncidentToIncident]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseLinkIncidentToIncident]
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--						2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)MDAIBase: LinkTablesChanges
--							3.2.1)ActionTheLinkChangeInMDA_IBase
--							3.2.2)DoDeletes
--							3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						3.3)MDAIBase: LinkEndTablesChanges
--							3.3.1)PopulateTheWorkingTable
--							3.3.2)ActionTheLinkEndChangeInMDA_IBase
--							3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
--						4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
--							4.1)ActionTheChangeInMDA
--							4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
--							4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						5)UpdateLoggingAndTidy
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-05-13			Paul Allen		Created
--		2015-10-16			Paul Allen		Addded Incident To Organisation links for CUE.
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT						
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT
			,@PhysicalTableName		VARCHAR(255) = 'Incident_Match_Link'
			,@Prefix				VARCHAR(3)   
			,@LinkTypeID			INT

	SELECT @LinkTypeID =Table_ID, @Prefix = TableCode FROM [$(MDA_IBase)].[dbo]._DataTable WHERE PhysicalName = @PhysicalTableName

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1) PRIMARY KEY, ChangeDatabase VARCHAR(10), PhysicalTableName VARCHAR(255), MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging
	CREATE TABLE #LinkEndStaging (Link_ID NVARCHAR(50), Entity_ID1 NVARCHAR(50), Confidence TINYINT, Direction TINYINT, Entity_ID2 NVARCHAR(50), EntityType_ID1 INT , EntityType_ID2 INT, LinkType_ID INT, Record_Status TINYINT, Record_Type TINYINT, SCC NVARCHAR(255))
			
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 129 --SynchroniseIncidentToIncidentLink
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT
	
	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Incident2Incident', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA)].[dbo].Incident2Incident, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Incident2Address', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA)].[dbo].Incident2Address, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Incident2Organisation', CT.ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA)].[dbo].Incident2Organisation, @MDAChangeVersionFrom) CT
	INNER JOIN [$(MDA)].[dbo].Incident2Organisation I2O ON I2O.Id = CT.ID
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL
	AND LEFT(IBaseID,3) = 'IN0' --WeOnlyWantDataFromIncidnetMatchLink // CUE Only....

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Incident_Match_Link', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA_IBase)].[dbo].Incident_Match_Link, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Link_End', Link_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA_IBase)].[dbo]._LinkEnd, @MDA_IBaseChangeVersionFrom) CT
	INNER JOIN [$(MDA_IBase)].[dbo].Incident_Match_Link IIML ON IIML.Unique_ID = CT.Link_ID
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL
	GROUP BY Link_ID, Sys_Change_Operation

	--2.2)AddSomeIndexesToMakeTheProcessQuicker 
	SET @Step ='2.2';

	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT (MDA_IBase_Unique_ID ASC)
	CREATE NONCLUSTERED INDEX IX_CT_PhysicalTableName ON #CT (PhysicalTableName ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3';

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IIML.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [$(MDA_IBase)].[dbo].Incident_Match_Link IIML ON IIML.Unique_ID = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Incident_Match_Link'
	AND IIML.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IVSL.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [$(MDA_IBase)].[dbo].[VW_StagingLinks] IVSL ON IVSL.[IBase8LinkID] = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Link_End'
	AND IVSL.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MI2I.IBaseId
	FROM #CT CT
	INNER JOIN [$(MDA)].[dbo].Incident2Incident MI2I ON MI2I.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Incident2Incident'

	UPDATE CT
	SET MDA_IBase_Unique_ID = MI2A.IBaseId
	FROM #CT CT
	INNER JOIN [$(MDA)].[dbo].Incident2Address MI2A ON MI2A.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Incident2Address'

	UPDATE CT
	SET MDA_IBase_Unique_ID = MI2A.IBaseId
	FROM #CT CT
	INNER JOIN [$(MDA)].[dbo].Incident2Organisation MI2A ON MI2A.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Incident2Organisation'

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4';

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				AND PhysicalTableName != 'Link_End'
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5';

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1
	
	--2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
	SET @Step ='2.6';
	UPDATE #CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT	RowID
				FROM	(
						SELECT   RowID
								,ROW_NUMBER() OVER (PARTITION BY MDA_IBase_Unique_ID ORDER BY RowID) RowNumber
						FROM #CT CT 
						WHERE EXISTS (SELECT TOP 1 1 FROM #CT CT1 WHERE MDA_IBase_Unique_ID = MDA_IBase_Unique_ID AND ChangeDatabase = 'MDA_IBase' GROUP BY MDA_IBase_Unique_ID HAVING COUNT(1) > 1)
						) Data 
				WHERE RowNumber !=1
				) IQ ON IQ.RowID = CT.RowID
	WHERE ChangeDatabase = 'MDA_IBase'

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1';

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM [$(MDA_IBase)].[dbo].[_Incident_Match_Link_NextID]
		UPDATE [$(MDA_IBase)].[dbo].[_Incident_Match_Link_NextID] SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	 @Prefix + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY RowID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--------------------------------------------------------------------------
		--3.2)MDAIBase: LinkTablesChanges
		--------------------------------------------------------------------------
		--3.2.1)ActionTheLinkChangeInMDA_IBase
		SET @Step ='3.2.1';

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [$(MDA_IBase)].[dbo].Incident_Match_Link [Target]
		USING  (
				SELECT *
				FROM	(
						SELECT ROW_NUMBER() OVER (PARTITION BY MDA_IBase_Unique_ID ORDER BY CT.RowID) RowNumber, CT.PhysicalTableName, CT.MDA_ID, CT.MDA_IBase_Unique_ID, IQ.*
						FROM #CT CT
						INNER JOIN	(
									SELECT RowID, ID, [IBaseId] [Unique_ID], CreatedDate [Create_Date], CreatedBy [Create_User], ModifiedDate [Last_Upd_Date], ModifiedBy [Last_Upd_User], RecordStatus [Record_Status], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], [IncidentMatchLinkId] [Incident_Match_Link_ID], FiveGrading  [x5x5x5_Grading_412284402], NULL [Insurer_], NULL [Insurer_Ref]
									FROM [$(MDA)].[dbo].Incident2Incident MI2I
									INNER JOIN #CT CT ON CT.MDA_ID = MI2I.ID
									WHERE CT.PhysicalTableName = 'Incident2Incident'
									UNION ALL
									SELECT RowID, ID, [IBaseId] [Unique_ID], CreatedDate [Create_Date], CreatedBy [Create_User], ModifiedDate [Last_Upd_Date], ModifiedBy [Last_Upd_User], RecordStatus [Record_Status], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], [AddressLinkId] [Incident_Match_Link_ID], FiveGrading  [x5x5x5_Grading_412284402], NULL [Insurer_], NULL [Insurer_Ref]
									FROM [$(MDA)].[dbo].[Incident2Address] MI2A
									INNER JOIN #CT CT ON CT.MDA_ID = MI2A.ID
									WHERE CT.PhysicalTableName = 'Incident2Address'
									UNION ALL
									SELECT RowID, ID, [IBaseId] [Unique_ID], CreatedDate [Create_Date], CreatedBy [Create_User], ModifiedDate [Last_Upd_Date], ModifiedBy [Last_Upd_User], RecordStatus [Record_Status], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], [IncidentLinkId]  [Incident_Match_Link_ID], FiveGrading  [x5x5x5_Grading_412284402], Insurer [Insurer_], ClaimNumber [Insurer_Ref]
									FROM [$(MDA)].[dbo].[Incident2Organisation] MI2A
									INNER JOIN #CT CT ON CT.MDA_ID = MI2A.ID
									WHERE CT.PhysicalTableName = 'Incident2Organisation'
									) IQ ON IQ.RowID = CT.RowID
						WHERE CT.DoNotReplicate IS NULL
						AND CT.ChangeDatabase = 'MDA'
						) IQ 
				WHERE RowNumber = 1
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET  Create_Date					= Source.Create_Date
			,Create_User					= Source.Create_User
			,Last_Upd_Date					= Source.Last_Upd_Date
			,Last_Upd_User					= Source.Last_Upd_User
			,Record_Status					= Source.Record_Status
			,Source_411765484				= Source.Source_411765484
			,Source_Description_411765489	= Source.Source_Description_411765489
			,Source_Reference_411765487		= Source.Source_Reference_411765487
			,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
			,[Insurer_]						= Source.[Insurer_]
			,[Insurer_Ref]					= Source.[Insurer_Ref]
		WHEN NOT MATCHED THEN INSERT
			(
			 Unique_ID
			,Create_Date
			,Create_User
			,Do_Not_Disseminate_412284494
			,Last_Upd_Date
			,Last_Upd_User
			,MDA_Incident_ID_412284502
			,[Incident_Match_Link_ID]
			,Record_Status
			,Source_411765484
			,Source_Description_411765489
			,Source_Reference_411765487
			,x5x5x5_Grading_412284402
			,[Insurer_]	
			,[Insurer_Ref]
			)
		VALUES
			(
			 Source.MDA_IBase_Unique_ID
			,Source.Create_Date
			,Source.Create_User
			,0
			,Source.Last_Upd_Date
			,Source.Last_Upd_User
			,CASE WHEN Source.PhysicalTableName= 'Incident2Incident' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Incident2Incident') WHEN Source.PhysicalTableName= 'Incident2Address' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Incident2Address') ELSE NULL END
			,Source.[Incident_Match_Link_ID]
			,Source.Record_Status
			,Source.Source_411765484
			,Source.Source_Description_411765489
			,Source.Source_Reference_411765487
			,Source.x5x5x5_Grading_412284402
			,[Insurer_]	
			,[Insurer_Ref]
			)
		OUTPUT   @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Incident_Match_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Incident_Match_Link_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Incident_Match_Link] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Incident_Match_Link_ID, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

		--3.2.2)DoDeletes
		--WeNeedToDeleteTheLinkEndrecordBeforetheDeletesOrWeWontBeAbleTo
		SET @Step ='3.2.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [$(MDA_IBase)].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [$(MDA_IBase)].[dbo].[_LinkEnd] LE
		INNER JOIN (
					SELECT [IBase8LinkID] Link_ID
					FROM	(
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident2Incident') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Incident2Incident'
							AND DMLAction = 'D'
							UNION ALL 
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident2Address') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Incident2Address'
							AND DMLAction = 'D'
							UNION ALL 
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident2Organisation') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Incident2Organisation'
							AND DMLAction = 'D'
							) DATA
					INNER JOIN [$(MDA_IBase)].[dbo].[VW_StagingLinks] IVSL ON IVSL.[MDA_Incident_ID_412284502] = DATA.MDA_Incident_ID_412284502
					) IQ ON IQ.Link_ID = LE.Link_ID;

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [$(MDA_IBase)].[dbo].Incident_Match_Link
		OUTPUT	 @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Incident_Match_Link_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Incident_Match_Link] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Incident_Match_Link_ID, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [$(MDA_IBase)].[dbo].Incident_Match_Link IIML
		INNER JOIN #CT CT ON CASE WHEN CT.PhysicalTableName = 'Incident2Incident' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID, 'MDA.dbo.Incident2Incident') WHEN CT.PhysicalTableName= 'Incident2Address' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident2Address') WHEN CT.PhysicalTableName= 'Incident2Organisation' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident2Organisation') ELSE NULL END = IIML.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';

		--3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		SET @Step ='3.2.3';
		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MI2I
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident1_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident1_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident2_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident2_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkType,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkType,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentMatchLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentMatchLinkId,'') + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Incident] (TaskID, DMLAction, Id, Incident1_Id, Incident2_Id, LinkType, IncidentMatchLinkId, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM [$(MDA)].[dbo].Incident2Incident MI2I
		INNER JOIN #CT CT ON CT.MDA_ID = MI2I.Id
 		WHERE (MI2I.IBaseId IS NULL
		OR MI2I.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Incident2Incident';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MI2A
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Address_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Address_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.AddressLinkId,'') + '}|I:{' + ISNULL(Inserted.AddressLinkId,'') + '}'
				,'D:{' + ISNULL(Deleted.RawAddress,'') + '}|I:{' + ISNULL(Inserted.RawAddress,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkType,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkType,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Address] (TaskID, DMLAction, Id, Incident_Id, Address_Id, AddressLinkId, RawAddress, LinkType, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [$(MDA)].[dbo].Incident2Address MI2A
		INNER JOIN #CT CT ON CT.MDA_ID = MI2A.Id
 		WHERE (MI2A.IBaseId IS NULL
		OR MI2A.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Incident2Address';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MI2O
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IsClaimLevelLink,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IsClaimLevelLink,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SubPartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SubPartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncidentTime,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncidentTime,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentCircs,'') + '}|I:{' + ISNULL(Inserted.IncidentCircs,'') + '}'
				,'D:{' + ISNULL(Deleted.IncidentLocation,'') + '}|I:{' + ISNULL(Inserted.IncidentLocation,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PaymentsToDate,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PaymentsToDate,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reserve,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reserve,'')) + '}'
				,'D:{' + ISNULL(Deleted.Insurer,'') + '}|I:{' + ISNULL(Inserted.Insurer,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimNumber,'') + '}|I:{' + ISNULL(Inserted.ClaimNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClaimType,'') + '}|I:{' + ISNULL(Inserted.ClaimType,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimCode,'') + '}|I:{' + ISNULL(Inserted.ClaimCode,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimNotificationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimNotificationDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Broker,'') + '}|I:{' + ISNULL(Inserted.Broker,'') + '}'
				,'D:{' + ISNULL(Deleted.ReferralSource,'') + '}|I:{' + ISNULL(Inserted.ReferralSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Solicitors,'') + '}|I:{' + ISNULL(Inserted.Solicitors,'') + '}'
				,'D:{' + ISNULL(Deleted.Engineer,'') + '}|I:{' + ISNULL(Inserted.Engineer,'') + '}'
				,'D:{' + ISNULL(Deleted.Recovery,'') + '}|I:{' + ISNULL(Inserted.Recovery,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage,'') + '}|I:{' + ISNULL(Inserted.Storage,'') + '}'
				,'D:{' + ISNULL(Deleted.StorageAddress,'') + '}|I:{' + ISNULL(Inserted.StorageAddress,'') + '}'
				,'D:{' + ISNULL(Deleted.Repairer,'') + '}|I:{' + ISNULL(Inserted.Repairer,'') + '}'
				,'D:{' + ISNULL(Deleted.Hire,'') + '}|I:{' + ISNULL(Inserted.Hire,'') + '}'
				,'D:{' + ISNULL(Deleted.AccidentManagement,'') + '}|I:{' + ISNULL(Inserted.AccidentManagement,'') + '}'
				,'D:{' + ISNULL(Deleted.MedicalExaminer,'') + '}|I:{' + ISNULL(Inserted.MedicalExaminer,'') + '}'
				,'D:{' + ISNULL(Deleted.UndefinedSupplier,'') + '}|I:{' + ISNULL(Inserted.UndefinedSupplier,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PoliceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PoliceAttended,'')) + '}'
				,'D:{' + ISNULL(Deleted.PoliceForce,'') + '}|I:{' + ISNULL(Inserted.PoliceForce,'') + '}'
				,'D:{' + ISNULL(Deleted.PoliceReference,'') + '}|I:{' + ISNULL(Inserted.PoliceReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AmbulanceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AmbulanceAttended,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AttendedHospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AttendedHospital,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Organisation] (TaskID, DMLAction, Id, Incident_Id, Organisation_Id, IsClaimLevelLink, IncidentLinkId, OrganisationType_Id, PartyType_Id, SubPartyType_Id, IncidentTime, IncidentCircs, IncidentLocation, PaymentsToDate, Reserve, Insurer, ClaimNumber, ClaimStatus_Id, ClaimType, ClaimCode, MojStatus_Id, ClaimNotificationDate, Broker, ReferralSource, Solicitors, Engineer, Recovery, Storage, StorageAddress, Repairer, Hire, AccidentManagement, MedicalExaminer, UndefinedSupplier, PoliceAttended, PoliceForce, PoliceReference, AmbulanceAttended, AttendedHospital, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].Incident2Organisation MI2O
		INNER JOIN #CT CT ON CT.MDA_ID = MI2O.Id
 		WHERE (MI2O.IBaseId IS NULL
		OR MI2O.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Incident2Organisation'

		--------------------------------------------------------------------------
		--3.3)MDAIBase: LinkEndTablesChanges
		-------------------------------------------------------------------------
		--3.3.1)PopulateTheWorkingTable
		--DoForEntityID1
		--CheckTheValuesThatArebeingAdded
		SET @Step ='3.3.1';

		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT	Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC
		FROM	(
				SELECT DISTINCT ROW_NUMBER() OVER (PARTITION BY Link_ID, Entity_ID1 ORDER BY Record_Status DESC) RowID, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC
				FROM	(
						SELECT MI2I.IBaseId Link_ID, MI1.IBaseId Entity_ID1, MI2I.LinkConfidence Confidence, 0 Direction, MI2.IBaseId Entity_ID2, 1688 EntityType_ID1, 1688 EntityType_ID2, @LinkTypeID LinkType_ID, MI2I.RecordStatus Record_Status, 1 Record_Type, NULL SCC
						FROM [$(MDA)].[dbo].Incident2Incident MI2I
						INNER JOIN [$(MDA)].[dbo].Incident MI1 ON MI1.ID = MI2I.[Incident1_Id]
						INNER JOIN [$(MDA)].[dbo].Incident MI2 ON MI2.ID = MI2I.[Incident2_Id]
						INNER JOIN #CT CT ON CT.MDA_ID = MI2I.ID AND CT.PhysicalTableName = 'Incident2Incident'
						UNION ALL
						SELECT MI2A.IBaseId, I.IBaseId, MI2A.LinkConfidence, 0, A.IBaseId, 1688, 1065, @LinkTypeID, MI2A.RecordStatus, 1, NULL
						FROM [$(MDA)].[dbo].Incident2Address MI2A 
						INNER JOIN [$(MDA)].[dbo].Incident I ON I.ID = MI2A.Incident_Id
						INNER JOIN [$(MDA)].[dbo].Address A ON A.ID = MI2A.Address_Id
						INNER JOIN #CT CT ON CT.MDA_ID = MI2A.ID AND CT.PhysicalTableName = 'Incident2Address'
						UNION ALL
						SELECT MI2O.IBaseId, MI.IBaseId, MI2O.LinkConfidence, 0, O.IBaseId, 1688, 1893, @LinkTypeID, MI2O.RecordStatus, 1, NULL
						FROM [MDA].[dbo].Incident2Organisation MI2O
						INNER JOIN [MDA].[dbo].Incident MI ON MI.ID = MI2O.Incident_Id
						INNER JOIN [MDA].[dbo].[Organisation] O ON O.ID = MI2O.Organisation_Id
						INNER JOIN #CT CT ON CT.MDA_ID = MI2O.ID AND CT.PhysicalTableName = 'Incident2Organisation'
						) Data
					) DATA
		WHERE RowID = 1

		--NowDoForEntityID2
		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT Link_ID, Entity_ID2, Confidence, Direction, Entity_ID1, EntityType_ID2, EntityType_ID1, LinkType_ID, Record_Status, 2, SCC
		FROM #LinkEndStaging;

		CREATE CLUSTERED INDEX IX_LinkEndStaging_Link_ID_Entity_ID1 ON #LinkEndStaging (Link_ID, Entity_ID1);

		--3.3.2)ActionTheLinkEndChangeInMDA_IBase
		SET @Step ='3.3.2';
		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		MERGE [$(MDA_IBase)].[dbo].[_LinkEnd] [Target]
		USING	(
				SELECT *
				FROM #LinkEndStaging
				) Source ON Source.Link_ID = [Target].Link_ID AND Source.Entity_ID1 = [Target].Entity_ID1
		WHEN MATCHED AND CHECKSUM(Source.Link_ID, Source.Entity_ID1, Source.Confidence, Source.Direction, Source.Entity_ID2, Source.EntityType_ID1, Source.EntityType_ID2, Source.LinkType_ID, Source.Record_Status, Source.Record_Type, Source.SCC) != CHECKSUM([Target].Link_ID, [Target].Entity_ID1, [Target].Confidence, [Target].Direction, [Target].Entity_ID2, [Target].EntityType_ID1, [Target].EntityType_ID2, [Target].LinkType_ID, [Target].Record_Status, [Target].Record_Type, [Target].SCC) THEN UPDATE
		SET  Confidence			= Source.Confidence
			,Direction			= Source.Direction
			,Entity_ID2			= Source.Entity_ID2
			,EntityType_ID1		= Source.EntityType_ID1
			,EntityType_ID2		= Source.EntityType_ID2
			,LinkType_ID		= Source.LinkType_ID
			,Record_Status		= Source.Record_Status
			,Record_Type		= Source.Record_Type
			,SCC				= Source.SCC
		WHEN NOT MATCHED THEN INSERT
			(
			 Link_ID
			,Entity_ID1
			,Confidence
			,Direction
			,Entity_ID2
			,EntityType_ID1
			,EntityType_ID2
			,LinkType_ID
			,Record_Status
			,Record_Type
			,SCC
			)
		VALUES
			(
			 Source.Link_ID
			,Source.Entity_ID1
			,Source.Confidence
			,Source.Direction
			,Source.Entity_ID2
			,Source.EntityType_ID1
			,Source.EntityType_ID2
			,Source.LinkType_ID
			,Source.Record_Status
			,Source.Record_Type
			,Source.SCC
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Link_ID],'') + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID1],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Confidence],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Direction],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID2],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID1],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID2],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkType_ID],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Type],'')) + '}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{' + ISNULL(Inserted.[SCC],'') + '}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC);

		--3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
		SET @Step ='3.3.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		DELETE  [$(MDA_IBase)].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [$(MDA_IBase)].[dbo].[_LinkEnd] LE
		WHERE EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID)
		AND NOT EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID AND LES.Entity_ID1 = LE.Entity_ID1);

	END

	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--Incident2Incident
		SET @Step ='4.1';

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [$(MDA)].[dbo].Incident2Incident [Target]
		USING	(
				SELECT	*
				FROM	(
						SELECT	 ROW_NUMBER() OVER (PARTITION BY ISNULL(CAST(CT.MDA_ID AS VARCHAR), CT.MDA_IBase_Unique_ID) ORDER BY ILE.Record_Type) RowID
								,CT.MDA_ID
								,CT.MDA_IBase_Unique_ID
								,[Sync].[fn_ExtractMDAID] (II1.MDA_Incident_ID_412284502) [Incident1_Id]
								,[Sync].[fn_ExtractMDAID] (II2.MDA_Incident_ID_412284502) [Incident2_Id]
								,0 [LinkType]
								,[Incident_Match_Link_ID] [IncidentMatchLinkId]
								,IIML.[x5x5x5_Grading_412284402] [FiveGrading] 
								,ILE.Confidence [LinkConfidence]
								,IIML.[Source_411765484] [Source]
								,IIML.[Source_Reference_411765487] [SourceReference]
								,IIML.[Source_Description_411765489] [SourceDescription]
								,IIML.Create_User [CreatedBy]
								,IIML.Create_Date [CreatedDate]
								,IIML.Last_Upd_User [ModifiedBy]
								,IIML.Last_Upd_Date [ModifiedDate]
								,IIML.Unique_ID [IBaseId]
								,IIML.Record_Status [RecordStatus]		
						FROM #CT CT
						INNER JOIN [$(MDA_IBase)].[dbo].Incident_Match_Link IIML ON IIML.Unique_ID = CT.MDA_IBase_Unique_ID
						INNER JOIN [$(MDA_IBase)].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IIML.Unique_ID
						INNER JOIN [$(MDA_IBase)].[dbo].[Incident_] II1 ON II1.Unique_ID = ILE.Entity_ID1
						INNER JOIN [$(MDA_IBase)].[dbo].[Incident_] II2 ON II2.Unique_ID = ILE.Entity_ID2
						WHERE CT.DoNotReplicate IS NULL
						AND CT.ChangeDatabase = 'MDA_IBase'
						) IQ
					WHERE RowID = 1
				) Source ON Source.MDA_ID = [Target].ID AND Source.[Incident1_Id] = [Target].[Incident1_Id] AND Source.[Incident2_Id] = [Target].[Incident2_Id] 
		WHEN MATCHED THEN UPDATE
		SET	 [Incident1_Id]				= Source.[Incident1_Id]
			,[Incident2_Id]				= Source.[Incident2_Id]
			,[LinkType]					= Source.[LinkType]
			,[IncidentMatchLinkId]		= Source.[IncidentMatchLinkId]
			,FiveGrading				= Source.FiveGrading
			,LinkConfidence				= Source.LinkConfidence
			,Source						= Source.Source
			,SourceReference			= Source.SourceReference
			,SourceDescription			= Source.SourceDescription
			,CreatedBy					= Source.CreatedBy
			,CreatedDate				= Source.CreatedDate
			,ModifiedBy					= Source.ModifiedBy
			,ModifiedDate				= Source.ModifiedDate
			,IBaseId					= Source.IBaseId
			,RecordStatus				= Source.RecordStatus
			,ADARecordStatus			= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
			 [Incident1_Id]
			,[Incident2_Id]
			,[LinkType]
			,[IncidentMatchLinkId]	
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES	
			(
			 Source.[Incident1_Id]
			,Source.[Incident2_Id]
			,Source.[LinkType]
			,Source.[IncidentMatchLinkId]
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,Source.IBaseId
			,Source.RecordStatus
			,10
			)
		OUTPUT	 @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident1_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident1_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident2_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident2_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkType,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkType,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentMatchLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentMatchLinkId,'') + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Incident] (TaskID, DMLAction, Id, Incident1_Id, Incident2_Id, LinkType, IncidentMatchLinkId, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus);

		--Incident2Address
		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [$(MDA)].[dbo].Incident2Address [Target]
		USING	(
				SELECT	*
				FROM	(
						SELECT	 ROW_NUMBER() OVER (PARTITION BY ISNULL(CAST(CT.MDA_ID AS VARCHAR), CT.MDA_IBase_Unique_ID) ORDER BY ILE.Record_Type) RowID
								,CT.MDA_ID
								,CT.MDA_IBase_Unique_ID
								,[Sync].[fn_ExtractMDAID] (I.MDA_Incident_ID_412284502) [Incident_Id]
								,[Sync].[fn_ExtractMDAID] (A.MDA_Incident_ID_412284502) [Address_Id]
								,IIML.[Incident_Match_Link_ID] [AddressLinkId]
								,0 [LinkType]
								,IIML.[x5x5x5_Grading_412284402] [FiveGrading]
								,ILE.Confidence [LinkConfidence]
								,IIML.[Source_411765484] [Source]
								,IIML.[Source_Reference_411765487] [SourceReference]
								,IIML.[Source_Description_411765489] [SourceDescription]
								,IIML.Create_User [CreatedBy]
								,IIML.Create_Date [CreatedDate]
								,IIML.Last_Upd_User [ModifiedBy]
								,IIML.Last_Upd_Date [ModifiedDate]
								,IIML.[Risk_Claim_ID_414244883] RiskClaim_Id
								,IIML.[Risk_Claim_ID_414244883] BaseRiskClaim_ID
								,IIML.Unique_ID [IBaseId]
								,IIML.Record_Status [RecordStatus]		
						FROM #CT CT
						INNER JOIN [$(MDA_IBase)].[dbo].Incident_Match_Link IIML ON IIML.Unique_ID = CT.MDA_IBase_Unique_ID
						INNER JOIN [$(MDA_IBase)].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IIML.Unique_ID
						INNER JOIN [$(MDA_IBase)].[dbo].[Incident_] I ON I.Unique_ID = ILE.Entity_ID1
						INNER JOIN [$(MDA_IBase)].[dbo].[Address_] A ON A.Unique_ID = ILE.Entity_ID2
						WHERE CT.DoNotReplicate IS NULL
						AND CT.ChangeDatabase = 'MDA_IBase'
						AND [Sync].[fn_ExtractMDAID] (I.MDA_Incident_ID_412284502) IS NOT NULL
						AND [Sync].[fn_ExtractMDAID] (A.MDA_Incident_ID_412284502) IS NOT NULL
						) IQ
				WHERE RowID = 1
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET	 [Incident_Id]			= Source.[Incident_Id]
			,[Address_Id]			= Source.[Address_Id]
			,[AddressLinkId]		= Source.[AddressLinkId]
			,[LinkType]				= Source.[LinkType]
			,FiveGrading			= Source.FiveGrading
			,LinkConfidence			= Source.LinkConfidence
			,Source					= Source.Source
			,SourceReference		= Source.SourceReference
			,SourceDescription		= Source.SourceDescription
			,CreatedBy				= Source.CreatedBy
			,CreatedDate			= Source.CreatedDate
			,ModifiedBy				= Source.ModifiedBy
			,ModifiedDate			= Source.ModifiedDate
			,IBaseId				= Source.IBaseId
			,RecordStatus			= Source.RecordStatus
			,ADARecordStatus		= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
	WHEN NOT MATCHED THEN INSERT
			(
			 [Incident_Id]
			,[Address_Id]
			,[AddressLinkId]
			,[LinkType]
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES	
			(
			 Source.[Incident_Id]
			,Source.[Address_Id]
			,Source.[AddressLinkId]
			,Source.[LinkType]
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Address_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Address_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.AddressLinkId,'') + '}|I:{' + ISNULL(Inserted.AddressLinkId,'') + '}'
				,'D:{' + ISNULL(Deleted.RawAddress,'') + '}|I:{' + ISNULL(Inserted.RawAddress,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkType,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkType,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Address] (TaskID, DMLAction, Id, Incident_Id, Address_Id, AddressLinkId, RawAddress, LinkType, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus);

		--IncidentToOrganisation
		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].Incident2Organisation [Target]
		USING	(
				SELECT	*
				FROM	(
						SELECT	 ROW_NUMBER() OVER (PARTITION BY ISNULL(CAST(CT.MDA_ID AS VARCHAR), CT.MDA_IBase_Unique_ID) ORDER BY ISNULL(CAST(CT.MDA_ID AS VARCHAR), CT.MDA_IBase_Unique_ID)) RowID
								,CT.MDA_ID
								,CT.MDA_IBase_Unique_ID
								,[Sync].[fn_ExtractMDAID] (MI.MDA_Incident_ID_412284502) [Incident_Id]
								,[Sync].[fn_ExtractMDAID] (MO.MDA_Incident_ID_412284502) [Organisation_Id]
								,IIL.[Incident_Match_Link_ID] IncidentLinkId
								,0 PartyType_Id
								,0 SubPartyType_Id
								,0 MojStatus_Id
								,LEFT(IIL.[Insurer_],255) Insurer
								,IIL.[Insurer_Ref] ClaimNumber
								,0 ClaimStatus_Id
								,LEFT(IIL.[x5x5x5_Grading_412284402],10) [FiveGrading]
								,ILE.Confidence [LinkConfidence]
								,IIL.[Source_411765484] [Source]
								,IIL.[Source_Reference_411765487] [SourceReference]
								,LEFT(IIL.[Source_Description_411765489],1024) [SourceDescription]
								,IIL.Create_User [CreatedBy]
								,IIL.Create_Date [CreatedDate]
								,IIL.Last_Upd_User [ModifiedBy]
								,IIL.Last_Upd_Date [ModifiedDate]
								,IIL.[Risk_Claim_ID_414244883] [RiskClaim_Id]
								,IIL.[Risk_Claim_ID_414244883] [BaseRiskClaim_Id]
								,IIL.Unique_ID [IBaseId]
								,IIL.Record_Status [RecordStatus]
						FROM #CT CT
						INNER JOIN [MDA_IBase].[dbo].Incident_Match_Link IIL ON IIL.Unique_ID = CT.MDA_IBase_Unique_ID
						INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IIL.Unique_ID
						INNER JOIN [MDA_IBase].[dbo].[Incident_] MI ON MI.Unique_ID = ILE.Entity_ID1
						INNER JOIN [MDA_IBase].[dbo].[Organisation_] MO ON MO.Unique_ID = ILE.Entity_ID2
						WHERE CT.DoNotReplicate IS NULL
						AND CT.ChangeDatabase = 'MDA_IBase'
						AND MI.[Incident_Date] < GETDATE()
					) Data
				WHERE RowID = 1
			) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  Incident_Id			= Source.Incident_Id
			,Organisation_Id		= Source.Organisation_Id
			,IncidentLinkId			= Source.IncidentLinkId
			,PartyType_Id			= Source.PartyType_Id
			,SubPartyType_Id		= Source.SubPartyType_Id
			,MojStatus_Id			= Source.MojStatus_Id
			,Insurer				= Source.Insurer
			,ClaimNumber			= Source.ClaimNumber
			,ClaimStatus_Id			= Source.ClaimStatus_Id
			,FiveGrading			= Source.FiveGrading
			,LinkConfidence			= Source.LinkConfidence
			,Source					= Source.Source
			,SourceReference		= Source.SourceReference
			,SourceDescription		= Source.SourceDescription
			,CreatedBy				= Source.CreatedBy
			,CreatedDate			= Source.CreatedDate
			,ModifiedBy				= Source.ModifiedBy
			,ModifiedDate			= Source.ModifiedDate
			,RiskClaim_Id			= Source.RiskClaim_Id
			,BaseRiskClaim_Id		= Source.BaseRiskClaim_Id
			,IBaseId				= Source.IBaseId
			,RecordStatus			= Source.RecordStatus
			,ADARecordStatus		= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT 
			(
			 Incident_Id
			,Organisation_Id
			,IncidentLinkId
			,PartyType_Id
			,SubPartyType_Id
			,MojStatus_Id
			,Insurer
			,ClaimNumber
			,ClaimStatus_Id
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES
			(
			 Source.Incident_Id
			,Source.Organisation_Id
			,Source.IncidentLinkId
			,Source.PartyType_Id
			,Source.SubPartyType_Id
			,Source.MojStatus_Id
			,Source.Insurer
			,Source.ClaimNumber
			,Source.ClaimStatus_Id
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10
			)
		OUTPUT	 @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IsClaimLevelLink,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IsClaimLevelLink,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SubPartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SubPartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncidentTime,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncidentTime,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentCircs,'') + '}|I:{' + ISNULL(Inserted.IncidentCircs,'') + '}'
				,'D:{' + ISNULL(Deleted.IncidentLocation,'') + '}|I:{' + ISNULL(Inserted.IncidentLocation,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PaymentsToDate,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PaymentsToDate,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reserve,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reserve,'')) + '}'
				,'D:{' + ISNULL(Deleted.Insurer,'') + '}|I:{' + ISNULL(Inserted.Insurer,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimNumber,'') + '}|I:{' + ISNULL(Inserted.ClaimNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClaimType,'') + '}|I:{' + ISNULL(Inserted.ClaimType,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimCode,'') + '}|I:{' + ISNULL(Inserted.ClaimCode,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimNotificationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimNotificationDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Broker,'') + '}|I:{' + ISNULL(Inserted.Broker,'') + '}'
				,'D:{' + ISNULL(Deleted.ReferralSource,'') + '}|I:{' + ISNULL(Inserted.ReferralSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Solicitors,'') + '}|I:{' + ISNULL(Inserted.Solicitors,'') + '}'
				,'D:{' + ISNULL(Deleted.Engineer,'') + '}|I:{' + ISNULL(Inserted.Engineer,'') + '}'
				,'D:{' + ISNULL(Deleted.Recovery,'') + '}|I:{' + ISNULL(Inserted.Recovery,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage,'') + '}|I:{' + ISNULL(Inserted.Storage,'') + '}'
				,'D:{' + ISNULL(Deleted.StorageAddress,'') + '}|I:{' + ISNULL(Inserted.StorageAddress,'') + '}'
				,'D:{' + ISNULL(Deleted.Repairer,'') + '}|I:{' + ISNULL(Inserted.Repairer,'') + '}'
				,'D:{' + ISNULL(Deleted.Hire,'') + '}|I:{' + ISNULL(Inserted.Hire,'') + '}'
				,'D:{' + ISNULL(Deleted.AccidentManagement,'') + '}|I:{' + ISNULL(Inserted.AccidentManagement,'') + '}'
				,'D:{' + ISNULL(Deleted.MedicalExaminer,'') + '}|I:{' + ISNULL(Inserted.MedicalExaminer,'') + '}'
				,'D:{' + ISNULL(Deleted.UndefinedSupplier,'') + '}|I:{' + ISNULL(Inserted.UndefinedSupplier,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PoliceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PoliceAttended,'')) + '}'
				,'D:{' + ISNULL(Deleted.PoliceForce,'') + '}|I:{' + ISNULL(Inserted.PoliceForce,'') + '}'
				,'D:{' + ISNULL(Deleted.PoliceReference,'') + '}|I:{' + ISNULL(Inserted.PoliceReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AmbulanceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AmbulanceAttended,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AttendedHospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AttendedHospital,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Organisation] (TaskID, DMLAction, Id, Incident_Id, Organisation_Id, IsClaimLevelLink, IncidentLinkId, OrganisationType_Id, PartyType_Id, SubPartyType_Id, IncidentTime, IncidentCircs, IncidentLocation, PaymentsToDate, Reserve, Insurer, ClaimNumber, ClaimStatus_Id, ClaimType, ClaimCode, MojStatus_Id, ClaimNotificationDate, Broker, ReferralSource, Solicitors, Engineer, Recovery, Storage, StorageAddress, Repairer, Hire, AccidentManagement, MedicalExaminer, UndefinedSupplier, PoliceAttended, PoliceForce, PoliceReference, AmbulanceAttended, AttendedHospital, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus);

		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--Incident2Incident
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [$(MDA)].[dbo].Incident2Incident
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident1_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident1_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident2_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident2_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkType,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkType,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentMatchLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentMatchLinkId,'') + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Incident] (TaskID, DMLAction, Id, Incident1_Id, Incident2_Id, LinkType, IncidentMatchLinkId, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM [$(MDA)].[dbo].Incident2Incident MI2I
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MI2I.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--Incident2Address
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [$(MDA)].[dbo].Incident2Address
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Address_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Address_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.AddressLinkId,'') + '}|I:{' + ISNULL(Inserted.AddressLinkId,'') + '}'
				,'D:{' + ISNULL(Deleted.RawAddress,'') + '}|I:{' + ISNULL(Inserted.RawAddress,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkType,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkType,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Address] (TaskID, DMLAction, Id, Incident_Id, Address_Id, AddressLinkId, RawAddress, LinkType, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [$(MDA)].[dbo].Incident2Address MI2A
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MI2A.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--Incident2Organisation
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Incident2Organisation]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IsClaimLevelLink,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IsClaimLevelLink,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SubPartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SubPartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncidentTime,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncidentTime,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentCircs,'') + '}|I:{' + ISNULL(Inserted.IncidentCircs,'') + '}'
				,'D:{' + ISNULL(Deleted.IncidentLocation,'') + '}|I:{' + ISNULL(Inserted.IncidentLocation,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PaymentsToDate,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PaymentsToDate,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reserve,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reserve,'')) + '}'
				,'D:{' + ISNULL(Deleted.Insurer,'') + '}|I:{' + ISNULL(Inserted.Insurer,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimNumber,'') + '}|I:{' + ISNULL(Inserted.ClaimNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClaimType,'') + '}|I:{' + ISNULL(Inserted.ClaimType,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimCode,'') + '}|I:{' + ISNULL(Inserted.ClaimCode,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimNotificationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimNotificationDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Broker,'') + '}|I:{' + ISNULL(Inserted.Broker,'') + '}'
				,'D:{' + ISNULL(Deleted.ReferralSource,'') + '}|I:{' + ISNULL(Inserted.ReferralSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Solicitors,'') + '}|I:{' + ISNULL(Inserted.Solicitors,'') + '}'
				,'D:{' + ISNULL(Deleted.Engineer,'') + '}|I:{' + ISNULL(Inserted.Engineer,'') + '}'
				,'D:{' + ISNULL(Deleted.Recovery,'') + '}|I:{' + ISNULL(Inserted.Recovery,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage,'') + '}|I:{' + ISNULL(Inserted.Storage,'') + '}'
				,'D:{' + ISNULL(Deleted.StorageAddress,'') + '}|I:{' + ISNULL(Inserted.StorageAddress,'') + '}'
				,'D:{' + ISNULL(Deleted.Repairer,'') + '}|I:{' + ISNULL(Inserted.Repairer,'') + '}'
				,'D:{' + ISNULL(Deleted.Hire,'') + '}|I:{' + ISNULL(Inserted.Hire,'') + '}'
				,'D:{' + ISNULL(Deleted.AccidentManagement,'') + '}|I:{' + ISNULL(Inserted.AccidentManagement,'') + '}'
				,'D:{' + ISNULL(Deleted.MedicalExaminer,'') + '}|I:{' + ISNULL(Inserted.MedicalExaminer,'') + '}'
				,'D:{' + ISNULL(Deleted.UndefinedSupplier,'') + '}|I:{' + ISNULL(Inserted.UndefinedSupplier,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PoliceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PoliceAttended,'')) + '}'
				,'D:{' + ISNULL(Deleted.PoliceForce,'') + '}|I:{' + ISNULL(Inserted.PoliceForce,'') + '}'
				,'D:{' + ISNULL(Deleted.PoliceReference,'') + '}|I:{' + ISNULL(Inserted.PoliceReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AmbulanceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AmbulanceAttended,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AttendedHospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AttendedHospital,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Organisation] (TaskID, DMLAction, Id, Incident_Id, Organisation_Id, IsClaimLevelLink, IncidentLinkId, OrganisationType_Id, PartyType_Id, SubPartyType_Id, IncidentTime, IncidentCircs, IncidentLocation, PaymentsToDate, Reserve, Insurer, ClaimNumber, ClaimStatus_Id, ClaimType, ClaimCode, MojStatus_Id, ClaimNotificationDate, Broker, ReferralSource, Solicitors, Engineer, Recovery, Storage, StorageAddress, Repairer, Hire, AccidentManagement, MedicalExaminer, UndefinedSupplier, PoliceAttended, PoliceForce, PoliceReference, AmbulanceAttended, AttendedHospital, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Incident2Organisation] MI2O
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MI2O.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [$(MDA_IBase)].[dbo].[Incident_Match_Link]
		SET  [MDA_Incident_ID_412284502] = IQ.MDA_Incident_ID_412284502
			,[Risk_Claim_ID_414244883]	 = IQ.RiskClaim_Id
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Incident_Match_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Incident_Match_Link_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Incident_Match_Link] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Incident_Match_Link_ID, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [$(MDA_IBase)].[dbo].[Incident_Match_Link] IIML
		INNER JOIN (
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MI2I.ID,'MDA.dbo.Incident2Incident') MDA_Incident_ID_412284502, NULL RiskClaim_Id
					FROM [$(MDA)].[dbo].Incident2Incident MI2I
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MI2I.IBaseId
					UNION ALL
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MI2A.ID,'MDA.dbo.Incident2Address') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [$(MDA)].[dbo].Incident2Address MI2A
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MI2A.IBaseId
					UNION ALL
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MI2A.ID,'MDA.dbo.Incident2Organisation') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [$(MDA)].[dbo].Incident2Organisation MI2A
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MI2A.IBaseId
					) IQ ON IQ.[Unique_ID] =  IIML.[Unique_ID]
		WHERE IIML.MDA_Incident_ID_412284502 IS NULL
		OR IIML.MDA_Incident_ID_412284502 != IQ.MDA_Incident_ID_412284502;

	END

	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Incident] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Incident] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Incident] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_Match_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_Match_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_Match_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging

END TRY
BEGIN CATCH
	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(500)	= ISNULL(ERROR_PROCEDURE(),@@PROCID)
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +ISNULL(@Step,'?') +']'
	
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

