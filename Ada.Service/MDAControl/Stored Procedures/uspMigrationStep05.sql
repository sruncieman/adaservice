﻿
CREATE PROCEDURE [IBaseM].[uspMigrationStep05]
/**************************************************************************************************/
-- ObjectName:			IBaseM.uspMigrationStep05
--
-- Description:			Populate the link tables within IBase8 Target database
--
-- Steps:				
--
-- Dependencies:		
--
-- Usage:				Called within SP IBaseM.uspMigrationMaster
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 12 --Populate_LinkEnd
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
			
	------------------------------------------------------------
	--2)MainBody
	------------------------------------------------------------
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._LinkEnd'

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndAccountLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndAddressToAddressLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndBBPinLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndCaseToIncidentLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndDrivingLicenceLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndEmailLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndFraudRingIncidentLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndIncidentMatchLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndPassportLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndPaymentCardLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndPersonToOrganisationLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndPolicyLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndPersonToPersonLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndPolicyPaymentLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndTelephoneLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndVehicleIncidentLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndVehicleLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndVehicleToVehicleLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndWebsiteLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndNINumberLink

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	EXECUTE IBaseM.uspGetLinkEndIP

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	SELECT 
		L.Unique_ID,
		LE.Entity_ID1, 
		LE.Confidence, 
		LE.Direction, 
		LE.Entity_ID2, 
		1893 EntityType_ID1, 
		1893 EntityType_ID2, 
		2595 LinkType_ID, 
		LE.Record_Status, 
		LE.Record_Type, 
		LE.SCC
	FROM IBaseM5Cur._LinkEnd AS LE 
	INNER JOIN IBaseM8Cur.Organisation_Match_Link AS L ON LE.Link_ID = L.Organisation_Match_Link_ID

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	SELECT
		X.Unique_ID,
		E1.Unique_ID,
		LE.Confidence, 
		LE.Direction, 
		E2.Unique_ID,
		E1.Table_ID,
		E2.Table_ID,
		2967 LinkType_ID, 
		LE.Record_Status, 
		LE.Record_Type, 
		LE.SCC
	FROM IBaseM5Cur._LinkEnd AS LE WITH (NOLOCK) 
	INNER JOIN IBaseM5Cur.Intelligence_Link AS I WITH (NOLOCK) ON LE.Link_ID = I.Unique_ID    
	INNER JOIN IBaseM.vNew_Id_Old_Id_Union E1  WITH (NOLOCK)ON Entity_ID1 = E1.Old_Unique_ID
	INNER JOIN IBaseM.vNew_Id_Old_Id_Union E2  WITH (NOLOCK)ON Entity_ID2 = E2.Old_Unique_ID
	INNER JOIN IBaseM.vNew_Id_Old_Id_Intelligence_Link X  WITH(NOLOCK) ON X.Old_Unique_ID = I.Unique_ID

	DELETE FROM IBaseM8Cur._LinkEnd WHERE Link_ID LIKE 'IN1%'
	INSERT INTO IBaseM8Cur._LinkEnd 
	SELECT 
		ID AS Link_ID,	
		CLA_VEH_Ent1 AS Entity_ID1	,
		0 AS Confidence,	
		0 AS Direction,	
		VEH_PER_Ent2 AS Entity_ID2,	
		1688 AS EntityType_ID1,	
		1969 AS EntityType_ID2,	
		2702 AS LinkType_ID,	
		0 AS Record_Status,	
		1 AS Record_Type,	
		'' AS SCC
	FROM IBaseM.IncidentLinks_Temp WITH(NOLOCK)
	WHERE VEH_PER_Ent2 IS NOT NULL

	INSERT INTO IBaseM8Cur._LinkEnd 
	SELECT 
		ID AS Link_ID,	
		VEH_PER_Ent2 AS Entity_ID1	,
		0 AS Confidence,	
		0 AS Direction,	
		CLA_VEH_Ent1 AS Entity_ID2,	
		1969 AS EntityType_ID1,	
		1688 AS EntityType_ID2,	
		2702 AS LinkType_ID,	
		0 AS Record_Status,	
		2 AS Record_Type,	
		'' AS SCC
	FROM IBaseM.IncidentLinks_Temp WITH(NOLOCK)
	WHERE VEH_PER_Ent2 IS NOT NULL

	INSERT INTO IBaseM8Cur._LinkEnd 
	SELECT
		B.Unique_ID,
		Entity_ID1,	
		Confidence,	
		Direction,	
		C.Unique_ID AS  Entity_ID2,	
		1969 AS EntityType_ID1,	
		C.Table_ID AS EntityType_ID2,	
		2702 AS LinkType_ID,	
		A.Record_Status,	
		Record_Type,
		A.SCC
	FROM IBaseM5Cur._LinkEnd A  WITH(NOLOCK)
	INNER JOIN IBaseM8Cur.Incident_Link B WITH(NOLOCK) ON A.Link_ID = B.Incident_Link_ID AND B.Incident_Link_ID LIKE 'M%' AND Entity_ID1 LIKE 'PER%'
	INNER JOIN IBaseM.vNew_Id_Old_Id_Incident_ C WITH(NOLOCK) ON A.Entity_ID2 = C.Old_Unique_ID

	INSERT INTO IBaseM8Cur._LinkEnd 
	SELECT
		B.Unique_ID,
		C.Unique_ID AS  Entity_ID1,	
		Confidence,	
		Direction,	
		Entity_ID2,	
		C.Table_ID AS  EntityType_ID1,	
		1969 AS EntityType_ID2,	
		2702 AS LinkType_ID,	
		A.Record_Status,	
		Record_Type,
		A.SCC
	FROM IBaseM5Cur._LinkEnd A WITH(NOLOCK)
	INNER JOIN IBaseM8Cur.Incident_Link B WITH(NOLOCK) ON A.Link_ID = B.Incident_Link_ID AND B.Incident_Link_ID LIKE 'M%' AND Entity_ID2 LIKE 'PER%'
	INNER JOIN IBaseM.vNew_Id_Old_Id_Incident_ C WITH(NOLOCK) ON A.Entity_ID1 = C.Old_Unique_ID

	INSERT INTO IBaseM8Cur._LinkEnd 
	SELECT
		A.Unique_ID,
		C.Unique_ID AS Entity_ID1,	
		Confidence,	
		Direction,	
		Entity_ID2,
		C.Table_ID AS EntityType_ID1,	
		CASE EntityType_ID2 
			WHEN 1001 THEN 1969 
			WHEN 1005 THEN 1893 
		END AS EntityType_ID2,
		2702 AS LinkType_ID,	
		B.Record_Status,	
		Record_Type,
		B.SCC
	FROM IBaseM8Cur.Incident_Link A WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd B WITH(NOLOCK) ON A.Incident_Link_ID = B.Link_ID AND Entity_ID1 LIKE 'CLA%'
	INNER JOIN IBaseM.vNew_Id_Old_Id_Incident_ C WITH(NOLOCK) ON C.Old_Unique_ID = B.Entity_ID1
	WHERE A.MDA_Incident_ID_412284502 = '001'

	INSERT INTO IBaseM8Cur._LinkEnd 
	SELECT
		A.Unique_ID,
		Entity_ID1,	
		Confidence,	
		Direction,	
		C.Unique_ID AS Entity_ID2,	
		CASE EntityType_ID1 
			WHEN 1001 THEN 1969 
			WHEN 1005 THEN 1893 
		END AS EntityType_ID1,
		C.Table_ID AS EntityType_ID2,
		2702 AS LinkType_ID,	
		B.Record_Status,	
		Record_Type,
		B.SCC
	FROM IBaseM8Cur.Incident_Link A WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd B WITH(NOLOCK) ON A.Incident_Link_ID = B.Link_ID AND Entity_ID2 LIKE 'CLA%'
	INNER JOIN IBaseM.vNew_Id_Old_Id_Incident_ C WITH(NOLOCK) ON C.Old_Unique_ID = B.Entity_ID2
	WHERE A.MDA_Incident_ID_412284502 = '001'

	DECLARE @T TABLE (	Link_ID VARCHAR(50),
						Entity_ID1 VARCHAR(50),
						Confidence TINYINT,
						Direction TINYINT,
						Entity_ID2 VARCHAR(50),
						EntityType_ID1 INT,
						EntityType_ID2 INT,
						LinkType_ID INT,
						Record_Status TINYINT,
						Record_Type TINYINT,
						SCC VARCHAR(50)) --AS t_LinkEnd

	INSERT INTO @T
	SELECT   DISTINCT
		B.Unique_ID AS LINK_ID, 
		C.Unique_ID AS Entity_ID1,
		A.Confidence,
		A.Direction, 
		ISNULL(P.Unique_ID, O.Unique_ID) AS  Entity_ID2,
		C.Table_ID AS EntityType_ID1,
		ISNULL(P.Table_ID, O.Table_ID) AS EntityType_ID2,
		2702 AS LinkType_ID,
		A.Record_Status,
		Record_Type,
		A.SCC	
	FROM IBaseM5Cur._LinkEnd A WITH(NOLOCK)
	INNER JOIN IBaseM8Cur.Incident_Link B WITH(NOLOCK) ON a.Link_ID = B.Incident_Link_ID
	INNER JOIN IBaseM.vNew_Id_Old_Id_Incident_ C WITH(NOLOCK) ON A.Entity_ID1 = C.Old_Unique_ID
	LEFT JOIN IBaseM.vNew_Id_Old_Id_Person_ P WITH(NOLOCK) ON P.Old_Unique_ID = A.Entity_ID2
	LEFT JOIN IBaseM.vNew_Id_Old_Id_Organisation_ O WITH(NOLOCK) ON O.Old_Unique_ID = A.Entity_ID2
	WHERE Source_Description_411765489 = 'Insured Claim' 
	AND ISNULL(P.Table_ID, O.Table_ID) IS NOT NULL
	AND B.Unique_ID NOT IN (SELECT Link_ID FROM IBaseM8Cur._LinkEnd )
			
	INSERT INTO  IBaseM8Cur._LinkEnd 
	SELECT   
		Link_ID,
		Entity_ID1,
		Confidence,
		Direction,
		Entity_ID2,
		EntityType_ID1,
		EntityType_ID2,
		LinkType_ID,
		Record_Status,
		Record_Type,
		SCC
	FROM @T
	UNION ALL
	SELECT   
		Link_ID,
		Entity_ID2,
		Confidence,
		Direction,
		Entity_ID1,
		EntityType_ID2,
		EntityType_ID1,
		LinkType_ID,
		Record_Status,
		CASE Record_Type 
			WHEN 1 THEN 2
			WHEN 2 THEN 1
		END AS Record_Type,
		SCC
	FROM @T

	INSERT INTO  IBaseM8Cur._LinkEnd 
	SELECT 
		T.Unique_ID,
		T1.Unique_ID,
		L.Confidence,
		L.Direction,
		A1.Unique_ID,
		T1.Table_ID,
		A1.Table_ID,
		2672 AS Link_Type_Id,
		L.Record_Status,
		L.Record_Type,
		L.SCC
	FROM IBaseM5Cur._LinkEnd L WITH(NOLOCK)
	INNER JOIN  IBaseM8Cur.Telephone_Link T WITH(NOLOCK)
	ON L.Link_ID = T.Telephone_Link_ID
	INNER JOIN IBaseM.vNew_Id_Old_Id_Address_ A1 WITH(NOLOCK)
	ON Entity_ID2 = A1.Old_Unique_ID
	INNER JOIN IBaseM.vNew_Id_Old_Id_Telephone_ T1 WITH(NOLOCK)
	ON Entity_ID1 = T1.Old_Unique_ID
	WHERE Link_ID LIKE 'LO0%'

	INSERT INTO  IBaseM8Cur._LinkEnd 
	SELECT 
		T.Unique_ID,
		A1.Unique_ID,
		L.Confidence,
		L.Direction,
		T1.Unique_ID,
		A1.Table_ID,
		T1.Table_ID,
		2672 AS Link_Type_Id,
		L.Record_Status,
		L.Record_Type,
		L.SCC
	FROM IBaseM5Cur._LinkEnd L WITH(NOLOCK)
	INNER JOIN  IBaseM8Cur.Telephone_Link T WITH(NOLOCK)
	ON L.Link_ID = T.Telephone_Link_ID
	INNER JOIN IBaseM.vNew_Id_Old_Id_Address_ A1 WITH(NOLOCK)
	ON Entity_ID1 = A1.Old_Unique_ID
	INNER JOIN IBaseM.vNew_Id_Old_Id_Telephone_ T1 WITH(NOLOCK)
	ON Entity_ID2 = T1.Old_Unique_ID
	WHERE Link_ID LIKE 'LO0%' 
	
	/*PA:PerformanceAffecting,20140314, removed and replaced with procedding code
	INSERT INTO  IBaseM8Cur._LinkEnd 
	SELECT
		A.Unique_ID,
		B.Unique_ID,
		LE.Confidence,
		LE.Direction,
		C.Unique_ID,
		B.Table_ID,
		C.Table_ID,
		2519 LinkType_ID,
		LE.Record_Status,
		LE.Record_Type,
		LE.SCC
	FROM IBaseM5Cur.MIAFTR_Match_Link L WITH (NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd LE WITH (NOLOCK) ON L.Unique_ID =  LE.Link_ID 
	INNER JOIN IBaseM8Cur.Incident_Match_Link A WITH (NOLOCK) ON A.Incident_Match_Link_ID = L.Unique_ID
	INNER JOIN IBaseM.vNew_Id_Old_Id_Union B  WITH (NOLOCK) ON LE.Entity_ID1 = B.Old_Unique_ID 
	INNER JOIN IBaseM.vNew_Id_Old_Id_Union C  WITH (NOLOCK) ON LE.Entity_ID2 = C.Old_Unique_ID
	WHERE A.Unique_ID + B.Unique_ID  NOT IN (SELECT Link_ID + Entity_ID1 FROM IBaseM8Cur._LinkEnd )
	*/
	IF OBJECT_ID('tempdb..#PerformanceTemp') IS NOT NULL
		DROP TABLE #PerformanceTemp
	
	SELECT 
		A.Unique_ID Link_ID,
		B.Unique_ID Entity_ID1,
		LE.Confidence,
		LE.Direction,
		C.Unique_ID Entity_ID2,
		B.Table_ID EntityType_ID1,
		C.Table_ID EntityType_ID2,
		2519 LinkType_ID,
		LE.Record_Status,
		LE.Record_Type,
		LE.SCC 
	INTO #PerformanceTemp
	FROM IBaseM5Cur.MIAFTR_Match_Link L WITH (NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd LE WITH (NOLOCK) ON L.Unique_ID =  LE.Link_ID 
	INNER JOIN IBaseM8Cur.Incident_Match_Link A WITH (NOLOCK) ON A.Incident_Match_Link_ID = L.Unique_ID
	INNER JOIN IBaseM.vNew_Id_Old_Id_Union B  WITH (NOLOCK) ON LE.Entity_ID1 = B.Old_Unique_ID 
	INNER JOIN IBaseM.vNew_Id_Old_Id_Union C  WITH (NOLOCK) ON LE.Entity_ID2 = C.Old_Unique_ID
	
	CREATE CLUSTERED INDEX IX_#PerformanceTemp_Link_ID_Entity_ID1_C  ON #PerformanceTemp (Link_ID, Entity_ID1)
	EXECUTE [IBaseM].[uspRebuildTableIndexAllSynonym] 'IBaseM8Cur._LinkEnd', 'PK__LinkEnd'

	INSERT INTO IBaseM8Cur._LinkEnd (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	SELECT Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC
	FROM #PerformanceTemp PT
	WHERE NOT EXISTS (SELECT TOP 1 1 FROM IBaseM8Cur._LinkEnd LE WHERE LE.Link_ID + LE.Entity_ID1 = PT.Link_ID + PT.Entity_ID1 )
	
	IF OBJECT_ID('tempdb..#PerformanceTemp') IS NOT NULL
		DROP TABLE #PerformanceTemp
	
	DECLARE @NextID INT
	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Incident_Link

	INSERT INTO IBaseM8Cur.Incident_Link(
		Unique_ID, 
		Incident_Link_ID,
		AltEntity,
		Create_Date, 
		Create_User,
		Source_Description_411765489,
		Source_Reference_411765487,
		Last_Upd_Date, 
		Last_Upd_User,
		Record_Status, 
		SCC,   
		Status_Binding,
		IconColour,
		Police_Attended, 
		Ambulance_Attended, 
		Do_Not_Disseminate_412284494,
		Sub_Party_Type
		)
	SELECT
	 		CNT,
			Unique_ID AS Unique_ID, 
			AltEntity AS AltEntity, 
			Create_Date	AS Create_Date, 
			Create_User	AS Create_User, 
			DB_Source_387004609, 
			NULL AS Link_Type, 
			Last_Upd_Date AS Last_Upd_Date, 
			Last_Upd_User AS Last_Upd_User,
			X.Record_Status AS Record_Status, 
			X.SCC	AS SCC, 
			Status_Binding AS Status_Binding,
			IconColour AS IconColour, 
			0 Police_Attended, 
			0 Ambulance_Attended, 
			1 Do_Not_Disseminate_412284494,
			Description_
	FROM (
	SELECT 
			'IN1' + CONVERT(VARCHAR(20), DENSE_RANK() OVER(ORDER BY N.Unique_ID) + @NextID) AS CNT,
			N.*, 
			Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, L.Record_Status LE_Record_Status, Record_Type, L.SCC LE_SCC
	FROM IBaseM5Cur.Nominal_Link N  WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd L  WITH(NOLOCK) ON N.Unique_ID = L.Link_ID AND(Entity_ID1 LIKE 'PER%' AND Entity_ID2 LIKE 'CLA%'OR Entity_ID1 LIKE 'CLA%' AND Entity_ID2 LIKE 'PER%')
	LEFT JOIN IBaseM.vNew_Id_Old_Id_Incident_ E1 WITH(NOLOCK) ON Entity_ID1 = E1.Old_Unique_ID
	LEFT JOIN IBaseM.vNew_Id_Old_Id_Incident_ E2  WITH(NOLOCK) ON Entity_ID2 = E2.Old_Unique_ID
	) AS X
	WHERE Record_Type = 1

	INSERT INTO  IBaseM8Cur._LinkEnd 
	SELECT 
		'IN1' + CONVERT(VARCHAR(20), DENSE_RANK() OVER(ORDER BY N.Unique_ID) + @NextID) AS CNT,
		ISNULL(E1.Unique_ID, Entity_ID1)  Entity_ID1, 
		Confidence, 
		Direction, 
		ISNULL(E2.Unique_ID, Entity_ID2)  Entity_ID2, 
		ISNULL(E1.Table_ID,  E2.Table_ID)  EntityType_ID1, 
		ISNULL(E2.Table_ID,  E1.Table_ID)  EntityType_ID2, 
		2702 LinkType_ID, 
		L.Record_Status 
		LE_Record_Status, 
		Record_Type, 
		L.SCC 
		LE_SCC
	FROM IBaseM5Cur.Nominal_Link N  WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd L  WITH(NOLOCK) ON N.Unique_ID = L.Link_ID AND(Entity_ID1 LIKE 'PER%' AND Entity_ID2 LIKE 'CLA%'OR Entity_ID1 LIKE 'CLA%' AND Entity_ID2 LIKE 'PER%')
	LEFT JOIN IBaseM.vNew_Id_Old_Id_Incident_ E1 WITH(NOLOCK) ON Entity_ID1 = E1.Old_Unique_ID
	LEFT JOIN IBaseM.vNew_Id_Old_Id_Incident_ E2  WITH(NOLOCK) ON Entity_ID2 = E2.Old_Unique_ID

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Incident_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Incident_Link_NextID'
	INSERT INTO IBaseM8Cur._Incident_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	-------------------------------------------------------------------------------------------------------------
	--------------------------------------Case_to_Incident_Link (New)---------------------------------
	-------------------------------------------------------------------------------------------------------------
	DECLARE @Tbl TABLE (
		Unique_ID_Link_Id varchar(23) NULL,
		Entity_ID1 nvarchar(50) NOT NULL,
		Confidence int NOT NULL,
		Direction int NOT NULL,
		Entity_ID2 nvarchar(50) NOT NULL,
		EntityType_ID1 int NOT NULL,
		EntityType_ID2 int NOT NULL,
		LinkType_ID int NOT NULL,
		Record_Status int NOT NULL,
		Record_Type int NOT NULL,
		SCC varchar(1) NOT NULL,
		Create_Date datetime NOT NULL,
		Create_User nvarchar(255) NOT NULL,
		Do_Not_Disseminate_412284494 smallint NOT NULL,
		IconColour int NULL,
		Key_Attractor_412284410 nvarchar(100) NULL,
		Last_Upd_Date datetime NULL,
		Last_Upd_User nvarchar(255) NULL,
		x5x5x5_Grading_412284402 nvarchar(255) NULL,
		Source_411765484  nvarchar(50),
		Source_Description_411765489  nvarchar(MAX) , 
		Source_Reference_411765487  nvarchar(50),
		Case_Incident_Link_ID nvarchar(50)
	) 

	INSERT INTO @Tbl
	SELECT 
		'CAS' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY K.Unique_ID)) AS Unique_ID_Link_Id,
		K.Unique_Id AS Entity_ID1,
		1 AS Confidence,
		0 AS Direction,
		I.Unique_Id AS Entity_ID2,
		1750 AS EntityType_ID1,
		1688 AS EntityType_ID2,
		2485 AS LinkType_ID,
		0 AS Record_Status,
		1 AS Record_Type,
		'' AS SCC,
		K.Create_Date,
		K.Create_User,
		K.Do_Not_Disseminate_412284494,
		K.IconColour,
		K.Key_Attractor_412284410,
		K.Last_Upd_Date,
		K.Last_Upd_User,
		K.x5x5x5_Grading_412284402,
		'Keoghs CFS' AS  Source, 
		K.Source_Description_411765489, 
		IBaseM.Fn_GetEliteRefFromDbSource(K.Keoghs_Elite_Reference)  AS Reference,
		K.Keoghs_Case_ID  --PA;Added2014/01/28
	FROM IBaseM8Cur.Keoghs_Case K WITH (NOLOCK)
	INNER JOIN IBaseM8Cur.Incident_ I  WITH (NOLOCK) ON K.Keoghs_Case_ID = I.Incident_ID

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Case_to_Incident_Link'
	INSERT INTO	IBaseM8Cur.Case_to_Incident_Link
		(Unique_ID, Create_User, Create_Date, Last_Upd_User, Last_Upd_Date, Record_Status, 
			Do_Not_Disseminate_412284494,IconColour, Key_Attractor_412284410, x5x5x5_Grading_412284402, 
			Source_411765484, Source_Description_411765489, Source_Reference_411765487,Case_Incident_Link_ID)
	SELECT 
		Unique_ID_Link_Id,
		Create_User,
		Create_Date,
		Last_Upd_User,
		Last_Upd_Date,
		Record_Status ,
		Do_Not_Disseminate_412284494,
		IconColour,
		Key_Attractor_412284410,
		x5x5x5_Grading_412284402,
		Source_411765484, 
		Source_Description_411765489, 
		Source_Reference_411765487,
		Case_Incident_Link_ID
	 FROM @Tbl

	DECLARE @NID INT

	--Reseting Next Id
	SELECT @NID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Case_to_Incident_Link 
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Case_to_Incident_Link_NextID'
	INSERT INTO IBaseM8Cur._Case_to_Incident_Link_NextID(NextID)
	SELECT ISNULL(@NID, 1)

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	SELECT Unique_ID_Link_Id, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC
	FROM @Tbl

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	SELECT Unique_ID_Link_Id, Entity_ID2, Confidence, Direction, Entity_ID1, EntityType_ID2, EntityType_ID1, LinkType_ID, Record_Status, 2, SCC
	FROM @Tbl

	UPDATE A
	SET A.Source_Reference_411765487 = Keoghs_Elite_Reference
	FROM  IBaseM8Cur.Incident_Link A
	INNER JOIN IBaseM8Cur._LinkEnd B ON A.Unique_ID = B.Link_ID
	INNER JOIN IBaseM8Cur.Incident_ C ON B.Entity_ID1 = C.Unique_ID

	if OBJECT_ID('tempdb..#IBaseM8Cur._LinkEnd') is not null
		DROP TABLE #IBaseM8Cur._LinkEnd

	if OBJECT_ID('tempdb..#_Elements') is not null
		DROP TABLE #_Elements
		
	if OBJECT_ID('tempdb..#_ElementLinks') is not null
		DROP TABLE #_ElementLinks

	--17 May 2013
	;WITH CTE(Link_ID, Entity_ID1,	Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	AS
	(
	SELECT 
		A.Unique_ID AS Link_ID,	
		INEW.Unique_ID AS Entity_ID1,	
		Confidence	,
		Direction	,
		U.Unique_ID AS Entity_ID2	,
		1688 AS EntityType_ID1	,
		CASE EntityType_ID2
			WHEN 1002 THEN 2321
			WHEN 1005 THEN 1893
			WHEN 1001 THEN 1969
		END AS EntityType_ID2,
		2702 AS LinkType_ID,
		0/*1*/ AS Record_Status,
		Record_Type,
		'' AS SCC
	FROM IBaseM8Cur.Incident_Link A 
	INNER JOIN  IBaseM5Cur._LinkEnd B
	ON A.Incident_Link_id = B.Link_ID
	INNER JOIN IBaseM8Cur.Incident_ INEW
	ON INEW.Incident_ID = Entity_ID1
	INNER JOIN
	(
	SELECT Unique_ID, Person_ID AS ID FROM IBaseM8Cur.Person_
	UNION ALL
	SELECT Unique_ID, Organisation_Id AS ID  FROM IBaseM8Cur.Organisation_
	--UNION ALL
	--SELECT  Unique_ID, Vehicle_Id  AS ID FROM IBaseM8Cur.Vehicle_
	) U
	ON U.ID = Entity_ID2
	WHERE A.Unique_ID NOT IN(SELECT Link_ID  FROM IBaseM8Cur._LinkEnd WHERE Link_ID LIKE 'IN1%' )
	) 

	INSERT INTO IBaseM8Cur._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	SELECT Link_ID, Entity_ID1,	Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC
	FROM CTE
	UNION ALL
	SELECT Link_ID, Entity_ID2,	Confidence, Direction, Entity_ID1, EntityType_ID2, EntityType_ID1, LinkType_ID, /*2*/ 0 AS Record_Status, Record_Type, SCC
	FROM CTE

	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					--,@InsertRowCount= @InsertRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity		INT				= ERROR_SEVERITY()
				,@ErrorState		INT				= ERROR_STATE()
				,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
				,@ErrorLine			INT				= ERROR_LINE()

		SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
END CATCH



