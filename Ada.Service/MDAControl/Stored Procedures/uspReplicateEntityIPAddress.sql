﻿CREATE PROCEDURE [Rep].[uspReplicateEntityIPAddress]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateEntityIPAddress]	
--
-- Description:		Replicate Entity IP Address	
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLogging/Tidy						
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-03-03			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.IP_Address')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 38 --ReplicateIPAddressEntity
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	UPDATE [$(MDA_IBase)].[dbo].IP_Address
	SET Record_Status = 254
	OUTPUT	 @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.[IP_Address],'') + '}|I:{' + ISNULL(Inserted.[IP_Address],'') + '}'
			,'D:{' + ISNULL(Deleted.[IP_Address_ID],'') + '}|I:{' + ISNULL(Inserted.[IP_Address_ID],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[IconColour],'')) + '}'
			,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
			,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
			,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SCC],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Status_Binding],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[x5x5x5_Grading_412284402],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Risk_Claim_ID_414244883],'')) + '}'
	INTO [SyncDML].[MDAIBase_IP_Address] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, IP_Address, IP_Address_ID, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.EntityRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo].IP_Address a ON ent.IBase8EntityID = a.Unique_ID
	WHERE RecordAction ='D'

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo].IP_Address [Target]
	USING	(
			SELECT   DISTINCT C.IBase8EntityID
					,C.IBase5EntityID
					,A.*
			FROM IBaseM8Cur.IP_Address A
			INNER JOIN Rep.EntityRecord C ON A.Unique_ID = C.IBase8EntityID_Staging	
			) AS Source ON [Target].Unique_ID = Source.IBase8EntityID
	WHEN MATCHED THEN UPDATE
	SET  IP_Address						= Source.IP_Address
		,Key_Attractor_412284410		= Source.Key_Attractor_412284410
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User
		,Record_Status					= Source.Record_Status
		,AltEntity						= Source.AltEntity
		,Do_Not_Disseminate_412284494	= Source.Do_Not_Disseminate_412284494
		,IconColour						= Source.IconColour
		,IP_Address_ID					= Source.IP_Address_ID
		,SCC							= Source.SCC
		,Status_Binding					= Source.Status_Binding
		,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883		= Source.Risk_Claim_ID_414244883
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,AltEntity
		,Create_Date
		,Create_User
		,Do_Not_Disseminate_412284494
		,IconColour
		,IP_Address
		,IP_Address_ID
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,Record_Status
		,SCC
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883	
		)
	VALUES
		(
		 Source.IBase8EntityID
		,Source.AltEntity
		,Source.Create_Date
		,Source.Create_User
		,Source.Do_Not_Disseminate_412284494
		,Source.IconColour
		,Source.IP_Address
		,Source.IP_Address_ID
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Record_Status
		,Source.SCC
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.x5x5x5_Grading_412284402
		,Source.Risk_Claim_ID_414244883	
		)
	OUTPUT	 @OutputTaskID
			,$Action
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.[IP_Address],'') + '}|I:{' + ISNULL(Inserted.[IP_Address],'') + '}'
			,'D:{' + ISNULL(Deleted.[IP_Address_ID],'') + '}|I:{' + ISNULL(Inserted.[IP_Address_ID],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[IconColour],'')) + '}'
			,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
			,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
			,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SCC],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Status_Binding],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[x5x5x5_Grading_412284402],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Risk_Claim_ID_414244883],'')) + '}'
	INTO [SyncDML].[MDAIBase_IP_Address] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, IP_Address, IP_Address_ID, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
		SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
		SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()
		
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
						@Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END	
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH


