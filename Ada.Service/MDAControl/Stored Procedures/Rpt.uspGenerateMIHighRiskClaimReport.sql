﻿
CREATE PROCEDURE [Rpt].[uspGenerateMIHighRiskClaimReport]
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

--------------------------------------
--DeclareAndSetRequiredObjects
--------------------------------------
DECLARE  @RowsToProcess INT
		,@Counter INT =1
		,@RunNameID INT = 138
		,@ControlRunID INT
		,@ReportCreationOutputDeliveryConfiguration_Id INT
		,@RiskBatch_ID INT = ''
		,@SQL NVARCHAR(MAX)
		,@BCP VARCHAR(8000)
		,@ResultFileName VARCHAR(200)
		,@ResultTableName NVARCHAR(100) = (SELECT '[dbo].[MIHighRiskClaims_' + CAST(NEWID() AS NVARCHAR(50)) + ']')
		,@RiskClientID TINYINT
		,@ScoreThreshold SMALLINT

BEGIN TRY

	DECLARE @ClientsToProcess TABLE (RowID INT IDENTITY(1,1), ReportCreationOutputDeliveryConfiguration_Id INT NOT NULL, RiskBatchID INT)

	IF OBJECT_ID('tempdb..#MIHighRiskClaims') IS NOT NULL
		DROP TABLE #MIHighRiskClaims
	CREATE TABLE #MIHighRiskClaims (RowID INT IDENTITY(1,1), DateOfADAReceipt VARCHAR(20), ClaimNumber VARCHAR(50), ClaimStatus VARCHAR(50), RiskRating VARCHAR(10), RiskScore VARCHAR(10), IsNewOrUpdatedClaim VARCHAR(20), Party VARCHAR(100), SubParty VARCHAR(100), PolicyNumber VARCHAR(100), NotificationDate VARCHAR(20), IncidentDate VARCHAR(20), Forename VARCHAR(100), Surname VARCHAR(100) ,DateOfBirth VARCHAR(20), SubBuilding VARCHAR(100), Building VARCHAR(100), BuildingNumber VARCHAR(100) ,Street VARCHAR(100), Locality VARCHAR(100) ,Town VARCHAR(100), PostCode VARCHAR(20),Telephone VARCHAR(20), VehicleRegistration VARCHAR(20), ClaimPayments VARCHAR(20), ClaimReserve VARCHAR(20))

	--DoWeHaveABatchThatWeNeedToSend?
	INSERT INTO @ClientsToProcess (ReportCreationOutputDeliveryConfiguration_Id, RiskBatchID)
	SELECT DISTINCT RptConfig.ID ReportCreationOutputDeliveryConfiguration_ID, RiskBatchID
	FROM [Rpt].[ReportCreationOutputDeliveryConfiguration] RptConfig
	INNER JOIN  (
				SELECT [RiskClient_Id], MAX([Id]) RiskBatchID
				FROM [MDA].[dbo].[RiskBatch]
				WHERE [BatchStatus] = 5 --Scored
				GROUP BY [RiskClient_Id]
				) LastBatch ON LastBatch.RiskClient_Id = RptConfig.RiskClient_Id
	LEFT JOIN [Rpt].[ReportNameRun] RptHistory ON RptHistory.[ReportCreationOutputDeliveryConfiguration_Id] = RptConfig.ID
	WHERE RptConfig.ReportName_Id = 2 --MI High Risk Claim Report
	AND ((LastBatch.RiskBatchID > RptHistory.[BatchID] AND NOT EXISTS (SELECT TOP 1 1 FROM [Rpt].[ReportNameRun] RptHistory1 WHERE RptHistory1.ReportCreationOutputDeliveryConfiguration_Id = RptConfig.ID AND RptHistory1.BatchID = LastBatch.RiskBatchID)) OR ([Resend] = 1 OR RptHistory.[Id] IS NULL))
	SELECT @RowsToProcess = @@ROWCOUNT

	--------------------------------------
	--LoopAroundGeneratingTheFileAndEmail
	--------------------------------------
	WHILE @RowsToProcess >= @Counter
	BEGIN
		--CreateTheControlRunRecord
		INSERT INTO [MDAControl].[dbo].[DBControlRun] ([RunNameID]) VALUES (@RunNameID)
		SELECT @ControlRunID = SCOPE_IDENTITY()

		--CreateTheImportTable
		SELECT @SQL = 'CREATE TABLE ' + @ResultTableName + ' (RowID INT IDENTITY(1,1), DateOfADAReceipt VARCHAR(20), ClaimNumber VARCHAR(50), ClaimStatus VARCHAR(50), RiskRating VARCHAR(10), RiskScore VARCHAR(10), IsNewOrUpdatedClaim VARCHAR(20), Party VARCHAR(100), SubParty VARCHAR(100), PolicyNumber VARCHAR(100), NotificationDate VARCHAR(20), IncidentDate VARCHAR(20), Forename VARCHAR(100), Surname VARCHAR(100) ,DateOfBirth VARCHAR(20), SubBuilding VARCHAR(100), Building VARCHAR(100), BuildingNumber VARCHAR(100) ,Street VARCHAR(100), Locality VARCHAR(100) ,Town VARCHAR(100), PostCode VARCHAR(20),Telephone VARCHAR(20), VehicleRegistration VARCHAR(20), ClaimPayments VARCHAR(20), ClaimReserve VARCHAR(20))'
		EXECUTE SP_EXECUTESQL  @SQL

		--PopulateTheParametersWeNeed
		SELECT	 @RiskClientID = RPT.RiskClient_Id
				,@ScoreThreshold = RPT.RiskRunExceedScore
				,@ReportCreationOutputDeliveryConfiguration_Id = ReportCreationOutputDeliveryConfiguration_Id
				,@ResultFileName = DestinationFileLocation + REPLACE(REPLACE(REPLACE(REPLACE([DestinationFileName],'@ClientName',RC.ClientName),'@CurrentDateTime',REPLACE(REPLACE(CONVERT(VARCHAR,GETDATE(),120),'-',''),':','')),'@RiskBatch_ID',@RiskBatch_ID),' ','') + '.' + [DestinationFileFormat]
				,@RiskBatch_ID = CTP.RiskBatchID
		FROM @ClientsToProcess CTP
		INNER JOIN [Rpt].[ReportCreationOutputDeliveryConfiguration] RPT ON RPT.[Id] = CTP.ReportCreationOutputDeliveryConfiguration_Id 
		INNER JOIN [MDA].[dbo].[RiskClient] RC ON RC.Id = RPT.RiskClient_Id
		WHERE CTP.RowID = @Counter

		--AddTheParemeterDetailsToTheControlRunTable
		UPDATE [dbo].[DBControlRun]
		SET [Details] = 'FileLocation:[' + ISNULL(@ResultFileName,'') + ']'
		WHERE [RunID] = @ControlRunID

		IF NULLIF(@ResultFileName,'') IS NOT NULL
		BEGIN

			--AddHeaderRecord
			INSERT INTO #MIHighRiskClaims (DateOfADAReceipt, ClaimNumber, ClaimStatus, RiskRating, RiskScore, IsNewOrUpdatedClaim, Party, SubParty, PolicyNumber, NotificationDate, IncidentDate, Forename, Surname, DateOfBirth, SubBuilding, Building, BuildingNumber, Street, Locality, Town, PostCode, Telephone, VehicleRegistration, ClaimPayments, ClaimReserve)
			VALUES ('DateOfADAReceipt', 'ClaimNumber', 'ClaimStatus', 'RiskRating', 'RiskScore', 'IsNewOrUpdatedClaim', 'Party', 'SubParty', 'PolicyNumber', 'NotificationDate', 'IncidentDate', 'Forename', 'Surname', 'DateOfBirth', 'SubBuilding', 'Building', 'BuildingNumber', 'Street', 'Locality', 'Town', 'PostCode', 'Telephone', 'VehicleRegistration', 'ClaimPayments', 'ClaimReserve');

			--GetTheDataThatWeNeed
			INSERT INTO #MIHighRiskClaims (DateOfADAReceipt, ClaimNumber, ClaimStatus, RiskRating, RiskScore, IsNewOrUpdatedClaim, Party, SubParty, PolicyNumber, NotificationDate, IncidentDate, Forename, Surname, DateOfBirth, SubBuilding, Building, BuildingNumber, Street, Locality, Town, PostCode, Telephone, VehicleRegistration, ClaimPayments, ClaimReserve)
			SELECT	 CONVERT(VARCHAR(20), NULLIF(DateOfADAReceipt,''),103) DateOfADAReceipt
					,NULLIF(ClaimNumber,'') ClaimNumber
					,NULLIF(ClaimStatus,'') ClaimStatus
					,NULLIF(Risk,'') RiskRating
					,NULLIF(TotalScore,'0') RiskScore
					,NULLIF([New/Updated],'') [IsNewOrUpdatedClaim]
					,NULLIF(Party,'') Party
					,NULLIF(SubParty,'Unknown') SubParty
					,NULLIF(PolicyNumber,'') PolicyNumber
					,CONVERT(VARCHAR(20), NULLIF(NotificationDate,''),103) NotificationDate
					,CONVERT(VARCHAR(20), NULLIF(IncidentDate,''),103) IncidentDate
					,NULLIF(Forename,'') Forename
					,NULLIF(Surname,'') Surname
					,CONVERT(VARCHAR(20), NULLIF(DateOfBirth,''),103) DateOfBirth
					,NULLIF(SubBuilding,'') SubBuilding
					,NULLIF(Building,'') Building
					,NULLIF(BuildingNumber,'') BuildingNumber
					,NULLIF(Street,'') Street
					,NULLIF(Locality,'') Locality
					,NULLIF(Town,'') Town
					,NULLIF(PostCode,'') PostCode
					,NULLIF(Telephone,'') Telephone
					,NULLIF(VehicleRegistration,'') VehicleRegistration
					,NULLIF(ClaimPayments,'') ClaimPayments
					,NULLIF(ClaimReserve,'') ClaimReserve	
			FROM	(
					SELECT	 DENSE_RANK() OVER (PARTITION BY INC.Id, PER.[Id] ORDER BY VEH.[Id] DESC) VehicleRowID
							,ROW_NUMBER() OVER (PARTITION BY INC.Id, PER.[Id] ORDER BY P2A.SourceReference DESC, ISNULL(P2A.ModifiedDate,P2A.CreatedDate) DESC) AddressRowID
							,PT.[PartyTypeText] Party
							,SPT.[SubPartyText] SubParty
							,RC.[ClientClaimRefNumber] ClaimNumber
							,CS.[Text] ClaimStatus
							,POL.[PolicyNumber]
							,I2P.ClaimNotificationDate NotificationDate
							,INC.IncidentDate
							,PER.[FirstName] Forename
							,PER.[LastName] Surname
							,PER.DateOfBirth
							,AD.[SubBuilding]
							,AD.[Building]
							,AD.[BuildingNumber]
							,AD.[Street]
							,AD.[Locality]
							,AD.[Town]
							,AD.PostCode
							,TEL_H.[TelephoneNumber] Telephone
							,VEH.[VehicleRegistration]
							,I2P.[TotalClaimCost] ClaimPayments
							,I2P.[Reserve] ClaimReserve
							,[SourceEntityXML]
							,RB.ClientBatchReference
							,RC.Incident_Id MDAIncidentID
							,RCR.TotalScore
							,CASE WHEN RCR.TotalScore >= RCR.RedThreshold THEN 'Red' WHEN RCR.TotalScore >= RCR.AmberThreshold AND RCR.TotalScore < RCR.RedThreshold THEN 'Amber' ELSE 'Green' END Risk
							,CASE WHEN RCR.PrevTotalScore IS NULL THEN 'New' ELSE 'Updated' END [New/Updated]
					FROM [MDA].[dbo].[RiskBatch] RB
					INNER JOIN [MDA].[dbo].[RiskClaim] RC ON RC.[RiskBatch_Id] = RB.[Id]
					INNER JOIN [MDA].[dbo].[RiskClaimRun] RCR ON RCR.[RiskClaim_Id] = RC.[Id]
					INNER JOIN [MDA].[dbo].[Incident] INC ON INC.[Id] = RC.Incident_Id
					INNER JOIN [MDA].[dbo].[Incident2Person] I2P ON I2P.Incident_Id = INC.Id
					INNER JOIN [MDA].[dbo].[PartyType] PT ON PT.Id = I2P.PartyType_Id
					INNER JOIN [MDA].[dbo].[SubPartyType] SPT ON SPT.Id = I2P.SubPartyType_Id
					INNER JOIN [MDA].[dbo].[ClaimStatus] CS ON CS.[Id] = I2P.ClaimStatus_ID
					LEFT JOIN [MDA].[dbo].[Incident2Policy] I2Pol ON I2Pol.[Incident_Id] = INC.Id
					LEFT JOIN [MDA].[dbo].[Policy] POL ON POL.[Id] = I2Pol.[Policy_Id]
					INNER JOIN [MDA].[dbo].[Person] PER ON PER.[Id] = I2P.[Person_Id]
					INNER JOIN [MDA].[dbo].[Person2Address] P2A ON P2A.[Person_Id] = PER.Id AND P2A.[RiskClaim_Id] = I2P.[RiskClaim_Id]
					INNER JOIN [MDA].[dbo].[Address] AD ON AD.[Id] = P2A.[Address_Id]
					LEFT JOIN [MDA].[dbo].[Person2Telephone] P2T ON P2T.[Person_Id] = PER.Id AND P2T.[RiskClaim_Id] = I2P.[RiskClaim_Id]
					LEFT JOIN [MDA].[dbo].[Telephone] TEL_H ON TEL_H.Id = P2T.[Telephone_Id]
					LEFT JOIN [MDA].[dbo].[Incident2Vehicle] I2V ON I2V.[Incident_Id] = INC.Id AND P2T.[RiskClaim_Id] = I2P.[RiskClaim_Id]
					LEFT JOIN [MDA].[dbo].[Vehicle2Person] V2P ON V2P.[Vehicle_Id] = I2V.[Vehicle_Id] AND V2P.[Person_Id] = PER.Id AND V2P.[RiskClaim_Id] = I2P.[RiskClaim_Id]
					LEFT JOIN [MDA].[dbo].[Vehicle] VEH ON VEH.Id = V2P.[Vehicle_Id]
					WHERE RiskClient_ID = @RiskClientID
					AND RC.[LatestVersion] = 1
					AND RCR.[TotalScore] > @ScoreThreshold
					) DATA
			INNER JOIN (
						SELECT RC.[Incident_Id], MIN(RB.[CreatedDate]) DateOfADAReceipt
						FROM [MDA].[dbo].[RiskBatch] RB
						INNER JOIN [MDA].[dbo].[RiskClaim] RC ON RC.[RiskBatch_Id] = RB.ID
						WHERE RiskClient_ID = @RiskClientID
						GROUP BY RC.[Incident_Id]
						) B ON B.[Incident_Id] = DATA.MDAIncidentID
			WHERE VehicleRowID = 1
			AND AddressRowID = 1
			ORDER BY DateOfADAReceipt, NotificationDate, IncidentDate, TotalScore DESC

			--GettheResultsIntoATableWhichWeWillUseTooutputFrom
			SELECT @SQL = 'INSERT INTO ' + @ResultTableName + ' SELECT DateOfADAReceipt, ClaimNumber, ClaimStatus, RiskRating, RiskScore, IsNewOrUpdatedClaim, Party, SubParty, PolicyNumber, NotificationDate, IncidentDate, Forename, Surname, DateOfBirth, SubBuilding, Building, BuildingNumber, Street, Locality, Town, PostCode, Telephone, VehicleRegistration, ClaimPayments, ClaimReserve FROM #MIHighRiskClaims ORDER BY RowID'
			EXECUTE SP_EXECUTESQL  @SQL

			--BCPFileOutToTheFileDirectory
			SELECT @BCP = 'BCP "SELECT DateOfADAReceipt, ClaimNumber, ClaimStatus, RiskRating, RiskScore, IsNewOrUpdatedClaim, Party, SubParty, PolicyNumber, NotificationDate, IncidentDate, Forename, Surname, DateOfBirth, SubBuilding, Building, BuildingNumber, Street, Locality, Town, PostCode, Telephone, VehicleRegistration, ClaimPayments, ClaimReserve FROM [MDAControl].' + @ResultTableName + ' ORDER BY RowID" queryout "' + @ResultFileName +'" -T -c -t"\",\"" -r"\"\n\""'
			EXEC master..XP_CMDSHELL @BCP--, no_output
		END

		--InsertRecordIntoRunHistory
		INSERT INTO [Rpt].[ReportNameRun] (ReportCreationOutputDeliveryConfiguration_Id, DBControlRun_Id, BatchID)
		VALUES (@ReportCreationOutputDeliveryConfiguration_Id, @ControlRunID, @RiskBatch_ID)

		--UpdateResendFlagForBatch
		UPDATE RptHistory
		SET [Resend] = 0
		FROM [Rpt].[ReportNameRun] RptHistory
		INNER JOIN [Rpt].[ReportCreationOutputDeliveryConfiguration] RptConfig ON RptConfig.Id = RptHistory.ReportCreationOutputDeliveryConfiguration_Id
		WHERE [ReportName_Id] = 2
		AND [RiskClient_Id] = @RiskClientID
		AND [Resend] = 1

		--CloseControlRunRecord
		UPDATE [dbo].[DBControlRun]
		SET [RunEndTime] = GETDATE()
		WHERE [RunID] = @ControlRunID

		--PrepareForTheNextLoop
		SELECT	 @SQL = 'IF OBJECT_ID('''+ @ResultTableName +''') IS NOT NULL DROP TABLE ' + @ResultTableName
				,@Counter += 1

		EXECUTE SP_EXECUTESQL  @SQL
		DELETE #MIHighRiskClaims
	END

	--------------------------------------
	-- Tidy
	--------------------------------------
	IF OBJECT_ID('tempdb..#MIHighRiskClaims') IS NOT NULL
		DROP TABLE #MIHighRiskClaims

END TRY
BEGIN CATCH
	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+ISNULL(@ErrorProcedure,'')+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'

	UPDATE [dbo].[DBControlRun]
	SET  [RunEndTime] = GETDATE()
		,[Error] = @ErrorSeverity
		,[ErrorDescription] = @ErrorMessage
	WHERE [RunID] = @ControlRunID

	SELECT	 @SQL = 'IF OBJECT_ID('''+ @ResultTableName +''') IS NOT NULL DROP TABLE ' + @ResultTableName
	EXECUTE SP_EXECUTESQL  @SQL

	IF OBJECT_ID('tempdb..#MIHighRiskClaims') IS NOT NULL
		DROP TABLE #MIHighRiskClaims

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH