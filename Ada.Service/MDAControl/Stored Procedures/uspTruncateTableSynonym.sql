﻿

CREATE PROCEDURE [IBaseM].[uspTruncateTableSynonym] 
/**************************************************************************************************/
-- ObjectName:			MDAControl.uspTruncateTableSynonym
--
-- Description:			Truncate table using Synonym
--
-- Usage:				Called within:
--							IBaseM.uspMigrationStep02
--							IBaseM.uspMigrationStep03
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
/**************************************************************************************************/
(
 @Synonym SYSNAME
)
AS
SET NOCOUNT ON
BEGIN TRY

	DECLARE     @stmt NVARCHAR(MAX)
	SELECT @stmt = 'TRUNCATE TABLE ' + base_object_name
	FROM sys.synonyms SYN
	INNER JOIN sys.schemas sch ON sch.schema_id=syn.schema_id
	WHERE sch.name + '.' + syn.name = @Synonym

	EXEC sp_executesql @stmt
END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

