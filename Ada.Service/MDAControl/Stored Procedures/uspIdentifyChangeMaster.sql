﻿CREATE PROCEDURE [Rep].[uspIdentifyChangeMaster]
/**************************************************************************************************/
-- ObjectName:			MDAControl.Rep.uspIdentifyChangeMaster
--
-- Description:			Wrapper SP that identifys all changes required for replication	
--
-- Steps:				1)ConfigureLogging,Create/DeclareRequiredObjects
--							1.1)ConfigureLogging
--							1.2)Create/DeclareRequiredObjects
--						2)IdentifyTheChanges
--						3)Generate/LookupUniqueKeyValues
--						4)Generate/LookupRiskClaimRunID
--							4.1)DeleteAnyRecordThatWillCauseAConflict
--						5)WriteAllReplicationChangesToHistory	
--						6)UpdateLogging/Tidy
--
-- Dependencies:		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-27			Paul Allen		Created
--		2014-03-24			Paul Allen		Added Section 4.1
/**************************************************************************************************/
(
 @Logging				BIT = 0	--DefaultIsOff
,@Debug					BIT = 0 --DefaultIsOff
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@OutputRunID		INT
				,@OutputProcessID	INT
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
		
		EXECUTE dbo.uspCRUDDBControlRun
			 @RunNameID		= 1 --Replication_IBaseLiveToMDAIBase
			,@OutputRunID	= @OutputRunID OUTPUT
			
		SELECT @Details = 'User=['+SYSTEM_USER+']'				
		EXECUTE dbo.uspCRUDDBControlProcess
					 @RunID				= @OutputRunID
					,@ProcessNameID		= 15 --IdentifyChangesRequiedForReplication
					,@Details			= @Details
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@OutputProcessID	= @OutputProcessID OUTPUT
						
		EXECUTE dbo.uspCRUDDBControlTask
					 @ProcessID			= @OutputProcessID
					,@TaskNameID		= 16 --IdentifyChangesRequiedForReplicationWrapper
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	
	-----------------------------------------------------------
	--2)IdentifyTheChanges
	-----------------------------------------------------------	
	EXECUTE Rep.uspIdentifyChange @Logging = 1, @ParentTaskID = @OutputTaskID
	
	-----------------------------------------------------------
	--3)Generate/LookupUniqueKeyValues
	-----------------------------------------------------------	
	EXECUTE Rep.uspGenerateLookupUniqueID @Logging = 1, @ParentTaskID = @OutputTaskID
		
	-----------------------------------------------------------
	--4)Generate/LookupRiskClaimRunID
	-----------------------------------------------------------		
	EXECUTE Rep.uspGenerateLookupRiskClaimRunID @Logging = 1, @ParentTaskID = @OutputTaskID

	-----------------------------------------------------------
	--4.1)DeleteAnyRecordThatWillCauseAConflict
	-----------------------------------------------------------		
	EXECUTE Rep.uspDeleteConflictRecords @Logging = 1, @ParentTaskID = @OutputTaskID
	
	-----------------------------------------------------------
	--5)WriteAllReplicationChangesToHistory				
	-----------------------------------------------------------		
	EXECUTE Rep.uspAddChangesToHistory @Logging = 1, @ParentTaskID = @OutputTaskID
		
	------------------------------------------------------------
	--6)UpdateLogging/Tidy
	-----------------------------------------------------------
	IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action		= 'U'
						,@TaskID		= @OutputTaskID
						,@ProcessID		= @OutputProcessID
						,@TaskEndTime	= @Now
						,@OutputTaskID	= @OutputTaskID OUTPUT
						
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@OutputProcessID	= @OutputProcessID OUTPUT
		END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@ProcessID			= @OutputProcessID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
			
			SELECT @ErrorMessage +=' [TaskID='+CAST(ISNULL(@OutputTaskID,-1) AS VARCHAR)+']'		
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputProcessID	= @OutputProcessID OUTPUT
						
			EXECUTE dbo.uspCRUDDBControlRun
						 @Action			= 'U'
						,@RunID				= @OutputRunID
						,@RunEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputRunID		= @OutputRunID OUTPUT
		END
END CATCH	



