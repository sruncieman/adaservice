﻿





CREATE PROCEDURE [Rep].[uspReplicateLinkIncident]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateLinkIncident]	
--
-- Description:		Replicate Link Incident
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLoggingTidyTidy			
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-03-03			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Incident_Link')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 56 --ReplicateIncidentLink
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	UPDATE [$(MDA_IBase)].[dbo].Incident_Link
	SET Record_Status =254
	OUTPUT	@OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Accident_Management,'') + '}|I:{' + ISNULL(Inserted.Accident_Management,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Ambulance_Attended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Ambulance_Attended,'')) + '}'
			,'D:{' + ISNULL(Deleted.Broker_,'') + '}|I:{' + ISNULL(Inserted.Broker_,'') + '}'
			,'D:{' + ISNULL(Deleted.Claim_Code,'') + '}|I:{' + ISNULL(Inserted.Claim_Code,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Claim_Notification_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Claim_Notification_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Claim_Number,'') + '}|I:{' + ISNULL(Inserted.Claim_Number,'') + '}'
			,'D:{' + ISNULL(Deleted.Claim_Status,'') + '}|I:{' + ISNULL(Inserted.Claim_Status,'') + '}'
			,'D:{' + ISNULL(Deleted.Claim_Type,'') + '}|I:{' + ISNULL(Inserted.Claim_Type,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Engineer_,'') + '}|I:{' + ISNULL(Inserted.Engineer_,'') + '}'
			,'D:{' + ISNULL(Deleted.Hire_,'') + '}|I:{' + ISNULL(Inserted.Hire_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Incident_Circs,'') + '}|I:{' + ISNULL(Inserted.Incident_Circs,'') + '}'
			,'D:{' + ISNULL(Deleted.Incident_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Incident_Link_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Incident_Location,'') + '}|I:{' + ISNULL(Inserted.Incident_Location,'') + '}'
			,'D:{' + ISNULL(Deleted.Incident_Time,'') + '}|I:{' + ISNULL(Inserted.Incident_Time,'') + '}'
			,'D:{' + ISNULL(Deleted.Insurer_,'') + '}|I:{' + ISNULL(Inserted.Insurer_,'') + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Medical_Examiner,'') + '}|I:{' + ISNULL(Inserted.Medical_Examiner,'') + '}'
			,'D:{' + ISNULL(Deleted.MoJ_Status,'') + '}|I:{' + ISNULL(Inserted.MoJ_Status,'') + '}'
			,'D:{' + ISNULL(Deleted.Party_Type,'') + '}|I:{' + ISNULL(Inserted.Party_Type,'') + '}'
			,'D:{' + ISNULL(Deleted.Payments_,'') + '}|I:{' + ISNULL(Inserted.Payments_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Police_Attended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Police_Attended,'')) + '}'
			,'D:{' + ISNULL(Deleted.Police_Force,'') + '}|I:{' + ISNULL(Inserted.Police_Force,'') + '}'
			,'D:{' + ISNULL(Deleted.Police_Reference,'') + '}|I:{' + ISNULL(Inserted.Police_Reference,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.Recovery_,'') + '}|I:{' + ISNULL(Inserted.Recovery_,'') + '}'
			,'D:{' + ISNULL(Deleted.Referral_Source,'') + '}|I:{' + ISNULL(Inserted.Referral_Source,'') + '}'
			,'D:{' + ISNULL(Deleted.Repairer_,'') + '}|I:{' + ISNULL(Inserted.Repairer_,'') + '}'
			,'D:{' + ISNULL(Deleted.Reserve_,'') + '}|I:{' + ISNULL(Inserted.Reserve_,'') + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Solicitors_,'') + '}|I:{' + ISNULL(Inserted.Solicitors_,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Storage_,'') + '}|I:{' + ISNULL(Inserted.Storage_,'') + '}'
			,'D:{' + ISNULL(Deleted.Storage_Address,'') + '}|I:{' + ISNULL(Inserted.Storage_Address,'') + '}'
			,'D:{' + ISNULL(Deleted.Sub_Party_Type,'') + '}|I:{' + ISNULL(Inserted.Sub_Party_Type,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Undefined_Supplier,'') + '}|I:{' + ISNULL(Inserted.Undefined_Supplier,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Attended_Hospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Attended_Hospital,'')) + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Incident_Link] (TaskID, DMLAction, Unique_ID, Accident_Management, AltEntity, Ambulance_Attended, Broker_, Claim_Code, Claim_Notification_Date, Claim_Number, Claim_Status, Claim_Type, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engineer_, Hire_, IconColour, Incident_Circs, Incident_Link_ID, Incident_Location, Incident_Time, Insurer_, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Medical_Examiner, MoJ_Status, Party_Type, Payments_, Police_Attended, Police_Force, Police_Reference, Record_Status, Recovery_, Referral_Source, Repairer_, Reserve_, SCC, Solicitors_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Storage_, Storage_Address, Sub_Party_Type, x5x5x5_Grading_412284402, Undefined_Supplier, Attended_Hospital, Risk_Claim_ID_414244883)
	FROM Rep.LinkRecord LR
	INNER JOIN [$(MDA_IBase)].[dbo].Incident_Link a ON LR.IBase8linkID = A.Unique_ID
	WHERE RecordAction ='D'

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo].Incident_Link [Target]
	USING	(
			SELECT   DISTINCT C.IBase8LinkID
					,C.IBase5LinkID
					,C.RiskClaim_Id
					,A.*
		FROM IBaseM8Cur.Incident_Link A
		INNER JOIN Rep.LinkRecord c ON a.Unique_ID = c.IBase8LinkID_Staging
		) AS Source ON [Target].Unique_ID = Source.IBase8LinkID
	WHEN MATCHED THEN UPDATE
	SET  Party_Type						= Source.Party_Type
		,Sub_Party_Type					= Source.Sub_Party_Type
		,Incident_Circs					= Source.Incident_Circs
		,Incident_Time					= Source.Incident_Time
		,Incident_Location				= Source.Incident_Location
		,Insurer_						= Source.Insurer_
		,Claim_Number					= Source.Claim_Number
		,Claim_Status					= Source.Claim_Status
		,Claim_Type						= Source.Claim_Type
		,Claim_Code						= Source.Claim_Code
		,MoJ_Status						= Source.MoJ_Status
		,Claim_Notification_Date		= Source.Claim_Notification_Date
		,Broker_						= Source.Broker_
		,Referral_Source				= Source.Referral_Source
		,Solicitors_					= Source.Solicitors_
		,Engineer_						= Source.Engineer_
		,Recovery_						= Source.Recovery_
		,Storage_						= Source.Storage_
		,Storage_Address				= Source.Storage_Address
		,Repairer_						= Source.Repairer_
		,Hire_							= Source.Hire_
		,Accident_Management			= Source.Accident_Management
		,Medical_Examiner				= Source.Medical_Examiner
		,Police_Attended				= Source.Police_Attended
		,Police_Force					= Source.Police_Force	
		,Police_Reference				= Source.Police_Reference
		,Ambulance_Attended				= Source.Ambulance_Attended
		,Reserve_						= Source.Reserve_
		,Payments_						= Source.Payments_
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User	
		,Risk_Claim_ID_414244883		= Source.RiskClaim_ID
		,Record_Status					= Source.Record_Status
		,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
		,[AltEntity]					= Source.[AltEntity]
		,[Do_Not_Disseminate_412284494]	= Source.[Do_Not_Disseminate_412284494]
		,[IconColour]					= Source.[IconColour]
		,[Incident_Link_ID]				= Source.[Incident_Link_ID]		
		,[Key_Attractor_412284410]		= Source.[Key_Attractor_412284410]
		,[SCC]							= Source.[SCC]
		,[Status_Binding]				= Source.[Status_Binding]
		,[Undefined_Supplier]			= Source.[Undefined_Supplier]
		,[Attended_Hospital]			= Source.[Attended_Hospital]
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,Accident_Management
		,AltEntity
		,Ambulance_Attended
		,Broker_
		,Claim_Code
		,Claim_Notification_Date
		,Claim_Number
		,Claim_Status
		,Claim_Type
		,Create_Date
		,Create_User
		,Do_Not_Disseminate_412284494
		,Engineer_
		,Hire_
		,IconColour
		,Incident_Circs
		,Incident_Link_ID
		,Incident_Location
		,Incident_Time
		,Insurer_
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,Medical_Examiner
		,MoJ_Status
		,Party_Type
		,Payments_
		,Police_Attended
		,Police_Force
		,Police_Reference
		,Record_Status
		,Recovery_
		,Referral_Source
		,Repairer_
		,Reserve_
		,SCC
		,Solicitors_
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding
		,Storage_
		,Storage_Address
		,Sub_Party_Type
		,x5x5x5_Grading_412284402
		,Undefined_Supplier
		,Attended_Hospital
		,Risk_Claim_ID_414244883		
		)
	VALUES
		(
		 Source.IBase8LinkID
		,Source.Accident_Management
		,Source.AltEntity
		,Source.Ambulance_Attended
		,Source.Broker_
		,Source.Claim_Code
		,Source.Claim_Notification_Date
		,Source.Claim_Number
		,Source.Claim_Status
		,Source.Claim_Type
		,Source.Create_Date
		,Source.Create_User
		,Source.Do_Not_Disseminate_412284494
		,Source.Engineer_
		,Source.Hire_
		,Source.IconColour
		,Source.Incident_Circs
		,Source.Incident_Link_ID
		,Source.Incident_Location
		,Source.Incident_Time
		,Source.Insurer_
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Medical_Examiner
		,Source.MoJ_Status
		,Source.Party_Type
		,Source.Payments_
		,Source.Police_Attended
		,Source.Police_Force
		,Source.Police_Reference
		,Source.Record_Status
		,Source.Recovery_
		,Source.Referral_Source
		,Source.Repairer_
		,Source.Reserve_
		,Source.SCC
		,Source.Solicitors_
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.Storage_
		,Source.Storage_Address
		,Source.Sub_Party_Type
		,Source.x5x5x5_Grading_412284402
		,Source.Undefined_Supplier
		,Source.Attended_Hospital
		,Source.RiskClaim_ID
		)	
	OUTPUT	@OutputTaskID
			,$Action
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Accident_Management,'') + '}|I:{' + ISNULL(Inserted.Accident_Management,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Ambulance_Attended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Ambulance_Attended,'')) + '}'
			,'D:{' + ISNULL(Deleted.Broker_,'') + '}|I:{' + ISNULL(Inserted.Broker_,'') + '}'
			,'D:{' + ISNULL(Deleted.Claim_Code,'') + '}|I:{' + ISNULL(Inserted.Claim_Code,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Claim_Notification_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Claim_Notification_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Claim_Number,'') + '}|I:{' + ISNULL(Inserted.Claim_Number,'') + '}'
			,'D:{' + ISNULL(Deleted.Claim_Status,'') + '}|I:{' + ISNULL(Inserted.Claim_Status,'') + '}'
			,'D:{' + ISNULL(Deleted.Claim_Type,'') + '}|I:{' + ISNULL(Inserted.Claim_Type,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Engineer_,'') + '}|I:{' + ISNULL(Inserted.Engineer_,'') + '}'
			,'D:{' + ISNULL(Deleted.Hire_,'') + '}|I:{' + ISNULL(Inserted.Hire_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Incident_Circs,'') + '}|I:{' + ISNULL(Inserted.Incident_Circs,'') + '}'
			,'D:{' + ISNULL(Deleted.Incident_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Incident_Link_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Incident_Location,'') + '}|I:{' + ISNULL(Inserted.Incident_Location,'') + '}'
			,'D:{' + ISNULL(Deleted.Incident_Time,'') + '}|I:{' + ISNULL(Inserted.Incident_Time,'') + '}'
			,'D:{' + ISNULL(Deleted.Insurer_,'') + '}|I:{' + ISNULL(Inserted.Insurer_,'') + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Medical_Examiner,'') + '}|I:{' + ISNULL(Inserted.Medical_Examiner,'') + '}'
			,'D:{' + ISNULL(Deleted.MoJ_Status,'') + '}|I:{' + ISNULL(Inserted.MoJ_Status,'') + '}'
			,'D:{' + ISNULL(Deleted.Party_Type,'') + '}|I:{' + ISNULL(Inserted.Party_Type,'') + '}'
			,'D:{' + ISNULL(Deleted.Payments_,'') + '}|I:{' + ISNULL(Inserted.Payments_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Police_Attended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Police_Attended,'')) + '}'
			,'D:{' + ISNULL(Deleted.Police_Force,'') + '}|I:{' + ISNULL(Inserted.Police_Force,'') + '}'
			,'D:{' + ISNULL(Deleted.Police_Reference,'') + '}|I:{' + ISNULL(Inserted.Police_Reference,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.Recovery_,'') + '}|I:{' + ISNULL(Inserted.Recovery_,'') + '}'
			,'D:{' + ISNULL(Deleted.Referral_Source,'') + '}|I:{' + ISNULL(Inserted.Referral_Source,'') + '}'
			,'D:{' + ISNULL(Deleted.Repairer_,'') + '}|I:{' + ISNULL(Inserted.Repairer_,'') + '}'
			,'D:{' + ISNULL(Deleted.Reserve_,'') + '}|I:{' + ISNULL(Inserted.Reserve_,'') + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Solicitors_,'') + '}|I:{' + ISNULL(Inserted.Solicitors_,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Storage_,'') + '}|I:{' + ISNULL(Inserted.Storage_,'') + '}'
			,'D:{' + ISNULL(Deleted.Storage_Address,'') + '}|I:{' + ISNULL(Inserted.Storage_Address,'') + '}'
			,'D:{' + ISNULL(Deleted.Sub_Party_Type,'') + '}|I:{' + ISNULL(Inserted.Sub_Party_Type,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Undefined_Supplier,'') + '}|I:{' + ISNULL(Inserted.Undefined_Supplier,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Attended_Hospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Attended_Hospital,'')) + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Incident_Link] (TaskID, DMLAction, Unique_ID, Accident_Management, AltEntity, Ambulance_Attended, Broker_, Claim_Code, Claim_Notification_Date, Claim_Number, Claim_Status, Claim_Type, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engineer_, Hire_, IconColour, Incident_Circs, Incident_Link_ID, Incident_Location, Incident_Time, Insurer_, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Medical_Examiner, MoJ_Status, Party_Type, Payments_, Police_Attended, Police_Force, Police_Reference, Record_Status, Recovery_, Referral_Source, Repairer_, Reserve_, SCC, Solicitors_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Storage_, Storage_Address, Sub_Party_Type, x5x5x5_Grading_412284402, Undefined_Supplier, Attended_Hospital, Risk_Claim_ID_414244883);

	------------------------------------------------------------
	--4)UpdateLoggingTidyTidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_Link] WHERE TaskID = @OutputTaskID AND DMLAction = 'INSERT')
		SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_Link] WHERE TaskID = @OutputTaskID AND DMLAction IN ('UPDATE','UPDATE DELETE'))
		SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_Link] WHERE TaskID = @OutputTaskID AND DMLAction = 'DELETE')
		
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()
		
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
						@Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'
END CATCH











