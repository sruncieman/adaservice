﻿CREATE PROCEDURE [Sync].[uspPopulateHeadOfClaim]
AS
/**************************************************************************************************/
-- ObjectName:		Sync.uspPopulateHeadOfClaim
--
-- Steps:			1)DeclareVariablesTempObjects
--					2)GetTheClaimLevelOrganisations
--					3)GetThePersonLevelOrganisations
--					4)ConcatenateAllHOCsIntoOneFieldForIncident2Person
--					5)PivotTheDataToMakeTheUpdateEasier
--					6)UpdateIncident2PersonTable
--					7)GetTheOrganisationLevelOrganisations
--					8)ConcatenateAllHOCsIntoOneFieldForIncident2Organisation
--					9)PivotTheDataToMakeTheUpdateEasier
--					10)UpdateOrganisation2PersonTable
--					11)DropObjects
--
-- RevisionHistory:  
--		Date				Author			Modification
--		2014-04-28			Paul Allen		Created
--		2014-07-31			Paul Allen		Defect5412 - HOCAppearingOnIncorrectLinks
--		2014-11-11			Paul Allen		Defect9771 - Amended RankID to use I2P.IBaseID not I2P.IncidentLinkID 
--		2015-02-12			Paul Allen		Refactored and Added Claim Level Organisations to The Insured Driver Record
--		2015-10-21			Paul Allen		Changed the Partition fuction for Vehicle HOCs to partition on Incident_Id and Vehicle_Id
/**************************************************************************************************/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

----------------------------------------------------
--1)DeclareVariablesTempObjects
----------------------------------------------------
IF OBJECT_ID('tempdb..#HOCData') IS NOT NULL
	DROP TABLE #HOCData
CREATE TABLE #HOCData (ID INT IDENTITY(1,1), IBaseID VARCHAR(50), HeadOfClaimTitle VARCHAR(50), HeadOfClaim VARCHAR(255))

IF OBJECT_ID('tempdb..#HOCDataUpdate') IS NOT NULL
	DROP TABLE #HOCDataUpdate
CREATE TABLE #HOCDataUpdate (IBaseID VARCHAR(50), HeadOfClaimTitle VARCHAR(50), HeadOfClaim VARCHAR(255))

IF OBJECT_ID('tempdb..#HOCPivot') IS NOT NULL
	DROP TABLE #HOCPivot
CREATE TABLE #HOCPivot (IBaseID VARCHAR(50), AccidentManagement VARCHAR(255), Broker VARCHAR(255), Engineer VARCHAR(255), Hire VARCHAR(255), MedicalExaminer VARCHAR(255), Recovery VARCHAR(255), RecoveryAddress VARCHAR(255), Repairer VARCHAR(255), RepairerAddress VARCHAR(255), Solicitors VARCHAR(255), UndefinedSupplier VARCHAR(255), Storage VARCHAR(255), StorageAddress VARCHAR(255), MedicalLegal VARCHAR(255), InspectionAddress VARCHAR(255))

DECLARE @InvolvementOrder TABLE (ID INT IDENTITY(1,1), PartyType VARCHAR(50), SubPartyType VARCHAR(50))
INSERT INTO @InvolvementOrder (PartyType, SubPartyType)
SELECT 'Insured', 'Driver'			UNION ALL
SELECT 'Policyholder', 'Driver'		UNION ALL
SELECT 'Policyholder', 'Unknown'	UNION ALL
SELECT 'Policyholder', 'Passenger'	UNION ALL
SELECT 'Insured', 'Unknown'			UNION ALL
SELECT 'Third Party', 'Driver'		UNION ALL
SELECT 'Claimant', 'Driver'			UNION ALL
SELECT 'Third Party', 'Unknown'		UNION ALL
SELECT 'Claimant', 'Unknown' 

----------------------------------------------------
--2)GetTheClaimLevelOrganisations
----------------------------------------------------
INSERT INTO #HOCData (IBaseID, HeadOfClaimTitle, HeadOfClaim)
SELECT	 I2P.IBaseId
		,CASE 
			WHEN HOC = 'Solicitor' THEN 'Solicitors'
			WHEN HOC = 'Credit Hire' THEN 'Hire'
			WHEN HOC = 'Unknown' THEN 'UndefinedSupplier'
			WHEN HOC = 'Vehicle Engineer' THEN 'Engineer'
			ELSE REPLACE(HOC,' ','')
			END HOC
		,HOCDetail
FROM	(
		SELECT DISTINCT I2O.Incident_Id, I2O.RiskClaim_Id, I2O.Organisation_Id, I2O.IBaseId, OLT.[TypeText] HOC, ORG.OrganisationName + ' (CL:' +  ORG.IBaseId + '); ' HOCDetail
		FROM [MDA].[dbo].[Incident2Organisation] I2O
		INNER JOIN [MDA].[dbo].[Organisation] ORG ON ORG.Id = I2O.Organisation_Id AND ORG.ADARecordStatus = 0
		INNER JOIN [MDA].[dbo].[OrganisationType] OLT ON OLT.ID = I2O.[OrganisationType_Id] AND OLT.ADARecordStatus = 0
		LEFT JOIN [MDA].[dbo].[Person2Organisation] P2O ON P2O.Organisation_Id = ORG.Id AND I2O.RiskClaim_Id = P2O.RiskClaim_Id AND P2O.ADARecordStatus = 0 
		LEFT JOIN [MDA].[dbo].[Incident2Person] I2P ON I2P.Incident_Id = I2O.Incident_Id AND I2P.Person_Id= P2O.Person_Id AND I2P.RiskClaim_Id = P2O.RiskClaim_Id AND I2P.ADARecordStatus = 0
		WHERE I2O.ADARecordStatus = 0
		AND OLT.[TypeText] NOT IN ('Policy Holder','Insurer')
		AND I2O.Source != 'Keoghs CFS'
		AND I2P.ID IS NULL
		UNION ALL
		SELECT DISTINCT I2O.Incident_Id, I2O.RiskClaim_Id, I2O.Organisation_Id, I2O.IBaseId, OLT.[TypeText] + ' Address' HOC, 
		REPLACE(ISNULL(NULLIF(A.[Building],'') + ', ','') + ISNULL(NULLIF(A.[SubBuilding],'') + ', ','') + ISNULL(NULLIF(A.[BuildingNumber],'') + ', ','') + ISNULL(NULLIF(A.[Street],'') + ', ','') + ISNULL(NULLIF(A.[Locality],'') + ', ','') + ISNULL(NULLIF(A.[Town],'') + ', ','') + ISNULL(NULLIF(A.[PostCode],'') + ', ','')+  '(CL:' +  A.IBaseId + '); ',', (',' (') HOCDetail
		FROM [MDA].[dbo].[Incident2Organisation] I2O
		INNER JOIN [MDA].[dbo].[Organisation] ORG ON ORG.Id = I2O.Organisation_Id AND ORG.ADARecordStatus = 0
		INNER JOIN [MDA].[dbo].[OrganisationType] OLT ON OLT.ID = I2O.[OrganisationType_Id] AND OLT.ADARecordStatus = 0
		LEFT JOIN [MDA].[dbo].[Person2Organisation] P2O ON P2O.Organisation_Id = ORG.Id AND P2O.ADARecordStatus = 0 AND I2O.RiskClaim_Id = P2O.RiskClaim_Id
		LEFT JOIN [MDA].[dbo].[Incident2Person] I2P ON I2P.Incident_Id = I2O.Incident_Id AND I2P.Person_Id= P2O.Person_Id AND I2P.RiskClaim_Id = P2O.RiskClaim_Id AND I2P.ADARecordStatus = 0
		INNER JOIN [MDA].[dbo].[Organisation2Address] O2A ON O2A.[Organisation_Id] = I2O.[Organisation_Id] AND O2A.RiskClaim_Id = O2A.RiskClaim_Id AND O2A.ADARecordStatus = 0 
		INNER JOIN [MDA].[dbo].[Address] A ON A.ID = O2A.Address_Id AND A.ADARecordStatus = 0 
		WHERE I2O.ADARecordStatus = 0
		AND OLT.[TypeText]  IN ('Recovery','Storage','Repairer')
		AND I2O.Source != 'Keoghs CFS'
		AND I2P.ID IS NULL
		) Data
INNER JOIN [MDA].[dbo].[Incident2Person] I2P ON I2P.Incident_Id = Data.Incident_Id AND I2P.RiskClaim_Id = Data.RiskClaim_Id
INNER JOIN [MDA].[dbo].[Incident] INC ON INC.Id = I2P.Incident_Id 
INNER JOIN [MDA].[dbo].[IncidentType] INCT ON INCT.Id = INC.IncidentType_Id
WHERE (I2P.PartyType_Id = 3 AND I2P.SubPartyType_Id = 1)
OR (I2P.PartyType_Id = 3 AND LEFT(INCT.IncidentType,12) = 'Mobile Phone' )
ORDER BY Data.Organisation_Id

----------------------------------------------------
--3)GetThePersonLevelOrganisations
----------------------------------------------------
INSERT INTO #HOCData (IBaseID, HeadOfClaimTitle, HeadOfClaim)
SELECT	 IBaseId
		,CASE 
			WHEN HOC = 'Solicitor' THEN 'Solicitors'
			WHEN HOC IN ('Accident Management','Medical Examiner','Storage Address','Medical Legal','Undefined Supplier') THEN REPLACE(HOC,' ','')
			WHEN HOC = 'Inspected By' THEN 'Engineer'
			WHEN HOC = 'Stored at' THEN 'Storage'
			WHEN HOC = 'Recovered to' THEN 'RecoveryAddress'
			WHEN HOC = 'Repaired at' THEN 'RepairerAddress'
			ELSE HOC
			END HOC
		,HOCDetail
FROM	(
		SELECT DISTINCT I2P.Incident_Id, I2P.Person_Id, I2P.IBaseId, CASE WHEN P2OLT.LinkTypeText = 'Unknown' THEN 'Undefined Supplier' ELSE  P2OLT.LinkTypeText END HOC, ORG.OrganisationName + ' (' +  ORG.IBaseId + '); ' HOCDetail, ORG.Id OrganisationID
		FROM [MDA].[dbo].[Incident2Person] I2P
		INNER JOIN [MDA].[dbo].[Person2Organisation] P2O ON P2O.Person_Id = I2P.Person_Id AND P2O.RiskClaim_Id = I2P.RiskClaim_Id AND P2O.ADARecordStatus = 0
		INNER JOIN [MDA].[dbo].[Organisation] ORG ON ORG.Id = P2O.Organisation_Id AND ORG.ADARecordStatus = 0
		INNER JOIN [MDA].[dbo].[Person2OrganisationLinkType] P2OLT ON P2OLT.ID = P2O.Person2OrganisationLinkType_Id AND P2OLT.ADARecordStatus = 0
		INNER JOIN [MDA].[dbo].[SubPartyType] SPT ON SPT.Id = I2P.SubPartyType_Id AND SPT.ADARecordStatus = 0
		WHERE I2P.ADARecordStatus = 0
		AND P2OLT.LinkTypeText IN ('Solicitor','Accident Management','Medical Examiner','Hire','Medical Legal','Broker','Undefined Supplier','Unknown')
		AND [SubPartyText] != 'Witness'
		UNION
		SELECT  Incident_Id,Person_Id,IBaseId,HOC,HOCDetail,OrganisationID
		FROM	(
				SELECT DISTINCT I2P.Incident_Id, I2P.Person_Id, I2P.IBaseId, CASE WHEN VLT.LinkText IN ('Recovered by','Recovered to') THEN 'Recovery' ELSE VLT.LinkText END HOC, ORG.OrganisationName + ' (' +  ORG.IBaseId + '); ' HOCDetail,DENSE_RANK() OVER (PARTITION BY I2P.Incident_Id, V2P.Vehicle_ID ORDER BY IOR.ID) RankID, ORG.Id OrganisationID
				FROM [MDA].[dbo].[Incident2Person] I2P
				INNER JOIN [MDA].[dbo].[Incident2Vehicle] I2V ON I2V.Incident_Id = I2P.Incident_Id AND I2V.RiskClaim_Id = I2P.RiskClaim_Id AND I2V.ADARecordStatus = 0
				INNER JOIN [MDA].[dbo].[Vehicle2Person] V2P ON V2P.Vehicle_Id = I2V.Vehicle_Id AND V2P.Person_Id = I2P.Person_Id AND V2P.RiskClaim_Id = I2P.RiskClaim_Id AND V2P.RiskClaim_Id = I2V.RiskClaim_Id AND V2P.ADARecordStatus = 0
				INNER JOIN [MDA].[dbo].[Vehicle2Organisation] V2O ON V2O.Vehicle_Id = V2P.Vehicle_Id  AND V2O.RiskClaim_Id = V2P.RiskClaim_Id AND V2O.RiskClaim_Id = I2V.RiskClaim_Id AND V2O.ADARecordStatus = 0
				INNER JOIN [MDA].[dbo].[Organisation] ORG ON ORG.Id = V2O.Organisation_Id AND ORG.ADARecordStatus = 0
				INNER JOIN [MDA].[dbo].[VehicleLinkType] VLT ON VLT.Id = V2O.VehicleLinkType_Id AND VLT.ADARecordStatus = 0
				INNER JOIN [MDA].[dbo].[PartyType] PT ON PT.Id = I2P.PartyType_Id AND PT.ADARecordStatus = 0
				INNER JOIN [MDA].[dbo].[SubPartyType] SPT ON SPT.Id = I2P.SubPartyType_Id AND SPT.ADARecordStatus = 0
				INNER JOIN @InvolvementOrder IOR ON IOR.PartyType = PT.PartyTypeText AND IOR.SubPartyType = SPT.SubPartyText
				WHERE I2P.ADARecordStatus = 0
				AND VLT.LinkText IN ('Inspected By','Recovered to','Recovered by','Repairer','Stored At')
				AND [SubPartyText] != 'Witness'
				) Data
		WHERE Data.RankID = 1
		UNION
		SELECT	Incident_Id,Person_Id,IBaseId,HOC,HOCDetail,OrganisationID
		FROM	(
				SELECT  DISTINCT I2P.Incident_Id, I2P.Person_Id, I2P.IBaseId, CASE WHEN ALT.[LinkType] = 'Inspected at' THEN 'InspectionAddress' ELSE ALT.[LinkType] END HOC, 
						REPLACE(ISNULL(NULLIF(A.[Building],'') + ', ','') + ISNULL(NULLIF(A.[SubBuilding],'') + ', ','') + ISNULL(NULLIF(A.[BuildingNumber],'') + ', ','') + ISNULL(NULLIF(A.[Street],'') + ', ','') + ISNULL(NULLIF(A.[Locality],'') + ', ','') + ISNULL(NULLIF(A.[Town],'') + ', ','') + ISNULL(NULLIF(A.[PostCode],'') + ', ','')+  '(' +  A.IBaseId + '); ',', (',' (') HOCDetail,
						DENSE_RANK() OVER (PARTITION BY I2P.Incident_Id, V2P.Vehicle_ID ORDER BY IOR.ID) RankID
						,PT.PartyTypeText
						,SPT.SubPartyText
						,IOR.ID
						,I2P.IncidentLinkID 
						,100000000 + A.Id OrganisationID
				FROM [MDA].[dbo].[Incident2Person] I2P
				INNER JOIN [MDA].[dbo].[Incident2Vehicle] I2V ON I2V.Incident_Id = I2P.Incident_Id AND I2V.RiskClaim_Id = I2P.RiskClaim_Id AND I2V.ADARecordStatus = 0
				INNER JOIN [MDA].[dbo].[Vehicle2Person] V2P ON V2P.Vehicle_Id = I2V.Vehicle_Id AND V2P.Person_Id = I2P.Person_Id AND V2P.RiskClaim_Id = I2P.RiskClaim_Id AND V2P.RiskClaim_Id = I2V.RiskClaim_Id AND V2P.ADARecordStatus = 0 
				INNER JOIN [MDA].[dbo].[Vehicle2Address] V2A ON V2A.Vehicle_Id = V2P.Vehicle_Id AND V2A.RiskClaim_Id = I2P.RiskClaim_Id AND V2A.ADARecordStatus = 0 
				INNER JOIN [MDA].[dbo].[Address] A ON A.ID = V2A.Address_Id AND A.ADARecordStatus = 0 
				INNER JOIN [MDA].[dbo].[AddressLinkType] ALT ON ALT.Id = V2A.AddressLinkType_Id AND V2A.ADARecordStatus = 0
				INNER JOIN [MDA].[dbo].[PartyType] PT ON PT.Id = I2P.PartyType_Id AND PT.ADARecordStatus = 0
				INNER JOIN [MDA].[dbo].[SubPartyType] SPT ON SPT.Id = I2P.SubPartyType_Id AND SPT.ADARecordStatus = 0
				INNER JOIN @InvolvementOrder IOR ON IOR.PartyType = PT.PartyTypeText AND IOR.SubPartyType = SPT.SubPartyText
				WHERE I2P.ADARecordStatus = 0
				AND ALT.[LinkType] IN ('Storage Address','Recovered to','Repaired at','Inspected at')
				AND [SubPartyText] != 'Witness'
				) DATA
		WHERE DATA.RankID = 1
		) Data
ORDER BY Data.OrganisationID

--AddIndexForPerformance
CREATE CLUSTERED INDEX IX_#HOCData_IBaseID_HeadOfClaimTitle_HeadOfClaim_C
ON #HOCData (IBaseID, HeadOfClaimTitle, HeadOfClaim)

----------------------------------------------------
--4)ConcatenateAllHOCsIntoOneFieldForIncident2Person
----------------------------------------------------
INSERT INTO #HOCDataUpdate (IBaseID, HeadOfClaimTitle, HeadOfClaim)
SELECT IBaseID, HeadOfClaimTitle, REPLACE(HeadOfClaim,'&amp;','&')
FROM	(
		SELECT DISTINCT IBaseID, HeadOfClaimTitle, LEFT(REPLACE(STUFF((SELECT ' '+HeadOfClaim FROM #HOCData H0 WHERE H0.IBaseID = H1.IBaseID  AND H0.HeadOfClaimTitle = H1.HeadOfClaimTitle ORDER BY H0.ID FOR XML PATH('')),1,1,'')  + '~','; ~',''),255) HeadOfClaim
		FROM  #HOCData H1
		) DATA
ORDER BY IBaseID

--------------------------
--5)PivotTheDataToMakeTheUpdateEasier
--------------------------
INSERT INTO #HOCPivot (IBaseID, AccidentManagement, Broker, Engineer, Hire, MedicalExaminer, Recovery, RecoveryAddress, Repairer, RepairerAddress, Solicitors, UndefinedSupplier,Storage,StorageAddress,MedicalLegal,InspectionAddress)
SELECT IBaseID, AccidentManagement, Broker, Engineer, Hire, MedicalExaminer, Recovery, RecoveryAddress, Repairer, RepairerAddress, Solicitors, UndefinedSupplier,Storage,StorageAddress,MedicalLegal,InspectionAddress
FROM	(
		SELECT IBaseID, HeadOfClaimTitle, HeadOfClaim
		FROM #HOCDataUpdate
		) SRC
PIVOT
		(
		  MAX(HeadOfClaim)
		  FOR HeadOfClaimTitle in ([RecoveryAddress],[MedicalExaminer],[AccidentManagement],[Broker],[UndefinedSupplier],[Repairer],[Engineer],[Hire],[Recovery],[Solicitors],[RepairerAddress],[Storage],[StorageAddress],[MedicalLegal],[InspectionAddress])
		) PIV;

--------------------------
--6)UpdateIncident2PersonTable
--------------------------
UPDATE I2P
SET	 [AccidentManagement]	= CASE WHEN NULLIF(HOC.[AccidentManagement],'') IS NOT NULL THEN HOC.[AccidentManagement] ELSE NULL END
	,[Broker]				= CASE WHEN NULLIF(HOC.[Broker],'') IS NOT NULL THEN HOC.[Broker] ELSE NULL END
	,[Engineer]				= CASE WHEN NULLIF(HOC.[Engineer],'') IS NOT NULL THEN HOC.[Engineer] ELSE NULL END
	,[Hire]					= CASE WHEN NULLIF(HOC.[Hire],'') IS NOT NULL THEN HOC.[Hire] ELSE NULL END
	,[MedicalExaminer]		= CASE WHEN NULLIF(HOC.[MedicalExaminer],'') IS NOT NULL THEN HOC.[MedicalExaminer] ELSE NULL END
	,[Recovery]				= CASE WHEN NULLIF(HOC.[Recovery],'') IS NOT NULL THEN HOC.[Recovery] ELSE NULL END
	,[RecoveryAddress]		= CASE WHEN NULLIF(HOC.[RecoveryAddress],'') IS NOT NULL THEN HOC.[RecoveryAddress] ELSE NULL END
	,[Repairer]				= CASE WHEN NULLIF(HOC.[Repairer],'') IS NOT NULL THEN HOC.[Repairer] ELSE NULL END
	,[RepairerAddress]		= LEFT(CASE WHEN NULLIF(HOC.[RepairerAddress],'') IS NOT NULL THEN HOC.[RepairerAddress] ELSE NULL END,225)
	,[Solicitors]			= CASE WHEN NULLIF(HOC.[Solicitors],'') IS NOT NULL THEN HOC.[Solicitors] ELSE NULL END
	,[Storage]				= CASE WHEN NULLIF(HOC.[Storage],'') IS NOT NULL THEN HOC.[Storage] ELSE NULL END
	,[StorageAddress]		= CASE WHEN NULLIF(HOC.[StorageAddress],'') IS NOT NULL THEN HOC.[StorageAddress] ELSE NULL END
	,[UndefinedSupplier]	= CASE WHEN NULLIF(HOC.[UndefinedSupplier],'') IS NOT NULL THEN HOC.[UndefinedSupplier] ELSE NULL END
	,[MedicalLegal]			= CASE WHEN NULLIF(HOC.[MedicalLegal],'') IS NOT NULL THEN HOC.[MedicalLegal] ELSE NULL END
	,[InspectionAddress]	= CASE WHEN NULLIF(HOC.[InspectionAddress],'') IS NOT NULL THEN HOC.[InspectionAddress] ELSE NULL END
FROM MDA.dbo.Incident2Person I2P
LEFT JOIN #HOCPivot HOC ON HOC.IBaseID = I2P.IBaseId
WHERE ISNULL(HOC.[AccidentManagement],'') != ISNULL(I2P.[AccidentManagement],'') 
OR ISNULL(HOC.[Broker],'') != ISNULL(I2P.[Broker],'') 
OR ISNULL(HOC.[Engineer],'') != ISNULL(I2P.[Engineer],'') 
OR ISNULL(HOC.[Hire],'') != ISNULL(I2P.[Hire],'') 
OR ISNULL(HOC.[MedicalExaminer],'') != ISNULL(I2P.[MedicalExaminer],'')
OR ISNULL(HOC.[Recovery],'') != ISNULL(I2P.[Recovery],'') 
OR ISNULL(HOC.[RecoveryAddress],'') != ISNULL(I2P.[RecoveryAddress],'') 
OR LEFT(ISNULL(HOC.[Repairer],''),225) != ISNULL(I2P.[Repairer],'') 
OR ISNULL(HOC.[Solicitors],'') != ISNULL(I2P.[Solicitors],'') 
OR ISNULL(HOC.[Storage],'') != ISNULL(I2P.[Storage],'') 
OR ISNULL(HOC.[StorageAddress],'') != ISNULL(I2P.[StorageAddress],'') 
OR ISNULL(HOC.[UndefinedSupplier],'') != ISNULL(I2P.[UndefinedSupplier],'') 
OR ISNULL(HOC.[MedicalLegal],'') != ISNULL(I2P.[MedicalLegal],'') 
OR ISNULL(HOC.[InspectionAddress],'') != ISNULL(I2P.[InspectionAddress],'') 
PRINT CAST(@@ROWCOUNT AS VARCHAR) + ' Incident2Person Records Updated...'

-------------------------------------------------
-->>** NowRepeatTheProcessForOrganisations <<**--
-------------------------------------------------
TRUNCATE TABLE #HOCData
TRUNCATE TABLE #HOCDataUpdate
TRUNCATE TABLE #HOCPivot

----------------------------------------------------
--7)GetTheOrganisationLevelOrganisations
----------------------------------------------------
INSERT INTO #HOCData (IBaseID, HeadOfClaimTitle, HeadOfClaim)
SELECT	 IBaseId
		,CASE 
			WHEN HOC = 'Solicitor' THEN 'Solicitors'
			WHEN HOC IN ('Accident Management','Medical Examiner','Storage Address','Medical Legal') THEN REPLACE(HOC,' ','')
			WHEN HOC = 'Inspected By' THEN 'Engineer'
			WHEN HOC = 'Stored at' THEN 'Storage'
			WHEN HOC = 'Recovered to' THEN 'RecoveryAddress'
			WHEN HOC = '??' THEN 'RepairerAddress'
			ELSE HOC
			END HOC
		,HOCDetail
FROM	(
		SELECT DISTINCT I2O.Incident_Id, I2O.Organisation_Id, I2O.IBaseId, OLT.[LinkType] HOC, ORG.OrganisationName + ' (' +  ORG.IBaseId + '); ' HOCDetail, ORG.Id OrganisationID
		FROM [MDA].[dbo].[Incident2Organisation] I2O
		INNER JOIN [MDA].[dbo].[Organisation2Organisation] O2O ON O2O.[Organisation1_Id] = I2O.Organisation_Id AND O2O.SourceReference = I2O.SourceReference  AND O2O.ADARecordStatus = 0
		INNER JOIN [MDA].[dbo].[Organisation] ORG ON ORG.Id = O2O.Organisation2_Id AND ORG.ADARecordStatus = 0
		INNER JOIN [MDA].[dbo].[OrganisationLinkType] OLT ON OLT.ID = O2O.[OrganisationLinkType_Id] AND OLT.ADARecordStatus = 0
		WHERE I2O.ADARecordStatus = 0
		AND OLT.[LinkType] IN ('Solicitor','Accident Management','Medical Examiner','Hire','Medical Legal','Broker')
		UNION
		SELECT DISTINCT I2O.Incident_Id, I2O.Organisation_Id, I2O.IBaseId, CASE WHEN VLT.LinkText IN ('Recovered by','Recovered to') THEN 'Recovery' ELSE VLT.LinkText END HOC, ORG.OrganisationName + ' (' +  ORG.IBaseId + '); ' HOCDetail, ORG.Id OrganisationID
		FROM [MDA].[dbo].[Incident2Organisation] I2O
		INNER JOIN [MDA].[dbo].[Vehicle2Organisation] V2O ON V2O.[Organisation_Id] = I2O.[Organisation_Id] AND V2O.RiskClaim_Id = I2O.RiskClaim_Id AND V2O.ADARecordStatus = 0
		INNER JOIN [MDA].[dbo].[Organisation] ORG ON ORG.Id = V2O.Organisation_Id AND ORG.ADARecordStatus = 0
		INNER JOIN [MDA].[dbo].[VehicleLinkType] VLT ON VLT.Id = V2O.VehicleLinkType_Id AND VLT.ADARecordStatus = 0
		WHERE I2O.ADARecordStatus = 0
		AND VLT.LinkText IN ('Inspected By','Recovered to','Recovered by','Repairer','Stored At')
		UNION
		SELECT  DISTINCT I2O.Incident_Id, I2O.Organisation_Id, I2O.IBaseId, CASE WHEN ALT.[LinkType] = 'Inspected at' THEN 'InspectionAddress' ELSE ALT.[LinkType] END HOC,
				REPLACE(ISNULL(NULLIF(A.[Building],'') + ', ','') + ISNULL(NULLIF(A.[SubBuilding],'') + ', ','') + ISNULL(NULLIF(A.[BuildingNumber],'') + ', ','') + ISNULL(NULLIF(A.[Street],'') + ', ','') + ISNULL(NULLIF(A.[Locality],'') + ', ','') + ISNULL(NULLIF(A.[Town],'') + ', ','') + ISNULL(NULLIF(A.[PostCode],'') + ', ','')+  '(' +  A.IBaseId + '); ',', (',' (') HOCDetail, 100000000 + A.Id OrganisationID
		FROM [MDA].[dbo].[Incident2Organisation] I2O
		INNER JOIN [MDA].[dbo].[Vehicle2Organisation] V2O ON V2O.[Organisation_Id] = I2O.[Organisation_Id] AND V2O.RiskClaim_Id = I2O.RiskClaim_Id AND V2O.ADARecordStatus = 0
		INNER JOIN [MDA].[dbo].[Organisation2Address] O2A ON O2A.[Organisation_Id] = V2O.[Organisation_Id] AND O2A.RiskClaim_Id = O2A.RiskClaim_Id AND V2O.ADARecordStatus = 0 
		INNER JOIN [MDA].[dbo].[Address] A ON A.ID = O2A.Address_Id AND A.ADARecordStatus = 0 
		INNER JOIN [MDA].[dbo].[AddressLinkType] ALT ON ALT.Id = O2A.AddressLinkType_Id AND ALT.ADARecordStatus = 0
		WHERE I2O.ADARecordStatus = 0
		AND ALT.[LinkType] IN ('Storage Address','Recovered to','??','Inspected at') --Missing:Repaired at | Check "Stored At"		
		) Data
ORDER BY Data.OrganisationID

----------------------------------------------------
--8)ConcatenateAllHOCsIntoOneFieldForIncident2Organisation
----------------------------------------------------
INSERT INTO #HOCDataUpdate (IBaseID, HeadOfClaimTitle, HeadOfClaim)
SELECT IBaseID, HeadOfClaimTitle, REPLACE(HeadOfClaim,'&amp;','&')
FROM	(
		SELECT DISTINCT IBaseID, HeadOfClaimTitle, LEFT(REPLACE(STUFF((SELECT ' '+HeadOfClaim FROM #HOCData H0 WHERE H0.IBaseID = H1.IBaseID  AND H0.HeadOfClaimTitle = H1.HeadOfClaimTitle ORDER BY H0.ID FOR XML PATH('')),1,1,'')  + '~','; ~',''),255) HeadOfClaim
		FROM  #HOCData H1
		) DATA
ORDER BY IBaseID

--------------------------
--9)PivotTheDataToMakeTheUpdateEasier
--------------------------
INSERT INTO #HOCPivot (IBaseID, AccidentManagement, Broker, Engineer, Hire, MedicalExaminer, Recovery, RecoveryAddress, Repairer, RepairerAddress, Solicitors, UndefinedSupplier,Storage,StorageAddress,MedicalLegal,InspectionAddress)
SELECT IBaseID, AccidentManagement, Broker, Engineer, Hire, MedicalExaminer, Recovery, RecoveryAddress, Repairer, RepairerAddress, Solicitors, UndefinedSupplier,Storage,StorageAddress,MedicalLegal,InspectionAddress
FROM	(
		SELECT IBaseID, HeadOfClaimTitle, HeadOfClaim
		FROM #HOCDataUpdate
		) SRC
PIVOT
		(
		  MAX(HeadOfClaim)
		  FOR HeadOfClaimTitle in ([RecoveryAddress],[MedicalExaminer],[AccidentManagement],[Broker],[UndefinedSupplier],[Repairer],[Engineer],[Hire],[Recovery],[Solicitors],[RepairerAddress],[Storage],[StorageAddress],[MedicalLegal],[InspectionAddress])
		) PIV;

--------------------------
--10)UpdateOrganisation2PersonTable
--------------------------
UPDATE I2O
SET	 [AccidentManagement]	= CASE WHEN NULLIF(HOC.[AccidentManagement],'') IS NOT NULL THEN HOC.[AccidentManagement] ELSE NULL END
	,[Broker]				= CASE WHEN NULLIF(HOC.[Broker],'') IS NOT NULL THEN HOC.[Broker] ELSE NULL END
	,[Engineer]				= CASE WHEN NULLIF(HOC.[Engineer],'') IS NOT NULL THEN HOC.[Engineer] ELSE NULL END
	,[Hire]					= CASE WHEN NULLIF(HOC.[Hire],'') IS NOT NULL THEN HOC.[Hire] ELSE NULL END
	,[MedicalExaminer]		= CASE WHEN NULLIF(HOC.[MedicalExaminer],'') IS NOT NULL THEN HOC.[MedicalExaminer] ELSE NULL END
	,[Recovery]				= CASE WHEN NULLIF(HOC.[Recovery],'') IS NOT NULL THEN HOC.[Recovery] ELSE NULL END
	,[RecoveryAddress]		= CASE WHEN NULLIF(HOC.[RecoveryAddress],'') IS NOT NULL THEN HOC.[RecoveryAddress] ELSE NULL END
	,[Repairer]				= CASE WHEN NULLIF(HOC.[Repairer],'') IS NOT NULL THEN HOC.[Repairer] ELSE NULL END
	,[RepairerAddress]		= LEFT(CASE WHEN NULLIF(HOC.[RepairerAddress],'') IS NOT NULL THEN HOC.[RepairerAddress] ELSE NULL END,225)
	,[Solicitors]			= CASE WHEN NULLIF(HOC.[Solicitors],'') IS NOT NULL THEN HOC.[Solicitors] ELSE NULL END
	,[Storage]				= CASE WHEN NULLIF(HOC.[Storage],'') IS NOT NULL THEN HOC.[Storage] ELSE NULL END
	,[StorageAddress]		= CASE WHEN NULLIF(HOC.[StorageAddress],'') IS NOT NULL THEN HOC.[StorageAddress] ELSE NULL END
	,[UndefinedSupplier]	= CASE WHEN NULLIF(HOC.[UndefinedSupplier],'') IS NOT NULL THEN HOC.[UndefinedSupplier] ELSE NULL END
	,[MedicalLegal]			= CASE WHEN NULLIF(HOC.[MedicalLegal],'') IS NOT NULL THEN HOC.[MedicalLegal] ELSE NULL END
	,[InspectionAddress]	= CASE WHEN NULLIF(HOC.[InspectionAddress],'') IS NOT NULL THEN HOC.[InspectionAddress] ELSE NULL END
FROM MDA.dbo.Incident2Organisation I2O
LEFT JOIN #HOCPivot HOC ON HOC.IBaseID = I2O.IBaseId
WHERE ISNULL(HOC.[AccidentManagement],'') != ISNULL(I2O.[AccidentManagement],'') 
OR ISNULL(HOC.[Broker],'') != ISNULL(I2O.[Broker],'') 
OR ISNULL(HOC.[Engineer],'') != ISNULL(I2O.[Engineer],'') 
OR ISNULL(HOC.[Hire],'') != ISNULL(I2O.[Hire],'') 
OR ISNULL(HOC.[MedicalExaminer],'') != ISNULL(I2O.[MedicalExaminer],'')
OR ISNULL(HOC.[Recovery],'') != ISNULL(I2O.[Recovery],'') 
OR ISNULL(HOC.[RecoveryAddress],'') != ISNULL(I2O.[RecoveryAddress],'') 
OR LEFT(ISNULL(HOC.[Repairer],''),225) != ISNULL(I2O.[Repairer],'') 
OR ISNULL(HOC.[Solicitors],'') != ISNULL(I2O.[Solicitors],'') 
OR ISNULL(HOC.[Storage],'') != ISNULL(I2O.[Storage],'') 
OR ISNULL(HOC.[StorageAddress],'') != ISNULL(I2O.[StorageAddress],'') 
OR ISNULL(HOC.[UndefinedSupplier],'') != ISNULL(I2O.[UndefinedSupplier],'') 
OR ISNULL(HOC.[MedicalLegal],'') != ISNULL(I2O.[MedicalLegal],'') 
OR ISNULL(HOC.[InspectionAddress],'') != ISNULL(I2O.[InspectionAddress],'') 
PRINT CAST(@@ROWCOUNT AS VARCHAR) + ' Incident2Organisation Records Updated...'

--------------------------
--11)DropObjects
--------------------------
IF OBJECT_ID('tempdb..#HOCData') IS NOT NULL
	DROP TABLE #HOCData

IF OBJECT_ID('tempdb..#HOCDataUpdate') IS NOT NULL
	DROP TABLE #HOCDataUpdate

IF OBJECT_ID('tempdb..#HOCPivot') IS NOT NULL
	DROP TABLE #HOCPivot
