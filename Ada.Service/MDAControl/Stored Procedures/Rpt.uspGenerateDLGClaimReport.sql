﻿

CREATE PROCEDURE [Rpt].[uspGenerateDLGClaimReport]
AS
SET NOCOUNT ON
SET DATEFORMAT DMY

--------------------------------------
--DeclareAndSetRequiredObjects
--------------------------------------
DECLARE  @RowsToProcess INT
		,@Counter INT =1
		,@RunNameID INT = 148 --DLGClaimReport Control Run
		,@ControlRunID INT
		,@ReportCreationOutputDeliveryConfiguration_Id INT
		,@RiskBatch_ID INT
		,@SQL NVARCHAR(MAX)
		,@BCP VARCHAR(8000)
		,@ResultFileName VARCHAR(100)
		,@ResultTableName NVARCHAR(100) = (SELECT '[dbo].[DLGBatchResults_' + CAST(NEWID() AS NVARCHAR(50)) + ']')
		,@MinScoreToRpt INT

BEGIN TRY
	DECLARE @BatchesToProcess TABLE (RowID INT IDENTITY(1,1), ReportCreationOutputDeliveryConfiguration_Id INT NOT NULL, RiskBatch_Id INT NOT NULL)
			  
	IF OBJECT_ID('tempdb..#KeoghsRule') IS NOT NULL
		DROP TABLE #KeoghsRule 
	CREATE TABLE #KeoghsRule (RiskClaim_Id INT, Risk VARCHAR(10), KeoghsRule VARCHAR(MAX))

	IF OBJECT_ID('tempdb..#DLGBatchResults') IS NOT NULL
		DROP TABLE #DLGBatchResults
	CREATE TABLE #DLGBatchResults (RowID INT IDENTITY(1,1), KeoghsRef VARCHAR(50), DLGClaimNumber VARCHAR(50), IncidentDate VARCHAR(20), Score INT, Risk VARCHAR(10), ADARules VARCHAR(MAX))

	--------------------------------------
	--GetAListOfAllReportsThatNeedGenerating
	--------------------------------------
	INSERT INTO @BatchesToProcess (ReportCreationOutputDeliveryConfiguration_Id, RiskBatch_Id)

	SELECT DISTINCT RPT.[Id] ReportCreationOutputDeliveryConfiguration_Id, RB.Id RiskBatch_id

	FROM [MDA].[dbo].[RiskBatch] RB 
		INNER JOIN [Rpt].[ReportCreationOutputDeliveryConfiguration] RPT ON RPT.[RiskClient_Id] = RB.RiskClient_Id 

	WHERE 
		RB.[BatchStatus] = 5
		AND RPT.[ReportName_Id] = 3
		AND RPT.[IsActive] = 1
		AND RB.Id > ISNULL(RPT.[IgnoreBatchIdsTo],0)
		AND NOT EXISTS (SELECT TOP 1 1 FROM [MDA].[dbo].[RiskClaim] RC WHERE RC.RiskBatch_Id = RB.Id AND RC.ClaimStatus BETWEEN 0 AND 8)
		AND (
			NOT EXISTS 
				(	SELECT TOP 1 1 
					FROM [Rpt].[ReportNameRun] RPTR 
					WHERE RPTR.[ReportCreationOutputDeliveryConfiguration_Id] = RPT.[Id] 
						AND RPTR.[BatchID] = RB.Id 
						AND [Resend] = 0
				)
			OR EXISTS 
				(	SELECT TOP 1 1 
					FROM [Rpt].[ReportNameRun] RPTR 
					WHERE RPTR.[ReportCreationOutputDeliveryConfiguration_Id] = RPT.[Id] 
					AND RPTR.[BatchID] = RB.Id 
					AND [Resend] = 1
				)
			)
	SELECT @RowsToProcess = @@ROWCOUNT

	--------------------------------------
	--LoopAroundGeneratingTheFile
	--------------------------------------
	WHILE @RowsToProcess >= @Counter
	BEGIN
		--CreateTheControlRunRecord
		INSERT INTO [MDAControl].[dbo].[DBControlRun] ([RunNameID]) VALUES (@RunNameID)
		SELECT @ControlRunID = SCOPE_IDENTITY()

		--CreateTheImportTable
		SELECT @SQL = 'CREATE TABLE ' + @ResultTableName + ' (RowID INT IDENTITY(1,1), KeoghsRef VARCHAR(50), DLGClaimNumber VARCHAR(50), IncidentDate VARCHAR(20), Score INT, Risk VARCHAR(10), ADARules VARCHAR(MAX))'
		EXECUTE SP_EXECUTESQL  @SQL

		--PopulateTheParametersWeNeed
		SELECT	 @RiskBatch_ID = BTP.RiskBatch_Id
				,@MinScoreToRpt = RPT.RiskRunExceedScore
				,@ReportCreationOutputDeliveryConfiguration_Id = ReportCreationOutputDeliveryConfiguration_Id
				,@ResultFileName = DestinationFileLocation + REPLACE(REPLACE(REPLACE(REPLACE([DestinationFileName],'@ClientName',RC.ClientName),'@CurrentDateTime',REPLACE(REPLACE(CONVERT(VARCHAR,GETDATE(),120),'-',''),':','')),'@RiskBatch_ID',@RiskBatch_ID),' ','') + '.' + [DestinationFileFormat]
		FROM @BatchesToProcess BTP
		INNER JOIN [Rpt].[ReportCreationOutputDeliveryConfiguration] RPT ON RPT.[Id] = BTP.ReportCreationOutputDeliveryConfiguration_Id 
		INNER JOIN [MDA].[dbo].[RiskClient] RC ON RC.Id = RPT.RiskClient_Id
		INNER JOIN [MDA].[dbo].[RiskBatch] RB ON RB.Id = BTP.RiskBatch_Id
		WHERE BTP.RowID = @Counter

		--AddTheParemeterDetailsToTheControlRunTable
		UPDATE [dbo].[DBControlRun]
		SET [Details] = 'FileLocation:[' + ISNULL(@ResultFileName,'') + '];'					
		WHERE [RunID] = @ControlRunID

		--CheckToSeeIfWeHaveAFileLocation
		IF NULLIF(@ResultFileName,'') IS NOT NULL
		BEGIN
			--GetTheKeoghRuleDataFromTheXML
			;WITH XMLNAMESPACES (DEFAULT 'http://keo/MsgsV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
			INSERT INTO #KeoghsRule (RiskClaim_Id, Risk, KeoghsRule)
			SELECT	 RCR.[RiskClaim_Id]
					,[Risk]	=	CASE	WHEN RCR.TotalScore >= 50 AND RCR.TotalScore < 250
										THEN 'Medium'
										WHEN RCR.TotalScore >= 250
										THEN 'High'
										ELSE 'Low'
										END	
					,CAST(Col.query('data(../H)') AS VARCHAR(MAX)) + ' ' +  CAST(Col.query('data(I)') AS VARCHAR(MAX)) + '; ' KeoghsRule
			FROM [MDA].[dbo].[RiskClaimRun] RCR
			INNER JOIN [MDA].[dbo].[RiskClaim] RC ON RC.Id = RCR.RiskClaim_Id
			INNER JOIN  (
						SELECT Id, CAST([SummaryMessagesXml] AS XML) SummaryMessagesXml
						FROM [MDA].[dbo].[RiskClaimRun]
						) RCRXML ON RCRXML.ID = RCR.ID 
			CROSS APPLY  RCRXML.SummaryMessagesXml.nodes('/ArrayOfML/ML/M') AS T(Col)
			WHERE RCR.TotalScore >= @MinScoreToRpt
			AND TotalScore != ISNULL(PrevTotalScore,0)
			AND RC.RiskBatch_Id = @RiskBatch_ID

			--AddHeaderRecord
			INSERT INTO #DLGBatchResults (KeoghsRef, DLGClaimNumber, IncidentDate, Score, Risk, ADARules)
			VALUES ('KeoghsRef', 'DLGClaimNumber','IncidentDate', '999','Risk','ADARules');

			--GetTheRequiredResultsIntoCTESoWeCanOrderBetter
			WITH CTE_RESULTS AS
			(
			SELECT   DISTINCT INC.IBaseId [KeoghsRef]
					,RC.[ClientClaimRefNumber] [ClaimNumber]
					,CONVERT(VARCHAR, INC.[IncidentDate], 103) [IncidentDate]
					,RCR.TotalScore
					,KR.Risk [Risk]
					,KR.KeoghsRule [KeoghsRule]
			FROM MDA.dbo.RiskClaimRun RCR
			INNER JOIN MDA.[dbo].[RiskClaim] RC ON RC.Id = RCR.RiskClaim_Id
			INNER JOIN MDA.[dbo].[Incident] INC ON INC.Id = RC.Incident_Id
			LEFT JOIN	(
						SELECT   DISTINCT RiskClaim_Id
								,Risk
								,REPLACE(STUFF((SELECT ' '+KeoghsRule FROM #KeoghsRule KR1 WHERE KR1.RiskClaim_Id = KR0.RiskClaim_Id FOR XML PATH('')),1,1,'')  + '~','; ~','') KeoghsRule
						FROM #KeoghsRule KR0
						) KR ON KR.RiskClaim_Id = RCR.RiskClaim_Id
			WHERE TotalScore >= @MinScoreToRpt
			AND TotalScore != ISNULL(PrevTotalScore,0)
			AND RC.RiskBatch_Id = @RiskBatch_ID
			)

			INSERT INTO #DLGBatchResults (KeoghsRef, DLGClaimNumber, IncidentDate, Score, Risk, ADARules)
			SELECT  KeoghsRef [KeoghsRef] 
					,ClaimNumber [Claim Number]
					,CONVERT(VARCHAR, [IncidentDate], 103) [Incident Date]
					,TotalScore
					,Risk [Risk]
					,KeoghsRule [Keoghs Rule]
			FROM CTE_RESULTS RES
			ORDER BY TotalScore DESC
					,ClaimNumber					

			--GettheResultsIntoATableWhichWeWillUseTooutputFrom
			SELECT @SQL = 'INSERT INTO ' + @ResultTableName + ' SELECT KeoghsRef, DLGClaimNumber, IncidentDate, Score, Risk, ADARules FROM #DLGBatchResults ORDER BY RowID'
			EXECUTE SP_EXECUTESQL  @SQL

			--BCPFileOutToTheFileDirectory
			SELECT @BCP = 'BCP "SELECT CHAR(34) + DLGClaimNumber + CHAR(34), CHAR(34) + IncidentDate + CHAR(34), CHAR(34) + Risk + CHAR(34), CHAR(34) + ADARules + CHAR(34) FROM [MDAControl].' + @ResultTableName + ' ORDER BY RowID" queryout ' + @ResultFileName +' -T -c -t,'
			EXEC master..XP_CMDSHELL @BCP, no_output

			--AddTheParemeterDetailsToTheControlRunTable
			UPDATE [dbo].[DBControlRun]
			SET [Details] = [Details] + '];UserName[' + SYSTEM_USER + ']'
			WHERE [RunID] = @ControlRunID	
			
		END

		--InsertRecordIntoRunHistory
		INSERT INTO [Rpt].[ReportNameRun] (ReportCreationOutputDeliveryConfiguration_Id, DBControlRun_Id, BatchID)
		VALUES (@ReportCreationOutputDeliveryConfiguration_Id, @ControlRunID, @RiskBatch_ID)

		--UpdateResendFlagForBatch
		UPDATE [Rpt].[ReportNameRun]
		SET [Resend] = 0
		WHERE [BatchID] = @RiskBatch_ID
		AND [Resend] = 1

		--CloseControlRunRecord
		UPDATE [dbo].[DBControlRun]
		SET [RunEndTime] = GETDATE()
		WHERE [RunID] = @ControlRunID

		--PrepareForTheNextLoop
		SELECT	 @SQL = 'IF OBJECT_ID('''+ @ResultTableName +''') IS NOT NULL DROP TABLE ' + @ResultTableName
				,@Counter += 1

		EXECUTE SP_EXECUTESQL  @SQL
		DELETE #KeoghsRule
		DELETE #DLGBatchResults
	END

	--------------------------------------
	-- Tidy
	--------------------------------------
	IF OBJECT_ID('tempdb..#KeoghsRule') IS NOT NULL
		DROP TABLE #KeoghsRule 

	IF OBJECT_ID('tempdb..#DLGBatchResults') IS NOT NULL
		DROP TABLE #DLGBatchResults

END TRY
BEGIN CATCH
	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+ISNULL(@ErrorProcedure,'')+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'

	UPDATE [dbo].[DBControlRun]
	SET  [RunEndTime] = GETDATE()
		,[Error] = @ErrorSeverity
		,[ErrorDescription] = @ErrorMessage
	WHERE [RunID] = @ControlRunID

	SELECT	 @SQL = 'IF OBJECT_ID('''+ @ResultTableName +''') IS NOT NULL DROP TABLE ' + @ResultTableName
	EXECUTE SP_EXECUTESQL  @SQL

	IF OBJECT_ID('tempdb..#KeoghsRule') IS NOT NULL
		DROP TABLE #KeoghsRule 

	IF OBJECT_ID('tempdb..#DLGBatchResults') IS NOT NULL
		DROP TABLE #DLGBatchResults

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH