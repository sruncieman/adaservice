﻿
CREATE PROCEDURE [Sync].[uspGetChangeTrackingFromToVersionsAndLog]
(
 @DatabaseId		TINYINT
,@RunId				INT = NULL
,@ChangeVersionFrom INT OUTPUT
,@ChangeVersionTo	INT OUTPUT
)
AS
SET NOCOUNT ON

DECLARE  @DatabaseName VARCHAR(50) = DB_NAME(@DatabaseId)
		,@Now DATETIME = GETDATE() 

DECLARE @SQL NVARCHAR(MAX) =	'
								USE ' + @DatabaseName +'

								SELECT @ChangeTrackingCurrentVersion = CHANGE_TRACKING_CURRENT_VERSION()
								'
		,@ParamDef NVARCHAR(100) = '@ChangeTrackingCurrentVersion NVARCHAR(20) OUTPUT'

EXECUTE sp_executesql @SQL, @ParamDef, @ChangeTrackingCurrentVersion = @ChangeVersionTo OUTPUT

SELECT @ChangeVersionFrom = ISNULL((SELECT MAX(ChangeVersionTo) FROM Sync.ChangeTrackingDatabase WHERE DatabaseId = @DatabaseId),0)

INSERT INTO Sync.ChangeTrackingDatabase (DatabaseId, [RunId], ChangeVersionFrom, ChangeVersionTo, ProcessStartDateTime)
VALUES (@DatabaseId, @RunId, @ChangeVersionFrom, @ChangeVersionTo, @Now)
