﻿

CREATE PROCEDURE [IBaseM].[uspGetLinkEndNINumberLink]
AS
SET NOCOUNT ON

BEGIN TRY

	SELECT 
		B.Unique_ID Link_ID, 
		A.Unique_ID Entity_ID1, 
		0 Confidence, 
		0 Direction, 
		NI_Number_ID Entity_ID2, 
		1882  EntityType_ID1, 
		1969  EntityType_ID2, 
		2746  LinkType_ID, 
		0 Record_Status, 
		1 Record_Type, 
		'' SCC
	FROM IBaseM8Cur.NI_Number A
	INNER JOIN 
	IBaseM8Cur.NI_Number_Link B
	ON A.NI_Number_ID = NI_Number_Link_ID
	UNION ALL
	SELECT 
		B.Unique_ID Link_ID, 
		NI_Number_ID Entity_ID1, 
		0 Confidence, 
		0 Direction, 
		A.Unique_ID Entity_ID2, 
		1969  EntityType_ID1, 
		1882  EntityType_ID2, 
		2746  LinkType_ID, 
		0 Record_Status, 
		2 Record_Type, 
		'' SCC
	FROM IBaseM8Cur.NI_Number A
	INNER JOIN 
	IBaseM8Cur.NI_Number_Link B
	ON A.NI_Number_ID = NI_Number_Link_ID
END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

