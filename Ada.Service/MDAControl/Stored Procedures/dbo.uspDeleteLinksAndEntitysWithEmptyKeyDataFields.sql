﻿CREATE PROCEDURE [Sync].[uspDeleteLinksAndEntitysWithEmptyKeyDataFields]	
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspDeleteLinksAndEntitysWithEmptyKeyDataFields]	
--
-- Description:		When an entity hase been cleansed based upon the rules within MDA.dbo.RiskWordFilter
--					there may be instances where he entity key value, i.e. passport is empty or null. This SP
--					removes entitys and links for these records.
--
-- Steps:			1)DeclareVariablesTempObjects
--					2)IdentifyErroneousDataAndDeleteFor:BankAccount
--					3)IdentifyErroneousDataAndDeleteFor:BBPin
--					4)IdentifyErroneousDataAndDeleteFor:DrivingLicense
--					5)IdentifyErroneousDataAndDeleteFor:Email
--					6)IdentifyErroneousDataAndDeleteFor:IPAddress
--					7)IdentifyErroneousDataAndDeleteFor:NINumber
--					8)IdentifyErroneousDataAndDeleteFor:Passport
--					9)IdentifyErroneousDataAndDeleteFor:PaymentCard
--					10)IdentifyErroneousDataAndDeleteFor:Telephone
--					11)IdentifyErroneousDataAndDeleteFor:WebSite				
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-11-24			Paul Allen		Created based on TFSItem:10065
/**************************************************************************************************/
AS
SET NOCOUNT ON

----------------------------------------------------
--1)DeclareVariablesTempObjects
----------------------------------------------------
IF OBJECT_ID('tempdb.dbo.#Ids') IS NOT NULL
	DROP TABLE #Ids
CREATE TABLE #Ids (Id INT)

-------------------------------------
--2)IdentifyErroneousDataAndDeleteFor:BankAccount
-------------------------------------
INSERT INTO #Ids (Id)
SELECT Id
FROM [$(MDA)].[dbo].[BankAccount] 
WHERE NULLIF([AccountNumber],'') IS NULL

DELETE D
FROM [$(MDA)].[dbo].[Person2BankAccount] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.BankAccount_Id)

DELETE D
FROM [$(MDA)].[dbo].[Organisation2BankAccount] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.BankAccount_Id)

DELETE D
FROM [$(MDA)].[dbo].[Policy2BankAccount] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.BankAccount_Id)

DELETE D
FROM [$(MDA)].[dbo].[BankAccount] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Id)

-------------------------------------
--3)IdentifyErroneousDataAndDeleteFor:BBPin
-------------------------------------
TRUNCATE TABLE #Ids
INSERT INTO #Ids (Id)
SELECT Id
FROM [$(MDA)].[dbo].[BBPin] WHERE NULLIF([BBPin],'') IS NULL

DELETE D
FROM [$(MDA)].[dbo].[Person2BBPin] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.BBPin_Id)

DELETE D
FROM [$(MDA)].[dbo].[BBPin] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Id)

-------------------------------------
--4)IdentifyErroneousDataAndDeleteFor:DrivingLicense
-------------------------------------
TRUNCATE TABLE #Ids
INSERT INTO #Ids (Id)
SELECT Id
FROM [$(MDA)].[dbo].[DrivingLicense] 
WHERE NULLIF([DriverNumber],'') IS NULL

DELETE D
FROM [$(MDA)].[dbo].[Person2DrivingLicense] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.DrivingLicense_Id)
 
DELETE D
FROM [$(MDA)].[dbo].[DrivingLicense] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Id)

-------------------------------------
--5)IdentifyErroneousDataAndDeleteFor:Email
-------------------------------------
TRUNCATE TABLE #Ids
INSERT INTO #Ids (Id)
SELECT Id
FROM [$(MDA)].[dbo].[Email] 
WHERE NULLIF([EmailAddress],'') IS NULL

DELETE D
FROM [$(MDA)].[dbo].[Person2Email] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Email_Id)

DELETE D
FROM [$(MDA)].[dbo].[Organisation2Email] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Email_Id)

DELETE D
FROM [$(MDA)].[dbo].[Email] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Id)

-------------------------------------
--6)IdentifyErroneousDataAndDeleteFor:IPAddress
-------------------------------------
TRUNCATE TABLE #Ids
INSERT INTO #Ids (Id)
SELECT Id
FROM [$(MDA)].[dbo].[IPAddress] 
WHERE NULLIF([IPAddress],'') IS NULL

DELETE D
FROM [$(MDA)].[dbo].[Person2IPAddress] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.IPAddress_Id)

DELETE D
FROM [$(MDA)].[dbo].[Organisation2IPAddress] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.IPAddress_Id)

DELETE D
FROM [$(MDA)].[dbo].[IPAddress] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Id)

-------------------------------------
--7)IdentifyErroneousDataAndDeleteFor:NINumber
-------------------------------------
TRUNCATE TABLE #Ids
INSERT INTO #Ids (Id)
SELECT Id
FROM [$(MDA)].[dbo].[NINumber] 
WHERE NULLIF([NINumber],'') IS NULL

DELETE D
FROM [$(MDA)].[dbo].[Person2NINumber] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.NINumber_Id)

DELETE D
FROM [$(MDA)].[dbo].[NINumber] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Id)

-------------------------------------
--8)IdentifyErroneousDataAndDeleteFor:Passport
-------------------------------------
TRUNCATE TABLE #Ids
INSERT INTO #Ids (Id)
SELECT Id
FROM [$(MDA)].[dbo].[Passport] 
WHERE NULLIF([PassportNumber],'') IS NULL

DELETE D
FROM [$(MDA)].[dbo].[Person2Passport] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Passport_Id)

DELETE D
FROM [$(MDA)].[dbo].[Passport] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Id)

-------------------------------------
--9)IdentifyErroneousDataAndDeleteFor:PaymentCard
-------------------------------------
TRUNCATE TABLE #Ids
INSERT INTO #Ids (Id)
SELECT Id
FROM [$(MDA)].[dbo].[PaymentCard] 
WHERE NULLIF([PaymentCardNumber],'') IS NULL

DELETE D
FROM [$(MDA)].[dbo].[Person2PaymentCard] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.PaymentCard_Id)

DELETE D
FROM [$(MDA)].[dbo].[Organisation2PaymentCard] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.PaymentCard_Id)

DELETE D
FROM [$(MDA)].[dbo].[Policy2PaymentCard] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.PaymentCard_Id)

DELETE D
FROM [$(MDA)].[dbo].[PaymentCard] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Id)

-------------------------------------
--10)IdentifyErroneousDataAndDeleteFor:Telephone
-------------------------------------
TRUNCATE TABLE #Ids
INSERT INTO #Ids (Id)
SELECT Id
FROM [$(MDA)].[dbo].[Telephone] 
WHERE NULLIF([TelephoneNumber],'') IS NULL

DELETE D
FROM [$(MDA)].[dbo].[Handset2Telephone] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Telephone_Id)

DELETE D
FROM [$(MDA)].[dbo].[Person2Telephone] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Telephone_Id)

DELETE D
FROM [$(MDA)].[dbo].[Organisation2Telephone] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Telephone_Id)

DELETE D
FROM [$(MDA)].[dbo].[Address2Telephone] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Telephone_Id)

DELETE D
FROM [$(MDA)].[dbo].[Telephone] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Id)

-------------------------------------
--11)IdentifyErroneousDataAndDeleteFor:WebSite
-------------------------------------
TRUNCATE TABLE #Ids
INSERT INTO #Ids (Id)
SELECT Id
FROM [$(MDA)].[dbo].[WebSite] 
WHERE NULLIF([URL],'') IS NULL

DELETE D
FROM [$(MDA)].[dbo].[Person2WebSite] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Website_Id)

DELETE D
FROM [$(MDA)].[dbo].[Organisation2WebSite] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Website_Id)

DELETE D
FROM [$(MDA)].[dbo].[WebSite] D
WHERE EXISTS (SELECT TOP 1 1 FROM #Ids IDS WHERE IDS.Id = D.Id)

