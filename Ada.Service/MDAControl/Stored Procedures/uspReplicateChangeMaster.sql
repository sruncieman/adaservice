﻿
CREATE PROCEDURE [Rep].[uspReplicateChangeMaster]
(
 @Logging				BIT = 0	--DefaultIsOff
,@Debug					BIT = 0 --DefaultIsOff
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@OutputRunID		INT
				,@OutputProcessID	INT
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@RunNameID			INT = 1
		
		EXECUTE dbo.uspCRUDDBControlRun
			 @RunNameID		= @RunNameID
			,@OutputRunID	= @OutputRunID OUTPUT
			
		SELECT @Details = 'User=['+SYSTEM_USER+']'				
		EXECUTE dbo.uspCRUDDBControlProcess
					 @RunID				= @OutputRunID
					,@ProcessNameID		= 23 --Replicate
					,@Details			= @Details
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@OutputProcessID	= @OutputProcessID OUTPUT
						
		EXECUTE dbo.uspCRUDDBControlTask
					 @ProcessID			= @OutputProcessID
					,@TaskNameID		= 24 --ReplicateWrapper
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	------------------------------------------------------------
	--2)ReplicateTheData
	------------------------------------------------------------
	BEGIN TRANSACTION
		-----------------------------------------
		--2.1)Entity
		-----------------------------------------
		--2.1.1)VehicleEntity
		EXECUTE Rep.uspReplicateEntityVehicle 
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
					
		--2.1.2)WebSiteEntity			
		EXECUTE Rep.uspReplicateEntityWebsite 
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
					
		--2.1.3)NINumberEntity 			
		EXECUTE Rep.uspReplicateEntityNINumber 
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.4)AccountEntity 			
		EXECUTE Rep.uspReplicateEntityAccount 
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.5)AddressEntity 			
		EXECUTE Rep.uspReplicateEntityAddress
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.6)BBPinEntity 			
		EXECUTE Rep.uspReplicateEntityBBPin
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.7)DrivingLicenceEntity 			
		EXECUTE Rep.uspReplicateEntityDrivingLicence
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.8)EmailEntity 			
		EXECUTE Rep.uspReplicateEntityEmail
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.9)FraudRingEntity 			
		EXECUTE Rep.uspReplicateEntityFraudRing
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.10)IncidentEntity		
		EXECUTE Rep.uspReplicateEntityIncident
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.11)IPAddressEntity 	
		EXECUTE Rep.uspReplicateEntityIPAddress
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.12)KeoghsCaseEntity 		
		EXECUTE Rep.uspReplicateEntityKeoghsCase
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.13)OrganisationEntity 		
		EXECUTE Rep.uspReplicateEntityOrganisation
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.14)PassportEntity 			
		EXECUTE Rep.uspReplicateEntityPassport
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.15)PaymentCardEntity 			
		EXECUTE Rep.uspReplicateEntityPaymentCard
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.16)PersonEntity 			
		EXECUTE Rep.uspReplicateEntityPerson
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.17)PolicyEntity			
		EXECUTE Rep.uspReplicateEntityPolicy
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.18)TelephoneEntity			
		EXECUTE Rep.uspReplicateEntityTelephone
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.19)TOGEntity 			
		EXECUTE Rep.uspReplicateEntityTOG
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.1.20)IntelligenceEntity			
		EXECUTE Rep.uspReplicateEntityIntelligence
					@Logging = @Logging, @ParentTaskID = @OutputTaskID

		-----------------------------------------
		--2.2)Link
		-----------------------------------------
		--2.2.1)AccountLink
		EXECUTE Rep.uspReplicateLinkAccount
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.2)AddressLink			
		EXECUTE Rep.uspReplicateLinkAddressToAddress
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.3)BBPinLink			
		EXECUTE Rep.uspReplicateLinkBBPin
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.4)CaseToIncidentLink			
		EXECUTE Rep.uspReplicateLinkCaseToIncident
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.5)DrivingLicenceLink			
		EXECUTE Rep.uspReplicateLinkDrivingLicence
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.6)EmailLink			
		EXECUTE Rep.uspReplicateLinkEmail
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.7)FraudRingIncidentLink			
		EXECUTE Rep.uspReplicateLinkFraudRingIncident
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.8)IncidentLink			
		EXECUTE Rep.uspReplicateLinkIncident
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.9)IncidentMatchLink			
		EXECUTE Rep.uspReplicateLinkIncidentMatch
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.10)LinkIPAddressLink		
		EXECUTE Rep.uspReplicateLinkIPAddress
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.11)OrganisationMatchLink		
		EXECUTE Rep.uspReplicateLinkOrganisationMatch
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.12)LinkOutcomeLink		
		EXECUTE Rep.uspReplicateLinkOutcome
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.13)PassportLink		
		EXECUTE Rep.uspReplicateLinkPassport
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.14)PaymentCardLink		
		EXECUTE Rep.uspReplicateLinkPaymentCard
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.15)PersonToOrgLink		
		EXECUTE Rep.uspReplicateLinkPersonToOrg
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.16)PersonToPersonLink		
		EXECUTE Rep.uspReplicateLinkPersonToPerson
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.17)PolicyLink			
		EXECUTE Rep.uspReplicateLinkPolicy
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.18)PolicyToPaymentCardLink			
		EXECUTE Rep.uspReplicateLinkPolicyToPaymentCard
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.19)TelephoneLink			
		EXECUTE Rep.uspReplicateLinkTelephone
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.20)TOGLink			
		EXECUTE Rep.uspReplicateLinkTOG
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.21)VehicleLink			
		EXECUTE Rep.uspReplicateLinkVehicle
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.22)VehicleToIncidentLink		
		EXECUTE Rep.uspReplicateLinkVehicleToIncident
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.23)VehicleToVehicleLink			
		EXECUTE Rep.uspReplicateLinkVehicleToVehicle
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.24)WebsiteLink			
		EXECUTE Rep.uspReplicateLinkWebsite
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.25)NINumberLink			
		EXECUTE Rep.uspReplicateLinkNINumber
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		--2.2.26)IntelligenceLink			
		EXECUTE Rep.uspReplicateLinkIntelligence
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
		-----------------------------------------
		--2.3)Link
		-----------------------------------------
		--2.3.1)LinkEnd
		EXECUTE Rep.uspReplicateLinkEnd
					@Logging = @Logging, @ParentTaskID = @OutputTaskID
		
	COMMIT TRANSACTION
	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	-----------------------------------------------------------
	IF @Logging = 1
		BEGIN
			SELECT @Now = GETDATE()
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action		= 'U'
						,@TaskID		= @OutputTaskID
						,@ProcessID		= @OutputProcessID
						,@TaskEndTime	= @Now
						,@OutputTaskID	= @OutputTaskID OUTPUT
						
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@OutputProcessID	= @OutputProcessID OUTPUT
		END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK

	DECLARE		 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity		INT				= ERROR_SEVERITY()
				,@ErrorState		INT				= ERROR_STATE()
				,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
				,@ErrorLine			INT				= ERROR_LINE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'

	IF @Logging = 1
		BEGIN
			SELECT   @Now = GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@ProcessID			= @OutputProcessID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
			
			SELECT @ErrorMessage +=' [TaskID='+CAST(ISNULL(@OutputTaskID,-1) AS VARCHAR)+']'		
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputProcessID	= @OutputProcessID OUTPUT
						
			EXECUTE dbo.uspCRUDDBControlRun
						 @Action			= 'U'
						,@RunID				= @OutputRunID
						,@RunEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputRunID		= @OutputRunID OUTPUT
		END
END CATCH



