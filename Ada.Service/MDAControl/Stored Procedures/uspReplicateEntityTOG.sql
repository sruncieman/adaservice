﻿









CREATE PROCEDURE [Rep].[uspReplicateEntityTOG]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateEntityTOG]	
--
-- Description:		Replicate Entity TOG	
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLogging/Tidy						
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-03-03			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.TOG_')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 47 --ReplicateTOGEntity
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	UPDATE [$(MDA_IBase)].[dbo].TOG_
	SET Record_Status = 254
	OUTPUT	 @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Allocated_to,'') + '}|I:{' + ISNULL(Inserted.Allocated_to,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_Allocated,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_Allocated,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_Completed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_Completed,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_of_Review,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_of_Review,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Decision_,'') + '}|I:{' + ISNULL(Inserted.Decision_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.Referred_by,'') + '}|I:{' + ISNULL(Inserted.Referred_by,'') + '}'
			,'D:{' + ISNULL(Deleted.Report_,'') + '}|I:{' + ISNULL(Inserted.Report_,'') + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.TOG_ID,'') + '}|I:{' + ISNULL(Inserted.TOG_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.TOG_Reference,'') + '}|I:{' + ISNULL(Inserted.TOG_Reference,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_TOG_] (TaskID, DMLAction, Unique_ID, Allocated_to, AltEntity, Create_Date, Create_User, Date_Allocated, Date_Completed, Date_of_Review, Decision_, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, Referred_by, Report_, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, TOG_ID, TOG_Reference, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.EntityRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo].TOG_ a ON ent.IBase8EntityID = a.Unique_ID
	WHERE RecordAction ='D'

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo].TOG_ [Target]
	USING	(
			SELECT   DISTINCT C.IBase8EntityID
					,C.IBase5EntityID
					,A.*
			FROM IBaseM8Cur.TOG_ A
			INNER JOIN Rep.EntityRecord C ON A.Unique_ID = C.IBase8EntityID_Staging	
			) AS Source ON [Target].Unique_ID = Source.IBase8EntityID
	WHEN MATCHED THEN UPDATE
	SET  TOG_Reference					= Source.TOG_Reference
		,Referred_by					= Source.Referred_by
		,Date_of_Review					= Source.Date_of_Review
		,Decision_						= Source.Decision_
		,Allocated_to					= Source.Allocated_to
		,Date_Allocated					= Source.Date_Allocated
		,Date_Completed					= Source.Date_Completed
		,Report_						= Source.Report_
		,Key_Attractor_412284410		= Source.Key_Attractor_412284410
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User
		,Record_Status					= Source.Record_Status
		,[AltEntity]					= Source.[AltEntity]
		,[Do_Not_Disseminate_412284494]	= Source.[Do_Not_Disseminate_412284494]
		,[IconColour]					= Source.[IconColour]
		,[SCC]							= Source.[SCC]
		,[Status_Binding]				= Source.[Status_Binding]
		,[TOG_ID]						= Source.[TOG_ID]
		,[x5x5x5_Grading_412284402]		= Source.[x5x5x5_Grading_412284402]
		,[Risk_Claim_ID_414244883]		= Source.[Risk_Claim_ID_414244883]
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,Allocated_to
		,AltEntity
		,Create_Date
		,Create_User
		,Date_Allocated
		,Date_Completed
		,Date_of_Review
		,Decision_
		,Do_Not_Disseminate_412284494
		,IconColour
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,Record_Status
		,Referred_by
		,Report_
		,SCC
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding
		,TOG_ID
		,TOG_Reference
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883
		)
	VALUES
		(
		 Source.IBase8EntityID
		,Source.Allocated_to
		,Source.AltEntity
		,Source.Create_Date
		,Source.Create_User
		,Source.Date_Allocated
		,Source.Date_Completed
		,Source.Date_of_Review
		,Source.Decision_
		,Source.Do_Not_Disseminate_412284494
		,Source.IconColour
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Record_Status
		,Source.Referred_by
		,Source.Report_
		,Source.SCC
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.TOG_ID
		,Source.TOG_Reference
		,Source.x5x5x5_Grading_412284402
		,Source.Risk_Claim_ID_414244883		
		)
	OUTPUT	 @OutputTaskID
			,$ACTION
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Allocated_to,'') + '}|I:{' + ISNULL(Inserted.Allocated_to,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_Allocated,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_Allocated,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_Completed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_Completed,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_of_Review,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_of_Review,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Decision_,'') + '}|I:{' + ISNULL(Inserted.Decision_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.Referred_by,'') + '}|I:{' + ISNULL(Inserted.Referred_by,'') + '}'
			,'D:{' + ISNULL(Deleted.Report_,'') + '}|I:{' + ISNULL(Inserted.Report_,'') + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.TOG_ID,'') + '}|I:{' + ISNULL(Inserted.TOG_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.TOG_Reference,'') + '}|I:{' + ISNULL(Inserted.TOG_Reference,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_TOG_] (TaskID, DMLAction, Unique_ID, Allocated_to, AltEntity, Create_Date, Create_User, Date_Allocated, Date_Completed, Date_of_Review, Decision_, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, Referred_by, Report_, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, TOG_ID, TOG_Reference, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);


	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
			SELECT @InsertRowCount		= (SELECT COUNT(1) FROM dbo.DBControlTaskDetail WHERE TaskID = @OutputTaskID AND ActionID = 'INSERT')
			SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM dbo.DBControlTaskDetail WHERE TaskID = @OutputTaskID AND ActionID = 'UPDATE')
			SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM dbo.DBControlTaskDetail WHERE TaskID = @OutputTaskID AND ActionID = 'DELETE')
		
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
END CATCH



