﻿
CREATE PROCEDURE  [Sync].[uspSynchroniseEntityHandset]
/**************************************************************************************************/
-- ObjectName:		[Sync].[[uspSynchroniseEntityHandset]]
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)ActionTheChangeInMDA_IBase
--						3.3)DoDeletes
--						3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)	
--						4.1)ActionTheChangeInMDA
--						4.2)DoDeletes
--						4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					5)UpdateLoggingAndTidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2015-06-09			Paul Allen		Created
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT			

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1), ChangeDatabase VARCHAR(10) ,MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)
					
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 132 --SynchroniseHandsetEntity
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, MDA_ID, DMLAction)
	SELECT 'MDA', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA].[dbo].[Handset], @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA_IBase].[dbo].[Handset_], @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	--2.2)AddSomeIndexesToMakeTheProcessQuicker
	SET @Step ='2.2'
	 
	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT(MDA_IBase_Unique_ID ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3'

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IV.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[Handset_] IV ON IV.Unique_ID = CT.MDA_IBase_Unique_ID
	AND IV.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MV.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].[Handset] MV ON MV.ID = CT.MDA_ID

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4'

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5'

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1'

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM [MDA_IBase].[dbo].[_Handset__NextID]
		UPDATE [MDA_IBase].[dbo].[_Handset__NextID] SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	'HAN' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY MDA_ID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--3.2)ActionTheChangeInMDA_IBase
		SET @Step ='3.2'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[Handset_] [Target]
		USING	(
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, MH.*, MHC.[Colour]
				FROM #CT CT
				INNER JOIN [MDA].[dbo].[Handset] MH ON MH.ID = CT.MDA_ID
				INNER JOIN [MDA].[dbo].[HandsetColour] MHC ON MHC.ID = MH.[HandsetColour_Id]
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET  Create_User					= Source.[CreatedBy]
			,Create_Date					= Source.[CreatedDate]
			,Last_Upd_User					= Source.[ModifiedBy]
			,Last_Upd_Date					= Source.[ModifiedDate]
			,Record_Status					= Source.[RecordStatus]
			,AltEntity						= NULL
			,Source_Reference_411765487		= Source.[SourceReference]
			,Source_Description_411765489	= Source.[SourceDescription]
			,Source_411765484				= Source.[Source]
			,Key_Attractor_412284410		= Source.[KeyAttractor]
			,Handset_Colour					= CASE WHEN Source.[HandsetColour_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN Handset_Colour ELSE NULLIF(Source.[Colour],'Unknown') END
			,Handset_Make					= Source.[HandsetMake]
			,Handset_Model					= Source.[HandsetModel]
			,Handset_IMEI					= Source.[HandsetIMEI]
			,Handset_Value					= Source.[HandsetValue]
		WHEN NOT MATCHED THEN INSERT
			(
			 Unique_ID
			,Create_User
			,Create_Date
			,Last_Upd_User
			,Last_Upd_Date
			,Record_Status
			,AltEntity
			,Source_411765484
			,Source_Reference_411765487
			,Source_Description_411765489
			,Key_Attractor_412284410
			,MDA_Incident_ID_412284502
			,Handset_Colour
			,Handset_Make
			,Handset_Model
			,Handset_IMEI
			,Handset_Value
			)
		VALUES
			(
			 Source.MDA_IBase_Unique_ID
			,Source.[CreatedBy]
			,Source.[CreatedDate]
			,Source.[ModifiedBy]
			,Source.[ModifiedDate]
			,Source.[RecordStatus]
			,NULL
			,Source.[Source]
			,Source.[SourceReference]
			,Source.[SourceDescription]
			,Source.[KeyAttractor]
			,[Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Handset')
			,CASE WHEN Source.[HandsetColour_Id] = 0 THEN NULL ELSE Source.[Colour] END
			,Source.[HandsetMake]
			,Source.[HandsetModel]
			,Source.[HandsetIMEI]
			,Source.[HandsetValue]
			)
		OUTPUT  @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'	
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'			
				,'D:{' + ISNULL(Deleted.Handset_Colour,'') + '}|I:{' + ISNULL(Inserted.Handset_Colour,'') + '}'
				,'D:{' + ISNULL(Deleted.Handset_Make,'') + '}|I:{' + ISNULL(Inserted.Handset_Make,'') + '}'				
				,'D:{' + ISNULL(Deleted.Handset_Model,'') + '}|I:{' + ISNULL(Inserted.Handset_Model,'') + '}'					
				,'D:{' + ISNULL(Deleted.Handset_ID,'') + '}|I:{' + ISNULL(Inserted.Handset_ID,'') + '}'			
				,'D:{' + ISNULL(Deleted.Handset_IMEI,'') + '}|I:{' + ISNULL(Inserted.Handset_IMEI,'') + '}'			
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Handset_Value,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Handset_Value,'')) + '}'
				,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{' + ISNULL(Inserted.Notes_,'') + '}'
		INTO [SyncDML].[MDAIBase_Handset_] (TaskId, DMLAction, Unique_ID, Create_User, Create_Date, Last_Upd_User, Last_Upd_Date, SCC, Record_Status, Status_Binding, AltEntity, IconColour, Source_411765484, Source_Reference_411765487, Source_Description_411765489, x5x5x5_Grading_412284402, Key_Attractor_412284410, Do_Not_Disseminate_412284494, MDA_Incident_ID_412284502, Risk_Claim_ID_414244883, Handset_Colour, Handset_Make, Handset_Model, Handset_ID, Handset_IMEI, Handset_Value, Notes_);

		--3.3)DoDeletes
		SET @Step ='3.3';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[Handset_]
		OUTPUT @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'	
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'			
				,'D:{' + ISNULL(Deleted.Handset_Colour,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Handset_Make,'') + '}|I:{}'				
				,'D:{' + ISNULL(Deleted.Handset_Model,'') + '}|I:{}'					
				,'D:{' + ISNULL(Deleted.Handset_ID,'') + '}|I:{}'			
				,'D:{' + ISNULL(Deleted.Handset_IMEI,'') + '}|I:{}'			
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Handset_Value,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Handset_] (TaskId, DMLAction, Unique_ID, Create_User, Create_Date, Last_Upd_User, Last_Upd_Date, SCC, Record_Status, Status_Binding, AltEntity, IconColour, Source_411765484, Source_Reference_411765487, Source_Description_411765489, x5x5x5_Grading_412284402, Key_Attractor_412284410, Do_Not_Disseminate_412284494, MDA_Incident_ID_412284502, Risk_Claim_ID_414244883, Handset_Colour, Handset_Make, Handset_Model, Handset_ID, Handset_IMEI, Handset_Value, Notes_)
		FROM [MDA_IBase].[dbo].[Handset_] IV
		INNER JOIN #CT CT ON [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Handset') = IV.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';

		--3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='3.4';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MH
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT  @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.[HandsetIMEI],'') + '}|I:{' + ISNULL(Inserted.[HandsetIMEI],'') + '}'
				,'D:{' + ISNULL(Deleted.[HandsetMake],'') + '}|I:{' + ISNULL(Inserted.[HandsetMake],'') + '}'
				,'D:{' + ISNULL(Deleted.[HandsetModel],'') + '}|I:{' + ISNULL(Inserted.[HandsetModel],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[HandsetColour_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[HandsetColour_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[HandsetValue],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[HandsetValue],'')) + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Handset] (TaskId, DMLAction, Id, HandsetIMEI, HandsetMake, HandsetModel, HandsetColour_Id, HandsetValue, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].Handset MH
		INNER JOIN #CT CT ON CT.MDA_ID = MH.Id
 		WHERE MH.IBaseId IS NULL
		OR MH.IBaseId != CT.MDA_IBase_Unique_ID;

	END

	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		SET @Step ='4.1'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].Handset [Target]
		USING  (
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, IH.*, ISNULL(MHC.ID,0) [HandsetColour_Id]
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Handset_] IH ON IH.Unique_ID = CT.MDA_IBase_Unique_ID
				LEFT JOIN [MDA].[dbo].[HandsetColour] MHC ON MHC.[Colour] = IH.[Handset_Colour]
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  HandsetIMEI				= Source.[Handset_IMEI]
			,HandsetMake				= Source.[Handset_Make]
			,HandsetModel				= Source.[Handset_Model]
			,HandsetColour_Id			= Source.[HandsetColour_Id]
			,HandsetValue				= Source.[Handset_Value]
			,KeyAttractor				= Source.[Key_Attractor_412284410]
			,Source						= Source.[Source_411765484]
			,SourceReference			= Source.[Source_Reference_411765487]
			,SourceDescription			= Source.[Source_Reference_411765487]
			,CreatedBy					= Source.[Create_User]
			,CreatedDate				= Source.[Create_Date]
			,ModifiedBy					= Source.[Last_Upd_User]
			,ModifiedDate				= Source.[Last_Upd_Date]
			,IBaseId					= Source.[Unique_ID]
			,RecordStatus				= Source.[Record_Status]
			,ADARecordStatus			= CASE WHEN Source.Record_Status = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
			 HandsetIMEI
			,HandsetMake
			,HandsetModel
			,HandsetColour_Id
			,HandsetValue
			,KeyAttractor
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES
			(
			 Source.[Handset_IMEI]
			,Source.[Handset_Make]
			,Source.[Handset_Model]
			,Source.[HandsetColour_Id]
			,Source.[Handset_Value]
			,Source.[Key_Attractor_412284410]
			,Source.[Source_411765484]
			,Source.[Source_Reference_411765487]
			,Source.[Source_Description_411765489]
			,Source.[Create_User]
			,Source.[Create_Date]
			,Source.[Last_Upd_User]
			,Source.[Last_Upd_Date]
			,Source.[Unique_ID]
			,Source.[Record_Status]
			,10
			)
		OUTPUT   @OutputTaskID
				,$Action
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.[HandsetIMEI],'') + '}|I:{' + ISNULL(Inserted.[HandsetIMEI],'') + '}'
				,'D:{' + ISNULL(Deleted.[HandsetMake],'') + '}|I:{' + ISNULL(Inserted.[HandsetMake],'') + '}'
				,'D:{' + ISNULL(Deleted.[HandsetModel],'') + '}|I:{' + ISNULL(Inserted.[HandsetModel],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[HandsetColour_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[HandsetColour_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[HandsetValue],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[HandsetValue],'')) + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Handset] (TaskId, DMLAction, Id, HandsetIMEI, HandsetMake, HandsetModel, HandsetColour_Id, HandsetValue, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus);

		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Handset]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.[HandsetIMEI],'') + '}|I:{' + ISNULL(Inserted.[HandsetIMEI],'') + '}'
				,'D:{' + ISNULL(Deleted.[HandsetMake],'') + '}|I:{' + ISNULL(Inserted.[HandsetMake],'') + '}'
				,'D:{' + ISNULL(Deleted.[HandsetModel],'') + '}|I:{' + ISNULL(Inserted.[HandsetModel],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[HandsetColour_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[HandsetColour_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[HandsetValue],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[HandsetValue],'')) + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Handset] (TaskId, DMLAction, Id, HandsetIMEI, HandsetMake, HandsetModel, HandsetColour_Id, HandsetValue, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Handset] MV
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MV.IBaseId
		WHERE CT.DMLAction = 'D';

		--4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='4.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE IV
		SET MDA_Incident_ID_412284502 = [Sync].[fn_ResolveMDAID] (MV.ID,'MDA.dbo.Handset')
		OUTPUT  @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'	
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'			
				,'D:{' + ISNULL(Deleted.Handset_Colour,'') + '}|I:{' + ISNULL(Inserted.Handset_Colour,'') + '}'
				,'D:{' + ISNULL(Deleted.Handset_Make,'') + '}|I:{' + ISNULL(Inserted.Handset_Make,'') + '}'				
				,'D:{' + ISNULL(Deleted.Handset_Model,'') + '}|I:{' + ISNULL(Inserted.Handset_Model,'') + '}'					
				,'D:{' + ISNULL(Deleted.Handset_ID,'') + '}|I:{' + ISNULL(Inserted.Handset_ID,'') + '}'			
				,'D:{' + ISNULL(Deleted.Handset_IMEI,'') + '}|I:{' + ISNULL(Inserted.Handset_IMEI,'') + '}'			
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Handset_Value,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Handset_Value,'')) + '}'
				,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{' + ISNULL(Inserted.Notes_,'') + '}'
		INTO [SyncDML].[MDAIBase_Handset_] (TaskId, DMLAction, Unique_ID, Create_User, Create_Date, Last_Upd_User, Last_Upd_Date, SCC, Record_Status, Status_Binding, AltEntity, IconColour, Source_411765484, Source_Reference_411765487, Source_Description_411765489, x5x5x5_Grading_412284402, Key_Attractor_412284410, Do_Not_Disseminate_412284494, MDA_Incident_ID_412284502, Risk_Claim_ID_414244883, Handset_Colour, Handset_Make, Handset_Model, Handset_ID, Handset_IMEI, Handset_Value, Notes_)
		FROM [MDA_IBase].[dbo].[Handset_] IV
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IV.Unique_ID
		INNER JOIN [MDA].[dbo].[Handset] MV ON MV.IBaseId = IV.Unique_ID
 		WHERE IV.MDA_Incident_ID_412284502 IS NULL
		OR [Sync].[fn_ResolveMDAID] (MV.ID,'MDA.dbo.Handset') != IV.MDA_Incident_ID_412284502;

	END

	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Handset] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Handset] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Handset] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Handset_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Handset_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Handset_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

END TRY
BEGIN CATCH

	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +@Step +']'

	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH