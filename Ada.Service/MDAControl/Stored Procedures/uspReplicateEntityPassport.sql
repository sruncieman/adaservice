﻿CREATE PROCEDURE [Rep].[uspReplicateEntityPassport]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateEntityPassport]	
--
-- Description:		Replicate Entity Passport	
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLogging/Tidy						
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
--		2016-01-20			Paul Allen		Changed logic of updating Link End table for Conflict records
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Passport_')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 41 --ReplicatePassportEntity
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------	
	/*ItsIsARequirementToNeverDeleteDueToDedupe
	UPDATE [$(MDA_IBase)].[dbo].Passport_
	SET Record_Status = 254
	OUTPUT	 @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Passport_ID,'') + '}|I:{' + ISNULL(Inserted.Passport_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Passport_Number,'') + '}|I:{' + ISNULL(Inserted.Passport_Number,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Passport_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Passport_ID, Passport_Number, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.EntityRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo].Passport_ a on ent.IBase8EntityID = a.Unique_ID
	WHERE RecordAction ='D'
	*/

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	--ThereIsARequirementNotToUpdateThePassportIfThisChangesButToInsertSetOrginalrecordtoDeleteAndRepointLinks
	DECLARE @Conflicts TABLE (RowID INT IDENTITY(1,1), IBase5 VARCHAR(20) , IBase8ID VARCHAR(20), OrgData VARCHAR(255), NewData VARCHAR(255), NewIB8ID VARCHAR(20))
	INSERT INTO @Conflicts (Ibase5, IBase8ID, OrgData, NewData)
	SELECT	 ME.IBase5EntityID
			,ME.IBase8EntityID
			,LN.Passport_Number
			,Pass.Passport_Number
	FROM Rep.EntityRecord ME
	INNER JOIN IBaseM8Cur.Passport_ Pass ON ME.IBase8EntityID_Staging = Pass.Unique_ID 
	INNER JOIN [$(MDA_IBase)].[dbo].Passport_ LN ON ME.IBase8EntityID = LN.Unique_ID
	WHERE ISNULL(LN.Passport_Number,'') <> ISNULL(Pass.Passport_Number,'')
	
	--PerfomMergeForNonConflictRecords
	MERGE [$(MDA_IBase)].[dbo].Passport_ [Target]
	USING	(
			SELECT  DISTINCT c.IBase8EntityID
					,C.IBase5EntityID
					,A.*
			FROM IBaseM8Cur.Passport_ A
			INNER JOIN Rep.EntityRecord C ON A.Unique_ID = C.IBase8EntityID_Staging
			LEFT JOIN @Conflicts CF ON C.IBase5EntityID = CF.IBase5
			WHERE CF.IBase5 IS NULL
			) AS Source ON [Target].Unique_ID = Source.IBase8EntityID
	WHEN MATCHED THEN UPDATE
	SET  Passport_Number				= Source.Passport_Number
		,Key_Attractor_412284410		= Source.Key_Attractor_412284410
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User
		,Record_Status					= Source.Record_Status
		,[AltEntity]					= Source.[AltEntity]
		,[Do_Not_Disseminate_412284494]	= Source.[Do_Not_Disseminate_412284494]
		,[IconColour]					= Source.[IconColour]
		,[Passport_ID]					= Source.[Passport_ID]
		,[SCC]							= Source.[SCC]
		,[Status_Binding]				= Source.[Status_Binding]
		,[x5x5x5_Grading_412284402]		= Source.[x5x5x5_Grading_412284402]
		,[Risk_Claim_ID_414244883]		= Source.[Risk_Claim_ID_414244883]
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,AltEntity
		,Create_Date
		,Create_User
		,Do_Not_Disseminate_412284494
		,IconColour
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,Passport_ID
		,Passport_Number
		,Record_Status
		,SCC
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883
		)
	VALUES
		(
		 Source.IBase8EntityID
		,Source.AltEntity
		,Source.Create_Date
		,Source.Create_User
		,Source.Do_Not_Disseminate_412284494
		,Source.IconColour
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Passport_ID
		,Source.Passport_Number
		,Source.Record_Status
		,Source.SCC
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.x5x5x5_Grading_412284402
		,Source.Risk_Claim_ID_414244883
		)
	OUTPUT	 @OutputTaskID
			,$ACTION
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Passport_ID,'') + '}|I:{' + ISNULL(Inserted.Passport_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Passport_Number,'') + '}|I:{' + ISNULL(Inserted.Passport_Number,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Passport_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Passport_ID, Passport_Number, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

	--HandleTheConflictsIfTheWebsiteNameHasChanged
	DECLARE @NextID INT , @RowCount INT
	SELECT @RowCount = COUNT(1) FROM @Conflicts
	
	IF @RowCount > 0
	BEGIN
		SELECT @NextID = NextID FROM [$(MDA_IBase)].[dbo].[_Passport__NextID]
		UPDATE [$(MDA_IBase)].[dbo]._Passport__NextID SET NextID = @NextID + @Rowcount 
	
		UPDATE U
		SET NewIB8ID = New_Unique_ID
		FROM	(
				SELECT  'PAS' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY RowID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM @Conflicts
				WHERE IBase5 IS NOT NULL) A
		INNER JOIN @Conflicts U ON A.RowID = U.RowID
			
		--Insert updates to Passport with newly generated ID	
		INSERT INTO [$(MDA_IBase)].[dbo].Passport_
				(
				 Unique_ID
				,AltEntity
				,Create_Date
				,Create_User
				,Do_Not_Disseminate_412284494
				,IconColour
				,Key_Attractor_412284410
				,Last_Upd_Date
				,Last_Upd_User
				,Passport_ID
				,Passport_Number
				,Record_Status
				,SCC
				,Source_411765484
				,Source_Description_411765489
				,Source_Reference_411765487
				,Status_Binding
				,x5x5x5_Grading_412284402
				,Risk_Claim_ID_414244883
				)
		OUTPUT	 @OutputTaskID
				,'INSERT'
				,'D:{}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Passport_ID,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Passport_Number,'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Passport_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Passport_ID, Passport_Number, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		SELECT	 U.NewIB8ID
				,AltEntity
				,Create_Date
				,Create_User
				,Do_Not_Disseminate_412284494
				,IconColour
				,Key_Attractor_412284410
				,Last_Upd_Date
				,Last_Upd_User
				,Passport_ID
				,Passport_Number
				,Record_Status
				,SCC
				,Source_411765484
				,Source_Description_411765489
				,Source_Reference_411765487
				,Status_Binding
				,x5x5x5_Grading_412284402
				,Risk_Claim_ID_414244883
		FROM @Conflicts U
		INNER JOIN IBaseM8Cur.Passport_ NI ON U.Ibase5 = NI.passport_ID AND NI.Passport_Number = U.NewData

		--RePointTheLinkEndToTheNewID	
		UPDATE LE
		SET  LE.Entity_ID1 = CASE WHEN LE.Entity_ID1 = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE LE.Entity_ID1 END
			,LE.Entity_ID2 = CASE WHEN LE.Entity_ID2 = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE LE.Entity_ID2 END
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Link_ID],'') + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID1],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Confidence],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Direction],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID2],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID1],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID2],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkType_ID],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Type],'')) + '}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{' + ISNULL(Inserted.[SCC],'') + '}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [$(MDA_IBase)].[dbo]._LinkEnd LE
		INNER JOIN (			
					SELECT LE.LinkID, C.IBase8ID, C.NewIB8ID
					FROM @Conflicts C
					INNER JOIN [Rep].[IBaseLinkEnd] LE on c.IBase8ID = LE.EntityID1 
					) LinkID ON LE.Link_ID = LinkID.LinkID

		--RemoveTheLinkFromRep.LinkEndRecord
		DELETE LER
		FROM [Rep].[LinkEndRecord] LER
		INNER JOIN (			
					SELECT LE.LinkID, C.IBase8ID, C.NewIB8ID
					FROM @Conflicts C
					INNER JOIN [Rep].[IBaseLinkEnd] LE on c.IBase8ID = LE.EntityID1 
					) LinkID ON LER.[IBase8LinkID] = LinkID.LinkID

		--AddedToPreventAdditionalRecordBeingInsertedIntoLinkEnd
		UPDATE RLER
		SET  [EntityID1] = CASE WHEN RLER.[EntityID1] = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE RLER.[EntityID1] END
			,[EntityID2] = CASE WHEN RLER.[EntityID2] = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE RLER.[EntityID2] END
		FROM [Rep].[IBaseLinkEnd] RLER
		INNER JOIN (			
					SELECT LE.Link_ID, C.IBase8ID, C.NewIB8ID
					FROM @conflicts C
					INNER JOIN [$(MDA_IBase)].[dbo]._LinkEnd LE on c.NewIB8ID = LE.Entity_ID1 
					) LinkID ON RLER.[LinkID] = LinkID.Link_ID
	END

	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Passport_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
		SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Passport_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
		SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Passport_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
		SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
		SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
		SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END	
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH