﻿

CREATE PROCEDURE [Rep].[uspPopulateHeadsOfClaim]
----------------------------------------------------------------------------------------------------
--Name:			[IBaseM].[uspPopulateHeadsOfClaim]
--
--Description:	Populates HOC data
--
--Revision History
--		Paul Allen		2014-02-10		Added Distinct to synamic select to prevent duplicates
----------------------------------------------------------------------------------------------------
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY

	IF (OBJECT_ID('tempdb..#_DATA') is null)
		CREATE TABLE #_DATA(ID NVARCHAR(50), Value NVARCHAR(500))

	DECLARE @SQL NVARCHAR(3000)

	UPDATE [$(MDA_IBase)].[dbo].Incident_Link
		SET Accident_Management = NULL, Engineer_ = NULL, Hire_ = NULL, Medical_Examiner = NULL,
			Recovery_ = NULL, Repairer_ = NULL, Solicitors_ = NULL, Storage_ = NULL, Storage_Address = NULL

	SET @SQL = 
	'
		SELECT DISTINCT I.Unique_ID, O.Organisation_Name + '' ('' +  O.Unique_ID + ''); ''
		FROM  [MDA_IBase].[dbo].Incident_Link I
			INNER JOIN [MDA_IBase].[dbo]._LinkEnd EL  ON I.Unique_ID = EL.Link_ID AND LEFT(Entity_ID1, 3) = ''INC''
			INNER JOIN [MDA_IBase].[dbo]._LinkEnd PO  ON EL.Entity_ID2 = PO.Entity_ID1
			INNER JOIN [MDA_IBase].[dbo].Organisation_ O ON O.Unique_ID = PO.Entity_ID2
			INNER JOIN [MDA_IBase].[dbo].Person_to_Organisation_Link PL ON PO.Link_ID = PL.Unique_ID
			AND I.Source_Reference_411765487 = PL.Source_Reference_411765487
			AND I.Record_Status = 0
			AND EL.Record_Status = 0
			AND PO.Record_Status = 0
			AND O.Record_Status = 0
			AND PL.Record_Status = 0
		WHERE Link_Type = ''Represented by''
		AND Sub_Party_Type != ''Witness''
		
		'
	INSERT INTO #_DATA
	EXEC Rep.uspGetHeadsOfHeadsClaim @SQL

	UPDATE A SET Solicitors_ = Value
	FROM [$(MDA_IBase)].[dbo].Incident_Link  A
	INNER JOIN
	#_DATA B 
	ON A.Unique_ID = B.ID


	TRUNCATE TABLE #_DATA
	SET @SQL = 
	'
	SELECT DISTINCT I.Unique_ID, Organisation_Name + '' ('' +  O.Unique_ID + ''); ''
	FROM  [MDA_IBase].[dbo].Incident_Link I
		INNER JOIN [MDA_IBase].[dbo]._LinkEnd EL  ON I.Unique_ID = EL.Link_ID AND LEFT(Entity_ID1, 3) = ''INC''
		INNER JOIN [MDA_IBase].[dbo]._LinkEnd PO  ON EL.Entity_ID2 = PO.Entity_ID1
		INNER JOIN [MDA_IBase].[dbo].Organisation_ O ON O.Unique_ID = PO.Entity_ID2
		INNER JOIN [MDA_IBase].[dbo].Person_to_Organisation_Link PL ON PO.Link_ID = PL.Unique_ID
		AND I.Source_Reference_411765487 = PL.Source_Reference_411765487
		AND I.Record_Status = 0
		AND EL.Record_Status = 0
		AND PO.Record_Status = 0
		AND O.Record_Status = 0
		AND PL.Record_Status = 0
	WHERE Link_Type = ''USES''
	AND Sub_Party_Type != ''Witness''
	'

	INSERT INTO #_DATA
	EXEC Rep.uspGetHeadsOfHeadsClaim @SQL

	UPDATE A SET Accident_Management = Value
	FROM [$(MDA_IBase)].[dbo].Incident_Link  A
	INNER JOIN
	#_DATA B 
	ON A.Unique_ID = B.ID

	TRUNCATE TABLE #_DATA
	SET @SQL = 
	'
	SELECT DISTINCT I.Unique_ID, Organisation_Name + '' ('' +  O.Unique_ID + ''); ''
	FROM  [MDA_IBase].[dbo].Incident_Link I
		INNER JOIN [MDA_IBase].[dbo]._LinkEnd EL  ON I.Unique_ID = EL.Link_ID AND LEFT(Entity_ID1, 3) = ''INC''
		INNER JOIN [MDA_IBase].[dbo]._LinkEnd PO  ON EL.Entity_ID2 = PO.Entity_ID1
		INNER JOIN [MDA_IBase].[dbo].Organisation_ O ON O.Unique_ID = PO.Entity_ID2
		INNER JOIN [MDA_IBase].[dbo].Person_to_Organisation_Link PL ON PO.Link_ID = PL.Unique_ID
		AND I.Source_Reference_411765487 = PL.Source_Reference_411765487
		AND I.Record_Status = 0
		AND EL.Record_Status = 0
		AND PO.Record_Status = 0
		AND O.Record_Status = 0
		AND PL.Record_Status = 0
	WHERE Link_Type = ''USES''
	AND Sub_Party_Type != ''Witness''
	'

	INSERT INTO #_DATA
	EXEC Rep.uspGetHeadsOfHeadsClaim @SQL

	UPDATE A SET Medical_Examiner = Value
	FROM [$(MDA_IBase)].[dbo].Incident_Link  A
	INNER JOIN
	#_DATA B 
	ON A.Unique_ID = B.ID

	TRUNCATE TABLE #_DATA
	SET @SQL = 
	'
	SELECT DISTINCT I.Unique_ID, Organisation_Name + '' ('' +  O.Unique_ID + ''); ''
	FROM  [MDA_IBase].[dbo].Incident_Link I
		INNER JOIN [MDA_IBase].[dbo]._LinkEnd EL  ON I.Unique_ID = EL.Link_ID AND LEFT(Entity_ID1, 3) = ''INC''
		INNER JOIN [MDA_IBase].[dbo]._LinkEnd PO  ON EL.Entity_ID2 = PO.Entity_ID1
		INNER JOIN [MDA_IBase].[dbo].Organisation_ O ON O.Unique_ID = PO.Entity_ID2
		INNER JOIN [MDA_IBase].[dbo].Person_to_Organisation_Link PL ON PO.Link_ID = PL.Unique_ID
		AND I.Source_Reference_411765487 = PL.Source_Reference_411765487
		AND I.Record_Status = 0
		AND EL.Record_Status = 0
		AND PO.Record_Status = 0
		AND O.Record_Status = 0
		AND PL.Record_Status = 0
	WHERE Link_Type = ''Hire Provided By''
	AND  Sub_Party_Type != ''Witness''
	'

	INSERT INTO #_DATA
	EXEC Rep.uspGetHeadsOfHeadsClaim @SQL

	UPDATE A SET Hire_ = Value
	FROM [$(MDA_IBase)].[dbo].Incident_Link  A
	INNER JOIN
	#_DATA B 
	ON A.Unique_ID = B.ID

	TRUNCATE TABLE #_DATA
	SET @SQL = 
	'
	SELECT	DISTINCT I.Unique_ID, Organisation_Name + '' ('' +  O.Unique_ID + ''); ''
	FROM	MDA_IBase.dbo.Incident_Link I
				INNER JOIN [MDA_IBase].[dbo]._LinkEnd EL  
				 ON I.Unique_ID = EL.Link_ID AND LEFT(Entity_ID1, 3) = ''NC''
				INNER JOIN [MDA_IBase].[dbo]._LinkEnd IV  
				 ON EL.Entity_ID1 = IV.Entity_ID1
				 INNER JOIN [MDA_IBase].[dbo]._LinkEnd IP  
				ON EL.Entity_ID2 = IP.Entity_ID1 AND IV.Entity_ID2 = IP.Entity_ID2
				 INNER JOIN [MDA_IBase].[dbo]._LinkEnd VO  
				 ON IP.Entity_ID2 = VO.Entity_ID1
				INNER JOIN [MDA_IBase].[dbo].Organisation_ O
				 ON VO.Entity_ID2 = O.Unique_ID
				INNER JOIN [MDA_IBase].[dbo].Vehicle_Link VL
				ON VO.Link_ID = VL.Unique_ID
				AND I.Source_Reference_411765487 = VL.Source_Reference_411765487
				AND I.Record_Status = 0
				AND EL.Record_Status = 0
				AND IV.Record_Status = 0
				AND IP.Record_Status = 0
				AND VO.Record_Status = 0
				AND O.Record_Status = 0
				AND VL.Record_Status = 0
		WHERE	Link_Type = ''Inspected by'' 
				AND I.Sub_Party_Type IN (''Driver'', ''Insured'')
	'

	INSERT INTO #_DATA
	EXEC Rep.uspGetHeadsOfHeadsClaim @SQL

	UPDATE A SET Engineer_ = Value
	FROM [$(MDA_IBase)].[dbo].Incident_Link  A
	INNER JOIN
	#_DATA B 
	ON A.Unique_ID = B.ID

	TRUNCATE TABLE #_DATA
	SET @SQL = 
	'
	SELECT	DISTINCT I.Unique_ID, Organisation_Name + '' ('' +  O.Unique_ID + ''); ''
	FROM	MDA_IBase.dbo.Incident_Link I
				INNER JOIN [MDA_IBase].[dbo]._LinkEnd EL  
				 ON I.Unique_ID = EL.Link_ID AND LEFT(Entity_ID1, 3) = ''INC''
				INNER JOIN [MDA_IBase].[dbo]._LinkEnd IV  
				 ON EL.Entity_ID1 = IV.Entity_ID1
				 INNER JOIN [MDA_IBase].[dbo]._LinkEnd IP  
				ON EL.Entity_ID2 = IP.Entity_ID1 AND IV.Entity_ID2 = IP.Entity_ID2
				 INNER JOIN [MDA_IBase].[dbo]._LinkEnd VO  
				 ON IP.Entity_ID2 = VO.Entity_ID1
				INNER JOIN [MDA_IBase].[dbo].Organisation_ O
				 ON VO.Entity_ID2 = O.Unique_ID
				INNER JOIN [MDA_IBase].[dbo].Vehicle_Link VL
				ON VO.Link_ID = VL.Unique_ID
				AND I.Source_Reference_411765487 = VL.Source_Reference_411765487
				AND I.Record_Status = 0
				AND EL.Record_Status = 0
				AND IV.Record_Status = 0
				AND IP.Record_Status = 0
				AND VO.Record_Status = 0
				AND O.Record_Status = 0
				AND VL.Record_Status = 0
		WHERE	Link_Type  LIKE ''Recovered %''
				AND I.Sub_Party_Type IN (''Driver'', ''Insured'')
	'


	INSERT INTO #_DATA
	EXEC Rep.uspGetHeadsOfHeadsClaim @SQL

	UPDATE A SET Recovery_ = Value
	FROM [$(MDA_IBase)].[dbo].Incident_Link  A
	INNER JOIN
	#_DATA B 
	ON A.Unique_ID = B.ID

	TRUNCATE TABLE #_DATA
	SET @SQL = 
	'
	SELECT	DISTINCT I.Unique_ID, Organisation_Name + '' ('' +  O.Unique_ID + ''); ''
	FROM	MDA_IBase.dbo.Incident_Link I
				INNER JOIN [$(MDA_IBase)].[dbo]._LinkEnd EL  
				 ON I.Unique_ID = EL.Link_ID AND LEFT(Entity_ID1, 3) = ''INC''
				INNER JOIN [MDA_IBase].[dbo]._LinkEnd IV  
				 ON EL.Entity_ID1 = IV.Entity_ID1
				 INNER JOIN [MDA_IBase].[dbo]._LinkEnd IP  
				ON EL.Entity_ID2 = IP.Entity_ID1 AND IV.Entity_ID2 = IP.Entity_ID2
				 INNER JOIN [MDA_IBase].[dbo]._LinkEnd VO  
				 ON IP.Entity_ID2 = VO.Entity_ID1
				INNER JOIN [MDA_IBase].[dbo].Organisation_ O
				 ON VO.Entity_ID2 = O.Unique_ID
				INNER JOIN [MDA_IBase].[dbo].Vehicle_Link VL
				ON VO.Link_ID = VL.Unique_ID
				AND I.Source_Reference_411765487 = VL.Source_Reference_411765487
				AND I.Record_Status = 0
				AND EL.Record_Status = 0
				AND IV.Record_Status = 0
				AND IP.Record_Status = 0
				AND VO.Record_Status = 0
				AND O.Record_Status = 0
				AND VL.Record_Status = 0
		WHERE	Link_Type  = ''Repairer''
				AND I.Sub_Party_Type IN (''Driver'', ''Insured'')
	'

	INSERT INTO #_DATA
	EXEC Rep.uspGetHeadsOfHeadsClaim @SQL

	UPDATE A SET Repairer_ = Value
	FROM [$(MDA_IBase)].[dbo].Incident_Link  A
	INNER JOIN
	#_DATA B 
	ON A.Unique_ID = B.ID

	TRUNCATE TABLE #_DATA
	SET @SQL = 
	'
	SELECT	DISTINCT I.Unique_ID, Organisation_Name + '' ('' +  O.Unique_ID + ''); ''
	FROM	MDA_IBase.dbo.Incident_Link I
				INNER JOIN [MDA_IBase].[dbo]._LinkEnd EL  
				 ON I.Unique_ID = EL.Link_ID AND LEFT(Entity_ID1, 3) = ''INC''
				INNER JOIN [MDA_IBase].[dbo]._LinkEnd IV  
				 ON EL.Entity_ID1 = IV.Entity_ID1
				 INNER JOIN [MDA_IBase].[dbo]._LinkEnd IP  
				ON EL.Entity_ID2 = IP.Entity_ID1 AND IV.Entity_ID2 = IP.Entity_ID2
				 INNER JOIN [MDA_IBase].[dbo]._LinkEnd VO  
				 ON IP.Entity_ID2 = VO.Entity_ID1
				INNER JOIN [MDA_IBase].[dbo].Organisation_ O
				 ON VO.Entity_ID2 = O.Unique_ID
				INNER JOIN [MDA_IBase].[dbo].Vehicle_Link VL
				ON VO.Link_ID = VL.Unique_ID
				AND I.Source_Reference_411765487 = VL.Source_Reference_411765487
				AND I.Record_Status = 0
				AND EL.Record_Status = 0
				AND IV.Record_Status = 0
				AND IP.Record_Status = 0
				AND VO.Record_Status = 0
				AND O.Record_Status = 0
				AND VL.Record_Status = 0
		WHERE	Link_Type  IN (''Stored at'', ''Recovered to'')
				AND I.Sub_Party_Type IN (''Driver'', ''Insured'')
	'


	INSERT INTO #_DATA
	EXEC Rep.uspGetHeadsOfHeadsClaim @SQL

	UPDATE A SET Storage_ = Value
	FROM [$(MDA_IBase)].[dbo].Incident_Link  A
	INNER JOIN
	#_DATA B 
	ON A.Unique_ID = B.ID

	TRUNCATE TABLE #_DATA

	SET @SQL = 
	'
	SELECT	
		 DISTINCT Unique_ID, 
		 CASE WHEN ISNULL(RTRIM(Building_), '''') != '''' THEN RTRIM(Building_) + '' '' ELSE '''' END +
		 CASE WHEN ISNULL(RTRIM(Sub_Building), '''') != '''' THEN RTRIM(Sub_Building) + '' '' ELSE '''' END +
		 CASE WHEN ISNULL(RTRIM(Building_Number), '''') != '''' THEN RTRIM(Building_Number) + '' '' ELSE '''' END +
		 CASE WHEN ISNULL(RTRIM(Street_), '''') != '''' THEN RTRIM(Street_) + '', '' ELSE '''' END +
		 CASE WHEN ISNULL(RTRIM(Locality_), '''') != '''' THEN RTRIM(Locality_) + '', '' ELSE '''' END +
		 CASE WHEN ISNULL(RTRIM(Town_), '''') != '''' THEN RTRIM(Town_) + '', '' ELSE '''' END +
		 CASE WHEN ISNULL(RTRIM(County_), '''') != '''' THEN RTRIM(County_) + '', '' ELSE '''' END  +
		 CASE WHEN ISNULL(RTRIM(Post_Code), '''') != '''' THEN RTRIM(Post_Code) + '' '' ELSE '''' END
			+ '' ('' +  Unique_ID + ''); '' 			
		FROM(				
			SELECT DISTINCT				
				Building_,
				Sub_Building,
				Building_Number,
				Street_,
				Locality_,
				Town_,
				County_,
				Post_Code,
				O.Unique_ID
			FROM	
				MDA_IBase.dbo.Incident_Link I
				INNER JOIN [MDA_IBase].[dbo]._LinkEnd EL  
				 ON I.Unique_ID = EL.Link_ID AND LEFT(Entity_ID1, 3) = ''INC''
				INNER JOIN [MDA_IBase].[dbo]._LinkEnd IV  
				 ON EL.Entity_ID1 = IV.Entity_ID1
				 INNER JOIN [MDA_IBase].[dbo]._LinkEnd IP  
				ON EL.Entity_ID2 = IP.Entity_ID1 AND IV.Entity_ID2 = IP.Entity_ID2
				 INNER JOIN [MDA_IBase].[dbo]._LinkEnd VO  
				 ON IP.Entity_ID2 = VO.Entity_ID1
				 INNER JOIN [MDA_IBase].[dbo].Address_to_Address_Link O
				 ON VO.Link_ID = O.Unique_ID
				 AND I.Source_Reference_411765487 = O.Source_Reference_411765487
				 AND I.Record_Status = 0
				 AND EL.Record_Status = 0
				 AND IV.Record_Status = 0
				 AND IP.Record_Status = 0
				 AND VO.Record_Status = 0
				 AND O.Record_Status = 0
				 INNER JOIN [MDA_IBase].[dbo].Address_ A
				 ON A.Unique_ID  = VO.Entity_ID2
			WHERE	
				O.Address_Link_Type = ''Stored at''
				AND I.Sub_Party_Type IN (''Driver'', ''Insured'')
			) AS DD
	'

	INSERT INTO #_DATA
	EXEC Rep.uspGetHeadsOfHeadsClaim @SQL

	UPDATE A SET Storage_Address = Value
	FROM [$(MDA_IBase)].[dbo].Incident_Link  A
	INNER JOIN
	#_DATA B 
	ON A.Unique_ID = B.ID

	IF OBJECT_ID('tempdb..#_DATA') is not null
		DROP TABLE #_DATA
END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
END CATCH

