﻿

CREATE PROCEDURE [IBaseM].[uspGetLinkEndPolicyLink]
AS
SET NOCOUNT ON

BEGIN TRY

	SELECT    New_Unique_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, EL_Record_Status, 
			  Record_Type, EL_SCC
	FROM      vPolicy_Plus_LinkEnd
END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

