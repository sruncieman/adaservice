﻿CREATE PROCEDURE [Rep].[uspReplicateEntityPerson]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateEntityPerson]
--
-- Description:		Replicate Entity Person	
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLogging/Tidy							
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-03-03			Paul Allen		Created
--		2014-07-23			Paul Allen		Added [NHS_Number]
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Person_')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 43 --ReplicatePersonEntity
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	UPDATE [$(MDA_IBase)].[dbo].Person_
	SET Record_Status = 254
	OUTPUT   @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_of_Birth,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_of_Birth,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Document__Binding,'') + '}|I:{' + ISNULL(Inserted.Document__Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.First_Name,'') + '}|I:{' + ISNULL(Inserted.First_Name,'') + '}'
			,'D:{' + ISNULL(Deleted.Gender_,'') + '}|I:{' + ISNULL(Inserted.Gender_,'') + '}'
			,'D:{' + ISNULL(Deleted.Hobbies_,'') + '}|I:{' + ISNULL(Inserted.Hobbies_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + ISNULL(Deleted.Last_Name,'') + '}|I:{' + ISNULL(Inserted.Last_Name,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Middle_Name,'') + '}|I:{' + ISNULL(Inserted.Middle_Name,'') + '}'
			,'D:{' + ISNULL(Deleted.Nationality_,'') + '}|I:{' + ISNULL(Inserted.Nationality_,'') + '}'
			,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{' + ISNULL(Inserted.Notes_,'') + '}'
			,'D:{' + ISNULL(Deleted.Occupation_,'') + '}|I:{' + ISNULL(Inserted.Occupation_,'') + '}'
			,'D:{' + ISNULL(Deleted.Person_ID,'') + '}|I:{' + ISNULL(Inserted.Person_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Picture__Binding,'') + '}|I:{' + ISNULL(Inserted.Picture__Binding,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.Salutation_,'') + '}|I:{' + ISNULL(Inserted.Salutation_,'') + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Schools_,'') + '}|I:{' + ISNULL(Inserted.Schools_,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Taxi_Driver_Licence,'') + '}|I:{' + ISNULL(Inserted.Taxi_Driver_Licence,'') + '}'
			,'D:{' + ISNULL(Deleted.VF_ID,'') + '}|I:{' + ISNULL(Inserted.VF_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Person_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Date_of_Birth, Do_Not_Disseminate_412284494, Document__Binding, First_Name, Gender_, Hobbies_, IconColour, Key_Attractor_412284410, Last_Name, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Middle_Name, Nationality_, Notes_, Occupation_, Person_ID, Picture__Binding, Record_Status, Salutation_, SCC, Schools_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Taxi_Driver_Licence, VF_ID, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.EntityRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo].Person_ a ON ent.IBase8EntityID = a.Unique_ID
	WHERE RecordAction ='D'

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo].Person_ [Target]
	USING	(
			SELECT   DISTINCT C.IBase8EntityID
					,C.IBase5EntityID
					,A.*
			FROM IBaseM8Cur.Person_ A
			INNER JOIN Rep.EntityRecord C ON A.Unique_ID = C.IBase8EntityID_Staging	
			) AS Source ON [Target].Unique_ID = Source.IBase8EntityID
	WHEN MATCHED THEN UPDATE
	SET  Salutation_					= Source.Salutation_
		,First_Name						= Source.First_Name
		,Middle_Name					= Source.Middle_Name
		,Last_Name						= Source.Last_Name
		,Date_of_Birth					= Source.Date_of_Birth
		,Gender_						= Source.Gender_
		,Nationality_					= Source.Nationality_
		,Occupation_					= Source.Occupation_
		,Taxi_Driver_Licence			= Source.Taxi_Driver_Licence
		,Schools_						= Source.Schools_	
		,Hobbies_						= Source.Hobbies_
		,Notes_							= Source.Notes_
		,Document_						= Source.Document_
		,Key_Attractor_412284410		= Source.Key_Attractor_412284410
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User
		,Record_Status					= Source.Record_Status
		,[AltEntity]					= Source.[AltEntity]
		,[Do_Not_Disseminate_412284494]	= Source.[Do_Not_Disseminate_412284494]
		,[Document__Binding]			= Source.[Document__Binding]
		,[IconColour]					= Source.[IconColour]
		,[Person_ID]					= Source.[Person_ID]
		,[Picture_]						= Source.[Picture_]
		,[Picture__Binding]				= Source.[Picture__Binding]
		,[SCC]							= Source.[SCC]
		,[Status_Binding]				= Source.[Status_Binding]
		,[VF_ID]						= Source.[VF_ID]
		,[x5x5x5_Grading_412284402]		= Source.[x5x5x5_Grading_412284402]
		,[Risk_Claim_ID_414244883]		= Source.[Risk_Claim_ID_414244883]
		,[NHS_Number]					= Source.[NHS_Number]
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,AltEntity
		,Create_Date
		,Create_User
		,Date_of_Birth
		,Do_Not_Disseminate_412284494
		,Document_
		,Document__Binding
		,First_Name
		,Gender_
		,Hobbies_
		,IconColour
		,Key_Attractor_412284410
		,Last_Name
		,Last_Upd_Date
		,Last_Upd_User
		,Middle_Name
		,Nationality_
		,Notes_
		,Occupation_
		,Person_ID
		,Picture_
		,Picture__Binding
		,Record_Status
		,Salutation_
		,SCC
		,Schools_
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding
		,Taxi_Driver_Licence
		,VF_ID
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883
		,[NHS_Number]
		)
	VALUES
		(
		 Source.IBase8EntityID
		,Source.AltEntity
		,Source.Create_Date
		,Source.Create_User
		,Source.Date_of_Birth
		,Source.Do_Not_Disseminate_412284494
		,Source.Document_
		,Source.Document__Binding
		,Source.First_Name
		,Source.Gender_
		,Source.Hobbies_
		,Source.IconColour
		,Source.Key_Attractor_412284410
		,Source.Last_Name
		,Source.Last_Upd_Date
		,source.Last_Upd_User
		,Source.Middle_Name
		,Source.Nationality_
		,Source.Notes_
		,Source.Occupation_
		,Source.Person_ID
		,Source.Picture_
		,Source.Picture__Binding
		,Source.Record_Status
		,Source.Salutation_
		,Source.SCC
		,Source.Schools_
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.Taxi_Driver_Licence
		,Source.VF_ID
		,Source.x5x5x5_Grading_412284402
		,Source.Risk_Claim_ID_414244883
		,Source.[NHS_Number]
		)
	OUTPUT   @OutputTaskID
			,$Action
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_of_Birth,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_of_Birth,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Document__Binding,'') + '}|I:{' + ISNULL(Inserted.Document__Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.First_Name,'') + '}|I:{' + ISNULL(Inserted.First_Name,'') + '}'
			,'D:{' + ISNULL(Deleted.Gender_,'') + '}|I:{' + ISNULL(Inserted.Gender_,'') + '}'
			,'D:{' + ISNULL(Deleted.Hobbies_,'') + '}|I:{' + ISNULL(Inserted.Hobbies_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + ISNULL(Deleted.Last_Name,'') + '}|I:{' + ISNULL(Inserted.Last_Name,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Middle_Name,'') + '}|I:{' + ISNULL(Inserted.Middle_Name,'') + '}'
			,'D:{' + ISNULL(Deleted.Nationality_,'') + '}|I:{' + ISNULL(Inserted.Nationality_,'') + '}'
			,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{' + ISNULL(Inserted.Notes_,'') + '}'
			,'D:{' + ISNULL(Deleted.Occupation_,'') + '}|I:{' + ISNULL(Inserted.Occupation_,'') + '}'
			,'D:{' + ISNULL(Deleted.Person_ID,'') + '}|I:{' + ISNULL(Inserted.Person_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Picture__Binding,'') + '}|I:{' + ISNULL(Inserted.Picture__Binding,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.Salutation_,'') + '}|I:{' + ISNULL(Inserted.Salutation_,'') + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Schools_,'') + '}|I:{' + ISNULL(Inserted.Schools_,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Taxi_Driver_Licence,'') + '}|I:{' + ISNULL(Inserted.Taxi_Driver_Licence,'') + '}'
			,'D:{' + ISNULL(Deleted.VF_ID,'') + '}|I:{' + ISNULL(Inserted.VF_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Person_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Date_of_Birth, Do_Not_Disseminate_412284494, Document__Binding, First_Name, Gender_, Hobbies_, IconColour, Key_Attractor_412284410, Last_Name, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Middle_Name, Nationality_, Notes_, Occupation_, Person_ID, Picture__Binding, Record_Status, Salutation_, SCC, Schools_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Taxi_Driver_Licence, VF_ID, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Person_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
		SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Person_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
		SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Person_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()
		
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
						@Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH


