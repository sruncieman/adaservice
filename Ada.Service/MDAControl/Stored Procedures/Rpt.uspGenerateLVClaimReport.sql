﻿


CREATE PROCEDURE [Rpt].[uspGenerateLVClaimReport]
AS
SET NOCOUNT ON
SET DATEFORMAT DMY

--------------------------------------
--DeclareAndSetRequiredObjects
--------------------------------------
DECLARE  @RowsToProcess INT
              ,@Counter INT =1
              ,@RunNameID INT = 149 --LVClaimReport Control Run
              ,@ControlRunID INT
              ,@ReportCreationOutputDeliveryConfiguration_Id INT
              ,@RiskBatch_ID INT
              ,@SQL NVARCHAR(MAX)
              ,@BCP VARCHAR(8000)
              ,@ResultFileName VARCHAR(100)
              ,@ResultTableName NVARCHAR(100) = (SELECT '[dbo].[LVReportResults_' + CAST(NEWID() AS NVARCHAR(50)) + ']')
              ,@MinScoreToRpt INT
              ,@EmailSubject VARCHAR(100)
              ,@MailProfile VARCHAR(50)
              ,@To VARCHAR(200)
              ,@CC VARCHAR(200)
              ,@BCC VARCHAR(200)
              ,@EmailBody_WithData VARCHAR(5000)
              ,@EmailBody_WithoutData VARCHAR(5000)
              ,@EmailBody VARCHAR(5000)

BEGIN TRY
       DECLARE @BatchesToProcess TABLE (RowID INT IDENTITY(1,1), ReportCreationOutputDeliveryConfiguration_Id INT NOT NULL, RiskBatch_Id INT NOT NULL)
                       
       IF OBJECT_ID('tempdb..#KeoghsRule') IS NOT NULL
              DROP TABLE #KeoghsRule 
       CREATE TABLE #KeoghsRule (RiskClaim_Id INT, Risk VARCHAR(10), KeoghsRule VARCHAR(MAX))

       IF OBJECT_ID('tempdb..#LVResults') IS NOT NULL
              DROP TABLE #LVResults
       CREATE TABLE #LVResults (RowID INT IDENTITY(1,1), LVClaimNumber VARCHAR(50), Risk VARCHAR(20))

       --------------------------------------
       --GetAListOfAllReportsThatNeedGenerating
       --------------------------------------
       INSERT INTO @BatchesToProcess (ReportCreationOutputDeliveryConfiguration_Id, RiskBatch_Id)

       SELECT DISTINCT RPT.[Id] ReportCreationOutputDeliveryConfiguration_Id, RB.Id RiskBatch_id

       FROM [MDA].[dbo].[RiskBatch] RB 
              INNER JOIN [Rpt].[ReportCreationOutputDeliveryConfiguration] RPT ON RPT.[RiskClient_Id] = RB.RiskClient_Id 

       WHERE 
              RB.[BatchStatus] = 5
              AND RPT.[ReportName_Id] = 4
              AND RPT.[IsActive] = 1
              AND RB.Id > ISNULL(RPT.[IgnoreBatchIdsTo],0)
			  AND NOT EXISTS (SELECT TOP 1 1 FROM [MDA].[dbo].[RiskClaim] RC WHERE RC.RiskBatch_Id = RB.Id AND RC.ClaimStatus BETWEEN 0 AND 8)
              AND (
                     NOT EXISTS 
                           (      SELECT TOP 1 1 
                                  FROM [Rpt].[ReportNameRun] RPTR 
                                  WHERE RPTR.[ReportCreationOutputDeliveryConfiguration_Id] = RPT.[Id] 
                                         AND RPTR.[BatchID] = RB.Id 
                                         AND [Resend] = 0
                           )
                     OR EXISTS 
                           (      SELECT TOP 1 1 
                                  FROM [Rpt].[ReportNameRun] RPTR 
                                  WHERE RPTR.[ReportCreationOutputDeliveryConfiguration_Id] = RPT.[Id] 
                                  AND RPTR.[BatchID] = RB.Id 
                                  AND [Resend] = 1
                           )
                     )

       --SELECT * FROM @BatchesToProcess --Debug



       SELECT @RowsToProcess = @@ROWCOUNT


       IF @RowsToProcess > 0
       BEGIN

              --CreateTheControlRunRecord
              INSERT INTO [MDAControl].[dbo].[DBControlRun] ([RunNameID]) VALUES (@RunNameID)
              SELECT @ControlRunID = SCOPE_IDENTITY()

              --PopulateTheReportParametersWeNeed
              SELECT TOP 1         @ReportCreationOutputDeliveryConfiguration_Id = ReportCreationOutputDeliveryConfiguration_Id
                                                --@ResultFileName = DestinationFileLocation + REPLACE(REPLACE(REPLACE([DestinationFileName],'@ClientName',RC.ClientName),'@CurrentDateTime',REPLACE(REPLACE(CONVERT(VARCHAR,GETDATE(),120),'-',''),':','')),' ','') + '.' + [DestinationFileFormat]
                                                ,@ResultFileName = DestinationFileLocation + REPLACE(REPLACE(REPLACE([DestinationFileName],'@ClientName','LV'),'@CurrentDateTime',REPLACE(REPLACE(CONVERT(VARCHAR,GETDATE(),120),'-',''),':','')),' ','') + '.' + [DestinationFileFormat]
                                                ,@EmailSubject = [EmailSubject]
                                                ,@MailProfile = [EmailFromProfile]
                                                ,@To = [EmailTo]
                                                ,@CC = [EmailCC]
                                                ,@BCC = [EMailBCC]
                                                ,@EmailBody_WithData = [EmailBody_WithData]
                                                ,@EmailBody_WithoutData = [EmailBody_WithoutData]              
              
              FROM @BatchesToProcess BTP
              INNER JOIN [Rpt].[ReportCreationOutputDeliveryConfiguration] RPT ON RPT.[Id] = BTP.ReportCreationOutputDeliveryConfiguration_Id
              --INNER JOIN [MDA].[dbo].[RiskClient] RC ON RC.Id = RPT.RiskClient_Id
              ORDER BY ReportCreationOutputDeliveryConfiguration_Id DESC

              --END SELECT @ResultFileName, @EmailSubject, @MailProfile, @CC

              --AddTheParemeterDetailsToTheControlRunTable
              UPDATE [dbo].[DBControlRun]
              SET [Details] = 'FileLocation:[' + ISNULL(@ResultFileName,'') + '];EmailSubject[' + ISNULL(@EmailSubject,'') + '];MailTo[' + ISNULL(@To,'') + '];MailCC[' + ISNULL(@CC,'') + '];MailBCC[' + ISNULL(@BCC,'') + '];'                               
              WHERE [RunID] = @ControlRunID

              --END 

              --CreateTheImportTable&Header
              SELECT @SQL = 'CREATE TABLE ' + @ResultTableName + ' (RowID INT IDENTITY(1,1), LVClaimNumber VARCHAR(50), Risk VARCHAR(20))'
                                         + 'INSERT INTO ' + @ResultTableName + ' (LVClaimNumber, Risk) VALUES (''"LVClaimNumber'', ''Risk'')'
              EXECUTE SP_EXECUTESQL  @SQL
              
              END

              --------------------------------------
              --LoopAroundGeneratingTheFile
              --------------------------------------
              WHILE @RowsToProcess >= @Counter
              BEGIN

                     --PopulateTheBatchParametersWeNeed
                     SELECT @RiskBatch_ID = BTP.RiskBatch_Id
                                  ,@MinScoreToRpt = RPT.RiskRunExceedScore                             
                     FROM @BatchesToProcess BTP
                     INNER JOIN [Rpt].[ReportCreationOutputDeliveryConfiguration] RPT ON RPT.[Id] = BTP.ReportCreationOutputDeliveryConfiguration_Id                     
                     INNER JOIN [MDA].[dbo].[RiskBatch] RB ON RB.Id = BTP.RiskBatch_Id
                     WHERE BTP.RowID = @Counter               

                     --CheckToSeeIfWeHaveATableLocation
                     IF OBJECT_ID(@ResultTableName) IS NOT NULL
                     --IF NULLIF(@ResultFileName,'') IS NOT NULL
                     BEGIN
                           --GetTheKeoghRuleDataFromTheXML
                           INSERT INTO #KeoghsRule (RiskClaim_Id, Risk)
                           SELECT RCR.[RiskClaim_Id]
                                         ,[Risk]       =      CASE   WHEN RCR.TotalScore >= 50 AND RCR.TotalScore < 250
                                                                           THEN 'Medium'
                                                                           WHEN RCR.TotalScore >= 250
                                                                           THEN 'High'
                                                                           ELSE 'Low'
                                                                           END    
                           FROM [MDA].[dbo].[RiskClaimRun] RCR
                           INNER JOIN [MDA].[dbo].[RiskClaim] RC ON RC.Id = RCR.RiskClaim_Id
                           WHERE RCR.TotalScore >= @MinScoreToRpt
                           AND TotalScore != ISNULL(PrevTotalScore,0)
                           AND RC.ClaimReadStatus = 0
                           AND RC.RiskBatch_Id = @RiskBatch_ID
                                                                     

                           --GetTheRequiredResultsIntoCTESoWeCanOrderBetter
                           ;WITH CTE_RESULTS AS
                           (
                           SELECT   DISTINCT 
                                         RC.[ClientClaimRefNumber] [Claim Number]
                                         ,KR.Risk [Risk]
                           FROM MDA.dbo.RiskClaimRun RCR
                           INNER JOIN MDA.[dbo].[RiskClaim] RC ON RC.Id = RCR.RiskClaim_Id
                           LEFT JOIN #KeoghsRule KR ON KR.RiskClaim_Id = RCR.RiskClaim_Id
                           WHERE RCR.TotalScore >= @MinScoreToRpt
                           AND RCR.TotalScore != ISNULL(PrevTotalScore,0)
                           AND RC.RiskBatch_Id = @RiskBatch_ID
                           )

                           INSERT INTO #LVResults (LVClaimNumber, Risk)
                           SELECT  [Claim Number]
                                         ,Risk [Risk]
                           FROM CTE_RESULTS RES
                           ORDER BY Risk DESC
                                         ,[Claim Number]                                 

                           --GettheResultsIntoATableWhichWeWillUseTooutputFrom
                           SELECT @SQL = 'INSERT INTO ' + @ResultTableName + ' SELECT LVClaimNumber, Risk FROM #LVResults ORDER BY RowID'
                           EXECUTE SP_EXECUTESQL  @SQL
                                                
                     END

                     --InsertRecordIntoRunHistory
                     INSERT INTO [Rpt].[ReportNameRun] (ReportCreationOutputDeliveryConfiguration_Id, DBControlRun_Id, BatchID)
                     VALUES (@ReportCreationOutputDeliveryConfiguration_Id, @ControlRunID, @RiskBatch_ID)

                     --UpdateResendFlagForBatch
                     UPDATE [Rpt].[ReportNameRun]
                     SET [Resend] = 0
                     WHERE [BatchID] = @RiskBatch_ID
                     AND [Resend] = 1

                     --CloseControlRunRecord
                     UPDATE [dbo].[DBControlRun]
                     SET [RunEndTime] = GETDATE()
                     WHERE [RunID] = @ControlRunID
                                                       
                     DELETE #KeoghsRule
                     DELETE #LVResults

                     SET @Counter += 1

              END
              

              --BCPFileOutToTheFileDirectory
              SELECT @BCP = 'BCP "SELECT LVClaimNumber FROM [MDAControl].' + @ResultTableName + ' ORDER BY RowID" queryout ' + @ResultFileName +' -S '+@@SERVERNAME +' -T -c -t"\",\"" -r"\"\n\""'
              EXEC master..XP_CMDSHELL @BCP, no_output

              --CheckToSeeIfWeCanSendTheEmail
              IF NULLIF(@To,'') IS NOT NULL AND NULLIF(@EmailSubject,'') IS NOT NULL
              BEGIN
                     --SendTheAttachementViaEmail
                     SELECT @EmailBody = CASE WHEN COUNT(1) = 1 THEN @EmailBody_WithoutData ELSE @EmailBody_WithData END FROM #LVResults
                     EXECUTE msdb.dbo.sp_send_dbmail
                                  @profile_name = @MailProfile,
                                  @recipients = @To,
                                  @copy_recipients = @CC,
                                  @blind_copy_recipients = @BCC,
                                  @subject = @EmailSubject,
                                  @body = @EmailBody,
                                  @file_attachments = @ResultFileName,
                                  @exclude_query_output = 1

                     --AddTheParemeterDetailsToTheControlRunTable
                     UPDATE [dbo].[DBControlRun]
                     SET [Details] = [Details] + ';EmailBody:[' + ISNULL(@EmailBody,'') + '];UserName[' + SYSTEM_USER + ']'
                     WHERE [RunID] = @ControlRunID
              END

              --AddTheParemeterDetailsToTheControlRunTable
              UPDATE [dbo].[DBControlRun]
              SET [Details] = [Details] + '];UserName[' + SYSTEM_USER + ']'
              WHERE [RunID] = @ControlRunID     

              --PrepareForTheNextLoop
              SELECT @SQL = 'IF OBJECT_ID('''+ @ResultTableName +''') IS NOT NULL DROP TABLE ' + @ResultTableName                         
              EXECUTE SP_EXECUTESQL  @SQL

       --------------------------------------
       -- Tidy
       --------------------------------------
       IF OBJECT_ID('tempdb..#KeoghsRule') IS NOT NULL
              DROP TABLE #KeoghsRule 

       IF OBJECT_ID('tempdb..#LVResults') IS NOT NULL
              DROP TABLE #LVResults


END TRY
BEGIN CATCH
       DECLARE       @ErrorMessage             NVARCHAR(4000)       = ERROR_MESSAGE()
                     ,@ErrorSeverity            INT                        = ERROR_SEVERITY()
                     ,@ErrorState         INT                        = ERROR_STATE()
                     ,@ErrorProcedure     NVARCHAR(100) = ERROR_PROCEDURE()
                     ,@ErrorLine                INT                        = ERROR_LINE()

       SELECT        @ErrorMessage = 'Procedure=['+ISNULL(@ErrorProcedure,'')+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'

       UPDATE [dbo].[DBControlRun]
       SET  [RunEndTime] = GETDATE()
              ,[Error] = @ErrorSeverity
              ,[ErrorDescription] = @ErrorMessage
       WHERE [RunID] = @ControlRunID

       SELECT @SQL = 'IF OBJECT_ID('''+ @ResultTableName +''') IS NOT NULL DROP TABLE ' + @ResultTableName
       EXECUTE SP_EXECUTESQL  @SQL

       IF OBJECT_ID('tempdb..#KeoghsRule') IS NOT NULL
              DROP TABLE #KeoghsRule 

       IF OBJECT_ID('tempdb..#LVResults') IS NOT NULL
              DROP TABLE #LVResults

       RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH