﻿CREATE PROCEDURE[Sync].[uspSynchroniseLinkPersonToPerson]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseLinkPersonToPerson]
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--						2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)MDAIBase: LinkTablesChanges
--							3.2.1)ActionTheLinkChangeInMDA_IBase
--							3.2.2)DoDeletes
--							3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						3.3)MDAIBase: LinkEndTablesChanges
--							3.3.1)PopulateTheWorkingTable
--							3.3.2)ActionTheLinkEndChangeInMDA_IBase
--							3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
--						4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
--							4.1)ActionTheChangeInMDA
--							4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
--							4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						5)UpdateLoggingAndTidy
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-05-12			Paul Allen		Created
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT						
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT
			,@PhysicalTableName		VARCHAR(255) = 'Person_to_Person_Link'
			,@Prefix				VARCHAR(3)   
			,@LinkTypeID			INT

	SELECT @LinkTypeID =Table_ID, @Prefix = TableCode FROM MDA_IBase.dbo._DataTable WHERE PhysicalName = @PhysicalTableName

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1) PRIMARY KEY, ChangeDatabase VARCHAR(10), PhysicalTableName VARCHAR(255), MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging
	CREATE TABLE #LinkEndStaging (Link_ID NVARCHAR(50), Entity_ID1 NVARCHAR(50), Confidence TINYINT, Direction TINYINT, Entity_ID2 NVARCHAR(50), EntityType_ID1 INT , EntityType_ID2 INT, LinkType_ID INT, Record_Status TINYINT, Record_Type TINYINT, SCC NVARCHAR(255))
			
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 123 --SynchronisePersonToPersonLink
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT
	
	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';
	
	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Person2Person', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA.dbo.Person2Person, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Person_to_Person_Link', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA_IBase.dbo.Person_to_Person_Link, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Link_End', Link_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA_IBase.dbo._LinkEnd, @MDA_IBaseChangeVersionFrom) CT
	INNER JOIN [MDA_IBase].[dbo].Person_to_Person_Link IP2PL ON IP2PL.Unique_ID = CT.Link_ID
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL
	GROUP BY Link_ID, Sys_Change_Operation

	--2.2)AddSomeIndexesToMakeTheProcessQuicker 
	SET @Step ='2.2';

	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT (MDA_IBase_Unique_ID ASC)
	CREATE NONCLUSTERED INDEX IX_CT_PhysicalTableName ON #CT (PhysicalTableName ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3';

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IP2PL.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].Person_to_Person_Link IP2PL ON IP2PL.Unique_ID = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Person_to_Person_Link'
	AND IP2PL.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IVSL.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[VW_StagingLinks] IVSL ON IVSL.[IBase8LinkID] = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Link_End'
	AND IVSL.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MP2P.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].Person2Person MP2P ON MP2P.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Person2Person'

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4';

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				AND PhysicalTableName != 'Link_End'
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5';

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1
	
	--2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
	SET @Step ='2.6';
	UPDATE #CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT	RowID
				FROM	(
						SELECT   RowID
								,ROW_NUMBER() OVER (PARTITION BY MDA_IBase_Unique_ID ORDER BY RowID) RowNumber
						FROM #CT CT 
						WHERE EXISTS (SELECT TOP 1 1 FROM #CT CT1 WHERE MDA_IBase_Unique_ID = MDA_IBase_Unique_ID AND ChangeDatabase = 'MDA_IBase' GROUP BY MDA_IBase_Unique_ID HAVING COUNT(1) > 1)
						) Data 
				WHERE RowNumber !=1
				) IQ ON IQ.RowID = CT.RowID
	WHERE ChangeDatabase = 'MDA_IBase'

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1';

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM MDA_IBase.[dbo].[_Person_to_Person_Link_NextID]
		UPDATE MDA_IBase.dbo.[_Person_to_Person_Link_NextID] SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	 @Prefix + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY RowID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--------------------------------------------------------------------------
		--3.2)MDAIBase: LinkTablesChanges
		--------------------------------------------------------------------------
		--3.2.1)ActionTheLinkChangeInMDA_IBase
		SET @Step ='3.2.1';

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[Person_to_Person_Link] [Target]
		USING  (
				SELECT CT.PhysicalTableName, CT.MDA_ID, CT.MDA_IBase_Unique_ID, IQ.*
				FROM #CT CT
				INNER JOIN	(
							SELECT RowID, MP2P.ID, [IBaseId] [Unique_ID], CreatedDate [Create_Date], CreatedBy [Create_User], ModifiedDate [Last_Upd_Date], ModifiedBy [Last_Upd_User], MP2P.RecordStatus [Record_Status], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], [Person2PersonLinkId] [Person_to_Person_Link_ID], FiveGrading  [x5x5x5_Grading_412284402], [Person2PersonLinkType_Id], MP2PLT.[LinkText] [Link_Type]
							FROM [MDA].[dbo].Person2Person MP2P
							INNER JOIN MDA.[dbo].[Person2PersonLinkType] MP2PLT ON MP2PLT.ID = MP2P.[Person2PersonLinkType_Id]
							INNER JOIN #CT CT ON CT.MDA_ID = MP2P.ID
							WHERE CT.PhysicalTableName = 'Person2Person'
							) IQ ON IQ.RowID = CT.RowID
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET  Create_Date					= Source.Create_Date
			,Create_User					= Source.Create_User
			,Last_Upd_Date					= Source.Last_Upd_Date
			,Last_Upd_User					= Source.Last_Upd_User
			,Link_Type						= CASE WHEN Source.[Person2PersonLinkType_Id] = 0 AND ISNULL(Source.Source_Description_411765489,'') !='ADA' THEN [Target].[Link_Type] ELSE NULLIF(Source.[Link_Type],'Unknown') END
			,Record_Status					= Source.Record_Status
			,Source_411765484				= Source.Source_411765484
			,Source_Description_411765489	= Source.Source_Description_411765489
			,Source_Reference_411765487		= Source.Source_Reference_411765487
			,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
		WHEN NOT MATCHED THEN INSERT
			(
			 Unique_ID
			,Create_Date
			,Create_User
			,Do_Not_Disseminate_412284494
			,Last_Upd_Date
			,Last_Upd_User
			,Link_Type
			,MDA_Incident_ID_412284502
			,[Person_to_Person_Link_ID]
			,Record_Status
			,Source_411765484
			,Source_Description_411765489
			,Source_Reference_411765487
			,x5x5x5_Grading_412284402
			)
		VALUES
			(
			 Source.MDA_IBase_Unique_ID
			,Source.Create_Date
			,Source.Create_User
			,0
			,Source.Last_Upd_Date
			,Source.Last_Upd_User
			,CASE WHEN Source.[Person2PersonLinkType_Id] = 0 THEN NULL ELSE Source.[Link_Type] END
			,CASE WHEN Source.PhysicalTableName= 'Person2Person' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Person2Person') ELSE NULL END
			,Source.[Person_to_Person_Link_ID]
			,Source.Record_Status
			,Source.Source_411765484
			,Source.Source_Description_411765489
			,Source.Source_Reference_411765487
			,Source.x5x5x5_Grading_412284402
			)
		OUTPUT  @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Link_Type,'') + '}|I:{' + ISNULL(Inserted.Link_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Person_to_Person_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Person_to_Person_Link_ID,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Person_to_Person_Link] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Link_Type, MDA_Incident_ID_412284502, Person_to_Person_Link_ID, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

		--3.2.2)DoDeletes
		--WeNeedToDeleteTheLinkEndrecordBeforetheDeletesOrWeWontBeAbleTo
		SET @Step ='3.2.2';

		--DeletesTakeFarTooLongSoLetsBreakThisUp...
		IF OBJECT_ID ('tempdb..#DeletePerformance') IS NOT NULL
			DROP TABLE #DeletePerformance
		CREATE TABLE #DeletePerformance (MDA_Incident_ID_412284502 VARCHAR(50));

		IF OBJECT_ID ('tempdb..#DeletePerformance1') IS NOT NULL
			DROP TABLE #DeletePerformance1
		CREATE TABLE #DeletePerformance1 (Link_ID VARCHAR(50));
		
		INSERT INTO #DeletePerformance (MDA_Incident_ID_412284502)
		SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Person2Person') MDA_Incident_ID_412284502
		FROM #CT CT
		WHERE PhysicalTableName = 'Person2Person'
		AND DMLAction = 'D'

		INSERT INTO #DeletePerformance1 (Link_ID)
		SELECT [IBase8LinkID] Link_ID
		FROM #DeletePerformance DATA
		INNER JOIN [MDA_IBase].[dbo].[VW_StagingLinks] IVSL ON IVSL.[MDA_Incident_ID_412284502] = DATA.MDA_Incident_ID_412284502;

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [MDA_IBase].[dbo].[_LinkEnd] LE
		INNER JOIN #DeletePerformance1 IQ ON IQ.Link_ID = LE.Link_ID;

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[Person_to_Person_Link]
		OUTPUT	 @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Link_Type,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Person_to_Person_Link_ID,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Person_to_Person_Link] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Link_Type, MDA_Incident_ID_412284502, Person_to_Person_Link_ID, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Person_to_Person_Link] IP2PL
		INNER JOIN #CT CT ON CASE WHEN CT.PhysicalTableName = 'Person2Person' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID, 'MDA.dbo.Person2Person') ELSE NULL END = IP2PL.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';

		--3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		SET @Step ='3.2.3';
		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MP2P
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT   @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person1_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person1_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person2_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person2_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.Person2PersonLinkId,'') + '}|I:{' + ISNULL(Inserted.Person2PersonLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person2PersonLinkType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person2PersonLinkType_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Person2Person] (TaskID, DMLAction, Id, Person1_Id, Person2_Id, Person2PersonLinkId, Person2PersonLinkType_Id, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM MDA.dbo.Person2Person MP2P
		INNER JOIN #CT CT ON CT.MDA_ID = MP2P.Id
 		WHERE (MP2P.IBaseId IS NULL
		OR MP2P.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Person2Person';

		--------------------------------------------------------------------------
		--3.3)MDAIBase: LinkEndTablesChanges
		-------------------------------------------------------------------------
		--3.3.1)PopulateTheWorkingTable
		--DoForEntityID1
		--CheckTheValuesThatArebeingAdded
		SET @Step ='3.3.1';

		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC
		FROM	(
				SELECT MP2P.IBaseId Link_ID, MP1.IBaseId Entity_ID1, MP2P.LinkConfidence Confidence, 0 Direction, MP2.IBaseId Entity_ID2, 1969 EntityType_ID1, 1969 EntityType_ID2, @LinkTypeID LinkType_ID, MP2P.RecordStatus Record_Status, 1 Record_Type, NULL SCC
				FROM MDA.dbo.Person2Person MP2P
				INNER JOIN MDA.dbo.Person MP1 ON MP1.ID = MP2P.[Person1_Id]
				INNER JOIN MDA.dbo.Person MP2 ON MP2.ID = MP2P.[Person2_Id]
				INNER JOIN #CT CT ON CT.MDA_ID = MP2P.ID AND CT.PhysicalTableName = 'Person2Person'
				) Data

		--NowDoForEntityID2
		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT Link_ID, Entity_ID2, Confidence, Direction, Entity_ID1, EntityType_ID2, EntityType_ID1, LinkType_ID, Record_Status, 2, SCC
		FROM #LinkEndStaging;

		CREATE CLUSTERED INDEX IX_LinkEndStaging_Link_ID_Entity_ID1 ON #LinkEndStaging (Link_ID, Entity_ID1);

		--3.3.2)ActionTheLinkEndChangeInMDA_IBase
		SET @Step ='3.3.2';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[_LinkEnd] [Target]
		USING	(
				SELECT *
				FROM #LinkEndStaging
				) Source ON Source.Link_ID = [Target].Link_ID AND Source.Entity_ID1 = [Target].Entity_ID1
		WHEN MATCHED AND CHECKSUM(Source.Link_ID, Source.Entity_ID1, Source.Confidence, Source.Direction, Source.Entity_ID2, Source.EntityType_ID1, Source.EntityType_ID2, Source.LinkType_ID, Source.Record_Status, Source.Record_Type, Source.SCC) != CHECKSUM([Target].Link_ID, [Target].Entity_ID1, [Target].Confidence, [Target].Direction, [Target].Entity_ID2, [Target].EntityType_ID1, [Target].EntityType_ID2, [Target].LinkType_ID, [Target].Record_Status, [Target].Record_Type, [Target].SCC) THEN UPDATE
		SET  Confidence			= Source.Confidence
			,Direction			= Source.Direction
			,Entity_ID2			= Source.Entity_ID2
			,EntityType_ID1		= Source.EntityType_ID1
			,EntityType_ID2		= Source.EntityType_ID2
			,LinkType_ID		= Source.LinkType_ID
			,Record_Status		= Source.Record_Status
			,Record_Type		= Source.Record_Type
			,SCC				= Source.SCC
		WHEN NOT MATCHED THEN INSERT
			(
			 Link_ID
			,Entity_ID1
			,Confidence
			,Direction
			,Entity_ID2
			,EntityType_ID1
			,EntityType_ID2
			,LinkType_ID
			,Record_Status
			,Record_Type
			,SCC
			)
		VALUES
			(
			 Source.Link_ID
			,Source.Entity_ID1
			,Source.Confidence
			,Source.Direction
			,Source.Entity_ID2
			,Source.EntityType_ID1
			,Source.EntityType_ID2
			,Source.LinkType_ID
			,Source.Record_Status
			,Source.Record_Type
			,Source.SCC
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Link_ID],'') + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID1],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Confidence],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Direction],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID2],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID1],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID2],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkType_ID],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Type],'')) + '}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{' + ISNULL(Inserted.[SCC],'') + '}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC);

		--3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
		SET @Step ='3.3.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		DELETE  [MDA_IBase].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [MDA_IBase].[dbo].[_LinkEnd] LE
		WHERE EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID)
		AND NOT EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID AND LES.Entity_ID1 = LE.Entity_ID1);

	END

	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--[Person2Person]
		SET @Step ='4.1';

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].[Person2Person] [Target]
		USING	(
				SELECT	*
				FROM	(
						SELECT	 ROW_NUMBER() OVER (PARTITION BY ISNULL(CAST(CT.MDA_ID AS VARCHAR), CT.MDA_IBase_Unique_ID) ORDER BY ILE.Record_Type) RowID
								,CT.MDA_ID
								,CT.MDA_IBase_Unique_ID
								,[Sync].[fn_ExtractMDAID] (IP1.MDA_Incident_ID_412284502) [Person1_Id]
								,[Sync].[fn_ExtractMDAID] (IP2.MDA_Incident_ID_412284502) [Person2_Id]
								,IP2PL.[Person_to_Person_Link_ID] [Person2PersonLinkId]
								,ISNULL(MPLT.ID,0) [Person2PersonLinkType_Id]
								,IP2PL.[x5x5x5_Grading_412284402] [FiveGrading] 
								,ILE.Confidence [LinkConfidence]
								,IP2PL.[Source_411765484] [Source]
								,IP2PL.[Source_Reference_411765487] [SourceReference]
								,IP2PL.[Source_Description_411765489] [SourceDescription]
								,IP2PL.Create_User [CreatedBy]
								,IP2PL.Create_Date [CreatedDate]
								,IP2PL.Last_Upd_User [ModifiedBy]
								,IP2PL.Last_Upd_Date [ModifiedDate]
								,IP2PL.Unique_ID [IBaseId]
								,IP2PL.Record_Status [RecordStatus]		
						FROM #CT CT
						INNER JOIN [MDA_IBase].[dbo].[Person_to_Person_Link] IP2PL ON IP2PL.Unique_ID = CT.MDA_IBase_Unique_ID
						INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IP2PL.Unique_ID
						INNER JOIN [MDA_IBase].[dbo].[Person_] IP1 ON IP1.Unique_ID = ILE.Entity_ID1
						INNER JOIN [MDA_IBase].[dbo].[Person_] IP2 ON IP2.Unique_ID = ILE.Entity_ID2
						LEFT JOIN MDA.[dbo].[Person2PersonLinkType] MPLT ON MPLT.[LinkText] = IP2PL.[Link_Type]
						WHERE CT.DoNotReplicate IS NULL
						AND CT.ChangeDatabase = 'MDA_IBase'
						) IQ
					WHERE RowID = 1
				) Source ON Source.MDA_ID = [Target].ID AND Source.[Person1_Id] = [Target].[Person1_Id] AND Source.[Person2_Id] = [Target].[Person2_Id] 
		WHEN MATCHED THEN UPDATE
		SET	 [Person1_Id]				= Source.[Person1_Id]
			,[Person2_Id]				= Source.[Person2_Id]
			,[Person2PersonLinkId]		= Source.[Person2PersonLinkId]
			,[Person2PersonLinkType_Id]	= Source.[Person2PersonLinkType_Id]
			,FiveGrading				= Source.FiveGrading
			,LinkConfidence				= Source.LinkConfidence
			,Source						= Source.Source
			,SourceReference			= Source.SourceReference
			,SourceDescription			= Source.SourceDescription
			,CreatedBy					= Source.CreatedBy
			,CreatedDate				= Source.CreatedDate
			,ModifiedBy					= Source.ModifiedBy
			,ModifiedDate				= Source.ModifiedDate
			,IBaseId					= Source.IBaseId
			,RecordStatus				= Source.RecordStatus
			,ADARecordStatus			= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
			 [Person1_Id]
			,[Person2_Id]
			,[Person2PersonLinkId]
			,[Person2PersonLinkType_Id]	
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES	
			(
			 Source.[Person1_Id]
			,Source.[Person2_Id]
			,Source.[Person2PersonLinkId]
			,Source.[Person2PersonLinkType_Id]
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,Source.IBaseId
			,Source.RecordStatus
			,10
			)
		OUTPUT	 @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person1_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person1_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person2_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person2_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.Person2PersonLinkId,'') + '}|I:{' + ISNULL(Inserted.Person2PersonLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person2PersonLinkType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person2PersonLinkType_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Person2Person] (TaskID, DMLAction, Id, Person1_Id, Person2_Id, Person2PersonLinkId, Person2PersonLinkType_Id, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus);

		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--[Person2Person]
		SET @Step ='4.2';
	
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Person2Person]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person1_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person1_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person2_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person2_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.Person2PersonLinkId,'') + '}|I:{' + ISNULL(Inserted.Person2PersonLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person2PersonLinkType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person2PersonLinkType_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Person2Person] (TaskID, DMLAction, Id, Person1_Id, Person2_Id, Person2PersonLinkId, Person2PersonLinkType_Id, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Person2Person] MP2P
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MP2P.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA_IBase].[dbo].[Person_to_Person_Link]
		SET  [MDA_Incident_ID_412284502] = IQ.MDA_Incident_ID_412284502
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Link_Type,'') + '}|I:{' + ISNULL(Inserted.Link_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Person_to_Person_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Person_to_Person_Link_ID,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Person_to_Person_Link] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Link_Type, MDA_Incident_ID_412284502, Person_to_Person_Link_ID, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Person_to_Person_Link] IP2PL
		INNER JOIN (
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MP2P.ID,'MDA.dbo.Person2Person') MDA_Incident_ID_412284502
					FROM [MDA].[dbo].Person2Person MP2P
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MP2P.IBaseId
					) IQ ON IQ.[Unique_ID] =  IP2PL.[Unique_ID]
		WHERE IP2PL.MDA_Incident_ID_412284502 IS NULL
		OR IP2PL.MDA_Incident_ID_412284502 != IQ.MDA_Incident_ID_412284502

	END

	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Person2Person] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Person2Person] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Person2Person] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Person_to_Person_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Person_to_Person_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Person_to_Person_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging

END TRY
BEGIN CATCH
	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(500)	= ISNULL(ERROR_PROCEDURE(),@@PROCID)
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +ISNULL(@Step,'?') +']'
	
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

