﻿CREATE PROCEDURE [dbo].[uspCRUDDBControlProcess]
/**************************************************************************************************/
-- ObjectName:		MDAControl.dbo.uspCRUDDBControlProcess
--
-- Description:		CRUD Procedure on table dbo.DBControlProcess
--
-- Usage:			Logging
--
-- Steps:			1)SetTheTaskIDIfExists
--					2)DeleteUpdateOrInsertTheControlTaskRecord
--					3)ReadTheTaskID
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-25			Paul Allen		Created
/**************************************************************************************************/
(
 @Action			VARCHAR(1)		= NULL
,@ProcessID			INT				= NULL
,@RunID				INT				= NULL
,@ProcessNameID		INT				= NULL
,@DatabaseID		INT				= NULL
,@ObjectID			INT				= NULL
,@ProcessStartTime	DATETIME		= NULL
,@ProcessEndTime	DATETIME		= NULL
,@Error				INT				= NULL
,@ErrorDescription	VARCHAR(1000)	= NULL
,@Details			VARCHAR(1000)	= NULL
,@OutputProcessID	INT				OUTPUT		
)

AS
SET NOCOUNT, XACT_ABORT ON

BEGIN TRY
	IF @Action IN ('U','D') AND NULLIF(@ProcessID,'') IS NULL
	BEGIN
		RAISERROR ('TheRunIDMustBeSuppliedForUpdateOrDeleteOperations',18,255)
		RETURN
	END

	/*1)SetTheTaskIDIfExists*/
	IF @ProcessID IS NULL
	SELECT @OutputProcessID =	(
								SELECT MAX(ProcessID) 
								FROM dbo.DBControlProcess
								WHERE RunID = @RunID
								AND ProcessNameID = @ProcessNameID
								AND Error IS NULL
								AND ProcessEndTime IS NULL
								AND ProcessID = (SELECT MAX(ProcessID) FROM dbo.DBControlProcess)
								)

	/*2)DeleteUpdateOrInsertTheControlTaskRecord*/
	IF @OutputProcessID IS NULL OR @Action IN ('U','D')
	BEGIN
		DECLARE @OuputTable TABLE (ProcessID INT)
		MERGE dbo.DBControlProcess [TARGET]
		USING  (
				SELECT	 @ProcessID			ProcessID
						,@RunID				RunID
						,@ProcessNameID		ProcessNameID
						,@DatabaseID		DatabaseID
						,@ObjectID			ObjectID	
						,@ProcessStartTime	ProcessStartTime
						,@ProcessEndTime	ProcessEndTime
						,@Error				Error
						,@ErrorDescription	ErrorDescription
						,@Details			Details
				) [SOURCE] ON [SOURCE].ProcessID = [TARGET].ProcessID
		WHEN MATCHED AND @Action = 'D'
			THEN DELETE		
		WHEN MATCHED THEN UPDATE
			SET  ProcessNameID		= ISNULL(@ProcessNameID, [TARGET].ProcessNameID)
				,DatabaseID			= ISNULL(@DatabaseID, [TARGET].DatabaseID)
				,ObjectID			= ISNULL(@ObjectID, [TARGET].ObjectID)
				,ProcessStartTime	= ISNULL(@ProcessStartTime, [TARGET].ProcessStartTime)
				,ProcessEndTime		= ISNULL(@ProcessEndTime, [TARGET].ProcessEndTime)
				,Error				= ISNULL(@Error, [TARGET].Error)
				,ErrorDescription	= ISNULL(@ErrorDescription, [TARGET].ErrorDescription)
				,Details			= ISNULL(@Details, [TARGET].Details)
		WHEN NOT MATCHED AND @ProcessNameID IS NOT NULL THEN INSERT
				(
				 RunID
				,ProcessNameID
				,DatabaseID
				,ObjectID
				,ProcessEndTime
				,Error
				,ErrorDescription
				,Details
				)
				VALUES
				(
				 @RunID
				,@ProcessNameID
				,@DatabaseID
				,@ObjectID
				,@ProcessEndTime
				,@Error
				,@ErrorDescription
				,@Details
				)
			OUTPUT Inserted.ProcessID
			INTO @OuputTable (ProcessID);
		END
		
		/*3)ReadTheTaskID*/
		IF @OutputProcessID IS NULL
			SELECT @OutputProcessID = ProcessID FROM @OuputTable
				
END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH
