﻿CREATE PROCEDURE [Rep].[uspReplicateEntityOrganisation]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateEntityOrganisation]	
--
-- Description:		Replicate Entit yOrganisation
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLogging/Tidy			
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-03-03			Paul Allen		Created
--		2014-07-23			Paul Allen		Added [Dissolved_Date], [Document_], [Document__Binding]
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Organisation_')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 40 --ReplicateOrganisationEntity
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	UPDATE [$(MDA_IBase)].[dbo].Organisation_
	SET Record_Status = 254
	OUTPUT	 @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + ISNULL(Deleted.Credit_Licence,'') + '}|I:{' + ISNULL(Inserted.Credit_Licence,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Effective_Date_of_Status,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Effective_Date_of_Status,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incorporated_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incorporated_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.MoJ_CRM_Number,'') + '}|I:{' + ISNULL(Inserted.MoJ_CRM_Number,'') + '}'
			,'D:{' + ISNULL(Deleted.MoJ_CRM_Status,'') + '}|I:{' + ISNULL(Inserted.MoJ_CRM_Status,'') + '}'
			,'D:{' + ISNULL(Deleted.Organisation_ID,'') + '}|I:{' + ISNULL(Inserted.Organisation_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Organisation_Name,'') + '}|I:{' + ISNULL(Inserted.Organisation_Name,'') + '}'
			,'D:{' + ISNULL(Deleted.Organisation_Status,'') + '}|I:{' + ISNULL(Inserted.Organisation_Status,'') + '}'
			,'D:{' + ISNULL(Deleted.Organisation_Type,'') + '}|I:{' + ISNULL(Inserted.Organisation_Type,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.Registered_Number,'') + '}|I:{' + ISNULL(Inserted.Registered_Number,'') + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.SIC_,'') + '}|I:{' + ISNULL(Inserted.SIC_,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.VAT_Number,'') + '}|I:{' + ISNULL(Inserted.VAT_Number,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VAT_Number_Validated_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VAT_Number_Validated_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Organisation_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Credit_Licence, Do_Not_Disseminate_412284494, Effective_Date_of_Status, IconColour, Incorporated_Date, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, MoJ_CRM_Number, MoJ_CRM_Status, Organisation_ID, Organisation_Name, Organisation_Status, Organisation_Type, Record_Status, Registered_Number, SCC, SIC_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, VAT_Number, VAT_Number_Validated_Date, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.EntityRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo].Organisation_ a ON ent.IBase8EntityID = a.Unique_ID
	WHERE RecordAction ='D'

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo].Organisation_ [Target]
	USING	(
			SELECT   DISTINCT C.IBase8EntityID
					,C.IBase5EntityID
					,A.*
			FROM IBaseM8Cur.Organisation_ A
			INNER JOIN Rep.EntityRecord C ON A.Unique_ID = C.IBase8EntityID_Staging	
			) AS Source ON [Target].Unique_ID = Source.IBase8EntityID
	WHEN MATCHED THEN UPDATE
	SET  Organisation_Name				= Source.Organisation_Name
		,Organisation_Type				= Source.Organisation_Type
		,Incorporated_Date				= Source.Incorporated_Date
		,Organisation_Status			= Source.Organisation_Status
		,Effective_Date_of_Status		= Source.Effective_Date_of_Status
		,Registered_Number				= Source.Registered_Number
		,SIC_							= Source.SIC_
		,VAT_Number						= Source.VAT_Number
		,VAT_Number_Validated_Date		= Source.VAT_Number_Validated_Date
		,MoJ_CRM_Number					= Source.MoJ_CRM_Number
		,MoJ_CRM_Status					= Source.MoJ_CRM_Status
		,Credit_Licence					= Source.Credit_Licence	
		,Key_Attractor_412284410		= Source.Key_Attractor_412284410
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User
		,Record_Status					= Source.Record_Status
		,AltEntity						= Source.AltEntity
		,Do_Not_Disseminate_412284494	= Source.Do_Not_Disseminate_412284494
		,IconColour						= Source.IconColour
		,Organisation_ID				= Source.Organisation_ID
		,SCC							= Source.SCC
		,Status_Binding					= Source.Status_Binding
		,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883		= Source.Risk_Claim_ID_414244883
		,[Dissolved_Date]				= Source.[Dissolved_Date]
		,[Document_]					= Source.[Document_]
		,[Document__Binding]			= Source.[Document__Binding]
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,AltEntity
		,Create_Date
		,Create_User
		,Credit_Licence
		,Do_Not_Disseminate_412284494
		,Effective_Date_of_Status
		,IconColour
		,Incorporated_Date
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,MoJ_CRM_Number
		,MoJ_CRM_Status
		,Organisation_ID
		,Organisation_Name
		,Organisation_Status
		,Organisation_Type
		,Record_Status
		,Registered_Number
		,SCC
		,SIC_
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding
		,VAT_Number
		,VAT_Number_Validated_Date
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883
		,[Dissolved_Date]
		,[Document_]
		,[Document__Binding]
		)
	VALUES
		(
		 Source.IBase8EntityID
		,Source.AltEntity
		,Source.Create_Date
		,Source.Create_User
		,Source.Credit_Licence
		,Source.Do_Not_Disseminate_412284494
		,Source.Effective_Date_of_Status
		,Source.IconColour
		,Source.Incorporated_Date
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.MoJ_CRM_Number
		,Source.MoJ_CRM_Status
		,Source.Organisation_ID
		,Source.Organisation_Name
		,Source.Organisation_Status
		,Source.Organisation_Type
		,Source.Record_Status
		,Source.Registered_Number
		,Source.SCC
		,Source.SIC_
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.VAT_Number
		,Source.VAT_Number_Validated_Date
		,Source.x5x5x5_Grading_412284402
		,Source.Risk_Claim_ID_414244883
		,Source.[Dissolved_Date]
		,Source.[Document_]
		,Source.[Document__Binding]
		)
	OUTPUT	 @OutputTaskID
			,$Action
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + ISNULL(Deleted.Credit_Licence,'') + '}|I:{' + ISNULL(Inserted.Credit_Licence,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Effective_Date_of_Status,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Effective_Date_of_Status,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incorporated_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incorporated_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.MoJ_CRM_Number,'') + '}|I:{' + ISNULL(Inserted.MoJ_CRM_Number,'') + '}'
			,'D:{' + ISNULL(Deleted.MoJ_CRM_Status,'') + '}|I:{' + ISNULL(Inserted.MoJ_CRM_Status,'') + '}'
			,'D:{' + ISNULL(Deleted.Organisation_ID,'') + '}|I:{' + ISNULL(Inserted.Organisation_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Organisation_Name,'') + '}|I:{' + ISNULL(Inserted.Organisation_Name,'') + '}'
			,'D:{' + ISNULL(Deleted.Organisation_Status,'') + '}|I:{' + ISNULL(Inserted.Organisation_Status,'') + '}'
			,'D:{' + ISNULL(Deleted.Organisation_Type,'') + '}|I:{' + ISNULL(Inserted.Organisation_Type,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.Registered_Number,'') + '}|I:{' + ISNULL(Inserted.Registered_Number,'') + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.SIC_,'') + '}|I:{' + ISNULL(Inserted.SIC_,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.VAT_Number,'') + '}|I:{' + ISNULL(Inserted.VAT_Number,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VAT_Number_Validated_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VAT_Number_Validated_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Organisation_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Credit_Licence, Do_Not_Disseminate_412284494, Effective_Date_of_Status, IconColour, Incorporated_Date, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, MoJ_CRM_Number, MoJ_CRM_Status, Organisation_ID, Organisation_Name, Organisation_Status, Organisation_Type, Record_Status, Registered_Number, SCC, SIC_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, VAT_Number, VAT_Number_Validated_Date, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Organisation_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
		SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Organisation_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
		SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Organisation_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()
		
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
						@Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH


