﻿

CREATE PROCEDURE [IBaseM].[uspCreateSynonymsForReplication]
/**************************************************************************************************/
-- ObjectName:			MDAControl.IBaseM.uspCreateSynonymsForReplication
--
-- Description:			Creates Synoymns for tables required for replicaion based upon the latest
--						restored databases.
--
-- Steps:				1)ConfigureLogging,Create/DeclareRequiredObjects		
--							1.1)ConfigureLogging
--							1.2)Create/DeclareRequiredObjects
--						2)DeleteAndCreateTheSynoymn
--							2.1)DropSynoymns
--							2.2)CreateSynoymsForIBaseM5Cur
--							2.3)CreateSynoymsForIBaseM8Cur
--							2.4)CreateSynoymsForIBaseM8Pre
--						3)UpdateLogging/Tidy				
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-25			Paul Allen		Created
--		2016-01-21			Paul Allen		Amended to allow for Vanilla POC Creation
/**************************************************************************************************/
( 
 @Logging				BIT = 0	--DefaultIsOff
,@Debug					BIT = 0 --DefaultIsOff
,@IsVanillaPOC			BIT = 0 --DefaultIsOff
)
AS
SET NOCOUNT ON
BEGIN TRY

	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@OutputRunID		INT
				,@OutputProcessID	INT
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@RunNameID			INT = CASE WHEN @IsVanillaPOC = 0 THEN 1 ELSE 140 END
	
		EXECUTE dbo.uspCRUDDBControlRun
					 @RunNameID		= @RunNameID
					,@OutputRunID	= @OutputRunID OUTPUT
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'					
		EXECUTE dbo.uspCRUDDBControlProcess
					 @RunID				= @OutputRunID
					,@ProcessNameID		= 5 --CreateSynonymsForReplication
					,@Details			= @Details
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@OutputProcessID	= @OutputProcessID OUTPUT
	END

	--1.2)Create/DeclareRequiredObjects
	DECLARE  @IBaseM5CurrentDB  VARCHAR(50) =	(SELECT TOP 1 name FROM sys.databases WHERE patindex('IBase5_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name DESC)
			,@IBaseM8CurrentDB  VARCHAR(50) =	CASE WHEN @IsVanillaPOC = 0 THEN (SELECT TOP 1 name FROM sys.databases WHERE patindex('IBase8_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name DESC)  ELSE 'MDA_IBase' END
			,@IBaseM8PreviousDB VARCHAR(50) =	(SELECT TOP 1 name FROM sys.databases WHERE patindex('IBase8_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name )

	DECLARE  @IBaseM5CurSynonyms TABLE (RowID INT IDENTITY(1,1), SQLStatement VARCHAR(200))
	DECLARE  @IBaseM8CurSynonyms TABLE (RowID INT IDENTITY(1,1), SQLStatement VARCHAR(200))
	DECLARE  @IBaseM8PreSynonyms TABLE (RowID INT IDENTITY(1,1), SQLStatement VARCHAR(200))

	DECLARE  @MaxRow INT
			,@SQL NVARCHAR(MAX) = ''
			,@Counter INT = 1
	
	------------------------------------------------------------
	--2)DeleteAndCreateTheSynoymn
	------------------------------------------------------------		
	IF ((@IsVanillaPOC = 0 AND @IBaseM5CurrentDB IS NOT NULL AND @IBaseM8CurrentDB IS NOT NULL AND @IBaseM8PreviousDB IS NOT NULL AND  @IBaseM8CurrentDB != @IBaseM8PreviousDB) OR @IsVanillaPOC = 1)
	BEGIN
			
		--2.1)DropSynoymns
		SELECT @SQL += 'DROP SYNONYM [' + sh.name + '].[' + sy.name  + ']; ' +CHAR(13)
		FROM sys.synonyms sy
		INNER JOIN sys.schemas sh
		ON sy.schema_id = sh.schema_id
		AND sh.name IN ('IBaseM5Cur', 'IBaseM8Cur', 'IBaseM8Pre')

		EXECUTE SP_EXECUTESQL @SQL		
		
		--2.2)CreateSynoymsForIBaseM5Cur
		SELECT @SQL = '
						 SELECT ''CREATE SYNONYM IBaseM5Cur.'' + Name + '' FOR ' + @IBaseM5CurrentDB + '.dbo.'' + Name + '';''
						 FROM ' + @IBaseM5CurrentDB + '.sys.tables
						 ORDER BY Name
						'
												
		INSERT INTO @IBaseM5CurSynonyms
		EXEC sp_executesql @SQL

		SET @MaxRow = @@ROWCOUNT
		WHILE @MaxRow >= @Counter
			BEGIN
				SET @SQL = (SELECT SQLStatement FROM @IBaseM5CurSynonyms WHERE RowID = @Counter)
				EXEC sp_executesql @SQL
				
				SET @Counter += 1
			END
			
		--2.3)CreateSynoymsForIBaseM8Cur
		SELECT @Counter = 1
		SELECT @SQL = '
						 SELECT ''CREATE SYNONYM IBaseM8Cur.'' + Name + '' FOR ' + @IBaseM8CurrentDB + '.dbo.'' + Name + '';''
						 FROM ' + @IBaseM8CurrentDB + '.sys.tables
						 ORDER BY Name
						'
								
		INSERT INTO @IBaseM8CurSynonyms
		EXEC sp_executesql @SQL

		SET @MaxRow = @@ROWCOUNT
		WHILE @MaxRow >= @Counter
			BEGIN
				SET @SQL = (SELECT SQLStatement FROM @IBaseM8CurSynonyms WHERE RowID = @Counter)
				EXEC sp_executesql @SQL
				
				SET @Counter += 1
			END
		
		IF 	@IsVanillaPOC = 0
		BEGIN
			--2.4)CreateSynoymsForIBaseM8Pre
			SELECT @Counter = 1
			SELECT @SQL = '
							 SELECT ''CREATE SYNONYM IBaseM8Pre.'' + Name + '' FOR ' + @IBaseM8PreviousDB + '.dbo.'' + Name + '';''
							 FROM ' + @IBaseM8PreviousDB + '.sys.tables
							 ORDER BY Name
							'
								
			INSERT INTO @IBaseM8PreSynonyms
			EXEC sp_executesql @SQL

			SET @MaxRow = @@ROWCOUNT
			WHILE @MaxRow >= @Counter
				BEGIN
					SET @SQL = (SELECT SQLStatement FROM @IBaseM8PreSynonyms WHERE RowID = @Counter)
					EXEC sp_executesql @SQL
				
					SET @Counter += 1
				END
		END
	END
	
	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	-----------------------------------------------------------
	IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()			
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@OutputProcessID	= @OutputProcessID OUTPUT
		END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
				
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()						
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputProcessID	= @OutputProcessID OUTPUT
						
			EXECUTE dbo.uspCRUDDBControlRun
						 @Action			= 'U'
						,@RunID				= @OutputRunID
						,@RunEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputRunID		= @OutputRunID OUTPUT
		END
END CATCH




