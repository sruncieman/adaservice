﻿

CREATE PROCEDURE [IBaseM].[uspMigrationStep02]
/**************************************************************************************************/
-- ObjectName:			MDAControl.IBaseM.uspMigrationStep02
--
-- Description:			Populate the Element tables within IBase8 Target database
--
-- Steps:				1)ConfigureLogging,Create/DeclareRequiredObjects
--							1.1)ConfigureLogging
--							1.2)Create/DeclareRequiredObjects
--						2)InsertDataIntoTable
--							2.1)Account_
--							2.2)Address_
--							2.3)BBPin_
--							2.4)Driving_Licence
--							2.5)Email_
--							2.6)Fraud_Ring
--							2.7)Incident_
--							2.8)Intelligence_
--							2.9)IP_Address
--							2.10)Keoghs_Case
--							2.11)NI_Number_
--							2.12)Organisation_
--							2.13)Passport_
--							2.14)Payment_Card
--							2.15)Person_
--							2.16)Policy_
--							2.17)Telephone_ 
--							2.18)Vehicle_
--							2.19)Website_
--						3)UpdateLogging/Tidy
--
-- Dependencies:		IBaseM.uspTruncateTableSynonym
--
-- Usage:				Called within SP IBaseM.uspMigrationMaster
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 9 --Populate_Elements
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	--1.2)Create/DeclareRequiredObjects
	DECLARE  @NextID INT
			,@Source_411765484 VARCHAR(20) = 'Keoghs CFS'
			,@InsertRowCount INT = 0

	------------------------------------------------------------
	--2)InsertDataIntoTable
	------------------------------------------------------------
	--2.1)Account_
	IF @Debug =1
		PRINT 'Insering Account'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Account_'
	INSERT INTO IBaseM8Cur.Account_(
		Unique_ID, Account_Number, AccountID_, AltEntity, Bank_, Create_Date, Create_User, 
		Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, 
		MDA_Incident_ID_412284502, Record_Status, SCC, Sort_Code, Source_411765484, Source_Description_411765489, 
		Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	 SELECT
		Unique_ID COLLATE DATABASE_DEFAULT  AS Unique_ID, 
		Account_Number COLLATE DATABASE_DEFAULT AS Account_Number,
		Unique_ID AS AccountID_,
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Bank_ COLLATE DATABASE_DEFAULT AS Bank_,
		CONVERT(DATETIME, Create_Date, 102) AS Create_Date, 
		Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		0 Do_Not_Disseminate_412284494,
		IconColour AS IconColour, 
		NULL Key_Attractor_412284410,
		CONVERT(DATETIME, Last_Upd_Date, 102) AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		Record_Status AS Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		Sort_Code AS Sort_Code, 
		'Keoghs CFS' AS Source_411765484, 
		NULL AS Source_Reference_411765487, 
		NULL AS Source_Description_411765489,
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Account_ WITH(NOLOCK)
	WHERE AltEntity  = 'Account'
	SELECT @InsertRowCount += @@ROWCOUNT

	IF @Debug =1
		PRINT 'Reseting Account Counter'
		
	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Account_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Account__NextID'
	INSERT INTO IBaseM8Cur._Account__NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.2)Address_
	IF @Debug =1
		PRINT 'Inserting Address'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Address_'
	INSERT INTO IBaseM8Cur.Address_(Unique_ID, Address_ID, Address_Type, AltEntity, Building_, Building_Number, County_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Document_Link, Document_Link_Binding, DX_Exchange, DX_Number, Grid_X, Grid_Y, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Locality_, MDA_Incident_ID_412284502, PAF_Validation, Post_Code, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Street_, Sub_Building, Town_, x5x5x5_Grading_412284402)
	SELECT 
		'ADD' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY CONVERT(INT, STUFF(Unique_ID, 1, 3, '')))) COLLATE DATABASE_DEFAULT AS Unique_ID, 
		Unique_ID COLLATE DATABASE_DEFAULT AS  Address_ID, 
		Type_ COLLATE DATABASE_DEFAULT AS Address_Type, 
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Property_Name COLLATE DATABASE_DEFAULT AS Building_, 
		Property_No COLLATE DATABASE_DEFAULT AS Building_Number, 
		County_ COLLATE DATABASE_DEFAULT AS County_,
		Create_Date AS Create_Date, 
		Create_User COLLATE DATABASE_DEFAULT AS Create_User,
		0 Do_Not_Disseminate_412284494,
		NULL AS Document_Link,
		NULL  Document_Link_Binding,	
		DX_Exchange COLLATE DATABASE_DEFAULT AS DX_Exchange, 
		DX_Number COLLATE DATABASE_DEFAULT AS DX_Number,
		NULL AS Grid_X, 
		NULL AS Grid_Y, 
		IconColour AS IconColour,
		NULL Key_Attractor_412284410,	
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		Locality_ COLLATE DATABASE_DEFAULT AS Locality_, 
		NULL MDA_Incident_ID_412284502,	
		'' AS PAF_Validation, 
		Post_Code COLLATE DATABASE_DEFAULT AS Post_Code, 
		Record_Status AS Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484  AS Source_411765484, 
		LEFT(DB_Source_387004609, 6) AS Source_Reference_411765487, 
		SUBSTRING(DB_Source_387004609, 8, 10) AS Source_Description_411765489, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		Street_ COLLATE DATABASE_DEFAULT AS Street_,
		Flat_No AS Sub_Building, 
		Town_ COLLATE DATABASE_DEFAULT AS Town_,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Location_ WITH(NOLOCK)
	WHERE ISNULL(Type_, '') != 'Accident Location'
	SELECT @InsertRowCount += @@ROWCOUNT

	IF @Debug =1
		PRINT 'Reseting Address Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Address_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Address__NextID'
	INSERT INTO IBaseM8Cur._Address__NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.3)BBPin_
	IF @Debug =1
		PRINT 'Inserting BBPin'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.BBPin_'
	INSERT INTO IBaseM8Cur.BBPin_(Unique_ID, AltEntity, BB_Pin, BB_Pin_ID, Create_Date, Create_User, Display_Name, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT 
		'BBP' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY CONVERT(INT, STUFF(Unique_ID, 1, 3, '')))) COLLATE DATABASE_DEFAULT AS Unique_ID,
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity,
		IP_Address COLLATE DATABASE_DEFAULT AS BB_Pin, 
		Unique_ID COLLATE DATABASE_DEFAULT AS BB_Pin_ID, 
		Create_Date Create_Date, 
		Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		NULL AS Display_Name,
		1 Do_Not_Disseminate_412284494,
		IconColour AS IconColour, 
		NULL Key_Attractor_412284410,	
		Last_Upd_Date Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		Record_Status AS Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 AS Source_411765484, 
		NULL AS Source_Description_411765489, 
		NULL AS Source_Reference_411765487, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL x5x5x5_Grading_412284402	 
	FROM IBaseM5Cur.Telephone_ WITH(NOLOCK)
	WHERE IP_Address LIKE '%BB%'
	SELECT @InsertRowCount += @@ROWCOUNT

	IF @Debug =1
		PRINT 'Reseting BBPin Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.BBPin_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._BBPin__NextID'
	INSERT INTO IBaseM8Cur._BBPin__NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.4)Driving_Licence
	IF @Debug =1
		PRINT 'Inserting Driving_Licence'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Driving_Licence'
	INSERT INTO IBaseM8Cur.Driving_Licence(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, Driving_Licence_ID, Driving_Licence_Number, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT
		'DRI' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY CONVERT(INT, STUFF(Unique_ID, 1, 3, '')))) COLLATE DATABASE_DEFAULT AS Unique_ID, 
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Create_Date AS Create_Date, 
		Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		0  Do_Not_Disseminate_412284494,
		Unique_ID COLLATE DATABASE_DEFAULT AS Driving_Licence_ID, 
		Driving_Licence_Number COLLATE DATABASE_DEFAULT AS Driving_Licence_Number,
		IconColour AS IconColour, 
		NULL Key_Attractor_412284410,
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		Record_Status AS Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 AS Source_411765484, 
		NULL AS Source_Description_411765489,
		DB_Source_387004609 AS Source_Reference_411765487, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Person_ WITH(NOLOCK)
	WHERE Driving_Licence_Number IS NOT NULL
	SELECT @InsertRowCount += @@ROWCOUNT

	IF @Debug =1
		PRINT 'Reseting Driving_Licence Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Driving_Licence
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Driving_Licence_NextID'
	INSERT INTO IBaseM8Cur._Driving_Licence_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.5)Email_
	IF @Debug =1
		PRINT 'Inserting Email_'
	  
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Email_'
	INSERT INTO IBaseM8Cur.Email_(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, Email_Address, Email_ID, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT 
		'EMA' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY CONVERT(INT, STUFF(Unique_ID, 1, 3, '')))) COLLATE DATABASE_DEFAULT AS Unique_ID, 
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Create_Date AS Create_Date,
		Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		0  Do_Not_Disseminate_412284494,
		EMail_Address COLLATE DATABASE_DEFAULT AS Email_Address,
		Unique_ID COLLATE DATABASE_DEFAULT AS Email_ID,
		IconColour AS IconColour, 
		NULL Key_Attractor_412284410,	
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User,
		NULL MDA_Incident_ID_412284502,
		Record_Status AS Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 Source_411765484, 
		NULL Source_Description_411765489,
		NULL Source_Reference_411765487, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Telephone_ WITH(NOLOCK)
	WHERE EMail_Address LIKE '%@%'
	SELECT @InsertRowCount += @@ROWCOUNT

	IF @Debug =1
		PRINT 'Reseting Email_ Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Email_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Email__NextID'
	INSERT INTO IBaseM8Cur._Email__NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.6)Fraud_Ring
	IF @Debug =1
		PRINT 'Inserting Fraud_Ring'
	 
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Fraud_Ring'
	INSERT INTO IBaseM8Cur.Fraud_Ring(Unique_ID, AltEntity, Briefing_Document, Champion_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Fraud_Ring_ID, Fraud_Ring_Name, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Lead_Analyst, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_, Status_Binding, x5x5x5_Grading_412284402)
	SELECT
		Unique_ID COLLATE DATABASE_DEFAULT AS Unique_ID, 
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		NULL Briefing_Document,
		NULL Champion_,
		Create_Date AS Create_Date, 
		Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		0  Do_Not_Disseminate_412284494,	
		Unique_ID AS Fraud_Ring_ID, 
		Name_ COLLATE DATABASE_DEFAULT AS Fraud_Ring_Name, 
		IconColour AS IconColour, 
		NULL Key_Attractor_412284410,
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL Lead_Analyst,
		NULL MDA_Incident_ID_412284502,
		Record_Status AS Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 AS Source_411765484,
		SUBSTRING(DB_Source_387004609, 8, 8) AS Source_Description_411765489,
		LEFT(DB_Source_387004609, 6) AS Source_Reference_411765487, 
		NULL AS Status_,
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Fraud_Ring WITH(NOLOCK)
	SELECT @InsertRowCount += @@ROWCOUNT
		
	IF @Debug =1
		PRINT 'Reseting Fraud_Ring Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Fraud_Ring
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Fraud_Ring_NextID'
	INSERT INTO IBaseM8Cur._Fraud_Ring_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.7)Incident_
	IF @Debug =1
		PRINT 'Inserting Incident_'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Incident_'
	INSERT INTO IBaseM8Cur.Incident_(Unique_ID, AltEntity, Claim_Type, Create_Date, Create_User, 
														Do_Not_Disseminate_412284494, Fraud_Ring_Name, IconColour, IFB_Reference, Incident_Date, Incident_ID, Incident_Type, Keoghs_Elite_Reference, 
														Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, 
														Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT 
		'INC' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY CONVERT(INT, STUFF(Unique_ID, 1, 3, '')))) COLLATE DATABASE_DEFAULT AS Unique_ID,
		AltEntity,
		Claim_Type, 
		Create_Date, 
		Create_User, 
		Do_Not_Disseminate_412284494,
		Fraud_Ring_Name,
		IconColour,
		IFB_Reference, 
		Incident_Date, 
		Incident_ID, 
		Incident_Type, 
		Keoghs_Elite_Reference, 
		Key_Attractor_412284410,
		Last_Upd_Date, 
		Last_Upd_User, 
		MDA_Incident_ID_412284502,
		Record_Status,
		SCC, 
		@Source_411765484 AS Source_411765484,
		Source_Description_411765489, 
		Source_Reference_411765487,
		Status_Binding, 
		x5x5x5_Grading_412284402
	FROM (
		SELECT 
			Unique_ID COLLATE DATABASE_DEFAULT AS  Unique_ID, 
			'Incident' AS AltEntity, --ICON
			Claim_Type COLLATE DATABASE_DEFAULT AS Claim_Type, 
			Create_Date AS Create_Date, 
			Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
			0  Do_Not_Disseminate_412284494,
			Fraud_Ring_Name COLLATE DATABASE_DEFAULT AS Fraud_Ring_Name,
			IconColour AS IconColour,
			IFB_Ref COLLATE DATABASE_DEFAULT AS IFB_Reference, 
			Incident_Date AS Incident_Date, 
			Unique_ID COLLATE DATABASE_DEFAULT AS Incident_ID, 
			CONVERT(VARCHAR(255), 'Keoghs CFS') AS Incident_Type, 
			Keoghs_Reference_2 COLLATE DATABASE_DEFAULT AS Keoghs_Elite_Reference, --*******
			NULL Key_Attractor_412284410,
			Last_Upd_Date AS Last_Upd_Date, 
			Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
			NULL MDA_Incident_ID_412284502,
			Record_Status AS Record_Status,
			SCC COLLATE DATABASE_DEFAULT AS SCC, 
			0 Source_411765484,
			NULL Source_Description_411765489, 
			Keoghs_Reference_2 COLLATE DATABASE_DEFAULT AS  Source_Reference_411765487,
			Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
			NULL x5x5x5_Grading_412284402
		FROM 
			IBaseM5Cur.ClaimFile_ WITH(NOLOCK)

		UNION ALL

		SELECT  
			Unique_ID COLLATE DATABASE_DEFAULT AS  Unique_ID, 
			'MIAFTR' AS AltEntity, --ICON
			NULL AS Claim_Type, 
			Create_Date AS Create_Date, 
			Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
			0 Do_Not_Disseminate_412284494,
			NULL AS Fraud_Ring_Name,
			NULL AS IconColour, 
			NULL AS IFB_Reference,
			Incident_Date AS Incident_Date, 
			Unique_ID COLLATE DATABASE_DEFAULT AS Incident_ID, 
			Match_Type COLLATE DATABASE_DEFAULT AS Incident_Type, --*************
			NULL AS Keoghs_Elite_Reference, 
			NULL Key_Attractor_412284410,
			Last_Upd_Date AS Last_Upd_Date, 
			Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
			NULL MDA_Incident_ID_412284502,
			Record_Status AS Record_Status, 
			SCC COLLATE DATABASE_DEFAULT AS SCC, 
			NULL Source_411765484, 
			'CUE/MIAFTR Match' Source_Description_411765489, 
			DB_Source_387004609 Source_Reference_411765487, 
			Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
			NULL x5x5x5_Grading_412284402
		FROM 
			IBaseM5Cur.MIAFTR_Match WITH(NOLOCK)
	) AS X
	SELECT @InsertRowCount += @@ROWCOUNT


	IF @Debug =1
		PRINT  'Reseting Incident_ Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Incident_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Incident__NextID'
	INSERT INTO IBaseM8Cur._Incident__NextID(NextID)
	SELECT ISNULL(@NextID, 1)
		
	--2.8)Intelligence_
	IF @Debug =1
		PRINT 'Inserting Intelligence_'
	 
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Intelligence_'
	INSERT INTO IBaseM8Cur.Intelligence_(
		Unique_ID, AltEntity, Create_Date, Create_User, Description_, 
		Do_Not_Disseminate_412284494, Document_, Document__Binding, IconColour, Intelligence_ID, 
		Key_Attractor_412284410, Key_Attractors, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, 
		Picture_, Picture__Binding, Picture_2, Picture_2_Binding, Record_Status, 
		Report_, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, 
		Type_, Web_Page, x5x5x5_Grading_412284402)

	SELECT 
		Unique_ID COLLATE DATABASE_DEFAULT AS Unique_ID, 
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity,
		Create_Date AS Create_Date, 
		Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		Description_ COLLATE DATABASE_DEFAULT AS Description_,
		0  Do_Not_Disseminate_412284494,
		CONVERT(VARBINARY(MAX), Word_Document) AS Document_, 
		Word_Document_Binding Document__Binding,
		IconColour AS IconColour,
		Unique_ID AS Intelligence_ID, 
		NULL Key_Attractor_412284410,
		Key_Attractors COLLATE DATABASE_DEFAULT AS Key_Attractors, 
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		CONVERT(VARBINARY(MAX), Pictures_) AS Picture_,
		Pictures__Binding COLLATE DATABASE_DEFAULT AS Picture__Binding, 
		CONVERT(VARBINARY(MAX), Capture_) Picture_2,
		Capture__Binding Picture_2_Binding,
		Record_Status AS Record_Status, 
		Intelligence_Note AS Report_,
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 AS Source_411765484,
		DB_Source_387004609 AS Source_Description_411765489, 
		CONVERT(VARCHAR(50), Source_) AS Source_Reference_411765487,  
		--Intelligence' records which contain a 'Source' 
		--should display in the new schema as 'Source Reference', 
		--however currently the data is is not being displayed.
			
		--Intelligence' records which contain a 'Source' 
		--should display in the new schema as 'Source Reference', 
		--however currently the data is is not being displayed.
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		Intelligence_Type COLLATE DATABASE_DEFAULT AS Type_, 
		Web_Page COLLATE DATABASE_DEFAULT AS Web_Page,
		Grading_ x5x5x5_Grading_412284402 
	FROM IBaseM5Cur.Intelligence_ WITH(NOLOCK)
	SELECT @InsertRowCount += @@ROWCOUNT
		
	IF @Debug =1
		PRINT 'Reseting Intelligence_ Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Intelligence_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Intelligence__NextID'
	INSERT INTO IBaseM8Cur._Intelligence__NextID(NextID)
	SELECT ISNULL(@NextID, 1)
		
	--2.9)IP_Address
	IF @Debug =1
		PRINT 'Inserting IP_Address'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.IP_Address'
	INSERT INTO IBaseM8Cur.IP_Address
		(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, IP_Address, IP_Address_ID, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT
		'IP_' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY CONVERT(INT, STUFF(Unique_ID, 1, 3, '')))) COLLATE DATABASE_DEFAULT AS Unique_ID,
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Create_Date AS Create_Date, 
		Create_User COLLATE DATABASE_DEFAULT AS Create_User,
		0  Do_Not_Disseminate_412284494,
		IconColour IconColour,
		IP_Address COLLATE DATABASE_DEFAULT AS IP_Address,
		Unique_ID COLLATE DATABASE_DEFAULT AS  IP_Address_ID, 
		NULL Key_Attractor_412284410,
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		Record_Status Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 AS Source_411765484, 
		NULL AS Source_Description_411765489,
		NULL AS Source_Reference_411765487,
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Telephone_ WITH(NOLOCK)
	WHERE AltEntity = 'PC'
	AND IP_Address != 'deleteme'
	AND IP_Address  NOT LIKE '%BB%'
	SELECT @InsertRowCount += @@ROWCOUNT

	IF @Debug =1
		PRINT 'Reseting IP_Address Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.IP_Address
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._IP_Address_NextID'
	INSERT INTO IBaseM8Cur._IP_Address_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.10)Keoghs_Case
	IF @Debug =1
		PRINT 'Inserting Keoghs_Case'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Keoghs_Case'
	INSERT INTO IBaseM8Cur.Keoghs_Case
	(Unique_ID, AltEntity, Case_Status, Client_Claims_Handler, Client_Reference, 
	Clients_, Closure_Date, Create_Date, Create_User, Current_Reserve, 
	Do_Not_Disseminate_412284494, Fee_Earner, IconColour, Keoghs_Case_ID, Keoghs_Elite_Reference, 
	Keoghs_Office, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, 
	Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, 
	Status_Binding, Weed_Date, x5x5x5_Grading_412284402)
	SELECT
		STUFF(Unique_ID, 1, 3, 'KEO') COLLATE DATABASE_DEFAULT AS  Unique_ID, 
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Status_ COLLATE DATABASE_DEFAULT AS Case_Status, 
		Insurer_Claims_Handler COLLATE DATABASE_DEFAULT AS Client_Claims_Handler, 
		Insurer_Reference COLLATE DATABASE_DEFAULT AS Client_Reference, 
		Insurer_ COLLATE DATABASE_DEFAULT AS Clients_, 
		Closure_Date AS Closure_Date,
		Create_Date AS Create_Date, 
		Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		Current_Estimate Current_Reserve,
		0 Do_Not_Disseminate_412284494,
		Keoghs_Fee_Earner COLLATE DATABASE_DEFAULT AS Fee_Earner,
		IconColour AS IconColour,
		Unique_ID COLLATE DATABASE_DEFAULT AS  Keoghs_Case_ID, 
		Keoghs_Reference_2 COLLATE DATABASE_DEFAULT AS Keoghs_Elite_Reference,
		Keoghs_Office COLLATE DATABASE_DEFAULT AS Keoghs_Office, 
		NULL Key_Attractor_412284410,
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		Record_Status AS Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 AS Source_411765484,
		NULL AS Source_Description_411765489, 
		Keoghs_Reference_2 COLLATE DATABASE_DEFAULT AS   Source_Reference_411765487, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL Weed_Date,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.ClaimFile_ WITH(NOLOCK)
	SELECT @InsertRowCount += @@ROWCOUNT
		
	IF @Debug =1
		PRINT 'Reseting Keoghs_Case Counter'
		
	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Keoghs_Case
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Keoghs_Case_NextID'
	INSERT INTO IBaseM8Cur._Keoghs_Case_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.11)NI_Number_
	IF @Debug =1
		PRINT 'Inserting NI_Number'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.NI_Number'
	INSERT INTO IBaseM8Cur.NI_Number(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, NI_Number, NI_Number_ID, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT
		'NI_' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY CONVERT(INT, STUFF(Unique_ID, 1, 3, '')))) COLLATE DATABASE_DEFAULT AS Unique_ID,
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Create_Date AS Create_Date, 
		Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		0  Do_Not_Disseminate_412284494,
		IconColour AS IconColour, 
		NULL Key_Attractor_412284410,
		Last_Upd_Date AS Last_Upd_Date,
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		NI_Number COLLATE DATABASE_DEFAULT AS NI_Number,
		Unique_ID COLLATE DATABASE_DEFAULT AS   NI_Number_ID, 
		Record_Status AS Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 AS Source_411765484,
		NULL AS Source_Description_411765489, 
		DB_Source_387004609 AS Source_Reference_411765487, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Person_ WITH(NOLOCK)
	WHERE NI_Number IS NOT NULL
	SELECT @InsertRowCount += @@ROWCOUNT

	IF @Debug =1
		PRINT 'Reseting NI_Number Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.NI_Number
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._NI_Number_NextID'
	INSERT INTO IBaseM8Cur._NI_Number_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.12)Organisation_
	IF @Debug =1
		PRINT  'Inserting Organisation_'
	 
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Organisation_'
	INSERT INTO IBaseM8Cur.Organisation_(Unique_ID, AltEntity, Create_Date, Create_User, Credit_Licence, Do_Not_Disseminate_412284494, Effective_Date_of_Status, IconColour, Incorporated_Date, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, MoJ_CRM_Number, MoJ_CRM_Status, Organisation_ID, Organisation_Name, Organisation_Status, Organisation_Type, Record_Status, Registered_Number, SCC, SIC_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, VAT_Number, VAT_Number_Validated_Date, x5x5x5_Grading_412284402)
	SELECT
		Unique_ID COLLATE DATABASE_DEFAULT AS Unique_ID, 
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Create_Date	AS Create_Date, 
		Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		NULL AS Credit_Licence,
		0  Do_Not_Disseminate_412284494,
		NULL AS	Effective_Date_of_Status, 
		IconColour AS IconColour, 
		NULL AS Incorporated_Date, 
		NULL Key_Attractor_412284410,
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS  Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		NULL AS MoJ_CRM_Number,
		NULL AS  MoJ_CRM_Status,
		Unique_ID COLLATE DATABASE_DEFAULT AS Organisation_ID,  
		Name_ COLLATE DATABASE_DEFAULT AS Organisation_Name, 
		NULL AS Organisation_Status,
		NULL AS  Organisation_Type, 
		Record_Status AS Record_Status, 
		Registered_No COLLATE DATABASE_DEFAULT AS Registered_Number, 
		SCC	COLLATE DATABASE_DEFAULT AS  SCC, 
		NULL AS SIC_, 
		@Source_411765484 AS Source_411765484,
		NULL AS Source_Description_411765489,
		DB_Source_387004609 AS Source_Reference_411765487, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		VAT_No COLLATE DATABASE_DEFAULT AS VAT_Number, 
		NULL AS VAT_Number_Validated_Date,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Organisation_ WITH(NOLOCK)
	SELECT @InsertRowCount += @@ROWCOUNT
		
	IF @Debug =1
		PRINT 'Reseting Organisation_ Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Organisation_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Organisation__NextID'
	INSERT INTO IBaseM8Cur._Organisation__NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.13)Passport_
	IF @Debug =1
		PRINT 'Inserting Passport_'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Passport_'
	INSERT INTO IBaseM8Cur.Passport_
	(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Passport_ID, Passport_Number, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT 
		'PAS' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY CONVERT(INT, STUFF(Unique_ID, 1, 3, '')))) COLLATE DATABASE_DEFAULT AS Unique_ID,
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Create_Date AS Create_Date, 
		Create_User	COLLATE DATABASE_DEFAULT AS Create_User, 
		0  Do_Not_Disseminate_412284494,
		IconColour AS IconColour, 
		NULL Key_Attractor_412284410,
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		Unique_ID COLLATE DATABASE_DEFAULT AS Passport_ID, 
		Passport_Number	COLLATE DATABASE_DEFAULT AS Passport_Number,
		Record_Status AS Record_Status, 
		SCC	COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 AS Source_411765484,
		NULL AS Source_Description_411765489,
		DB_Source_387004609 COLLATE DATABASE_DEFAULT AS Source_Reference_411765487, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Person_ WITH(NOLOCK)
	WHERE Passport_Number IS NOT NULL 
	AND Unique_ID != 'PER125009'
	SELECT @InsertRowCount += @@ROWCOUNT

	IF @Debug =1
		PRINT  '<<< ID:PER125009 PASSPORT NUMBER:P<POLSKARZYNSKI<<MICHAL AM40428022POL8205029M1509304 (not inserted string data would be truncated)>>>'

	IF @Debug =1
		PRINT  'Reseting Passport_ Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Passport_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Passport__NextID'
	INSERT INTO IBaseM8Cur._Passport__NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.14)Payment_Card
	IF @Debug =1
		PRINT 'Inserting Payment_Card'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Payment_Card'
	INSERT INTO IBaseM8Cur.Payment_Card
		(Unique_ID, AltEntity, Bank_, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Payment_Card_ID, Payment_Card_Number, Payment_Card_Type, Record_Status, SCC, Sort_Code, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT
		STUFF(Unique_ID, 1, 3, 'PAY') COLLATE DATABASE_DEFAULT AS  Unique_ID, 
		AltEntity	COLLATE DATABASE_DEFAULT AS AltEntity, 
		Bank_	COLLATE DATABASE_DEFAULT AS Bank_, 
		Create_Date	AS Create_Date, 
		Create_User	COLLATE DATABASE_DEFAULT AS Create_User, 
		0  Do_Not_Disseminate_412284494,
		IconColour	AS IconColour, 
		NULL Key_Attractor_412284410,
		Last_Upd_Date	AS Last_Upd_Date, 
		Last_Upd_User	COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		Unique_ID	COLLATE DATABASE_DEFAULT AS Payment_Card_ID,
		--REF 201305211036
		Card_Number	COLLATE DATABASE_DEFAULT AS Payment_Card_Number, 
		NULL AS Payment_Card_Type,
		Record_Status	AS Record_Status, 
		SCC	COLLATE DATABASE_DEFAULT AS SCC, 
		Sort_Code	COLLATE DATABASE_DEFAULT AS Sort_Code, 
		@Source_411765484 AS Source_411765484, 
		NULL AS Source_Description_411765489, 
		NULL AS Source_Reference_411765487, 
		Status_Binding	COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Account_ WITH(NOLOCK)
	WHERE AltEntity  = 'Credit'
	SELECT @InsertRowCount += @@ROWCOUNT
		
	IF @Debug =1
		PRINT 'Reseting Payment_Card Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Payment_Card
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Payment_Card_NextID'
	INSERT INTO IBaseM8Cur._Payment_Card_NextID(NextID)
	SELECT ISNULL(@NextID, 1)
		
	--2.15)Person_
	IF @Debug =1
		PRINT 'Inserting Person_'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Person_'
	INSERT INTO IBaseM8Cur.Person_
		(Unique_ID, AltEntity, Create_Date, Create_User, Date_of_Birth, Do_Not_Disseminate_412284494, Document_, Document__Binding, First_Name, Gender_, Hobbies_, IconColour, Key_Attractor_412284410, Last_Name, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Middle_Name, Nationality_, Notes_, Occupation_, Person_ID, Picture_, Picture__Binding, Record_Status, Salutation_, SCC, Schools_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Taxi_Driver_Licence, VF_ID, x5x5x5_Grading_412284402)
	SELECT
		Unique_ID AS Unique_ID, 
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Create_Date AS Create_Date, 
		Create_User	COLLATE DATABASE_DEFAULT AS Create_User, 
		Date_Of_Birth AS Date_of_Birth, 
		0 Do_Not_Disseminate_412284494,
		CONVERT(VARBINARY,	Document_)AS Document_ ,
		NULL Document__Binding,
		Forename_1 COLLATE DATABASE_DEFAULT	AS First_Name, 
		AltEntity COLLATE DATABASE_DEFAULT AS Gender_,
		Hobbies_ COLLATE DATABASE_DEFAULT AS Hobbies_, 
		IconColour AS IconColour,
		NULL Key_Attractor_412284410,
		Surname_ COLLATE DATABASE_DEFAULT AS Last_Name, 
		Last_Upd_Date AS Last_Upd_Date,
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		Middlename_	COLLATE DATABASE_DEFAULT AS Middle_Name, 
		Nationality_ COLLATE DATABASE_DEFAULT AS Nationality_, 
		Comments_ COLLATE DATABASE_DEFAULT AS Notes_, 
		Occupation_	COLLATE DATABASE_DEFAULT AS Occupation_, 
		Unique_ID AS Person_ID, 
		Picture_ AS Picture_, 
		Picture__Binding COLLATE DATABASE_DEFAULT AS Picture__Binding,
		Record_Status AS Record_Status, 
		Title_ AS Salutation_,
		SCC	COLLATE DATABASE_DEFAULT AS SCC, 
		SchoolsUniversityColleges_ COLLATE DATABASE_DEFAULT AS Schools_, 
		@Source_411765484 AS Source_411765484, 
		NULL AS Source_Description_411765489, 
		DB_Source_387004609 AS Source_Reference_411765487, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL AS Taxi_Driver_Licence, 
		NULL VF_ID,
		NULL x5x5x5_Grading_412284402
	 FROM IBaseM5Cur.Person_ WITH(NOLOCK)
	 SELECT @InsertRowCount += @@ROWCOUNT
	 
	IF @Debug =1
		PRINT 'Reseting Person_ Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Person_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Person__NextID'
	INSERT INTO IBaseM8Cur._Person__NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.16)Policy_
	IF @Debug =1
		PRINT  'Inserting Policy_'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Policy_'
	INSERT INTO IBaseM8Cur.Policy_
	(Unique_ID, AltEntity, Broker_, Cover_Type, Create_Date, 
	Create_User, Do_Not_Disseminate_412284494, IconColour, Insurer_, Insurer_Trading_Name, 
	Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Policy_End_Date, 
	Policy_ID, Policy_Number, Policy_Start_Date, Policy_Type, Premium_, 
	Previous_Claims_Disclosed, Previous_Fault_Claims_Disclosed, Record_Status, SCC, Source_411765484, 
	Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT
		STUFF(Unique_ID, 1, 3, 'POL') COLLATE DATABASE_DEFAULT AS  Unique_ID,
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		NULL AS Broker_,
		NULL AS Cover_Type,
		Create_Date AS Create_Date,
		Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		0 Do_Not_Disseminate_412284494,
		IconColour AS IconColour, 
		LEFT(Insurer_,50) COLLATE DATABASE_DEFAULT AS Insurer_,
		NULL AS Insurer_Trading_Name, 
		NULL Key_Attractor_412284410,
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User,
		NULL MDA_Incident_ID_412284502,
		Cancellation_Date AS Policy_End_Date, 
		Unique_ID COLLATE DATABASE_DEFAULT AS Policy_ID, 
		Policy_Number COLLATE DATABASE_DEFAULT AS Policy_Number, 
		Inception_Date AS Policy_Start_Date, 
		Type_ COLLATE DATABASE_DEFAULT AS Policy_Type, 
		NULL AS Premium_,
		NULL AS Previous_Claims_Disclosed, 
		NULL AS Previous_Fault_Claims_Disclosed,
		Record_Status AS Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 AS Source_411765484, 
		NULL AS Source_Description_411765489, 
		NULL AS Source_Reference_411765487, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Insurance_Policy WITH(NOLOCK)
	SELECT @InsertRowCount += @@ROWCOUNT
		
	IF @Debug =1
		PRINT 'Reseting Policy_ Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Policy_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Policy__NextID'
	INSERT INTO IBaseM8Cur._Policy__NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.17)Telephone_ 
	IF @Debug =1
		PRINT  'Inserting Telephone_'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Telephone_'
	INSERT INTO IBaseM8Cur.Telephone_ (
	Unique_ID, AltEntity, Country_Code, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, STD_Code, Telephone_ID, Telephone_Number, Telephone_Type, x5x5x5_Grading_412284402)
	SELECT
		'TEL' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY CONVERT(INT, STUFF(UNIQUE_ID, 1, 3, '')))) COLLATE DATABASE_DEFAULT AS Unique_ID,
		AltEntity,
		Country_Code, 
		Create_Date, 
		Create_User, 
		Do_Not_Disseminate_412284494,
		IconColour, 
		Key_Attractor_412284410,
		Last_Upd_Date, 
		Last_Upd_User, 
		MDA_Incident_ID_412284502,
		Record_Status, 
		SCC, 
		Source_411765484,
		Source_Description_411765489, 
		Source_Reference_411765487, 
		Status_Binding, 
		STD_Code, 
		Telephone_ID, 
		Telephone_Number,
		Telephone_Type,
		x5x5x5_Grading_412284402
		FROM(
	SELECT 
		ROW_NUMBER() OVER(PARTITION BY UNIQUE_ID ORDER BY UNIQUE_ID) CNT,
		 UNIQUE_ID,
		 AltEntity COLLATE DATABASE_DEFAULT AS AltEntity,
		 CASE WHEN Code IS NOT NULL THEN '+44' END AS Country_Code, 
		 Create_Date AS Create_Date, 
		 Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		 0 Do_Not_Disseminate_412284494,
		 IconColour AS IconColour, 
		 NULL Key_Attractor_412284410,
		 Last_Upd_Date AS Last_Upd_Date, 
		 Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		 NULL MDA_Incident_ID_412284502,
		 Record_Status AS Record_Status, 
		 SCC COLLATE DATABASE_DEFAULT AS SCC, 
		 @Source_411765484 AS Source_411765484,
		 NULL AS Source_Description_411765489, 
		 NULL AS Source_Reference_411765487, 
		 Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		 CASE WHEN Code IS NOT NULL THEN '0' + Code END COLLATE DATABASE_DEFAULT AS STD_Code, 
		 Unique_ID AS Telephone_ID, 
		 Number_ AS Telephone_Number,
		 CASE AltEntity
			WHEN 'Cellphone' THEN 'Mobile' 
			WHEN 'Telephone' THEN 'Landline'
			WHEN 'Fax' THEN 'FAX'
			ELSE 'Unknown'
		 END AS Telephone_Type,
		 NULL x5x5x5_Grading_412284402
	 FROM IBaseM5Cur.Telephone_  WITH(NOLOCK)
	LEFT JOIN IBaseM.[UKAreaCodes] ON LEFT(Number_, LEN(Code) + 1) COLLATE DATABASE_DEFAULT = '0' + Code
	 WHERE AltEntity IN('Telephone', 'Cellphone', 'Fax')

	) AS X
	WHERE CNT = 1
	SELECT @InsertRowCount += @@ROWCOUNT

	UPDATE IBaseM8Cur.Telephone_
	SET  AltEntity = 'Unknown'
		,Telephone_Type  = 'Unknown'
	WHERE Telephone_Type  != 'Mobile'
	AND LEFT(Telephone_Number,2) = '07'

	UPDATE IBaseM8Cur.Telephone_
	SET  AltEntity = 'Unknown'
		,Telephone_Type  = 'Unknown'
	WHERE Telephone_Type  = 'Mobile'
	AND LEFT(Telephone_Number,2) != '07'

	IF @Debug =1
		PRINT  'Reseting Telephone_ Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Telephone_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Telephone__NextID'
	INSERT INTO IBaseM8Cur._Telephone__NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.18)Vehicle_
	IF @Debug =1
		PRINT  'Inserting Vehicle_'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Vehicle_'
	INSERT INTO IBaseM8Cur.Vehicle_
		(Unique_ID, AltEntity, Colour_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engine_Capacity, Fuel_, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Make_, MDA_Incident_ID_412284502, Model_, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Transmission_, Vehicle_ID, Vehicle_Registration, Vehicle_Type, VIN_, x5x5x5_Grading_412284402)	
	SELECT
		Unique_ID COLLATE DATABASE_DEFAULT AS Unique_ID, 
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Colour_ COLLATE DATABASE_DEFAULT AS Colour_, 
		Create_Date AS Create_Date,
		Create_User COLLATE DATABASE_DEFAULT AS Create_User,
		0 Do_Not_Disseminate_412284494,
		NULL AS Engine_Capacity, 
		NULL AS Fuel_, 
		IconColour AS IconColour, 
		NULL Key_Attractor_412284410,
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		Make_ COLLATE DATABASE_DEFAULT AS Make_, 
		NULL MDA_Incident_ID_412284502,
		Model_ COLLATE DATABASE_DEFAULT AS Model_, 
		NULL AS Notes_,
		Record_Status AS Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 AS Source_411765484, 
		NULL AS Source_Description_411765489, 
		NULL AS Source_Reference_411765487, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		NULL AS Transmission_, 
		Unique_ID AS Vehicle_ID, 
		Registration_No COLLATE DATABASE_DEFAULT AS Vehicle_Registration, 
		'Car' AS Vehicle_Type, 
		NULL AS VIN_, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Vehicle_ WITH(NOLOCK)
	SELECT @InsertRowCount += @@ROWCOUNT
			
	IF @Debug =1
		PRINT  'Reseting Vehicle_ Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Vehicle_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Vehicle__NextID'
	INSERT INTO IBaseM8Cur._Vehicle__NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.19)Website_
	IF @Debug =1
		PRINT 'Inserting Website_'

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Website_'
	INSERT INTO IBaseM8Cur.Website_
		(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Website_, Website_ID, x5x5x5_Grading_412284402)
	SELECT
		'WEB' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY CONVERT(INT, STUFF(Unique_ID, 1, 3, '')))) COLLATE DATABASE_DEFAULT AS Unique_ID,
		AltEntity COLLATE DATABASE_DEFAULT AS AltEntity, 
		Create_Date AS Create_Date, 
		Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		0 Do_Not_Disseminate_412284494,
		IconColour AS IconColour, 
		NULL Key_Attractor_412284410,
		Last_Upd_Date AS Last_Upd_Date, 
		Last_Upd_User COLLATE DATABASE_DEFAULT AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		Record_Status AS Record_Status, 
		SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source_411765484 AS Source_411765484, 
		NULL AS Source_Description_411765489, 
		NULL AS Source_Reference_411765487, 
		Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		Web_Site COLLATE DATABASE_DEFAULT AS Website_,
		Unique_ID COLLATE DATABASE_DEFAULT AS  Website_ID, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Telephone_	 WITH(NOLOCK)
	WHERE Web_Site IS NOT NULL AND Web_Site != 'deleteme'
	SELECT @InsertRowCount += @@ROWCOUNT

	IF @Debug =1
		PRINT 'Reseting Website_ Counter'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Website_
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Website__NextID'
	INSERT INTO IBaseM8Cur._Website__NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
END CATCH


