﻿
/****** Object:  StoredProcedure [IBaseM].[uspTruncateTableSynonym]    Script Date: 03/14/2014 14:44:07 ******/
CREATE PROCEDURE [IBaseM].[uspRebuildTableIndexAllSynonym] 
/**************************************************************************************************/
-- ObjectName:			
--
-- Description:			
--
-- Usage:				
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
/**************************************************************************************************/
(
 @Synonym		SYSNAME,
 @IndexName		VARCHAR(100)
)
AS
SET NOCOUNT ON
BEGIN TRY

	DECLARE     @stmt NVARCHAR(MAX)
	SELECT @stmt = 'ALTER INDEX ' + @IndexName + ' ON ' + base_object_name + ' REBUILD'
	FROM sys.synonyms SYN
	INNER JOIN sys.schemas sch ON sch.schema_id=syn.schema_id
	WHERE sch.name + '.' + syn.name = @Synonym

	EXEC sp_executesql @stmt
END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH


