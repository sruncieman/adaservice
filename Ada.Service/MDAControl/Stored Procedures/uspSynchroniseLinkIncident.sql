﻿

CREATE PROCEDURE[Sync].[uspSynchroniseLinkIncident]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseLinkAccount]		
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--						2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)MDAIBase: LinkTablesChanges
--							3.2.1)ActionTheLinkChangeInMDA_IBase
--							3.2.2)DoDeletes
--							3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						3.3)MDAIBase: LinkEndTablesChanges
--							3.3.1)PopulateTheWorkingTable
--							3.3.2)ActionTheLinkEndChangeInMDA_IBase
--							3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
--						4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
--							4.1)ActionTheChangeInMDA
--							4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
--							4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						5)UpdateLoggingAndTidy	
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-04-28			Paul Allen		Created
--		2015-01-26			Paul Allen		Bug 11784 - IBaseID wrongly being set within Incident2Organisation.
--											Added where criteria to section 3.2.3
--		2015-06-10			Paul Allen		Added 4 Fields to Incident2Person
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT						
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT
			,@PhysicalTableName		VARCHAR(255) = 'Incident_Link'
			,@Prefix				VARCHAR(3)   
			,@LinkTypeID			INT

	SELECT @LinkTypeID =Table_ID, @Prefix = TableCode FROM [MDA_IBase].[dbo]._DataTable WHERE PhysicalName = @PhysicalTableName

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1) PRIMARY KEY, ChangeDatabase VARCHAR(10), PhysicalTableName VARCHAR(255), MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging
	CREATE TABLE #LinkEndStaging (Link_ID NVARCHAR(50), Entity_ID1 NVARCHAR(50), Confidence TINYINT, Direction TINYINT, Entity_ID2 NVARCHAR(50), EntityType_ID1 INT , EntityType_ID2 INT, LinkType_ID INT, Record_Status TINYINT, Record_Type TINYINT, SCC NVARCHAR(255))
					
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 101 --SynchroniseIncidentLink
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Incident2Person', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA].[dbo].Incident2Person, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Incident2Organisation', CT.ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA].[dbo].Incident2Organisation, @MDAChangeVersionFrom) CT
	INNER JOIN [MDA].[dbo].Incident2Organisation I2O ON I2O.ID = CT.ID
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL
	AND ISNULL(I2O.IBaseID,'') != 'IN0' --Filter Out Cue

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Incident_Link', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA_IBase].[dbo].Incident_Link, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Link_End', Link_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA_IBase].[dbo]._LinkEnd, @MDA_IBaseChangeVersionFrom) CT
	INNER JOIN [MDA_IBase].[dbo].Incident_Link IL ON IL.Unique_ID = CT.Link_ID
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL
	GROUP BY Link_ID, Sys_Change_Operation

	--2.2)AddSomeIndexesToMakeTheProcessQuicker 
	SET @Step ='2.2';

	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT (MDA_IBase_Unique_ID ASC)
	CREATE NONCLUSTERED INDEX IX_CT_PhysicalTableName ON #CT (PhysicalTableName ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3';

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IIL.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].Incident_Link IIL ON IIL.Unique_ID = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Incident_Link'
	AND IIL.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IVSL.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[VW_StagingLinks] IVSL ON IVSL.[IBase8LinkID] = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Link_End'
	AND IVSL.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MI2P.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].Incident2Person MI2P ON MI2P.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Incident2Person'

	UPDATE CT
	SET MDA_IBase_Unique_ID = MI2O.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].Incident2Organisation MI2O ON MI2O.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Incident2Organisation'

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4';

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				AND PhysicalTableName != 'Link_End'
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5';

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1
	
	--2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
	SET @Step ='2.6';
	UPDATE #CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT	RowID
				FROM	(
						SELECT   RowID
								,ROW_NUMBER() OVER (PARTITION BY MDA_IBase_Unique_ID ORDER BY RowID) RowNumber
						FROM #CT CT 
						WHERE EXISTS (SELECT TOP 1 1 FROM #CT CT1 WHERE MDA_IBase_Unique_ID = MDA_IBase_Unique_ID AND ChangeDatabase = 'MDA_IBase' GROUP BY MDA_IBase_Unique_ID HAVING COUNT(1) > 1)
						) Data 
				WHERE RowNumber !=1
				) IQ ON IQ.RowID = CT.RowID
	WHERE ChangeDatabase = 'MDA_IBase'

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1';

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM [MDA_IBase].[dbo].[_Incident_Link_NextID]
		UPDATE [MDA_IBase].[dbo].[_Incident_Link_NextID] SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	 @Prefix + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY RowID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--------------------------------------------------------------------------
		--3.2)MDAIBase: LinkTablesChanges
		--------------------------------------------------------------------------
		--3.2.1)ActionTheLinkChangeInMDA_IBase
		SET @Step ='3.2.1';

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[Incident_Link] [Target]
		USING  (
				SELECT *
				FROM	(
						SELECT ROW_NUMBER() OVER (PARTITION BY MDA_IBase_Unique_ID ORDER BY CT.RowID) RowNumber,  CT.PhysicalTableName, CT.MDA_ID, CT.MDA_IBase_Unique_ID, IQ.*
						FROM #CT CT
						INNER JOIN	(
									SELECT RowID, MI2P.ID, [IBaseId] Unique_ID, [AccidentManagement] Accident_Management, [AmbulanceAttended] Ambulance_Attended, [Broker] Broker_, [ClaimCode] Claim_Code, [ClaimNotificationDate] Claim_Notification_Date, [ClaimNumber] Claim_Number, [ClaimType] Claim_Type, [CreatedDate] Create_Date, [CreatedBy] Create_User, [Engineer] Engineer_, [Hire] Hire_, [IncidentCircs] Incident_Circs, [IncidentLinkId] Incident_Link_ID, [IncidentLocation] Incident_Location, [IncidentTime] Incident_Time, [Insurer] Insurer_, [ModifiedDate] Last_Upd_Date, [ModifiedBy] Last_Upd_User, [MedicalExaminer] Medical_Examiner, [PaymentsToDate] Payments_, [PoliceAttended] Police_Attended, [PoliceForce] Police_Force, [PoliceReference] Police_Reference, MI2P.[RecordStatus] Record_Status,[Recovery] Recovery_, [ReferralSource] Referral_Source, [Repairer] Repairer_, [Reserve] Reserve_, [Solicitors] Solicitors_, [Source] Source_411765484, [SourceDescription] Source_Description_411765489, [SourceReference] Source_Reference_411765487, [Storage] Storage_, [StorageAddress] Storage_Address, [FiveGrading] x5x5x5_Grading_412284402, [UndefinedSupplier] Undefined_Supplier, [AttendedHospital] Attended_Hospital, [RiskClaim_Id] Risk_Claim_ID_414244883, ClaimStatus_Id, MCS.Text Claim_Status, MojStatus_Id ,MMS.Text [MoJ_Status], PartyType_Id, MPT.PartyTypeText [Party_Type], SubPartyType_Id, MSPT.SubPartyText [Sub_Party_Type],[MedicalLegal] [Medical_Legal], [RecoveryAddress] [Recovery_Address], [RepairerAddress] [Repairer_Address], [InspectionAddress] [Inspection_Address], [SourceClaimStatus] [Source_Claim_Status], [TotalClaimCost] [Total_Claim_Cost], [TotalClaimCostLessExcess] [Total_Claim_Cost_Less_Excess], [BypassFraud] [Bypass_Fraud]
									FROM [MDA].[dbo].Incident2Person MI2P
									INNER JOIN #CT CT ON CT.MDA_ID = MI2P.ID
									INNER JOIN [MDA].[dbo].[ClaimStatus] MCS ON MCS.Id = MI2P.ClaimStatus_Id
									INNER JOIN [MDA].[dbo].[MojStatus] MMS ON MMS.Id = MI2P.MojStatus_Id
									INNER JOIN [MDA].[dbo].[PartyType] MPT ON MPT.Id = MI2P.PartyType_Id
									INNER JOIN [MDA].[dbo].[SubPartyType] MSPT ON  MSPT.Id = MI2P.SubPartyType_Id
									WHERE CT.PhysicalTableName = 'Incident2Person'
									UNION ALL
									SELECT RowID, MI2O.ID, [IBaseId] Unique_ID, [AccidentManagement] Accident_Management, [AmbulanceAttended] Ambulance_Attended, [Broker] Broker_, [ClaimCode] Claim_Code, [ClaimNotificationDate] Claim_Notification_Date, [ClaimNumber] Claim_Number, [ClaimType] Claim_Type, [CreatedDate] Create_Date, [CreatedBy] Create_User, [Engineer] Engineer_, [Hire] Hire_, [IncidentCircs] Incident_Circs, [IncidentLinkId] Incident_Link_ID, [IncidentLocation] Incident_Location, [IncidentTime] Incident_Time, [Insurer] Insurer_, [ModifiedDate] Last_Upd_Date, [ModifiedBy] Last_Upd_User, [MedicalExaminer] Medical_Examiner, [PaymentsToDate] Payments_, [PoliceAttended] Police_Attended, [PoliceForce] Police_Force, [PoliceReference] Police_Reference, MI2O.[RecordStatus] Record_Status,[Recovery] Recovery_, [ReferralSource] Referral_Source, [Repairer] Repairer_, [Reserve] Reserve_, [Solicitors] Solicitors_, [Source] Source_411765484, [SourceDescription] Source_Description_411765489, [SourceReference] Source_Reference_411765487, [Storage] Storage_, [StorageAddress] Storage_Address, [FiveGrading] x5x5x5_Grading_412284402, [UndefinedSupplier] Undefined_Supplier, [AttendedHospital] Attended_Hospital, [RiskClaim_Id] Risk_Claim_ID_414244883, ClaimStatus_Id, MCS.Text Claim_Status, MojStatus_Id ,MMS.Text [MoJ_Status], PartyType_Id, MPT.PartyTypeText [Party_Type], SubPartyType_Id, MSPT.SubPartyText [Sub_Party_Type],[MedicalLegal] [Medical_Legal], [RecoveryAddress] [Recovery_Address], [RepairerAddress] [Repairer_Address], [InspectionAddress] [Inspection_Address], NULL [Source_Claim_Status], NULL [Total_Claim_Cost], NULL [Total_Claim_Cost_Less_Excess], NULL [Bypass_Fraud]
									FROM [MDA].[dbo].Incident2Organisation MI2O
									INNER JOIN #CT CT ON CT.MDA_ID = MI2O.ID
									INNER JOIN [MDA].[dbo].[ClaimStatus] MCS ON MCS.Id = MI2O.ClaimStatus_Id
									INNER JOIN [MDA].[dbo].[MojStatus] MMS ON MMS.Id = MI2O.MojStatus_Id
									INNER JOIN [MDA].[dbo].[PartyType] MPT ON MPT.Id = MI2O.PartyType_Id
									INNER JOIN [MDA].[dbo].[SubPartyType] MSPT ON  MSPT.Id = MI2O.SubPartyType_Id
									INNER JOIN [MDA].[dbo].[OrganisationType] OT ON OT.Id = MI2O.OrganisationType_Id
									WHERE CT.PhysicalTableName = 'Incident2Organisation'
									AND (OT.TypeText IN ('Policy Holder') OR LEFT(MI2O.[SourceDescription],3) != 'ADA')
									) IQ ON IQ.RowID = CT.RowID
						WHERE CT.DoNotReplicate IS NULL
						AND CT.ChangeDatabase = 'MDA'
						) DATA
					WHERE RowNumber = 1
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET  Accident_Management			= Source.Accident_Management
			,Ambulance_Attended				= ISNULL(Source.Ambulance_Attended,0)
			,Broker_						= Source.Broker_
			,Claim_Code						= LEFT(Source.Claim_Code,50)
			,Claim_Notification_Date		= Source.Claim_Notification_Date
			,Claim_Number					= Source.Claim_Number
			,Claim_Status					= CASE WHEN Source.ClaimStatus_Id = 0 AND ISNULL(Source.Source_Description_411765489,'') !='ADA' THEN [Target].Claim_Status ELSE NULLIF(Source.Claim_Status,'Unknown') END
			,Claim_Type						= Source.Claim_Type
			,Create_Date					= Source.Create_Date
			,Create_User					= Source.Create_User
			,Engineer_						= Source.Engineer_
			,Hire_							= Source.Hire_
			,Incident_Circs					= Source.Incident_Circs
			,Incident_Location				= Source.Incident_Location
			,Incident_Time					= Source.Incident_Time
			,Insurer_						= Source.Insurer_
			,Last_Upd_Date					= Source.Last_Upd_Date
			,Last_Upd_User					= Source.Last_Upd_User
			,Medical_Examiner				= Source.Medical_Examiner
			,MoJ_Status						= CASE WHEN Source.[MojStatus_Id] = 0 AND ISNULL(Source.Source_Description_411765489,'') !='ADA' THEN [Target].MoJ_Status ELSE NULLIF(Source.MoJ_Status,'Unknown') END
			,Party_Type						= CASE WHEN Source.[PartyType_Id] = 0 AND ISNULL(Source.Source_Description_411765489,'') !='ADA' THEN [Target].Party_Type ELSE NULLIF(Source.Party_Type,'Unknown') END
			,Payments_						= Source.Payments_
			,Police_Attended				= ISNULL(Source.Police_Attended,0)
			,Police_Force					= Source.Police_Force
			,Police_Reference				= Source.Police_Reference
			,Record_Status					= Source.Record_Status
			,Recovery_						= Source.Recovery_
			,Referral_Source				= Source.Referral_Source
			,Repairer_						= Source.Repairer_
			,Reserve_						= Source.Reserve_
			,Solicitors_					= Source.Solicitors_
			,Source_411765484				= Source.Source_411765484
			,Source_Description_411765489	= Source.Source_Description_411765489
			,Source_Reference_411765487		= Source.Source_Reference_411765487
			,Storage_						= Source.Storage_
			,Storage_Address				= Source.Storage_Address
			,Sub_Party_Type					= CASE WHEN Source.[SubPartyType_Id] = 0 AND ISNULL(Source.Source_Description_411765489,'') !='ADA' THEN [Target].Sub_Party_Type ELSE NULLIF(Source.Sub_Party_Type,'Unknown') END
			,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
			,Undefined_Supplier				= Source.Undefined_Supplier
			,Attended_Hospital				= ISNULL(Source.Attended_Hospital,0)
			,Risk_Claim_ID_414244883		= Source.Risk_Claim_ID_414244883
			,[Medical_Legal]				= Source.Medical_Legal
			,[Recovery_Address]				= Source.Recovery_Address
			,[Repairer_Address]				= Source.Repairer_Address
			,[Inspection_Address]			= Source.Inspection_Address
			,[Source_Claim_Status]			= Source.[Source_Claim_Status]
			,[Total_Claim_Cost]				= Source.[Total_Claim_Cost]
			,[Total_Claim_Cost_Less_Excess] = Source.[Total_Claim_Cost_Less_Excess]
			,[Bypass_Fraud]					= Source.[Bypass_Fraud]
		WHEN NOT MATCHED THEN INSERT
			(
			 Unique_ID
			,Accident_Management
			,Ambulance_Attended
			,Broker_
			,Claim_Code
			,Claim_Notification_Date
			,Claim_Number
			,Claim_Status
			,Claim_Type
			,Create_Date
			,Create_User
			,Do_Not_Disseminate_412284494
			,Engineer_
			,Hire_
			,Incident_Circs
			,Incident_Link_ID
			,Incident_Location
			,Incident_Time
			,Insurer_
			,Last_Upd_Date
			,Last_Upd_User
			,MDA_Incident_ID_412284502
			,Medical_Examiner
			,MoJ_Status
			,Party_Type
			,Payments_
			,Police_Attended
			,Police_Force
			,Police_Reference
			,Record_Status
			,Recovery_
			,Referral_Source
			,Repairer_
			,Reserve_
			,Solicitors_
			,Source_411765484
			,Source_Description_411765489
			,Source_Reference_411765487
			,Storage_
			,Storage_Address
			,Sub_Party_Type
			,x5x5x5_Grading_412284402
			,Undefined_Supplier
			,Attended_Hospital
			,Risk_Claim_ID_414244883
			,[Medical_Legal]
			,[Recovery_Address]
			,[Repairer_Address]
			,[Inspection_Address]
			,[Source_Claim_Status]
			,[Total_Claim_Cost]
			,[Total_Claim_Cost_Less_Excess]
			,[Bypass_Fraud]
			)
		VALUES
			(
			 Source.MDA_IBase_Unique_ID
			,Source.Accident_Management
			,ISNULL(Source.Ambulance_Attended,0)
			,Source.Broker_
			,LEFT(Source.Claim_Code,50)
			,Source.Claim_Notification_Date
			,Source.Claim_Number
			,CASE WHEN Source.ClaimStatus_Id = 0 THEN NULL ELSE Source.Claim_Status END
			,Source.Claim_Type
			,Source.Create_Date
			,Source.Create_User
			,0
			,Source.Engineer_
			,Source.Hire_
			,Source.Incident_Circs
			,Source.Incident_Link_ID
			,Source.Incident_Location
			,Source.Incident_Time
			,Source.Insurer_
			,Source.Last_Upd_Date
			,Source.Last_Upd_User
			,CASE WHEN Source.PhysicalTableName= 'Incident2Person' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Incident2Person') WHEN Source.PhysicalTableName= 'Incident2Organisation' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Incident2Organisation') ELSE NULL END
			,Source.Medical_Examiner
			,CASE WHEN Source.[MojStatus_Id] = 0 THEN NULL ELSE Source.MoJ_Status END
			,CASE WHEN Source.[PartyType_Id] = 0 THEN NULL ELSE Source.Party_Type END
			,Source.Payments_
			,ISNULL(Source.Police_Attended,0)
			,Source.Police_Force
			,Source.Police_Reference
			,Source.Record_Status
			,Source.Recovery_
			,Source.Referral_Source
			,Source.Repairer_
			,Source.Reserve_
			,Source.Solicitors_
			,Source.Source_411765484
			,Source.Source_Description_411765489
			,Source.Source_Reference_411765487
			,Source.Storage_
			,Source.Storage_Address
			,CASE WHEN Source.[SubPartyType_Id] = 0 THEN NULL ELSE Source.Sub_Party_Type END
			,Source.x5x5x5_Grading_412284402
			,Source.Undefined_Supplier
			,ISNULL(Source.Attended_Hospital,0)
			,Source.Risk_Claim_ID_414244883
			,Source.Medical_Legal
			,Source.Recovery_Address
			,Source.Repairer_Address
			,Source.Inspection_Address
			,Source.[Source_Claim_Status]
			,Source.[Total_Claim_Cost]
			,Source.[Total_Claim_Cost_Less_Excess]
			,Source.[Bypass_Fraud]
			)
		OUTPUT	@OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Accident_Management,'') + '}|I:{' + ISNULL(Inserted.Accident_Management,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Ambulance_Attended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Ambulance_Attended,'')) + '}'
				,'D:{' + ISNULL(Deleted.Broker_,'') + '}|I:{' + ISNULL(Inserted.Broker_,'') + '}'
				,'D:{' + ISNULL(Deleted.Claim_Code,'') + '}|I:{' + ISNULL(Inserted.Claim_Code,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Claim_Notification_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Claim_Notification_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Claim_Number,'') + '}|I:{' + ISNULL(Inserted.Claim_Number,'') + '}'
				,'D:{' + ISNULL(Deleted.Claim_Status,'') + '}|I:{' + ISNULL(Inserted.Claim_Status,'') + '}'
				,'D:{' + ISNULL(Deleted.Claim_Type,'') + '}|I:{' + ISNULL(Inserted.Claim_Type,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Engineer_,'') + '}|I:{' + ISNULL(Inserted.Engineer_,'') + '}'
				,'D:{' + ISNULL(Deleted.Hire_,'') + '}|I:{' + ISNULL(Inserted.Hire_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Incident_Circs,'') + '}|I:{' + ISNULL(Inserted.Incident_Circs,'') + '}'
				,'D:{' + ISNULL(Deleted.Incident_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Incident_Link_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Incident_Location,'') + '}|I:{' + ISNULL(Inserted.Incident_Location,'') + '}'
				,'D:{' + ISNULL(Deleted.Incident_Time,'') + '}|I:{' + ISNULL(Inserted.Incident_Time,'') + '}'
				,'D:{' + ISNULL(Deleted.Insurer_,'') + '}|I:{' + ISNULL(Inserted.Insurer_,'') + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Medical_Examiner,'') + '}|I:{' + ISNULL(Inserted.Medical_Examiner,'') + '}'
				,'D:{' + ISNULL(Deleted.MoJ_Status,'') + '}|I:{' + ISNULL(Inserted.MoJ_Status,'') + '}'
				,'D:{' + ISNULL(Deleted.Party_Type,'') + '}|I:{' + ISNULL(Inserted.Party_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.Payments_,'') + '}|I:{' + ISNULL(Inserted.Payments_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Police_Attended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Police_Attended,'')) + '}'
				,'D:{' + ISNULL(Deleted.Police_Force,'') + '}|I:{' + ISNULL(Inserted.Police_Force,'') + '}'
				,'D:{' + ISNULL(Deleted.Police_Reference,'') + '}|I:{' + ISNULL(Inserted.Police_Reference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.Recovery_,'') + '}|I:{' + ISNULL(Inserted.Recovery_,'') + '}'
				,'D:{' + ISNULL(Deleted.Referral_Source,'') + '}|I:{' + ISNULL(Inserted.Referral_Source,'') + '}'
				,'D:{' + ISNULL(Deleted.Repairer_,'') + '}|I:{' + ISNULL(Inserted.Repairer_,'') + '}'
				,'D:{' + ISNULL(Deleted.Reserve_,'') + '}|I:{' + ISNULL(Inserted.Reserve_,'') + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Solicitors_,'') + '}|I:{' + ISNULL(Inserted.Solicitors_,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage_,'') + '}|I:{' + ISNULL(Inserted.Storage_,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage_Address,'') + '}|I:{' + ISNULL(Inserted.Storage_Address,'') + '}'
				,'D:{' + ISNULL(Deleted.Sub_Party_Type,'') + '}|I:{' + ISNULL(Inserted.Sub_Party_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Undefined_Supplier,'') + '}|I:{' + ISNULL(Inserted.Undefined_Supplier,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Attended_Hospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Attended_Hospital,'')) + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
				,'D:{' + ISNULL(Deleted.[Source_Claim_Status],'') + '}|I:{' + ISNULL(Inserted.[Source_Claim_Status],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Total_Claim_Cost],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Total_Claim_Cost],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Total_Claim_Cost_Less_Excess],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Total_Claim_Cost_Less_Excess],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Bypass_Fraud],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Bypass_Fraud],'')) + '}'
		INTO [SyncDML].[MDAIBase_Incident_Link] (TaskId, DMLAction, Unique_ID, Accident_Management, AltEntity, Ambulance_Attended, Broker_, Claim_Code, Claim_Notification_Date, Claim_Number, Claim_Status, Claim_Type, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engineer_, Hire_, IconColour, Incident_Circs, Incident_Link_ID, Incident_Location, Incident_Time, Insurer_, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Medical_Examiner, MoJ_Status, Party_Type, Payments_, Police_Attended, Police_Force, Police_Reference, Record_Status, Recovery_, Referral_Source, Repairer_, Reserve_, SCC, Solicitors_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Storage_, Storage_Address, Sub_Party_Type, x5x5x5_Grading_412284402, Undefined_Supplier, Attended_Hospital, Risk_Claim_ID_414244883, Source_Claim_Status, Total_Claim_Cost, Total_Claim_Cost_Less_Excess, Bypass_Fraud);

		--3.2.2)DoDeletes
		--WeNeedToDeleteTheLinkEndrecordBeforetheDeletesOrWeWontBeAbleTo
		SET @Step ='3.2.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [MDA_IBase].[dbo].[_LinkEnd] LE
		INNER JOIN (
					SELECT [IBase8LinkID] Link_ID
					FROM
							(
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident2Person') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Incident2Person'
							AND DMLAction = 'D'
							UNION ALL
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident2Organisation') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Incident2Organisation'
							AND DMLAction = 'D'
							) DATA
					INNER JOIN [MDA_IBase].[dbo].[VW_StagingLinks] IVSL ON IVSL.[MDA_Incident_ID_412284502] = DATA.MDA_Incident_ID_412284502
					) IQ ON IQ.Link_ID = LE.Link_ID;

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[Incident_Link]
		OUTPUT	 @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Accident_Management,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Ambulance_Attended,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Broker_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Claim_Code,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Claim_Notification_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Claim_Number,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Claim_Status,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Claim_Type,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Engineer_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Hire_,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Incident_Circs,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Incident_Link_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Incident_Location,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Incident_Time,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Insurer_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Medical_Examiner,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MoJ_Status,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Party_Type,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Payments_,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Police_Attended,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Police_Force,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Police_Reference,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Recovery_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Referral_Source,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Repairer_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Reserve_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Solicitors_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Storage_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Storage_Address,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Sub_Party_Type,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Undefined_Supplier,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Attended_Hospital,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Source_Claim_Status],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Total_Claim_Cost],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Total_Claim_Cost_Less_Excess],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Bypass_Fraud],'')) + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Incident_Link] (TaskId, DMLAction, Unique_ID, Accident_Management, AltEntity, Ambulance_Attended, Broker_, Claim_Code, Claim_Notification_Date, Claim_Number, Claim_Status, Claim_Type, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engineer_, Hire_, IconColour, Incident_Circs, Incident_Link_ID, Incident_Location, Incident_Time, Insurer_, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Medical_Examiner, MoJ_Status, Party_Type, Payments_, Police_Attended, Police_Force, Police_Reference, Record_Status, Recovery_, Referral_Source, Repairer_, Reserve_, SCC, Solicitors_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Storage_, Storage_Address, Sub_Party_Type, x5x5x5_Grading_412284402, Undefined_Supplier, Attended_Hospital, Risk_Claim_ID_414244883, Source_Claim_Status, Total_Claim_Cost, Total_Claim_Cost_Less_Excess, Bypass_Fraud)
		FROM [MDA_IBase].[dbo].[Incident_Link] IIL
		INNER JOIN #CT CT ON CASE WHEN CT.PhysicalTableName = 'Incident2Person' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID, 'MDA.dbo.Incident2Person') WHEN CT.PhysicalTableName= 'Incident2Organisation' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident2Organisation') ELSE NULL END = IIL.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';

		--3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		SET @Step ='3.2.3';
		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MI2P
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SubPartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SubPartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncidentTime,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncidentTime,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentCircs,'') + '}|I:{' + ISNULL(Inserted.IncidentCircs,'') + '}'
				,'D:{' + ISNULL(Deleted.IncidentLocation,'') + '}|I:{' + ISNULL(Inserted.IncidentLocation,'') + '}'
				,'D:{' + ISNULL(Deleted.Insurer,'') + '}|I:{' + ISNULL(Inserted.Insurer,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimNumber,'') + '}|I:{' + ISNULL(Inserted.ClaimNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClaimType,'') + '}|I:{' + ISNULL(Inserted.ClaimType,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimCode,'') + '}|I:{' + ISNULL(Inserted.ClaimCode,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimNotificationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimNotificationDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PaymentsToDate,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PaymentsToDate,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reserve,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reserve,'')) + '}'
				,'D:{' + ISNULL(Deleted.Broker,'') + '}|I:{' + ISNULL(Inserted.Broker,'') + '}'
				,'D:{' + ISNULL(Deleted.ReferralSource,'') + '}|I:{' + ISNULL(Inserted.ReferralSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Solicitors,'') + '}|I:{' + ISNULL(Inserted.Solicitors,'') + '}'
				,'D:{' + ISNULL(Deleted.Engineer,'') + '}|I:{' + ISNULL(Inserted.Engineer,'') + '}'
				,'D:{' + ISNULL(Deleted.Recovery,'') + '}|I:{' + ISNULL(Inserted.Recovery,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage,'') + '}|I:{' + ISNULL(Inserted.Storage,'') + '}'
				,'D:{' + ISNULL(Deleted.StorageAddress,'') + '}|I:{' + ISNULL(Inserted.StorageAddress,'') + '}'
				,'D:{' + ISNULL(Deleted.Repairer,'') + '}|I:{' + ISNULL(Inserted.Repairer,'') + '}'
				,'D:{' + ISNULL(Deleted.Hire,'') + '}|I:{' + ISNULL(Inserted.Hire,'') + '}'
				,'D:{' + ISNULL(Deleted.AccidentManagement,'') + '}|I:{' + ISNULL(Inserted.AccidentManagement,'') + '}'
				,'D:{' + ISNULL(Deleted.MedicalExaminer,'') + '}|I:{' + ISNULL(Inserted.MedicalExaminer,'') + '}'
				,'D:{' + ISNULL(Deleted.UndefinedSupplier,'') + '}|I:{' + ISNULL(Inserted.UndefinedSupplier,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PoliceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PoliceAttended,'')) + '}'
				,'D:{' + ISNULL(Deleted.PoliceForce,'') + '}|I:{' + ISNULL(Inserted.PoliceForce,'') + '}'
				,'D:{' + ISNULL(Deleted.PoliceReference,'') + '}|I:{' + ISNULL(Inserted.PoliceReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AmbulanceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AmbulanceAttended,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AttendedHospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AttendedHospital,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
				,'D:{' + ISNULL(Deleted.[SourceClaimStatus],'') + '}|I:{' + ISNULL(Inserted.[SourceClaimStatus],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[TotalClaimCost],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[TotalClaimCost],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[TotalClaimCostLessExcess],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[TotalClaimCostLessExcess],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BypassFraud],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BypassFraud],'')) + '}'
		INTO [SyncDML].[MDA_Incident2Person] (TaskId, DMLAction, Id, Incident_ID, Person_ID, IncidentLinkId, PartyType_Id, SubPartyType_Id, MojStatus_Id, IncidentTime, IncidentCircs, IncidentLocation, Insurer, ClaimNumber, ClaimStatus_Id, ClaimType, ClaimCode, ClaimNotificationDate, PaymentsToDate, Reserve, Broker, ReferralSource, Solicitors, Engineer, Recovery, Storage, StorageAddress, Repairer, Hire, AccidentManagement, MedicalExaminer, UndefinedSupplier, PoliceAttended, PoliceForce, PoliceReference, AmbulanceAttended, AttendedHospital, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus, SourceClaimStatus, TotalClaimCost, TotalClaimCostLessExcess, BypassFraud)
		FROM [MDA].[dbo].Incident2Person MI2P
		INNER JOIN #CT CT ON CT.MDA_ID = MI2P.Id
 		WHERE (MI2P.IBaseId IS NULL
		OR MI2P.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Incident2Person';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MI2O
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IsClaimLevelLink,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IsClaimLevelLink,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SubPartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SubPartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncidentTime,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncidentTime,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentCircs,'') + '}|I:{' + ISNULL(Inserted.IncidentCircs,'') + '}'
				,'D:{' + ISNULL(Deleted.IncidentLocation,'') + '}|I:{' + ISNULL(Inserted.IncidentLocation,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PaymentsToDate,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PaymentsToDate,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reserve,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reserve,'')) + '}'
				,'D:{' + ISNULL(Deleted.Insurer,'') + '}|I:{' + ISNULL(Inserted.Insurer,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimNumber,'') + '}|I:{' + ISNULL(Inserted.ClaimNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClaimType,'') + '}|I:{' + ISNULL(Inserted.ClaimType,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimCode,'') + '}|I:{' + ISNULL(Inserted.ClaimCode,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimNotificationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimNotificationDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Broker,'') + '}|I:{' + ISNULL(Inserted.Broker,'') + '}'
				,'D:{' + ISNULL(Deleted.ReferralSource,'') + '}|I:{' + ISNULL(Inserted.ReferralSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Solicitors,'') + '}|I:{' + ISNULL(Inserted.Solicitors,'') + '}'
				,'D:{' + ISNULL(Deleted.Engineer,'') + '}|I:{' + ISNULL(Inserted.Engineer,'') + '}'
				,'D:{' + ISNULL(Deleted.Recovery,'') + '}|I:{' + ISNULL(Inserted.Recovery,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage,'') + '}|I:{' + ISNULL(Inserted.Storage,'') + '}'
				,'D:{' + ISNULL(Deleted.StorageAddress,'') + '}|I:{' + ISNULL(Inserted.StorageAddress,'') + '}'
				,'D:{' + ISNULL(Deleted.Repairer,'') + '}|I:{' + ISNULL(Inserted.Repairer,'') + '}'
				,'D:{' + ISNULL(Deleted.Hire,'') + '}|I:{' + ISNULL(Inserted.Hire,'') + '}'
				,'D:{' + ISNULL(Deleted.AccidentManagement,'') + '}|I:{' + ISNULL(Inserted.AccidentManagement,'') + '}'
				,'D:{' + ISNULL(Deleted.MedicalExaminer,'') + '}|I:{' + ISNULL(Inserted.MedicalExaminer,'') + '}'
				,'D:{' + ISNULL(Deleted.UndefinedSupplier,'') + '}|I:{' + ISNULL(Inserted.UndefinedSupplier,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PoliceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PoliceAttended,'')) + '}'
				,'D:{' + ISNULL(Deleted.PoliceForce,'') + '}|I:{' + ISNULL(Inserted.PoliceForce,'') + '}'
				,'D:{' + ISNULL(Deleted.PoliceReference,'') + '}|I:{' + ISNULL(Inserted.PoliceReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AmbulanceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AmbulanceAttended,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AttendedHospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AttendedHospital,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Organisation] (TaskID, DMLAction, Id, Incident_Id, Organisation_Id, IsClaimLevelLink, IncidentLinkId, OrganisationType_Id, PartyType_Id, SubPartyType_Id, IncidentTime, IncidentCircs, IncidentLocation, PaymentsToDate, Reserve, Insurer, ClaimNumber, ClaimStatus_Id, ClaimType, ClaimCode, MojStatus_Id, ClaimNotificationDate, Broker, ReferralSource, Solicitors, Engineer, Recovery, Storage, StorageAddress, Repairer, Hire, AccidentManagement, MedicalExaminer, UndefinedSupplier, PoliceAttended, PoliceForce, PoliceReference, AmbulanceAttended, AttendedHospital, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].Incident2Organisation MI2O
		INNER JOIN #CT CT ON CT.MDA_ID = MI2O.Id
		INNER JOIN [MDA].[dbo].[OrganisationType] OT ON OT.Id = MI2O.OrganisationType_Id
 		WHERE (MI2O.IBaseId IS NULL
		OR MI2O.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Incident2Organisation'
		AND (OT.TypeText IN ('Policy Holder') OR LEFT(MI2O.[SourceDescription],3) != 'ADA');

		--------------------------------------------------------------------------
		--3.3)MDAIBase: LinkEndTablesChanges
		-------------------------------------------------------------------------
		--3.3.1)PopulateTheWorkingTable
		--DoForEntityID1
		--CheckTheValuesThatArebeingAdded
		SET @Step ='3.3.1';

		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC
		FROM	(
				SELECT MI2P.IBaseId Link_ID, MI.IBaseId Entity_ID1, MI2P.LinkConfidence Confidence, 0 Direction, P.IBaseId Entity_ID2, 1688 EntityType_ID1, 1969 EntityType_ID2, @LinkTypeID LinkType_ID, MI2P.RecordStatus Record_Status, 1 Record_Type, NULL SCC
				FROM [MDA].[dbo].Incident2Person MI2P
				INNER JOIN [MDA].[dbo].Incident MI ON MI.ID = MI2P.Incident_Id
				INNER JOIN [MDA].[dbo].[Person] P ON P.ID = MI2P.Person_ID
				INNER JOIN #CT CT ON CT.MDA_ID = MI2P.ID AND CT.PhysicalTableName = 'Incident2Person'
				UNION ALL
				SELECT MI2O.IBaseId, MI.IBaseId, MI2O.LinkConfidence, 0, O.IBaseId, 1688, 1893, @LinkTypeID, MI2O.RecordStatus, 1, NULL
				FROM [MDA].[dbo].Incident2Organisation MI2O
				INNER JOIN [MDA].[dbo].Incident MI ON MI.ID = MI2O.Incident_Id
				INNER JOIN [MDA].[dbo].[Organisation] O ON O.ID = MI2O.Organisation_Id
				INNER JOIN #CT CT ON CT.MDA_ID = MI2O.ID AND CT.PhysicalTableName = 'Incident2Organisation'
				INNER JOIN [MDA].[dbo].[OrganisationType] OT ON OT.Id = MI2O.OrganisationType_Id
				WHERE (OT.TypeText IN ('Policy Holder') OR LEFT(MI2O.[SourceDescription],3) != 'ADA')
				) Data

		--NowDoForEntityID2
		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT Link_ID, Entity_ID2, Confidence, Direction, Entity_ID1, EntityType_ID2, EntityType_ID1, LinkType_ID, Record_Status, 2, SCC
		FROM #LinkEndStaging;

		CREATE CLUSTERED INDEX IX_LinkEndStaging_Link_ID_Entity_ID1 ON #LinkEndStaging (Link_ID, Entity_ID1);

		--3.3.2)ActionTheLinkEndChangeInMDA_IBase
		SET @Step ='3.3.2';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[_LinkEnd] [Target]
		USING	(
				SELECT *
				FROM #LinkEndStaging
				) Source ON Source.Link_ID = [Target].Link_ID AND Source.Entity_ID1 = [Target].Entity_ID1
		WHEN MATCHED AND CHECKSUM(Source.Link_ID, Source.Entity_ID1, Source.Confidence, Source.Direction, Source.Entity_ID2, Source.EntityType_ID1, Source.EntityType_ID2, Source.LinkType_ID, Source.Record_Status, Source.Record_Type, Source.SCC) != CHECKSUM([Target].Link_ID, [Target].Entity_ID1, [Target].Confidence, [Target].Direction, [Target].Entity_ID2, [Target].EntityType_ID1, [Target].EntityType_ID2, [Target].LinkType_ID, [Target].Record_Status, [Target].Record_Type, [Target].SCC) THEN UPDATE
		SET  Confidence			= Source.Confidence
			,Direction			= Source.Direction
			,Entity_ID2			= Source.Entity_ID2
			,EntityType_ID1		= Source.EntityType_ID1
			,EntityType_ID2		= Source.EntityType_ID2
			,LinkType_ID		= Source.LinkType_ID
			,Record_Status		= Source.Record_Status
			,Record_Type		= Source.Record_Type
			,SCC				= Source.SCC
		WHEN NOT MATCHED THEN INSERT
			(
			 Link_ID
			,Entity_ID1
			,Confidence
			,Direction
			,Entity_ID2
			,EntityType_ID1
			,EntityType_ID2
			,LinkType_ID
			,Record_Status
			,Record_Type
			,SCC
			)
		VALUES
			(
			 Source.Link_ID
			,Source.Entity_ID1
			,Source.Confidence
			,Source.Direction
			,Source.Entity_ID2
			,Source.EntityType_ID1
			,Source.EntityType_ID2
			,Source.LinkType_ID
			,Source.Record_Status
			,Source.Record_Type
			,Source.SCC
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Link_ID],'') + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID1],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Confidence],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Direction],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID2],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID1],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID2],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkType_ID],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Type],'')) + '}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{' + ISNULL(Inserted.[SCC],'') + '}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC);

		--3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
		SET @Step ='3.3.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		DELETE  [MDA_IBase].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [MDA_IBase].[dbo].[_LinkEnd] LE
		WHERE EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID)
		AND NOT EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID AND LES.Entity_ID1 = LE.Entity_ID1);
	END

	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--Incident2Person
		SET @Step ='4.1';
		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].Incident2Person [Target]
		USING	(
				SELECT *
				FROM	(
						SELECT	 ROW_NUMBER() OVER (PARTITION BY ISNULL(CAST(CT.MDA_ID AS VARCHAR), CT.MDA_IBase_Unique_ID) ORDER BY ISNULL(CAST(CT.MDA_ID AS VARCHAR), CT.MDA_IBase_Unique_ID)) RowID
								,CT.MDA_ID
								,CT.MDA_IBase_Unique_ID
								,[Sync].[fn_ExtractMDAID] (MI.MDA_Incident_ID_412284502) [Incident_Id]
								,[Sync].[fn_ExtractMDAID] (MP.MDA_Incident_ID_412284502) [Person_Id]
								,IIL.[Incident_Link_ID] IncidentLinkId
								,ISNULL(MPT.ID,0)  PartyType_Id
								,ISNULL(MSPT.ID,0) SubPartyType_Id
								,ISNULL(MMS.ID,0)  MojStatus_Id
								,IIL.[Incident_Time] IncidentTime
								,LEFT(IIL.[Incident_Circs],1024) IncidentCircs
								,LEFT(IIL.[Incident_Location],255) IncidentLocation
								,LEFT(IIL.[Insurer_],255) Insurer
								,LEFT(IIL.[Claim_Number],50) ClaimNumber
								,ISNULL(MCS.ID,0) ClaimStatus_Id
								,LEFT(IIL.[Claim_Type],50) ClaimType
								,LEFT(IIL.[Claim_Code],80) ClaimCode
								,IIL.[Claim_Notification_Date] ClaimNotificationDate
								,IIL.[Payments_] PaymentsToDate
								,IIL.[Reserve_] Reserve
								,LEFT(IIL.[Broker_],255) Broker
								,LEFT(IIL.[Referral_Source],255) ReferralSource
								,LEFT(IIL.[Solicitors_],255) Solicitors
								,LEFT(IIL.[Engineer_],255) Engineer
								,LEFT(IIL.[Recovery_],255) Recovery
								,LEFT(IIL.[Storage_],255) Storage
								,LEFT(IIL.[Storage_Address],255) StorageAddress
								,LEFT(IIL.[Repairer_],255) Repairer
								,LEFT(IIL.[Hire_],255) Hire
								,LEFT(IIL.[Accident_Management],255) AccidentManagement
								,LEFT(IIL.[Medical_Examiner],255) MedicalExaminer
								,LEFT(IIL.[Undefined_Supplier],255) UndefinedSupplier
								,IIL.[Police_Attended] PoliceAttended
								,LEFT(IIL.[Police_Force],50) PoliceForce
								,LEFT(IIL.[Police_Reference],50) PoliceReference
								,IIL.[Ambulance_Attended] AmbulanceAttended
								,IIL.[Attended_Hospital] AttendedHospital
								,LEFT(IIL.[x5x5x5_Grading_412284402],10) [FiveGrading]
								,ILE.Confidence [LinkConfidence]
								,LEFT(IIL.[Source_411765484],50) [Source]
								,LEFT(IIL.[Source_Reference_411765487],50) [SourceReference]
								,LEFT(IIL.[Source_Description_411765489],1024) [SourceDescription]
								,LEFT(IIL.Create_User,50) [CreatedBy]
								,IIL.Create_Date [CreatedDate]
								,IIL.Last_Upd_User [ModifiedBy]
								,IIL.Last_Upd_Date [ModifiedDate]
								,IIL.[Risk_Claim_ID_414244883] [RiskClaim_Id]
								,IIL.[Risk_Claim_ID_414244883] [BaseRiskClaim_Id]
								,IIL.Unique_ID [IBaseId]
								,IIL.Record_Status [RecordStatus]
								,IIL.[Medical_Legal] [MedicalLegal]
								,IIL.[Recovery_Address] [RecoveryAddress]
								,IIL.[Repairer_Address] [RepairerAddress]
								,IIL.[Inspection_Address] [InspectionAddress]
								,IIL.[Source_Claim_Status] [SourceClaimStatus]
								,IIL.[Total_Claim_Cost] [TotalClaimCost]
								,IIL.[Total_Claim_Cost_Less_Excess] [TotalClaimCostLessExcess]
								,IIL.[Bypass_Fraud] [BypassFraud]
						FROM #CT CT
						INNER JOIN [MDA_IBase].[dbo].[Incident_Link] IIL ON IIL.Unique_ID = CT.MDA_IBase_Unique_ID
						INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IIL.Unique_ID
						INNER JOIN [MDA_IBase].[dbo].[Incident_] MI ON MI.Unique_ID = ILE.Entity_ID1
						INNER JOIN [MDA_IBase].[dbo].[Person_] MP ON MP.Unique_ID = ILE.Entity_ID2
						LEFT JOIN [MDA].[dbo].[ClaimStatus] MCS ON MCS.[Text] = IIL.[Claim_Status]
						LEFT JOIN [MDA].[dbo].[MojStatus] MMS ON MMS.[Text] = IIL.Moj_Status
						LEFT JOIN [MDA].[dbo].[PartyType] MPT ON MPT.[PartyTypeText] = IIL.Party_Type
						LEFT JOIN [MDA].[dbo].[SubPartyType] MSPT ON  MSPT.[SubPartyText] = IIL.Sub_Party_Type
						WHERE CT.DoNotReplicate IS NULL
						AND CT.ChangeDatabase = 'MDA_IBase'
						AND MI.[Incident_Date] < GETDATE()
						AND [Sync].[fn_ExtractMDAID] (MI.MDA_Incident_ID_412284502) IS NOT NULL
						AND [Sync].[fn_ExtractMDAID] (MP.MDA_Incident_ID_412284502) IS NOT NULL
						) Data
				WHERE RowID = 1
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  Incident_Id			= Source.Incident_Id
			,Person_Id				= Source.Person_Id
			,IncidentLinkId			= Source.IncidentLinkId
			,PartyType_Id			= Source.PartyType_Id
			,SubPartyType_Id		= Source.SubPartyType_Id
			,MojStatus_Id			= Source.MojStatus_Id
			,IncidentTime			= Source.IncidentTime
			,IncidentCircs			= Source.IncidentCircs
			,IncidentLocation		= Source.IncidentLocation
			,Insurer				= Source.Insurer
			,ClaimNumber			= Source.ClaimNumber
			,ClaimStatus_Id			= Source.ClaimStatus_Id
			,ClaimType				= Source.ClaimType
			,ClaimCode				= Source.ClaimCode
			,ClaimNotificationDate	= Source.ClaimNotificationDate
			,PaymentsToDate			= Source.PaymentsToDate
			,Reserve				= Source.Reserve
			,Broker					= Source.Broker
			,ReferralSource			= Source.ReferralSource
			,Solicitors				= Source.Solicitors
			,Engineer				= Source.Engineer
			,Recovery				= Source.Recovery
			,Storage				= Source.Storage
			,StorageAddress			= Source.StorageAddress
			,Repairer				= Source.Repairer
			,Hire					= Source.Hire
			,AccidentManagement		= Source.AccidentManagement
			,MedicalExaminer		= Source.MedicalExaminer
			,UndefinedSupplier		= Source.UndefinedSupplier
			,PoliceAttended			= Source.PoliceAttended
			,PoliceForce			= Source.PoliceForce
			,PoliceReference		= Source.PoliceReference
			,AmbulanceAttended		= Source.AmbulanceAttended
			,AttendedHospital		= Source.AttendedHospital
			,FiveGrading			= Source.FiveGrading
			,LinkConfidence			= Source.LinkConfidence
			,Source					= Source.Source
			,SourceReference		= Source.SourceReference
			,SourceDescription		= Source.SourceDescription
			,CreatedBy				= Source.CreatedBy
			,CreatedDate			= Source.CreatedDate
			,ModifiedBy				= Source.ModifiedBy
			,ModifiedDate			= Source.ModifiedDate
			,IBaseId				= Source.IBaseId
			,RecordStatus			= Source.RecordStatus
			,ADARecordStatus		= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
			,[MedicalLegal]			= Source.MedicalLegal
			,[RecoveryAddress]		= Source.RecoveryAddress
			,[RepairerAddress]		= Source.RepairerAddress
			,[InspectionAddress]	= Source.InspectionAddress
			,[SourceClaimStatus]	= Source.[SourceClaimStatus]
			,[TotalClaimCost]		= Source.[TotalClaimCost]
			,[TotalClaimCostLessExcess]	= Source.[TotalClaimCostLessExcess]
			,[BypassFraud]			= Source.[BypassFraud]
		WHEN NOT MATCHED THEN INSERT 
			(
			 Incident_Id
			,Person_Id
			,IncidentLinkId
			,PartyType_Id
			,SubPartyType_Id
			,MojStatus_Id
			,IncidentTime
			,IncidentCircs
			,IncidentLocation
			,Insurer
			,ClaimNumber
			,ClaimStatus_Id
			,ClaimType
			,ClaimCode
			,ClaimNotificationDate
			,PaymentsToDate
			,Reserve
			,Broker
			,ReferralSource
			,Solicitors
			,Engineer
			,Recovery
			,Storage
			,StorageAddress
			,Repairer
			,Hire
			,AccidentManagement
			,MedicalExaminer
			,UndefinedSupplier
			,PoliceAttended
			,PoliceForce
			,PoliceReference
			,AmbulanceAttended
			,AttendedHospital
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			,[MedicalLegal]
			,[RecoveryAddress]
			,[RepairerAddress]
			,[InspectionAddress]
			,[SourceClaimStatus]
			,[TotalClaimCost]
			,[TotalClaimCostLessExcess]
			,[BypassFraud]
			)
		VALUES
			(
			 Source.Incident_Id
			,Source.Person_Id
			,Source.IncidentLinkId
			,Source.PartyType_Id
			,Source.SubPartyType_Id
			,Source.MojStatus_Id
			,Source.IncidentTime
			,Source.IncidentCircs
			,Source.IncidentLocation
			,Source.Insurer
			,Source.ClaimNumber
			,Source.ClaimStatus_Id
			,Source.ClaimType
			,Source.ClaimCode
			,Source.ClaimNotificationDate
			,Source.PaymentsToDate
			,Source.Reserve
			,Source.Broker
			,Source.ReferralSource
			,Source.Solicitors
			,Source.Engineer
			,Source.Recovery
			,Source.Storage
			,Source.StorageAddress
			,Source.Repairer
			,Source.Hire
			,Source.AccidentManagement
			,Source.MedicalExaminer
			,Source.UndefinedSupplier
			,Source.PoliceAttended
			,Source.PoliceForce
			,Source.PoliceReference
			,Source.AmbulanceAttended
			,Source.AttendedHospital
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10
			,Source.MedicalLegal
			,Source.RecoveryAddress
			,Source.RepairerAddress
			,Source.InspectionAddress
			,Source.[SourceClaimStatus]
			,Source.[TotalClaimCost]
			,Source.[TotalClaimCostLessExcess]
			,Source.[BypassFraud]
			)
		OUTPUT	 @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SubPartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SubPartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncidentTime,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncidentTime,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentCircs,'') + '}|I:{' + ISNULL(Inserted.IncidentCircs,'') + '}'
				,'D:{' + ISNULL(Deleted.IncidentLocation,'') + '}|I:{' + ISNULL(Inserted.IncidentLocation,'') + '}'
				,'D:{' + ISNULL(Deleted.Insurer,'') + '}|I:{' + ISNULL(Inserted.Insurer,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimNumber,'') + '}|I:{' + ISNULL(Inserted.ClaimNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClaimType,'') + '}|I:{' + ISNULL(Inserted.ClaimType,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimCode,'') + '}|I:{' + ISNULL(Inserted.ClaimCode,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimNotificationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimNotificationDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PaymentsToDate,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PaymentsToDate,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reserve,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reserve,'')) + '}'
				,'D:{' + ISNULL(Deleted.Broker,'') + '}|I:{' + ISNULL(Inserted.Broker,'') + '}'
				,'D:{' + ISNULL(Deleted.ReferralSource,'') + '}|I:{' + ISNULL(Inserted.ReferralSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Solicitors,'') + '}|I:{' + ISNULL(Inserted.Solicitors,'') + '}'
				,'D:{' + ISNULL(Deleted.Engineer,'') + '}|I:{' + ISNULL(Inserted.Engineer,'') + '}'
				,'D:{' + ISNULL(Deleted.Recovery,'') + '}|I:{' + ISNULL(Inserted.Recovery,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage,'') + '}|I:{' + ISNULL(Inserted.Storage,'') + '}'
				,'D:{' + ISNULL(Deleted.StorageAddress,'') + '}|I:{' + ISNULL(Inserted.StorageAddress,'') + '}'
				,'D:{' + ISNULL(Deleted.Repairer,'') + '}|I:{' + ISNULL(Inserted.Repairer,'') + '}'
				,'D:{' + ISNULL(Deleted.Hire,'') + '}|I:{' + ISNULL(Inserted.Hire,'') + '}'
				,'D:{' + ISNULL(Deleted.AccidentManagement,'') + '}|I:{' + ISNULL(Inserted.AccidentManagement,'') + '}'
				,'D:{' + ISNULL(Deleted.MedicalExaminer,'') + '}|I:{' + ISNULL(Inserted.MedicalExaminer,'') + '}'
				,'D:{' + ISNULL(Deleted.UndefinedSupplier,'') + '}|I:{' + ISNULL(Inserted.UndefinedSupplier,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PoliceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PoliceAttended,'')) + '}'
				,'D:{' + ISNULL(Deleted.PoliceForce,'') + '}|I:{' + ISNULL(Inserted.PoliceForce,'') + '}'
				,'D:{' + ISNULL(Deleted.PoliceReference,'') + '}|I:{' + ISNULL(Inserted.PoliceReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AmbulanceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AmbulanceAttended,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AttendedHospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AttendedHospital,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
				,'D:{' + ISNULL(Deleted.[SourceClaimStatus],'') + '}|I:{' + ISNULL(Inserted.[SourceClaimStatus],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[TotalClaimCost],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[TotalClaimCost],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[TotalClaimCostLessExcess],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[TotalClaimCostLessExcess],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BypassFraud],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BypassFraud],'')) + '}'
		INTO [SyncDML].[MDA_Incident2Person] (TaskId, DMLAction, Id, Incident_ID, Person_ID, IncidentLinkId, PartyType_Id, SubPartyType_Id, MojStatus_Id, IncidentTime, IncidentCircs, IncidentLocation, Insurer, ClaimNumber, ClaimStatus_Id, ClaimType, ClaimCode, ClaimNotificationDate, PaymentsToDate, Reserve, Broker, ReferralSource, Solicitors, Engineer, Recovery, Storage, StorageAddress, Repairer, Hire, AccidentManagement, MedicalExaminer, UndefinedSupplier, PoliceAttended, PoliceForce, PoliceReference, AmbulanceAttended, AttendedHospital, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus, SourceClaimStatus, TotalClaimCost, TotalClaimCostLessExcess, BypassFraud);

		--Incident2Organisation
		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].Incident2Organisation [Target]
		USING	(
				SELECT	*
				FROM	(
						SELECT	 ROW_NUMBER() OVER (PARTITION BY ISNULL(CAST(CT.MDA_ID AS VARCHAR), CT.MDA_IBase_Unique_ID) ORDER BY ISNULL(CAST(CT.MDA_ID AS VARCHAR), CT.MDA_IBase_Unique_ID)) RowID
								,CT.MDA_ID
								,CT.MDA_IBase_Unique_ID
								,[Sync].[fn_ExtractMDAID] (MI.MDA_Incident_ID_412284502) [Incident_Id]
								,[Sync].[fn_ExtractMDAID] (MO.MDA_Incident_ID_412284502) [Organisation_Id]
								,IIL.[Incident_Link_ID] IncidentLinkId
								,ISNULL(MPT.ID,0)  PartyType_Id
								,ISNULL(MSPT.ID,0) SubPartyType_Id
								,ISNULL(MMS.ID,0)  MojStatus_Id
								,IIL.[Incident_Time] IncidentTime
								,LEFT(IIL.[Incident_Circs],1024) IncidentCircs
								,LEFT(IIL.[Incident_Location],255) IncidentLocation
								,LEFT(IIL.[Insurer_],255) Insurer
								,IIL.[Claim_Number] ClaimNumber
								,ISNULL(MCS.ID,0) ClaimStatus_Id
								,LEFT(IIL.[Claim_Type],50) ClaimType
								,LEFT(IIL.[Claim_Code],80) ClaimCode
								,IIL.[Claim_Notification_Date] ClaimNotificationDate
								,IIL.[Payments_] PaymentsToDate
								,IIL.[Reserve_] Reserve
								,LEFT(IIL.[Broker_],255) Broker
								,LEFT(IIL.[Referral_Source],255) ReferralSource
								,LEFT(IIL.[Solicitors_],255) Solicitors
								,LEFT(IIL.[Engineer_],255) Engineer
								,LEFT(IIL.[Recovery_],255) Recovery
								,LEFT(IIL.[Storage_],255) Storage
								,LEFT(IIL.[Storage_Address],255) StorageAddress
								,LEFT(IIL.[Repairer_],255) Repairer
								,LEFT(IIL.[Hire_],255) Hire
								,LEFT(IIL.[Accident_Management],255) AccidentManagement
								,LEFT(IIL.[Medical_Examiner],255) MedicalExaminer
								,LEFT(IIL.[Undefined_Supplier],255) UndefinedSupplier
								,IIL.[Police_Attended] PoliceAttended
								,IIL.[Police_Force] PoliceForce
								,IIL.[Police_Reference] PoliceReference
								,IIL.[Ambulance_Attended] AmbulanceAttended
								,IIL.[Attended_Hospital] AttendedHospital
								,LEFT(IIL.[x5x5x5_Grading_412284402],10) [FiveGrading]
								,ILE.Confidence [LinkConfidence]
								,IIL.[Source_411765484] [Source]
								,IIL.[Source_Reference_411765487] [SourceReference]
								,LEFT(IIL.[Source_Description_411765489],1024) [SourceDescription]
								,IIL.Create_User [CreatedBy]
								,IIL.Create_Date [CreatedDate]
								,IIL.Last_Upd_User [ModifiedBy]
								,IIL.Last_Upd_Date [ModifiedDate]
								,IIL.[Risk_Claim_ID_414244883] [RiskClaim_Id]
								,IIL.[Risk_Claim_ID_414244883] [BaseRiskClaim_Id]
								,IIL.Unique_ID [IBaseId]
								,IIL.Record_Status [RecordStatus]
								,IIL.[Medical_Legal] [MedicalLegal]
								,IIL.[Recovery_Address] [RecoveryAddress]
								,IIL.[Repairer_Address] [RepairerAddress]
								,IIL.[Inspection_Address] [InspectionAddress]
						FROM #CT CT
						INNER JOIN [MDA_IBase].[dbo].[Incident_Link] IIL ON IIL.Unique_ID = CT.MDA_IBase_Unique_ID
						INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IIL.Unique_ID
						INNER JOIN [MDA_IBase].[dbo].[Incident_] MI ON MI.Unique_ID = ILE.Entity_ID1
						INNER JOIN [MDA_IBase].[dbo].[Organisation_] MO ON MO.Unique_ID = ILE.Entity_ID2
						LEFT JOIN [MDA].[dbo].[ClaimStatus] MCS ON MCS.[Text] = IIL.[Claim_Status]
						LEFT JOIN [MDA].[dbo].[MojStatus] MMS ON MMS.[Text] = IIL.Moj_Status
						LEFT JOIN [MDA].[dbo].[PartyType] MPT ON MPT.[PartyTypeText] = IIL.Party_Type
						LEFT JOIN [MDA].[dbo].[SubPartyType] MSPT ON  MSPT.[SubPartyText] = IIL.Sub_Party_Type
						WHERE CT.DoNotReplicate IS NULL
						AND CT.ChangeDatabase = 'MDA_IBase'
						AND MI.[Incident_Date] < GETDATE()
					) Data
				WHERE RowID = 1
			) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  Incident_Id			= Source.Incident_Id
			,Organisation_Id		= Source.Organisation_Id
			,IncidentLinkId			= Source.IncidentLinkId
			,PartyType_Id			= Source.PartyType_Id
			,SubPartyType_Id		= Source.SubPartyType_Id
			,MojStatus_Id			= Source.MojStatus_Id
			,IncidentTime			= Source.IncidentTime
			,IncidentCircs			= Source.IncidentCircs
			,IncidentLocation		= Source.IncidentLocation
			,Insurer				= Source.Insurer
			,ClaimNumber			= Source.ClaimNumber
			,ClaimStatus_Id			= Source.ClaimStatus_Id
			,ClaimType				= Source.ClaimType
			,ClaimCode				= Source.ClaimCode
			,ClaimNotificationDate	= Source.ClaimNotificationDate
			,PaymentsToDate			= Source.PaymentsToDate
			,Reserve				= Source.Reserve
			,Broker					= Source.Broker
			,ReferralSource			= Source.ReferralSource
			,Solicitors				= Source.Solicitors
			,Engineer				= Source.Engineer
			,Recovery				= Source.Recovery
			,Storage				= Source.Storage
			,StorageAddress			= Source.StorageAddress
			,Repairer				= Source.Repairer
			,Hire					= Source.Hire
			,AccidentManagement		= Source.AccidentManagement
			,MedicalExaminer		= Source.MedicalExaminer
			,UndefinedSupplier		= Source.UndefinedSupplier
			,PoliceAttended			= Source.PoliceAttended
			,PoliceForce			= Source.PoliceForce
			,PoliceReference		= Source.PoliceReference
			,AmbulanceAttended		= Source.AmbulanceAttended
			,AttendedHospital		= Source.AttendedHospital
			,FiveGrading			= Source.FiveGrading
			,LinkConfidence			= Source.LinkConfidence
			,Source					= Source.Source
			,SourceReference		= Source.SourceReference
			,SourceDescription		= Source.SourceDescription
			,CreatedBy				= Source.CreatedBy
			,CreatedDate			= Source.CreatedDate
			,ModifiedBy				= Source.ModifiedBy
			,ModifiedDate			= Source.ModifiedDate
			,RiskClaim_Id			= Source.RiskClaim_Id
			,BaseRiskClaim_Id		= Source.BaseRiskClaim_Id
			,IBaseId				= Source.IBaseId
			,RecordStatus			= Source.RecordStatus
			,ADARecordStatus		= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
			,[MedicalLegal]			= Source.MedicalLegal
			,[RecoveryAddress]		= Source.RecoveryAddress
			,[RepairerAddress]		= Source.RepairerAddress
			,[InspectionAddress]	= Source.InspectionAddress
		WHEN NOT MATCHED THEN INSERT 
			(
			 Incident_Id
			,Organisation_Id
			,IncidentLinkId
			,PartyType_Id
			,SubPartyType_Id
			,MojStatus_Id
			,IncidentTime
			,IncidentCircs
			,IncidentLocation
			,Insurer
			,ClaimNumber
			,ClaimStatus_Id
			,ClaimType
			,ClaimCode
			,ClaimNotificationDate
			,PaymentsToDate
			,Reserve
			,Broker
			,ReferralSource
			,Solicitors
			,Engineer
			,Recovery
			,Storage
			,StorageAddress
			,Repairer
			,Hire
			,AccidentManagement
			,MedicalExaminer
			,UndefinedSupplier
			,PoliceAttended
			,PoliceForce
			,PoliceReference
			,AmbulanceAttended
			,AttendedHospital
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			,[MedicalLegal]
			,[RecoveryAddress]
			,[RepairerAddress]
			,[InspectionAddress]
			,[OrganisationType_Id]
			)
		VALUES
			(
			 Source.Incident_Id
			,Source.Organisation_Id
			,Source.IncidentLinkId
			,Source.PartyType_Id
			,Source.SubPartyType_Id
			,Source.MojStatus_Id
			,Source.IncidentTime
			,Source.IncidentCircs
			,Source.IncidentLocation
			,Source.Insurer
			,Source.ClaimNumber
			,Source.ClaimStatus_Id
			,Source.ClaimType
			,Source.ClaimCode
			,Source.ClaimNotificationDate
			,Source.PaymentsToDate
			,Source.Reserve
			,Source.Broker
			,Source.ReferralSource
			,Source.Solicitors
			,Source.Engineer
			,Source.Recovery
			,Source.Storage
			,Source.StorageAddress
			,Source.Repairer
			,Source.Hire
			,Source.AccidentManagement
			,Source.MedicalExaminer
			,Source.UndefinedSupplier
			,Source.PoliceAttended
			,Source.PoliceForce
			,Source.PoliceReference
			,Source.AmbulanceAttended
			,Source.AttendedHospital
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10
			,Source.MedicalLegal
			,Source.RecoveryAddress
			,Source.RepairerAddress
			,Source.InspectionAddress
			,1
			)
		OUTPUT	 @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IsClaimLevelLink,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IsClaimLevelLink,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SubPartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SubPartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncidentTime,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncidentTime,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentCircs,'') + '}|I:{' + ISNULL(Inserted.IncidentCircs,'') + '}'
				,'D:{' + ISNULL(Deleted.IncidentLocation,'') + '}|I:{' + ISNULL(Inserted.IncidentLocation,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PaymentsToDate,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PaymentsToDate,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reserve,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reserve,'')) + '}'
				,'D:{' + ISNULL(Deleted.Insurer,'') + '}|I:{' + ISNULL(Inserted.Insurer,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimNumber,'') + '}|I:{' + ISNULL(Inserted.ClaimNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClaimType,'') + '}|I:{' + ISNULL(Inserted.ClaimType,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimCode,'') + '}|I:{' + ISNULL(Inserted.ClaimCode,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimNotificationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimNotificationDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Broker,'') + '}|I:{' + ISNULL(Inserted.Broker,'') + '}'
				,'D:{' + ISNULL(Deleted.ReferralSource,'') + '}|I:{' + ISNULL(Inserted.ReferralSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Solicitors,'') + '}|I:{' + ISNULL(Inserted.Solicitors,'') + '}'
				,'D:{' + ISNULL(Deleted.Engineer,'') + '}|I:{' + ISNULL(Inserted.Engineer,'') + '}'
				,'D:{' + ISNULL(Deleted.Recovery,'') + '}|I:{' + ISNULL(Inserted.Recovery,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage,'') + '}|I:{' + ISNULL(Inserted.Storage,'') + '}'
				,'D:{' + ISNULL(Deleted.StorageAddress,'') + '}|I:{' + ISNULL(Inserted.StorageAddress,'') + '}'
				,'D:{' + ISNULL(Deleted.Repairer,'') + '}|I:{' + ISNULL(Inserted.Repairer,'') + '}'
				,'D:{' + ISNULL(Deleted.Hire,'') + '}|I:{' + ISNULL(Inserted.Hire,'') + '}'
				,'D:{' + ISNULL(Deleted.AccidentManagement,'') + '}|I:{' + ISNULL(Inserted.AccidentManagement,'') + '}'
				,'D:{' + ISNULL(Deleted.MedicalExaminer,'') + '}|I:{' + ISNULL(Inserted.MedicalExaminer,'') + '}'
				,'D:{' + ISNULL(Deleted.UndefinedSupplier,'') + '}|I:{' + ISNULL(Inserted.UndefinedSupplier,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PoliceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PoliceAttended,'')) + '}'
				,'D:{' + ISNULL(Deleted.PoliceForce,'') + '}|I:{' + ISNULL(Inserted.PoliceForce,'') + '}'
				,'D:{' + ISNULL(Deleted.PoliceReference,'') + '}|I:{' + ISNULL(Inserted.PoliceReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AmbulanceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AmbulanceAttended,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AttendedHospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AttendedHospital,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Organisation] (TaskID, DMLAction, Id, Incident_Id, Organisation_Id, IsClaimLevelLink, IncidentLinkId, OrganisationType_Id, PartyType_Id, SubPartyType_Id, IncidentTime, IncidentCircs, IncidentLocation, PaymentsToDate, Reserve, Insurer, ClaimNumber, ClaimStatus_Id, ClaimType, ClaimCode, MojStatus_Id, ClaimNotificationDate, Broker, ReferralSource, Solicitors, Engineer, Recovery, Storage, StorageAddress, Repairer, Hire, AccidentManagement, MedicalExaminer, UndefinedSupplier, PoliceAttended, PoliceForce, PoliceReference, AmbulanceAttended, AttendedHospital, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus);
	
		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--Incident2Person
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Incident2Person]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SubPartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SubPartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncidentTime,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncidentTime,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentCircs,'') + '}|I:{' + ISNULL(Inserted.IncidentCircs,'') + '}'
				,'D:{' + ISNULL(Deleted.IncidentLocation,'') + '}|I:{' + ISNULL(Inserted.IncidentLocation,'') + '}'
				,'D:{' + ISNULL(Deleted.Insurer,'') + '}|I:{' + ISNULL(Inserted.Insurer,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimNumber,'') + '}|I:{' + ISNULL(Inserted.ClaimNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClaimType,'') + '}|I:{' + ISNULL(Inserted.ClaimType,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimCode,'') + '}|I:{' + ISNULL(Inserted.ClaimCode,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimNotificationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimNotificationDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PaymentsToDate,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PaymentsToDate,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reserve,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reserve,'')) + '}'
				,'D:{' + ISNULL(Deleted.Broker,'') + '}|I:{' + ISNULL(Inserted.Broker,'') + '}'
				,'D:{' + ISNULL(Deleted.ReferralSource,'') + '}|I:{' + ISNULL(Inserted.ReferralSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Solicitors,'') + '}|I:{' + ISNULL(Inserted.Solicitors,'') + '}'
				,'D:{' + ISNULL(Deleted.Engineer,'') + '}|I:{' + ISNULL(Inserted.Engineer,'') + '}'
				,'D:{' + ISNULL(Deleted.Recovery,'') + '}|I:{' + ISNULL(Inserted.Recovery,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage,'') + '}|I:{' + ISNULL(Inserted.Storage,'') + '}'
				,'D:{' + ISNULL(Deleted.StorageAddress,'') + '}|I:{' + ISNULL(Inserted.StorageAddress,'') + '}'
				,'D:{' + ISNULL(Deleted.Repairer,'') + '}|I:{' + ISNULL(Inserted.Repairer,'') + '}'
				,'D:{' + ISNULL(Deleted.Hire,'') + '}|I:{' + ISNULL(Inserted.Hire,'') + '}'
				,'D:{' + ISNULL(Deleted.AccidentManagement,'') + '}|I:{' + ISNULL(Inserted.AccidentManagement,'') + '}'
				,'D:{' + ISNULL(Deleted.MedicalExaminer,'') + '}|I:{' + ISNULL(Inserted.MedicalExaminer,'') + '}'
				,'D:{' + ISNULL(Deleted.UndefinedSupplier,'') + '}|I:{' + ISNULL(Inserted.UndefinedSupplier,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PoliceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PoliceAttended,'')) + '}'
				,'D:{' + ISNULL(Deleted.PoliceForce,'') + '}|I:{' + ISNULL(Inserted.PoliceForce,'') + '}'
				,'D:{' + ISNULL(Deleted.PoliceReference,'') + '}|I:{' + ISNULL(Inserted.PoliceReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AmbulanceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AmbulanceAttended,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AttendedHospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AttendedHospital,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
				,'D:{' + ISNULL(Deleted.[SourceClaimStatus],'') + '}|I:{' + ISNULL(Inserted.[SourceClaimStatus],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[TotalClaimCost],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[TotalClaimCost],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[TotalClaimCostLessExcess],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[TotalClaimCostLessExcess],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BypassFraud],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BypassFraud],'')) + '}'
		INTO [SyncDML].[MDA_Incident2Person] (TaskId, DMLAction, Id, Incident_ID, Person_ID, IncidentLinkId, PartyType_Id, SubPartyType_Id, MojStatus_Id, IncidentTime, IncidentCircs, IncidentLocation, Insurer, ClaimNumber, ClaimStatus_Id, ClaimType, ClaimCode, ClaimNotificationDate, PaymentsToDate, Reserve, Broker, ReferralSource, Solicitors, Engineer, Recovery, Storage, StorageAddress, Repairer, Hire, AccidentManagement, MedicalExaminer, UndefinedSupplier, PoliceAttended, PoliceForce, PoliceReference, AmbulanceAttended, AttendedHospital, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus, SourceClaimStatus, TotalClaimCost, TotalClaimCostLessExcess, BypassFraud)
		FROM [MDA].[dbo].[Incident2Person] MI2P
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MI2P.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--Incident2Organisation
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Incident2Organisation]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IsClaimLevelLink,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IsClaimLevelLink,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentLinkId,'') + '}|I:{' + ISNULL(Inserted.IncidentLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SubPartyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SubPartyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncidentTime,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncidentTime,'')) + '}'
				,'D:{' + ISNULL(Deleted.IncidentCircs,'') + '}|I:{' + ISNULL(Inserted.IncidentCircs,'') + '}'
				,'D:{' + ISNULL(Deleted.IncidentLocation,'') + '}|I:{' + ISNULL(Inserted.IncidentLocation,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PaymentsToDate,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PaymentsToDate,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reserve,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reserve,'')) + '}'
				,'D:{' + ISNULL(Deleted.Insurer,'') + '}|I:{' + ISNULL(Inserted.Insurer,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimNumber,'') + '}|I:{' + ISNULL(Inserted.ClaimNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClaimType,'') + '}|I:{' + ISNULL(Inserted.ClaimType,'') + '}'
				,'D:{' + ISNULL(Deleted.ClaimCode,'') + '}|I:{' + ISNULL(Inserted.ClaimCode,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClaimNotificationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClaimNotificationDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Broker,'') + '}|I:{' + ISNULL(Inserted.Broker,'') + '}'
				,'D:{' + ISNULL(Deleted.ReferralSource,'') + '}|I:{' + ISNULL(Inserted.ReferralSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Solicitors,'') + '}|I:{' + ISNULL(Inserted.Solicitors,'') + '}'
				,'D:{' + ISNULL(Deleted.Engineer,'') + '}|I:{' + ISNULL(Inserted.Engineer,'') + '}'
				,'D:{' + ISNULL(Deleted.Recovery,'') + '}|I:{' + ISNULL(Inserted.Recovery,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage,'') + '}|I:{' + ISNULL(Inserted.Storage,'') + '}'
				,'D:{' + ISNULL(Deleted.StorageAddress,'') + '}|I:{' + ISNULL(Inserted.StorageAddress,'') + '}'
				,'D:{' + ISNULL(Deleted.Repairer,'') + '}|I:{' + ISNULL(Inserted.Repairer,'') + '}'
				,'D:{' + ISNULL(Deleted.Hire,'') + '}|I:{' + ISNULL(Inserted.Hire,'') + '}'
				,'D:{' + ISNULL(Deleted.AccidentManagement,'') + '}|I:{' + ISNULL(Inserted.AccidentManagement,'') + '}'
				,'D:{' + ISNULL(Deleted.MedicalExaminer,'') + '}|I:{' + ISNULL(Inserted.MedicalExaminer,'') + '}'
				,'D:{' + ISNULL(Deleted.UndefinedSupplier,'') + '}|I:{' + ISNULL(Inserted.UndefinedSupplier,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PoliceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PoliceAttended,'')) + '}'
				,'D:{' + ISNULL(Deleted.PoliceForce,'') + '}|I:{' + ISNULL(Inserted.PoliceForce,'') + '}'
				,'D:{' + ISNULL(Deleted.PoliceReference,'') + '}|I:{' + ISNULL(Inserted.PoliceReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AmbulanceAttended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AmbulanceAttended,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.AttendedHospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.AttendedHospital,'')) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2Organisation] (TaskID, DMLAction, Id, Incident_Id, Organisation_Id, IsClaimLevelLink, IncidentLinkId, OrganisationType_Id, PartyType_Id, SubPartyType_Id, IncidentTime, IncidentCircs, IncidentLocation, PaymentsToDate, Reserve, Insurer, ClaimNumber, ClaimStatus_Id, ClaimType, ClaimCode, MojStatus_Id, ClaimNotificationDate, Broker, ReferralSource, Solicitors, Engineer, Recovery, Storage, StorageAddress, Repairer, Hire, AccidentManagement, MedicalExaminer, UndefinedSupplier, PoliceAttended, PoliceForce, PoliceReference, AmbulanceAttended, AttendedHospital, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Incident2Organisation] MI2O
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MI2O.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA_IBase].[dbo].[Incident_Link]
		SET  [MDA_Incident_ID_412284502] = IQ.MDA_Incident_ID_412284502
			,[Risk_Claim_ID_414244883]	 = IQ.RiskClaim_Id
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Accident_Management,'') + '}|I:{' + ISNULL(Inserted.Accident_Management,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Ambulance_Attended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Ambulance_Attended,'')) + '}'
				,'D:{' + ISNULL(Deleted.Broker_,'') + '}|I:{' + ISNULL(Inserted.Broker_,'') + '}'
				,'D:{' + ISNULL(Deleted.Claim_Code,'') + '}|I:{' + ISNULL(Inserted.Claim_Code,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Claim_Notification_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Claim_Notification_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Claim_Number,'') + '}|I:{' + ISNULL(Inserted.Claim_Number,'') + '}'
				,'D:{' + ISNULL(Deleted.Claim_Status,'') + '}|I:{' + ISNULL(Inserted.Claim_Status,'') + '}'
				,'D:{' + ISNULL(Deleted.Claim_Type,'') + '}|I:{' + ISNULL(Inserted.Claim_Type,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Engineer_,'') + '}|I:{' + ISNULL(Inserted.Engineer_,'') + '}'
				,'D:{' + ISNULL(Deleted.Hire_,'') + '}|I:{' + ISNULL(Inserted.Hire_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Incident_Circs,'') + '}|I:{' + ISNULL(Inserted.Incident_Circs,'') + '}'
				,'D:{' + ISNULL(Deleted.Incident_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Incident_Link_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Incident_Location,'') + '}|I:{' + ISNULL(Inserted.Incident_Location,'') + '}'
				,'D:{' + ISNULL(Deleted.Incident_Time,'') + '}|I:{' + ISNULL(Inserted.Incident_Time,'') + '}'
				,'D:{' + ISNULL(Deleted.Insurer_,'') + '}|I:{' + ISNULL(Inserted.Insurer_,'') + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Medical_Examiner,'') + '}|I:{' + ISNULL(Inserted.Medical_Examiner,'') + '}'
				,'D:{' + ISNULL(Deleted.MoJ_Status,'') + '}|I:{' + ISNULL(Inserted.MoJ_Status,'') + '}'
				,'D:{' + ISNULL(Deleted.Party_Type,'') + '}|I:{' + ISNULL(Inserted.Party_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.Payments_,'') + '}|I:{' + ISNULL(Inserted.Payments_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Police_Attended,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Police_Attended,'')) + '}'
				,'D:{' + ISNULL(Deleted.Police_Force,'') + '}|I:{' + ISNULL(Inserted.Police_Force,'') + '}'
				,'D:{' + ISNULL(Deleted.Police_Reference,'') + '}|I:{' + ISNULL(Inserted.Police_Reference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.Recovery_,'') + '}|I:{' + ISNULL(Inserted.Recovery_,'') + '}'
				,'D:{' + ISNULL(Deleted.Referral_Source,'') + '}|I:{' + ISNULL(Inserted.Referral_Source,'') + '}'
				,'D:{' + ISNULL(Deleted.Repairer_,'') + '}|I:{' + ISNULL(Inserted.Repairer_,'') + '}'
				,'D:{' + ISNULL(Deleted.Reserve_,'') + '}|I:{' + ISNULL(Inserted.Reserve_,'') + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Solicitors_,'') + '}|I:{' + ISNULL(Inserted.Solicitors_,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage_,'') + '}|I:{' + ISNULL(Inserted.Storage_,'') + '}'
				,'D:{' + ISNULL(Deleted.Storage_Address,'') + '}|I:{' + ISNULL(Inserted.Storage_Address,'') + '}'
				,'D:{' + ISNULL(Deleted.Sub_Party_Type,'') + '}|I:{' + ISNULL(Inserted.Sub_Party_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Undefined_Supplier,'') + '}|I:{' + ISNULL(Inserted.Undefined_Supplier,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Attended_Hospital,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Attended_Hospital,'')) + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
				,'D:{' + ISNULL(Deleted.[Source_Claim_Status],'') + '}|I:{' + ISNULL(Inserted.[Source_Claim_Status],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Total_Claim_Cost],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Total_Claim_Cost],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Total_Claim_Cost_Less_Excess],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Total_Claim_Cost_Less_Excess],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Bypass_Fraud],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Bypass_Fraud],'')) + '}'
		INTO [SyncDML].[MDAIBase_Incident_Link] (TaskId, DMLAction, Unique_ID, Accident_Management, AltEntity, Ambulance_Attended, Broker_, Claim_Code, Claim_Notification_Date, Claim_Number, Claim_Status, Claim_Type, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engineer_, Hire_, IconColour, Incident_Circs, Incident_Link_ID, Incident_Location, Incident_Time, Insurer_, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Medical_Examiner, MoJ_Status, Party_Type, Payments_, Police_Attended, Police_Force, Police_Reference, Record_Status, Recovery_, Referral_Source, Repairer_, Reserve_, SCC, Solicitors_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Storage_, Storage_Address, Sub_Party_Type, x5x5x5_Grading_412284402, Undefined_Supplier, Attended_Hospital, Risk_Claim_ID_414244883, Source_Claim_Status, Total_Claim_Cost, Total_Claim_Cost_Less_Excess, Bypass_Fraud)
		FROM [MDA_IBase].[dbo].[Incident_Link] IIL
		INNER JOIN (			
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MI2P.ID,'MDA.dbo.Incident2Person') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [MDA].[dbo].[Incident2Person] MI2P
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MI2P.IBaseId
					UNION ALL
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MI2O.ID,'MDA.dbo.Incident2Organisation') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [MDA].[dbo].[Incident2Organisation] MI2O
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MI2O.IBaseId
					) IQ ON IQ.[Unique_ID] =  IIL.[Unique_ID]
		WHERE IIL.MDA_Incident_ID_412284502 IS NULL
		OR IIL.MDA_Incident_ID_412284502 != IQ.MDA_Incident_ID_412284502

	END

	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Person] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Person] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Person] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Incident_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging

END TRY
BEGIN CATCH
	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(500)	= ISNULL(ERROR_PROCEDURE(),@@PROCID)
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +ISNULL(@Step,'?') +']'
	
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH