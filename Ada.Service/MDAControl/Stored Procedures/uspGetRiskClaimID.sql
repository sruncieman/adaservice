﻿

CREATE PROCEDURE [Sync].[uspGetRiskClaimID]
	@TableName VARCHAR(255),
	@Debug BIT = 0
AS
BEGIN

	DECLARE @RiskBatch_ID INT
	DECLARE @Date VARCHAR(10)
	DECLARE @SQL NVARCHAR(4000)

	SET @Date = CONVERT(varchar(10),GETDATE(),112)
	
	

	IF(SELECT TOP 1 ClientBatchReference FROM [$(MDA)].[dbo].RiskBatch ORDER BY ID DESC) <> @Date
	BEGIN
		INSERT INTO [$(MDA)].[dbo].RiskBatch (RiskClient_Id, BatchReference, BatchEntityType, ClientBatchReference, BatchStatus, BatchStatusLastModified, RiskOriginalFile_Id, CreatedDate, CreatedBy, TotalClaimsReceived, TotalNewClaims, TotalModifiedClaims, TotalClaimsLoaded, TotalClaimsInError, TotalClaimsSkipped, LoadingDurationInMs, [VisibilityStatus])
		SELECT 0, 'IBase8Syncronisation_' + CONVERT(VARCHAR(10),GETDATE(),112) + REPLACE(CONVERT(VARCHAR(10),GETDATE(),108),':',''), 'None', CONVERT(VARCHAR(10),GETDATE(),112), 0, GETDATE(), 0, GETDATE(), 'SynchAdmin',0,0,0,0,0,0,0,1
		SELECT @RiskBatch_Id = @@IDENTITY
	END
	ELSE
		SELECT @RiskBatch_ID =MAX(ID) FROM [$(MDA)].[dbo].RiskBatch 
	
	SET @SQL ='
	INSERT INTO #RiskClaimID 
	SELECT distinct 
		ct.MDA_IBase_Unique_ID, 
		ISNULL(Source_Reference_411765487,''0''), 
		CASE ISNULL(Source_Reference_411765487,''0'') WHEN ''0'' THEN 0 ELSE  rc.ID END 
	FROM #CT ct
		INNER JOIN [MDA_IBase].[dbo].' + @TableName +' tbl ON ct.MDA_IBase_Unique_ID = tbl.Unique_ID
		LEFT JOIN [MDA].[dbo].RiskClaim rc	ON tbl.Source_Reference_411765487 = rc.ClientClaimRefNumber'
	
	IF @Debug =1	
		PRINT @SQL

	Exec sp_ExecuteSQL @SQL


	INSERT INTO [$(MDA)].[dbo].RiskClaim (RiskBatch_Id, Incident_Id, MdaClaimRef, BaseRiskClaim_Id, SuppliedClaimStatus, ClaimStatus, ClaimStatusLastModified, ClientClaimRefNumber, LevelOneRequestedCount, LevelTwoRequestedCount, LevelOneRequestedWhen, LevelTwoRequestedWhen, LevelOneRequestedWho, LevelTwoRequestedWho, LevelOneUnavailable, LevelTwoUnavailable, SourceReference, SourceEntityEncodeFormat, SourceEntityXML, EntityClassType, CreatedDate, CreatedBy, LatestVersion, LatestScored, CallbackRequested, CallbackRequestedWhen, CallbackRequestedWho, ExternalServicesRequired, ExternalServicesRequestsRequired, ClaimReadStatus, LoadingDurationInMs, ValidationResults, CleansingResults)
	SELECT distincT @RiskBatch_Id, 0, '', 0, 0, 0, GETDATE(), SUBSTRING(SourceDescripion,1,50), 0, 0, NULL, NULL, NULL, NULL, 0 , 0, substring('Elite-' + SourceDescripion,1,50), 'sql', '', 'sql', GETDATE(), 'SynchnAdmin', 1, 0 ,0, NULL, NULL, 0, 0, 0, 0, NULL, NULL
	FROM #RiskClaimID r
	WHERE  ISNULL(IBaseM.Fn_GetEliteRefFromDbSource(SourceDescripion),'0')<>'0' and ISNULL(RiskClaimID,0)=0 


	UPDATE r
	SET RiskClaimID = ISNULL(rc.Id,0)
	FROM #RiskClaimID r
		LEFT JOIN [$(MDA)].[dbo].RiskClaim rc
			on rc.ClientClaimRefNumber = r.SourceDescripion

END



