﻿CREATE PROCEDURE [Rep].[uspReplicateEntityVehicle]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateEntityVehicle]
--
-- Description:		Replicate Entity Vehicle	
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLogging/Tidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Vehicle_')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 25 --ReplicateVehicleEntity
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	UPDATE [$(MDA_IBase)].[dbo].Vehicle_
	SET Record_Status = 254
	OUTPUT  @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + ISNULL(Deleted.Colour_,'') + '}|I:{' + ISNULL(Inserted.Colour_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Engine_Capacity,'') + '}|I:{' + ISNULL(Inserted.Engine_Capacity,'') + '}'
			,'D:{' + ISNULL(Deleted.Fuel_,'') + '}|I:{' + ISNULL(Inserted.Fuel_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.Make_,'') + '}|I:{' + ISNULL(Inserted.Make_,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Model_,'') + '}|I:{' + ISNULL(Inserted.Model_,'') + '}'
			,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{' + ISNULL(Inserted.Notes_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Transmission_,'') + '}|I:{' + ISNULL(Inserted.Transmission_,'') + '}'
			,'D:{' + ISNULL(Deleted.Vehicle_ID,'') + '}|I:{' + ISNULL(Inserted.Vehicle_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Vehicle_Registration,'') + '}|I:{' + ISNULL(Inserted.Vehicle_Registration,'') + '}'
			,'D:{' + ISNULL(Deleted.Vehicle_Type,'') + '}|I:{' + ISNULL(Inserted.Vehicle_Type,'') + '}'
			,'D:{' + ISNULL(Deleted.VIN_,'') + '}|I:{' + ISNULL(Inserted.VIN_,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Vehicle_] (TaskID, DMLAction, Unique_ID, AltEntity, Colour_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engine_Capacity, Fuel_, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Make_, MDA_Incident_ID_412284502, Model_, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Transmission_, Vehicle_ID, Vehicle_Registration, Vehicle_Type, VIN_, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.EntityRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo].Vehicle_ a ON ent.IBase8EntityID = a.Unique_ID
	WHERE RecordAction ='D'

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo].Vehicle_ [Target]
	USING	(
			SELECT   DISTINCT C.IBase8EntityID
					,C.IBase5EntityID
					,A.*
			FROM IBaseM8Cur.Vehicle_ A
			INNER JOIN Rep.EntityRecord C ON A.Unique_ID = C.IBase8EntityID_Staging	
			) AS Source ON [Target].Unique_ID = Source.IBase8EntityID
	WHEN MATCHED THEN UPDATE
	SET  Vehicle_Registration			= Source.Vehicle_Registration
		,Colour_						= Source.Colour_
		,Make_							= Source.Make_
		,Model_							= Source.Model_
		,Engine_Capacity				= Source.Engine_Capacity
		,Fuel_							= Source.Fuel_
		,Transmission_					= Source.Transmission_
		,Vehicle_Type					= Source.Vehicle_Type	
		,Notes_							= Source.Notes_
		,Key_Attractor_412284410		= Source.Key_Attractor_412284410
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User
		,Record_Status					= Source.Record_Status
		,AltEntity						= Source.AltEntity
		,Do_Not_Disseminate_412284494	= Source.Do_Not_Disseminate_412284494
		,IconColour						= Source.IconColour
		,SCC							= Source.SCC
		,Status_Binding					= Source.Status_Binding
		,Vehicle_ID						= Source.Vehicle_ID
		,VIN_							= Source.VIN_
		,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883		= Source.Risk_Claim_ID_414244883
	WHEN NOT MATCHED THEN INSERT   
		(
		 Unique_ID
		,Vehicle_ID
		,Vehicle_Registration
		,Colour_
		,Make_
		,Model_
		,Fuel_
		,Engine_Capacity
		,Transmission_
		,Vehicle_Type
		,Notes_
		,Create_User
		,Create_Date
		,Do_Not_Disseminate_412284494
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,Source_411765484
		,Source_Reference_411765487
		,Source_Description_411765489
		,VIN_
		,Record_Status
		,AltEntity
		,IconColour
		,SCC
		,Status_Binding
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883
		)
	VALUES
		(
		 Source.IBase8EntityID
		,Source.IBase5EntityID
		,Source.Vehicle_Registration
		,Source.Colour_
		,Source.Make_
		,Source.Model_
		,Source.Fuel_
		,Source.Engine_Capacity
		,Source.Transmission_
		,Source.Vehicle_Type
		,Source.Notes_
		,Source.Create_User
		,Source.Create_Date
		,Source.Do_Not_Disseminate_412284494
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Source_411765484
		,Source.Source_Reference_411765487
		,Source.Source_Description_411765489
		,Source.VIN_
		,Source.Record_Status
		,Source.AltEntity
		,Source.IconColour
		,Source.SCC
		,Source.Status_Binding
		,Source.x5x5x5_Grading_412284402
		,Source.Risk_Claim_ID_414244883	
		)
	OUTPUT  @OutputTaskID
			,$Action
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + ISNULL(Deleted.Colour_,'') + '}|I:{' + ISNULL(Inserted.Colour_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Engine_Capacity,'') + '}|I:{' + ISNULL(Inserted.Engine_Capacity,'') + '}'
			,'D:{' + ISNULL(Deleted.Fuel_,'') + '}|I:{' + ISNULL(Inserted.Fuel_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.Make_,'') + '}|I:{' + ISNULL(Inserted.Make_,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Model_,'') + '}|I:{' + ISNULL(Inserted.Model_,'') + '}'
			,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{' + ISNULL(Inserted.Notes_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Transmission_,'') + '}|I:{' + ISNULL(Inserted.Transmission_,'') + '}'
			,'D:{' + ISNULL(Deleted.Vehicle_ID,'') + '}|I:{' + ISNULL(Inserted.Vehicle_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Vehicle_Registration,'') + '}|I:{' + ISNULL(Inserted.Vehicle_Registration,'') + '}'
			,'D:{' + ISNULL(Deleted.Vehicle_Type,'') + '}|I:{' + ISNULL(Inserted.Vehicle_Type,'') + '}'
			,'D:{' + ISNULL(Deleted.VIN_,'') + '}|I:{' + ISNULL(Inserted.VIN_,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Vehicle_] (TaskID, DMLAction, Unique_ID, AltEntity, Colour_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engine_Capacity, Fuel_, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Make_, MDA_Incident_ID_412284502, Model_, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Transmission_, Vehicle_ID, Vehicle_Registration, Vehicle_Type, VIN_, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Vehicle_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
		SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Vehicle_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
		SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Vehicle_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()
		
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH



