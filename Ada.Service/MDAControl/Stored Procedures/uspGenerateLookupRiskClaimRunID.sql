﻿



CREATE PROCEDURE [Rep].[uspGenerateLookupRiskClaimRunID]
/**************************************************************************************************/
-- ObjectName:			
--
-- Description:			
--
-- Steps:				
--
-- Dependencies:		
--
-- Usage:				Called within SP IBaseM.uspMigrationMaster
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-27			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 21 --GenerateLookupRiskClaimRunID
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END		
	------------------------------------------------------------
	--2)MainBody
	------------------------------------------------------------
	IF (SELECT TOP 1 ID FROM Rep.LinkRecord) IS NOT NULL
	BEGIN
		
		UPDATE ML
		SET RiskClaim_Id = RC.Id
		FROM Rep.LinkRecord ML
		INNER JOIN [$(MDA)].[dbo].RiskClaim RC ON RC.ClientClaimRefNumber = ML.EliteReference
		
		IF (SELECT TOP 1 ID FROM Rep.LinkRecord WHERE RiskClaim_Id = 0 AND EliteReference IS NOT NULL) IS NOT NULL
		BEGIN
			DECLARE @RiskBatch_Id INT
			DECLARE @EliteReferenceTable TABLE (EliteReference VARCHAR(50))
			
			INSERT INTO [$(MDA)].[dbo].RiskBatch (RiskClient_Id, BatchReference, BatchEntityType, ClientBatchReference, BatchStatus, BatchStatusLastModified, RiskOriginalFile_Id, CreatedDate, CreatedBy, TotalClaimsReceived, TotalNewClaims, TotalModifiedClaims, TotalClaimsLoaded, TotalClaimsInError, TotalClaimsSkipped, LoadingDurationInMs,[VisibilityStatus])
			SELECT 0, 'IBase5Replication_' + CONVERT(VARCHAR(10),GETDATE(),112) + REPLACE(CONVERT(VARCHAR(10),GETDATE(),108),':',''), 'None', CONVERT(VARCHAR(10),GETDATE(),112), 0, GETDATE(), 0, GETDATE(), 'ReplicationAdmin',0,0,0,0,0,0,0,1
			SELECT @RiskBatch_Id = @@IDENTITY 
			
			INSERT INTO @EliteReferenceTable (EliteReference)
			SELECT EliteReference
			FROM Rep.LinkRecord ML
			WHERE ISNULL(RiskClaim_Id,0) = 0 
			AND EliteReference IS NOT NULL
			GROUP BY EliteReference
			
			INSERT INTO [$(MDA)].[dbo].RiskClaim (RiskBatch_Id, Incident_Id, MdaClaimRef, BaseRiskClaim_Id, SuppliedClaimStatus, ClaimStatus, ClaimStatusLastModified, ClientClaimRefNumber, LevelOneRequestedCount, LevelTwoRequestedCount, LevelOneRequestedWhen, LevelTwoRequestedWhen, LevelOneRequestedWho, LevelTwoRequestedWho, LevelOneUnavailable, LevelTwoUnavailable, SourceReference, SourceEntityEncodeFormat, SourceEntityXML, EntityClassType, CreatedDate, CreatedBy, LatestVersion, LatestScored, CallbackRequested, CallbackRequestedWhen, CallbackRequestedWho, ExternalServicesRequired, ExternalServicesRequestsRequired, ClaimReadStatus, LoadingDurationInMs, ValidationResults, CleansingResults)
			SELECT @RiskBatch_Id, 0, '', 0, 0, 0, GETDATE(), SUBSTRING(EliteReference,1,50), 0, 0, NULL, NULL, NULL, NULL, 0 , 0, SUBSTRING('Elite-' + EliteReference,1,0), 'sql', '', 'sql', GETDATE(), 'ReplicationAdmin', 1, 0 ,0, NULL, NULL, 0, 0, 0, 0, NULL, NULL
			FROM @EliteReferenceTable
			
			UPDATE RC
			SET  MdaClaimRef		= 'IB5-' + RIGHT(REPLICATE('0',7) + CAST(ID AS VARCHAR(7)),7)
				,BaseRiskClaim_Id	= ID
			FROM [$(MDA)].[dbo].RiskClaim RC
			WHERE MdaClaimRef IS NULL
			AND CreatedBy = 'ReplicationAdmin'
			
			UPDATE ML
			SET RiskClaim_Id = RC.Id
			FROM Rep.LinkRecord ML
			INNER JOIN [$(MDA)].[dbo].RiskClaim RC ON RC.ClientClaimRefNumber = ML.EliteReference
			WHERE RiskClaim_Id = 0	
		END
	END
          
 	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					--,@InsertRowCount= @InsertRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END
END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
END CATCH








