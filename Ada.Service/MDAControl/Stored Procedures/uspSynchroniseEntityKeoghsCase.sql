﻿

CREATE PROCEDURE  [Sync].[uspSynchroniseEntityKeoghsCase]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseEntityKeoghsCase]
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)ActionTheChangeInMDA_IBase
--						3.3)DoDeletes
--						3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)	
--						4.1)ActionTheChangeInMDA
--						4.2)DoDeletes
--						4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					5)UpdateLoggingAndTidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-05-06			Paul Allen		Created
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT			

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1), ChangeDatabase VARCHAR(10) ,MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)
					
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 104 --SynchroniseKeoghsCaseEntity
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, MDA_ID, DMLAction)
	SELECT 'MDA', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA.dbo.KeoghsCase, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA_IBase.dbo.Keoghs_Case, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	--2.2)AddSomeIndexesToMakeTheProcessQuicker
	SET @Step ='2.2'
	 
	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT(MDA_IBase_Unique_ID ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3'

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IKC.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[Keoghs_Case] IKC ON IKC.Unique_ID = CT.MDA_IBase_Unique_ID
	AND IKC.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MKC.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].[KeoghsCase] MKC ON MKC.ID = CT.MDA_ID

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4'

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5'

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1'

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM MDA_IBase.[dbo].[_Keoghs_Case_NextID]
		UPDATE MDA_IBase.[dbo].[_Keoghs_Case_NextID] SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	'KEO' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY MDA_ID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--3.2)ActionTheChangeInMDA_IBase
		SET @Step ='3.2';

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[Keoghs_Case] [Target]
		USING	(
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, MKC.*, MKO.[OfficeName] [OfficeName], MIC.[ClientName], MCS.[StatusText] [CaseStatus]
				FROM #CT CT
				INNER JOIN MDA.[dbo].[KeoghsCase] MKC ON MKC.ID = CT.MDA_ID
				INNER JOIN MDA.[dbo].[KeoghsOffice] MKO ON MKO.ID = MKC.[KeoghsOffice_Id]
				INNER JOIN MDA.[dbo].[InsurersClients] MIC ON MIC.ID = MKC.[Client_Id]
				INNER JOIN MDA.[dbo].[KeoghsCaseStatus] MCS ON MCS.ID = MKC.[CaseStatus_Id]
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET  Case_Status					= CASE WHEN Source.[CaseStatus_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Target].Case_Status ELSE NULLIF(Source.[CaseStatus],'Unknown') END
			,Client_Claims_Handler			= Source.[ClientClaimsHandler]
			,Client_Reference				= Source.[ClientReference]
			,Clients_						= CASE WHEN Source.[Client_Id] = 1 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Target].Clients_ ELSE NULLIF(Source.[ClientName],'Unknown') END
			,Closure_Date					= Source.[ClosureDate]
			,Create_Date					= Source.[CreatedDate]
			,Create_User					= Source.[CreatedBy]
			,Current_Reserve				= Source.[CurrentReserve]
			,Fee_Earner						= Source.[FeeEarner]
			,Keoghs_Elite_Reference			= Source.[KeoghsEliteReference]
			,Keoghs_Office					= CASE WHEN Source.[KeoghsOffice_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Target].Keoghs_Office ELSE NULLIF(Source.[OfficeName],'Unknown') END
			,Key_Attractor_412284410		= Source.[KeyAttractor]
			,Last_Upd_Date					= Source.[ModifiedDate]
			,Last_Upd_User					= Source.[ModifiedBy]
			,Record_Status					= Source.[RecordStatus]
			,Source_Description_411765489	= Source.[SourceDescription]
			,Source_Reference_411765487		= Source.[SourceReference]
			,Weed_Date						= Source.[WeedDate]
		WHEN NOT MATCHED THEN INSERT
			(
			 Unique_ID
			,AltEntity
			,Case_Status
			,Client_Claims_Handler
			,Client_Reference
			,Clients_
			,Closure_Date
			,Create_Date
			,Create_User
			,Current_Reserve
			,Do_Not_Disseminate_412284494
			,Fee_Earner
			,Keoghs_Case_ID
			,Keoghs_Elite_Reference
			,Keoghs_Office
			,Key_Attractor_412284410
			,Last_Upd_Date
			,Last_Upd_User
			,MDA_Incident_ID_412284502
			,Record_Status
			,Source_411765484
			,Source_Description_411765489
			,Source_Reference_411765487
			,Weed_Date
			)
		VALUES
			(
			 Source.MDA_IBase_Unique_ID
			,'Keoghs Case'
			,CASE WHEN Source.[CaseStatus_Id] = 0 THEN NULL ELSE Source.[CaseStatus] END
			,Source.[ClientClaimsHandler]
			,Source.[ClientReference]
			,CASE WHEN Source.[Client_Id] = 1 THEN NULL ELSE Source.[ClientName] END
			,Source.[ClosureDate]
			,Source.[CreatedDate]
			,Source.[CreatedBy]
			,Source.[CurrentReserve]
			,0
			,Source.[FeeEarner]
			,Source.[KeoghsCaseId]
			,Source.[KeoghsEliteReference]
			,CASE WHEN Source.[KeoghsOffice_Id] = 0 THEN NULL ELSE Source.[OfficeName] END
			,Source.[KeyAttractor]
			,Source.[ModifiedDate]
			,Source.[ModifiedBy]
			,[Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.KeoghsCase')
			,Source.[RecordStatus]
			,Source.[Source]
			,Source.[SourceDescription]
			,Source.[SourceReference]
			,Source.[WeedDate]
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Case_Status,'') + '}|I:{' + ISNULL(Inserted.Case_Status,'') + '}'
				,'D:{' + ISNULL(Deleted.Client_Claims_Handler,'') + '}|I:{' + ISNULL(Inserted.Client_Claims_Handler,'') + '}'
				,'D:{' + ISNULL(Deleted.Client_Reference,'') + '}|I:{' + ISNULL(Inserted.Client_Reference,'') + '}'
				,'D:{' + ISNULL(Deleted.Clients_,'') + '}|I:{' + ISNULL(Inserted.Clients_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Closure_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Closure_Date,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Current_Reserve,'') + '}|I:{' + ISNULL(Inserted.Current_Reserve,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Fee_Earner,'') + '}|I:{' + ISNULL(Inserted.Fee_Earner,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Keoghs_Case_ID,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Case_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Keoghs_Elite_Reference,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Elite_Reference,'') + '}'
				,'D:{' + ISNULL(Deleted.Keoghs_Office,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Office,'') + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Weed_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Weed_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Keoghs_Case] (TaskID, DMLAction, Unique_ID, AltEntity, Case_Status, Client_Claims_Handler, Client_Reference, Clients_, Closure_Date, Create_Date, Create_User, Current_Reserve, Do_Not_Disseminate_412284494, Fee_Earner, IconColour, Keoghs_Case_ID, Keoghs_Elite_Reference, Keoghs_Office, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Weed_Date, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

		--3.3)DoDeletes
		SET @Step ='3.3';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[Keoghs_Case]
		OUTPUT	 @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Case_Status,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Client_Claims_Handler,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Client_Reference,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Clients_,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Closure_Date,''),126) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Current_Reserve,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Fee_Earner,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Keoghs_Case_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Keoghs_Elite_Reference,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Keoghs_Office,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Weed_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Keoghs_Case] (TaskID, DMLAction, Unique_ID, AltEntity, Case_Status, Client_Claims_Handler, Client_Reference, Clients_, Closure_Date, Create_Date, Create_User, Current_Reserve, Do_Not_Disseminate_412284494, Fee_Earner, IconColour, Keoghs_Case_ID, Keoghs_Elite_Reference, Keoghs_Office, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Weed_Date, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Keoghs_Case] IKC
		INNER JOIN #CT CT ON [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.KeoghsCase') = IKC.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';

		--3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='3.4';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MKC
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.KeoghsCaseId,'') + '}|I:{' + ISNULL(Inserted.KeoghsCaseId,'') + '}'
				,'D:{' + ISNULL(Deleted.KeoghsEliteReference,'') + '}|I:{' + ISNULL(Inserted.KeoghsEliteReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.KeoghsOffice_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.KeoghsOffice_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.FeeEarner,'') + '}|I:{' + ISNULL(Inserted.FeeEarner,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Client_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Client_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClientReference,'') + '}|I:{' + ISNULL(Inserted.ClientReference,'') + '}'
				,'D:{' + ISNULL(Deleted.ClientClaimsHandler,'') + '}|I:{' + ISNULL(Inserted.ClientClaimsHandler,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CaseStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CaseStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.CurrentReserve,'') + '}|I:{' + ISNULL(Inserted.CurrentReserve,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClosureDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClosureDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.WeedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.WeedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_KeoghsCase] (TaskID, DMLAction, Id, KeoghsCaseId, KeoghsEliteReference, KeoghsOffice_Id, FeeEarner, Client_Id, ClientReference, ClientClaimsHandler, CaseStatus_Id, CurrentReserve, ClosureDate, WeedDate, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM MDA.dbo.[KeoghsCase] MKC
		INNER JOIN #CT CT ON CT.MDA_ID = MKC.Id
 		WHERE MKC.IBaseId IS NULL
		OR MKC.IBaseId != CT.MDA_IBase_Unique_ID;

	END
	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		SET @Step ='4.1'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].[KeoghsCase] [Target]
		USING  (
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, IKC.*, MKO.ID [KeoghsOffice_Id], MIC.ID [Client_Id], MCS.ID [CaseStatus_Id]
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Keoghs_Case] IKC ON IKC.Unique_ID = CT.MDA_IBase_Unique_ID
				LEFT JOIN MDA.[dbo].[KeoghsOffice] MKO ON MKO.[OfficeName] = IKC.[Keoghs_Office]
				LEFT JOIN MDA.[dbo].[InsurersClients] MIC ON MIC.[ClientName] = IKC.[Clients_]
				LEFT JOIN MDA.[dbo].[KeoghsCaseStatus] MCS ON MCS.[StatusText] = IKC.[Case_Status]
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  KeoghsEliteReference	= Source.[Keoghs_Elite_Reference]
			,KeoghsOffice_Id		= ISNULL(Source.[KeoghsOffice_Id],0)
			,FeeEarner				= Source.[Fee_Earner]
			,Client_Id				= ISNULL(Source.[Client_Id],1)
			,ClientReference		= Source.[Client_Reference]
			,ClientClaimsHandler	= Source.[Client_Claims_Handler]
			,CaseStatus_Id			= ISNULL(Source.[CaseStatus_Id],0)
			,CurrentReserve			= Source.[Current_Reserve]
			,ClosureDate			= Source.[Closure_Date]
			,WeedDate				= Source.[Weed_Date]
			,KeyAttractor			= Source.[Key_Attractor_412284410]
			,SourceReference		= Source.[Source_Reference_411765487]
			,SourceDescription		= Source.[Source_Description_411765489]
			,CreatedBy				= Source.[Create_User]
			,CreatedDate			= Source.[Create_Date]
			,ModifiedBy				= Source.[Last_Upd_User]
			,ModifiedDate			= Source.[Last_Upd_Date]
			,IBaseId				= Source.[Unique_ID]
			,RecordStatus			= Source.[Record_Status]
			,ADARecordStatus		= CASE WHEN Source.Record_Status = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
				KeoghsCaseId
			,KeoghsEliteReference
			,KeoghsOffice_Id
			,FeeEarner
			,Client_Id
			,ClientReference
			,ClientClaimsHandler
			,CaseStatus_Id
			,CurrentReserve
			,ClosureDate
			,WeedDate
			,KeyAttractor
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES
			(
			Source.[Keoghs_Case_ID]
			,Source.[Keoghs_Elite_Reference]
			,ISNULL(Source.[KeoghsOffice_Id],0)
			,Source.[Fee_Earner]
			,ISNULL(Source.[Client_Id],1)
			,Source.[Client_Reference]
			,Source.[Client_Claims_Handler]
			,ISNULL(Source.[CaseStatus_Id],0)
			,Source.[Current_Reserve]
			,Source.[Closure_Date]
			,Source.[Weed_Date]
			,Source.[Key_Attractor_412284410]
			,Source.[Source_411765484]
			,Source.[Source_Reference_411765487]
			,Source.[Source_Description_411765489]
			,Source.[Create_User]
			,Source.[Create_Date]
			,Source.[Last_Upd_User]
			,Source.[Last_Upd_Date]
			,Source.[Unique_ID]
			,Source.[Record_Status]
			,10
			)
		OUTPUT	 @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.KeoghsCaseId,'') + '}|I:{' + ISNULL(Inserted.KeoghsCaseId,'') + '}'
				,'D:{' + ISNULL(Deleted.KeoghsEliteReference,'') + '}|I:{' + ISNULL(Inserted.KeoghsEliteReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.KeoghsOffice_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.KeoghsOffice_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.FeeEarner,'') + '}|I:{' + ISNULL(Inserted.FeeEarner,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Client_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Client_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClientReference,'') + '}|I:{' + ISNULL(Inserted.ClientReference,'') + '}'
				,'D:{' + ISNULL(Deleted.ClientClaimsHandler,'') + '}|I:{' + ISNULL(Inserted.ClientClaimsHandler,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CaseStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CaseStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.CurrentReserve,'') + '}|I:{' + ISNULL(Inserted.CurrentReserve,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClosureDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClosureDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.WeedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.WeedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_KeoghsCase] (TaskID, DMLAction, Id, KeoghsCaseId, KeoghsEliteReference, KeoghsOffice_Id, FeeEarner, Client_Id, ClientReference, ClientClaimsHandler, CaseStatus_Id, CurrentReserve, ClosureDate, WeedDate, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus);

		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[KeoghsCase]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.KeoghsCaseId,'') + '}|I:{' + ISNULL(Inserted.KeoghsCaseId,'') + '}'
				,'D:{' + ISNULL(Deleted.KeoghsEliteReference,'') + '}|I:{' + ISNULL(Inserted.KeoghsEliteReference,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.KeoghsOffice_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.KeoghsOffice_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.FeeEarner,'') + '}|I:{' + ISNULL(Inserted.FeeEarner,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Client_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Client_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.ClientReference,'') + '}|I:{' + ISNULL(Inserted.ClientReference,'') + '}'
				,'D:{' + ISNULL(Deleted.ClientClaimsHandler,'') + '}|I:{' + ISNULL(Inserted.ClientClaimsHandler,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CaseStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CaseStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.CurrentReserve,'') + '}|I:{' + ISNULL(Inserted.CurrentReserve,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ClosureDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ClosureDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.WeedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.WeedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_KeoghsCase] (TaskID, DMLAction, Id, KeoghsCaseId, KeoghsEliteReference, KeoghsOffice_Id, FeeEarner, Client_Id, ClientReference, ClientClaimsHandler, CaseStatus_Id, CurrentReserve, ClosureDate, WeedDate, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[KeoghsCase] MKC
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MKC.IBaseId
		WHERE CT.DMLAction = 'D';

		--4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='4.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE IKC
		SET MDA_Incident_ID_412284502 = [Sync].[fn_ResolveMDAID] (MKC.ID,'MDA.dbo.KeoghsCase')
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Case_Status,'') + '}|I:{' + ISNULL(Inserted.Case_Status,'') + '}'
				,'D:{' + ISNULL(Deleted.Client_Claims_Handler,'') + '}|I:{' + ISNULL(Inserted.Client_Claims_Handler,'') + '}'
				,'D:{' + ISNULL(Deleted.Client_Reference,'') + '}|I:{' + ISNULL(Inserted.Client_Reference,'') + '}'
				,'D:{' + ISNULL(Deleted.Clients_,'') + '}|I:{' + ISNULL(Inserted.Clients_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Closure_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Closure_Date,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Current_Reserve,'') + '}|I:{' + ISNULL(Inserted.Current_Reserve,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Fee_Earner,'') + '}|I:{' + ISNULL(Inserted.Fee_Earner,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Keoghs_Case_ID,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Case_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Keoghs_Elite_Reference,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Elite_Reference,'') + '}'
				,'D:{' + ISNULL(Deleted.Keoghs_Office,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Office,'') + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Weed_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Weed_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Keoghs_Case] (TaskID, DMLAction, Unique_ID, AltEntity, Case_Status, Client_Claims_Handler, Client_Reference, Clients_, Closure_Date, Create_Date, Create_User, Current_Reserve, Do_Not_Disseminate_412284494, Fee_Earner, IconColour, Keoghs_Case_ID, Keoghs_Elite_Reference, Keoghs_Office, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Weed_Date, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Keoghs_Case] IKC
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IKC.Unique_ID
		INNER JOIN [MDA].[dbo].[KeoghsCase] MKC ON MKC.IBaseId = IKC.Unique_ID
 		WHERE IKC.MDA_Incident_ID_412284502 IS NULL
		OR [Sync].[fn_ResolveMDAID] (MKC.ID,'MDA.dbo.KeoghsCase') != IKC.MDA_Incident_ID_412284502;

	END
	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_KeoghsCase] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_KeoghsCase] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_KeoghsCase] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Keoghs_Case] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Keoghs_Case] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Keoghs_Case] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

END TRY
BEGIN CATCH
	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +@Step +']'

	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH



