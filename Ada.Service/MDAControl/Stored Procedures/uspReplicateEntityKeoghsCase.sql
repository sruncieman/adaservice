﻿CREATE PROCEDURE [Rep].[uspReplicateEntityKeoghsCase]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateEntityKeoghsCase]
--
-- Description:		Replicate Entity Keoghs Case
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLogging/Tidy						
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-03-03			Paul Allen		Created
--		2014-07-23			Paul Allen		Added [Data_Type], [Primary_Weed_Date], [Primary_Weed_Details], [Secondary_Weed_Date], [Secondary_Weed_Details]
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Keoghs_Case')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 39 --ReplicateKeoghsCaseEntity
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	UPDATE [$(MDA_IBase)].[dbo].Keoghs_Case
	SET Record_Status = 254
	OUTPUT	 @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + ISNULL(Deleted.Case_Status,'') + '}|I:{' + ISNULL(Inserted.Case_Status,'') + '}'
			,'D:{' + ISNULL(Deleted.Client_Claims_Handler,'') + '}|I:{' + ISNULL(Inserted.Client_Claims_Handler,'') + '}'
			,'D:{' + ISNULL(Deleted.Client_Reference,'') + '}|I:{' + ISNULL(Inserted.Client_Reference,'') + '}'
			,'D:{' + ISNULL(Deleted.Clients_,'') + '}|I:{' + ISNULL(Inserted.Clients_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Closure_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Closure_Date,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + ISNULL(Deleted.Current_Reserve,'') + '}|I:{' + ISNULL(Inserted.Current_Reserve,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Fee_Earner,'') + '}|I:{' + ISNULL(Inserted.Fee_Earner,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Keoghs_Case_ID,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Case_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Keoghs_Elite_Reference,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Elite_Reference,'') + '}'
			,'D:{' + ISNULL(Deleted.Keoghs_Office,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Office,'') + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Weed_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Weed_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Keoghs_Case] (TaskID, DMLAction, Unique_ID, AltEntity, Case_Status, Client_Claims_Handler, Client_Reference, Clients_, Closure_Date, Create_Date, Create_User, Current_Reserve, Do_Not_Disseminate_412284494, Fee_Earner, IconColour, Keoghs_Case_ID, Keoghs_Elite_Reference, Keoghs_Office, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Weed_Date, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.EntityRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo].Keoghs_Case a ON ent.IBase8EntityID = a.Unique_ID
	WHERE RecordAction ='D'
	
	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo].Keoghs_Case [Target]
	USING	(
			SELECT   DISTINCT C.IBase8EntityID
					,C.IBase5EntityID
					,A.*
			FROM IBaseM8Cur.Keoghs_Case A
			INNER JOIN Rep.EntityRecord C ON A.Unique_ID = C.IBase8EntityID_Staging	
			) AS Source ON [Target].Unique_ID = Source.IBase8EntityID
	WHEN MATCHED THEN UPDATE
	SET  Keoghs_Office					= Source.Keoghs_Office
		,Fee_Earner						= Source.Fee_Earner
		,Clients_						= Source.Clients_
		,Client_reference				= Source.Client_reference
		,Client_Claims_Handler			= Source.Client_Claims_Handler
		,Case_Status					= Source.Case_Status
		,Current_Reserve				= Source.Current_Reserve
		,Closure_Date					= Source.Closure_Date
		,Weed_Date						= Source.Weed_Date
		,Key_Attractor_412284410		= Source.Key_Attractor_412284410
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User
		,Record_Status					= Source.Record_Status
		,AltEntity						= Source.AltEntity
		,Do_Not_Disseminate_412284494	= Source.Do_Not_Disseminate_412284494
		,IconColour						= Source.IconColour
		,Keoghs_Case_ID					= Source.Keoghs_Case_ID
		,Keoghs_Elite_Reference			= Source.Keoghs_Elite_Reference
		,SCC							= Source.SCC
		,Status_Binding					= Source.Status_Binding
		,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883		= Source.Risk_Claim_ID_414244883
		,[Data_Type]					= Source.[Data_Type]
		,[Primary_Weed_Date]			= Source.[Primary_Weed_Date]
		,[Primary_Weed_Details]			= Source.[Primary_Weed_Details]
		,[Secondary_Weed_Date]			= Source.[Secondary_Weed_Date]
		,[Secondary_Weed_Details]		= Source.[Secondary_Weed_Details]
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,AltEntity
		,Case_Status
		,Client_Claims_Handler
		,Client_Reference
		,Clients_
		,Closure_Date
		,Create_Date
		,Create_User
		,Current_Reserve
		,Do_Not_Disseminate_412284494
		,Fee_Earner
		,IconColour
		,Keoghs_Case_ID
		,Keoghs_Elite_Reference
		,Keoghs_Office
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,Record_Status
		,SCC
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding
		,Weed_Date
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883
		,[Primary_Weed_Date]
		,[Primary_Weed_Details]
		,[Secondary_Weed_Date]
		,[Secondary_Weed_Details]
		)
	VALUES
		(
		 Source.IBase8EntityID
		,Source.AltEntity
		,Source.Case_Status
		,Source.Client_Claims_Handler
		,Source.Client_Reference
		,Source.Clients_
		,Source.Closure_Date
		,Source.Create_Date
		,Source.Create_User
		,Source.Current_Reserve
		,Source.Do_Not_Disseminate_412284494
		,Source.Fee_Earner
		,Source.IconColour
		,Source.Keoghs_Case_ID
		,Source.Keoghs_Elite_Reference
		,Source.Keoghs_Office
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Record_Status
		,Source.SCC
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.Weed_Date
		,Source.x5x5x5_Grading_412284402
		,Source.Risk_Claim_ID_414244883	
		,Source.[Primary_Weed_Date]
		,Source.[Primary_Weed_Details]
		,Source.[Secondary_Weed_Date]
		,Source.[Secondary_Weed_Details]
		)
	OUTPUT	 @OutputTaskID
			,$Action
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + ISNULL(Deleted.Case_Status,'') + '}|I:{' + ISNULL(Inserted.Case_Status,'') + '}'
			,'D:{' + ISNULL(Deleted.Client_Claims_Handler,'') + '}|I:{' + ISNULL(Inserted.Client_Claims_Handler,'') + '}'
			,'D:{' + ISNULL(Deleted.Client_Reference,'') + '}|I:{' + ISNULL(Inserted.Client_Reference,'') + '}'
			,'D:{' + ISNULL(Deleted.Clients_,'') + '}|I:{' + ISNULL(Inserted.Clients_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Closure_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Closure_Date,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + ISNULL(Deleted.Current_Reserve,'') + '}|I:{' + ISNULL(Inserted.Current_Reserve,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Fee_Earner,'') + '}|I:{' + ISNULL(Inserted.Fee_Earner,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Keoghs_Case_ID,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Case_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Keoghs_Elite_Reference,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Elite_Reference,'') + '}'
			,'D:{' + ISNULL(Deleted.Keoghs_Office,'') + '}|I:{' + ISNULL(Inserted.Keoghs_Office,'') + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Weed_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Weed_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Keoghs_Case] (TaskID, DMLAction, Unique_ID, AltEntity, Case_Status, Client_Claims_Handler, Client_Reference, Clients_, Closure_Date, Create_Date, Create_User, Current_Reserve, Do_Not_Disseminate_412284494, Fee_Earner, IconColour, Keoghs_Case_ID, Keoghs_Elite_Reference, Keoghs_Office, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Weed_Date, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Keoghs_Case] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
		SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Keoghs_Case] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
		SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Keoghs_Case] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()
		
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
						@Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH


