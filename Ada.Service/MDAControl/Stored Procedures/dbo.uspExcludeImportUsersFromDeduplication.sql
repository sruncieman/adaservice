﻿CREATE PROCEDURE Sync.uspExcludeImportUsersFromDeduplication
/**************************************************************************************************/
-- ObjectName:		Sync.uspExcludeImportUsersFromDeduplication
--
-- Description:		Prevent upserts made by specific users from being deduplicate in ADA. Achieved
--					by amending the ADA Record Status. Users configured in table [MDAControl].[Sync].[DedupeUserExclusion].
--					Change Context used to prevent and infinite loop.
--
-- RevisionHistory:
--		Date				Author			Modification
--		2015-04-16			Paul Allen		Created
/**************************************************************************************************/
AS
SET NOCOUNT ON

---------------------------------------
--DeclareAndSetTemporaryObjects
---------------------------------------
SET NOCOUNT ON
DECLARE  @Sys_Change_Context	VARBINARY(128) = CONVERT(VARBINARY(128),'ExcludeImportUserFromDeduplication')
		,@StatementsToProcess	INT
		,@Counter				INT = 1
		,@SQL					NVARCHAR(MAX)

DECLARE @UpdateStatements TABLE (RowID INT IDENTITY(1,1), UpdateStatement NVARCHAR(MAX) NOT NULL)
INSERT INTO @UpdateStatements (UpdateStatement)
SELECT    ' SET NOCOUNT ON'
		+ ' ;WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)'
		+ ' UPDATE [MDA].' + QUOTENAME(S.Name) + '.' + QUOTENAME(T.Name)
		+ ' SET ADARecordStatus = ADARecordStatus - 10'
		+ ' WHERE ADARecordStatus >= 10'
		+ ' AND ISNULL(ModifiedBy,CreatedBy) IN (SELECT Username FROM [MDAControl].[Sync].[DedupeUserExclusion])'
		+ ' PRINT '' >> '' + CAST(@@ROWCOUNT AS VARCHAR) + '' Rows updated in '+ QUOTENAME(S.Name) + '.' + QUOTENAME(T.Name) +''''
FROM MDA.SYS.TABLES T
INNER JOIN SYS.SCHEMAS S ON S.SCHEMA_ID = T.SCHEMA_ID
INNER JOIN (
			SELECT T.OBJECT_ID TableID
			FROM MDA.SYS.TABLES T
			INNER JOIN MDA.SYS.COLUMNS C ON C.OBJECT_ID = T.OBJECT_ID
			AND C.Name IN ('CreatedBy','ModifiedBy','ADARecordStatus')
			GROUP BY T.OBJECT_ID
			HAVING COUNT(C.Name) = 3
			) Data ON Data.TableID = T.OBJECT_ID
SELECT @StatementsToProcess = @@ROWCOUNT

---------------------------------------
--ProcessTheUpdates
---------------------------------------
WHILE @StatementsToProcess >= @Counter
BEGIN
	SELECT @SQL = UpdateStatement FROM @UpdateStatements WHERE RowID = @Counter
	EXECUTE SP_EXECUTESQL	  @SQL
							, N'@Sys_Change_Context VARBINARY(128) '
							, @Sys_Change_Context
	SELECT @Counter += 1
END
