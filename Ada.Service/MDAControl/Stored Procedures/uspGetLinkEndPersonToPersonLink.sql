﻿
CREATE PROCEDURE [IBaseM].[uspGetLinkEndPersonToPersonLink]
AS
SET NOCOUNT ON

BEGIN TRY

	IF OBJECT_Id('tempdb..#_LinkEnd') IS NOT NULL
		DROP TABLE #_LinkEnd

	CREATE TABLE #_LinkEnd
	(
		Unique_ID VARCHAR(50),
		Old_Unique_ID  VARCHAR(50),
		Table_Id INT,
		TableCode  VARCHAR(3)
	)

	--OLD LINKS
	--PER

	INSERT INTO #_LinkEnd
	SELECT * FROM IBaseM.vNew_Id_Old_Id_Person_

	SELECT 
		B.Unique_ID,
		C.Unique_ID AS Entity_ID1,
		A.Confidence,
		A.Direction,
		D.Unique_ID AS Entity_ID2,
		C.Table_Id EntityType_ID1,	
		D.Table_Id EntityType_ID2,
		B.Table_Id LinkType_ID,
		Record_Status,	
		Record_Type,	
		SCC
	FROM 
		v_LinkEnd A
	INNER JOIN 
		 IBaseM.vNew_Id_Old_Id_Person_to_Person_Link B
		 ON A.Link_ID = B.Old_Unique_ID
	INNER JOIN
		 #_LinkEnd C
		 ON A.Entity_ID1 = C.Old_Unique_ID 
	INNER JOIN
		 #_LinkEnd D
		 ON A.Entity_ID2 = D.Old_Unique_ID 

	IF OBJECT_Id('tempdb..#_LinkEnd') IS NOT NULL
		DROP TABLE #_LinkEnd
END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH
