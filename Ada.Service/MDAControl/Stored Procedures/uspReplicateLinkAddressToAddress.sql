﻿





CREATE PROCEDURE [Rep].[uspReplicateLinkAddressToAddress]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateLinkAddressToAddress]	
--
-- Description:		Replicate Link Address To Address	
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLoggingTidyTidy							
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-03-03			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Address_to_Address_Link')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 50 --ReplicateAddressToAddressLink
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	UPDATE [$(MDA_IBase)].[dbo].Address_to_Address_Link
	SET Record_Status =254
	OUTPUT	 @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.[Address_Link_Type],'') + '}|I:{' + ISNULL(Inserted.[Address_Link_Type],'') + '}'
			,'D:{' + ISNULL(Deleted.[Address_to_Address_Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Address_to_Address_Link_ID],'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[End_of_Residency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[End_of_Residency],''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[IconColour],'')) + '}'
			,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
			,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
			,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
			,'D:{' + ISNULL(Deleted.[Notes_],'') + '}|I:{' + ISNULL(Inserted.[Notes_],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SCC],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Start_of_Residency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Start_of_Residency],''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Status_Binding],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[x5x5x5_Grading_412284402],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Risk_Claim_ID_414244883],'')) + '}'
	INTO [SyncDML].[MDAIBase_Address_to_Address_Link] (TaskID, DMLAction, Unique_ID, Address_Link_Type, Address_to_Address_Link_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, End_of_Residency, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Start_of_Residency, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.LinkRecord LR
	INNER JOIN [$(MDA_IBase)].[dbo].Address_to_Address_Link a ON LR.IBase8linkID = A.Unique_ID
	WHERE RecordAction ='D'

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo].Address_to_Address_Link [Target]
	USING	(
			SELECT   DISTINCT C.IBase8LinkID
					,C.IBase5LinkID
					,C.RiskClaim_Id
					,A.*
		FROM IBaseM8Cur.Address_to_Address_Link a
		INNER JOIN Rep.LinkRecord c ON a.Unique_ID = c.IBase8LinkID_Staging
		) AS Source ON [Target].Unique_ID = Source.IBase8LinkID
	WHEN MATCHED THEN UPDATE
	SET  Address_Link_Type				= Source.Address_Link_Type
		,[Start_of_Residency]			= Source.[Start_of_Residency]
		,End_of_Residency				= Source.End_of_Residency
		,Notes_							= Source.Notes_
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User
		,Risk_Claim_ID_414244883		= Source.RiskClaim_Id
		,Record_Status					= Source.Record_Status
		,[Address_to_Address_Link_ID]	= Source.[Address_to_Address_Link_ID]
		,[AltEntity]					= Source.[AltEntity]
		,[Do_Not_Disseminate_412284494]	= Source.[Do_Not_Disseminate_412284494]
		,[IconColour]					= Source.[IconColour]
		,[Key_Attractor_412284410]		= Source.[Key_Attractor_412284410]
		,[SCC]							= Source.[SCC]
		,[Status_Binding]				= Source.[Status_Binding]
		,[x5x5x5_Grading_412284402]		= Source.[x5x5x5_Grading_412284402]	
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,Address_Link_Type
		,Address_to_Address_Link_ID
		,AltEntity
		,Create_Date
		,Create_User
		,Do_Not_Disseminate_412284494
		,End_of_Residency
		,IconColour
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,Notes_
		,Record_Status
		,SCC
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,[Start_of_Residency]
		,Status_Binding
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883
		)
	VALUES
		(
		 Source.IBase8LinkID
		,Source.Address_Link_Type
		,Source.Address_to_Address_Link_ID
		,Source.AltEntity
		,Source.Create_Date
		,Source.Create_User
		,Source.Do_Not_Disseminate_412284494
		,Source.End_of_Residency
		,Source.IconColour
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Notes_
		,Source.Record_Status
		,Source.SCC
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.[Start_of_Residency]
		,Source.Status_Binding
		,Source.x5x5x5_Grading_412284402
		,Source.RiskClaim_ID	
		)
	OUTPUT	 @OutputTaskID
			,$Action
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.[Address_Link_Type],'') + '}|I:{' + ISNULL(Inserted.[Address_Link_Type],'') + '}'
			,'D:{' + ISNULL(Deleted.[Address_to_Address_Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Address_to_Address_Link_ID],'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[End_of_Residency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[End_of_Residency],''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[IconColour],'')) + '}'
			,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
			,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
			,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
			,'D:{' + ISNULL(Deleted.[Notes_],'') + '}|I:{' + ISNULL(Inserted.[Notes_],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SCC],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Start_of_Residency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Start_of_Residency],''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Status_Binding],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[x5x5x5_Grading_412284402],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Risk_Claim_ID_414244883],'')) + '}'
	INTO [SyncDML].[MDAIBase_Address_to_Address_Link] (TaskID, DMLAction, Unique_ID, Address_Link_Type, Address_to_Address_Link_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, End_of_Residency, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Start_of_Residency, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

	------------------------------------------------------------
	--4)UpdateLoggingTidyTidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_to_Address_Link]  WHERE TaskID = @OutputTaskID AND DMLAction = 'INSERT')
		SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_to_Address_Link]  WHERE TaskID = @OutputTaskID AND DMLAction IN ('UPDATE','UPDATE DELETE'))
		SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_to_Address_Link]  WHERE TaskID = @OutputTaskID AND DMLAction = 'DELETE')
		
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()
		
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END	
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'
END CATCH









