﻿CREATE PROCEDURE [dbo].[uspCRUDDBControlRun]
/**************************************************************************************************/
-- ObjectName:		MDAControl.dbo.uspCRUDDBControlRun
--
-- Description:		CRUD Procedure on table dbo.DBControlRun
--
-- Usage:			Logging
--
-- Steps:			1)SetTheTaskIDIfExists
--					2)DeleteUpdateOrInsertTheControlTaskRecord
--					3)ReadTheTaskID
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-24			Paul Allen		Created
/**************************************************************************************************/
(
 @Action			VARCHAR(1)		= NULL
,@RunID				INT				= NULL
,@RunNameID			INT				= NULL
,@RunStartTime		DATETIME		= NULL
,@RunEndTime		DATETIME		= NULL
,@Error				INT				= NULL
,@ErrorDescription	VARCHAR(1000)	= NULL
,@Details			VARCHAR(1000)	= NULL
,@OutputRunID		INT	OUTPUT
)

AS
SET NOCOUNT, XACT_ABORT ON

BEGIN TRY
	IF @Action IN ('U','D') AND NULLIF(@RunID,'') IS NULL
	BEGIN
		RAISERROR ('TheRunIDMustBeSuppliedForUpdateOrDeleteOperations',18,255)
		RETURN
	END

	/*1)SetTheTaskIDIfExists*/
	IF @RunID IS NULL
	SELECT @OutputRunID =	(
							SELECT MAX(RunID) 
							FROM dbo.DBControlRun
							WHERE RunNameID  = @RunNameID 
							AND Error IS NULL
							AND RunEndTime IS NULL
							AND RunID = (SELECT MAX(RunID) FROM dbo.DBControlRun)
							)
	
	/*2)DeleteUpdateOrInsertTheControlTaskRecord*/
	IF @OutputRunID IS NULL OR @Action IN ('U','D')
	BEGIN
		DECLARE @OuputTable TABLE (RunID INT)
		
		MERGE dbo.DBControlRun [TARGET]
		USING  (
				SELECT	 @RunID				RunID
						,@RunNameID			RunNameID
						,@RunStartTime		RunStartTime
						,@RunEndTime		RunEndTime
						,@Error				Error
						,@ErrorDescription	ErrorDescription
						,@Details			Details
				) [SOURCE] ON [SOURCE].RunID = [TARGET].RunID
		WHEN MATCHED AND @Action = 'D'
			THEN DELETE		
		WHEN MATCHED THEN UPDATE
			SET  RunNameID			= ISNULL(@RunNameID, [TARGET].RunNameID)
				,RunStartTime		= ISNULL(@RunStartTime, [TARGET].RunStartTime)
				,RunEndTime			= ISNULL(@RunEndTime, [TARGET].RunEndTime)
				,Error				= ISNULL(@Error, [TARGET].Error)
				,ErrorDescription	= ISNULL(@ErrorDescription, [TARGET].ErrorDescription)
				,Details			= ISNULL(@Details, [TARGET].Details)
		WHEN NOT MATCHED AND @RunNameID IS NOT NULL THEN INSERT
				(
				 RunNameID
				,RunEndTime
				,Error
				,ErrorDescription
				,Details
				)
				VALUES
				(
				 @RunNameID
				,@RunEndTime
				,@Error
				,@ErrorDescription
				,@Details
				)
			OUTPUT Inserted.RunID
			INTO @OuputTable (RunID);
		END
		
		/*3)ReadTheTaskID*/
		IF @OutputRunID IS NULL
			SELECT @OutputRunID = RunID FROM @OuputTable
				
END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH
