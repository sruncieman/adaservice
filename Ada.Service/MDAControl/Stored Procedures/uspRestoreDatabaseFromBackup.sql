﻿CREATE PROCEDURE [IBaseM].[uspRestoreDatabaseFromBackup]
/**************************************************************************************************/
-- ObjectName:			MDAControl.IBaseM.uspRestoreDatabaseFromBackup
--
-- Description:			Restore databases from a backup, checking that the backup is valid prior.
--
-- Steps:				1)ConfigureLogging,Create/DeclareRequiredObjects
--							1.1)ConfigureLogging
--							1.2)Create/DeclareRequiredObjects	
--						2)RestoreDatabaseFromBackupIfValid
--						3)RaiseErrorIfBackupIsNotValid
--						4)UpdateLogging/Tidy
--
-- Dependencies:		1)IBaseM.uspCheckBackupSuccess			
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-25			Paul Allen		Created
--		2015-01-15			Paul Allen		Added secondary data files
--		2015-04-21			Paul Allen		On Ibase restore drop tables relating to Search360 and reclaim space.
--		2015-11-08			Paul Allen		Now Drop the old database before the restore.
--		2016-01-21			Paul Allen		Amended to allow for Vanilla POC Creation
/**************************************************************************************************/
( 
 @BackupDatabaseName_ID	INT
,@RetainBackupDays		INT = 3 --DefaultIs3Days
,@Logging				BIT = 0	--DefaultIsOff
,@Debug					BIT = 0 --DefaultIsOff
,@IsVanillaPOC			BIT = 0 --DefaultIsOff
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@OutputRunID		INT
				,@OutputProcessID	INT
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@RunNameID			INT = CASE WHEN @IsVanillaPOC = 0 THEN 1 ELSE 140 END
	
		EXECUTE dbo.uspCRUDDBControlRun
					 @RunNameID		= @RunNameID --Replication_IBaseLiveToMDAIBase
					,@OutputRunID	= @OutputRunID OUTPUT
		
		SELECT @Details = 'User=['+SYSTEM_USER+']'				
		EXECUTE dbo.uspCRUDDBControlProcess
					 @RunID				= @OutputRunID
					,@ProcessNameID		= 2 --DatabaseRestore
					,@Details			= @Details
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@OutputProcessID	= @OutputProcessID OUTPUT
					
		SELECT @Details = 'Parameters=[@BackupDatabaseName_ID:'+CAST(@BackupDatabaseName_ID AS VARCHAR)+';@RetainBackupDays:'+CAST(@RetainBackupDays AS VARCHAR)+']User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ProcessID			= @OutputProcessID
					,@TaskNameID		= 3 --DatabaseRestore
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	
	--1.2)Create/DeclareRequiredObjects
	DECLARE	 @Success					INT
			,@Date						DATETIME		= GETDATE()
			,@DateString				VARCHAR(10)
			,@BackedUpDB				VARCHAR(255)
			,@CurrentDB					VARCHAR(100)
			,@PreviousDB				VARCHAR(100)
			,@SQL						NVARCHAR(MAX) = ''
			,@DataFilelocation			VARCHAR(255)
			,@SecondaryDataFileLocation	VARCHAR(255)
			,@LogFileLocation			VARCHAR(255)
			,@BackUp					VARCHAR(255)
			,@DatabaseName				VARCHAR(255)	= (SELECT DatabaseName FROM IBaseM.BackupDatabaseName WHERE ID = @BackupDatabaseName_ID)
			,@LiveDataFileName			VARCHAR(255)
			,@LiveSecondaryDataFileName	VARCHAR(255)
			,@LiveLogFileName			VARCHAR(255)
			,@SQLOut					NVARCHAR(MAX) = ''
			

	SELECT	 @DataFilelocation			= DataFileLocation
			,@SecondaryDataFilelocation = SecondaryDataFileLocation
			,@LogFileLocation			= LogFileLocation
			,@BackUp					= LiveBackupPath 
			,@LiveDataFileName			= LiveDataFileName
			,@LiveSecondaryDataFileName = LiveSecondaryDataFileName
			,@LiveLogFileName			= LiveLogFileName
	
	FROM IBaseM.BackupRestoreConfig
	WHERE BackupDatabaseName_ID = @BackupDatabaseName_ID

	SELECT	 @DateString				=  CONVERT(VARCHAR,@Date,112)
	SELECT	 @CurrentDB					=  @DatabaseName + '_' + @DateString
	SELECT	 @DataFilelocation			+= @CurrentDB + '.MDF'
			,@SecondaryDataFilelocation += @CurrentDB + '.NDF'
			,@LogFileLocation			+= @CurrentDB +'.LDF'
			,@BackUp					+= '.BAK'

	------------------------------------------------------------
	--2)RestoreDatabaseFromBackupIfValid
	------------------------------------------------------------
	IF @BackupDatabaseName_ID !=2 AND @IsVanillaPOC = 0
	BEGIN
		EXECUTE IBaseM.uspCheckBackupSuccess  @BackupDatabaseName_ID ,@Logging, @Debug, @OutputTaskID, @Success OUTPUT
	END
	ELSE
	BEGIN
		SELECT @Success = 0
	END

	IF @Success = 0
	BEGIN
		/*DropTheOldDatbase*/
		IF  (SELECT COUNT(1) FROM sys.databases WHERE PATINDEX('' + @DatabaseName +'_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', Name) >0)= @RetainBackupDays
		BEGIN
			SET @PreviousDB  = (SELECT TOP 1 name FROM sys.databases WHERE PATINDEX('' + @DatabaseName +'_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name )
			SET @SQL =   'ALTER DATABASE ' + @PreviousDB + 
						 ' SET SINGLE_USER ' +
						 ' WITH ROLLBACK IMMEDIATE' +
						 ' DROP DATABASE ' + @PreviousDB
			
			IF @Debug = 1
			PRINT @SQL
			
			EXEC SP_EXECUTESQL @SQL
		END
		/*RestoreTheDatbase*/
		IF NOT EXISTS(SELECT Name FROM sys.databases WHERE Name = @CurrentDB)
		BEGIN
		
			SET @SQL = (SELECT	'RESTORE DATABASE ' + @CurrentDB  + ' FROM  
								 DISK = ''' + @Backup + ''' WITH  FILE = 1,  
								 MOVE ''' + @LiveDataFileName + ''' TO ''' + @DataFileLocation +''',' +
								 CASE WHEN @LiveSecondaryDataFileName IS NULL THEN '' ELSE ' MOVE ''' + @LiveSecondaryDataFileName + ''' TO ''' + @SecondaryDataFilelocation +''',' END +
								 ' MOVE ''' + @LiveLogFileName + ''' TO ''' + @LogFilelocation +''''
						)
			PRINT @SQL
			
			--MapNetworkfile
			IF DEFAULT_DOMAIN() = 'FOXTEST' AND @BackupDatabaseName_ID = 1
			BEGIN
				EXEC XP_CMDSHELL 'NET USE R: \\192.168.200.70\KeoghsLiveADABackup /user:KNET\svc-ada-proxy "c0$t@c0ffee" /Persistent:No'
			END
			
			--RestoreTheDatabase
			EXEC SP_EXECUTESQL @SQL
			
			--UnMapNetworkfile
			IF DEFAULT_DOMAIN() = 'FOXTEST' AND @BackupDatabaseName_ID = 1
			BEGIN
				EXEC XP_CMDSHELL 'NET USE R: /Delete /Y'
			END

			--SetTheRecoveryModelToSimple
			SET @SQL =  'ALTER DATABASE ' + @CurrentDB + 
						' SET RECOVERY SIMPLE'

			EXEC SP_EXECUTESQL @SQL

			--DeleteTheSearch360TablesInIBase
			IF @BackupDatabaseName_ID = 1 AND @IsVanillaPOC = 0
			BEGIN
				--DropFKs
				SET @SQL =		'SELECT @SQLOut = @SQLOut + ''ALTER TABLE ' + @CurrentDB + '.[dbo].['' + TABLE_NAME + ''] DROP CONSTRAINT ['' + CONSTRAINT_NAME + ''];'' + CHAR(13) + CHAR(10)
								 FROM ' + @CurrentDB + '.INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
								 WHERE CONSTRAINT_TYPE = ''FOREIGN KEY'''

				EXECUTE SP_EXECUTESQL   @SQL,N'@SQLOut NVARCHAR(MAX) OUTPUT',@SQLOut OUTPUT
				SET @SQL = @SQLOut
				EXECUTE SP_EXECUTESQL	@SQL
				SET @SQLOut = ''

				--DropTriggers
				SET @SQL =		'SELECT @SQLOut = @SQLOut + ''DROP TRIGGER [dbo].['' + NAME + ''];'' + CHAR(13) + CHAR(10)
								 FROM ' + @CurrentDB + '.SYS.OBJECTS
								 WHERE TYPE = ''TR'''

				EXECUTE SP_EXECUTESQL   @SQL,N'@SQLOut NVARCHAR(MAX) OUTPUT',@SQLOut OUTPUT
				SET @SQL = 'USE ' + @CurrentDB + CHAR(13) + CHAR(10) + @SQLOut
				EXECUTE SP_EXECUTESQL	@SQL
				SET @SQLOut = ''

				--DropTables
				SET @SQL =		'SELECT @SQLOut = @SQLOut + ''DROP TABLE ' + @CurrentDB + '.[dbo].['' + NAME + ''];'' + CHAR(13) + CHAR(10)
								 FROM ' + @CurrentDB + '.SYS.TABLES
								 WHERE LEFT(Name,4) = ''_FS_''
								 '
				EXECUTE SP_EXECUTESQL   @SQL,N'@SQLOut NVARCHAR(MAX) OUTPUT',@SQLOut OUTPUT
				SET @SQL = @SQLOut
				EXECUTE SP_EXECUTESQL	@SQL

			END

			--ShrinkTheDataFiles
			SET @SQL =  'USE ' + @CurrentDB + 
						' DBCC SHRINKFILE (N'''+ @LiveDataFileName + ''', 0)'
						
			EXEC SP_EXECUTESQL @SQL

			--ShrinkTheLogFiles
			SET @SQL =  'USE ' + @CurrentDB + 
						' DBCC SHRINKFILE (N'''+ @LiveLogFileName + ''', 0)'
	
			EXEC SP_EXECUTESQL @SQL


		END
	END
	
	------------------------------------------------------------
	--3)RaiseErrorIfBackupIsNotValid
	-----------------------------------------------------------
	IF ISNULL(@Success,-1) > 0
		BEGIN
		DECLARE @Msg VARCHAR(1000) = 'TheBackupStatusIsNotEqualtoZERO'
		RAISERROR (@Msg,18,255)
			IF @Logging = 1
			BEGIN
				SELECT   @Now		= GETDATE()				
				EXECUTE dbo.uspCRUDDBControlTask
							 @Action			= 'U'
							,@TaskID			= @OutputTaskID
							,@ProcessID			= @OutputProcessID
							,@TaskEndTime		= @Now
							,@Error				= -1
							,@ErrorDescription	= @Msg
							,@OutputTaskID		= @OutputTaskID OUTPUT
				
				SELECT @Msg +=' [TaskID='+CAST(@OutputTaskID AS VARCHAR)+']'		
				EXECUTE dbo.uspCRUDDBControlProcess
							 @Action			= 'U'
							,@ProcessID			= @OutputProcessID
							,@RunID				= @OutputRunID
							,@ProcessEndTime	= @Now
							,@Error				= -1
							,@ErrorDescription	= @Msg
							,@OutputProcessID	= @OutputProcessID OUTPUT
							
				EXECUTE dbo.uspCRUDDBControlRun
							 @Action			= 'U'
							,@RunID				= @OutputRunID
							,@RunEndTime		= @Now
							,@Error				= -1
							,@ErrorDescription	= @Msg
							,@OutputRunID		= @OutputRunID OUTPUT
			END
		END
		
	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	-----------------------------------------------------------
	IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action		= 'U'
						,@TaskID		= @OutputTaskID
						,@ProcessID		= @OutputProcessID
						,@TaskEndTime	= @Now
						,@OutputTaskID	= @OutputTaskID OUTPUT
						
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@OutputProcessID	= @OutputProcessID OUTPUT
		END
	
END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
				
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@ProcessID			= @OutputProcessID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
			
			SELECT @ErrorMessage +=' [TaskID='+CAST(ISNULL(@OutputTaskID,-1) AS VARCHAR)+']'		
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputProcessID	= @OutputProcessID OUTPUT
						
			EXECUTE dbo.uspCRUDDBControlRun
						 @Action			= 'U'
						,@RunID				= @OutputRunID
						,@RunEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputRunID		= @OutputRunID OUTPUT
		END
END CATCH
