﻿

CREATE PROCEDURE  [Sync].[uspSynchroniseEntityPerson]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseEntityPerson]
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)ActionTheChangeInMDA_IBase
--						3.3)DoDeletes
--						3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)	
--						4.1)ActionTheChangeInMDA
--						4.2)DoDeletes
--						4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					5)UpdateLoggingAndTidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-05-12			Paul Allen		Created
--		2014-11-19			Paul Allen		Sync SanctionList Column - TFS 10036
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT			

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1), ChangeDatabase VARCHAR(10) ,MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)
					
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 125 --SynchronisePersonEntity
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, MDA_ID, DMLAction)
	SELECT 'MDA', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA.dbo.Person, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA_IBase.dbo.Person_, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	--2.2)AddSomeIndexesToMakeTheProcessQuicker
	SET @Step ='2.2'
	 
	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT(MDA_IBase_Unique_ID ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3'

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IP.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[Person_] IP ON IP.Unique_ID = CT.MDA_IBase_Unique_ID
	AND IP.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MP.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].[Person] MP ON MP.ID = CT.MDA_ID

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4'

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5'

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1'

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM MDA_IBase.[dbo].[_Person__NextID]
		UPDATE MDA_IBase.[dbo].[_Person__NextID] SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	'PER' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY MDA_ID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--3.2)ActionTheChangeInMDA_IBase
		SET @Step ='3.2'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[Person_] [Target]
		USING	(
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, MP.*, MG.[Text] [Gender_], MS.[Text] [Salutation_]
				FROM #CT CT
				INNER JOIN [MDA].[dbo].[Person] MP ON MP.ID = CT.MDA_ID
				INNER JOIN MDA.[dbo].[Gender] MG ON MG.[Id] = MP.[Gender_Id]
				INNER JOIN MDA.[dbo].[Salutation] MS ON MS.ID = MP.[Salutation_Id]
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET  AltEntity						= CASE WHEN Source.[Gender_] = 'Unknown' THEN 'Person' ELSE Source.[Gender_] END
			,Create_Date					= Source.[CreatedDate]
			,Create_User					= Source.[CreatedBy]
			,Date_of_Birth					= Source.[DateOfBirth]
			,First_Name						= Source.[FirstName]
			,Gender_						= CASE WHEN Source.[Gender_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Target].Gender_ ELSE NULLIF(Source.Gender_,'Unknown') END
			,Hobbies_						= Source.[Hobbies]
			,Key_Attractor_412284410		= Source.[KeyAttractor]
			,Last_Name						= Source.[LastName]
			,Last_Upd_Date					= Source.[ModifiedDate]
			,Last_Upd_User					= Source.[ModifiedBy]
			,Middle_Name					= Source.[MiddleName]
			,Nationality_					= Source.[Nationality]
			,Notes_							= Source.[Notes]
			,Occupation_					= Source.[Occupation]
			,Record_Status					= Source.[RecordStatus]
			,Salutation_					= CASE WHEN Source.[Salutation_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Target].[Salutation_] ELSE NULLIF(Source.[Salutation_],'Unknown') END
			,Schools_						= Source.[Schools]
			,Source_411765484				= Source.[Source]
			,Source_Description_411765489	= Source.[SourceDescription]
			,Source_Reference_411765487		= Source.[SourceReference]
			,Taxi_Driver_Licence			= Source.[TaxiDriverLicense]
			,Sanction_List					= Source.[SanctionList]
		WHEN NOT MATCHED THEN INSERT
			(
			 Unique_ID
			,AltEntity
			,Create_Date
			,Create_User
			,Date_of_Birth
			,Do_Not_Disseminate_412284494
			,First_Name
			,Gender_
			,Hobbies_
			,Key_Attractor_412284410
			,Last_Name
			,Last_Upd_Date
			,Last_Upd_User
			,MDA_Incident_ID_412284502
			,Middle_Name
			,Nationality_
			,Notes_
			,Occupation_
			,Person_ID
			,Record_Status
			,Salutation_
			,Schools_
			,Source_411765484
			,Source_Description_411765489
			,Source_Reference_411765487
			,Taxi_Driver_Licence
			,Sanction_List
			)
		VALUES
			(
			 Source.MDA_IBase_Unique_ID
			,CASE WHEN Source.[Gender_] = 'Unknown' THEN 'Person' ELSE Source.[Gender_] END
			,Source.[CreatedDate]
			,Source.[CreatedBy]
			,Source.[DateOfBirth]
			,0
			,Source.[FirstName]
			,CASE WHEN Source.[Gender_Id] = 0 THEN NULL ELSE Source.Gender_ END
			,Source.[Hobbies]
			,Source.[KeyAttractor]
			,Source.[LastName]
			,Source.[ModifiedDate]
			,Source.[ModifiedBy]
			,[Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Person')
			,Source.[MiddleName]
			,Source.[Nationality]
			,Source.[Notes]
			,Source.[Occupation]
			,Source.PersonID
			,Source.[RecordStatus]
			,CASE WHEN Source.[Salutation_Id] = 0 THEN NULL ELSE Source.[Salutation_] END
			,Source.[Schools]
			,Source.[Source]
			,Source.[SourceDescription]
			,Source.[SourceReference]
			,Source.[TaxiDriverLicense]
			,Source.[SanctionList]
			)
		OUTPUT   @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_of_Birth,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_of_Birth,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Document__Binding,'') + '}|I:{' + ISNULL(Inserted.Document__Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.First_Name,'') + '}|I:{' + ISNULL(Inserted.First_Name,'') + '}'
				,'D:{' + ISNULL(Deleted.Gender_,'') + '}|I:{' + ISNULL(Inserted.Gender_,'') + '}'
				,'D:{' + ISNULL(Deleted.Hobbies_,'') + '}|I:{' + ISNULL(Inserted.Hobbies_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + ISNULL(Deleted.Last_Name,'') + '}|I:{' + ISNULL(Inserted.Last_Name,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Middle_Name,'') + '}|I:{' + ISNULL(Inserted.Middle_Name,'') + '}'
				,'D:{' + ISNULL(Deleted.Nationality_,'') + '}|I:{' + ISNULL(Inserted.Nationality_,'') + '}'
				,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{' + ISNULL(Inserted.Notes_,'') + '}'
				,'D:{' + ISNULL(Deleted.Occupation_,'') + '}|I:{' + ISNULL(Inserted.Occupation_,'') + '}'
				,'D:{' + ISNULL(Deleted.Person_ID,'') + '}|I:{' + ISNULL(Inserted.Person_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Picture__Binding,'') + '}|I:{' + ISNULL(Inserted.Picture__Binding,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.Salutation_,'') + '}|I:{' + ISNULL(Inserted.Salutation_,'') + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Schools_,'') + '}|I:{' + ISNULL(Inserted.Schools_,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.Taxi_Driver_Licence,'') + '}|I:{' + ISNULL(Inserted.Taxi_Driver_Licence,'') + '}'
				,'D:{' + ISNULL(Deleted.VF_ID,'') + '}|I:{' + ISNULL(Inserted.VF_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Person_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Date_of_Birth, Do_Not_Disseminate_412284494, Document__Binding, First_Name, Gender_, Hobbies_, IconColour, Key_Attractor_412284410, Last_Name, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Middle_Name, Nationality_, Notes_, Occupation_, Person_ID, Picture__Binding, Record_Status, Salutation_, SCC, Schools_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Taxi_Driver_Licence, VF_ID, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

		--3.3)DoDeletes
		SET @Step ='3.3';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[Person_]
		OUTPUT @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_of_Birth,''),126) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Document__Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.First_Name,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Gender_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Hobbies_,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Name,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Middle_Name,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Nationality_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Occupation_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Person_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Picture__Binding,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Salutation_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Schools_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Taxi_Driver_Licence,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.VF_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Person_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Date_of_Birth, Do_Not_Disseminate_412284494, Document__Binding, First_Name, Gender_, Hobbies_, IconColour, Key_Attractor_412284410, Last_Name, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Middle_Name, Nationality_, Notes_, Occupation_, Person_ID, Picture__Binding, Record_Status, Salutation_, SCC, Schools_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Taxi_Driver_Licence, VF_ID, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Person_] IP
		INNER JOIN #CT CT ON [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Person') = IP.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';

		--3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='3.4';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MP
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT  @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.PersonId,'') + '}|I:{' + ISNULL(Inserted.PersonId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Gender_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Gender_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Salutation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Salutation_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.FirstName,'') + '}|I:{' + ISNULL(Inserted.FirstName,'') + '}'
				,'D:{' + ISNULL(Deleted.MiddleName,'') + '}|I:{' + ISNULL(Inserted.MiddleName,'') + '}'
				,'D:{' + ISNULL(Deleted.LastName,'') + '}|I:{' + ISNULL(Inserted.LastName,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.DateOfBirth,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.DateOfBirth,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Nationality,'') + '}|I:{' + ISNULL(Inserted.Nationality,'') + '}'
				,'D:{' + ISNULL(Deleted.Occupation,'') + '}|I:{' + ISNULL(Inserted.Occupation,'') + '}'
				,'D:{' + ISNULL(Deleted.TaxiDriverLicense,'') + '}|I:{' + ISNULL(Inserted.TaxiDriverLicense,'') + '}'
				,'D:{' + ISNULL(Deleted.Schools,'') + '}|I:{' + ISNULL(Inserted.Schools,'') + '}'
				,'D:{' + ISNULL(Deleted.Hobbies,'') + '}|I:{' + ISNULL(Inserted.Hobbies,'') + '}'
				,'D:{' + ISNULL(Deleted.Notes,'') + '}|I:{' + ISNULL(Inserted.Notes,'') + '}'
				,'D:{' + ISNULL(Deleted.Documents,'') + '}|I:{' + ISNULL(Inserted.Documents,'') + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SanctionDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SanctionDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.SanctionSource,'') + '}|I:{' + ISNULL(Inserted.SanctionSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Person] (TaskID, DMLAction, Id, PersonId, Gender_Id, Salutation_Id, FirstName, MiddleName, LastName, DateOfBirth, Nationality, Occupation, TaxiDriverLicense, Schools, Hobbies, Notes, Documents, KeyAttractor, SanctionDate, SanctionSource, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM MDA.dbo.Person MP
		INNER JOIN #CT CT ON CT.MDA_ID = MP.Id
 		WHERE MP.IBaseId IS NULL
		OR MP.IBaseId != CT.MDA_IBase_Unique_ID;

	END

	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		SET @Step ='4.1'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].Person [Target]
		USING  (
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, IP.*, ISNULL(MG.[Id],0) [Gender_Id] ,MG.[Text] Gender, ISNULL(MS.ID,0) [Salutation_Id], MS.[Text] [Salutation]
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Person_] IP ON IP.Unique_ID = CT.MDA_IBase_Unique_ID
				LEFT JOIN MDA.[dbo].[Gender] MG ON MG.[Text] = IP.[Gender_]
				LEFT JOIN MDA.[dbo].[Salutation] MS ON MS.[Text] = IP.[Salutation_]
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  Gender_Id				= Source.Gender_Id
			,Salutation_Id			= Source.Salutation_Id
			,FirstName				= Source.[First_Name]
			,MiddleName				= Source.[Middle_Name]
			,LastName				= Source.[Last_Name]
			,DateOfBirth			= Sync.fnResolveSmallDateTimeToNull(Source.[Date_of_Birth])
			,Nationality			= LEFT(Source.[Nationality_],50)
			,Occupation				= Source.[Occupation_]
			,TaxiDriverLicense		= Source.[Taxi_Driver_Licence]
			,Schools				= Source.[Schools_]
			,Hobbies				= Source.[Hobbies_]
			,Notes					= LEFT(Source.[Notes_],1024)
			,KeyAttractor			= Source.[Key_Attractor_412284410]
			,Source					= Source.[Source_411765484]
			,SourceReference		= Source.[Source_Reference_411765487]
			,SourceDescription		= LEFT(Source.[Source_Reference_411765487],1024)
			,CreatedBy				= Source.[Create_User]
			,CreatedDate			= Source.[Create_Date]
			,ModifiedBy				= Source.[Last_Upd_User]
			,ModifiedDate			= Source.[Last_Upd_Date]
			,IBaseId				= Source.[Unique_ID]
			,RecordStatus			= Source.[Record_Status]
			,ADARecordStatus		= CASE WHEN Source.Record_Status = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
			,SanctionList			= Source.[Sanction_List]
		WHEN NOT MATCHED THEN INSERT
			(
			 PersonId
			,Gender_Id
			,Salutation_Id
			,FirstName
			,MiddleName
			,LastName
			,DateOfBirth
			,Nationality
			,Occupation
			,TaxiDriverLicense
			,Schools
			,Hobbies
			,Notes
			,KeyAttractor
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			,SanctionList
			)
		VALUES
			(
			 Source.Person_ID
			,Source.Gender_Id
			,Source.Salutation_Id
			,Source.[First_Name]
			,Source.[Middle_Name]
			,Source.[Last_Name]
			,Sync.fnResolveSmallDateTimeToNull(Source.[Date_of_Birth])
			,LEFT(Source.[Nationality_],50)
			,Source.[Occupation_]
			,Source.[Taxi_Driver_Licence]
			,Source.[Schools_]
			,Source.[Hobbies_]
			,LEFT(Source.[Notes_],1024)
			,Source.[Key_Attractor_412284410]
			,Source.[Source_411765484]
			,Source.[Source_Reference_411765487]
			,LEFT(Source.[Source_Reference_411765487],1024)
			,Source.[Create_User]
			,Source.[Create_Date]
			,Source.[Last_Upd_User]
			,Source.[Last_Upd_Date]
			,Source.[Unique_ID]
			,Source.[Record_Status]
			,10
			,Source.[Sanction_List]
			)
		OUTPUT  @OutputTaskID
				,$Action
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.PersonId,'') + '}|I:{' + ISNULL(Inserted.PersonId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Gender_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Gender_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Salutation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Salutation_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.FirstName,'') + '}|I:{' + ISNULL(Inserted.FirstName,'') + '}'
				,'D:{' + ISNULL(Deleted.MiddleName,'') + '}|I:{' + ISNULL(Inserted.MiddleName,'') + '}'
				,'D:{' + ISNULL(Deleted.LastName,'') + '}|I:{' + ISNULL(Inserted.LastName,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.DateOfBirth,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.DateOfBirth,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Nationality,'') + '}|I:{' + ISNULL(Inserted.Nationality,'') + '}'
				,'D:{' + ISNULL(Deleted.Occupation,'') + '}|I:{' + ISNULL(Inserted.Occupation,'') + '}'
				,'D:{' + ISNULL(Deleted.TaxiDriverLicense,'') + '}|I:{' + ISNULL(Inserted.TaxiDriverLicense,'') + '}'
				,'D:{' + ISNULL(Deleted.Schools,'') + '}|I:{' + ISNULL(Inserted.Schools,'') + '}'
				,'D:{' + ISNULL(Deleted.Hobbies,'') + '}|I:{' + ISNULL(Inserted.Hobbies,'') + '}'
				,'D:{' + ISNULL(Deleted.Notes,'') + '}|I:{' + ISNULL(Inserted.Notes,'') + '}'
				,'D:{' + ISNULL(Deleted.Documents,'') + '}|I:{' + ISNULL(Inserted.Documents,'') + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SanctionDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SanctionDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.SanctionSource,'') + '}|I:{' + ISNULL(Inserted.SanctionSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Person] (TaskID, DMLAction, Id, PersonId, Gender_Id, Salutation_Id, FirstName, MiddleName, LastName, DateOfBirth, Nationality, Occupation, TaxiDriverLicense, Schools, Hobbies, Notes, Documents, KeyAttractor, SanctionDate, SanctionSource, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus);

		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Person]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.PersonId,'') + '}|I:{' + ISNULL(Inserted.PersonId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Gender_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Gender_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Salutation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Salutation_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.FirstName,'') + '}|I:{' + ISNULL(Inserted.FirstName,'') + '}'
				,'D:{' + ISNULL(Deleted.MiddleName,'') + '}|I:{' + ISNULL(Inserted.MiddleName,'') + '}'
				,'D:{' + ISNULL(Deleted.LastName,'') + '}|I:{' + ISNULL(Inserted.LastName,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.DateOfBirth,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.DateOfBirth,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Nationality,'') + '}|I:{' + ISNULL(Inserted.Nationality,'') + '}'
				,'D:{' + ISNULL(Deleted.Occupation,'') + '}|I:{' + ISNULL(Inserted.Occupation,'') + '}'
				,'D:{' + ISNULL(Deleted.TaxiDriverLicense,'') + '}|I:{' + ISNULL(Inserted.TaxiDriverLicense,'') + '}'
				,'D:{' + ISNULL(Deleted.Schools,'') + '}|I:{' + ISNULL(Inserted.Schools,'') + '}'
				,'D:{' + ISNULL(Deleted.Hobbies,'') + '}|I:{' + ISNULL(Inserted.Hobbies,'') + '}'
				,'D:{' + ISNULL(Deleted.Notes,'') + '}|I:{' + ISNULL(Inserted.Notes,'') + '}'
				,'D:{' + ISNULL(Deleted.Documents,'') + '}|I:{' + ISNULL(Inserted.Documents,'') + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SanctionDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SanctionDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.SanctionSource,'') + '}|I:{' + ISNULL(Inserted.SanctionSource,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Person] (TaskID, DMLAction, Id, PersonId, Gender_Id, Salutation_Id, FirstName, MiddleName, LastName, DateOfBirth, Nationality, Occupation, TaxiDriverLicense, Schools, Hobbies, Notes, Documents, KeyAttractor, SanctionDate, SanctionSource, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Person] MP
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MP.IBaseId
		WHERE CT.DMLAction = 'D';

		--4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='4.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE IP
		SET MDA_Incident_ID_412284502 = [Sync].[fn_ResolveMDAID] (MP.ID,'MDA.dbo.Person')
		OUTPUT  @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_of_Birth,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_of_Birth,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Document__Binding,'') + '}|I:{' + ISNULL(Inserted.Document__Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.First_Name,'') + '}|I:{' + ISNULL(Inserted.First_Name,'') + '}'
				,'D:{' + ISNULL(Deleted.Gender_,'') + '}|I:{' + ISNULL(Inserted.Gender_,'') + '}'
				,'D:{' + ISNULL(Deleted.Hobbies_,'') + '}|I:{' + ISNULL(Inserted.Hobbies_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + ISNULL(Deleted.Last_Name,'') + '}|I:{' + ISNULL(Inserted.Last_Name,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Middle_Name,'') + '}|I:{' + ISNULL(Inserted.Middle_Name,'') + '}'
				,'D:{' + ISNULL(Deleted.Nationality_,'') + '}|I:{' + ISNULL(Inserted.Nationality_,'') + '}'
				,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{' + ISNULL(Inserted.Notes_,'') + '}'
				,'D:{' + ISNULL(Deleted.Occupation_,'') + '}|I:{' + ISNULL(Inserted.Occupation_,'') + '}'
				,'D:{' + ISNULL(Deleted.Person_ID,'') + '}|I:{' + ISNULL(Inserted.Person_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Picture__Binding,'') + '}|I:{' + ISNULL(Inserted.Picture__Binding,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.Salutation_,'') + '}|I:{' + ISNULL(Inserted.Salutation_,'') + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Schools_,'') + '}|I:{' + ISNULL(Inserted.Schools_,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.Taxi_Driver_Licence,'') + '}|I:{' + ISNULL(Inserted.Taxi_Driver_Licence,'') + '}'
				,'D:{' + ISNULL(Deleted.VF_ID,'') + '}|I:{' + ISNULL(Inserted.VF_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Person_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Date_of_Birth, Do_Not_Disseminate_412284494, Document__Binding, First_Name, Gender_, Hobbies_, IconColour, Key_Attractor_412284410, Last_Name, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Middle_Name, Nationality_, Notes_, Occupation_, Person_ID, Picture__Binding, Record_Status, Salutation_, SCC, Schools_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Taxi_Driver_Licence, VF_ID, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Person_] IP
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IP.Unique_ID
		INNER JOIN [MDA].[dbo].[Person] MP ON MP.IBaseId = IP.Unique_ID
 		WHERE IP.MDA_Incident_ID_412284502 IS NULL
		OR [Sync].[fn_ResolveMDAID] (MP.ID,'MDA.dbo.Person') != IP.MDA_Incident_ID_412284502;

	END

	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Person] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Person] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Person] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Person_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Person_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Person_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

END TRY
BEGIN CATCH

	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +@Step +']'

	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH


