﻿CREATE PROCEDURE [Rpt].[uspGenerateMobileClaimReport]
AS
SET NOCOUNT ON
SET DATEFORMAT DMY

--------------------------------------
--DeclareAndSetRequiredObjects
--------------------------------------
DECLARE  @RowsToProcess INT
		,@Counter INT =1
		,@RunNameID INT = 136
		,@ControlRunID INT
		,@ReportCreationOutputDeliveryConfiguration_Id INT
		,@RiskBatch_ID INT 
		,@SQL NVARCHAR(MAX)
		,@BCP VARCHAR(8000)
		,@ResultFileName VARCHAR(100)
		,@ResultTableName NVARCHAR(100) = (SELECT '[dbo].[MobileBatchResults_' + CAST(NEWID() AS NVARCHAR(50)) + ']')
		,@EmailSubject VARCHAR(100)
		,@MailProfile VARCHAR(50)
		,@To VARCHAR(200)
		,@CC VARCHAR(200)
		,@BCC VARCHAR(200)
		,@EmailBody_WithData VARCHAR(5000)
		,@EmailBody_WithoutData VARCHAR(5000)
		,@EmailBody VARCHAR(5000)

BEGIN TRY
	DECLARE @BatchesToProcess TABLE (RowID INT IDENTITY(1,1), ReportCreationOutputDeliveryConfiguration_Id INT NOT NULL, RiskBatch_Id INT NOT NULL)
			  
	IF OBJECT_ID('tempdb..#KeoghsRule') IS NOT NULL
		DROP TABLE #KeoghsRule 
	CREATE TABLE #KeoghsRule (RiskClaim_Id INT, KeoghsRule VARCHAR(MAX))

	IF OBJECT_ID('tempdb..#MobileBatchResults') IS NOT NULL
		DROP TABLE #MobileBatchResults
	CREATE TABLE #MobileBatchResults (RowID INT IDENTITY(1,1), ClaimNumber VARCHAR(50), IncidentDate VARCHAR(20), NotificationDate VARCHAR(20), ACEClaimStatus VARCHAR(80), NewOrUpdated VARCHAR(15), KeoghsRule VARCHAR(MAX))

	--------------------------------------
	--GetAListOfAllReportsThatNeedGenerating
	--------------------------------------
	INSERT INTO @BatchesToProcess (ReportCreationOutputDeliveryConfiguration_Id, RiskBatch_Id)
	SELECT DISTINCT RPT.[Id] ReportCreationOutputDeliveryConfiguration_Id, RB.Id RiskBatch_id
	FROM [MDA].[dbo].[RiskBatch] RB 
	INNER JOIN [Rpt].[ReportCreationOutputDeliveryConfiguration] RPT ON RPT.[RiskClient_Id] = RB.RiskClient_Id 
	WHERE RB.BatchStatus = 5
	AND [ReportName_Id] = 1 --Mobile Claim Report
	AND RPT.[IsActive] = 1
	AND RB.Id > ISNULL(RPT.[IgnoreBatchIdsTo],0)
	AND (NOT EXISTS (SELECT TOP 1 1 FROM [Rpt].[ReportNameRun] RPTR WHERE RPTR.[ReportCreationOutputDeliveryConfiguration_Id] = RPT.[Id] AND RPTR.[BatchID] = RB.Id AND [Resend] = 0)
	OR EXISTS (SELECT TOP 1 1 FROM [Rpt].[ReportNameRun] RPTR WHERE RPTR.[ReportCreationOutputDeliveryConfiguration_Id] = RPT.[Id] AND RPTR.[BatchID] = RB.Id AND [Resend] = 1))
	SELECT @RowsToProcess = @@ROWCOUNT

	--------------------------------------
	--LoopAroundGeneratingTheFileAndEmail
	--------------------------------------
	WHILE @RowsToProcess >= @Counter
	BEGIN
		--CreateTheControlRunRecord
		INSERT INTO [MDAControl].[dbo].[DBControlRun] ([RunNameID]) VALUES (@RunNameID)
		SELECT @ControlRunID = SCOPE_IDENTITY()

		--CreateTheImportTable
		SELECT @SQL = 'CREATE TABLE ' + @ResultTableName + ' (RowID INT IDENTITY(1,1), ClaimNumber VARCHAR(50), IncidentDate VARCHAR(20), NotificationDate VARCHAR(20), ACEClaimStatus VARCHAR(80), NewOrUpdated VARCHAR(15), KeoghsRule VARCHAR(MAX))'
		EXECUTE SP_EXECUTESQL  @SQL

		--PopulateTheParametersWeNeed
		SELECT	 @RiskBatch_ID = BTP.RiskBatch_Id
				,@ReportCreationOutputDeliveryConfiguration_Id = ReportCreationOutputDeliveryConfiguration_Id
				,@ResultFileName = DestinationFileLocation + REPLACE(REPLACE(REPLACE(REPLACE([DestinationFileName],'@ClientName',RC.ClientName),'@CurrentDateTime',REPLACE(REPLACE(CONVERT(VARCHAR,GETDATE(),120),'-',''),':','')),'@RiskBatch_ID',@RiskBatch_ID),' ','') + '.' + [DestinationFileFormat]
				,@EmailSubject = REPLACE(EmailSubject,'@BatchReceivedDate',CONVERT(VARCHAR,[CreatedDate],103))
				,@MailProfile = [EmailFromProfile]
				,@To = [EmailTo]
				,@CC = [EmailCC]
				,@BCC = [EMailBCC]
				,@EmailBody_WithData = [EmailBody_WithData]
				,@EmailBody_WithoutData = [EmailBody_WithoutData]
		FROM @BatchesToProcess BTP
		INNER JOIN [Rpt].[ReportCreationOutputDeliveryConfiguration] RPT ON RPT.[Id] = BTP.ReportCreationOutputDeliveryConfiguration_Id 
		INNER JOIN [MDA].[dbo].[RiskClient] RC ON RC.Id = RPT.RiskClient_Id
		INNER JOIN [MDA].[dbo].[RiskBatch] RB ON RB.Id = BTP.RiskBatch_Id
		WHERE BTP.RowID = @Counter

		--AddTheParemeterDetailsToTheControlRunTable
		UPDATE [dbo].[DBControlRun]
		SET [Details] = 'FileLocation:[' + ISNULL(@ResultFileName,'') + '];EmailSubject[' + ISNULL(@EmailSubject,'') + '];MailTo[' + ISNULL(@To,'') + '];MailCC[' + ISNULL(@CC,'') + '];MailBCC[' + ISNULL(@BCC,'') + ']'
		WHERE [RunID] = @ControlRunID

		--CheckToSeeIfWeHaveAFileLocation
		IF NULLIF(@ResultFileName,'') IS NOT NULL
		BEGIN
			--GetTheKeoghRuleDataFromTheXML
			;WITH XMLNAMESPACES (DEFAULT 'http://keo/MsgsV1', 'http://www.w3.org/2001/XMLSchema-instance' as i)
			INSERT INTO #KeoghsRule (RiskClaim_Id, KeoghsRule)
			SELECT	 RCR.[RiskClaim_Id]
					,CAST(Col.query('data(../H)') AS VARCHAR(MAX)) + ' ' +  CAST(Col.query('data(I)') AS VARCHAR(MAX)) + '; ' KeoghsRule
			FROM [MDA].[dbo].[RiskClaimRun] RCR
			INNER JOIN [MDA].[dbo].[RiskClaim] RC ON RC.Id = RCR.RiskClaim_Id
			INNER JOIN  (
						SELECT Id, CAST([SummaryMessagesXml] AS XML) SummaryMessagesXml
						FROM [MDA].[dbo].[RiskClaimRun]
						) RCRXML ON RCRXML.ID = RCR.ID 
			CROSS APPLY  RCRXML.SummaryMessagesXml.nodes('/ArrayOfML/ML/M') AS T(Col)
			WHERE RCR.TotalScore > 0
			AND TotalScore != ISNULL(PrevTotalScore,0)
			AND RC.RiskBatch_Id = @RiskBatch_ID

			--AddHeaderRecord
			INSERT INTO #MobileBatchResults (ClaimNumber, IncidentDate, NotificationDate, ACEClaimStatus, NewOrUpdated, KeoghsRule)
			VALUES ('Claim Number','Incident Date','Notification Date','ACE Claim Status','New / Updated','Keoghs Rule');

			--GetTheRequiredResultsIntoCTESoWeCanOrderBetter
			WITH CTE_RESULTS AS
			(
			SELECT   DISTINCT RC.[ClientClaimRefNumber] ClaimNumber
					,CONVERT(VARCHAR, INC.[IncidentDate], 103) IncidentDate
					,CONVERT(VARCHAR, I2P.[ClaimNotificationDate], 103) NotificationDate
					,SourceClaimStatus  ACEClaimStatus
					,CASE WHEN RCR.PrevTotalScore IS NULL THEN 'New' ELSE 'Updated' END [New/Updated]
					,KR.KeoghsRule [KeoghsRule]
			FROM MDA.dbo.RiskClaimRun RCR
			INNER JOIN MDA.[dbo].[RiskClaim] RC ON RC.Id = RCR.RiskClaim_Id
			INNER JOIN MDA.[dbo].[Incident] INC ON INC.Id = RC.Incident_Id
			INNER JOIN MDA.[dbo].[Incident2Person] I2P ON I2P.Incident_Id = INC.Id
			LEFT JOIN	(
						SELECT   DISTINCT RiskClaim_Id
								,REPLACE(STUFF((SELECT ' '+KeoghsRule FROM #KeoghsRule KR1 WHERE KR1.RiskClaim_Id = KR0.RiskClaim_Id FOR XML PATH('')),1,1,'')  + '~','; ~','') KeoghsRule
						FROM #KeoghsRule KR0
						) KR ON KR.RiskClaim_Id = RCR.RiskClaim_Id
			WHERE TotalScore > 0
			AND TotalScore != ISNULL(PrevTotalScore,0)
			AND RC.RiskBatch_Id = @RiskBatch_ID
			)

			INSERT INTO #MobileBatchResults (ClaimNumber, IncidentDate, NotificationDate, ACEClaimStatus, NewOrUpdated, KeoghsRule)
			SELECT   ClaimNumber [Claim Number]
					,CONVERT(VARCHAR, [IncidentDate], 103) [Incident Date]
					,CONVERT(VARCHAR, NotificationDate, 103) [Notification Date]
					,ACEClaimStatus [ACE Claim Status]
					,[New/Updated] [New / Updated]
					,KeoghsRule [Keoghs Rule]
			FROM CTE_RESULTS RES
			ORDER BY CAST(NotificationDate AS DATE) DESC
					,CASE [New/Updated] WHEN 'New' THEN 0 ELSE 1 END ASC
					,CASE [ACEClaimStatus] WHEN 'Resolved - Received' THEN 0 WHEN 'Resolved – Shipped' THEN 1 ELSE 2 END ASC
					,[ACEClaimStatus]
					,ClaimNumber

			--GettheResultsIntoATableWhichWeWillUseTooutputFrom
			SELECT @SQL = 'INSERT INTO ' + @ResultTableName + ' SELECT ClaimNumber, IncidentDate, NotificationDate, ACEClaimStatus, NewOrUpdated, KeoghsRule FROM #MobileBatchResults ORDER BY RowID'
			EXECUTE SP_EXECUTESQL  @SQL

			--BCPFileOutToTheFileDirectory
			SELECT @BCP = 'BCP "SELECT ClaimNumber, IncidentDate, NotificationDate, ACEClaimStatus, NewOrUpdated, KeoghsRule FROM [MDAControl].' + @ResultTableName + ' ORDER BY RowID" queryout ' + @ResultFileName +' -T -c -t"\",\"" -r"\"\n\""'
			EXEC master..XP_CMDSHELL @BCP, no_output

			--CheckToSeeIfWeCanSendTheEmail
			IF NULLIF(@To,'') IS NOT NULL AND NULLIF(@EmailSubject,'') IS NOT NULL
			BEGIN
				--SendTheAttachementViaEmail
				SELECT @EmailBody = CASE WHEN COUNT(1) = 1 THEN @EmailBody_WithoutData ELSE @EmailBody_WithData END FROM #MobileBatchResults
				EXECUTE msdb.dbo.sp_send_dbmail
						@profile_name = @MailProfile,
						@recipients = @To,
						@copy_recipients = @CC,
						@blind_copy_recipients = @BCC,
						@subject = @EmailSubject,
						@body = @EmailBody,
						@file_attachments = @ResultFileName,
						@exclude_query_output = 1

				--AddTheParemeterDetailsToTheControlRunTable
				UPDATE [dbo].[DBControlRun]
				SET [Details] = [Details] + ';EmailBody:[' + ISNULL(@EmailBody,'') + '];UserName[' + SYSTEM_USER + ']'
				WHERE [RunID] = @ControlRunID
			END
		END

		--InsertRecordIntoRunHistory
		INSERT INTO [Rpt].[ReportNameRun] (ReportCreationOutputDeliveryConfiguration_Id, DBControlRun_Id, BatchID)
		VALUES (@ReportCreationOutputDeliveryConfiguration_Id, @ControlRunID, @RiskBatch_ID)

		--UpdateResendFlagForBatch
		UPDATE [Rpt].[ReportNameRun]
		SET [Resend] = 0
		WHERE [BatchID] = @RiskBatch_ID
		AND [Resend] = 1

		--CloseControlRunRecord
		UPDATE [dbo].[DBControlRun]
		SET [RunEndTime] = GETDATE()
		WHERE [RunID] = @ControlRunID

		--PrepareForTheNextLoop
		SELECT	 @SQL = 'IF OBJECT_ID('''+ @ResultTableName +''') IS NOT NULL DROP TABLE ' + @ResultTableName
				,@Counter += 1

		EXECUTE SP_EXECUTESQL  @SQL
		DELETE #KeoghsRule
		DELETE #MobileBatchResults
	END

	--------------------------------------
	-- Tidy
	--------------------------------------
	IF OBJECT_ID('tempdb..#KeoghsRule') IS NOT NULL
		DROP TABLE #KeoghsRule 

	IF OBJECT_ID('tempdb..#MobileBatchResults') IS NOT NULL
		DROP TABLE #MobileBatchResults
END TRY
BEGIN CATCH
	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+ISNULL(@ErrorProcedure,'')+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'

	UPDATE [dbo].[DBControlRun]
	SET  [RunEndTime] = GETDATE()
		,[Error] = @ErrorSeverity
		,[ErrorDescription] = @ErrorMessage
	WHERE [RunID] = @ControlRunID

	SELECT	 @SQL = 'IF OBJECT_ID('''+ @ResultTableName +''') IS NOT NULL DROP TABLE ' + @ResultTableName
	EXECUTE SP_EXECUTESQL  @SQL

	IF OBJECT_ID('tempdb..#KeoghsRule') IS NOT NULL
		DROP TABLE #KeoghsRule 

	IF OBJECT_ID('tempdb..#MobileBatchResults') IS NOT NULL
		DROP TABLE #MobileBatchResults

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH