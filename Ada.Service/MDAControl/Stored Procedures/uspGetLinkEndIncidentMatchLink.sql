﻿
CREATE PROCEDURE [IBaseM].[uspGetLinkEndIncidentMatchLink]
AS
SET NOCOUNT ON

BEGIN TRY

	IF OBJECT_Id('tempdb..#_LinkEnd') IS NOT NULL
		DROP TABLE #_LinkEnd

	CREATE TABLE #_LinkEnd
	(
		Unique_ID VARCHAR(50),
		Old_Unique_ID  VARCHAR(50),
		Table_Id INT,
		TableCode  VARCHAR(3)
	)

	--OLD LINKS
	--MIA IBaseM.vNew_Id_Old_Id_Incident_
	--VEH IBaseM.vNew_Id_Old_Id_Vehicle_
	--LOC IBaseM.vNew_Id_Old_Id_Address_
	--TEL IBaseM.vNew_Id_Old_Id_Telephone_
	--CLA IBaseM.vNew_Id_Old_Id_Incident_
	--PER IBaseM.vNew_Id_Old_Id_Person_
	--ORG IBaseM.vNew_Id_Old_Id_Organisation_

	INSERT INTO #_LinkEnd
	SELECT * FROM IBaseM.vNew_Id_Old_Id_Incident_
	UNION ALL
	SELECT * FROM IBaseM. vNew_Id_Old_Id_Vehicle_
	UNION ALL
	SELECT * FROM IBaseM.vNew_Id_Old_Id_Address_
	UNION ALL
	SELECT * FROM IBaseM.vNew_Id_Old_Id_Telephone_
	UNION ALL
	SELECT * FROM IBaseM.vNew_Id_Old_Id_Person_
	UNION ALL
	SELECT * FROM IBaseM.vNew_Id_Old_Id_Organisation_

	SELECT 
		B.Unique_ID,
		C.Unique_ID AS Entity_ID1,
		A.Confidence,
		A.Direction,
		D.Unique_ID AS Entity_ID2,
		C.Table_Id EntityType_ID1,	
		D.Table_Id EntityType_ID2,
		B.Table_Id LinkType_ID,
		Record_Status,	
		Record_Type,	
		SCC

	FROM 
		v_LinkEnd A
	INNER JOIN 
		 IBaseM.vNew_Id_Old_Id_Incident_Match_Link B
		 ON A.Link_ID = B.Old_Unique_ID
	INNER JOIN
		 #_LinkEnd C
		 ON A.Entity_ID1 = C.Old_Unique_ID 
	INNER JOIN
		 #_LinkEnd D
		 ON A.Entity_ID2 = D.Old_Unique_ID 
	WHERE 
		(LEFT(C.Old_Unique_ID, 3) != 'MIA' AND LEFT(D.Old_Unique_ID, 3) = 'VEH')
		OR (LEFT(C.Old_Unique_ID, 3) != 'VEH' AND LEFT(D.Old_Unique_ID, 3) = 'MIA')


	IF OBJECT_Id('tempdb..#_LinkEnd') IS NOT NULL
		DROP TABLE #_LinkEnd
END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

