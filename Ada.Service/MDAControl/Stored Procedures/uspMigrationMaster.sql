﻿

CREATE PROCEDURE [IBaseM].[uspMigrationMaster]
/**************************************************************************************************/
-- ObjectName:			MDAControl.IBaseM.uspMigrationMaster
--
-- Description:			Perform the migration of Ibase5 Schema into an empty IBase8 database.
--
-- Steps:				1)ConfigureLogging,Create/DeclareRequiredObjects
--							1.1)ConfigureLogging
--							1.2)Create/DeclareRequiredObjects	
--						2)BuildRequiedIndexes
--						3)DeleteAnyRecordThatHasARecordStatusOf254-Deleted
--						4)Populate_OffcomUKAreaCodeData
--						5)Populate_Elements
--						6)Populate_Links
--						7)Populate_IncidentLink
--						8)Populate_LinkEnd
--						9)Populate_IncidentLinkUpdates
--						10)Populate_MigrationFix01
--						10.1)UpdateRecordStatus
--						11)DropIndexes
--						12)UpdateLogging/Tidy
--
-- Dependencies:		IBaseM.uspMigrationStep01		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
--		2016-01-21			Paul Allen		Amended to allow for Vanilla POC Creation
/**************************************************************************************************/
(
 @Logging				BIT = 0	--DefaultIsOff
,@Debug					BIT = 0 --DefaultIsOff
,@IsVanillaPOC			BIT = 0 --DefaultIsOff
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@OutputRunID		INT
				,@OutputProcessID	INT
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@RunNameID			INT = CASE WHEN @IsVanillaPOC = 0 THEN 1 ELSE 140 END
	
		EXECUTE dbo.uspCRUDDBControlRun
					 @RunNameID		= @RunNameID
					,@OutputRunID	= @OutputRunID OUTPUT
		
		SELECT @Details = 'User=['+SYSTEM_USER+']'				
		EXECUTE dbo.uspCRUDDBControlProcess
					 @RunID				= @OutputRunID
					,@ProcessNameID		= 6 --MigrateIBase5DataIntoIBase8Schema
					,@Details			= @Details
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@OutputProcessID	= @OutputProcessID OUTPUT
						
		EXECUTE dbo.uspCRUDDBControlTask
					 @ProcessID			= @OutputProcessID
					,@TaskNameID		= 7 --MigrationMasterWrapper
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	
	--1.2)Create/DeclareRequiredObjects
	DECLARE  @CurrentIBase5DB  VARCHAR(50) = (SELECT TOP 1 name FROM sys.databases WHERE patindex('IBase5_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name DESC)
			,@CurrentIBase8DB  VARCHAR(50) = CASE WHEN @IsVanillaPOC = 0 THEN (SELECT TOP 1 name FROM sys.databases WHERE patindex('IBase8_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name DESC) ELSE 'MDA_IBase' END
			,@SQL NVARCHAR(MAX)
			
	--------------------------------
	--2)BuildRequiedIndexes
	--------------------------------		
	SET @SQL = '
		USE  '+ @CurrentIBase8DB +'
		
		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''[dbo].[Address_to_Address_Link]'') AND name = N''IX_Address_to_Address_Link'')
		CREATE NONCLUSTERED INDEX [IX_Address_to_Address_Link]
		ON [dbo].[Address_to_Address_Link] ([Address_Link_Type],[Record_Status])
		INCLUDE ([Unique_ID],[Source_Reference_411765487])
		'
	EXEC SP_EXECUTESQL @SQL

	SET @SQL = '
		USE '+ @CurrentIBase5DB +'
		
		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''[dbo].[_LinkEnd]'') AND name = N''IX__LinkEnd_Record_Status_20130823'')
		CREATE NONCLUSTERED INDEX [IX__LinkEnd_Record_Status_20130823]
		ON [dbo].[_LinkEnd] ([Record_Status])
		INCLUDE ([Link_ID],[Entity_ID1],[Entity_ID2])
		'
	EXEC SP_EXECUTESQL @SQL

	---------------------------------------------------------------
	--3)DeleteAnyRecordThatHasARecordStatusOf254-Deleted
	---------------------------------------------------------------
	DELETE FROM IBaseM5Cur._LinkEnd WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.ClaimFile_ WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Person_ WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Vehicle_ WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Location_ WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Telephone_ WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Organisation_ WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Intelligence_ WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Account_ WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Nominal_Link WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Location_Link WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.MIAFTR_Match WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.MIAFTR_Match_Link WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Vehicle_Link WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Finance_Link WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Intelligence_Link WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Claim_ WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Insurance_Policy WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Settlement_ WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Insured_Claim_Link WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Settlement__000 WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Keoghs_Ref WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Reference_Link WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Report_ WHERE Record_Status = 254
	DELETE FROM IBaseM5Cur.Fraud_Ring WHERE Record_Status = 254

	---------------------------------------------------------------
	--4)Populate_OffcomUKAreaCodeData
	---------------------------------------------------------------
	IF @Debug = 1
		PRINT 'OffcomUKAreaCodeData'
	EXECUTE IBaseM.uspMigrationStep01 @Logging = 1, @ParentTaskID = @OutputTaskID
	
	---------------------------------------------------------------
	--5)Populate_Elements
	---------------------------------------------------------------	
	IF @Debug = 1
		PRINT 'Elements'
	EXECUTE IBaseM.uspMigrationStep02 @Logging = 1, @ParentTaskID = @OutputTaskID

	---------------------------------------------------------------
	--6)Populate_Links
	---------------------------------------------------------------	
	IF @Debug = 1
		PRINT 'Links'
	EXECUTE IBaseM.uspMigrationStep03 @Logging = 1, @ParentTaskID = @OutputTaskID

	---------------------------------------------------------------
	--7)Populate_IncidentLink
	---------------------------------------------------------------	
	IF @Debug = 1
		PRINT 'IncidentLink'
	EXECUTE IBaseM.uspMigrationStep04 @Logging = 1, @ParentTaskID = @OutputTaskID

	---------------------------------------------------------------
	--8)Populate_LinkEnd
	---------------------------------------------------------------	
	IF @Debug = 1
		PRINT 'LinkEnd'
	EXECUTE IBaseM.uspMigrationStep05 @Logging = 1, @ParentTaskID = @OutputTaskID

	---------------------------------------------------------------
	--9)Populate_IncidentLinkUpdates
	---------------------------------------------------------------	
	IF @Debug = 1
		PRINT 'IncidentLinkUpdates'
	EXECUTE IBaseM.uspMigrationStep06 @Logging = 1, @ParentTaskID = @OutputTaskID

	---------------------------------------------------------------
	--10)Populate_MigrationFix01
	---------------------------------------------------------------	
	IF @Debug = 1
		PRINT 'MigrationFix01'
	EXECUTE IBaseM.uspMigrationFix01 @Logging = 1, @ParentTaskID = @OutputTaskID

	--CreateAdditionalLookupValues
	IF @IsVanillaPOC = 1
	BEGIN
		IF @Debug = 1
		PRINT 'CreatingAdditionalLookupValuesForVanillaPOCCreation'

		--InsurerClients
		INSERT INTO [MDA].[dbo].[InsurersClients] ([ClientName])
		SELECT [Clients_]
		FROM [MDA_IBase].[dbo].[Keoghs_Case] KC
		WHERE NOT EXISTS (SELECT TOP 1 1 FROM [MDA].[dbo].[InsurersClients] IC WHERE IC.ClientName = KC.Clients_)
		AND [Clients_] IS NOT NULL
		AND ISNUMERIC([Clients_]) = 0
		GROUP BY [Clients_]
		ORDER BY [Clients_]
	END
	
	---------------------------------------------------------------
	--10.1)UpdateRecordStatus
	---------------------------------------------------------------	
	EXECUTE IBaseM.uspResetRecordStatus

	---------------------------------------------------------------
	--11)DropIndexes
	---------------------------------------------------------------	
	SET @SQL = '
		USE  '+ @CurrentIBase5DB +'
		
		IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''[dbo].[Address_to_Address_Link]'') AND name = N''IX_Address_to_Address_Link'')
		DROP INDEX [IX_Address_to_Address_Link] ON [dbo].[Address_to_Address_Link] WITH ( ONLINE = OFF )'
	EXEC SP_EXECUTESQL @SQL

	------------------------------------------------------------
	--12)UpdateLogging/Tidy
	-----------------------------------------------------------
	IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action		= 'U'
						,@TaskID		= @OutputTaskID
						,@ProcessID		= @OutputProcessID
						,@TaskEndTime	= @Now
						,@OutputTaskID	= @OutputTaskID OUTPUT
						
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@OutputProcessID	= @OutputProcessID OUTPUT
		END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@ProcessID			= @OutputProcessID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
			
			SELECT @ErrorMessage +=' [TaskID='+CAST(ISNULL(@OutputTaskID,-1) AS VARCHAR)+']'		
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputProcessID	= @OutputProcessID OUTPUT
						
			EXECUTE dbo.uspCRUDDBControlRun
						 @Action			= 'U'
						,@RunID				= @OutputRunID
						,@RunEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputRunID		= @OutputRunID OUTPUT
		END
END CATCH

