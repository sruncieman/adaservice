﻿CREATE PROCEDURE [Sync].[uspPAFValidateAndCleanse] @LinkServerName VARCHAR(200) = NULL, @SynchronisationDetailID INT = NULL
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

-----------------------------------------
--1)CreateAndDeclareObjects
-----------------------------------------
DECLARE  @Id					INT
		,@SubBuilding			VARCHAR(50)
		,@Building				VARCHAR(50)
		,@BuildingNumber		VARCHAR(50)
		,@Street				VARCHAR(500)
		,@Locality				VARCHAR(50)
		,@Town					VARCHAR(50)
		,@PostCode				VARCHAR(20)
		,@SQL					NVARCHAR(MAX)
		,@UpdateStatementCount	INT
		,@Counter				INT		=1
		,@AddressToProcesscount INT
		,@OSubBuildingName		VARCHAR(50)
		,@OBuildingName			VARCHAR(50)
		,@OBuildingNumber		VARCHAR(50)
		,@OStreet				VARCHAR(500)
		,@OLocality				VARCHAR(50)
		,@OTown					VARCHAR(50)
		,@OPostCode				VARCHAR(20)
		,@OPafUPRN				VARCHAR(17)

SELECT @LinkServerName = QUOTENAME(@LinkServerName)

IF OBJECT_ID('tempdb..#RawAddress') IS NOT NULL
	DROP TABLE #RawAddress
CREATE TABLE #RawAddress (AddressID INT NOT NULL, RawAddress VARCHAR(1000))

IF OBJECT_ID('tempdb..#AddressProcess') IS NOT NULL
	DROP TABLE #AddressProcess
CREATE TABLE #AddressProcess (ID INT IDENTITY(1,1),AddressID INT, SubBuildingName VARCHAR(50), BuildingNumber VARCHAR(50), Building VARCHAR(50), Street VARCHAR(500), Locality	VARCHAR(50), Town VARCHAR(50), PostCode VARCHAR(50))
	
IF OBJECT_ID('tempdb..#AddressValidation') IS NOT NULL
	DROP TABLE #AddressValidation
CREATE TABLE #AddressValidation (AddressID INT, SubBuildingName	VARCHAR(50), BuildingNumber	VARCHAR(50), Building VARCHAR(50), Street VARCHAR(500), Locality	VARCHAR(50), Town VARCHAR(50), PostCode VARCHAR(50), PafValidation INT, PafUPRN	VARCHAR(20))

IF OBJECT_ID('tempdb..#RiskWordFilter') IS NOT NULL
	DROP TABLE #RiskWordFilter	

IF OBJECT_ID('tempdb..#UpdateStatement') IS NOT NULL
	DROP TABLE #UpdateStatement
CREATE TABLE #UpdateStatement (ID INT IDENTITY(1,1), SQLStatement VARCHAR(MAX))
		
DECLARE @AddressValidation TABLE (SubBuildingName VARCHAR(50), BuildingNumber VARCHAR(50), Building VARCHAR(50), Street VARCHAR(500), Locality VARCHAR(50), Town VARCHAR(50), PostCode VARCHAR(50), PafValidation	INT, Flag INT, PafUPRN VARCHAR(20))
	
-----------------------------------------
--2)PopulateRawAddressInAll2AddressTables
-----------------------------------------
UPDATE A
SET [PafValidation] = 0
FROM [MDA].[dbo].[Address] A
WHERE [ADARecordStatus] >= 10 --ThisWillPickUpAnythingFromReplication

--GetTheIDAndRawAddress
DECLARE @AddressCleanCount INT

INSERT INTO #RawAddress (AddressID, RawAddress)
SELECT   Id AddressID
		,Sync.FnRawAddress (SubBuilding,BuildingNumber,Building,Street,Locality,Town,County,REPLACE(PostCode,' ','')) RawAddress
FROM [MDA].[dbo].Address
WHERE PafValidation = 0 --AddressNeverBeenValidated
ORDER BY Id
SELECT @AddressCleanCount = @@ROWCOUNT

IF @SynchronisationDetailID IS NOT NULL
BEGIN
	UPDATE [dbo].[DBControlSynchronisationProcess]
	SET  [Details] = CAST(@AddressCleanCount AS VARCHAR) + ' Address(es) Identified to Clean...'
	WHERE [SynchronisationDetailID] = @SynchronisationDetailID 
END

--Incident2AddressUpdate
UPDATE I2A
SET RawAddress = RA.RawAddress
FROM #RawAddress RA
INNER JOIN [MDA].[dbo].Incident2Address I2A ON I2A.Address_Id = RA.AddressID
WHERE I2A.RawAddress IS NULL

--Organisation2Address
UPDATE O2A
SET RawAddress = RA.RawAddress
FROM #RawAddress RA
INNER JOIN [MDA].[dbo].Organisation2Address O2A ON O2A.Address_Id = RA.AddressID
WHERE O2A.RawAddress IS NULL

--Person2Address
UPDATE P2A
SET RawAddress = RA.RawAddress
FROM #RawAddress RA
INNER JOIN [MDA].[dbo].Person2Address P2A ON P2A.Address_Id = RA.AddressID
WHERE P2A.RawAddress IS NULL

--Vehicle2Address
UPDATE V2A
SET RawAddress = RA.RawAddress
FROM #RawAddress RA
INNER JOIN [MDA].[dbo].Vehicle2Address V2A ON V2A.Address_Id = RA.AddressID
WHERE V2A.RawAddress IS NULL

-----------------------------------------
--3)BeforWeStartTheCleanseWeNeedToRemoveAnyAnomalies
-----------------------------------------
--NoLongerNeedToDoThis!!

-----------------------------------------
--4)PAFValidateTheAddress
-----------------------------------------
--WantToDoThisInLoopsOf1000.
WHILE (SELECT COUNT(1) FROM [MDA].[dbo].Address WHERE PafValidation = 0) > 0
BEGIN
	INSERT INTO #AddressProcess (AddressID, SubBuildingName, Building, BuildingNumber, Street, Locality, Town, PostCode)
	SELECT TOP 1000 ID, NULLIF(UPPER(SubBuilding),''), NULLIF(UPPER(Building),''), NULLIF(UPPER(BuildingNumber),''), NULLIF(UPPER(Street),''), NULLIF(UPPER(Locality),''), NULLIF(UPPER(Town),''), NULLIF(REPLACE(UPPER(PostCode),' ',''),'')
	FROM [MDA].[dbo].Address 
	WHERE PafValidation = 0

	SELECT @AddressToProcesscount = @@ROWCOUNT
	SELECT @Counter = 1

	WHILE @AddressToProcesscount >= @Counter
	BEGIN
		SELECT @ID = AddressID, @SubBuilding = SubBuildingName ,@Building = Building, @BuildingNumber = BuildingNumber, @Street = Street, @Locality = Locality, @Town = Town , @PostCode = PostCode 
		FROM #AddressProcess
		WHERE ID = @Counter

		SELECT @SQL = 
			'EXECUTE ' + CASE WHEN @LinkServerName IS NULL THEN '' ELSE @LinkServerName + '.' END + 'PAF.dbo.usp_GetAddress 
					 @SubBuilding			= @SubBuilding
					,@Building				= @Building
					,@BuildingNumber		= @BuildingNumber
					,@Street				= @Street
					,@Locality				= @Locality 
					,@Town					= @Town
					,@PostCode				= @PostCode
					,@Debug					= 0
					,@OutputParameters		= 1 
					,@OSubBuildingName		= @OSubBuildingName		OUTPUT
					,@OBuildingName			= @OBuildingName		OUTPUT	
					,@OBuildingNumber		= @OBuildingNumber		OUTPUT
					,@OStreet				= @OStreet				OUTPUT
					,@OLocality				= @OLocality			OUTPUT
					,@OTown					= @OTown				OUTPUT
					,@OPostCode				= @OPostCode			OUTPUT
					,@OPafUPRN				= @OPafUPRN				OUTPUT'

		EXECUTE SP_EXECUTESQL	 @SQL
								,N'  @SubBuilding			VARCHAR(50)
									,@Building				VARCHAR(50)
									,@BuildingNumber		VARCHAR(50)
									,@Street				VARCHAR(500)
									,@Locality				VARCHAR(50)
									,@Town					VARCHAR(50)
									,@PostCode				VARCHAR(20)
									,@OSubBuildingName		VARCHAR(50) OUTPUT
									,@OBuildingName			VARCHAR(50) OUTPUT
									,@OBuildingNumber		VARCHAR(50) OUTPUT
									,@OStreet				VARCHAR(500)OUTPUT
									,@OLocality				VARCHAR(50) OUTPUT
									,@OTown					VARCHAR(50) OUTPUT
									,@OPostCode				VARCHAR(20) OUTPUT
									,@OPafUPRN				VARCHAR(17) OUTPUT'
								,@SubBuilding, @Building, @BuildingNumber, @Street, @Locality, @Town, @PostCode, @OSubBuildingName OUTPUT, @OBuildingName OUTPUT, @OBuildingNumber OUTPUT, @OStreet OUTPUT, @OLocality OUTPUT, @OTown OUTPUT, @OPostCode OUTPUT, @OPafUPRN OUTPUT
	
		INSERT INTO @AddressValidation (SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, Flag, PafUPRN)
		SELECT @OSubBuildingName, @OBuildingNumber, @OBuildingName, @OStreet, @OLocality, @OTown, @OPostCode, 1, 1, @OPafUPRN
		WHERE LEN(ISNULL(@OSubBuildingName,'') + ISNULL(@OBuildingName,'') + ISNULL(@OBuildingNumber,'') + ISNULL(@OStreet,'') + ISNULL(@OLocality,'') + ISNULL(@OTown,'') + ISNULL(@OPostCode,'') + ISNULL(@OPafUPRN,'')) >0
		
		IF @@ROWCOUNT > 0
		BEGIN
			INSERT INTO #AddressValidation (AddressID, SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, PafUPRN)
			SELECT @ID, SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, ISNULL(PafUPRN,'')
			FROM @AddressValidation
		END
		ELSE
		BEGIN
			INSERT INTO #AddressValidation (AddressID, SubBuildingName, BuildingNumber, Building, Street, Locality, Town, PostCode, PafValidation, PafUPRN)
			SELECT @ID, @SubBuilding, @BuildingNumber ,@Building, @Street, @Locality, @Town, @PostCode, 1, NULL
		END
	
		DELETE FROM @AddressValidation
		SELECT @OSubBuildingName = NULL, @OBuildingName = NULL, @OBuildingNumber = NULL, @OStreet = NULL, @OLocality = NULL, @OTown = NULL, @OPostCode = NULL, @OPafUPRN  = NULL
		SELECT @Counter +=1
	END

	UPDATE AD
	SET		 SubBuilding	= UPPER(NULLIF(AV.SubBuildingName,''))
			,Building		= UPPER(NULLIF(AV.Building,''))
			,BuildingNumber	= UPPER(NULLIF(AV.BuildingNumber,''))
			,Street			= UPPER(NULLIF(AV.Street,''))
			,Locality		= UPPER(NULLIF(AV.Locality,''))
			,Town			= UPPER(NULLIF(AV.Town,''))
			,County			= CASE WHEN AV.PafUPRN IS NULL THEN UPPER(NULLIF(AD.County,'')) ELSE NULL END
			,PostCode		= UPPER(NULLIF(REPLACE(AV.PostCode,' ',''),''))
			,PafValidation	= 1 --InMDAPAFValidateCalledForEverything
			,PafUPRN		= AV.PafUPRN
	FROM #AddressValidation AV
	INNER JOIN [MDA].[dbo].Address AD ON AD.ID = AV.AddressID

	TRUNCATE TABLE #AddressProcess
END

-----------------------------------------
--5)Tidy
-----------------------------------------
IF OBJECT_ID('tempdb..#RawAddress') IS NOT NULL
	DROP TABLE #RawAddress
	
IF OBJECT_ID('tempdb..#AddressValidation') IS NOT NULL
	DROP TABLE #AddressValidation

IF OBJECT_ID('tempdb..#RiskWordFilter') IS NOT NULL
	DROP TABLE #RiskWordFilter	

IF OBJECT_ID('tempdb..#UpdateStatement') IS NOT NULL
	DROP TABLE #UpdateStatement

IF OBJECT_ID('tempdb..#AddressProcess') IS NOT NULL
	DROP TABLE #AddressProcess