﻿


CREATE PROCEDURE   [Sync].[uspSynchroniseEntityTelephone]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseEntityTelephone]
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)ActionTheChangeInMDA_IBase
--						3.3)DoDeletes
--						3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)	
--						4.1)ActionTheChangeInMDA
--						4.2)DoDeletes
--						4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					5)UpdateLoggingAndTidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-05-07			Simon Hundleby	Created
--		2014-10-28			Paul Allen		Added AltEntity in Merge; TelephoneTypeUpdated if value changes to unknown
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY

	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT			

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1), ChangeDatabase VARCHAR(10) ,MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)
					
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 120 --SynchroniseTelephoneEntity
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, MDA_ID, DMLAction)
	SELECT 'MDA', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA)].[dbo].Telephone, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA_IBase)].[dbo].Telephone_, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	--2.2)AddSomeIndexesToMakeTheProcessQuicker
	SET @Step ='2.2'
	 
	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT(MDA_IBase_Unique_ID ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3'

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IA.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [$(MDA_IBase)].[dbo].[Telephone_] IA ON IA.Unique_ID = CT.MDA_IBase_Unique_ID
	AND IA.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MA.IBaseId
	FROM #CT CT
	INNER JOIN [$(MDA)].[dbo].[Telephone] MA ON MA.ID = CT.MDA_ID

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4'

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5'

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1


	-----------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	------------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1'

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM [$(MDA_IBase)].[dbo]._Telephone__NextID
		UPDATE [$(MDA_IBase)].[dbo]._Telephone__NextID SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	'TEL' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY MDA_ID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--3.2)ActionTheChangeInMDA_IBase
		SET @Step ='3.2'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [$(MDA_IBase)].[dbo].[Telephone_] [Target]
		USING	(
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID,tt.PhoneText, MADD.*
				FROM #CT CT
				INNER JOIN [$(MDA)].[dbo].[Telephone] MADD ON MADD.ID = CT.MDA_ID
				INNER JOIN [$(MDA)].[dbo].[TelephoneType] TT ON MADD.TelephoneType_ID =TT.ID 
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				) Source 
				ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET	 Country_Code					=	source.CountryCode
			,Create_date					=	source.CreatedDate
			,Create_User					=	source.CreatedBy
			,Key_Attractor_412284410		=	source.KeyAttractor
			,Telephone_ID					=	source.TelephoneID
			,Source_411765484				=	source.[Source]
			,Source_Description_411765489	=	source.SourceDescription
			,Source_Reference_411765487		=	source.SourceReference
			,STD_Code						=	source.STDCode
			,Telephone_Number				=	source.TelephoneNumber
			,Telephone_Type					=	CASE WHEN source.TelephoneType_id = 0 AND [Target].Telephone_Type NOT IN (SELECT PhoneText FROM [$(MDA)].[dbo].[TelephoneType]) THEN [Target].Telephone_Type ELSE source.PhoneText END
			,Last_upd_date					=	source.ModifiedDate
			,Last_Upd_User					=	source.ModifiedBy
			,Record_Status					=	Source.[RecordStatus]
			,[AltEntity]					=	CASE WHEN source.TelephoneType_id = 0 THEN 'Phone Unknown' ELSE source.PhoneText END
		WHEN NOT MATCHED THEN INSERT 
		(
			[Unique_ID],
			[MDA_Incident_ID_412284502],
			[Country_Code],
			[Create_date],
			[Create_User],
			[Do_Not_Disseminate_412284494],
			[Key_Attractor_412284410],
			[Telephone_ID],
			[Source_411765484],
			[Source_Description_411765489],
			[Source_Reference_411765487],
			[STD_Code],
			[Telephone_Number],
			[Telephone_Type],
			[Last_upd_date],
			[Last_Upd_User],
			Record_Status,
			[AltEntity]
		)
		VALUES
		(
			source.MDA_IBase_Unique_ID,
			[sync].[fn_ResolveMDAID](source.Id,'mda.dbo.Telephone'),
			source.[CountryCode],
			source.[CreatedDate],
			source.[CreatedBy],
			0,
			source.[KeyAttractor],
			source.[TelephoneID],
			source.[Source],
			source.[SourceDescription],
			source.[SourceReference],
			source.[STDCode],
			source.[TelephoneNumber],
			CASE WHEN source.TelephoneType_ID =0 THEN NULL ELSE source.[PhoneText]END,
			source.[ModifiedDate],
			source.[ModifiedBy],
			Source.[RecordStatus],
			CASE WHEN source.TelephoneType_id = 0 THEN 'Phone Unknown' ELSE source.PhoneText END
		)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Country_Code,'') + '}|I:{' + ISNULL(Inserted.Country_Code,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.STD_Code,'') + '}|I:{' + ISNULL(Inserted.STD_Code,'') + '}'
				,'D:{' + ISNULL(Deleted.Telephone_ID,'') + '}|I:{' + ISNULL(Inserted.Telephone_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Telephone_Number,'') + '}|I:{' + ISNULL(Inserted.Telephone_Number,'') + '}'
				,'D:{' + ISNULL(Deleted.Telephone_Type,'') + '}|I:{' + ISNULL(Inserted.Telephone_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIbase_Telephone_]
           ([TaskID]
           ,[DMLAction]
           ,[Unique_ID]
           ,[AltEntity]
           ,[Country_Code]
           ,[Create_Date]
           ,[Create_User]
           ,[Do_Not_Disseminate_412284494]
           ,[IconColour]
           ,[Key_Attractor_412284410]
           ,[Last_Upd_Date]
           ,[Last_Upd_User]
           ,[MDA_Incident_ID_412284502]
           ,[Record_Status]
           ,[SCC]
           ,[Source_411765484]
           ,[Source_Description_411765489]
           ,[Source_Reference_411765487]
           ,[Status_Binding]
           ,[STD_Code]
           ,[Telephone_ID]
           ,[Telephone_Number]
           ,[Telephone_Type]
           ,[x5x5x5_Grading_412284402]
           ,[Risk_Claim_ID_414244883]
		   );
				
		--3.3)DoDeletes
		SET @Step ='3.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		DELETE [$(MDA_IBase)].[dbo].[Telephone_]
		OUTPUT	 @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Country_Code,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.STD_Code,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Telephone_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Telephone_Number,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Telephone_Type,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'
		INTO [SyncDML].[MDAIbase_Telephone_]
           ([TaskID]
           ,[DMLAction]
           ,[Unique_ID]
           ,[AltEntity]
           ,[Country_Code]
           ,[Create_Date]
           ,[Create_User]
           ,[Do_Not_Disseminate_412284494]
           ,[IconColour]
           ,[Key_Attractor_412284410]
           ,[Last_Upd_Date]
           ,[Last_Upd_User]
           ,[MDA_Incident_ID_412284502]
           ,[Record_Status]
           ,[SCC]
           ,[Source_411765484]
           ,[Source_Description_411765489]
           ,[Source_Reference_411765487]
           ,[Status_Binding]
           ,[STD_Code]
           ,[Telephone_ID]
           ,[Telephone_Number]
           ,[Telephone_Type]
           ,[x5x5x5_Grading_412284402]
           ,[Risk_Claim_ID_414244883]
		   )
		FROM [$(MDA_IBase)].[dbo].[Telephone_] MADD
		INNER JOIN #CT CT ON [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Telephone') = MADD.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';


		--3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='3.4';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MADD
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.TelephoneId,'') + '}|I:{' + ISNULL(Inserted.TelephoneId,'') + '}'
				,'D:{' + ISNULL(Deleted.TelephoneNumber,'') + '}|I:{' + ISNULL(Inserted.TelephoneNumber,'') + '}'
				,'D:{' + ISNULL(Deleted.CountryCode,'') + '}|I:{' + ISNULL(Inserted.CountryCode,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.STDCode,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.STDCode,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.TelephoneType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.TelephoneType_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Telephone]
           ([TaskID]
           ,[DMLAction]
           ,[Id]
           ,[TelephoneId]
           ,[TelephoneNumber]
           ,[CountryCode]
           ,[STDCode]
           ,[TelephoneType_Id]
           ,[KeyAttractor]
           ,[Source]
           ,[SourceReference]
           ,[SourceDescription]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[IBaseId]
           ,[RecordStatus]
           ,[ADARecordStatus]
		   )
		FROM [$(MDA)].[dbo].[Telephone] MADD
		INNER JOIN #CT CT ON CT.MDA_ID = MADD.Id
 		WHERE MADD.IBaseId IS NULL
		OR MADD.IBaseId != CT.MDA_IBase_Unique_ID
			



	END

	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		SET @Step ='4.1'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [$(MDA)].[dbo].[Telephone] [Target]
		USING  (
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID,TT.ID as TelephoneType_ID, MTEL.*
				FROM #CT CT
				INNER JOIN [$(MDA_IBase)].[dbo].[Telephone_] MTEL ON MTEL.Unique_ID = CT.MDA_IBase_Unique_ID
				LEFT JOIN [$(MDA)].[dbo].[TelephoneType] TT ON MTEL.Telephone_Type = TT.PhoneText
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET
				CountryCode			=source.[Country_Code]
				,CreatedDate		=source.[Create_date]
				,CreatedBy			=source.[Create_User]
				,IBaseId			=source.[Unique_ID]
				,KeyAttractor		=source.[Key_Attractor_412284410]
				,TelephoneID		=source.[Telephone_ID]
				,[Source]			=source.[Source_411765484]
				,SourceDescription	=source.[Source_Description_411765489]
				,SourceReference	=source.[Source_Reference_411765487]
				,STDCode			=source.[STD_Code]
				,TelephoneNumber	=source.[Telephone_Number]
				,TelephoneType_id	=ISNULL(source.[TelephoneType_ID],0)
				,ModifiedDate		=source.[Last_upd_date]
				,ModifiedBy			=source.[Last_Upd_User]
				,RecordStatus		=Source.[Record_Status]
			   ,ADARecordStatus		=CASE WHEN Source.Record_Status = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT 
		(
				[CountryCode],
				[CreatedDate],
				[CreatedBy],
				[KeyAttractor],
				[TelephoneID],
				[Source],
				[SourceDescription],
				[SourceReference],
				[STDCode],
				[TelephoneNumber],
				[TelephoneType_id],
				[ModifiedDate],
				[ModifiedBy],
				[IbaseID],
				[RecordStatus],
				[ADARecordStatus]
		)
		VALUES
		(
				source.[Country_Code],
				source.[Create_date],
				source.[Create_User],
				source.[Key_Attractor_412284410],
				source.[Telephone_ID],
				source.[Source_411765484],
				source.[Source_Description_411765489],
				source.[Source_Reference_411765487],
				source.[STD_Code],
				source.[Telephone_Number],
				ISNULL(source.[TelephoneType_ID],0),
				source.[Last_upd_date],
				source.[Last_Upd_User],
				source.[Unique_ID],
				source.[Record_Status],
				10
		)
		OUTPUT
			@OutputtaskID
			,$Action
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
			,'D:{' + ISNULL(Deleted.TelephoneId,'') + '}|I:{' + ISNULL(Inserted.TelephoneId,'') + '}'
			,'D:{' + ISNULL(Deleted.TelephoneNumber,'') + '}|I:{' + ISNULL(Inserted.TelephoneNumber,'') + '}'
			,'D:{' + ISNULL(Deleted.CountryCode,'') + '}|I:{' + ISNULL(Inserted.CountryCode,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.STDCode,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.STDCode,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.TelephoneType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.TelephoneType_Id,'')) + '}'
			,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
			,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
			,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
			,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
			,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
			,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
			,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Telephone]
           ([TaskID]
           ,[DMLAction]
           ,[Id]
           ,[TelephoneId]
           ,[TelephoneNumber]
           ,[CountryCode]
           ,[STDCode]
           ,[TelephoneType_Id]
           ,[KeyAttractor]
           ,[Source]
           ,[SourceReference]
           ,[SourceDescription]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[IBaseId]
           ,[RecordStatus]
           ,[ADARecordStatus]
		   );

		
		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [$(MDA)].[dbo].[Telephone]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.TelephoneId,'') + '}|I:{' + ISNULL(Inserted.TelephoneId,'') + '}'
				,'D:{' + ISNULL(Deleted.TelephoneNumber,'') + '}|I:{' + ISNULL(Inserted.TelephoneNumber,'') + '}'
				,'D:{' + ISNULL(Deleted.CountryCode,'') + '}|I:{' + ISNULL(Inserted.CountryCode,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.STDCode,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.STDCode,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.TelephoneType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.TelephoneType_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Telephone]
           ([TaskID]
           ,[DMLAction]
           ,[Id]
           ,[TelephoneId]
           ,[TelephoneNumber]
           ,[CountryCode]
           ,[STDCode]
           ,[TelephoneType_Id]
           ,[KeyAttractor]
           ,[Source]
           ,[SourceReference]
           ,[SourceDescription]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[IBaseId]
           ,[RecordStatus]
           ,[ADARecordStatus]
		   )
		FROM [$(MDA)].[dbo].[Telephone] IADD
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IADD.IBaseId
		WHERE CT.DMLAction = 'D';

		
		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE IADD
		SET MDA_Incident_ID_412284502 = [Sync].[fn_ResolveMDAID] (MADD.ID,'MDA.dbo.Telephone')
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Country_Code,'') + '}|I:{' + ISNULL(Inserted.Country_Code,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.STD_Code,'') + '}|I:{' + ISNULL(Inserted.STD_Code,'') + '}'
				,'D:{' + ISNULL(Deleted.Telephone_ID,'') + '}|I:{' + ISNULL(Inserted.Telephone_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Telephone_Number,'') + '}|I:{' + ISNULL(Inserted.Telephone_Number,'') + '}'
				,'D:{' + ISNULL(Deleted.Telephone_Type,'') + '}|I:{' + ISNULL(Inserted.Telephone_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIbase_Telephone_]
           ([TaskID]
           ,[DMLAction]
           ,[Unique_ID]
           ,[AltEntity]
           ,[Country_Code]
           ,[Create_Date]
           ,[Create_User]
           ,[Do_Not_Disseminate_412284494]
           ,[IconColour]
           ,[Key_Attractor_412284410]
           ,[Last_Upd_Date]
           ,[Last_Upd_User]
           ,[MDA_Incident_ID_412284502]
           ,[Record_Status]
           ,[SCC]
           ,[Source_411765484]
           ,[Source_Description_411765489]
           ,[Source_Reference_411765487]
           ,[Status_Binding]
           ,[STD_Code]
           ,[Telephone_ID]
           ,[Telephone_Number]
           ,[Telephone_Type]
           ,[x5x5x5_Grading_412284402]
           ,[Risk_Claim_ID_414244883]
		   )
		FROM [$(MDA_IBase)].[dbo].[Telephone_] IADD
		INNER JOIN #CT CT 
			ON CT.MDA_IBase_Unique_ID = IADD.Unique_ID
		INNER JOIN [$(MDA)].[dbo].[Telephone] MADD 
			ON MADD.IBaseId = IADD.Unique_ID
 		WHERE IADD.MDA_Incident_ID_412284502 IS NULL
			OR [Sync].[fn_ResolveMDAID] (MADD.ID,'MDA.dbo.Telephone') != IADD.MDA_Incident_ID_412284502
	END


	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Telephone] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Telephone] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Telephone] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Telephone_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Telephone_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Telephone_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')		
	
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

END TRY
BEGIN CATCH


	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +@Step +']'

	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)

END CATCH



