﻿

CREATE PROCEDURE [Sync].[uspRunSync]
(
 @Success INT OUTPUT
  ,@IsVanillaPOC BIT = 0 --DefaultIsOff
)
AS
SET NOCOUNT ON
BEGIN TRY
	/*
	 0 - Complete, 
	-1 - Busy, try again later, 
	-2 - Sync Error needs manual intervention, 
	-3 - Migration Error needs manual intervention
	*/
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	DECLARE  @Now				DATETIME 
			,@Details			VARCHAR(1000)
			,@OutputRunID		INT
			,@DatabaseID		INT = DB_ID()
			,@ObjectID			INT = ISNULL(@@PROCID,-1)
			,@RunNameID			INT = CASE WHEN @IsVanillaPOC = 0 THEN 139 ELSE 140 END

	SELECT @Details = 'User=['+SYSTEM_USER+']'		
	EXECUTE dbo.uspCRUDDBControlRun
				 @RunNameID		= 139 --Synchronisation /*76-- Synch_MDA_IBaseToMDA*/
				,@Details		= @Details
				,@OutputRunID	= @OutputRunID OUTPUT

	DECLARE @SQLStatements TABLE  (ID INT IDENTITY(1,1), SQLCommand NVARCHAR(MAX))
	DECLARE  @StatementsToProcess	INT
			,@Counter				INT = 1
			,@SQLStatement			NVARCHAR(MAX)

	------------------------------------------------------------
	--2)Body
	------------------------------------------------------------
	/*MigrationSuccessfulRunSync*/
	IF EXISTS (SELECT TOP 1 1 FROM [DBControlRun] WHERE RunID = (SELECT MAX(RunID) from [DBControlRun] WHERE RunNameID =1) AND RunEndTime IS NOT NULL AND ISNULL(Error,0)=0)
	OR (SELECT COUNT(1) FROM [DBControlRun] WHERE RunNameID =1) = 0
	OR @IsVanillaPOC = 1
	BEGIN
		--AddToDBControlSynchronisation
		DECLARE  @SynchronisationID INT
				,@SynchronisationDetailID INT
		INSERT INTO [dbo].[DBControlSynchronisation] ([StartDateTime]) VALUES (GETDATE())
		SELECT @SynchronisationID = @@IDENTITY

		------------------------------
		--PerformDifferentialBackups
		------------------------------
		IF @IsVanillaPOC = 0
		BEGIN
			--MDAControl
			DECLARE @SaveSetName VARCHAR(100) = 'MDAControl-PreSync-ControlRunID:' + CAST(@OutputRunID AS VARCHAR(6))
		
			INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'DiffBackup:MDAControl')
			SELECT @SynchronisationDetailID = @@IDENTITY
		
		EXECUTE [dbo].[uspBackupDatabase] @BackupDatabaseName = 'MDAControl', @BackupType = 2, @SaveSetName = @SaveSetName

			UPDATE [dbo].[DBControlSynchronisationProcess]
			SET  [EndDateTime] = GETDATE()
				,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
			WHERE [SynchronisationDetailID] = @SynchronisationDetailID

			--MDA_IBase
		SELECT @SaveSetName = 'MDA_IBase-PreSync-ControlRunID:' + CAST(@OutputRunID AS VARCHAR(6))

			INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'DiffBackup:MDA_IBase')
			SELECT @SynchronisationDetailID = @@IDENTITY

		EXECUTE [dbo].[uspBackupDatabase] @BackupDatabaseName = 'MDA_IBase', @BackupType = 2, @SaveSetName = @SaveSetName

			UPDATE [dbo].[DBControlSynchronisationProcess]
			SET  [EndDateTime] = GETDATE()
				,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
			WHERE [SynchronisationDetailID] = @SynchronisationDetailID

			--MDA_IBase_Log
		SELECT @SaveSetName = 'MDA_IBase_Log-PreSync-ControlRunID:' + CAST(@OutputRunID AS VARCHAR(6))

			INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'DiffBackup:MDA_IBase_Log')
			SELECT @SynchronisationDetailID = @@IDENTITY

		EXECUTE [dbo].[uspBackupDatabase] @BackupDatabaseName = 'MDA_IBase_Log', @BackupType = 2, @SaveSetName = @SaveSetName

			UPDATE [dbo].[DBControlSynchronisationProcess]
			SET  [EndDateTime] = GETDATE()
				,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
			WHERE [SynchronisationDetailID] = @SynchronisationDetailID
		END

		------------------------------
		--PerformTheFirstSync
		------------------------------
		INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'Sync1:Sync.uspSynchroniseMaster')
		SELECT @SynchronisationDetailID = @@IDENTITY

		EXECUTE [Sync].[uspSynchroniseMaster] 

		UPDATE [dbo].[DBControlSynchronisationProcess]
		SET  [EndDateTime] = GETDATE()
			,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
		WHERE [SynchronisationDetailID] = @SynchronisationDetailID

		------------------------------
		--PerformUpdatesOnTheDataSync
		------------------------------
		--DataAnomaliesOnlyNeedUpdatingIfAIBase5MigrationHasntOccurredOrWasTheLastAction
		IF 1 IN (SELECT TOP 3 RunNameID FROM [dbo].[DBControlRun] WHERE Error IS NULL ORDER BY RunID DESC)
		OR @IsVanillaPOC = 1
		BEGIN
			--UpdateAnyDataAnormalies
			INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName],[Details]) VALUES (@SynchronisationID,'DataAnomaliesCleansed:Sync.uspUpdateDataAnomalies','Should only run after a successful nightly replication or Vanilla POC Set up')
			SELECT @SynchronisationDetailID = @@IDENTITY

			EXECUTE [Sync].[uspUpdateDataAnomalies]

			UPDATE [dbo].[DBControlSynchronisationProcess]
			SET  [EndDateTime] = GETDATE()
				,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
			WHERE [SynchronisationDetailID] = @SynchronisationDetailID

			--DeleteAnyEmptyEntitysAndLinks
			INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName],[Details]) VALUES (@SynchronisationID,'EmptyEntitysAndLinksCleansed:Sync.uspDeleteLinksAndEntitysWithEmptyKeyDataFields','Should only run after a successful nightly replication or Vanilla POC Set up')
			SELECT @SynchronisationDetailID = @@IDENTITY

			EXECUTE [Sync].[uspDeleteLinksAndEntitysWithEmptyKeyDataFields]	

			UPDATE [dbo].[DBControlSynchronisationProcess]
			SET  [EndDateTime] = GETDATE()
				,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
			WHERE [SynchronisationDetailID] = @SynchronisationDetailID

			--PAFValidate
			IF (SELECT @@SERVERNAME) NOT LIKE 'INTELTST-POC%' OR @IsVanillaPOC = 1
		BEGIN
			INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'PAFLocal:Sync.uspPAFValidateAndCleanse')
			SELECT @SynchronisationDetailID = @@IDENTITY

			--WhatToGetTheCountAndAddTotheDeatil:ACoupleOfChallenges...
			--OutputParameter
			EXECUTE [Sync].[uspPAFValidateAndCleanse] @SynchronisationDetailID = @SynchronisationDetailID

			UPDATE [dbo].[DBControlSynchronisationProcess]
			SET  [EndDateTime] = GETDATE()
				,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
			WHERE [SynchronisationDetailID] = @SynchronisationDetailID
		END
		ELSE
		BEGIN
				INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'PAFRemote:Sync.uspPAFValidateAndCleanse')
				SELECT @SynchronisationDetailID = @@IDENTITY

				EXECUTE [Sync].[uspPAFValidateAndCleanse]	 @LinkServerName = '192.168.32.151'--UKBOLIBASEVM1
															,@SynchronisationDetailID = @SynchronisationDetailID

				UPDATE [dbo].[DBControlSynchronisationProcess]
				SET  [EndDateTime] = GETDATE()
					,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
				WHERE [SynchronisationDetailID] = @SynchronisationDetailID
			END
		END

		-----------------------------
		--RebuildHeadsOfClaim
		-----------------------------
		INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'HOCRebuild:Sync.uspPopulateHeadOfClaim')
		SELECT @SynchronisationDetailID = @@IDENTITY

		EXECUTE [Sync].[uspPopulateHeadOfClaim]

		UPDATE [dbo].[DBControlSynchronisationProcess]
		SET  [EndDateTime] = GETDATE()
			,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
		WHERE [SynchronisationDetailID] = @SynchronisationDetailID

		-----------------------------
		--ReAlignADARecordStatus
		-----------------------------
		INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'Fix:ReAlignADARecordStatus')
		SELECT @SynchronisationDetailID = @@IDENTITY

		INSERT INTO @SQLStatements (SQLCommand)
		SELECT 'UPDATE MDA.dbo.' + T.name + ' SET ADARecordStatus = 0 WHERE RecordStatus = 0 AND ADARecordStatus != 0 AND ADARecordStatus <10; /*PRINT ''>>'' + CAST(@@ROWCOUNT AS VARCHAR) + '' ' + T.name + ' Records Updated...''*/'
		FROM MDA.Sys.tables T
		INNER JOIN MDA.SYS.columns C ON C.object_id = T.object_id
		WHERE C.name IN ('RecordStatus','ADARecordStatus','IBaseID')
		GROUP BY T.name
		HAVING COUNT(1) = 3
		ORDER BY T.name
		SELECT @StatementsToProcess = @@ROWCOUNT

		WHILE @StatementsToProcess >= @Counter
		BEGIN
			SELECT @SQLStatement = SQLCommand FROM @SQLStatements WHERE ID = @Counter
			EXECUTE SP_EXECUTESQL @SQLStatement

			SELECT @Counter += 1
		END

		UPDATE [dbo].[DBControlSynchronisationProcess]
		SET  [EndDateTime] = GETDATE()
			,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
		WHERE [SynchronisationDetailID] = @SynchronisationDetailID

		-----------------------------
		--Add/UpdateTheHireDatesFoeVehicleLinks
		-----------------------------
		INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'Fix:UpdateHireDatesForVehicles')
		SELECT @SynchronisationDetailID = @@IDENTITY

		UPDATE V2P
		SET  HireStartDate = V2O.HireStartDate
			,HireEndDate = V2O.HireEndDate
		FROM MDA.dbo.Vehicle2Person AS V2P
		INNER JOIN MDA.dbo.Vehicle2Organisation AS V2O ON V2P.Vehicle_Id = V2O.Vehicle_Id and V2P.RiskClaim_Id = V2O.RiskClaim_Id
		WHERE V2P.VehicleLinkType_Id = 4 
		AND V2O.VehicleLinkType_Id = 3 
		AND (ISNULL(V2P.HireStartDate,@Now) != ISNULL(V2O.HireStartDate,@Now) OR ISNULL(V2P.HireEndDate,@Now)  !=  ISNULL(V2O.HireEndDate,@Now))
		AND V2O.VehicleLinkId IS NULL
		AND V2P.ADARecordStatus = 0
		AND V2O.ADARecordStatus = 0

		UPDATE [dbo].[DBControlSynchronisationProcess]
		SET  [EndDateTime] = GETDATE()
			,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
		WHERE [SynchronisationDetailID] = @SynchronisationDetailID

		------------------------------
		--PerformTheSecondSync - ToGetTheUpdatesBack
		------------------------------
		INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName],[Details]) VALUES (@SynchronisationID,'Sync2:Sync.uspSynchroniseMaster','Called a 2nd time to implement any changes this scrpt has made')
		SELECT @SynchronisationDetailID = @@IDENTITY

		EXECUTE [Sync].[uspSynchroniseMaster] 

		UPDATE [dbo].[DBControlSynchronisationProcess]
		SET  [EndDateTime] = GETDATE()
			,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
		WHERE [SynchronisationDetailID] = @SynchronisationDetailID

		------------------------------
		--DeleteRecordsThatShouldntHaveBeenCreatedInIbase
		------------------------------
		INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'Fix:DeleteAnyRecordsThatWeShould''tHaveCreated')
		SELECT @SynchronisationDetailID = @@IDENTITY

		DECLARE @Sys_Change_Context VARBINARY(128) = CONVERT(VARBINARY(128),'NoVehicleInvolvedUpdate');

		IF OBJECT_ID('tempdb..#VehicleIncident') IS NOT NULL
			DROP TABLE #VehicleIncident 
		CREATE TABLE #VehicleIncident (VehicleIncidentLink VARCHAR(50) NOT NULL, Vehicle VARCHAR(50) NOT NULL, Incident VARCHAR(50) NOT NULL)

		IF OBJECT_ID('tempdb..#PersonVehicle') IS NOT NULL
			DROP TABLE #PersonVehicle
		CREATE TABLE #PersonVehicle (PersonVehicleLink VARCHAR(50) NOT NULL)

		--GetTheDataForVehicleIncident
		INSERT INTO #VehicleIncident (VehicleIncidentLink, Vehicle, Incident)
		SELECT LE.Link_ID VehicleIncidentLink, LE.Entity_ID1 Vehicle, LE.Entity_ID2 Incident
		FROM [MDA_IBase].[dbo].[Vehicle_Incident_Link] VIL
		INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] LE ON LE.Link_ID = VIL.Unique_ID AND LE.EntityType_ID1 = 2321 /*Vehicle*/ AND LE.EntityType_ID2 = 1688 /*Incident*/
		WHERE [Link_Type] = 'No Vehicle Involved'

		--GetTheDataForVehicleToPerson
		INSERT INTO #PersonVehicle (PersonVehicleLink)
		SELECT LE_V.Link_ID PersonVehicle
		FROM #VehicleIncident VI
		INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] LE ON LE.Entity_ID1 = VI.Incident AND LE.EntityType_ID2 = 1969 /*Person*/
		INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] LE_V ON LE_V.Entity_ID1 = LE.Entity_ID2 AND LE_V.Entity_ID2 = VI.Vehicle

		BEGIN TRANSACTION
			;WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
			 DELETE VL
			 FROM [MDA_IBase].[dbo].[Vehicle_Link] VL
			 INNER JOIN #PersonVehicle PV ON PV.PersonVehicleLink = VL.Unique_ID

			;WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
			 DELETE VIL
			 FROM [MDA_IBase].[dbo].[Vehicle_Incident_Link] VIL
			 INNER JOIN #VehicleIncident VI ON VI.VehicleIncidentLink = VIL.Unique_ID

			 ;WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
			 DELETE LE
			 FROM [MDA_IBase].[dbo].[_LinkEnd] LE
			 INNER JOIN (
						 SELECT VehicleIncidentLink Link_ID
						 FROM #VehicleIncident
						 UNION 
						 SELECT PersonVehicleLink 
						 FROM #PersonVehicle
						) Links ON Links.Link_ID = LE.Link_ID

			;WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
			 DELETE VEH
			 FROM [MDA_IBase].[dbo].[Vehicle_] VEH
			 INNER JOIN #VehicleIncident VI ON VI.Vehicle = VEH.Unique_ID
		COMMIT TRANSACTION

		--TIDY
		IF OBJECT_ID('tempdb..#VehicleIncident') IS NOT NULL
			DROP TABLE #VehicleIncident 

		IF OBJECT_ID('tempdb..#PersonVehicle') IS NOT NULL
			DROP TABLE #PersonVehicle

		UPDATE [dbo].[DBControlSynchronisationProcess]
		SET  [EndDateTime] = GETDATE()
			,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
		WHERE [SynchronisationDetailID] = @SynchronisationDetailID

		------------------------------
		--SetRecordsNotToBeDeduplicated
		------------------------------
		INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'PreventMassUpdatesFromBeingDeduped:sync.uspExcludeImportUsersFromDeduplication')
		SELECT @SynchronisationDetailID = @@IDENTITY

		EXECUTE [Sync].[uspExcludeImportUsersFromDeduplication]

		UPDATE [dbo].[DBControlSynchronisationProcess]
		SET  [EndDateTime] = GETDATE()
			,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
		WHERE [SynchronisationDetailID] = @SynchronisationDetailID

		------------------------------
		--PerformDifferentialBackups
		------------------------------
		IF @IsVanillaPOC = 0
		BEGIN
			--MDAControl
			SELECT @SaveSetName = 'MDAControl-PreSync-ControlRunID:' + CAST(@OutputRunID AS VARCHAR(6))
		
			INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'TranBackup:MDAControl')
			SELECT @SynchronisationDetailID = @@IDENTITY
		
		EXECUTE [dbo].[uspBackupDatabase] @BackupDatabaseName = 'MDAControl', @BackupType = 3, @SaveSetName = @SaveSetName

			UPDATE [dbo].[DBControlSynchronisationProcess]
			SET  [EndDateTime] = GETDATE()
				,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
			WHERE [SynchronisationDetailID] = @SynchronisationDetailID

			--MDA_IBase
		SELECT @SaveSetName = 'MDA_IBase-PreSync-ControlRunID:' + CAST(@OutputRunID AS VARCHAR(6))

			INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'TranBackup:MDA_IBase')
			SELECT @SynchronisationDetailID = @@IDENTITY

		EXECUTE [dbo].[uspBackupDatabase] @BackupDatabaseName = 'MDA_IBase', @BackupType = 3, @SaveSetName = @SaveSetName

			UPDATE [dbo].[DBControlSynchronisationProcess]
			SET  [EndDateTime] = GETDATE()
				,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
			WHERE [SynchronisationDetailID] = @SynchronisationDetailID

			--MDA_IBase_Log
		SELECT @SaveSetName = 'MDA_IBase_Log-PreSync-ControlRunID:' + CAST(@OutputRunID AS VARCHAR(6))

			INSERT INTO [dbo].[DBControlSynchronisationProcess] ([SynchronisationID],[ProcessName]) VALUES (@SynchronisationID,'TranBackup:MDA_IBase_Log')
			SELECT @SynchronisationDetailID = @@IDENTITY

		EXECUTE [dbo].[uspBackupDatabase] @BackupDatabaseName = 'MDA_IBase_Log', @BackupType = 3, @SaveSetName = @SaveSetName

			UPDATE [dbo].[DBControlSynchronisationProcess]
			SET  [EndDateTime] = GETDATE()
				,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
			WHERE [SynchronisationDetailID] = @SynchronisationDetailID
		END 

		------------------------------
		--CloseDBControlSynchronisation
		------------------------------
		UPDATE [dbo].[DBControlSynchronisation]
		SET  [EndDateTime] = GETDATE()
			,[DurationHHMMSS] = CONVERT(VARCHAR,DATEADD(MS,DATEDIFF(MS,[StartDateTime],GETDATE()),0),114)
		WHERE [SynchronisationID] = @SynchronisationID

		--UpdateOutputAndLogging
		SELECT	 @Success =0
				,@Now =  GETDATE()

		EXECUTE [dbo].[uspCRUDDBControlRun] 
					 @Action ='U'
					,@RunID =  @OutputRunID
					,@RunEndTime = @Now
					,@OutputRunID = @OutputRunID OUTPUT
		RETURN --ExitProcedure
	END

	/*MigrationStillRunning*/
	IF EXISTS (SELECT TOP 1 1 FROM [DBControlRun] WHERE RunID = (SELECT MAX(RunID) from [DBControlRun] WHERE RunNameID =1) AND RunEndTime IS NULL AND ISNULL(Error,0) =0)
	BEGIN
		--UpdateOutputAndLogging
		SELECT	 @Success =-1
				,@Now =  GETDATE()

		EXECUTE [dbo].[uspCRUDDBControlRun] 
					 @Action			='U'
					,@RunID				= @OutputRunID
					,@RunEndTime		= @Now
					,@Error				= @Success
					,@ErrorDescription	= 'Busy, try again later'
					,@OutputRunID		= @OutputRunID OUTPUT
		RETURN --ExitProcedure
	END	
			
	/*MigrationErrored*/
	IF EXISTS (SELECT TOP 1 1 FROM [DBControlRun] WHERE RunID = (SELECT MAX(RunID) from [DBControlRun] WHERE RunNameID =1) AND RunEndTime IS NOT NULL AND ISNULL(Error,0) !=0)
	BEGIN
		--UpdateOutputAndLogging
		SELECT	 @Success =-3
				,@Now =  GETDATE()

		EXECUTE [dbo].[uspCRUDDBControlRun] 
					 @Action			='U'
					,@RunID				= @OutputRunID
					,@RunEndTime		= @Now
					,@Error				= @Success
					,@ErrorDescription	= 'Migration Error needs manual intervention'
					,@OutputRunID		= @OutputRunID OUTPUT
		RETURN --ExitProcedure
	END	

END TRY
BEGIN CATCH
	--RaiseTheerror
	DECLARE  @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()		

	--UpdateOutputAndLogging
		SELECT	 @Success =-2
				,@Now =  GETDATE()

	EXECUTE  [dbo].[uspCRUDDBControlRun] 
					 @Action			='U'
					,@RunID				= @OutputRunID
					,@RunEndTime		= @Now
					,@Error				= @Success
					,@ErrorDescription  = @ErrorMessage
					,@OutputRunID		= @OutputRunID OUTPUT

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'

END CATCH