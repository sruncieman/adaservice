﻿CREATE PROCEDURE [Rep].[uspReplicateEntityWebsite]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateEntityWebsite]	
--
-- Description:		Replicate Entity Website	
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLogging/Tidy							
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
--		2016-01-20			Paul Allen		Changed logic of updating Link End table for Conflict records
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Website_')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 26 --ReplicateWebsiteEntity
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	/*ItsIsARequirementToNeverDeleteDueToDedupe
	UPDATE [$(MDA_IBase)].[dbo].Website_
	SET Record_Status = 254
	OUTPUT	 @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Website_ID,'') + '}|I:{' + ISNULL(Inserted.Website_ID,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Website_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Website_],'')) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
			,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
			,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
	INTO [SyncDML].[MDAIBase_Website_]([TaskID],[DMLAction],[Unique_ID],[Website_ID],[Website_],[Create_User],[Create_Date],[Do_Not_Disseminate_412284494],[Key_Attractor_412284410],[Last_Upd_Date],[Last_Upd_User],[MDA_Incident_ID_412284502],[Source_411765484],[Source_Description_411765489],[Source_Reference_411765487])
	FROM Rep.EntityRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo].Website_ a ON ENT.IBase8EntityID = A.Unique_ID
	WHERE RecordAction ='D'
	*/

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	--ThereIsARequirementNotToUpdateTheWebSiteNameIfThisChangesButToInsertSetOrginalrecordtoDeleteAndRepointLinks
	DECLARE @Conflicts TABLE (RowID INT IDENTITY(1,1), IBase5 VARCHAR(20) , IBase8ID VARCHAR(20), OrgData VARCHAR(255), NewData VARCHAR(255), NewIB8ID VARCHAR(20))
	INSERT INTO @Conflicts (Ibase5, IBase8ID, OrgData, NewData)
	SELECT	 ME.IBase5EntityID
			,ME.IBase8EntityID
			,LN.Website_
			,WEB.Website_
	FROM Rep.EntityRecord ME
	INNER JOIN IBaseM8Cur.Website_ WEB ON ME.IBase8EntityID_Staging = WEB.Unique_ID 
	INNER JOIN [$(MDA_IBase)].[dbo].Website_ LN ON ME.IBase8EntityID = LN.Unique_ID
	WHERE ISNULL(LN.Website_,'') <> ISNULL(WEB.Website_,'')

	--PerfomMergeForNonConflictRecords
	MERGE [$(MDA_IBase)].[dbo].Website_ [Target]
	USING	(
			SELECT	 DISTINCT c.IBase8EntityID
					,C.IBase5EntityID
					,A.*
			FROM IBaseM8Cur.Website_ A
			INNER JOIN Rep.EntityRecord C ON A.Unique_ID = C.IBase8EntityID_Staging
			LEFT JOIN @Conflicts CF ON C.IBase5EntityID = CF.IBase5
			WHERE CF.IBase5 IS NULL
			) AS Source ON [Target].Unique_ID = Source.IBase8EntityID
	WHEN MATCHED THEN UPDATE
	SET  Website_						= Source.Website_
		,Key_Attractor_412284410		= Source.Key_Attractor_412284410
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User
		,Record_Status					= Source.Record_Status
		,AltEntity						= Source.AltEntity
		,Do_Not_Disseminate_412284494	= Source.Do_Not_Disseminate_412284494
		,IconColour						= Source.IconColour
		,SCC							= Source.SCC
		,Status_Binding					= Source.Status_Binding
		,Website_ID						= Source.Website_ID
		,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883		= Source.Risk_Claim_ID_414244883	
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,AltEntity
		,Create_Date
		,Create_User
		,Do_Not_Disseminate_412284494
		,IconColour
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,Record_Status
		,SCC
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding
		,Website_
		,Website_ID
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883
		)
	VALUES
		(
		 Source.IBase8EntityID
		,Source.AltEntity
		,Source.Create_Date
		,Source.Create_User
		,Source.Do_Not_Disseminate_412284494
		,Source.IconColour
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Record_Status
		,Source.SCC
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.Website_
		,Source.Website_ID
		,Source.x5x5x5_Grading_412284402
		,Source.Risk_Claim_ID_414244883
		)
	OUTPUT	 @OutputTaskID
			,$Action
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Website_ID,'') + '}|I:{' + ISNULL(Inserted.Website_ID,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Website_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Website_],'')) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
			,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
			,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
	INTO [SyncDML].[MDAIBase_Website_]([TaskID],[DMLAction],[Unique_ID],[Website_ID],[Website_],[Create_User],[Create_Date],[Do_Not_Disseminate_412284494],[Key_Attractor_412284410],[Last_Upd_Date],[Last_Upd_User],[MDA_Incident_ID_412284502],[Source_411765484],[Source_Description_411765489],[Source_Reference_411765487]);

	
	--HandleTheConflictsIfTheWebsiteNameHasChanged
	DECLARE @NextID INT , @RowCount INT
	SELECT @RowCount = COUNT(1) FROM @Conflicts
	
	IF @RowCount > 0
	BEGIN
		SELECT @NextID = NextID FROM [$(MDA_IBase)].[dbo]._Website__NextID
		UPDATE [$(MDA_IBase)].[dbo]._Website__NextID SET NextID = @NextID + @Rowcount 
	
		UPDATE U
		SET NewIB8ID = New_Unique_ID
		FROM	(
				SELECT  'WEB' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY RowID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM @Conflicts
				WHERE IBase5 IS NOT NULL) A
		INNER JOIN @Conflicts U ON A.RowID = U.RowID
			
		--InsertTheRecordForConflicts
		INSERT INTO [$(MDA_IBase)].[dbo].Website_
				(
				 Unique_ID
				,AltEntity
				,Create_Date
				,Create_User
				,Do_Not_Disseminate_412284494
				,IconColour
				,Key_Attractor_412284410
				,Last_Upd_Date
				,Last_Upd_User
				,Record_Status
				,SCC
				,Source_411765484
				,Source_Description_411765489
				,Source_Reference_411765487
				,Status_Binding
				,Website_
				,Website_ID
				,x5x5x5_Grading_412284402
				,Risk_Claim_ID_414244883	
				)
		OUTPUT	 @OutputTaskID
				,'INSERT'
				,'D:{}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Website_ID,'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Website_],'')) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
		INTO [SyncDML].[MDAIBase_Website_]([TaskID],[DMLAction],[Unique_ID],[Website_ID],[Website_],[Create_User],[Create_Date],[Do_Not_Disseminate_412284494],[Key_Attractor_412284410],[Last_Upd_Date],[Last_Upd_User],[MDA_Incident_ID_412284502],[Source_411765484],[Source_Description_411765489],[Source_Reference_411765487])
		SELECT	 C.NewIB8ID
				,AltEntity
				,Create_Date
				,Create_User
				,Do_Not_Disseminate_412284494
				,IconColour
				,Key_Attractor_412284410
				,Last_Upd_Date
				,Last_Upd_User
				,Record_Status
				,SCC
				,Source_411765484
				,Source_Description_411765489
				,Source_Reference_411765487
				,Status_Binding
				,C.NewData
				,C.IBase5
				,x5x5x5_Grading_412284402
				,Risk_Claim_ID_414244883	
		FROM @Conflicts c
		INNER JOIN IBaseM8Cur.Website_ NI ON C.IBase5 = NI.Website_ID AND NI.Website_ = C.NewData
		
		--RePointTheLinkEndToTheNewID	
		UPDATE LE
		SET  LE.Entity_ID1 = CASE WHEN LE.Entity_ID1 = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE LE.Entity_ID1 END
			,LE.Entity_ID2 = CASE WHEN LE.Entity_ID2 = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE LE.Entity_ID2 END
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Link_ID],'') + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID1],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Confidence],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Direction],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID2],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID1],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID2],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkType_ID],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Type],'')) + '}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{' + ISNULL(Inserted.[SCC],'') + '}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [$(MDA_IBase)].[dbo]._LinkEnd LE
		INNER JOIN (			
					SELECT LE.LinkID, C.IBase8ID, C.NewIB8ID
					FROM @Conflicts C
					INNER JOIN [Rep].[IBaseLinkEnd] LE on c.IBase8ID = LE.EntityID1 
					) LinkID ON LE.Link_ID = LinkID.LinkID

		--RemoveTheLinkFromRep.LinkEndRecord
		DELETE LER
		FROM [Rep].[LinkEndRecord] LER
		INNER JOIN (			
					SELECT LE.LinkID, C.IBase8ID, C.NewIB8ID
					FROM @Conflicts C
					INNER JOIN [Rep].[IBaseLinkEnd] LE on c.IBase8ID = LE.EntityID1 
					) LinkID ON LER.[IBase8LinkID] = LinkID.LinkID

		--AddedToPreventAdditionalRecordBeingInsertedIntoLinkEnd
		UPDATE RLER
		SET  [EntityID1] = CASE WHEN RLER.[EntityID1] = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE RLER.[EntityID1] END
			,[EntityID2] = CASE WHEN RLER.[EntityID2] = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE RLER.[EntityID2] END
		FROM [Rep].[IBaseLinkEnd] RLER
		INNER JOIN (			
					SELECT LE.Link_ID, C.IBase8ID, C.NewIB8ID
					FROM @conflicts C
					INNER JOIN [$(MDA_IBase)].[dbo]._LinkEnd LE on c.NewIB8ID = LE.Entity_ID1 
					) LinkID ON RLER.[LinkID] = LinkID.Link_ID
	END
				
	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Website_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
		SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Website_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
		SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Website_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')		
		
		SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
		SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
		SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()
		
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
						@Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END	
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH







