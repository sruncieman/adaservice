﻿




CREATE PROCEDURE [Rep].[uspGenerateLookupUniqueID]
/**************************************************************************************************/
-- ObjectName:			
--
-- Description:			
--
-- Steps:				
--
-- Dependencies:		
--
-- Usage:				Called within SP IBaseM.uspMigrationMaster
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-27			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 19 --GenerateLookupUniqueID
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	
	--1.2)Create/DeclareRequiredObjects
	DECLARE  @SQL					NVARCHAR(MAX)
			,@TableCode				VARCHAR(3)
			,@TablePhysicalName		VARCHAR(50)
			,@NextID				INT
			,@SQLParam				NVARCHAR(255)
			
	------------------------------------------------------------
	--2)MainBody
	------------------------------------------------------------
	-------------------------------------------
	--GenerateIDsForEntitys
	-------------------------------------------
	DECLARE EntityUniqueIDCreation CURSOR FOR
	SELECT ME.TableCode, DT.PhysicalName
	FROM Rep.EntityRecord ME
	INNER JOIN dbo.DataTable DT ON DT.TableCode = ME.TableCode
	WHERE ME.RecordAction = 'I'
	GROUP BY ME.TableCode, DT.PhysicalName

	OPEN EntityUniqueIDCreation
	FETCH NEXT FROM EntityUniqueIDCreation INTO @TableCode, @TablePhysicalName
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @SQLParam	= '@NextID INT OUT'
			SET @SQL		= 'SELECT @NextID = NextID FROM [MDA_IBase].[dbo]._' +@TablePhysicalName +'_NextID'
			EXECUTE SP_EXECUTESQL @SQl, @SQlParam, @NextID = @NextID OUT
			
			UPDATE U
			SET IBase8EntityID = D.IBase8EntityID
			FROM Rep.EntityRecord U
			INNER JOIN	(
						SELECT	 ID
								,TableCode + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY ID) + @NextID - 1) IBase8EntityID
						FROM Rep.EntityRecord A
						WHERE TableCode = @TableCode
						AND RecordAction = 'I'
						) D ON D.ID = U.ID
			
			SELECT @NextID += @@ROWCOUNT
			
			SET @SQLParam	= '@NextID INT'
			SET @SQL		= 'UPDATE [MDA_IBase].[dbo]._' +@TablePhysicalName +'_NextID SET NextID = @NextID'
			EXECUTE SP_EXECUTESQL @SQl, @SQlParam, @NextID = @NextID
			
			FETCH NEXT FROM EntityUniqueIDCreation INTO @TableCode, @TablePhysicalName
		END
	CLOSE EntityUniqueIDCreation
	DEALLOCATE EntityUniqueIDCreation

	-------------------------------------------
	--GenerateIDsForLinks
	-------------------------------------------
	DECLARE LinkUniqueIDCreation CURSOR FOR
	SELECT ML.TableCode, DT.PhysicalName
	FROM Rep.LinkRecord ML
	INNER JOIN dbo.DataTable DT ON DT.TableCode = ML.TableCode
	WHERE ML.RecordAction = 'I'
	GROUP BY ML.TableCode, DT.PhysicalName

	OPEN LinkUniqueIDCreation
	FETCH NEXT FROM LinkUniqueIDCreation INTO @TableCode, @TablePhysicalName
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @SQLParam	= '@NextID INT OUT'
			SET @SQL		= 'SELECT @NextID = NextID FROM [MDA_IBase].[dbo]._' +@TablePhysicalName +'_NextID'
			EXECUTE SP_EXECUTESQL @SQl, @SQlParam, @NextID = @NextID OUT
			
			UPDATE U
			SET IBase8LinkID = D.IBase8LinkID
			FROM Rep.LinkRecord U
			INNER JOIN	(
						SELECT	 ID
								,TableCode + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY ID) + @NextID - 1) IBase8LinkID
						FROM Rep.LinkRecord A
						WHERE TableCode = @TableCode
						AND RecordAction = 'I'
						) D ON D.ID = U.ID
			
			SELECT @NextID += @@ROWCOUNT
			
			SET @SQLParam	= '@NextID INT'
			SET @SQL		= 'UPDATE [MDA_IBase].[dbo]._' +@TablePhysicalName +'_NextID SET NextID = @NextID'
			EXECUTE SP_EXECUTESQL @SQl, @SQlParam, @NextID = @NextID
			
			FETCH NEXT FROM LinkUniqueIDCreation INTO @TableCode, @TablePhysicalName
		END
	CLOSE LinkUniqueIDCreation
	DEALLOCATE LinkUniqueIDCreation

	--ForInstancesWhereTheLinkIDDoesNotExistInLinkEnd	
	UPDATE ML
	SET IBase8LinkID = SL.IBase8LinkID
	FROM Rep.LinkRecord ML
	INNER JOIN [$(MDA_IBase)].[dbo].VW_StagingLinks SL ON SL.IBase5LinkID = ML.IBase5LinkID
	WHERE ML.RecordAction = 'D'
	AND ML.IBase8LinkID IS NULL

	-------------------------------------------
	--UpdateAnyUniqueIDsCreatedForEntitiesInLinkEnd
	-------------------------------------------
	--UpdateTheEntities
	UPDATE	 MLE
	SET		 IBase8EntityID1 = ISNULL(ME.IBase8EntityID,SE.IBase8EntityID)
			,IBase8EntityID2 = ISNULL(ME2.IBase8EntityID,SE2.IBase8EntityID)
	FROM Rep.LinkEndRecord MLE
	LEFT JOIN Rep.EntityRecord ME ON ME.IBase5EntityID = MLE.IBase5EntityID1 AND LEFT(ME.IBase8EntityID_Staging,3) = LEFT(MLE.IBase8EntityID1Staging,3)
	LEFT JOIN Rep.EntityRecord ME2 ON ME2.IBase5EntityID = MLE.IBase5EntityID2 AND LEFT(ME2.IBase8EntityID_Staging,3) = LEFT(MLE.IBase8EntityID2Staging,3)
	LEFT JOIN [$(MDA_IBase)].[dbo].VW_StagingEntities SE ON SE.IBase5EntityID = MLE.IBase5EntityID1 AND LEFT(SE.IBase8EntityID,3) = LEFT(MLE.IBase8EntityID1Staging,3)
	LEFT JOIN [$(MDA_IBase)].[dbo].VW_StagingEntities SE2 ON SE2.IBase5EntityID = MLE.IBase5EntityID2 AND LEFT(SE2.IBase8EntityID,3) = LEFT(MLE.IBase8EntityID2Staging,3)
	WHERE MLE.RecordAction = 'I'


	--UpdateTheLinkID
	UPDATE	 MLE
	SET		 IBase8LinkID = ISNULL(ML.IBase8LinkID,LE.LinkID) 
	FROM Rep.LinkEndRecord MLE
	LEFT JOIN Rep.LinkRecord ML ON ML.IBase5LinkID = MLE.IBase5LinkID AND ML.IBase8LinkID_Staging = MLE.IBase8LinkIDStaging
	LEFT JOIN [Rep].[IBaseLinkEnd] LE ON LE.EntityID1 = MLE.IBase8EntityID1 AND LE.EntityID2 = MLE.IBase8EntityID2
	WHERE MLE.RecordAction = 'I'


	
	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					--,@InsertRowCount= @InsertRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
		IF CURSOR_STATUS('global','EntityUniqueIDCreation') >-2 
		BEGIN
		  CLOSE EntityUniqueIDCreation
		  DEALLOCATE EntityUniqueIDCreation 
		END
		
		IF CURSOR_STATUS('global','LinkUniqueIDCreation') > -2 
		BEGIN
		  CLOSE LinkUniqueIDCreation
		  DEALLOCATE LinkUniqueIDCreation 
		END
		
END CATCH





