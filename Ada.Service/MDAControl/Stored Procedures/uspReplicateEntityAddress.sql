﻿CREATE PROCEDURE [Rep].[uspReplicateEntityAddress]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateEntityAddress]
--
-- Description:		Replicate Entity Address	
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLogging/Tidy					
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
--		2014-09-03			Paul Allen		If Address has changes create new and repoint links.
--		2016-01-20			Paul Allen		Changed logic of updating Link End table for Conflict records
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Address_')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 30 --ReplicateAddressEntity
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	/*ItsIsARequirementToNeverDeleteDueToDedupe
	UPDATE [$(MDA_IBase)].[dbo].Address_
	SET Record_Status = 254
	OUTPUT	 @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Address_ID,'') + '}|I:{' + ISNULL(Inserted.Address_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Address_Type,'') + '}|I:{' + ISNULL(Inserted.Address_Type,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + ISNULL(Deleted.Building_,'') + '}|I:{' + ISNULL(Inserted.Building_,'') + '}'
			,'D:{' + ISNULL(Deleted.Building_Number,'') + '}|I:{' + ISNULL(Inserted.Building_Number,'') + '}'
			,'D:{' + ISNULL(Deleted.County_,'') + '}|I:{' + ISNULL(Inserted.County_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.[Document_Link_Binding],'') + '}|I:{' + ISNULL(Inserted.[Document_Link_Binding],'') + '}'
			,'D:{' + ISNULL(Deleted.[DX_Exchange],'') + '}|I:{' + ISNULL(Inserted.[DX_Exchange],'') + '}'
			,'D:{' + ISNULL(Deleted.[DX_Number],'') + '}|I:{' + ISNULL(Inserted.[DX_Number],'') + '}'
			,'D:{' + ISNULL(Deleted.[Grid_X],'') + '}|I:{' + ISNULL(Inserted.[Grid_X],'') + '}'
			,'D:{' + ISNULL(Deleted.[Grid_Y],'') + '}|I:{' + ISNULL(Inserted.[Grid_Y],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[IconColour],'')) + '}'
			,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
			,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
			,'D:{' + ISNULL(Deleted.[Locality_],'') + '}|I:{' + ISNULL(Inserted.[Locality_],'') + '}'
			,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[PAF_Validation],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[PAF_Validation],'')) + '}'
			,'D:{' + ISNULL(Deleted.[Post_Code],'') + '}|I:{' + ISNULL(Inserted.[Post_Code],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SCC],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Status_Binding],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Street_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Street_],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Sub_Building],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Sub_Building],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Town_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Town_],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[x5x5x5_Grading_412284402],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Risk_Claim_ID_414244883],'')) + '}'
	INTO [SyncDML].[MDAIBase_Address_] (TaskID, DMLAction, Unique_ID, Address_ID, Address_Type, AltEntity, Building_, Building_Number, County_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Document_Link_Binding, DX_Exchange, DX_Number, Grid_X, Grid_Y, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Locality_, MDA_Incident_ID_412284502, PAF_Validation, Post_Code, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Street_, Sub_Building, Town_, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.EntityRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo].Address_ a ON ENT.IBase8EntityID = A.Unique_ID
	WHERE RecordAction ='D'
	*/
	
	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	--ThereIsARequirementNotToUpdateTheNINumberIfThisChangesButToInsertSetOrginalrecordtoDeleteAndRepointLinks
	DECLARE @Conflicts TABLE (RowID INT IDENTITY(1,1), IBase5 VARCHAR(20) , IBase8ID VARCHAR(20), OrgData VARCHAR(255), NewData VARCHAR(255), NewIB8ID VARCHAR(20))
	INSERT INTO @Conflicts (Ibase5, IBase8ID, OrgData, NewData)
	SELECT	 ME.IBase5EntityID
			,ME.IBase8EntityID
			,NULL
			,NULL
	FROM Rep.EntityRecord ME
	INNER JOIN IBaseM8Cur.Address_ AD ON ME.IBase8EntityID_Staging = AD.Unique_ID 
	INNER JOIN IBaseM8Pre.Address_ AD1 ON ME.IBase5EntityID = AD1.[Address_ID]
	WHERE CHECKSUM(AD.[Sub_Building],AD.[Building_],AD.[Building_Number],AD.[Street_],AD.[Locality_],AD.[Town_],AD.[County_],AD.[Post_Code]) != CHECKSUM(AD1.[Sub_Building],AD1.[Building_],AD1.[Building_Number],AD1.[Street_],AD1.[Locality_],AD1.[Town_],AD1.[County_],AD1.[Post_Code]) 

	--PerfomMergeForNonConflictRecords
	MERGE [$(MDA_IBase)].[dbo].Address_ [Target]
	USING	(
			SELECT	 DISTINCT c.IBase8EntityID
					,C.IBase5EntityID
					,A.*
			FROM IBaseM8Cur.Address_ A
			INNER JOIN Rep.EntityRecord C ON A.Unique_ID = C.IBase8EntityID_Staging
			LEFT JOIN @Conflicts CF ON C.IBase5EntityID = CF.IBase5
			WHERE CF.IBase5 IS NULL
			) AS Source ON [Target].Unique_ID = Source.IBase8EntityID
	WHEN MATCHED THEN UPDATE
	SET  Sub_Building					= Source.Sub_Building
		,Building_						= Source.Building_
		,Building_Number				= Source.Building_Number
		,Street_						= Source.Street_
		,Locality_						= Source.locality_
		,Town_							= Source.Town_
		,County_						= Source.County_
		,Post_Code						= Source.Post_Code
		,DX_Number						= Source.DX_Number
		,DX_Exchange					= Source.DX_Exchange
		,Address_Type					= Source.Address_Type
		,Grid_X							= Source.Grid_X
		,Grid_Y							= Source.Grid_Y
		,PAF_Validation					= Source.PAF_Validation
		,Document_Link					= Source.Document_Link
		,Key_Attractor_412284410		= Source.Key_Attractor_412284410
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Record_Status					= Source.Record_Status
		,Address_ID						= Source.Address_ID
		,AltEntity						= Source.AltEntity
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Do_Not_Disseminate_412284494	= Source.Do_Not_Disseminate_412284494
		,Document_Link_Binding			= Source.Document_Link_Binding
		,IconColour						= Source.IconColour
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User
		,SCC							= Source.SCC
		,Status_Binding					= Source.Status_Binding
		,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883		= Source.Risk_Claim_ID_414244883
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,Address_ID
		,Address_Type
		,AltEntity
		,Building_
		,Building_Number
		,County_
		,Create_Date
		,Create_User
		,Do_Not_Disseminate_412284494
		,Document_Link
		,Document_Link_Binding
		,DX_Exchange
		,DX_Number
		,Grid_X
		,Grid_Y
		,IconColour
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,Locality_
		,PAF_Validation
		,Post_Code
		,Record_Status
		,SCC
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding
		,Street_
		,Sub_Building
		,Town_
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883	
		)
	VALUES
		(
		 Source.IBase8EntityID
		,Source.Address_ID
		,Source.Address_Type
		,Source.AltEntity
		,Source.Building_
		,Source.Building_Number
		,Source.County_
		,Source.Create_Date
		,Source.Create_User
		,Source.Do_Not_Disseminate_412284494
		,Source.Document_Link
		,Source.Document_Link_Binding
		,Source.DX_Exchange
		,Source.DX_Number
		,Source.Grid_X
		,Source.Grid_Y
		,Source.IconColour
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Locality_
		,Source.PAF_Validation
		,Source.Post_Code
		,Source.Record_Status
		,Source.SCC
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.Street_
		,Source.Sub_Building
		,Source.Town_
		,Source.x5x5x5_Grading_412284402
		,Source.Risk_Claim_ID_414244883			
		)
	OUTPUT	 @OutputTaskID
			,$Action
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Address_ID,'') + '}|I:{' + ISNULL(Inserted.Address_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Address_Type,'') + '}|I:{' + ISNULL(Inserted.Address_Type,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + ISNULL(Deleted.Building_,'') + '}|I:{' + ISNULL(Inserted.Building_,'') + '}'
			,'D:{' + ISNULL(Deleted.Building_Number,'') + '}|I:{' + ISNULL(Inserted.Building_Number,'') + '}'
			,'D:{' + ISNULL(Deleted.County_,'') + '}|I:{' + ISNULL(Inserted.County_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.[Document_Link_Binding],'') + '}|I:{' + ISNULL(Inserted.[Document_Link_Binding],'') + '}'
			,'D:{' + ISNULL(Deleted.[DX_Exchange],'') + '}|I:{' + ISNULL(Inserted.[DX_Exchange],'') + '}'
			,'D:{' + ISNULL(Deleted.[DX_Number],'') + '}|I:{' + ISNULL(Inserted.[DX_Number],'') + '}'
			,'D:{' + ISNULL(Deleted.[Grid_X],'') + '}|I:{' + ISNULL(Inserted.[Grid_X],'') + '}'
			,'D:{' + ISNULL(Deleted.[Grid_Y],'') + '}|I:{' + ISNULL(Inserted.[Grid_Y],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[IconColour],'')) + '}'
			,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
			,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
			,'D:{' + ISNULL(Deleted.[Locality_],'') + '}|I:{' + ISNULL(Inserted.[Locality_],'') + '}'
			,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[PAF_Validation],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[PAF_Validation],'')) + '}'
			,'D:{' + ISNULL(Deleted.[Post_Code],'') + '}|I:{' + ISNULL(Inserted.[Post_Code],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SCC],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Status_Binding],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Street_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Street_],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Sub_Building],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Sub_Building],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Town_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Town_],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[x5x5x5_Grading_412284402],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Risk_Claim_ID_414244883],'')) + '}'
	INTO [SyncDML].[MDAIBase_Address_] (TaskID, DMLAction, Unique_ID, Address_ID, Address_Type, AltEntity, Building_, Building_Number, County_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Document_Link_Binding, DX_Exchange, DX_Number, Grid_X, Grid_Y, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Locality_, MDA_Incident_ID_412284502, PAF_Validation, Post_Code, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Street_, Sub_Building, Town_, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

	--HandleTheConflictsIfTheWebsiteNameHasChanged
	DECLARE @NextID INT , @RowCount INT
	SELECT @RowCount = COUNT(1) FROM @Conflicts
	
	IF @RowCount > 0
	BEGIN
		SELECT @NextID = NextID FROM [$(MDA_IBase)].[dbo]._Address__NextID
		UPDATE [$(MDA_IBase)].[dbo]._Address__NextID SET NextID = @NextID + @Rowcount

		UPDATE U
		SET NewIB8ID = New_Unique_ID
		FROM	(
				SELECT  'ADD' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY RowID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM @Conflicts
				WHERE IBase5 IS NOT NULL) A
		INNER JOIN @Conflicts U ON A.RowID = U.RowID

		INSERT INTO [$(MDA_IBase)].[dbo].Address_
				(
				Unique_ID
				,Address_ID
				,Address_Type
				,AltEntity
				,Building_
				,Building_Number
				,County_
				,Create_Date
				,Create_User
				,Do_Not_Disseminate_412284494
				,Document_Link
				,Document_Link_Binding
				,DX_Exchange
				,DX_Number
				,Grid_X
				,Grid_Y
				,IconColour
				,Key_Attractor_412284410
				,Last_Upd_Date
				,Last_Upd_User
				,Locality_
				,PAF_Validation
				,Post_Code
				,Record_Status
				,SCC
				,Source_411765484
				,Source_Description_411765489
				,Source_Reference_411765487
				,Status_Binding
				,Street_
				,Sub_Building
				,Town_
				,x5x5x5_Grading_412284402
				,Risk_Claim_ID_414244883
				)
		OUTPUT	 @OutputTaskID
				,'INSERT'
				,'D:{}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Address_ID,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Address_Type,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Building_,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Building_Number,'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.County_,'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[Document_Link_Binding],'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[DX_Exchange],'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[DX_Number],'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[Grid_X],'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[Grid_Y],'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[IconColour],'')) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[Locality_],'') + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[PAF_Validation],'')) + '}'
				,'D:{}|I:{' + ISNULL(Inserted.[Post_Code],'') + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SCC],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Status_Binding],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Street_],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Sub_Building],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Town_],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[x5x5x5_Grading_412284402],'')) + '}'
				,'D:{}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Risk_Claim_ID_414244883],'')) + '}'
		INTO [SyncDML].[MDAIBase_Address_] (TaskID, DMLAction, Unique_ID, Address_ID, Address_Type, AltEntity, Building_, Building_Number, County_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Document_Link_Binding, DX_Exchange, DX_Number, Grid_X, Grid_Y, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Locality_, MDA_Incident_ID_412284502, PAF_Validation, Post_Code, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Street_, Sub_Building, Town_, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		SELECT 	 C.NewIB8ID
				,Address_ID
				,Address_Type
				,AltEntity
				,Building_
				,Building_Number
				,County_
				,Create_Date
				,Create_User
				,Do_Not_Disseminate_412284494
				,Document_Link
				,Document_Link_Binding
				,DX_Exchange
				,DX_Number
				,Grid_X
				,Grid_Y
				,IconColour
				,Key_Attractor_412284410
				,Last_Upd_Date
				,Last_Upd_User
				,Locality_
				,PAF_Validation
				,Post_Code
				,Record_Status
				,SCC
				,Source_411765484
				,Source_Description_411765489
				,Source_Reference_411765487
				,Status_Binding
				,Street_
				,Sub_Building
				,Town_
				,x5x5x5_Grading_412284402
				,Risk_Claim_ID_414244883
		FROM @Conflicts c
		INNER JOIN IBaseM8Cur.Address_ AD ON AD.Address_ID = C.IBase5

		--RePointTheLinkEndToTheNewID	
		UPDATE LE
		SET  LE.Entity_ID1 = CASE WHEN LE.Entity_ID1 = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE LE.Entity_ID1 END
			,LE.Entity_ID2 = CASE WHEN LE.Entity_ID2 = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE LE.Entity_ID2 END
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Link_ID],'') + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID1],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Confidence],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Direction],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID2],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID1],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID2],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkType_ID],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Type],'')) + '}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{' + ISNULL(Inserted.[SCC],'') + '}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [$(MDA_IBase)].[dbo]._LinkEnd LE
		INNER JOIN (			
					SELECT LE.LinkID, C.IBase8ID, C.NewIB8ID
					FROM @Conflicts C
					INNER JOIN [Rep].[IBaseLinkEnd] LE on c.IBase8ID = LE.EntityID1 
					) LinkID ON LE.Link_ID = LinkID.LinkID

		--RemoveTheLinkFromRep.LinkEndRecord
		DELETE LER
		FROM [Rep].[LinkEndRecord] LER
		INNER JOIN (			
					SELECT LE.LinkID, C.IBase8ID, C.NewIB8ID
					FROM @Conflicts C
					INNER JOIN [Rep].[IBaseLinkEnd] LE on c.IBase8ID = LE.EntityID1 
					) LinkID ON LER.[IBase8LinkID] = LinkID.LinkID

		--AddedToPreventAdditionalRecordBeingInsertedIntoLinkEnd
		UPDATE RLER
		SET  [EntityID1] = CASE WHEN RLER.[EntityID1] = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE RLER.[EntityID1] END
			,[EntityID2] = CASE WHEN RLER.[EntityID2] = LinkID.IBase8ID THEN LinkID.NewIB8ID ELSE RLER.[EntityID2] END
		FROM [Rep].[IBaseLinkEnd] RLER
		INNER JOIN (			
					SELECT LE.Link_ID, C.IBase8ID, C.NewIB8ID
					FROM @conflicts C
					INNER JOIN [$(MDA_IBase)].[dbo]._LinkEnd LE on c.NewIB8ID = LE.Entity_ID1 
					) LinkID ON RLER.[LinkID] = LinkID.Link_ID
	END

	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
		SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
		SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()
				
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
						@Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT

	END	
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH
