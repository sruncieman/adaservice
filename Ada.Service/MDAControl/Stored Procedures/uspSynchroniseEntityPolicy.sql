﻿

CREATE PROCEDURE [Sync].[uspSynchroniseEntityPolicy]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseEntityPerson]
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)ActionTheChangeInMDA_IBase
--						3.3)DoDeletes
--						3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)	
--						4.1)ActionTheChangeInMDA
--						4.2)DoDeletes
--						4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					5)UpdateLoggingAndTidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-10-09			Paul Allen		Added Header formatted and Removed Update of UniqueID within MDA_IBase
--		2014-11-10			Paul Allen		Amended Section 4.3: Error The select list for the INSERT statement contains more items than the insert list.
/**************************************************************************************************/

(
 @MDAChangeVersionFrom            INT
,@MDAChangeVersionTo              INT
,@MDA_IBaseChangeVersionFrom      INT
,@MDA_IBaseChangeVersionTo        INT
,@ParentTaskID                    INT
,@Step                            NVARCHAR(10) = '0' OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE	 @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID             INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID          INT
			,@DatabaseID            INT = DB_ID()
			,@ObjectID              INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount        INT = NULL
			,@UpdateRowCount        INT = NULL
			,@DeleteRowCount        INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID                INT
			,@Counter               INT                  

    IF OBJECT_ID('tempdb..#CT') IS NOT NULL
            DROP TABLE #CT       
    CREATE TABLE #CT (RowID INT IDENTITY(1,1), ChangeDatabase VARCHAR(10) ,MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)
                                  
	SELECT @Details = 'User=['+SYSTEM_USER+']'                    
	EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID        = 126 --SynchronisePolicyEntity
					,@DatabaseID        = @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, MDA_ID, DMLAction)
	SELECT 'MDA', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA].[dbo].Policy, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA_IBase].[dbo].Policy_, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	--2.2)AddSomeIndexesToMakeTheProcessQuicker
	SET @Step ='2.2'
       
	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT(MDA_IBase_Unique_ID ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3'

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IA.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[Policy_] IA ON IA.Unique_ID = CT.MDA_IBase_Unique_ID
	AND IA.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MA.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].[Policy] MA ON MA.ID = CT.MDA_ID

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4'

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN	(
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5'

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1

	-----------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	------------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1'

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM [MDA_IBase].[dbo]._Policy__NextID
		UPDATE [MDA_IBase].[dbo]._Policy__NextID SET NextID = @NextID + @Counter 
       
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM   (
				SELECT 'POL' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY MDA_ID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--3.2)ActionTheChangeInMDA_IBase
		SET @Step ='3.2'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[Policy_] [Target]
		USING  (
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID,PT.[Text],pct.CoverText, MADD.*
				FROM #CT CT
				INNER JOIN [MDA].[dbo].[Policy] MADD ON MADD.ID = CT.MDA_ID
				INNER JOIN [MDA].[dbo].[PolicyType] PT ON MADD.PolicyType_ID =PT.ID 
				INNER JOIN [MDA].[dbo].[PolicyCoverType] PCT ON MADD.PolicyCoverType_ID =PCT.ID 
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				AND (NULLIF(MADD.PolicyNumber,'') IS NOT NULL OR LEFT(MADD.[SourceDescription],3) != 'ADA')
				) Source 
				ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET	 Broker_							= Source.[Broker]
			,Cover_Type							= CASE WHEN [PolicyCoverType_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN Target.Cover_Type ELSE NULLIF(Source.CoverText,'unknown') END
			,Create_date						= Source.CreatedDate
			,Create_User						= Source.CreatedBy
			,Insurer_							= Source.Insurer
			,Insurer_Trading_Name				= Source.InsurerTradingName
			,Key_Attractor_412284410			= Source.KeyAttractor
			,Policy_End_Date					= Source.PolicyEndDate
			,Policy_Number						= Source.PolicyNumber
			,Policy_Start_Date					= Source.PolicyStartDate
			,Policy_Type						= CASE WHEN [PolicyType_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN Target.Policy_Type ELSE NULLIF(Source.[Text],'Unknown') END
			,Premium_							= Source.Premium
			,Previous_Fault_Claims_Disclosed	= Source.PreviousFaultClaimsCount
			,Source_411765484					= Source.[Source]
			,Source_Description_411765489		= Source.SourceDescription
			,Source_Reference_411765487			= Source.SourceReference
			,Last_upd_date						= Source.ModifiedDate
			,Last_Upd_User						= Source.ModifiedBy
		WHEN NOT MATCHED THEN INSERT 
			(
			[Unique_ID]
			,[MDA_Incident_ID_412284502]
			,[Broker_]
			,[Cover_Type]
			,[Create_date]
			,[Create_User]
			,[Do_Not_Disseminate_412284494]
			,[Insurer_]
			,[Insurer_Trading_Name]
			,[Key_Attractor_412284410]
			,[Policy_End_Date]
			,[Policy_Number]
			,[Policy_Start_Date]
			,[Policy_Type]
			,[Premium_]
			,[Previous_Fault_Claims_Disclosed]
			,[Policy_ID]
			,[Previous_Claims_Disclosed]
			,[Source_411765484]
			,[Source_Description_411765489]
			,[Source_Reference_411765487]
			,[Last_upd_date]
			,[Last_Upd_User]
			)
		VALUES
			(
			Source.MDA_IBase_Unique_ID
			,[SYNC].[fn_ResolveMDAID](Source.Id,'MDA.dbo.Policy')
			,Source.[Broker]
			,CASE WHEN [PolicyCoverType_Id] = 0 THEN NULL ELSE Source.CoverText END
			,Source.[CreatedDate]
			,Source.[CreatedBy]
			,0
			,Source.[Insurer]
			,Source.[InsurerTradingName]
			,Source.[KeyAttractor]
			,Source.[PolicyEndDate]
			,Source.[PolicyNumber]
			,Source.[PolicyStartDate]
			,CASE WHEN [PolicyType_Id] = 0 THEN NULL ELSE Source.[Text] END
			,Source.[Premium]
			,Source.[PreviousFaultClaimsCount]
			,Source.[PolicyID]
			,Source.[PreviousNoFaultClaimsCount]
			,Source.[Source]
			,Source.[SourceDescription]
			,Source.[SourceReference]
			,Source.[ModifiedDate]
			,Source.[ModifiedBy]
			)
		OUTPUT  @OutputTaskID
				,$ACTION
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Broker_,'') + '}|I:{' + ISNULL(Inserted.Broker_,'') + '}'
				,'D:{' + ISNULL(Deleted.Cover_Type,'') + '}|I:{' + ISNULL(Inserted.Cover_Type,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Insurer_,'') + '}|I:{' + ISNULL(Inserted.Insurer_,'') + '}'
				,'D:{' + ISNULL(Deleted.Insurer_Trading_Name,'') + '}|I:{' + ISNULL(Inserted.Insurer_Trading_Name,'') + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Policy_End_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Policy_End_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Policy_ID,'') + '}|I:{' + ISNULL(Inserted.Policy_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Policy_Number,'') + '}|I:{' + ISNULL(Inserted.Policy_Number,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Policy_Start_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Policy_Start_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Policy_Type,'') + '}|I:{' + ISNULL(Inserted.Policy_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.Premium_,'') + '}|I:{' + ISNULL(Inserted.Premium_,'') + '}'
				,'D:{' + ISNULL(Deleted.Previous_Claims_Disclosed,'') + '}|I:{' + ISNULL(Inserted.Previous_Claims_Disclosed,'') + '}'
				,'D:{' + ISNULL(Deleted.Previous_Fault_Claims_Disclosed,'') + '}|I:{' + ISNULL(Inserted.Previous_Fault_Claims_Disclosed,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Policy_] ([TaskID],[DMLAction],[Unique_ID],[AltEntity] ,[Broker_],[Cover_Type],[Create_Date],[Create_User],[Do_Not_Disseminate_412284494],[IconColour] ,[Insurer_],[Insurer_Trading_Name],[Key_Attractor_412284410],[Last_Upd_Date],[Last_Upd_User],[MDA_Incident_ID_412284502],[Policy_End_Date],[Policy_ID],[Policy_Number],[Policy_Start_Date],[Policy_Type],[Premium_],[Previous_Claims_Disclosed],[Previous_Fault_Claims_Disclosed],[Record_Status],[SCC],[Source_411765484],[Source_Description_411765489],[Source_Reference_411765487],[Status_Binding],[x5x5x5_Grading_412284402],[Risk_Claim_ID_414244883]);
     
		--3.3)DoDeletes
		SET @Step ='3.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[Policy_]
		OUTPUT  @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Broker_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Cover_Type,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Insurer_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Insurer_Trading_Name,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Policy_End_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Policy_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Policy_Number,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Policy_Start_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Policy_Type,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Premium_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Previous_Claims_Disclosed,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Previous_Fault_Claims_Disclosed,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Policy_] ([TaskID],[DMLAction],[Unique_ID],[AltEntity],[Broker_] ,[Cover_Type],[Create_Date],[Create_User],[Do_Not_Disseminate_412284494],[IconColour],[Insurer_],[Insurer_Trading_Name],[Key_Attractor_412284410],[Last_Upd_Date],[Last_Upd_User],[MDA_Incident_ID_412284502],[Policy_End_Date],[Policy_ID],[Policy_Number],[Policy_Start_Date],[Policy_Type],[Premium_],[Previous_Claims_Disclosed],[Previous_Fault_Claims_Disclosed],[Record_Status],[SCC],[Source_411765484],[Source_Description_411765489],[Source_Reference_411765487],[Status_Binding],[x5x5x5_Grading_412284402],[Risk_Claim_ID_414244883])
		FROM [MDA_IBase].[dbo].[Policy_] MADD
		INNER JOIN #CT CT ON [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Policy') = MADD.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';

		--3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='3.4';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MADD
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT  @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.PolicyId,'') + '}|I:{' + ISNULL(Inserted.PolicyId,'') + '}'
				,'D:{' + ISNULL(Deleted.PolicyNumber,'') + '}|I:{' + ISNULL(Inserted.PolicyNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Insurer,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Insurer,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.InsurerTradingName,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.InsurerTradingName,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Broker,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Broker,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyCoverType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyCoverType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyEndDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PreviousNoFaultClaimsCount,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PreviousNoFaultClaimsCount,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PreviousFaultClaimsCount,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PreviousFaultClaimsCount,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Premium,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Premium,'')) + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Policy] ([TaskID],[DMLAction],[Id] ,[PolicyId],[PolicyNumber],[Insurer],[InsurerTradingName],[Broker],[PolicyType_Id],[PolicyCoverType_Id],[PolicyStartDate],[PolicyEndDate],[PreviousNoFaultClaimsCount],[PreviousFaultClaimsCount] ,[Premium],[KeyAttractor],[Source],[SourceReference],[SourceDescription],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[IBaseId],[RecordStatus],[ADARecordStatus])
		FROM [MDA].[dbo].[Policy] MADD
		INNER JOIN #CT CT ON CT.MDA_ID = MADD.Id
		WHERE MADD.IBaseId IS NULL
		OR MADD.IBaseId != CT.MDA_IBase_Unique_ID
                     
	END

	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
    IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
    BEGIN
        --4.1)ActionTheChangeInMDA
        SET @Step ='4.1'

        SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
        WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
        MERGE [MDA].[dbo].[Policy] [Target]
        USING  (
                    SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID,tt.ID as PolicyType_ID, pct.ID as PolicyCover_ID, IADD.*
                    FROM #CT CT
                    INNER JOIN [MDA_IBase].[dbo].[Policy_] IADD ON IADD.Unique_ID = CT.MDA_IBase_Unique_ID
                    LEFT JOIN [MDA].[dbo].[PolicyType] tt ON IADD.Policy_Type = tt.[Text]
                    LEFT JOIN [MDA].[dbo].[PolicyCoverType] pct ON IADD.Policy_Type = pct.CoverText
                    WHERE CT.DoNotReplicate IS NULL
                    AND CT.ChangeDatabase = 'MDA_IBase'
                    ) Source ON Source.MDA_ID = [Target].ID
        WHEN MATCHED THEN UPDATE
        SET	[Broker]					= Source.[Broker_]
            ,PolicyCoverType_id			= ISNULL(Source.policyCover_ID,0)
            ,CreatedDate				= Source.[Create_date]
            ,CreatedBy					= Source.[Create_User]
            ,Insurer					= Source.[Insurer_]
            ,InsurerTradingName			= Source.[Insurer_Trading_Name]
            ,KeyAttractor				= Source.[Key_Attractor_412284410]
            ,PolicyEndDate				= Source.[Policy_End_Date]
            ,PolicyNumber				= Source.[Policy_Number]
            ,PolicyStartDate			= Source.[Policy_Start_Date]
            ,PolicyType_id				= ISNULL(Source.[PolicyType_ID],0)
            ,Premium					= Source.[Premium_]
            ,PreviousFaultClaimsCount	= Source.[Previous_Fault_Claims_Disclosed]
            ,PolicyID					= Source.[Policy_ID]
            ,PreviousNoFaultClaimsCount = Source.[Previous_Claims_Disclosed]
            ,[Source]					= Source.[Source_411765484]
            ,SourceDescription			= Source.[Source_Description_411765489]
            ,SourceReference			= Source.[Source_Reference_411765487]
            ,ModifiedDate				= Source.[Last_upd_date]
            ,ModifiedBy					= Source.[Last_Upd_User]
		WHEN NOT MATCHED THEN INSERT 
            (
			 [Broker]
			,[PolicyCoverType_id]
			,[CreatedDate]
			,[CreatedBy]
			,[IBaseId]
			,[Insurer]
			,[InsurerTradingName]
			,[KeyAttractor]
			,[PolicyEndDate]
			,[PolicyNumber]
			,[PolicyStartDate]
			,[PolicyType_id]
			,[Premium]
			,[PreviousFaultClaimsCount]
			,[PolicyID]
			,[PreviousNoFaultClaimsCount]
			,[Source]
			,[SourceDescription]
			,[SourceReference]
			,[ModifiedDate]
            ,[ModifiedBy]
            )
		VALUES
            ( 
			 Source.[Broker_]
			,ISNULL(Source.PolicyCover_ID,0)
			,Source.[Create_date]
			,Source.[Create_User]
			,Source.[Unique_ID]
			,Source.[Insurer_]
			,Source.[Insurer_Trading_Name]
			,Source.[Key_Attractor_412284410]
			,Source.[Policy_End_Date]
			,Source.[Policy_Number]
			,Source.[Policy_Start_Date]
			,ISNULL(Source.[PolicyType_ID],0)
			,Source.[Premium_]
			,Source.[Previous_Fault_Claims_Disclosed]
			,Source.[Policy_ID]
			,Source.[Previous_Claims_Disclosed]
			,Source.[Source_411765484]
			,Source.[Source_Description_411765489]
			,Source.[Source_Reference_411765487]
			,Source.[Last_upd_date]
			,Source.[Last_Upd_User]
            )
		OUTPUT   @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.PolicyId,'') + '}|I:{' + ISNULL(Inserted.PolicyId,'') + '}'
				,'D:{' + ISNULL(Deleted.PolicyNumber,'') + '}|I:{' + ISNULL(Inserted.PolicyNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Insurer,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Insurer,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.InsurerTradingName,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.InsurerTradingName,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Broker,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Broker,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyCoverType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyCoverType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyEndDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PreviousNoFaultClaimsCount,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PreviousNoFaultClaimsCount,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PreviousFaultClaimsCount,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PreviousFaultClaimsCount,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Premium,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Premium,'')) + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Policy] ([TaskID] ,[DMLAction],[Id] ,[PolicyId],[PolicyNumber],[Insurer],[InsurerTradingName] ,[Broker],[PolicyType_Id],[PolicyCoverType_Id],[PolicyStartDate],[PolicyEndDate],[PreviousNoFaultClaimsCount],[PreviousFaultClaimsCount],[Premium],[KeyAttractor],[Source],[SourceReference],[SourceDescription] ,[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[IBaseId],[RecordStatus],[ADARecordStatus]);
              
        --4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
        SET @Step ='4.2';

        WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
        UPDATE [MDA].[dbo].[Policy]
        SET  RecordStatus		= 254
			,ADARecordStatus	= 4
        OUTPUT 
                @OutputTaskID
                ,'UPDATE DELETE'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
                ,'D:{' + ISNULL(Deleted.PolicyId,'') + '}|I:{' + ISNULL(Inserted.PolicyId,'') + '}'
                ,'D:{' + ISNULL(Deleted.PolicyNumber,'') + '}|I:{' + ISNULL(Inserted.PolicyNumber,'') + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Insurer,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Insurer,'')) + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.InsurerTradingName,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.InsurerTradingName,'')) + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Broker,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Broker,'')) + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyType_Id,'')) + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyCoverType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyCoverType_Id,'')) + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyStartDate,''),126) + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PolicyEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PolicyEndDate,''),126) + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PreviousNoFaultClaimsCount,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PreviousNoFaultClaimsCount,'')) + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.PreviousFaultClaimsCount,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.PreviousFaultClaimsCount,'')) + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Premium,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Premium,'')) + '}'
                ,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
                ,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
                ,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
                ,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
                ,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
                ,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
                ,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
                ,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
        INTO [SyncDML].[MDA_Policy] ([TaskID],[DMLAction] ,[Id],[PolicyId],[PolicyNumber],[Insurer],[InsurerTradingName],[Broker],[PolicyType_Id],[PolicyCoverType_Id],[PolicyStartDate],[PolicyEndDate],[PreviousNoFaultClaimsCount],[PreviousFaultClaimsCount],[Premium],[KeyAttractor] ,[Source],[SourceReference],[SourceDescription],[CreatedBy],[CreatedDate],[ModifiedBy] ,[ModifiedDate],[IBaseId] ,[RecordStatus],[ADARecordStatus])
		FROM [MDA].[dbo].[Policy] IADD
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IADD.IBaseId
		WHERE CT.DMLAction = 'D';
        
		--4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='4.3';
		   
        WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
        UPDATE IADD
        SET MDA_Incident_ID_412284502 = [Sync].[fn_ResolveMDAID] (MADD.ID,'MDA.dbo.Policy')
        OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Broker_,'') + '}|I:{' + ISNULL(Inserted.Broker_,'') + '}'
				,'D:{' + ISNULL(Deleted.Cover_Type,'') + '}|I:{' + ISNULL(Inserted.Cover_Type,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Insurer_,'') + '}|I:{' + ISNULL(Inserted.Insurer_,'') + '}'
				,'D:{' + ISNULL(Deleted.Insurer_Trading_Name,'') + '}|I:{' + ISNULL(Inserted.Insurer_Trading_Name,'') + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Policy_End_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Policy_End_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Policy_ID,'') + '}|I:{' + ISNULL(Inserted.Policy_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Policy_Number,'') + '}|I:{' + ISNULL(Inserted.Policy_Number,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Policy_Start_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Policy_Start_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Policy_Type,'') + '}|I:{' + ISNULL(Inserted.Policy_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.Premium_,'') + '}|I:{' + ISNULL(Inserted.Premium_,'') + '}'
				,'D:{' + ISNULL(Deleted.Previous_Claims_Disclosed,'') + '}|I:{' + ISNULL(Inserted.Previous_Claims_Disclosed,'') + '}'
				,'D:{' + ISNULL(Deleted.Previous_Fault_Claims_Disclosed,'') + '}|I:{' + ISNULL(Inserted.Previous_Fault_Claims_Disclosed,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Policy_] ([TaskID],[DMLAction],[Unique_ID],[AltEntity] ,[Broker_],[Cover_Type],[Create_Date],[Create_User],[Do_Not_Disseminate_412284494],[IconColour] ,[Insurer_],[Insurer_Trading_Name],[Key_Attractor_412284410],[Last_Upd_Date],[Last_Upd_User],[MDA_Incident_ID_412284502],[Policy_End_Date],[Policy_ID],[Policy_Number],[Policy_Start_Date],[Policy_Type],[Premium_],[Previous_Claims_Disclosed],[Previous_Fault_Claims_Disclosed],[Record_Status],[SCC],[Source_411765484],[Source_Description_411765489],[Source_Reference_411765487],[Status_Binding],[x5x5x5_Grading_412284402],[Risk_Claim_ID_414244883])
		FROM [MDA_IBase].[dbo].[Policy_] IADD
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IADD.Unique_ID
		INNER JOIN [MDA].[dbo].[Policy] MADD  ON MADD.IBaseId = IADD.Unique_ID
		WHERE IADD.MDA_Incident_ID_412284502 IS NULL
        OR [Sync].[fn_ResolveMDAID] (MADD.ID,'MDA.dbo.Policy') != IADD.MDA_Incident_ID_412284502

    END

    --------------------------------------------------------------------------
    --5)UpdateLoggingAndTidy
    --------------------------------------------------------------------------
    SELECT @InsertRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Policy] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
    SELECT @UpdateRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Policy] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
    SELECT @DeleteRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Policy] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

    SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Policy_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
    SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Policy_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
    SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Policy_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
              
    SELECT   @Now	 = GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	   

END TRY
BEGIN CATCH

	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +@Step +']'

	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH






