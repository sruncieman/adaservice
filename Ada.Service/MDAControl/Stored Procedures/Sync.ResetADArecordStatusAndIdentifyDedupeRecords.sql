﻿CREATE PROCEDURE [Sync].[ResetADArecordStatusAndIdentifyDedupeRecords]
AS
SET NOCOUNT ON
DECLARE  @SQL NVARCHAR(MAX) = ''
		,@Counter TINYINT = 1
		,@ToProcess TINYINT
		,@Sys_Change_Context VARBINARY(128)

SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');

IF OBJECT_ID('tempdb..#SQLStatement') IS NOT NULL
	DROP TABLE #SQLStatement
CREATE TABLE #SQLStatement (RowID INT IDENTITY(1,1), SQLStatement NVARCHAR(MAX)NOT NULL)

IF OBJECT_ID('tempdb..#AddressInstances') IS NOT NULL
	DROP TABLE #AddressInstances

--------------------------
--ResetTheADARecordStatus
--------------------------
INSERT INTO #SQLStatement (SQLStatement)
SELECT 'DECLARE @Sys_Change_Context	VARBINARY(128)'+CHAR(10)+'SET @Sys_Change_Context = CONVERT(VARBINARY(128),''VanillaPOCSetUp'');'+CHAR(10)+' WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context) ' +CHAR(10)+'UPDATE MDA.dbo.'+T.name+' SET ADARecordStatus = ADARecordStatus - 10 WHERE ADARecordStatus >=10'
FROM MDA.sys.Columns C
INNER JOIN MDA.sys.Tables T ON T.object_id = C.object_id
WHERE C.Name = 'ADARecordStatus'
AND EXISTS (SELECT TOP 1 1 FROM MDA.sys.Columns C_ WHERE C_.object_id = T.object_id AND C_.Name = 'IBaseID')
AND C.Name != 'Organisation'
ORDER BY T.Name
SELECT @ToProcess = @@ROWCOUNT

WHILE @ToProcess >= @Counter
BEGIN
	SELECT @SQL = SQLStatement
	FROM #SQLStatement
	WHERE RowID = @Counter
	
	EXECUTE SP_EXECUTESQL @SQL

	SELECT @Counter += 1
END

--WeWantToSetcertainAddressRecordsToBeDeduped
SELECT ROW_NUMBER() OVER (ORDER BY COUNT(1) DESC) InstanceID,[SubBuilding],[Building],[BuildingNumber],[Street],[Locality],[Town],[County],[PostCode],COUNT(1) Instances
INTO #AddressInstances
FROM MDA.[dbo].[Address]
WHERE [ADARecordStatus] = 0
AND NULLIF([PafUPRN],'') IS NULL
GROUP BY [SubBuilding],[Building],[BuildingNumber],[Street],[Locality],[Town],[County],[PostCode]
HAVING COUNT(1) >1
ORDER BY COUNT(1) DESC

;WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
UPDATE A 
SET [ADARecordStatus] = [ADARecordStatus] + 10
FROM MDA.[dbo].[Address] A
INNER JOIN (
			SELECT Id
			FROM MDA.[dbo].[Address] A
			INNER JOIN #AddressInstances AI 
				ON  ISNULL(A.[SubBuilding],'') = ISNULL(AI.[SubBuilding],'')
				AND ISNULL(A.[Building],'') = ISNULL(AI.[Building],'')
				AND ISNULL(A.[BuildingNumber],'') = ISNULL(AI.[BuildingNumber],'')
				AND ISNULL(A.[Street],'') = ISNULL(AI.[Street],'')
				AND ISNULL(A.[Locality],'') = ISNULL(AI.[Locality],'')
				AND ISNULL(A.[Town],'') = ISNULL(AI.[Town],'')
				AND ISNULL(A.[County],'') = ISNULL(AI.[County],'')
				AND ISNULL(A.[PostCode],'') = ISNULL(AI.[PostCode],'')
			) DATA ON DATA.Id = A.ID

-------------------------------------
--Tidy
-------------------------------------
IF OBJECT_ID('tempdb..#SQLStatement') IS NOT NULL
	DROP TABLE #SQLStatement
IF OBJECT_ID('tempdb..#AddressInstances') IS NOT NULL
	DROP TABLE #AddressInstances