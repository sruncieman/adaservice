﻿

CREATE PROCEDURE [IBaseM].[uspKeoPersonOrganisations]
AS
SET NOCOUNT ON

BEGIN TRY

	IF OBJECT_Id('tempdb..#_P_O') is not null
		DROP TABLE #_P_O			  
				  
	DECLARE @SEP VARCHAR(5) = '; '

	CREATE TABLE #_P_O
	(
		PER_ORG_KRef VARCHAR(15),
		PER_ORG_Link VARCHAR(15),
		PER_ORG_Ent1 VARCHAR(15),
		PER_ORG_Ent2 VARCHAR(15),
		PER_ORG_NL_Type VARCHAR(50)
	)

	INSERT INTO #_P_O
	SELECT KEO.Entity_ID1, LE.Link_ID, LE.Entity_ID1, LE.Entity_ID2, N.Description_ AS NL_Type
		FROM 							
			IBaseM5Cur._LinkEnd KEO  WITH (NOLOCK)
		INNER JOIN 
			IBaseM5Cur._LinkEnd LE  WITH (NOLOCK)
			ON KEO.Entity_ID2 = LE.Entity_ID1 
			AND LEFT(LE.Entity_ID1, 3) = 'PER'
			AND LEFT(LE.Entity_ID2, 3) = 'ORG'
		LEFT JOIN IBaseM5Cur.Nominal_Link N  WITH (NOLOCK)
		ON LE.Link_ID = N.Unique_ID
			WHERE 
			LEFT(KEO.Entity_ID1, 3) = 'KEO' 
			AND (KEO.Record_Status !=  254 )
			AND (LE.Record_Status != 254)

	UPDATE #_P_O
	SET PER_ORG_NL_Type = 'Hirer'
	WHERE PER_ORG_Link IN(SELECT  Link_ID FROM [IBaseM].TempNomLinkDescFromUsesHirer)
	AND PER_ORG_NL_Type =  'Uses'


	SELECT 
		PER_ORG_KRef,
		PER_ORG_Ent1,
		MAX(MedExam) MedExam,
		MAX(Hire) Hire,
		MAX(Solicitors) Solicitors,
		MAX(Accident_Management) Accident_Management

	FROM (

		SELECT 
			PER_ORG_KRef,
			PER_ORG_Ent1,
			CASE WHEN PER_ORG_NL_Type = 'Examined By' THEN Val END AS MedExam,
			CASE WHEN PER_ORG_NL_Type = 'Hirer' THEN Val END AS Hire,
			CASE WHEN PER_ORG_NL_Type = 'Represented by' THEN Val END AS Solicitors,
			CASE WHEN PER_ORG_NL_Type = 'Uses' THEN Val END AS Accident_Management
		FROM 
		(
				SELECT 
					PER_ORG_KRef,
					PER_ORG_Ent1,
					PER_ORG_NL_Type,
					MAX(C1) +
					MAX(C2) +
					MAX(C3) +
					MAX(C4) +
					MAX(C5) +
					MAX(C6) +
					MAX(C7) +
					MAX(C8) + 
					MAX(C9) +
					MAX(C10) AS Val
		FROM
			(

				SELECT 
					PER_ORG_KRef,
					PER_ORG_Ent1,
					PER_ORG_NL_Type,
					CASE WHEN RN = 1 THEN ORG  + @SEP ELSE '' END C1,
					CASE WHEN RN = 2 THEN ORG  + @SEP ELSE '' END C2,
					CASE WHEN RN = 3 THEN ORG  + @SEP ELSE '' END C3,
					CASE WHEN RN = 4 THEN ORG  + @SEP ELSE '' END C4,
					CASE WHEN RN = 5 THEN ORG  + @SEP ELSE '' END C5,
					CASE WHEN RN = 6 THEN ORG  + @SEP ELSE '' END C6,
					CASE WHEN RN = 7 THEN ORG  + @SEP ELSE '' END C7,
					CASE WHEN RN = 8 THEN ORG  + @SEP ELSE '' END C8, 
					CASE WHEN RN = 9 THEN ORG  + @SEP ELSE '' END C9,
					CASE WHEN RN = 10 THEN ORG + @SEP ELSE '' END C10
				FROM (

					SELECT 
					ROW_NUMBER() OVER(PARTITION BY PER_ORG_KRef, PER_ORG_Ent1, PER_ORG_NL_Type ORDER BY PER_ORG_Ent2 ) RN,
						PER_ORG_KRef,
						PER_ORG_Ent1,
						PER_ORG_Ent2,
						PER_ORG_NL_Type,
						O.Name_ + ' (' +  PER_ORG_Ent2 + ')' AS ORG
					 FROM #_P_O
						INNER JOIN 
						IBaseM5Cur.Organisation_ O
						ON PER_ORG_Ent2 = o.Unique_ID
					

				 ) RN


					GROUP BY  
						PER_ORG_KRef,
						PER_ORG_Ent1,
						PER_ORG_NL_Type,
						ORG,
						RN
					) G

		GROUP BY
			PER_ORG_KRef,
			PER_ORG_Ent1,

			PER_ORG_NL_Type
			)  AS  P
	GROUP BY		
		PER_ORG_KRef,
			PER_ORG_Ent1,

			CASE WHEN PER_ORG_NL_Type = 'Examined By' THEN Val END,
			CASE WHEN PER_ORG_NL_Type = 'Hirer' THEN Val END,
			CASE WHEN PER_ORG_NL_Type = 'Represented by' THEN Val END,
			CASE WHEN PER_ORG_NL_Type = 'Uses' THEN Val END
		
		) O
			
	GROUP BY 		
		PER_ORG_KRef,
		PER_ORG_Ent1

		
	IF OBJECT_Id('tempdb..#_P_O') is not null
		DROP TABLE #_P_O
END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH




