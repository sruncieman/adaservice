﻿
CREATE PROCEDURE [Rep].[uspIdentifyInfoOnlyCases]
AS 
BEGIN

	UPDATE [$(MDA_IBase)].[dbo].[Case_to_Incident_Link]
	SET Link_Type = 'Instruction'
	WHERE Link_Type IS NULL

	UPDATE l
	SET Link_Type = 'Info Only'
	FROM [$(MDA_IBase)].[dbo].[Case_to_Incident_Link] l
		INNER JOIN [$(MDA_IBase)].[dbo]._LinkEnd le
			ON l.Unique_ID = le.Link_ID
		INNER JOIN  [$(MDA_IBase)].[dbo].[Keoghs_Case]kc
			ON le.Entity_ID1 = kc.Unique_ID
	WHERE ((ISNULL(Client_Reference,'')=''AND LEN(Keoghs_Elite_Reference)!=6)
			OR (ISNULL(Client_Reference,'')=''  AND LEN(Keoghs_Elite_Reference)=6	AND LEFT([Keoghs_Elite_Reference],1) >5)
			OR (ISNULL(Client_Reference,'') = [Keoghs_Elite_Reference])
			OR (ISNULL(Client_Reference,'') <> '' AND Keoghs_Elite_Reference NOT LIKE '%/%' AND Keoghs_Elite_Reference LIKE '%[a-z]%'))
		AND Link_Type <> 'Info Only'


END






