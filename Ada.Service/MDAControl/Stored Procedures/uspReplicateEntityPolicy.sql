﻿CREATE PROCEDURE [Rep].[uspReplicateEntityPolicy]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateEntityPolicy]	
--
-- Description:		Replicate Entity Policy	
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLogging/Tidy			
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Policy_')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 45 --ReplicatePolicyEntity
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------	
	UPDATE [$(MDA_IBase)].[dbo].Policy_
	SET Record_Status = 254
	OUTPUT  @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + ISNULL(Deleted.Broker_,'') + '}|I:{' + ISNULL(Inserted.Broker_,'') + '}'
			,'D:{' + ISNULL(Deleted.Cover_Type,'') + '}|I:{' + ISNULL(Inserted.Cover_Type,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Insurer_,'') + '}|I:{' + ISNULL(Inserted.Insurer_,'') + '}'
			,'D:{' + ISNULL(Deleted.Insurer_Trading_Name,'') + '}|I:{' + ISNULL(Inserted.Insurer_Trading_Name,'') + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Policy_End_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Policy_End_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Policy_ID,'') + '}|I:{' + ISNULL(Inserted.Policy_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Policy_Number,'') + '}|I:{' + ISNULL(Inserted.Policy_Number,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Policy_Start_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Policy_Start_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Policy_Type,'') + '}|I:{' + ISNULL(Inserted.Policy_Type,'') + '}'
			,'D:{' + ISNULL(Deleted.Premium_,'') + '}|I:{' + ISNULL(Inserted.Premium_,'') + '}'
			,'D:{' + ISNULL(Deleted.Previous_Claims_Disclosed,'') + '}|I:{' + ISNULL(Inserted.Previous_Claims_Disclosed,'') + '}'
			,'D:{' + ISNULL(Deleted.Previous_Fault_Claims_Disclosed,'') + '}|I:{' + ISNULL(Inserted.Previous_Fault_Claims_Disclosed,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Policy_] (TaskID, DMLAction, Unique_ID, AltEntity, Broker_, Cover_Type, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Insurer_, Insurer_Trading_Name, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Policy_End_Date, Policy_ID, Policy_Number, Policy_Start_Date, Policy_Type, Premium_, Previous_Claims_Disclosed, Previous_Fault_Claims_Disclosed, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.EntityRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo].Policy_ a ON ent.IBase8EntityID = a.Unique_ID
	WHERE RecordAction ='D'
	
	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo].Policy_ [Target]
	USING	(
			SELECT   DISTINCT C.IBase8EntityID
					,C.IBase5EntityID
					,A.*
			FROM IBaseM8Cur.Policy_ A
			INNER JOIN Rep.EntityRecord C ON A.Unique_ID = C.IBase8EntityID_Staging	
			) AS Source ON [Target].Unique_ID = Source.IBase8EntityID
	WHEN MATCHED THEN UPDATE
	SET  Policy_Number						= Source.Policy_Number
		,Insurer_							= Source.Insurer_	
		,Insurer_Trading_Name				= Source.Insurer_Trading_Name
		,Broker_							= Source.Broker_
		,Policy_Type						= Source.Policy_Type
		,Cover_Type							= Source.Cover_Type
		,Policy_Start_Date					= Source.Policy_Start_Date
		,Policy_End_Date					= Source.Policy_End_Date
		,Previous_Fault_Claims_Disclosed	= Source.Previous_Fault_Claims_Disclosed
		,Previous_Claims_Disclosed			= Source.Previous_Claims_Disclosed
		,Premium_							= Source.Premium_
		,Key_Attractor_412284410			= Source.Key_Attractor_412284410
		,Source_411765484					= Source.Source_411765484	
		,Source_Reference_411765487			= Source.Source_Reference_411765487
		,Source_Description_411765489		= Source.Source_Description_411765489
		,Create_Date						= Source.Create_Date
		,Create_User						= Source.Create_User
		,Last_Upd_Date						= Source.Last_Upd_Date
		,Last_Upd_User						= Source.Last_Upd_User
		,Record_Status						= Source.Record_Status
		,[AltEntity]						= Source.[AltEntity]
		,[Do_Not_Disseminate_412284494]		= Source.[Do_Not_Disseminate_412284494]
		,[IconColour]						= Source.[IconColour]
		,[Policy_ID]						= Source.[Policy_ID]
		,[SCC]								= Source.[SCC]
		,[Status_Binding]					= Source.[Status_Binding]
		,[x5x5x5_Grading_412284402]			= Source.[x5x5x5_Grading_412284402]
		,[Risk_Claim_ID_414244883]			= Source.[Risk_Claim_ID_414244883]
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,AltEntity
		,Broker_
		,Cover_Type
		,Create_Date
		,Create_User
		,Do_Not_Disseminate_412284494
		,IconColour
		,Insurer_
		,Insurer_Trading_Name
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,Policy_End_Date
		,Policy_ID
		,Policy_Number
		,Policy_Start_Date
		,Policy_Type
		,Premium_
		,Previous_Claims_Disclosed
		,Previous_Fault_Claims_Disclosed
		,Record_Status
		,SCC
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding, x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883
		)
	VALUES
		(
		 Source.IBase8EntityID
		,Source.AltEntity
		,Source.Broker_
		,Source.Cover_Type
		,Source.Create_Date
		,Source.Create_User
		,Source.Do_Not_Disseminate_412284494
		,Source.IconColour
		,Source.Insurer_
		,Source.Insurer_Trading_Name
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Policy_End_Date
		,Source.Policy_ID
		,Source.Policy_Number
		,Source.Policy_Start_Date
		,Source.Policy_Type
		,Source.Premium_
		,Source.Previous_Claims_Disclosed
		,Source.Previous_Fault_Claims_Disclosed
		,Source.Record_Status
		,Source.SCC
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.x5x5x5_Grading_412284402
		,Source.Risk_Claim_ID_414244883
		)
	OUTPUT  @OutputTaskID
			,'UPDATE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + ISNULL(Deleted.Broker_,'') + '}|I:{' + ISNULL(Inserted.Broker_,'') + '}'
			,'D:{' + ISNULL(Deleted.Cover_Type,'') + '}|I:{' + ISNULL(Inserted.Cover_Type,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Insurer_,'') + '}|I:{' + ISNULL(Inserted.Insurer_,'') + '}'
			,'D:{' + ISNULL(Deleted.Insurer_Trading_Name,'') + '}|I:{' + ISNULL(Inserted.Insurer_Trading_Name,'') + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Policy_End_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Policy_End_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Policy_ID,'') + '}|I:{' + ISNULL(Inserted.Policy_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Policy_Number,'') + '}|I:{' + ISNULL(Inserted.Policy_Number,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Policy_Start_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Policy_Start_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Policy_Type,'') + '}|I:{' + ISNULL(Inserted.Policy_Type,'') + '}'
			,'D:{' + ISNULL(Deleted.Premium_,'') + '}|I:{' + ISNULL(Inserted.Premium_,'') + '}'
			,'D:{' + ISNULL(Deleted.Previous_Claims_Disclosed,'') + '}|I:{' + ISNULL(Inserted.Previous_Claims_Disclosed,'') + '}'
			,'D:{' + ISNULL(Deleted.Previous_Fault_Claims_Disclosed,'') + '}|I:{' + ISNULL(Inserted.Previous_Fault_Claims_Disclosed,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Policy_] (TaskID, DMLAction, Unique_ID, AltEntity, Broker_, Cover_Type, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Insurer_, Insurer_Trading_Name, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Policy_End_Date, Policy_ID, Policy_Number, Policy_Start_Date, Policy_Type, Premium_, Previous_Claims_Disclosed, Previous_Fault_Claims_Disclosed, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount            += (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Policy_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
		SELECT @UpdateRowCount            += (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Policy_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
		SELECT @DeleteRowCount            += (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Policy_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()
		
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
						@Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH


