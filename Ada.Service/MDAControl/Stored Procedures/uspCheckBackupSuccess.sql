﻿
 
CREATE PROCEDURE [IBaseM].[uspCheckBackupSuccess]
/**************************************************************************************************/
-- ObjectName:			MDAControl.IBaseM.uspCheckBackupSuccess
--
-- Description:			Analyse the backup file and return the status:
--							0 = Backup Exists and is in a loadable state
--							1 = No new file or file older than the one previously logged
--							2 = Invalid file Over 50% Loss in Data 
--
-- Steps:				1)ConfigureLogging,Create/DeclareRequiredObjects
--							1.1)ConfigureLogging
--							1.2)Create/DeclareRequiredObjects
--						2)AnalsyseBackupFile
--						3)GetStatus
--						4)UpdateLogging/Tidy
--
-- Usage:				Called within SP IBaseM.uspRestoreDatabaseFromBackup
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-25			Paul Allen		Created
/**************************************************************************************************/
(
 @BackupDatabaseName_ID	INT
,@Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL
,@Success				INT OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
					
		SELECT @Details = 'Parameters=[@BackupDatabaseName_ID:'+CAST(@BackupDatabaseName_ID AS VARCHAR)+']User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 4 --DatabaseBackupCheck
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	--1.2)Create/DeclareRequiredObjects
	DECLARE @Output TABLE (ID INT IDENTITY(1,1), [Output] VARCHAR(250))
	DECLARE  @BakFileDetails	VARCHAR(250)
			,@BackupDate		DATE
			,@BackupTime		VARCHAR(5)
			,@FileSizeSTR		VARCHAR(150)
			,@FileSize			FLOAT
			,@LastBKDate		DATETIME
			,@PreviousSize		FLOAT 
			,@FileName			VARCHAR(50)
			,@BackupValid		INT
			,@DINT				INT
			,@DBINT				INT
			,@XP_Cmdshell		VARCHAR(500)
			,@FirstRestore		BIT
	 
	--MapNetworkfile
	IF DEFAULT_DOMAIN() = 'FOXTEST' AND @BackupDatabaseName_ID = 1
	BEGIN
		EXEC XP_CMDSHELL 'NET USE R: \\192.168.200.70\KeoghsLiveADABackup /user:KNET\svc-ada-proxy "c0$t@c0ffee" /Persistent:No'
	END
	 
	SELECT @XP_Cmdshell = 'EXEC xp_Cmdshell ''DIR ' + LiveBackupDirectory + ' sortorder e'''  FROM IBaseM.BackupRestoreConfig WHERE BackupDatabaseName_ID = @BackupDatabaseName_ID
	INSERT INTO @Output
	EXECUTE (@XP_Cmdshell)
	
	--UnMapNetworkfile
	IF DEFAULT_DOMAIN() = 'FOXTEST' AND @BackupDatabaseName_ID = 1
	BEGIN
		EXEC XP_CMDSHELL 'NET USE R: /Delete /Y'
	END

	DECLARE @LiveBackupName VARCHAR(255) = (SELECT LiveBackupName FROM IBaseM.BackupRestoreConfig WHERE BackupDatabaseName_ID = @BackupDatabaseName_ID)

	SELECT TOP 1 @BakFileDetails = [Output] 
	FROM @Output 
	WHERE [Output] LIKE '%' + @LiveBackupName + '%'

	IF NOT EXISTS (SELECT TOP 1 1 FROM IBaseM.BackupFileLog WHERE BackupDatabaseName_ID = @BackupDatabaseName_ID)
	 SELECT @FirstRestore = 1

	SELECT   @BackUpDate	 = CONVERT(DATETIME,SUBSTRING(@BakFileDetails,PATINDEX('%[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]%',@BakFileDetails),10),103)
			,@BackUpTime	 = SUBSTRING(@BakFileDetails,PATINDEX('%[0-9][0-9]:[0-9][0-9]%',@BakFileDetails),5)
	
	SELECT	 @BackupDate	= CONVERT(DATETIME,CONVERT(VARCHAR,@BackUpdate, 103) + ' ' + @BackupTime,103)
			,@FilesizeSTR	= SUBSTRING(@BakFileDetails,PATINDEX('%[0-9][0-9][0-9][0-9][0-9]%',REPLACE(@BakFileDetails,',','')),CHARINDEX('k',REPLACE(@BakFileDetails,',','')))
			
	SELECT	@FileSize =ROUND(CONVERT(NUMERIC(18,2),REPLACE(RTRIM(LEFT(@FileSizeSTR,CHARINDEX(' ',@FilesizeSTR))),',',''))/100000000,2),@FileName =@LiveBackupName
		
	SELECT	 @LastBKDate	= FileDate
			,@PreviousSize	= FileSize
	FROM IBaseM.BackupFileLog
	WHERE LoadID = (
					SELECT MAX(LoadID)
					FROM IBaseM.BackupFileLog
					WHERE BackupValid = 0
					AND BackupDatabaseName_ID = @BackupDatabaseName_ID
					)
	AND BackupDatabaseName_ID = @BackupDatabaseName_ID

	------------------------------------------------------------
	--2)AnalsyseBackupFile
	------------------------------------------------------------
	IF @BackUpDate >= @LastBKdate OR @BackupDatabaseName_ID = 2 OR @FirstRestore = 1
	BEGIN
		IF 100-((@Filesize/@PreviousSize)*100)<50 OR @BackupDatabaseName_ID = 2 OR @FirstRestore = 1
		BEGIN
			--Backup Exists and is in a loadable state
			IF @BackUpDate <> @LastBKdate OR @BackupDatabaseName_ID = 2 OR @FirstRestore = 1
			BEGIN
				INSERT INTO IBaseM.BackupFileLog ([FileDate], BackupDatabaseName_ID, [FileSize], [FileName], [SystemUSer], BackupValid)
				SELECT @BackUpDate, @BackupDatabaseName_ID, @FileSize, @FileName, SYSTEM_USER, 0
			END	
		END	
		ELSE
		BEGIN
			-- Invalid file Over 50% Loss in Data 
			INSERT INTO IBaseM.BackupFileLog ([FileDate], BackupDatabaseName_ID, [FileSize], [FileName], [SystemUSer], BackupValid)
			SELECT @BackUpDate, @BackupDatabaseName_ID, @FileSize, @FileName, SYSTEM_USER, 2
				
		END
	END
	ELSE
	BEGIN
		-- No new file or file older than the one previously logged
		INSERT INTO IBaseM.BackupFileLog ([FileDate], BackupDatabaseName_ID, [FileSize], [FileName], [SystemUSer], BackupValid)
		SELECT @BackUpDate, @BackupDatabaseName_ID, @FileSize, @FileName, SYSTEM_USER, 1
	END
	
	------------------------------------------------------------
	--3)GetStatus
	------------------------------------------------------------		
	SELECT @Success = BackupValid
	FROM IBaseM.BackupFileLog
	WHERE LoadID = (SELECT MAX(loadID) FROM IBaseM.BackupFileLog WHERE BackupDatabaseName_ID = @BackupDatabaseName_ID)
	AND BackupDatabaseName_ID = @BackupDatabaseName_ID
	
	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END
	
END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
END CATCH

