﻿

CREATE PROCEDURE[Sync].[uspSynchroniseLinkVehicle]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseLinkVehicle]	
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--						2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)MDAIBase: LinkTablesChanges
--							3.2.1)ActionTheLinkChangeInMDA_IBase
--							3.2.2)DoDeletes
--							3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						3.3)MDAIBase: LinkEndTablesChanges
--							3.3.1)PopulateTheWorkingTable
--							3.3.2)ActionTheLinkEndChangeInMDA_IBase
--							3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
--						4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
--							4.1)ActionTheChangeInMDA
--							4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
--							4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						5)UpdateLoggingAndTidy	
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-05-08			Paul Allen		Created
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT						
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT
			,@PhysicalTableName		VARCHAR(255) = 'Vehicle_Link'
			,@Prefix				VARCHAR(3)   
			,@LinkTypeID			INT

	SELECT @LinkTypeID =Table_ID, @Prefix = TableCode FROM MDA_IBase.dbo._DataTable WHERE PhysicalName = @PhysicalTableName

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1) PRIMARY KEY, ChangeDatabase VARCHAR(10), PhysicalTableName VARCHAR(255), MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)
	
	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging
	CREATE TABLE #LinkEndStaging (Link_ID NVARCHAR(50), Entity_ID1 NVARCHAR(50), Confidence TINYINT, Direction TINYINT, Entity_ID2 NVARCHAR(50), EntityType_ID1 INT , EntityType_ID2 INT, LinkType_ID INT, Record_Status TINYINT, Record_Type TINYINT, SCC NVARCHAR(255))
					
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 117 --SynchroniseVehicleLink
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Vehicle2Person', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA.dbo.Vehicle2Person, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Vehicle2Organisation', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA.dbo.Vehicle2Organisation, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Vehicle_Link', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA_IBase.dbo.Vehicle_Link, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Link_End', Link_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA_IBase.dbo._LinkEnd, @MDA_IBaseChangeVersionFrom) CT
	INNER JOIN [MDA_IBase].[dbo].Vehicle_Link IVL ON IVL.Unique_ID = CT.Link_ID
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL
	GROUP BY Link_ID, Sys_Change_Operation

	--2.2)AddSomeIndexesToMakeTheProcessQuicker 
	SET @Step ='2.2';

	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT (MDA_IBase_Unique_ID ASC)
	CREATE NONCLUSTERED INDEX IX_CT_PhysicalTableName ON #CT (PhysicalTableName ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3';

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IVL.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].Vehicle_Link IVL ON IVL.Unique_ID = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Vehicle_Link'
	AND IVL.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IVSL.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[VW_StagingLinks] IVSL ON IVSL.[IBase8LinkID] = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Link_End'
	AND IVSL.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MV2P.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].Vehicle2Person MV2P ON MV2P.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Vehicle2Person'

	UPDATE CT
	SET MDA_IBase_Unique_ID = MV2O.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].Vehicle2Organisation MV2O ON MV2O.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Vehicle2Organisation'

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4';

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				AND PhysicalTableName != 'Link_End'
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5';

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1
	
	--2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
	SET @Step ='2.6';
	UPDATE #CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT	RowID
				FROM	(
						SELECT   RowID
								,ROW_NUMBER() OVER (PARTITION BY MDA_IBase_Unique_ID ORDER BY RowID) RowNumber
						FROM #CT CT 
						WHERE EXISTS (SELECT TOP 1 1 FROM #CT CT1 WHERE MDA_IBase_Unique_ID = MDA_IBase_Unique_ID AND ChangeDatabase = 'MDA_IBase' GROUP BY MDA_IBase_Unique_ID HAVING COUNT(1) > 1)
						) Data 
				WHERE RowNumber !=1
				) IQ ON IQ.RowID = CT.RowID
	WHERE ChangeDatabase = 'MDA_IBase'

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1';

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM MDA_IBase.[dbo].[_Vehicle_Link_NextID]
		UPDATE MDA_IBase.dbo.[_Vehicle_Link_NextID] SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	 @Prefix + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY RowID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--------------------------------------------------------------------------
		--3.2)MDAIBase: LinkTablesChanges
		--------------------------------------------------------------------------
		--3.2.1)ActionTheLinkChangeInMDA_IBase
		SET @Step ='3.2.1';

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[Vehicle_Link] [Target]
		USING  (
				SELECT CT.PhysicalTableName, CT.MDA_ID, CT.MDA_IBase_Unique_ID, IQ.*
				FROM #CT CT
				INNER JOIN	(
							SELECT RowID, MV2P.ID, [IBaseId] [Unique_ID], CreatedDate [Create_Date], CreatedBy [Create_User], [CrossHireCompany] [CrossHire_Company], [HireCompany] [Hire_Company], [HireEndDate] [Hire_End_Date], [HireStartDate] [Hire_Start_Date], ModifiedDate [Last_Upd_Date], ModifiedBy [Last_Upd_User], MVLT.[LinkText] [Link_Type], MV2P.RecordStatus [Record_Status], [RegKeeperEndDate] [Reg_Keeper_End_Date], [RegKeeperStartDate] [Reg_Keeper_Start_Date], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], [VehicleLinkId] [Vehicle_Link_ID],FiveGrading  [x5x5x5_Grading_412284402], RiskClaim_Id [Risk_Claim_ID_414244883], [VehicleLinkType_Id]
							FROM [MDA].[dbo].Vehicle2Person MV2P
							INNER JOIN MDA.[dbo].[VehicleLinkType] MVLT ON MVLT.ID = [VehicleLinkType_Id]
							INNER JOIN #CT CT ON CT.MDA_ID = MV2P.ID
							WHERE CT.PhysicalTableName = 'Vehicle2Person'
							UNION ALL
							SELECT RowID, MV2O.ID, [IBaseId] [Unique_ID], CreatedDate [Create_Date], CreatedBy [Create_User], [CrossHireCompany] [CrossHire_Company], [HireCompany] [Hire_Company], [HireEndDate] [Hire_End_Date], [HireStartDate] [Hire_Start_Date], ModifiedDate [Last_Upd_Date], ModifiedBy [Last_Upd_User], MVLT.[LinkText] [Link_Type], MV2O.RecordStatus [Record_Status], [RegKeeperEndDate] [Reg_Keeper_End_Date], [RegKeeperStartDate] [Reg_Keeper_Start_Date], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], [VehicleLinkId] [Vehicle_Link_ID],FiveGrading  [x5x5x5_Grading_412284402], RiskClaim_Id [Risk_Claim_ID_414244883], [VehicleLinkType_Id]
							FROM [MDA].[dbo].Vehicle2Organisation MV2O
							INNER JOIN MDA.[dbo].[VehicleLinkType] MVLT ON MVLT.ID = [VehicleLinkType_Id]
							INNER JOIN #CT CT ON CT.MDA_ID = MV2O.ID
							WHERE CT.PhysicalTableName = 'Vehicle2Organisation'
							AND (MVLT.LinkText NOT IN ('Stored at','Recovered by','Recovered to','Repairer','Inspected By') OR LEFT(MV2O.[SourceDescription],3) != 'ADA')
							) IQ ON IQ.RowID = CT.RowID
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET	 Create_Date					= Source.Create_Date
			,Create_User					= Source.Create_User
			,CrossHire_Company				= Source.CrossHire_Company
			,Hire_Company					= Source.Hire_Company
			,Hire_End_Date					= Source.Hire_End_Date
			,Hire_Start_Date				= Source.Hire_Start_Date
			,Last_Upd_Date					= Source.Last_Upd_Date
			,Last_Upd_User					= Source.Last_Upd_User
			,Link_Type						= CASE WHEN Source.[VehicleLinkType_Id] = 0 AND ISNULL(Source.Source_Description_411765489,'') !='ADA' THEN [Target].[Link_Type] ELSE NULLIF(Source.[Link_Type],'Unknown') END
			,Record_Status					= Source.Record_Status
			,Reg_Keeper_End_Date			= Source.Reg_Keeper_End_Date
			,Reg_Keeper_Start_Date			= Source.Reg_Keeper_Start_Date
			,Source_411765484				= Source.Source_411765484
			,Source_Description_411765489	= Source.Source_Description_411765489
			,Source_Reference_411765487		= Source.Source_Reference_411765487
			,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
			,Risk_Claim_ID_414244883		= Source.Risk_Claim_ID_414244883
		WHEN NOT MATCHED THEN INSERT
			(
			 Unique_ID
			,Create_Date
			,Create_User
			,CrossHire_Company
			,Do_Not_Disseminate_412284494
			,Hire_Company
			,Hire_End_Date
			,Hire_Start_Date
			,Last_Upd_Date
			,Last_Upd_User
			,Link_Type
			,[MDA_Incident_ID_412284502]
			,Record_Status
			,Reg_Keeper_End_Date
			,Reg_Keeper_Start_Date
			,Source_411765484
			,Source_Description_411765489
			,Source_Reference_411765487
			,Vehicle_Link_ID
			,x5x5x5_Grading_412284402
			,Risk_Claim_ID_414244883
			)
		VALUES
			(
			 Source.MDA_IBase_Unique_ID
			,Source.Create_Date
			,Source.Create_User
			,Source.CrossHire_Company
			,0
			,Source.Hire_Company
			,Source.Hire_End_Date
			,Source.Hire_Start_Date
			,Source.Last_Upd_Date
			,Source.Last_Upd_User
			,CASE WHEN Source.[VehicleLinkType_Id] = 0 THEN NULL ELSE Source.[Link_Type] END
			,CASE WHEN Source.PhysicalTableName= 'Vehicle2Person' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Vehicle2Person') WHEN Source.PhysicalTableName= 'Vehicle2Organisation' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Vehicle2Organisation') ELSE NULL END
			,Source.Record_Status
			,Source.Reg_Keeper_End_Date
			,Source.Reg_Keeper_Start_Date
			,Source.Source_411765484
			,Source.Source_Description_411765489
			,Source.Source_Reference_411765487
			,Source.Vehicle_Link_ID
			,Source.x5x5x5_Grading_412284402
			,Source.Risk_Claim_ID_414244883
			)
		OUTPUT  @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + ISNULL(Deleted.CrossHire_Company,'') + '}|I:{' + ISNULL(Inserted.CrossHire_Company,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Hire_Company,'') + '}|I:{' + ISNULL(Inserted.Hire_Company,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Hire_End_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Hire_End_Date,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Hire_Start_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Hire_Start_Date,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Link_Type,'') + '}|I:{' + ISNULL(Inserted.Link_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reg_Keeper_End_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reg_Keeper_End_Date,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reg_Keeper_Start_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reg_Keeper_Start_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.Vehicle_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Vehicle_Link_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Vehicle_Link] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, CrossHire_Company, Do_Not_Disseminate_412284494, Hire_Company, Hire_End_Date, Hire_Start_Date, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Link_Type, MDA_Incident_ID_412284502, Record_Status, Reg_Keeper_End_Date, Reg_Keeper_Start_Date, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Vehicle_Link_ID, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

		--3.2.2)DoDeletes
		--WeNeedToDeleteTheLinkEndrecordBeforetheDeletesOrWeWontBeAbleTo
		SET @Step ='3.2.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [MDA_IBase].[dbo].[_LinkEnd] LE
		INNER JOIN (
					SELECT [IBase8LinkID] Link_ID
					FROM	(
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Vehicle2Person') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Vehicle2Person'
							AND DMLAction = 'D'
							UNION ALL
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Vehicle2Organisation') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Vehicle2Organisation'
							AND DMLAction = 'D'
							) DATA
					INNER JOIN [MDA_IBase].[dbo].[VW_StagingLinks] IVSL ON IVSL.[MDA_Incident_ID_412284502] = DATA.MDA_Incident_ID_412284502
					) IQ ON IQ.Link_ID = LE.Link_ID;

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[Vehicle_Link]
		OUTPUT	 @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.CrossHire_Company,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Hire_Company,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Hire_End_Date,''),126) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Hire_Start_Date,''),126) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Link_Type,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reg_Keeper_End_Date,''),126) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reg_Keeper_Start_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Vehicle_Link_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Vehicle_Link] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, CrossHire_Company, Do_Not_Disseminate_412284494, Hire_Company, Hire_End_Date, Hire_Start_Date, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Link_Type, MDA_Incident_ID_412284502, Record_Status, Reg_Keeper_End_Date, Reg_Keeper_Start_Date, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Vehicle_Link_ID, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Vehicle_Link] IVL
		INNER JOIN #CT CT ON CASE WHEN CT.PhysicalTableName = 'Vehicle2Person' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID, 'MDA.dbo.Vehicle2Person') WHEN CT.PhysicalTableName= 'Vehicle2Organisation' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Vehicle2Organisation') ELSE NULL END = IVL.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';

		--3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		SET @Step ='3.2.3';
		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MV2P
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Vehicle_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Vehicle_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleLinkId,'') + '}|I:{' + ISNULL(Inserted.VehicleLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleLinkType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleLinkType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.HireCompany,'') + '}|I:{' + ISNULL(Inserted.HireCompany,'') + '}'
				,'D:{' + ISNULL(Deleted.CrossHireCompany,'') + '}|I:{' + ISNULL(Inserted.CrossHireCompany,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Vehicle2Person] (TaskID, DMLAction, Id, Vehicle_Id, Person_Id, VehicleLinkId, VehicleLinkType_Id, RegKeeperStartDate, RegKeeperEndDate, HireCompany, CrossHireCompany, HireStartDate, HireEndDate, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM MDA.dbo.Vehicle2Person MV2P
		INNER JOIN #CT CT ON CT.MDA_ID = MV2P.Id
 		WHERE (MV2P.IBaseId IS NULL
		OR MV2P.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Vehicle2Person';

		SET @Step ='3.2.3';
		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MV2O
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Vehicle_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Vehicle_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleLinkId,'') + '}|I:{' + ISNULL(Inserted.VehicleLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleLinkType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleLinkType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.HireCompany,'') + '}|I:{' + ISNULL(Inserted.HireCompany,'') + '}'
				,'D:{' + ISNULL(Deleted.CrossHireCompany,'') + '}|I:{' + ISNULL(Inserted.CrossHireCompany,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Vehicle2Organisation] (TaskID, DMLAction, Id, Vehicle_Id, Organisation_Id, VehicleLinkId, VehicleLinkType_Id, RegKeeperStartDate, RegKeeperEndDate, HireCompany, CrossHireCompany, HireStartDate, HireEndDate, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM MDA.dbo.Vehicle2Organisation MV2O
		INNER JOIN #CT CT ON CT.MDA_ID = MV2O.Id
 		WHERE (MV2O.IBaseId IS NULL
		OR MV2O.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Vehicle2Organisation';

		--------------------------------------------------------------------------
		--3.3)MDAIBase: LinkEndTablesChanges
		-------------------------------------------------------------------------
		--3.3.1)PopulateTheWorkingTable
		--DoForEntityID1
		--CheckTheValuesThatArebeingAdded
		SET @Step ='3.3.1';

		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC
		FROM	(
				SELECT MV2P.IBaseId Link_ID, MV.IBaseId Entity_ID1, MV2P.LinkConfidence Confidence, 0 Direction, P.IBaseId Entity_ID2, 2321 EntityType_ID1, 1969 EntityType_ID2, @LinkTypeID LinkType_ID, MV2P.RecordStatus Record_Status, 1 Record_Type, NULL SCC
				FROM MDA.dbo.Vehicle2Person MV2P 
				INNER JOIN MDA.dbo.Vehicle MV ON MV.ID = MV2P.Vehicle_Id
				INNER JOIN MDA.dbo.[Person] P ON P.ID = MV2P.Person_ID
				INNER JOIN #CT CT ON CT.MDA_ID = MV2P.ID AND CT.PhysicalTableName = 'Vehicle2Person'
				UNION ALL
				SELECT MV2O.IBaseId, MV.IBaseId, MV2O.LinkConfidence, 0, O.IBaseId, 2321, 1893, @LinkTypeID, MV2O.RecordStatus, 1, NULL
				FROM MDA.dbo.Vehicle2Organisation MV2O
				INNER JOIN MDA.dbo.Vehicle MV ON MV.ID = MV2O.Vehicle_Id
				INNER JOIN MDA.dbo.[Organisation] O ON O.ID = MV2O.Organisation_Id
				INNER JOIN #CT CT ON CT.MDA_ID = MV2O.ID AND CT.PhysicalTableName = 'Vehicle2Organisation'
				INNER JOIN MDA.[dbo].[VehicleLinkType] MVLT ON MVLT.ID = MV2O.[VehicleLinkType_Id]
				WHERE (MVLT.LinkText NOT IN ('Stored at','Recovered by','Recovered to','Repairer','Inspected By') OR LEFT(MV2O.[SourceDescription],3) != 'ADA')
				) Data

		--NowDoForEntityID2
		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT Link_ID, Entity_ID2, Confidence, Direction, Entity_ID1, EntityType_ID2, EntityType_ID1, LinkType_ID, Record_Status, 2, SCC
		FROM #LinkEndStaging;

		CREATE CLUSTERED INDEX IX_LinkEndStaging_Link_ID_Entity_ID1 ON #LinkEndStaging (Link_ID, Entity_ID1);

		--3.3.2)ActionTheLinkEndChangeInMDA_IBase
		SET @Step ='3.3.2';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[_LinkEnd] [Target]
		USING	(
				SELECT *
				FROM #LinkEndStaging
				) Source ON Source.Link_ID = [Target].Link_ID AND Source.Entity_ID1 = [Target].Entity_ID1
		WHEN MATCHED AND CHECKSUM(Source.Link_ID, Source.Entity_ID1, Source.Confidence, Source.Direction, Source.Entity_ID2, Source.EntityType_ID1, Source.EntityType_ID2, Source.LinkType_ID, Source.Record_Status, Source.Record_Type, Source.SCC) != CHECKSUM([Target].Link_ID, [Target].Entity_ID1, [Target].Confidence, [Target].Direction, [Target].Entity_ID2, [Target].EntityType_ID1, [Target].EntityType_ID2, [Target].LinkType_ID, [Target].Record_Status, [Target].Record_Type, [Target].SCC) THEN UPDATE
		SET  Confidence			= Source.Confidence
			,Direction			= Source.Direction
			,Entity_ID2			= Source.Entity_ID2
			,EntityType_ID1		= Source.EntityType_ID1
			,EntityType_ID2		= Source.EntityType_ID2
			,LinkType_ID		= Source.LinkType_ID
			,Record_Status		= Source.Record_Status
			,Record_Type		= Source.Record_Type
			,SCC				= Source.SCC
		WHEN NOT MATCHED THEN INSERT
			(
			 Link_ID
			,Entity_ID1
			,Confidence
			,Direction
			,Entity_ID2
			,EntityType_ID1
			,EntityType_ID2
			,LinkType_ID
			,Record_Status
			,Record_Type
			,SCC
			)
		VALUES
			(
			 Source.Link_ID
			,Source.Entity_ID1
			,Source.Confidence
			,Source.Direction
			,Source.Entity_ID2
			,Source.EntityType_ID1
			,Source.EntityType_ID2
			,Source.LinkType_ID
			,Source.Record_Status
			,Source.Record_Type
			,Source.SCC
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Link_ID],'') + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID1],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Confidence],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Direction],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID2],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID1],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID2],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkType_ID],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Type],'')) + '}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{' + ISNULL(Inserted.[SCC],'') + '}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC);

		--3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
		SET @Step ='3.3.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		DELETE  [MDA_IBase].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [MDA_IBase].[dbo].[_LinkEnd] LE
		WHERE EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID)
		AND NOT EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID AND LES.Entity_ID1 = LE.Entity_ID1);

	END

	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--Vehicle2Person
		SET @Step ='4.1';

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].Vehicle2Person [Target]
		USING	(
				SELECT	 CT.MDA_ID
						,CT.MDA_IBase_Unique_ID
						,[Sync].[fn_ExtractMDAID] (V.MDA_Incident_ID_412284502) [Vehicle_Id]
						,[Sync].[fn_ExtractMDAID] (P.MDA_Incident_ID_412284502) [Person_Id]
						,VL.[Vehicle_Link_ID] [VehicleLinkId]
						,ISNULL(MVLT.ID,0) [VehicleLinkType_Id]
						,VL.[Reg_Keeper_Start_Date] [RegKeeperStartDate]
						,VL.[Reg_Keeper_End_Date] [RegKeeperEndDate]
						,VL.[Hire_Company] [HireCompany]
						,VL.[CrossHire_Company] [CrossHireCompany]
						,VL.[IconColour] [HireStartDate]
						,VL.[Hire_End_Date] [HireEndDate]
						,VL.[x5x5x5_Grading_412284402] [FiveGrading]
						,ILE.Confidence [LinkConfidence]
						,VL.[Source_411765484] [Source]
						,VL.[Source_Reference_411765487] [SourceReference]
						,VL.[Source_Description_411765489] [SourceDescription]
						,VL.Create_User [CreatedBy]
						,VL.Create_Date [CreatedDate]
						,VL.Last_Upd_User [ModifiedBy]
						,VL.Last_Upd_Date [ModifiedDate]
						,VL.[Risk_Claim_ID_414244883] RiskClaim_Id
						,VL.[Risk_Claim_ID_414244883] BaseRiskClaim_ID
						,VL.Unique_ID [IBaseId]
						,VL.Record_Status [RecordStatus]		
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Vehicle_Link] VL ON VL.Unique_ID = CT.MDA_IBase_Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = VL.Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[Vehicle_] V ON V.Unique_ID = ILE.Entity_ID1
				INNER JOIN [MDA_IBase].[dbo].[Person_] P ON P.Unique_ID = ILE.Entity_ID2
				LEFT JOIN MDA.[dbo].[VehicleLinkType] MVLT ON MVLT.[LinkText] = VL.[Link_Type]
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  Vehicle_Id					= Source.Vehicle_Id
			,Person_Id					= Source.Person_Id
			,VehicleLinkId				= Source.VehicleLinkId
			,VehicleLinkType_Id			= Source.VehicleLinkType_Id
			,RegKeeperStartDate			= Source.RegKeeperStartDate
			,RegKeeperEndDate			= Source.RegKeeperEndDate
			,HireCompany				= Source.HireCompany
			,CrossHireCompany			= Source.CrossHireCompany
			,HireStartDate				= Source.HireStartDate
			,HireEndDate				= Source.HireEndDate
			,FiveGrading				= Source.FiveGrading
			,LinkConfidence				= Source.LinkConfidence
			,Source						= Source.Source
			,SourceReference			= Source.SourceReference
			,SourceDescription			= Source.SourceDescription
			,CreatedBy					= Source.CreatedBy
			,CreatedDate				= Source.CreatedDate
			,ModifiedBy					= Source.ModifiedBy
			,ModifiedDate				= Source.ModifiedDate
			,IBaseId					= Source.IBaseId
			,RecordStatus				= Source.RecordStatus
			,ADARecordStatus			= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
			 Vehicle_Id
			,Person_Id
			,VehicleLinkId
			,VehicleLinkType_Id
			,RegKeeperStartDate
			,RegKeeperEndDate
			,HireCompany
			,CrossHireCompany
			,HireStartDate
			,HireEndDate
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES
			(
			 Source.Vehicle_Id
			,Source.Person_Id
			,Source.VehicleLinkId
			,Source.VehicleLinkType_Id
			,Source.RegKeeperStartDate
			,Source.RegKeeperEndDate
			,Source.HireCompany
			,Source.CrossHireCompany
			,Source.HireStartDate
			,Source.HireEndDate
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10
			)
		OUTPUT	 @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Vehicle_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Vehicle_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleLinkId,'') + '}|I:{' + ISNULL(Inserted.VehicleLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleLinkType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleLinkType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.HireCompany,'') + '}|I:{' + ISNULL(Inserted.HireCompany,'') + '}'
				,'D:{' + ISNULL(Deleted.CrossHireCompany,'') + '}|I:{' + ISNULL(Inserted.CrossHireCompany,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Vehicle2Person] (TaskID, DMLAction, Id, Vehicle_Id, Person_Id, VehicleLinkId, VehicleLinkType_Id, RegKeeperStartDate, RegKeeperEndDate, HireCompany, CrossHireCompany, HireStartDate, HireEndDate, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus);

		--Vehicle2Organisation
		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].Vehicle2Organisation [Target]
		USING	(
				SELECT	 CT.MDA_ID
						,CT.MDA_IBase_Unique_ID
						,[Sync].[fn_ExtractMDAID] (V.MDA_Incident_ID_412284502) [Vehicle_Id]
						,[Sync].[fn_ExtractMDAID] (O.MDA_Incident_ID_412284502) [Organisation_Id]
						,VL.[Vehicle_Link_ID] [VehicleLinkId]
						,ISNULL(MVLT.ID,0) [VehicleLinkType_Id]
						,VL.[Reg_Keeper_Start_Date] [RegKeeperStartDate]
						,VL.[Reg_Keeper_End_Date] [RegKeeperEndDate]
						,VL.[Hire_Company] [HireCompany]
						,VL.[CrossHire_Company] [CrossHireCompany]
						,VL.[IconColour] [HireStartDate]
						,VL.[Hire_End_Date] [HireEndDate]
						,VL.[x5x5x5_Grading_412284402] [FiveGrading]
						,ILE.Confidence [LinkConfidence]
						,VL.[Source_411765484] [Source]
						,VL.[Source_Reference_411765487] [SourceReference]
						,VL.[Source_Description_411765489] [SourceDescription]
						,VL.Create_User [CreatedBy]
						,VL.Create_Date [CreatedDate]
						,VL.Last_Upd_User [ModifiedBy]
						,VL.Last_Upd_Date [ModifiedDate]
						,VL.[Risk_Claim_ID_414244883] RiskClaim_Id
						,VL.[Risk_Claim_ID_414244883] BaseRiskClaim_ID
						,VL.Unique_ID [IBaseId]
						,VL.Record_Status [RecordStatus]		
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Vehicle_Link] VL ON VL.Unique_ID = CT.MDA_IBase_Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = VL.Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[Vehicle_] V ON V.Unique_ID = ILE.Entity_ID1
				INNER JOIN [MDA_IBase].[dbo].[Organisation_] O ON O.Unique_ID = ILE.Entity_ID2
				LEFT JOIN MDA.[dbo].[VehicleLinkType] MVLT ON MVLT.[LinkText] = VL.[Link_Type]
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  Vehicle_Id					= Source.Vehicle_Id
			,[Organisation_Id]			= Source.[Organisation_Id]
			,VehicleLinkId				= Source.VehicleLinkId
			,VehicleLinkType_Id			= Source.VehicleLinkType_Id
			,RegKeeperStartDate			= Source.RegKeeperStartDate
			,RegKeeperEndDate			= Source.RegKeeperEndDate
			,HireCompany				= Source.HireCompany
			,CrossHireCompany			= Source.CrossHireCompany
			,HireStartDate				= Source.HireStartDate
			,HireEndDate				= Source.HireEndDate
			,FiveGrading				= Source.FiveGrading
			,LinkConfidence				= Source.LinkConfidence
			,Source						= Source.Source
			,SourceReference			= Source.SourceReference
			,SourceDescription			= Source.SourceDescription
			,CreatedBy					= Source.CreatedBy
			,CreatedDate				= Source.CreatedDate
			,ModifiedBy					= Source.ModifiedBy
			,ModifiedDate				= Source.ModifiedDate
			,IBaseId					= Source.IBaseId
			,RecordStatus				= Source.RecordStatus
			,ADARecordStatus			= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
			 Vehicle_Id
			,[Organisation_Id]
			,VehicleLinkId
			,VehicleLinkType_Id
			,RegKeeperStartDate
			,RegKeeperEndDate
			,HireCompany
			,CrossHireCompany
			,HireStartDate
			,HireEndDate
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES
			(
			 Source.Vehicle_Id
			,Source.[Organisation_Id]
			,Source.VehicleLinkId
			,Source.VehicleLinkType_Id
			,Source.RegKeeperStartDate
			,Source.RegKeeperEndDate
			,Source.HireCompany
			,Source.CrossHireCompany
			,Source.HireStartDate
			,Source.HireEndDate
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10
			)
		OUTPUT	 @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Vehicle_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Vehicle_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleLinkId,'') + '}|I:{' + ISNULL(Inserted.VehicleLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleLinkType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleLinkType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.HireCompany,'') + '}|I:{' + ISNULL(Inserted.HireCompany,'') + '}'
				,'D:{' + ISNULL(Deleted.CrossHireCompany,'') + '}|I:{' + ISNULL(Inserted.CrossHireCompany,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Vehicle2Organisation] (TaskID, DMLAction, Id, Vehicle_Id, Organisation_Id, VehicleLinkId, VehicleLinkType_Id, RegKeeperStartDate, RegKeeperEndDate, HireCompany, CrossHireCompany, HireStartDate, HireEndDate, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus);

		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--Vehicle2Person
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].Vehicle2Person
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Vehicle_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Vehicle_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleLinkId,'') + '}|I:{' + ISNULL(Inserted.VehicleLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleLinkType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleLinkType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.HireCompany,'') + '}|I:{' + ISNULL(Inserted.HireCompany,'') + '}'
				,'D:{' + ISNULL(Deleted.CrossHireCompany,'') + '}|I:{' + ISNULL(Inserted.CrossHireCompany,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Vehicle2Person] (TaskID, DMLAction, Id, Vehicle_Id, Person_Id, VehicleLinkId, VehicleLinkType_Id, RegKeeperStartDate, RegKeeperEndDate, HireCompany, CrossHireCompany, HireStartDate, HireEndDate, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].Vehicle2Person MV2P
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MV2P.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--Vehicle2Organisation
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].Vehicle2Organisation
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Vehicle_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Vehicle_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleLinkId,'') + '}|I:{' + ISNULL(Inserted.VehicleLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleLinkType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleLinkType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RegKeeperEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RegKeeperEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.HireCompany,'') + '}|I:{' + ISNULL(Inserted.HireCompany,'') + '}'
				,'D:{' + ISNULL(Deleted.CrossHireCompany,'') + '}|I:{' + ISNULL(Inserted.CrossHireCompany,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireStartDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireStartDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.HireEndDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.HireEndDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Vehicle2Organisation] (TaskID, DMLAction, Id, Vehicle_Id, Organisation_Id, VehicleLinkId, VehicleLinkType_Id, RegKeeperStartDate, RegKeeperEndDate, HireCompany, CrossHireCompany, HireStartDate, HireEndDate, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].Vehicle2Organisation MV2E
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MV2E.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA_IBase].[dbo].[Vehicle_Link]
		SET  [MDA_Incident_ID_412284502] = IQ.MDA_Incident_ID_412284502
			,[Risk_Claim_ID_414244883]	 = IQ.RiskClaim_Id
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + ISNULL(Deleted.CrossHire_Company,'') + '}|I:{' + ISNULL(Inserted.CrossHire_Company,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Hire_Company,'') + '}|I:{' + ISNULL(Inserted.Hire_Company,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Hire_End_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Hire_End_Date,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Hire_Start_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Hire_Start_Date,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Link_Type,'') + '}|I:{' + ISNULL(Inserted.Link_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reg_Keeper_End_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reg_Keeper_End_Date,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Reg_Keeper_Start_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Reg_Keeper_Start_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.Vehicle_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Vehicle_Link_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Vehicle_Link] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, CrossHire_Company, Do_Not_Disseminate_412284494, Hire_Company, Hire_End_Date, Hire_Start_Date, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Link_Type, MDA_Incident_ID_412284502, Record_Status, Reg_Keeper_End_Date, Reg_Keeper_Start_Date, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Vehicle_Link_ID, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Vehicle_Link] IVL
		INNER JOIN (
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MV2P.ID,'MDA.dbo.Vehicle2Person') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [MDA].[dbo].Vehicle2Person MV2P
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MV2P.IBaseId
					UNION ALL
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MV2E.ID,'MDA.dbo.Vehicle2Organisation') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [MDA].[dbo].Vehicle2Organisation MV2E
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MV2E.IBaseId
					) IQ ON IQ.[Unique_ID] =  IVL.[Unique_ID]
		WHERE IVL.MDA_Incident_ID_412284502 IS NULL
		OR IVL.MDA_Incident_ID_412284502 != IQ.MDA_Incident_ID_412284502;

	END

	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle2Person] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle2Person] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle2Person] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle2Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle2Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle2Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Vehicle_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Vehicle_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Vehicle_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging

END TRY
BEGIN CATCH
	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(500)	= ISNULL(ERROR_PROCEDURE(),@@PROCID)
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +ISNULL(@Step,'?') +']'
	
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

