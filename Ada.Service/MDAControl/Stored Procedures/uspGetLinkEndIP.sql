﻿

CREATE PROCEDURE [IBaseM].[uspGetLinkEndIP]
AS
SET NOCOUNT ON

BEGIN TRY

	SELECT     C.Unique_ID,  B.Unique_ID Entity_ID1, A.Confidence, A.Direction, A.Entity_ID2,  1739 EntityType_ID1, 1969 EntityType_ID2, 2652 LinkType_ID, A.Record_Status, A.Record_Type, A.SCC
	FROM         
		IBaseM5Cur._LinkEnd AS A 
	INNER JOIN
		IBaseM8Cur.IP_Address AS B 
		ON B.IP_Address_ID = A.Entity_ID1 
	INNER JOIN IBaseM8Cur.IP_Address_Link AS C 
		ON C.IP_Address_Link_ID = A.Link_ID
	WHERE LEFT(A.Entity_ID2, 3) = 'PER'
	UNION ALL
	SELECT   C.Unique_ID,  
	A.Entity_ID1, A.Confidence, A.Direction, B.Unique_ID  Entity_ID2, 1969 EntityType_ID1, 1739 EntityType_ID2, 2652 LinkType_ID, A.Record_Status, A.Record_Type, A.SCC
	FROM         
		IBaseM5Cur._LinkEnd AS A 
	INNER JOIN
		IBaseM8Cur.IP_Address AS B 
		ON B.IP_Address_ID = A.Entity_ID2 
	INNER JOIN IBaseM8Cur.IP_Address_Link AS C 
		ON C.IP_Address_Link_ID = A.Link_ID
	WHERE LEFT(A.Entity_ID1, 3) = 'PER'
	UNION ALL
	SELECT     C.Unique_ID,  B.Unique_ID Entity_ID1, A.Confidence, A.Direction, D.Unique_ID  Entity_ID2,  1005 EntityType_ID1, 1969 EntityType_ID2, 2652 LinkType_ID, A.Record_Status, A.Record_Type, A.SCC
	FROM         
		IBaseM5Cur._LinkEnd AS A 
	INNER JOIN
		IBaseM8Cur.IP_Address AS B 
		ON B.IP_Address_ID = A.Entity_ID1 
	INNER JOIN IBaseM8Cur.IP_Address_Link AS C 
		ON C.IP_Address_Link_ID = A.Link_ID
	INNER JOIN IBaseM8Cur.Organisation_ AS D 
		ON D.Organisation_ID = A.Entity_ID2
	UNION ALL
	SELECT   C.Unique_ID,  
	D.Unique_ID Entity_ID1, A.Confidence, A.Direction, B.Unique_ID  Entity_ID2, 1969 EntityType_ID1, 1005 EntityType_ID2, 2652 LinkType_ID, A.Record_Status, A.Record_Type, A.SCC
	FROM         
		IBaseM5Cur._LinkEnd AS A 
	INNER JOIN
		IBaseM8Cur.IP_Address AS B 
		ON B.IP_Address_ID = A.Entity_ID2 
	INNER JOIN IBaseM8Cur.IP_Address_Link AS C 
		ON C.IP_Address_Link_ID = A.Link_ID
	INNER JOIN IBaseM8Cur.Organisation_ AS D 
		ON D.Organisation_ID = A.Entity_ID1

END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH


