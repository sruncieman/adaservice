﻿CREATE PROCEDURE dbo.uspPurgeData (@DaysToKeep SMALLINT = 90) 
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	DECLARE  @Now				DATETIME 
			,@Details			VARCHAR(1000)
			,@OutputRunID		INT
			,@DatabaseID		INT = DB_ID()
			,@ObjectID			INT = ISNULL(@@PROCID,-1)
			,@RunNameID			INT = 144 --PurgeData

	SELECT @Details = 'User=['+SYSTEM_USER+']'		
	EXECUTE dbo.uspCRUDDBControlRun
				 @RunNameID		= @RunNameID
				,@Details		= @Details
				,@OutputRunID	= @OutputRunID OUTPUT

	DECLARE @SQLStatements TABLE  (ID INT IDENTITY(1,1), SQLCommand NVARCHAR(MAX))
	DECLARE  @StatementsToProcess	INT
			,@Counter				INT = 1
			,@SQLStatement			NVARCHAR(MAX)
			,@RiskBatch_Id			INT
			,@RiskOriginalFile_Id	INT

	IF OBJECT_ID('tempdb..#BatchesToDelete') IS NOT NULL
		DROP TABLE #BatchesToDelete
	CREATE TABLE #BatchesToDelete (RowId INT IDENTITY(1,1), RiskBatch_Id INT NOT NULL, RiskOriginalFile_Id INT NOT NULL)

	------------------------------------------------------------
	--2)Body
	------------------------------------------------------------
	--GetAllTheBatchesToPurge
	INSERT INTO #BatchesToDelete (RiskBatch_Id, RiskOriginalFile_Id)
	SELECT Id RiskBatch_Id, [RiskOriginalFile_Id]
	FROM [MDA].[dbo].[RiskBatch] RB
	WHERE [CreatedDate] <= DATEADD(DAY,-@DaysToKeep,GETDATE())
	AND NULLIF([RiskOriginalFile_Id],0) IS NOT NULL
	AND NOT EXISTS (SELECT TOP 1 1 FROM [dbo].[DBControlPurgeHistory] PH WHERE PH.[RiskBatch_Id] = RB.Id)
	SELECT @StatementsToProcess = @@ROWCOUNT

	WHILE @Counter <= @StatementsToProcess
	BEGIN
		SELECT @RiskBatch_Id = RiskBatch_Id, @RiskOriginalFile_Id = RiskOriginalFile_Id
		FROM #BatchesToDelete
		WHERE RowId = @Counter

		BEGIN TRANSACTION
			UPDATE [MDA].[dbo].[RiskOriginalFile]
			SET [FileData] = CONVERT(VARBINARY(MAX), '')
			WHERE [PkId] = @RiskOriginalFile_Id

			UPDATE [MDA].[dbo].[RiskClaim]
			SET  [SourceEntityXML] = ''
			WHERE [RiskBatch_Id] = @RiskBatch_Id

			UPDATE RCR
			SET [ScoreEntityXml] = NULL
			FROM [MDA].[dbo].[RiskClaimRun] RCR
			WHERE EXISTS (SELECT TOP 1 1 FROM [MDA].[dbo].[RiskClaim] RC WHERE RC.Id = RCR.RiskClaim_Id AND RC.RiskBatch_Id = @RiskBatch_Id)

			INSERT INTO [dbo].[DBControlPurgeHistory] (RunID, RiskBatch_Id)
			VALUES (@OutputRunID, @RiskBatch_Id)

		COMMIT TRANSACTION
		SELECT @Counter += 1
	END

	---------------------------------------
	--UpdateOutputAndLogging
	---------------------------------------
	SELECT @Now =  GETDATE()
	EXECUTE [dbo].[uspCRUDDBControlRun] 
				 @Action ='U'
				,@RunID =  @OutputRunID
				,@RunEndTime = @Now
				,@OutputRunID = @OutputRunID OUTPUT

END TRY
BEGIN CATCH
	--RaiseTheerror
	DECLARE  @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()		

	--UpdateOutputAndLogging
	SELECT	 @Now =  GETDATE()

	EXECUTE  [dbo].[uspCRUDDBControlRun] 
					 @Action			='U'
					,@RunID				= @OutputRunID
					,@RunEndTime		= @Now
					,@Error				= @ErrorState
					,@ErrorDescription  = @ErrorMessage
					,@OutputRunID		= @OutputRunID OUTPUT

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'
END CATCH