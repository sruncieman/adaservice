﻿
CREATE PROCEDURE [dbo].[uspBackupDatabase]
(
 @BackupDatabaseName			VARCHAR(100)
,@BackupType					SMALLINT	 = 1 --0 Full and backup in new set, 1 Full, 2 Differential, 3 Transactional
,@SaveSetName					VARCHAR(100) = NULL
)
AS

--ValidateTheInputParameters
IF @BackupType NOT IN (1,2,3)
BEGIN
	RAISERROR ('TheBackupTypeCanOnlyBe1(Full),2(Differential)or3(Transactional):BackupFailed',18,255)
	RETURN
END

IF (SELECT DB_ID(@BackupDatabaseName)) IS NULL
BEGIN
	RAISERROR ('TheBackupDatabaseNameIsNotValid:BackupFailed',18,255)
	RETURN
END


DECLARE  @FolderLocation VARCHAR(200) = (SELECT 'D:\SQLServerR2\Backup\' + @BackupDatabaseName + '\' + REPLACE(CONVERT(VARCHAR, GETDATE(), 102),'.','-'))
		,@FileName VARCHAR(200) = (SELECT @BackupDatabaseName)
		,@ExecuteSQL NVARCHAR(MAX)

DECLARE @xp_fileexist_output TABLE (FILE_EXISTS INT NOT NULL, FILE_IS_DIRECTORY INT NOT NULL, PARENT_DIRECTORY_EXISTS INT NOT NULL)

--------------------------------------
--CreateFolderIfItDoesntExist
---------------------------------------
INSERT INTO @xp_fileexist_output
EXECUTE Master.dbo.xp_fileexist @FolderLocation

IF NOT EXISTS (SELECT TOP 1 1 FROM @xp_fileexist_output WHERE FILE_IS_DIRECTORY = 1 )
BEGIN
	EXECUTE Master.dbo.xp_create_subdir @FolderLocation
	SET @BackupType = 0 --IfWeNeedToCreateTheFolderWeNeedToForceAFullFormattedBackup
END

--------------------------------------
--PerformBackup
---------------------------------------
SELECT @BackupDatabaseName = '[' + @BackupDatabaseName + ']'

SELECT @ExecuteSQL = 
			'BACKUP' + CASE WHEN @BackupType !=3 THEN ' DATABASE ' ELSE ' LOG ' END + @BackupDatabaseName + ' TO DISK =N'+ '''' + @FolderLocation + '\' + @FileName + '.BAK''' + ' WITH' + CASE WHEN @BackupType = 0 THEN ' FORMAT,' ELSE ' NOFORMAT,' END + CASE WHEN @BackupType IN (0,1) THEN ' INIT,' ELSE ' NOINIT,' END + CASE WHEN @BackupType IN (2) THEN ' DIFFERENTIAL,' ELSE '' END +
			' NAME = N''' + ISNULL(@SaveSetName,@BackupDatabaseName) + ''', SKIP, NOREWIND, NOUNLOAD'
--PRINT @ExecuteSQL
EXECUTE SP_EXECUTESQL @ExecuteSQL












