﻿


CREATE PROCEDURE [IBaseM].[uspGetEntity]
(
	@Ent1 CHAR(3),
	@Ent2 CHAR(3),
	@ExcDeleted BIT = NULL
)
AS
SET NOCOUNT ON
BEGIN TRY

	SELECT LE.Entity_ID1_Keo Entity_ID1, LE.Link_ID, LE.Entity_ID1, LE.Entity_ID2, N.Description_ AS NL_Type, CL.Description_ AS CL_Description, ICL.Description_ AS Ins_Description, Reference_Link_Record_Status
	FROM IBaseM.v_LinkEnd_Keo2 LE  WITH (NOLOCK)
	LEFT JOIN IBaseM5Cur.Nominal_Link N  WITH (NOLOCK)
	ON LE.Link_ID = N.Unique_ID
	LEFT JOIN  IBaseM5Cur.Claim_ CL
	ON LE.Link_ID  = CL.Unique_ID
	LEFT JOIN  IBaseM5Cur.Reference_Link ICL
	ON LE.Link_ID  = ICL.Unique_ID
	WHERE 
	(LE.Entity_ID1_Record_Status !=  @ExcDeleted * 254  OR @ExcDeleted IS NULL)
	AND (LE.Entity_ID2_Record_Status !=  @ExcDeleted * 254  OR @ExcDeleted IS NULL)
	AND (LE.Record_Status != @ExcDeleted * 254 OR @ExcDeleted IS NULL)
	AND LEFT(LE.Entity_ID1, 3) = @Ent1
	AND LEFT(LE.Entity_ID2, 3) = @Ent2

END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

