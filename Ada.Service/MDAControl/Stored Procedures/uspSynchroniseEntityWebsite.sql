﻿

CREATE PROCEDURE   [Sync].[uspSynchroniseEntityWebsite]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseEntityWebsite]
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)ActionTheChangeInMDA_IBase
--						3.3)DoDeletes
--						3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)	
--						4.1)ActionTheChangeInMDA
--						4.2)DoDeletes
--						4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					5)UpdateLoggingAndTidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-05-07			Paul Allen		Created
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY

	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT			

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1), ChangeDatabase VARCHAR(10) ,MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)
					
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 108 
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT


	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, MDA_ID, DMLAction)
	SELECT 'MDA', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA)].[dbo].website, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA_IBase)].[dbo].Website_, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	--2.2)AddSomeIndexesToMakeTheProcessQuicker
	SET @Step ='2.2'
	 
	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT(MDA_IBase_Unique_ID ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3'

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IA.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [$(MDA_IBase)].[dbo].[Website_] IA ON IA.Unique_ID = CT.MDA_IBase_Unique_ID
	AND IA.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MA.IBaseId
	FROM #CT CT
	INNER JOIN [$(MDA)].[dbo].[Website] MA ON MA.ID = CT.MDA_ID

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4'

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5'

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1


	-----------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	------------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1'

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM [$(MDA_IBase)].[dbo]._Website__NextID
		UPDATE [$(MDA_IBase)].[dbo]._Website__NextID SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	'WEB' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY MDA_ID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--3.2)ActionTheChangeInMDA_IBase
		SET @Step ='3.2'

		

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		
		
			


		MERGE [$(MDA_IBase)].[dbo].[Website_] [Target]
		USING	(
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, MADD.*
				FROM #CT CT
				INNER JOIN [$(MDA)].[dbo].[Website] MADD ON MADD.ID = CT.MDA_ID
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				) Source 
				ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET		
			 Website_						= source.URL
			,Key_Attractor_412284410		= source.KeyAttractor
			,Source_411765484				= source.[Source]
			,Source_Reference_411765487		= source.SourceReference
			,Source_Description_411765489	= source.SourceDescription
			,Create_Date					= source.CreatedDate
			,Create_User					= source.CreatedBy
			,Last_Upd_Date					= source.ModifiedDate
			,Last_Upd_User					= source.ModifiedBy
			,Record_Status					= source.RecordStatus
		WHEN NOT MATCHED THEN INSERT 
		(
			Unique_ID, 
			website_ID,
			MDA_Incident_ID_412284502,
			Website_,
			Create_User, 
			Create_Date,
			Do_Not_Disseminate_412284494,
			Key_Attractor_412284410,
			Last_Upd_Date,
			Last_Upd_User,
			Source_411765484,
			Source_Reference_411765487,
			Source_Description_411765489
		)
		VALUES
		(
			Source.MDA_IBase_Unique_ID
			,source.WebsiteID
			,[Sync].[fn_ResolveMDAID](source.Id,'mda.dbo.Website')
			,source.URL
			,Source.[CreatedBy]
			,Source.[CreatedDate]
			,0
			,Source.[KeyAttractor]
			,Source.[ModifiedDate]
			,Source.[ModifiedBy]
			,Source.[Source]
			,Source.[SourceDescription]
			,Source.[SourceReference]
		)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Website_ID,'') + '}|I:{' + ISNULL(Inserted.Website_ID,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Website_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Website_],'')) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
				,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
		INTO [SyncDML].[MDAIBase_Website_]([TaskID],[DMLAction],[Unique_ID],[Website_ID],[Website_],[Create_User],[Create_Date],[Do_Not_Disseminate_412284494],[Key_Attractor_412284410],[Last_Upd_Date],[Last_Upd_User],[MDA_Incident_ID_412284502],[Source_411765484],[Source_Description_411765489],[Source_Reference_411765487] );
				
		--3.3)DoDeletes
		SET @Step ='3.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		DELETE [$(MDA_IBase)].[dbo].[Website_]
		OUTPUT	 @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Website_ID,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Website_],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Website_]([TaskID],[DMLAction],[Unique_ID],[Website_ID],[Website_],[Create_User],[Create_Date],[Do_Not_Disseminate_412284494],[Key_Attractor_412284410],[Last_Upd_Date],[Last_Upd_User],[MDA_Incident_ID_412284502],[Source_411765484],[Source_Description_411765489],[Source_Reference_411765487] )
				
		FROM [$(MDA_IBase)].[dbo].[Website_] MADD
		INNER JOIN #CT CT ON [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Website') = MADD.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';


		--3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='3.4';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MADD
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(CONVERT(VARCHAR,Deleted.ID),'') + '}|I:{' + ISNULL(CONVERT(VARCHAR,Inserted.ID),'') + '}'
				,'D:{' + ISNULL(Deleted.WebsiteID,'') + '}|I:{' + ISNULL(Inserted.WebsiteID,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[URL],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[URL],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SourceDescription],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SourceDescription],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SourceReference],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SourceReference],'')) + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.[KeyAttractor],'') + '}|I:{' + ISNULL(Inserted.[KeyAttractor],'') + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
				 INTO [SyncDML].[MDA_Website]([TaskID],[DMLAction],[Id],[WebsiteId],[URL],[Source],[SourceDescription],[SourceReference],[CreatedBy],[CreatedDate],[KeyAttractor],[ModifiedBy],[ModifiedDate],[IBaseId],[RecordStatus],[ADARecordStatus])
		FROM [$(MDA)].[dbo].[Website] MADD
		INNER JOIN #CT CT ON CT.MDA_ID = MADD.Id
 		WHERE MADD.IBaseId IS NULL
		OR MADD.IBaseId != CT.MDA_IBase_Unique_ID
			



	END
	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		SET @Step ='4.1'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [$(MDA)].[dbo].[Website] [Target]
		USING  (
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, IADD.*
				FROM #CT CT
				INNER JOIN [$(MDA_IBase)].[dbo].[Website_] IADD ON IADD.Unique_ID = CT.MDA_IBase_Unique_ID
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET 
			 url					=	source.Website_						
			,createdBy				=	source.Create_User					
			,CreatedDate			=	source.Create_Date					
			,KeyAttractor			=	source.Key_Attractor_412284410
			,ModifiedDate			=	source.Last_Upd_Date
			,ModifiedBy				=	source.Last_Upd_User
			,[Source]				=	source.Source_411765484
			,SourceReference		=	source.Source_Reference_411765487
			,SourceDescription		=	source.Source_Description_411765489
			,ibaseID				=	source.[Unique_ID]
			,RecordStatus			=   Source.[Record_Status]
			,ADARecordStatus		=   CASE WHEN Source.Record_Status = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
			,[WebSiteId]			= Source.[WebSite_Id]
		WHEN NOT MATCHED THEN INSERT 
		(
			url
			,createdBy								
			,CreatedDate						
			,KeyAttractor			
			,ModifiedDate			
			,ModifiedBy				
			,[Source]				
			,SourceReference		
			,SourceDescription	
			,IbaseID	
			,RecordStatus
			,ADARecordStatus	
			,[WebSiteId]
		)
		Values
		(
			source.Website_						
			,source.Create_User					
			,source.Create_Date					
			,source.Key_Attractor_412284410
			,source.Last_Upd_Date
			,source.Last_Upd_User
			,source.Source_411765484
			,source.Source_Reference_411765487
			,source.Source_Description_411765489
			,Source.Unique_ID
			,source.Record_Status
			,10
			,Source.[WebSite_Id]
		)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(CONVERT(VARCHAR,Deleted.ID),'') + '}|I:{' + ISNULL(CONVERT(VARCHAR,Inserted.ID),'') + '}'
				,'D:{' + ISNULL(Deleted.WebsiteID,'') + '}|I:{' + ISNULL(Inserted.WebsiteID,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[URL],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[URL],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SourceDescription],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SourceDescription],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SourceReference],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SourceReference],'')) + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.[KeyAttractor],'') + '}|I:{' + ISNULL(Inserted.[KeyAttractor],'') + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
				 INTO [SyncDML].[MDA_Website]([TaskID],[DMLAction],[Id],[WebsiteId],[URL],[Source],[SourceDescription],[SourceReference],[CreatedBy],[CreatedDate],[KeyAttractor],[ModifiedBy],[ModifiedDate],[IBaseId],[RecordStatus],[ADARecordStatus]);



		
		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [$(MDA)].[dbo].[Website]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + ISNULL(CONVERT(VARCHAR,Deleted.ID),'') + '}|I:{' + ISNULL(CONVERT(VARCHAR,Inserted.ID),'') + '}'
				,'D:{' + ISNULL(Deleted.WebsiteID,'') + '}|I:{' + ISNULL(Inserted.WebsiteID,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[URL],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[URL],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SourceDescription],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SourceDescription],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SourceReference],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SourceReference],'')) + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.[KeyAttractor],'') + '}|I:{' + ISNULL(Inserted.[KeyAttractor],'') + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		 INTO [SyncDML].[MDA_Website]([TaskID],[DMLAction],[Id],[WebsiteId],[URL],[Source],[SourceDescription],[SourceReference],[CreatedBy],[CreatedDate],[KeyAttractor],[ModifiedBy],[ModifiedDate],[IBaseId],[RecordStatus],[ADARecordStatus])
		FROM [$(MDA)].[dbo].[WebSite] IADD
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IADD.IBaseId
		WHERE CT.DMLAction = 'D';

		
		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE IADD
		SET MDA_Incident_ID_412284502 = [Sync].[fn_ResolveMDAID] (MADD.ID,'MDA.dbo.Website')
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Website_ID,'') + '}|I:{' + ISNULL(Inserted.Website_ID,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Website_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Website_],'')) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
				,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
		INTO [SyncDML].[MDAIBase_Website_]([TaskID],[DMLAction],[Unique_ID],[Website_ID],[Website_],[Create_User],[Create_Date],[Do_Not_Disseminate_412284494],[Key_Attractor_412284410],[Last_Upd_Date],[Last_Upd_User],[MDA_Incident_ID_412284502],[Source_411765484],[Source_Description_411765489],[Source_Reference_411765487] )		
		FROM [$(MDA_IBase)].[dbo].[Website_] IADD
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IADD.Unique_ID
		INNER JOIN [$(MDA)].[dbo].[Website] MADD ON MADD.IBaseId = IADD.Unique_ID
 		WHERE IADD.MDA_Incident_ID_412284502 IS NULL
		OR [Sync].[fn_ResolveMDAID] (MADD.ID,'MDA.dbo.Website') != IADD.MDA_Incident_ID_412284502
	
	END

	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Website] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Website] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Website] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Website_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Website_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Website_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')		
	
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

END TRY
BEGIN CATCH


	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +@Step +']'

	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)

END CATCH


