﻿

CREATE PROCEDURE [IBaseM].[uspGetHeadsOfHeadsClaim]
(@SQL NVARCHAR(3000))
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY


	DECLARE @Marker CHAR(1) = ';'
	DECLARE @EXE_SQL NVARCHAR(3000) = 
	'
	IF OBJECT_Id(''tempdb..#_T'') is not null
		DROP TABLE #_T	
		CREATE TABLE #_T(Id NVARCHAR(10), Value NVARCHAR(254))

	INSERT INTO #_T '
	+ @SQL +
	' 

	SELECT DISTINCT Id,  REPLACE(STUFF((
		SELECT ''' + @Marker + ' '' + value FROM #_T t2 WHERE t1.Id = t2.Id  
		FOR XML PATH('''')), 1, 1, ''''), ''&amp;'', ''&'') as val
	FROM #_T t1;

		IF OBJECT_Id(''tempdb..#_T'') is not null
			DROP TABLE #_T	

	'
	--PRINT @EXE_SQL

	EXEC sp_executesql @EXE_SQL

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
END CATCH

