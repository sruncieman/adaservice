﻿



CREATE PROCEDURE [Rep].[uspIdentifyChange]
/**************************************************************************************************/
-- ObjectName:			MDAControl.IBaseM.uspMigrationStep06
--
-- Description:			Populate the Incident Link tables within IBase8 Target database
--
-- Steps:				
--
-- Dependencies:		
--
-- Usage:				Called within SP IBaseM.uspMigrationMaster
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-27			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 17 --IdentifyChanges
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
			
	------------------------------------------------------------
	--2)MainBody
	------------------------------------------------------------
	/*** AsUniqueIDsCanChangeBetweenMigratedIBase8DatabasesWeAlwaysNeedToCompareTheIBase5Values **/
	---------------------------------
	--DetectEntityChanges
	---------------------------------
	TRUNCATE TABLE Rep.EntityRecord
	INSERT INTO Rep.EntityRecord(TableCode, IBase5EntityID, IBase8EntityID_Staging, IBase8EntityID, RecordAction)
	SELECT   LEFT(SE.IBase8EntityIDStaging,3) TableCode 
			,A.IBase5EntityID
			,SE.IBase8EntityIDStaging IBase8EntityID_Staging
			,IB8SE.IBase8EntityID
			,CASE 
				WHEN A.RecordAction IS NOT NULL THEN A.RecordAction 
				WHEN A.RecordAction IS NULL AND IB8SE.IBase8EntityID IS NOT NULL THEN 'U'
				WHEN A.RecordAction IS NULL AND IB8SE.IBase8EntityID IS NULL THEN 'I'
			 END RecordAction
	FROM	(
			SELECT  IBase5EntityID, NULL RecordAction
			FROM	(
					SELECT IBase5EntityID, ModifiedDate
					FROM IBaseM8Cur.VW_StagingEntities SE
					EXCEPT
					SELECT IBase5EntityID, ModifiedDate
					FROM IBaseM8Pre.VW_StagingEntities SE
					) A
			UNION ALL
			SELECT	IBase5EntityID, 'D' RecordAction
			FROM	(
					SELECT IBase5EntityID
					FROM IBaseM8Pre.VW_StagingEntities SE
					EXCEPT
					SELECT IBase5EntityID
					FROM IBaseM8Cur.VW_StagingEntities SE
					) A
			) A
	LEFT JOIN IBaseM8Cur.VW_StagingEntities SE ON SE.IBase5EntityID = A.IBase5EntityID
	LEFT JOIN [$(MDA_IBase)].[dbo].VW_StagingEntities IB8SE ON IB8SE.IBase5EntityID = A.IBase5EntityID AND (LEFT(IB8SE.IBase8EntityID,3) = LEFT(SE.IBase8EntityIDStaging,3) OR SE.IBase8EntityIDStaging IS NULL)
	GROUP BY A.IBase5EntityID
			,SE.IBase8EntityIDStaging
			,IB8SE.IBase8EntityID
			,CASE 
				WHEN A.RecordAction IS NOT NULL THEN A.RecordAction 
				WHEN A.RecordAction IS NULL AND IB8SE.IBase8EntityID IS NOT NULL THEN 'U'
				WHEN A.RecordAction IS NULL AND IB8SE.IBase8EntityID IS NULL THEN 'I'
			 END 
	ORDER BY A.IBase5EntityID

	IF @Debug = 1
		PRINT CAST(@@ROWCOUNT AS VARCHAR) + ' EntityRecordsToInsertOrUpdate'

	---------------------------------
	--DetectLinkChanges
	---------------------------------
	TRUNCATE TABLE Rep.LinkRecord
	INSERT INTO Rep.LinkRecord (TableCode, IBase5LinkID, IBase8LinkID_Staging, IBase8LinkID, EliteReference, RecordAction)
	SELECT	 LEFT(SL.IBase8LinkIDStaging,3) TableCode
			,A.IBase5LinkID
			,SL.IBase8LinkIDStaging
			,LIB.LinkID
			,SL.Keoghs_Elite_Reference
			,CASE 
				WHEN A.RecordAction IS NOT NULL THEN A.RecordAction 
				WHEN A.RecordAction IS NULL AND LIB.LinkID IS NOT NULL THEN 'U'
				WHEN A.RecordAction IS NULL AND LIB.LinkID IS NULL THEN 'I'
			 END RecordAction
	FROM	(
			SELECT  IBase5LinkID, NULL RecordAction
			FROM	(
					SELECT IBase5LinkID, ModifiedDate
					FROM IBaseM8Cur.VW_StagingLinks SL
					EXCEPT
					SELECT IBase5LinkID, ModifiedDate
					FROM IBaseM8Pre.VW_StagingLinks SL
					) A	
			UNION ALL 
			SELECT	IBase5LinkID, 'D' RecordAction
			FROM	(
					SELECT IBase5LinkID
					FROM IBaseM8Pre.VW_StagingLinks SL
					EXCEPT
					SELECT IBase5LinkID
					FROM IBaseM8Cur.VW_StagingLinks SL
					) A
			)A
	LEFT JOIN IBaseM8Cur.VW_StagingLinks SL ON SL.IBase5LinkID = A.IBase5LinkID
	LEFT JOIN IBaseM8Cur._LinkEnd LE ON LE.Link_ID = SL.IBase8LinkIDStaging
	LEFT JOIN IBaseM8Cur.VW_StagingEntities ENT ON ENT.IBase8EntityIDStaging = LE.Entity_ID1 
	LEFT JOIN IBaseM8Cur.VW_StagingEntities ENT2 ON ENT2.IBase8EntityIDStaging = LE.Entity_ID2
	LEFT JOIN [Rep].[IBaseLinkEnd]  LIB ON LIB.IBase5LinkID = A.IBase5LinkID AND ((LIB.IBase5EntityID1 = ENT.IBase5EntityID AND LIB.IBase5EntityID2 = ENT2.IBase5EntityID AND LEFT(LIB.LinkID,3) = LEFT(SL.IBase8LinkIDStaging,3)) OR ENT.IBase5EntityID IS NULL AND ENT2.IBase5EntityID IS NULL)
	GROUP BY A.IBase5LinkID
			,SL.IBase8LinkIDStaging
			,SL.Keoghs_Elite_Reference
			,LIB.LinkID
			,CASE 
				WHEN A.RecordAction IS NOT NULL THEN A.RecordAction 
				WHEN A.RecordAction IS NULL AND LIB.LinkID IS NOT NULL THEN 'U'
				WHEN A.RecordAction IS NULL AND LIB.LinkID IS NULL THEN 'I'
			 END
	ORDER BY A.IBase5LinkID

	IF @Debug = 1
		PRINT CAST(@@ROWCOUNT AS VARCHAR) + ' LinkRecordsToInsertOrUpdate_TimeStamp'

	--AsIBase8CreatesAdditinalLinksInherritingDataFromOneLinkInIBase5TheseNeedAccountedFor
	INSERT INTO Rep.LinkRecord (TableCode, IBase5LinkID, IBase8LinkID_Staging, IBase8LinkID, EliteReference, RecordAction)
	SELECT	 LEFT(SL.IBase8LinkIDStaging,3)  TableCode
			,SL.IBase5LinkID
			,SL.IBase8LinkIDStaging IBase8LinkID_Staging
			,LIB.LinkID
			,SL.Keoghs_Elite_Reference
			,'I' RecordAction
	FROM IBaseM8Cur.VW_StagingLinks SL
	INNER JOIN IBaseM8Cur._LinkEnd LE ON LE.Link_ID = SL.IBase8LinkIDStaging
	INNER JOIN IBaseM8Cur.VW_StagingEntities ENT ON ENT.IBase8EntityIDStaging = LE.Entity_ID1 
	INNER JOIN IBaseM8Cur.VW_StagingEntities ENT2 ON ENT2.IBase8EntityIDStaging = LE.Entity_ID2
	LEFT JOIN [Rep].[IBaseLinkEnd]  LIB ON LIB.IBase5LinkID = SL.IBase5LinkID AND ((LIB.IBase5EntityID1 = ENT.IBase5EntityID AND LIB.IBase5EntityID2 = ENT2.IBase5EntityID AND LEFT(LIB.LinkID,3) = LEFT(SL.IBase8LinkIDStaging,3)) OR ENT.IBase5EntityID IS NULL AND ENT2.IBase5EntityID IS NULL)
	WHERE NOT EXISTS (SELECT TOP 1 1 FROM Rep.LinkRecord ML WHERE ML.IBase5LinkID = SL.IBase5LinkID)
	AND LIB.LinkID IS NULL
	GROUP BY SL.IBase5LinkID
			,SL.IBase8LinkIDStaging
			,LIB.LinkID
			,SL.Keoghs_Elite_Reference
	ORDER BY SL.IBase5LinkID

	IF @Debug = 1
		PRINT CAST(@@ROWCOUNT AS VARCHAR) + ' LinkRecordsToInsertOrUpdate_ThoseRecordsCreatedAsPartOfIBase8Migration'

	--SameConceptAsAboveButHandlesDeletes
	--AdddedForPerformance
	IF OBJECT_ID('tempdb..#PreData') IS NOT NULL
		DROP TABLE #PreData
	CREATE TABLE #PreData (IBase5LinkID VARCHAR(50), LinkID VARCHAR(50)) 
	
	INSERT INTO #PreData (IBase5LinkID, LinkID)
	SELECT SL.IBase5LinkID,LIB.LinkID
	FROM IBaseM8Pre.VW_StagingLinks SL
	INNER JOIN IBaseM8Pre._LinkEnd LE ON LE.Link_ID = SL.IBase8LinkIDStaging
	INNER JOIN IBaseM8Pre.VW_StagingEntities ENT ON ENT.IBase8EntityIDStaging = LE.Entity_ID1 
	INNER JOIN IBaseM8Pre.VW_StagingEntities ENT2 ON ENT2.IBase8EntityIDStaging = LE.Entity_ID2
	LEFT JOIN [Rep].[IBaseLinkEnd]  LIB ON LIB.IBase5LinkID = SL.IBase5LinkID AND ((LIB.IBase5EntityID1 = ENT.IBase5EntityID AND LIB.IBase5EntityID2 = ENT2.IBase5EntityID AND LEFT(LIB.LinkID,3) = LEFT(SL.IBase8LinkIDStaging,3)) OR ENT.IBase5EntityID IS NULL AND ENT2.IBase5EntityID IS NULL)
	GROUP BY SL.IBase5LinkID
			,LIB.LinkID

	CREATE INDEX IX_#PreData_IBase5LinkID_LinkID
	ON #PreData (IBase5LinkID, LinkID)

	IF OBJECT_ID('tempdb..#CurData') IS NOT NULL
		DROP TABLE #CurData
	CREATE TABLE #CurData(IBase5LinkID VARCHAR(50), LinkID VARCHAR(50))
	
	INSERT INTO #CurData (IBase5LinkID, LinkID)
	SELECT SL.IBase5LinkID ,LIB.LinkID
	FROM IBaseM8Cur.VW_StagingLinks SL
	INNER JOIN IBaseM8Cur._LinkEnd LE ON LE.Link_ID = SL.IBase8LinkIDStaging
	INNER JOIN IBaseM8Cur.VW_StagingEntities ENT ON ENT.IBase8EntityIDStaging = LE.Entity_ID1 
	INNER JOIN IBaseM8Cur.VW_StagingEntities ENT2 ON ENT2.IBase8EntityIDStaging = LE.Entity_ID2
	LEFT JOIN [Rep].[IBaseLinkEnd]  LIB ON LIB.IBase5LinkID = SL.IBase5LinkID AND ((LIB.IBase5EntityID1 = ENT.IBase5EntityID AND LIB.IBase5EntityID2 = ENT2.IBase5EntityID AND LEFT(LIB.LinkID,3) = LEFT(SL.IBase8LinkIDStaging,3)) OR ENT.IBase5EntityID IS NULL AND ENT2.IBase5EntityID IS NULL)
	GROUP BY SL.IBase5LinkID
			,LIB.LinkID

	CREATE INDEX IX_#CurData_IBase5LinkID_LinkID
	ON #CurData (IBase5LinkID, LinkID)

	INSERT INTO  Rep.LinkRecord (TableCode, IBase5LinkID, IBase8LinkID_Staging, IBase8LinkID, RecordAction)
	SELECT	 NULL
			,A.IBase5LinkID
			,NULL
			,A.LinkID
			,'D'
	FROM	(
			SELECT IBase5LinkID, LinkID
			FROM #PreData
			EXCEPT
			SELECT IBase5LinkID, LinkID
			FROM #CurData
			) A	
	WHERE NOT EXISTS (SELECT TOP 1 1 FROM Rep.LinkRecord ML WHERE ML.IBase8LinkID = A.LinkID)
	GROUP BY A.IBase5LinkID
			,A.LinkID


	IF @@ROWCOUNT > 0
	BEGIN
		UPDATE ML
		SET EliteReference = SL.Keoghs_Elite_Reference
		FROM Rep.LinkRecord ML
		INNER JOIN [$(MDA_IBase)].[dbo].VW_StagingLinks SLL ON SLL.IBase8LinkID = ML.IBase8LinkID
		INNER JOIN IBaseM8Pre.VW_StagingLinks SL ON SL.IBase5LinkID = ML.IBase5LinkID
		WHERE RecordAction = 'D'
		AND EliteReference IS NULL
	END	
			
	IF @Debug = 1
		PRINT CAST(@@ROWCOUNT AS VARCHAR) + ' LinkRecordsToInsertOrUpdate_ThoseRecordsDeletedAsPartOfIBase8Migration'		
	---------------------------------
	--DetectLinkEndChanges
	---------------------------------
	--AsTheLinkEndTableHasNoLastModifiedDateWeMustCompareAllColumnData
	IF OBJECT_ID('tempdb..#IBaseDataChanges') is not null
		DROP TABLE #IBaseDataChanges
	IF OBJECT_ID('tempdb..#IBaseStagingIDs') is not null
		DROP TABLE #IBaseStagingIDs	

	SELECT Link_ID, Entity_ID1, Entity_ID2, NULL RecordAction
	INTO #IBaseDataChanges
	FROM	(
			SELECT   SL.IBase5LinkID Link_ID
					,SE1.IBase5EntityID Entity_ID1
					,LE.Confidence
					,LE.Direction
					,SE2.IBase5EntityID Entity_ID2
					,LE.EntityType_ID1
					,LE.EntityType_ID2
					,LE.LinkType_ID
					,LE.Record_Status
					,LE.Record_Type
					,LE.SCC
			FROM IBaseM8Cur._LinkEnd LE
			LEFT JOIN IBaseM8Cur.VW_StagingEntities SE1 ON SE1.IBase8EntityIDStaging = LE.Entity_ID1 --LookupEntityID1IBase5Value
			LEFT JOIN IBaseM8Cur.VW_StagingEntities SE2 ON SE2.IBase8EntityIDStaging = LE.Entity_ID2 --LookupEntityID2IBase5Value
			LEFT JOIN IBaseM8Cur.VW_StagingLinks SL ON SL.IBase8LinkIDStaging = LE.Link_ID --LookupLinkIDIBase5Value
			EXCEPT
			SELECT   SL.IBase5LinkID Link_ID
					,SE1.IBase5EntityID Entity_ID1
					,LE.Confidence
					,LE.Direction
					,SE2.IBase5EntityID Entity_ID2
					,LE.EntityType_ID1
					,LE.EntityType_ID2
					,LE.LinkType_ID
					,LE.Record_Status
					,LE.Record_Type
					,LE.SCC
			FROM IBaseM8Pre._LinkEnd LE
			LEFT JOIN IBaseM8Pre.VW_StagingEntities SE1 ON SE1.IBase8EntityIDStaging = LE.Entity_ID1	--LookupEntityID1IBase5Value
			LEFT JOIN IBaseM8Pre.VW_StagingEntities SE2 ON SE2.IBase8EntityIDStaging = LE.Entity_ID2	--LookupEntityID2IBase5Value
			LEFT JOIN IBaseM8Pre.VW_StagingLinks SL ON SL.IBase8LinkIDStaging = LE.Link_ID				--LookupLinkIDIBase5Value
			) A
		GROUP BY Link_ID, Entity_ID1, Entity_ID2
		UNION ALL
		SELECT	Link_ID, Entity_ID1, Entity_ID2, 'D' RecordAction
		FROM	(
				SELECT   SL.IBase5LinkID Link_ID
						,SE1.IBase5EntityID Entity_ID1
						,SE2.IBase5EntityID Entity_ID2
				FROM IBaseM8Pre._LinkEnd LE
				LEFT JOIN IBaseM8Pre.VW_StagingEntities SE1 ON SE1.IBase8EntityIDStaging = LE.Entity_ID1 --LookupEntityID1IBase5Value
				LEFT JOIN IBaseM8Pre.VW_StagingEntities SE2 ON SE2.IBase8EntityIDStaging = LE.Entity_ID2 --LookupEntityID2IBase5Value
				LEFT JOIN IBaseM8Pre.VW_StagingLinks SL ON SL.IBase8LinkIDStaging = LE.Link_ID			 --LookupLinkIDIBase5Value
				EXCEPT
				SELECT   SL.IBase5LinkID Link_ID
						,SE1.IBase5EntityID Entity_ID1
						,SE2.IBase5EntityID Entity_ID2
				FROM IBaseM8Cur._LinkEnd LE
				LEFT JOIN IBaseM8Cur.VW_StagingEntities SE1 ON SE1.IBase8EntityIDStaging = LE.Entity_ID1	--LookupEntityID1IBase5Value
				LEFT JOIN IBaseM8Cur.VW_StagingEntities SE2 ON SE2.IBase8EntityIDStaging = LE.Entity_ID2	--LookupEntityID2IBase5Value
				LEFT JOIN IBaseM8Cur.VW_StagingLinks SL ON SL.IBase8LinkIDStaging = LE.Link_ID				--LookupLinkIDIBase5Value
				) A
		GROUP BY Link_ID, Entity_ID1, Entity_ID2

	SELECT SL.IBase5LinkID, ENT.IBase5EntityID IBase5EntityID1, ENT2.IBase5EntityID IBase5EntityID2, SL.IBase8LinkIDStaging, ENT.IBase8EntityIDStaging IBase8EntityID1Staging, ENT2.IBase8EntityIDStaging IBase8EntityID2Staging
	INTO #IBaseStagingIDs
	FROM IBaseM8Cur._LinkEnd LE 
	INNER JOIN IBaseM8Cur.VW_StagingEntities ENT ON ENT.IBase8EntityIDStaging = LE.Entity_ID1 
	INNER JOIN IBaseM8Cur.VW_StagingLinks SL ON SL.IBase8LinkIDStaging = LE.Link_ID
	INNER JOIN IBaseM8Cur.VW_StagingEntities ENT2 ON ENT2.IBase8EntityIDStaging = LE.Entity_ID2
	GROUP BY SL.IBase5LinkID, ENT.IBase5EntityID, ENT2.IBase5EntityID, SL.IBase8LinkIDStaging, ENT.IBase8EntityIDStaging, ENT2.IBase8EntityIDStaging

	TRUNCATE TABLE Rep.LinkEndRecord
	INSERT INTO Rep.LinkEndRecord (IBase5LinkID, IBase5EntityID1, IBase5EntityID2, IBase8LinkIDStaging, IBase8EntityID1Staging, IBase8EntityID2Staging, IBase8LinkID, IBase8EntityID1, IBase8EntityID2, RecordAction)
	SELECT	 A.Link_ID IBase5Link_ID
			,A.Entity_ID1 IBase5Entity_ID1
			,A.Entity_ID2 IBase5Entity_ID2
			,SLE.IBase8LinkIDStaging IBase8Link_ID_Staging
			,SLE.IBase8EntityID1Staging IBase8Entity_ID1_Staging
			,SLE.IBase8EntityID2Staging IBase8Entity_ID2_Staging
			,LLE.LinkID
			,LLE.EntityID1
			,LLE.EntityID2
			,CASE 
				WHEN A.RecordAction IS NOT NULL THEN A.RecordAction 
				WHEN A.RecordAction IS NULL AND LLE.LinkID IS NOT NULL THEN 'U'
				WHEN A.RecordAction IS NULL AND LLE.LinkID IS NULL THEN 'I'
			 END RecordAction
	FROM #IBaseDataChanges A
	LEFT JOIN #IBaseStagingIDs SLE ON SLE.IBase5LinkID = A.Link_ID AND SLE.IBase5EntityID1 = A.Entity_ID1 AND SLE.IBase5EntityID2 = A.Entity_ID2
	LEFT JOIN [Rep].[IBaseLinkEnd]  LLE ON LLE.IBase5LinkID = A.Link_ID AND LLE.IBase5EntityID1 = A.Entity_ID1 AND LLE.IBase5EntityID2 = A.Entity_ID2 AND (LEFT(SLE.IBase8LinkIDStaging,3) = LEFT(LLE.LinkID,3) AND (LEFT(SLE.IBase8EntityID1Staging,3) = LEFT(LLE.EntityID1,3) OR  LEFT(SLE.IBase8EntityID1Staging,3) IN ('PAY','ACC','POL')) OR SLE.IBase8LinkIDStaging IS NULL AND SLE.IBase8EntityID1Staging IS NULL)
	GROUP BY A.Link_ID 
			,A.Entity_ID1 
			,A.Entity_ID2 
			,SLE.IBase8LinkIDStaging 
			,SLE.IBase8EntityID1Staging 
			,SLE.IBase8EntityID2Staging 
			,LLE.LinkID
			,LLE.EntityID1
			,LLE.EntityID2
			,CASE 
				WHEN A.RecordAction IS NOT NULL THEN A.RecordAction 
				WHEN A.RecordAction IS NULL AND LLE.LinkID IS NOT NULL THEN 'U'
				WHEN A.RecordAction IS NULL AND LLE.LinkID IS NULL THEN 'I'
			 END 
	ORDER BY A.Link_ID
			,A.Entity_ID1

	IF @Debug = 1
		PRINT CAST(@@ROWCOUNT AS VARCHAR) + ' LinkEndRecordsToInsertOrUpdate'	

	IF OBJECT_ID('tempdb..#IBaseDataChanges') is not null
		DROP TABLE #IBaseDataChanges
	IF OBJECT_ID('tempdb..#IBaseStagingIDs') is not null
		DROP TABLE #IBaseStagingIDs	
	IF OBJECT_ID('tempdb..#PreData') IS NOT NULL
		DROP TABLE #PreData
	IF OBJECT_ID('tempdb..#CurData') IS NOT NULL
		DROP TABLE #CurData
		
	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					--,@InsertRowCount= @InsertRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
	IF OBJECT_ID('tempdb..#IBaseDataChanges') is not null
		DROP TABLE #IBaseDataChanges
	IF OBJECT_ID('tempdb..#IBaseStagingIDs') is not null
		DROP TABLE #IBaseStagingIDs	
	IF OBJECT_ID('tempdb..#PreData') IS NOT NULL
		DROP TABLE #PreData
	IF OBJECT_ID('tempdb..#CurData') IS NOT NULL
		DROP TABLE #CurData

END CATCH




