﻿CREATE PROCEDURE [Rep].[uspReplicateEntityIntelligence]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateEntityIntelligence]	
--
-- Description:		Replicate Entity Intelligence	
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLogging/Tidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-03-03			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Intelligence_')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 48 --ReplicateInteligenceEntity
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	UPDATE [$(MDA_IBase)].[dbo].Intelligence_
	SET Record_Status = 254
	OUTPUT	 @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + ISNULL(Deleted.Description_,'') + '}|I:{' + ISNULL(Inserted.Description_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Document__Binding,'') + '}|I:{' + ISNULL(Inserted.Document__Binding,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Intelligence_ID,'') + '}|I:{' + ISNULL(Inserted.Intelligence_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractors,'') + '}|I:{' + ISNULL(Inserted.Key_Attractors,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Picture__Binding,'') + '}|I:{' + ISNULL(Inserted.Picture__Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Picture_2_Binding,'') + '}|I:{' + ISNULL(Inserted.Picture_2_Binding,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.Report_,'') + '}|I:{' + ISNULL(Inserted.Report_,'') + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Type_,'') + '}|I:{' + ISNULL(Inserted.Type_,'') + '}'
			,'D:{' + ISNULL(Deleted.Web_Page,'') + '}|I:{' + ISNULL(Inserted.Web_Page,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Intelligence_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Description_, Do_Not_Disseminate_412284494, Document__Binding, IconColour, Intelligence_ID, Key_Attractor_412284410, Key_Attractors, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Picture__Binding, Picture_2_Binding, Record_Status, Report_, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Type_, Web_Page, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.EntityRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo].Intelligence_ a ON ent.IBase8EntityID = a.Unique_ID
	WHERE RecordAction ='D'

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo].Intelligence_ [Target]
	USING	(
			SELECT   DISTINCT C.IBase8EntityID
					,C.IBase5EntityID
					,A.*
			FROM IBaseM8Cur.Intelligence_ A
			INNER JOIN Rep.EntityRecord C ON A.Unique_ID = C.IBase8EntityID_Staging	
			) AS Source ON [Target].Unique_ID = Source.IBase8EntityID
	WHEN MATCHED THEN UPDATE
	SET  AltEntity							= Source.AltEntity
		,Create_Date						= Source.Create_Date
		,Create_User						= Source.Create_User
		,Description_						= Source.Description_
		,Do_Not_Disseminate_412284494		= Source.Do_Not_Disseminate_412284494
		,Document_							= Source.Document_
		,Document__Binding					= Source.Document__Binding
		,IconColour							= Source.IconColour
		,Intelligence_ID					= Source.Intelligence_ID
		,Key_Attractor_412284410			= Source.Key_Attractor_412284410
		,Key_Attractors						= Source.Key_Attractors
		,Last_Upd_Date						= Source.Last_Upd_Date
		,Last_Upd_User						= Source.Last_Upd_User
		,Picture_							= Source.Picture_
		,Picture__Binding					= Source.Picture__Binding
		,Picture_2							= Source.Picture_2
		,Picture_2_Binding					= Source.Picture_2_Binding
		,Record_Status						= Source.Record_Status
		,Report_							= Source.Report_
		,SCC								= Source.SCC
		,Source_411765484					= Source.Source_411765484
		,Source_Description_411765489		= Source.Source_Description_411765489
		,Source_Reference_411765487			= Source.Source_Reference_411765487
		,Status_Binding						= Source.Status_Binding
		,Type_								= Source.Type_
		,Web_Page							= Source.Web_Page
		,x5x5x5_Grading_412284402			= Source.x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883			= Source.Risk_Claim_ID_414244883	
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,AltEntity
		,Create_Date
		,Create_User
		,Description_
		,Do_Not_Disseminate_412284494
		,Document_
		,Document__Binding
		,IconColour
		,Intelligence_ID
		,Key_Attractor_412284410
		,Key_Attractors
		,Last_Upd_Date
		,Last_Upd_User
		,Picture_
		,Picture__Binding
		,Picture_2
		,Picture_2_Binding
		,Record_Status
		,Report_
		,SCC
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding
		,Type_
		,Web_Page
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883	
		)
	VALUES
		(
		 Source.Ibase8EntityID
		,Source.AltEntity
		,Source.Create_Date
		,Source.Create_User
		,Source.Description_
		,Source.Do_Not_Disseminate_412284494
		,Source.Document_
		,Source.Document__Binding
		,Source.IconColour
		,Source.Ibase5EntityID
		,Source.Key_Attractor_412284410
		,Source.Key_Attractors
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Picture_
		,Source.Picture__Binding
		,Source.Picture_2
		,Source.Picture_2_Binding
		,Source.Record_Status
		,Source.Report_
		,Source.SCC
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.Type_
		,Source.Web_Page
		,Source.x5x5x5_Grading_412284402
		,Source.Risk_Claim_ID_414244883			
		)
	OUTPUT	 @OutputTaskID
			,$ACTION
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + ISNULL(Deleted.Description_,'') + '}|I:{' + ISNULL(Inserted.Description_,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Document__Binding,'') + '}|I:{' + ISNULL(Inserted.Document__Binding,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Intelligence_ID,'') + '}|I:{' + ISNULL(Inserted.Intelligence_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractors,'') + '}|I:{' + ISNULL(Inserted.Key_Attractors,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Picture__Binding,'') + '}|I:{' + ISNULL(Inserted.Picture__Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Picture_2_Binding,'') + '}|I:{' + ISNULL(Inserted.Picture_2_Binding,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.Report_,'') + '}|I:{' + ISNULL(Inserted.Report_,'') + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Type_,'') + '}|I:{' + ISNULL(Inserted.Type_,'') + '}'
			,'D:{' + ISNULL(Deleted.Web_Page,'') + '}|I:{' + ISNULL(Inserted.Web_Page,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Intelligence_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Description_, Do_Not_Disseminate_412284494, Document__Binding, IconColour, Intelligence_ID, Key_Attractor_412284410, Key_Attractors, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Picture__Binding, Picture_2_Binding, Record_Status, Report_, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Type_, Web_Page, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

	------------------------------------------------------------
	--4)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
			SELECT @InsertRowCount		= (SELECT COUNT(1) FROM dbo.DBControlTaskDetail WHERE TaskID = @OutputTaskID AND ActionID = 'INSERT')
			SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM dbo.DBControlTaskDetail WHERE TaskID = @OutputTaskID AND ActionID = 'UPDATE')
			SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM dbo.DBControlTaskDetail WHERE TaskID = @OutputTaskID AND ActionID = 'DELETE')
		
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()
		
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
						@Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)	
END CATCH



