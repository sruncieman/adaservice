﻿

CREATE PROCEDURE [IBaseM].[uspGetIncidentLinks]
(
 @Party_Type		VARCHAR(50) = NULL
,@Sub_Party_Type	VARCHAR(50) = NULL
)
AS
SET NOCOUNT ON

BEGIN TRY

	IF OBJECT_Id('tempdb..#_CLA_VEH') IS NOT NULL
		DROP TABLE #_CLA_VEH			  
		
	IF OBJECT_Id('tempdb..#_VEH_PER') IS NOT NULL
		DROP TABLE #_VEH_PER	
		
	IF OBJECT_Id('tempdb..#_VEH_ORG') IS NOT NULL
		DROP TABLE #_VEH_ORG		  
		
	IF OBJECT_Id('tempdb..#_PER_ORG') IS NOT NULL
		DROP TABLE #_PER_ORG			  
				  
	IF OBJECT_Id('tempdb..#_KVO') IS NOT NULL
		DROP TABLE #_KVO	
		
	IF OBJECT_Id('tempdb..#_LA') IS NOT NULL
		DROP TABLE #_LA	

	IF OBJECT_Id('tempdb..#_Sto') IS NOT NULL
		DROP TABLE #_Sto
	
	CREATE TABLE #_CLA_VEH
	(
		CLA_VEH_KRef VARCHAR(15),
		CLA_VEH_Link VARCHAR(15),
		CLA_VEH_Ent1 VARCHAR(15),
		CLA_VEH_Ent2 VARCHAR(15),
		CLA_VEH_NL_Type VARCHAR(50),
		CL_Description_  VARCHAR(50),
		CLA_VEH_Ins_Description  VARCHAR(50),
		Reference_Link_Record_Status INT
	)

	CREATE TABLE #_VEH_PER
	(
		VEH_PER_KRef VARCHAR(15),
		VEH_PER_Link VARCHAR(15),
		VEH_PER_Ent1 VARCHAR(15),
		VEH_PER_Ent2 VARCHAR(15),
		VEH_PER_NL_Type VARCHAR(50),
		VEH_PER_CL_Description  VARCHAR(50),
		VEH_PER_Ins_Description  VARCHAR(50), 
		Reference_Link_Record_Status INT
	)

	CREATE TABLE #_VEH_ORG
	(
		VEH_ORG_KRef VARCHAR(15),
		VEH_ORG_Link VARCHAR(15),
		VEH_ORG_Ent1 VARCHAR(15),
		VEH_ORG_Ent2 VARCHAR(15),
		VEH_ORG_NL_Type VARCHAR(50),
		VEH_ORG_CL_Description  VARCHAR(50),
		VEH_ORG_Ins_Description  VARCHAR(50),
		Reference_Link_Record_Status INT
	)

	CREATE TABLE #_PER_ORG
	(
		PER_ORG_KRef VARCHAR(15),
		PER_ORG_Ent1 VARCHAR(15),
		MedExam VARCHAR(500),
		Hire VARCHAR(500),
		Solicitors VARCHAR(500), 
		Accident_Management VARCHAR(500)
	)

	CREATE TABLE #_KVO
	(
		KEO_Entity_ID1 VARCHAR(20),
		VehicleEntity_ID1 VARCHAR(20),
		Recovered_To VARCHAR(500),
		Repairer VARCHAR(500),
		Hired_From VARCHAR(500),
		Stored_At VARCHAR(500),
		Inspected_By VARCHAR(500),
		Recovered_By VARCHAR(500)
	)

	CREATE TABLE #_LA
	(
		KRef VARCHAR(15),
		Link VARCHAR(15),
		Ent1 VARCHAR(15),
		Ent2 VARCHAR(15),
		Description_  VARCHAR(50),
		Location_Description  VARCHAR(254),
		New_Id  VARCHAR(15)
	)

	CREATE TABLE #_Sto
	(
		KRef VARCHAR(15),
		Ent1 VARCHAR(15),
		Storage_Description  VARCHAR(50),
		Storage_Address  VARCHAR(500)
	)

	INSERT INTO #_CLA_VEH
	EXEC  IBaseM.uspGetEntity 'CLA', 'VEH', NULL

	INSERT INTO #_VEH_PER
	EXEC  IBaseM.uspGetEntity 'VEH', 'PER', NULL

	INSERT INTO #_VEH_ORG
	EXEC  IBaseM.uspGetEntity 'VEH', 'ORG', NULL

	INSERT INTO #_PER_ORG
	EXEC IBaseM.uspKeoPersonOrganisations

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_Id(N'[IBaseM].[IncidentLinks_Temp]') AND type in (N'U'))
	DROP TABLE [IBaseM].[IncidentLinks_Temp]

	SELECT 
		'IN1' + CONVERT(VARCHAR(10), ROW_NUMBER() OVER (ORDER BY Incident_Link_ID)) Id,
		*
	INTO [IBaseM].IncidentLinks_Temp
	FROM (
	SELECT DISTINCT 
	Accident_Management,
	NULL AS AltEntity,
	0 AS Ambulance_Attended,
	NULL AS Broker_,
	NULL AS Claim_Code,
	NULL AS Claim_Notification_Date,
	CF.Insurer_Reference AS Claim_Number,
	CF.Insurer_,
	NULL AS Claim_Status,
	CF.Claim_Type AS Claim_Type,
	CF.Create_Date AS Create_Date,
	CF.Create_User AS Create_User,
	Inspected_By,
	Hire,
	CF.IconColour AS IconColour,
	CONVERT(VARCHAR(MAX), CF.Comments_) AS Incident_Circs,
	CLA_VEH_Link Incident_Link_ID,
	Location_Description AS Incident_Location,
	CONVERT(TIME, CF.Incident_Time) AS Incident_Time,
	CF.Last_Upd_Date AS Last_Upd_Date,
	CF.Last_Upd_User AS Last_Upd_User,
	MedExam AS Medical_Examiner, 
	NULL AS MoJ_Status,
	ISNULL( CLA_VEH.CL_Description_ ,  CLA_VEH.CLA_VEH_Ins_Description )     AS Party_Type,
	0 AS Police_Attended,
	NULL AS Police_Force,
	NULL AS Police_Reference,
	CF.Record_Status AS Record_Status, 
	Recovered_By, 
	NULL AS Referral_Source,
	Repairer, 
	CF.SCC AS SCC,
	Solicitors, 
	NULL AS Source,
	NULL AS Source_Description, 
	CF.DB_Source_387004609 AS Source_Reference,
	CF.Status_Binding AS Status_Binding,	
	Stored_At AS Storage,
	Sto.Storage_Address,
	VEH_PER_NL_Type  AS Sub_Party_Type,
	1 Do_Not_Disseminate_412284494	
	,CLA_VEH.*,
	VEH_PER_KRef ,
	NULL VEH_PER_Link,
	VEH_PER_Ent1,
	VEH_PER_Ent2,
	VEH_PER_NL_Type,
	VEH_PER_CL_Description,
	VEH_PER_Ins_Description,
	CLA_VEH.Reference_Link_Record_Status  AS Reference_Link_Record_Status_CLA_VEH,
	VEH_PER.Reference_Link_Record_Status  AS Reference_Link_Record_Status_VEH_PER,
	VEH_ORG.Reference_Link_Record_Status  AS Reference_Link_Record_Status_VEH_ORG
	FROM        #_CLA_VEH AS CLA_VEH  LEFT OUTER JOIN
				#_VEH_PER AS VEH_PER
				 ON CLA_VEH_Ent2 = VEH_PER_Ent1 AND CLA_VEH_KRef = VEH_PER_KRef LEFT OUTER JOIN
				#_VEH_ORG AS VEH_ORG
				 ON VEH_PER_Ent1 = VEH_ORG_Ent1 AND VEH_PER_KRef = VEH_ORG_KRef LEFT OUTER JOIN
				#_PER_ORG AS PER_ORG
				 ON VEH_PER_Ent2 = PER_ORG_Ent1 AND VEH_PER_KRef = PER_ORG_KRef LEFT OUTER JOIN
				#_KVO AS KVO
				 ON CLA_VEH_KRef = KVO.KEO_Entity_ID1 AND CLA_VEH_Ent2 = KVO.VehicleEntity_ID1 LEFT OUTER JOIN
				#_LA AS LA
				 ON LA.Ent2 = CLA_VEH_Ent1 AND LA.KRef = CLA_VEH_KRef  LEFT OUTER JOIN
					#_Sto AS Sto
				 ON Sto.Ent1 = CLA_VEH_Ent2 AND Sto.KRef = CLA_VEH_KRef 	 
	LEFT JOIN
		IBaseM5Cur.ClaimFile_ CF  WITH (NOLOCK) 
		ON CLA_VEH.CLA_VEH_Ent1 = CF.Unique_ID

	WHERE		(ISNULL(CLA_VEH_Ins_Description, '') != 'Insured Vehicle' AND ISNULL(VEH_PER_NL_Type, '') 
				NOT IN ('Owner', 'PaId Policy', 'Previous Keeper', 'Registered Keeper', 'Sold to'))
	OR			(ISNULL(CL_Description_, '') != 'TP Vehicle' AND ISNULL(VEH_PER_NL_Type, '') 
				NOT IN ('Owner', 'PaId Policy', 'Previous Keeper', 'Registered Keeper', 'Sold to'))		
		) X
--INNER JOIN IBaseM5Cur.[Nominal_Link] NL ON NL.[Unique_ID] = X.VEH_PER_Link
--INNER JOIN IBaseM5Cur.[Keoghs_Ref] KR ON KR.[Unique_ID] = X.VEH_PER_KREF
--WHERE (KR.Keoghs_ELITE_Ref = [MDAControl].[IBaseM].[Fn_GetEliteRefFromDbSource](NL.DB_Source_387004609))-- OR KR.[Keoghs_AXIA_Ref] = [MDAControl].[IBaseM].[Fn_GetEliteRefFromDbSource](NL.DB_Source_387004609))

		

	IF OBJECT_Id('tempdb..#_CLA_VEH') is not null
		DROP TABLE #_CLA_VEH			  
		
	IF OBJECT_Id('tempdb..#_VEH_PRE') is not null
		DROP TABLE #_VEH_PRE	
		
	IF OBJECT_Id('tempdb..#_VEH_ORG') is not null
		DROP TABLE #_VEH_ORG		  
		
	IF OBJECT_Id('tempdb..#_PER_ORG') is not null
		DROP TABLE #_PER_ORG			  
				  
	IF OBJECT_Id('tempdb..#_KVO') is not null
		DROP TABLE #_KVO	
		
	IF OBJECT_Id('tempdb..#_LA') is not null
		DROP TABLE #_LA	

	IF OBJECT_Id('tempdb..#_Sto') is not null
		DROP TABLE #_Sto
END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

