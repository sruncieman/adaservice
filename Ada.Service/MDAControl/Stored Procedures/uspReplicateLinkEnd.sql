﻿




CREATE PROCEDURE [Rep].[uspReplicateLinkEnd]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateLinkEnd]	
--
-- Description:		Replicate Link End
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--						3.1)Delete from linkEnd Record where theres a count of 3.
--					4)UpdateLoggingTidyTidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-03-03			Paul Allen		Created
--		2014-12-03			Paul Allen		Fixed issues when a record that's been deduped is modified, Section 3.1
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo._LinkEnd')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 75 --ReplicateLinkEnd
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	IF OBJECT_ID('tempdb..#DMLActions') IS NOT NULL
		DROP TABLE #DMLActions 
	CREATE TABLE #DMLActions (OutputTaskID INT, DMLAction VARCHAR(20), InsertedLink_ID VARCHAR(50), InsertedEntity_ID1 VARCHAR(50), InsertedConfidence INT, InsertedDirection TINYINT, InsertedEntity_ID2 VARCHAR(50), InsertedEntityType_ID1 INT, InsertedEntityType_ID2 INT, InsertedLinkType_ID INT, InsertedRecord_Status TINYINT, InsertedRecord_Type TINYINT, InsertedSCC VARCHAR(255)
							 ,DeletedLink_ID VARCHAR(50), DeletedEntity_ID1 VARCHAR(50), DeletedConfidence INT, DeletedDirection TINYINT, DeletedEntity_ID2 VARCHAR(50), DeletedEntityType_ID1 INT, DeletedEntityType_ID2 INT, DeletedLinkType_ID INT, DeletedRecord_Status TINYINT, DeletedRecord_Type TINYINT, DeletedSCC VARCHAR(255)
							 ,IBase5Link_ID VARCHAR(50), IBase5Entity_ID1 VARCHAR(50), IBase5Entity_ID2 VARCHAR(50))

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	UPDATE [$(MDA_IBase)].[dbo]._LinkEnd
	SET Record_Status = 254
	OUTPUT	 @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Link_ID],'') + '}'
			,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID1],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Confidence],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Direction],'')) + '}'
			,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID2],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID1],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID2],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkType_ID],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Type],'')) + '}'
			,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{' + ISNULL(Inserted.[SCC],'') + '}'
	INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	FROM Rep.LinkEndRecord ent
	INNER JOIN [$(MDA_IBase)].[dbo]._LinkEnd a on ent.IBase8LinkID = a.Link_ID
	WHERE RecordAction ='D'

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo]._LinkEnd [Target]
	USING	(
			SELECT	 DISTINCT C.IBase8LinkID
					,C.IBase8EntityID1
					,C.IBase8EntityID2
					,a.*
			FROM IBaseM8Cur._LinkEnd a
			INNER JOIN Rep.LinkEndRecord  c ON a.Link_ID = c.IBase8LinkIDStaging AND A.Entity_ID1 = c.IBase8EntityID1Staging
			) AS Source ON [Target].Link_ID = Source.IBase8LinkID AND ([Target].Entity_ID1 = Source.IBase8EntityID1) --OR [Target].Record_Type = Source.Record_Type)
	WHEN MATCHED THEN UPDATE
	SET 		
		 Confidence			= Source.Confidence
		,Direction			= Source.Direction
		,Entity_ID2			= Source.IBase8EntityID2
		,EntityType_ID1		= Source.EntityType_ID1
		,EntityType_ID2		= Source.EntityType_ID2
		,LinkType_ID		= Source.LinkType_ID
		,Record_Status		= Source.Record_Status
		,Record_Type		= Source.Record_Type
		,SCC				= Source.SCC
	WHEN NOT MATCHED THEN INSERT  
		(
		 Link_ID
		,Entity_ID1
		,Confidence
		,Direction
		,Entity_ID2
		,EntityType_ID1
		,EntityType_ID2
		,LinkType_ID
		,Record_Status
		,Record_Type
		,SCC
		)
	VALUES
		(
		 Source.IBase8LinkID
		,Source.IBase8EntityID1
		,Source.Confidence
		,Source.Direction
		,Source.IBase8EntityID2
		,Source.EntityType_ID1
		,Source.EntityType_ID2
		,Source.LinkType_ID
		,Source.Record_Status
		,Source.Record_Type
		,Source.SCC		
		)
	OUTPUT   @OutputTaskID
			,$Action
			,Inserted.Link_ID
			,Inserted.Entity_ID1
			,Inserted.Confidence
			,Inserted.Direction
			,Inserted.Entity_ID2
			,Inserted.EntityType_ID1
			,Inserted.EntityType_ID2
			,Inserted.LinkType_ID
			,Inserted.Record_Status
			,Inserted.Record_Type
			,Inserted.SCC
			,Deleted.Link_ID
			,Deleted.Entity_ID1
			,Deleted.Confidence
			,Deleted.Direction
			,Deleted.Entity_ID2
			,Deleted.EntityType_ID1
			,Deleted.EntityType_ID2
			,Deleted.LinkType_ID
			,Deleted.Record_Status
			,Deleted.Record_Type
			,Deleted.SCC
	INTO #DMLActions (OutputTaskID, DMLAction, InsertedLink_ID, InsertedEntity_ID1, InsertedConfidence, InsertedDirection, InsertedEntity_ID2, InsertedEntityType_ID1, InsertedEntityType_ID2, InsertedLinkType_ID, InsertedRecord_Status, InsertedRecord_Type, InsertedSCC, DeletedLink_ID, DeletedEntity_ID1, DeletedConfidence, DeletedDirection, DeletedEntity_ID2, DeletedEntityType_ID1, DeletedEntityType_ID2, DeletedLinkType_ID, DeletedRecord_Status, DeletedRecord_Type, DeletedSCC);

	--------------------------------------------
	--3.1:ThereAreInstancesThatWeWillInsertAndUpdateLinkEndAsDedupeHasMovedTheLink.ThisWillResultInThreeRecords,WeNeedToDeleteOne
	--------------------------------------------
	DELETE LE
	OUTPUT   @OutputTaskID
			,'DELETE'
			,Deleted.Link_ID
			,Deleted.Entity_ID1
			,Deleted.Confidence
			,Deleted.Direction
			,Deleted.Entity_ID2
			,Deleted.EntityType_ID1
			,Deleted.EntityType_ID2
			,Deleted.LinkType_ID
			,Deleted.Record_Status
			,Deleted.Record_Type
			,Deleted.SCC
	INTO #DMLActions (OutputTaskID, DMLAction, DeletedLink_ID, DeletedEntity_ID1, DeletedConfidence, DeletedDirection, DeletedEntity_ID2, DeletedEntityType_ID1, DeletedEntityType_ID2, DeletedLinkType_ID, DeletedRecord_Status, DeletedRecord_Type, DeletedSCC)
	FROM [$(MDA_IBase)].dbo._LinkEnd LE
	INNER JOIN	(
				SELECT DISTINCT C.IBase8LinkID
				FROM IBaseM8Cur._LinkEnd A
				INNER JOIN Rep.LinkEndRecord  c ON a.Link_ID = c.IBase8LinkIDStaging 
				) DLK0 ON DLK0.IBase8LinkID = LE.Link_ID
	LEFT JOIN	(
				SELECT	 DISTINCT C.IBase8LinkID
						,C.IBase8EntityID1
						,C.IBase8EntityID2
				FROM IBaseM8Cur._LinkEnd A
				INNER JOIN Rep.LinkEndRecord  c ON a.Link_ID = c.IBase8LinkIDStaging AND A.Entity_ID1 = c.IBase8EntityID1Staging
				) DLK ON DLK.IBase8LinkID = LE.Link_ID AND DLK.IBase8EntityID1 = LE.Entity_ID1 AND DLK.IBase8EntityID2 = LE.Entity_ID2
	WHERE EXISTS (SELECT TOP 1 1 FROM [$(MDA_IBase)].dbo._LinkEnd LE2 WHERE LE2.Link_ID = LE.Link_ID GROUP BY LE2.Link_ID HAVING COUNT(1) = 3)
	AND DLK.IBase8LinkID IS NULL;

	-----------------------------------------------------------
	--UpdateLogingTable
	-----------------------------------------------------------
	INSERT INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
	SELECT   OutputTaskID
			,DMLAction
			,'D:{' + ISNULL([DeletedLink_ID],'') + '}|I:{' + ISNULL([InsertedLink_ID],'') + '}'
			,'D:{' + ISNULL([DeletedEntity_ID1],'') + '}|I:{' + ISNULL([InsertedEntity_ID1],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL([DeletedConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL([InsertedConfidence],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL([DeletedDirection],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL([InsertedDirection],'')) + '}'
			,'D:{' + ISNULL([DeletedEntity_ID2],'') + '}|I:{' + ISNULL([InsertedEntity_ID2],'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL([DeletedEntityType_ID1],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL([InsertedEntityType_ID1],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL([DeletedEntityType_ID2],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL([InsertedEntityType_ID2],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL([DeletedLinkType_ID],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL([InsertedLinkType_ID],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL([DeletedRecord_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL([InsertedRecord_Status],'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL([DeletedRecord_Type],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL([InsertedRecord_Type],'')) + '}'
			,'D:{' + ISNULL([DeletedSCC],'') + '}|I:{' + ISNULL([InsertedSCC],'') + '}'
	FROM #DMLActions

	-----------------------------------------------------------
	--Update:[MDAControl].[Rep].[IBaseLinkEnd]
	-----------------------------------------------------------
	--GetTheIBaseValues
	UPDATE DML
	SET  IBase5Link_ID		= LK.IBase5LinkID
		,IBase5Entity_ID1	= LK.IBase5EntityID1
		,IBase5Entity_ID2	= LK.IBase5EntityID2
	FROM #DMLActions DML
	INNER JOIN	(
				SELECT SL.IBase8LinkID, ENT.IBase8EntityID IBase8EntityID1, ENT2.IBase8EntityID IBase8EntityID2, SL.IBase5LinkID, ENT.IBase5EntityID IBase5EntityID1, ENT2.IBase5EntityID IBase5EntityID2
				FROM [$(MDA_IBase)].[dbo]._LinkEnd LE
				INNER JOIN [$(MDA_IBase)].[dbo].VW_StagingEntities ENT ON ENT.IBase8EntityID = LE.Entity_ID1
				INNER JOIN [$(MDA_IBase)].[dbo].VW_StagingLinks SL ON SL.IBase8LinkID = LE.Link_ID
				INNER JOIN [$(MDA_IBase)].[dbo].VW_StagingEntities ENT2 ON ENT2.IBase8EntityID = LE.Entity_ID2
				GROUP BY SL.IBase5LinkID, ENT.IBase5EntityID, ENT2.IBase5EntityID, SL.IBase8LinkID, ENT.IBase8EntityID, ENT2.IBase8EntityID
				) LK ON LK.IBase8LinkID = DML.InsertedLink_ID AND LK.IBase8EntityID1 = DML.InsertedEntity_ID1
	WHERE DMLAction = 'INSERT'

	--AddJustTheInsertsToTheTable
	INSERT INTO [Rep].[IBaseLinkEnd] (LinkID, EntityID1, EntityID2, IBase5LinkID, IBase5EntityID1, IBase5EntityID2)
	SELECT InsertedLink_ID, InsertedEntity_ID1, InsertedEntity_ID2, IBase5Link_ID, IBase5Entity_ID1, IBase5Entity_ID2
	FROM #DMLActions DML
	WHERE DMLAction = 'INSERT'
	AND NOT EXISTS (SELECT TOP 1 1 FROM [Rep].[IBaseLinkEnd] T WHERE T.LinkID = DML.InsertedLink_ID AND T.EntityID1 = DML.InsertedEntity_ID2)

	------------------------------------------------------------
	--4)UpdateLoggingTidyTidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd]  WHERE TaskID = @OutputTaskID AND DMLAction = 'INSERT')
		SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd]  WHERE TaskID = @OutputTaskID AND DMLAction IN ('UPDATE','UPDATE DELETE'))
		SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd]  WHERE TaskID = @OutputTaskID AND DMLAction = 'DELETE')
		
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

	IF OBJECT_ID('tempdb..#DMLActions') IS NOT NULL
		DROP TABLE #DMLActions 

END TRY
BEGIN CATCH
	IF OBJECT_ID('tempdb..#DMLActions') IS NOT NULL
		DROP TABLE #DMLActions 

	DECLARE  @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()
				
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
						@Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END	
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'
END CATCH













