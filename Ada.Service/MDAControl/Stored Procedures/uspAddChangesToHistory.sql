﻿



CREATE PROCEDURE [Rep].[uspAddChangesToHistory]
/**************************************************************************************************/
-- ObjectName:			
--
-- Description:			
--
-- Steps:				
--
-- Dependencies:		
--
-- Usage:				Called within SP IBaseM.uspMigrationMaster
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-27			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 22 --AddChangesToHistory
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END		
	
	--1.2)Create/DeclareRequiredObjects
	DECLARE  @BatchesToRetain	INT = 10
			,@IBaseM8CurrentDB  VARCHAR(50) = (SELECT TOP 1 name FROM sys.databases WHERE patindex('IBase8_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name DESC)
			,@IBaseM8PreviousDB VARCHAR(50) = (SELECT TOP 1 name FROM sys.databases WHERE patindex('IBase8_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name )
			,@Today				DATETIME	= (SELECT DATEADD(DAY, DATEDIFF(DAY, '19000101', GETDATE()), '19000101'))
			
	------------------------------------------------------------
	--2)MainBody
	------------------------------------------------------------
	--Entity
	DELETE E
	FROM Rep.EntityRecordHistory E
	WHERE CompareDate = @Today
	AND DatabaseFromName = @IBaseM8PreviousDB
	AND DatabaseToName = @IBaseM8CurrentDB
	
	DELETE E
	FROM Rep.EntityRecordHistory E
	WHERE CompareDate NOT IN (SELECT TOP (@BatchesToRetain) CompareDate
							  FROM Rep.EntityRecordHistory E1
							  GROUP BY CompareDate
							  ORDER BY CompareDate DESC)
							  
	INSERT INTO Rep.EntityRecordHistory (CompareDate, DatabaseFromName, DatabaseToName, TableCode, IBase5EntityID, IBase8EntityID_Staging, IBase8EntityID, RecordAction)
	SELECT @Today, @IBaseM8PreviousDB, @IBaseM8CurrentDB, TableCode, IBase5EntityID, IBase8EntityID_Staging, IBase8EntityID, RecordAction
	FROM Rep.EntityRecord
	
	--Links
	DELETE L
	FROM Rep.LinkRecordHistory L
	WHERE CompareDate = @Today
	AND DatabaseFromName = @IBaseM8PreviousDB
	AND DatabaseToName = @IBaseM8CurrentDB
	
	DELETE L
	FROM Rep.LinkRecordHistory L
	WHERE CompareDate NOT IN (SELECT TOP (@BatchesToRetain) CompareDate
							  FROM Rep.LinkRecordHistory L1
							  GROUP BY CompareDate
							  ORDER BY CompareDate DESC)
	
	INSERT INTO Rep.LinkRecordHistory (CompareDate, DatabaseFromName, DatabaseToName, TableCode, IBase5LinkID, IBase8LinkID_Staging, IBase8LinkID, EliteReference, RiskClaim_Id, RecordAction)
	SELECT @Today, @IBaseM8PreviousDB, @IBaseM8CurrentDB, TableCode, IBase5LinkID, IBase8LinkID_Staging, IBase8LinkID, EliteReference, RiskClaim_Id, RecordAction
	FROM Rep.LinkRecord
	
	--LinkEnd
	DELETE LE
	FROM Rep.LinkEndRecordHistory LE
	WHERE CompareDate = @Today
	AND DatabaseFromName = @IBaseM8PreviousDB
	AND DatabaseToName = @IBaseM8CurrentDB
	
	DELETE LE
	FROM Rep.LinkEndRecordHistory LE
	WHERE CompareDate NOT IN (SELECT TOP (@BatchesToRetain) CompareDate
							  FROM Rep.LinkEndRecordHistory LE
							  GROUP BY CompareDate
							  ORDER BY CompareDate DESC)
							  
	INSERT INTO Rep.LinkEndRecordHistory (CompareDate, DatabaseFromName, DatabaseToName, IBase5LinkID, IBase5EntityID1, IBase5EntityID2, IBase8LinkIDStaging, IBase8EntityID1Staging, IBase8EntityID2Staging, IBase8LinkID, IBase8EntityID1, IBase8EntityID2, RecordAction)
	SELECT @Today, @IBaseM8PreviousDB, @IBaseM8CurrentDB, IBase5LinkID, IBase5EntityID1, IBase5EntityID2, IBase8LinkIDStaging, IBase8EntityID1Staging, IBase8EntityID2Staging, IBase8LinkID, IBase8EntityID1, IBase8EntityID2, RecordAction
	FROM Rep.LinkEndRecord
	
	 ------------------------------------------------------------
	--3)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					--,@InsertRowCount= @InsertRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END
END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
END CATCH



