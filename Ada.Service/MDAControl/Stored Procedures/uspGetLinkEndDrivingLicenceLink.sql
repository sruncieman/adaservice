﻿

CREATE PROCEDURE [IBaseM].[uspGetLinkEndDrivingLicenceLink]
AS
SET NOCOUNT ON

BEGIN TRY

	SELECT 
		B.Unique_ID Link_ID, 
		A.Unique_ID Entity_ID1, 
		0 Confidence, 
		0 Direction, 
		Driving_Licence_ID Entity_ID2, 
		1186  EntityType_ID1, 
		1969  EntityType_ID2, 
		2692  LinkType_ID, 
		0 Record_Status, 
		1 Record_Type, 
		'' SCC
	FROM IBaseM8Cur.Driving_Licence A
	INNER JOIN 
	IBaseM8Cur.Driving_Licence_Link B
	ON A.Driving_Licence_ID = Driving_Licence_Link_ID

	UNION ALL

	SELECT 
		B.Unique_ID Link_ID, 
		Driving_Licence_ID Entity_ID1, 
		0 Confidence, 
		0 Direction, 
		A.Unique_ID Entity_ID2, 
		1969  EntityType_ID1, 
		1186  EntityType_ID2, 
		2692  LinkType_ID, 
		0 Record_Status, 
		1 Record_Type, 
		'' SCC
	FROM IBaseM8Cur.Driving_Licence A
	INNER JOIN 
	IBaseM8Cur.Driving_Licence_Link B
	ON A.Driving_Licence_ID = Driving_Licence_Link_ID
	
END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

