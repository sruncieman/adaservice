﻿

CREATE PROCEDURE  [Sync].[uspSynchroniseEntityAddress]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSyncEntityAddress]			
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)ActionTheChangeInMDA_IBase
--						3.3)DoDeletes
--						3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)	
--						4.1)ActionTheChangeInMDA
--						4.2)DoDeletes
--						4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					5)UpdateLoggingAndTidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-04-22			Paul Allen		Created
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT			

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1), ChangeDatabase VARCHAR(10) ,MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)
					
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 84 --SynchroniseAddressEntity
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, MDA_ID, DMLAction)
	SELECT 'MDA', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA.dbo.Address, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA_IBase.dbo.Address_, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	--2.2)AddSomeIndexesToMakeTheProcessQuicker
	SET @Step ='2.2'
	 
	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT(MDA_IBase_Unique_ID ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3'

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IA.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[Address_] IA ON IA.Unique_ID = CT.MDA_IBase_Unique_ID
	AND IA.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MA.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].[Address] MA ON MA.ID = CT.MDA_ID

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4'

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5'

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1'

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM MDA_IBase.dbo._Address__NextID
		UPDATE MDA_IBase.dbo._Address__NextID SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	'ADD' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY MDA_ID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--3.2)ActionTheChangeInMDA_IBase
		SET @Step ='3.2'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[Address_] [Target]
		USING	(
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, MADD.*, AT.[Text] AddressType
				FROM #CT CT
				INNER JOIN [MDA].[dbo].[Address] MADD ON MADD.ID = CT.MDA_ID
				INNER JOIN MDA.dbo.AddressType AS AT ON AT.Id = MADD.AddressType_Id
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET  Address_Type					= CASE WHEN Source.[AddressType_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Target].Address_Type ELSE NULLIF(Source.AddressType,'Unknown') END
			,Building_						= Source.[Building]
			,Building_Number				= Source.[BuildingNumber]
			,County_						= Source.[County]
			,Create_Date					= Source.[CreatedDate]
			,Create_User					= Source.[CreatedBy]
			,DX_Exchange					= Source.[DxExchange]
			,DX_Number						= Source.[DxNumber]
			,Grid_X							= Source.[GridX]
			,Grid_Y							= Source.[GridY]
			,Key_Attractor_412284410		= Source.[KeyAttractor]
			,Last_Upd_Date					= Source.[ModifiedDate]
			,Last_Upd_User					= Source.[ModifiedBy]
			,Locality_						= Source.[Locality]
			,PAF_Validation					= CASE WHEN NULLIF(Source.[PafUPRN],'') IS NULL THEN 0 ELSE 1 END 
			,Post_Code						= REPLACE(Source.[PostCode],' ','')
			,Record_Status					= Source.[RecordStatus]
			,Source_Description_411765489	= Source.[SourceDescription]
			,Source_Reference_411765487		= Source.[SourceReference]
			,Street_						= Source.[Street]
			,Sub_Building					= Source.[SubBuilding]
			,Town_							= Source.[Town]
		WHEN NOT MATCHED THEN INSERT 
			(
			 Unique_ID
			,Address_ID
			,Address_Type
			,AltEntity
			,Building_
			,Building_Number
			,County_
			,Create_Date
			,Create_User
			,Do_Not_Disseminate_412284494
			,DX_Exchange
			,DX_Number
			,Grid_X
			,Grid_Y
			,Key_Attractor_412284410
			,Last_Upd_Date
			,Last_Upd_User
			,Locality_
			,MDA_Incident_ID_412284502
			,PAF_Validation
			,Post_Code
			,Record_Status
			,Source_411765484
			,Source_Description_411765489
			,Source_Reference_411765487
			,Street_
			,Sub_Building
			,Town_
			)
		VALUES
			(
			 Source.MDA_IBase_Unique_ID
			,Source.[AddressId]
			,CASE WHEN Source.AddressType_Id = 0 THEN NULL ELSE Source.AddressType END
			,'House'
			,Source.[Building]
			,Source.[BuildingNumber]
			,Source.[County]
			,Source.[CreatedDate]
			,Source.[CreatedBy]
			,0
			,Source.[DxExchange]
			,Source.[DxNumber]
			,Source.[GridX]
			,Source.[GridY]
			,Source.[KeyAttractor]
			,Source.[ModifiedDate]
			,Source.[ModifiedBy]
			,Source.[Locality]
			,[Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Address')
			,CASE WHEN NULLIF(Source.[PafUPRN],'') IS NULL THEN 0 ELSE 1 END 
			,REPLACE(Source.[PostCode],' ','')
			,Source.[RecordStatus]
			,Source.[Source]
			,Source.[SourceDescription]
			,Source.[SourceReference]
			,Source.[Street]
			,Source.[SubBuilding]
			,Source.[Town]
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Address_ID,'') + '}|I:{' + ISNULL(Inserted.Address_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Address_Type,'') + '}|I:{' + ISNULL(Inserted.Address_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Building_,'') + '}|I:{' + ISNULL(Inserted.Building_,'') + '}'
				,'D:{' + ISNULL(Deleted.Building_Number,'') + '}|I:{' + ISNULL(Inserted.Building_Number,'') + '}'
				,'D:{' + ISNULL(Deleted.County_,'') + '}|I:{' + ISNULL(Inserted.County_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.[Document_Link_Binding],'') + '}|I:{' + ISNULL(Inserted.[Document_Link_Binding],'') + '}'
				,'D:{' + ISNULL(Deleted.[DX_Exchange],'') + '}|I:{' + ISNULL(Inserted.[DX_Exchange],'') + '}'
				,'D:{' + ISNULL(Deleted.[DX_Number],'') + '}|I:{' + ISNULL(Inserted.[DX_Number],'') + '}'
				,'D:{' + ISNULL(Deleted.[Grid_X],'') + '}|I:{' + ISNULL(Inserted.[Grid_X],'') + '}'
				,'D:{' + ISNULL(Deleted.[Grid_Y],'') + '}|I:{' + ISNULL(Inserted.[Grid_Y],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[IconColour],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
				,'D:{' + ISNULL(Deleted.[Locality_],'') + '}|I:{' + ISNULL(Inserted.[Locality_],'') + '}'
				,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[PAF_Validation],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[PAF_Validation],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Post_Code],'') + '}|I:{' + ISNULL(Inserted.[Post_Code],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SCC],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Status_Binding],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Street_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Street_],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Sub_Building],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Sub_Building],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Town_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Town_],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[x5x5x5_Grading_412284402],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Risk_Claim_ID_414244883],'')) + '}'
		INTO [SyncDML].[MDAIBase_Address_] (TaskID, DMLAction, Unique_ID, Address_ID, Address_Type, AltEntity, Building_, Building_Number, County_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Document_Link_Binding, DX_Exchange, DX_Number, Grid_X, Grid_Y, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Locality_, MDA_Incident_ID_412284502, PAF_Validation, Post_Code, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Street_, Sub_Building, Town_, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);
		
		--3.3)DoDeletes
		SET @Step ='3.3';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[Address_]
		OUTPUT	 @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Address_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Address_Type,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Building_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Building_Number,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.County_,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Document_Link_Binding],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[DX_Exchange],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[DX_Number],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Grid_X],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Grid_Y],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Locality_],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[PAF_Validation],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Post_Code],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Street_],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Sub_Building],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Town_],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Address_] (TaskID, DMLAction, Unique_ID, Address_ID, Address_Type, AltEntity, Building_, Building_Number, County_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Document_Link_Binding, DX_Exchange, DX_Number, Grid_X, Grid_Y, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Locality_, MDA_Incident_ID_412284502, PAF_Validation, Post_Code, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Street_, Sub_Building, Town_, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Address_] MADD
		INNER JOIN #CT CT ON [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Address') = MADD.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';
		
		--3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='3.4';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MADD
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressId],'') + '}|I:{' + ISNULL(Inserted.[AddressId],'') + '}'
				,'D:{' + ISNULL(Deleted.[SubBuilding],'') + '}|I:{' + ISNULL(Inserted.[SubBuilding],'') + '}'
				,'D:{' + ISNULL(Deleted.[Building],'') + '}|I:{' + ISNULL(Inserted.[Building],'') + '}'
				,'D:{' + ISNULL(Deleted.[BuildingNumber],'') + '}|I:{' + ISNULL(Inserted.[BuildingNumber],'') + '}'
				,'D:{' + ISNULL(Deleted.[Street],'') + '}|I:{' + ISNULL(Inserted.[Street],'') + '}'
				,'D:{' + ISNULL(Deleted.[Locality],'') + '}|I:{' + ISNULL(Inserted.[Locality],'') + '}'
				,'D:{' + ISNULL(Deleted.[Town],'') + '}|I:{' + ISNULL(Inserted.[Town],'') + '}'
				,'D:{' + ISNULL(Deleted.[County],'') + '}|I:{' + ISNULL(Inserted.[County],'') + '}'
				,'D:{' + ISNULL(Deleted.[PostCode],'') + '}|I:{' + ISNULL(Inserted.[PostCode],'') + '}'
				,'D:{' + ISNULL(Deleted.[DxNumber],'') + '}|I:{' + ISNULL(Inserted.[DxNumber],'') + '}'
				,'D:{' + ISNULL(Deleted.[DxExchange],'') + '}|I:{' + ISNULL(Inserted.[DxExchange],'') + '}'
				,'D:{' + ISNULL(Deleted.[GridX],'') + '}|I:{' + ISNULL(Inserted.[GridX],'') + '}'
				,'D:{' + ISNULL(Deleted.[GridY],'') + '}|I:{' + ISNULL(Inserted.[GridY],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[PafValidation],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[PafValidation],'')) + '}'
				,'D:{' + ISNULL(Deleted.[PafUPRN],'') + '}|I:{' + ISNULL(Inserted.[PafUPRN],'') + '}'
				,'D:{' + ISNULL(Deleted.[KeyAttractor],'') + '}|I:{' + ISNULL(Inserted.[KeyAttractor],'') + '}'
				,'D:{' + ISNULL(Deleted.[PropertyType],'') + '}|I:{' + ISNULL(Inserted.[PropertyType],'') + '}'
				,'D:{' + ISNULL(Deleted.[MosaicCode],'') + '}|I:{' + ISNULL(Inserted.[MosaicCode],'') + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressType_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Address] (TaskID, DMLAction, Id, AddressId, SubBuilding, Building, BuildingNumber, Street, Locality, Town, County, PostCode, DxNumber, DxExchange, GridX, GridY, PafValidation, PafUPRN, KeyAttractor, PropertyType, MosaicCode, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, AddressType_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM MDA.dbo.[Address] MADD
		INNER JOIN #CT CT ON CT.MDA_ID = MADD.Id
 		WHERE MADD.IBaseId IS NULL
		OR MADD.IBaseId != CT.MDA_IBase_Unique_ID
			
	END
	
	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		SET @Step ='4.1'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].[Address] [Target]
		USING  (
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, IADD.*, AT.[Text] AddressType, AT.ID AddressTypeID
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Address_] IADD ON IADD.Unique_ID = CT.MDA_IBase_Unique_ID
				LEFT JOIN MDA.dbo.AddressType AS AT ON AT.[Text] = IADD.Address_Type
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  SubBuilding			= Source.[Sub_Building]
			,Building				= Source.[Building_]
			,BuildingNumber			= Source.[Building_Number]
			,Street					= Source.[Street_]
			,Locality				= Source.[Locality_]
			,Town					= Source.[Town_]
			,County					= Source.[County_]
			,PostCode				= REPLACE(Source.[Post_Code],' ','')
			,DxNumber				= Source.[DX_Number]
			,DxExchange				= Source.[DX_Exchange]
			,GridX					= Source.[Grid_X]
			,GridY					= Source.[Grid_Y]
			,PafValidation			= 0
			,KeyAttractor			= Source.[Key_Attractor_412284410]
			,SourceReference		= Source.[Source_Reference_411765487]
			,SourceDescription		= Source.[Source_Description_411765489]
			,CreatedBy				= Source.[Create_User]
			,CreatedDate			= Source.[Create_Date]
			,ModifiedBy				= Source.[Last_Upd_User]
			,ModifiedDate			= Source.[Last_Upd_Date]
			,AddressType_Id			= ISNULL(Source.AddressTypeID,0)
			,IBaseId				= Source.[Unique_ID]
			,RecordStatus			= Source.[Record_Status]
			,ADARecordStatus		= CASE WHEN Source.Record_Status = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT 
			(
			 AddressId
			,SubBuilding
			,Building
			,BuildingNumber
			,Street
			,Locality
			,Town
			,County
			,PostCode
			,DxNumber
			,DxExchange
			,GridX
			,GridY
			,PafValidation
			,KeyAttractor
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,AddressType_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES
			(
			 Source.Address_ID
			,Source.Sub_Building
			,Source.Building_
			,Source.Building_Number
			,Source.Street_
			,Source.Locality_
			,Source.Town_
			,Source.County_
			,REPLACE(Source.[Post_Code],' ','')
			,Source.DX_Number
			,Source.DX_Exchange
			,Source.Grid_X
			,Source.Grid_Y
			,0
			,Source.[Key_Attractor_412284410]
			,Source.[Source_411765484]
			,Source.[Source_Reference_411765487]
			,Source.[Source_Description_411765489]
			,Source.[Create_User]
			,Source.[Create_Date]
			,Source.[Last_Upd_User]
			,Source.[Last_Upd_Date]
			,ISNULL(Source.AddressTypeID,0)
			,Source.[Unique_ID]
			,Source.[Record_Status]
			,10
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressId],'') + '}|I:{' + ISNULL(Inserted.[AddressId],'') + '}'
				,'D:{' + ISNULL(Deleted.[SubBuilding],'') + '}|I:{' + ISNULL(Inserted.[SubBuilding],'') + '}'
				,'D:{' + ISNULL(Deleted.[Building],'') + '}|I:{' + ISNULL(Inserted.[Building],'') + '}'
				,'D:{' + ISNULL(Deleted.[BuildingNumber],'') + '}|I:{' + ISNULL(Inserted.[BuildingNumber],'') + '}'
				,'D:{' + ISNULL(Deleted.[Street],'') + '}|I:{' + ISNULL(Inserted.[Street],'') + '}'
				,'D:{' + ISNULL(Deleted.[Locality],'') + '}|I:{' + ISNULL(Inserted.[Locality],'') + '}'
				,'D:{' + ISNULL(Deleted.[Town],'') + '}|I:{' + ISNULL(Inserted.[Town],'') + '}'
				,'D:{' + ISNULL(Deleted.[County],'') + '}|I:{' + ISNULL(Inserted.[County],'') + '}'
				,'D:{' + ISNULL(Deleted.[PostCode],'') + '}|I:{' + ISNULL(Inserted.[PostCode],'') + '}'
				,'D:{' + ISNULL(Deleted.[DxNumber],'') + '}|I:{' + ISNULL(Inserted.[DxNumber],'') + '}'
				,'D:{' + ISNULL(Deleted.[DxExchange],'') + '}|I:{' + ISNULL(Inserted.[DxExchange],'') + '}'
				,'D:{' + ISNULL(Deleted.[GridX],'') + '}|I:{' + ISNULL(Inserted.[GridX],'') + '}'
				,'D:{' + ISNULL(Deleted.[GridY],'') + '}|I:{' + ISNULL(Inserted.[GridY],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[PafValidation],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[PafValidation],'')) + '}'
				,'D:{' + ISNULL(Deleted.[PafUPRN],'') + '}|I:{' + ISNULL(Inserted.[PafUPRN],'') + '}'
				,'D:{' + ISNULL(Deleted.[KeyAttractor],'') + '}|I:{' + ISNULL(Inserted.[KeyAttractor],'') + '}'
				,'D:{' + ISNULL(Deleted.[PropertyType],'') + '}|I:{' + ISNULL(Inserted.[PropertyType],'') + '}'
				,'D:{' + ISNULL(Deleted.[MosaicCode],'') + '}|I:{' + ISNULL(Inserted.[MosaicCode],'') + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressType_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Address] (TaskID, DMLAction, Id, AddressId, SubBuilding, Building, BuildingNumber, Street, Locality, Town, County, PostCode, DxNumber, DxExchange, GridX, GridY, PafValidation, PafUPRN, KeyAttractor, PropertyType, MosaicCode, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, AddressType_Id, IBaseId, RecordStatus, ADARecordStatus);

		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Address]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressId],'') + '}|I:{' + ISNULL(Inserted.[AddressId],'') + '}'
				,'D:{' + ISNULL(Deleted.[SubBuilding],'') + '}|I:{' + ISNULL(Inserted.[SubBuilding],'') + '}'
				,'D:{' + ISNULL(Deleted.[Building],'') + '}|I:{' + ISNULL(Inserted.[Building],'') + '}'
				,'D:{' + ISNULL(Deleted.[BuildingNumber],'') + '}|I:{' + ISNULL(Inserted.[BuildingNumber],'') + '}'
				,'D:{' + ISNULL(Deleted.[Street],'') + '}|I:{' + ISNULL(Inserted.[Street],'') + '}'
				,'D:{' + ISNULL(Deleted.[Locality],'') + '}|I:{' + ISNULL(Inserted.[Locality],'') + '}'
				,'D:{' + ISNULL(Deleted.[Town],'') + '}|I:{' + ISNULL(Inserted.[Town],'') + '}'
				,'D:{' + ISNULL(Deleted.[County],'') + '}|I:{' + ISNULL(Inserted.[County],'') + '}'
				,'D:{' + ISNULL(Deleted.[PostCode],'') + '}|I:{' + ISNULL(Inserted.[PostCode],'') + '}'
				,'D:{' + ISNULL(Deleted.[DxNumber],'') + '}|I:{' + ISNULL(Inserted.[DxNumber],'') + '}'
				,'D:{' + ISNULL(Deleted.[DxExchange],'') + '}|I:{' + ISNULL(Inserted.[DxExchange],'') + '}'
				,'D:{' + ISNULL(Deleted.[GridX],'') + '}|I:{' + ISNULL(Inserted.[GridX],'') + '}'
				,'D:{' + ISNULL(Deleted.[GridY],'') + '}|I:{' + ISNULL(Inserted.[GridY],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[PafValidation],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[PafValidation],'')) + '}'
				,'D:{' + ISNULL(Deleted.[PafUPRN],'') + '}|I:{' + ISNULL(Inserted.[PafUPRN],'') + '}'
				,'D:{' + ISNULL(Deleted.[KeyAttractor],'') + '}|I:{' + ISNULL(Inserted.[KeyAttractor],'') + '}'
				,'D:{' + ISNULL(Deleted.[PropertyType],'') + '}|I:{' + ISNULL(Inserted.[PropertyType],'') + '}'
				,'D:{' + ISNULL(Deleted.[MosaicCode],'') + '}|I:{' + ISNULL(Inserted.[MosaicCode],'') + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressType_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Address] (TaskID, DMLAction, Id, AddressId, SubBuilding, Building, BuildingNumber, Street, Locality, Town, County, PostCode, DxNumber, DxExchange, GridX, GridY, PafValidation, PafUPRN, KeyAttractor, PropertyType, MosaicCode, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, AddressType_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Address] IADD
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IADD.IBaseId
		WHERE CT.DMLAction = 'D';

		--4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='4.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE IADD
		SET MDA_Incident_ID_412284502 = [Sync].[fn_ResolveMDAID] (MADD.ID,'MDA.dbo.Address')
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Address_ID,'') + '}|I:{' + ISNULL(Inserted.Address_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Address_Type,'') + '}|I:{' + ISNULL(Inserted.Address_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Building_,'') + '}|I:{' + ISNULL(Inserted.Building_,'') + '}'
				,'D:{' + ISNULL(Deleted.Building_Number,'') + '}|I:{' + ISNULL(Inserted.Building_Number,'') + '}'
				,'D:{' + ISNULL(Deleted.County_,'') + '}|I:{' + ISNULL(Inserted.County_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.[Document_Link_Binding],'') + '}|I:{' + ISNULL(Inserted.[Document_Link_Binding],'') + '}'
				,'D:{' + ISNULL(Deleted.[DX_Exchange],'') + '}|I:{' + ISNULL(Inserted.[DX_Exchange],'') + '}'
				,'D:{' + ISNULL(Deleted.[DX_Number],'') + '}|I:{' + ISNULL(Inserted.[DX_Number],'') + '}'
				,'D:{' + ISNULL(Deleted.[Grid_X],'') + '}|I:{' + ISNULL(Inserted.[Grid_X],'') + '}'
				,'D:{' + ISNULL(Deleted.[Grid_Y],'') + '}|I:{' + ISNULL(Inserted.[Grid_Y],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[IconColour],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
				,'D:{' + ISNULL(Deleted.[Locality_],'') + '}|I:{' + ISNULL(Inserted.[Locality_],'') + '}'
				,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[PAF_Validation],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[PAF_Validation],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Post_Code],'') + '}|I:{' + ISNULL(Inserted.[Post_Code],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SCC],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Status_Binding],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Street_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Street_],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Sub_Building],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Sub_Building],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Town_],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Town_],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[x5x5x5_Grading_412284402],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Risk_Claim_ID_414244883],'')) + '}'		
		INTO [SyncDML].[MDAIBase_Address_] (TaskID, DMLAction, Unique_ID, Address_ID, Address_Type, AltEntity, Building_, Building_Number, County_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Document_Link_Binding, DX_Exchange, DX_Number, Grid_X, Grid_Y, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Locality_, MDA_Incident_ID_412284502, PAF_Validation, Post_Code, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Street_, Sub_Building, Town_, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Address_] IADD
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IADD.Unique_ID
		INNER JOIN [MDA].[dbo].[Address] MADD ON MADD.IBaseId = IADD.Unique_ID
 		WHERE IADD.MDA_Incident_ID_412284502 IS NULL
		OR [Sync].[fn_ResolveMDAID] (MADD.ID,'MDA.dbo.Address') != IADD.MDA_Incident_ID_412284502
		
	END
	
	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Address] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

END TRY
BEGIN CATCH

	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +@Step +']'

	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH


