﻿

CREATE PROCEDURE [IBaseM].[uspMigrationStep04]
/**************************************************************************************************/
-- ObjectName:			MDAControl.IBaseM.uspMigrationStep04
--
-- Description:			Populate the link tables within IBase8 Target database
--
-- Steps:				1)ConfigureLogging,Create/DeclareRequiredObjects
--							1.1)ConfigureLogging
--							1.2)Create/DeclareRequiredObjects
--						2)InsertDataIntoTable
--						3)UpdateLogging/Tidy
--
-- Dependencies:		IBaseM.uspGetIncidentLinks
--						IBaseM.uspTruncateTableSynonym 
--
-- Usage:				Called within SP IBaseM.uspMigrationMaster
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 11 --Populate_IncidentLink
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	
	--1.2)Create/DeclareRequiredObjects
	DECLARE @NextID INT
			
	------------------------------------------------------------
	--2)MainBody
	------------------------------------------------------------
	EXECUTE IBaseM.uspGetIncidentLinks 
	 
	UPDATE A
	SET CLA_VEH_Ent1 = B.Unique_ID  
	FROM  IBaseM.IncidentLinks_Temp A 
	INNER JOIN IBaseM8Cur.Incident_ B
	ON Incident_ID = CLA_VEH_Ent1

	--Incident_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Incident_Link'
	INSERT INTO IBaseM8Cur.Incident_Link
			(Unique_ID, Accident_Management, AltEntity, Ambulance_Attended, Broker_, 
			Claim_Code, Claim_Notification_Date, Claim_Number, Claim_Status, Claim_Type, 
			Create_Date, Create_User, Do_Not_Disseminate_412284494, Engineer_, Hire_, 
			IconColour, Incident_Circs, Incident_Link_ID, Incident_Location, Incident_Time, 
			Insurer_, Last_Upd_Date, Last_Upd_User, 
			Medical_Examiner, MoJ_Status, Party_Type, Police_Attended, 
			Police_Force, Police_Reference, Record_Status, Recovery_, Referral_Source, 
			Repairer_, SCC, Solicitors_, Source_411765484, Source_Description_411765489, 
			Source_Reference_411765487, Status_Binding, Storage_, Storage_Address, Sub_Party_Type)
	SELECT	
		ID,
		Accident_Management, 
		AltEntity, 
		Ambulance_Attended, 
		Broker_, 
		Claim_Code, 
		Claim_Notification_Date, 
		Claim_Number, 
		Claim_Status, 
		Claim_Type, 
		Create_Date,  
		Create_User, 
		Do_Not_Disseminate_412284494, 
		Inspected_By, 
		Hire, 
		IconColour, 
		Incident_Circs, 
		Incident_Link_ID, 
		Incident_Location, 
		Incident_Time, 
		Insurer_, 
		Last_Upd_Date, 
		Last_Upd_User, 
		Medical_Examiner, 
		MoJ_Status, 
		Party_Type,
		Police_Attended, 
		Police_Force, 
		Police_Reference, 
		Reference_Link_Record_Status_VEH_PER AS Record_Status, 
		Recovered_By, 
		Referral_Source, 
		Repairer, 
		SCC, 
		Solicitors, 
		'Keoghs CFS' AS Source, 
		NULL Source_Description,
		IBaseM.Fn_GetEliteRefFromDbSource(Source_Reference) AS  EliteRef,
		Status_Binding, 
		Storage, 
		Storage_Address, 
		Sub_Party_Type
	FROM IBaseM.IncidentLinks_Temp  WITH(NOLOCK)

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Incident_Link
	INSERT INTO IBaseM8Cur.Incident_Link(
		Unique_ID, 
		Incident_Link_ID,
		AltEntity,
		Create_Date, 
		Create_User,
		Source_Description_411765489,
		Source_Reference_411765487,
		Last_Upd_Date, 
		Last_Upd_User,
		Record_Status, 
		SCC,   
		Status_Binding,
		IconColour,
		Police_Attended, 
		Ambulance_Attended, 
		Do_Not_Disseminate_412284494,
		Party_Type,
		Source_411765484,
		Insurer_,
		Claim_Number,
		Sub_Party_Type

	)
	SELECT
		'IN1' + CONVERT(VARCHAR(10), ROW_NUMBER() OVER (ORDER BY M.Unique_ID) + 1000000 ) ID,
		MM.Unique_ID, --PA:Added20140204
		M.AltEntity, 
		M.Create_Date, 
		M.Create_User, 
		'CUE / MIAFTR Match' AS Description_,
		IBaseM.Fn_GetEliteRefFromDbSource(MM.DB_Source_387004609) AS  EliteRef,
		M.Last_Upd_Date, 
		M.Last_Upd_User, 
		M.Record_Status, 
		M.SCC, 
		M.Status_Binding, 
		M.IconColour,
		0 Police_Attended, 
		0 Ambulance_Attended, 
		1 Do_Not_Disseminate_412284494,
		'Unknown' /*'CUE / MIAFTR Match'*/ 	Party_Type,
		'Keoghs CFS',
		M.Insurer_,
		M.Insurer_Ref,
		'Unknown' Sub_Party_Type
	FROM IBaseM5Cur._LinkEnd LE WITH(NOLOCK)
	INNER JOIN IBaseM5Cur.MIAFTR_Match_Link MM WITH(NOLOCK) ON LE.Link_ID = MM.Unique_ID
	INNER JOIN IBaseM5Cur.MIAFTR_Match M WITH(NOLOCK) ON LE.Entity_ID1 = M.Unique_ID
	WHERE (Entity_ID1 LIKE 'MIA%')
	AND (Entity_ID2 LIKE 'PER%')

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Incident_Link
	INSERT INTO IBaseM8Cur.Incident_Link(
		Unique_ID, 
		Incident_Link_ID,
		AltEntity,
		Create_Date, 
		Create_User,
		Source_Description_411765489,
		Source_Reference_411765487,
		Last_Upd_Date, 
		Last_Upd_User,
		Record_Status, 
		SCC,   
		Status_Binding,
		IconColour,
		Police_Attended, 
		Ambulance_Attended, 
		Do_Not_Disseminate_412284494,
		Party_Type
	)
	SELECT  DISTINCT
			'IN1' + CONVERT(VARCHAR(10), ROW_NUMBER() OVER (ORDER BY Unique_ID) + 1100000 ) ID,
			Unique_ID,
			AltEntity AS AltEntity,
			Create_Date	AS Create_Date, 
			Create_User	AS Create_User,
			DB_Source_387004609 Description_ , 
			IBaseM.Fn_GetEliteRefFromDbSource(DB_Source_387004609) AS  EliteRef,
			Last_Upd_Date, 
			Last_Upd_User, 
			Record_Status, 
			SCC,
			Status_Binding,
			IconColour, 
			0 Police_Attended, 
			0 Ambulance_Attended, 
			1 Do_Not_Disseminate_412284494,
			'Keoghs CFS' AS Source
	FROM IBaseM5Cur.Insured_Claim_Link  WITH(NOLOCK)
	WHERE Description_ = 'Insured Claim'

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Incident_Link
	INSERT INTO IBaseM8Cur.Incident_Link(
		Unique_ID, 
		Incident_Link_ID, 
		AltEntity, 
		Create_Date, 
		Create_User, 
		Source_Description_411765489,
		Source_Reference_411765487,
		Last_Upd_Date, 
		Last_Upd_User,
		Record_Status, 
		SCC, 
		Status_Binding,
		IconColour, 
		Police_Attended, 
		Ambulance_Attended, 
		Do_Not_Disseminate_412284494,
		Source_411765484,
		Party_Type,
		Sub_Party_Type,
		Incident_Circs,
		Incident_Time,
		Insurer_,
		Claim_Number)

	SELECT   DISTINCT
			'IN1' + CONVERT(VARCHAR(10), ROW_NUMBER() OVER (ORDER BY A.Unique_ID) + 1150000 ) ID,
			A.Unique_ID AS Incident_Link_ID, 
			A.AltEntity AS AltEntity, 
			A.Create_Date	AS Create_Date, 
			A.Create_User	AS Create_User, 
			A.DB_Source_387004609 AS Source_Description_411765489,
			IBaseM.Fn_GetEliteRefFromDbSource(A.DB_Source_387004609) AS  EliteRef,
			A.Last_Upd_Date AS Last_Upd_Date, 
			A.Last_Upd_User AS Last_Upd_User,
			A.Record_Status AS Record_Status, 
			A.SCC AS SCC, 
			A.Status_Binding AS Status_Binding,
			A.IconColour AS IconColour, 
			0 Police_Attended, 
			0 Ambulance_Attended, 
			1 Do_Not_Disseminate_412284494,
			'Keoghs CFS' AS Source_411765484,
			Description_ AS Party_Type,
			'Unknown' AS Sub_Party_Type,
			CONVERT(VARCHAR(300), C.Comments_) AS Incident_Circs,
			CONVERT(TIME, C.Incident_Time) AS Incident_Time,
			C.Insurer_,
			C.Insurer_Reference AS Claim_Number
	FROM IBaseM5Cur.Claim_ A WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd B WITH(NOLOCK) ON A.Unique_ID = B.Link_ID AND B.Entity_ID1 LIKE 'CLA%' AND B.Entity_ID2 NOT LIKE 'VEH%'
	INNER JOIN IBaseM5Cur.ClaimFile_ C WITH(NOLOCK) ON  B.Entity_ID1 = C.Unique_Id
			
	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Incident_Link
	EXECUTE IBaseM.uspTruncateTableSynonym  'IBaseM8Cur._Incident_Link_NextID'
	INSERT INTO IBaseM8Cur._Incident_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	UPDATE A SET Incident_Location = _ADDRESS
	FROM IBaseM8Cur.Incident_Link A
	INNER JOIN IBaseM.vGetClaimAddress B ON A.Unique_ID = B.ID

	--Case_to_Incident_Link (Nominal_Link)
	DELETE FROM  IBaseM8Cur._LinkEnd
	WHERE LEFT(Link_ID, 3) = 'CAS'

	INSERT INTO IBaseM8Cur._LinkEnd
	SELECT  
	'CAS' +  CONVERT(VARCHAR(10), ROW_NUMBER() OVER(ORDER BY I.Unique_ID)) AS Link_Id,
	K.Unique_ID AS EntityType_ID1, 
	0 AS Confidence,
	0 AS  Direction,
	I.Unique_ID AS EntityType_ID2,
	1750 EntityType_ID1,	 
	1688 EntityType_ID2,	
	2485 AS LinkType_ID,
	0 AS Record_Status,
	1 Record_Type,
	'' AS SCC
	FROM IBaseM8Cur.Keoghs_Case K WITH(NOLOCK)
	INNER JOIN IBaseM8Cur.Incident_ I WITH(NOLOCK) ON K.Keoghs_Case_ID =  I.Incident_ID

	INSERT INTO IBaseM8Cur._LinkEnd
	SELECT  Link_ID, Entity_ID2, Confidence, Direction, Entity_ID1, EntityType_ID2, EntityType_ID1, LinkType_ID, Record_Status, 2 AS  Record_Type, SCC
	FROM IBaseM8Cur._LinkEnd
	WHERE LEFT(Link_ID, 3) = 'CAS'

	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					--,@InsertRowCount= @InsertRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
END CATCH



