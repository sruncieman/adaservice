﻿

CREATE PROCEDURE  [Sync].[uspSynchroniseEntityVehicle]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseEntityVehicle]
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)ActionTheChangeInMDA_IBase
--						3.3)DoDeletes
--						3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)	
--						4.1)ActionTheChangeInMDA
--						4.2)DoDeletes
--						4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					5)UpdateLoggingAndTidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-05-08			Paul Allen		Created
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT			

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1), ChangeDatabase VARCHAR(10) ,MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)
					
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 116 --SynchroniseVehicleEntity
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, MDA_ID, DMLAction)
	SELECT 'MDA', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA.dbo.Vehicle, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA_IBase.dbo.Vehicle_, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	--2.2)AddSomeIndexesToMakeTheProcessQuicker
	SET @Step ='2.2'
	 
	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT(MDA_IBase_Unique_ID ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3'

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IV.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[Vehicle_] IV ON IV.Unique_ID = CT.MDA_IBase_Unique_ID
	AND IV.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MV.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].[Vehicle] MV ON MV.ID = CT.MDA_ID

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4'

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5'

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1'

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM MDA_IBase.[dbo].[_Vehicle_Link_NextID]
		UPDATE MDA_IBase.[dbo].[_Vehicle_Link_NextID] SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	'VEH' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY MDA_ID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--3.2)ActionTheChangeInMDA_IBase
		SET @Step ='3.2'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[Vehicle_] [Target]
		USING	(
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, MV.*, MVC.[Colour], MVF.[FuelText], MVT.[Text] [VehicleTransmissionText], MVTY.[Text] [VehicleTypeText], MVCL.[CatText]
				FROM #CT CT
				INNER JOIN [MDA].[dbo].[Vehicle] MV ON MV.ID = CT.MDA_ID
				INNER JOIN MDA.[dbo].[VehicleColour] MVC ON MVC.ID = MV.[VehicleColour_Id]
				INNER JOIN MDA.[dbo].[VehicleFuel] MVF ON MVF.ID = MV.[VehicleFuel_Id]
				INNER JOIN MDA.[dbo].[VehicleTransmission] MVT ON MVT.ID = MV.[VehicleTransmission_Id]
				INNER JOIN MDA.[dbo].[VehicleType] MVTY ON MVTY.ID = MV.[VehicleType_Id]
				INNER JOIN MDA.[dbo].[VehicleCategoryOfLoss] MVCL ON MVCL.ID = [VehicleCategoryOfLoss_Id]
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET  AltEntity						= NULLIF(Source.[VehicleTypeText],'Unknown')
			,Colour_						= CASE WHEN Source.[VehicleColour_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Colour_] ELSE NULLIF(Source.[Colour],'Unknown') END
			,Create_Date					= Source.[CreatedDate]
			,Create_User					= Source.[CreatedBy]
			,Engine_Capacity				= Source.[EngineCapacity]
			,Fuel_							= CASE WHEN Source.[VehicleFuel_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Fuel_] ELSE NULLIF(Source.[FuelText],'Unknown') END
			,Key_Attractor_412284410		= Source.[KeyAttractor]
			,Last_Upd_Date					= Source.[ModifiedDate]
			,Last_Upd_User					= Source.[ModifiedBy]
			,Make_							= Source.[VehicleMake]
			,Model_							= Source.[Model]
			,Notes_							= Source.[Notes]
			,Record_Status					= Source.[RecordStatus]
			,Source_411765484				= Source.[Source]
			,Source_Description_411765489	= Source.[SourceDescription]
			,Source_Reference_411765487		= Source.[SourceReference]
			,Transmission_					= CASE WHEN Source.[VehicleTransmission_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Transmission_] ELSE NULLIF(Source.[VehicleTransmissionText],'Unknown') END
			,Vehicle_Registration			= Source.[VehicleRegistration]
			,Vehicle_Type					= CASE WHEN Source.[VehicleType_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Vehicle_Type] ELSE NULLIF(Source.[VehicleTypeText],'Unknown') END
			,VIN_							= Source.[VIN]
		WHEN NOT MATCHED THEN INSERT
			(
			 Unique_ID
			,AltEntity
			,Colour_
			,Create_Date
			,Create_User
			,Do_Not_Disseminate_412284494
			,Engine_Capacity
			,Fuel_
			,Key_Attractor_412284410
			,Last_Upd_Date
			,Last_Upd_User
			,Make_
			,MDA_Incident_ID_412284502
			,Model_
			,Notes_
			,Record_Status
			,Source_411765484
			,Source_Description_411765489
			,Source_Reference_411765487
			,Transmission_
			,Vehicle_Registration
			,Vehicle_Type
			,VIN_
			)
		VALUES
			(
			 Source.MDA_IBase_Unique_ID
			,NULLIF(Source.[VehicleTypeText],'Unknown')
			,CASE WHEN Source.[VehicleColour_Id] = 0 THEN NULL ELSE Source.[Colour] END
			,Source.[CreatedDate]
			,Source.[CreatedBy]
			,0
			,Source.[EngineCapacity]
			,CASE WHEN Source.[VehicleFuel_Id] = 0 THEN NULL ELSE Source.[FuelText] END
			,Source.[KeyAttractor]
			,Source.[ModifiedDate]
			,Source.[ModifiedBy]
			,Source.[VehicleMake]
			,[Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Vehicle')
			,Source.[Model]
			,Source.[Notes]
			,Source.[RecordStatus]
			,Source.[Source]
			,Source.[SourceDescription]
			,Source.[SourceReference]
			,CASE WHEN Source.[VehicleTransmission_Id] = 0 THEN NULL ELSE Source.[VehicleTransmissionText] END
			,Source.[VehicleRegistration]
			,CASE WHEN Source.[VehicleType_Id] = 0 THEN NULL ELSE Source.[VehicleTypeText] END
			,Source.[VIN]
			)
		OUTPUT  @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Colour_,'') + '}|I:{' + ISNULL(Inserted.Colour_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Engine_Capacity,'') + '}|I:{' + ISNULL(Inserted.Engine_Capacity,'') + '}'
				,'D:{' + ISNULL(Deleted.Fuel_,'') + '}|I:{' + ISNULL(Inserted.Fuel_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Make_,'') + '}|I:{' + ISNULL(Inserted.Make_,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Model_,'') + '}|I:{' + ISNULL(Inserted.Model_,'') + '}'
				,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{' + ISNULL(Inserted.Notes_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.Transmission_,'') + '}|I:{' + ISNULL(Inserted.Transmission_,'') + '}'
				,'D:{' + ISNULL(Deleted.Vehicle_ID,'') + '}|I:{' + ISNULL(Inserted.Vehicle_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Vehicle_Registration,'') + '}|I:{' + ISNULL(Inserted.Vehicle_Registration,'') + '}'
				,'D:{' + ISNULL(Deleted.Vehicle_Type,'') + '}|I:{' + ISNULL(Inserted.Vehicle_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.VIN_,'') + '}|I:{' + ISNULL(Inserted.VIN_,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Vehicle_] (TaskID, DMLAction, Unique_ID, AltEntity, Colour_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engine_Capacity, Fuel_, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Make_, MDA_Incident_ID_412284502, Model_, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Transmission_, Vehicle_ID, Vehicle_Registration, Vehicle_Type, VIN_, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

		--3.3)DoDeletes
		SET @Step ='3.3';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[Vehicle_]
		OUTPUT @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Colour_,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Engine_Capacity,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Fuel_,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Make_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Model_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Transmission_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Vehicle_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Vehicle_Registration,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Vehicle_Type,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.VIN_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Vehicle_] (TaskID, DMLAction, Unique_ID, AltEntity, Colour_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engine_Capacity, Fuel_, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Make_, MDA_Incident_ID_412284502, Model_, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Transmission_, Vehicle_ID, Vehicle_Registration, Vehicle_Type, VIN_, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Vehicle_] IV
		INNER JOIN #CT CT ON [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident') = IV.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';

		--3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='3.4';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MV
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT  @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleId,'') + '}|I:{' + ISNULL(Inserted.VehicleId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleColour_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleColour_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleRegistration,'') + '}|I:{' + ISNULL(Inserted.VehicleRegistration,'') + '}'
				,'D:{' + ISNULL(Deleted.VehicleMake,'') + '}|I:{' + ISNULL(Inserted.VehicleMake,'') + '}'
				,'D:{' + ISNULL(Deleted.Model,'') + '}|I:{' + ISNULL(Inserted.Model,'') + '}'
				,'D:{' + ISNULL(Deleted.EngineCapacity,'') + '}|I:{' + ISNULL(Inserted.EngineCapacity,'') + '}'
				,'D:{' + ISNULL(Deleted.VIN,'') + '}|I:{' + ISNULL(Inserted.VIN,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleFuel_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleFuel_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleTransmission_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleTransmission_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleCategoryOfLoss_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleCategoryOfLoss_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.Notes,'') + '}|I:{' + ISNULL(Inserted.Notes,'') + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Vehicle] (TaskID, DMLAction, Id, VehicleId, VehicleColour_Id, VehicleRegistration, VehicleMake, Model, EngineCapacity, VIN, VehicleFuel_Id, VehicleTransmission_Id, VehicleType_Id, VehicleCategoryOfLoss_Id, Notes, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM MDA.dbo.Vehicle MV
		INNER JOIN #CT CT ON CT.MDA_ID = MV.Id
 		WHERE MV.IBaseId IS NULL
		OR MV.IBaseId != CT.MDA_IBase_Unique_ID;

	END

	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		SET @Step ='4.1'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].Vehicle [Target]
		USING  (
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, IV.*, ISNULL(MVC.ID,0) [VehicleColour_Id], ISNULL(MVF.ID,0) [VehicleFuel_Id], ISNULL(MVT.ID,0) [VehicleTransmission_Id], ISNULL(MVTY.ID,0) [VehicleType_Id], 0 [VehicleCategoryOfLoss_Id]
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Vehicle_] IV ON IV.Unique_ID = CT.MDA_IBase_Unique_ID
				LEFT JOIN MDA.[dbo].[VehicleColour] MVC ON MVC.[Colour] = IV.[Colour_]
				LEFT JOIN MDA.[dbo].[VehicleFuel] MVF ON MVF.[FuelText] = IV.[Fuel_]
				LEFT JOIN MDA.[dbo].[VehicleTransmission] MVT ON MVT.[Text] = IV.[Transmission_]
				LEFT JOIN MDA.[dbo].[VehicleType] MVTY ON MVTY.[Text] = IV.[Vehicle_Type]
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  VehicleColour_Id			= Source.[VehicleColour_Id]
			,VehicleRegistration		= Source.[Vehicle_Registration]
			,VehicleMake				= Source.[Make_]
			,Model						= Source.[Model_]
			,EngineCapacity				= Source.[Engine_Capacity]
			,VIN						= Source.[VIN_]
			,VehicleFuel_Id				= Source.[VehicleFuel_Id]
			,VehicleTransmission_Id		= Source.[VehicleTransmission_Id]
			,VehicleType_Id				= Source.[VehicleType_Id]
			,VehicleCategoryOfLoss_Id	= Source.[VehicleCategoryOfLoss_Id]
			,Notes						= Source.[Notes_]
			,KeyAttractor				= Source.[Key_Attractor_412284410]
			,Source						= Source.[Source_411765484]
			,SourceReference			= Source.[Source_Reference_411765487]
			,SourceDescription			= Source.[Source_Reference_411765487]
			,CreatedBy					= Source.[Create_User]
			,CreatedDate				= Source.[Create_Date]
			,ModifiedBy					= Source.[Last_Upd_User]
			,ModifiedDate				= Source.[Last_Upd_Date]
			,IBaseId					= Source.[Unique_ID]
			,RecordStatus				= Source.[Record_Status]
			,ADARecordStatus			= CASE WHEN Source.Record_Status = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
			 VehicleId
			,VehicleColour_Id
			,VehicleRegistration
			,VehicleMake
			,Model
			,EngineCapacity
			,VIN
			,VehicleFuel_Id
			,VehicleTransmission_Id
			,VehicleType_Id
			,VehicleCategoryOfLoss_Id
			,Notes
			,KeyAttractor
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES
			(
			 Source.Vehicle_Id
			,Source.[VehicleColour_Id]
			,Source.[Vehicle_Registration]
			,Source.[Make_]
			,Source.[Model_]
			,Source.[Engine_Capacity]
			,Source.[VIN_]
			,Source.[VehicleFuel_Id]
			,Source.[VehicleTransmission_Id]
			,Source.[VehicleType_Id]
			,Source.[VehicleCategoryOfLoss_Id]
			,Source.[Notes_]
			,Source.[Key_Attractor_412284410]
			,Source.[Source_411765484]
			,Source.[Source_Reference_411765487]
			,Source.[Source_Description_411765489]
			,Source.[Create_User]
			,Source.[Create_Date]
			,Source.[Last_Upd_User]
			,Source.[Last_Upd_Date]
			,Source.[Unique_ID]
			,Source.[Record_Status]
			,10
			)
		OUTPUT   @OutputTaskID
				,$Action
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleId,'') + '}|I:{' + ISNULL(Inserted.VehicleId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleColour_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleColour_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleRegistration,'') + '}|I:{' + ISNULL(Inserted.VehicleRegistration,'') + '}'
				,'D:{' + ISNULL(Deleted.VehicleMake,'') + '}|I:{' + ISNULL(Inserted.VehicleMake,'') + '}'
				,'D:{' + ISNULL(Deleted.Model,'') + '}|I:{' + ISNULL(Inserted.Model,'') + '}'
				,'D:{' + ISNULL(Deleted.EngineCapacity,'') + '}|I:{' + ISNULL(Inserted.EngineCapacity,'') + '}'
				,'D:{' + ISNULL(Deleted.VIN,'') + '}|I:{' + ISNULL(Inserted.VIN,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleFuel_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleFuel_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleTransmission_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleTransmission_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleCategoryOfLoss_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleCategoryOfLoss_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.Notes,'') + '}|I:{' + ISNULL(Inserted.Notes,'') + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Vehicle] (TaskID, DMLAction, Id, VehicleId, VehicleColour_Id, VehicleRegistration, VehicleMake, Model, EngineCapacity, VIN, VehicleFuel_Id, VehicleTransmission_Id, VehicleType_Id, VehicleCategoryOfLoss_Id, Notes, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus);

		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Vehicle]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleId,'') + '}|I:{' + ISNULL(Inserted.VehicleId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleColour_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleColour_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.VehicleRegistration,'') + '}|I:{' + ISNULL(Inserted.VehicleRegistration,'') + '}'
				,'D:{' + ISNULL(Deleted.VehicleMake,'') + '}|I:{' + ISNULL(Inserted.VehicleMake,'') + '}'
				,'D:{' + ISNULL(Deleted.Model,'') + '}|I:{' + ISNULL(Inserted.Model,'') + '}'
				,'D:{' + ISNULL(Deleted.EngineCapacity,'') + '}|I:{' + ISNULL(Inserted.EngineCapacity,'') + '}'
				,'D:{' + ISNULL(Deleted.VIN,'') + '}|I:{' + ISNULL(Inserted.VIN,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleFuel_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleFuel_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleTransmission_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleTransmission_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VehicleCategoryOfLoss_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VehicleCategoryOfLoss_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.Notes,'') + '}|I:{' + ISNULL(Inserted.Notes,'') + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Vehicle] (TaskID, DMLAction, Id, VehicleId, VehicleColour_Id, VehicleRegistration, VehicleMake, Model, EngineCapacity, VIN, VehicleFuel_Id, VehicleTransmission_Id, VehicleType_Id, VehicleCategoryOfLoss_Id, Notes, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Vehicle] MV
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MV.IBaseId
		WHERE CT.DMLAction = 'D';

		--4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='4.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE IV
		SET MDA_Incident_ID_412284502 = [Sync].[fn_ResolveMDAID] (MV.ID,'MDA.dbo.Incident')
		OUTPUT  @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Colour_,'') + '}|I:{' + ISNULL(Inserted.Colour_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Engine_Capacity,'') + '}|I:{' + ISNULL(Inserted.Engine_Capacity,'') + '}'
				,'D:{' + ISNULL(Deleted.Fuel_,'') + '}|I:{' + ISNULL(Inserted.Fuel_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Make_,'') + '}|I:{' + ISNULL(Inserted.Make_,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Model_,'') + '}|I:{' + ISNULL(Inserted.Model_,'') + '}'
				,'D:{' + ISNULL(Deleted.Notes_,'') + '}|I:{' + ISNULL(Inserted.Notes_,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.Transmission_,'') + '}|I:{' + ISNULL(Inserted.Transmission_,'') + '}'
				,'D:{' + ISNULL(Deleted.Vehicle_ID,'') + '}|I:{' + ISNULL(Inserted.Vehicle_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Vehicle_Registration,'') + '}|I:{' + ISNULL(Inserted.Vehicle_Registration,'') + '}'
				,'D:{' + ISNULL(Deleted.Vehicle_Type,'') + '}|I:{' + ISNULL(Inserted.Vehicle_Type,'') + '}'
				,'D:{' + ISNULL(Deleted.VIN_,'') + '}|I:{' + ISNULL(Inserted.VIN_,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Vehicle_] (TaskID, DMLAction, Unique_ID, AltEntity, Colour_, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engine_Capacity, Fuel_, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Make_, MDA_Incident_ID_412284502, Model_, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Transmission_, Vehicle_ID, Vehicle_Registration, Vehicle_Type, VIN_, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Vehicle_] IV
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IV.Unique_ID
		INNER JOIN [MDA].[dbo].[Vehicle] MV ON MV.IBaseId = IV.Unique_ID
 		WHERE IV.MDA_Incident_ID_412284502 IS NULL
		OR [Sync].[fn_ResolveMDAID] (MV.ID,'MDA.dbo.Vehicle') != IV.MDA_Incident_ID_412284502;

	END

	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Vehicle_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Vehicle_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Vehicle_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

END TRY
BEGIN CATCH

	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +@Step +']'

	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

