﻿CREATE PROCEDURE [Sync].[uspSynchroniseMaster] (@Debug BIT = 0)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--DeclareVariablesTempObjectsAndStartLogiing
	----------------------------------------------------
	DECLARE  @Now							DATETIME 
			,@Details						VARCHAR(1000) = 'User=['+system_user+']'	
			,@OutputRunId					INT	
			,@OutputProcessId				INT
			,@OutputTaskId					INT
			,@DatabaseId					INT = DB_Id()
			,@ObjectId						INT = ISNULL(@@PROCId,-1)
			,@MDADatabaseId					INT = DB_Id('MDA')
			,@MDAChangeVersionFrom			INT
			,@MDAChangeVersionTo			INT
			,@MDA_IBaseDatabaseId			INT = DB_Id('MDA_IBase')
			,@MDA_IBaseChangeVersionFrom	INT
			,@MDA_IBaseChangeVersionTo		INT
			,@Step							NVARCHAR(10) = '0'

	--CreateTheRunRecord(Synch_MDA_IBaseToMDA)	
	EXECUTE dbo.uspCRUDDBControlRun @RunNameId = 76, @Details = @Details ,@OutputRunId = @OutputRunId OUTPUT
	--CreateTheProcessRecord(Synchronisastion)
	EXECUTE dbo.uspCRUDDBControlProcess @RunId	= @OutputRunId ,@ProcessNameId = 82 ,@Details = @Details ,@DatabaseId = @DatabaseId ,@ObjectId = @ObjectId ,@OutputProcessId = @OutputProcessId OUTPUT
	--CreateTheTaskRecord(SychronisationWrapper)
	EXECUTE dbo.uspCRUDDBControlTask @ProcessId	= @OutputProcessId, @TaskNameId	= 83, @DatabaseId = @DatabaseId, @ObjectId = @ObjectId, @Details = @Details, @OutputTaskId = @OutputTaskId OUTPUT
	
	BEGIN TRANSACTION
		----------------------------------------------------
		--GetTheLatestChangeVersions
		----------------------------------------------------
		EXECUTE Sync.uspGetChangeTrackingFromToVersionsAndLog @MDADatabaseId, @OutputRunId, @MDAChangeVersionFrom OUTPUT, @MDAChangeVersionTo OUTPUT
		EXECUTE Sync.uspGetChangeTrackingFromToVersionsAndLog @MDA_IBaseDatabaseId, @OutputRunId, @MDA_IBaseChangeVersionFrom OUTPUT, @MDA_IBaseChangeVersionTo OUTPUT

		----------------------------------------------------
		--SynchroniseTheEntities
		----------------------------------------------------
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityAccount]'
		EXECUTE [Sync].[uspSynchroniseEntityAccount]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityAddress]'
		EXECUTE [Sync].[uspSynchroniseEntityAddress]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityBBPin]'
		EXECUTE [Sync].[uspSynchroniseEntityBBPin]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityDrivingLicence]'
		EXECUTE [Sync].[uspSynchroniseEntityDrivingLicence]		@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityEmail]'
		EXECUTE [Sync].[uspSynchroniseEntityEmail]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityFraudRing]'		
		EXECUTE [Sync].[uspSynchroniseEntityFraudRing]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityIncident]'
		EXECUTE [Sync].[uspSynchroniseEntityIncident]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityIPAddress]'
		EXECUTE [Sync].[uspSynchroniseEntityIPAddress]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityKeoghsCase]'
		EXECUTE [Sync].[uspSynchroniseEntityKeoghsCase]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityNINumber]'
		EXECUTE [Sync].[uspSynchroniseEntityNINumber]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityOrganisation]'
		EXECUTE [Sync].[uspSynchroniseEntityOrganisation]		@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityPassport]'
		EXECUTE [Sync].[uspSynchroniseEntityPassport]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityPaymentCard]'
		EXECUTE [Sync].[uspSynchroniseEntityPaymentCard]		@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityPerson]'
		EXECUTE [Sync].[uspSynchroniseEntityPerson]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityPolicy]'
		EXECUTE [Sync].[uspSynchroniseEntityPolicy]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityTelephone]'
		EXECUTE [Sync].[uspSynchroniseEntityTelephone]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityVehicle]'
		EXECUTE [Sync].[uspSynchroniseEntityVehicle]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityWebSite]'
		EXECUTE [Sync].[uspSynchroniseEntityWebSite]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseEntityHandset]'
		EXECUTE [Sync].[uspSynchroniseEntityHandset]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT

		----------------------------------------------------
		--SynchroniseTheLinks
		----------------------------------------------------
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkAccount]'
		EXECUTE [Sync].[uspSynchroniseLinkAccount]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkAddress]'
		EXECUTE [Sync].[uspSynchroniseLinkAddress]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkBBPin]'
		EXECUTE [Sync].[uspSynchroniseLinkBBPin]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkDrivingLicence]'
		EXECUTE [Sync].[uspSynchroniseLinkDrivingLicence]		@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkEmail]'
		EXECUTE [Sync].[uspSynchroniseLinkEmail]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkFraudRing]'
		EXECUTE [Sync].[uspSynchroniseLinkFraudRing]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkIncident]'
		EXECUTE [Sync].[uspSynchroniseLinkIncident]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkIncidentToIncident]'
		EXECUTE [Sync].[uspSynchroniseLinkIncidentToIncident]	@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkIPAddress]'
		EXECUTE [Sync].[uspSynchroniseLinkIPAddress]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkKeoghsCase]'
		EXECUTE [Sync].[uspSynchroniseLinkKeoghsCase]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkNINumber]'
		EXECUTE [Sync].[uspSynchroniseLinkNINumber]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkOrganisation]'
		EXECUTE [Sync].[uspSynchroniseLinkOrganisation]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkOutcome]'
		EXECUTE [Sync].[uspSynchroniseLinkOutcome]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkPassport]'
		EXECUTE [Sync].[uspSynchroniseLinkPassport]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkPaymentCard]'
		EXECUTE [Sync].[uspSynchroniseLinkPaymentCard]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkPersonToPerson]'
		EXECUTE [Sync].[uspSynchroniseLinkPersonToPerson]		@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkPersonToOrganisation]'
		EXECUTE [Sync].[uspSynchroniseLinkPersonToOrganisation]	@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkPolicy]'
		EXECUTE [Sync].[uspSynchroniseLinkPolicy]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkPolicyPaymentType]'
		EXECUTE [Sync].[uspSynchroniseLinkPolicyPaymentType]	@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkTelephone]'
		EXECUTE [Sync].[uspSynchroniseLinkTelephone]			@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkVehicle]'
		EXECUTE [Sync].[uspSynchroniseLinkVehicle]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkVehicleIncident]'
		EXECUTE [Sync].[uspSynchroniseLinkVehicleIncident]		@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkVehicleToVehicle]'
		EXECUTE [Sync].[uspSynchroniseLinkVehicleToVehicle]		@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkWebSite]'
		EXECUTE [Sync].[uspSynchroniseLinkWebSite]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkHandsetToHandset]'
		EXECUTE [Sync].[uspSynchroniseLinkHandsetToHandset]		@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkHandest]'
		EXECUTE [Sync].[uspSynchroniseLinkHandest]				@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
		IF @Debug = 1 PRINT 'Executing:[uspSynchroniseLinkHandsetIncident]'
		EXECUTE [Sync].[uspSynchroniseLinkHandsetIncident]		@MDAChangeVersionFrom, @MDAChangeVersionTo, @MDA_IBaseChangeVersionFrom, @MDA_IBaseChangeVersionTo, @OutputTaskId, @Step OUTPUT
	COMMIT TRANSACTION

	----------------------------------------------------
	--UpdateTheLoggingRecord
	----------------------------------------------------
	SELECT	 @Now =  GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskId		= @OutputTaskId
				,@ProcessId		= @OutputProcessId
				,@TaskEndTime	= @Now
				,@OutputTaskId	= @OutputTaskId OUTPUT
						
	EXECUTE dbo.uspCRUDDBControlProcess
					 @Action			= 'U'
					,@ProcessId			= @OutputProcessId
					,@RunId				= @OutputRunId
					,@ProcessEndTime	= @Now
					,@OutputProcessId	= @OutputProcessId OUTPUT

	EXECUTE [dbo].[uspCRUDDBControlRun] 
					 @Action		= 'U'
					,@RunId			= @OutputRunId
					,@RunEndTime	= @Now
					,@OutputRunId	= @OutputRunId OUTPUT


END TRY
BEGIN CATCH
	IF XACT_STATE() = -1
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_Id('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	DECLARE		 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity		INT				= ERROR_SEVERITY()
				,@ErrorState		INT				= ERROR_STATE()
				,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
				,@ErrorLine			INT				= ERROR_LINE()

	SELECT   @Now		= GETDATE()				
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskId			= @OutputTaskId
				,@ProcessId			= @OutputProcessId
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskId		= @OutputTaskId OUTPUT

	SELECT @ErrorMessage +=' [TaskId='+CAST(ISNULL(@OutputTaskId,-1) AS VARCHAR)+']'		
	EXECUTE dbo.uspCRUDDBControlProcess
				@Action			= 'U'
				,@ProcessId			= @OutputProcessId
				,@RunId				= @OutputRunId
				,@ProcessEndTime	= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputProcessId	= @OutputProcessId OUTPUT

	EXECUTE  [dbo].[uspCRUDDBControlRun] 
				 @Action			= 'U'
				,@RunId				= @OutputRunId
				,@RunEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription  = @ErrorMessage
				,@OutputRunId		= @OutputRunId OUTPUT

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +@Step +']'

END CATCH