﻿

CREATE PROCEDURE [IBaseM].[uspMigrationStep03]
/**************************************************************************************************/
-- ObjectName:			MDAControl.IBaseM.uspMigrationStep03
--
-- Description:			Populate the link tables within IBase8 Target database
--
-- Steps:				1)ConfigureLogging,Create/DeclareRequiredObjects
--							1.1)ConfigureLogging
--							1.2)Create/DeclareRequiredObjects
--						2)InsertDataIntoTable
--							2.1)Person_to_Organisation_Link (Nominal_Link)
--							2.2)Person_to_Person_Link (Nominal_Link)
--							2.3)Vehicle_to_Vehicle_Link (Nominal_Link)
--							2.4)Address_to_Address_Link (Nominal_Link)
--							2.5)Driving Licence (Nominal_Link)
--							2.6)Email (Nominal_Link)
--							2.7)IP Address (Nominal_Link)
--							2.8)NI Number (Nominal_Link)
--							2.9)Passport (Nominal_Link)
--							2.10)Payment Card (Nominal_Link)
--							2.11)BB Pin (Nominal_Link)
--							2.12)Fraud Ring Incident (Nominal_Link)
--							2.13)Telephone (Nominal_Link)
--							2.14)Website (Nominal_Link)
--							2.15)Policy (Nominal_Link)
--							2.16)Vehicle (Nominal_Link) 
--							2.17)Account (Nominal_Link)
--							2.18)Intelligence (Nominal_Link)
--							2.19)Policy_Payment_Link
--							2.20)Incident_Match_Link
--							2.21)Vehicle_to_incident_Link
--							2.22)Organisation_Match_Link 
--							2.23)IBaseM.Intelligence_Link
--						3)UpdateLogging/Tidy
--
-- Dependencies:		IBaseM.uspTruncateTableSynonym
--						IBaseM.Fn_GetEliteRefFromDbSource
--
-- Usage:				Called within SP IBaseM.uspMigrationMaster
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-26			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 10 --Populate_Links
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
			
	--1.2)Create/DeclareRequiredObjects
	DECLARE  @NextID INT
			,@Source VARCHAR(20) = 'Keoghs CFS'
			,@InsertRowCount INT = 0
		
	------------------------------------------------------------
	--2)InsertDataIntoTable
	------------------------------------------------------------	
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Person_to_Organisation_Link'
	INSERT INTO IBaseM8Cur.Person_to_Organisation_Link(Unique_ID, AltEntity, Create_Date, Create_User, IconColour, Last_Upd_Date, Last_Upd_User, Link_Type, Person_to_Organisation_Link_ID, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, 
		Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402)
	SELECT DISTINCT
		STUFF(a.Unique_ID, 1, 3, 'PE0')	COLLATE DATABASE_DEFAULT AS	Unique_ID, 
		a.AltEntity	COLLATE DATABASE_DEFAULT AS AltEntity, 
		a.Create_Date AS Create_Date, 
		a.Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		a.IconColour AS	IconColour,
		a.Last_Upd_Date	AS	Last_Upd_Date, 
		a.Last_Upd_User	COLLATE DATABASE_DEFAULT AS	Last_Upd_User, 
		a.Description_ AS Link_type,
		a.Unique_ID AS	Person_to_Organisation_Link_ID, 
		a.Record_Status	AS	Record_Status, 
		a.SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source AS	Source_411765484, 
		a.DB_Source_387004609 COLLATE DATABASE_DEFAULT AS Source_Description_411765489, 
		IBaseM.Fn_GetEliteRefFromDbSource(a.DB_Source_387004609) AS  EliteRef, 
		a.Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding,
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Nominal_Link AS a  WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd AS b  WITH(NOLOCK) ON a.Unique_ID = b.Link_ID
	WHERE (LEFT(b.Entity_ID1, 3) = 'PER') 
	AND (LEFT(b.Entity_ID2, 3) = 'ORG')
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Person_to_Organisation_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Person_to_Organisation_Link_NextID'
	INSERT INTO IBaseM8Cur._Person_to_Organisation_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.2)Person_to_Person_Link (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Person_to_Person_Link'
	INSERT INTO IBaseM8Cur.Person_to_Person_Link(Unique_ID, AltEntity, Create_Date, Create_User, IconColour, Last_Upd_Date, Last_Upd_User, Link_Type, 
	Person_to_Person_Link_ID, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding,
	Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402)
	SELECT DISTINCT
		STUFF(a.Unique_ID, 1, 3, 'PE1')	COLLATE DATABASE_DEFAULT AS	Unique_ID, 
		a.AltEntity	COLLATE DATABASE_DEFAULT AS AltEntity, 
		a.Create_Date AS Create_Date, 
		a.Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		a.IconColour AS	IconColour,
		a.Last_Upd_Date	AS	Last_Upd_Date, 
		a.Last_Upd_User	COLLATE DATABASE_DEFAULT AS	Last_Upd_User, 
		a.Description_ AS Link_Type, 
		a.Unique_ID AS	Person_to_Person_Link_ID, 
		a.Record_Status	AS	Record_Status, 
		a.SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source AS	Source_, 
		a.DB_Source_387004609 COLLATE DATABASE_DEFAULT AS Source_Description,
		IBaseM.Fn_GetEliteRefFromDbSource(a.DB_Source_387004609) AS  EliteRef,
		a.Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding,
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Nominal_Link AS a  WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd AS b  WITH(NOLOCK)ON a.Unique_ID = b.Link_ID
	WHERE (LEFT(b.Entity_ID1, 3) = 'PER') 
	AND (LEFT(b.Entity_ID2, 3) = 'PER')
	SELECT @InsertRowCount += @@ROWCOUNT
		
	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Person_to_Person_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Person_to_Person_Link_NextID'
	INSERT INTO IBaseM8Cur._Person_to_Person_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)
	
	--2.3)Vehicle_to_Vehicle_Link (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Vehicle_to_Vehicle_Link'
	INSERT INTO IBaseM8Cur.Vehicle_to_Vehicle_Link
		(Unique_ID, AltEntity, Create_Date, Create_User, Date_fo_Reg_Change, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Vehicle_to_Vehicle_Link_ID, x5x5x5_Grading_412284402)
	SELECT DISTINCT
		STUFF(a.Unique_ID, 1, 3, 'VE2')	COLLATE DATABASE_DEFAULT AS	Unique_ID, 
		a.AltEntity	COLLATE DATABASE_DEFAULT AS AltEntity, 
		a.Create_Date AS Create_Date, 
		a.Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		NULL AS Date_fo_Reg_Change,
		0 Do_Not_Disseminate_412284494,
		a.IconColour AS	IconColour,
		NULL Key_Attractor_412284410,
		a.Last_Upd_Date	AS	Last_Upd_Date, 
		a.Last_Upd_User	COLLATE DATABASE_DEFAULT AS	Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,	
		a.Record_Status	AS	Record_Status, 
		a.SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source AS Source_, 
		a.DB_Source_387004609 COLLATE DATABASE_DEFAULT AS Source_Description,
		IBaseM.Fn_GetEliteRefFromDbSource(a.DB_Source_387004609) AS  EliteRef,
		a.Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding, 
		a.Unique_ID AS	Vehicle_to_Vehicle_Link_ID,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Vehicle_Link a WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd AS b  WITH(NOLOCK) ON a.Unique_ID = b.Link_ID
	WHERE (LEFT(b.Entity_ID1, 3) = 'VEH') 
	AND (LEFT(b.Entity_ID2, 3) = 'VEH')
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Vehicle_to_Vehicle_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Vehicle_to_Vehicle_Link_NextID'
	INSERT INTO IBaseM8Cur._Vehicle_to_Vehicle_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)
		
	--2.4)Address_to_Address_Link (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Address_to_Address_Link '
	INSERT INTO IBaseM8Cur.Address_to_Address_Link 
		(Unique_ID, Address_Link_Type, Address_to_Address_Link_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, End_of_Residency, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Start_of_Residency, Status_Binding, x5x5x5_Grading_412284402)
	SELECT DISTINCT
		STUFF(a.Unique_ID, 1, 3, 'AD0')	COLLATE DATABASE_DEFAULT AS	Unique_ID,
		Description_ AS Address_Link_Type,
		a.Unique_ID AS Address_to_Address_Link_ID, 
		a.AltEntity	COLLATE DATABASE_DEFAULT AS AltEntity, 
		a.Create_Date AS Create_Date, 
		a.Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		0 Do_Not_Disseminate_412284494,
		NULL AS End_of_Residency,
		a.IconColour AS	IconColour,
		NULL Key_Attractor_412284410,
		a.Last_Upd_Date	AS	Last_Upd_Date, 
		a.Last_Upd_User	COLLATE DATABASE_DEFAULT AS	Last_Upd_User, 
		NULL MDA_Incident_ID_412284502, 
		NULL AS Notes_,
		a.Record_Status	AS	Record_Status, 
		a.SCC COLLATE DATABASE_DEFAULT AS SCC, 
		--@Source AS	Source_,
		'Keoghs CFS'  AS Source_,
		a.DB_Source_387004609 COLLATE DATABASE_DEFAULT AS Source_Description,
		IBaseM.Fn_GetEliteRefFromDbSource(a.DB_Source_387004609) AS  EliteRef,
		NULL AS	Start_of_Residency, 
		a.Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Location_Link AS a WITH (NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd AS b WITH (NOLOCK)ON a.Unique_ID = b.Link_ID
	WHERE  Entity_ID1 NOT LIKE 'TEL%' OR Entity_ID2 NOT LIKE 'TEL%'
	SELECT @InsertRowCount += @@ROWCOUNT	

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Address_to_Address_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Address_to_Address_Link_NextID'
	INSERT INTO IBaseM8Cur._Address_to_Address_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.5)Driving Licence (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Driving_Licence_Link'
	INSERT INTO IBaseM8Cur.Driving_Licence_Link
		(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, Driving_Licence_Link_ID, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT 
		'DR0' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY n.Unique_ID)) AS Unique_ID,
		n.AltEntity AS AltEntity, 
		n.Create_Date AS Create_Date,
		n.Create_User AS Create_User, 
		0 Do_Not_Disseminate_412284494,
		n.Unique_ID Driving_Licence_Link_ID, 
		n.IconColour AS IconColour, 
		NULL Key_Attractor_412284410,
		n.Last_Upd_Date AS Last_Upd_Date,
		n.Last_Upd_User AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		n.Record_Status AS Record_Status, 
		n.SCC AS SCC, 
		@Source Source_411765484, 
		n.DB_Source_387004609 AS Source_Description_411765489,
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef,
		n.Status_Binding AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Person_ n WITH(NOLOCK)
	INNER JOIN IBaseM8Cur.Driving_Licence L WITH(NOLOCK) ON n.Unique_ID = l.Driving_Licence_ID
	SELECT @InsertRowCount += @@ROWCOUNT
		
	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1  FROM  IBaseM8Cur.Driving_Licence_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Driving_Licence_Link_NextID'
	INSERT INTO IBaseM8Cur._Driving_Licence_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.6)Email (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Email_Link'
	INSERT INTO IBaseM8Cur.Email_Link
		(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, Email_Link_ID, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT 	
		'EM0' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Email_Link_ID)) AS Unique_ID,
		*
	FROM(
	SELECT DISTINCT
		n.AltEntity AS AltEntity,
		n.Create_Date AS Create_Date, 
		n.Create_User AS Create_User, 
		0 Do_Not_Disseminate_412284494,
		n.Unique_ID Email_Link_ID, 
		n.IconColour AS IconColour, 
		NULL Key_Attractor_412284410,
		n.Last_Upd_Date AS Last_Upd_Date, 
		n.Last_Upd_User AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		n.Record_Status AS Record_Status, 
		n.SCC AS SCC, 
		@Source Source_411765484, 
		n.DB_Source_387004609 Source_Description_411765489,
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef,
		n.Status_Binding AS Status_Binding, 
		x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Nominal_Link n WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd e WITH(NOLOCK) ON n.Unique_ID =  e.Link_ID  
	INNER JOIN IBaseM8Cur.Email_ x WITH(NOLOCK) ON e.Entity_ID1 = x.Email_ID COLLATE DATABASE_DEFAULT 
	) X
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Email_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Email_Link_NextID'
	INSERT INTO IBaseM8Cur._Email_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.7)IP Address (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.IP_Address_Link'
	INSERT INTO IBaseM8Cur.IP_Address_Link
		(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, IP_Address_Link_ID, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT 
		'IP0' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY IP_Address_Link_ID)) AS Unique_ID,
		*
	FROM(
	SELECT DISTINCT
		n.AltEntity AS AltEntity, 
		n.Create_Date AS Create_Date, 
		n.Create_User AS Create_User,
		0 Do_Not_Disseminate_412284494,
		n.IconColour AS IconColour, 
		n.Unique_ID IP_Address_Link_ID, 
		NULL Key_Attractor_412284410,
		n.Last_Upd_Date AS Last_Upd_Date, 
		n.Last_Upd_User AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		n.Record_Status AS Record_Status, 
		n.SCC AS SCC, 
		@Source Source_411765484, 
		n.DB_Source_387004609  Source_Description_411765489, 
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef,
		n.Status_Binding AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Nominal_Link n WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd e WITH(NOLOCK) ON n.Unique_ID =  e.Link_ID  
	INNER JOIN IBaseM8Cur.IP_Address x WITH(NOLOCK) ON e.Entity_ID1 = x.IP_Address_ID COLLATE DATABASE_DEFAULT 
	) X
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.IP_Address_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._IP_Address_Link_NextID'
	INSERT INTO IBaseM8Cur._IP_Address_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.8)NI Number (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.NI_Number_Link'
	INSERT INTO IBaseM8Cur.NI_Number_Link
		(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, NI_Number_Link_ID, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT 
		'NI0' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY n.Unique_ID)) AS Unique_ID,
		n.AltEntity AS AltEntity, 
		n.Create_Date AS Create_Date, 
		n.Create_User AS Create_User, 
		0 Do_Not_Disseminate_412284494,	
		n.IconColour AS IconColour, 
		NULL Key_Attractor_412284410,
		n.Last_Upd_Date AS Last_Upd_Date, 
		n.Last_Upd_User AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,
		n.Unique_ID NI_Number_Link_ID, 
		n.Record_Status AS Record_Status,
		n.SCC AS SCC,
		@Source Source_411765484, 
		n.DB_Source_387004609  Source_Description_411765489, 
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef,
		n.Status_Binding AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Person_ n WITH(NOLOCK)
	INNER JOIN IBaseM8Cur.NI_Number L WITH(NOLOCK) ON n.Unique_ID = l.NI_Number_ID
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.NI_Number_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._NI_Number_Link_NextID'
	INSERT INTO IBaseM8Cur._NI_Number_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.9)Passport (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Passport_Link'
	INSERT INTO IBaseM8Cur.Passport_Link
		(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Passport_Link_ID, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT
		'PA1' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY n.Unique_ID)) AS Unique_ID,
		n.AltEntity AS AltEntity, 
		n.Create_Date AS Create_Date, 
		n.Create_User AS Create_User, 
		0 Do_Not_Disseminate_412284494,
		n.IconColour AS IconColour, 
		NULL Key_Attractor_412284410,	
		n.Last_Upd_Date AS Last_Upd_Date, 
		n.Last_Upd_User AS Last_Upd_User, 
		NULL MDA_Incident_ID_412284502,	
		n.Unique_ID Passport_Link_ID,	
		n.Record_Status AS Record_Status, 
		n.SCC AS SCC, 
		@Source Source_411765484, 
		n.DB_Source_387004609  Source_Description_411765489, 
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef,
		n.Status_Binding AS Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Person_ n WITH(NOLOCK)
	WHERE Passport_Number IS NOT NULL
	SELECT @InsertRowCount += @@ROWCOUNT
	
	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM  IBaseM8Cur.Passport_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Passport_Link_NextID'
	INSERT INTO IBaseM8Cur._Passport_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.10)Payment Card (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Payment_Card_Link'
	INSERT INTO IBaseM8Cur.Payment_Card_Link
		(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Payment_Card_Link_ID, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT DISTINCT
		STUFF(F.Unique_ID, 1, 3, 'PA0')	COLLATE DATABASE_DEFAULT AS	Unique_ID, 
		F.AltEntity	COLLATE DATABASE_DEFAULT AS AltEntity, 
		F.Create_Date AS Create_Date,
		F.Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		0 Do_Not_Disseminate_412284494,
		F.IconColour AS	IconColour,
		NULL Key_Attractor_412284410,
		F.Last_Upd_Date	AS	Last_Upd_Date, 
		F.Last_Upd_User	COLLATE DATABASE_DEFAULT AS	Last_Upd_User,
		NULL MDA_Incident_ID_412284502,
		F.Unique_ID COLLATE DATABASE_DEFAULT AS	Payment_Card_Link_ID,
		F.Record_Status	AS	Record_Status, 
		F.SCC COLLATE DATABASE_DEFAULT AS SCC,
		@Source AS Source_411765484,
		F.DB_Source_387004609 COLLATE DATABASE_DEFAULT AS 	 Source_Description_411765489, 
		IBaseM.Fn_GetEliteRefFromDbSource(a.DB_Source_387004609) AS  EliteRef,
		F.Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Finance_Link  AS F WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd LE WITH(NOLOCK) ON F.Unique_ID = LE.Link_ID
	INNER JOIN IBaseM5Cur.Account_ A WITH(NOLOCK) ON A.Unique_ID = LE.Entity_ID1
	WHERE A.AltEntity  != 'Account' AND LEFT(Entity_ID2, 3) != 'INS'
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM  IBaseM8Cur.Payment_Card_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Payment_Card_Link_NextID'
	INSERT INTO IBaseM8Cur._Payment_Card_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.11)BB Pin (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.BB_Pin_Link'
	INSERT INTO IBaseM8Cur.BB_Pin_Link
		(Unique_ID, Create_User, Create_Date, Last_Upd_User, Last_Upd_Date, SCC, Record_Status, Status_Binding, AltEntity, IconColour, Source_411765484, 
		Source_Reference_411765487, Source_Description_411765489, BB_Pin_Link_ID, 
		Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402)

	SELECT 
	'BB_' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Unique_ID)) AS Unique_ID,
	*
	FROM (
	SELECT DISTINCT
		n.Create_User AS Create_User, 
		n.Create_Date AS Create_Date, 
		n.Last_Upd_User AS Last_Upd_User, 
		n.Last_Upd_Date AS Last_Upd_Date, 
		n.SCC AS SCC, 
		n.Record_Status AS Record_Status, 
		n.Status_Binding AS Status_Binding, 
		n.AltEntity AS AltEntity, 
		n.IconColour AS IconColour, 
		@Source Source_411765484,  
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef,
		n.DB_Source_387004609 Source_Description_411765489, 
		n.Unique_ID, --###CHECK### 
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Nominal_Link n WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd e WITH(NOLOCK) ON n.Unique_ID =  e.Link_ID  
	INNER JOIN IBaseM8Cur.BBPin_ x WITH(NOLOCK) ON e.Entity_ID1 = x.BB_Pin_ID COLLATE DATABASE_DEFAULT
	) X
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM  IBaseM8Cur.BB_Pin_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._BB_Pin_Link_NextID'
	INSERT INTO IBaseM8Cur._BB_Pin_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.12)Fraud Ring Incident (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Fraud_Ring_Incident_Link'
	INSERT INTO IBaseM8Cur.Fraud_Ring_Incident_Link(Unique_ID, AltEntity, Create_Date, Create_User, Fraud_Ring_Incident_Link_ID, IconColour, Last_Upd_Date, 
	Last_Upd_User, Link_Type, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402)
	SELECT DISTINCT
		STUFF(a.Unique_ID, 1, 3, 'FR0')	COLLATE DATABASE_DEFAULT AS	Unique_ID, 
		a.AltEntity	COLLATE DATABASE_DEFAULT AS AltEntity, 
		a.Create_Date AS Create_Date, 
		a.Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		a.Unique_ID AS Fraud_Ring_Incident_Link_ID,
		a.IconColour AS	IconColour,
		a.Last_Upd_Date	AS	Last_Upd_Date, 
		a.Last_Upd_User	COLLATE DATABASE_DEFAULT AS	Last_Upd_User, 
		NULL AS Link_Type, 
		a.Record_Status	AS	Record_Status, 
		a.SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source AS	Source_, 
		a.DB_Source_387004609 COLLATE DATABASE_DEFAULT AS Source_Description,
		IBaseM.Fn_GetEliteRefFromDbSource(a.DB_Source_387004609) AS  EliteRef, 
		a.Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding,
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Intelligence_Link AS a  WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd AS b  WITH(NOLOCK)ON a.Unique_ID = b.Link_ID
	WHERE (LEFT(b.Entity_ID1, 3) = 'FRA') 
	AND (LEFT(b.Entity_ID2, 3) = 'CLA')
	SELECT @InsertRowCount += @@ROWCOUNT
		
	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Fraud_Ring_Incident_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Fraud_Ring_Incident_Link_NextID'
	INSERT INTO IBaseM8Cur._Fraud_Ring_Incident_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)
		
	--2.13)Telephone (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Telephone_Link'
	INSERT INTO IBaseM8Cur.Telephone_Link
		(Unique_ID, Create_User, Create_Date, Last_Upd_User, Last_Upd_Date, SCC, Record_Status, Status_Binding, AltEntity, IconColour, Source_411765484, 
		Source_Reference_411765487, Source_Description_411765489, Telephone_Link_ID, Do_Not_Disseminate_412284494, Key_Attractor_412284410, 
		MDA_Incident_ID_412284502, x5x5x5_Grading_412284402, Link_Type)
	SELECT
		'TE0' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Unique_ID)) AS Unique_ID, *
	FROM(
	SELECT DISTINCT
		n.Create_User AS Create_User, 
		n.Create_Date AS Create_Date, 
		n.Last_Upd_User AS Last_Upd_User, 
		n.Last_Upd_Date AS Last_Upd_Date, 
		n.SCC AS SCC, 
		n.Record_Status AS Record_Status, 
		n.Status_Binding AS Status_Binding, 
		n.AltEntity AS AltEntity, 
		n.IconColour AS IconColour, 
		@Source Source_411765484, 
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef, 
		n.DB_Source_387004609 Source_Description_411765489, 
		n.Unique_ID, --###CHECK### 
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402,
		Description_
	FROM IBaseM5Cur.Nominal_Link n WITH (NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd e WITH (NOLOCK) ON n.Unique_ID =  e.Link_ID  
	INNER JOIN IBaseM8Cur.Telephone_ x WITH (NOLOCK) ON e.Entity_ID1 = x.Telephone_ID COLLATE DATABASE_DEFAULT
	UNION ALL
	SELECT DISTINCT
		LL.Create_User AS Create_User, 
		LL.Create_Date AS Create_Date, 
		LL.Last_Upd_User AS Last_Upd_User, 
		LL.Last_Upd_Date AS Last_Upd_Date, 
		LL.SCC AS SCC, 
		LL.Record_Status AS Record_Status, 
		LL.Status_Binding AS Status_Binding, 
		LL.AltEntity AS AltEntity, 
		LL.IconColour AS IconColour, 
		@Source Source_411765484, 
		IBaseM.Fn_GetEliteRefFromDbSource(LL.DB_Source_387004609) AS  EliteRef,
		LL.DB_Source_387004609 Source_Description_411765489, 
		LL.Unique_ID, --###CHECK### 
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402,
		Description_
	FROM IBaseM5Cur.Location_Link LL WITH (NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd E WITH (NOLOCK) ON LL.Unique_ID = E.Link_ID
	INNER JOIN IBaseM5Cur.Location_ L WITH (NOLOCK) ON E.Entity_ID1  = L.Unique_ID
	INNER JOIN IBaseM8Cur.Telephone_ x WITH (NOLOCK) ON e.Entity_ID2 = x.Telephone_ID COLLATE DATABASE_DEFAULT) U
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Telephone_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Telephone_Link_NextID'
	INSERT INTO IBaseM8Cur._Telephone_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.14)Website (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Website_Link'
	INSERT INTO IBaseM8Cur.Website_Link
		(Unique_ID, Create_User, Create_Date, Last_Upd_User, Last_Upd_Date, SCC, Record_Status, Status_Binding, AltEntity, IconColour, Source_411765484, 
		Source_Reference_411765487, Source_Description_411765489, Website_Link_ID, Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402)
	SELECT
		'WE0' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Unique_ID)) AS Unique_ID,
		*
	FROM (
	SELECT DISTINCT
		n.Create_User AS Create_User, 
		n.Create_Date AS Create_Date, 
		n.Last_Upd_User AS Last_Upd_User, 
		n.Last_Upd_Date AS Last_Upd_Date, 
		n.SCC AS SCC, 
		n.Record_Status AS Record_Status, 
		n.Status_Binding AS Status_Binding, 
		n.AltEntity AS AltEntity, 
		n.IconColour AS IconColour, 
		@Source Source_411765484, 
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef,
		n.DB_Source_387004609 Source_Description_411765489, 
		n.Unique_ID, --###CHECK### 
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Nominal_Link n WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd e WITH(NOLOCK) ON n.Unique_ID =  e.Link_ID  
	INNER JOIN IBaseM8Cur.Website_ x WITH(NOLOCK) ON e.Entity_ID1 = x.Website_ID COLLATE DATABASE_DEFAULT
	) X
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Website_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Website_Link_NextID'
	INSERT INTO IBaseM8Cur._Website_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.15)Policy (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Policy_Link'
	INSERT INTO IBaseM8Cur.Policy_Link
		(Unique_ID, Create_User, Create_Date, Last_Upd_User, Last_Upd_Date, SCC, Record_Status, Status_Binding, AltEntity, IconColour, Source_411765484, Source_Reference_411765487, 
		Source_Description_411765489, Policy_Link_ID, Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402, Link_Type)
	SELECT
		n.New_Unique_ID,
		n.Create_User AS Create_User, 
		n.Create_Date AS Create_Date, 
		n.Last_Upd_User AS Last_Upd_User, 
		n.Last_Upd_Date AS Last_Upd_Date, 
		n.SCC AS SCC, 
		n.Record_Status AS Record_Status, 
		n.Status_Binding AS Status_Binding, 
		n.AltEntity AS AltEntity, 
		n.IconColour AS IconColour, 
		'Keoghs CFS' AS  Source_411765484, 
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef,
		n.DB_Source_387004609 Source_Description_411765489, 
		n.Unique_ID, --###CHECK### 
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402,
		n.Description_
	FROM IBaseM.vPolicy_Plus_LinkEnd n WITH(NOLOCK)
	WHERE Record_Type = 1
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Policy_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Policy_Link_NextID'
	INSERT INTO IBaseM8Cur._Policy_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.16)Vehicle (Nominal_Link) 
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Vehicle_Link'
	INSERT INTO IBaseM8Cur.Vehicle_Link
		(Unique_ID, Create_User, Create_Date, Last_Upd_User, Last_Upd_Date, SCC, Record_Status, Status_Binding, AltEntity, IconColour, Source_411765484, 
		Source_Reference_411765487, Source_Description_411765489, Vehicle_Link_ID, Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402, Link_Type)
	SELECT 
		'VE1' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Unique_ID)) AS Unique_ID, *
	FROM(
		SELECT DISTINCT
			n.Create_User AS Create_User, 
			n.Create_Date AS Create_Date, 
			n.Last_Upd_User AS Last_Upd_User, 
			n.Last_Upd_Date AS Last_Upd_Date, 
			n.SCC AS SCC, 
			n.Record_Status AS Record_Status, 
			n.Status_Binding AS Status_Binding, 
			n.AltEntity AS AltEntity, 
			n.IconColour AS IconColour, 
			@Source Source_411765484, 
			IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef , 
			n.DB_Source_387004609 Source_Description_411765489, 
			n.Unique_ID, --###CHECK### 
			0 Do_Not_Disseminate_412284494,
			NULL Key_Attractor_412284410,
			NULL MDA_Incident_ID_412284502,
			NULL x5x5x5_Grading_412284402,
			n.Description_
		FROM IBaseM5Cur.Nominal_Link n WITH(NOLOCK)
		INNER JOIN IBaseM5Cur._LinkEnd e WITH(NOLOCK) ON n.Unique_ID =  e.Link_ID  
		INNER JOIN IBaseM8Cur.Vehicle_ X WITH(NOLOCK) ON e.Entity_ID1 = x.Vehicle_ID COLLATE DATABASE_DEFAULT
		UNION ALL
		SELECT DISTINCT  
			Create_User, 
			Create_Date, 
			Last_Upd_User,
			Last_Upd_Date, 
			SCC, 
			Record_Status,
			Status_Binding, 
			AltEntity,
			IconColour,
			@Source Source_411765484, 
			IBaseM.Fn_GetEliteRefFromDbSource(DB_Source_387004609) AS  EliteRef, 
			DB_Source_387004609 Source_Description_411765489, 
			Unique_ID, 
			0 Do_Not_Disseminate_412284494,
			NULL Key_Attractor_412284410,
			NULL MDA_Incident_ID_412284502,
			NULL x5x5x5_Grading_412284402,
			Description_
		FROM IBaseM5Cur.Vehicle_Link WITH(NOLOCK)
	) A
	WHERE NOT EXISTS (SELECT TOP 1 1 FROM IBaseM8Cur.Vehicle_to_Vehicle_Link B WHERE B.Vehicle_to_Vehicle_Link_ID = A.Unique_ID)
	AND NOT EXISTS (SELECT TOP 1 1 FROM IBaseM8Cur.Policy_Link B WHERE B.Policy_Link_ID = A.Unique_ID)
	--PA:20140205_AddedToPreventVehicleToVehicleLinksBeingCreatedAndVehicleToPolicy,MovedPolicyInsertAboveThis
	SELECT @InsertRowCount += @@ROWCOUNT
	
	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Vehicle_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Vehicle_Link_NextID'
	INSERT INTO IBaseM8Cur._Vehicle_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.17)Account (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Account_Link'
	INSERT INTO IBaseM8Cur.Account_Link
		(Unique_ID, Create_User, Create_Date, Last_Upd_User, Last_Upd_Date, SCC, Record_Status, Status_Binding, AltEntity, IconColour, Source_411765484, 
		Source_Reference_411765487, Source_Description_411765489, Account_Link_ID, Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402)
	SELECT 
		'AC0' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Unique_ID)) AS Unique_ID,
		*
	FROM(
	SELECT DISTINCT	
		n.Create_User AS Create_User, 
		n.Create_Date AS Create_Date, 
		n.Last_Upd_User AS Last_Upd_User, 
		n.Last_Upd_Date AS Last_Upd_Date, 
		n.SCC AS SCC, 
		n.Record_Status AS Record_Status, 
		n.Status_Binding AS Status_Binding, 
		n.AltEntity AS AltEntity, 
		n.IconColour AS IconColour, 
		@Source Source_411765484, 
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef,
		n.DB_Source_387004609 Source_Description_411765489, 
		n.Unique_ID, --###CHECK### 
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Finance_Link n WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd e WITH(NOLOCK) ON n.Unique_ID =  e.Link_ID  
	INNER JOIN IBaseM8Cur.Account_ X WITH(NOLOCK) ON e.Entity_ID1 = x.AccountID_ COLLATE DATABASE_DEFAULT AND LEFT(e.Entity_ID2, 3) IN ('PER', 'ORG')
	) X
	SELECT @InsertRowCount += @@ROWCOUNT
	
	
	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Account_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Account_Link_NextID'
	INSERT INTO IBaseM8Cur._Account_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.18)Intelligence (Nominal_Link)
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Intelligence_Link'
	INSERT INTO IBaseM8Cur.Intelligence_Link
		(Unique_ID, Create_User, Create_Date, Last_Upd_User, Last_Upd_Date, SCC, Record_Status, Status_Binding, AltEntity, IconColour, Source_411765484, Source_Reference_411765487, 
		Source_Description_411765489, Intelligence_Link_ID, Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402)
	SELECT 
	'IN2' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Unique_ID)) AS Unique_ID,
	*
	FROM(
	SELECT DISTINCT
		
		n.Create_User AS Create_User, 
		n.Create_Date AS Create_Date, 
		n.Last_Upd_User AS Last_Upd_User, 
		n.Last_Upd_Date AS Last_Upd_Date, 
		n.SCC AS SCC, 
		n.Record_Status AS Record_Status, 
		n.Status_Binding AS Status_Binding, 
		n.AltEntity AS AltEntity, 
		n.IconColour AS IconColour, 
		@Source Source_411765484, 
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef,
		n.DB_Source_387004609  Source_Description_411765489, 
		n.Unique_ID, --###CHECK### 
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Nominal_Link n WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd e WITH(NOLOCK) ON n.Unique_ID =  e.Link_ID  
	INNER JOIN IBaseM8Cur.Intelligence_ X WITH(NOLOCK) ON e.Entity_ID1 = x.Intelligence_ID COLLATE DATABASE_DEFAULT
	) X
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Intelligence_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Intelligence_Link_NextID'
	INSERT INTO IBaseM8Cur._Intelligence_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.19)Policy_Payment_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Policy_Payment_Link'
	INSERT INTO IBaseM8Cur.Policy_Payment_Link
		(Unique_ID, AltEntity, Create_Date, Create_User, Date_payment_details_taken, IconColour, Last_Upd_Date, Last_Upd_User, Payment_Type, 
		Policy_Payment_Link_ID, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402)
	SELECT DISTINCT
		STUFF(F.Unique_ID, 1, 3, 'PO1')	COLLATE DATABASE_DEFAULT AS	Unique_ID, 
		F.AltEntity	COLLATE DATABASE_DEFAULT AS AltEntity, 
		F.Create_Date AS Create_Date, 
		F.Create_User COLLATE DATABASE_DEFAULT AS Create_User, 
		NULL AS Date_payment_details_taken,
		F.IconColour AS	IconColour,
		F.Last_Upd_Date	AS	Last_Upd_Date, 
		F.Last_Upd_User	COLLATE DATABASE_DEFAULT AS	Last_Upd_User, 
		F.Description_ AS Payment_Type,
		F.Unique_ID COLLATE DATABASE_DEFAULT AS	Policy_Payment_Link_ID, 
		F.Record_Status	AS	Record_Status, 
		F.SCC COLLATE DATABASE_DEFAULT AS SCC, 
		@Source AS	Source_411765484, 
		F.DB_Source_387004609 COLLATE DATABASE_DEFAULT  AS	Source_Description_411765489, 
		IBaseM.Fn_GetEliteRefFromDbSource(f.DB_Source_387004609) AS  EliteRef,
		F.Status_Binding COLLATE DATABASE_DEFAULT AS Status_Binding,
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Finance_Link  AS F WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd LE WITH(NOLOCK) ON F.Unique_ID = LE.Link_ID
	INNER JOIN IBaseM5Cur.Account_ A WITH(NOLOCK) ON A.Unique_ID = LE.Entity_ID1 AND LEFT(Entity_ID2, 3) = 'INS'
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Policy_Payment_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Policy_Payment_Link_NextID'
	INSERT INTO IBaseM8Cur._Policy_Payment_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.20)Incident_Match_Link
	/*	In the new schema, where a person is linked to an Incident which was a CUE/MIAFTR Match in the old schema. 
		There is a double link. 
		1 link is an Incident Link (Correct), the other is an Incident Match Link. 
		Incident Match Links should only be used to link to CUE/MIAFTR Match entities (previously linked by a MIAFTR Match Link). 
		This has also happened where it is a vehicle linked to an Incident which was a CUE/MIAFTR Match in the old schema
		TABLE CODE = IN0 */
	
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Incident_Match_Link'
	INSERT INTO IBaseM8Cur.Incident_Match_Link
		(Unique_ID, AltEntity, Create_Date, Create_User, IconColour, Incident_Match_Link_ID, 
		Last_Upd_Date, Last_Upd_User, 
		Record_Status, 
		SCC, 
		Source_411765484, 
		Source_Description_411765489, Source_Reference_411765487, Status_Binding, 
		Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402)
	SELECT
		'IN0' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Unique_ID)) AS Unique_ID,
		AltEntity	COLLATE DATABASE_DEFAULT AS	AltEntity, 
		Create_Date	AS	Create_Date, 
		Create_User	COLLATE DATABASE_DEFAULT AS	Create_User, 
		IconColour	AS	IconColour, 
		Unique_ID	COLLATE DATABASE_DEFAULT AS	Incident_Match_Link_ID,
		Last_Upd_Date	AS	Last_Upd_Date, 
		Last_Upd_User	COLLATE DATABASE_DEFAULT AS	Last_Upd_User, 
		L.Record_Status	AS	Record_Status, 
		L.SCC	COLLATE DATABASE_DEFAULT AS	SCC, 
		 'Keoghs CFS' AS	Source_411765484, 
		DB_Source_387004609	COLLATE DATABASE_DEFAULT  AS	Source_Description_411765489, 
		IBaseM.Fn_GetEliteRefFromDbSource(l.DB_Source_387004609) AS  EliteRef ,
		Status_Binding	COLLATE DATABASE_DEFAULT AS	Status_Binding,
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.MIAFTR_Match_Link L WITH (NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd LE WITH (NOLOCK) ON L.Unique_ID = LE.Link_ID  AND LEFT(Entity_ID1, 3) = 'MIA'
	WHERE LEFT(Entity_ID2, 3) NOT IN  ('VEH', 'PER')
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Incident_Match_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Incident_Match_Link_NextID'
	INSERT INTO IBaseM8Cur._Incident_Match_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.21)Vehicle_to_incident_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Vehicle_Incident_Link'
	INSERT INTO IBaseM8Cur.Vehicle_Incident_Link
		(Unique_ID, AltEntity, Category_of_Loss, Create_Date, Create_User, IconColour, Last_Upd_Date, Last_Upd_User, Link_Type, Record_Status, SCC, Source_411765484, 
		Source_Description_411765489, Source_Reference_411765487, Status_Binding, Vehicle_Incident_Link_ID, Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402)
	SELECT 
		'VE0' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Unique_ID)) AS Unique_ID,
			AltEntity, 
			Category_of_Loss, 
			Create_Date, 
			Create_User, 
			IconColour, 
			Last_Upd_Date, 
			Last_Upd_User, 
			Link_Type, 
			Record_Status, 
			SCC, 
			Source_411765484, 
			Source_Description_411765489, 
			EliteRef,
			Status_Binding, 
			Vehicle_Incident_Link_ID, 
			Do_Not_Disseminate_412284494,
			Key_Attractor_412284410,
			MDA_Incident_ID_412284502,
			x5x5x5_Grading_412284402 
		FROM(
		SELECT  DISTINCT
			Unique_ID AS Unique_ID, 
			AltEntity AS AltEntity, 
			NULL AS Category_of_Loss, 
			Create_Date	AS Create_Date, 
			Create_User	AS Create_User, 
			IconColour AS IconColour, 
			Last_Upd_Date AS Last_Upd_Date, 
			Last_Upd_User AS Last_Upd_User, 
			CASE Description_ WHEN 'TP Vehicle' THEN 'Third Party Vehicle' ELSE Description_ END AS Link_Type, 
			Record_Status AS Record_Status, 
			SCC	AS SCC, 
			@Source AS Source_411765484, 
			DB_Source_387004609 AS Source_Description_411765489, 
			IBaseM.Fn_GetEliteRefFromDbSource(DB_Source_387004609) AS  EliteRef,
			Status_Binding AS Status_Binding, 
			Unique_ID AS Vehicle_Incident_Link_ID, 
			0 Do_Not_Disseminate_412284494,
			NULL Key_Attractor_412284410,
			NULL MDA_Incident_ID_412284502,
			NULL x5x5x5_Grading_412284402        
		FROM IBaseM5Cur.Insured_Claim_Link WITH(NOLOCK)
		WHERE Description_ != 'Insured Claim'
		
		UNION ALL

		SELECT DISTINCT
			Unique_ID AS Unique_ID, 
			AltEntity AS AltEntity, 
			NULL AS Category_of_Loss, 
			Create_Date	AS Create_Date, 
			Create_User	AS Create_User, 
			IconColour AS IconColour, 
			Last_Upd_Date AS Last_Upd_Date, 
			Last_Upd_User AS Last_Upd_User, 
			Description_ AS Link_Type, 
			L.Record_Status AS Record_Status, 
			L.SCC	AS SCC, 
			@Source AS Source_411765484, 
			'CUE / MIAFTR Match' AS Source_Description_411765489, 
			IBaseM.Fn_GetEliteRefFromDbSource(DB_Source_387004609) AS  EliteRef,
			Status_Binding AS Status_Binding, 
			Unique_ID AS Vehicle_Incident_Link_ID,
			0 Do_Not_Disseminate_412284494,
			NULL Key_Attractor_412284410,
			NULL MDA_Incident_ID_412284502,
			NULL x5x5x5_Grading_412284402  
		FROM IBaseM5Cur.MIAFTR_Match_Link L WITH (NOLOCK)
		INNER JOIN IBaseM5Cur._LinkEnd LE WITH (NOLOCK) ON L.Unique_ID =  LE.Link_ID 
		WHERE LEFT(Entity_ID1, 3) = 'MIA' AND  LEFT(Entity_ID2, 3) = 'VEH'

		UNION ALL
		
		SELECT   DISTINCT
			Unique_ID AS Unique_ID, 
			AltEntity AS AltEntity, 
			NULL AS Category_of_Loss, 
			Create_Date	AS Create_Date, 
			Create_User	AS Create_User, 
			IconColour AS IconColour, 
			Last_Upd_Date AS Last_Upd_Date, 
			Last_Upd_User AS Last_Upd_User, 
			Description_ AS Link_Type, 
			A.Record_Status AS Record_Status, 
			A.SCC	AS SCC, 
			@Source AS Source_411765484, 
			DB_Source_387004609 AS  Source_Description_411765489, 
			IBaseM.Fn_GetEliteRefFromDbSource(a.DB_Source_387004609) AS  EliteRef,
			Status_Binding AS Status_Binding, 
			Unique_ID AS Vehicle_Incident_Link_ID,
			0 Do_Not_Disseminate_412284494,
			NULL Key_Attractor_412284410,
			NULL MDA_Incident_ID_412284502,
			NULL x5x5x5_Grading_412284402                
		FROM IBaseM5Cur.Claim_ A WITH(NOLOCK)
		INNER JOIN IBaseM5Cur._LinkEnd B WITH(NOLOCK) ON A.Unique_ID = B.Link_ID AND B.Entity_ID1 LIKE 'CLA%' AND B.Entity_ID2 LIKE 'VEH%'     	
		) AS U
	SELECT @InsertRowCount += @@ROWCOUNT

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Vehicle_Incident_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Vehicle_Incident_Link_NextID'
	INSERT INTO IBaseM8Cur._Vehicle_Incident_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.22)Organisation_Match_Link 
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Organisation_Match_Link'
	INSERT INTO IBaseM8Cur.Organisation_Match_Link
		(Unique_ID, AltEntity, Create_Date, Create_User, IconColour, Last_Upd_Date, Last_Upd_User, Link_Type, Organisation_Match_Link_ID, 
		Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Do_Not_Disseminate_412284494, Key_Attractor_412284410, MDA_Incident_ID_412284502, x5x5x5_Grading_412284402)
	SELECT 
		'OR5' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Unique_ID)) AS Unique_ID,
		*
	FROM (
	SELECT DISTINCT
		n.AltEntity AS AltEntity, 
		n.Create_Date AS Create_Date, 
		n.Create_User AS Create_User, 
		n.IconColour AS IconColour, 
		n.Last_Upd_Date AS Last_Upd_Date,
		n.Last_Upd_User AS Last_Upd_User, 
		n.Description_,
		n.Unique_ID,
		n.Record_Status AS Record_Status, 
		n.SCC AS SCC, 
		@Source Source_411765484, 
		n.DB_Source_387004609 Source_Description_411765489,
		IBaseM.Fn_GetEliteRefFromDbSource(n.DB_Source_387004609) AS  EliteRef,
		n.Status_Binding AS Status_Binding,
		0 Do_Not_Disseminate_412284494,
		NULL Key_Attractor_412284410,
		NULL MDA_Incident_ID_412284502,
		NULL x5x5x5_Grading_412284402
	FROM IBaseM5Cur.Nominal_Link n WITH(NOLOCK)
	INNER JOIN IBaseM5Cur._LinkEnd e WITH(NOLOCK) ON n.Unique_ID =  e.Link_ID  
	WHERE e.Entity_ID1 LIKE 'ORG%' AND  e.Entity_ID2 LIKE 'ORG%'
	) X
	SELECT @InsertRowCount += @@ROWCOUNT
		
	SELECT @NextID =  MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1  FROM  IBaseM8Cur.Organisation_Match_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Organisation_Match_Link_NextID'
	INSERT INTO IBaseM8Cur._Organisation_Match_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	--2.23)IBaseM.Intelligence_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Intelligence_Link'
	INSERT INTO IBaseM8Cur.Intelligence_Link
	(Unique_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, IconColour, Intelligence_Link_ID, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, x5x5x5_Grading_412284402)
	SELECT DISTINCT
		'IN2' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY CONVERT(INT, STUFF(Unique_ID, 1, 3, '')))) COLLATE DATABASE_DEFAULT AS Unique_ID, 
		AltEntity, 
		Create_Date, 
		Create_User, 
		0 Do_Not_Disseminate_412284494, 
		IconColour, 
		Intelligence_Link_ID, 
		NULL Key_Attractor_412284410, 
		Last_Upd_Date, Last_Upd_User, 
		NULL MDA_Incident_ID_412284502, 
		Notes_, Record_Status, 
		SCC, 
		Source_, 
		Source_Description_, 
		EliteRef, 
		Status_Binding, 
		NULL x5x5x5_Grading_412284402
	FROM(
			SELECT DISTINCT   
				I.Unique_ID AS Unique_ID, 
				I.AltEntity AS AltEntity, 
				I.Create_Date AS Create_Date, 
				I.Create_User AS Create_User, 
				I.IconColour AS IconColour,
				I.Unique_ID AS Intelligence_Link_ID,
				I.Last_Upd_Date AS Last_Upd_Date, 
				I.Last_Upd_User AS Last_Upd_User, 
				I.Description_ AS Notes_,
				I.Record_Status AS Record_Status,
				I.SCC AS SCC, 
				@Source AS Source_,
				I.DB_Source_387004609 AS Source_Description_,
				IBaseM.Fn_GetEliteRefFromDbSource(i.DB_Source_387004609) AS  EliteRef,
				i.Status_Binding AS Status_Binding
			FROM IBaseM5Cur.Intelligence_Link AS I  WITH(NOLOCK) 
			INNER JOIN IBaseM5Cur._LinkEnd AS L   WITH(NOLOCK) ON I.Unique_ID = L.Link_ID
		UNION ALL
			SELECT DISTINCT
				I.Unique_ID AS Unique_ID, 
				I.AltEntity AS AltEntity, 
				I.Create_Date AS Create_Date, 
				I.Create_User AS Create_User, 
				I.IconColour AS IconColour,
				I.Unique_ID AS Intelligence_Link_ID,
				I.Last_Upd_Date AS Last_Upd_Date, 
				I.Last_Upd_User AS Last_Upd_User, 
				I.Description_ AS Notes_,
				I.Record_Status AS Record_Status,
				I.SCC AS SCC, 
				@Source AS Source_,
				I.DB_Source_387004609 AS Source_Description_,
				IBaseM.Fn_GetEliteRefFromDbSource(i.DB_Source_387004609) AS  EliteRef,
				i.Status_Binding AS Status_Binding
			FROM  IBaseM5Cur.Report_ I  WITH(NOLOCK)) X  
	SELECT @InsertRowCount += @@ROWCOUNT
				
	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Intelligence_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Intelligence_Link_NextID'
	INSERT INTO IBaseM8Cur._Intelligence_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Outcome_Link'

	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
END CATCH

