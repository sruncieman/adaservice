﻿





CREATE PROCEDURE [Rep].[uspReplicateLinkOutcome]
/**************************************************************************************************/
-- ObjectName:		[Rep].[uspReplicateLinkOutcome]	
--
-- Description:		Replicate Link Outcome
--
-- Steps:			1)ConfigureLogging,Create/DeclareRequiredObjects
--					2)UpdateDeletes
--					3)Insert/UpdateChangesViaMerge
--					4)UpdateLoggingTidy				
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-03-03			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 1			--DefaultIsOn
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@InsertRowCount	INT	= 0
				,@UpdateRowCount	INT = 0
				,@DeleteRowCount	INT = 0
				,@TableID			INT = OBJECT_ID('MDA_IBase.dbo.Outcome_Link')
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 60 --ReplicateOutcomeLink
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@TableID			= @TableID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END

	---------------------------------------------------------
	--2)UpdateDeletes
	---------------------------------------------------------
	UPDATE [$(MDA_IBase)].[dbo].Outcome_Link
	SET Record_Status =254
	OUTPUT	 @OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + ISNULL(Deleted.Amount_Recovered_to_Date,'') + '}|I:{' + ISNULL(Inserted.Amount_Recovered_to_Date,'') + '}'
			,'D:{' + ISNULL(Deleted.Amount_Still_Outstanding,'') + '}|I:{' + ISNULL(Inserted.Amount_Still_Outstanding,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_Settlement_Agreed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_Settlement_Agreed,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Enforcement_Role,'') + '}|I:{' + ISNULL(Inserted.Enforcement_Role,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.Manner_of_Resolution,'') + '}|I:{' + ISNULL(Inserted.Manner_of_Resolution,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Method_of_Enforcement,'') + '}|I:{' + ISNULL(Inserted.Method_of_Enforcement,'') + '}'
			,'D:{' + ISNULL(Deleted.Outcome_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Outcome_Link_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Property_Owned,'') + '}|I:{' + ISNULL(Inserted.Property_Owned,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Settlement_Amount_Agreed,'') + '}|I:{' + ISNULL(Inserted.Settlement_Amount_Agreed,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Settlement_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Settlement_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Type_of_Success,'') + '}|I:{' + ISNULL(Inserted.Type_of_Success,'') + '}'
			,'D:{' + ISNULL(Deleted.Was_Enforcement_Successful,'') + '}|I:{' + ISNULL(Inserted.Was_Enforcement_Successful,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Outcome_Link] (TaskID, DMLAction, Unique_ID, AltEntity, Amount_Recovered_to_Date, Amount_Still_Outstanding, Create_Date, Create_User, Date_Settlement_Agreed, Do_Not_Disseminate_412284494, Enforcement_Role, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Manner_of_Resolution, MDA_Incident_ID_412284502, Method_of_Enforcement, Outcome_Link_ID, Property_Owned, Record_Status, SCC, Settlement_Amount_Agreed, Settlement_Date, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Type_of_Success, Was_Enforcement_Successful, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
	FROM Rep.LinkRecord LR
	INNER JOIN [$(MDA_IBase)].[dbo].Outcome_Link a ON LR.IBase8linkID = A.Unique_ID
	WHERE RecordAction ='D'

	---------------------------------------------------------
	--3)Insert/UpdateChangesViaMerge
	---------------------------------------------------------
	MERGE [$(MDA_IBase)].[dbo].Outcome_Link [Target]
	USING	(
			SELECT   DISTINCT C.IBase8LinkID
					,C.IBase5LinkID
					,C.RiskClaim_Id
					,A.*
		FROM IBaseM8Cur.Outcome_Link A
		INNER JOIN Rep.LinkRecord c ON a.Unique_ID = c.IBase8LinkID_Staging
		) AS Source ON [Target].Unique_ID = Source.IBase8LinkID
	WHEN MATCHED THEN UPDATE
	SET  Settlement_Date					= Source.Settlement_Date
		,Manner_of_Resolution			= Source.Manner_of_Resolution
		,Enforcement_Role				= Source.Enforcement_Role
		,Property_Owned					= Source.Property_Owned
		,Method_of_Enforcement			= Source.Method_of_Enforcement
		,Date_Settlement_Agreed			= Source.Date_Settlement_Agreed
		,Settlement_Amount_Agreed		= Source.Settlement_Amount_Agreed
		,Amount_Recovered_to_Date		= Source.Amount_Recovered_to_Date
		,Amount_Still_Outstanding		= Source.Amount_Still_Outstanding
		,Was_Enforcement_Successful		= Source.Was_Enforcement_Successful
		,Type_of_Success				= Source.Type_of_Success
		,Source_411765484				= Source.Source_411765484
		,Source_Reference_411765487		= Source.Source_Reference_411765487
		,Source_Description_411765489	= Source.Source_Description_411765489
		,Create_Date					= Source.Create_Date
		,Create_User					= Source.Create_User
		,Last_Upd_Date					= Source.Last_Upd_Date
		,Last_Upd_User					= Source.Last_Upd_User
		,Risk_Claim_ID_414244883		= Source.RiskClaim_ID
		,Record_Status					= Source.Record_Status
		,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
		,[AltEntity]					= Source.[AltEntity]
		,[Do_Not_Disseminate_412284494] = Source.[Do_Not_Disseminate_412284494]
		,[IconColour]					= Source.[IconColour]
		,[Key_Attractor_412284410]		= Source.[Key_Attractor_412284410]
		,[Outcome_Link_ID]				= Source.[Outcome_Link_ID]
		,[SCC]							= Source.[SCC]
		,[Status_Binding]				= Source.[Status_Binding]
	WHEN NOT MATCHED THEN INSERT  
		(
		 Unique_ID
		,AltEntity
		,Amount_Recovered_to_Date
		,Amount_Still_Outstanding
		,Create_Date
		,Create_User
		,Date_Settlement_Agreed
		,Do_Not_Disseminate_412284494
		,Enforcement_Role
		,IconColour
		,Key_Attractor_412284410
		,Last_Upd_Date
		,Last_Upd_User
		,Manner_of_Resolution
		,Method_of_Enforcement
		,Outcome_Link_ID
		,Property_Owned
		,Record_Status
		,SCC
		,Settlement_Amount_Agreed
		,Settlement_Date
		,Source_411765484
		,Source_Description_411765489
		,Source_Reference_411765487
		,Status_Binding
		,Type_of_Success
		,Was_Enforcement_Successful
		,x5x5x5_Grading_412284402
		,Risk_Claim_ID_414244883
		)
	VALUES
		(
		 Source.IBase8LinkID
		,Source.AltEntity
		,Source.Amount_Recovered_to_Date
		,Source.Amount_Still_Outstanding
		,Source.Create_Date
		,Source.Create_User
		,Source.Date_Settlement_Agreed
		,Source.Do_Not_Disseminate_412284494
		,Source.Enforcement_Role
		,Source.IconColour
		,Source.Key_Attractor_412284410
		,Source.Last_Upd_Date
		,Source.Last_Upd_User
		,Source.Manner_of_Resolution
		,Source.Method_of_Enforcement
		,Source.Outcome_Link_ID
		,Source.Property_Owned
		,Source.Record_Status
		,Source.SCC
		,Source.Settlement_Amount_Agreed
		,Source.Settlement_Date
		,Source.Source_411765484
		,Source.Source_Description_411765489
		,Source.Source_Reference_411765487
		,Source.Status_Binding
		,Source.Type_of_Success
		,Source.Was_Enforcement_Successful
		,Source.x5x5x5_Grading_412284402
		,Source.RiskClaim_ID
	)
	OUTPUT	 @OutputTaskID
			,$ACTION
			,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
			,'D:{' + ISNULL(Deleted.Amount_Recovered_to_Date,'') + '}|I:{' + ISNULL(Inserted.Amount_Recovered_to_Date,'') + '}'
			,'D:{' + ISNULL(Deleted.Amount_Still_Outstanding,'') + '}|I:{' + ISNULL(Inserted.Amount_Still_Outstanding,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_Settlement_Agreed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_Settlement_Agreed,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
			,'D:{' + ISNULL(Deleted.Enforcement_Role,'') + '}|I:{' + ISNULL(Inserted.Enforcement_Role,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
			,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
			,'D:{' + ISNULL(Deleted.Manner_of_Resolution,'') + '}|I:{' + ISNULL(Inserted.Manner_of_Resolution,'') + '}'
			,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
			,'D:{' + ISNULL(Deleted.Method_of_Enforcement,'') + '}|I:{' + ISNULL(Inserted.Method_of_Enforcement,'') + '}'
			,'D:{' + ISNULL(Deleted.Outcome_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Outcome_Link_ID,'') + '}'
			,'D:{' + ISNULL(Deleted.Property_Owned,'') + '}|I:{' + ISNULL(Inserted.Property_Owned,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
			,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
			,'D:{' + ISNULL(Deleted.Settlement_Amount_Agreed,'') + '}|I:{' + ISNULL(Inserted.Settlement_Amount_Agreed,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Settlement_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Settlement_Date,''),126) + '}'
			,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
			,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
			,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
			,'D:{' + ISNULL(Deleted.Type_of_Success,'') + '}|I:{' + ISNULL(Inserted.Type_of_Success,'') + '}'
			,'D:{' + ISNULL(Deleted.Was_Enforcement_Successful,'') + '}|I:{' + ISNULL(Inserted.Was_Enforcement_Successful,'') + '}'
			,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
			,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
	INTO [SyncDML].[MDAIBase_Outcome_Link] (TaskID, DMLAction, Unique_ID, AltEntity, Amount_Recovered_to_Date, Amount_Still_Outstanding, Create_Date, Create_User, Date_Settlement_Agreed, Do_Not_Disseminate_412284494, Enforcement_Role, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, Manner_of_Resolution, MDA_Incident_ID_412284502, Method_of_Enforcement, Outcome_Link_ID, Property_Owned, Record_Status, SCC, Settlement_Amount_Agreed, Settlement_Date, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Type_of_Success, Was_Enforcement_Successful, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

	------------------------------------------------------------
	--4)UpdateLoggingTidyTidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Outcome_Link] WHERE TaskID = @OutputTaskID AND DMLAction = 'INSERT')
		SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Outcome_Link] WHERE TaskID = @OutputTaskID AND DMLAction IN ('UPDATE','UPDATE DELETE'))
		SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Outcome_Link] WHERE TaskID = @OutputTaskID AND DMLAction = 'DELETE')
		
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@InsertRowCount= @InsertRowCount
					,@UpdateRowCount= @UpdateRowCount
					,@DeleteRowCount= @DeleteRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
	DECLARE  @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()
		
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()				
		EXECUTE dbo.uspCRUDDBControlTask
						@Action			= 'U'
					,@TaskID			= @OutputTaskID
					,@TaskEndTime		= @Now
					,@Error				= @ErrorSeverity
					,@ErrorDescription	= @ErrorMessage
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+']'
END CATCH






