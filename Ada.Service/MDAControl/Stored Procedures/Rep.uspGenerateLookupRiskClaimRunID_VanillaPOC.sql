﻿CREATE PROCEDURE [Rep].[uspGenerateLookupRiskClaimRunID_VanillaPOC]
(
 @Logging				BIT = 0	--DefaultIsOff
,@Debug					BIT = 0 --DefaultIsOff
)
AS
SET NOCOUNT ON
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@OutputRunID		INT
				,@OutputProcessID	INT
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
	
		EXECUTE dbo.uspCRUDDBControlRun
					 @RunNameID		= 140
					,@OutputRunID	= @OutputRunID OUTPUT
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'					
		EXECUTE dbo.uspCRUDDBControlProcess
					 @RunID				= @OutputRunID
					,@ProcessNameID		= 142--141 --GenerateLookupRiskClaimRunID_VanillaPOC
					,@Details			= @Details
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@OutputProcessID	= @OutputProcessID OUTPUT
	END

	------------------------------------------------------------
	--BODY
	------------------------------------------------------------
	DECLARE  @SQL NVARCHAR(MAX) = ''
			,@Counter TINYINT = 1
			,@ToProcess TINYINT
			,@RiskBatch_Id INT

	IF OBJECT_ID('tempdb..#SQLStatement') IS NOT NULL
		DROP TABLE #SQLStatement
	CREATE TABLE #SQLStatement (RowID INT IDENTITY(1,1), SQLStatement NVARCHAR(MAX)NOT NULL)

	IF OBJECT_ID('tempdb..#IBaseEliteRef') IS NOT NULL
		DROP TABLE #IBaseEliteRef
	CREATE TABLE #IBaseEliteRef (EliteReference VARCHAR(50) NOT NULL)
	CREATE CLUSTERED INDEX IX_#IBaseEliteRef_EliteReference_NC ON #IBaseEliteRef (EliteReference)

	-------------------------
	--CreateIndexes
	-------------------------
	SELECT @SQL = @SQL + COALESCE('CREATE INDEX IX_'+[PhysicalName]+'_Source_Reference_411765487_NC ON MDA_IBase.dbo.'+[PhysicalName]+' (Source_Reference_411765487);' + CHAR(13),'')
	FROM [MDA_IBase].[dbo].[_DataTable]
	WHERE [Type] = 1
	ORDER BY [PhysicalName]

	EXECUTE SP_EXECUTESQL @SQL

	------------------------------------
	--GetTheKeoghsEliteRef:Grouped
	------------------------------------
	INSERT INTO #SQLStatement (SQLStatement)
	SELECT 'INSERT INTO #IBaseEliteRef (EliteReference) SELECT Source_Reference_411765487 FROM MDA_IBase.dbo.'+[PhysicalName]+' SRC WHERE NULLIF(Source_Reference_411765487,'''') IS NOT NULL AND NOT EXISTS (SELECT TOP 1 1 FROM #IBaseEliteRef DEST WHERE DEST.EliteReference = SRC.Source_Reference_411765487) GROUP BY Source_Reference_411765487'
	FROM [MDA_IBase].[dbo].[_DataTable]
	WHERE [Type] = 1
	ORDER BY [PhysicalName]
	SELECT @ToProcess = @@ROWCOUNT

	WHILE @ToProcess >= @Counter
	BEGIN
		SELECT @SQL = SQLStatement
		FROM #SQLStatement
		WHERE RowID = @Counter

		EXECUTE SP_EXECUTESQL @SQL

		SELECT @Counter += 1
	END

	------------------------------------
	--GenerateAndCreateTheRiskClaimIDs
	------------------------------------
	INSERT INTO [MDA].[dbo].RiskBatch (RiskClient_Id, BatchReference, BatchEntityType, ClientBatchReference, BatchStatus, BatchStatusLastModified, RiskOriginalFile_Id, CreatedDate, CreatedBy, TotalClaimsReceived, TotalNewClaims, TotalModifiedClaims, TotalClaimsLoaded, TotalClaimsInError, TotalClaimsSkipped, LoadingDurationInMs,[VisibilityStatus])
	SELECT 0, 'VanillaPOCSetUp_' + CONVERT(VARCHAR(10),GETDATE(),112) + REPLACE(CONVERT(VARCHAR(10),GETDATE(),108),':',''), 'None', CONVERT(VARCHAR(10),GETDATE(),112), 0, GETDATE(), 0, GETDATE(), 'ReplicationAdmin',0,0,0,0,0,0,0,1
	SELECT @RiskBatch_Id = @@IDENTITY 

	INSERT INTO [MDA].[dbo].RiskClaim (RiskBatch_Id, Incident_Id, MdaClaimRef, BaseRiskClaim_Id, SuppliedClaimStatus, ClaimStatus, ClaimStatusLastModified, ClientClaimRefNumber, LevelOneRequestedCount, LevelTwoRequestedCount, LevelOneRequestedWhen, LevelTwoRequestedWhen, LevelOneRequestedWho, LevelTwoRequestedWho, LevelOneUnavailable, LevelTwoUnavailable, SourceReference, SourceEntityEncodeFormat, SourceEntityXML, EntityClassType, CreatedDate, CreatedBy, LatestVersion, LatestScored, CallbackRequested, CallbackRequestedWhen, CallbackRequestedWho, ExternalServicesRequired, ExternalServicesRequestsRequired, ClaimReadStatus, LoadingDurationInMs, ValidationResults, CleansingResults)
	SELECT @RiskBatch_Id, 0, '', 0, 0, 0, GETDATE(), EliteReference, 0, 0, NULL, NULL, NULL, NULL, 0 , 0, SUBSTRING('Elite-' + EliteReference,1,50), 'sql', '', 'sql', GETDATE(), 'ReplicationAdmin', 1, 0 ,0, NULL, NULL, 0, 0, 0, 0, NULL, NULL
	FROM #IBaseEliteRef

	IF EXISTS(SELECT TOP 1 1 FROM MDA.sys.indexes WHERE object_id = object_id('MDA.dbo.RiskClaim') AND NAME ='IX_RiskClaim_ClientClaimRefNumber_NC')
		DROP INDEX IX_RiskClaim_ClientClaimRefNumber_NC ON MDA.dbo.RiskClaim
	CREATE INDEX IX_RiskClaim_ClientClaimRefNumber_NC ON MDA.dbo.RiskClaim(ClientClaimRefNumber)

	-------------------------------------
	--PopulateTheRiskClaimIDsWithinIBase
	-------------------------------------
	SELECT @SQL = ''
	SELECT @SQL = @SQL + COALESCE('UPDATE DEST SET [Risk_Claim_ID_414244883] = SRC.[Id] FROM MDA_IBase.dbo.'+[PhysicalName]+' DEST INNER JOIN MDA.dbo.RiskClaim SRC ON SRC.ClientClaimRefNumber = DEST.Source_Reference_411765487;' + CHAR(13),'')
	FROM [MDA_IBase].[dbo].[_DataTable]
	WHERE [Type] = 1
	ORDER BY [PhysicalName]

	EXECUTE SP_EXECUTESQL @SQL

	-------------------------------------
	--DropTheIndexes
	-------------------------------------
	SELECT @SQL = ''
	SELECT @SQL = @SQL + COALESCE('DROP INDEX IX_'+[PhysicalName]+'_Source_Reference_411765487_NC ON MDA_IBase.dbo.'+[PhysicalName]+';' + CHAR(13),'')
	FROM [MDA_IBase].[dbo].[_DataTable]
	WHERE [Type] = 1
	ORDER BY [PhysicalName]
	EXECUTE SP_EXECUTESQL @SQL

	-------------------------------------
	--Tidy
	-------------------------------------
	IF OBJECT_ID('tempdb..#SQLStatement') IS NOT NULL
		DROP TABLE #SQLStatement

	IF OBJECT_ID('tempdb..#IBaseEliteRef') IS NOT NULL
		DROP TABLE #IBaseEliteRef

	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	-----------------------------------------------------------
	IF @Logging = 1
		BEGIN
			SELECT   @Now = GETDATE()			
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@OutputProcessID	= @OutputProcessID OUTPUT
		END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
				
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()						
			EXECUTE dbo.uspCRUDDBControlProcess
						 @Action			= 'U'
						,@ProcessID			= @OutputProcessID
						,@RunID				= @OutputRunID
						,@ProcessEndTime	= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputProcessID	= @OutputProcessID OUTPUT
						
			EXECUTE dbo.uspCRUDDBControlRun
						 @Action			= 'U'
						,@RunID				= @OutputRunID
						,@RunEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputRunID		= @OutputRunID OUTPUT
		END
END CATCH