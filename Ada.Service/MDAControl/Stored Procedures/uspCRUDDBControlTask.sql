﻿CREATE PROCEDURE [dbo].[uspCRUDDBControlTask]
/**************************************************************************************************/
-- ObjectName:		MDAControl.dbo.uspCRUDControlTaskID	
--
-- Description:		CRUD Procedure on table dbo.DBControlTask	
--
-- Usage:			Logging
--
-- Steps:			1)SetTheTaskIDIfExists
--					2)CreateOrUpdateTheControlTaskRecord
--					3)ReadTheTaskID
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-24			Paul Allen		Created
/**************************************************************************************************/
(
 @Action			VARCHAR(1)		= NULL
,@TaskID			INT				= NULL
,@ParentTaskID		INT				= NULL
,@ProcessID			INT				= NULL
,@TaskNameID		INT				= NULL
,@DatabaseID		INT				= NULL
,@ObjectID			INT				= NULL
,@TableID			INT				= NULL
,@TaskStartTime		DATETIME		= NULL
,@TaskEndTime		DATETIME		= NULL
,@InsertRowCount	INT				= NULL
,@UpdateRowCount	INT				= NULL
,@DeleteRowCount	INT				= NULL
,@Error				INT				= NULL
,@ErrorDescription	VARCHAR(1000)	= NULL
,@Details			VARCHAR(1000)	= NULL
,@OutputTaskID		INT	OUTPUT
)

AS
SET NOCOUNT ON--, XACT_ABORT ON

BEGIN TRY
	IF @Action IN ('U','D') AND NULLIF(REPLACE(@TaskID,0,1),'') IS NULL
	BEGIN
		RAISERROR ('TheTaskIDMustBeSuppliedForUpdateOrDeleteOperations',18,255)
		RETURN
	END

	/*1)SetTheTaskIDIfExists*/
	IF @TaskID IS NULL
	SELECT @OutputTaskID =	(
							SELECT MAX(TaskID) 
							FROM dbo.DBControlTask 
							WHERE ProcessID = @ProcessID 
							AND TaskNameID = @TaskNameID 
							AND (ParentTaskID = @ParentTaskID OR @ParentTaskID IS NULL) 
							AND Error IS NULL
							AND TaskEndTime IS NOT NULL
							AND TaskID = (SELECT MAX(TaskID) FROM dbo.DBControlTask )
							)
	
	/*2)DeleteUpdateOrInsertTheControlTaskRecord*/
	IF @OutputTaskID IS NULL OR @Action IN ('U','D')
	BEGIN
		DECLARE @OuputTable TABLE (TaskID INT)
		
		MERGE dbo.DBControlTask [TARGET]
		USING  (
				SELECT	 @TaskID			TaskID
						,@ParentTaskID		ParentTaskID
						,@ProcessID			ProcessID
						,@TaskNameID		TaskNameID
						,@DatabaseID		DatabaseID
						,@ObjectID			ObjectID
						,@TableID			TableID
						,@TaskStartTime		TaskStartTime
						,@TaskEndTime		TaskEndTime
						,@InsertRowCount	InsertRowCount
						,@UpdateRowCount	UpdateRowCount
						,@DeleteRowCount	DeleteRowCount
						,@Error				Error
						,@ErrorDescription	ErrorDescription
						,@Details			Details
				) [SOURCE] ON [SOURCE].TaskID = [TARGET].TaskID
		WHEN MATCHED AND @Action = 'D'
			THEN DELETE				
		WHEN MATCHED THEN UPDATE
			SET  ParentTaskID		= ISNULL(@ParentTaskID, [TARGET].ParentTaskID)
				,ProcessID			= ISNULL(@ProcessID, [TARGET].ProcessID)
				,TaskNameID			= ISNULL(@TaskNameID, [TARGET].TaskNameID)
				,DatabaseID			= ISNULL(@DatabaseID, [TARGET].DatabaseID)
				,ObjectID			= ISNULL(@ObjectID, [TARGET].ObjectID)
				,TableID			= ISNULL(@TableID, [TARGET].TableID)
				,TaskStartTime		= ISNULL(@TaskStartTime, [TARGET].TaskStartTime)
				,TaskEndTime		= ISNULL(@TaskEndTime, [TARGET].TaskEndTime)
				,InsertRowCount		= ISNULL(@InsertRowCount, [TARGET].InsertRowCount)
				,UpdateRowCount		= ISNULL(@UpdateRowCount, [TARGET].UpdateRowCount)
				,DeleteRowCount		= ISNULL(@DeleteRowCount, [TARGET].DeleteRowCount)
				,Error				= ISNULL(@Error, [TARGET].Error)
				,ErrorDescription	= ISNULL(@ErrorDescription, [TARGET].ErrorDescription)
				,Details			= ISNULL(@Details, [TARGET].Details)
		WHEN NOT MATCHED AND @TaskNameID IS NOT NULL THEN INSERT
				(
				 ParentTaskID
				,ProcessID
				,TaskNameID
				,DatabaseID
				,ObjectID
				,TableID
				,TaskEndTime
				,InsertRowCount
				,UpdateRowCount
				,DeleteRowCount
				,Error
				,ErrorDescription
				,Details
				)
				VALUES
				(
				 @ParentTaskID
				,@ProcessID
				,@TaskNameID
				,@DatabaseID
				,@ObjectID
				,@TableID
				,@TaskEndTime
				,@InsertRowCount
				,@UpdateRowCount
				,@DeleteRowCount
				,@Error
				,@ErrorDescription
				,@Details
				)
			OUTPUT Inserted.TaskID
			INTO @OuputTable (TaskID);
		END
		
		/*3)ReadTheTaskID*/
		IF @OutputTaskID IS NULL
			SELECT @OutputTaskID = TaskID FROM @OuputTable
				
END TRY
BEGIN CATCH		
	DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity INT				= ERROR_SEVERITY()
			,@ErrorState	INT				= ERROR_STATE()

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH
