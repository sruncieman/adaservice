﻿CREATE PROCEDURE [dbo].[uspDeleteBackupDirectories]
(
 @DirectoryPath VARCHAR(200)
,@RetentionPeriodInDays INT
)
AS
SET NOCOUNT ON

--------------------------------------
--ValidateInputVariablesAndDeclareCreateTemporayObject
--------------------------------------
IF NOT EXISTS (SELECT TOP 1 1 FROM [dbo].[DBControlDeleteEnabledDirectories] WHERE [DirectoryPath] = @DirectoryPath)
BEGIN
	RAISERROR ('ThisDirectoryPathCannotBeDeleted:ProcessAborted',18,255)
	RETURN
END

IF @RetentionPeriodInDays < (SELECT [MinimumRetentionPeriodInDays] FROM [dbo].[DBControlDeleteEnabledDirectories] WHERE [DirectoryPath] = @DirectoryPath)
BEGIN
	RAISERROR ('TheRetentionPeriodInDaysIsLessThanTheMinimum:ProcessAborted',18,255)
	RETURN
END

DECLARE @MyFiles TABLE (MyID INT IDENTITY(1,1) PRIMARY KEY, FullPath VARCHAR(2000))
DECLARE @CMDRemoveDirectories TABLE (ID INT IDENTITY(1,1), CMDCommand VARCHAR(200))

DECLARE  @CommandLine	VARCHAR(4000) = (SELECT LEFT('DIR "' + @DirectoryPath + '" /A-D /B /S /O-D',4000))
		,@CurrentID		INT = 1
		,@RowsToProcess INT = 0

------------------------------------
--GenerateDirectoryList
------------------------------------
INSERT INTO @MyFiles (FullPath)
EXECUTE xp_cmdshell @CommandLine
DELETE FROM @MyFiles WHERE fullpath IS NULL OR fullpath='File Not Found' OR fullpath = 'The system cannot find the file specified.' or patindex('%[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]%',fullpath) = 0

INSERT INTO @CMDRemoveDirectories (CMDCommand)
SELECT 'RD /S /Q "' + Directory + '"'
FROM	(
		SELECT   fullpath
				,SUBSTRING(fullpath, 1, PATINDEX ('%[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]%' , fullpath) + 9) Directory
				,CAST(SUBSTRING(fullpath,PATINDEX ('%[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]%' , fullpath),10) as date)  DirectoryDate
				,CASE WHEN CAST(SUBSTRING(fullpath,PATINDEX ('%[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]%' , fullpath),10) as date) < DATEADD(D,-@RetentionPeriodInDays,GETDATE()) THEN 1 ELSE 0 END DeleteFile
		FROM @MyFiles
		) Files
WHERE DeleteFile = 1
GROUP BY Directory
SELECT @RowsToProcess = @@ROWCOUNT 

--------------------------------
--LoopTableExecutingCMDCommand
---------------------------------
WHILE @CurrentID <= @RowsToProcess
BEGIN
	SELECT @CommandLine = (SELECT CMDCommand FROM @CMDRemoveDirectories WHERE ID = @CurrentID)
	EXECUTE xp_cmdshell @CommandLine
	PRINT 'TheFollowingCMDCommandWasExecute:' + @CommandLine
	SELECT @CurrentID += 1
END




