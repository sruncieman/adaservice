﻿


CREATE PROCEDURE[Sync].[uspSynchroniseLinkAddress]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseLinkAddress]		
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--						2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)MDAIBase: LinkTablesChanges
--							3.2.1)ActionTheLinkChangeInMDA_IBase
--							3.2.2)DoDeletes
--							3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						3.3)MDAIBase: LinkEndTablesChanges
--							3.3.1)PopulateTheWorkingTable
--							3.3.2)ActionTheLinkEndChangeInMDA_IBase
--							3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
--						4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
--							4.1)ActionTheChangeInMDA
--							4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
--							4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						5)UpdateLoggingAndTidy
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-04-23			Paul Allen		Created
--		2015-06-10			Paul Allen		Added [dbo].[Handset2Address]
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT						
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT
			,@PhysicalTableName		VARCHAR(255) = 'Address_to_Address_Link'
			,@Prefix				VARCHAR(3)   
			,@LinkTypeID			INT

	SELECT @LinkTypeID =Table_ID, @Prefix = TableCode FROM [MDA_IBase].[dbo]._DataTable WHERE PhysicalName = @PhysicalTableName

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1) PRIMARY KEY, ChangeDatabase VARCHAR(10), PhysicalTableName VARCHAR(255), MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging
	CREATE TABLE #LinkEndStaging (Link_ID NVARCHAR(50), Entity_ID1 NVARCHAR(50), Confidence TINYINT, Direction TINYINT, Entity_ID2 NVARCHAR(50), EntityType_ID1 INT , EntityType_ID2 INT, LinkType_ID INT, Record_Status TINYINT, Record_Type TINYINT, SCC NVARCHAR(255))
			
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 85 --SynchroniseAddressLink
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Organisation2Address', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA].[dbo].Organisation2Address, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Person2Address', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA].[dbo].Person2Address, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Vehicle2Address', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA].[dbo].Vehicle2Address, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Handset2Address', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA].[dbo].Handset2Address, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Address_to_Address_Link', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA_IBase].[dbo].Address_to_Address_Link, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Link_End', Link_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [MDA_IBase].[dbo]._LinkEnd, @MDA_IBaseChangeVersionFrom) CT
	INNER JOIN [MDA_IBase].[dbo].[Address_to_Address_Link] IA2AL ON IA2AL.Unique_ID = CT.Link_ID
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL
	GROUP BY Link_ID, Sys_Change_Operation

	--2.2)AddSomeIndexesToMakeTheProcessQuicker 
	SET @Step ='2.2';

	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT (MDA_IBase_Unique_ID ASC)
	CREATE NONCLUSTERED INDEX IX_CT_PhysicalTableName ON #CT (PhysicalTableName ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3';

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IA2A.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[Address_to_Address_Link] IA2A ON IA2A.Unique_ID = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Address_to_Address_Link'
	AND IA2A.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IVSL.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[VW_StagingLinks] IVSL ON IVSL.[IBase8LinkID] = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Link_End'
	AND IVSL.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MO2A.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].[Organisation2Address] MO2A ON MO2A.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Organisation2Address'

	UPDATE CT
	SET MDA_IBase_Unique_ID = MP2A.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].[Person2Address] MP2A ON MP2A.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Person2Address'

	UPDATE CT
	SET MDA_IBase_Unique_ID = MV2A.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].[Vehicle2Address] MV2A ON MV2A.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Vehicle2Address'

	UPDATE CT
	SET MDA_IBase_Unique_ID = MV2A.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].[Handset2Address] MV2A ON MV2A.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Handset2Address'

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4';

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				AND PhysicalTableName != 'Link_End'
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5';

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1
	
	--2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
	SET @Step ='2.6';
	UPDATE #CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT	RowID
				FROM	(
						SELECT   RowID
								,ROW_NUMBER() OVER (PARTITION BY MDA_IBase_Unique_ID ORDER BY RowID) RowNumber
						FROM #CT CT 
						WHERE EXISTS (SELECT TOP 1 1 FROM #CT CT1 WHERE MDA_IBase_Unique_ID = MDA_IBase_Unique_ID AND ChangeDatabase = 'MDA_IBase' GROUP BY MDA_IBase_Unique_ID HAVING COUNT(1) > 1)
						) Data 
				WHERE RowNumber !=1
				) IQ ON IQ.RowID = CT.RowID
	WHERE ChangeDatabase = 'MDA_IBase'

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1';

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM [MDA_IBase].[dbo]._Address_to_Address_Link_NextID
		UPDATE [MDA_IBase].[dbo]._Address_to_Address_Link_NextID SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	 @Prefix + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY RowID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--------------------------------------------------------------------------
		--3.2)MDAIBase: LinkTablesChanges
		--------------------------------------------------------------------------
		--3.2.1)ActionTheLinkChangeInMDA_IBase
		SET @Step ='3.2.1';

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[Address_to_Address_Link] [Target]
		USING  (
				SELECT CT.PhysicalTableName, CT.MDA_ID, CT.MDA_IBase_Unique_ID, IQ.*, ALT.[LinkType] AddressLinkType
				FROM #CT CT
				INNER JOIN (
							SELECT RowID, ID, IBaseId [Unique_ID], AddressLinkType_Id [Address_Link_Type], AddressLinkId [Address_to_Address_Link_ID], CreatedDate [Create_Date], CreatedBy [Create_User], EndOfResidency [End_of_Residency], ModifiedDate [Last_Upd_Date] ,ModifiedBy [Last_Upd_User], Notes [Notes_], RecordStatus [Record_Status], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], StartOfResidency [Start_of_Residency], RiskClaim_Id [Risk_Claim_ID_414244883], FiveGrading  [x5x5x5_Grading_412284402]  
							FROM [MDA].[dbo].[Organisation2Address] MO2A
							INNER JOIN #CT CT ON CT.MDA_ID = MO2A.ID
							WHERE CT.PhysicalTableName = 'Organisation2Address'
							UNION ALL
							SELECT RowID, ID, IBaseId [Unique_ID], AddressLinkType_Id [Address_Link_Type], AddressLinkId [Address_to_Address_Link_ID], CreatedDate [Create_Date], CreatedBy [Create_User], EndOfResidency [End_of_Residency], ModifiedDate [Last_Upd_Date] ,ModifiedBy [Last_Upd_User], Notes [Notes_], RecordStatus [Record_Status], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], StartOfResidency [Start_of_Residency], RiskClaim_Id [Risk_Claim_ID_414244883], FiveGrading  [x5x5x5_Grading_412284402] 
							FROM [MDA].[dbo].[Person2Address] MP2A
							INNER JOIN #CT CT ON CT.MDA_ID = MP2A.ID
							WHERE CT.PhysicalTableName = 'Person2Address'
							UNION ALL
							SELECT RowID, MV2A.ID, IBaseId [Unique_ID], AddressLinkType_Id [Address_Link_Type], AddressLinkId [Address_to_Address_Link_ID], CreatedDate [Create_Date], CreatedBy [Create_User], EndOfResidency [End_of_Residency], ModifiedDate [Last_Upd_Date] ,ModifiedBy [Last_Upd_User], Notes [Notes_], MV2A.RecordStatus [Record_Status], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], StartOfResidency [Start_of_Residency], RiskClaim_Id [Risk_Claim_ID_414244883], FiveGrading  [x5x5x5_Grading_412284402] 
							FROM [MDA].[dbo].[Vehicle2Address] MV2A
							INNER JOIN #CT CT ON CT.MDA_ID = MV2A.ID
							INNER JOIN [MDA].[dbo].[AddressLinkType] ALT ON ALT.[Id] = MV2A.AddressLinkType_Id
							WHERE CT.PhysicalTableName = 'Vehicle2Address'
							AND (ALT.LinkType NOT IN ('Storage Address','Recovered to','Repaired at','Inspected at') OR LEFT(MV2A.[SourceDescription],3) != 'ADA') 
							UNION ALL 
							SELECT RowID, ID, IBaseId [Unique_ID], AddressLinkType_Id [Address_Link_Type], AddressLinkId [Address_to_Address_Link_ID], CreatedDate [Create_Date], CreatedBy [Create_User], EndOfResidency [End_of_Residency], ModifiedDate [Last_Upd_Date] ,ModifiedBy [Last_Upd_User], Notes [Notes_], RecordStatus [Record_Status], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], StartOfResidency [Start_of_Residency], RiskClaim_Id [Risk_Claim_ID_414244883], FiveGrading  [x5x5x5_Grading_412284402] 
							FROM [MDA].[dbo].[Handset2Address] MP2A
							INNER JOIN #CT CT ON CT.MDA_ID = MP2A.ID
							WHERE CT.PhysicalTableName = 'Handset2Address'
							) IQ ON IQ.RowID = CT.RowID
				INNER JOIN [MDA].[dbo].[AddressLinkType] ALT ON ALT.[Id] = IQ.[Address_Link_Type]
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET  Address_Link_Type				= CASE WHEN Source.[Address_Link_Type] = 0 AND ISNULL(Source.Source_Description_411765489,'') !='ADA' THEN [Target].Address_Link_Type ELSE NULLIF(REPLACE(REPLACE(Source.AddressLinkType,'Current Address','Lives at'),'Storage Address','Stored at'),'Unknown') END
			,Create_Date					= Source.Create_Date
			,Create_User					= Source.Create_User
			,End_of_Residency				= Source.End_of_Residency
			,Last_Upd_Date					= Source.Last_Upd_Date
			,Last_Upd_User					= Source.Last_Upd_User
			,Notes_							= Source.Notes_
			,Record_Status					= Source.Record_Status
			,Source_411765484				= Source.Source_411765484
			,Source_Description_411765489	= Source.Source_Description_411765489
			,Source_Reference_411765487		= Source.Source_Reference_411765487
			,Start_of_Residency				= Source.Start_of_Residency
			,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
			,Risk_Claim_ID_414244883		= Source.Risk_Claim_ID_414244883
		WHEN NOT MATCHED THEN INSERT 
			(
			 Unique_ID
			,Address_Link_Type
			,Create_Date
			,Create_User
			,Do_Not_Disseminate_412284494
			,End_of_Residency
			,Last_Upd_Date
			,Last_Upd_User
			,MDA_Incident_ID_412284502
			,Notes_
			,Record_Status
			,Source_411765484
			,Source_Description_411765489
			,Source_Reference_411765487
			,Start_of_Residency
			,x5x5x5_Grading_412284402
			,Risk_Claim_ID_414244883
			)
		VALUES
			(
			 Source.MDA_IBase_Unique_ID
			,CASE WHEN Source.[Address_Link_Type] = 0 THEN NULL ELSE REPLACE(REPLACE(Source.AddressLinkType,'Current Address','Lives at'),'Storage Address','Stored at') END
			,Source.Create_Date
			,Source.Create_User
			,0
			,Source.End_of_Residency
			,Source.Last_Upd_Date
			,Source.Last_Upd_User
			,CASE WHEN Source.PhysicalTableName= 'Organisation2Address' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Organisation2Address') WHEN Source.PhysicalTableName= 'Person2Address' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Person2Address') WHEN Source.PhysicalTableName= 'Vehicle2Address' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Vehicle2Address') WHEN Source.PhysicalTableName= 'Handset2Address' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Handset2Address') ELSE NULL END
			,Source.Notes_
			,Source.Record_Status
			,Source.Source_411765484
			,Source.Source_Description_411765489
			,Source.Source_Reference_411765487
			,Source.Start_of_Residency
			,Source.x5x5x5_Grading_412284402
			,Source.Risk_Claim_ID_414244883
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.[Address_Link_Type],'') + '}|I:{' + ISNULL(Inserted.[Address_Link_Type],'') + '}'
				,'D:{' + ISNULL(Deleted.[Address_to_Address_Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Address_to_Address_Link_ID],'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[End_of_Residency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[End_of_Residency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[IconColour],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
				,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes_],'') + '}|I:{' + ISNULL(Inserted.[Notes_],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SCC],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Start_of_Residency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Start_of_Residency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Status_Binding],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[x5x5x5_Grading_412284402],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Risk_Claim_ID_414244883],'')) + '}'
		INTO [SyncDML].[MDAIBase_Address_to_Address_Link] (TaskID, DMLAction, Unique_ID, Address_Link_Type, Address_to_Address_Link_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, End_of_Residency, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Start_of_Residency, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

		--3.2.2)DoDeletes
		--WeNeedToDeleteTheLinkEndrecordBeforetheDeletesOrWeWontBeAbleTo
		SET @Step ='3.2.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [MDA_IBase].[dbo].[_LinkEnd] LE
		INNER JOIN (
					SELECT [IBase8LinkID] Link_ID
					FROM
							(
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Organisation2Address') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Organisation2Address'
							AND DMLAction = 'D'
							UNION ALL
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Person2Address') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Person2Address'
							AND DMLAction = 'D'
							UNION ALL
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Vehicle2Address') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Vehicle2Address'
							AND DMLAction = 'D'
							UNION ALL
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Handset2Address') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Handset2Address'
							AND DMLAction = 'D'
							) DATA
					INNER JOIN [MDA_IBase].[dbo].[VW_StagingLinks] IVSL ON IVSL.[MDA_Incident_ID_412284502] = DATA.MDA_Incident_ID_412284502
					) IQ ON IQ.Link_ID = LE.Link_ID;

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[Address_to_Address_Link]
		OUTPUT	 @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Address_Link_Type],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Address_to_Address_Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[End_of_Residency],''),126) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Notes_],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Start_of_Residency],''),126) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Address_to_Address_Link] (TaskID, DMLAction, Unique_ID, Address_Link_Type, Address_to_Address_Link_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, End_of_Residency, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Start_of_Residency, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Address_to_Address_Link] IA2AL
		INNER JOIN #CT CT ON CASE WHEN CT.PhysicalTableName = 'Organisation2Address' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID, 'MDA.dbo.Organisation2Address') WHEN CT.PhysicalTableName= 'Person2Address' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Person2Address') WHEN CT.PhysicalTableName= 'Vehicle2Address' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Vehicle2Address') WHEN CT.PhysicalTableName= 'Handset2Address' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Handset2Address') ELSE NULL END = IA2AL.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';

		--3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		SET @Step ='3.2.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MADD
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Organisation_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Organisation_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Address_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Address_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Organisation2Address] (TaskID, DMLAction, Id, Organisation_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].Organisation2Address MADD
		INNER JOIN #CT CT ON CT.MDA_ID = MADD.Id
 		WHERE (MADD.IBaseId IS NULL
		OR MADD.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Organisation2Address';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MADD
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Person_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Person_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Address_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Address_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[TracesmartIKey],'') + '}|I:{' + ISNULL(Inserted.[TracesmartIKey],'') + '}'
				,'D:{' + ISNULL(Deleted.[TracesmartAddrSource],'') + '}|I:{' + ISNULL(Inserted.[TracesmartAddrSource],'') + '}'
				,'D:{' + ISNULL(Deleted.[TSIDUAMLResult],'') + '}|I:{' + ISNULL(Inserted.[TSIDUAMLResult],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[TracesmartDOB],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[TracesmartDOB],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ExperianDOB],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ExperianDOB],'')) + '}'
				,'D:{' + ISNULL(Deleted.[GoneAway],'') + '}|I:{' + ISNULL(Inserted.[GoneAway],'') + '}'
				,'D:{' + ISNULL(Deleted.[Insolvency],'') + '}|I:{' + ISNULL(Inserted.[Insolvency],'') + '}'
				,'D:{' + ISNULL(Deleted.[CCJHistory],'') + '}|I:{' + ISNULL(Inserted.[CCJHistory],'') + '}'
				,'D:{' + ISNULL(Deleted.[InsolvencyHistory],'') + '}|I:{' + ISNULL(Inserted.[InsolvencyHistory],'') + '}'
				,'D:{' + ISNULL(Deleted.[DeathScreenMatchType],'') + '}|I:{' + ISNULL(Inserted.[DeathScreenMatchType],'') + '}'
				,'D:{' + ISNULL(Deleted.[DeathScreenDoD],'') + '}|I:{' + ISNULL(Inserted.[DeathScreenDoD],'') + '}'
				,'D:{' + ISNULL(Deleted.[DeathScreenRegNo],'') + '}|I:{' + ISNULL(Inserted.[DeathScreenRegNo],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CredivaCheck],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CredivaCheck],'')) + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Person2Address] (TaskID, [DMLAction], Id, Person_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, TracesmartIKey, TracesmartAddrSource, TSIDUAMLResult, TracesmartDOB, ExperianDOB, GoneAway, Insolvency, CCJHistory, InsolvencyHistory, DeathScreenMatchType, DeathScreenDoD, DeathScreenRegNo, CredivaCheck, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].Person2Address MADD
		INNER JOIN #CT CT ON CT.MDA_ID = MADD.Id
 		WHERE (MADD.IBaseId IS NULL
		OR MADD.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Person2Address';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MADD
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Vehicle_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Vehicle_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Address_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Address_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Vehicle2Address] (TaskID, DMLAction, Id, Vehicle_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].Vehicle2Address MADD
		INNER JOIN #CT CT ON CT.MDA_ID = MADD.Id
 		WHERE (MADD.IBaseId IS NULL
		OR MADD.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Vehicle2Address';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MADD
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Handset_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Handset_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Address_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Address_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Handset2Address] (TaskID, DMLAction, Id, Handset_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].Handset2Address MADD
		INNER JOIN #CT CT ON CT.MDA_ID = MADD.Id
 		WHERE (MADD.IBaseId IS NULL
		OR MADD.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Handset2Address';

		--------------------------------------------------------------------------
		--3.3)MDAIBase: LinkEndTablesChanges
		-------------------------------------------------------------------------
		--3.3.1)PopulateTheWorkingTable
		--DoForEntityID1
		--CheckTheValuesThatArebeingAdded
		SET @Step ='3.3.1';

		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC
		FROM	(
				SELECT MO2A.IBaseId Link_ID, MA.IBaseId Entity_ID1, MO2A.LinkConfidence Confidence, 0 Direction, MO.IBaseId Entity_ID2, 1065 EntityType_ID1, 1893 EntityType_ID2, @LinkTypeID LinkType_ID, MO2A.RecordStatus Record_Status, 1 Record_Type, NULL SCC
				FROM [MDA].[dbo].Organisation2Address MO2A 
				INNER JOIN [MDA].[dbo].[Address] MA ON MA.ID = MO2A.Address_Id
				INNER JOIN [MDA].[dbo].Organisation MO ON MO.ID = MO2A.Organisation_ID
				INNER JOIN #CT CT ON CT.MDA_ID = MO2A.ID AND CT.PhysicalTableName = 'Organisation2Address'
				UNION ALL
				SELECT MP2A.IBaseId, MA.IBaseId, MP2A.LinkConfidence, 0, MP.IBaseId, 1065, 1969, @LinkTypeID, MP2A.RecordStatus, 1, NULL
				FROM [MDA].[dbo].Person2Address MP2A 
				INNER JOIN [MDA].[dbo].[Address] MA ON MA.ID = MP2A.Address_Id
				INNER JOIN [MDA].[dbo].Person MP ON MP.ID = MP2A.Person_ID
				INNER JOIN #CT CT ON CT.MDA_ID = MP2A.ID AND CT.PhysicalTableName = 'Person2Address'
				UNION ALL
				SELECT MV2A.IBaseId, MA.IBaseId, MV2A.LinkConfidence, 0, MV.IBaseId, 1065, 2321, @LinkTypeID, MV2A.RecordStatus, 1, NULL
				FROM [MDA].[dbo].Vehicle2Address MV2A 
				INNER JOIN [MDA].[dbo].[Address] MA ON MA.ID = MV2A.Address_Id
				INNER JOIN [MDA].[dbo].Vehicle MV ON MV.ID = MV2A.Vehicle_Id
				INNER JOIN #CT CT ON CT.MDA_ID = MV2A.ID AND CT.PhysicalTableName = 'Vehicle2Address'
				INNER JOIN [MDA].[dbo].[AddressLinkType] ALT ON ALT.[Id] = MV2A.AddressLinkType_Id
				WHERE (ALT.LinkType NOT IN ('Storage Address','Recovered to','Repaired at','Inspected at') OR LEFT(MV2A.[SourceDescription],3) != 'ADA') 
				UNION ALL
				SELECT MP2A.IBaseId, MA.IBaseId, MP2A.LinkConfidence, 0, MP.IBaseId, 1065, 4027, @LinkTypeID, MP2A.RecordStatus, 1, NULL
				FROM [MDA].[dbo].Handset2Address MP2A 
				INNER JOIN [MDA].[dbo].[Address] MA ON MA.ID = MP2A.Address_Id
				INNER JOIN [MDA].[dbo].Handset MP ON MP.ID = MP2A.Handset_Id
				INNER JOIN #CT CT ON CT.MDA_ID = MP2A.ID AND CT.PhysicalTableName = 'Person2Handset'
				) Data

		--NowDoForEntityID2
		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT Link_ID, Entity_ID2, Confidence, Direction, Entity_ID1, EntityType_ID2, EntityType_ID1, LinkType_ID, Record_Status, 2, SCC
		FROM #LinkEndStaging;

		CREATE CLUSTERED INDEX IX_LinkEndStaging_Link_ID_Entity_ID1 ON #LinkEndStaging (Link_ID, Entity_ID1);

		--3.3.2)ActionTheLinkEndChangeInMDA_IBase
		SET @Step ='3.3.2';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[_LinkEnd] [Target]
		USING	(
				SELECT *
				FROM #LinkEndStaging
				) Source ON Source.Link_ID = [Target].Link_ID AND Source.Entity_ID1 = [Target].Entity_ID1
		WHEN MATCHED AND CHECKSUM(Source.Link_ID, Source.Entity_ID1, Source.Confidence, Source.Direction, Source.Entity_ID2, Source.EntityType_ID1, Source.EntityType_ID2, Source.LinkType_ID, Source.Record_Status, Source.Record_Type, Source.SCC) != CHECKSUM([Target].Link_ID, [Target].Entity_ID1, [Target].Confidence, [Target].Direction, [Target].Entity_ID2, [Target].EntityType_ID1, [Target].EntityType_ID2, [Target].LinkType_ID, [Target].Record_Status, [Target].Record_Type, [Target].SCC) THEN UPDATE
		SET  Confidence			= Source.Confidence
			,Direction			= Source.Direction
			,Entity_ID2			= Source.Entity_ID2
			,EntityType_ID1		= Source.EntityType_ID1
			,EntityType_ID2		= Source.EntityType_ID2
			,LinkType_ID		= Source.LinkType_ID
			,Record_Status		= Source.Record_Status
			,Record_Type		= Source.Record_Type
			,SCC				= Source.SCC
		WHEN NOT MATCHED THEN INSERT
			(
			 Link_ID
			,Entity_ID1
			,Confidence
			,Direction
			,Entity_ID2
			,EntityType_ID1
			,EntityType_ID2
			,LinkType_ID
			,Record_Status
			,Record_Type
			,SCC
			)
		VALUES
			(
			 Source.Link_ID
			,Source.Entity_ID1
			,Source.Confidence
			,Source.Direction
			,Source.Entity_ID2
			,Source.EntityType_ID1
			,Source.EntityType_ID2
			,Source.LinkType_ID
			,Source.Record_Status
			,Source.Record_Type
			,Source.SCC
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Link_ID],'') + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID1],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Confidence],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Direction],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID2],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID1],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID2],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkType_ID],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Type],'')) + '}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{' + ISNULL(Inserted.[SCC],'') + '}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC);

		--3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
		SET @Step ='3.3.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		DELETE  [MDA_IBase].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [MDA_IBase].[dbo].[_LinkEnd] LE
		WHERE EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID)
		AND NOT EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID AND LES.Entity_ID1 = LE.Entity_ID1);

	END

	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--Organisation2Address
		SET @Step ='4.1';

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].[Organisation2Address] [Target]
		USING  (
				SELECT   CT.MDA_ID
						,CT.MDA_IBase_Unique_ID
						,[Sync].[fn_ExtractMDAID] (IOR.MDA_Incident_ID_412284502) [Organisation_ID]
						,[Sync].[fn_ExtractMDAID] (IA.MDA_Incident_ID_412284502) [Address_ID]
						,IA2AL.Address_to_Address_Link_ID [AddressLinkId]
						,ISNULL(ALT.ID,0) [AddressLinkType_Id]
						,IA2AL.Start_of_Residency [StartOfResidency]
						,IA2AL.End_of_Residency [EndOfResidency]
						,IA2AL.Notes_ [Notes]
						,IA2AL.[x5x5x5_Grading_412284402] [FiveGrading]
						,ILE.Confidence [LinkConfidence]
						,IA2AL.[Source_411765484] [Source]
						,IA2AL.[Source_Reference_411765487] [SourceReference]
						,IA2AL.[Source_Description_411765489] [SourceDescription]
						,IA2AL.Create_User [CreatedBy]
						,IA2AL.Create_Date [CreatedDate]
						,IA2AL.Last_Upd_User [ModifiedBy]
						,IA2AL.Last_Upd_Date [ModifiedDate]
						,IA2AL.[Risk_Claim_ID_414244883] [RiskClaim_Id]
						,IA2AL.[Risk_Claim_ID_414244883] [BaseRiskClaim_Id]
						,IA2AL.Unique_ID [IBaseId]
						,IA2AL.Record_Status [RecordStatus]		
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Address_to_Address_Link] IA2AL ON IA2AL.Unique_ID = CT.MDA_IBase_Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IA2AL.Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[Address_] IA ON IA.Unique_ID = ILE.Entity_ID1
				INNER JOIN [MDA_IBase].[dbo].[Organisation_] IOR ON IOR.Unique_ID = ILE.Entity_ID2
				LEFT JOIN [MDA].[dbo].[AddressLinkType] ALT ON ALT.[LinkType] = REPLACE(REPLACE(IA2AL.[Address_Link_Type],'Lives at','Current Address'),'Stored at','Storage Address')
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET	 Organisation_Id			= Source.Organisation_Id
			,Address_Id					= Source.Address_Id
			,AddressLinkId				= Source.AddressLinkId
			,AddressLinkType_Id			= Source.AddressLinkType_Id
			,StartOfResidency			= Source.StartOfResidency
			,EndOfResidency				= Source.EndOfResidency
			,Notes						= Source.Notes
			,FiveGrading				= Source.FiveGrading
			,LinkConfidence				= Source.LinkConfidence
			,Source						= Source.Source
			,SourceReference			= Source.SourceReference
			,SourceDescription			= Source.SourceDescription
			,CreatedBy					= Source.CreatedBy
			,CreatedDate				= Source.CreatedDate
			,ModifiedBy					= Source.ModifiedBy
			,ModifiedDate				= Source.ModifiedDate
			,IBaseId					= Source.IBaseId
			,RecordStatus				= Source.RecordStatus
			,ADARecordStatus			= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT 
			(
			 Organisation_Id
			,Address_Id
			,AddressLinkId
			,AddressLinkType_Id
			,StartOfResidency
			,EndOfResidency
			,Notes
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES
			(
			 Source.Organisation_Id
			,Source.Address_Id
			,Source.AddressLinkId
			,Source.AddressLinkType_Id
			,Source.StartOfResidency
			,Source.EndOfResidency
			,Source.Notes
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Organisation_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Organisation_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Address_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Address_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Organisation2Address] (TaskID, DMLAction, Id, Organisation_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus);

		--Person2Address
		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].[Person2Address] [Target]
		USING  (
				SELECT   CT.MDA_ID
						,CT.MDA_IBase_Unique_ID
						,[Sync].[fn_ExtractMDAID] (IP.MDA_Incident_ID_412284502) [Person_Id]
						,[Sync].[fn_ExtractMDAID] (IA.MDA_Incident_ID_412284502)  [Address_ID]
						,IA2AL.Address_to_Address_Link_ID [AddressLinkId]
						,ISNULL(ALT.ID,0) [AddressLinkType_Id]
						,IA2AL.Start_of_Residency [StartOfResidency]
						,IA2AL.End_of_Residency [EndOfResidency]
						,IA2AL.Notes_ [Notes]
						,IA2AL.[x5x5x5_Grading_412284402] [FiveGrading]
						,ILE.Confidence [LinkConfidence]
						,IA2AL.[Source_411765484] [Source]
						,IA2AL.[Source_Reference_411765487] [SourceReference]
						,IA2AL.[Source_Description_411765489] [SourceDescription]
						,IA2AL.Create_User [CreatedBy]
						,IA2AL.Create_Date [CreatedDate]
						,IA2AL.Last_Upd_User [ModifiedBy]
						,IA2AL.Last_Upd_Date [ModifiedDate]
						,IA2AL.[Risk_Claim_ID_414244883] [RiskClaim_Id]
						,IA2AL.[Risk_Claim_ID_414244883] [BaseRiskClaim_Id]
						,IA2AL.Unique_ID [IBaseId]
						,IA2AL.Record_Status [RecordStatus]		
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Address_to_Address_Link] IA2AL ON IA2AL.Unique_ID = CT.MDA_IBase_Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IA2AL.Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[Address_] IA ON IA.Unique_ID = ILE.Entity_ID1
				INNER JOIN [MDA_IBase].[dbo].[Person_] IP ON IP.Unique_ID = ILE.Entity_ID2
				LEFT JOIN [MDA].[dbo].[AddressLinkType] ALT ON ALT.[LinkType] = REPLACE(REPLACE(IA2AL.[Address_Link_Type],'Lives at','Current Address'),'Stored at','Storage Address')
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  Person_Id				= Source.Person_Id
			,Address_Id				= Source.Address_Id
			,AddressLinkId			= Source.AddressLinkId
			,AddressLinkType_Id		= Source.AddressLinkType_Id
			,StartOfResidency		= Source.StartOfResidency
			,EndOfResidency			= Source.EndOfResidency
			,Notes					= Source.Notes
			,FiveGrading			= Source.FiveGrading
			,LinkConfidence			= Source.LinkConfidence
			,Source					= Source.Source
			,SourceReference		= Source.SourceReference	
			,SourceDescription		= Source.SourceDescription
			,CreatedBy				= Source.CreatedBy
			,CreatedDate			= Source.CreatedDate
			,ModifiedBy				= Source.ModifiedBy
			,ModifiedDate			= Source.ModifiedDate
			,IBaseId				= Source.IBaseId
			,RecordStatus			= Source.RecordStatus
			,ADARecordStatus		= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
			 Person_Id
			,Address_Id
			,AddressLinkId
			,AddressLinkType_Id
			,StartOfResidency
			,EndOfResidency
			,Notes
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus	
			)
		VALUES
			(
			 Source.Person_Id
			,Source.Address_Id
			,Source.AddressLinkId
			,Source.AddressLinkType_Id
			,Source.StartOfResidency
			,Source.EndOfResidency
			,Source.Notes
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10		
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Person_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Person_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Address_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Address_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[TracesmartIKey],'') + '}|I:{' + ISNULL(Inserted.[TracesmartIKey],'') + '}'
				,'D:{' + ISNULL(Deleted.[TracesmartAddrSource],'') + '}|I:{' + ISNULL(Inserted.[TracesmartAddrSource],'') + '}'
				,'D:{' + ISNULL(Deleted.[TSIDUAMLResult],'') + '}|I:{' + ISNULL(Inserted.[TSIDUAMLResult],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[TracesmartDOB],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[TracesmartDOB],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ExperianDOB],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ExperianDOB],'')) + '}'
				,'D:{' + ISNULL(Deleted.[GoneAway],'') + '}|I:{' + ISNULL(Inserted.[GoneAway],'') + '}'
				,'D:{' + ISNULL(Deleted.[Insolvency],'') + '}|I:{' + ISNULL(Inserted.[Insolvency],'') + '}'
				,'D:{' + ISNULL(Deleted.[CCJHistory],'') + '}|I:{' + ISNULL(Inserted.[CCJHistory],'') + '}'
				,'D:{' + ISNULL(Deleted.[InsolvencyHistory],'') + '}|I:{' + ISNULL(Inserted.[InsolvencyHistory],'') + '}'
				,'D:{' + ISNULL(Deleted.[DeathScreenMatchType],'') + '}|I:{' + ISNULL(Inserted.[DeathScreenMatchType],'') + '}'
				,'D:{' + ISNULL(Deleted.[DeathScreenDoD],'') + '}|I:{' + ISNULL(Inserted.[DeathScreenDoD],'') + '}'
				,'D:{' + ISNULL(Deleted.[DeathScreenRegNo],'') + '}|I:{' + ISNULL(Inserted.[DeathScreenRegNo],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CredivaCheck],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CredivaCheck],'')) + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Person2Address] (TaskID, [DMLAction], Id, Person_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, TracesmartIKey, TracesmartAddrSource, TSIDUAMLResult, TracesmartDOB, ExperianDOB, GoneAway, Insolvency, CCJHistory, InsolvencyHistory, DeathScreenMatchType, DeathScreenDoD, DeathScreenRegNo, CredivaCheck, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus);

		--Vehicle2Address
		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].[Vehicle2Address] [Target]
		USING  (
				SELECT   CT.MDA_ID
						,CT.MDA_IBase_Unique_ID
						,[Sync].[fn_ExtractMDAID] (IV.MDA_Incident_ID_412284502) [Vehicle_Id]
						,[Sync].[fn_ExtractMDAID] (IA.MDA_Incident_ID_412284502)  [Address_ID]
						,IA2AL.Address_to_Address_Link_ID [AddressLinkId]
						,ISNULL(ALT.ID,0) [AddressLinkType_Id]
						,IA2AL.Start_of_Residency [StartOfResidency]
						,IA2AL.End_of_Residency [EndOfResidency]
						,IA2AL.Notes_ [Notes]
						,IA2AL.[x5x5x5_Grading_412284402] [FiveGrading]
						,ILE.Confidence [LinkConfidence]
						,IA2AL.[Source_411765484] [Source]
						,IA2AL.[Source_Reference_411765487] [SourceReference]
						,IA2AL.[Source_Description_411765489] [SourceDescription]
						,IA2AL.Create_User [CreatedBy]
						,IA2AL.Create_Date [CreatedDate]
						,IA2AL.Last_Upd_User [ModifiedBy]
						,IA2AL.Last_Upd_Date [ModifiedDate]
						,IA2AL.[Risk_Claim_ID_414244883] [RiskClaim_Id]
						,IA2AL.[Risk_Claim_ID_414244883] [BaseRiskClaim_Id]
						,IA2AL.Unique_ID [IBaseId]
						,IA2AL.Record_Status [RecordStatus]		
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Address_to_Address_Link] IA2AL ON IA2AL.Unique_ID = CT.MDA_IBase_Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IA2AL.Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[Address_] IA ON IA.Unique_ID = ILE.Entity_ID1
				INNER JOIN [MDA_IBase].[dbo].[Vehicle_] IV ON IV.Unique_ID = ILE.Entity_ID2
				LEFT JOIN [MDA].[dbo].[AddressLinkType] ALT ON ALT.[LinkType] = REPLACE(REPLACE(IA2AL.[Address_Link_Type],'Lives at','Current Address'),'Stored at','Storage Address')
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  Vehicle_Id				= Source.Vehicle_Id
			,Address_Id				= Source.Address_Id
			,AddressLinkId			= Source.AddressLinkId
			,AddressLinkType_Id		= Source.AddressLinkType_Id
			,StartOfResidency		= Source.StartOfResidency
			,EndOfResidency			= Source.EndOfResidency
			,Notes					= Source.Notes
			,FiveGrading			= Source.FiveGrading
			,LinkConfidence			= Source.LinkConfidence
			,Source					= Source.Source
			,SourceReference		= Source.SourceReference	
			,SourceDescription		= Source.SourceDescription
			,CreatedBy				= Source.CreatedBy
			,CreatedDate			= Source.CreatedDate
			,ModifiedBy				= Source.ModifiedBy
			,ModifiedDate			= Source.ModifiedDate
			,IBaseId				= Source.IBaseId
			,RecordStatus			= Source.RecordStatus
			,ADARecordStatus		= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
			 Vehicle_Id
			,Address_Id
			,AddressLinkId
			,AddressLinkType_Id
			,StartOfResidency
			,EndOfResidency
			,Notes
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus	
			)
		VALUES
			(
			 Source.Vehicle_Id
			,Source.Address_Id
			,Source.AddressLinkId
			,Source.AddressLinkType_Id
			,Source.StartOfResidency
			,Source.EndOfResidency
			,Source.Notes
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10		
			)
		OUTPUT	 @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Vehicle_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Vehicle_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Address_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Address_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Vehicle2Address] (TaskID, DMLAction, Id, Vehicle_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus);

		--Handset2Address
		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].[Handset2Address] [Target]
		USING  (
				SELECT   CT.MDA_ID
						,CT.MDA_IBase_Unique_ID
						,[Sync].[fn_ExtractMDAID] (IV.MDA_Incident_ID_412284502) [Handset_Id]
						,[Sync].[fn_ExtractMDAID] (IA.MDA_Incident_ID_412284502)  [Address_ID]
						,IA2AL.Address_to_Address_Link_ID [AddressLinkId]
						,ISNULL(ALT.ID,0) [AddressLinkType_Id]
						,IA2AL.Start_of_Residency [StartOfResidency]
						,IA2AL.End_of_Residency [EndOfResidency]
						,IA2AL.Notes_ [Notes]
						,IA2AL.[x5x5x5_Grading_412284402] [FiveGrading]
						,ILE.Confidence [LinkConfidence]
						,IA2AL.[Source_411765484] [Source]
						,IA2AL.[Source_Reference_411765487] [SourceReference]
						,IA2AL.[Source_Description_411765489] [SourceDescription]
						,IA2AL.Create_User [CreatedBy]
						,IA2AL.Create_Date [CreatedDate]
						,IA2AL.Last_Upd_User [ModifiedBy]
						,IA2AL.Last_Upd_Date [ModifiedDate]
						,IA2AL.[Risk_Claim_ID_414244883] [RiskClaim_Id]
						,IA2AL.[Risk_Claim_ID_414244883] [BaseRiskClaim_Id]
						,IA2AL.Unique_ID [IBaseId]
						,IA2AL.Record_Status [RecordStatus]		
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Address_to_Address_Link] IA2AL ON IA2AL.Unique_ID = CT.MDA_IBase_Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IA2AL.Unique_ID
				INNER JOIN [MDA_IBase].[dbo].[Address_] IA ON IA.Unique_ID = ILE.Entity_ID1
				INNER JOIN [MDA_IBase].[dbo].[Handset_] IV ON IV.Unique_ID = ILE.Entity_ID2
				LEFT JOIN [MDA].[dbo].[AddressLinkType] ALT ON ALT.[LinkType] = REPLACE(REPLACE(IA2AL.[Address_Link_Type],'Lives at','Current Address'),'Stored at','Storage Address')
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  Handset_Id				= Source.Handset_Id
			,Address_Id				= Source.Address_Id
			,AddressLinkId			= Source.AddressLinkId
			,AddressLinkType_Id		= Source.AddressLinkType_Id
			,StartOfResidency		= Source.StartOfResidency
			,EndOfResidency			= Source.EndOfResidency
			,Notes					= Source.Notes
			,FiveGrading			= Source.FiveGrading
			,LinkConfidence			= Source.LinkConfidence
			,Source					= Source.Source
			,SourceReference		= Source.SourceReference	
			,SourceDescription		= Source.SourceDescription
			,CreatedBy				= Source.CreatedBy
			,CreatedDate			= Source.CreatedDate
			,ModifiedBy				= Source.ModifiedBy
			,ModifiedDate			= Source.ModifiedDate
			,IBaseId				= Source.IBaseId
			,RecordStatus			= Source.RecordStatus
			,ADARecordStatus		= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
			 Handset_Id
			,Address_Id
			,AddressLinkId
			,AddressLinkType_Id
			,StartOfResidency
			,EndOfResidency
			,Notes
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus	
			)
		VALUES
			(
			 Source.Handset_Id
			,Source.Address_Id
			,Source.AddressLinkId
			,Source.AddressLinkType_Id
			,Source.StartOfResidency
			,Source.EndOfResidency
			,Source.Notes
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10		
			)
		OUTPUT	 @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Handset_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Handset_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Address_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Address_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Handset2Address] (TaskID, DMLAction, Id, Handset_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus);

		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--Organisation2Address
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Organisation2Address]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Organisation_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Organisation_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Address_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Address_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Organisation2Address] (TaskID, DMLAction, Id, Organisation_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Organisation2Address] O2A
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = O2A.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--Person2Address
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Person2Address]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Person_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Person_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Address_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Address_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Person2Address] (TaskID, DMLAction, Id, Person_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Person2Address] P2A
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = P2A.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--Vehicle2Address
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Vehicle2Address]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Vehicle_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Vehicle_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Address_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Address_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Vehicle2Address] (TaskID, DMLAction, Id, Vehicle_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Vehicle2Address] V2A
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = V2A.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--Handset2Address
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Handset2Address]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Handset_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Handset_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Address_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Address_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[AddressLinkId],'') + '}|I:{' + ISNULL(Inserted.[AddressLinkId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[AddressLinkType_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[AddressLinkType_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[StartOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[StartOfResidency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EndOfResidency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EndOfResidency],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[RawAddress],'') + '}|I:{' + ISNULL(Inserted.[RawAddress],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes],'') + '}|I:{' + ISNULL(Inserted.[Notes],'') + '}'
				,'D:{' + ISNULL(Deleted.[FiveGrading],'') + '}|I:{' + ISNULL(Inserted.[FiveGrading],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkConfidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkConfidence],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Source],'') + '}|I:{' + ISNULL(Inserted.[Source],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceReference],'') + '}|I:{' + ISNULL(Inserted.[SourceReference],'') + '}'
				,'D:{' + ISNULL(Deleted.[SourceDescription],'') + '}|I:{' + ISNULL(Inserted.[SourceDescription],'') + '}'
				,'D:{' + ISNULL(Deleted.[CreatedBy],'') + '}|I:{' + ISNULL(Inserted.[CreatedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[CreatedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[CreatedDate],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[ModifiedBy],'') + '}|I:{' + ISNULL(Inserted.[ModifiedBy],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ModifiedDate],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ModifiedDate],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RiskClaim_Id],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[BaseRiskClaim_Id],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[BaseRiskClaim_Id],'')) + '}'
				,'D:{' + ISNULL(Deleted.[IBaseId],'') + '}|I:{' + ISNULL(Inserted.[IBaseId],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[RecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[RecordStatus],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[ADARecordStatus],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[ADARecordStatus],'')) + '}'
		INTO [SyncDML].[MDA_Handset2Address] (TaskID, DMLAction, Id, Handset_Id, Address_Id, AddressLinkId, AddressLinkType_Id, StartOfResidency, EndOfResidency, RawAddress, Notes, FiveGrading, LinkConfidence, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, RiskClaim_Id, BaseRiskClaim_Id, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Handset2Address] V2A
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = V2A.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA_IBase].[dbo].[Address_to_Address_Link]
		SET  [MDA_Incident_ID_412284502] = IQ.MDA_Incident_ID_412284502
			,[Risk_Claim_ID_414244883]	 = IQ.RiskClaim_Id
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.[Address_Link_Type],'') + '}|I:{' + ISNULL(Inserted.[Address_Link_Type],'') + '}'
				,'D:{' + ISNULL(Deleted.[Address_to_Address_Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Address_to_Address_Link_ID],'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[End_of_Residency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[End_of_Residency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[IconColour],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[IconColour],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Key_Attractor_412284410],'') + '}|I:{' + ISNULL(Inserted.[Key_Attractor_412284410],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Last_Upd_Date],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Last_Upd_Date],''),126) + '}'
				,'D:{' + ISNULL(Deleted.[Last_Upd_User],'') + '}|I:{' + ISNULL(Inserted.[Last_Upd_User],'') + '}'
				,'D:{' + ISNULL(Deleted.[MDA_Incident_ID_412284502],'') + '}|I:{' + ISNULL(Inserted.[MDA_Incident_ID_412284502],'') + '}'
				,'D:{' + ISNULL(Deleted.[Notes_],'') + '}|I:{' + ISNULL(Inserted.[Notes_],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[SCC],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[SCC],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_411765484],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_411765484],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Description_411765489],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Description_411765489],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Source_Reference_411765487],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Source_Reference_411765487],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Start_of_Residency],''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Start_of_Residency],''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Status_Binding],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Status_Binding],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[x5x5x5_Grading_412284402],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[x5x5x5_Grading_412284402],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Risk_Claim_ID_414244883],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Risk_Claim_ID_414244883],'')) + '}'
		INTO [SyncDML].[MDAIBase_Address_to_Address_Link] (TaskID, DMLAction, Unique_ID, Address_Link_Type, Address_to_Address_Link_ID, AltEntity, Create_Date, Create_User, Do_Not_Disseminate_412284494, End_of_Residency, IconColour, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Notes_, Record_Status, SCC, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Start_of_Residency, Status_Binding, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Address_to_Address_Link] IA2AL
		INNER JOIN (
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MO2A.ID,'MDA.dbo.Organisation2Address') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [MDA].[dbo].[Organisation2Address] MO2A
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MO2A.IBaseId
					UNION ALL
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MP2A.ID,'MDA.dbo.Person2Address') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [MDA].[dbo].[Person2Address] MP2A
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MP2A.IBaseId
					UNION ALL
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MV2A.ID,'MDA.dbo.Vehicle2Address') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [MDA].[dbo].[Vehicle2Address] MV2A
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MV2A.IBaseId
					UNION ALL
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MV2A.ID,'MDA.dbo.Handset2Address') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [MDA].[dbo].[Handset2Address] MV2A
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MV2A.IBaseId
					) IQ ON IQ.[Unique_ID] =  IA2AL.[Unique_ID]
		WHERE IA2AL.MDA_Incident_ID_412284502 IS NULL
		OR IA2AL.MDA_Incident_ID_412284502 != IQ.MDA_Incident_ID_412284502

	END 

	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Organisation2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Organisation2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Organisation2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Person2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Person2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Person2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Vehicle2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Handset2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Handset2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Handset2Address] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_to_Address_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_to_Address_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Address_to_Address_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging

END TRY
BEGIN CATCH
	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(500)	= ISNULL(ERROR_PROCEDURE(),@@PROCID)
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +ISNULL(@Step,'?') +']'
	
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH

