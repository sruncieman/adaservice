﻿

CREATE PROCEDURE  [Sync].[uspSynchroniseEntityOrganisation]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseEntityOrganisation]
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)ActionTheChangeInMDA_IBase
--						3.3)DoDeletes
--						3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)	
--						4.1)ActionTheChangeInMDA
--						4.2)DoDeletes
--						4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
--					5)UpdateLoggingAndTidy		
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-05-07			Paul Allen		Created
--		2014-11-19			Paul Allen		Sync SanctionList Column - TFS 10036
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT
)
AS
SET NOCOUNT ON
BEGIN TRY
	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT			

	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1), ChangeDatabase VARCHAR(10) ,MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)
					
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 110 --SynchroniseOrganisationEntity
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, MDA_ID, DMLAction)
	SELECT 'MDA', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA.dbo.Organisation, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES MDA_IBase.dbo.Organisation_, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	--2.2)AddSomeIndexesToMakeTheProcessQuicker
	SET @Step ='2.2'
	 
	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT(MDA_IBase_Unique_ID ASC)

	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3'

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IOR.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [MDA_IBase].[dbo].[Organisation_] IOR ON IOR.Unique_ID = CT.MDA_IBase_Unique_ID
	AND IOR.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MO.IBaseId
	FROM #CT CT
	INNER JOIN [MDA].[dbo].[Organisation] MO ON MO.ID = CT.MDA_ID

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4'

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5'

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1'

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM MDA_IBase.[dbo].[_Organisation__NextID]
		UPDATE MDA_IBase.dbo.[_Organisation__NextID] SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	'ORG' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY MDA_ID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--3.2)ActionTheChangeInMDA_IBase
		SET @Step ='3.2'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA_IBase].[dbo].[Organisation_] [Target]
		USING	(
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, MO.*, MOT.[TypeText] [Organisation_Type], MOS.[StatusText] [Organisation_Status], MMS.[Status] [MoJ_CRM_Status]
				FROM #CT CT
				INNER JOIN [MDA].[dbo].[Organisation] MO ON MO.ID = CT.MDA_ID
				INNER JOIN MDA.dbo.OrganisationType MOT ON MOT.ID = MO.OrganisationType_Id
				INNER JOIN MDA.dbo.OrganisationStatus MOS ON MOS.ID = MO.[OrganisationStatus_Id]
				INNER JOIN MDA.dbo.MojCRMStatus MMS ON MMS.ID = MO.MojCRMStatus_Id
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA'
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET	 AltEntity						= CASE WHEN Source.[Organisation_Type] IN ('Accident Management','Broker','Credit Hire','Insurer','Self Insured','Solicitor') THEN 'Office' WHEN Source.[Organisation_Type] IN ('Medical Examiner') THEN 'Hospital' WHEN Source.[Organisation_Type] IN ('Recovery','Storage','Repairer','Vehicle Engineer') THEN 'Garage' ELSE [Target].AltEntity END
			,Create_Date					= Source.[CreatedDate]
			,Create_User					= Source.[CreatedBy]
			,Credit_Licence					= Source.[CreditLicense]
			,Effective_Date_of_Status		= Source.[EffectiveDateOfStatus]
			,Incorporated_Date				= Source.[IncorporatedDate]
			,Key_Attractor_412284410		= Source.[KeyAttractor]
			,Last_Upd_Date					= Source.[ModifiedDate]
			,Last_Upd_User					= Source.[ModifiedBy]
			,MoJ_CRM_Number					= Source.[MojCRMNumber]
			,MoJ_CRM_Status					= CASE WHEN Source.[MojCRMStatus_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Target].[Organisation_Status] ELSE NULLIF(Source.[Organisation_Status],'Unknown') END
			,Organisation_Name				= Source.[OrganisationName]
			,Organisation_Status			= CASE WHEN Source.[OrganisationStatus_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Target].[MoJ_CRM_Status] ELSE NULLIF(Source.[MoJ_CRM_Status],'Unknown') END
			,Organisation_Type				= CASE WHEN Source.[OrganisationType_Id] = 0 AND ISNULL(Source.[SourceDescription],'') !='ADA' THEN [Target].Organisation_Type ELSE NULLIF(REPLACE(Source.Organisation_Type,'Accident Management','Accident or Claims Management'),'Unknown') END
			,Record_Status					= Source.[RecordStatus]
			,Registered_Number				= Source.[RegisteredNumber]
			,SIC_							= Source.[SIC]
			,Source_411765484				= Source.[Source]
			,Source_Description_411765489	= Source.[SourceDescription]
			,Source_Reference_411765487		= Source.[SourceReference]
			,VAT_Number						= Source.[VATNumber]
			,VAT_Number_Validated_Date		= Source.[VATNumberValidationDate]
			,[CHFKeyAttractor_]				= ISNULL(REPLACE(Source.[CHFKeyAttractor],1,-1),0)
			,[CHFKeyAttractorRemoveDate_]	= Source.[CHFKeyAttractorRemoveDate]
			,Sanction_List					= Source.SanctionList
		WHEN NOT MATCHED THEN INSERT 
			(
			 Unique_ID
			,AltEntity
			,Create_Date
			,Create_User
			,Credit_Licence
			,Do_Not_Disseminate_412284494
			,Effective_Date_of_Status
			,Incorporated_Date
			,Key_Attractor_412284410
			,Last_Upd_Date
			,Last_Upd_User
			,MDA_Incident_ID_412284502
			,MoJ_CRM_Number
			,MoJ_CRM_Status
			,Organisation_ID
			,Organisation_Name
			,Organisation_Status
			,Organisation_Type
			,Record_Status
			,Registered_Number
			,SIC_
			,Source_411765484
			,Source_Description_411765489
			,Source_Reference_411765487
			,VAT_Number
			,VAT_Number_Validated_Date
			,[CHFKeyAttractor_]
			,[CHFKeyAttractorRemoveDate_]
			,Sanction_List
			)
		VALUES 
			(
			 Source.MDA_IBase_Unique_ID
			,CASE WHEN Source.[Organisation_Type] IN ('Accident Management','Broker','Credit Hire','Insurer','Self Insured','Solicitor') THEN 'Office' WHEN Source.[Organisation_Type] IN ('Medical Examiner') THEN 'Hospital' WHEN Source.[Organisation_Type] IN ('Recovery','Storage','Repairer','Vehicle Engineer') THEN 'Garage' ELSE NULL END
			,Source.[CreatedDate]
			,Source.[CreatedBy]
			,Source.[CreditLicense]
			,0
			,Source.[EffectiveDateOfStatus]
			,Source.[IncorporatedDate]
			,Source.[KeyAttractor]
			,Source.[ModifiedDate]
			,Source.[ModifiedBy]
			,[Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Organisation')
			,Source.[MojCRMNumber]
			,CASE WHEN Source.[MojCRMStatus_Id] = 0 THEN NULL ELSE Source.[Organisation_Status] END
			,Source.[OrganisationId]
			,Source.[OrganisationName]
			,CASE WHEN Source.[MojCRMStatus_Id] = 0 THEN NULL ELSE Source.[Organisation_Status] END
			,CASE WHEN Source.[OrganisationType_Id] = 0 THEN NULL ELSE REPLACE(Source.Organisation_Type,'Accident Management','Accident or Claims Management') END
			,Source.[RecordStatus]
			,Source.[RegisteredNumber]
			,Source.[SIC]
			,Source.[SourceReference]
			,Source.[SourceDescription]
			,Source.[SourceReference]
			,Source.[VATNumber]
			,Source.[VATNumberValidationDate]
			,ISNULL(REPLACE(Source.[CHFKeyAttractor],1,-1),0)
			,Source.[CHFKeyAttractorRemoveDate]
			,Source.SanctionList
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Credit_Licence,'') + '}|I:{' + ISNULL(Inserted.Credit_Licence,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Effective_Date_of_Status,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Effective_Date_of_Status,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incorporated_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incorporated_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.MoJ_CRM_Number,'') + '}|I:{' + ISNULL(Inserted.MoJ_CRM_Number,'') + '}'
				,'D:{' + ISNULL(Deleted.MoJ_CRM_Status,'') + '}|I:{' + ISNULL(Inserted.MoJ_CRM_Status,'') + '}'
				,'D:{' + ISNULL(Deleted.Organisation_ID,'') + '}|I:{' + ISNULL(Inserted.Organisation_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Organisation_Name,'') + '}|I:{' + ISNULL(Inserted.Organisation_Name,'') + '}'
				,'D:{' + ISNULL(Deleted.Organisation_Status,'') + '}|I:{' + ISNULL(Inserted.Organisation_Status,'') + '}'
				,'D:{' + ISNULL(Deleted.Organisation_Type,'') + '}|I:{' + ISNULL(Inserted.Organisation_Type,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.Registered_Number,'') + '}|I:{' + ISNULL(Inserted.Registered_Number,'') + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.SIC_,'') + '}|I:{' + ISNULL(Inserted.SIC_,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.VAT_Number,'') + '}|I:{' + ISNULL(Inserted.VAT_Number,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VAT_Number_Validated_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VAT_Number_Validated_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Organisation_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Credit_Licence, Do_Not_Disseminate_412284494, Effective_Date_of_Status, IconColour, Incorporated_Date, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, MoJ_CRM_Number, MoJ_CRM_Status, Organisation_ID, Organisation_Name, Organisation_Status, Organisation_Type, Record_Status, Registered_Number, SCC, SIC_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, VAT_Number, VAT_Number_Validated_Date, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883);

		--3.3)DoDeletes
		SET @Step ='3.3';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [MDA_IBase].[dbo].[Organisation_]
		OUTPUT	 @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Credit_Licence,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Effective_Date_of_Status,''),126) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incorporated_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MoJ_CRM_Number,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MoJ_CRM_Status,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Organisation_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Organisation_Name,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Organisation_Status,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Organisation_Type,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Registered_Number,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SIC_,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.VAT_Number,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VAT_Number_Validated_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase_Organisation_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Credit_Licence, Do_Not_Disseminate_412284494, Effective_Date_of_Status, IconColour, Incorporated_Date, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, MoJ_CRM_Number, MoJ_CRM_Status, Organisation_ID, Organisation_Name, Organisation_Status, Organisation_Type, Record_Status, Registered_Number, SCC, SIC_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, VAT_Number, VAT_Number_Validated_Date, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Organisation_] MOR
		INNER JOIN #CT CT ON [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Organisation') = MOR.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';

		--3.4)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='3.4';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MOR
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.OrganisationId,'') + '}|I:{' + ISNULL(Inserted.OrganisationId,'') + '}'
				,'D:{' + ISNULL(Deleted.OrganisationName,'') + '}|I:{' + ISNULL(Inserted.OrganisationName,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncorporatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncorporatedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.EffectiveDateOfStatus,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.EffectiveDateOfStatus,''),126) + '}'
				,'D:{' + ISNULL(Deleted.RegisteredNumber,'') + '}|I:{' + ISNULL(Inserted.RegisteredNumber,'') + '}'
				,'D:{' + ISNULL(Deleted.SIC,'') + '}|I:{' + ISNULL(Inserted.SIC,'') + '}'
				,'D:{' + ISNULL(Deleted.VATNumber,'') + '}|I:{' + ISNULL(Inserted.VATNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VATNumberValidationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VATNumberValidationDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.MojCRMNumber,'') + '}|I:{' + ISNULL(Inserted.MojCRMNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojCRMStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojCRMStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.CreditLicense,'') + '}|I:{' + ISNULL(Inserted.CreditLicense,'') + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Organisation] (TaskID, DMLAction,  Id, OrganisationId, OrganisationName, OrganisationType_Id, IncorporatedDate, OrganisationStatus_Id, EffectiveDateOfStatus, RegisteredNumber, SIC, VATNumber, VATNumberValidationDate, MojCRMNumber, MojCRMStatus_Id, CreditLicense, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM MDA.dbo.[Organisation] MOR
		INNER JOIN #CT CT ON CT.MDA_ID = MOR.Id
 		WHERE MOR.IBaseId IS NULL
		OR MOR.IBaseId != CT.MDA_IBase_Unique_ID;

	END

	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		SET @Step ='4.1'

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [MDA].[dbo].[Organisation] [Target]
		USING  (
				SELECT CT.MDA_ID, CT.MDA_IBase_Unique_ID, IOR.*, MOT.ID OrganisationType_Id, MOS.ID [OrganisationStatus_Id], MMS.ID MojCRMStatus_Id
				FROM #CT CT
				INNER JOIN [MDA_IBase].[dbo].[Organisation_] IOR ON IOR.Unique_ID = CT.MDA_IBase_Unique_ID
				LEFT JOIN MDA.dbo.OrganisationType MOT ON MOT.[TypeText] = REPLACE(IOR.[Organisation_Type],'Accident or Claims Management','Accident Management')
				LEFT JOIN MDA.dbo.OrganisationStatus MOS ON MOS.[StatusText] = IOR.[Organisation_Status]
				LEFT JOIN MDA.dbo.MojCRMStatus MMS ON MMS.[Status] = IOR.[MoJ_CRM_Status]
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED THEN UPDATE
		SET  OrganisationName			= Source.[Organisation_Name]
			,OrganisationType_Id		= ISNULL(Source.OrganisationType_Id,0)
			,IncorporatedDate			= Source.[Incorporated_Date]
			,OrganisationStatus_Id		= ISNULL(Source.[OrganisationStatus_Id],0)
			,EffectiveDateOfStatus		= Source.[Effective_Date_of_Status]
			,RegisteredNumber			= Source.[Registered_Number]
			,SIC						= Source.[SIC_]
			,VATNumber					= Source.[VAT_Number]
			,VATNumberValidationDate	= Source.[VAT_Number_Validated_Date]
			,MojCRMNumber				= Source.[MoJ_CRM_Number]
			,MojCRMStatus_Id			= ISNULL(Source.MojCRMStatus_Id,0)
			,CreditLicense				= Source.[Credit_Licence]
			,KeyAttractor				= Source.[Key_Attractor_412284410]
			,CHFKeyAttractor			= ABS(ISNULL(Source.[CHFKeyAttractor_],0))
			,CHFKeyAttractorRemoveDate  = Source.[CHFKeyAttractorRemoveDate_]
			,Source						= Source.[Source_411765484]
			,SourceReference			= Source.[Source_Reference_411765487]
			,SourceDescription			= Source.[Source_Description_411765489]
			,CreatedBy					= Source.[Create_User]
			,CreatedDate				= Source.[Create_Date]
			,ModifiedBy					= Source.[Last_Upd_User]
			,ModifiedDate				= Source.[Last_Upd_Date]
			,IBaseId					= Source.[Unique_ID]
			,RecordStatus				= Source.[Record_Status]
			,ADARecordStatus			= CASE WHEN Source.Record_Status = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
			,SanctionList				= Source.Sanction_List
		WHEN NOT MATCHED THEN INSERT
			(
			 OrganisationId
			,OrganisationName
			,OrganisationType_Id
			,IncorporatedDate
			,OrganisationStatus_Id
			,EffectiveDateOfStatus
			,RegisteredNumber
			,SIC
			,VATNumber
			,VATNumberValidationDate
			,MojCRMNumber
			,MojCRMStatus_Id
			,CreditLicense
			,KeyAttractor
			,CHFKeyAttractor
			,CHFKeyAttractorRemoveDate
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			,SanctionList
			)
		VALUES
			(
			 Source.[Organisation_ID]
			,Source.[Organisation_Name]
			,ISNULL(Source.OrganisationType_Id,0)
			,Source.[Incorporated_Date]
			,ISNULL(Source.[OrganisationStatus_Id],0)
			,Source.[Effective_Date_of_Status]
			,Source.[Registered_Number]
			,Source.[SIC_]
			,Source.[VAT_Number]
			,Source.[VAT_Number_Validated_Date]
			,Source.[MoJ_CRM_Number]
			,ISNULL(Source.MojCRMStatus_Id,0)
			,Source.[Credit_Licence]
			,Source.[Key_Attractor_412284410]
			,ABS(ISNULL(Source.[CHFKeyAttractor_],0))
			,Source.[CHFKeyAttractorRemoveDate_]
			,Source.[Source_411765484]
			,Source.[Source_Reference_411765487]
			,Source.[Source_Description_411765489]
			,Source.[Create_User]
			,Source.[Create_Date]
			,Source.[Last_Upd_User]
			,Source.[Last_Upd_Date]
			,Source.[Unique_ID]
			,Source.[Record_Status]
			,10
			,Source.Sanction_List
			)
		OUTPUT	 @OutputTaskID
				,$ACTION
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.OrganisationId,'') + '}|I:{' + ISNULL(Inserted.OrganisationId,'') + '}'
				,'D:{' + ISNULL(Deleted.OrganisationName,'') + '}|I:{' + ISNULL(Inserted.OrganisationName,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncorporatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncorporatedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.EffectiveDateOfStatus,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.EffectiveDateOfStatus,''),126) + '}'
				,'D:{' + ISNULL(Deleted.RegisteredNumber,'') + '}|I:{' + ISNULL(Inserted.RegisteredNumber,'') + '}'
				,'D:{' + ISNULL(Deleted.SIC,'') + '}|I:{' + ISNULL(Inserted.SIC,'') + '}'
				,'D:{' + ISNULL(Deleted.VATNumber,'') + '}|I:{' + ISNULL(Inserted.VATNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VATNumberValidationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VATNumberValidationDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.MojCRMNumber,'') + '}|I:{' + ISNULL(Inserted.MojCRMNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojCRMStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojCRMStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.CreditLicense,'') + '}|I:{' + ISNULL(Inserted.CreditLicense,'') + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Organisation] (TaskID, DMLAction,  Id, OrganisationId, OrganisationName, OrganisationType_Id, IncorporatedDate, OrganisationStatus_Id, EffectiveDateOfStatus, RegisteredNumber, SIC, VATNumber, VATNumberValidationDate, MojCRMNumber, MojCRMStatus_Id, CreditLicense, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus);

		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [MDA].[dbo].[Organisation]
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	 @OutputTaskID
				,'UPDATE DELETE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.OrganisationId,'') + '}|I:{' + ISNULL(Inserted.OrganisationId,'') + '}'
				,'D:{' + ISNULL(Deleted.OrganisationName,'') + '}|I:{' + ISNULL(Inserted.OrganisationName,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationType_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationType_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IncorporatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IncorporatedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OrganisationStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OrganisationStatus_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.EffectiveDateOfStatus,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.EffectiveDateOfStatus,''),126) + '}'
				,'D:{' + ISNULL(Deleted.RegisteredNumber,'') + '}|I:{' + ISNULL(Inserted.RegisteredNumber,'') + '}'
				,'D:{' + ISNULL(Deleted.SIC,'') + '}|I:{' + ISNULL(Inserted.SIC,'') + '}'
				,'D:{' + ISNULL(Deleted.VATNumber,'') + '}|I:{' + ISNULL(Inserted.VATNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VATNumberValidationDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VATNumberValidationDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.MojCRMNumber,'') + '}|I:{' + ISNULL(Inserted.MojCRMNumber,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.MojCRMStatus_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.MojCRMStatus_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.CreditLicense,'') + '}|I:{' + ISNULL(Inserted.CreditLicense,'') + '}'
				,'D:{' + ISNULL(Deleted.KeyAttractor,'') + '}|I:{' + ISNULL(Inserted.KeyAttractor,'') + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Organisation] (TaskID, DMLAction,  Id, OrganisationId, OrganisationName, OrganisationType_Id, IncorporatedDate, OrganisationStatus_Id, EffectiveDateOfStatus, RegisteredNumber, SIC, VATNumber, VATNumberValidationDate, MojCRMNumber, MojCRMStatus_Id, CreditLicense, KeyAttractor, Source, SourceReference, SourceDescription, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, IBaseId, RecordStatus, ADARecordStatus)
		FROM [MDA].[dbo].[Organisation] MO
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MO.IBaseId
		WHERE CT.DMLAction = 'D';

		--4.3)NowWritretheUniqueIDInserteditoMDAIBaseBackToMDA
		SET @Step ='4.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE IOR
		SET MDA_Incident_ID_412284502 = [Sync].[fn_ResolveMDAID] (MO.ID,'MDA.dbo.Organisation')
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Credit_Licence,'') + '}|I:{' + ISNULL(Inserted.Credit_Licence,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Effective_Date_of_Status,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Effective_Date_of_Status,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incorporated_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incorporated_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.MoJ_CRM_Number,'') + '}|I:{' + ISNULL(Inserted.MoJ_CRM_Number,'') + '}'
				,'D:{' + ISNULL(Deleted.MoJ_CRM_Status,'') + '}|I:{' + ISNULL(Inserted.MoJ_CRM_Status,'') + '}'
				,'D:{' + ISNULL(Deleted.Organisation_ID,'') + '}|I:{' + ISNULL(Inserted.Organisation_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Organisation_Name,'') + '}|I:{' + ISNULL(Inserted.Organisation_Name,'') + '}'
				,'D:{' + ISNULL(Deleted.Organisation_Status,'') + '}|I:{' + ISNULL(Inserted.Organisation_Status,'') + '}'
				,'D:{' + ISNULL(Deleted.Organisation_Type,'') + '}|I:{' + ISNULL(Inserted.Organisation_Type,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.Registered_Number,'') + '}|I:{' + ISNULL(Inserted.Registered_Number,'') + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.SIC_,'') + '}|I:{' + ISNULL(Inserted.SIC_,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.VAT_Number,'') + '}|I:{' + ISNULL(Inserted.VAT_Number,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.VAT_Number_Validated_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.VAT_Number_Validated_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		INTO [SyncDML].[MDAIBase_Organisation_] (TaskID, DMLAction, Unique_ID, AltEntity, Create_Date, Create_User, Credit_Licence, Do_Not_Disseminate_412284494, Effective_Date_of_Status, IconColour, Incorporated_Date, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, MoJ_CRM_Number, MoJ_CRM_Status, Organisation_ID, Organisation_Name, Organisation_Status, Organisation_Type, Record_Status, Registered_Number, SCC, SIC_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, VAT_Number, VAT_Number_Validated_Date, x5x5x5_Grading_412284402, Risk_Claim_ID_414244883)
		FROM [MDA_IBase].[dbo].[Organisation_] IOR
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = IOR.Unique_ID
		INNER JOIN [MDA].[dbo].[Organisation] MO ON MO.IBaseId = IOR.Unique_ID
 		WHERE IOR.MDA_Incident_ID_412284502 IS NULL
		OR [Sync].[fn_ResolveMDAID] (MO.ID,'MDA.dbo.Organisation') != IOR.MDA_Incident_ID_412284502
		
	END

	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		= (SELECT COUNT(1) FROM [SyncDML].[MDA_Organisation] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Organisation_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Organisation_] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount		+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Organisation_] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

END TRY
BEGIN CATCH
	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(100)	= ERROR_PROCEDURE()
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +@Step +']'

	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH