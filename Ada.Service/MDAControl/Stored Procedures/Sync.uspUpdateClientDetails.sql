﻿CREATE PROCEDURE Sync.uspUpdateClientDetails
AS
SET NOCOUNT ON

---------------------------------------
--ClientNamesAsPerVFExtract..
---------------------------------------
DECLARE @VFClientNames TABLE (ClientName VARCHAR(50))
INSERT INTO @VFClientNames (ClientName)
SELECT 'esure' UNION ALL
SELECT 'The Co-operative Insurance' UNION ALL
SELECT 'Aviva Pre Proc Credit Hire' UNION ALL
SELECT 'ERS Claims Ltd' UNION ALL
SELECT 'Service' UNION ALL
SELECT 'Aviva Insurance UK Limited' UNION ALL
SELECT 'Service Underwriting Management Ltd' UNION ALL
SELECT 'Direct Line Group' UNION ALL
SELECT 'LV=' UNION ALL
SELECT 'NFU Mutual' UNION ALL
SELECT 'Zurich Commercial' UNION ALL
SELECT 'Allianz Insurance PLC' UNION ALL
SELECT 'Haven Insurance' UNION ALL
SELECT 'Churchill Insurance' UNION ALL
SELECT 'Zurich Global Corporate' UNION ALL
SELECT 'AXA Insurance UK Plc' UNION ALL
SELECT 'Zurich UK PL Motor' UNION ALL
SELECT 'Zurich Municipal' UNION ALL
SELECT 'Uk Insurance Limited' UNION ALL
SELECT 'Hastings Insurance Services' UNION ALL
SELECT 'Privilege' UNION ALL
SELECT 'Prospect Legal Ltd' UNION ALL
SELECT 'Hastings Direct' UNION ALL
SELECT 'Admiral Insurance Ltd - JG' UNION ALL
SELECT 'Covea Insurance Plc - FF'

--ClientNamesAsPerMDAClients
INSERT INTO @VFClientNames (ClientName)
SELECT DISTINCT REPLACE(REPLACE(REPLACE(REPLACE(ClientName,' Pre-Lit Credit Hire',''),' Litigated Credit Hire',''),' - JG',''),' - FF','')
FROM [MDA].[dbo].[RiskClient]

UPDATE [MDA].[dbo].[InsurersClients] SET [InsurerClientParent_Id] = NULL

--AddTheNewClientName
INSERT INTO [MDA].[dbo].[InsurersClients] (ClientName)
SELECT DISTINCT ClientName
FROM @VFClientNames VF
WHERE NOT EXISTS (SELECT TOP 1 1 FROM [MDA].[dbo].[InsurersClients] IC WHERE IC.ClientName = VF.ClientName collate SQL_Latin1_General_CP1_CI_AS)

---------------------------------
--SetTheInsurerClientParent_Id
---------------------------------
--ForAdmiral
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Admiral')
WHERE LEFT(ClientName,7) =  'Admiral' 

--ForAllianz
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Allianz')
WHERE LEFT(ClientName,7) =  'Allianz' 

--ForAviva
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Aviva')
WHERE LEFT(ClientName,5) ='Aviva'

--ForAXA
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'AXA')
WHERE LEFT(ClientName,3) ='AXA'

--ForCoOp
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Co-operative Insurance')
WHERE ClientName LIKE '%Co-op%' OR ClientName = 'CIS'

--ForCovea
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Covea')
WHERE LEFT(ClientName,5) ='Covea'

--ForDLG
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'DLG')
WHERE LEFT(ClientName,3) ='DLG' OR ClientName = 'Direct Line Group'

--ForEsure
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Esure')
WHERE LEFT(ClientName,5) ='Esure'

--ForHastings
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Hastings Direct')
WHERE LEFT(ClientName,8) ='Hastings' 

--ForLiberty
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Liberty')
WHERE LEFT(ClientName,7) ='Liberty' 

--ForLV=
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'LV=')
WHERE LEFT(ClientName,2) ='LV' OR ClientName = 'Liverpool Victoria'

--ForService
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Service')
WHERE LEFT(ClientName,7) ='Service'

--ForChurchill
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Churchill')
WHERE LEFT(ClientName,9) ='Churchill'

--ForNFU
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'NFU Mutual')
WHERE LEFT(ClientName,3) ='NFU'

--ForProspect
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Prospect Legal')
WHERE LEFT(ClientName,8) ='Prospect'

--ForUKI
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'UKI')
WHERE LEFT(ClientName,2) ='UK'

--ForHaven
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'Haven')
WHERE LEFT(ClientName,5) ='Haven'

--ForAce
UPDATE [MDA].[dbo].[InsurersClients]
SET [InsurerClientParent_Id] = (SELECT Id FROM [MDA].[dbo].[InsurersClients] WHERE ClientName = 'ACE')
WHERE LEFT(ClientName,3) ='ACE'

-------------------------------------------
--UpdateRiskClient
-------------------------------------------
UPDATE RC
SET [InsurersClients_Id] = UPD.InsurersClients_Id
FROM [MDA].[dbo].[RiskClient] RC
INNER JOIN (
			SELECT	 ROW_NUMBER() OVER (PARTITION BY RC.ID ORDER BY LEN(IC.ClientName) DESC) RowID
					,RC.ID ID
					,ISNULL(IC.[InsurerClientParent_Id],IC.ID) [InsurersClients_Id]
			FROM [MDA].[dbo].[RiskClient] RC
			LEFT JOIN [MDA].[dbo].[InsurersClients] IC ON IC.ClientName = LEFT(RC.ClientName,LEN(IC.ClientName))
			WHERE RC.ClientName NOT IN ('Cue')
			) UPD ON UPD.ID = RC.ID
WHERE RowID = 1