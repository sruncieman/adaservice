﻿CREATE PROCEDURE [IBaseM].[uspRepopulateOutcomeData]
--WITH EXECUTE AS 'SVC-ADA-PowerUser'
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
SET DATEFORMAT DMY

------------------------------------------
--DeclareVariablesAndTempObjects
------------------------------------------
--Drop the Table [IBaseM].[OutcomeDatafromDWH] and recreate...
DECLARE  @NextId				INT				= 0
		,@RecordCount			INT
		,@Create_Date			SMALLDATETIME	= GETDATE()
		,@Create_User			VARCHAR(50)		= 'ADA Automated Outcome Data User'
		,@Sys_Change_Context	VARBINARY(128)	= CONVERT(VARBINARY(128),'DWHOutcomeData')
		,@InsertCount			INT

IF OBJECT_ID('tempdb..#OutcomeDatafromDWHMatched') IS NOT NULL
	DROP TABLE #OutcomeDatafromDWHMatched
CREATE TABLE #OutcomeDatafromDWHMatched (RowID INT IDENTITY(1,1), Claimant_Key VARCHAR(50), VF_Manner_of_Resolution VARCHAR(200), Settlement_Date VARCHAR(30))

IF OBJECT_ID('tempdb..#IBaseData') IS NOT NULL
	DROP TABLE #IBaseData
CREATE TABLE #IBaseData (RowID INT IDENTITY(1,1), FirstName VARCHAR(100), LastName VARCHAR(100), PostCode VARCHAR(20), KeoghsEliteReference VARCHAR(50), DateOfBirth DATE, EntityID1 VARCHAR(50) NOT NULL, EntityID2 VARCHAR(50) NOT NULL)

IF OBJECT_ID('tempdb..#_TBL') IS NOT NULL
	DROP TABLE #_TBL
CREATE TABLE #_TBL (Claimant_Key VARCHAR(50),Inc_Unique_ID VARCHAR(20),Per_Unique_ID VARCHAR(20),Post_Code VARCHAR(20))

IF OBJECT_ID('tempdb..#_TBL2') IS NOT NULL
	DROP TABLE #_TBL2    
CREATE TABLE #_TBL2 (Unique_ID VARCHAR(20),Claimant_Key	VARCHAR(50),Inc_Unique_ID VARCHAR(20),Per_Unique_ID	VARCHAR(20),Post_Code VARCHAR(20), PassNo VARCHAR(20))

IF INDEXPROPERTY(OBJECT_ID('IBaseM.OutcomeDatafromDWH'), 'IX_OutcomeDatafromDWH_Matter_Number', 'IndexId') IS NOT NULL
	DROP INDEX IX_OutcomeDatafromDWH_Matter_Number ON [IBaseM].[OutcomeDatafromDWH] 
CREATE INDEX IX_OutcomeDatafromDWH_Matter_Number ON [IBaseM].[OutcomeDatafromDWH] (Matter_Number)

IF INDEXPROPERTY(OBJECT_ID('IBaseM.OutcomeDatafromDWH'), 'IX_OutcomeDatafromDWH_Claimant_First_Name_Claimant_Last_Name_Claimant_Post_Code_Matter_Number_Claimant_DOB', 'IndexId') IS NOT NULL
	DROP INDEX IX_OutcomeDatafromDWH_Claimant_First_Name_Claimant_Last_Name_Claimant_Post_Code_Matter_Number_Claimant_DOB ON [IBaseM].[OutcomeDatafromDWH] 
CREATE INDEX IX_OutcomeDatafromDWH_Claimant_First_Name_Claimant_Last_Name_Claimant_Post_Code_Matter_Number_Claimant_DOB ON [IBaseM].[OutcomeDatafromDWH] (Claimant_First_Name,Claimant_Last_Name,Claimant_Post_Code,Matter_Number,Claimant_DOB)

IF INDEXPROPERTY(OBJECT_ID('IBaseM.OutcomeDatafromDWH'), 'IX_OutcomeDatafromDWH_Claimant_Key', 'IndexId') IS NOT NULL
	DROP INDEX IX_OutcomeDatafromDWH_Claimant_Key ON [IBaseM].[OutcomeDatafromDWH] 
CREATE INDEX IX_OutcomeDatafromDWH_Claimant_Key ON [IBaseM].[OutcomeDatafromDWH] (Claimant_Key)

CREATE INDEX IX_#TBL2_Claimant_Key ON #_TBL2 (Claimant_Key)
CREATE INDEX IX_#OutcomeDatafromDWHMatched_Claimant_Key ON #OutcomeDatafromDWHMatched (Claimant_Key)

--CleanTheImportTable
UPDATE [IBaseM].[OutcomeDatafromDWH]
SET  Division					= LTRIM(RTRIM(Division))
	,Claimant_Name				= LTRIM(RTRIM(Claimant_Name))
	,Claimant_Address1			= LTRIM(RTRIM(Claimant_Address1))
	,Claimant_Address2			= LTRIM(RTRIM(Claimant_Address2))
	,Claimant_DOB				= CASE WHEN ISDATE(Claimant_DOB) = 1 THEN CONVERT(DATE, LTRIM(RTRIM(Claimant_DOB)), 103) ELSE Claimant_DOB END
	,claimID					= LTRIM(RTRIM(claimID))
	,Matter_Number				= LTRIM(RTRIM(Matter_Number))
	,VF_Manner_Of_Resolution	= LTRIM(RTRIM(VF_Manner_Of_Resolution))
	,Matter_Opening_Date		= LTRIM(RTRIM(Matter_Opening_Date))
	,Matter_Status_Desc			= LTRIM(RTRIM(Matter_Status_Desc))
	,Settlement_Date			= LTRIM(RTRIM(Settlement_Date))
	,Claimant_Title				= LTRIM(RTRIM(Claimant_Title))
	,Claimant_First_Name		= LTRIM(RTRIM(Claimant_First_Name))
	,Claimant_Last_Name			= LTRIM(RTRIM(Claimant_Last_Name))
	,Claimant_Post_Code			= LTRIM(RTRIM(REPLACE([Claimant_Post_Code],' ','')))
	,Claimant_Key				= LTRIM(RTRIM(Claimant_Key))

--UpdateCaseStatus
UPDATE A
SET Case_Status = CASE WHEN Matter_Status_Desc = 'Open for Time' THEN 'Open' ELSE Matter_Status_Desc END
FROM MDA_IBase.dbo.Keoghs_Case A
INNER JOIN [IBaseM].[OutcomeDatafromDWH] B ON A.Keoghs_Elite_Reference = B.Matter_Number
WHERE CASE WHEN Matter_Status_Desc = 'Open for Time' THEN 'Open' ELSE Matter_Status_Desc END != Case_Status

INSERT INTO #IBaseData (FirstName, LastName, PostCode, KeoghsEliteReference, DateOfBirth, EntityID1, EntityID2)
SELECT DISTINCT  NULLIF(LTRIM(RTRIM(P.First_Name)),'') FirstName
		,NULLIF(LTRIM(RTRIM(P.Last_Name)),'') LastName
		,NULLIF(LTRIM(RTRIM(REPLACE(A.[Post_Code ],' ',''))),'') PostCode
		,NULLIF(LTRIM(RTRIM(I.Keoghs_Elite_Reference)),'') KeoghsEliteReference
		,NULLIF(CONVERT(DATE, LTRIM(RTRIM(P.Date_of_Birth)), 101),'') DateOfBirth
		,LE.Entity_ID1
		,LE.Entity_ID2
FROM MDA_IBase.dbo.Incident_Link IL 
INNER JOIN MDA_IBase.dbo._LinkEnd LE ON LE.Link_ID = IL.Unique_ID
INNER JOIN MDA_IBase.dbo.Incident_ I ON LE.Entity_ID1 = I.Unique_ID
INNER JOIN MDA_IBase.dbo.Person_ P ON LE.Entity_ID2 = P.Unique_ID
INNER JOIN MDA_IBase.dbo._LinkEnd LE_AP ON LE_AP.Entity_ID1 = P.Unique_ID
INNER JOIN MDA_IBase.dbo.Address_to_Address_Link AL ON LE_AP.Link_ID = AL.Unique_ID
INNER JOIN MDA_IBase.dbo.Address_ A ON LE_AP.Entity_ID2 = A.Unique_ID

CREATE INDEX IX_IBaseData
ON #IBaseData (FirstName, LastName, PostCode, KeoghsEliteReference, DateOfBirth)

------------------------------------------
--OutcomePasses
------------------------------------------
--Pass1
INSERT INTO #_TBL (Claimant_Key, Inc_Unique_ID, Per_Unique_ID, Post_Code)
SELECT DISTINCT KAS.Claimant_Key
		,A.EntityID1
		,A.EntityID2
		,A.PostCode
FROM #IBaseData A
INNER JOIN IBaseM.OutcomeDatafromDWH KAS ON A.FirstName = KAS.Claimant_First_Name AND A.LastName = KAS.Claimant_Last_Name AND A.PostCode= KAS.Claimant_Post_Code AND KAS.Matter_Number = A.KeoghsEliteReference AND A.DateOfBirth = KAS.Claimant_DOB
SELECT @InsertCount = @@ROWCOUNT

INSERT INTO #_TBL2 (Unique_ID, Claimant_Key, Inc_Unique_ID, Per_Unique_ID, Post_Code, PassNo)
SELECT	 'OUT' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Claimant_Key) + @NextId) AS Unique_ID
		,Claimant_Key
		,Inc_Unique_ID
		,Per_Unique_ID
		,Post_Code
		,'Pass: 1'
FROM #_TBL

--UpdateTheOutcomeLinkVariable
SELECT @NextId = @NextId + @InsertCount

--Pass2
DELETE SRC
OUTPUT DELETED.[Claimant_Key], DELETED.[VF_Manner_Of_Resolution], DELETED.Settlement_Date
INTO #OutcomeDatafromDWHMatched (Claimant_Key, VF_Manner_of_Resolution, Settlement_Date)
FROM [IBaseM].[OutcomeDatafromDWH] SRC
WHERE EXISTS (SELECT TOP 1 1 FROM #_TBL DEST WHERE DEST.Claimant_Key = SRC.Claimant_Key)

TRUNCATE TABLE #_TBL
INSERT INTO #_TBL (Claimant_Key, Inc_Unique_ID, Per_Unique_ID, Post_Code)
SELECT DISTINCT KAS.Claimant_Key
		,A.EntityID1
		,A.EntityID2
		,A.PostCode
FROM #IBaseData A
INNER JOIN IBaseM.OutcomeDatafromDWH KAS ON A.FirstName = KAS.Claimant_First_Name AND A.LastName = KAS.Claimant_Last_Name AND A.PostCode = KAS.Claimant_Post_Code AND KAS.Matter_Number = A.KeoghsEliteReference
SELECT @InsertCount = @@ROWCOUNT

INSERT INTO #_TBL2 (Unique_ID, Claimant_Key, Inc_Unique_ID, Per_Unique_ID, Post_Code, PassNo)
SELECT	 'OUT' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Claimant_Key) + @NextId) AS Unique_ID
		,Claimant_Key
		,Inc_Unique_ID
		,Per_Unique_ID
		,Post_Code
		,'Pass: 2'
FROM #_TBL

--UpdateTheOutcomeLinkVariable
SELECT @NextId = @NextId + @InsertCount

--Pass3
DELETE SRC
OUTPUT DELETED.[Claimant_Key], DELETED.[VF_Manner_Of_Resolution], DELETED.Settlement_Date
INTO #OutcomeDatafromDWHMatched (Claimant_Key, VF_Manner_of_Resolution, Settlement_Date)
FROM [IBaseM].[OutcomeDatafromDWH] SRC
WHERE EXISTS (SELECT TOP 1 1 FROM #_TBL DEST WHERE DEST.Claimant_Key = SRC.Claimant_Key)

TRUNCATE TABLE #_TBL
INSERT INTO #_TBL (Claimant_Key, Inc_Unique_ID, Per_Unique_ID, Post_Code)
SELECT DISTINCT KAS.Claimant_Key
		,A.EntityID1
		,A.EntityID2
		,A.PostCode
FROM #IBaseData A
INNER JOIN IBaseM.OutcomeDatafromDWH KAS ON DIFFERENCE(A.FirstName, KAS.Claimant_First_Name) = 4 AND A.LastName = KAS.Claimant_Last_Name AND A.PostCode = KAS.Claimant_Post_Code AND KAS.Matter_Number = A.KeoghsEliteReference AND A.DateOfBirth = KAS.Claimant_DOB 
SELECT @InsertCount = @@ROWCOUNT

INSERT INTO #_TBL2 (Unique_ID, Claimant_Key, Inc_Unique_ID, Per_Unique_ID, Post_Code, PassNo)
SELECT	 'OUT' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Claimant_Key) + @NextId) AS Unique_ID
		,Claimant_Key
		,Inc_Unique_ID
		,Per_Unique_ID
		,Post_Code
		,'Pass: 3'
FROM #_TBL

--UpdateTheOutcomeLinkVariable
SELECT @NextId = @NextId + @InsertCount

--Pass4
DELETE SRC
OUTPUT DELETED.[Claimant_Key], DELETED.[VF_Manner_Of_Resolution], DELETED.Settlement_Date
INTO #OutcomeDatafromDWHMatched (Claimant_Key, VF_Manner_of_Resolution, Settlement_Date)
FROM [IBaseM].[OutcomeDatafromDWH] SRC
WHERE EXISTS (SELECT TOP 1 1 FROM #_TBL DEST WHERE DEST.Claimant_Key = SRC.Claimant_Key)

TRUNCATE TABLE #_TBL
INSERT INTO #_TBL (Claimant_Key, Inc_Unique_ID, Per_Unique_ID, Post_Code)
SELECT DISTINCT KAS.Claimant_Key
		,A.EntityID1
		,A.EntityID2
		,A.PostCode
FROM #IBaseData A
INNER JOIN IBaseM.OutcomeDatafromDWH KAS ON  DIFFERENCE(A.FirstName, KAS.Claimant_First_Name) = 4 AND A.LastName = KAS.Claimant_Last_Name AND A.PostCode = KAS.Claimant_Post_Code AND KAS.Matter_Number = A.KeoghsEliteReference
SELECT @InsertCount = @@ROWCOUNT

INSERT INTO #_TBL2 (Unique_ID, Claimant_Key, Inc_Unique_ID, Per_Unique_ID, Post_Code, PassNo)
SELECT	 'OUT' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Claimant_Key) + @NextId) AS Unique_ID
		,Claimant_Key
		,Inc_Unique_ID
		,Per_Unique_ID
		,Post_Code
		,'Pass: 4'
FROM #_TBL

--UpdateTheOutcomeLinkVariable
SELECT @NextId = @NextId + @InsertCount

--Pass5
DELETE SRC
OUTPUT DELETED.[Claimant_Key], DELETED.[VF_Manner_Of_Resolution], DELETED.Settlement_Date
INTO #OutcomeDatafromDWHMatched (Claimant_Key, VF_Manner_of_Resolution, Settlement_Date)
FROM [IBaseM].[OutcomeDatafromDWH] SRC
WHERE EXISTS (SELECT TOP 1 1 FROM #_TBL DEST WHERE DEST.Claimant_Key = SRC.Claimant_Key)

TRUNCATE TABLE #_TBL
INSERT INTO #_TBL (Claimant_Key, Inc_Unique_ID, Per_Unique_ID, Post_Code)
SELECT DISTINCT KAS.Claimant_Key
		,A.EntityID1
		,A.EntityID2
		,A.PostCode
FROM #IBaseData A
INNER JOIN IBaseM.OutcomeDatafromDWH KAS ON LEFT(A.FirstName, 1) = LEFT(KAS.Claimant_First_Name, 1) AND A.LastName = KAS.Claimant_Last_Name AND A.PostCode = KAS.Claimant_Post_Code AND KAS.Matter_Number = A.KeoghsEliteReference AND A.DateOfBirth = KAS.Claimant_DOB 
SELECT @InsertCount = @@ROWCOUNT

INSERT INTO #_TBL2 (Unique_ID, Claimant_Key, Inc_Unique_ID, Per_Unique_ID, Post_Code, PassNo)
SELECT	 'OUT' + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY Claimant_Key) + @NextId) AS Unique_ID
		,Claimant_Key
		,Inc_Unique_ID
		,Per_Unique_ID
		,Post_Code
		,'Pass: 5'
FROM #_TBL

--UpdateTheOutcomeLinkVariable
SELECT @NextId = @NextId + @InsertCount

DELETE SRC
OUTPUT DELETED.[Claimant_Key], DELETED.[VF_Manner_Of_Resolution], DELETED.Settlement_Date
INTO #OutcomeDatafromDWHMatched (Claimant_Key, VF_Manner_of_Resolution, Settlement_Date)
FROM [IBaseM].[OutcomeDatafromDWH] SRC
WHERE EXISTS (SELECT TOP 1 1 FROM #_TBL DEST WHERE DEST.Claimant_Key = SRC.Claimant_Key)

------------------------------------------
--InsertIntoMDA_IBase
------------------------------------------
;WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
DELETE FROM MDA_IBase.dbo._LinkEnd 
WHERE Link_ID IN (SELECT Unique_ID FROM MDA_IBase.dbo.Outcome_Link)
OR LEFT(Link_ID,3) = 'OUT';

WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
DELETE FROM MDA_IBase.dbo.Outcome_Link

UPDATE MDA_IBase.dbo._Outcome_Link_NextID 
SET NextID = @NextId

INSERT INTO [MDA_IBase].dbo.Outcome_Link(Unique_ID, Outcome_Link_ID, Create_Date, Create_User, Do_Not_Disseminate_412284494, Manner_of_Resolution, Record_Status, Settlement_Date, Source_Reference_411765487) 
SELECT   Unique_ID
		,KAS.Claimant_Key
		,@Create_Date
		,@Create_User
		,1
		,VF_Manner_of_Resolution
		,0
		,CONVERT(SMALLDATETIME, Settlement_Date, 103)
		,PassNo
FROM #_TBL2 T
INNER JOIN #OutcomeDatafromDWHMatched KAS ON T.Claimant_Key= KAS.Claimant_Key

INSERT INTO [MDA_IBase].dbo._LinkEnd(Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
SELECT	DISTINCT Unique_ID
		,Inc_Unique_ID
		,0 AS Confidence
		,0 AS Direction
		,Per_Unique_ID
		,1688 EntType2
		,1969 EntType1
		,3858 LinkType
		,0 AS Record_Status
		,1 AS Record_Type
		,NULL AS SCC 
FROM #_TBL2 T
UNION 
SELECT	DISTINCT Unique_ID
		,Per_Unique_ID
		,0 AS Confidence
		,0 AS Direction
		,Inc_Unique_ID
		,1969 EntType1
		,1688 EntType2
		,3858 LinkType
		,0 AS Record_Status
		,2 AS Record_Type
		,NULL AS SCC 
FROM #_TBL2 T

-------------------------------------
--Tidy
------------------------------------
IF OBJECT_ID('tempdb..#OutcomeDatafromDWHMatched') IS NOT NULL
	DROP TABLE #OutcomeDatafromDWHMatched
IF OBJECT_ID('tempdb..#IBaseData') IS NOT NULL
	DROP TABLE #IBaseData
IF OBJECT_ID('tempdb..#_TBL') IS NOT NULL
	DROP TABLE #_TBL
IF OBJECT_ID('tempdb..#_TBL2') IS NOT NULL
	DROP TABLE #_TBL2    

IF INDEXPROPERTY(OBJECT_ID('IBaseM.OutcomeDatafromDWH'), 'IX_OutcomeDatafromDWH_Matter_Number', 'IndexId') IS NOT NULL
	DROP INDEX IX_OutcomeDatafromDWH_Matter_Number ON [IBaseM].[OutcomeDatafromDWH] 
IF INDEXPROPERTY(OBJECT_ID('IBaseM.OutcomeDatafromDWH'), 'IX_OutcomeDatafromDWH_Matter_Number', 'IndexId') IS NOT NULL
	DROP INDEX IX_OutcomeDatafromDWH_Claimant_First_Name_Claimant_Last_Name_Claimant_Post_Code_Matter_Number_Claimant_DOB ON [IBaseM].[OutcomeDatafromDWH] 
IF INDEXPROPERTY(OBJECT_ID('IBaseM.OutcomeDatafromDWH'), 'IX_OutcomeDatafromDWH_Claimant_Key', 'IndexId') IS NOT NULL
	DROP INDEX IX_OutcomeDatafromDWH_Claimant_Key ON [IBaseM].[OutcomeDatafromDWH] 
GO
GRANT EXECUTE
    ON OBJECT::[IBaseM].[uspRepopulateOutcomeData] TO [KNET\svc-mi]
    AS [dbo];

