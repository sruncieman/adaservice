﻿

CREATE PROCEDURE [IBaseM].[uspMigrationFix01]
(
 @Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 14 --Populate_MigrationFix01
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
			
	------------------------------------------------------------
	--2)MainBody
	------------------------------------------------------------

	--PaulAllen_20140618_InstanceWhereTheRecordStatusIsNull
	UPDATE IBaseM8Cur.[Vehicle_]
	SET [Record_Status] = 0
	WHERE [Record_Status] IS NULL

	----------------------------------------------------
	--UpdateIncidentLocationForAllAccidentLocations:Defect775
	--PaulAllen
	--20140618
	----------------------------------------------------
	UPDATE IL
	SET Incident_Location = Data.Incident_Location
	FROM IBaseM8Cur.Incident_Link IL
	INNER JOIN	(
				SELECT	 LE1.Link_ID
						,ALOC.Incident_Location
				FROM	(
						SELECT	 LOC.Unique_ID
								,CASE WHEN Property_Name IS NOT NULL THEN Property_Name + ' ' ELSE '' END +
								 CASE WHEN Property_No IS NOT NULL THEN Property_No + ' ' ELSE '' END +
								 CASE WHEN Street_ IS NOT NULL THEN Street_ + ', ' ELSE '' END +
								 CASE WHEN Locality_	IS NOT NULL THEN Locality_ + ', ' ELSE '' END +
								 CASE WHEN Town_	IS NOT NULL THEN Town_ + ', ' ELSE '' END +
								 CASE WHEN County_	IS NOT NULL THEN County_ + ', ' ELSE '' END +
								 CASE WHEN Post_Code	IS NOT NULL THEN Post_Code ELSE '' END Incident_Location
						FROM [IBaseM5Cur].[Location_] LOC
						WHERE [Type_] = 'Accident Location'
						) ALOC
				INNER JOIN [IBaseM5Cur].[_LinkEnd] LE ON LE.Entity_ID1 = ALOC.Unique_ID AND EntityType_ID2 = 1000 --CLA
				INNER JOIN [IBaseM5Cur].[_LinkEnd] LE1 ON LE1.Entity_ID1 = LE.Entity_ID2
				WHERE LEFT(LE1.Link_ID,3) IN ('IN1','CL0')
				GROUP BY LE1.Link_ID
						,ALOC.Incident_Location
				) Data ON  Data.Link_ID =  IL.Incident_Link_ID
	WHERE IL.Incident_Location IS NULL

	--I've updated the first 2 HIGH priority defects in the Intel MB. These are defects 75 and 134. I'll let you know when the others are ready.
	--75
	--This defect only appies where the person or organisation was linked directly to the case in the old schema by a TP Claim link. 
	--Affects 666 records.Where TP Claim Links have migrated to Incident Links, 
	--Description is migrating to Sub Party Type when it should be Party Type. 
	--Party Type as well as Source is populated with 'Keoghs CFS' - should be in Source only. 
	--Incident Circs, Incident Time and Incident Location have not been migrated
	--The Incident Location isn't pulling in the County

	UPDATE IL
	SET   Incident_Location = 
			CASE WHEN Property_Name IS NOT NULL THEN Property_Name + ' ' ELSE '' END +
			CASE WHEN Property_No IS NOT NULL THEN Property_No + ' ' ELSE '' END +
			CASE WHEN Street_ IS NOT NULL THEN Street_ + ', ' ELSE '' END +
			CASE WHEN Locality_	IS NOT NULL THEN Locality_ + ', ' ELSE '' END +
			CASE WHEN Town_	IS NOT NULL THEN Town_ + ', ' ELSE '' END +
			CASE WHEN County_	IS NOT NULL THEN County_ + ', ' ELSE '' END +
			CASE WHEN Post_Code	IS NOT NULL THEN Post_Code ELSE '' END
	FROM IBaseM8Cur.Incident_Link  IL
	INNER JOIN IBaseM5Cur._LinkEnd LE ON IL.Incident_Link_ID = LE.LINK_ID
	INNER JOIN IBaseM5Cur.Person_ P ON P.Unique_ID = LE.Entity_ID1
	INNER JOIN IBaseM5Cur.ClaimFile_ C ON C.Unique_ID = LE.Entity_ID2
	INNER JOIN IBaseM5Cur._LinkEnd LELL ON LE.Entity_ID2 = LELL.Entity_ID1
	INNER JOIN IBaseM5Cur.Location_Link LL ON LELL.Link_Id = LL.Unique_id
	INNER JOIN IBaseM5Cur.Location_  L ON LELL.Entity_ID2 = L.Unique_id
	WHERE Incident_Link_ID LIKE 'CL0%'
	AND Party_Type = 'Keoghs CFS'
	AND Type_ = 'Accident Location'

	UPDATE IL
	SET Sub_Party_Type = NULL,
		Party_Type = 'TP Claim',
		Incident_Time = CONVERT(TIME, C.Incident_Time),
		Incident_Circs = C.Comments_
	FROM IBaseM8Cur.Incident_Link  IL
	INNER JOIN IBaseM5Cur._LinkEnd LE ON IL.Incident_Link_ID = LE.LINK_ID
	INNER JOIN IBaseM5Cur.Person_ P ON P.Unique_ID = LE.Entity_ID1
	INNER JOIN IBaseM5Cur.ClaimFile_ C ON C.Unique_ID = LE.Entity_ID2
	WHERE Incident_Link_ID LIKE 'CL0%'
	AND Party_Type = 'Keoghs CFS'
	
	-----------------------------------------------------------------------------------------------------------------------------
	--134
	--Where a Person was linked directly to the Claim via a Nominal link in the old schema, 
	--various fields have not been populated on the Incident link in the new schema: 
	--Incident Circs, Incident Time, Incident Location, Insurer (Insured only) and Claim Number (Insured only). 
	--Affects 369 records 

	UPDATE IL
	SET   Incident_Location = 
			CASE WHEN Property_Name IS NOT NULL THEN Property_Name + ' ' ELSE '' END +
			CASE WHEN Property_No IS NOT NULL THEN Property_No + ' ' ELSE '' END +
			CASE WHEN Street_ IS NOT NULL THEN Street_ + ', ' ELSE '' END +
			CASE WHEN Locality_	IS NOT NULL THEN Locality_ + ', ' ELSE '' END +
			CASE WHEN Town_	IS NOT NULL THEN Town_ + ', ' ELSE '' END +
			CASE WHEN County_	IS NOT NULL THEN County_ + ', ' ELSE '' END +
			CASE WHEN Post_Code	IS NOT NULL THEN Post_Code ELSE '' END
	FROM IBaseM8Cur.Incident_Link  IL
	INNER JOIN IBaseM5Cur._LinkEnd LE ON IL.Incident_Link_ID = LE.LINK_ID
	INNER JOIN IBaseM5Cur.Person_ P ON P.Unique_ID = LE.Entity_ID1
	INNER JOIN IBaseM5Cur.ClaimFile_ C ON C.Unique_ID = LE.Entity_ID2
	INNER JOIN IBaseM5Cur.Nominal_Link N ON N.Unique_ID = LE.Link_Id
	INNER JOIN IBaseM5Cur._LinkEnd LELL ON LE.Entity_ID2 = LELL.Entity_ID1
	INNER JOIN IBaseM5Cur.Location_Link LL ON LELL.Link_Id = LL.Unique_id
	INNER JOIN IBaseM5Cur.Location_  L ON LELL.Entity_ID2 = L.Unique_id
	WHERE Incident_Link_ID LIKE 'NOM%'

	UPDATE IL
	SET	Incident_Circs = C.Comments_,
		Incident_Time = C.Incident_Time,
		Insurer_ = C.Insurer_,
		Claim_Number = C.Insurer_Reference
	FROM IBaseM8Cur.Incident_Link  IL
	INNER JOIN IBaseM5Cur._LinkEnd LE ON IL.Incident_Link_ID = LE.LINK_ID
	INNER JOIN IBaseM5Cur.Person_ P ON P.Unique_ID = LE.Entity_ID1
	INNER JOIN IBaseM5Cur.ClaimFile_ C ON C.Unique_ID = LE.Entity_ID2
	INNER JOIN IBaseM5Cur.Nominal_Link N ON N.Unique_ID = LE.Link_Id
	WHERE Incident_Link_ID LIKE 'NOM%'

	-----------------------------------------------------------------------------------------------------------------------------
	--The rest of the high priority defects have now been updated in their folders in the Intel MB where necessary. These are 71, 96, 98, 129, 146 and 162.
	--71
	--This defect only appies where the person was linked directly to the case in the old schema by an Insured Claim link. Affects 256 records.
	--Source needs to be popupated with 'Keoghs CFS' 'Keoghs CFS' is currently going into  
	--Also, 
	--Accident Location, 
	--Incident Circs, 
	--Incident Time, 
	--Insurer and 
	--Claim Number aren't being pulled into the Incident Link. 
	--Reopen: Source Description should be blank. 
	--It's pulling in the Description from the Insured Claim link and the Incident Time is wrong: 
	--IN125121 is 11:15 in the old schema. In the new one it's Dec 30 189.

	UPDATE A 
		SET	Source_411765484 =  'Keoghs CFS',
			Party_Type = NULL,
			Incident_Location = B.Accident_Location,
			Incident_Circs = B.IncidentCircumstances,
			Incident_Time = B.Incident_Time,
			Insurer_ = B.Insurer_,
			Claim_Number = B.Claim_Number
	FROM IBaseM8Cur.Incident_Link A
	INNER JOIN 
		(
		SELECT 
			ICL.Unique_id,
			Accident_Location,
			CF.Comments_ AS IncidentCircumstances,
			CONVERT(TIME, CF.Incident_Time) Incident_Time,
			CF.Insurer_,
			CF.Insurer_Reference AS Claim_Number
		FROM IBaseM5Cur.Insured_Claim_Link ICL
		INNER JOIN IBaseM5Cur._LinkEnd LE ON ICL.Unique_id = LE.Link_ID AND LE.Entity_ID1 LIKE 'PER%' 
		INNER JOIN  IBaseM5Cur.ClaimFile_ CF ON CF.Unique_id = LE.Entity_ID2
		LEFT JOIN 
		(SELECT LE1.Entity_ID2,
		CASE WHEN Property_Name	IS NOT NULL THEN Property_Name + ', ' ELSE '' END +
			CASE WHEN Property_No IS NOT NULL THEN Property_No + ', ' ELSE '' END +
			CASE WHEN Street_ IS NOT NULL THEN Street_ + ', ' ELSE '' END +
			CASE WHEN Locality_	IS NOT NULL THEN Locality_ + ',' ELSE '' END +
			CASE WHEN Town_	IS NOT NULL THEN Town_ + ' ' ELSE '' END +
			CASE WHEN Post_Code	IS NOT NULL THEN Post_Code  ELSE '' END AS Accident_Location
		FROM IBaseM5Cur._LinkEnd LE1
		INNER JOIN IBaseM5Cur.Location_ L ON LE1.Entity_ID1 = L.Unique_id AND LE1.Entity_ID2 LIKE 'CLA%'
		) L ON CF.Unique_ID = L.Entity_ID2		
	) B ON A.Incident_Link_ID = B.Unique_id
	
	-----------------------------------------------------------------------------------------------------------------------------
	--96
	--There are instances where people have been linked to the incident via an Incident Link in the new schema 
	--which shouldn't have been, e.g. PER72575 is the current keeper in claim 177146. 
	--This party type shouldn't be linked to the incident following migration to the new schema.
	--Reopening: This is still happening. It only appears to be a problem for 'Current Keeper'. Affects 515 records.

	--SELECT COUNT(*),Sub_Party_Type
	--FROM IBaseM8Cur.Incident_Link A
	--GROUP BY Sub_Party_Type

	-----------------------------------------------------------------------------------------------------------------------------

	--98
	--In the new schema, where a person is linked to an Incident which was a CUE/MIAFTR Match in the old schema. 
	--There is a double link. 
	--1 link is an Incident Link (Correct), the other is an Incident Match Link. 
	--Incident Match Links should only be used to link to CUE/MIAFTR Match entities (previously linked by a MIAFTR Match Link). 
	--This has also happened where it is a vehicle linked to an Incident which was a CUE/MIAFTR Match in the old schema

	--FIXED IN IBaseM.Migration_Step_03
	-----------------------------------------------------------------------------------------------------------------------------
	--162.
	--Where Incident Links have been created from an Insured Claim link, 
	--there is no Party Type. This part of the replication has worked in the past

	UPDATE B 
	SET Source_411765484 = B.Party_Type
	FROM IBaseM5Cur.Insured_Claim_Link A
	INNER JOIN IBaseM8Cur.Incident_Link B ON A.Unique_ID = B.Incident_Link_ID
	WHERE B.Source_411765484 IS NULL

	UPDATE B 
	SET Party_Type = A.Description_
	FROM IBaseM5Cur.Insured_Claim_Link A
	INNER JOIN IBaseM8Cur.Incident_Link B
	ON A.Unique_ID = B.Incident_Link_ID
	
	-----------------------------------------------------------------------------------------------------------------------------
	--129,
	--The Insurer field on the Incident Link is being populated for all Person types. 
	--It should only be populated for 'Policyholder', 'Named Driver' or 'Insured Driver'
	--UPDATE: This is also the case for the Claim Number

	UPDATE IBaseM8Cur.Incident_Link
	SET Insurer_ = NULL, Claim_Number = NULL
	WHERE Unique_ID NOT IN (SELECT Unique_ID
							FROM  IBaseM8Cur.Incident_Link A  WITH(NOLOCK) 
							WHERE (Party_Type = 'Insured Claim')
									--OR	(Party_Type = 'Insured Claim' AND Sub_Party_Type = 'Insured')
									--OR	(Party_Type = 'Insured Claim' AND Sub_Party_Type = 'Named Driver')
									OR	(Party_Type = 'Insured Vehicle' AND Sub_Party_Type = 'Driver')
									OR	(Party_Type = 'Insured Vehicle' AND Sub_Party_Type = 'Insured')
									OR	(Party_Type = 'Insured Vehicle' AND Sub_Party_Type = 'Named Driver')
									OR LEFT(Incident_Link_ID,3) = 'MI0') --PA:Added20140204,CUEMatchesShouldretainThisData
	
	UPDATE IBaseM8Cur.Incident_Link
	SET  Party_Type = 'Insured',  Sub_Party_Type = 'Unknown'
	WHERE Party_Type IN('Insured Claim')

	UPDATE IBaseM8Cur.Incident_Link
	SET  Party_Type = 'Insured'
	WHERE Party_Type IN('Insured Vehicle')

	UPDATE IBaseM8Cur.Incident_Link
	SET  Party_Type = 'Third Party',  Sub_Party_Type = 'Unknown'
	WHERE Party_Type IN('TP Claim')

	UPDATE IBaseM8Cur.Incident_Link
	SET  Party_Type = 'Third Party'
	WHERE Party_Type IN('TP Vehicle')

	UPDATE IBaseM8Cur.Incident_Link
	SET  Sub_Party_Type = 'Driver'
	WHERE Sub_Party_Type IN('Named Driver' ,'Insured')

	UPDATE IBaseM8Cur.Incident_Link
	SET  Sub_Party_Type = 'Passenger'
	WHERE Sub_Party_Type  = 'Passenger/Claimant'

	--I noticed that slight problem with one of the updates - Everyone with a Party Type of Insured has a Sub Party Type of Driver. 
	--I'll speak to you about it in the morning because it's all part of the other thing we need to finish off anyway.

	-----------------------------------------------------------------------------------------------------------------------------
	--146 
	--Where there was a CUE/MIAFTR Match entity linked to a Location entity by a MIAFTR Match link in the old schema, 
	--these need to be migrated as Incident entities linked to Address entities by Incident Match links in the new schema

	--LOOKS OK TO ME
	
	-----------------------------------------------------------------------------------------------------------------------------
	--Where there is an Incident Link connecting an Organisation to an Incident, the link fields have not been populated correctly. 

	--UPDATE: This defect only appies where the organisation was linked directly to the case in the old schema by either an Insured Claim Link 
	--or TP Claim Link, rather than via a vehicle. Affects 706 records.

	--Incident Location, Incident Circs, Incident Time, Insurer (Insured only) and Claim Number (Insured only) are not being pulled in.

	--Where it came from a TP Claim link, the Description has gone into the Sub Party Type rather than Party Type. 
	--Sub Party Type needs to be Unknown. Source Description should be blank. It's currently storing the Elite ref which already goes into the Source Ref 

	--Where it came from an Insured Claim link, Description has gone into the Source Description as well as Party Type. Sub Party Type should be 'Unknown'.

	--FIXED IN Migration_Step_04 REF:201305151233
	
	-----------------------------------------------------------------------------------------------------------------------------
	--131
	--Where it came from an Insured Claim Link, 
	--Party Type and Sub Party Type are now correct, 
	--but Incident Location, Incident Circs, Incident Time, 
	--Insurer and Claim Number are still not being pulled in. 
	--Description is still going into Source Description. Source Description should be blank.
	UPDATE A
	SET	Incident_Circs = D.Comments_,
		Incident_Time = CONVERT(TIME, D.Incident_Time),
		Insurer_ = D.Insurer_,
		Claim_Number = D.Insurer_Reference,
		Incident_Location = IBaseM.fnGetClaimAddress(C.Incident_ID)
	FROM IBaseM8Cur.Incident_Link A   WITH(NOLOCK) 
	INNER JOIN IBaseM8Cur._LinkEnd B  WITH(NOLOCK) ON A.Unique_ID = B.Link_ID AND B.Entity_ID1 LIKE 'ORG%'
	INNER JOIN IBaseM8Cur.Incident_ C  WITH(NOLOCK) ON B.Entity_ID2 = C.Unique_ID
	INNER JOIN IBaseM5Cur.ClaimFile_ D  WITH(NOLOCK) ON C.Incident_ID = D.Unique_ID
	WHERE A.Incident_Link_ID LIKE 'IN1%'
	AND A.Party_Type = 'Insured' 
	AND A.Sub_Party_Type = 'Unknown'

	-----------------------------------------------------------------------------------------------------------------------------
	--134
	--Where a Person was linked directly to the Claim via a Nominal link in the old schema, 
	--various fields have not been populated on the Incident link in the new schema: 
	--Incident Circs, Incident Time, Incident Location, Insurer (Insured only) and Claim Number (Insured only). Affects 369 records 

	UPDATE A
		SET Incident_Circs = U.Comments_,
		Incident_Time = CONVERT(TIME, U.Incident_Time),
		Insurer_ = U.Insurer_,
		Claim_Number = U.Insurer_Reference,
		Incident_Location = IBaseM.fnGetClaimAddress(U.Unique_ID)
	FROM IBaseM8Cur.Incident_Link A WITH(NOLOCK)
	INNER JOIN IBaseM8Cur._LinkEnd B WITH(NOLOCK) ON A.Unique_ID = B.Link_ID
	INNER JOIN IBaseM8Cur.Incident_ C WITH(NOLOCK) ON B.Entity_ID1 = C.Unique_ID
	INNER JOIN IBaseM5Cur.ClaimFile_ U WITH(NOLOCK) ON C.Incident_ID = U.Unique_ID
	WHERE LEFT(A.Incident_Link_ID, 3) = 'NOM' 

	-----------------------------------------------------------------------------------------------------------------------------
	--173
	--The Payment Card Number field of all 
	--Payment Card records is blank. 
	--This should be populated from the Card Number field of the corresponding Account record in the old schema

	--FIXED  IN IBaseM.Migration_Step_02 REF: 201305211036

	-----------------------------------------------------------------------------------------------------------------------------
	--170
	--Where a Person was linked directly to the Claim via a Nominal link in the old schema, 
	--in the corresponding Incident link, 
	--the Party Type should be 'Witness' 
	--and the Sub Party Type should be NULL. 
	--Currently Party Type is Null and Sub Party Type is 'Witness'.

	UPDATE IBaseM8Cur.Incident_Link
	SET Sub_Party_Type = NULL, Party_Type = 'Witness'
	WHERE LEFT(Incident_Link_ID, 3) = 'NOM' AND Sub_Party_Type = 'Witness' AND Party_Type IS NULL

	UPDATE A
	SET Record_Status = B.Record_Status 
	FROM  IBaseM8Cur.Incident_Link A
	INNER JOIN IBaseM5Cur.Nominal_Link B
	ON A.Incident_Link_ID = B.Unique_ID
	  
	UPDATE A
	SET Record_Status = B.Record_Status 
	FROM  IBaseM8Cur.Incident_Link A
	INNER JOIN IBaseM5Cur.MIAFTR_Match_Link B
	ON A.Incident_Link_ID = B.Unique_ID
	  
	UPDATE A
	SET Record_Status = B.Record_Status 
	FROM  IBaseM8Cur.Incident_Link A
	INNER JOIN IBaseM5Cur.Intelligence_Link B
	ON A.Incident_Link_ID = B.Unique_ID

	UPDATE A
	SET Record_Status = B.Record_Status 
	FROM  IBaseM8Cur.Incident_Link A
	INNER JOIN IBaseM5Cur.Claim_ B
	ON A.Incident_Link_ID = B.Unique_ID

	UPDATE A
	SET Record_Status = B.Record_Status 
	FROM  IBaseM8Cur.Incident_Link A
	INNER JOIN IBaseM5Cur.Insured_Claim_Link B
	ON A.Incident_Link_ID = B.Unique_ID

	UPDATE A
	SET Record_Status = B.Record_Status 
	FROM  IBaseM8Cur.Incident_Link A
	INNER JOIN IBaseM5Cur.Report_ B
	ON A.Incident_Link_ID = B.Unique_ID

	IF OBJECT_ID('tempdb..#_ILT') is not null
		DROP TABLE #_ILT		

	SELECT  MIN(IL.Unique_ID) AS Unique_ID, IL.Accident_Management, IL.AltEntity, IL.Ambulance_Attended, IL.Broker_, IL.Claim_Code, IL.Claim_Notification_Date, 
						  IL.Claim_Number, IL.Claim_Status, IL.Claim_Type, IL.Create_Date, IL.Create_User, IL.Do_Not_Disseminate_412284494, IL.Engineer_, IL.Hire_, IL.IconColour, 
						  IL.Incident_Circs, IL.Incident_Link_ID, MIN(Incident_Location) AS Incident_Location,
	                      IL.Incident_Time, IL.Insurer_, IL.Key_Attractor_412284410, IL.Last_Upd_Date, IL.Last_Upd_User, 
	                      IL.MDA_Incident_ID_412284502, IL.Medical_Examiner, IL.MoJ_Status, IL.Party_Type, IL.Payments_, IL.Police_Attended, IL.Police_Force, IL.Police_Reference, 
						  IL.Record_Status, IL.Recovery_, IL.Referral_Source, IL.Repairer_, IL.Reserve_, IL.SCC, IL.Solicitors_, IL.Source_411765484, IL.Source_Description_411765489, 
						  IL.Source_Reference_411765487, IL.Status_Binding, IL.Storage_, IL.Storage_Address, IL.Sub_Party_Type, IL.x5x5x5_Grading_412284402, LE.Entity_ID1, 
						  LE.Confidence, LE.Direction, LE.Entity_ID2, LE.EntityType_ID1, LE.EntityType_ID2, LE.LinkType_ID, LE.Record_Status AS LE_Record_Status, LE.Record_Type, 
						  LE.SCC AS LE_SCC
	INTO #_ILT                       
	FROM IBaseM8Cur.Incident_Link AS IL 
	INNER JOIN IBaseM8Cur._LinkEnd AS LE ON IL.Unique_ID = LE.Link_Id
	WHERE LEFT(IL.Unique_ID, 3 ) = 'IN1' AND  ISNULL(CONVERT(INT, IL.Record_Status), 0) + ISNULL(CONVERT(INT, LE.Record_Status), 0) = 0                
	GROUP BY IL.Accident_Management, IL.AltEntity, IL.Ambulance_Attended, IL.Broker_, IL.Claim_Code, IL.Claim_Notification_Date, IL.Claim_Number, IL.Claim_Status, 
			IL.Claim_Type, IL.Create_Date, IL.Create_User, IL.Do_Not_Disseminate_412284494, IL.Engineer_, IL.Hire_, IL.IconColour, IL.Incident_Circs, IL.Incident_Link_ID, 
			IL.Incident_Time, IL.Insurer_, IL.Key_Attractor_412284410, IL.Last_Upd_Date, IL.Last_Upd_User, IL.MDA_Incident_ID_412284502, 
			IL.Medical_Examiner, IL.MoJ_Status, IL.Party_Type, IL.Payments_, IL.Police_Attended, IL.Police_Force, IL.Police_Reference, IL.Record_Status, IL.Recovery_, 
			IL.Referral_Source, IL.Repairer_, IL.Reserve_, IL.SCC, IL.Solicitors_, IL.Source_411765484, IL.Source_Description_411765489, IL.Source_Reference_411765487, 
			IL.Status_Binding, IL.Storage_, IL.Storage_Address, IL.Sub_Party_Type, IL.x5x5x5_Grading_412284402, LE.Entity_ID1, LE.Confidence, LE.Direction, LE.Entity_ID2, 
			LE.EntityType_ID1, LE.EntityType_ID2, LE.LinkType_ID, LE.Record_Status, LE.Record_Type, LE.SCC

	DELETE FROM IBaseM8Cur._LinkEnd WHERE  LEFT(Link_ID, 3 ) = 'IN1' 
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Incident_Link'


	INSERT INTO IBaseM8Cur.Incident_Link (Unique_ID, Accident_Management, AltEntity, Ambulance_Attended, Broker_, Claim_Code, Claim_Notification_Date, Claim_Number, Claim_Status, Claim_Type, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engineer_, Hire_, IconColour, Incident_Circs, Incident_Link_ID, Incident_Location, Incident_Time, Insurer_, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, MDA_Incident_ID_412284502, Medical_Examiner, MoJ_Status, Party_Type, Payments_, Police_Attended, Police_Force, Police_Reference, Record_Status, Recovery_, Referral_Source, Repairer_, Reserve_, SCC, Solicitors_, Source_411765484, Source_Description_411765489, Source_Reference_411765487, Status_Binding, Storage_, Storage_Address, Sub_Party_Type, x5x5x5_Grading_412284402)
	SELECT DISTINCT	  IL.Unique_ID, IL.Accident_Management, IL.AltEntity, IL.Ambulance_Attended, IL.Broker_, IL.Claim_Code, IL.Claim_Notification_Date, 
					  IL.Claim_Number, IL.Claim_Status, IL.Claim_Type, IL.Create_Date, IL.Create_User, IL.Do_Not_Disseminate_412284494, IL.Engineer_, IL.Hire_, IL.IconColour, 
					  IL.Incident_Circs, IL.Incident_Link_ID, IL.Incident_Location, IL.Incident_Time, IL.Insurer_, IL.Key_Attractor_412284410, IL.Last_Upd_Date, IL.Last_Upd_User, 
					  IL.MDA_Incident_ID_412284502, IL.Medical_Examiner, IL.MoJ_Status, IL.Party_Type, IL.Payments_, IL.Police_Attended, IL.Police_Force, IL.Police_Reference, 
					  IL.Record_Status, IL.Recovery_, IL.Referral_Source, IL.Repairer_, IL.Reserve_, IL.SCC, IL.Solicitors_, IL.Source_411765484, IL.Source_Description_411765489, 
					  IL.Source_Reference_411765487, IL.Status_Binding, IL.Storage_, IL.Storage_Address, IL.Sub_Party_Type, IL.x5x5x5_Grading_412284402
	FROM #_ILT IL

	INSERT INTO IBaseM8Cur._LinkEnd 
	SELECT LE.Unique_ID, LE.Entity_ID1, LE.Confidence, LE.Direction, LE.Entity_ID2, LE.EntityType_ID1, LE.EntityType_ID2, LE.LinkType_ID, LE.Record_Status, LE.Record_Type, LE.SCC
	FROM #_ILT LE

	IF OBJECT_ID('tempdb..#_ILT') is not null
		DROP TABLE #_ILT		
		
	--Sub Party Type    
	UPDATE IBaseM8Cur._Codes 
	SET Expansion = 'Driver'
	WHERE CodeGroup_ID = 2713 AND Expansion = 'Insured'

	--Incident_Link Double link Clean up
	DECLARE @T TABLE(Link_ID VARCHAR(50), Entity_ID1 VARCHAR(50), Entity_ID2 VARCHAR(50), Sub_Party_Type VARCHAR(50))
	DECLARE @D TABLE(Link_ID VARCHAR(50), Entity_ID1 VARCHAR(50), Entity_ID2 VARCHAR(50))

	;WITH CTE AS(
		SELECT LE.Entity_ID1, LE.Entity_ID2 , Incident_Link_ID
		FROM  IBaseM8Cur.Incident_Link AS IL WITH (NOLOCK) 
		INNER JOIN IBaseM8Cur._LinkEnd AS LE WITH (NOLOCK) ON IL.Unique_ID = LE.Link_ID
		WHERE LE.Entity_ID1 LIKE 'INC%'
		GROUP BY LE.Entity_ID1, LE.Entity_ID2 ,Incident_Link_ID
		HAVING COUNT(LE.Link_ID) > 1) 

	INSERT INTO @T
	SELECT LE.Link_ID, LE.Entity_ID1, LE.Entity_ID2, IL.Sub_Party_Type
	FROM  IBaseM8Cur.Incident_Link AS IL WITH (NOLOCK) 
	INNER JOIN IBaseM8Cur._LinkEnd AS LE WITH (NOLOCK) ON IL.Unique_ID = LE.Link_ID
	INNER JOIN CTE X ON LE.Entity_ID1 =  X.Entity_ID1 AND LE.Entity_ID2 = X.Entity_ID2 AND IL.Incident_Link_ID = X.Incident_Link_ID
	ORDER BY LE.Entity_ID1, LE.Entity_ID2

	INSERT INTO @D
	SELECT DISTINCT A.Link_ID, A.Entity_ID1, A.Entity_ID2
	FROM @T A
	INNER JOIN IBaseM8Cur._LinkEnd L1 ON A.Entity_ID1 = L1.Entity_ID1 COLLATE DATABASE_DEFAULT AND  L1.Entity_ID2 LIKE 'VEH%'
	INNER JOIN IBaseM8Cur._LinkEnd L2 ON A.Entity_ID2 = L2.Entity_ID1 COLLATE DATABASE_DEFAULT AND  L2.Entity_ID2 LIKE 'VEH%' COLLATE DATABASE_DEFAULT AND  L1.Entity_ID2  = L2.Entity_ID2
	INNER JOIN IBaseM8Cur.Vehicle_Link VL ON L2.Link_ID = VL.Unique_ID
	WHERE A.Sub_Party_Type !=  VL.Link_Type  COLLATE DATABASE_DEFAULT 

	--302
	DELETE FROM IBaseM8Cur._LinkEnd WHERE Link_Id IN(SELECT Link_ID  COLLATE DATABASE_DEFAULT  FROM @D)
	DELETE FROM IBaseM8Cur.Incident_Link WHERE Unique_ID IN (SELECT Link_ID COLLATE DATABASE_DEFAULT  FROM @D)

	---------------------------------------------------------------------
	--Paul Allen: Added: 20140624 - UpdateLookupValuesAndAltEntitys
	---------------------------------------------------------------------
	-------------------------------------
	--AddressType
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Address_Type] = 'Unknown'
	FROM IBaseM8Cur.[Address_] A
	WHERE [Address_Type] = 'Accident Location'

	UPDATE A
	SET [Address_Type] = NULL
	FROM IBaseM8Cur.[Address_] A
	WHERE [Address_Type] = 'Unknown'

	-------------------------------------
	--AddressLinkType
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Address_Link_Type] = 'Unknown'
	FROM IBaseM8Cur.[Address_to_Address_Link] A
	WHERE [Address_Link_Type] = 'Accident Location'

	UPDATE A
	SET [Address_Link_Type] = 'Trading Address'
	FROM IBaseM8Cur.[Address_to_Address_Link] A
	WHERE [Address_Link_Type] = 'Trades from'

	UPDATE A
	SET [Address_Link_Type] = 'Lives At'
	FROM IBaseM8Cur.[Address_to_Address_Link] A
	WHERE [Address_Link_Type] = 'Current Address'

	UPDATE A
	SET [Address_Link_Type] = 'Storage Address'
	FROM IBaseM8Cur.[Address_to_Address_Link] A
	WHERE [Address_Link_Type] = 'Stored at'

	UPDATE A
	SET [Address_Link_Type] = NULL
	FROM IBaseM8Cur.[Address_to_Address_Link] A
	WHERE [Address_Link_Type] = 'Unknown'

	-------------------------------------
	--IncidentType
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Incident_Type] = NULL
	FROM IBaseM8Cur.[Incident_] A
	WHERE [Incident_Type] = 'Unknown'

	-------------------------------------
	--ClaimType
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Claim_Type] = 'Home'
	FROM IBaseM8Cur.[Incident_] A
	WHERE [Claim_Type] = 'Household'

	UPDATE A
	SET [Claim_Type] = NULL
	FROM IBaseM8Cur.[Incident_] A
	WHERE [Claim_Type] = 'Unknown'

	-------------------------------------
	--KeoghsCaseStatus
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Case_Status] = NULL
	FROM IBaseM8Cur.[Keoghs_Case] A
	WHERE [Case_Status] = 'Unknown'

	-------------------------------------
	--PolicyPaymentType  
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Payment_Type] = NULL
	FROM IBaseM8Cur.[Policy_Payment_Link] A
	WHERE [Payment_Type] = 'Unknown'

	-------------------------------------
	--OrganisationStatus  
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Organisation_Status] = NULL
	FROM IBaseM8Cur.[Organisation_] A
	WHERE [Organisation_Status] = 'Unknown'

	-------------------------------------
	--OrganisationLinkType
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Link_Type] = 'Solicitor'
	FROM IBaseM8Cur.[Organisation_Match_Link] A
	WHERE [Link_Type] = 'Represented by'

	UPDATE A
	SET [Link_Type] = 'Accident Management'
	FROM IBaseM8Cur.[Organisation_Match_Link] A
	WHERE [Link_Type] = 'Uses'

	UPDATE A
	SET [Link_Type] = 'Medical Examiner'
	FROM IBaseM8Cur.[Organisation_Match_Link] A
	WHERE [Link_Type] = 'Examined by'

	UPDATE A
	SET [Link_Type] = 'Medical Legal'
	FROM IBaseM8Cur.[Organisation_Match_Link] A
	WHERE [Link_Type] = 'Referred by'

	UPDATE A
	SET [Link_Type] = 'Hire'
	FROM IBaseM8Cur.[Organisation_Match_Link] A
	WHERE [Link_Type] = 'Hire Provided By'

	UPDATE A
	SET [Link_Type] = NULL
	FROM IBaseM8Cur.[Organisation_Match_Link] A
	WHERE [Link_Type] = 'Unknown'

	-------------------------------------
	--OrganisationType 
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Organisation_Type] = 'Accident or Claims Management'
	FROM IBaseM8Cur.[Organisation_] A
	WHERE [Organisation_Type] = 'Acc Man Co'

	UPDATE A
	SET [Organisation_Type] = 'Credit Hire'
	FROM IBaseM8Cur.[Organisation_] A
	WHERE [Organisation_Type] = 'Hire Co'

	UPDATE A
	SET [Organisation_Type] = 'Vehicle Engineer'
	FROM IBaseM8Cur.[Organisation_] A
	WHERE [Organisation_Type] = 'Engineer'

	UPDATE A
	SET [Organisation_Type] = 'Recovery and / or Storage'
	FROM IBaseM8Cur.[Organisation_] A
	WHERE [Organisation_Type] = 'Recovery & Storage'

	UPDATE A
	SET [Organisation_Type] = 'Recovery'
	FROM IBaseM8Cur.[Organisation_] A
	WHERE [Organisation_Type] = 'Recovery Co'

	UPDATE A
	SET [Organisation_Type] = 'Storage'
	FROM IBaseM8Cur.[Organisation_] A
	WHERE [Organisation_Type] = 'Storage Co'

	UPDATE A
	SET [Organisation_Type] = 'Medical Examiner'
	FROM IBaseM8Cur.[Organisation_] A
	WHERE [Organisation_Type] = 'Medic Exp'

	UPDATE A
	SET [Organisation_Type] = 'Repairer'
	FROM IBaseM8Cur.[Organisation_] A
	WHERE [Organisation_Type] = 'Repair Centre'

	UPDATE A
	SET [Organisation_Type] = NULL
	FROM IBaseM8Cur.[Organisation_] A
	WHERE [Organisation_Type] = 'Unknown'

	UPDATE A
	SET [Organisation_Type] = 'Accident or Claims Management'
	FROM IBaseM8Cur.[Organisation_] A
	WHERE [Organisation_Type] = 'Accident Management'

	-------------------------------------
	--Person2OrganisationLinkType
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Link_Type] = 'Secretary'
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Co Secretary'

	UPDATE A
	SET [Link_Type] = 'Previous Director'
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Former Director'

	UPDATE A
	SET [Link_Type] = 'Pervious Secretary'
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Former Secretary'

	UPDATE A
	SET [Link_Type] = 'Previous Employee'
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Former Employee'

	UPDATE A
	SET [Link_Type] = 'Solicitor'
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Represented by'

	UPDATE A
	SET [Link_Type] = 'Engineer'
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Inspected by'

	UPDATE A
	SET [Link_Type] = 'Recovery'
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Recovered to, Recovered by'

	UPDATE A
	SET [Link_Type] = 'Storage'
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Stored at'

	UPDATE A
	SET [Link_Type] = 'Accident Management'
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Uses'

	UPDATE A
	SET [Link_Type] = 'Medical Examiner'
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Examined by'

	UPDATE A
	SET [Link_Type] = 'Medical Legal'
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Referred By'

	UPDATE A
	SET [Link_Type] = 'Hire'
	FROM [IBaseM8Cur].[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Hire Provided By'

	UPDATE A
	SET [Link_Type] = NULL
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Unknown'

	-------------------------------------
	--PaymentCardType
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Link_Type] = NULL
	FROM IBaseM8Cur.[Person_to_Organisation_Link] A
	WHERE [Link_Type] = 'Unknown'

	-------------------------------------
	--PolicyLinkType   
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Link_Type] = NULL
	FROM IBaseM8Cur.[Policy_Link] A
	WHERE [Link_Type] = 'Unknown'

	-------------------------------------
	--Person2PersonLinkType 
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Link_Type] = NULL
	FROM IBaseM8Cur.[Person_to_Person_Link] A
	WHERE [Link_Type] = 'Unknown'

	-------------------------------------
	--VehicleLinkType
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Link_Type] = 'Passenger'
	FROM IBaseM8Cur.[Vehicle_Link] A
	WHERE [Link_Type] = 'Passenger/Claimant'

	UPDATE A
	SET [Link_Type] = 'Driver'
	FROM IBaseM8Cur.[Vehicle_Link] A
	WHERE [Link_Type] = 'Named Driver'

	UPDATE A
	SET [Link_Type] = 'Inspected by'
	FROM IBaseM8Cur.[Vehicle_Link] A
	WHERE [Link_Type] = 'Inspected At'

	UPDATE A
	SET [Link_Type] = NULL
	FROM IBaseM8Cur.[Vehicle_Link] A
	WHERE [Link_Type] = 'Unknown'

	-------------------------------------
	--PartyType 
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Party_Type] = NULL
	FROM IBaseM8Cur.[Incident_Link] A
	WHERE [Party_Type] = 'Unknown'

	-------------------------------------
	--SubPartyType 
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Sub_Party_Type] = 'Passenger'
	FROM IBaseM8Cur.[Incident_Link] A
	WHERE [Sub_Party_Type] = 'Claimant'

	UPDATE A
	SET [Sub_Party_Type] = NULL
	FROM IBaseM8Cur.[Incident_Link] A
	WHERE [Sub_Party_Type] = 'Unknown'

	-------------------------------------
	--Salutation
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Salutation_] = 'Professor'
	FROM IBaseM8Cur.[Person_] A
	WHERE [Salutation_] = 'Prof'

	UPDATE A
	SET [Salutation_] = NULL
	FROM IBaseM8Cur.[Person_] A
	WHERE [Salutation_] = 'Unknown'

	-------------------------------------
	--Gender  
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Gender_] = NULL
	FROM IBaseM8Cur.[Person_] A
	WHERE [Gender_] = 'Unknown'

	-------------------------------------
	--PolicyType  
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Policy_Type] = NULL
	FROM IBaseM8Cur.[Policy_] A
	WHERE [Policy_Type] = 'Unknown'

	-------------------------------------
	--Incident2VehicleLinkType   
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Link_Type] = 'Third Party Vehicle'
	FROM IBaseM8Cur.[Vehicle_Incident_Link] A
	WHERE [Link_Type] = 'TP Vehicle'

	UPDATE A
	SET [Link_Type] = NULL
	FROM IBaseM8Cur.[Vehicle_Incident_Link] A
	WHERE [Link_Type] = 'Unknown'

	-------------------------------------
	--TelephoneLinkType
	-------------------------------------
	--1.SetTheIBase8Values
	UPDATE A
	SET [Link_Type] = NULL
	FROM IBaseM8Cur.[Telephone_Link] A
	WHERE [Link_Type] = 'Unknown'

	-------------------------------------------
	--UpdateAltEntitys
	-------------------------------------------
	UPDATE IBaseM8Cur.[Driving_Licence]
	SET AltEntity = 'Driving License'
	WHERE AltEntity != 'Driving License'

	UPDATE IBaseM8Cur.[NI_Number]
	SET AltEntity = 'NI'
	WHERE AltEntity != 'NI' 

	UPDATE IBaseM8Cur.[Passport_]
	SET AltEntity = 'Passport'
	WHERE AltEntity != 'Passport' 

	UPDATE IBaseM8Cur.[Passport_]
	SET AltEntity = 'Passport'
	WHERE AltEntity != 'Passport'
	
	UPDATE IBaseM8Cur.[Person_]
	SET AltEntity = CASE WHEN ISNULL(Gender_,'') IN ('Male','Female','Anonymous','Other name') THEN Gender_ ELSE 'Person' END
	WHERE AltEntity != CASE WHEN ISNULL(Gender_,'') IN ('Male','Female','Anonymous','Other name') THEN Gender_ ELSE 'Person' END

	UPDATE IBaseM8Cur.[Telephone_]
	SET AltEntity = CASE WHEN ISNULL(Telephone_Type,'') IN ('Landline','Mobile','FAX') THEN Telephone_Type ELSE 'Phone Unknown' END
	WHERE AltEntity != CASE WHEN ISNULL(Telephone_Type,'') IN ('Landline','Mobile','FAX') THEN Telephone_Type ELSE 'Phone Unknown' END
	
	UPDATE IBaseM8Cur.[Vehicle_]
	SET AltEntity = CASE WHEN ISNULL(Vehicle_Type,'') IN ('Bicycle','Car','Coach','Lorry','Minibus','Motorcycle','Pickup','Van') THEN Vehicle_Type ELSE '' END
	WHERE AltEntity != CASE WHEN ISNULL(Vehicle_Type,'') IN ('Bicycle','Car','Coach','Lorry','Minibus','Motorcycle','Pickup','Van') THEN Vehicle_Type ELSE '' END

	UPDATE IBaseM8Cur.[Organisation_]
	SET AltEntity = CASE
						WHEN ISNULL([Organisation_Type],'') IN ('Accident Management','Broker','Credit Hire','Insurer','Self Insured','Solicitor') THEN 'Office'
						WHEN ISNULL([Organisation_Type],'') IN ('Medical Examiner') THEN 'Hospital'
						WHEN ISNULL([Organisation_Type],'') IN ('Recovery','Storage','Repairer','Vehicle Engineer') THEN 'Garage' 
						ELSE '' 
					END
	WHERE AltEntity !=	CASE
							WHEN ISNULL([Organisation_Type],'') IN ('Accident Management','Broker','Credit Hire','Insurer','Self Insured','Solicitor') THEN 'Office'
							WHEN ISNULL([Organisation_Type],'') IN ('Medical Examiner') THEN 'Hospital'
							WHEN ISNULL([Organisation_Type],'') IN ('Recovery','Storage','Repairer','Vehicle Engineer') THEN 'Garage' 
							ELSE '' 
						END


	UPDATE IBaseM8Cur.[Incident_]
	SET AltEntity = CASE 
						WHEN ISNULL([Incident_Type],'') IN ('RTC') THEN 'Motor' 
						WHEN ISNULL([Incident_Type],'') IN ('Commercial') THEN 'Commercial Claim'
						WHEN ISNULL([Incident_Type],'') IN ('Employment') THEN 'EL Claim'
						WHEN ISNULL([Incident_Type],'') IN ('Public Liability') THEN 'PL Claim'
						WHEN ISNULL([Incident_Type],'') IN ('Travel') THEN 'Travel Claim'
						WHEN ISNULL([Incident_Type],'') IN ('Home Contents','Home Building','CUE PI','MIAFTR') THEN [Incident_Type]
						WHEN ISNULL([Incident_Type],'') IN ('Keoghs CMS') THEN 'CMS'
						WHEN ISNULL([Incident_Type],'') IN ('Keoghs TCS') THEN 'TCS'
						WHEN ISNULL([Incident_Type],'') IN ('Theft') THEN 'Theft Claim'
						WHEN ISNULL([Incident_Type],'') IN ('Pet') THEN 'Pet Claim'
						WHEN ISNULL([Incident_Type],'') IN ('Mobile Phone') THEN 'Mobile Claim'
						WHEN ISNULL([Incident_Type],'') IN ('Keoghs CFS') AND ISNULL([Fraud_Ring_Name],'') = '' THEN 'Keoghs CFS'
						WHEN ISNULL([Incident_Type],'') IN ('Keoghs CFS') AND ISNULL([Fraud_Ring_Name],'') != '' THEN 'Keoghs CFS Fraud Ring'
						ELSE ''
					END
		WHERE AltEntity != CASE 
								WHEN ISNULL([Incident_Type],'') IN ('RTC') THEN 'Motor' 
								WHEN ISNULL([Incident_Type],'') IN ('Commercial') THEN 'Commercial Claim'
								WHEN ISNULL([Incident_Type],'') IN ('Employment') THEN 'EL Claim'
								WHEN ISNULL([Incident_Type],'') IN ('Public Liability') THEN 'PL Claim'
								WHEN ISNULL([Incident_Type],'') IN ('Travel') THEN 'Travel Claim'
								WHEN ISNULL([Incident_Type],'') IN ('Home Contents','Home Building','CUE PI','MIAFTR') THEN [Incident_Type]
								WHEN ISNULL([Incident_Type],'') IN ('Keoghs CMS') THEN 'CMS'
								WHEN ISNULL([Incident_Type],'') IN ('Keoghs TCS') THEN 'TCS'
								WHEN ISNULL([Incident_Type],'') IN ('Theft') THEN 'Theft Claim'
								WHEN ISNULL([Incident_Type],'') IN ('Pet') THEN 'Pet Claim'
								WHEN ISNULL([Incident_Type],'') IN ('Mobile Phone') THEN 'Mobile Claim'
								WHEN ISNULL([Incident_Type],'') IN ('Keoghs CFS') AND ISNULL([Fraud_Ring_Name],'') = '' THEN 'Keoghs CFS'
								WHEN ISNULL([Incident_Type],'') IN ('Keoghs CFS') AND ISNULL([Fraud_Ring_Name],'') != '' THEN 'Keoghs CFS Fraud Ring'
								ELSE ''
							END

	----------------------------------------------------
	--SetTheInsurerNameAndRefForCueMatches:ItemTFS,4943
	--PaulAllen
	--20140723
	----------------------------------------------------
	UPDATE IML
	SET  [Insurer_]		= Data.Insurer_
		,[Insurer_Ref]	= Data.Insurer_Ref
	FROM IBaseM8Cur.Incident_Match_Link IML
	INNER JOIN	(
				SELECT LE.Link_ID, CUE.[Unique_ID], CUE.[Insurer_], CUE.[Insurer_Ref]
				FROM IBaseM5Cur.[MIAFTR_Match] CUE
				INNER JOIN IBaseM5Cur.[_LinkEnd] LE ON LE.Entity_ID1 = CUE.Unique_ID
				INNER JOIN IBaseM5Cur.[MIAFTR_Match_Link] CUELink ON CUELink.Unique_ID = LE.Link_ID
				GROUP BY LE.Link_ID, CUE.[Unique_ID], CUE.[Insurer_], CUE.[Insurer_Ref]
				) Data ON Data.Link_ID = IML.Incident_Match_Link_ID

	UPDATE VIL
	SET  [Insurer_]		= Data.Insurer_
		,[Insurer_Ref]	= Data.Insurer_Ref
	FROM IBaseM8Cur.[Vehicle_Incident_Link] VIL
	INNER JOIN	(
				SELECT LE.Link_ID, CUE.[Unique_ID], CUE.[Insurer_], CUE.[Insurer_Ref]
				FROM IBaseM5Cur.[MIAFTR_Match] CUE
				INNER JOIN IBaseM5Cur.[_LinkEnd] LE ON LE.Entity_ID1 = CUE.Unique_ID
				INNER JOIN IBaseM5Cur.[MIAFTR_Match_Link] CUELink ON CUELink.Unique_ID = LE.Link_ID
				GROUP BY LE.Link_ID, CUE.[Unique_ID], CUE.[Insurer_], CUE.[Insurer_Ref]
				) Data ON Data.Link_ID = VIL.[Vehicle_Incident_Link_ID]

	----------------------------------------------------
	--IBase5SchemaChangesNewColumns
	--PaulAllen
	--20140723
	----------------------------------------------------
	--KeoghsCase
	UPDATE KC
	SET  [Data_Type]				= Data.Data_Type
		,[Primary_Weed_Date]		= Data.Primary_Weed_Date
		,[Primary_Weed_Details]		= Data.Primary_Weed_Details
		,[Secondary_Weed_Date]		= Data.Secondary_Weed_Date
		,[Secondary_Weed_Details]	= Data.Secondary_Weed_Details
	FROM IBaseM8Cur.[Keoghs_Case] KC
	INNER JOIN	(
				SELECT [Unique_ID], [Data_Type], [Primary_Weed_Date], [Primary_Weed_Details], [Secondary_Weed_Date], [Secondary_Weed_Details]
				FROM IBaseM5Cur.[ClaimFile_]
				) Data ON Data.[Unique_ID] = KC.Keoghs_Case_ID

	--FraudRing
	UPDATE FR
	SET	 [MO_]							= Data.MO_
		,[Owning_Insurer]				= Data.Owning_Insurer
		,[Closed_Date]					= Data.Closed_Date
		,[Industry_Alternative_Names]	= Data.Industry_Alternative_Names
	FROM IBaseM8Cur.[Fraud_Ring] FR
	INNER JOIN (
				SELECT [Unique_ID], [MO_], [Owning_Insurer], [Closed_Date], [Industry_Alternative_Names]
				FROM IBaseM5Cur.[Fraud_Ring]
				) Data ON Data.[Unique_ID] = FR.[Fraud_Ring_ID]

	--Organisation
	UPDATE O
	SET  [Dissolved_Date]		= Data.Dissolved_Date
		,[Incorporated_Date]	= Data.Incorporated_Date
		,[Document_]			= Data.Document_
		,[Document__Binding]	= Data.Document__Binding
		,[Organisation_Type]	= Data.Organisation_Type
	FROM IBaseM8Cur.[Organisation_] O
	INNER JOIN	(
				SELECT [Unique_ID], [Dissolved_Date], [Incorporated_Date], [Document_], [Document__Binding], [Organisation_Type]
				FROM IBaseM5Cur.[Organisation_]
				) Data ON Data.[Unique_ID] = O.[Organisation_ID]

	--Person
	UPDATE P
	SET [NHS_Number] =  Data.NHS_Number
	FROM IBaseM8Cur.[Person_] P
	INNER JOIN	(
				SELECT [Unique_ID], [NHS_Number]
				FROM IBaseM5Cur.[Person_]
				) Data ON Data.[Unique_ID] = P.[Person_ID]

	--Vehicle
	UPDATE V
	SET [VIN_] = Data.[VIN_Number]
	FROM IBaseM8Cur.[Vehicle_] V
	INNER JOIN	(
				SELECT [Unique_ID], [VIN_Number]
				FROM IBaseM5Cur.[Vehicle_]
				) Data ON Data.[Unique_ID] = V.Vehicle_ID

	-------------------------------------------
	--DeleteWronglyCreatedIntelligenceLinksBetweenFraudRingAndIncident
	--PaulAllen
	--20150410
	-------------------------------------------
	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#IntelligenceLinks') IS NOT NULL
		DROP TABLE #IntelligenceLinks
	CREATE TABLE #IntelligenceLinks (IntelligenceLinkID VARCHAR(50) NOT NULL)

	--GetTheData
	INSERT INTO #IntelligenceLinks (IntelligenceLinkID)
	SELECT Link_ID
	FROM IBaseM8Cur.[_LinkEnd]
	WHERE [EntityType_ID1] = 1208 --FraudRing
	AND [EntityType_ID2] = 1688 --Incident
	AND LinkType_ID = 2967 -- Intelligence

	--DeleteTheDATA
	DELETE IL
	FROM IBaseM8Cur.[Intelligence_Link] IL
	INNER JOIN #IntelligenceLinks DEL ON DEL.IntelligenceLinkID = IL.Unique_ID

	DELETE LE
	FROM IBaseM8Cur.[_LinkEnd] LE
	INNER JOIN #IntelligenceLinks DEL ON DEL.IntelligenceLinkID = LE.Link_ID

	--Tidy
	DROP TABLE #IntelligenceLinks

	/* NOT GOING INTO JOHNNY FIAMA RELEASE
	-----------------------------------------------------------
	--RemoveAnyIncidentsWithInvalidDates
	--PaulAllen
	--20151102
	-----------------------------------------------------------
	DECLARE  @SQL NVARCHAR(MAX)
			,@LinkTablesToProcess SMALLINT = 0
			,@LinkTableCounter SMALLINT = 1
			,@LinkTableName VARCHAR(50)
			,@EntityTablesToProcess SMALLINT = 0
			,@EntityTableCounter SMALLINT = 1
			,@EntityTableName VARCHAR(50)

	IF OBJECT_ID('tempdb..#DeleteList') IS NOT NULL
		DROP TABLE #DeleteList
	CREATE TABLE #DeleteList (RowID INT IDENTITY(1,1), Link_Id VARCHAR(50) NOT NULL, LinkTableName VARCHAR(50) NOT NULL, LinkTableRowID TINYINT NOT NULL, Entity_ID VARCHAR(50) NOT NULL, EntityTableName VARCHAR(50) NOT NULL, EntityTableRowID TINYINT NOT NULL)

	--GetAllInavlidIncidents
	DECLARE @IncidentsWithoutValidDate TABLE (UniqueID VARCHAR(10) NOT NULL)
	INSERT INTO @IncidentsWithoutValidDate (UniqueID)
	SELECT Unique_Id
	FROM IBaseM8Cur.[Incident_]
	WHERE ISDATE([Incident_Date]) != 1
	OR [Incident_Date] > GETDATE();

	--GetFlattenedListOfAllRelatedLinksAndEntitys
	WITH CTE_FlatLinksAndEntitys AS
		(
		SELECT   Link_Id
				,Entity_ID
		FROM	(
				SELECT Link_Id, [Entity_ID1], [Entity_ID2]
				FROM IBaseM8Cur.[_LinkEnd] LE
				WHERE EXISTS (SELECT TOP 1 1 FROM @IncidentsWithoutValidDate IWVD_1 WHERE IWVD_1.UniqueID = LE.[Entity_ID1])
				OR EXISTS (SELECT TOP 1 1 FROM @IncidentsWithoutValidDate IWVD_1 WHERE IWVD_1.UniqueID = LE.[Entity_ID2])
				) DATA
		UNPIVOT (Entity_ID FOR Value IN (Entity_ID1,Entity_ID2)) PIV
		GROUP BY Link_Id, Entity_ID
		)
	INSERT INTO #DeleteList (Link_Id, LinkTableName, LinkTableRowID, Entity_ID, EntityTableName, EntityTableRowID)
	SELECT   CTE.Link_Id
			,DT1.[PhysicalName] LinkTableName
			,DENSE_RANK() OVER (ORDER BY LEFT(Link_Id,3)) LinkTableRowID
			,Entity_ID
			,DT2.[PhysicalName] EntityTableName
			,DENSE_RANK() OVER (ORDER BY LEFT(Entity_ID,3)) LinkTableRowID
	FROM CTE_FlatLinksAndEntitys CTE
	INNER JOIN [MDAControl].[dbo].[DataTable] DT1 ON DT1.[TableCode] = LEFT(Link_Id,3)
	INNER JOIN [MDAControl].[dbo].[DataTable] DT2 ON DT2.[TableCode] = LEFT(Entity_ID,3)
	ORDER BY Link_Id, Entity_ID;

	SELECT   @LinkTablesToProcess	= MAX(LinkTableRowID)
			,@EntityTablesToProcess = MAX(EntityTableRowID) 
	FROM #DeleteList

	--IfThereIsNothingToDoReturn
	IF @LinkTablesToProcess + @EntityTablesToProcess = 0
		GOTO Abort

		PRINT 'Deleting LinkEnd Records...'
		--1.)DeleteTheLinkEndRecords
		DELETE LE
		FROM IBaseM8Cur.[_LinkEnd] LE
		WHERE EXISTS (SELECT TOP 1 1 FROM #DeleteList DL WHERE DL.Link_Id = LE.Link_Id GROUP BY DL.Link_Id)
		PRINT '>> ' +CAST(@@ROWCOUNT AS VARCHAR) + ' Records deleted from LinkEnd...'

		--2.)DeleteTheLinkRecord
		PRINT 'Deleting Link Records..'
		WHILE @LinkTableCounter <= @LinkTablesToProcess
		BEGIN
			SELECT @LinkTableName = (SELECT LinkTableName FROM #DeleteList WHERE LinkTableRowID = @LinkTableCounter GROUP BY LinkTableName)
			SELECT @SQL = ' DELETE DEL
							FROM IBaseM8Cur.' + @LinkTableName + ' DEL
							WHERE EXISTS (SELECT TOP 1 1 FROM #DeleteList DL WHERE DL.LinkTableRowID = '+CAST(@LinkTableCounter AS VARCHAR)+' AND DL.Link_Id = DEL.Unique_Id)'

			--PRINT @SQL
			EXECUTE (@SQL)
			PRINT '>> ' +CAST(@@ROWCOUNT AS VARCHAR) + ' Records deleted from '+@LinkTableName+'...'
			SELECT @LinkTableCounter += 1
		END

		--3.)DeleteTheEntityRecords
		--WeCheckThatTheEntityIsntOnAnyOtherLink
		PRINT 'Deleting Entity Records..'
		WHILE @EntityTableCounter <= @EntityTablesToProcess
		BEGIN
			SELECT @EntityTableName = (SELECT EntityTableName FROM #DeleteList WHERE EntityTableRowID = @EntityTableCounter GROUP BY EntityTableName)
			SELECT @SQL = ' DELETE DEL
							FROM IBaseM8Cur.' + @EntityTableName + ' DEL
							WHERE EXISTS (SELECT TOP 1 1 FROM #DeleteList DL WHERE DL.EntityTableRowID = '+CAST(@EntityTableCounter AS VARCHAR)+' AND DL.Entity_Id = DEL.Unique_Id)
							AND NOT EXISTS (SELECT TOP 1 1 FROM [IBaseM8Cur].[_LinkEnd] LE WHERE NOT EXISTS (SELECT TOP 1 1 FROM #DeleteList DL2 WHERE DL2.EntityTableRowID = '+CAST(@EntityTableCounter AS VARCHAR)+' AND DL2.Link_Id = LE.Link_ID) AND LE.[Entity_ID1] = DEL.Unique_Id)'
			--PRINT @SQL
			EXECUTE (@SQL)
			PRINT '>> ' +CAST(@@ROWCOUNT AS VARCHAR) + ' Records deleted from '+@EntityTableName+'...'
			SELECT @EntityTableCounter += 1
		END

		--WeNowNeedToRunASync
		EXECUTE [MDAControl].[Sync].[uspSynchroniseMaster]

	Abort:
	*/
	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					--,@InsertRowCount= @InsertRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)

		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
END CATCH