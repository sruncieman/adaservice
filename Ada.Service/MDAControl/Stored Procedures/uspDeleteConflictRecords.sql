﻿
CREATE PROCEDURE [Rep].[uspDeleteConflictRecords]
/**************************************************************************************************/
-- ObjectName:			Rep.uspDeleteConflictRecords
--
-- Description:			
--
-- Steps:				
--
-- Dependencies:		
--
-- Usage:				
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-03-24			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
				,@DeleteCount		INT = 0
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 77 --DeleteConflictRecordsForReplication
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
			
	------------------------------------------------------------
	--2)MainBody
	------------------------------------------------------------
	DECLARE  @IBaseM8CurrentDB  VARCHAR(50) = (SELECT TOP 1 name FROM sys.databases WHERE patindex('IBase8_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name DESC)
			,@IBaseM8PreviousDB VARCHAR(50) = (SELECT TOP 1 name FROM sys.databases WHERE patindex('IBase8_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name )
			,@Today				DATETIME	= (SELECT DATEADD(DAY, DATEDIFF(DAY, '19000101', GETDATE()), '19000101'))
	
	/*RemoveConfilctRecordsFromRep.EntityRecord*/
	DELETE ER
	OUTPUT @Today, @IBaseM8PreviousDB, @IBaseM8CurrentDB, Deleted.TableCode, Deleted.IBase5EntityID, Deleted.IBase8EntityID_Staging, Deleted.IBase8EntityID, Deleted.RecordAction
	INTO [Rep].[EntityRecordHistory] (CompareDate, DatabaseFromName, DatabaseToName, TableCode, IBase5EntityID, IBase8EntityID_Staging, IBase8EntityID, RecordAction)
	FROM [Rep].[EntityRecordHistory] ER
	INNER JOIN (
				SELECT  ID
				FROM	(	
						SELECT ROW_NUMBER() OVER (PARTITION BY IBase8EntityID ORDER BY ID) RowID , ID, IBase8EntityID
						FROM Rep.EntityRecord ER
						WHERE EXISTS	(
										SELECT TOP 1 1 
										FROM Rep.EntityRecord ER1
										WHERE ER1.IBase8EntityID = ER.IBase8EntityID
										GROUP BY IBase8EntityID 
										HAVING COUNT(1) > 1
										)
						OR IBase8EntityID IS NULL
						) A
				WHERE RowID != 1
				AND IBase8EntityID IS NOT NULL
				) DEL ON DEL.ID = ER.ID
	SELECT @DeleteCount += @@ROWCOUNT

	/*RemoveConfilctRecordsFromRep.LinkRecord*/
	DELETE LE
	OUTPUT @Today, @IBaseM8PreviousDB, @IBaseM8CurrentDB, Deleted.TableCode, Deleted.IBase5LinkID, Deleted.IBase8LinkID_Staging, Deleted.IBase8LinkID, Deleted.EliteReference, Deleted.RiskClaim_Id, Deleted.RecordAction
	INTO [Rep].[LinkRecordConflict] (CompareDate, DatabaseFromName, DatabaseToName, TableCode, IBase5LinkID, IBase8LinkID_Staging, IBase8LinkID, EliteReference, RiskClaim_Id, RecordAction)
	FROM Rep.LinkRecord LE
	INNER JOIN (
				SELECT  ID
				FROM	(	
						SELECT ROW_NUMBER() OVER (PARTITION BY IBase8LinkID ORDER BY ID) RowID , ID, IBase8LinkID
						FROM Rep.LinkRecord LR
						WHERE EXISTS	(
										SELECT TOP 1 1 
										FROM Rep.LinkRecord LR1
										WHERE LR1.IBase8LinkID = LR.IBase8LinkID
										GROUP BY IBase8LinkID 
										HAVING COUNT(1) > 1
										)
						OR IBase8LinkID IS NULL
						) A
				WHERE RowID != 1
				AND IBase8LinkID IS NOT NULL
				) DEL ON DEL.ID = LE.ID
	SELECT @DeleteCount += @@ROWCOUNT

	/*RemoveConfilctRecordsFromRep.LinkEndRecord*/
	DELETE LE
	OUTPUT @Today, @IBaseM8PreviousDB, @IBaseM8CurrentDB, Deleted.IBase5LinkID, Deleted.IBase5EntityID1, Deleted.IBase5EntityID2, Deleted.IBase8LinkIDStaging, Deleted.IBase8EntityID1Staging, Deleted.IBase8EntityID2Staging, Deleted.IBase8LinkID, Deleted.IBase8EntityID1, Deleted.IBase8EntityID2, Deleted.RecordAction
	INTO [Rep].[LinkEndRecordConflict] (CompareDate, DatabaseFromName, DatabaseToName, IBase5LinkID, IBase5EntityID1, IBase5EntityID2, IBase8LinkIDStaging, IBase8EntityID1Staging, IBase8EntityID2Staging, IBase8LinkID, IBase8EntityID1, IBase8EntityID2, RecordAction)
	FROM [Rep].[LinkEndRecord] LE
	INNER JOIN (
				SELECT  ID
				FROM	(	
						SELECT ROW_NUMBER() OVER (PARTITION BY IBase8LinkID, IBase8EntityID1 ORDER BY ID) RowID , ID, IBase8LinkID, IBase8EntityID1
						FROM Rep.LinkEndRecord LE
						WHERE EXISTS	(
										SELECT TOP 1 1 
										FROM Rep.LinkEndRecord LE1
										WHERE LE1.IBase8LinkID = LE.IBase8LinkID
										AND LE1.IBase8EntityID1 = LE.IBase8EntityID1
										GROUP BY IBase8LinkID, IBase8EntityID1  
										HAVING COUNT(1) > 1
										)
						OR IBase8LinkID IS NULL
						OR IBase8EntityID1 IS NULL
						) A
				WHERE RowID != 1
				AND IBase8LinkID IS NOT NULL
				AND IBase8EntityID1 IS NOT NULL
				) DEL ON DEL.ID = LE.ID
	SELECT @DeleteCount += @@ROWCOUNT

	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					,@DeleteRowCount= @DeleteCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
END CATCH



