﻿CREATE PROCEDURE dbo.[uspDeleteFromLoggingTables]
/**************************************************************************************************/
-- ObjectName:		[dbo].[DeleteFromLoggingTables]
--
-- Steps:			1)DeclareVariablesTempObjects
--					2)BuildTheSQLStatementsToExecute
--					3)ExecuteTheSQLStatements
--					4)Tidy	
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-06-02			Paul Allen		Created
/**************************************************************************************************/
(
@DaysToRetain INT = 14
)
AS
SET NOCOUNT ON
SET DATEFORMAT YMD

----------------------------------------------------
--1)DeclareVariablesTempObjects
----------------------------------------------------
DECLARE  @SQL					NVARCHAR(MAX)
		,@DeleteStatementCount	INT
		,@Counter				INT			= 1
		,@DeleteDatePoint		VARCHAR(20)	= CONVERT(VARCHAR,DATEADD(D,-@DaysToRetain,GETDATE()),110) 

IF OBJECT_ID('tempdb..#DeleteStatement') IS NOT NULL
	DROP TABLE #DeleteStatement
CREATE TABLE #DeleteStatement (ID INT IDENTITY(1,1), SQLStatement VARCHAR(MAX))

----------------------------------------------------
--2)BuildTheSQLStatementsToExecute
----------------------------------------------------
INSERT INTO #DeleteStatement (SQLStatement)
SELECT 'SET DATEFORMAT YMD DELETE FROM ' + S.NAME + '.' + T.NAME + ' WHERE DMLDateTimeStamp <= ''' +  @DeleteDatePoint + ''''
FROM SYS.columns c
INNER JOIN SYS.tables T ON T.object_id = C.object_id
INNER JOIN SYS.schemas S ON S.schema_id = T.schema_id
WHERE C.NAME = 'DMLDateTimeStamp'
AND S.name = 'SyncDML'
SELECT @DeleteStatementCount = @@ROWCOUNT

----------------------------------------------------
--3)ExecuteTheSQLStatements
----------------------------------------------------
WHILE @Counter <= @DeleteStatementCount
BEGIN
	SELECT @SQL = (SELECT SQLStatement FROM #DeleteStatement WHERE ID = @Counter)
	EXECUTE SP_EXECUTESQL @SQL
	SELECT @Counter +=1
END

----------------------------------------------------
--4)Tidy
----------------------------------------------------
IF OBJECT_ID('tempdb..#DeleteStatement') IS NOT NULL
	DROP TABLE #DeleteStatement