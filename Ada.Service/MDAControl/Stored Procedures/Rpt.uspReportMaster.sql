﻿CREATE PROCEDURE [Rpt].[uspReportMaster]
AS
SET NOCOUNT ON

EXECUTE Rpt.uspGenerateDLGClaimReport

EXECUTE Rpt.uspGenerateMobileClaimReport

EXECUTE Rpt.uspGenerateMIHighRiskClaimReport