﻿CREATE PROCEDURE [Sync].[uspSynchroniseLinkOutcome]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspSynchroniseLinkOutcome]
--
-- Steps:			1)DeclareVariablesTempObjectsAndStartLogging
--					2)QueryTheCTTablesAndPrepForAction
--						2.1)GetTheChanges
--						2.2)AddSomeIndexesToMakeTheProcessQuicker 
--						2.3)GetTheUniqueIDsForTheseRecords
--						2.4)AreThereAnyConflictRecords
--						2.5)AddTheseConflictsToAConflictTable
--						2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
--						2.7)TruncateOutcomeTablesInMDAIfFirstRun
--					3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
--						3.1)GenerateTheNewIBaseUniqueIDs
--						3.2)MDAIBase: LinkTablesChanges
--							3.2.1)ActionTheLinkChangeInMDA_IBase
--							3.2.2)DoDeletes
--							3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						3.3)MDAIBase: LinkEndTablesChanges
--							3.3.1)PopulateTheWorkingTable
--							3.3.2)ActionTheLinkEndChangeInMDA_IBase
--							3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
--						4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
--							4.1)ActionTheChangeInMDA
--							4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
--							4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
--						5)UpdateLoggingAndTidy	
--
-- RevisionHistory:
--		Date				Author				Modification
--		2014-04-28			Simon Hundleby		Created
--		2014-05-22			Paul Allen			Added section 2.7
--		2014-11-26			Paul Allen			Fix on Section 3.1, ROW_NUMBER added. 
/**************************************************************************************************/
(
 @MDAChangeVersionFrom			INT
,@MDAChangeVersionTo			INT
,@MDA_IBaseChangeVersionFrom	INT
,@MDA_IBaseChangeVersionTo		INT
,@ParentTaskID					INT
,@Step							NVARCHAR(10) = '0' OUTPUT						
)
AS
SET NOCOUNT ON
BEGIN TRY

	----------------------------------------------------
	--1)DeclareVariablesTempObjectsAndStartLogging
	----------------------------------------------------
	DECLARE  @Now					DATETIME 
			,@Details				VARCHAR(1000)
			,@ProcessID				INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
			,@OutputTaskID			INT
			,@DatabaseID			INT = DB_ID()
			,@ObjectID				INT = ISNULL(@@PROCID,-1)
			,@InsertRowCount		INT	= NULL
			,@UpdateRowCount		INT = NULL
			,@DeleteRowCount		INT = NULL
			,@Sys_Change_Context	VARBINARY(128)
			,@NextID				INT
			,@Counter				INT
			,@PhysicalTableName		VARCHAR(255) = 'Outcome_Link'
			,@Prefix				VARCHAR(3)   
			,@LinkTypeID			INT

	SELECT @LinkTypeID =Table_ID, @Prefix = TableCode FROM [$(MDA_IBase)].[dbo]._DataTable WHERE PhysicalName = @PhysicalTableName


	IF OBJECT_ID('tempdb..#CT') IS NOT NULL
		DROP TABLE #CT	 
	CREATE TABLE #CT (RowID INT IDENTITY(1,1) PRIMARY KEY, ChangeDatabase VARCHAR(10), PhysicalTableName VARCHAR(255), MDA_ID INT, MDA_IBase_Unique_ID VARCHAR(20), DMLAction VARCHAR(1), DoNotReplicate BIT)

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging
	CREATE TABLE #LinkEndStaging (Link_ID NVARCHAR(50), Entity_ID1 NVARCHAR(50), Confidence TINYINT, Direction TINYINT, Entity_ID2 NVARCHAR(50), EntityType_ID1 INT , EntityType_ID2 INT, LinkType_ID INT, Record_Status TINYINT, Record_Type TINYINT, SCC NVARCHAR(255))
					
	SELECT @Details = 'User=['+SYSTEM_USER+']'			
	EXECUTE dbo.uspCRUDDBControlTask
				 @ParentTaskID		= @ParentTaskID
				,@ProcessID			= @ProcessID
				,@TaskNameID		= 130 --SynchroniseOutcome Link
				,@DatabaseID		= @DatabaseID
				,@ObjectID			= @ObjectID
				,@Details			= @Details
				,@OutputTaskID		= @OutputTaskID OUTPUT

	----------------------------------------------------
	--2)QueryTheCTTablesAndPrepForAction
	----------------------------------------------------
	--2.1)GetTheChanges
	SET @Step ='2.1';

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Incident2PersonOutcome', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA)].[dbo].Incident2PersonOutcome, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_ID, DMLAction)
	SELECT 'MDA', 'Incident2OrganisationOutcome', ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA)].[dbo].Incident2OrganisationOutcome, @MDAChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDAChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Outcome_Link', Unique_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA_IBase)].[dbo].Outcome_Link, @MDA_IBaseChangeVersionFrom) CT
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL

	INSERT INTO #CT (ChangeDatabase, PhysicalTableName, MDA_IBase_Unique_ID, DMLAction)
	SELECT 'MDA_IBase', 'Link_End', Link_ID, Sys_Change_Operation
	FROM CHANGETABLE(CHANGES [$(MDA_IBase)].[dbo]._LinkEnd, @MDA_IBaseChangeVersionFrom) CT
	INNER JOIN [$(MDA_IBase)].[dbo].Outcome_Link IAL ON IAL.Unique_ID = CT.Link_ID
	WHERE Sys_Change_Version <= @MDA_IBaseChangeVersionTo
	AND Sys_Change_Context IS NULL
	GROUP BY Link_ID, Sys_Change_Operation

	--2.2)AddSomeIndexesToMakeTheProcessQuicker 
	SET @Step ='2.2';

	CREATE NONCLUSTERED INDEX IX_CT_MDA_ID ON #CT (MDA_ID)
	CREATE NONCLUSTERED INDEX IX_CT_MDA_IBase_Unique_ID ON #CT (MDA_IBase_Unique_ID ASC)
	CREATE NONCLUSTERED INDEX IX_CT_PhysicalTableName ON #CT (PhysicalTableName ASC)


	--2.3)GetTheUniqueIDsForTheseRecords
	SET @Step ='2.3';

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IAL.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [$(MDA_IBase)].[dbo].Outcome_Link IAL ON IAL.Unique_ID = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Outcome_Link'
	AND IAL.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_ID = [Sync].[fn_ExtractMDAID] (IVSL.MDA_Incident_ID_412284502)
	FROM #CT CT
	INNER JOIN [$(MDA_IBase)].[dbo].[VW_StagingLinks] IVSL ON IVSL.[IBase8LinkID] = CT.MDA_IBase_Unique_ID
	WHERE CT.PhysicalTableName = 'Link_End'
	AND IVSL.MDA_Incident_ID_412284502 IS NOT NULL

	UPDATE CT
	SET MDA_IBase_Unique_ID = MP2B.IBaseId
	FROM #CT CT
	INNER JOIN [$(MDA)].[dbo].Incident2PersonOutcome MP2B ON MP2B.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Incident2PersonOutcome'

	UPDATE CT
	SET MDA_IBase_Unique_ID = MO2A.IBaseId
	FROM #CT CT
	INNER JOIN [$(MDA)].[dbo].Incident2OrganisationOutcome MO2A ON MO2A.ID = CT.MDA_ID
	WHERE CT.PhysicalTableName = 'Incident2OrganisationOutcome'

	--2.4)AreThereAnyConflictRecords
	SET @Step ='2.4';

	UPDATE CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT MDA_ID, MDA_IBase_Unique_ID
				FROM #CT
				WHERE MDA_ID IS NOT NULL
				AND MDA_IBase_Unique_ID IS NOT NULL
				AND PhysicalTableName != 'Link_End'
				GROUP BY MDA_ID, MDA_IBase_Unique_ID
				HAVING COUNT(1) > 1
				) IQ ON IQ.MDA_ID = CT.MDA_ID AND IQ.MDA_IBase_Unique_ID = CT.MDA_IBase_Unique_ID

	--2.5)AddTheseConflictsToAConflictTable
	SET @Step ='2.5';

	INSERT INTO Sync.ReplicationConflict (MDA_ID, MDA_IBase_Unique_ID, ObjectID, AlertSent, ConflictDateTime)
	SELECT MDA_ID, MDA_IBase_Unique_ID, @ObjectID, 0, GETDATE()
	FROM #CT CT
	WHERE DoNotReplicate = 1

	--2.6)AsDuplicateWilloccurInLinKEndWeWanttoRemoveTheseIfTheEntityExists
	SET @Step ='2.6';
	UPDATE #CT
	SET DoNotReplicate = 1
	FROM #CT CT
	INNER JOIN (
				SELECT	RowID
				FROM	(
						SELECT   RowID
								,ROW_NUMBER() OVER (PARTITION BY MDA_IBase_Unique_ID ORDER BY RowID) RowNumber
						FROM #CT CT 
						WHERE EXISTS (SELECT TOP 1 1 FROM #CT CT1 WHERE MDA_IBase_Unique_ID = MDA_IBase_Unique_ID AND ChangeDatabase = 'MDA_IBase' GROUP BY MDA_IBase_Unique_ID HAVING COUNT(1) > 1)
						) Data 
				WHERE RowNumber !=1
				) IQ ON IQ.RowID = CT.RowID
	WHERE ChangeDatabase = 'MDA_IBase'

	--2.7)TruncateOutcomeTablesInMDAIfFirstRun
	--The Outcome Data is repopulated in full as part of the nightly replication process, therefore in the first Sync post this the
	--tables [$(MDA)].[dbo].[Incident2PersonOutcome] and [$(MDA)].[dbo].[Incident2OrganisationOutcome] need truncating.
	IF (SELECT COUNT(1) FROM #CT WHERE PhysicalTableName = 'Outcome_Link') = (SELECT COUNT(1) FROM  [$(MDA_IBase)].[dbo].Outcome_Link) AND (SELECT COUNT(1) FROM  [$(MDA_IBase)].[dbo].Outcome_Link) !=0
	BEGIN
		SELECT 'Truncating Table'
		TRUNCATE TABLE [$(MDA)].[dbo].[Incident2PersonOutcome]
		TRUNCATE TABLE [$(MDA)].[dbo].[Incident2OrganisationOutcome]
	END;

	--------------------------------------------------------------------------
	--3)ImplementTheChangesIntoMDAIBase,WriteNewlyInsertedIDsBackToTheSource(MDA)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA') > 0
	BEGIN
		--3.1)GenerateTheNewIBaseUniqueIDs
		SET @Step ='3.1';

		SELECT @Counter = (SELECT COUNT(1) FROM #CT WHERE MDA_IBase_Unique_ID IS NULL)

		SELECT @NextID = NextID FROM [$(MDA_IBase)].[dbo].[_Outcome_Link_NextID]
		UPDATE [$(MDA_IBase)].[dbo].[_Outcome_Link_NextID] SET NextID = @NextID + @Counter 
	
		UPDATE #CT
		SET MDA_IBase_Unique_ID = New_Unique_ID
		FROM	(
				SELECT	 @Prefix + CONVERT(VARCHAR(20), ROW_NUMBER() OVER(ORDER BY RowID) + @NextID - 1) AS New_Unique_ID
						,RowID
				FROM #CT 
				WHERE MDA_IBase_Unique_ID IS NULL
				AND MDA_ID IS NOT NULL
				) IQ
		INNER JOIN #CT CT ON CT.RowID = IQ.RowID

		--3.2)MDAIBase: LinkTablesChanges
		--------------------------------------------------------------------------
		--3.2.1)ActionTheLinkChangeInMDA_IBase
		SET @Step ='3.2.1';
		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [$(MDA_IBase)].[dbo].[Outcome_Link] [Target]
		USING  (
				SELECT *
				FROM	(
						SELECT ROW_NUMBER() OVER (PARTITION BY CT.MDA_IBase_Unique_ID ORDER BY SettlementDate DESC) RowNumberID, CT.PhysicalTableName, CT.MDA_ID, CT.MDA_IBase_Unique_ID, IQ.*
						FROM #CT CT
						INNER JOIN	(
									SELECT RowID, ID, [IBaseId] [Unique_ID], [OutcomeLinkId] [Outcome_Link_ID], EnforcementRole,SettlementDate,PropertyOwned, MannerOfResolution,MethodOfEnforcement,DateOfSettlementAgreed,SettlementAmountAgreed,AmountRecoveredToDate,AmountStillOutstanding,WasEnforcementSuccessful,TypeOfSuccess,CreatedDate [Create_Date], CreatedBy [Create_User], ModifiedDate [Last_Upd_Date], ModifiedBy [Last_Upd_User], RecordStatus [Record_Status], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], FiveGrading  [x5x5x5_Grading_412284402], RiskClaim_Id [Risk_Claim_ID_414244883]
									FROM [$(MDA)].[dbo].Incident2PersonOutcome P2W
									INNER JOIN #CT CT ON CT.MDA_ID = P2W.ID
									WHERE CT.PhysicalTableName = 'Incident2PersonOutcome'
									UNION ALL
									SELECT RowID, ID, [IBaseId] [Unique_ID], [OutcomeLinkId] [Outcome_Link_ID], EnforcementRole,SettlementDate,PropertyOwned,MannerOfResolution,MethodOfEnforcement, DateOfSettlementAgreed,SettlementAmountAgreed,AmountRecoveredToDate,AmountStillOutstanding,WasEnforcementSuccessful,TypeOfSuccess,CreatedDate [Create_Date], CreatedBy [Create_User], ModifiedDate [Last_Upd_Date], ModifiedBy [Last_Upd_User], RecordStatus [Record_Status], Source [Source_411765484], SourceDescription [Source_Description_411765489], SourceReference [Source_Reference_411765487], FiveGrading  [x5x5x5_Grading_412284402], RiskClaim_Id [Risk_Claim_ID_414244883]
									FROM [$(MDA)].[dbo].Incident2OrganisationOutcome O2BA
									INNER JOIN #CT CT ON CT.MDA_ID = O2BA.ID
									WHERE CT.PhysicalTableName = 'Incident2OrganisationOutcome'
									) IQ ON IQ.RowID = CT.RowID
						WHERE CT.DoNotReplicate IS NULL
						AND CT.ChangeDatabase = 'MDA'
						) DATA
				WHERE DATA.RowNumberID = 1
				) Source ON Source.MDA_IBase_Unique_ID = [Target].Unique_ID
		WHEN MATCHED THEN UPDATE
		SET	 EnforceMent_Role				= Source.EnforceMentRole
			,Settlement_Date				= Source.SettlementDate
			,Property_Owned					= Source.PropertyOwned
			,Manner_Of_Resolution			= Source.MannerOfResolution
			,Method_Of_Enforcement			= Source.MethodOfEnforcement
			,Date_Settlement_Agreed			= Source.DateOfSettlementAgreed
			,Settlement_Amount_Agreed		= Source.SettlementAmountAgreed
			,Amount_Recovered_To_Date		= Source.AmountRecoveredToDate
			,Amount_Still_Outstanding		= Source.AmountStillOutstanding
			,Was_Enforcement_Successful		= Source.WasEnforcementSuccessful
			,Create_Date					= Source.Create_Date
			,Create_User					= Source.Create_User
			,Last_Upd_Date					= Source.Last_Upd_Date
			,Last_Upd_User					= Source.Last_Upd_User
			,Record_Status					= Source.Record_Status
			,Source_411765484				= Source.Source_411765484
			,Source_Description_411765489	= Source.Source_Description_411765489
			,Source_Reference_411765487		= Source.Source_Reference_411765487
			,x5x5x5_Grading_412284402		= Source.x5x5x5_Grading_412284402
			,Risk_Claim_ID_414244883		= Source.Risk_Claim_ID_414244883
		WHEN NOT MATCHED THEN INSERT
			(
			 Unique_ID
			,EnforceMent_Role
			,Settlement_Date
			,Property_Owned
			,Manner_Of_Resolution
			,Method_Of_Enforcement
			,Date_Settlement_Agreed
			,Settlement_Amount_Agreed
			,Amount_Recovered_To_Date
			,Amount_Still_Outstanding
			,Was_Enforcement_Successful
			,Create_Date
			,Create_User
			,Do_Not_Disseminate_412284494
			,Last_Upd_Date
			,Last_Upd_User
			,MDA_Incident_ID_412284502
			,Record_Status
			,Source_411765484
			,Source_Description_411765489
			,Source_Reference_411765487
			,x5x5x5_Grading_412284402
			,Risk_Claim_ID_414244883
			)
		VALUES
			(
			 Source.MDA_IBase_Unique_ID
			,Source.EnforceMentRole
			,Source.SettlementDate
			,Source.PropertyOwned
			,Source.MannerOfResolution
			,Source.MethodOfEnforcement
			,Source.DateOfSettlementAgreed
			,Source.SettlementAmountAgreed
			,Source.AmountRecoveredToDate
			,Source.AmountStillOutstanding
			,Source.WasEnforcementSuccessful
			,Source.Create_Date
			,Source.Create_User
			,0
			,Source.Last_Upd_Date
			,Source.Last_Upd_User
			,CASE WHEN Source.PhysicalTableName= 'Incident2PersonOutcome' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Incident2PersonOutcome') WHEN Source.PhysicalTableName= 'Incident2OrganisationOutcome' THEN [Sync].[fn_ResolveMDAID] (Source.Id,'MDA.dbo.Incident2OrganisationOutcome') ELSE NULL END
			,Source.Record_Status
			,Source.Source_411765484
			,Source.Source_Description_411765489
			,Source.Source_Reference_411765487
			,Source.x5x5x5_Grading_412284402
			,Source.Risk_Claim_ID_414244883
		)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Amount_Recovered_to_Date,'') + '}|I:{' + ISNULL(Inserted.Amount_Recovered_to_Date,'') + '}'
				,'D:{' + ISNULL(Deleted.Amount_Still_Outstanding,'') + '}|I:{' + ISNULL(Inserted.Amount_Still_Outstanding,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_Settlement_Agreed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_Settlement_Agreed,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Enforcement_Role,'') + '}|I:{' + ISNULL(Inserted.Enforcement_Role,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Manner_of_Resolution,'') + '}|I:{' + ISNULL(Inserted.Manner_of_Resolution,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Method_of_Enforcement,'') + '}|I:{' + ISNULL(Inserted.Method_of_Enforcement,'') + '}'
				,'D:{' + ISNULL(Deleted.Outcome_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Outcome_Link_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Property_Owned,'') + '}|I:{' + ISNULL(Inserted.Property_Owned,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Settlement_Amount_Agreed,'') + '}|I:{' + ISNULL(Inserted.Settlement_Amount_Agreed,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Settlement_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Settlement_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.Type_of_Success,'') + '}|I:{' + ISNULL(Inserted.Type_of_Success,'') + '}'
				,'D:{' + ISNULL(Deleted.Was_Enforcement_Successful,'') + '}|I:{' + ISNULL(Inserted.Was_Enforcement_Successful,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		 INTO [SyncDML].[MDAIbase_Outcome_Link]
           ([TaskID]
           ,[DMLAction]
           ,[Unique_ID]
           ,[AltEntity]
           ,[Amount_Recovered_to_Date]
           ,[Amount_Still_Outstanding]
           ,[Create_Date]
           ,[Create_User]
           ,[Date_Settlement_Agreed]
           ,[Do_Not_Disseminate_412284494]
           ,[Enforcement_Role]
           ,[IconColour]
           ,[Key_Attractor_412284410]
           ,[Last_Upd_Date]
           ,[Last_Upd_User]
           ,[Manner_of_Resolution]
           ,[MDA_Incident_ID_412284502]
           ,[Method_of_Enforcement]
           ,[Outcome_Link_ID]
           ,[Property_Owned]
           ,[Record_Status]
           ,[SCC]
           ,[Settlement_Amount_Agreed]
           ,[Settlement_Date]
           ,[Source_411765484]
           ,[Source_Description_411765489]
           ,[Source_Reference_411765487]
           ,[Status_Binding]
           ,[Type_of_Success]
           ,[Was_Enforcement_Successful]
           ,[x5x5x5_Grading_412284402]
           ,[Risk_Claim_ID_414244883]);

		
		--3.2.2)DoDeletes
		--WeNeedToDeleteTheLinkEndrecordBeforetheDeletesOrWeWontBeAbleTo
		SET @Step ='3.2.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [$(MDA_IBase)].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [$(MDA_IBase)].[dbo].[_LinkEnd] LE
		INNER JOIN (
					SELECT [IBase8LinkID] Link_ID
					FROM
							(
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident2PersonOutcome') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Incident2PersonOutcome'
							AND DMLAction = 'D'
							UNION ALL
							SELECT [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident2OrganisationOutcome') MDA_Incident_ID_412284502
							FROM #CT CT
							WHERE PhysicalTableName = 'Incident2OrganisationOutcome'
							AND DMLAction = 'D'
							) DATA
					INNER JOIN [$(MDA_IBase)].[dbo].[VW_StagingLinks] IVSL ON IVSL.[MDA_Incident_ID_412284502] = DATA.MDA_Incident_ID_412284502
					) IQ ON IQ.Link_ID = LE.Link_ID;
				

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		DELETE [$(MDA_IBase)].[dbo].[Outcome_Link]
		OUTPUT	 @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Amount_Recovered_to_Date,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Amount_Still_Outstanding,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_Settlement_Agreed,''),126) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Enforcement_Role,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Manner_of_Resolution,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Method_of_Enforcement,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Outcome_Link_ID,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Property_Owned,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Settlement_Amount_Agreed,'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Settlement_Date,''),126) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Type_of_Success,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Was_Enforcement_Successful,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{}'
		 INTO [SyncDML].[MDAIbase_Outcome_Link]
           ([TaskID]
           ,[DMLAction]
           ,[Unique_ID]
           ,[AltEntity]
           ,[Amount_Recovered_to_Date]
           ,[Amount_Still_Outstanding]
           ,[Create_Date]
           ,[Create_User]
           ,[Date_Settlement_Agreed]
           ,[Do_Not_Disseminate_412284494]
           ,[Enforcement_Role]
           ,[IconColour]
           ,[Key_Attractor_412284410]
           ,[Last_Upd_Date]
           ,[Last_Upd_User]
           ,[Manner_of_Resolution]
           ,[MDA_Incident_ID_412284502]
           ,[Method_of_Enforcement]
           ,[Outcome_Link_ID]
           ,[Property_Owned]
           ,[Record_Status]
           ,[SCC]
           ,[Settlement_Amount_Agreed]
           ,[Settlement_Date]
           ,[Source_411765484]
           ,[Source_Description_411765489]
           ,[Source_Reference_411765487]
           ,[Status_Binding]
           ,[Type_of_Success]
           ,[Was_Enforcement_Successful]
           ,[x5x5x5_Grading_412284402]
           ,[Risk_Claim_ID_414244883])
		FROM [$(MDA_IBase)].[dbo].[Outcome_Link] IAL
		INNER JOIN #CT CT ON CASE WHEN CT.PhysicalTableName = 'Incident2PersonOutcome' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID, 'MDA.dbo.Incident2PersonOutcome') WHEN CT.PhysicalTableName= 'Incident2OrganisationOutcome' THEN [Sync].[fn_ResolveMDAID] (CT.MDA_ID,'MDA.dbo.Incident2OrganisationOutcome') ELSE NULL END = IAL.MDA_Incident_ID_412284502
		WHERE CT.DMLAction = 'D';


		
		--3.2.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		SET @Step ='3.2.3';
		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MP2WEB
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'			
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.OutcomeLinkId,'') + '}|I:{' + ISNULL(Inserted.OutcomeLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SettlementDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SettlementDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.MannerOfResolution,'') + '}|I:{' + ISNULL(Inserted.MannerOfResolution,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OutcomeCategory_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OutcomeCategory_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.EnforcementRole,'') + '}|I:{' + ISNULL(Inserted.EnforcementRole,'') + '}'
				,'D:{' + ISNULL(Deleted.PropertyOwned,'') + '}|I:{' + ISNULL(Inserted.PropertyOwned,'') + '}'
				,'D:{' + ISNULL(Deleted.MethodOfEnforcement,'') + '}|I:{' + ISNULL(Inserted.MethodOfEnforcement,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.DateOfSettlementAgreed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.DateOfSettlementAgreed,''),126) + '}'
				,'D:{' + ISNULL(Deleted.SettlementAmountAgreed,'') + '}|I:{' + ISNULL(Inserted.SettlementAmountAgreed,'') + '}'
				,'D:{' + ISNULL(Deleted.AmountRecoveredToDate,'') + '}|I:{' + ISNULL(Inserted.AmountRecoveredToDate,'') + '}'
				,'D:{' + ISNULL(Deleted.AmountStillOutstanding,'') + '}|I:{' + ISNULL(Inserted.AmountStillOutstanding,'') + '}'
				,'D:{' + ISNULL(Deleted.WasEnforcementSuccessful,'') + '}|I:{' + ISNULL(Inserted.WasEnforcementSuccessful,'') + '}'
				,'D:{' + ISNULL(Deleted.TypeOfSuccess,'') + '}|I:{' + ISNULL(Inserted.TypeOfSuccess,'') + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2PersonOutcome]
           ([TaskID]
           ,[DMLAction]
           ,[Id]
           ,[Incident_Id]
           ,[Person_Id]
           ,[OutcomeLinkId]
           ,[SettlementDate]
           ,[MannerOfResolution]
           ,[OutcomeCategory_Id]
           ,[EnforcementRole]
           ,[PropertyOwned]
           ,[MethodOfEnforcement]
           ,[DateOfSettlementAgreed]
           ,[SettlementAmountAgreed]
           ,[AmountRecoveredToDate]
           ,[AmountStillOutstanding]
           ,[WasEnforcementSuccessful]
           ,[TypeOfSuccess]
           ,[FiveGrading]
           ,[LinkConfidence]
           ,[Source]
           ,[SourceReference]
           ,[SourceDescription]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[RiskClaim_Id]
           ,[BaseRiskClaim_Id]
           ,[IBaseId]
           ,[RecordStatus]
           ,[ADARecordStatus]
          )
		FROM [$(MDA)].[dbo].Incident2PersonOutcome MP2WEB
		INNER JOIN #CT CT ON CT.MDA_ID = MP2WEB.Id
 		WHERE (MP2WEB.IBaseId IS NULL
		OR MP2WEB.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Incident2PersonOutcome';


		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		UPDATE MO2WEB
		SET IBaseID = CT.MDA_IBase_Unique_ID
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.OutcomeLinkId,'') + '}|I:{' + ISNULL(Inserted.OutcomeLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SettlementDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SettlementDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.MannerOfResolution,'') + '}|I:{' + ISNULL(Inserted.MannerOfResolution,'') + '}'
				,'D:{' + ISNULL(Deleted.EnforcementRole,'') + '}|I:{' + ISNULL(Inserted.EnforcementRole,'') + '}'
				,'D:{' + ISNULL(Deleted.PropertyOwned,'') + '}|I:{' + ISNULL(Inserted.PropertyOwned,'') + '}'
				,'D:{' + ISNULL(Deleted.MethodOfEnforcement,'') + '}|I:{' + ISNULL(Inserted.MethodOfEnforcement,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.DateOfSettlementAgreed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.DateOfSettlementAgreed,''),126) + '}'
				,'D:{' + ISNULL(Deleted.SettlementAmountAgreed,'') + '}|I:{' + ISNULL(Inserted.SettlementAmountAgreed,'') + '}'
				,'D:{' + ISNULL(Deleted.AmountRecoveredToDate,'') + '}|I:{' + ISNULL(Inserted.AmountRecoveredToDate,'') + '}'
				,'D:{' + ISNULL(Deleted.AmountStillOutstanding,'') + '}|I:{' + ISNULL(Inserted.AmountStillOutstanding,'') + '}'
				,'D:{' + ISNULL(Deleted.WasEnforcementSuccessful,'') + '}|I:{' + ISNULL(Inserted.WasEnforcementSuccessful,'') + '}'
				,'D:{' + ISNULL(Deleted.TypeOfSuccess,'') + '}|I:{' + ISNULL(Inserted.TypeOfSuccess,'') + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		 INTO [SyncDML].[MDA_Incident2OrganisationOutcome]
           ([TaskID]
           ,[DMLAction]
           ,[Id]
           ,[Incident_Id]
           ,[Organisation_Id]
           ,[OutcomeLinkId]
           ,[SettlementDate]
           ,[MannerOfResolution]
           ,[EnforcementRole]
           ,[PropertyOwned]
           ,[MethodOfEnforcement]
           ,[DateOfSettlementAgreed]
           ,[SettlementAmountAgreed]
           ,[AmountRecoveredToDate]
           ,[AmountStillOutstanding]
           ,[WasEnforcementSuccessful]
           ,[TypeOfSuccess]
           ,[FiveGrading]
           ,[LinkConfidence]
           ,[Source]
           ,[SourceReference]
           ,[SourceDescription]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[RiskClaim_Id]
           ,[BaseRiskClaim_Id]
           ,[IBaseId]
           ,[RecordStatus]
           ,[ADARecordStatus]
           ) 
		FROM [$(MDA)].[dbo].Incident2OrganisationOutcome MO2WEB
		INNER JOIN #CT CT ON CT.MDA_ID = MO2WEB.Id
 		WHERE (MO2WEB.IBaseId IS NULL
		OR MO2WEB.IBaseId != CT.MDA_IBase_Unique_ID)
		AND CT.PhysicalTableName = 'Incident2OrganisationOutcome';


		--------------------------------------------------------------------------
		--3.3)MDAIBase: LinkEndTablesChanges
		-------------------------------------------------------------------------
		--3.3.1)PopulateTheWorkingTable
		--DoForEntityID1
		--CheckTheValuesThatArebeingAdded
		SET @Step ='3.3.1';

		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC
		FROM	(
				SELECT MP2WEB.IBaseId Link_ID, BA.IBaseId Entity_ID1, MP2WEB.LinkConfidence Confidence, 0 Direction, P.IBaseId Entity_ID2, 1688 EntityType_ID1, 1969 EntityType_ID2, @LinkTypeID LinkType_ID, MP2WEB.RecordStatus Record_Status, 1 Record_Type, NULL SCC
				FROM [$(MDA)].[dbo].Incident2PersonOutcome MP2WEB 
				INNER JOIN [$(MDA)].[dbo].Incident BA ON BA.ID = MP2WEB.Incident_Id
				INNER JOIN [$(MDA)].[dbo].[Person] P ON P.ID = MP2WEB.Person_ID
				INNER JOIN #CT CT ON CT.MDA_ID = MP2WEB.ID AND CT.PhysicalTableName = 'Incident2PersonOutcome'
				UNION ALL
				SELECT MO2WEB.IBaseId, BA.IBaseId, MO2WEB.LinkConfidence, 0, O.IBaseId, 1688, 1893, @LinkTypeID, MO2WEB.RecordStatus, 1, NULL
				FROM [$(MDA)].[dbo].Incident2OrganisationOutcome MO2WEB 
				INNER JOIN [$(MDA)].[dbo].Incident BA ON BA.ID = MO2WEB.Incident_Id
				INNER JOIN [$(MDA)].[dbo].[Organisation] O ON O.ID = MO2WEB.Organisation_Id
				INNER JOIN #CT CT ON CT.MDA_ID = MO2WEB.ID AND CT.PhysicalTableName = 'Incident2OrganisationOutcome'
				) Data

		--NowDoForEntityID2
		INSERT INTO #LinkEndStaging (Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		SELECT Link_ID, Entity_ID2, Confidence, Direction, Entity_ID1, EntityType_ID2, EntityType_ID1, LinkType_ID, Record_Status, 2, SCC
		FROM #LinkEndStaging;

		CREATE CLUSTERED INDEX IX_LinkEndStaging_Link_ID_Entity_ID1 ON #LinkEndStaging (Link_ID, Entity_ID1);

		--3.3.2)ActionTheLinkEndChangeInMDA_IBase
		SET @Step ='3.3.2';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		MERGE [$(MDA_IBase)].[dbo].[_LinkEnd] [Target]
		USING	(
				SELECT *
				FROM #LinkEndStaging
				) Source ON Source.Link_ID = [Target].Link_ID AND Source.Entity_ID1 = [Target].Entity_ID1
		WHEN MATCHED AND CHECKSUM(Source.Link_ID, Source.Entity_ID1, Source.Confidence, Source.Direction, Source.Entity_ID2, Source.EntityType_ID1, Source.EntityType_ID2, Source.LinkType_ID, Source.Record_Status, Source.Record_Type, Source.SCC) != CHECKSUM([Target].Link_ID, [Target].Entity_ID1, [Target].Confidence, [Target].Direction, [Target].Entity_ID2, [Target].EntityType_ID1, [Target].EntityType_ID2, [Target].LinkType_ID, [Target].Record_Status, [Target].Record_Type, [Target].SCC) THEN UPDATE
		SET  Confidence			= Source.Confidence
			,Direction			= Source.Direction
			,Entity_ID2			= Source.Entity_ID2
			,EntityType_ID1		= Source.EntityType_ID1
			,EntityType_ID2		= Source.EntityType_ID2
			,LinkType_ID		= Source.LinkType_ID
			,Record_Status		= Source.Record_Status
			,Record_Type		= Source.Record_Type
			,SCC				= Source.SCC
		WHEN NOT MATCHED THEN INSERT
			(
			 Link_ID
			,Entity_ID1
			,Confidence
			,Direction
			,Entity_ID2
			,EntityType_ID1
			,EntityType_ID2
			,LinkType_ID
			,Record_Status
			,Record_Type
			,SCC
			)
		VALUES
			(
			 Source.Link_ID
			,Source.Entity_ID1
			,Source.Confidence
			,Source.Direction
			,Source.Entity_ID2
			,Source.EntityType_ID1
			,Source.EntityType_ID2
			,Source.LinkType_ID
			,Source.Record_Status
			,Source.Record_Type
			,Source.SCC
			)
		OUTPUT	 @OutputTaskID
				,$Action
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{' + ISNULL(Inserted.[Link_ID],'') + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID1],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Confidence],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Direction],'')) + '}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{' + ISNULL(Inserted.[Entity_ID2],'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID1],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[EntityType_ID2],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[LinkType_ID],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Status],'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.[Record_Type],'')) + '}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{' + ISNULL(Inserted.[SCC],'') + '}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC);

		--3.3.3)NowIdentifyAnythingThatNeedsToBeDeletedDueToTheAbove:BecauseTheDeDuplicationProcessCanAmendThePrimaryKeyWillNeedToRemoveAndOrphanedRecords
		SET @Step ='3.3.3';

		WITH CHANGE_TRACKING_CONTEXT (@Sys_Change_Context)
		DELETE  [$(MDA_IBase)].[dbo].[_LinkEnd]
		OUTPUT   @OutputTaskID
				,'DELETE'
				,'D:{' + ISNULL(Deleted.[Link_ID],'') + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID1],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Confidence],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Direction],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[Entity_ID2],'') + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID1],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[EntityType_ID2],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[LinkType_ID],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Status],'')) + '}|I:{}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.[Record_Type],'')) + '}|I:{}'
				,'D:{' + ISNULL(Deleted.[SCC],'') + '}|I:{}'
		INTO [SyncDML].[MDAIBase__LinkEnd] (TaskID, DMLAction, Link_ID, Entity_ID1, Confidence, Direction, Entity_ID2, EntityType_ID1, EntityType_ID2, LinkType_ID, Record_Status, Record_Type, SCC)
		FROM [$(MDA_IBase)].[dbo].[_LinkEnd] LE
		WHERE EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID)
		AND NOT EXISTS (SELECT TOP 1 1 FROM #LinkEndStaging LES WHERE LES.Link_ID = LE.Link_ID AND LES.Entity_ID1 = LE.Entity_ID1);

	END
	
	--------------------------------------------------------------------------
	--4)ImplementTheChangesIntoMDA,WriteNewlyInsertedIDsBackToTheSource(IBase)
	--------------------------------------------------------------------------
	IF (SELECT COUNT(1) FROM #CT WHERE ChangeDatabase = 'MDA_IBase') > 0
	BEGIN
		--4.1)ActionTheChangeInMDA
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--Person2BankAccount
		SET @Step ='4.1';

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [$(MDA)].[dbo].Incident2PersonOutcome [Target]
		USING	(
				SELECT	 CT.MDA_ID
						,CT.MDA_IBase_Unique_ID
						,[Sync].[fn_ExtractMDAID] (P.MDA_Incident_ID_412284502) [Person_Id]
						,[Sync].[fn_ExtractMDAID] (IW.MDA_Incident_ID_412284502) [Incident_Id]
						,IWL.[Unique_ID] [OutcomeLinkId]
						,IWL.[x5x5x5_Grading_412284402] [FiveGrading]
						,IWL.EnforceMent_Role
						,IWL.Settlement_Date
						,IWL.Property_Owned
						,IWL.Manner_Of_Resolution
						,IWL.Method_Of_Enforcement
						,IWL.Date_Settlement_Agreed
						,IWL.Settlement_Amount_Agreed
						,IWL.Amount_Recovered_To_Date
						,IWL.Amount_Still_Outstanding
						,IWL.Was_Enforcement_Successful
						,ILE.Confidence [LinkConfidence]
						,IWL.[Source_411765484] [Source]
						,IWL.[Source_Reference_411765487] [SourceReference]
						,IWL.[Source_Description_411765489] [SourceDescription]
						,IWL.Create_User [CreatedBy]
						,IWL.Create_Date [CreatedDate]
						,IWL.Last_Upd_User [ModifiedBy]
						,IWL.Last_Upd_Date [ModifiedDate]
						,IWL.[Risk_Claim_ID_414244883] RiskClaim_Id
						,IWL.[Risk_Claim_ID_414244883] BaseRiskClaim_ID
						,IWL.Unique_ID [IBaseId]
						,IWL.Record_Status [RecordStatus]		
				FROM #CT CT
				INNER JOIN [$(MDA_IBase)].[dbo].[Outcome_Link] IWL ON IWL.Unique_ID = CT.MDA_IBase_Unique_ID
				INNER JOIN [$(MDA_IBase)].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IWL.Unique_ID
				INNER JOIN [$(MDA_IBase)].[dbo].[Incident_] IW ON IW.Unique_ID = ILE.Entity_ID1
				INNER JOIN [$(MDA_IBase)].[dbo].[Person_] P ON P.Unique_ID = ILE.Entity_ID2
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				AND IW.[Incident_Date] < GETDATE() --WeOnlyInsertRecordThatHaveAValidIncidentDate
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED 
		THEN UPDATE
		SET	 
			 EnforceMentRole				= Source.EnforceMent_Role	
			,SettlementDate					= Source.Settlement_Date	
			,PropertyOwned					= Source.Property_Owned	
			,MannerOfResolution				= Source.Manner_Of_Resolution	
			,MethodOfEnforcement			= Source.Method_Of_Enforcement	
			,DateOfSettlementAgreed			= Source.Date_Settlement_Agreed	
			,SettlementAmountAgreed			= Source.Settlement_Amount_Agreed	
			,AmountRecoveredToDate			= Source.Amount_Recovered_To_Date	
			,AmountStillOutstanding			= Source.Amount_Still_Outstanding	
			,WasEnforcementSuccessful		= Source.Was_Enforcement_Successful	
			,CreatedDate					= Source.CreatedDate	
			,CreatedBy						= Source.CreatedBy	
			,ModifiedDate					= Source.ModifiedDate
			,ModifiedBy						= Source.ModifiedBy	
			,Source							= Source.Source	
			,SourceDescription				= Source.SourceDescription	
			,SourceReference				= Source.SourceReference
			,FiveGrading					= Source.FiveGrading
			,RecordStatus					= Source.Recordstatus
			,ADARecordStatus				= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
			 Person_Id
			,Incident_Id
			,OutcomeLinkId
			,EnforceMentRole
			,SettlementDate
			,PropertyOwned
			,MannerOfResolution
			,MethodOfEnforcement
			,DateOfSettlementAgreed
			,SettlementAmountAgreed
			,AmountRecoveredToDate
			,AmountStillOutstanding
			,WasEnforcementSuccessful
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
		VALUES	
			(
			 Source.Person_Id
			,Source.Incident_Id
			,Source.OutcomeLinkId
			,Source.EnforceMent_Role
			,Source.Settlement_Date
			,Source.Property_Owned
			,Source.Manner_Of_Resolution
			,Source.Method_Of_Enforcement
			,Source.Date_Settlement_Agreed
			,Source.Settlement_Amount_Agreed
			,Source.Amount_Recovered_To_Date
			,Source.Amount_Still_Outstanding
			,Source.Was_Enforcement_Successful
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10
			)
		OUTPUT 
			@OutputTaskID
			,$ACTION
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person_Id,'')) + '}'
			,'D:{' + ISNULL(Deleted.OutcomeLinkId,'') + '}|I:{' + ISNULL(Inserted.OutcomeLinkId,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SettlementDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SettlementDate,''),126) + '}'
			,'D:{' + ISNULL(Deleted.MannerOfResolution,'') + '}|I:{' + ISNULL(Inserted.MannerOfResolution,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OutcomeCategory_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OutcomeCategory_Id,'')) + '}'
			,'D:{' + ISNULL(Deleted.EnforcementRole,'') + '}|I:{' + ISNULL(Inserted.EnforcementRole,'') + '}'
			,'D:{' + ISNULL(Deleted.PropertyOwned,'') + '}|I:{' + ISNULL(Inserted.PropertyOwned,'') + '}'
			,'D:{' + ISNULL(Deleted.MethodOfEnforcement,'') + '}|I:{' + ISNULL(Inserted.MethodOfEnforcement,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.DateOfSettlementAgreed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.DateOfSettlementAgreed,''),126) + '}'
			,'D:{' + ISNULL(Deleted.SettlementAmountAgreed,'') + '}|I:{' + ISNULL(Inserted.SettlementAmountAgreed,'') + '}'
			,'D:{' + ISNULL(Deleted.AmountRecoveredToDate,'') + '}|I:{' + ISNULL(Inserted.AmountRecoveredToDate,'') + '}'
			,'D:{' + ISNULL(Deleted.AmountStillOutstanding,'') + '}|I:{' + ISNULL(Inserted.AmountStillOutstanding,'') + '}'
			,'D:{' + ISNULL(Deleted.WasEnforcementSuccessful,'') + '}|I:{' + ISNULL(Inserted.WasEnforcementSuccessful,'') + '}'
			,'D:{' + ISNULL(Deleted.TypeOfSuccess,'') + '}|I:{' + ISNULL(Inserted.TypeOfSuccess,'') + '}'
			,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
			,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
			,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
			,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
			,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
			,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
			,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2PersonOutcome]
           ([TaskID]
           ,[DMLAction]
           ,[Id]
           ,[Incident_Id]
           ,[Person_Id]
           ,[OutcomeLinkId]
           ,[SettlementDate]
           ,[MannerOfResolution]
           ,[OutcomeCategory_Id]
           ,[EnforcementRole]
           ,[PropertyOwned]
           ,[MethodOfEnforcement]
           ,[DateOfSettlementAgreed]
           ,[SettlementAmountAgreed]
           ,[AmountRecoveredToDate]
           ,[AmountStillOutstanding]
           ,[WasEnforcementSuccessful]
           ,[TypeOfSuccess]
           ,[FiveGrading]
           ,[LinkConfidence]
           ,[Source]
           ,[SourceReference]
           ,[SourceDescription]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[RiskClaim_Id]
           ,[BaseRiskClaim_Id]
           ,[IBaseId]
           ,[RecordStatus]
           ,[ADARecordStatus]
          );


		--Incident2OrganisationOutcome

		SET @Sys_Change_Context = CONVERT(VARBINARY(128),'SyncFromMDA_IBase');
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		MERGE [$(MDA)].[dbo].Incident2OrganisationOutcome [Target]
		USING	(
				SELECT	 CT.MDA_ID
						,CT.MDA_IBase_Unique_ID
						,[Sync].[fn_ExtractMDAID] (O.MDA_Incident_ID_412284502) [Organisation_Id]
						,[Sync].[fn_ExtractMDAID] (IW.MDA_Incident_ID_412284502) [Incident_Id]
						,IWL.[Outcome_Link_ID] [OutComeLinkId]
						,IWL.[x5x5x5_Grading_412284402] [FiveGrading]
						,IWL.EnforceMent_Role
						,IWL.Settlement_Date
						,IWL.Property_Owned
						,IWL.Manner_Of_Resolution
						,IWL.Method_Of_Enforcement
						,IWL.Date_Settlement_Agreed
						,IWL.Settlement_Amount_Agreed
						,IWL.Amount_Recovered_To_Date
						,IWL.Amount_Still_Outstanding
						,IWL.Was_Enforcement_Successful
						,ILE.Confidence [LinkConfidence]
						,IWL.[Source_411765484] [Source]
						,IWL.[Source_Reference_411765487] [SourceReference]
						,IWL.[Source_Description_411765489] [SourceDescription]
						,IWL.Create_User [CreatedBy]
						,IWL.Create_Date [CreatedDate]
						,IWL.Last_Upd_User [ModifiedBy]
						,IWL.Last_Upd_Date [ModifiedDate]
						,IWL.[Risk_Claim_ID_414244883] RiskClaim_Id
						,IWL.[Risk_Claim_ID_414244883] BaseRiskClaim_ID
						,IWL.Unique_ID [IBaseId]	
						,IWL.Record_Status [RecordStatus]	
				FROM #CT CT
				INNER JOIN [$(MDA_IBase)].[dbo].[Outcome_Link] IWL ON IWL.Unique_ID = CT.MDA_IBase_Unique_ID
				INNER JOIN [$(MDA_IBase)].[dbo].[_LinkEnd] ILE ON ILE.Link_ID = IWL.Unique_ID
				INNER JOIN [$(MDA_IBase)].[dbo].[Incident_] IW ON IW.Unique_ID = ILE.Entity_ID1
				INNER JOIN [$(MDA_IBase)].[dbo].[Organisation_] O ON O.Unique_ID = ILE.Entity_ID2
				WHERE CT.DoNotReplicate IS NULL
				AND CT.ChangeDatabase = 'MDA_IBase'
				) Source ON Source.MDA_ID = [Target].ID
		WHEN MATCHED 
		THEN UPDATE
		SET	  
			EnforceMentRole					= Source.EnforceMent_Role	
			,SettlementDate					= Source.Settlement_Date	
			,PropertyOwned					= Source.Property_Owned	
			,MannerOfResolution				= Source.Manner_Of_Resolution	
			,MethodOfEnforcement			= Source.Method_Of_Enforcement	
			,DateOfSettlementAgreed			= Source.Date_Settlement_Agreed	
			,SettlementAmountAgreed			= Source.Settlement_Amount_Agreed	
			,AmountRecoveredToDate			= Source.Amount_Recovered_To_Date	
			,AmountStillOutstanding			= Source.Amount_Still_Outstanding	
			,WasEnforcementSuccessful		= Source.Was_Enforcement_Successful	
			,CreatedDate					= Source.CreatedDate	
			,CreatedBy						= Source.Createdby	
			,ModifiedDate					= Source.ModifiedDate	
			,ModifiedBy						= Source.Modifiedby	
			,Source							= Source.Source
			,SourceDescription				= Source.SourceDescription	
			,SourceReference				= Source.SourceReference	
			,FiveGrading					= Source.FiveGrading
			,RecordStatus					= Source.RecordStatus
			,ADARecordStatus		= CASE WHEN Source.RecordStatus = 254 THEN 4 ELSE CASE WHEN [Target].ADARecordStatus < 10 THEN [Target].ADARecordStatus + 10 ELSE [Target].ADARecordStatus END END
		WHEN NOT MATCHED THEN INSERT
			(
			 Organisation_Id
			,Incident_Id
			,OutcomeLinkId
			,EnforceMentRole
			,SettlementDate
			,PropertyOwned
			,MannerOfResolution
			,MethodOfEnforcement
			,DateOfSettlementAgreed
			,SettlementAmountAgreed
			,AmountRecoveredToDate
			,AmountStillOutstanding
			,WasEnforcementSuccessful
			,FiveGrading
			,LinkConfidence
			,Source
			,SourceReference
			,SourceDescription
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,RiskClaim_Id
			,BaseRiskClaim_Id
			,IBaseId
			,RecordStatus
			,ADARecordStatus
			)
			VALUES	
			(
			 Source.Organisation_Id
			,Source.Incident_Id
			,Source.OutcomeLinkId
			,Source.EnforceMent_Role
			,Source.Settlement_Date
			,Source.Property_Owned
			,Source.Manner_Of_Resolution
			,Source.Method_Of_Enforcement
			,Source.Date_Settlement_Agreed
			,Source.Settlement_Amount_Agreed
			,Source.Amount_Recovered_To_Date
			,Source.Amount_Still_Outstanding
			,Source.Was_Enforcement_Successful
			,Source.FiveGrading
			,Source.LinkConfidence
			,Source.Source
			,Source.SourceReference
			,Source.SourceDescription
			,Source.CreatedBy
			,Source.CreatedDate
			,Source.ModifiedBy
			,Source.ModifiedDate
			,ISNULL(Source.RiskClaim_Id,0)
			,ISNULL(Source.BaseRiskClaim_Id,0)
			,Source.IBaseId
			,Source.RecordStatus
			,10
			)
		OUTPUT 
			@OutputTaskID
			,$ACTION
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.OutcomeLinkId,'') + '}|I:{' + ISNULL(Inserted.OutcomeLinkId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SettlementDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SettlementDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.MannerOfResolution,'') + '}|I:{' + ISNULL(Inserted.MannerOfResolution,'') + '}'
				,'D:{' + ISNULL(Deleted.EnforcementRole,'') + '}|I:{' + ISNULL(Inserted.EnforcementRole,'') + '}'
				,'D:{' + ISNULL(Deleted.PropertyOwned,'') + '}|I:{' + ISNULL(Inserted.PropertyOwned,'') + '}'
				,'D:{' + ISNULL(Deleted.MethodOfEnforcement,'') + '}|I:{' + ISNULL(Inserted.MethodOfEnforcement,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.DateOfSettlementAgreed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.DateOfSettlementAgreed,''),126) + '}'
				,'D:{' + ISNULL(Deleted.SettlementAmountAgreed,'') + '}|I:{' + ISNULL(Inserted.SettlementAmountAgreed,'') + '}'
				,'D:{' + ISNULL(Deleted.AmountRecoveredToDate,'') + '}|I:{' + ISNULL(Inserted.AmountRecoveredToDate,'') + '}'
				,'D:{' + ISNULL(Deleted.AmountStillOutstanding,'') + '}|I:{' + ISNULL(Inserted.AmountStillOutstanding,'') + '}'
				,'D:{' + ISNULL(Deleted.WasEnforcementSuccessful,'') + '}|I:{' + ISNULL(Inserted.WasEnforcementSuccessful,'') + '}'
				,'D:{' + ISNULL(Deleted.TypeOfSuccess,'') + '}|I:{' + ISNULL(Inserted.TypeOfSuccess,'') + '}'
				,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
				,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
				,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
				,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
				,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
				,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		 INTO [SyncDML].[MDA_Incident2OrganisationOutcome]
           ([TaskID]
           ,[DMLAction]
           ,[Id]
           ,[Incident_Id]
           ,[Organisation_Id]
           ,[OutcomeLinkId]
           ,[SettlementDate]
           ,[MannerOfResolution]
           ,[EnforcementRole]
           ,[PropertyOwned]
           ,[MethodOfEnforcement]
           ,[DateOfSettlementAgreed]
           ,[SettlementAmountAgreed]
           ,[AmountRecoveredToDate]
           ,[AmountStillOutstanding]
           ,[WasEnforcementSuccessful]
           ,[TypeOfSuccess]
           ,[FiveGrading]
           ,[LinkConfidence]
           ,[Source]
           ,[SourceReference]
           ,[SourceDescription]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[RiskClaim_Id]
           ,[BaseRiskClaim_Id]
           ,[IBaseId]
           ,[RecordStatus]
           ,[ADARecordStatus]
           ) ;

		--4.2)DoDeletes:WeCannotDothisInMDADueToReferentialIntegrity:InsteadUpdateTheRecordStatusAsDeleted
		/*UnfortuatelyThisNeedsToBeDoneforEachTable*/
		--Person2BankAccount
		SET @Step ='4.2';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [$(MDA)].[dbo].Incident2PersonOutcome
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	
			@OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Person_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Person_Id,'')) + '}'
			,'D:{' + ISNULL(Deleted.OutcomeLinkId,'') + '}|I:{' + ISNULL(Inserted.OutcomeLinkId,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SettlementDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SettlementDate,''),126) + '}'
			,'D:{' + ISNULL(Deleted.MannerOfResolution,'') + '}|I:{' + ISNULL(Inserted.MannerOfResolution,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.OutcomeCategory_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.OutcomeCategory_Id,'')) + '}'
			,'D:{' + ISNULL(Deleted.EnforcementRole,'') + '}|I:{' + ISNULL(Inserted.EnforcementRole,'') + '}'
			,'D:{' + ISNULL(Deleted.PropertyOwned,'') + '}|I:{' + ISNULL(Inserted.PropertyOwned,'') + '}'
			,'D:{' + ISNULL(Deleted.MethodOfEnforcement,'') + '}|I:{' + ISNULL(Inserted.MethodOfEnforcement,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.DateOfSettlementAgreed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.DateOfSettlementAgreed,''),126) + '}'
			,'D:{' + ISNULL(Deleted.SettlementAmountAgreed,'') + '}|I:{' + ISNULL(Inserted.SettlementAmountAgreed,'') + '}'
			,'D:{' + ISNULL(Deleted.AmountRecoveredToDate,'') + '}|I:{' + ISNULL(Inserted.AmountRecoveredToDate,'') + '}'
			,'D:{' + ISNULL(Deleted.AmountStillOutstanding,'') + '}|I:{' + ISNULL(Inserted.AmountStillOutstanding,'') + '}'
			,'D:{' + ISNULL(Deleted.WasEnforcementSuccessful,'') + '}|I:{' + ISNULL(Inserted.WasEnforcementSuccessful,'') + '}'
			,'D:{' + ISNULL(Deleted.TypeOfSuccess,'') + '}|I:{' + ISNULL(Inserted.TypeOfSuccess,'') + '}'
			,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
			,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
			,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
			,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
			,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
			,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
			,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		INTO [SyncDML].[MDA_Incident2PersonOutcome]
           ([TaskID]
           ,[DMLAction]
           ,[Id]
           ,[Incident_Id]
           ,[Person_Id]
           ,[OutcomeLinkId]
           ,[SettlementDate]
           ,[MannerOfResolution]
           ,[OutcomeCategory_Id]
           ,[EnforcementRole]
           ,[PropertyOwned]
           ,[MethodOfEnforcement]
           ,[DateOfSettlementAgreed]
           ,[SettlementAmountAgreed]
           ,[AmountRecoveredToDate]
           ,[AmountStillOutstanding]
           ,[WasEnforcementSuccessful]
           ,[TypeOfSuccess]
           ,[FiveGrading]
           ,[LinkConfidence]
           ,[Source]
           ,[SourceReference]
           ,[SourceDescription]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[RiskClaim_Id]
           ,[BaseRiskClaim_Id]
           ,[IBaseId]
           ,[RecordStatus]
           ,[ADARecordStatus]
         )
		FROM [$(MDA)].[dbo].Incident2PersonOutcome P2W
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = P2W.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [$(MDA)].[dbo].Incident2OrganisationOutcome
		SET  RecordStatus			= 254
			,ADARecordStatus		= 4
		OUTPUT	
			@OutputTaskID
			,'UPDATE DELETE'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Id,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Incident_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Incident_Id,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Organisation_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Organisation_Id,'')) + '}'
			,'D:{' + ISNULL(Deleted.OutcomeLinkId,'') + '}|I:{' + ISNULL(Inserted.OutcomeLinkId,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.SettlementDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.SettlementDate,''),126) + '}'
			,'D:{' + ISNULL(Deleted.MannerOfResolution,'') + '}|I:{' + ISNULL(Inserted.MannerOfResolution,'') + '}'
			,'D:{' + ISNULL(Deleted.EnforcementRole,'') + '}|I:{' + ISNULL(Inserted.EnforcementRole,'') + '}'
			,'D:{' + ISNULL(Deleted.PropertyOwned,'') + '}|I:{' + ISNULL(Inserted.PropertyOwned,'') + '}'
			,'D:{' + ISNULL(Deleted.MethodOfEnforcement,'') + '}|I:{' + ISNULL(Inserted.MethodOfEnforcement,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.DateOfSettlementAgreed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.DateOfSettlementAgreed,''),126) + '}'
			,'D:{' + ISNULL(Deleted.SettlementAmountAgreed,'') + '}|I:{' + ISNULL(Inserted.SettlementAmountAgreed,'') + '}'
			,'D:{' + ISNULL(Deleted.AmountRecoveredToDate,'') + '}|I:{' + ISNULL(Inserted.AmountRecoveredToDate,'') + '}'
			,'D:{' + ISNULL(Deleted.AmountStillOutstanding,'') + '}|I:{' + ISNULL(Inserted.AmountStillOutstanding,'') + '}'
			,'D:{' + ISNULL(Deleted.WasEnforcementSuccessful,'') + '}|I:{' + ISNULL(Inserted.WasEnforcementSuccessful,'') + '}'
			,'D:{' + ISNULL(Deleted.TypeOfSuccess,'') + '}|I:{' + ISNULL(Inserted.TypeOfSuccess,'') + '}'
			,'D:{' + ISNULL(Deleted.FiveGrading,'') + '}|I:{' + ISNULL(Inserted.FiveGrading,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.LinkConfidence,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.LinkConfidence,'')) + '}'
			,'D:{' + ISNULL(Deleted.Source,'') + '}|I:{' + ISNULL(Inserted.Source,'') + '}'
			,'D:{' + ISNULL(Deleted.SourceReference,'') + '}|I:{' + ISNULL(Inserted.SourceReference,'') + '}'
			,'D:{' + ISNULL(Deleted.SourceDescription,'') + '}|I:{' + ISNULL(Inserted.SourceDescription,'') + '}'
			,'D:{' + ISNULL(Deleted.CreatedBy,'') + '}|I:{' + ISNULL(Inserted.CreatedBy,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.CreatedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.CreatedDate,''),126) + '}'
			,'D:{' + ISNULL(Deleted.ModifiedBy,'') + '}|I:{' + ISNULL(Inserted.ModifiedBy,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ModifiedDate,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ModifiedDate,''),126) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RiskClaim_Id,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.BaseRiskClaim_Id,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.BaseRiskClaim_Id,'')) + '}'
			,'D:{' + ISNULL(Deleted.IBaseId,'') + '}|I:{' + ISNULL(Inserted.IBaseId,'') + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.RecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.RecordStatus,'')) + '}'
			,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.ADARecordStatus,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.ADARecordStatus,'')) + '}'
		 INTO [SyncDML].[MDA_Incident2OrganisationOutcome]
           ([TaskID]
           ,[DMLAction]
           ,[Id]
           ,[Incident_Id]
           ,[Organisation_Id]
           ,[OutcomeLinkId]
           ,[SettlementDate]
           ,[MannerOfResolution]
           ,[EnforcementRole]
           ,[PropertyOwned]
           ,[MethodOfEnforcement]
           ,[DateOfSettlementAgreed]
           ,[SettlementAmountAgreed]
           ,[AmountRecoveredToDate]
           ,[AmountStillOutstanding]
           ,[WasEnforcementSuccessful]
           ,[TypeOfSuccess]
           ,[FiveGrading]
           ,[LinkConfidence]
           ,[Source]
           ,[SourceReference]
           ,[SourceDescription]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[RiskClaim_Id]
           ,[BaseRiskClaim_Id]
           ,[IBaseId]
           ,[RecordStatus]
           ,[ADARecordStatus]
           ) 
		FROM [$(MDA)].[dbo].Incident2OrganisationOutcome O2A
		INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = O2A.IBaseId
		WHERE CT.DMLAction = 'D'
		AND CT.DoNotReplicate IS NULL
		AND CT.PhysicalTableName != 'Link_End';

		--4.3)NowWritetheUniqueIDInserteditoMDAIBaseBackToMDA
		WITH CHANGE_TRACKING_CONTEXT(@Sys_Change_Context)
		UPDATE [$(MDA_IBase)].[dbo].[Outcome_Link]
		SET  [MDA_Incident_ID_412284502] = IQ.MDA_Incident_ID_412284502
			,[Risk_Claim_ID_414244883]	 = IQ.RiskClaim_Id
		OUTPUT	 @OutputTaskID
				,'UPDATE'
				,'D:{' + ISNULL(Deleted.Unique_ID,'') + '}|I:{' + ISNULL(Inserted.Unique_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.AltEntity,'') + '}|I:{' + ISNULL(Inserted.AltEntity,'') + '}'
				,'D:{' + ISNULL(Deleted.Amount_Recovered_to_Date,'') + '}|I:{' + ISNULL(Inserted.Amount_Recovered_to_Date,'') + '}'
				,'D:{' + ISNULL(Deleted.Amount_Still_Outstanding,'') + '}|I:{' + ISNULL(Inserted.Amount_Still_Outstanding,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Create_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Create_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Create_User,'') + '}|I:{' + ISNULL(Inserted.Create_User,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Date_Settlement_Agreed,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Date_Settlement_Agreed,''),126) + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Do_Not_Disseminate_412284494,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Do_Not_Disseminate_412284494,'')) + '}'
				,'D:{' + ISNULL(Deleted.Enforcement_Role,'') + '}|I:{' + ISNULL(Inserted.Enforcement_Role,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.IconColour,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.IconColour,'')) + '}'
				,'D:{' + ISNULL(Deleted.Key_Attractor_412284410,'') + '}|I:{' + ISNULL(Inserted.Key_Attractor_412284410,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Last_Upd_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Last_Upd_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Last_Upd_User,'') + '}|I:{' + ISNULL(Inserted.Last_Upd_User,'') + '}'
				,'D:{' + ISNULL(Deleted.Manner_of_Resolution,'') + '}|I:{' + ISNULL(Inserted.Manner_of_Resolution,'') + '}'
				,'D:{' + ISNULL(Deleted.MDA_Incident_ID_412284502,'') + '}|I:{' + ISNULL(Inserted.MDA_Incident_ID_412284502,'') + '}'
				,'D:{' + ISNULL(Deleted.Method_of_Enforcement,'') + '}|I:{' + ISNULL(Inserted.Method_of_Enforcement,'') + '}'
				,'D:{' + ISNULL(Deleted.Outcome_Link_ID,'') + '}|I:{' + ISNULL(Inserted.Outcome_Link_ID,'') + '}'
				,'D:{' + ISNULL(Deleted.Property_Owned,'') + '}|I:{' + ISNULL(Inserted.Property_Owned,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Record_Status,'')) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Record_Status,'')) + '}'
				,'D:{' + ISNULL(Deleted.SCC,'') + '}|I:{' + ISNULL(Inserted.SCC,'') + '}'
				,'D:{' + ISNULL(Deleted.Settlement_Amount_Agreed,'') + '}|I:{' + ISNULL(Inserted.Settlement_Amount_Agreed,'') + '}'
				,'D:{' + CONVERT(VARCHAR,ISNULL(Deleted.Settlement_Date,''),126) + '}|I:{' + CONVERT(VARCHAR,ISNULL(Inserted.Settlement_Date,''),126) + '}'
				,'D:{' + ISNULL(Deleted.Source_411765484,'') + '}|I:{' + ISNULL(Inserted.Source_411765484,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Description_411765489,'') + '}|I:{' + ISNULL(Inserted.Source_Description_411765489,'') + '}'
				,'D:{' + ISNULL(Deleted.Source_Reference_411765487,'') + '}|I:{' + ISNULL(Inserted.Source_Reference_411765487,'') + '}'
				,'D:{' + ISNULL(Deleted.Status_Binding,'') + '}|I:{' + ISNULL(Inserted.Status_Binding,'') + '}'
				,'D:{' + ISNULL(Deleted.Type_of_Success,'') + '}|I:{' + ISNULL(Inserted.Type_of_Success,'') + '}'
				,'D:{' + ISNULL(Deleted.Was_Enforcement_Successful,'') + '}|I:{' + ISNULL(Inserted.Was_Enforcement_Successful,'') + '}'
				,'D:{' + ISNULL(Deleted.x5x5x5_Grading_412284402,'') + '}|I:{' + ISNULL(Inserted.x5x5x5_Grading_412284402,'') + '}'
				,'D:{' + ISNULL(Deleted.Risk_Claim_ID_414244883,'') + '}|I:{' + ISNULL(Inserted.Risk_Claim_ID_414244883,'') + '}'
		 INTO [SyncDML].[MDAIbase_Outcome_Link]
           ([TaskID]
           ,[DMLAction]
           ,[Unique_ID]
           ,[AltEntity]
           ,[Amount_Recovered_to_Date]
           ,[Amount_Still_Outstanding]
           ,[Create_Date]
           ,[Create_User]
           ,[Date_Settlement_Agreed]
           ,[Do_Not_Disseminate_412284494]
           ,[Enforcement_Role]
           ,[IconColour]
           ,[Key_Attractor_412284410]
           ,[Last_Upd_Date]
           ,[Last_Upd_User]
           ,[Manner_of_Resolution]
           ,[MDA_Incident_ID_412284502]
           ,[Method_of_Enforcement]
           ,[Outcome_Link_ID]
           ,[Property_Owned]
           ,[Record_Status]
           ,[SCC]
           ,[Settlement_Amount_Agreed]
           ,[Settlement_Date]
           ,[Source_411765484]
           ,[Source_Description_411765489]
           ,[Source_Reference_411765487]
           ,[Status_Binding]
           ,[Type_of_Success]
           ,[Was_Enforcement_Successful]
           ,[x5x5x5_Grading_412284402]
           ,[Risk_Claim_ID_414244883])
		FROM [$(MDA_IBase)].[dbo].[Outcome_Link] IAL
		INNER JOIN (
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MP2A.ID,'MDA.dbo.Incident2PersonOutcome') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [$(MDA)].[dbo].Incident2PersonOutcome MP2A
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MP2A.IBaseId
					UNION ALL
					SELECT ID, IBaseId [Unique_ID], [Sync].[fn_ResolveMDAID] (MO2A.ID,'MDA.dbo.Incident2OrganisationOutcome') MDA_Incident_ID_412284502, RiskClaim_Id
					FROM [$(MDA)].[dbo].[Incident2OrganisationOutcome] MO2A
					INNER JOIN #CT CT ON CT.MDA_IBase_Unique_ID = MO2A.IBaseId
					) IQ ON IQ.[Unique_ID] =  IAL.[Unique_ID]
		WHERE IAL.MDA_Incident_ID_412284502 IS NULL
		OR IAL.MDA_Incident_ID_412284502 != IQ.MDA_Incident_ID_412284502;
	


	END

	--------------------------------------------------------------------------
	--5)UpdateLoggingAndTidy
	--------------------------------------------------------------------------
	SELECT @InsertRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2PersonOutcome] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2PersonOutcome] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2PersonOutcome] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2OrganisationOutcome] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2OrganisationOutcome] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDA_Incident2OrganisationOutcome] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Outcome_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase_Outcome_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIbase_Outcome_Link] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')

	SELECT @InsertRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Insert')
	SELECT @UpdateRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] IN ('Update','Update Delete'))
	SELECT @DeleteRowCount	+= (SELECT COUNT(1) FROM [SyncDML].[MDAIBase__LinkEnd] WHERE TaskID = @OutputTaskID AND [DMLAction] = 'Delete')
		
	SELECT   @Now	= GETDATE()
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action		= 'U'
				,@TaskID		= @OutputTaskID
				,@ProcessID		= @ProcessID
				,@TaskEndTime	= @Now
				,@InsertRowCount= @InsertRowCount
				,@UpdateRowCount= @UpdateRowCount
				,@DeleteRowCount= @DeleteRowCount
				,@OutputTaskID	= @OutputTaskID OUTPUT

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging


END TRY
BEGIN CATCH
	DECLARE	 @ErrorMessage		NVARCHAR(4000)	= ERROR_MESSAGE()
			,@ErrorSeverity		INT				= ERROR_SEVERITY()
			,@ErrorState		INT				= ERROR_STATE()
			,@ErrorProcedure	NVARCHAR(500)	= ISNULL(ERROR_PROCEDURE(),@@PROCID)
			,@ErrorLine			INT				= ERROR_LINE()

	SELECT 	@ErrorMessage = 'Procedure=['+@ErrorProcedure+'];Line=['+CAST(@ErrorLine AS VARCHAR)+'];Message=['+@ErrorMessage+'];Step=[' +ISNULL(@Step,'?') +']'
	
	EXECUTE dbo.uspCRUDDBControlTask
				 @Action			= 'U'
				,@TaskID			= @OutputTaskID
				,@TaskEndTime		= @Now
				,@Error				= @ErrorSeverity
				,@ErrorDescription	= @ErrorMessage
				,@OutputTaskID		= @OutputTaskID OUTPUT

	IF XACT_STATE() != 0 AND @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF @@TRANCOUNT > 0 
		ROLLBACK TRANSACTION

	IF OBJECT_ID('tempdb..#_CT') IS NOT NULL
		DROP TABLE #_CT	

	IF OBJECT_ID('tempdb..#LinkEndStaging') IS NOT NULL
		DROP TABLE #LinkEndStaging

	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)




END CATCH

