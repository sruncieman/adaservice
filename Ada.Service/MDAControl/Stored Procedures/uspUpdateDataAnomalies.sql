﻿CREATE PROCEDURE [Sync].[uspUpdateDataAnomalies]
/**************************************************************************************************/
-- ObjectName:		[Sync].[uspUpdateDataAnomalies]	
--
-- Steps:			1)DeclareVariablesTempObjects
--					2)BuildTheSQLStatementsToExecute
--					3)ExecuteTheSQLStatements
--					4)Tidy
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-05-30			Paul Allen		Created
/**************************************************************************************************/
AS
SET NOCOUNT ON

----------------------------------------------------
--1)DeclareVariablesTempObjects
----------------------------------------------------
DECLARE  @SQL					NVARCHAR(MAX)
		,@UpdateStatementCount	INT
		,@Counter				INT		=1

IF OBJECT_ID('tempdb..#RiskWordFilter') IS NOT NULL
	DROP TABLE #RiskWordFilter	

IF OBJECT_ID('tempdb..#UpdateStatement') IS NOT NULL
	DROP TABLE #UpdateStatement
CREATE TABLE #UpdateStatement (ID INT IDENTITY(1,1), SQLStatement VARCHAR(MAX))

----------------------------------------------------
--2)BuildTheSQLStatementsToExecute
----------------------------------------------------
SELECT Id, TableName, FieldName, LookupWord, ReplacementWord, ReplaceWholeString, SearchType
INTO #RiskWordFilter
FROM [MDA].[dbo].[RiskWordFilter]
WHERE RiskClient_Id = 0

DELETE #RiskWordFilter WHERE (TableName = 'Vehicle' AND FieldName = 'DamageDescription') OR (TableName = 'Claim' AND FieldName = 'IncidentDate')

UPDATE #RiskWordFilter
SET	TableName =  CASE
					WHEN TableName = 'Claim' THEN 'Incident'
					WHEN TableName = 'ClaimInfo' THEN 'Incident2Person'
					WHEN TableName IN ('Person','Organisation') AND FieldName = 'Telephone' THEN 'Telephone'
					WHEN TableName = 'Person' AND FieldName = 'DriverNumber' THEN 'DrivingLicense'
					WHEN TableName = 'Person' AND FieldName = 'PassportNumber' THEN 'Passport'
					ELSE TableName
				 END
	,FieldName = CASE
					WHEN FieldName = 'Telephone' THEN 'TelephoneNumber'
					WHEN FieldName = 'Make' THEN 'VehicleMake'
					WHEN FieldName = 'IMEI' THEN 'HandsetIMEI'
					WHEN FieldName = 'Telephone Number' THEN 'TelephoneNumber'
					ELSE FieldName
				 END
	,ReplacementWord = CASE
							WHEN TableName = 'BankAccount' AND FieldName = 'AccountNumber' AND LookupWord = 'Unknown' THEN ''
							ELSE ReplacementWord
						END

INSERT INTO #RiskWordFilter (TableName, FieldName, LookupWord, ReplacementWord, ReplaceWholeString, SearchType)
SELECT 'Incident2Organisation', FieldName, LookupWord, ReplacementWord, ReplaceWholeString, SearchType
FROM #RiskWordFilter
WHERE TableName = 'Incident2Person'

INSERT INTO #UpdateStatement (SQLStatement)
SELECT	'UPDATE [MDA].[dbo].' + TableName + ' 
		 SET ' + FieldName + ' =  ' + CASE 
										WHEN ReplaceWholeString = 1 THEN ISNULL( + '''' + ReplacementWord + '''','NULL')
										WHEN ReplaceWholeString = 0  THEN 'REPLACE(' + FieldName + ',''' + REPLACE(LookupWord,'''','''''') + ''',''' + ISNULL(ReplacementWord,'') + ''')' END + '
		 WHERE (' + FieldName + ' ' +	CASE 
											WHEN SearchType = '~' THEN 'LIKE ''%' + REPLACE(LookupWord,'''','''''') + '%'')' 
											WHEN SearchType = '=' THEN '= ''' + REPLACE(LookupWord,'''','''''') + ''')' 
											WHEN SearchType = '>' THEN 'LIKE ''' + REPLACE(LookupWord,'''','''''') + '%'')' 
											WHEN SearchType = '<' THEN 'LIKE ''%' + REPLACE(LookupWord,'''','''''') + '%'')' 
											WHEN SearchType = '<>' THEN 'LIKE ''' + REPLACE(LookupWord,'''','''''') + '%'' OR ' + FieldName + ' LIKE ''%' + REPLACE(LookupWord,'''','''''') + ''')'
										END +
		' AND [Source] = ''Keoghs CFS'''
FROM #RiskWordFilter
SELECT @UpdateStatementCount = @@ROWCOUNT

----------------------------------------------------
--3)ExecuteTheSQLStatements
----------------------------------------------------
WHILE @Counter <= @UpdateStatementCount
BEGIN
	SELECT @SQL = (SELECT SQLStatement FROM #UpdateStatement WHERE ID = @Counter)
	EXECUTE SP_EXECUTESQL @SQL
	SELECT @Counter +=1
END

----------------------------------------------------
--4)Tidy
----------------------------------------------------
IF OBJECT_ID('tempdb..#RiskWordFilter') IS NOT NULL
	DROP TABLE #RiskWordFilter	

IF OBJECT_ID('tempdb..#UpdateStatement') IS NOT NULL
	DROP TABLE #UpdateStatement