﻿

CREATE PROCEDURE [IBaseM].[uspMigrationStep06]
/**************************************************************************************************/
-- ObjectName:			MDAControl.IBaseM.uspMigrationStep06
--
-- Description:			Populate the Incident Link tables within IBase8 Target database
--
-- Steps:				
--
-- Dependencies:		
--
-- Usage:				Called within SP IBaseM.uspMigrationMaster
--
-- RevisionHistory:
--		Date				Author			Modification
--		2014-02-27			Paul Allen		Created
/**************************************************************************************************/
(
 @Logging				BIT = 0			--DefaultIsOff
,@Debug					BIT = 0			--DefaultIsOff
,@ParentTaskID			INT = NULL	
)
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
BEGIN TRY
	------------------------------------------------------------
	--1)ConfigureLogging,Create/DeclareRequiredObjects
	------------------------------------------------------------
	--1.1)ConfigureLogging
	IF @Logging = 1
	BEGIN
		DECLARE  @Now				DATETIME 
				,@Details			VARCHAR(1000)
				,@ProcessID			INT = (SELECT ProcessID FROM dbo.DBControlTask WHERE TaskID = @ParentTaskID)
				,@OutputTaskID		INT
				,@DatabaseID		INT = DB_ID()
				,@ObjectID			INT = ISNULL(@@PROCID,-1)
					
		SELECT @Details = 'User=['+SYSTEM_USER+']'			
		EXECUTE dbo.uspCRUDDBControlTask
					 @ParentTaskID		= @ParentTaskID
					,@ProcessID			= @ProcessID
					,@TaskNameID		= 13 --Populate_IncidentLinkUpdates
					,@DatabaseID		= @DatabaseID
					,@ObjectID			= @ObjectID
					,@Details			= @Details
					,@OutputTaskID		= @OutputTaskID OUTPUT
	END
			
	------------------------------------------------------------
	--2)MainBody
	------------------------------------------------------------
	UPDATE A 
	SET Record_Status = B.Record_Status
	FROM IBaseM8Cur.Incident_Link A
	INNER JOIN IBaseM5Cur.MIAFTR_Match  B ON A.Incident_Link_ID = B.Unique_ID

	UPDATE A 
	SET Record_Status = B.Record_Status
	FROM IBaseM8Cur.Incident_Link A
	INNER JOIN IBaseM5Cur.Claim_  B ON A.Incident_Link_ID = B.Unique_ID

	UPDATE A 
	SET Record_Status = B.Record_Status
	FROM IBaseM8Cur.Incident_Link A
	INNER JOIN IBaseM5Cur.Report_  B ON A.Incident_Link_ID = B.Unique_ID

	UPDATE A 
	SET Record_Status = B.Record_Status
	FROM IBaseM8Cur.Incident_Link A
	INNER JOIN 
	(
	SELECT
		 A.Unique_ID ,
		 CASE WHEN A.Record_Status = 254 THEN 254 ELSE
		 CASE WHEN B.Record_Status = 254 THEN 254 ELSE
		 CASE WHEN C.Record_Status = 254 THEN 254 ELSE 0 
		 END END END AS Record_Status
	FROM IBaseM5Cur.Nominal_Link  A
	INNER JOIN IBaseM5Cur._LinkEnd B ON A.Unique_ID = B.Link_ID
	INNER JOIN IBaseM5Cur.ClaimFile_ C ON B.Entity_ID1 = C.Unique_ID
	)  B ON A.Incident_Link_ID = B.Unique_ID

	UPDATE A 
	SET Record_Status = B.Record_Status
	FROM IBaseM8Cur.Incident_Link A
	INNER JOIN 
	(
	SELECT
		A.Unique_ID ,
		 CASE WHEN A.Record_Status = 254 THEN 254 ELSE
		 CASE WHEN B.Record_Status = 254 THEN 254 ELSE
		 CASE WHEN C.Record_Status = 254 THEN 254 ELSE 0 
		 END END END AS Record_Status
	FROM IBaseM5Cur.Claim_  A
	INNER JOIN IBaseM5Cur._LinkEnd B ON A.Unique_ID = B.Link_ID
	INNER JOIN IBaseM5Cur.ClaimFile_ C ON B.Entity_ID1 = C.Unique_ID
	)  B ON A.Incident_Link_ID = B.Unique_ID

	UPDATE A 
	SET Record_Status = B.Record_Status
	FROM IBaseM8Cur.Incident_Link A
	INNER JOIN 
	(SELECT
		A.Unique_ID , 
		 CASE WHEN A.Record_Status = 254 THEN 254 ELSE
		 CASE WHEN B.Record_Status = 254 THEN 254 ELSE
		 CASE WHEN C.Record_Status = 254 THEN 254 ELSE 0 
		 END END END AS Record_Status
	FROM IBaseM5Cur.Insured_Claim_Link  A
	INNER JOIN IBaseM5Cur._LinkEnd B ON A.Unique_ID = B.Link_ID
	INNER JOIN IBaseM5Cur.ClaimFile_ C ON B.Entity_ID1 = C.Unique_ID
	)  B ON A.Incident_Link_ID = B.Unique_ID

	IF OBJECT_ID('tempdb..#_ILT') is not null
		DROP TABLE #_ILT		
		
	SELECT  MIN(IL.Unique_ID) AS Unique_ID, IL.Accident_Management, IL.AltEntity, IL.Ambulance_Attended, IL.Broker_, IL.Claim_Code, IL.Claim_Notification_Date, 
						  IL.Claim_Number, IL.Claim_Status, IL.Claim_Type, IL.Create_Date, IL.Create_User, IL.Do_Not_Disseminate_412284494, IL.Engineer_, IL.Hire_, IL.IconColour, 
						  IL.Incident_Circs, IL.Incident_Link_ID, MIN(Incident_Location) AS Incident_Location,
						  IL.Incident_Time, IL.Insurer_, IL.Key_Attractor_412284410, IL.Last_Upd_Date, IL.Last_Upd_User, 
						  IL.MDA_Incident_ID_412284502, IL.Medical_Examiner, IL.MoJ_Status, IL.Party_Type, IL.Payments_, IL.Police_Attended, IL.Police_Force, IL.Police_Reference, 
						  IL.Record_Status, IL.Recovery_, IL.Referral_Source, IL.Repairer_, IL.Reserve_, IL.SCC, IL.Solicitors_, IL.Source_411765484, IL.Source_Description_411765489, 
						  IL.Source_Reference_411765487, IL.Status_Binding, IL.Storage_, IL.Storage_Address, IL.Sub_Party_Type, IL.x5x5x5_Grading_412284402, LE.Entity_ID1, 
						  LE.Confidence, LE.Direction, LE.Entity_ID2, LE.EntityType_ID1, LE.EntityType_ID2, LE.LinkType_ID, LE.Record_Status AS LE_Record_Status, LE.Record_Type, 
						  LE.SCC AS LE_SCC
	INTO #_ILT                       
	FROM IBaseM8Cur.Incident_Link AS IL 
	INNER JOIN IBaseM8Cur._LinkEnd AS LE ON IL.Unique_ID = LE.Link_Id
	WHERE LEFT(IL.Unique_ID, 3 ) = 'IN1' AND  ISNULL(CONVERT(INT, IL.Record_Status), 0) + ISNULL(CONVERT(INT, LE.Record_Status), 0) = 0                
	GROUP BY  IL.Accident_Management, IL.AltEntity, IL.Ambulance_Attended, IL.Broker_, IL.Claim_Code, IL.Claim_Notification_Date, IL.Claim_Number, IL.Claim_Status, 
			  IL.Claim_Type, IL.Create_Date, IL.Create_User, IL.Do_Not_Disseminate_412284494, IL.Engineer_, IL.Hire_, IL.IconColour, IL.Incident_Circs, IL.Incident_Link_ID, 
			  IL.Incident_Time, IL.Insurer_, IL.Key_Attractor_412284410, IL.Last_Upd_Date, IL.Last_Upd_User, IL.MDA_Incident_ID_412284502, 
			  IL.Medical_Examiner, IL.MoJ_Status, IL.Party_Type, IL.Payments_, IL.Police_Attended, IL.Police_Force, IL.Police_Reference, IL.Record_Status, IL.Recovery_, 
			  IL.Referral_Source, IL.Repairer_, IL.Reserve_, IL.SCC, IL.Solicitors_, IL.Source_411765484, IL.Source_Description_411765489, IL.Source_Reference_411765487, 
			  IL.Status_Binding, IL.Storage_, IL.Storage_Address, IL.Sub_Party_Type, IL.x5x5x5_Grading_412284402, LE.Entity_ID1, LE.Confidence, LE.Direction, LE.Entity_ID2, 
			  LE.EntityType_ID1, LE.EntityType_ID2, LE.LinkType_ID, LE.Record_Status, LE.Record_Type, LE.SCC
	          
	DELETE FROM IBaseM8Cur._LinkEnd WHERE  LEFT(Link_ID, 3 ) = 'IN1'
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur.Incident_Link' 

	INSERT INTO IBaseM8Cur.Incident_Link(
	Unique_ID, Accident_Management, AltEntity, Ambulance_Attended, Broker_, Claim_Code, Claim_Notification_Date, 
	Claim_Number, Claim_Status, Claim_Type, Create_Date, Create_User, Do_Not_Disseminate_412284494, Engineer_, Hire_, IconColour, 
	Incident_Circs, Incident_Link_ID, Incident_Location, Incident_Time, Insurer_, Key_Attractor_412284410, Last_Upd_Date, Last_Upd_User, 
	MDA_Incident_ID_412284502, Medical_Examiner, MoJ_Status, Party_Type, Payments_, Police_Attended, Police_Force, Police_Reference, 
	Record_Status, Recovery_, Referral_Source, Repairer_, Reserve_, SCC, Solicitors_, Source_411765484, Source_Description_411765489, 
	Source_Reference_411765487, Status_Binding, Storage_, Storage_Address, Sub_Party_Type, x5x5x5_Grading_412284402)
	SELECT DISTINCT
		IL.Unique_ID, IL.Accident_Management, IL.AltEntity, IL.Ambulance_Attended, IL.Broker_, IL.Claim_Code, IL.Claim_Notification_Date, 
		IL.Claim_Number, IL.Claim_Status, IL.Claim_Type, IL.Create_Date, IL.Create_User, IL.Do_Not_Disseminate_412284494, IL.Engineer_, IL.Hire_, IL.IconColour,              
		IL.Incident_Circs, IL.Incident_Link_ID, IL.Incident_Location, IL.Incident_Time, IL.Insurer_, IL.Key_Attractor_412284410, IL.Last_Upd_Date, IL.Last_Upd_User, 
		IL.MDA_Incident_ID_412284502, IL.Medical_Examiner, IL.MoJ_Status, IL.Party_Type, IL.Payments_, IL.Police_Attended, IL.Police_Force, IL.Police_Reference, 
		IL.Record_Status, IL.Recovery_, IL.Referral_Source, IL.Repairer_, IL.Reserve_, IL.SCC, IL.Solicitors_, IL.Source_411765484, IL.Source_Description_411765489, 
		IBaseM.Fn_GetEliteRefFromDbSource(Source_Reference_411765487) AS Source_Reference_411765487, IL.Status_Binding, IL.Storage_, IL.Storage_Address, IL.Sub_Party_Type, IL.x5x5x5_Grading_412284402
	FROM #_ILT IL

	INSERT INTO IBaseM8Cur._LinkEnd 
	SELECT LE.Unique_ID, LE.Entity_ID1, LE.Confidence, LE.Direction, LE.Entity_ID2, LE.EntityType_ID1, LE.EntityType_ID2, LE.LinkType_ID, LE.Record_Status, 
	LE.Record_Type, LE.SCC
	FROM #_ILT LE

	IF OBJECT_ID('tempdb..#_ILT') is not null
		DROP TABLE #_ILT		

	UPDATE IBaseM8Cur.Person_to_Organisation_Link  
	SET MDA_Incident_ID_412284502 = NULL

	UPDATE POL
	SET Source_Description_411765489 = Person_Organisation_DB_Source_,
		Source_Reference_411765487 = IBaseM.Fn_GetEliteRefFromDbSource(USE_REF),
		MDA_Incident_ID_412284502 = MatchType
	FROM IBaseM8Cur.Person_to_Organisation_Link POL WITH(NOLOCK)
	INNER JOIN IBaseM.vClaVehPerOrg2 CVPO WITH(NOLOCK) ON POL.Person_to_Organisation_Link_ID = CVPO.PO_Link_ID AND Link_Type NOT IN('Employee', 'Former Secretary','Co Secretary', 'Director', 'Former Director') AND  USE_REF IS NOT NULL

	UPDATE IBaseM8Cur.Vehicle_Link
	SET  MDA_Incident_ID_412284502 = NULL

	DECLARE  @CurrentIBase5DB  VARCHAR(50) = (SELECT TOP 1 name FROM sys.databases WHERE patindex('IBase5_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name DESC)
			,@CurrentIBase8DB  VARCHAR(50) = (SELECT TOP 1 name FROM sys.databases WHERE patindex('IBase8_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]', name) > 0 ORDER BY name DESC)
			,@SQL NVARCHAR(MAX)
			
	SET @SQL = '
		USE  '+ @CurrentIBase8DB +'
		
		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''Vehicle_Link'') AND name = N''IX_Vehicle_Link_Vehicle_Link_ID_Unique_ID'')	
		CREATE NONCLUSTERED INDEX [IX_Vehicle_Link_Vehicle_Link_ID_Unique_ID]
		ON dbo.Vehicle_Link (Vehicle_Link_ID)
		INCLUDE (Unique_ID)
		'

	EXEC SP_EXECUTESQL @SQL

	SET @SQL = '
		USE  '+ @CurrentIBase8DB +'
		
		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''Vehicle_Link'') AND name = N''IX_Vehicle_Link_Vehicle_Link_Type_Record_Status'')
		CREATE NONCLUSTERED INDEX [IX_Vehicle_Link_Vehicle_Link_Type_Record_Status]
		ON [dbo].[Vehicle_Link] ([Link_Type],[Record_Status],[Source_Reference_411765487])
		INCLUDE ([Unique_ID])
		'
	EXEC SP_EXECUTESQL @SQL

	SET @SQL = '
		USE  '+ @CurrentIBase8DB +'
		
		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''Vehicle_Link'') AND name = N''IX_Vehicle_Link_201305171101'')
		CREATE NONCLUSTERED INDEX [IX_Vehicle_Link_201305171101]
		ON [dbo].[Vehicle_Link] ([Record_Status],[Source_Reference_411765487],[Link_Type])
		INCLUDE ([Unique_ID])
		'
	EXEC SP_EXECUTESQL @SQL
/*RemovedDefect890
	UPDATE POL
	SET	Source_Description_411765489 = V.Person_Organisation_DB_Source_,
		Source_Reference_411765487 = IBaseM.Fn_GetEliteRefFromDbSource(USE_REF),
		MDA_Incident_ID_412284502 = MatchType
	FROM IBaseM8Cur.Vehicle_Link POL WITH(NOLOCK)
	INNER JOIN IBaseM.vClaVehPerVehOrg V WITH(NOLOCK) ON POL.Vehicle_Link_ID = V.PO_Link_ID AND USE_REF IS NOT NULL
*/
	SET @SQL = '
		USE  '+ @CurrentIBase8DB +'
		
		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''Address_to_Address_Link'') AND name = N''IX_Address_to_Address_Link_Address_to_Address_Link_ID_Unique_ID'')
		CREATE NONCLUSTERED INDEX IX_Address_to_Address_Link_Address_to_Address_Link_ID_Unique_ID
		ON dbo.Address_to_Address_Link (Address_to_Address_Link_ID)
		INCLUDE (Unique_ID)
		'
	EXEC SP_EXECUTESQL @SQL

	SET @SQL = '
		USE  '+ @CurrentIBase8DB +'
		
		IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N''Address_to_Address_Link'') AND name = N''IX_Address_to_Address_Link_Address_to_Address_Link_ID_Address_Link_Type_Record_Status'')
		CREATE NONCLUSTERED INDEX [IX_Address_to_Address_Link_Address_to_Address_Link_ID_Address_Link_Type_Record_Status]
		ON [dbo].[Address_to_Address_Link] ([Address_Link_Type],[Record_Status])
		INCLUDE ([Unique_ID],[Source_Reference_411765487])
		'
	EXEC SP_EXECUTESQL @SQL

	UPDATE POL 
	SET		MDA_Incident_ID_412284502 = MatchType
	FROM IBaseM8Cur.Address_to_Address_Link POL WITH(NOLOCK)
	INNER JOIN IBaseM.vClaVehPerVehLoc V WITH(NOLOCK) ON POL.Address_to_Address_Link_ID =  V.PO_Link_ID AND  USE_REF IS NOT NULL

	--PA:NoRequiredForMigrationAsTheIDsCanChangeWithinTheStagingEnvironment
	--EXEC IBaseM.uspPopulateHeadsOfClaim

	DECLARE @NextID INT

	SELECT @NextID = MAX(CONVERT(INT, STUFF(Unique_ID, 1, 3, ''))) + 1 FROM IBaseM8Cur.Incident_Link
	EXECUTE IBaseM.uspTruncateTableSynonym 'IBaseM8Cur._Incident_Link_NextID'
	INSERT INTO IBaseM8Cur._Incident_Link_NextID(NextID)
	SELECT ISNULL(@NextID, 1)

	UPDATE A 
	SET Expansion = Item
	FROM IBaseM8Cur._Codes A
	INNER JOIN IBaseM.PickLists B
	ON Item1 = Expansion COLLATE DATABASE_DEFAULT

	UPDATE A 
	SET Address_Type = Item
	FROM IBaseM8Cur.Address_ A
	INNER JOIN IBaseM.PickLists B
	ON Item1 = Address_Type COLLATE DATABASE_DEFAULT

	UPDATE A 
	SET AltEntity = Item
	FROM IBaseM8Cur.Payment_Card A
	INNER JOIN IBaseM.PickLists B
	ON Item1 = AltEntity COLLATE DATABASE_DEFAULT

	UPDATE A 
	SET  Policy_Type= Item
	FROM IBaseM8Cur.Policy_ A
	INNER JOIN IBaseM.PickLists B
	ON Item1 = Policy_Type COLLATE DATABASE_DEFAULT

	UPDATE A 
	SET  AltEntity = Item
	FROM IBaseM8Cur.Telephone_ A
	INNER JOIN IBaseM.PickLists B
	ON Item1 = AltEntity COLLATE DATABASE_DEFAULT

	UPDATE A 
	SET  Address_Link_Type = Item
	FROM IBaseM8Cur.Address_to_Address_Link A
	INNER JOIN IBaseM.PickLists B
	ON Item1 = Address_Link_Type COLLATE DATABASE_DEFAULT

	DELETE FROM IBaseM8Cur._Codes
	WHERE CodeGroup_ID = 2317 AND Expansion = 'Mobile Phone'

	SET @SQL = '
		DROP INDEX IX_Vehicle_Link_Vehicle_Link_ID_Unique_ID ON '+ @CurrentIBase8DB +'.dbo.Vehicle_Link WITH ( ONLINE = OFF )
		'
	EXEC SP_EXECUTESQL @SQL

	SET @SQL = '
		 DROP INDEX IX_Vehicle_Link_Vehicle_Link_Type_Record_Status ON  '+ @CurrentIBase8DB +'.dbo.Vehicle_Link WITH ( ONLINE = OFF )
		'
	EXEC SP_EXECUTESQL @SQL

	SET @SQL = '
		DROP INDEX  IX_Vehicle_Link_201305171101 ON  '+ @CurrentIBase8DB +'.dbo.Vehicle_Link WITH ( ONLINE = OFF )
		'
	EXEC SP_EXECUTESQL @SQL

	SET @SQL = '
		DROP INDEX IX_Address_to_Address_Link_Address_to_Address_Link_ID_Unique_ID ON  '+ @CurrentIBase8DB +'.dbo.Address_to_Address_Link WITH ( ONLINE = OFF )
		'
	EXEC SP_EXECUTESQL @SQL

	SET @SQL = '
		DROP INDEX IX_Address_to_Address_Link_Address_to_Address_Link_ID_Address_Link_Type_Record_Status ON  '+ @CurrentIBase8DB +'.dbo.Address_to_Address_Link WITH ( ONLINE = OFF )
		'
	EXEC SP_EXECUTESQL @SQL

	------------------------------------------------------------
	--3)UpdateLogging/Tidy
	------------------------------------------------------------
	IF @Logging = 1
	BEGIN
		SELECT   @Now		= GETDATE()
		EXECUTE dbo.uspCRUDDBControlTask
					 @Action		= 'U'
					,@TaskID		= @OutputTaskID
					,@ProcessID		= @ProcessID
					,@TaskEndTime	= @Now
					--,@InsertRowCount= @InsertRowCount
					,@OutputTaskID	= @OutputTaskID OUTPUT
	END

END TRY
BEGIN CATCH
		DECLARE  @ErrorMessage	NVARCHAR(4000)	= ERROR_MESSAGE()
				,@ErrorSeverity INT				= ERROR_SEVERITY()
				,@ErrorState	INT				= ERROR_STATE()
				
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState)
		
		IF @Logging = 1
		BEGIN
			SELECT   @Now		= GETDATE()				
			EXECUTE dbo.uspCRUDDBControlTask
						 @Action			= 'U'
						,@TaskID			= @OutputTaskID
						,@TaskEndTime		= @Now
						,@Error				= @ErrorSeverity
						,@ErrorDescription	= @ErrorMessage
						,@OutputTaskID		= @OutputTaskID OUTPUT
		END
		
END CATCH



