﻿CREATE procedure IBaseM.uspResetRecordStatus
As
BEGIN

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Account_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Account_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Address_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Address_to_Address_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.BB_Pin_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.BBPin_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Case_to_Incident_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Driving_Licence t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Driving_Licence_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Email_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Email_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Fraud_Ring t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0	
	FROM IBaseM8Cur.Fraud_Ring_Incident_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Incident_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Incident_Link t
	WHERE ISNULL(t.Record_Status,0) =0
	
	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Incident_Match_Link t
	WHERE ISNULL(t.Record_Status,0) =0
	
	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Intelligence_Link t
	WHERE ISNULL(t.Record_Status,0) =0
	
	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.IP_Address t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.IP_Address_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Keoghs_Case t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.NI_Number t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.NI_Number_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Organisation_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Organisation_Match_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Outcome_Link t
	WHERE ISNULL(t.Record_Status,0) =0
	
	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Passport_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Passport_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Payment_Card t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Payment_Card_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Person_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Person_to_Organisation_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Person_to_Person_Link t
	WHERE ISNULL(t.Record_Status,0) =0
	
	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Policy_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Policy_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Policy_Payment_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Telephone_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Telephone_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.TOG_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.TOG_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Vehicle_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Vehicle_Incident_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t	
	SET t.Record_Status =0
	FROM IBaseM8Cur.Vehicle_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Vehicle_to_Vehicle_Link t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Website_ t
	WHERE ISNULL(t.Record_Status,0) =0

	UPDATE t
	SET t.Record_Status =0
	FROM IBaseM8Cur.Website_Link t
	WHERE ISNULL(t.Record_Status,0) =0

END