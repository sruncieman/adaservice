﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using MDA.Common;
using MDA.Common.Server;
using MDA.MappingService.Liberty.Motor;
using MDA.Pipeline.Model;

namespace MDA.Mapping.Liberty
{
    class Program
    {

        static List<IPipelineClaim> motorClaimBatch = new List<IPipelineClaim>();

        private static int ProcessClaimIntoDb(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim, object o)
        {
            motorClaimBatch.Add(claim);

            return 0;
        }

        static void Main(string[] args)
        {
            //string path = @"D:\Dev\Projects\MDA\MDASolution\MDA.Mapping.Liberty\Resource";

            CurrentContext ctx = new CurrentContext(5, 17, "Test");

            //System.IO.FileStream strFile = new System.IO.FileStream(path + @"\LSM155215.xml", System.IO.FileMode.Open, System.IO.FileAccess.Read);

            Console.WriteLine("Start");

            ProcessingResults pr;

            new FileMapping().ConvertToClaimBatch(ctx, null, null, ProcessClaimIntoDb,out pr);

            string xml = WriteToXml();

            //WriteToJson(xml);

            //WriteToXmlOriginal(motorClaimBatch);
        }

        private static string WriteToXml()
        {

            //string strXmlFileFullSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Liberty\Xml\LibertyFull.xml";

            //string xml = null;

            //var ms = new MemoryStream();

            //var ser = new XmlSerializer(typeof(List<MDA.Pipeline.Model.PipelineClaimBatch>));
  
            //ser.Serialize(ms, motorClaimBatch);

            //ms.Position = 0;

            //var sr = new StreamReader(ms);

            //xml = sr.ReadToEnd();

            //TextWriter WriteFileStream = new StreamWriter(strXmlFileFullSave);
            //ser.Serialize(WriteFileStream, motorClaimBatch);

            //WriteFileStream.Close();
            //return xml;



            string strXmlFileFullSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Liberty\Xml\";

            string xml = null;

            foreach (var item in motorClaimBatch)
            {

                var ms = new MemoryStream();

                var ser = new XmlSerializer(typeof(MDA.Pipeline.Model.PipelineMotorClaim));

                ser.Serialize(ms, item);

                ms.Position = 0;

                var sr = new StreamReader(ms);

                xml = sr.ReadToEnd();

                TextWriter WriteFileStream = new StreamWriter(strXmlFileFullSave + item.ClaimNumber + ".xml");
                ser.Serialize(WriteFileStream, item);

                WriteFileStream.Close();

            }

            return xml;
        }
    }
}
