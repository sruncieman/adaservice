using System;
using System.Configuration;
using System.Reflection;
using System.Xml.Serialization;


namespace AdminConsole
{
    /// <summary>
    /// This class is used by the Configuration system to read the Administration Console Configuration
    /// data from the app.config file.
    /// 
    /// Requires
    ///   <section name="adminConsoleConfigurationSection" type="YorkshireWater.AF.Common.AdministrationConsoleConfiguration, YorkshireWater.AF.Common, Version=2.0.0.0, Culture=neutral, PublicKeyToken=e9b31bf34182bd8b" />
    ///
    ///     <adminConsoleConfigurationSection>
	///	        <administrationControls>
	///		        <administrationControl name="fred" access="Console: Batch Parameters,Console: View Batch Parameters" treeNode="Batch/Parameters"
	///	    		    description="Maintain Dynamic Batch Parameters" type="YorkshireWater.Billing.Administration.Controls.BatchParametersControl, YorkshireWater.Billing.Administration.Controls, Version=2.0.0.0, Culture=neutral, PublicKeyToken=e9b31bf34182bd8b" />
	/// 	    </administrationControls>
	///     </adminConsoleConfigurationSection>
    /// </summary>
    public class AdministrationConsoleConfiguration : ConfigurationSection
    {
        private static ConfigurationProperty _controls;
        private static ConfigurationPropertyCollection _properties;

        static AdministrationConsoleConfiguration()
        {
            _properties = new ConfigurationPropertyCollection();

            _controls = new ConfigurationProperty("administrationControls",
                typeof(AdministrationControlsCollection), null, ConfigurationPropertyOptions.IsRequired);

            _properties.Add(_controls);
        }

        /// <summary>
        /// 
        /// </summary>
        [ConfigurationProperty("administrationControls")]
        public AdministrationControlsCollection AdministrationControls
        {
            get { return (AdministrationControlsCollection)base[_controls]; }
        }
    }

    #region AdministrationControlsCollection
    /// <summary>
    /// 
    /// </summary>
    [ConfigurationCollection(typeof(AdministrationControl), AddItemName = "administrationControl", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class AdministrationControlsCollection : ConfigurationElementCollection
    {
        private static ConfigurationPropertyCollection _properties;

        static AdministrationControlsCollection()
        {
            _properties = new ConfigurationPropertyCollection();

            _needReferenceData = new ConfigurationProperty("needReferenceData", typeof(bool), false, ConfigurationPropertyOptions.IsRequired);
            _properties.Add(_needReferenceData);
        }

        #region NeedReferenceData
        private static ConfigurationProperty _needReferenceData;

        /// <summary>Name</summary>
        [ConfigurationProperty("needReferenceData", IsRequired = true)]
        public bool NeedReferenceData
        {
            get { return (bool)base[_needReferenceData]; }
        }
        #endregion NeedReferenceData

        /// <summary>
        /// 
        /// </summary>
        public AdministrationControlsCollection()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected override ConfigurationPropertyCollection Properties
        {
            get { return _properties; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override string ElementName
        {
            get { return "administrationControl"; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public AdministrationControl this[int index]
        {
            get { return (AdministrationControl)base.BaseGet(index); }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                base.BaseAdd(index, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public new AdministrationControl this[string name]
        {
            get { return (AdministrationControl)base.BaseGet(name); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new AdministrationControl();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as AdministrationControl).Name;
        }
    }
    #endregion AdministrationControlsCollection

    #region AdministrationControl
    /// <summary>
    /// 
    /// </summary>
    public class AdministrationControl : ConfigurationElement
    {
        private static ConfigurationPropertyCollection _properties;

        static AdministrationControl()
        {
            _properties = new ConfigurationPropertyCollection();

            _name = new ConfigurationProperty("name", typeof(string), null, ConfigurationPropertyOptions.IsRequired);
            _properties.Add(_name);

            _access = new ConfigurationProperty("access", typeof(string), null, ConfigurationPropertyOptions.IsRequired);
            _properties.Add(_access);

            _treeNode = new ConfigurationProperty("treeNode", typeof(string), null, ConfigurationPropertyOptions.IsRequired);
            _properties.Add(_treeNode);

            _description = new ConfigurationProperty("description", typeof(string), null, ConfigurationPropertyOptions.IsRequired);
            _properties.Add(_description);

            _type = new ConfigurationProperty("type", typeof(string), null, ConfigurationPropertyOptions.IsRequired);
            _properties.Add(_type);
        }

        
        #region Name
        private static ConfigurationProperty _name;

        /// <summary>Name</summary>
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string)base[_name]; }
        }
        #endregion Name

        #region Access
        private static ConfigurationProperty _access;

        /// <summary>Name</summary>
        [ConfigurationProperty("access", IsRequired = true)]
        public string Access
        {
            get { return (string)base[_access]; }
        }
        #endregion Access

        #region TreeNode
        private static ConfigurationProperty _treeNode;

        /// <summary>Tree Node</summary>
        [ConfigurationProperty("treeNode", IsRequired = true)]
        public string TreeNode
        {
            get { return (string)base[_treeNode]; }
        }
        #endregion TreeNode

        #region Description
        private static ConfigurationProperty _description;

        /// <summary>Service Name</summary>
        [ConfigurationProperty("description", IsRequired = true)]
        public string Description
        {
            get { return (string)base[_description]; }
        }
        #endregion Description

        #region Type
        private static ConfigurationProperty _type;

        /// <summary>Type</summary>
        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get { return (string)base[_type]; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <param name="assemblyPath"></param>
        /// <returns></returns>
        public object CreateInstance(object[] args, string assemblyPath)
        {
            return CreateInstance(this.Type, args, assemblyPath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object CreateInstance()
        {
            return CreateInstance(this.Type, null, null);
        }

        public static object CreateInstance(string configString, object[] args, string assemblyPath)
        {
            string _typeName = string.Empty;
            string _assembly = string.Empty;
            string _assemblyDll = string.Empty;

            if (configString == null || configString.Length <= 0)
            {
                throw new Exception("ExceptionBadTypeArgument");
            }

            string[] parts = configString.Split(new char[] { ',' });

            if (parts.Length == 5)
            {
                //  set the object type name
                _typeName = parts[0].Trim();

                //  set the object assembly name
                _assembly = String.Concat(parts[1].Trim(), ",", parts[2].Trim(), ",", parts[3].Trim(), ",", parts[4].Trim());

                _assemblyDll = String.Concat(parts[1].Trim() + ".dll");
            }
            else
                throw new Exception("ExceptionBadTypeArgument");

            Assembly assemblyInstance = null;

            try
            {
                if (assemblyPath != null && assemblyPath.Length > 0)
                {
                    string path = assemblyPath;

                    if (!path.EndsWith("\\")) path += "\\";

                    assemblyInstance = Assembly.LoadFrom(path + _assemblyDll);
                }
                else
                    //  use full assembly name to get assembly instance
                    assemblyInstance = Assembly.Load(_assembly);
            }
            catch (Exception e)
            {
                throw new Exception("ExceptionCantLoadAssembly");
            }

            //  use type name to get type from assembly
            Type _type = assemblyInstance.GetType(_typeName, true, false);

            object ans = null;

            try
            {
                if (args != null)
                    ans = Activator.CreateInstance(_type, args);
                else
                    ans = Activator.CreateInstance(_type);
            }
            catch (Exception e)
            {
                throw new Exception("ExceptionCantCreateInstanceUsingActivate");
            }

            return ans;
        }
        #endregion Type

    }
    #endregion AdministrationControl
}
