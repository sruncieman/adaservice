﻿using System;
using System.Drawing;
using System.Configuration;
using System.Windows.Forms;
using AdminConsole.Controls;

namespace AdminConsole
{
    public partial class Form1 : Form
    {
        private AdministrationConsoleConfiguration _ControlConfig;

        /// <summary>
        /// 
        /// </summary>
		public Form1()
		{
			InitializeComponent();
		}

		struct ControlState
		{
			public AdministrationControl ControlDefinition;
			public UserControl ControlInstance;
		}

		private TreeNode FindNode( TreeNodeCollection parent, string s )
		{
			foreach( TreeNode n in parent )
			{
				if ( n.Text == s ) 
					return n;
			}
			return null;
		}

        /// <summary>
        /// Build  the TREE from the PATH parameter
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
		private TreeNode AddTreeNode( string path )
		{
			string[] nodeNames = path.Split( new char[] { '/' } );

			TreeNodeCollection root = treeView1.Nodes;   // Get the ROOT node
			TreeNode leaf = null;

			foreach( string s in nodeNames )  // loop through each NODE in the path
			{
				TreeNode tn = FindNode( root, s );  // Check if we already habe this node

				if ( tn == null )   // We haven't, so create a node and move ROOT to the child NODES of this node
				{
					TreeNode newNode = new TreeNode( s );

					root.Add( newNode );

					root = newNode.Nodes;
					
					leaf = newNode;
				}
				else  // We have so just move ROOT to the child NODES of the node we found
				{
					root = tn.Nodes;

					leaf = tn;
				}
			}
			return leaf;
		}

		private TreeNode _CurrentNode = null;

		private bool UserHasAccess( string[] priv, string[] privParts )
		{
			foreach( string up in privParts )
			{
				foreach( string p in priv )
				{
					if ( string.Compare( up, p, true ) == 0 )
						return true;
				}
			}
			return false;
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
            _ControlConfig = ConfigurationManager.GetSection("administrationConsoleConfiguration") as AdministrationConsoleConfiguration;

			bool ok = true;
			string error = "";
            this.Text += " v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(); 

			if ( ok )
			{
				lblError.Visible = false;

                // Loop through each control declared in CONFIG
				foreach( AdministrationControl mc in _ControlConfig.AdministrationControls )
				{
                    // Split apart the comma separated list of ACCESS values (nor used in KEO version)
					string[] privParts = mc.Access.Split( ',' );
					for( int i = 0; i < privParts.Length; privParts[i] = privParts[i].Trim(), i++ );

					ControlState cState;

					TreeNode node = AddTreeNode( mc.TreeNode );

                    // Create instance of the User Control
					cState.ControlDefinition = mc;
					cState.ControlInstance = (UserControl)(mc.CreateInstance());

                    // Check it is inherited from BaseAdminControl, ignore if not
                    if (cState.ControlInstance is BaseAdminControl)
                    {
                        cState.ControlInstance.Location = new Point(0, 0);
                        cState.ControlInstance.Size = new Size(1024, 768);
                        cState.ControlInstance.Dock = DockStyle.Fill;

                        // Set the TAG on the TREENODE to point to the instance of the CONTROL
                        node.Tag = cState;
                    }
                    else
                    {
                        MessageBox.Show(mc.Description + " does not derive from BaseAdminControl.");
                    }
				}
			}
			else
			{
				lblError.Text = error;
			}
		}

		private void treeView1_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
            // First remove the control from the panel assuming there is on there in the first place
    		if (_CurrentNode != null && _CurrentNode.Tag != null)
			{
				this.panel1.Controls.Clear();
			}

            // Set the CurrentNode to the one selected
			_CurrentNode = treeView1.SelectedNode;

            // If there is a selected node
			if ( _CurrentNode != null && _CurrentNode.Tag != null )
			{
				try   // Add the Control to the panel (which is conveniently pointed too by the TAG on the selected TreeNode itself)
				{
					this.panel1.Controls.Add(((ControlState)_CurrentNode.Tag).ControlInstance);
				}
				catch( Exception ex )
				{
					MessageBox.Show( ex.Message, "Failed to load control" );
				}

                // Set the Status Text from the config values
				if ( ((ControlState)_CurrentNode.Tag).ControlDefinition.Description != null &&
					 ((ControlState)_CurrentNode.Tag).ControlDefinition.Description.Length > 0 )
					statusBar1.Text = ((ControlState)_CurrentNode.Tag).ControlDefinition.Description;
				else
					statusBar1.Text = "Select Child Node To Access Administration Control";
			}
			else
			{
				statusBar1.Text = "Select Node To Access Administration Control";
			}
		}
	}
    
}
