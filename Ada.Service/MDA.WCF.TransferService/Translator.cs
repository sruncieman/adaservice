﻿using System.Collections.Generic;
using ADAServices.Interface;
using MDA.RiskService.Model;
using System.Runtime.Serialization;
using MDA.Common;

namespace MDA.WCF.TransferServices
{

    [DataContract]
    public class GenerateBatchReportRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskBatchId { get; set; }
    }

    [DataContract]
    public class GenerateLevel1ReportRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }
    }

    [DataContract]
    public class GenerateLevel2ReportRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }

        [DataMember]
        public int RiskClaimId { get; set; }
    }


 



    public partial class Translator
    {
        public static GetCueReportStreamParam Translate(GetCueReportStreamRequest request)
        {
            return new GetCueReportStreamParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                RiskClaimId = request.RiskClaimId,
                Unique_Id = request.Unique_Id,
            };
        }

        public static GetBatchReportStreamParam Translate(GetBatchReportStreamRequest request)
        {
            return new GetBatchReportStreamParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who, 
                 RiskBatchId = request.RiskBatchId
            };
        }

        public static GetLevel1ReportStreamParam Translate(GetLevel1ReportStreamRequest request)
        {
            return new GetLevel1ReportStreamParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                RiskClaimId = request.RiskClaimId
            };
        }

        public static GetLevel2ReportStreamParam Translate(GetLevel2ReportStreamRequest request)
        {
            return new GetLevel2ReportStreamParam()
            {
                UserId = request.UserId,
                ClientId = request.ClientId,
                Who = request.Who,
                RiskClaimId = request.RiskClaimId
            };
        }



    }
}
