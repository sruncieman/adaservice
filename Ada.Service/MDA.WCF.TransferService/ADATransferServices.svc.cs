﻿using System;
using System.IO;
using ADAServices.Factory;

namespace MDA.WCF.TransferServices
{
    public class ADATransferServices : IADATransferServices
    {
        public void UploadBatchOfClaimsFile(RemoteFileInfo request)
        {
            ADAServicesFactory.CreateADAServices().UploadBatchOfClaimsFile(request.FileByteStream, request.BatchEntityType, request.FileName, request.MimeType, request.ClientId, request.UserId, request.ClientBatchReference, request.Who);
        }

        //public Byte[] GetReportBytes(int riskClaimId)
        //{
        //    return ADAServicesFactory.CreateADAServices().GetReportBytes(riskClaimId);
        //}

        public Stream GetLevel1ReportStream(GetLevel1ReportStreamRequest request)
        {
            return ADAServicesFactory.CreateADAServices().GetLevel1ReportStream(Translator.Translate(request));
        }

        public Stream GetLevel2ReportStream(GetLevel2ReportStreamRequest request)
        {
            return ADAServicesFactory.CreateADAServices().GetLevel2ReportStream(Translator.Translate(request));
        }

        public Stream GetBatchReportStream(GetBatchReportStreamRequest request)
        {
            return ADAServicesFactory.CreateADAServices().GetBatchReportStream(Translator.Translate(request));
        }

        public Stream GetCueReportStream(GetCueReportStreamRequest request)
        {
            return ADAServicesFactory.CreateADAServices().GetCueReportStream(Translator.Translate(request));
        }
    }
}
