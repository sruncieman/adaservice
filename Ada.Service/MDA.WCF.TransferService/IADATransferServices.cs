﻿using System;
using System.ServiceModel;
using System.IO;
using System.Runtime.Serialization;

namespace MDA.WCF.TransferServices
{
    [ServiceContract(Namespace = "http://www.keoghs.co.uk/ADA")]
    public interface IADATransferServices
    {
        [OperationContract]
        void UploadBatchOfClaimsFile(RemoteFileInfo request);

        [OperationContract]
        Stream GetLevel1ReportStream(GetLevel1ReportStreamRequest request);

        [OperationContract]
        Stream GetLevel2ReportStream(GetLevel2ReportStreamRequest request);

        [OperationContract]
        Stream GetBatchReportStream(GetBatchReportStreamRequest request);
        

    }

    [MessageContract]
    public class RemoteFileInfo : IDisposable
    {
        [MessageHeader(MustUnderstand = true)]
        public int ClientId;

        [MessageHeader(MustUnderstand = true)]
        public string ClientBatchReference;

        [MessageHeader(MustUnderstand = true)]
        public string BatchEntityType;

        [MessageHeader(MustUnderstand = true)]
        public string FileName;

        [MessageHeader(MustUnderstand = true)]
        public string MimeType;

        [MessageHeader(MustUnderstand = true)]
        public string Who;

        [MessageHeader(MustUnderstand = true)]
        public int UserId;

        [MessageHeader(MustUnderstand = true)]
        public long FileLength;

        [MessageBodyMember(Order = 1)]
        public Stream FileByteStream;

        public void Dispose()
        {
            if (FileByteStream != null)
            {
                FileByteStream.Close();
                FileByteStream = null;
            }
        }
    }

    [DataContract]
    public class GetLevel1ReportStreamRequest
    {
        [DataMember]
        public int RiskClaimId { get; set; }


        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class GetLevel2ReportStreamRequest
    {
        [DataMember]
        public int RiskClaimId { get; set; }


        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class GetBatchReportStreamRequest
    {
        [DataMember]
        public int RiskBatchId { get; set; }


        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }

    [DataContract]
    public class GetCueReportStreamRequest
    {
        [DataMember]
        public int RiskClaimId { get; set; }

        [DataMember]
        public int Unique_Id { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Who { get; set; }
    }
}
