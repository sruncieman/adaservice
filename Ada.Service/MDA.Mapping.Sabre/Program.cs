﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using MDA.Pipeline.Model;
using MDA.MappingService.Sabre.Motor;
using MDA.Common.Server;
using MDA.Common;

namespace MDA.Mapping.Sabre
{
    class Program
    {
        static PipelineClaimBatch motorClaimBatch = new PipelineClaimBatch();

        private static int ProcessClaimIntoDb(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim, object o)
        {
            motorClaimBatch.Claims.Add((PipelineMotorClaim)claim);

            return 0;
        }

        static void Main(string[] args)
        {
            Stream mockStream = null;

            CurrentContext ctx = new CurrentContext(0, 1, "Test");

            ProcessingResults pr;

            new FileMapping().ConvertToClaimBatch(ctx, mockStream, null, ProcessClaimIntoDb, out pr);

            string strXmlFileSave = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Sabre\Xml\SabreXml.xml";

            //var claims = motorClaimBatch.Claims.Skip(500000).Take(500000);
            using (var writer = new FileStream(strXmlFileSave, FileMode.Create, FileAccess.Write))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(PipelineClaimBatch));
                Console.WriteLine("Serializing to XML");
                ser.WriteObject(writer, motorClaimBatch);
            }
           
        }
    }
}
