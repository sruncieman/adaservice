﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Standard.Mobile.Attributes
{
    public class CommonNonValuesConverter : ConverterBase
    {
        public override object StringToField(string value)
        {
            if (string.IsNullOrEmpty(value.ToString()))
                return value;

            string[] valuesToNull = new string[] {
                "unknown",
                @"/", @"//", @"///", @"///",
                @"\\", @"\\", @"\\\", @"\\\\",
                ".", "..", "...",
                "tbc",
                "-",
            };

            foreach (string item in valuesToNull)
                if (value.ToLower() == item)
                    return null;

            return value;
        }
    }
}
