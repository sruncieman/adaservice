﻿using MDA.Common;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MDA.MappingService.Standard.Mobile
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        /// <summary>
        /// Convert Standard file format to ClaimBatch.  Simply deserialise the file. It if fails it's broken. 
        /// </summary>
        /// <param name="ctx">Current Context containing DB connection to use</param>
        /// <param name="fs">FileStream containing the file to be loaded</param>
        /// <param name="statusTracking"></param>
        /// <param name="processClaim">CallBack function to call for each PipelineClaim created</param>
        /// <param name="ShowProgress">Show Progress callback</param>
        /// <param name="processingResults">Structure to hold ALL processing results</param>
        public void ConvertToClaimBatch(Common.Server.CurrentContext ctx, System.IO.Stream fs, object statusTracking, Func<Common.Server.CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn, out Common.ProcessingResults processingResults)
         {
            fs.Seek(0, SeekOrigin.Begin);

            PipelineClaimBatch ret = new PipelineClaimBatch();

            processingResults = new ProcessingResults("Mapping");

            try
            {
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                DataContractSerializer ser = new DataContractSerializer(typeof(MDA.Common.FileModel.MobileClaimBatch));

                MDA.Common.FileModel.MobileClaimBatch cb = (MDA.Common.FileModel.MobileClaimBatch)ser.ReadObject(reader, true);

                MDA.Pipeline.Model.Translator tr = new MDA.Pipeline.Model.Translator();

                foreach (var c in cb.Claims)
                    if (ProcessClaimFn(ctx, tr.Translate(c), statusTracking) == -1) return;
            }
            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Failed to recognise XML file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

    }
   
}
