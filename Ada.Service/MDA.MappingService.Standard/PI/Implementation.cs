﻿using MDA.Pipeline.Model;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using MDA.MappingService.Interface;
using System.Collections.Generic;
using System;
using MDA.Common;
using MDA.Common.Server;
using MDA.MappingService;

namespace MDA.MappingService.Standard.PI

{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        /// <summary>
        /// Convert Standard file format to ClaimBatch.  Simply deserialise the file. It if fails it's broken. 
        /// </summary>
        /// <param name="ctx">Current Context containing DB connection to use</param>
        /// <param name="fs">FileStream containing the file to be loaded</param>
        /// <param name="statusTracking"></param>
        /// <param name="processClaim">CallBack function to call for each PipelineClaim created</param>
        /// <param name="ShowProgress">Show Progress callback</param>
        /// <param name="processingResults">Structure to hold ALL processing results</param>
        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking, 
                                                Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn,
                                                out ProcessingResults processingResults)
        {
            fs.Seek(0, SeekOrigin.Begin);

            PipelineClaimBatch ret = new PipelineClaimBatch();

            processingResults = new ProcessingResults("Mapping");

            try
            {
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                DataContractSerializer ser = new DataContractSerializer(typeof(MDA.Common.FileModel.PIClaimBatch));

                MDA.Common.FileModel.PIClaimBatch cb = (MDA.Common.FileModel.PIClaimBatch)ser.ReadObject(reader, true);

                MDA.Pipeline.Model.Translator tr = new MDA.Pipeline.Model.Translator();

                foreach (var c in cb.Claims)
                    if (ProcessClaimFn(ctx, tr.Translate(c), statusTracking) == -1) return;
            }
            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Failed to recognise XML file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

    }

    //public class StandardBigFileMapping : ITableMappingService
    //{
    //    /// <summary>
    //    /// Convert Standard file format to ClaimBatch.  Simply deserialise the file. It if fails it's broken. 
    //    /// </summary>
    //    /// <param name="fs"></param>
    //    /// <returns></returns>
    //    public PipelineClaimBatch ConvertToClaimBatch(CurrentContext ctx, Stream fs, out ProcessingResults processingResults, Func<CurrentContext, PipelineClaim, int> ProcessClaim)
    //    {
    //        fs.Seek(0, SeekOrigin.Begin);

    //        PipelineClaimBatch ret = new PipelineClaimBatch();

    //        processingResults = new ProcessingResults("Mapping");

    //        try
    //        {
    //            XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
    //            DataContractSerializer ser = new DataContractSerializer(typeof(MDA.Common.FileModel.ClaimBatch));

    //            MDA.Common.FileModel.ClaimBatch cb = (MDA.Common.FileModel.ClaimBatch)ser.ReadObject(reader, true);


    //            MDA.Pipeline.Model.Translator tr = new MDA.Pipeline.Model.Translator();

    //            foreach( var claim in cb.Claims )
    //                ProcessClaim(ctx, tr.Translate(claim));

    //            //ret = new MDA.Pipeline.Model.Translator().Translate(cb);

    //            return ret;
    //        }
    //        catch (Exception ex)
    //        {
    //            processingResults.Message.AddError("Failed to recognise XML file. " + ex.Message);

    //            return null;
    //        }
    //    }

    //}
}
