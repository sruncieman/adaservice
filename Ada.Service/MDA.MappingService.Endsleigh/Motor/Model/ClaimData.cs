﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.Endsleigh.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String SubClaimNotifiedDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String SchemeCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int PolicyNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EisClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String SubClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NameTitle;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NameForename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NameInitials;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NameSurname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DateOfBirth;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PermanentAddressLine1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PermanentAddressLine2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PermanentOutwardPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PermanentInwardPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TemporaryAddressLine1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TemporaryAddressLine2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TemporaryOutwardPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TemporaryInwardPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DaytimePhoneNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EveningPhoneNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MobilePhone;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EmailAddressDaytime;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleRegistrationNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NationalInsuranceNo;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AssociateName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AssociateAddressLine1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AssociateAddressLine2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AssociateOutwardPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AssociateInwardPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DaytimePhoneNumber1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String IncidentDate;

    }
}





