﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FileHelpers;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.Common.Helpers;
using MDA.MappingService.Endsleigh.Model;
using MDA.Common.Server;
using MDA.Common;
using System.Globalization;

namespace MDA.MappingService.Endsleigh.Motor
{
    public class GetMotorClaimBatch
    {

        private string _claimDataFile;
        private FileHelperEngine _claimDataEngine;
        private ClaimData[] _claimData;
        private StreamReader _claimDataFileStreamReader;

        public void RetrieveMotorClaimBatch(CurrentContext ctx, Stream filem, string clientFolder, object statusTracking, Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn, ProcessingResults processingResults)
        {
            #region Files

            if (filem == null)
            {
                _claimDataFile = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Endsleigh\Resource\MotorKeoghs.csv";
            }
            else
            {
                try
                {
                    _claimDataFileStreamReader = new StreamReader(filem);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in creating csv file: " + ex);
                }

            }
            #endregion

            #region Initialise FileHelper Engine
            try
            {
                _claimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
            #endregion

            #region Populate FileHelper Engine with data from file

            try
            {
                if (filem == null)
                {
                    _claimData = _claimDataEngine.ReadFile(_claimDataFile) as ClaimData[];
                }
                else
                {
                    _claimData = _claimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in reading csv file via filehelper engine: " + ex);
            }

            #endregion

            TranslateToXml(ctx, _claimData, statusTracking, ProcessClaimFn, processingResults);

        }

        private void TranslateToXml(CurrentContext ctx, ClaimData[] claimData, object statusTracking, Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn, ProcessingResults processingResults)
        {
            List<int?> processedClaims = new List<int?>(); //Create list of proccessed claims to ensure same claim not translated twice

            foreach (var claim in claimData)
            {

                if (!processedClaims.Contains(claim.PolicyNumber))
                {

                    PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                    motorClaim.ClaimNumber = claim.SchemeCode + claim.PolicyNumber;

                    if (!string.IsNullOrEmpty(claim.IncidentDate))
                    {
                        DateTime incidentDate = DateTime.ParseExact(claim.IncidentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                        motorClaim.IncidentDate = incidentDate;
                    }

                    if (!string.IsNullOrEmpty(claim.SubClaimNotifiedDate))
                    {
                        DateTime claimNotificationDate = DateTime.ParseExact(claim.SubClaimNotifiedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                        motorClaim.ExtraClaimInfo.ClaimNotificationDate = claimNotificationDate;
                    }

                    motorClaim.ClaimType_Id = (int)ClaimType.Motor;

                    motorClaim.Policy.PolicyNumber = claim.PolicyNumber.ToString();
                    motorClaim.Policy.Insurer = "Endsleigh";

                    ClaimData[] uniqueCliams = (from x in claimData     // Find all rows relating to one claim, claim number may appear on several rows linking parties to claim
                                                where x.PolicyNumber == claim.PolicyNumber
                                                select x).ToArray();


                    var vehicles =
                        from v in uniqueCliams
                        group v by new
                        {
                            v.VehicleRegistrationNumber,
                            v.NameForename,
                            v.NameSurname,
                        } into uniqueIds
                        select uniqueIds.FirstOrDefault();


                    foreach (var v in vehicles)
                    {

                        PipelineVehicle vehicle = new PipelineVehicle();

                        if (string.IsNullOrWhiteSpace(v.VehicleRegistrationNumber))
                        {
                            vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.Unknown;
                        }
                        else
                        {
                            vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                        }

                        vehicle.VehicleRegistration = v.VehicleRegistrationNumber;
                        motorClaim.Vehicles.Add(vehicle);

                        var people =
                            from p in uniqueCliams
                            group p by new
                            {
                                p.NameForename,
                                p.NameSurname,
                            } into uniqueIds
                            select uniqueIds.FirstOrDefault();

                        foreach (var p in people)
                        {

                            if (p.VehicleRegistrationNumber == v.VehicleRegistrationNumber && p.SubClaimNumber == v.SubClaimNumber)
                            {
                                #region Person

                                if (!string.IsNullOrEmpty(p.NameForename) || !string.IsNullOrEmpty(p.NameSurname))
                                {
                                    PipelinePerson person = new PipelinePerson();
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                                    if (!string.IsNullOrEmpty(p.NameTitle))
                                    {
                                        person.Salutation_Id = (int)SalutationHelper.GetSalutation(p.NameTitle);
                                    }

                                    person.FirstName = p.NameForename;
                                    person.LastName = p.NameSurname;

                                    if (!string.IsNullOrEmpty(p.DateOfBirth))
                                    {
                                        DateTime dateOfBirth = DateTime.ParseExact(p.DateOfBirth, "dd/MM/yyyy", CultureInfo.InvariantCulture); // Convert string to date
                                        person.DateOfBirth = dateOfBirth;
                                    }

                                    if (!string.IsNullOrWhiteSpace(p.NationalInsuranceNo))
                                    {
                                        person.NINumbers.Add(new PipelineNINumber() { NINumber1 = p.NationalInsuranceNo });
                                    }

                                    if (!string.IsNullOrWhiteSpace(p.EmailAddressDaytime))
                                    {
                                        person.EmailAddresses.Add(new PipelineEmail() { EmailAddress = p.EmailAddressDaytime });
                                    }

                                    if (!string.IsNullOrWhiteSpace(p.DaytimePhoneNumber))
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = p.DaytimePhoneNumber });
                                    }

                                    if (!string.IsNullOrWhiteSpace(p.DaytimePhoneNumber1) && p.DaytimePhoneNumber1 != p.DaytimePhoneNumber)
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = p.DaytimePhoneNumber1 });
                                    }

                                    if (!string.IsNullOrWhiteSpace(p.EveningPhoneNumber) && p.EveningPhoneNumber != p.DaytimePhoneNumber)
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = p.EveningPhoneNumber });
                                    }

                                    if (!string.IsNullOrWhiteSpace(p.MobilePhone) && p.MobilePhone != p.DaytimePhoneNumber)
                                    {
                                        person.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = p.MobilePhone });
                                    }

                                    #region Address

                                    if (!string.IsNullOrWhiteSpace(p.PermanentAddressLine1) || !string.IsNullOrWhiteSpace(p.PermanentAddressLine2) || !string.IsNullOrWhiteSpace(p.PermanentOutwardPostcode) || !string.IsNullOrWhiteSpace(p.PermanentInwardPostcode))
                                    {
                                        PipelineAddress address1 = new PipelineAddress();

                                        address1.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;

                                        address1.Street = p.PermanentAddressLine1;
                                        address1.Town = p.PermanentAddressLine2;
                                        address1.PostCode = p.PermanentOutwardPostcode + p.PermanentInwardPostcode;

                                        person.Addresses.Add(address1);
                                    }

                                    if (!string.IsNullOrWhiteSpace(p.TemporaryAddressLine1) || !string.IsNullOrWhiteSpace(p.TemporaryAddressLine2) || !string.IsNullOrWhiteSpace(p.TemporaryOutwardPostcode) || !string.IsNullOrWhiteSpace(p.TemporaryInwardPostcode))
                                    {
                                        PipelineAddress address2 = new PipelineAddress();

                                        address2.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.LinkedAddress;

                                        address2.Street = p.TemporaryAddressLine1;
                                        address2.Town = p.TemporaryAddressLine2;
                                        address2.PostCode = p.TemporaryOutwardPostcode + p.TemporaryInwardPostcode;

                                        person.Addresses.Add(address2);
                                    }

                                    #endregion

                                    #region Organisation

                                    if (!string.IsNullOrWhiteSpace(p.AssociateName))
                                    {
                                        PipelineOrganisation organisation = new PipelineOrganisation();

                                        organisation.OrganisationName = p.AssociateName;

                                        if (!string.IsNullOrWhiteSpace(p.AssociateAddressLine1) || !string.IsNullOrWhiteSpace(p.AssociateAddressLine2) || !string.IsNullOrWhiteSpace(p.AssociateOutwardPostcode) || !string.IsNullOrWhiteSpace(p.AssociateInwardPostcode))
                                        {
                                            PipelineAddress address1 = new PipelineAddress();
                                            address1.AddressType_Id = (int)AddressLinkType.TradingAddress;

                                            address1.Street = p.AssociateAddressLine1;
                                            address1.Town = p.AssociateAddressLine2;
                                            address1.PostCode = p.AssociateOutwardPostcode + p.AssociateInwardPostcode;

                                            organisation.Addresses.Add(address1);
                                        }

                                        person.Organisations.Add(organisation);
                                    }

                                    #endregion

                                    vehicle.People.Add(person);
                                }

                                #endregion
                            }
                        }
                    }

                    PipelineVehicle insVehicle = new PipelineVehicle();

                    insVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                    PipelinePerson insPerson = new PipelinePerson();
                    insPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    insPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                    insVehicle.People.Add(insPerson);

                    motorClaim.Vehicles.Add(insVehicle);

                    processedClaims.Add(claim.PolicyNumber); // Add claim number to list so that the claim is not processed again

                    if (ProcessClaimFn(ctx, motorClaim, statusTracking) == -1) return;
                }
            }
        }
    }
}
