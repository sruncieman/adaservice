﻿using System;
using System.Diagnostics;
using System.IO;
using MDA.Common.Debug;
using MDA.Common.Server;
using MDA.RiskService;
using MDA.RiskService.Model;
using MDA.Common;
using MDA.ClaimService.Interface;
using MDA.RiskService.Interface;
using System.Configuration;
using MDA.MappingService.Interface;
using MDA.Common.Enum;

namespace MDA.Pipeline
{
    public partial class Pipeline
    {
        /// <summary>
        /// Find a batch record with a file to process which has not had it's claims created
        /// Leaves Batch Record set to CLAIMS CREATED AND QUEUED and sets all claims in this batch to QUEUED FOR LOADING
        /// </summary>
        /// <param name="ShowProgress"></param>
        /// <param name="maxBatchesToProcess"></param>
        /// <param name="_trace"></param>
        public static int? Stage2_ProcessBatchQueue(CurrentContext ctx, ServiceConfigSettings configSettings, int? batchId)
        {
            bool _trace = ctx.TraceLoad;

            IRiskServices riskServices = new RiskServices(ctx);

            var riskBatch = riskServices.GetABatchToLoad(batchId, ctx.SubmitDirect);   // Look for batch with QUEUED status and set status to CREATING CLAIMS

            if (riskBatch != null)
            {
                #region Database Backup
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["PerformDbBackups"]))
                {
                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Perform DIFFERENTIAL backup of db : ", false);
                    if (_trace)
                        ADATrace.WriteLine("Perform DIFFERENTIAL backup of db");

                    try
                    {
                        Stopwatch sw = new Stopwatch();
                        sw.Start();

                        // 3-DIFF will become 1-FULL automatically if no full backup done already or it is a new day
                        riskServices.BackupTheDatabase("Before Create [" + riskBatch.Id + "][" + riskBatch.ClientBatchReference + "]", 3,
                            configSettings.BackupDbPath, configSettings.BackupDbFolder);

                        riskServices.SaveBackupTiming(sw.ElapsedMilliseconds, BackupType.DiffBeforeCreate);


                        if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                        if (_trace)
                            ADATrace.WriteLine("DIFFERENTIAL backup of db : Done");
                    }
                    catch (Exception ex)
                    {
                        while (ex != null)
                        {
                            ctx.ShowProgress(ProgressSeverity.Info, "Exception: " + ex.Message, true);
                            if (_trace)
                                ADATrace.WriteLine("DIFFERENTIAL backup of db : " + ex.Message);

                            ex = ex.InnerException;
                        }

                        if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "FAILED", true);
                        
                    }
                }
                #endregion

                var riskClient = riskServices.GetRiskClient(riskBatch.RiskClient_Id);

                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "ProcessBatchQueue [" + riskBatch.Id + "] for Client [" + riskBatch.RiskClient_Id +  "] : Starting", true);

                try
                {
                    ctx.RiskClientId = riskClient.Id;
                    ctx.Who = riskBatch.CreatedBy;
                    ctx.ClientName = riskClient.ClientName;

                    if (_trace) ADATrace.SwitchToNewUniqueLogFile(riskBatch.RiskClient_Id, riskBatch.ClientBatchReference, "PopulateClaims");

                    Pipeline p = (Pipeline)CreatePipeline(riskBatch.BatchEntityType, riskClient.Id);

                    if (_trace)
                    {
                        ADATrace.WriteLine("");
                        ADATrace.WriteLine("=========================================NEW PIPELINE (Creating)==========================================================");
                        ADATrace.WriteLine("Creating Pipeline(submitDirect " + p.SubmitClaimsFromPipeline.ToString() +
                            ", scoreDirect " + p.ScoreClaimsFromPipeline.ToString() + ", liveRules " + p.ScoreClaimsWithLiveRules.ToString());
                    }

                    p.CreateClientService().BeforeCreate(ctx, riskBatch.Id);

                    //Leaves Batch Record set to CLAIMS CREATED AND QUEUED and sets all claims in this batch to QUEUED FOR LOADING
                    p.ProcessBatchCreateClaims(ctx, riskBatch.Id);

                    p.CreateClientService().AfterCreate(ctx, riskBatch.Id);

                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "ProcessBatchQueue : Done", true);
                    if (_trace) ADATrace.WriteLine("ProcessBatchQueue: Ended...Returning BatchID [" + riskBatch.Id.ToString() + "]");

                    #region Database Backup
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["PerformDbBackups"]))
                    {
                        if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Perform LOGFILE backup of db : ", false);
                        if (_trace)
                            ADATrace.WriteLine("Perform LOGFILE backup of db");

                        try
                        {
                            // LOG Backup
                            riskServices.BackupTheDatabase("After Create [" + riskBatch.Id + "]", 4,
                                configSettings.BackupDbPath, configSettings.BackupDbFolder);

                            if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                            if (_trace)
                                ADATrace.WriteLine("LOGFILE backup of db : Done");
                        }
                        catch (Exception ex)
                        {
                            while (ex != null)
                            {
                                ctx.ShowProgress(ProgressSeverity.Info, "Exception: " + ex.Message, true);
                                if (_trace)
                                    ADATrace.WriteLine("LOGFILE backup of db : " + ex.Message);

                                ex = ex.InnerException;
                            }
                        }
                    }
                    #endregion

                    return riskBatch.Id;
                }
                catch (Exception ex)    // by the time it gets here batch status is already set to error code unless DB can't be updated, so status stuck at CREATING CLAIMS
                {
                    if (_trace) ADATrace.TraceException(ex);

                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "ProcessBatchQueue ERROR: " + ex.Message, true);

                    throw;
                }
                finally
                {
                    

                    if (_trace)
                    {
                        ADATrace.WriteLine("=========================================END PIPELINE (Creating)==========================================================");
                        ADATrace.WriteLine("");
                    }

                    ADATrace.SwitchToDefaultLogFile();
                }
            }

            return null;
        }

        /// <summary>
        /// Called as part of recovery.  Looked for Batches which are stuck at CreatingClaims.  Having found any delete all the claims created so
        /// far and reset the batch status to QueuedForCreating
        /// </summary>
        /// <param name="ctx"></param>
        public static void Stage2_CleanUpPartiallyCreatedBatchClaims(CurrentContext ctx)
        {
            IRiskServices riskServices = new RiskServices(ctx);

            riskServices.CleanUpPartiallyCreatedBatchClaims();
        }

        /// <summary>
        /// Passed the BatchID of a CREATING CLAIMS batch. It MAPS it and then creates the RISKCLAIM records.
        /// Leaves Batch Record set to CLAIMSCREATEDANDQUEUED and sets all claims in this batch to QUEUED FOR LOADING
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="riskBatchId"></param>
        /// <param name="ShowProgress"></param>
        /// <returns></returns>
        public void ProcessBatchCreateClaims(CurrentContext ctx, int riskBatchId)
        {
            ClaimCreationStatus currentCreateStatus = new ClaimCreationStatus() { RiskBatchId = riskBatchId };

            #region Assert
            Debug.Assert(ctx.RiskClientId >= 0);
            Debug.Assert(!string.IsNullOrEmpty(ctx.Who));
            #endregion

            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                #region Trace
                if (TraceLoad) ADATrace.WriteLine("ProcessBatchCreateClaims: Starting...");
                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Process Client [" + ctx.RiskClientId + "] Batch Id[ " + riskBatchId.ToString() + "]..", false);
                #endregion

                IMappingService MappingService = CreateMappingService();

                #region If no Mapping module provided, abort and return FALSE. Set BatchStatus=UnableToProcessNoMappingDefined
                if (MappingService == null)
                {
                    if (TraceLoad) ADATrace.WriteLine("ProcessBatchCreateClaims: Aborting, set status to UnableToProcessNoMappingDefined");

                    using (var ctxx = new CurrentContext(ctx))
                    {
                        using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                        {
                            try
                            {
                                CreateRiskService(ctxx).SetRiskBatchStatus(riskBatchId, RiskBatchStatus.UnableToProcessNoMappingDefined);

                                transaction.Commit();
                            }
                            catch (Exception)
                            {
                                transaction.Rollback();
                                throw;
                            }
                        }
                    }

                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, " : Failed No Mapping Defined : " + sw.ElapsedMilliseconds + "ms", true);

                    return;
                }
                #endregion

                #region Set BATCH to CreatingClaims and read the original file back from DB. If no file set status=UnableToProcessNoFileFound

                byte[] oFile = null;

                using (var ctxx = new CurrentContext(ctx))
                {
                    ctxx.db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["FetchFileCommandTimeout "]);

                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                    {
                        try
                        {
                            if (TraceLoad) ADATrace.WriteLine("ProcessBatchCreateClaims: Fetching file from Db");

                            IRiskServices RiskService = CreateRiskService(ctxx);

                            oFile = RiskService.GetOriginalFile(riskBatchId);

                            if (oFile == null)
                            {
                                if (TraceLoad)
                                    ADATrace.WriteLine(
                                        "ProcessBatchCreateClaims: File not found in Db. Set batch to UnableToProcessNoFileFound");

                                RiskService.SetRiskBatchStatus(riskBatchId, RiskBatchStatus.UnableToProcessNoFileFound);

                                if (ctx.ShowProgress != null)
                                    ctx.ShowProgress(ProgressSeverity.Info, " : Failed No File to process : " + sw.ElapsedMilliseconds + "ms", true);
                            }
                            else
                            {
                                RiskService.SetRiskBatchStatus(riskBatchId, RiskBatchStatus.CreatingClaims);
                            }
                            transaction.Commit();

                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();

                            return;
                        }
                    }

                    if (oFile == null) return;
                }

                #endregion

                #region MAP the file. Uses Callback (ProcessClaimIntoDb) to process each record.  Returns early is ERROR/EXCEPTION occurs

                #region Trace
                if (TraceLoad) ADATrace.WriteLine("ProcessBatchCreateClaims: Before Mapping");
                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, " : Start Mapping ", true);
                #endregion

                ProcessingResults mappingResults;

                MappingService.ConvertToClaimBatch(ctx, new MemoryStream(oFile), currentCreateStatus, CreateClaimService(ctx).ProcessClaimIntoRiskClaimTable, out mappingResults);

                #region Trace
                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Mapping Complete : " + sw.ElapsedMilliseconds + "ms", true);
                if (TraceLoad) ADATrace.WriteLine("ProcessBatchCreateClaims: After Mapping. Saving results to Db");
                #endregion

                #region Save Mapping Results

                using (var ctxx = new CurrentContext(ctx))
                {
                    ctxx.db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["MappingCommandTimeout "]);

                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                    {
                        try
                        {
                            IRiskServices RiskService = CreateRiskService(ctxx);

                            RiskService.SaveMappingResults(riskBatchId, mappingResults);

                            if (!mappingResults.IsValid)
                            {
                                if (TraceLoad)
                                {
                                    ADATrace.WriteLine(
                                        "ProcessBatchCreateClaims: Errors Found. Set Batch to UnableToProcessMappingFailed and return");
                                    ADATrace.DumpProcessingResults(mappingResults);
                                }

                                if (ctx.ShowProgress != null)
                                    ctx.ShowProgress(ProgressSeverity.Info, "Failed Unable to map the file : " + sw.ElapsedMilliseconds + "ms",
                                        true);

                                RiskService.SetRiskBatchStatus(riskBatchId, RiskBatchStatus.UnableToProcessMappingFailed);
                            }
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }

                #endregion

                if (!mappingResults.IsValid)
                {
                    throw new Exception("Mapping created errors, so abort");
                }

                #endregion

                if (TraceLoad) ADATrace.WriteLine("ProcessBatchCreateClaims: Save Counters");

                #region Save Counters, Set BATCH to ClaimsQueuedForLoading and set all CLAIMS in this batch to QueuedForLoading

                using (var ctxx = new CurrentContext(ctx))
                {
                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                    {
                        try
                        {
                            IRiskServices RiskService = CreateRiskService(ctxx);

                            RiskService.SaveRiskBatchCounters(riskBatchId, sw.ElapsedMilliseconds,
                                currentCreateStatus.CountTotal,
                                currentCreateStatus.CountInsert, currentCreateStatus.CountUpdate,
                                currentCreateStatus.CountErrors, currentCreateStatus.CountSkipped);

                            var nothingToProcess = currentCreateStatus.CountInsert + currentCreateStatus.CountUpdate == 0;

                            if (nothingToProcess)
                            {
                                if (TraceLoad) ADATrace.WriteLine("ProcessBatchCreateClaims: Nothing to process");

                                RiskService.SetRiskBatchStatus(riskBatchId, RiskBatchStatus.NoModifiedClaims);

                                // There should be no claims, but just in case set any to archived
                                RiskService.SetAllClaimStatusInBatch(riskBatchId, RiskClaimStatus.IsNew, RiskClaimStatus.Archived);
                            }
                            else
                            {
                                if (TraceLoad)
                                    ADATrace.WriteLine("ProcessBatchCreateClaims: Set Batch to QueuedForLoading");

                                RiskService.SetRiskBatchStatus(riskBatchId, RiskBatchStatus.ClaimsQueuedForLoading);

                                // All the new claims should now be moved on from IS NEW to QUEUED FOR LOADING
                                RiskService.SetAllClaimStatusInBatch(riskBatchId, RiskClaimStatus.IsNew, RiskClaimStatus.QueuedForLoading);
                            }

                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }

                #endregion

                #region Trace
                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "ProcessBatchCreateClaims : Success : " + sw.ElapsedMilliseconds + "ms", true);

                if (TraceLoad)
                {
                    ADATrace.WriteLine("ProcessClaimBatch: Batch and Claim Status Updated");

                    ADATrace.WriteLine(string.Format("Inserts:{0}, Updates:{1}, Skipped:{2}, Errors:{3}, Total:{4}",
                        currentCreateStatus.CountInsert.ToString(), currentCreateStatus.CountUpdate.ToString(), currentCreateStatus.CountSkipped.ToString(),
                        currentCreateStatus.CountErrors.ToString(), currentCreateStatus.CountTotal.ToString()));
                }
                #endregion


            }
            catch (Exception ex)  // unhandled or toomanyerrors come here.  try to set status = BAD. Log and rethrow normal exception
            {
                #region Trace
                if (TraceLoad)
                {
                    ADATrace.TraceException(ex);
                    ADATrace.WriteLine("Exception: Changing status's to BAD");
                }
                #endregion

                #region Set BATCH to error (Bad or TooManyErrors) , Set ALL CLAIMS in BATCH to MemberBadBatch

                using (var ctxx = new CurrentContext(ctx))
                {
                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                    {
                        try
                        {
                            IRiskServices RiskService = CreateRiskService(ctxx);

                            RiskService.SetRiskBatchStatus(riskBatchId,
                                ((ex is TooManyErrorsException)
                                    ? RiskBatchStatus.BadTooManyConsecutiveErrors
                                    : RiskBatchStatus.Bad)); // if this fails batch status left as CREATING CLAIMS

                            RiskService.SetAllRiskClaimStatusAsMemberOfBadBatch(riskBatchId);
                                // and CleanUp should recover it

                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                        }
                    }
                }

                #endregion

                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "ProcessBatchCreateClaims : Failed : " + sw.ElapsedMilliseconds + "ms", true);

                throw;
            }
        }
    }
}
