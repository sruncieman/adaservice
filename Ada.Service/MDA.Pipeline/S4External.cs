﻿using System;
using System.Diagnostics;
using MDA.Common.Debug;
using MDA.DAL;
using MDA.Common.Server;
using MDA.RiskService;
using MDA.RiskService.Model;
using System.Threading;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using MDA.RiskService.Interface;
using MDA.ExternalService.Interface;
using MDA.ExternalServices.Model;
using MDA.Pipeline.Config;
using MDA.Common.Enum;

namespace MDA.Pipeline
{
    public partial class Pipeline
    {
        public static int? Stage4_ProcessClaimQueueThirdParties(CurrentContext ctx, ServiceConfigSettings configSettings, int? batchId)
        {
            IRiskServices riskServices = new RiskServices(ctx);

            // Get any claim, from any client, that needs processing
            var claimToProcess = riskServices.GetAClaimToProcessThirdPartiesFromBatch(batchId, ctx.SubmitDirect);

            if (claimToProcess != null && claimToProcess.RiskClaim != null)   // We have an external service to call
            {
                try
                {
                    batchId = (int)claimToProcess.RiskClaim.RiskBatch_Id;

                    int clientId = claimToProcess.RiskClient.Id;

                    RiskBatch riskBatch = riskServices.GetRiskBatch((int)batchId);

                    ctx.RiskClientId = clientId;
                    ctx.Who = claimToProcess.RiskClaim.CreatedBy;

                    Pipeline p = (Pipeline)CreatePipeline(riskBatch.BatchEntityType, clientId);

                    while (claimToProcess.RiskClaim != null)
                    {
                        #region Create new LOGFILE
                        if (ctx.TraceLoad) ADATrace.SwitchToNewUniqueLogFile(clientId, riskBatch.ClientBatchReference, "Process-" +
                            MDA.Common.Helpers.FileNameHelper.CreateCleanFileName(claimToProcess.RiskClaim.ClientClaimRefNumber));
                        #endregion

                        #region Trace
                        if (ctx.TraceLoad)
                        {
                            ADATrace.WriteLine("");
                            ADATrace.WriteLine("=========================================NEW PIPELINE (External)==========================================================");
                            ADATrace.WriteLine("Creating Pipeline(submitDirect " + p.SubmitClaimsFromPipeline.ToString() +
                                ", scoreDirect " + p.ScoreClaimsFromPipeline.ToString() + ", liveRules " + p.ScoreClaimsWithLiveRules.ToString());
                        }
                        #endregion

                        p.ProcessSingleClaimThirdParties(ctx, configSettings, (int)batchId, claimToProcess);

                        claimToProcess = riskServices.GetAClaimToProcessThirdPartiesFromBatch(batchId, ctx.SubmitDirect);
                    }

                    #region Now move all claims en-masse from ThirdPartyComplete to QueuedForScoring
                    if (!riskServices.IsABatchToLoadForClient(riskBatch.RiskClient_Id, ctx.SubmitDirect))
                        riskServices.SetAllClaimStatusInAllClientBatches(clientId, RiskClaimStatus.ThirdPartyComplete, RiskClaimStatus.QueuedForScoring);
                    #endregion

                    #region Database Backup
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["PerformDbBackups"]))
                    {
                        if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Perform LOGFILE backup of db : ", false);
                        if (ctx.TraceLoad)
                            ADATrace.WriteLine("Perform LOGFILE backup of db");

                        try
                        {
                            Stopwatch sw = new Stopwatch();
                            sw.Start();

                            // LOG Backup
                            riskServices.BackupTheDatabase("After External [" + riskBatch.Id + "]", 4,
                                configSettings.BackupDbPath, configSettings.BackupDbFolder);

                            riskServices.SaveBackupTiming(sw.ElapsedMilliseconds, BackupType.LogAfterExternal);


                            if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                            if (ctx.TraceLoad)
                                ADATrace.WriteLine("LOGFILE backup of db : Done");
                        }
                        catch (Exception ex)
                        {
                            while (ex != null)
                            {
                                ctx.ShowProgress(ProgressSeverity.Info, "Exception: " + ex.Message, true);
                                if (ctx.TraceLoad)
                                    ADATrace.WriteLine("LOGFILE backup of db : " + ex.Message);

                                ex = ex.InnerException;
                            }
                        }
                    }
                    #endregion
                }
                finally
                {
                    #region Trace
                    if (ctx.TraceLoad)
                    {
                        ADATrace.WriteLine("=========================================END PIPELINE (External)==========================================================");
                        ADATrace.WriteLine("");
                    }
                    #endregion

                    #region Switch back to default LOGFILE
                    ADATrace.SwitchToDefaultLogFile();
                    #endregion
                }

                return batchId;
            }
            else if (batchId != null)   // We have a batch with no external calls so just move them on (if there are any)
            {
                RiskBatch riskBatch = riskServices.GetRiskBatch((int)batchId);

                if (!riskServices.IsABatchToLoadForClient(riskBatch.RiskClient_Id, ctx.SubmitDirect))
                    riskServices.SetAllClaimStatusInAllClientBatches(riskBatch.RiskClient_Id, RiskClaimStatus.ThirdPartyComplete, RiskClaimStatus.QueuedForScoring);

                return batchId;
            }
            else  // batch is null. Look for any batches where all claims stuck on ThirdPartyComplete(6) or Archived(10) or in error (<0)
            {
                var batchIds = riskServices.FindBatchesStuckOnStatus(RiskClaimStatus.ThirdPartyComplete);

                foreach (var batch_Id in batchIds)
                {
                    RiskBatch riskBatch = riskServices.GetRiskBatch((int)batch_Id);

                    if (!riskServices.IsABatchToLoadForClient(riskBatch.RiskClient_Id, ctx.SubmitDirect))
                        riskServices.SetAllClaimStatusInAllClientBatches(riskBatch.RiskClient_Id, RiskClaimStatus.ThirdPartyComplete, RiskClaimStatus.QueuedForScoring);
                }
                return null;
            }
        }

        public static void ReProcessClaimQueueThirdParties(CurrentContext ctx, ServiceConfigSettings configSettings)
        {
            new RiskServices(ctx).ResetExternalServiceCallStatus();

            Stage4_ProcessClaimQueueThirdParties(ctx, configSettings, null);
        }

        private void ProcessSingleClaimThirdParties(CurrentContext ctx, ServiceConfigSettings configSettings, int batchId, RiskClaimToProcess claimToProcess)
        {
            if (claimToProcess == null) return;

            if (ctx.ShowProgress != null)
                ctx.ShowProgress(ProgressSeverity.Info, "External : " + claimToProcess.RiskClaim.ClientClaimRefNumber, false);

            int retryLimit     = Convert.ToInt32(ConfigurationManager.AppSettings["ThirdPartyRetryLimit"]);
            int retrySleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["ThirdPartyRetrySleepTimeMs"]);

            List<RiskExternalServicesRequest> externalServicesToProcessForThisClaim;

            using (var ctxx = new CurrentContext(ctx))   // Get all the external records that have never been processed (Status = created)
            {
                IRiskServices RiskService = CreateRiskService(ctxx);

                externalServicesToProcessForThisClaim = RiskService.GetExternalRequestRecords(claimToProcess.RiskClaim.Id, RiskExternalServiceCallStatus.Created);
            }

            foreach (var ep in externalServicesToProcessForThisClaim)
            {
                #region Build the service request object
                ExternalServiceCallRequest req = new ExternalServiceCallRequest()
                {
                    RiskClaim_Id = ep.RiskClaim_Id,
                    RiskExternalService_Id = ep.RiskExternalService_Id,
                    Unique_Id = ep.Unique_Id
                };
                #endregion

                using (var ctxx = new CurrentContext(ctx))
                {
                    ctxx.db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["ExternalCommandTimeout "]);

                    int retryCount = 0;
                    bool needRetry = true;

                    IRiskServices RiskService = CreateRiskService(ctxx);

                    #region Deal with service coming online or disappearing

                    // Check if service available. If not set status on all records in this batch the use this service to 
                    // Needs Retry (so scoring will take place) and refresh the list
                    if (!RiskService.IsRiskExternalServiceAvailable(ep.RiskExternalService_Id))
                    {
                        // Set ALL records for claims in this batch that use this service (that have not yet been called) to NeedsRetry
                        RiskService.SetRiskExternalRequestCallStatusForAllClaimsInBatch(batchId, ep.RiskExternalService_Id, RiskExternalServiceCallStatus.Created, RiskExternalServiceCallStatus.UnavailableNeedsRetry);

                        //RiskService.SetRiskExternalRequestCallStatus(ep.RiskClaim_Id, ep.RiskExternalService_Id, ep.Unique_Id, RiskExternalServiceCallStatus.UnavailableNeedsRetry);

                        // skip to next record
                        continue;
                    }
                    else
                    {
                        // we know service is (has become) available.  So reset ANY claim in ANY batch that has THIS ServiceID marked as RETRY
                        RiskService.ResetRetriesForServicesBeingUnavailable(null, ep.RiskExternalService_Id);
                    }
                    #endregion

                    while (needRetry) // loop around for retries
                    {
                        try
                        {
                            #region Create a DUMMY response (used if calls fails to pop db)
                            ExternalServiceCallResponse res = new ExternalServiceCallResponse() 
                            { 
                                CallStatus = RiskExternalServiceCallStatus.ServiceCallNotCoded,
                                ReturnedData = new Dictionary<string,object>()
                            };
                            #endregion

                            int batchRiskClientId;
                            bool submitAsBatch;

                            // Use pipeline method to create an instance of the correct external service
                            IExternalService extSrv = CreateExternalService(ep.RiskExternalService_Id, out submitAsBatch, out batchRiskClientId);

                            Stopwatch sw = Stopwatch.StartNew();

                            #region Call the external service
                            using (var transaction = ctxx.db.Database.BeginTransaction())
                            {
                                try
                                {
                                    if (TraceLoad)
                                        ADATrace.WriteLine("ProcessSingleClaimThirdParties:  Call Service [" + ep.RiskExternalService_Id + "]");

                                    // Call the BEFORE callback
                                    CreateClientService().BeforeExternal(ctxx, req);

                                    res = extSrv.ProcessPipelineClaim(ctxx, req);  // THE ACTUAL CALL????

                                    // If the returned data contains a KEY call "Return" this is a normal single return value to be
                                    // written to the DB. We can therefore throw away the dictionary object and just serialise the data
                                    // If it does not then assume it has several different collections of claims inside.
                                    object resData = null;
                                    
                                    if ( res.ReturnedData != null )
                                        resData = res.ReturnedData.ContainsKey("Return") ? res.ReturnedData["Return"] : res.ReturnedData;

                                    // Create the history record and save away the results and source data objects
                                    RiskService.CreateServiceRequestHistory(req.RiskClaim_Id, req.RiskExternalService_Id, req.Unique_Id, 
                                                            res.CallStatus, res.ServicesCalledMask, res.ErrorMessage, res.RequestData, resData, res.UseJsonSerialisation );

                                    // Call the AFTER callback
                                    CreateClientService().AfterExternal(ctxx, res);

                                    ctxx.db.SaveChanges();

                                    transaction.Commit();
                                }
                                catch (Exception)
                                {
                                    transaction.Rollback();
                                    throw;
                                }
                            }
                            #endregion

                            #region Check and submit return data back through the pipeline
                            if (submitAsBatch && res.CanCreateBatch)
                            {
                                try
                                {
                                    // We are going to get the PolicyTypes for each PolicyPipeline (motor, pi etc)
                                    var pp = ctx.PipelineSettings.PolicyPipelines.GetEnumerator();  // Walk over each PolicyPipeline section

                                    while (pp.MoveNext())
                                    {
                                        string policyType = ((PolicyPipeline)pp.Current).PolicyType;  // "Motor", "PI" etc

                                        // check if the return DICT<> has that type as a key. ie Do we have data of that type?
                                        if ( res.ReturnedData.ContainsKey(policyType) )
                                        {
                                            // Get the "name" of this service from Db
                                            RiskExternalService riskExtSrv = RiskService.GetRiskExternalService(ep.RiskExternalService_Id);

                                            // Create a new context but for DIFFERENT client. BatchRiskClient is taken from config and was
                                            // passed to use when we created the service instance. This is the value of the client that
                                            // has been configured to MAP the data we are about to try and push through the pipeline
                                            using (var ctxxx = new CurrentContext(ctx))
                                            {
                                                ctxxx.RiskClientId = batchRiskClientId;   // The client setup to read this data

                                                // Create a SourceDescription to put on every record created
                                                ctxxx.SourceDescription = riskExtSrv.ServiceName.ToUpper();

                                                MDA.Common.ProcessingResults processingResults;

                                                // Build a make-shift client ref
                                                string clientBatchRef = riskExtSrv.ServiceName + policyType + 
                                                    DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() +
                                                    DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString();

                                                // Call the pipeline, passing in the policyType and the correct matching data from DICT<>
                                                Stage1_SubmitExternalServiceData(ctxxx, policyType, res.ReturnedData[policyType], clientBatchRef, out processingResults);
                                            }
                                        }
                                    }
                                }
                                catch(Exception ex)
                                {
                                    if (ctx.ShowProgress != null)
                                    {
                                        ctx.ShowProgress(ProgressSeverity.Error, "Calling pipeline from external Stage 4. Failed", true);
                                    }

                                    if (TraceLoad)
                                    {
                                        ADATrace.WriteLine("Calling pipeline from external Stage 4. Failed");
                                        ADATrace.TraceException(ex);
                                        ADATrace.WriteLine("Ignored. Moving onto next record");
                                    }
                                }
                            }
                            #endregion

                            #region Trace
                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, " : Service " + sw.ElapsedMilliseconds + "ms", false);

                            if (TraceLoad)
                                ADATrace.WriteLine("ProcessSingleClaimThirdParties: Progress...Service Done [" + sw.ElapsedMilliseconds.ToString() + "ms" + "]");
                            #endregion

                            switch (res.CallStatus)
                            {
                                case RiskExternalServiceCallStatus.Success:
                                    needRetry = false;
                                    break;

                                case RiskExternalServiceCallStatus.FailedNeedsRetry:
                                    #region Trace
                                    if (TraceLoad)
                                        ADATrace.WriteLine("ProcessSingleClaimThirdParties: Failed Needs retry");
                                    #endregion
                                     if (retryCount++ < retryLimit)   // sleep and retry
                                    {
                                        #region Trace
                                        if (TraceLoad)
                                            ADATrace.WriteLine("ProcessSingleClaimThirdParties:  Service retry[" +
                                                                retryCount.ToString() + "] sleep[" +
                                                                retrySleepTime.ToString() + "]");

                                        if (ctx.ShowProgress != null)
                                            ctx.ShowProgress(ProgressSeverity.Info, 
                                                " : Service retry[" + retryCount.ToString() + "] sleep[" +
                                                retrySleepTime.ToString() + "]", false);
                                        #endregion

                                        Thread.Sleep(retrySleepTime);

                                        #region Trace
                                        if (TraceLoad)
                                            ADATrace.WriteLine(
                                                "ProcessSingleClaimThirdParties:  Service Retry Sleep Continuing");
                                        #endregion
                                    }
                                    else  // give up
                                    {
                                        #region Trace
                                        if (TraceLoad)
                                            ADATrace.WriteLine("ProcessSingleClaimThirdParties: Too many retries, give up");
                                        #endregion

                                        // Disable the service as after "n" retries it is still failing
                                        RiskService.SetRiskExternalServiceStatus(ep.RiskExternalService_Id, RiskExternalServiceCurrentStatus.AdaAutoDisabled, "Too many retries failed");

                                        needRetry = false;
                                    }
                                    break;

                                default: // all errors
                                    needRetry = false;
                                    break;
                            }

                        }
                        catch (Exception ex) // This is unexpected
                        {
                            #region Trace
                            if (TraceLoad)
                            {
                                ADATrace.WriteLine("ProcessSingleClaimThirdParties: Unexpected Exception:");
                                ADATrace.TraceException(ex);
                            }
                            #endregion

                            throw;
                        }
                    }
                }
            }
            

            #region Move Claim onto Pre-SCORING if all external services have been attempted

            using (var ctxx = new CurrentContext(ctx))
            {
                IRiskServices RiskService = CreateRiskService(ctxx);

                // Check is there are any service calls we have not tried (ie any  that are status 0)
                if (RiskService.AnyExternalServicesUntried(claimToProcess.RiskClaim.Id)) 
                {
                    if (ctx.ShowProgress != null)
                        ctx.ShowProgress(ProgressSeverity.Info, " : Not Completed (some services have not been tried - SHOULD NOT HAPPEN) : ", true);
                }
                else  // They have all been tried so move status of this claim to ThirdPartyComplete
                {
                    RiskService.SetRiskClaimStatus(claimToProcess.RiskClaim.Id, RiskClaimStatus.ThirdPartyComplete);

                    if (ctx.ShowProgress != null)
                        ctx.ShowProgress(ProgressSeverity.Info, " : Completed : ", true);
                }
            }

            #endregion
        }
    }
}
 