﻿using System;
using MDA.Common.Server;
using MDA.RiskService;
using MDA.MappingService.Interface;
using MDA.ClaimService.Interface;
using MDA.MatchingService.Interface;
using MDA.VerificationService.Interface;
using MDA.TracesmartService.Interface;
using MDA.CompHouseService.Interface;
using MDA.CleansingService.Interface;
using MDA.Pipeline.Config;
using System.Reflection;
using MDA.RiskService.Interface;
using RiskEngine.Interfaces;
using MDA.AddressService.Interface;
using MDA.ExternalService.Interface;
using MDA.ClientService.Interface;

namespace MDA.Pipeline
{

    public partial class Pipeline : IPipeline 
    {
        //public CurrentContext ctx { get; set; }
        //public CurrentContext ctxTemplate { get; set; }

        private PipelineConfiguration PipelineSettings { get; set; }
        private DefaultClientModule DefaultClientSettings { get; set; }
        private ClientModule CustomClientSettings { get; set; }

        public bool SubmitClaimsFromPipeline { get { return PipelineSettings.SubmitDirectFromPipeline.Value; } }
        public bool ScoreClaimsFromPipeline { get { return PipelineSettings.ScoreDirectFromPipeline.Value; } }
        public bool ScoreClaimsWithLiveRules { get { return PipelineSettings.ScoreClaimsWithLiveRules.Value; } }

        public bool TraceLoad { get { return PipelineSettings.TraceLoading.Value; } }
        public bool TraceScore { get { return PipelineSettings.TraceScore.Value; } }
        public bool TraceScoring { get { return PipelineSettings.TraceScoring.Value; } }

        public bool LoadAllBeforeScoring { get { return CustomClientSettings.LoadAllBatchesBeforeScoring; } }

        #region FACTORY Methods to dynamically create modules used by the pipline
        /// <summary>
        /// Factory method to create a RiskEngineService
        /// </summary>
        public IRiskEngine CreateRiskEngineService()
        {
            return new RiskEngine.Engine.RiskEngine();
        }

        /// <summary>
        /// Factory method to create a RiskService
        /// </summary>
        public IRiskServices CreateRiskService(CurrentContext ctx)
        {
            return new RiskServices(ctx);
        }

        /// <summary>
        /// Factory method to create a ClaimService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires injection of current context and 5 other services
        /// </summary>
        public IClaimService CreateClaimService(CurrentContext ctx)
        {
            var typeName = DefaultClientSettings.ClaimAssembly.TypeName;

            if (string.IsNullOrEmpty(typeName)) throw new Exception("ClaimService config missing");

            if (!string.IsNullOrEmpty(CustomClientSettings.ClaimAssembly.TypeName))
                typeName = CustomClientSettings.ClaimAssembly.TypeName;

            return (IClaimService)CreateInstance(typeName, "", 
                                        new object[]
                                        {
                                            ctx, 
                                            CreateCleansingService(ctx),
                                            CreateMatchingService(ctx), 
                                            CreateValidationService(), 
                                            CreateRiskService(ctx), 
                                            CreateRiskEngineService()
                                        });

        }

        /// <summary>
        /// Factory method to create a AddressService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires injection of "trace" flag
        /// </summary>
        public IAddressService CreateAddressService(CurrentContext ctx)
        {
            var typeName = DefaultClientSettings.AddressAssembly.TypeName;

            if (string.IsNullOrEmpty(typeName)) throw new Exception("AddressService config missing");

            if (!string.IsNullOrEmpty(CustomClientSettings.AddressAssembly.TypeName))
                typeName = CustomClientSettings.AddressAssembly.TypeName;

            return (IAddressService)CreateInstance(typeName, "", new object[] { ctx.TraceLoad });
        }

        /// <summary>
        /// Factory method to create a MatchingService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires injection of current context
        /// </summary>
        public IMatchingService CreateMatchingService(CurrentContext ctx)
        {
            var typeName = DefaultClientSettings.MatchingAssembly.TypeName;

            if (string.IsNullOrEmpty(typeName)) throw new Exception("MatchingService config missing");

            if (!string.IsNullOrEmpty(CustomClientSettings.MatchingAssembly.TypeName))
                typeName = CustomClientSettings.MatchingAssembly.TypeName;

            return (IMatchingService)CreateInstance(typeName, "", new object[] { ctx }); 
        }

        /// <summary>
        /// Factory method to create a MappingService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires no additional parameters
        /// </summary>
        public IMappingService CreateMappingService()
        {
            var typeName = DefaultClientSettings.MappingAssembly.TypeName;

            if (string.IsNullOrEmpty(typeName)) throw new Exception("MappingService config missing");

            if (!string.IsNullOrEmpty(CustomClientSettings.MappingAssembly.TypeName))
                typeName = CustomClientSettings.MappingAssembly.TypeName;

            return (IMappingService)CreateInstance(typeName, "");
        }

        /// <summary>
        /// Factory method to create a CleansingService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires the injection of current context and Address Service
        /// </summary>
        public ICleansingService CreateCleansingService(CurrentContext ctx)
        {
            var typeName = DefaultClientSettings.CleansingAssembly.TypeName;

            if (string.IsNullOrEmpty(typeName)) throw new Exception("CleansingService config missing");

            if (!string.IsNullOrEmpty(CustomClientSettings.CleansingAssembly.TypeName))
                typeName = CustomClientSettings.CleansingAssembly.TypeName;

            return (ICleansingService)CreateInstance(typeName, "", new object[] { ctx, CreateAddressService(ctx) });
        }

        /// <summary>
        /// Factory method to create a VerificationService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires no additional parameters
        /// </summary>
        public IVerificationService CreateVerificationService()
        {
            var typeName = DefaultClientSettings.VerificationAssembly.TypeName;

            if (string.IsNullOrEmpty(typeName)) throw new Exception("VerificationService config missing");

            if (!string.IsNullOrEmpty(CustomClientSettings.VerificationAssembly.TypeName))
                typeName = CustomClientSettings.VerificationAssembly.TypeName;

            return (IVerificationService)CreateInstance(typeName, "");
        }

        /// <summary>
        /// Factory method to create a ValidationService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires no additional parameters
        /// </summary>
        public IValidationService CreateValidationService()
        {
            var typeName = DefaultClientSettings.ValidationAssembly.TypeName;

            if (string.IsNullOrEmpty(typeName)) throw new Exception("ValidationService config missing");

            if (!string.IsNullOrEmpty(CustomClientSettings.ValidationAssembly.TypeName))
                typeName = CustomClientSettings.ValidationAssembly.TypeName;

            return (IValidationService)CreateInstance(typeName, "");
        }

        public IClientService CreateClientService()
        {
            var typeName = DefaultClientSettings.ClientAssembly.TypeName;

            if (string.IsNullOrEmpty(typeName)) throw new Exception("ClientService config missing");

            if (!string.IsNullOrEmpty(CustomClientSettings.ClientAssembly.TypeName))
                typeName = CustomClientSettings.ClientAssembly.TypeName;

            return (IClientService)CreateInstance(typeName, "");
        }

        /// <summary>
        /// Factory method to create a TracesmartService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires injection of current context
        /// </summary>
        //public ITracesmartService CreateTracesmartService()
        //{
        //    var typeName = DefaultClientSettings.ExternalServices.GetItemByKey("Tracesmart").TypeName;

        //    if (string.IsNullOrEmpty(typeName)) throw new Exception("TracesmartService config missing");

        //    if (!string.IsNullOrEmpty(CustomClientSettings.ExternalServices.GetItemByKey("Tracesmart").TypeName))
        //        typeName = CustomClientSettings.ExternalServices.GetItemByKey("Tracesmart").TypeName;

        //    return (ITracesmartService)CreateInstance(typeName, "");
        //}

        /// <summary>
        /// Factory method to create a CompHouseService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires injection of current context
        /// </summary>
        //public ICompHouseService CreateCompHouseService()
        //{
        //    var typeName = DefaultClientSettings.ExternalServices.GetItemByKey("Companies House").TypeName;

        //    if (string.IsNullOrEmpty(typeName)) throw new Exception("CompHouseService config missing");

        //    if (!string.IsNullOrEmpty(CustomClientSettings.ExternalServices.GetItemByKey("Companies House").TypeName))
        //        typeName = CustomClientSettings.ExternalServices.GetItemByKey("Companies House").TypeName;

        //    return (ICompHouseService)CreateInstance(typeName, "");
        //}

        public IExternalService CreateExternalService(int riskExtServiceId, out bool submitAsBatch, out int batchRiskClientId)
        {
            var typeName      = DefaultClientSettings.ExternalServices.GetItemByKey(riskExtServiceId).TypeName;
            batchRiskClientId = DefaultClientSettings.ExternalServices.GetItemByKey(riskExtServiceId).ClientId;
            submitAsBatch     = DefaultClientSettings.ExternalServices.GetItemByKey(riskExtServiceId).SubmitAsBatch;

            if (string.IsNullOrEmpty(typeName)) throw new Exception("External Service " + riskExtServiceId.ToString() + " config missing");

            if (CustomClientSettings.ExternalServices.Count > 0)
            {
                if (!string.IsNullOrEmpty(CustomClientSettings.ExternalServices.GetItemByKey(riskExtServiceId).TypeName))
                {
                    typeName = CustomClientSettings.ExternalServices.GetItemByKey(riskExtServiceId).TypeName;
                    batchRiskClientId = CustomClientSettings.ExternalServices.GetItemByKey(riskExtServiceId).ClientId;
                    submitAsBatch = CustomClientSettings.ExternalServices.GetItemByKey(riskExtServiceId).SubmitAsBatch;
                }
            }

            return (IExternalService)CreateInstance(typeName, "");
        }
        #endregion

        /// <summary>
        /// All parameters are defaulted. Called by Activator in CreateInstance below
        /// </summary>
        public Pipeline(PipelineConfiguration pipelineConfig, PolicyPipeline policyConfig, int riskClientId)
        {
            //this.ctx = new CurrentContext(cctx);
            //this.ctxTemplate = cctx;

            PipelineSettings = pipelineConfig;

            DefaultClientSettings = policyConfig.DefaultClientModules;
            CustomClientSettings = policyConfig.ClientModules.GetItemByKey(riskClientId.ToString());

            if (CustomClientSettings == null)
                throw new System.NotImplementedException("Client " + riskClientId.ToString() + " not defined in XML config");

           // DefaultClientSettings = cctx.DefaultClientSettings;
           // CustomClientSettings = cctx.CustomClientSettings; 

            //PipelineSettings      = ctx.PipelineSettings;
            //DefaultClientSettings = ctx.DefaultClientSettings;
            //CustomClientSettings  = ctx.CustomClientSettings;

            //this.ScoreClaimsWithLiveRules = PipelineSettings.ScoreClaimsWithLiveRules.Value;
            //this.SubmitClaimsFromPipeline = PipelineSettings.SubmitDirectFromPipeline.Value;
            //this.ScoreClaimsFromPipeline  = PipelineSettings.ScoreDirectFromPipeline.Value;
        }

        public void Dispose()
        {
        }

        public static IPipeline CreatePipeline(string policyType, int riskClientId)
        {
            PipelineConfiguration pc = MDA.Pipeline.Config.PipelineConfig.GetConfig;

           


            PolicyPipeline pType = pc.PolicyPipelines[policyType];

            return (IPipeline)CreateInstance(pc.PipelineAssembly.TypeName, "", new object[] { pc, pType, riskClientId });
        }

        /// <summary>
        /// method to create a "COPY" of the current context but with a new database connection
        /// </summary>
        /// <returns></returns>
        //private CurrentContext NewCurrentContext()
        //{
        //    ctx = new CurrentContext(ctxTemplate);

        //    //if (_mappingSvc != null) MappingService.SetDbContext = ctx.db;
        //    //if (_verificationSvc != null) VerificationService.SetDbContext = ctx.db;
        //    //if (_validationSvc != null) ValidationService.SetDbContext = ctx.db;

        //    //if (_matchingSvc != null) MatchingService.CurrentContext     = ctx;
        //    //if (_cleansingSvc != null)  CleansingService.CurrentContext  = ctx;
        //    //if (_tracesmartSvc != null) TracesmartService.CurrentContext = ctx;
        //    //if (_compHouseSvc != null)  CompHouseService.CurrentContext  = ctx;
        //    //if (_claimSvc != null) ClaimService.CurrentContext           = ctx;
        //    //if (_riskSvc != null) RiskService.CurrentContext             = ctx;

        //    return ctx;
        //}

        #region Code to dynamically load DLL and create INSTANCE
        /// <summary>
        /// Creates an instance of the object identified by this class.
        /// </summary>
        /// <param name="fullTypeName"></param>
        /// <param name="assemblyPath"></param>
        /// <param name="args">Parameters required by the object's constructor.</param>
        /// <returns>An instance of the specified type</returns>
        private static object CreateInstance(string fullTypeName, string assemblyPath, object[] args = null)
        {
            //object ans = null;
            string typeName;
            string assemblyName;
            string assemblyDll;

            if (fullTypeName == null || string.IsNullOrEmpty(fullTypeName)) return null;

            SplitType(fullTypeName, out typeName, out assemblyName, out assemblyDll);

            Assembly assemblyInstance = null;

            if (assemblyPath.Length > 0)
            {
                string path = assemblyPath;

                if (!path.EndsWith("\\")) path += "\\";

                assemblyInstance = Assembly.LoadFrom(path + assemblyDll);
            }
            else
                //  use full assembly name to get assembly instance
                assemblyInstance = Assembly.Load(assemblyName);

            //  use type name to get type from assembly
            Type type = assemblyInstance.GetType(typeName, true, false);

            if (args != null)
                return Activator.CreateInstance(type, args);

            return Activator.CreateInstance(type);
        }

        /// <summary>
        /// Takes incoming full type string, and splits the type into two strings, the typeName and assemblyName.
        /// This routine also cleans up any extra whitespace, and throws an exception if the full type string
        /// does not have five comma-delimited parts. Sets the private variables behind the public properties.
        /// </summary>
        /// <param name="fullType">The string to be parsed.</param>
        /// <param name="typeName"></param>
        /// <param name="assemblyName"></param>
        /// <param name="assemblyDll"></param>
        /// <exception cref="ArgumentException">Raised if the type is not a full 5-part name.</exception>
        private static void SplitType(string fullType, out string typeName, out string assemblyName, out string assemblyDll)
        {
            if (fullType == null)
                throw new ArgumentException("Invalid Type specified in config : fullType is NULL");

            string[] parts = fullType.Split(',');

            if (parts.Length == 5)
            {
                typeName     = parts[0].Trim();
                assemblyName = parts[1].Trim() + "," + parts[2].Trim() + "," + parts[3].Trim() + "," + parts[4].Trim();
                assemblyDll  = parts[1].Trim() + ".dll";
            }
            else
                throw new ArgumentException("Invalid Type specified in config :" + fullType);
        }
        #endregion
    }
}
