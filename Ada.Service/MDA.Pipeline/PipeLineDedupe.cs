﻿using System;
using MDA.Common.Debug;
using MDA.Common.Server;
using MDA.RiskService;
using MDA.RiskService.Interface;
using System.Configuration;
using System.Diagnostics;
using MDA.Common.Enum;

namespace MDA.Pipeline
{
    public partial class Pipeline
    {
        /// <summary>
        /// Call IBASE Sync
        /// </summary>
        /// <returns>0 - Complete, -1 - Busy, try again later, -2 - Sync Error needs manual intervention, -3 - Migration Error needs manual intervention</returns>
        public static int Synchronise(CurrentContext ctx, ServiceConfigSettings configSettings, bool beforeDeDupe)
        {
            ctx.db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["SyncCommandTimeout"]);

            IRiskServices riskServices = new RiskServices(ctx);

            #region Database Backup
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["PerformDbBackups"]))
            {
                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Perform LOG backup of db before SYNC: ", false);
                if (ctx.TraceLoad)
                    ADATrace.WriteLine("Perform LOG backup of db before SYNC");

                try
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();

                    // 4=Log will become 1-FULL automatically if no full backup done already or it is a new day
                    riskServices.BackupTheDatabase((beforeDeDupe) ? "Before SYNC, Before DEDUPE" : "Before SYNC, After DEDUPE", 4,
                        configSettings.BackupDbPath, configSettings.BackupDbFolder);

                    riskServices.SaveBackupTiming(sw.ElapsedMilliseconds, BackupType.BeforeSyncLog);

                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                    if (ctx.TraceLoad)
                        ADATrace.WriteLine("LOG backup of db : Done");
                }
                catch (Exception ex)
                {
                    while (ex != null)
                    {
                        if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Exception: " + ex.Message, true);
                        if (ctx.TraceLoad)
                            ADATrace.WriteLine("LOG backup of db before SYNC: " + ex.Message);

                        ex = ex.InnerException;
                    }

                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "FAILED", true);

                }
            }
            #endregion

            int ret = riskServices.SynchroniseDb();

            #region Database Backup
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["PerformDbBackups"]))
            {
                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Perform LOG backup of db after SYNC: ", false);
                if (ctx.TraceLoad)
                    ADATrace.WriteLine("Perform LOG backup of db after SYNC");

                try
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();

                    // 4=Log will become 1-FULL automatically if no full backup done already or it is a new day
                    riskServices.BackupTheDatabase((beforeDeDupe) ? "After SYNC, Before DEDUPE" : "After SYNC, After DEDUPE", 4,
                        configSettings.BackupDbPath, configSettings.BackupDbFolder);

                    riskServices.SaveBackupTiming(sw.ElapsedMilliseconds, BackupType.AfterSyncLog);

                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                    if (ctx.TraceLoad)
                        ADATrace.WriteLine("LOG backup of db : Done");
                }
                catch (Exception ex)
                {
                    while (ex != null)
                    {
                        if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Exception: " + ex.Message, true);
                        if (ctx.TraceLoad)
                            ADATrace.WriteLine("LOG backup of db after SYNC: " + ex.Message);

                        ex = ex.InnerException;
                    }

                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "FAILED", true);

                }
            }
            #endregion

            return ret;
        }

        public static void Deduplicate(CurrentContext ctx, ServiceConfigSettings config)
        {
            try
            {
                var riskClient = new RiskServices(ctx).GetRiskClient(0);

                ctx.RiskClientId      = 0;
                ctx.InsurersClientId  = riskClient.InsurersClients_Id;
                ctx.Who               = "ADA System";
                ctx.SourceDescription = "ADA DeDupe";
                ctx.SourceRef         = "IBASE-ADA";

                string fileName = string.Format("{0:D4}_{1:D2}_{2:D2}-{3:D2}_{4:D2}_{5:D2}-DeDupe",
                                  DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,
                                  DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                #region Logging
                if (ctx.TraceLoad)
                {
                    ADATrace.SwitchToNewUniqueLogFile(null, "DeDupe", fileName);
                    ADATrace.IndentLevel = 0;
                }
                #endregion

                using (IPipeline p = Pipeline.CreatePipeline("DeDupe", 0))
                {
                    ctx.db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["DedupeCommandTimeout"]);

                    try
                    {
                        p.CreateClaimService(ctx).Deduplicate(ctx);
                    }
                    catch (Exception ex)
                    {
                        if (ctx.TraceLoad) ADATrace.TraceException(ex);

                        while (ex != null)
                        {
                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, "Exception: " + ex.Message, true);

                            ex = ex.InnerException;
                        }

                        throw;
                    }
                }
            }
            finally
            {
                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Deduplicate Process Completed", true);
                }

                ADATrace.SwitchToDefaultLogFile();
                ADATrace.IndentLevel = 0;
            }

        }
    }
}
