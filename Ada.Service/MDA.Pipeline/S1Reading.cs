﻿using System;
using System.Diagnostics;
using System.IO;
using MDA.Common.Debug;
using MDA.DAL;
using MDA.Common.Server;
using MDA.RiskService;
using MDA.RiskService.Model;
using System.Data.Entity.Validation;
using MDA.RiskService.Interface;
using System.Configuration;
using MDA.VerificationService.Interface;
using System.Runtime.Serialization.Formatters.Binary;
using MDA.Common.Enum;

namespace MDA.Pipeline
{
    public partial class Pipeline
    {
        public static void PerformInitialFullBackup(CurrentContext ctx, ServiceConfigSettings config)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            // 0 does nothing, except if dir or file does not exist it becomes a 1, full backup
            new RiskServices(ctx).BackupTheDatabase("Initial Full Backup", 0, config.BackupDbPath, config.BackupDbFolder);

            IRiskServices riskServices = new RiskServices(ctx);

            riskServices.SaveBackupTiming(sw.ElapsedMilliseconds, BackupType.InititalFullBackup);

        }

        /// <summary>
        /// Process the file passed in. Will call the rest of the pipeline it SubmitDirect is true
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <param name="clientBatchReference"></param>
        /// <param name="processingResults"></param>
        /// <returns></returns>
        public static int? Stage1_ProcessClaimBatchFile(CurrentContext ctx, string batchEntityType, string filePath, string fileName, string clientBatchReference, out MDA.Common.ProcessingResults processingResults)
        {
            int? riskBatchId = null;

            ServiceConfigSettings config = new ServiceConfigSettings();

            try
            {
                #region Trace
                ADATrace.SwitchToDefaultLogFile();

                if (ctx.TraceLoad)
                {
                    ADATrace.WriteLine("");
                    ADATrace.WriteLine("=========================================NEW BATCH FILE==========================================================");
                    ADATrace.WriteLine("LoadClaimBatchFile: Opening file: " + filePath);
                }
                #endregion

                Pipeline pipeline = (Pipeline)CreatePipeline(batchEntityType, ctx.RiskClientId);

                riskBatchId = pipeline.ReadClaimBatchFile(ctx, batchEntityType, filePath, fileName, clientBatchReference, out processingResults);

                if (riskBatchId != null && riskBatchId >= 0)
                {
                    if (pipeline.SubmitClaimsFromPipeline)
                    {
                        riskBatchId = Stage2_ProcessBatchQueue(ctx, config, riskBatchId);

                        if ( riskBatchId != null )
                            Stage3_LoadBatchClaimsIntoDatabase(ctx, config, riskBatchId);

                        if (riskBatchId != null)
                        if (config.Process3rdParty)
                        {
                            Stage4_ProcessClaimQueueThirdParties(ctx, config, riskBatchId);
                        }
                    }

                    if (pipeline.ScoreClaimsFromPipeline)
                    {
                        if (riskBatchId != null)
                        {
                            if (config.ScoringMultithreaded)
                                Stage5_ScoreClaimQueueMultithreaded(ctx, config, riskBatchId);
                            else
                                Stage5_ScoreClaimQueueSingleProcess(ctx, config, riskBatchId);
                        }
                    }
                            
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                if (ctx.TraceLoad) ADATrace.TraceException(dbEx);

                throw;
            }
            catch (Exception ex)
            {
                if (ctx.TraceLoad) ADATrace.TraceException(ex);

                throw;
            }
            finally
            {
                if (ctx.TraceLoad)
                {
                    ADATrace.WriteLine("=========================================END BATCH FILE===========================================");
                }
            }

            return riskBatchId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <param name="clientBatchReference"></param>
        /// <param name="processingResults"></param>
        /// <returns></returns>
        private int? ReadClaimBatchFile(CurrentContext ctx, string batchEntityType, string filePath, string fileName, string clientBatchReference, out MDA.Common.ProcessingResults processingResults)
        {
            Debug.Assert(ctx.RiskClientId >= 0);
            Debug.Assert(!string.IsNullOrEmpty(filePath));
            Debug.Assert(!string.IsNullOrEmpty(ctx.Who));

            FileStream fs = File.OpenRead(filePath);

            string batchRef = "ADA" + DateTime.Now.Ticks.ToString();

            IRiskServices RiskService = CreateRiskService(ctx);

            RiskClient riskClient = RiskService.GetRiskClient(ctx.RiskClientId);

            ctx.db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["ReadingCommandTimeout"]);

            byte[] fileBytes = File.ReadAllBytes(filePath);

            using (var transaction = ctx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
            {
                try
                {
                    #region Create the batch record
                    if (TraceLoad)
                        ADATrace.WriteLine("_ReadClaimBatchFile: Creating Batch Record for client: " + ctx.RiskClientId.ToString());

                    int riskBatchId = RiskService.CreateRiskBatchRecord(RiskBatchStatus.Initialising, batchEntityType, ctx.RiskClientId, ctx.RiskUserId, 
                        batchRef, clientBatchReference, ctx.SubmitDirect, ctx.ScoreDirect, ctx.Who);

                    if (TraceLoad)
                        ADATrace.WriteLine("_ReadClaimBatchFile: Created RiskBatchRecord [" + riskBatchId.ToString() + "]");
                    #endregion

                    #region Quick verify

                    IVerificationService VerificationService = CreateVerificationService();

                    if (VerificationService != null)
                    {
                        if (TraceLoad)
                            ADATrace.WriteLine("_ReadClaimBatchFile: Before quick verify");

                        processingResults = VerificationService.QuickVerification(fs, ctx, riskClient.ExpectedFileExtension);

                        if (TraceLoad)
                            ADATrace.WriteLine("_ReadClaimBatchFile: After quick verify. ProcessResults Errors:" + processingResults.ErrorCount.ToString());

                        RiskService.SaveVerificationResults(riskBatchId, processingResults);

                        if (!processingResults.IsValid)
                        {
                            if (TraceLoad)
                            {
                                ADATrace.WriteLine("_ReadClaimBatchFile: Errors found. Set Batch to BAD");
                                ADATrace.DumpProcessingResults(processingResults);
                            }
                            RiskService.SetRiskBatchStatus(riskBatchId, RiskBatchStatus.Bad);

                            transaction.Commit();

                            return null;
                        }
                    }
                    else
                    {
                        processingResults = null;

                        if (TraceLoad)
                            ADATrace.WriteLine("_ReadClaimBatchFile: No quick verify defined.");
                    }
                    #endregion

                    // Save original file into database
                    RiskService.SaveFileIntoRiskBatchRecord(fileName, riskBatchId, fileBytes);

                    // Set batch to QUEUED
                    RiskService.SetRiskBatchStatus(riskBatchId, RiskBatchStatus.QueuedForCreating);

                    transaction.Commit();

                    return riskBatchId;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    fs.Close();
                }
            }
        }

        private int? ReadClaimBatchFile(CurrentContext ctx, string batchEntityType, object batch, string clientBatchReference, out MDA.Common.ProcessingResults processingResults)
        {
            Debug.Assert(ctx.RiskClientId >= 0);
            Debug.Assert(!string.IsNullOrEmpty(ctx.Who));

            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(ms, batch);


            string batchRef = "QUE" + DateTime.Now.Ticks.ToString();

            IRiskServices RiskService = CreateRiskService(ctx);

            RiskClient riskClient = RiskService.GetRiskClient(ctx.RiskClientId);

            ctx.db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["ReadingCommandTimeout"]);

            using (var transaction = ctx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
            {
                try
                {
                    #region Create the batch record
                    if (TraceLoad)
                        ADATrace.WriteLine("_ReadClaimBatchFile: Creating Batch Record for client: " + ctx.RiskClientId.ToString());

                    int riskBatchId = RiskService.CreateRiskBatchRecord(RiskBatchStatus.Initialising, batchEntityType, ctx.RiskClientId, ctx.RiskUserId, batchRef, clientBatchReference, ctx.SubmitDirect, ctx.ScoreDirect, ctx.Who);

                    if (TraceLoad)
                        ADATrace.WriteLine("_ReadClaimBatchFile: Created RiskBatchRecord [" + riskBatchId.ToString() + "]");
                    #endregion

                    #region Quick verify

                    IVerificationService VerificationService = CreateVerificationService();

                    if (VerificationService != null)
                    {
                        if (TraceLoad)
                            ADATrace.WriteLine("_ReadClaimBatchFile: Before quick verify");

                        processingResults = VerificationService.QuickVerification(ms, ctx, riskClient.ExpectedFileExtension);

                        if (TraceLoad)
                            ADATrace.WriteLine("_ReadClaimBatchFile: After quick verify. ProcessResults Errors:" + processingResults.ErrorCount.ToString());

                        RiskService.SaveVerificationResults(riskBatchId, processingResults);

                        if (!processingResults.IsValid)
                        {
                            if (TraceLoad)
                            {
                                ADATrace.WriteLine("_ReadClaimBatchFile: Errors found. Set Batch to BAD");
                                ADATrace.DumpProcessingResults(processingResults);
                            }
                            RiskService.SetRiskBatchStatus(riskBatchId, RiskBatchStatus.Bad);

                            transaction.Commit();

                            return null;
                        }
                    }
                    else
                    {
                        processingResults = null;

                        if (TraceLoad)
                            ADATrace.WriteLine("_ReadClaimBatchFile: No quick verify defined.");
                    }
                    #endregion

                    // Save original file into database
                    RiskService.SaveFileIntoRiskBatchRecord("QueBatch", riskBatchId, ms.ToArray());

                    // Set batch to QUEUED
                    RiskService.SetRiskBatchStatus(riskBatchId, RiskBatchStatus.QueuedForCreating);

                    transaction.Commit();

                    return riskBatchId;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    //fs.Close();
                }
            }
        }

        public static int? Stage1_SubmitExternalServiceData(CurrentContext ctx, string batchEntityType, object batch, string clientBatchReference, out MDA.Common.ProcessingResults processingResults)
        {
            int? riskBatchId = null;

            ServiceConfigSettings config = new ServiceConfigSettings();

            //MemoryStream ms = new MemoryStream();
            //BinaryFormatter bf = new BinaryFormatter();
            //bf.Serialize(ms, batch);

            try
            {
                #region Trace
                ADATrace.SwitchToDefaultLogFile();

                if (ctx.TraceLoad)
                {
                    ADATrace.WriteLine("");
                    ADATrace.WriteLine("=========================================NEW EXT BATCH FILE==========================================================");
                }
                #endregion

                Pipeline pipeline = (Pipeline)CreatePipeline(batchEntityType, ctx.RiskClientId);

                riskBatchId = pipeline.ReadClaimBatchFile(ctx, batchEntityType, batch, clientBatchReference, out processingResults);

                if (riskBatchId != null && riskBatchId >= 0)
                {
                    Stage2_ProcessBatchQueue(ctx, config, riskBatchId);
                    Stage3_LoadBatchClaimsIntoDatabase(ctx, config, riskBatchId);
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                if (ctx.TraceLoad) ADATrace.TraceException(dbEx);

                throw;
            }
            catch (Exception ex)
            {
                if (ctx.TraceLoad) ADATrace.TraceException(ex);

                throw;
            }
            finally
            {
                if (ctx.TraceLoad)
                {
                    ADATrace.WriteLine("=========================================END BATCH FILE===========================================");
                }
            }

            return riskBatchId;
        }
    }
}
