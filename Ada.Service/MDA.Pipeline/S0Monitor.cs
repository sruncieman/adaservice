﻿using MDA.Common.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.Pipeline.Config;
using System.IO;
using MDA.Common;
using System.Configuration;
using MDA.RiskService;
using MDA.DAL;
using MDA.RiskService;
using MDA.RiskService.Interface;

namespace MDA.Pipeline
{
    public partial class Pipeline
    {
        public static void Stage0_PerformFolderMonitoringCheck(CurrentContext ctx, ServiceConfigSettings config, 
                                   Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {

            try
            {
                var pp = ctx.PipelineSettings.PolicyPipelines.GetEnumerator();  // Walk over each PolicyPipeline section
                var msg = new StringBuilder();
                var countFolderCheck = 0;

                while (pp.MoveNext())
                {
                    //var PolicyPipelineSettings = ctx.PipelineSettings.PolicyPipelines["Motor"];
                    var PolicyPipelineConfig = (PolicyPipeline)pp.Current;

                    var cc = PolicyPipelineConfig.ClientModules.GetEnumerator(); // Walk over each ClientModule

                    while (cc.MoveNext())
                    {
                        MDA.Pipeline.Config.ClientModule c = (MDA.Pipeline.Config.ClientModule)(cc.Current);

                        if (c.MonitoredFolder != null && c.MonitoredFolder.SourceFolder.Length > 0)
                        {
                            string path = c.MonitoredFolder.SourceFolder;
                            string wildCard = c.MonitoredFolder.Wildcard;
                            string postOp = c.MonitoredFolder.PostOp;
                            string archivePath = c.MonitoredFolder.ArchiveFolder;
                            int batchSize = c.MonitoredFolder.BatchSize;
                            int settleTime = c.MonitoredFolder.SettleTime;
                            int userId = 0;
                            string userName = string.Empty;

                            int clientId = Convert.ToInt32(c.ClientId);

                            RiskServices rs = new RiskServices(ctx);


                            var autoLoader = rs.GetAutoloaderFromClientId(clientId);

                            userId = autoLoader.Id;
                            userName = autoLoader.UserName;
                            
                            if (userName != null)
                            {                                
                                using (var ctxx = new CurrentContext(clientId, userId, userName))
                                {
                                    using (IPipeline p = Pipeline.CreatePipeline(PolicyPipelineConfig.PolicyType, clientId))
                                    {
                                        try
                                        {
                                            RiskClient riskClient = rs.GetRiskClient(clientId);

                                            if (riskClient.Status == 1)
                                            {
                                                p.CreateMappingService().ProcessMonitoredFolder(ctxx, path, wildCard, settleTime, batchSize, postOp, archivePath, processClientBatch);
                                            }

                                            countFolderCheck++;
                                            if (msg.Length > 0)
                                            {
                                                msg.Append(",");
                                            }
                                            msg.Append(clientId);
                                        }
                                        catch (Exception ex)
                                        {
                                            ctx.ShowProgress(ProgressSeverity.Info, "Monitored Folder Check for client " + clientId.ToString() + " failed. " + ex.Message, true);
                                            throw;
                                        }
                                    }
                                }                                
                            }
                            
                        }
                    }                                                                
                }
                if (config.TraceMonitor)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, string.Format("Monitored {0} Folders for client Id's [{1}]", countFolderCheck, msg), true);
                }
            }
            catch (Exception ex)
            {
                ctx.ShowProgress(ProgressSeverity.Info, "Stage0 : FolderMonitoring failed. Possible pipeline config section problem. " + ex.Message, true);
                throw;
            }

        }
    }
}
