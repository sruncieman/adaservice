﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.Pipeline
{

    /// <summary>
    /// A class used by the Windows Service to Cache the values in the app.config file.
    /// </summary>
    public class ServiceConfigSettings
    {
        private bool _configChanged;

        public bool ConfigChanged
        {
            get
            {
                return _configChanged;
                //_configChanged = false;
                //return r;
            }

            set { _configChanged = value; }
        }

        private int _scoringThreadLimit;
        public int ScoringThreadLimit 
        { 
            get
            {
                return _scoringThreadLimit;
            }
            set
            {
                if ( value != _scoringThreadLimit )
                {
                    _scoringThreadLimit = value;
                    _configChanged = true;
                }
            }
        }

        private int _externalThreadLimit;
        public int ExternalThreadLimit
        {
            get
            {
                return _externalThreadLimit;
            }
            set
            {
                if (value != _externalThreadLimit)
                {
                    _externalThreadLimit = value;
                    _configChanged = true;
                }
            }
        }

        private int _loadingThreadLimit;
        public int LoadingThreadLimit
        {
            get
            {
                return _loadingThreadLimit;
            }
            set
            {
                if (value != _loadingThreadLimit)
                {
                    _loadingThreadLimit = value;
                    _configChanged = true;
                }
            }
        }

        private int _creatingThreadLimit;
        public int CreatingThreadLimit
        {
            get
            {
                return _creatingThreadLimit;
            }
            set
            {
                if (value != _creatingThreadLimit)
                {
                    _creatingThreadLimit = value;
                    _configChanged = true;
                }
            }
        }

        private bool _enableDedupe;
        public bool EnableDedupe
        {
            get
            {
                return _enableDedupe;
            }
            set
            {
                if (value != _enableDedupe)
                {
                    _enableDedupe = value;
                    _configChanged = true;
                }
            }
        }

        private bool _enableMonitoring;
        public bool EnableMonitoring
        {
            get
            {
                return _enableMonitoring;
            }
            set
            {
                if (value != _enableMonitoring)
                {
                    _enableMonitoring = value;
                    _configChanged = true;
                }
            }
        }

        private bool _enableSync;
        public bool EnableSync
        {
            get
            {
                return _enableSync;
            }
            set
            {
                if (value != _enableSync)
                {
                    _enableSync = value;
                    _configChanged = true;
                }
            }
        }

        private bool _enableBackups;
        public bool EnableBackups
        {
            get
            {
                return _enableBackups;
            }
            set
            {
                if (value != _enableBackups)
                {
                    _enableBackups = value;
                    _configChanged = true;
                }
            }
        }

        private bool _reqBatchReports;
        public bool ReqBatchReports
        {
            get
            {
                return _reqBatchReports;
            }
            set
            {
                if (value != _reqBatchReports)
                {
                    _reqBatchReports = value;
                    _configChanged = true;
                }
            }
        }

        private bool _processBatches;
        public bool ProcessBatches
        {
            get
            {
                return _processBatches;
            }
            set
            {
                if (value != _processBatches)
                {
                    _processBatches = value;
                    _configChanged = true;
                }
            }
        }

        private bool _scoringMultithreaded;
        public bool ScoringMultithreaded
        {
            get
            {
                return _scoringMultithreaded;
            }
            set
            {
                if (value != _scoringMultithreaded)
                {
                    _scoringMultithreaded = value;
                    _configChanged = true;
                }
            }
        }

        private bool _processLoading;
        public bool ProcessLoading
        {
            get
            {
                return _processLoading;
            }
            set
            {
                if (value != _processLoading)
                {
                    _processLoading = value;
                    _configChanged = true;
                }
            }
        }

        private bool _process3rdParty;
        public bool Process3rdParty
        {
            get
            {
                return _process3rdParty;
            }
            set
            {
                if (value != _process3rdParty)
                {
                    _process3rdParty = value;
                    _configChanged = true;
                }
            }
        }

        private bool _processScoring;
        public bool ProcessScoring
        {
            get
            {
                return _processScoring;
            }
            set
            {
                if (value != _processScoring)
                {
                    _processScoring = value;
                    _configChanged = true;
                }
            }
        }

        private int _serviceSleepTime;
        public int ServiceSleepTime
        {
            get
            {
                return _serviceSleepTime;
            }
            set
            {
                if (value != _serviceSleepTime)
                {
                    _serviceSleepTime = value;
                    _configChanged = true;
                }
            }
        }


        private int _monitorSleepTime;
        public int MonitorSleepTime
        {
            get
            {
                return _monitorSleepTime;
            }
            set
            {
                if (value != _monitorSleepTime)
                {
                    _monitorSleepTime = value;
                    _configChanged = true;
                }
            }
        }

        private int _monitorErrorRetrySleepTime;
        public int MonitorErrorRetrySleepTime
        {
            get
            {
                return _monitorErrorRetrySleepTime;
            }
            set
            {
                if (value != _monitorErrorRetrySleepTime)
                {
                    _monitorErrorRetrySleepTime = value;
                    _configChanged = true;
                }
            }
        }

        private int _threadSleepTime;
        public int ThreadSleepTime
        {
            get
            {
                return _threadSleepTime;
            }
            set
            {
                if (value != _threadSleepTime)
                {
                    _threadSleepTime = value;
                    _configChanged = true;
                }
            }
        }

        public bool TraceMonitor { get; set; }
        public string BackupDbPath { get; set; }
        public string BackupDbFolder { get; set; }

        public ServiceConfigSettings()
        {
            ProcessBatches             = Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessBatches"]);
            ProcessLoading             = Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessLoading"]);
            Process3rdParty            = Convert.ToBoolean(ConfigurationManager.AppSettings["Process3rdParty"]);
            ProcessScoring             = Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessScoring"]);
            EnableMonitoring           = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableMonitoring"]);
            EnableBackups              = Convert.ToBoolean(ConfigurationManager.AppSettings["PerformDbBackups"]);
            EnableSync                 = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSync"]);
            EnableDedupe               = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableDeDupe"]);
            ReqBatchReports            = Convert.ToBoolean(ConfigurationManager.AppSettings["RequestBatchReports"]);

            ScoringThreadLimit         = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfScoringThreads"]);
            ExternalThreadLimit        = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfExternalThreads"]);
            LoadingThreadLimit         = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfLoadingThreads"]);
            CreatingThreadLimit        = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfCreatingThreads"]);

            ScoringMultithreaded       = Convert.ToBoolean(ConfigurationManager.AppSettings["ScoringMultithreaded"]);
            


            ServiceSleepTime           = Convert.ToInt32(ConfigurationManager.AppSettings["ServiceSleepTimeInMs"]);
            ThreadSleepTime            = Convert.ToInt32(ConfigurationManager.AppSettings["ThreadSleepTimeInMs"]);
            MonitorSleepTime           = Convert.ToInt32(ConfigurationManager.AppSettings["MonitorSleepTimeInMs"]);
            MonitorErrorRetrySleepTime = Convert.ToInt32(ConfigurationManager.AppSettings["MonitorErrorRetrySleepTimeInMs"]);

            BackupDbPath               = ConfigurationManager.AppSettings["BackupDbPath"];
            BackupDbFolder             = ConfigurationManager.AppSettings["BackupDbFolder"];
            TraceMonitor               = Convert.ToBoolean(ConfigurationManager.AppSettings["TraceMonitor"]);
            ConfigChanged              = false;
        }        
    }
}
