﻿using MDA.AddressService.Interface;
using MDA.ClaimService.Interface;
using MDA.CleansingService.Interface;
using MDA.Common.Server;
using MDA.CompHouseService.Interface;
using MDA.ExternalService.Interface;
using MDA.MappingService.Interface;
using MDA.MatchingService.Interface;
using MDA.RiskService.Interface;
using MDA.TracesmartService.Interface;
using MDA.VerificationService.Interface;
using RiskEngine.Interfaces;
using System;

namespace MDA.Pipeline
{
    public interface IPipeline : IDisposable
    {
        bool SubmitClaimsFromPipeline { get; }
        bool ScoreClaimsFromPipeline { get; }
        bool ScoreClaimsWithLiveRules { get; }

        bool TraceLoad { get; }
        bool TraceScore { get; }
        bool TraceScoring { get; }

        /// <summary>
        /// Factory method to create a RiskEngineService
        /// </summary>
        IRiskEngine CreateRiskEngineService();

        /// <summary>
        /// Factory method to create a RiskEngineService
        /// </summary>
        IRiskServices CreateRiskService(CurrentContext ctx);

        /// <summary>
        /// Factory method to create a ClaimService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires injection of current context and 5 other services
        /// </summary>
        IClaimService CreateClaimService(CurrentContext ctx);

        /// <summary>
        /// Factory method to create a AddressService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires injection of "trace" flag
        /// </summary>
        IAddressService CreateAddressService(CurrentContext ctx);

        /// <summary>
        /// Factory method to create a MatchingService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires injection of current context
        /// </summary>
        IMatchingService CreateMatchingService(CurrentContext ctx);

        /// <summary>
        /// Factory method to create a MappingService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires no additional parameters
        /// </summary>
        IMappingService CreateMappingService();

        /// <summary>
        /// Factory method to create a CleansingService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires the injection of current context and Address Service
        /// </summary>
        ICleansingService CreateCleansingService(CurrentContext ctx);

        /// <summary>
        /// Factory method to create a VerificationService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires no additional parameters
        /// </summary>
        IVerificationService CreateVerificationService();

        /// <summary>
        /// Factory method to create a ValidationService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires no additional parameters
        /// </summary>
        IValidationService CreateValidationService();

        IExternalService CreateExternalService(int riskExtServiceId, out bool submitAsBatch, out int batchRiskClientId);

        /// <summary>
        /// Factory method to create a TracesmartService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires injection of current context
        /// </summary>
        //ITracesmartService CreateTracesmartService();

        /// <summary>
        /// Factory method to create a CompHouseService. The path to the DLL and instance stored in x.config. The
        /// constructor for this service requires injection of current context
        /// </summary>
        //ICompHouseService CreateCompHouseService();
    }
}
