﻿using System;
using System.Diagnostics;
using MDA.Common.Debug;
using MDA.DAL;
using MDA.Common.Server;
using MDA.RiskService;
using MDA.RiskService.Model;
using MDA.Common;
using System.Collections.Generic;
using MDA.RiskService.Interface;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using MDA.CleansingService.Interface;
using MDA.Common.Enum;

namespace MDA.Pipeline
{
    public partial class Pipeline
    {
        /// <summary>
        /// Find a BATCH with claims to process (BatchStatus=QUEUED) and set BatchStatus=CREATING CLAIMS.  
        /// Process each CLAIM in that BATCH whose status=QUEUED FOR LOADING
        /// </summary>
        /// <param name="ShowProgress">Callback function to show progress</param>
        public static int? Stage3_LoadBatchClaimsIntoDatabase(CurrentContext ctx, ServiceConfigSettings configSettings, int? batchId)
        {
            bool _trace = ctx.TraceLoad;

            IRiskServices riskServices = new RiskServices(ctx);

            #region Find batch where RiskBatchStatus.ClaimsQueuedForLoading

            var riskBatch = riskServices.GetABatchWithClaimsToLoad(batchId, ctx.SubmitDirect);

            #endregion

            if (riskBatch != null)
            {
                batchId = riskBatch.Id;

                try
                {
                    int clientId = riskBatch.RiskClient_Id;

                    ctx.RiskClientId = clientId;

                    var riskClient = riskServices.GetRiskClient(clientId);  // Will throw exception if not exists

                    bool scoreClosedClaims = riskClient.ScoreClosedClaims;

                    Pipeline p = (Pipeline)CreatePipeline(riskBatch.BatchEntityType, clientId);

                    #region Get a claim from batch where RiskClaimStatus.QueuedForLoading. RiskBatchStatus.ClaimsQueuedForLoading

                    TaskData1 td1a = new TaskData1() { ctx = ctx, batchId = (int)batchId } ;
                    p.GetNextClaimToPocess(td1a);
                    var claimToProcess = td1a.NextClaimToProcess;

                    if (claimToProcess == null || claimToProcess.RiskClaim == null)
                    {
                        if (_trace) ADATrace.WriteLine("LoadBatchClaimsIntoDatabase: Batch Empty : No Claims to Process");

                        p.MoveOntoNextStage(ctx, riskBatch, false, "ADA System");

                        return (int?)riskBatch.Id;
                    }
                    #endregion

                    ctx.Who = claimToProcess.RiskClaim.CreatedBy;
                    ctx.InsurersClientId = claimToProcess.RiskClient.InsurersClients_Id;

                    #region Trace
                    if (_trace)
                        ADATrace.WriteLine("LoadBatchClaimsIntoDatabase: Processing Batch [" + riskBatch.Id.ToString() + "]");
                    #endregion

                    while (claimToProcess.RiskClaim != null)
                    {
                        #region Trace
                        if (_trace) ADATrace.SwitchToNewUniqueLogFile(clientId, riskBatch.ClientBatchReference, "Process-" +
                                MDA.Common.Helpers.FileNameHelper.CreateCleanFileName(claimToProcess.RiskClaim.ClientClaimRefNumber));

                        if (_trace)
                        {
                            ADATrace.WriteLine("");
                            ADATrace.WriteLine("");
                            ADATrace.WriteLine("=========================================NEW PIPELINE (Loading)==========================================================");
                            ADATrace.WriteLine("Creating Pipeline(submitDirect " + p.SubmitClaimsFromPipeline.ToString() +
                                ", scoreDirect " + p.ScoreClaimsFromPipeline.ToString() + ", liveRules " + p.ScoreClaimsWithLiveRules.ToString());
                        }
                        #endregion

                        try
                        {
                            Task<int>[] tasks = new Task<int>[2];



                            TaskData1 td1 = new TaskData1() { ctx = ctx, batchId = (int)batchId };
                            TaskData2 td2 = new TaskData2() { ctx = ctx, ClaimToProcess = claimToProcess };

                            tasks[0] = Task<int>.Factory.StartNew(p.GetNextClaimToPocess, td1 );
                            tasks[1] = Task<int>.Factory.StartNew(p.ProcessSingleClaim, td2 );

                            Task.WaitAll(tasks);

                            int processTime = Math.Max(claimToProcess.CleanseProcessingTime, claimToProcess.LoadProcessingTime);

                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, string.Format("Load : {0} : Clean[{1}ms] : Load[{2}ms] : Done", claimToProcess.RiskClaim.ClientClaimRefNumber,
                                    claimToProcess.CleanseProcessingTime, claimToProcess.LoadProcessingTime), true);

                            #region Save loading times
                            riskServices.SaveRiskClaimLoadingTime(claimToProcess.RiskClaim.Id, processTime);
                            #endregion

                            claimToProcess = td1.NextClaimToProcess;
                        }
                        catch (Exception ex)  // something bad so stop
                        {
                            if (_trace) ADATrace.TraceException(ex);

                            throw;
                        }
                    }

                    p.MoveOntoNextStage(ctx, riskBatch, scoreClosedClaims, ctx.Who);

                    #region Database Backup
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["PerformDbBackups"]))
                    {
                        if (ctx.ShowProgress != null)ctx.ShowProgress(ProgressSeverity.Info, "Perform LOGFILE backup of db : ", false);
                        if (_trace)
                            ADATrace.WriteLine("Perform LOGFILE backup of db");

                        try
                        {
                            Stopwatch sw = new Stopwatch();
                            sw.Start();

                            // LOG Backup
                            riskServices.BackupTheDatabase("After Loading [" + riskBatch.Id + "]", 4,
                                configSettings.BackupDbPath, configSettings.BackupDbFolder);

                            riskServices.SaveBackupTiming(sw.ElapsedMilliseconds, BackupType.LogAfterLoading);


                            if (ctx.ShowProgress != null)ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                            if (_trace)
                                ADATrace.WriteLine("LOGFILE backup of db : Done");
                        }
                        catch (Exception ex)
                        {
                            while (ex != null)
                            {
                                ctx.ShowProgress(ProgressSeverity.Info, "Exception: " + ex.Message, true);
                                if (_trace)
                                    ADATrace.WriteLine("LOGFILE backup of db : " + ex.Message);

                                ex = ex.InnerException;
                            }
                        }
                    }
                    #endregion
                }
                finally
                {
                    #region Trace
                    if (_trace)
                    {
                        ADATrace.WriteLine("");
                        ADATrace.WriteLine("LoadBatchClaimsIntoDatabase: Ended...Returning BatchID [" + riskBatch.Id.ToString() + "]");
                        ADATrace.WriteLine("=========================================END PIPELINE (Loading)==========================================================");
                        ADATrace.WriteLine("");
                    }
                    #endregion

                    ADATrace.SwitchToDefaultLogFile();
                }

                return (int?)batchId;
            }

            return null;
        }

        private int GetNextClaimToPocess(object taskData)
        {
            TaskData1 td = taskData as TaskData1;

            using (var ctxx = new CurrentContext(td.ctx))
            {
                Stopwatch sw = Stopwatch.StartNew();
              
                IRiskServices riskServices = new RiskServices(ctxx);

                var nextClaimToProcess = riskServices.GetAClaimToLoadFromBatch((int)td.batchId);

                if (nextClaimToProcess != null && nextClaimToProcess.RiskClaim != null)
                {


                    nextClaimToProcess.CleansedPipelineClaim = CleanseSingleClaim(ctxx, nextClaimToProcess);

                    nextClaimToProcess.CleanseProcessingTime = (int)sw.ElapsedMilliseconds;
                }

                td.NextClaimToProcess = nextClaimToProcess;

            }

            return 1;
        }

        private void MoveOntoNextStage(CurrentContext ctx, RiskBatch riskBatch, bool scoreClosedClaims, string who)
        {
            using (var ctxx = new CurrentContext(ctx))
            {
                using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    try
                    {
                        IRiskServices RiskService = CreateRiskService(ctxx);

                        RiskService.CreateExternalRequestRecords(riskBatch.Id, who);
                            // Only for unknown, open, reopened claims

                        // Advance all non-open claims to Archived or 3rdComplete - to avoid 3rd party or scoring
                        RiskService.SetClaimStatusIfClosed(riskBatch.Id, RiskClaimStatus.LoadingComplete,
                            ((scoreClosedClaims) ? RiskClaimStatus.ThirdPartyComplete : RiskClaimStatus.Archived));
 
                        // Advance any claims with no external services (mask = 0) to ThirdPartyComplete
                        RiskService.SetClaimStatusToThirdPartyCompleteIfNoThirdParty(riskBatch.Id,
                            RiskClaimStatus.LoadingComplete);

                        // Advance all other claims to QueuedForThirdParty
                        RiskService.SetAllClaimStatusInBatch(riskBatch.Id, RiskClaimStatus.LoadingComplete,
                            RiskClaimStatus.QueuedForThirdParty);

                        // If no claims need external services ensure all claims get advanced to QueuedForScoring
                        if (RiskService.CountRiskClaimStatusForBatch(riskBatch.Id, RiskClaimStatus.QueuedForThirdParty) == 0 &&
                            (!LoadAllBeforeScoring || !RiskService.IsABatchToLoadForClient(riskBatch.RiskClient_Id, ctx.SubmitDirect)))
                        {
                            RiskService.SetAllClaimStatusInAllClientBatches(riskBatch.RiskClient_Id, RiskClaimStatus.ThirdPartyComplete,
                                RiskClaimStatus.QueuedForScoring);
                        }

                        RiskService.SetRiskBatchStatus(riskBatch.Id, RiskBatchStatus.BatchLoadComplete);

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        private MDA.Pipeline.Model.IPipelineClaim CleanseSingleClaim(CurrentContext ctx, RiskClaimToProcess claimToProcess)
        {
            IRiskServices RiskService = CreateRiskService(ctx);

            CreateClientService().BeforeCleansing(ctx, claimToProcess);

            MDA.Pipeline.Model.IPipelineClaim claimXml = null;

            claimXml = RiskService.RetrieveTheIClaim(claimToProcess.RiskClaim);

            //ctx.WordFilter = riskServices.GetRiskWordsForClient(ctx.riskClientId);

            #region Cleansing

            if (TraceLoad)
                ADATrace.WriteLine("ProcessSingleClaim: Cleanse Start");

            ProcessingResults cleanseResults = null;

            using (var ctxx = new CurrentContext(ctx))
            {
                ICleansingService CleansingService = CreateCleansingService(ctxx);

                if (CleansingService != null)
                    cleanseResults = CleansingService.CleanseTheClaim(claimXml);
            }

            using (var ctxx = new CurrentContext(ctx))
            {
                using (var transaction = ctxx.db.Database.BeginTransaction())
                {
                    try
                    {
                        IRiskServices riskService = CreateRiskService(ctxx);

                        riskService.SaveCleansingResults(claimToProcess.RiskClaim.Id, cleanseResults);

                        if (!cleanseResults.IsValid)
                        {
                            riskService.SetRiskClaimStatus(claimToProcess.RiskClaim.Id, RiskClaimStatus.FailedOnCleansing);

                            transaction.Commit();

                            return null;
                        }
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
            

            if (TraceLoad)
                ADATrace.WriteLine("ProcessSingleClaim: Cleanse Completed Successfully");

            #endregion

            CreateClientService().AfterCleansing(ctx, claimToProcess, claimXml);

            return claimXml;
        }

        private int ProcessSingleClaim(object taskData)
        {
            TaskData2 td = taskData as TaskData2;

            CreateClientService().BeforeLoading(td.ctx, td.ClaimToProcess);

            ProcessSingleClaim(td.ctx, td.ClaimToProcess, td.ShowProgress);

            CreateClientService().AfterLoading(td.ctx, td.ClaimToProcess);

            return 1;
        }

        /// <summary>
        /// Process (load into DB) a single Claim
        /// </summary>
        /// <param name="claimToProcess">Structure containing RiskClaim and RiskClient</param>
        /// <param name="ShowProgress">Progress callback function</param>
        private void ProcessSingleClaim(CurrentContext ctx, RiskClaimToProcess claimToProcess, Func<ProgressSeverity, string, bool, int> ShowProgress = null)
        {
            if (claimToProcess == null) return;

            Stopwatch sw = Stopwatch.StartNew();

            #region Trace
            //if (ShowProgress != null)
            //    ctx.ShowProgress(ProgressSeverity.Info, "Load : " + claimToProcess.RiskClaim.ClientClaimRefNumber + " : ", false);

            if (TraceLoad)
                ADATrace.WriteLine("ProcessSingleClaim: Progress...Starting [" + claimToProcess.RiskClaim.ClientClaimRefNumber + "]");
            #endregion

            try
            {
                //MDA.Pipeline.Model.PipelineMotorClaim claimXml = null;
                //MDA.Pipeline.Model.PipelineMobileClaim claimMobileXml = null;

                MDA.Pipeline.Model.IPipelineClaim claimXml = null;

                //switch (claimToProcess.RiskClaim.RiskBatch.BatchEntityType.ToUpper())
                //{
                //    case "MOBILE":
                //        claimMobileXml = claimToProcess.CleansedPipelineClaim as Model.PipelineMobileClaim;
                //        break;
                //    case "MOTOR":
                //        claimXml = claimToProcess.CleansedPipelineClaim as Model.PipelineMotorClaim;
                //        break;
                //}

                claimXml = claimToProcess.CleansedPipelineClaim as Model.IPipelineClaim;
                
                //if (claimToProcess.RiskClaim.RiskBatch.BatchEntityType == "Mobile")
                //{
                //    claimMobileXml = claimToProcess.CleansedPipelineClaim as Model.PipelineMobileClaim;
                //}
                //else
                //{
                //    claimXml = claimToProcess.CleansedPipelineClaim as Model.PipelineMotorClaim;
                //}
                

                #region Load Claim Into Database

                using (var ctxx = new CurrentContext(ctx))
                {
                    ctxx.db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["LoadingCommandTimeout "]);

                    using (var transaction = ctxx.db.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
                    {
                        try
                        {
                            IRiskServices RiskService = CreateRiskService(ctxx);

                            #region Trace
                            if (TraceLoad)
                            {
                                ADATrace.WriteLine("ProcessSingleClaim: Calling Pipeline");
                                ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                            }
                            #endregion

                            #region Process the claim

                            MDA.DAL.Incident incident = null;



                            //if (claimXml != null)
                            //{
                            //    incident = CreateClaimService(ctxx).LoadClaimIntoDatabase(claimXml, claimToProcess.RiskClaim);
                            //}
                            //else
                            //{
                            //    incident = CreateClaimService(ctxx).LoadClaimIntoDatabase(claimMobileXml, claimToProcess.RiskClaim);
                            //}

                            incident = CreateClaimService(ctxx).LoadClaimIntoDatabase(claimXml, claimToProcess.RiskClaim);

                            #endregion

                            #region Patch the Incident ID back into the RiskClaim record

                            RiskService.SetRiskClaimIncidentId(claimToProcess.RiskClaim.Id, incident.Id);

                            #endregion

                            #region Set the NextStatus value for Claim LoadingComplete or Archived

                            // If we have just loaded an OLD version of the claim then move straight to ARCHIVE
                            RiskClaimStatus nextStatus = (claimToProcess.RiskClaim.LatestVersion)
                                ? RiskClaimStatus.LoadingComplete
                                : RiskClaimStatus.Archived;

                            // If CLAIM outside active period in days move to ARCHIVE
                            if (claimToProcess.RiskClient.ClaimsActivePeriodInDays > 0)
                            {
                                DateTime lastClaimActiveDate =
                                    DateTime.Now.AddDays(-claimToProcess.RiskClient.ClaimsActivePeriodInDays);

                                // Set the RiskClaim.Status to Archived so it is not 3rd partied or scored
                                if (incident.IncidentDate < lastClaimActiveDate)
                                    nextStatus = RiskClaimStatus.Archived;
                            }

                            RiskService.SetRiskClaimStatus(claimToProcess.RiskClaim.Id, nextStatus);

                            #endregion

                            //#region Save loading times
                            //RiskService.SaveRiskClaimLoadingTime(claimToProcess.RiskClaim.Id, sw.ElapsedMilliseconds + claimToProcess.CleanseProcessingTime);
                            //#endregion

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            #region Trace
                            if (TraceLoad)
                            {
                                ADATrace.WriteLine("ProcessSingleClaim: Pipeline Exception occured:" + ex.Message);
                                ADATrace.TraceException(ex);
                            }
                            #endregion
                            if (transaction.UnderlyingTransaction.Connection != null)
                            {
                                transaction.Rollback();
                            }
                            throw;
                        }
                        finally
                        {
                            if (TraceLoad) ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                        }
                    }
                }

                claimToProcess.LoadProcessingTime = (int)sw.ElapsedMilliseconds;

                #region Trace


                if (TraceLoad)
                    ADATrace.WriteLine("ProcessSingleClaim: Progress...Done [" + sw.ElapsedMilliseconds + "ms" + "]");
                #endregion

                #endregion

            }
            catch (System.Data.Entity.Core.EntityException eex)   // not a data fault
            {
                #region Trace
                if (TraceLoad)
                    ADATrace.WriteLine("ProcessSingleClaim:  Process Claim EXCEPTION:" + eex.Message);

                if (ShowProgress != null)
                    ctx.ShowProgress(ProgressSeverity.Info, "Error[" + sw.ElapsedMilliseconds + "ms]", true);
                #endregion

                throw;
            }
            catch (Exception ex)
            {
                // Mark this RiskClaim as FAILED TO LOAD and stop future re-processing
                using (var ctxx = new CurrentContext(ctx))
                {
                    CreateRiskService(ctxx).SetRiskClaimStatus(claimToProcess.RiskClaim.Id, RiskClaimStatus.FailedToLoad);
                }

                #region Trace
                if (TraceLoad)
                {
                    ADATrace.WriteLine("ProcessSingleClaim:  Process Claim EXCEPTION:" + ex.Message);
                }

                if (ShowProgress != null)
                    ctx.ShowProgress(ProgressSeverity.Info, "Error[" + sw.ElapsedMilliseconds + "ms] ", true);
                #endregion

                //throw;
            }
        }

        /// <summary>
        /// Called from recovery.  look for batches which are stuck at ClaimsBeingLoaded. Having found some process them.
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="ShowProgress"></param>
        public static int Stage3_RecoverBatchesWithClaimsBeingLoaded(CurrentContext ctx, Func<ProgressSeverity, string, bool, int> ShowProgress = null)
        {
            bool _trace = ctx.TraceLoad;

            IRiskServices riskServices = new RiskServices(ctx);

            // Find batches where RiskBatchStatus.ClaimsQueuedForLoading
            List<RiskBatch> batches = riskServices.GetListOfBatchesWithClaimsBeingLoaded();

            try
            {
                foreach (var riskBatch in batches)
                {
                    var batchId = riskBatch.Id;

                    int clientId = riskBatch.RiskClient_Id;

                    var riskClient = riskServices.GetRiskClient(clientId);

                    Pipeline p = (Pipeline)CreatePipeline(riskBatch.BatchEntityType, clientId);

                    // Get a claim from batch where RiskClaimStatus.QueuedForLoading. RiskBatchStatus.ClaimsQueuedForLoading
                    TaskData1 td1a = new TaskData1() { ctx = ctx, batchId = batchId, ShowProgress = ShowProgress };

                    p.GetNextClaimToPocess(td1a);

                    var claimToProcess = td1a.NextClaimToProcess;

                    if (claimToProcess != null && claimToProcess.RiskClaim != null)
                    {
                        ctx.RiskClientId = clientId;
                        ctx.Who = claimToProcess.RiskClaim.CreatedBy;
                        ctx.InsurersClientId = claimToProcess.RiskClient.InsurersClients_Id;

                        bool scoreClosedClaims = claimToProcess.RiskClient.ScoreClosedClaims;

                        if (_trace)
                            ADATrace.WriteLine("RecoverBatchesWithClaimsBeingLoaded: Processing Batch [" + riskBatch.Id.ToString() + "]");

                        while (claimToProcess.RiskClaim != null)
                        {
                            if (_trace) ADATrace.SwitchToNewUniqueLogFile(clientId, riskBatch.ClientBatchReference, "Process-" +
                                MDA.Common.Helpers.FileNameHelper.CreateCleanFileName(claimToProcess.RiskClaim.ClientClaimRefNumber));

                            try
                            {
                                Task<int>[] tasks = new Task<int>[2];

                                TaskData1 td1 = new TaskData1() { ctx = ctx, batchId = batchId, ShowProgress = ShowProgress };
                                TaskData2 td2 = new TaskData2() { ctx = ctx, ClaimToProcess = claimToProcess, ShowProgress = ShowProgress };

                                tasks[0] = Task<int>.Factory.StartNew(p.GetNextClaimToPocess, td1);
                                tasks[1] = Task<int>.Factory.StartNew(p.ProcessSingleClaim, td2);

                                Task.WaitAll(tasks);

                                int processTime = Math.Max(claimToProcess.CleanseProcessingTime, claimToProcess.LoadProcessingTime);

                                if (ctx.ShowProgress != null)
                                    ctx.ShowProgress(ProgressSeverity.Info, string.Format("Load : {0} : Clean[{1}ms] : Load[{2}ms] : Done", claimToProcess.RiskClaim.ClientClaimRefNumber,
                                        claimToProcess.CleanseProcessingTime, claimToProcess.LoadProcessingTime), true);

                                #region Save loading times
                                riskServices.SaveRiskClaimLoadingTime(claimToProcess.RiskClaim.Id, processTime);
                                #endregion

                                claimToProcess = td1.NextClaimToProcess;
                            }
                            catch (Exception ex)  // something bad so stop
                            {
                                if (_trace) ADATrace.TraceException(ex);

                                throw;
                            }
                        }
                        p.MoveOntoNextStage(ctx, riskBatch, scoreClosedClaims, ctx.Who);
                    }
                }
            }
            finally
            {
                ADATrace.SwitchToDefaultLogFile();
            }

            return 0;
        }
    }

    class TaskData1
    {
        public CurrentContext ctx { get; set; }
        public int batchId { get; set; }
        public Func<ProgressSeverity, string, bool, int> ShowProgress { get; set; }

        public RiskClaimToProcess NextClaimToProcess { get; set; }
    }

    class TaskData2
    {
        public CurrentContext ctx { get; set; }
        public Func<ProgressSeverity, string, bool, int> ShowProgress { get; set; }

        public RiskClaimToProcess ClaimToProcess { get; set; }
    }
}
