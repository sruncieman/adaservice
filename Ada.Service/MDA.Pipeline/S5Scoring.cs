﻿using MDA.ClaimService.Interface;
using MDA.ClientService.Interface;
using MDA.Common.Debug;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.DAL;
using MDA.RiskService;
using MDA.RiskService.Interface;
using MDA.RiskService.Model;
using RiskEngine.Model;
using RiskEngine.Scoring.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace MDA.Pipeline
{
    public partial class Pipeline
    {
        private static int threadCount = 0;
        private static SortedList<long, RiskClaim> mScoreProcessList;
        private static List<Exception> mExceptions;
        private static CurrentContext mCurrentContext;
        private static bool _anyScoreTracing;
        private static Semaphore mSemaphore;
        private static object mLock;

        private class ScoreData
        {
            public CurrentContext ctx { get; set; }

            public RiskClaimToProcess ClaimToProcess { get; set; }
            public RiskBatch RiskBatch { get; set; }

            public string BatchEntityType { get; set; }
        }

        #region Original Single Process

        /// <summary>
        /// Score all the RiskClaims whose status is LOADED. It gets the riskClientId from the first claim found and then
        /// process all the claims for that client before moving onto the next client. If successful RiskClaim.Status becomes SCORED
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="batchId"></param>
        /// <param name="ShowProgress"></param>
        /// <param name="threads"></param>
        /// <param name="threadSleepTime"></param>
        public static int? Stage5_ScoreClaimQueueSingleProcess(CurrentContext ctx, ServiceConfigSettings config, int? batchId)
        {
            bool _anyScoreTracing = ctx.TraceScore || ctx.TraceScoring;

            // bool scoreWithLiveRules = this..ScoreClaimsWithLiveRules.Value;

            IRiskServices riskServices = new RiskServices(ctx);

            threadCount = 0;

            List<int> clientBatchIds = riskServices.GetBatchesForClientToScore(batchId, ctx.ScoreDirect);

            foreach (var bId in clientBatchIds)
            {
                batchId = bId;

                // Get a claim that needs scoring for ANY client
                var claimToProcess = riskServices.GetAClaimToScoreFromBatch(batchId, ctx.ScoreDirect);

                while (claimToProcess != null && claimToProcess.RiskClaim != null)   // Any claims for ANY client
                {
                    batchId = claimToProcess.RiskClaim.RiskBatch_Id;

                    int clientId = claimToProcess.RiskClient.Id;

                    RiskBatch riskBatch = riskServices.GetRiskBatch(claimToProcess.RiskClaim.RiskBatch_Id);

                    if (config.ScoringThreadLimit > 1)
                    {
                        while (threadCount > config.ScoringThreadLimit)
                            Thread.Sleep(config.ThreadSleepTime);
                    }

                    try
                    {
                        ctx.RiskClientId = clientId;
                        ctx.Who = claimToProcess.RiskClaim.CreatedBy;

                        if (config.ScoringThreadLimit > 1)
                        {
                            ScoreData data = new ScoreData()
                            {
                                ctx = ctx,
                                BatchEntityType = riskBatch.BatchEntityType,
                                ClaimToProcess = claimToProcess,
                                RiskBatch = riskBatch,
                                //ScoreWithLiveRules = scoreWithLiveRules,
                                //ShowProgress = ShowProgress
                            };

                            Thread t = new Thread(new ParameterizedThreadStart(_ScoreClaim));
                            t.Start(data);

                            threadCount++;
                        }
                        else
                        {
                            if (_anyScoreTracing) ADATrace.SwitchToNewUniqueLogFile(clientId, riskBatch.ClientBatchReference, "Process-" +
                                    MDA.Common.Helpers.FileNameHelper.CreateCleanFileName(claimToProcess.RiskClaim.ClientClaimRefNumber));

                            Pipeline p = (Pipeline)CreatePipeline(riskBatch.BatchEntityType, clientId);

                            if (_anyScoreTracing)
                            {
                                ADATrace.WriteLine("");
                                ADATrace.WriteLine("=========================================NEW PIPELINE (Scoring)==========================================================");
                                ADATrace.WriteLine("Creating Pipeline(submitDirect " + p.SubmitClaimsFromPipeline.ToString() +
                                    ", scoreDirect " + p.ScoreClaimsFromPipeline.ToString() + ", liveRules " + p.ScoreClaimsWithLiveRules.ToString());
                            }

                            p.ScoreSingleClaim(ctx, riskBatch, claimToProcess, ctx.ScoreWithLiveRules);
                        }
                    }
                    catch (System.Data.Entity.Core.EntityException eex)
                    {
                        if (_anyScoreTracing) ADATrace.TraceException(eex);

                        ADATrace.SwitchToDefaultLogFile();

                        ADATrace.TraceException(eex);

                        throw;
                    }
                    catch (Exception ex)
                    {
                        if (_anyScoreTracing) ADATrace.TraceException(ex);

                        ADATrace.SwitchToDefaultLogFile();

                        ADATrace.TraceException(ex);

                        //throw;
                    }
                    finally
                    {
                        if (_anyScoreTracing)
                        {
                            ADATrace.WriteLine("=========================================END PIPELINE (Scoring)==========================================================");
                            ADATrace.WriteLine("");
                        }

                        ADATrace.SwitchToDefaultLogFile();
                    }

                    // Get a claim that needs scoring for THIS client
                    claimToProcess = riskServices.GetAClaimToScoreFromBatch(batchId, ctx.ScoreDirect);
                }
            }

            #region Database Backup
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["PerformDbBackups"]))
            {
                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Perform LOGFILE backup of db : ", false);
                if (ctx.TraceLoad)
                    ADATrace.WriteLine("Perform LOGFILE backup of db");

                try
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();

                    // LOG Backup
                    riskServices.BackupTheDatabase("After Scoring [" + batchId + "]", 4,
                        config.BackupDbPath, config.BackupDbFolder);

                    riskServices.SaveBackupTiming(sw.ElapsedMilliseconds, BackupType.LogAfterScoring);

                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                    if (ctx.TraceLoad)
                        ADATrace.WriteLine("LOGFILE backup of db : Done");
                }
                catch (Exception ex)
                {
                    while (ex != null)
                    {
                        ctx.ShowProgress(ProgressSeverity.Info, "Exception: " + ex.Message, true);
                        if (ctx.TraceLoad)
                            ADATrace.WriteLine("LOGFILE backup of db : " + ex.Message);

                        ex = ex.InnerException;
                    }
                }
            }
            #endregion

            return batchId;
        }

        #endregion

        #region MultiThreaded (TPL)

        /// <summary>
        /// Score all the RiskClaims whose status is LOADED. It gets the riskClientId from the first claim found and then
        /// process all the claims for that client before moving onto the next client. If successful RiskClaim.Status becomes SCORED
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="batchId"></param>
        /// <param name="ShowProgress"></param>
        /// <param name="threads"></param>
        /// <param name="threadSleepTime"></param>
        public static int? Stage5_ScoreClaimQueueMultithreaded(CurrentContext ctx, ServiceConfigSettings config, int? batchId)
        {
            mCurrentContext = ctx;

            //int processorCount = Environment.ProcessorCount;
            //int maxNumberOfThreads = processorCount;

            var maxNumberOfThreads = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfScoringThreads"]);

            _anyScoreTracing = ctx.TraceScore || ctx.TraceScoring;

            int batchSize = 1;

            IRiskServices riskServices = new RiskServices(ctx);

            List<int> clientBatchIdsList = riskServices.GetBatchesForClientToScore(batchId, ctx.ScoreDirect);

            if (clientBatchIdsList.Count > 0)
                mSemaphore = new Semaphore(maxNumberOfThreads, maxNumberOfThreads);

            var factory = new TaskFactory(TaskCreationOptions.PreferFairness, TaskContinuationOptions.None);

            foreach (var bId in clientBatchIdsList)
            {
                batchId = bId;

                mScoreProcessList = new SortedList<long, RiskClaim>();

                // Get a claim that needs scoring for ANY client
                var listClaimToProcess = riskServices.GetClaimsToScoreFromBatch(Convert.ToInt32(batchId), ctx.ScoreDirect);

                foreach (var riskClaim in listClaimToProcess.RiskClaims)
                {
                    mScoreProcessList.Add(riskClaim.Id, riskClaim);
                }

                // get a list of all the key values to process
                List<long> allRiskClaimIDs = new List<long>(mScoreProcessList.Keys);

                while (allRiskClaimIDs.Count > 0)
                {
                    // make a list of riskclaim IDs to process in the next batch
                    List<long> riskClaimsIDsToProcess = allRiskClaimIDs.GetRange(0, System.Math.Min(batchSize, allRiskClaimIDs.Count));
                    // remove those riskClaim IDs from the master list so they are not processed again
                    allRiskClaimIDs.RemoveRange(0, System.Math.Min(batchSize, allRiskClaimIDs.Count));

                    // wait for the semaphore to let us launch another thread
                    mSemaphore.WaitOne();

                    foreach (var riskClaimId in riskClaimsIDsToProcess)
                    {
                        Task t = factory.StartNew(() =>
                        {
                            ScoreRiskClaim(riskClaimId);
                        });
                    }
                }
            }

            if (mSemaphore != null)
            {
                // ensure all threads have exited by waiting until we can get all the semaphore requests
                for (int ctr = 0; ctr < maxNumberOfThreads; ctr++)
                {
                    mSemaphore.WaitOne();
                }

                mSemaphore.Release(maxNumberOfThreads);
            }

            #region Database Backup

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["PerformDbBackups"]))
            {
                if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Perform LOGFILE backup of db : ", false);
                if (ctx.TraceLoad)
                    ADATrace.WriteLine("Perform LOGFILE backup of db");

                try
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();

                    // LOG Backup
                    riskServices.BackupTheDatabase("After Scoring [" + batchId + "]", 4,
                        config.BackupDbPath, config.BackupDbFolder);

                    riskServices.SaveBackupTiming(sw.ElapsedMilliseconds, BackupType.LogAfterScoring);

                    if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
                    if (ctx.TraceLoad)
                        ADATrace.WriteLine("LOGFILE backup of db : Done");
                }
                catch (Exception ex)
                {
                    while (ex != null)
                    {
                        ctx.ShowProgress(ProgressSeverity.Info, "Exception: " + ex.Message, true);
                        if (ctx.TraceLoad)
                            ADATrace.WriteLine("LOGFILE backup of db : " + ex.Message);

                        ex = ex.InnerException;
                    }
                }
            }

            #endregion Database Backup

            return batchId;
        }

        #endregion MultiThreaded (ThreadPool.QueueUserWorkItem)

        private static void ScoreClaimQueueInSubBatch(object state)
        {
            CurrentContext ctx = null;

            try
            {
                List<long> riskClaimIDsToProcess = state as List<long>;

                foreach (long riskClaimID in riskClaimIDsToProcess)
                {
                    ctx = new CurrentContext(mCurrentContext);
                    //ctx.ShowProgress(ProgressSeverity.Info, "Thread : " + Environment.CurrentManagedThreadId + " Started.", true);

                    IRiskServices riskServices = new RiskServices(ctx);

                    var rb = riskServices.GetRiskClaimParentBatch(Convert.ToInt32(riskClaimID));
                    mLock = new object();
                    lock (mLock)
                    {
                        using (Pipeline p = (Pipeline)CreatePipeline(rb.BatchEntityType, rb.RiskClient_Id))
                        {
                            var claimToProcess = new RiskClaimToProcess { RiskClient = rb.RiskClient, RiskClaim = riskServices.GetRiskClaim(Convert.ToInt32(riskClaimID)) };

                            ctx.RiskClientId = claimToProcess.RiskClient.Id;
                            ctx.Who = claimToProcess.RiskClaim.CreatedBy;

                            p.ScoreSingleClaim(ctx, rb, claimToProcess, mCurrentContext.ScoreWithLiveRules);
                        }
                    }
                }
            }
            catch (System.Data.Entity.Core.EntityException eex)
            {
                if (_anyScoreTracing) ADATrace.TraceException(eex);

                ADATrace.SwitchToDefaultLogFile();

                ADATrace.TraceException(eex);

                throw;
            }
            catch (Exception ex)
            {
                lock (mLock)
                {
                    if (_anyScoreTracing) ADATrace.TraceException(ex);

                    ADATrace.SwitchToDefaultLogFile();

                    ADATrace.TraceException(ex);

                    // An exception was raised. This thread has no access to the UI, so store the exception in
                    // mExceptions and get out.
                    ctx.ShowProgress(ProgressSeverity.Info, "Thread Exception : " + ex.ToString(), true);
                    mExceptions.Add(ex);
                }
            }
            finally
            {
                // the work in this thread is complete, so release the semaphore request so that it can be reused to
                // launch another thread worker for the next batch.

                if (_anyScoreTracing)
                {
                    ADATrace.WriteLine("=========================================END PIPELINE (Scoring)==========================================================");
                    ADATrace.WriteLine("");
                }

                ADATrace.SwitchToDefaultLogFile();

                //Queuectx.ShowProgress(ProgressSeverity.Info, "Thread : " + Environment.CurrentManagedThreadId + " Released.", true);
                mSemaphore.Release();
            }
        }

        private static void ScoreRiskClaim(long riskClaimId)
        {
            CurrentContext ctxxx = null;

            try
            {
                ctxxx = new CurrentContext(mCurrentContext);
                
                IRiskServices rServices = new RiskServices(ctxxx);

                var rb = rServices.GetRiskClaimParentBatch(Convert.ToInt32(riskClaimId));
                
                using (Pipeline p = (Pipeline)CreatePipeline(rb.BatchEntityType, rb.RiskClient_Id))
                {
                    var riskClaim = rServices.GetRiskClaim(Convert.ToInt32(riskClaimId));
                    
                    var claimToProcess = new RiskClaimToProcess { RiskClient = rb.RiskClient, RiskClaim = riskClaim };

                    ctxxx.RiskClientId = claimToProcess.RiskClient.Id;
                    ctxxx.Who = claimToProcess.RiskClaim.CreatedBy;

                    rServices.SetRiskClaimStatusToBeingScored(riskClaim);

                    p.ScoreSingleClaim(ctxxx, rb, claimToProcess, mCurrentContext.ScoreWithLiveRules);
                }
                 
            }
            catch (System.Data.Entity.Core.EntityException eex)
            {
                if (_anyScoreTracing) ADATrace.TraceException(eex);

                ADATrace.SwitchToDefaultLogFile();

                ADATrace.TraceException(eex);

                throw;
            }
            catch (AggregateException ex)
            {
                ctxxx.ShowProgress(ProgressSeverity.Info, "Thread Exception : " + ex.ToString(), true);
                mExceptions.Add(ex);
            }
            catch (Exception eeex)
            {
                if (_anyScoreTracing) ADATrace.TraceException(eeex);

                ADATrace.SwitchToDefaultLogFile();

                ADATrace.TraceException(eeex);

                //throw;
            }
            finally
            {
                // the work in this thread is complete, so release the semaphore request so that it can be reused to
                // launch another thread worker for the next batch.

                if (_anyScoreTracing)
                {
                    ADATrace.WriteLine("=========================================END PIPELINE (Scoring)==========================================================");
                    ADATrace.WriteLine("");
                }

                ADATrace.SwitchToDefaultLogFile();

                mSemaphore.Release();
            }
        }

        /// <summary>
        /// Score all the RiskClaims whose status is LOADED. It gets the riskClientId from the first claim found and then
        /// process all the claims for that client before moving onto the next client. If successful RiskClaim.Status becomes SCORED
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="batchId"></param>
        /// <param name="ShowProgress"></param>
        /// <param name="threads"></param>
        /// <param name="threadSleepTime"></param>
        //public static int? Stage5_ScoreClaimQueue(CurrentContext ctx, ServiceConfigSettings config, int? batchId)
        //{
        //    bool _anyScoreTracing = ctx.TraceScore || ctx.TraceScoring;

        //   // bool scoreWithLiveRules = this..ScoreClaimsWithLiveRules.Value;

        //    IRiskServices riskServices = new RiskServices(ctx);

        //    threadCount = 0;

        //    List<int> clientBatchIds = riskServices.GetBatchesForClientToScore(batchId, ctx.ScoreDirect);

        //    foreach (var bId in clientBatchIds)
        //    {
        //        batchId = bId;

        //        // Get a claim that needs scoring for ANY client
        //        var claimToProcess = riskServices.GetAClaimToScoreFromBatch(batchId, ctx.ScoreDirect);

        //        while (claimToProcess != null && claimToProcess.RiskClaim != null)   // Any claims for ANY client
        //        {
        //            batchId = claimToProcess.RiskClaim.RiskBatch_Id;

        //            int clientId = claimToProcess.RiskClient.Id;

        //            RiskBatch riskBatch = riskServices.GetRiskBatch(claimToProcess.RiskClaim.RiskBatch_Id);

        //            if (config.ScoringThreadLimit > 1)
        //            {
        //                while (threadCount > config.ScoringThreadLimit)
        //                    Thread.Sleep(config.ThreadSleepTime);
        //            }

        //            try
        //            {
        //                ctx.RiskClientId = clientId;
        //                ctx.Who = claimToProcess.RiskClaim.CreatedBy;

        //                if (config.ScoringThreadLimit > 1)
        //                {
        //                    ScoreData data = new ScoreData()
        //                    {
        //                        ctx = ctx,
        //                        BatchEntityType = riskBatch.BatchEntityType,
        //                        ClaimToProcess = claimToProcess,
        //                        RiskBatch = riskBatch,
        //                        //ScoreWithLiveRules = scoreWithLiveRules,
        //                        //ShowProgress = ShowProgress
        //                    };

        //                    Thread t = new Thread(new ParameterizedThreadStart(_ScoreClaim));
        //                    t.Start(data);

        //                    threadCount++;
        //                }
        //                else
        //                {
        //                    if (_anyScoreTracing) ADATrace.SwitchToNewUniqueLogFile(clientId, riskBatch.ClientBatchReference, "Process-" +
        //                            MDA.Common.Helpers.FileNameHelper.CreateCleanFileName(claimToProcess.RiskClaim.ClientClaimRefNumber));

        //                    Pipeline p = (Pipeline)CreatePipeline(riskBatch.BatchEntityType, clientId);

        //                    if (_anyScoreTracing)
        //                    {
        //                        ADATrace.WriteLine("");
        //                        ADATrace.WriteLine("=========================================NEW PIPELINE (Scoring)==========================================================");
        //                        ADATrace.WriteLine("Creating Pipeline(submitDirect " + p.SubmitClaimsFromPipeline.ToString() +
        //                            ", scoreDirect " + p.ScoreClaimsFromPipeline.ToString() + ", liveRules " + p.ScoreClaimsWithLiveRules.ToString());
        //                    }

        //                    p.ScoreSingleClaim(ctx, riskBatch, claimToProcess, ctx.ScoreWithLiveRules);
        //                }
        //            }
        //            catch (System.Data.Entity.Core.EntityException eex)
        //            {
        //                if (_anyScoreTracing) ADATrace.TraceException(eex);

        //                ADATrace.SwitchToDefaultLogFile();

        //                ADATrace.TraceException(eex);

        //                throw;
        //            }
        //            catch (Exception ex)
        //            {
        //                if (_anyScoreTracing) ADATrace.TraceException(ex);

        //                ADATrace.SwitchToDefaultLogFile();

        //                ADATrace.TraceException(ex);

        //                //throw;
        //            }
        //            finally
        //            {
        //                if (_anyScoreTracing)
        //                {
        //                    ADATrace.WriteLine("=========================================END PIPELINE (Scoring)==========================================================");
        //                    ADATrace.WriteLine("");
        //                }

        //                ADATrace.SwitchToDefaultLogFile();
        //            }

        //            // Get a claim that needs scoring for THIS client
        //            claimToProcess = riskServices.GetAClaimToScoreFromBatch(batchId, ctx.ScoreDirect);
        //        }
        //    }

        //    #region Database Backup
        //    if (Convert.ToBoolean(ConfigurationManager.AppSettings["PerformDbBackups"]))
        //    {
        //        if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Perform LOGFILE backup of db : ", false);
        //        if (ctx.TraceLoad)
        //            ADATrace.WriteLine("Perform LOGFILE backup of db");

        //        try
        //        {
        //            Stopwatch sw = new Stopwatch();
        //            sw.Start();

        //            // LOG Backup
        //            riskServices.BackupTheDatabase("After Scoring [" + batchId + "]", 4,
        //                config.BackupDbPath, config.BackupDbFolder);

        //            riskServices.SaveBackupTiming(sw.ElapsedMilliseconds, BackupType.LogAfterScoring);

        //            if (ctx.ShowProgress != null) ctx.ShowProgress(ProgressSeverity.Info, "Done", true);
        //            if (ctx.TraceLoad)
        //                ADATrace.WriteLine("LOGFILE backup of db : Done");
        //        }
        //        catch (Exception ex)
        //        {
        //            while (ex != null)
        //            {
        //                ctx.ShowProgress(ProgressSeverity.Info, "Exception: " + ex.Message, true);
        //                if (ctx.TraceLoad)
        //                    ADATrace.WriteLine("LOGFILE backup of db : " + ex.Message);

        //                ex = ex.InnerException;
        //            }
        //        }
        //    }
        //    #endregion

        //    return batchId;
        //}

        /// <summary>
        /// Score the RiskClaim provided.
        /// </summary>
        /// <param name="riskBatch">The claim to be scored</param>
        /// <param name="claimToScore">The claim to be scored</param>
        /// <param name="scoreWithLiveRules">Force scoring with live rules only</param>
        /// <param name="ShowProgress">Progress callback</param>
        private void ScoreSingleClaim(CurrentContext ctx, RiskBatch riskBatch, RiskClaimToProcess claimToScore, bool scoreWithLiveRules)
        {
            if (claimToScore == null) return;

            //ctx = new CurrentContext(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            bool multiThreaded = Convert.ToBoolean(ConfigurationManager.AppSettings["ScoringMultithreaded"]);

            #region Trace

            if (ctx.ShowProgress != null)
                ctx.ShowProgress(ProgressSeverity.Info, "Score : " + claimToScore.RiskClaim.ClientClaimRefNumber, false);

            if (TraceScore)
                ADATrace.WriteLine("ScoreSingleClaim: Progress...Starting RiskClaimId[" + claimToScore.RiskClaim.Id.ToString() + "] ClientCRef[" + claimToScore.RiskClaim.ClientClaimRefNumber + "]");

            #endregion Trace

            try
            {
                ClaimScoreResults scores = null;

                bool unableToScore = false;

                if (claimToScore.RiskClaim.LatestVersion)
                {
                    #region Trace

                    if (TraceScoring)
                    {
                        ADATrace.WriteLine("--------------------");
                        ADATrace.WriteLine("ScoreSingleClaim: Calling ScoreClaim");
                        ADATrace.IndentLevel = ADATrace.IndentLevel + 1;
                    }

                    #endregion Trace

                    #region Score the claim

                    using (var ctxx = new CurrentContext(ctx))
                    {
                        ctxx.db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["ScoringCommandTimeout "]);

                        IClientService clientService = CreateClientService();

                        try
                        {
                            IClaimService claimService = CreateClaimService(ctxx);

                            clientService.BeforeScore(ctxx, claimToScore, scoreWithLiveRules);

                            // this is outside transaction as it creates its own context
                            scores = claimService.ScoreRiskEntityMap(claimToScore.RiskClaim.Id, claimToScore.RiskClient.Id,
                                                                    scoreWithLiveRules, claimToScore.RiskClaim.CreatedBy);
                        }
                        catch (ScoringException sex)
                        {
                            scores = new ClaimScoreResults();

                            unableToScore = true;

                            scores.StatusReturnCode = sex.ReturnCode;
                        }
                        catch (Exception ex)
                        {
                            scores = new ClaimScoreResults();

                            unableToScore = true;
                        }
                        finally
                        {
                            clientService.AfterScore(ctxx, claimToScore, scoreWithLiveRules, unableToScore, scores);
                        }
                    }

                    #endregion Score the claim

                    #region Trace

                    if (TraceScoring)
                    {
                        ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                        ADATrace.WriteLine("ScoreSingleClaim: Calling ScoreClaim Complete");
                        ADATrace.WriteLine("--------------------");
                    }

                    #endregion Trace
                }

                #region Save the results and set status to SCORED

                using (var ctxx = new CurrentContext(ctx))
                {
                    ctxx.db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["ScoringCommandTimeout "]);

                    using (var transaction = ctxx.db.Database.BeginTransaction())
                    {
                        try
                        {
                            IRiskServices RiskService = CreateRiskService(ctxx);

                            if (claimToScore.RiskClaim.LatestVersion)
                            {
                                #region Trace

                                if (TraceScoring)
                                {
                                    ADATrace.WriteLine("Creating RISKCLAIMRUN Record");
                                    ADATrace.WriteLine("Serialising Results");
                                    ADATrace.IndentLevel += 1;
                                }

                                #endregion Trace

                                // Create a new RiskClaimRun record
                                if (multiThreaded)
                                    RiskService.CreateRiskClaimRun(claimToScore.RiskClaim.Id, scores, riskBatch.ClientBatchReference, claimToScore.RiskClaim.CreatedBy, sw.ElapsedMilliseconds / Environment.ProcessorCount);
                                else
                                    RiskService.CreateRiskClaimRun(claimToScore.RiskClaim.Id, scores, riskBatch.ClientBatchReference, claimToScore.RiskClaim.CreatedBy, sw.ElapsedMilliseconds);

                                #region Trace

                                if (TraceScoring)
                                {
                                    ADATrace.IndentLevel -= 1;
                                    ADATrace.WriteLine("Setting RISKCLAIM status to SCORED");
                                }

                                #endregion Trace

                                // Set the RiskClaim Status to SCORED
                                RiskService.SetRiskClaimStatus(claimToScore.RiskClaim.Id,
                                    ((unableToScore)
                                        ? RiskClaimStatus.FailedUnableToScoreKeoghsCFSCase
                                        : RiskClaimStatus.Scored));

                                if (scores.TotalScore == 0)
                                {
                                    int CurrentClaimReadStatus;

                                    RiskService.SetClaimReadStatus(claimToScore.RiskClaim.Id, (int)RiskClaimReadStatus.Read, ctx.RiskUserId, out CurrentClaimReadStatus);
                                }

                                #region Trace

                                if (TraceScoring)
                                {
                                    ADATrace.WriteLine("Setting RISKCLAIM ClaimReadStatus to READ");
                                }

                                #endregion Trace
                            }
                            else
                            {
                                #region Trace

                                if (TraceScoring)
                                    ADATrace.WriteLine("Scoring skipped, not latest version of the claim");

                                #endregion Trace

                                RiskService.SetRiskClaimStatus(claimToScore.RiskClaim.Id, RiskClaimStatus.Archived);
                            }

                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }

                #endregion Save the results and set status to SCORED

                #region Trace

                if (ctx.ShowProgress != null)
                {
                    string scoreStr = string.Format("{0,3}", ((scores != null) ? scores.TotalScore.ToString() : "ERR"));
                    
                    if (multiThreaded)
                        ctx.ShowProgress(ProgressSeverity.Info, " : Done [" + scoreStr + "] : " + sw.ElapsedMilliseconds/Environment.ProcessorCount + "ms", true);
                    else
                        ctx.ShowProgress(ProgressSeverity.Info, " : Done [" + scoreStr + "] : " + sw.ElapsedMilliseconds + "ms", true);
                }

                if ((TraceScore || TraceScoring) && scores != null)
                {
                    ADATrace.WriteLine("---------------------------------------------");
                    if (multiThreaded)
                        ADATrace.WriteLine("|ScoreSingleClaim: Progress...Done [" + sw.ElapsedMilliseconds/Environment.ProcessorCount + "ms" + "]");
                    else
                        ADATrace.WriteLine("|ScoreSingleClaim: Progress...Done [" + sw.ElapsedMilliseconds + "ms" + "]");
                    ADATrace.WriteLine("|Score    = " + scores.TotalScore);
                    ADATrace.WriteLine("|KA Count = " + scores.TotalKeyAttractorCount);
                    ADATrace.WriteLine("---------------------------------------------");
                }

                #endregion Trace
            }
            catch (System.Data.Entity.Core.EntityException eex)   // An exception that stops processing of this and all other claims
            {
                #region Trace

                if ((TraceScore || TraceScoring))
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("ScoreSingleClaim: Scoring Exception:" + eex.Message);
                }

                if (ctx.ShowProgress != null)
                {
                    if (multiThreaded)
                        ctx.ShowProgress(ProgressSeverity.Info, " : Error : " + sw.ElapsedMilliseconds / Environment.ProcessorCount + "ms", true);
                    else
                        ctx.ShowProgress(ProgressSeverity.Info, " : Error : " + sw.ElapsedMilliseconds  + "ms", true);

                }

                #endregion Trace

                throw;
            }
            catch (Exception ex)   // An exception that stops this claim being processed.  Mark as FAILED_TO_SCORE and continue
            {
                #region Trace

                if ((TraceScore || TraceScoring))
                {
                    ADATrace.IndentLevel = ADATrace.IndentLevel - 1;
                    ADATrace.WriteLine("ScoreSingleClaim: Scoring Exception:" + ex.Message);
                }

                if (ctx.ShowProgress != null)
                {
                    if (multiThreaded)
                        ctx.ShowProgress(ProgressSeverity.Info, " : Error : " + sw.ElapsedMilliseconds / Environment.ProcessorCount + "ms", true);
                    else
                        ctx.ShowProgress(ProgressSeverity.Info, " : Error : " + sw.ElapsedMilliseconds + "ms", true);    

                }

                #endregion Trace

                IRiskServices RiskService = CreateRiskService(ctx);

                // Set the RiskClaim.Status to BAD
                RiskService.SetRiskClaimStatus(claimToScore.RiskClaim.Id, RiskClaimStatus.FailedToScore);

                throw;
            }
        }

        private static void _ScoreClaim(object data)
        {
            Stopwatch sw = Stopwatch.StartNew();
            ScoreData scoreData = data as ScoreData;

            if (scoreData == null) return;

            try
            {
                if (scoreData.ctx.TraceScoring)
                    ADATrace.SwitchToNewUniqueLogFile(scoreData.ClaimToProcess.RiskClient.Id, scoreData.RiskBatch.ClientBatchReference, "Process-" +
                            MDA.Common.Helpers.FileNameHelper.CreateCleanFileName(scoreData.ClaimToProcess.RiskClaim.ClientClaimRefNumber));

                Pipeline p = (Pipeline)CreatePipeline(scoreData.BatchEntityType, scoreData.ctx.RiskClientId);

                p.ScoreSingleClaim(scoreData.ctx, scoreData.RiskBatch, scoreData.ClaimToProcess, scoreData.ctx.ScoreWithLiveRules);

                if (scoreData.ctx.ShowProgress != null)
                    scoreData.ctx.ShowProgress(ProgressSeverity.Info, "Scoring claim : " + scoreData.ClaimToProcess.RiskClaim.ClientClaimRefNumber + " : Done : " + sw.ElapsedMilliseconds + "ms", true);
            }
            catch (Exception)
            {
                if (scoreData.ctx.ShowProgress != null)
                    scoreData.ctx.ShowProgress(ProgressSeverity.Info, "Scoring claim : " + scoreData.ClaimToProcess.RiskClaim.ClientClaimRefNumber + " : Error : " + sw.ElapsedMilliseconds + "ms", true);
            }
            finally
            {
                threadCount--;
            }
        }
    }
}