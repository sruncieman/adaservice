﻿using System;
using System.Collections.Generic;
using System.IO;
using MDA.RiskService;
using MDA.RiskService.Model;
using MDA.DAL;
using ADAServices.Interface;
using ADAServices.Translator;
using MDA.Common;
using MDA.Common.Server;
using MDA.RiskService.Interface;

namespace ADAServices.Mock
{
    public class ADAServices : IADAServices
    {
        public void UploadBatchOfClaimsFile(Stream sourceStream, string batchEntityType, string fileName, string mimeType, int userId, int clientId, string clientBatchRef, string who)
        {
        }
        public int GetPasswordFailuresSinceLastSuccess(GetPasswordFailuresSinceLastSuccessParam param)
        {
            return 0;
        }

        public SetClaimReadStatusResult SetClaimReadStatus(SetClaimReadStatusParam param)
        {
            return new SetClaimReadStatusResult();
        }

        public ProcessClaimBatchResult ProcessClaimBatch(ProcessClaimBatchParam param)
        {
            return new ProcessClaimBatchResult() { Results = new MDA.Common.ProcessingResults("ProcessClaimBatch") {  } };
        }

        public ProcessMobileClaimBatchResult ProcessMobileClaimBatch(ProcessMobileClaimBatchParam param)
        {
            return new ProcessMobileClaimBatchResult() { Results = new MDA.Common.ProcessingResults("ProcessMobileClaimBatch") { } };
        }

        public ProcessClaimResult ProcessClaim(ProcessClaimParam param)
        {
            return new ProcessClaimResult() { Results = new MDA.Common.ProcessingResults("ProcessClaim") { } };
        }

        public IsClientBatchReferenceUniqueResult IsClientBatchReferenceUnique(IsClientBatchReferenceUniqueParam param)
        {
            return new IsClientBatchReferenceUniqueResult() { IsUnique = true, Error = null };
        }

        public IsClientNameUniqueResult IsClientNameUnique(IsClientNameUniqueParam param)
        {
            return new IsClientNameUniqueResult() { IsUnique = true, Error = null };
        }

        public IsPasswordUniqueResult IsPasswordUnique(IsPasswordUniqueParam param)
        {
            return new IsPasswordUniqueResult() { IsUnique = true, Error = null };
        }

        public IsUserNameUniqueResult IsUserNameUnique(IsUserNameUniqueParam param)
        {
            return new IsUserNameUniqueResult() { IsUnique = true, Error = null };
        }

        public ClaimsForClientResult GetClaimsForClient(ClaimsForClientParam param)
        {
            string[] ClaimStatusText = { "Bad", "Created", "Loaded", "Scored" };
            string[] BatchStatusText = { "Bad", "Loading", "Loaded" };

            var res = new ClaimsForClientResult();

            res.TotalRows = 25;

            res.ClaimsList = new List<ERiskClaimDetails>();

            for (int i = 0; i < 25; i++)
            {
                var r = new MDA.RiskService.Model.RiskClaimRunDetails();

                r.BaseRiskClaim_Id = i;
                r.BatchReference = "BREF" + i.ToString();
                r.BatchStatus = 2;
                r.ClaimStatus = 3;
                r.ClaimReadStatus = 0;
                r.ClientBatchReference = "CBREF" + i.ToString();
                r.ClientClaimRefNumber = "CCREF" + i.ToString();
                r.CreatedBy = "Fred";
                r.CreatedDate = new DateTime(2012, 1, i);
                r.Incident_Id = 1;
                r.IncidentDate = new DateTime(2012, 1, i);
                r.LevelOneRequestedCount = 0;
                r.LevelOneRequestedWhen = new DateTime(2012, 1, i);
                r.LevelOneRequestedWho = "fred";
                r.LevelTwoRequestedCount = 0;
                r.LevelTwoRequestedWhen = new DateTime(2012, 1, i);
                r.LevelTwoRequestedWho = "fred";
                r.MDAClaimRef = "MDA:" + i.ToString();
                r.PaymentsToDate = 0;
                r.Reserve = 0;
                r.TotalKeyAttractorCount = 0;
                r.TotalScore = 0;
                r.RiskBatch_Id = 1;
                r.RiskClaim_Id = 1;
                r.RiskClaimRun_Id = 1;

                r.MessageList = new List<MDA.RiskService.Model.EntityScoreMessageList>();

                var m = new MDA.RiskService.Model.EntityScoreMessageList();

                m.EntityHeader = "Header" + i.ToString();
                m.Messages = new ListOfMsgStrings(); // new List<string>();

                for (int j = 0; j < i; j++)
                    m.Messages.Add("Message Line 1:" + i.ToString() + ":" + j.ToString());

                r.MessageList.Add(m);

                res.ClaimsList.Add( Translator.Translator.Translate(r) );

            }

            return res;
        }

        public RegisterNewClientResult RegisterNewClient(RegisterNewClientParam param)
        {
            return new RegisterNewClientResult()
            {
                ClientId = 2,
                Error = null
            };
        }

        public RequestLevelOneReportResult RequestLevelOneReport(RequestLevelOneReportParam param)
        {
            return new RequestLevelOneReportResult();
        }

        public RequestLevelTwoReportResult RequestLevelTwoReport(RequestLevelTwoReportParam param)
        {
            return new RequestLevelTwoReportResult();
        }

        public RequestCallbackResult RequestCallback(RequestCallbackParam param)
        {
            return new RequestCallbackResult();
        }

        public ClearCallbackRequestResult ClearCallbackRequest(ClearCallbackRequestParam param)
        {
            return new ClearCallbackRequestResult();
        }

        //public Byte[] GetReportBytes(GetReportBytesParam param)
        //{
        //    return null;
        //}
        public Stream GetLevel1ReportStream(GetLevel1ReportStreamParam param)
        {
            return null;
        }

        public Stream GetLevel2ReportStream(GetLevel2ReportStreamParam param)
        {
            return null;
        }

        public Stream GetBatchReportStream(GetBatchReportStreamParam param)
        {
            return null;
        }

        public Stream GetCueReportStream(GetCueReportStreamParam param)
        {
            return null;
        }

        public WebsiteCueSearchResult SubmitCueSearch(WebsiteCueSearchParam param)
        {
            return null;
        }

        public byte[] GenerateBatchReport(GenerateBatchReportParam param)
        {
            return null;
        }

        public byte[] GenerateLevel1Report(GenerateLevel1ReportParam param)
        {
            return null;
        }

        public byte[] GenerateLevel2Report(GenerateLevel2ReportParam param)
        {
            return null;
        }

        public GetBatchStatusResult GetBatchStatus(GetBatchStatusParam param)
        {
            return new GetBatchStatusResult();
        }

        public GetBatchProcessingResultsResult GetBatchProcessingResults(GetBatchProcessingResultsParam param)
        {
            return new GetBatchProcessingResultsResult();
        }

        #region File Editor Mehods
        public BatchListResult GetClientBatchList(BatchListParam param)
        {
            string[] batchStatus = { "Bad", "Loading", "Loaded" };

            CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, null);

            IRiskServices riskServices = new RiskServices(ctx);

            var listBatch = riskServices.GetBatchesForClient(param.ClientId);

            var newBatchList = new List<EBatchDetails>();

            foreach (var batchRecord in listBatch)
            {
                EBatchDetails batch = new EBatchDetails()
                {
                    BatchId             = batchRecord.Id,
                    BatchStatusId       = batchRecord.Id,
                    Reference           = batchRecord.BatchReference,
                    Status              = batchStatus[batchRecord.BatchStatus],
                    UploadedBy          = batchRecord.CreatedBy,
                    UploadedDate        = batchRecord.CreatedDate,
                    TotalClaimsReceived = batchRecord.TotalClaimsReceived,
                    TotalNewClaims      = batchRecord.TotalNewClaims,
                    TotalModifiedClaims = batchRecord.TotalModifiedClaims,
                    TotalClaimsLoaded   = batchRecord.TotalClaimsLoaded,
                    TotalClaimsInError  = batchRecord.TotalClaimsInError,
                    TotalClaimsSkipped  = batchRecord.TotalClaimsSkipped,
                    LoadingDuration     = batchRecord.LoadingDurationInMs
                };

                newBatchList.Add(batch);
            }

            return new BatchListResult() { BatchList = newBatchList };
        }

        public ClaimsInBatchResult GetClaimsInBatch(ClaimsInBatchParam param)
        {
            CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, null);   //TODO client id

            IRiskServices riskServices = new RiskServices(ctx);

            ClaimsInBatchResult response = new ClaimsInBatchResult();
            response.ClaimsList = new List<ERiskClaim>();

            var l = riskServices.GetClaimsInBatch(param.BatchId);

            foreach (var i in l)
            {
                var latestScoreRun = riskServices.GetLatestRiskClaimRun(i.Id);

                int latestScore = (latestScoreRun == null) ? 0 : latestScoreRun.TotalScore;

                response.ClaimsList.Add(Translator.Translator.Translate(i, latestScore));
            }

            return response;
        }

        public GetClaimScoreDataResult GetClaimScoreData(GetClaimScoreDataParam param)
        {
            CurrentContext ctx = new CurrentContext(param.ClientId, param.UserId, null);   //TODO client id

            IRiskServices riskServices = new RiskServices(ctx);

            RiskClaimRun rcr = riskServices.GetLatestRiskClaimRun(param.RiskClaimId);

            MessageNode scoreNodes = riskServices.GetScoresAsNodeTree(rcr);

            return new GetClaimScoreDataResult() 
            {
                RiskClaimRun = Translator.Translator.Translate(rcr), 
                Error = "", 
                ScoreNodes = scoreNodes 
            };
        }

        #endregion


        #region membership

        public bool IsUserInRole(IsUserInRoleParam param)
        {
            return true;
        }

        public string[] GetRolesForUser(GetRolesForUserParam param)
        {
            return null;
        }

        public bool ValidateUser(ValidateUserParam param)
        {
            return true;
        }

        public void CreateUser(CreateUserParam param)
        {
        }

        public void CreateClient(CreateClientParam param)
        {
        }

        public void CreateRiskWord(CreateRiskWordParam param)
        {
        }

        public void CreateRiskDefaultData(CreateRiskDefaultDataParam param)
        {
        }

        public void CreateRiskNoteDecision(CreateRiskNoteDecisionParam param)
        {
        }

        public void DeleteRiskDefaultData(DeleteRiskDefaultDataParam param)
        {
        }

        public void DeleteRiskNoteDecision(DeleteRiskNoteDecisionParam param)
        {
        }

        public void UpdateClient(UpdateClientParam param)
        {
        }

        public void UpdateUser(UpdateUserParam param)
        {
        }

        public void CreateRiskUser2Client(CreateRiskUser2ClientParam param)
        {
        }

        public void DeleteRiskUser2Client(DeleteRiskUser2ClientParam param)
        {
        }

        public bool ChangePassword(ChangePasswordParam param)
        {
            return false;
        }

        #endregion

        public RiskClientsResult GetRiskClients(RiskClientsParam param)
        {
            return new RiskClientsResult();
        }

        public RiskWordsResult GetRiskWords(RiskWordsParam param)
        {
            return new RiskWordsResult();
        }

        public RiskNotesResult GetRiskNotes(RiskNotesParam param)
        {
            return new RiskNotesResult();
        }

        public GetRiskNoteResult GetRiskNote(GetRiskNoteParam param)
        {
            return new GetRiskNoteResult();
        }

        public RiskNoteDecisionsResult GetRiskNoteDecisions(RiskNoteDecisionsParam param)
        {
            return new RiskNoteDecisionsResult();
        }

        public RiskWordDeleteResult DeleteRiskWord(RiskWordDeleteParam param)
        {
            return new RiskWordDeleteResult();
        }

        public RiskNoteCreateResult CreateRiskNote(RiskNoteCreateParam param)
        {
            return new RiskNoteCreateResult();
        }

        public RiskNoteUpdateResult UpdateRiskNote(RiskNoteUpdateParam param)
        {
            return new RiskNoteUpdateResult();
        }

        public RiskDefaultDataResult GetRiskDefaultData(RiskDefaultDataParam param)
        {
            return new RiskDefaultDataResult();
        }

        public GetAssignedRiskClientsForUserResult GetAssignedRiskClientsForUser(GetAssignedRiskClientsForUserParam param)
        {
            return new GetAssignedRiskClientsForUserResult();
        }

        public RiskRolesResult GetRiskRoles(RiskRolesParam param)
        {
            return new RiskRolesResult();
        }

        public InsurersClientsResult GetInsurersClients(InsurersClientsParam param)
        {
            return new InsurersClientsResult();
        }


        /// <summary>
        /// Get a list of all users for a team
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public UsersForTeamResult GetUsersForTeam(UsersForTeamParam param)
        {
            return new UsersForTeamResult();
        }

        public GetRiskUserInfoResult GetRiskUserInfo(GetRiskUserInfoParam param)
        {
            CurrentContext ctx = new CurrentContext(0, param.UserId, null);   //TODO client id

            IRiskServices riskServices = new RiskServices(ctx);

            var rc = riskServices.GetUserDetails(param.UserId, param.ClientId);

            return new GetRiskUserInfoResult()
            {
                UserInfo = Translator.Translator.Translate(rc)
            };
        }

        public WebsiteBatchListResult GetBatchList(WebsiteBatchListParam param)
        {
            return new WebsiteBatchListResult();

        }


        public int GetUserId(GetUserIdParam param)
        {
            return 0;
        }

        public bool CheckUserBelongsToClient(CheckUserBelongsToClientParam param)
        {
            return true;
        }

        public bool CheckUserHasAccessToReport(CheckUserHasAccessToReportParam param)
        {
            return true;
        }


        public ClaimsForUserResult GetClaimsForUser(ClaimsForUserParam param)
        {
            throw new NotImplementedException();
        }

        public SaveClaimForUserResult GetClaimToSaveForUser(SaveClaimForUserParam param)
        {
            return new SaveClaimForUserResult();
        }


        public SingleClaimForUserResult GetSingleClaimForUser(SingleClaimForUserParam param)
        {
            return new SingleClaimForUserResult();
        }


        public WebsiteSingleClaimsListResult GetSingleClaims(WebsiteSingleClaimsListParam param)
        {
            return new WebsiteSingleClaimsListResult();
        }


        public WebsiteSaveSingleClaimResult SaveSingleClaim(WebsiteSaveSingleClaimParam param)
        {
            return new WebsiteSaveSingleClaimResult();
        }

        public WebsiteGetTotalSingleClaimsForClientResult GetTotalSingleClaimForClient(WebsiteGetTotalSingleClaimsForClientParam param)
        {
            return new WebsiteGetTotalSingleClaimsForClientResult();
        }

        public WebsiteFetchSingleClaimDetailResult FetchSingleClaimDetail(WebsiteFetchSingleClaimDetailParam param)
        {
            return new WebsiteFetchSingleClaimDetailResult();
        }


        public WebsiteDeleteSingleClaimResult DeleteSingleClaim(WebsiteDeleteSingleClaimParam param)
        {
            return new WebsiteDeleteSingleClaimResult();
        }


        public WebsiteFilterBatchNumbersResult FilterBatchNumbers(WebsiteFilterBatchNumbersParam param)
        {
            return new WebsiteFilterBatchNumbersResult();
        }


        public WebsiteCueInvolvementsResult GetCueInvolvements(WebsiteCueInvolvementsParam param)
        {
            return new WebsiteCueInvolvementsResult();
        }


        public WebsiteCueReportResult GetCueReport(WebsiteCueReportParam param)
        {
            return new WebsiteCueReportResult();
        }

        public RiskUsersLockedResult GetRiskUsersLocked(RiskUsersLockedParam param)
        {
            return new RiskUsersLockedResult();
        }

        public RiskUsersResult GetRiskUsers(RiskUsersParam param)
        {
            return new RiskUsersResult();
        }

        public GeneratePasswordResetTokenResult GeneratePasswordResetToken(GeneratePasswordResetTokenParam param)
        {
            return new GeneratePasswordResetTokenResult();
        }

        public ValidatePasswordResetTokenResult ValidatePasswordResetToken(ValidatePasswordResetTokenParam param)
        {
            return new ValidatePasswordResetTokenResult();
        }

        public RiskUserUnlockResult UnlockRiskUser(RiskUserUnlockParam param)
        {
            return new RiskUserUnlockResult();
        }

        public WebsiteSaveTemplateFunctionsResult SaveTemplateFunctions(WebsiteSaveTemplateFunctionsParam param)
        {
            return new WebsiteSaveTemplateFunctionsResult();
        }


        public WebsiteLoadTemplateFunctionsResult LoadTemplateFunctions(WebsiteLoadTemplateFunctionsParam param)
        {
            return new WebsiteLoadTemplateFunctionsResult();
        }


        public WebsiteRiskRoleCreateResult CreateRiskRole(WebsiteRiskRoleCreateParam param)
        {
            return new WebsiteRiskRoleCreateResult();
        }


        public WebsiteRiskRoleEditResult EditRiskRole(WebsiteRiskRoleEditParam param)
        {
            return new WebsiteRiskRoleEditResult();
        }


        public WebsiteRiskRoleDeleteResult DeleteRiskRole(WebsiteRiskRoleDeleteParam param)
        {
            return new WebsiteRiskRoleDeleteResult();
        }


        public FindUserNameResult FindUserName(FindUserNameParam param)
        {
            return new FindUserNameResult();
        }


        public SendElmahErrorResult SendElmahEmail(SendElmahErrorParam param)
        {
            return new SendElmahErrorResult();
        }


        public GetListOfBatchPriorityClientsResult GetListOfBatchPriorityClients(GetListOfBatchPriorityClientsParam param)
        {
            return new GetListOfBatchPriorityClientsResult();
        }

        public SaveBatchPriorityClientsResult SaveBatchPriorityClients(SaveBatchPriorityClientsParam param)
        {
            return new SaveBatchPriorityClientsResult();
        }


        public GetBatchQueueResult GetBatchQueue(GetBatchQueueParam param)
        {
            return new GetBatchQueueResult();
        }


        public SaveOverriddenBatchPriorityResult SaveOverriddenBatchPriority(SaveOverriddenBatchPriorityParam param)
        {
            return new SaveOverriddenBatchPriorityResult();
        }


        public WebSearchResultsResult FindSearchResultDetails(WebSearchResultsParam param)
        {
            return new WebSearchResultsResult();
        }
    }

}
