﻿using KeoghsClientSite.Controllers;
using MDA.Common.Debug;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace KeoghsClientSite
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            //
            if (!Directory.Exists(ConfigurationManager.AppSettings["WebsiteErrorLoggingFolderPath"]))
                Directory.CreateDirectory(ConfigurationManager.AppSettings["WebsiteErrorLoggingFolderPath"]);
            //
            MvcHandler.DisableMvcResponseHeader = true;
        }

         protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)       
         {        
             //Only access session state if it is available      
             if (Context.Handler is IRequiresSessionState || Context.Handler is IReadOnlySessionState)    
             {   
                 //If we are authenticated AND we dont have a session here.. redirect to login page.               
                 HttpCookie authenticationCookie = Request.Cookies[FormsAuthentication.FormsCookieName]; 
        
                 if (authenticationCookie != null)              
                 {   
                     FormsAuthenticationTicket authenticationTicket = FormsAuthentication.Decrypt(authenticationCookie.Value);     
      
                     if (!authenticationTicket.Expired)                 
                     {
                         //         
                         if (Session["UserInfo"] == null)             
                         {   
                             //This means for some reason the session expired before the authentication ticket. Force a login.                
                             FormsAuthentication.SignOut();                        
                             Response.Redirect(FormsAuthentication.LoginUrl, true);           
                             return;                
                         }                
                     }           
                 }         
             }      
         }
        //
        protected void Application_Error(object sender, EventArgs e)
        {    
             bool _trace = false;

             try
             {
                 _trace = Convert.ToBoolean(ConfigurationManager.AppSettings["WebsiteErrorLogging"]);

             }
             catch(Exception)
             {
             }

            var httpContext = ((MvcApplication)sender).Context; 
            var currentController = " ";  
            var currentAction = " ";  
            var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext)); 
            //
            // Get Controller and action that errored if any
            if (currentRouteData != null) 
            {       
                if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))  
                {           
                    currentController = currentRouteData.Values["controller"].ToString();   
                }         
                if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))  
                {           
                    currentAction = currentRouteData.Values["action"].ToString();       
                }  
            }    
            //
            // Get the exception
            var ex = Server.GetLastError();  
            //
            // Log the exception
            if (_trace)
            {
                ADATrace.WriteLine("-----------------------------------------------------------------------");
                ADATrace.WriteLine("CONTROLLER: " + currentController);
                ADATrace.WriteLine("ACTION: " + currentAction);
                ADATrace.WriteLine("EXCEPTION MESSAGE: " + ex.Message);
                ADATrace.WriteLine("STACK TRACE: " + ex.ToString());
                ADATrace.WriteLine("-----------------------------------------------------------------------");
            }
            //

            // Instantiate error controller
            var controller = new ErrorController(); 
            // Route data for error controller
            var routeData = new RouteData(); 
            //
            //var action = "Index";  
            //
            HttpException httpException = ex as HttpException;
            //
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                routeData.Values.Add("action", "NotAuthenticated");
            }
            else if (httpException == null)
            {
                routeData.Values.Add("action", "Index");
            }
            else //It's an Http Exception 
            {   
                //var httpEx = ex as HttpException;   
                switch (httpException.GetHttpCode())    
                {           
                    case 404:              
                        routeData.Values.Add("action","NotFound");    
                        break;            
                    case 500:
                        //Server error
                        routeData.Values.Add("action", "HttpError500");
                        break;

                    default:
                        routeData.Values.Add("action", "GeneralHttp");
                        break;

                }   
            }    
            //
            httpContext.ClearError();  
            httpContext.Response.Clear();  
            //httpContext.Response.StatusCode = ex is HttpException ? ((HttpException)ex).GetHttpCode() : 500; 
            //
            //Clear the error on server.
            Server.ClearError();
            //
            //httpContext.Response.TrySkipIisCustomErrors = true;  
            Response.TrySkipIisCustomErrors = true; 
            //
            routeData.Values["controller"] = "Error";  
 
            if(httpException != null && httpException.GetHttpCode()==404)
            {
                HttpContext.Current.Response.Redirect("/Home/Index");
            }


            //routeData.Values["action"] = action;   
            controller.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction); 
            // Call error controller
            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(Context), routeData));

        }

        void ErrorMail_Mailing(object sender, Elmah.ErrorMailEventArgs e)
        {
            Repositories.Repository.SendElmahError(e);
        }
    }
}