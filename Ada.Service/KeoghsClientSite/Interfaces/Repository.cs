﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.Note;
using MDA.WCF.WebServices.Interface;
using MDA.WCF.WebServices.Messages;
using System.Web.Mvc;

namespace KeoghsClientSite.Interfaces
{
    public interface IRepository
    {
        List<Note> GetAll();

        List<ExRiskNote> GetAll(int riskCliamId);

        RiskNoteCreateResponse CreateNote(Note note);

        RiskNoteUpdateResponse UpdateNote(Note note);

        IEnumerable<SelectListItem> GetAllDecisions(int riskClientId);

        ExRiskNote GetRiskNote(int riskNoteId);

        IEnumerable<SelectListItem> GetAllVisibilityStatus();
    }
}


