﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.WCF.TransferServices;
using System.IO;

namespace KeoghsClientSite.Interfaces
{
    public class ADATransferService : IADATransferServices
    {
        ADATransferServiceReference.ADATransferServicesClient GetTransferServicesProxy()
        {
            var p = new ADATransferServiceReference.ADATransferServicesClient();

            var pType = MDA.Common.Config.UserAccountConfig.GetConfig;

            p.ClientCredentials.Windows.ClientCredential.Domain = pType.UserDomain.Value;
            p.ClientCredentials.Windows.ClientCredential.UserName = pType.UserName.Value;
            p.ClientCredentials.Windows.ClientCredential.Password = pType.UserPassword.Value;

            return p;
        }

        public void UploadBatchOfClaimsFile(RemoteFileInfo request)
        {
            GetTransferServicesProxy().UploadBatchOfClaimsFile(request.ClientBatchReference, request.BatchEntityType, request.ClientId, request.FileLength, request.FileName, request.MimeType, request.UserId, request.Who, request.FileByteStream);
        }

        public Stream GetLevel1ReportStream(GetLevel1ReportStreamRequest request)
        {
            return GetTransferServicesProxy().GetLevel1ReportStream(request);
        }

        public Stream GetLevel2ReportStream(GetLevel2ReportStreamRequest request)
        {
            return GetTransferServicesProxy().GetLevel2ReportStream(request);
        }

        public Stream GetBatchReportStream(GetBatchReportStreamRequest request)
        {
            return GetTransferServicesProxy().GetBatchReportStream(request);
        }
    }
}
