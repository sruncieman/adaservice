﻿using MDA.WCF.WebServices.Entities;
using MDA.WCF.WebServices.Interface;
using MDA.WCF.WebServices.Messages;

namespace KeoghsClientSite.Interfaces
{
    public class ADAWebService : IADAWebServices
    {
        private ADAWebServiceReference.ADAWebServicesClient GetWebServicesProxy()
        {
            var p = new ADAWebServiceReference.ADAWebServicesClient();

            var pType = MDA.Common.Config.UserAccountConfig.GetConfig;

            p.ClientCredentials.Windows.ClientCredential.Domain = pType.UserDomain.Value;
            p.ClientCredentials.Windows.ClientCredential.UserName = pType.UserName.Value;
            p.ClientCredentials.Windows.ClientCredential.Password = pType.UserPassword.Value;

            return p;
        }

        public bool CheckUserHasAccessToReport(CheckUserHasAccessToReportRequest request)
        {
            return GetWebServicesProxy().CheckUserHasAccessToReport(request);
        }

        public ProcessClaimBatchResponse ProcessClaimBatch(ProcessClaimBatchRequest request)
        {
            return GetWebServicesProxy().ProcessClaimBatch(request);
        }

        public ProcessMobileClaimBatchResponse ProcessMobileClaimBatch(ProcessMobileClaimBatchRequest request)
        {
            return GetWebServicesProxy().ProcessMobileClaimBatch(request);
        }

        public ProcessClaimResponse ProcessClaim(ProcessClaimRequest request)
        {
            return GetWebServicesProxy().ProcessClaim(request);
        }

        public ClaimsForClientResponse GetClaimsForClient(ClaimsForClientRequest request)
        {
            return GetWebServicesProxy().GetClaimsForClient(request);
        }

        public RegisterNewClientResponse RegisterNewClient(RegisterNewClientRequest request)
        {
            return GetWebServicesProxy().RegisterNewClient(request);
        }

        public RequestLevelOneReportResponse RequestLevelOneReport(RequestLevelOneReportRequest request)
        {
            return GetWebServicesProxy().RequestLevelOneReport(request);
        }

        public RequestLevelTwoReportResponse RequestLevelTwoReport(RequestLevelTwoReportRequest request)
        {
            return GetWebServicesProxy().RequestLevelTwoReport(request);
        }

        public RequestCallbackResponse RequestCallback(RequestCallbackRequest request)
        {
            return GetWebServicesProxy().RequestCallback(request);
        }

        public ClearCallbackRequestResponse ClearCallbackRequest(ClearCallbackRequestRequest request)
        {
            return GetWebServicesProxy().ClearCallbackRequest(request);
        }

        public SetClaimReadStatusResponse SetClaimReadStatus(SetClaimReadStatusRequest request)
        {
            return GetWebServicesProxy().SetClaimReadStatus(request);
        }

        public ClaimsInBatchResponse GetClaimsInBatch(ClaimsInBatchRequest request)
        {
            throw new System.NotSupportedException("This method for file editor only");
        }

        public GetClaimScoreDataResponse GetClaimScoreData(GetClaimScoreDataRequest request)
        {
            throw new System.NotSupportedException("This method for file editor only");
        }

        public BatchListResponse GetClientBatchList(BatchListRequest batchListRequest)
        {
            throw new System.NotSupportedException("This method for file editor only");
            //return p.GetClientBatchList(batchListRequest);
        }

        public GetRiskUserInfoResponse GetRiskUserInfo(GetRiskUserInfoRequest request)
        {
            return GetWebServicesProxy().GetRiskUserInfo(request);
        }

        public WebsiteBatchListResponse GetBatchList(WebsiteBatchListRequest batchListRequest)
        {
            return GetWebServicesProxy().GetBatchList(batchListRequest);
        }

        public GetBatchStatusResponse GetBatchStatus(GetBatchStatusRequest request)
        {
            return GetWebServicesProxy().GetBatchStatus(request);
        }

        public GetBatchProcessingResultsResponse GetBatchProcessingResults(GetBatchProcessingResultsRequest request)
        {
            return GetWebServicesProxy().GetBatchProcessingResults(request);
        }

        public IsClientBatchReferenceUniqueResponse IsClientBatchReferenceUnique(
            IsClientBatchReferenceUniqueRequest request)
        {
            return GetWebServicesProxy().IsClientBatchReferenceUnique(request);
        }

        public IsClientNameUniqueResponse IsClientNameUnique(
            IsClientNameUniqueRequest request)
        {
            return GetWebServicesProxy().IsClientNameUnique(request);
        }

        public IsPasswordUniqueResponse IsPasswordUnique(
            IsPasswordUniqueRequest request)
        {
            return GetWebServicesProxy().IsPasswordUnique(request);
        }

        public IsUserNameUniqueResponse IsUserNameUnique(
            IsUserNameUniqueRequest request)
        {
            return GetWebServicesProxy().IsUserNameUnique(request);
        }

        #region membership

        public int GetPasswordFailuresSinceLastSuccess(GetPasswordFailuresSinceLastSuccessRequest request)
        {
            return GetWebServicesProxy().GetPasswordFailuresSinceLastSuccess(request);
        }

        public RiskClientsResponse GetRiskClients(RiskClientsRequest request)
        {
            return GetWebServicesProxy().GetRiskClients(request);
        }

        public RiskWordsResponse GetRiskWords(RiskWordsRequest request)
        {
            return GetWebServicesProxy().GetRiskWords(request);
        }

        public RiskNotesResponse GetRiskNotes(RiskNotesRequest request)
        {
            return GetWebServicesProxy().GetRiskNotes(request);
        }

        public GetRiskNoteResponse GetRiskNote(GetRiskNoteRequest request)
        {
            return GetWebServicesProxy().GetRiskNote(request);
        }

        public RiskNoteDecisionsResponse GetRiskNoteDecisions(RiskNoteDecisionsRequest request)
        {
            return GetWebServicesProxy().GetRiskNoteDecisions(request);
        }

        public RiskWordDeleteResponse DeleteRiskWord(RiskWordDeleteRequest request)
        {
            return GetWebServicesProxy().DeleteRiskWord(request);
        }

        public RiskNoteCreateResponse CreateRiskNote(RiskNoteCreateRequest request)
        {
            return GetWebServicesProxy().CreateRiskNote(request);
        }

        public RiskNoteUpdateResponse UpdateRiskNote(RiskNoteUpdateRequest request)
        {
            return GetWebServicesProxy().UpdateRiskNote(request);
        }

        public RiskDefaultDataResponse GetRiskDefaultData(RiskDefaultDataRequest request)
        {
            return GetWebServicesProxy().GetRiskDefaultData(request);
        }

        public GetAssignedRiskClientsForUserResponse GetAssignedRiskClientsForUser(GetAssignedRiskClientsForUserRequest request)
        {
            return GetWebServicesProxy().GetAssignedRiskClientsForUser(request);
        }

        public RiskRolesResponse GetRiskRoles(RiskRolesRequest request)
        {
            return GetWebServicesProxy().GetRiskRoles(request);
        }

        public InsurersClientsResponse GetInsurersClients(InsurersClientsRequest request)
        {
            return GetWebServicesProxy().GetInsurersClients(request);
        }

        //GetInsurersClients

        public RiskUsersResponse GetRiskUsers(RiskUserRequest request)
        {
            return GetWebServicesProxy().GetRiskUsers(request);
        }

        public bool IsUserInRole(IsUserInRoleRequest request)
        {
            return false;
        }

        public string[] GetRolesForUser(GetRolesForUserRequest request)
        {
            return new string[] { "" };
        }

        public bool ValidateUser(ValidateUserRequest request)
        {
            return false;
        }

        public void CreateUser(CreateUserRequest request)
        {
            GetWebServicesProxy().CreateUser(request);
        }

        public void CreateClient(CreateClientRequest request)
        {
            GetWebServicesProxy().CreateClient(request);
        }

        public void CreateRiskWord(CreateRiskWordRequest request)
        {
            GetWebServicesProxy().CreateRiskWord(request);
        }

        public void CreateRiskDefaultData(CreateRiskDefaultDataRequest request)
        {
            GetWebServicesProxy().CreateRiskDefaultData(request);
        }

        public void CreateRiskNoteDecision(CreateRiskNoteDecisionRequest request)
        {
            GetWebServicesProxy().CreateRiskNoteDecision(request);
        }

        public void DeleteRiskDefaultData(DeleteRiskDefaultDataRequest request)
        {
            GetWebServicesProxy().DeleteRiskDefaultData(request);
        }

        public void DeleteRiskNoteDecision(DeleteRiskNoteDecisionRequest request)
        {
            GetWebServicesProxy().DeleteRiskNoteDecision(request);
        }

        public void UpdateClient(UpdateClientRequest request)
        {
            GetWebServicesProxy().UpdateClient(request);
        }

        public void UpdateUser(UpdateUserRequest request)
        {
            GetWebServicesProxy().UpdateUser(request);
        }

        public void CreateRiskUser2Client(CreateRiskUser2ClientRequest request)
        {
            GetWebServicesProxy().CreateRiskUser2Client(request);
        }

        public void DeleteRiskUser2Client(DeleteRiskUser2ClientRequest request)
        {
            GetWebServicesProxy().DeleteRiskUser2Client(request);
        }

        public bool ChangePassword(ChangePasswordRequest request)
        {
            return false;
        }

        public int GetUserId(GetUserIdRequest request)
        {
            return GetWebServicesProxy().GetUserId(request);
        }

        #endregion membership

        public WebsiteSingleClaimsListResponse GetSingleClaimsList(
            WebsiteSingleClaimsListRequest websiteSingleClaimsListRequest)
        {
            return GetWebServicesProxy().GetSingleClaimsList(websiteSingleClaimsListRequest);
        }

        public WebsiteSaveSingleClaimResponse SaveSingleClaim(
            WebsiteSaveSingleClaimRequest websiteSaveSingleClaimRequest)
        {
            return GetWebServicesProxy().SaveSingleClaim(websiteSaveSingleClaimRequest);
        }

        public WebsiteGetTotalSingleClaimsForClientResponse GetTotalSingleClaimForClient(
            WebsiteGetTotalSingleClaimsForClientRequest websiteGetTotalSingleClaimForClientRequest)
        {
            return GetWebServicesProxy().GetTotalSingleClaimForClient(websiteGetTotalSingleClaimForClientRequest);
        }

        public WebsiteFetchSingleClaimResponse FetchSingleClaimDetail(
            WebsiteFetchSingleClaimRequest websiteFetchSingleClaimRequest)
        {
            return GetWebServicesProxy().FetchSingleClaimDetail(websiteFetchSingleClaimRequest);
        }

        public WebsiteDeleteSingleClaimResponse DeleteSingleClaim(WebsiteDeleteSingleClaimRequest websiteDeleteSingleClaimRequest)
        {
            return GetWebServicesProxy().DeleteSingleClaim(websiteDeleteSingleClaimRequest);
        }

        public WebsiteFilterBatchNumbersResponse FilterBatchNumbers(WebsiteFilterBatchNumbersRequest websiteFilterBatchNumbersRequest)
        {
            return GetWebServicesProxy().FilterBatchNumbers(websiteFilterBatchNumbersRequest);
        }

        #region CUE

        public WebsiteCueInvolvementsResponse GetCueInvolvements(WebsiteCueInvolvementsRequest websiteCueInvolvementsRequest)
        {
            return GetWebServicesProxy().GetCueInvolvements(websiteCueInvolvementsRequest);
        }

        public WebsiteCueSearchResponse SubmitCueSearch(WebsiteCueSearchRequest websiteCueSearchRequest)
        {
            return GetWebServicesProxy().SubmitCueSearch(websiteCueSearchRequest);
        }

        #endregion CUE

        public void GenerateBatchReport(GenerateBatchReportRequest request)
        {
            GetWebServicesProxy().GenerateBatchReport(request);
        }

        public void GenerateLevel1Report(GenerateLevel1ReportRequest request)
        {
            GetWebServicesProxy().GenerateLevel1Report(request);
        }

        public void GenerateLevel2Report(GenerateLevel2ReportRequest request)
        {
            GetWebServicesProxy().GenerateLevel2Report(request);
        }

        public WebsiteCueReportResponse GetCueReport(WebsiteCueReportRequest request)
        {
            return GetWebServicesProxy().GetCueReport(request);
        }

        public RiskUsersLockedResponse GetRiskUsersLocked(RiskUsersLockedRequest request)
        {
            return GetWebServicesProxy().GetRiskUsersLocked(request);
        }

        public RiskUserUnlockResponse UnlockRiskUser(RiskUserUnlockRequest request)
        {
            return GetWebServicesProxy().UnlockRiskUser(request);
        }

        public GeneratePasswordResetTokenResponse GeneratePasswordResetToken(GeneratePasswordResetTokenRequest request)
        {
            return GetWebServicesProxy().GeneratePasswordResetToken(request);
        }

        public ValidatePasswordResetTokenResponse ValidatePasswordResetToken(ValidatePasswordResetTokenRequest request)
        {
            return GetWebServicesProxy().ValidatePasswordResetToken(request);
        }

        public WebsiteSaveTemplateFunctionsResponse SaveTemplateFunctions(WebsiteSaveTemplateFunctionsRequest request)
        {
            return GetWebServicesProxy().SaveTemplateFunctions(request);
        }

        public WebsiteLoadTemplateFunctionsResponse LoadTemplateFunctions(WebsiteLoadTemplateFunctionsRequest request)
        {
            return GetWebServicesProxy().LoadTemplateFunctions(request);
        }

        public WebsiteRiskRoleCreateResponse CreateRiskRole(WebsiteRiskRoleCreateRequest request)
        {
            return GetWebServicesProxy().CreateRiskRole(request);
        }

        public WebsiteRiskRoleEditResponse EditRiskRole(WebsiteRiskRoleEditRequest request)
        {
            return GetWebServicesProxy().EditRiskRole(request);
        }

        public WebsiteRiskRoleDeleteResponse DeleteRiskRole(WebsiteRiskRoleDeleteRequest request)
        {
            return GetWebServicesProxy().DeleteRiskRole(request);
        }

        public FindUserNameResponse FindUserName(FindUserNameRequest request)
        {
            return GetWebServicesProxy().FindUserName(request);
        }


        public SendElmahErrorResponse SendElmahError(SendElmahErrorRequest request)
        {
            return GetWebServicesProxy().SendElmahError(request);
        }


        public GetListOfBatchPriorityClientsResponse GetListOfBatchPriorityClients(GetListOfBatchPriorityClientsRequest request)
        {
            return GetWebServicesProxy().GetListOfBatchPriorityClients(request);   
        }

        public SaveBatchPriorityClientsResponse SaveBatchPriorityClients(SaveBatchPriorityClientsRequest request)
        {
            return GetWebServicesProxy().SaveBatchPriorityClients(request);
        }


        public GetBatchQueueResponse GetBatchQueue(GetBatchQueueRequest request)
        {
            return GetWebServicesProxy().GetBatchQueue(request);
        }


        public SaveOverriddenBatchPriorityResponse SaveOverriddenBatchPriority(SaveOverriddenBatchPriorityRequest request)
        {
            return GetWebServicesProxy().SaveOverriddenBatchPriority(request);
        }


        public WebSearchResultsResponse FindSearchResults(WebSearchResultsRequest request)
        {
            return GetWebServicesProxy().FindSearchResults(request);
        }
    }
}