﻿using KeoghsClientSite.StaticClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace KeoghsClientSite.ActionFilters
{
    public class RedirectingAction : ActionFilterAttribute
    {
        
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            bool _resultsEnabled = false;
            bool _uploadEnabled = false;
            bool _adminEnabled = false;
            bool _singleClaimEnabled = false;
            bool _searchEnabled = false;
            
            base.OnActionExecuting(context);

            string controllerName = context.ActionDescriptor.ControllerDescriptor.ControllerName;

            foreach (var item in SiteHelpers.LoggedInUser.TemplateFunctions)
            {
                switch (item.Key)
                {
                    case "chkEnabled_5_RESULTS":
                        _resultsEnabled = true;
                        break;
                    case "chkEnabled_1_UPLOAD":
                        _uploadEnabled = true;
                        break;
                    case "chkEnabled_25_ADAMAINTENANCE":
                        _adminEnabled = true;
                        break;
                    case "chkEnabled_4_SingleClaimInput":
                        _singleClaimEnabled = true;
                        break;
                    case "chkEnbaled_26_SEARCH":
                        _searchEnabled = true;
                        break;
                }
            }



            switch (controllerName)
            {
                case "Home":

                    if (!_resultsEnabled)
                        Redirect(context, "Upload");
                    break;
                case "Upload":
                    if (!_uploadEnabled)
                        Redirect(context, "Home");
                    break;
                case "Admin":
                    if (!_adminEnabled)
                        Redirect(context, "Home");
                    break;
                case "Search":
                    if (!_searchEnabled)
                        Redirect(context, "Search");
                    break;
                case "SingleClaim":
                    if (!_singleClaimEnabled)
                    {
                        if (!_resultsEnabled)
                            Redirect(context, "Upload");
                        else
                            Redirect(context, "Home");
                    }
                    if (!_uploadEnabled)
                        Redirect(context, "Home");
                   
                    break;
            }
        }

        private static void Redirect(ActionExecutingContext context, string controllerName)
        {
            context.Result = new RedirectToRouteResult(new RouteValueDictionary(new
            {
                controller = controllerName,
                action = "Index"
            }));
        }
    }
}