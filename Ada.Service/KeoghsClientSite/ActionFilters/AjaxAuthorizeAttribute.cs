﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.ActionFilters
{
    public class AjaxAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext context)
        {
            //
            // returns a 401 already
            base.HandleUnauthorizedRequest(context);
            if (context.HttpContext.Request.IsAjaxRequest())
            {
                // we simply have to tell mvc not to redirect to login page
                context.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
                context.HttpContext.Response.StatusCode = 401;
            }
            //
            //if (context.HttpContext.Request.IsAjaxRequest())
            //{
            //    var urlHelper = new UrlHelper(context.RequestContext);
            //    //context.HttpContext.Response.StatusCode = 403;
            //    context.Result = new JsonResult
            //    {
            //        Data = new
            //        {
            //            Error = "NotAuthorized",
            //            LogOnUrl = urlHelper.Action("LogOn", "Account")
            //        },
            //        JsonRequestBehavior = JsonRequestBehavior.AllowGet
            //    };
            //}
            //else
            //{
            //    base.HandleUnauthorizedRequest(context);
            //}
        }
        //
        //public override void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    //Called when a process requests authorization.
        //    base.OnAuthorization(filterContext);
        //}
    }
}