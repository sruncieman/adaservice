﻿using KeoghsClientSite.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace KeoghsClientSite.StaticClasses
{
    public static class ExtensionMethods
    {
        public static MvcHtmlString EnumDropDownList<TEnum>(this HtmlHelper htmlHelper, string name, TEnum selectedValue)
        {
            IEnumerable<TEnum> values = Enum.GetValues(typeof (TEnum))
                                            .Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                from value in values
                select new SelectListItem
                    {
                        Text = value.ToString(),
                        Value = value.ToString(),
                        Selected = (value.Equals(selectedValue))
                    };

            return htmlHelper.DropDownList(
                name,
                items
                );
        }

        //
        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper,
                                                                       Expression<Func<TModel, TEnum>> expression)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<TEnum> values = Enum.GetValues(typeof (TEnum)).Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                values.Select(value => new SelectListItem
                    {
                        Text = value.ToString(),
                        Value = value.ToString(),
                        Selected = value.Equals(metadata.Model)
                    });

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }

        //
        public static MvcHtmlString PageLinks(this HtmlHelper html, PagingInfo PagingInfo,
                                              Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            //
            result.Append(Convert.ToString(PagingInfo.CurrentPage));
            result.Append(" of " + PagingInfo.TotalPages);

            //
            ////for (int i = 1; i <= claimsFilterViewModel.PagingInfo.TotalPages; i++)
            ////{
            ////    TagBuilder tag = new TagBuilder("a");
            ////    // Add model properties
                
            ////    //tag.MergeAttribute("href","javascript:void(0)");
            ////    tag.MergeAttribute("href", "#");
            ////    //
            ////    //tag.MergeAttribute("id", "Page" + i);
            ////    tag.MergeAttribute("title", "Page" + i);
            ////    //
            ////    tag.InnerHtml = i.ToString();
            ////    if (i == claimsFilterViewModel.PagingInfo.CurrentPage)
            ////        tag.AddCssClass("pageselected");
            ////    else
            ////        tag.AddCssClass("pagelink");
            ////    result.Append(tag.ToString());

            ////}
            return MvcHtmlString.Create(result.ToString());

        }

        public static MvcHtmlString SetActiveUploadMenu(this UrlHelper urlHelper, string Controller, string ActionMethod)
        {
            var routeValueDictionary = urlHelper.RequestContext.RouteData.Values;

            string controller = routeValueDictionary["controller"].ToString() ;
            string actionMethod  = routeValueDictionary["action"].ToString() ;
            
            if (controller== Controller && actionMethod == ActionMethod)
            {
                return MvcHtmlString.Create("tab_active");
            }
            else
                return MvcHtmlString.Empty;
        }

    }

    public static class MenuExtensions
    {
        public static MvcHtmlString MenuItem(
            this HtmlHelper htmlHelper,
            string text,
            string action,
            string controller,
            string liCssClass = null
        )
        {
            var li = new TagBuilder("li");
            if (!String.IsNullOrEmpty(liCssClass))
            {
                li.AddCssClass(liCssClass);
            }
            var routeData = htmlHelper.ViewContext.RouteData;
            var currentAction = routeData.GetRequiredString("action");
            var currentController = routeData.GetRequiredString("controller");
            if (string.Equals(currentAction, action, StringComparison.OrdinalIgnoreCase) &&
                string.Equals(currentController, controller, StringComparison.OrdinalIgnoreCase))
            {
                li.AddCssClass("active");
            }
            li.InnerHtml = String.Format("<a href=\"{0}\"><i class=\"glyphicon glyphicon-chevron-right\"></i>{1}</a>",
               new UrlHelper(htmlHelper.ViewContext.RequestContext).Action(action, controller).ToString()
                , text);
            return MvcHtmlString.Create(li.ToString());
        }
    }
}