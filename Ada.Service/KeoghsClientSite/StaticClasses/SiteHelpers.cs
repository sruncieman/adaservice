﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using KeoghsClientSite.Models;
using KeoghsClientSite.Repositories;
using MDA.WCF.WebServices.Messages;
using System.Web.Mvc;
using MDA.WCF.WebServices.Interface;
using System.Text.RegularExpressions;



namespace KeoghsClientSite.StaticClasses
{
    public static class SiteHelpers
    {

        public static ExRiskUser LoggedInUser
         {
             get
             {
                 ////if (HttpContext.Current.Session["UserInfo"] == null)
                 ////{
                 ////    var context = new UsersContext();
                 ////    if (HttpContext.Current.User.Identity.IsAuthenticated)
                 ////    {
                 ////        var username = HttpContext.Current.User.Identity.Name;
                 ////        // var user = context.UserProfiles.SingleOrDefault(u => u.UserName == username);
                 ////        var user = Repository.GetRiskUserInfo(username);
                 ////        LoggedInUser = user;
                 ////        return user;
                 ////    }
                 ////    else
                 ////        return null;
                 ////}
                 return HttpContext.Current.Session["UserInfo"] as ExRiskUser;
             }
             set 
             { 
                 HttpContext.Current.Session["UserInfo"] = value; 
             }
         }


        
        public static IEnumerable<SelectListItem> GetAssignedRiskClientsForUser(int userId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            GetAssignedRiskClientsForUserResponse response = proxy.GetAssignedRiskClientsForUser(new GetAssignedRiskClientsForUserRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                FilterUserId = userId
            });
            //
            var clientList = response.ClientList.Where(x => x.Status == 1).Select(client =>
                    new SelectListItem
                    {
                        Text = client.ClientName.Replace(" (Default)", ""),
                        Value = client.Id.ToString()
                    });
        
            return clientList;
        }

        public static int GetAssignedRiskClientsForUserCount(int userId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            GetAssignedRiskClientsForUserResponse response = proxy.GetAssignedRiskClientsForUser(new GetAssignedRiskClientsForUserRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                FilterUserId = userId
            });
            return response.ClientList.Where(x=>x.Status==1).Count();
        }

        public static IEnumerable<SelectListItem> GetRiskClients(int clientId = -1)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RiskClientsResponse response = proxy.GetRiskClients(new RiskClientsRequest()
            {
                ClientId       = SiteHelpers.LoggedInUser.RiskClientId,
                UserId         = SiteHelpers.LoggedInUser.Id,
                Who            = SiteHelpers.LoggedInUser.UserName,

                FilterClientId = clientId
            });


            var orderedList = response.ClientList.OrderBy(client => client.ClientName).ToList();

            var clientList = orderedList.OrderBy(client => !client.ClientName.Contains("Keoghs")).Select(client =>
                      new SelectListItem
                      {
                          Text  = client.ClientName,
                          Value = client.Id.ToString()
                      });

            return clientList;
        }

        public static int GetRiskClientsCount(int clientId = -1)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RiskClientsResponse response = proxy.GetRiskClients(new RiskClientsRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                FilterClientId = clientId
            });

            return response.ClientList.Count();
        }


        public static ExRiskClient LoggedInClient
        {
            get
            {
               
                return HttpContext.Current.Session["ClientInfo"] as ExRiskClient;
            }

            set
            {
                HttpContext.Current.Session["ClientInfo"] = value;
            }
        }

        //
        // Returns true if the users browser is supported, false if not
        public static string SupportedBrowser()
        {

            //string ReturnMessage = "";
            //
            System.Web.HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
            //
            string BrowserName = "";
            decimal IEVersion;
            //
            BrowserName = browser.Browser;
            //

            if (Regex.IsMatch(HttpContext.Current.Request.UserAgent, @"Trident/7.") == false)
            {

                if (BrowserName.ToUpper() == "IE" || BrowserName.ToUpper() == "INTERNETEXPLORER" || BrowserName.ToUpper() == "MOZILLA") // IE11 identifies itself as a Mozilla browser
                {
                    // User is using IE browser
                    try
                    {
                        // User is using Internet Explorer
                        IEVersion = Convert.ToDecimal(browser.Version);

                        if (IEVersion < 9)
                        {
                            return "You are viewing this website with an unsupported browser. Only Internet Explorer versions 9 and 10 are currently supported.";
                        }
                    }
                    catch (Exception excep)
                    {
                        return "Error: " + excep.Message;
                    }
                }
                else
                {
                    return "You are viewing this website with an unsupported browser. Only Internet Explorer versions 9 and 10 are currently supported.";
                }
            }
            
            //    string s = "Browser Capabilities\n"
            //+ "Type = " + browser.Type + "\n"
            //+ "Name = " + browser.Browser + "\n"
            //+ "Version = " + browser.Version + "\n"
            //+ "Major Version = " + browser.MajorVersion + "\n"
            //+ "Minor Version = " + browser.MinorVersion + "\n"
            //+ "Platform = " + browser.Platform + "\n"
            //+ "Is Beta = " + browser.Beta + "\n"
            //+ "Is Crawler = " + browser.Crawler + "\n"
            //+ "Is AOL = " + browser.AOL + "\n"
            //+ "Is Win16 = " + browser.Win16 + "\n"
            //+ "Is Win32 = " + browser.Win32 + "\n"
            //+ "Supports Frames = " + browser.Frames + "\n"
            //+ "Supports Tables = " + browser.Tables + "\n"
            //+ "Supports Cookies = " + browser.Cookies + "\n"
            //+ "Supports VBScript = " + browser.VBScript + "\n"
            //+ "Supports JavaScript = " +
            //    browser.EcmaScriptVersion.ToString() + "\n"
            //+ "Supports Java Applets = " + browser.JavaApplets + "\n"
            //+ "Supports ActiveX Controls = " + browser.ActiveXControls
            //      + "\n"
            //+ "Supports JavaScript Version = " +
            //    browser["JavaScriptVersion"] + "\n";

            return "";

        }

        public static string BrowserCapabilities()
        {
            System.Web.HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
            //
            string sBrowserCapabilities = "Browser Capabilities: "
           + " Type = " + browser.Type
           + "; Name = " + browser.Browser
           + "; Version = " + browser.Version
           + "; Major Version = " + browser.MajorVersion
           + "; Minor Version = " + browser.MinorVersion;

            return sBrowserCapabilities;
        }

        public static ExRiskClient GetRiskClientInfo(int clientId)
        {
            var proxy = Helper.Services.CreateADAServices();

            RiskClientsResponse response = proxy.GetRiskClients(new RiskClientsRequest()
            {
                ClientId = clientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
            });

            var firstClient = response.ClientList.FirstOrDefault();

            return firstClient;

        }

    }

    

}