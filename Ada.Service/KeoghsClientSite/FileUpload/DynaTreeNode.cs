﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.FileUpload
{
    public class DynaTreeNode
    {


        //    children: [   
        //{    title: null, // (required) Displayed name of the node (html is allowed here)  
        //    key: null, // May be used with activate(), select(), find(), ...   
        //    isFolder: false, // Use a folder icon. Also the node is expandable but not selectable. 
        //    isLazy: false, // Call onLazyRead(), when the node is expanded for the first time to allow for delayed creation of children. 
        //    tooltip: null, // Show this popup text.  
        //    href: null, // Added to the generated <a> tag.  
        //    icon: null, // Use a custom image (filename relative to tree.options.imagePath). 'null' for default icon, 'false' for no icon.  
        //    addClass: null, // Class name added to the node's span tag.  
        //    noLink: false, // Use <span> instead of <a> tag for this node 
        //    activate: false, // Initial active status.   
        //    focus: false, // Initial focused status.   
        //    expand: false, // Initial expanded status.  
        //    select: false, // Initial selected status.   
        //    hideCheckbox: false, // Suppress checkbox display for this node.   
        //    unselectable: false, // Prevent selection.   
        //    // The following attributes are only valid if passed to some functions:    
        //    children: null // Array of child nodes.   
        //    // NOTE: we can also add custom attributes here.    
        //    // This may then also be used in the onActivate(), onSelect() or onLazyTree() callbacks.    },    […]

        //}

        public string title { get; set; }
        //
        public string key { get; set; }
        //
        public bool isFolder { get; set; }
        //
        //public bool isLazy { get; set; }
        ////
        //public string tooltip { get; set; }
        ////
        //public string href { get; set; }
        ////
        //public string icon { get; set; }
        ////
        //public string addClass { get; set; }
        ////
        //public bool noLink { get; set; }
        ////
        //public bool activate { get; set; }
        ////
        //public bool focus { get; set; }
        ////
        //public bool expand { get; set; }
        ////
        //public bool select { get; set; }
        ////
        //public bool hideCheckbox { get; set; }
        ////
        //public bool unselectable { get; set; }
        //
        public List<DynaTreeNode> children { get; set; }
        //
        public DynaTreeNode()
        {
            this.children = new List<DynaTreeNode>();
        }
    }
}