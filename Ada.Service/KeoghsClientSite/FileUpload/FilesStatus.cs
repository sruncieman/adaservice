﻿using System;
using System.IO;

namespace jQuery_File_Upload.MVC3.Upload
{
    public class FilesStatus
    {
        //public const string HandlerPath = "/Upload/";

        //
        public string name { get; set; }
        //
        public int size { get; set; }
        //
        public string BatchStatusAsString { get; set; }
        //
        public int BatchStatus { get; set; }
        //
        public string BatchError { get; set; }
        //
        public string VerificationResults { get; set; }
        //
        public string MappingResults { get; set; }
        //
        public FilesStatus() { }

        //public FilesStatus(FileInfo fileInfo)
        //{ 
        //    SetValues(fileInfo.Name, (int)fileInfo.Length);
        //}

        public FilesStatus(string fileName, int fileLength)
        {
            name = fileName;
            size = fileLength;
        }

        //private void SetValues(string fileName, int fileLength)
        //{
           
        //    //
        //}

    }
}