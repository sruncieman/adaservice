﻿using KeoghsClientSite.Repositories;
using MDA.WCF.WebServices.Messages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.SessionState;

namespace jQuery_File_Upload.MVC3.Upload
{
    /// <summary>
    /// Summary description for UploadHandler
    /// </summary>
    public class UploadHandler : IHttpHandler, IRequiresSessionState 
    {
        private readonly JavaScriptSerializer js;


        public UploadHandler()
        {
            js = new JavaScriptSerializer();
            js.MaxJsonLength = 41943040;
        }

        public bool IsReusable { get { return false; } }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.AddHeader("Cache-Control", "private, no-cache");

            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // If user not logged in re-direct to login page
                var statuses = new List<FilesStatus>();
                statuses.Add(new FilesStatus("NotLoggedIn", 0));
                statuses[0].BatchStatusAsString = "NotLoggedIn";
                statuses[0].BatchError="NotLoggedIn";
                WriteJsonIframeSafe(context, statuses);

            }
            else
            {
                HandleMethod(context);
            }

        }

        // Handle request based on method
        private void HandleMethod(HttpContext context)
        {
            switch (context.Request.HttpMethod)
            {
               
                case "POST":
                case "PUT":
                    UploadFile(context);
                    break;

                default:
                    context.Response.ClearHeaders();
                    context.Response.StatusCode = 405;
                    break;
            }
        }

        // Upload file to the server
        private void UploadFile(HttpContext context)
        {
            var statuses = new List<FilesStatus>();
            var headers = context.Request.Headers;

            UploadWholeFile(context, statuses);

            WriteJsonIframeSafe(context, statuses);
        }

        // Upload entire file
        private void UploadWholeFile(HttpContext context, List<FilesStatus> statuses)
        {

            //ExRiskBatch riskBatch;
            GetBatchStatusResponse batchStatusResponse;
            //
            try
            {
                var file = context.Request.Files[0];
           
                string fullName = Path.GetFileName(file.FileName);
                statuses.Add(new FilesStatus(fullName, file.ContentLength));
                //
                // get batchref
                var batchref = context.Request.Form["batchref"];
                //
                // Upload the file
                Repository.CreateBatchFromUpload(file, batchref, fullName, file.ContentType);

                // Wait for time period before checking status of batch upload
                //Thread.Sleep(Convert.ToInt32(ConfigurationManager.AppSettings["BatchUploadThreadSleepTime"]));
                //
                batchStatusResponse = Repository.GetBatchStatus(batchref);
                //
                if (batchStatusResponse.Error == null)
                {
                    //
                    statuses[0].BatchStatus = batchStatusResponse.RiskBatch.BatchStatus;
                    statuses[0].BatchStatusAsString = batchStatusResponse.RiskBatch.BatchStatusAsString;
                    //statuses[0].MappingResults = batchStatusResponse.RiskBatch.MappingResults != null ? batchStatusResponse.RiskBatch.MappingResults.Text : "";
                    //statuses[0].VerificationResults = batchStatusResponse.RiskBatch.VerificationResults != null ? batchStatusResponse.RiskBatch.VerificationResults.Text : "";
                    statuses[0].BatchError = "";
                }
                else
                {
                    // Unhandled error display appropriate message
                    statuses[0].BatchError = batchStatusResponse.Error;
                    statuses[0].BatchStatus = -99;
                }
            }
            catch (Exception excep)
            {
                if (statuses.Count() == 0)
                {
                    statuses.Add(new FilesStatus() { BatchError = excep.Message, BatchStatus = -99, BatchStatusAsString="File size exceeded"});
                }
                else
                {
                    statuses[0].BatchError = excep.Message;
                    statuses[0].BatchStatus = -99;
                }
                //excep.
            }

        }

        private void WriteJsonIframeSafe(HttpContext context, List<FilesStatus> statuses)
        {
            context.Response.AddHeader("Vary", "Accept");
           // context.Response.ContentType = "application/json";
            try
            {
                if (context.Request["HTTP_ACCEPT"].Contains("application/json"))
                    context.Response.ContentType = "application/json";
                else
                    context.Response.ContentType = "text/plain";
            }
            catch
            {
                context.Response.ContentType = "text/plain";
            }

            var jsonObj = js.Serialize(statuses.ToArray());
            context.Response.Write(jsonObj);
        }

    }
}