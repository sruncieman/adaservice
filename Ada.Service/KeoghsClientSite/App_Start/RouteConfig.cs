﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace KeoghsClientSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("favicon.ico");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}/{batchRef}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, batchRef = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ClaimListDefault",
                url: "{controller}/{action}/{id}/{batchRef}/{uploadedBy}/{description}",
                defaults: new { controller = "Claims", action = "ClaimDetails", id = UrlParameter.Optional, batchRef = UrlParameter.Optional, uploadedBy = UrlParameter.Optional , description=UrlParameter.Optional }
            );
        }
    }
}