﻿using KeoghsClientSite.ActionFilters;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new XframeOptionsFilter());

        }
    }
}