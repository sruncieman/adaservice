﻿
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using KeoghsClientSite.Controllers;
using MDA.WCF.WebServices.Messages;
using WebMatrix.WebData;
using System.Web.Configuration;

namespace KeoghsClientSite
{
    public class MemRoleProviderWeb : RoleProvider
    {
        private string _appName = "ADA";

        public override string ApplicationName
        {
            get
            {
                return _appName;
            }
            set
            {
                _appName = value;
            }
        }

        ADAWebServiceReference.ADAWebServicesClient GetWebServicesProxy()
        {
            ADAWebServiceReference.ADAWebServicesClient p = new ADAWebServiceReference.ADAWebServicesClient();

            var pType = MDA.Common.Config.UserAccountConfig.GetConfig;

            p.ClientCredentials.Windows.ClientCredential.Domain = pType.UserDomain.Value;
            p.ClientCredentials.Windows.ClientCredential.UserName = pType.UserName.Value;
            p.ClientCredentials.Windows.ClientCredential.Password = pType.UserPassword.Value; 

            return p;
        }

        public override string[] GetRolesForUser(string username)
        {
            return GetWebServicesProxy().GetRolesForUser( new GetRolesForUserRequest() { ClientId = 0, UserId = 0, UserName = username, Who =""});
        }


        public override bool IsUserInRole(string username, string roleName)
        {
            return
                GetWebServicesProxy()
                    .IsUserInRole(new IsUserInRoleRequest()
                    {
                        ClientId = 0,
                        UserId = 0,
                        RoleName = roleName,
                        UserName = username,
                        Who = ""
                    });
            ;
        }


        #region Not Implemented
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }


        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }


        #endregion
    }

    public class MemProviderWeb : ExtendedMembershipProvider
    {
        //private readonly IUsersService usersService;
        private int _MaxInvalidPasswordAttempts;

        public MemProviderWeb()
        {
            //this.usersService = (IUsersService)MvcApplication.Container.Resolve(typeof(IUsersService), null);
        }

        ADAWebServiceReference.ADAWebServicesClient GetWebServicesProxy()
        {
            //var config = WebConfigurationManager.OpenWebConfiguration("/");

            //var configSection = config.GetSection("userAccountSection");

            //configSection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");

            //config.Save();

            ADAWebServiceReference.ADAWebServicesClient p = new ADAWebServiceReference.ADAWebServicesClient();

            var pType = MDA.Common.Config.UserAccountConfig.GetConfig;

            p.ClientCredentials.Windows.ClientCredential.Domain = pType.UserDomain.Value;
            p.ClientCredentials.Windows.ClientCredential.UserName = pType.UserName.Value;
            p.ClientCredentials.Windows.ClientCredential.Password = pType.UserPassword.Value; 

            return p;
        }

        #region MembershipProvider

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            var request = new ChangePasswordRequest()
            {
                ClientId = 0,
                UserName = userName,
                UserId = 0,
                Who = "",
                OldPassword = oldPassword,
                NewPassword = newPassword
            };

            return GetWebServicesProxy().ChangePassword(request);
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out System.Web.Security.MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override System.Web.Security.MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
            { throw new ArgumentNullException("config"); }

            if (string.IsNullOrEmpty(name))
            { name = "WebMembershipProvider"; }

            base.Initialize(name, config);

            _MaxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return _MaxInvalidPasswordAttempts; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override System.Web.Security.MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(System.Web.Security.MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            //var userProfile = this.usersService.GetUserProfile(username);
            //if (userProfile == null)
            //{
            //    return false;
            //}
            //var membership = this.usersService.GetMembership(userProfile.UserId);
            //if (membership == null)
            //{
            //    return false;
            //}
            //if (!membership.IsConfirmed)
            //{
            //    return false;
            //}
            //if (membership.PasswordSalt == this.usersService.GetHash(password))
            //{
            //    return true;
            //}
            //// first once time we can validate through membership ConfirmationToken, 
            //// to be logged in immediately after confirmation
            //if (membership.ConfirmationToken != null)
            //{
            //    if (membership.ConfirmationToken == password)
            //    {
            //        membership.ConfirmationToken = null;
            //        this.usersService.Save(membership, add: false);
            //        return true;
            //    }
            //}

            // there was no endpoint listening

            //bool ValidateUser = false;

            //try
            //{
            //    ValidateUser = GetWebServicesProxy().ValidateUser(username, password);
            //}

            //catch(Exception ex)
            //{
            //    var context = new RequestContext(
            //    new HttpContextWrapper(System.Web.HttpContext.Current),
            //    new RouteData());
            //    var urlHelper = new UrlHelper(context);
            //    var url = urlHelper.Action("NotFound", "Error");
            //    HttpContext.Current.Response.Redirect(url);
            //}

            //return ValidateUser;

            var request = new ValidateUserRequest()
            {
                ClientId = 0,
                UserName = username,
                UserId = 0,
                Who = "",
                Password = password,
                IPAddress = HttpContext.Current.Request.UserHostAddress
            };

            return GetWebServicesProxy().ValidateUser(request);
        }

        #endregion MembershipProvider

        #region ExtendedMembershipProvider

        public override bool ConfirmAccount(string accountConfirmationToken)
        {
            //var membership = this.usersService.GetMembershipByConfirmToken(accountConfirmationToken, withUserProfile: false);
            //if (membership == null)
            //{
            //    throw new Exception("Activation code is incorrect.");
            //}
            //if (membership.IsConfirmed)
            //{
            //    throw new Exception("Your account is already activated.");
            //}
            //membership.IsConfirmed = true;
            //this.usersService.Save(membership, add: false);
            return true;
        }

        public override bool ConfirmAccount(string userName, string accountConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override string CreateAccount(string userName, string password, bool requireConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override string CreateUserAndAccount(string userName, string password, bool requireConfirmation, IDictionary<string, object> values)
        {
            //userName = userName.Trim().ToLower();

            //var userProfile = this.usersService.GetUserProfile(userName);
            //if (userProfile != null)
            //{
            //    throw new MembershipCreateUserException(MembershipCreateStatus.DuplicateEmail);
            //}

            //var newUserProfile = new UserProfile { UserName = userName, DisplayName = userName };
            //this.usersService.Save(newUserProfile);

            //var membership = new App.Core.Data.Membership 
            //{
            //    UserId = newUserProfile.UserId,
            //    CreateDate = DateTime.Now,
            //    PasswordSalt = this.usersService.GetHash(password),
            //    IsConfirmed = false,
            //    ConfirmationToken = Guid.NewGuid().ToString().ToLower()
            //};
            //this.usersService.Save(membership, add: true);

            //return membership.ConfirmationToken;

            string email = (values != null && values.ContainsKey("email")) ? (string)values["email"] : "";
            int clientId = (values != null && values.ContainsKey("clientId")) ? (int)values["clientId"] : 0;
            int teamId = (values != null && values.ContainsKey("teamId")) ? (int)values["teamId"] : 0;
            int roleId = (values != null && values.ContainsKey("roleId")) ? (int)values["roleId"] : 0;

            string firstName = (values != null && values.ContainsKey("firstName")) ? (string)values["firstName"] : "";
            string lastName = (values != null && values.ContainsKey("lastName")) ? (string)values["lastName"] : "";
            string telephoneNumber = (values != null && values.ContainsKey("telephoneNumber")) ? (string)values["telephoneNumber"] : "";

            bool? autoLoader = (values != null && values.ContainsKey("autoLoader")) ? (bool)values["autoLoader"] : false;
            int? status = (values != null && values.ContainsKey("statusId")) ? (int)values["statusId"] : 0;

            int? userType = (values != null && values.ContainsKey("userType")) ? (int)values["userType"] : 0;

            var request = new CreateUserRequest()
            {
                ClientId = clientId,
                UserName = userName,
                UserId = 0,
                Who = "",
                Password = password,
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                TelephoneNumber = telephoneNumber,
                RoleId = roleId,
                TeamId = teamId,
                Autoloader = autoLoader,
                Status = status,
                UserTypeId = userType
                
            };

            GetWebServicesProxy().CreateUser(request);

            return "";
        }

        public override bool DeleteAccount(string userName)
        {
            throw new NotImplementedException();
        }

        public override string GeneratePasswordResetToken(string userName, int tokenExpirationInMinutesFromNow)
        {
            throw new NotImplementedException();
        }

        public override ICollection<OAuthAccountData> GetAccountsForUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetCreateDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetLastPasswordFailureDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetPasswordChangedDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override int GetPasswordFailuresSinceLastSuccess(string userName)
        {
            var request = new GetPasswordFailuresSinceLastSuccessRequest()
            {
                ClientId = 0,
                UserName = userName,
                UserId = 0,
                Who = ""
            };
            return GetWebServicesProxy().GetPasswordFailuresSinceLastSuccess(request);
        }

        public override int GetUserIdFromPasswordResetToken(string token)
        {
            throw new NotImplementedException();
        }

        public override bool IsConfirmed(string userName)
        {
            throw new NotImplementedException();
        }

        public override bool ResetPasswordWithToken(string token, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override int GetUserIdFromOAuth(string provider, string providerUserId)
        {
            //var oAuthMembership = this.usersService.GetOAuthMembership(provider, providerUserId);
            //if (oAuthMembership != null)
            //{
            //    return oAuthMembership.UserId;
            //}
            return -1;
        }

        public override void CreateOrUpdateOAuthAccount(string provider, string providerUserId, string userName)
        {
            //var userProfile = this.usersService.GetUserProfile(userName);
            //if (userProfile == null)
            //{
            //    throw new Exception("User profile was not created.");
            //}
            //this.usersService.SaveOAuthMembership(provider, providerUserId, userProfile.UserId);
        }

        public override string GetUserNameFromId(int userId)
        {
            //var userProfile = this.usersService.GetUserProfile(userId);
            //if (userProfile != null)
            //{
            //    return userProfile.UserName;
            //}
            return null;
        }

        #endregion ExtendedMembershipProvider

        /*#region Helpers
        private const string salt = "HJIO6589";
        public static string GetHash(string text)
        {
            var buffer = Encoding.UTF8.GetBytes(String.Concat(text, salt));
            var cryptoTransformSHA1 = new SHA1CryptoServiceProvider();
            string hash = BitConverter.ToString(cryptoTransformSHA1.ComputeHash(buffer)).Replace("-", "");
            return hash;
        }
        #endregion Helpers*/

        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }

        public string UpdateUser(string userName, string password, bool requireConfirmation, IDictionary<string, object> values)
        {
            string email = (values != null && values.ContainsKey("email")) ? (string)values["email"] : "";
            int clientId = (values != null && values.ContainsKey("clientId")) ? (int)values["clientId"] : 0;
            int teamId = (values != null && values.ContainsKey("teamId")) ? (int)values["teamId"] : 0;
            int roleId = (values != null && values.ContainsKey("roleId")) ? (int)values["roleId"] : 0;

            string firstName = (values != null && values.ContainsKey("firstName")) ? (string)values["firstName"] : "";
            string lastName = (values != null && values.ContainsKey("lastName")) ? (string)values["lastName"] : "";
            string telephoneNumber = (values != null && values.ContainsKey("telephoneNumber")) ? (string)values["telephoneNumber"] : "";

            bool? autoLoader = (values != null && values.ContainsKey("autoLoader")) ? (bool)values["autoLoader"] : false;
            int? status = (values != null && values.ContainsKey("statusId")) ? (int)values["statusId"] : 0;

            int? userType = (values != null && values.ContainsKey("userType")) ? (int)values["userType"] : 0;

            var request = new UpdateUserRequest()
            {
                ClientId = clientId,
                UserName = userName,
                UserId = 0,
                Who = "",
                Password = password,
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                TelephoneNumber = telephoneNumber,
                RoleId = roleId,
                TeamId = teamId,
                Autoloader = autoLoader,
                Status = status,
                UserTypeId = userType

            };

            GetWebServicesProxy().UpdateUser(request);

            return "";
        }

    }
}