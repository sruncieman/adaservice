﻿using MDA.WCF.WebServices.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models
{
    public class SearchClaimsListViewModel
    {

        public SearchClaimsListViewModel()
        {
            searchFilterViewModel = new SearchFilterViewModel();
        }

        public List<ExSearchDetails> Results { get; set; }

        public SearchFilterViewModel searchFilterViewModel { get; set; }
    }
}