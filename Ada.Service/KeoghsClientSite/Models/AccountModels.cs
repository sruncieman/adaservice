﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using MDA.WCF.WebServices.Messages;

namespace KeoghsClientSite.Models
{
    //public class UsersContext : DbContext
    //{
    //    public UsersContext()
    //        : base("DefaultConnection")
    //    {
    //    }

    //    public DbSet<UserProfile> UserProfiles { get; set; }
    //}

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public int TeamId { get; set; }
        public string TeamName { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel 
    {
       
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Display(Name = "Users")]
        public IEnumerable<SelectListItem> Users { get; set; }

     

    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class User
    {
        public int Id { get; set; }
        public string FullName { get; set; }
    }

    public class UnlockModel
    {
        private List<User> _users;

        [Display(Name = "Users")]
        public List<ExRiskUserLocked> Users { get; set; }

        public int UnlockSuccess { get; set; }
    }

    public class RegisterModel
    {
        [Display(Name = "Username")]
        [Required]
        [Remote("doesUserNameExist", "Account", HttpMethod = "POST", ErrorMessage = "Username already exists. Please enter a different Username.")]
        public string UserName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Telephone Number")]
        public string Telephone { get; set; }

        [Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*[!#$%&()*+,-./:?£^_=@|<>¬~])(?=.*[0-9])(?=.*[a-z]).{8,20}$", ErrorMessage = "Doesnt meet standards")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Status")]
        public int? StatusId { get; set; }

        [Display(Name = "Status")]
        public IEnumerable<SelectListItem> Status { get; set; }

        [Required]
        [Display(Name = "Template")]
        public int? RoleId { get; set; }

        [Display(Name = "Roles")]
        public IEnumerable<SelectListItem> Roles { get; set; }

        //[Required]
        //[Display(Name = "Group")]
        //public int? GroupId { get; set; }

        //[Display(Name = "Groups")]
        //public IEnumerable<SelectListItem> Groups { get; set; }

        [Required]
        [Display(Name = "Client")]
        public int? ClientId { get; set; }

        [Display(Name = "Clients")]
        public IEnumerable<SelectListItem> Clients { get; set; }

        [Display(Name = "Autoloader")]
        public bool Autoloader { get; set; }

        //[Required]
        //[Display(Name = "Email Address")]
        //public string Email { get; set; }
        
        //[Required]
        //[Display(Name = "Team")]
        //public int TeamId { get; set; }

        //[Display(Name = "Teams")]
        //public IEnumerable<SelectListItem> Teams { get; set; }

        [Required]
        [Display(Name = "User Type")]
        public int? UserTypeId { get; set; }

        [Display(Name = "User Type")]
        public IEnumerable<SelectListItem> UserTypes { get; set; }


    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }

    public class RecoverPassword
    {
        [Display(Name = "Username")]
        [Required]
        [EmailAddress(ErrorMessage = "Please enter a valid email address.")]
        public string UserName { get; set; }
    }

    public class ResetPasswordModel
    {
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "New Password")]
        [DataType(DataType.Password)]
        //[Remote("IsPreviousPassword", "Account", AdditionalFields = "Username", HttpMethod = "POST", ErrorMessage = "Username already exists. Please enter a different Username.")]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*[!#$%&()*+,-./:?£^_=@|<>¬~])(?=.*[0-9])(?=.*[a-z]).{8,20}$", ErrorMessage = " ")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string ReturnToken { get; set; }
    }
}
