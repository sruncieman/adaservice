﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using MDA.WCF.WebServices.Messages;

namespace KeoghsClientSite.Models
{
    public class AdminModel
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string btnSubmit { get; set; }

        /// <summary>
        /// This is the List to which the Selection List for "Available" items is
        /// bound in initial render of view
        /// </summary>
        public IList<SelectListItem> AvailableList { get; set; }

        /// <summary>
        /// The json view-state of Availablelist
        /// </summary>
        public string currentAvailableList { get; set; }

        /// <summary>
        /// List of selected Available items submitted for swap (Add)
        /// </summary>
        public IList<string> SelectedAvailableItems { get; set; }

        /// <summary>
        /// This is the List to which the Selection List for "Assigned" items is
        /// bound in initial render of view
        /// </summary>
        public IList<SelectListItem> AssignedList { get; set; }

        /// <summary>
        /// The json view-state of AssignedList
        /// </summary>
        public string currentAssignedList { get; set; }

        /// <summary>
        /// The List of selected Assigned items submitted for swap (Remove)
        /// </summary>
        public IList<string> SelectedAssignedItems { get; set; }


        [Required]
        [Display(Name = "User")]
        public int? UserId { get; set; }

        [Display(Name = "Users")]
        public IEnumerable<SelectListItem> Users { get; set; }

        public List<ExRiskUser> UserInfo { get; set; }

        public bool AssignWarningDivIsVisible { get; set; }


        [Display(Name = "User name")]
        public string UserName { get; set; }

    }

    public class BatchQueue
    {
        public int BatchId { get; set; }

        public string Client { get; set; }
        public string BatchRef { get; set; }
        public DateTime? Uploaded { get; set; }
        public int ClaimsReceieved { get; set; }
    }
    
    public class TemplateFunctionsModel
    {
        [Display(Name = "Roles")]
        public IEnumerable<SelectListItem> Roles { get; set; }

        public string SelectedProfile { get; set; }

        public int? Profile_Id { get; set; }

        public string FunctionValues { get; set; }
    }
    
    public class TemplateFunction
    {
        public List<Dictionary<string, string>> lstTemplateFunction { get; set; }
    }
    
    public class FancyTreeItem
    {
        [JsonProperty("expanded")]
        public bool expanded { get; set; }

        [JsonProperty("folder")]
        public bool folder { get; set; }

        [JsonProperty("key")]
        public string key { get; set; }

        [JsonProperty("title")]
        public string title { get; set; }

        //[JsonProperty("data")]
        //public Data data { get; set; }

        [JsonProperty("children")]
        public List<FancyTreeItem> children { get; set; }
    

        public FancyTreeItem()
        {
            this.children = new List<FancyTreeItem>();
        }

        public string JsonToFancyTree()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }

    public class EditUserDetails
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Telephone Number")]
        public string Telephone { get; set; }

        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Status")]
        public int? StatusId { get; set; }

        [Display(Name = "Status")]
        public IEnumerable<SelectListItem> Status { get; set; }

        [Required]
        [Display(Name = "Template")]
        public int? RoleId { get; set; }

        [Display(Name = "Roles")]
        public IEnumerable<SelectListItem> Roles { get; set; }

        //[Required]
        //[Display(Name = "Group")]
        //public int? GroupId { get; set; }

        [Display(Name = "Groups")]
        public IEnumerable<SelectListItem> Groups { get; set; }

        [Required]
        [Display(Name = "Client")]
        public int? ClientId { get; set; }

        [Display(Name = "Clients")]
        public IEnumerable<SelectListItem> Clients { get; set; }

        [Display(Name = "Autoloader")]
        public bool Autoloader { get; set; }

        [Required]
        [Display(Name = "User Type")]
        public int? UserTypeId { get; set; }

        [Display(Name = "User Type")]
        public IEnumerable<SelectListItem> UserTypes { get; set; }

    }
    
    public class CreateClient
    {
        public CreateClient()
        {
            RedThreshold = 250;
            AmberThreshold = 51;

        }

        [Display(Name = "Client Name")]
        [Required]
        [Remote("doesClientNameExist", "Admin", HttpMethod = "POST", ErrorMessage = "Client name already exists. Please enter a different client name.")]
        public string ClientName { get; set; }

        [Display(Name = "Status")]
        [Required]
        public int? StatusId { get; set; }

        [Display(Name = "Status")]
        public IEnumerable<SelectListItem> Status { get; set; }

        [Display(Name = "Accepted File Extensions")]
        [Required]
        public string FileExtensions { get; set; }

        [Display(Name = "Amber ")]
        [Required(ErrorMessage = "Amber Threshold is required.")]
        //[Range(1, 99, ErrorMessage = "Must be between 1 and 99")]
        public int AmberThreshold { get; set; }

        [Display(Name = "Red ")]
        [Required(ErrorMessage = "Red Threshold is required.")]
        //[Range(1, 99, ErrorMessage = "Must be between 1 and 99")]
        public int RedThreshold { get; set; }

        [Required]
        [Display(Name = "Insurer Client")]
        public int InsurersClientsId { get; set; }

        [Display(Name = "InsurerClients")]
        public IEnumerable<SelectListItem> InsurersClients { get; set; }

    }

    public class EditClientDetails
    {

        public int Id { get; set; }

        [Display(Name = "Client Name")]
        [Required]
        [Remote("doesNewClientNameExist", "Admin", AdditionalFields = "Id", HttpMethod = "POST", ErrorMessage = "Client name already exists. Please enter a different client name.")]
        public string ClientName { get; set; }

        [Display(Name = "Status")]
        [Required]
        public int? StatusId { get; set; }

        [Display(Name = "Status")]
        public IEnumerable<SelectListItem> Status { get; set; }

        [Display(Name = "Accepted File Extensions")]
        public string FileExtensions { get; set; }


        public bool Xml { get; set; }

        public bool Xls { get; set; }

        public bool Csv { get; set; }

        public bool Txt { get; set; }

        public bool Xlsx { get; set; }

        public bool Zip { get; set; }


        [Display(Name = "Amber ")]
        [Required(ErrorMessage = "Amber Threshold is required.")]
        //[Range(1, 99, ErrorMessage = "Must be between 1 and 99")]
        public int AmberThreshold { get; set; }

        [Display(Name = "Red ")]
        [Required(ErrorMessage = "Red Threshold is required.")]
        //[Range(1, 99, ErrorMessage = "Must be between 1 and 99")]
        public int RedThreshold { get; set; }

        [Required]
        [Display(Name = "Insurer Client")]
        public int InsurersClientsId { get; set; }

        [Display(Name = "InsurerClients")]
        public IEnumerable<SelectListItem> InsurersClients { get; set; }

        public bool RecordReserveChanges { get; set; }

        public int? BatchPriority { get; set; }

    }
    
    public class EditClient
    {
        public List<ExRiskClient> ClientInfo { get; set; }
    }

    public class ViewRiskWords
    {
        public List<ExRiskWord> Words { get; set; }
    }


    public class PriorityClients
    {
        public List<PriorityClient> ListPriorityClients { get; set; }
    }


    public class PriorityClient
    {
        public string Client { get; set; }
        public int PriorityId { get; set; }
    }



    public class CreateRiskWord
    {
        [Required]
        [Display(Name = "Client")]
        public int RiskClient_Id { get; set; }

        [Display(Name = "Clients")]
        public IEnumerable<SelectListItem> Clients { get; set; }

        [Display(Name = "Table Name")]
        public string TableName { get; set; }

        [Required]
        [Display(Name = "Field Name")]
        public string FieldName { get; set; }

        [Display(Name = "Fields")]
        public IEnumerable<SelectListItem> Fields { get; set; }

        [Required]
        [Display(Name = "Anomaly")]
        public string LookupWord { get; set; }

        [Display(Name = "Replacement Word")]
        public string ReplacementWord { get; set; }

        [Display(Name = "Replace Whole String")]
        public bool ReplaceWholeString { get; set; }

        [Display(Name = "Date Added")]
        public System.DateTime DateAdded { get; set; }

        [Required]
        [Display(Name = "Search Type")]
        public string SearchTypeValue { get; set; }

        [Display(Name = "Search Type")]
        public IEnumerable<SelectListItem> SearchType { get; set; }

        //public List<SelectListItem> ClaimInfoFields { get; set; }
        //public List<SelectListItem> PolicyFields { get; set; }
        //public List<SelectListItem> AddressFields { get; set; }
        //public List<SelectListItem> BankAccountFields { get; set; }
        //public List<SelectListItem> OrganisationFields { get; set; }
        //public List<SelectListItem> PersonFields { get; set; }
        //public List<SelectListItem> VehicleFields { get; set; }

    }

    public class EditRiskWord
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Client")]
        public int RiskClient_Id { get; set; }

        [Display(Name = "Clients")]
        public IEnumerable<SelectListItem> Clients { get; set; }

        [Display(Name = "Table Name")]
        public string TableName { get; set; }

        [Display(Name = "Field Name")]
        public string FieldName { get; set; }

        [Required]
        [Display(Name = "Anomaly")]
        public string LookupWord { get; set; }

        [Display(Name = "Replacement Word")]
        public string ReplacementWord { get; set; }

        [Display(Name = "Replace Whole String")]
        public bool ReplaceWholeString { get; set; }

        [Display(Name = "Date Added")]
        public System.DateTime DateAdded { get; set; }

        [Required]
        [Display(Name = "Search Type")]
        public string SearchTypeValue { get; set; }

        [Display(Name = "Search Type")]
        public IEnumerable<SelectListItem> SearchType { get; set; }

    }

    public class DeleteRiskWord
    {
        public int Id { get; set; }
    }

    public class ViewRiskDefaultData
    {
        public List<ExRiskDefaultData> DefaultData { get; set; }
    }

    public class CreateRiskDefaultData : IValidatableObject
    {
        [Required]
        [Display(Name = "Client")]
        public int RiskClient_Id { get; set; }

        [Display(Name = "Clients")]
        public IEnumerable<SelectListItem> Clients { get; set; }
            

        [Display(Name = "Default Data")]
        public string ConfigurationValue { get; set; }

        [Display(Name = "Is Active?")]
        public bool IsActive { get; set; }

        [Required]
        [Display(Name = "Data")]
        public int RiskConfigurationDescriptionId { get; set; }

        [Display(Name = "Fields")]
        public IEnumerable<SelectListItem> Fields { get; set; }

        [Display(Name = "First Name")]
        public string OrganisationName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Sub Building")]
        public string SubBuilding { get; set; }

        [Display(Name = "Building Name")]
        public string BuildingName { get; set; }

        [Display(Name = "Building Number")]
        public string BuildingNumber { get; set; }

        [Display(Name = "Street")]
        public string Street { get; set; }

        [Display(Name = "Locality")]
        public string Locality { get; set; }

        [Display(Name = "Town")]
        public string Town { get; set; }

        [Display(Name = "County")]
        public string County { get; set; }

        [Display(Name = "Postcode")]
        public string Postcode { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (RiskConfigurationDescriptionId == 1)
            {
                 if (string.IsNullOrEmpty(SubBuilding) && string.IsNullOrEmpty(BuildingName) && string.IsNullOrEmpty(BuildingNumber) && string.IsNullOrEmpty(Street) && string.IsNullOrEmpty(Locality) && string.IsNullOrEmpty(Town) && string.IsNullOrEmpty(County) && string.IsNullOrEmpty(Postcode))
                {
                    yield return new ValidationResult("At least one address field must be populated");
                }
            }
            else if (RiskConfigurationDescriptionId == 2)
            {
                if (string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName))
                {
                    yield return new ValidationResult("Both name fields must be populated");
                }

            }

        }



    }

    public class DeleteRiskDefaultData
    {
        public int Id { get; set; }
    }

    public class ViewRiskNoteDecisions
    {
        public List<ExRiskNoteDecision> RiskNoteDecisions { get; set; }
    }

    public class CreateRiskNoteDecision
    {
        [Required]
        [Display(Name = "Client")]
        public int RiskClient_Id { get; set; }

        [Display(Name = "Clients")]
        public IEnumerable<SelectListItem> Clients { get; set; }

        [Required]
        [Display(Name = "Decision")]
        public string Decision { get; set; }

        [Display(Name = "Is Active?")]
        public byte IsActive { get; set; }
             
    }

    public class DeleteRiskNoteDecision
    {
        public int Id { get; set; }
    }

    //public class Data
    //{
    //    [JsonProperty("icon")]
    //    public string Icon { get; set; }
    //}

  

   
}