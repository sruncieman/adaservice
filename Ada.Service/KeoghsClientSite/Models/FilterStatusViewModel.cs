﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models
{
    public class FilterStatusViewModel
    {
        public string FilterStatus { get; set; }
     
        public bool IsChecked { get; set; }
    }
}