﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models
{
    public class BatchProcessingResultsAjaxResponse
    {

        public string Result { get; set; }
        //
        public string Error { get; set; }
    }
}