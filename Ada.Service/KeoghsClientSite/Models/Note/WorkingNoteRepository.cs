﻿using KeoghsClientSite.Interfaces;
using System.Collections.Generic;
using KeoghsClientSite.StaticClasses;
using MDA.WCF.WebServices.Interface;
using MDA.WCF.WebServices.Messages;
using System.Linq;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.Note
{
    public class WorkingNoteRepository : IRepository
    {
        public List<Note> GetAll()
        {
            //  ToDo: Add Repository Data
            return null;
        }
        
        public RiskNoteCreateResponse CreateNote(Note note)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RiskNoteCreateResponse response = proxy.CreateRiskNote(new RiskNoteCreateRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                CreatedBy = note.CreatedBy,
                CreatedDate = note.CreatedDate,
                Decision_Id = note.Decision_Id,
                Deleted = note.Deleted,
                NoteDesc = note.NoteDesc,
                RiskClaim_Id = note.RiskClaim_Id,
                Visibilty = note.Visibilty_Id,
                UserTypeId = note.UserTypeId,
                OriginalFile = note.OriginalFile,
                FileName = note.FileName,
                FileType = note.FileType
                
            });


            return response;
        }

        public RiskNoteUpdateResponse UpdateNote(Note note)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RiskNoteUpdateResponse response = proxy.UpdateRiskNote(new RiskNoteUpdateRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                Decision_Id = note.Decision_Id,
                Deleted = note.Deleted,
                NoteDesc = note.NoteDesc,
                Visibilty = note.Visibilty_Id,
                RiskNoteId = note.NoteId,
                UserTypeId = note.UserTypeId,
                UpdatedBy = note.UpdatedBy,
                UpdatedDate = note.UpdatedDate
                
            });


            return response;
        }

        public List<ExRiskNote> GetAll(int riskCliamId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RiskNotesResponse response = proxy.GetRiskNotes(new RiskNotesRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                FilterRiskClaimId = riskCliamId
            });


            var noteList = response.NoteList.OrderByDescending(x => x.CreatedDate);

            return noteList.ToList();
        }

        public ExRiskNote GetRiskNote(int riskNoteId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            GetRiskNoteResponse response = proxy.GetRiskNote(new GetRiskNoteRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                FilterRiskNoteId = riskNoteId
            });

            return response.RiskNote;
        }

        public IEnumerable<SelectListItem> GetAllDecisions(int riskClientId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RiskNoteDecisionsResponse response = proxy.GetRiskNoteDecisions(new RiskNoteDecisionsRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                FilterRiskClientId = riskClientId
            });

            var noteList = response.DecisionList.OrderBy(d => d.Decision).Select(d =>
              new SelectListItem
              {
                  Text = d.Decision,
                  Value = d.Id.ToString()
              });

            return noteList;
        }

        public IEnumerable<SelectListItem> GetAllVisibilityStatus()
        {
            IEnumerable<SelectListItem> ListItems = new List<SelectListItem>{
                new SelectListItem(){Text="Public", Value="1"},
                new SelectListItem(){Text="Restricted", Value="2"},
             };

            return ListItems;
        }


    }
}