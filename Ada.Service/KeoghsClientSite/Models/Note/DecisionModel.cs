﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.Note
{
    public class DecisionModel
    {
        public int Decision_Id { get; set; }

        public string Decision_Desc { get; set; }
    }
}