﻿using System;
using System.Collections.Generic;
using MDA.WCF.WebServices.Interface;
using MDA.WCF.WebServices.Messages;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web;


namespace KeoghsClientSite.Models.Note
{
    public class Note
    {
        public List<ExRiskNote> History { get; set; }

        public int NoteId { get; set; }

        [Display(Name = "Note")]
        public string NoteDesc { get; set; }

        //public int[] BaseRiskClaim_Id { get; set; }

        public int[] RiskClaim_Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        [Display(Name = "Decisions")]
        public IEnumerable<SelectListItem> Decisions { get; set; }

        [Display(Name = "Decision")]
        public int? Decision_Id { get; set; }

        [Display(Name = "Visibilty")]
        public IEnumerable<SelectListItem> Visibilty { get; set; }

        [Display(Name = "Visibilty")]
        public int? Visibilty_Id { get; set; }

        public bool Deleted { get; set; }

        [Display(Name = "User Type")]
        public int? UserTypeId { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }

        public byte[] OriginalFile { get; set; }

        [Display(Name = "Attach Intel")]
        public HttpPostedFileBase DocumentUpload { get; set; }

        public string FileName { get; set; }

        public string FileType { get; set; }

    }


}