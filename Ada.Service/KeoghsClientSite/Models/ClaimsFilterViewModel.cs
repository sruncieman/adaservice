﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MDA.WCF.WebServices.Messages;

namespace KeoghsClientSite.Models
{
    public class ClaimsFilterViewModel
    {
        public ClaimsFilterViewModel()
        {
            // 
            PagingInfo=new PagingInfo();
        }
        //
        public string SortColumn { get; set; }

        // Specifies if the sort order for the results list is in 
        // ascending or descending order
        public bool SortAscending { get; set; }

        [DisplayName("Uploaded By")]
        public string FilterUploadedBy { get; set; }

        [DisplayName("Client Batch Ref.")]
        public string FilterBatchRef { get; set; }

        [DisplayName("Claim Number")]
        public string FilterClaimNumber { get; set; }

        [DisplayName("Date range From")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? FilterStartDate { get; set; }

        //public string FilterStartDateForDisplay { get; set; }

        [DisplayName("Date range To")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? FilterEndDate { get; set; }

        [DisplayName("Level One Reports Requested Only")]
        public bool FilterReportsRequestedOnly { get; set; }
        //
        [DisplayName("Clients")]
        public IEnumerable<SelectListItem> Clients { get; set; }
        //
        public int? SelectedClientId { get; set; }
        //
        [DisplayName("Teams")]
        public IEnumerable<SelectListItem> Teams { get; set; }
        //
        public int? SelectedTeamId { get; set; }

        [DisplayName("Batches")]
        public IEnumerable<SelectListItem> Batches { get; set; }

        public string[] SelectedBatchId { get; set; }

            //
        [DisplayName("Users")]
        public IEnumerable<SelectListItem> Users { get; set; }
        //
        public int? SelectedUserId { get; set; }
        //
        //public int ItemsPerPage { get; set; }
        //// Current page in the results list
        //public int CurrentPage { get; set; }
        //
        // The controller to post to
        public string Controller { get; set; }
        //
        public IEnumerable<FilterRiskRatingViewModel> RiskRatings { get; set; }

        public IEnumerable<FilterStatusViewModel> FilterStatus { get; set; }

        public IEnumerable<ReportStatusViewModel> ReportStatus { get; set; } 
        //
        public FilterReportsRequestedViewModel ReportsRequested { get; set; }

        [DisplayName("Batches")]
        public List<ExRiskNoteDecision> Decisions { get; set; }

        public int?[] SelectedDecisionId { get; set; }

        public bool FilterNotes { get; set; }

        public bool FilterIntel { get; set; }

        //
        public PagingInfo PagingInfo { get; set; }
        //
        public string ExpectedFileExtension { get; set; }

        #region CollapsedStateMembers
        public string ClaimNumberCollapsedState { get; set; }
        public string BatchReferenceCollapsedState { get; set; }
        public string UploadedByCollapsedState { get; set; }
        public string UploadDateRangeCollapsedState { get; set; }
        public string RiskRatingCollapsedState { get; set; }
        public string StatusCollapsedState { get; set; }
        public string DecisionCollapsedState { get; set; }
        #endregion
    }
}