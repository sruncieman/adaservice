﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MDA.WCF.WebServices.Messages;
using System.Web.Routing;

namespace KeoghsClientSite.Models
{
    public class ClaimsListViewModel
    {
        public ClaimsListViewModel()
        {
            // 
            //PagingInfo=new PagingInfo();
            //
            claimsFilterViewModel = new ClaimsFilterViewModel();
        }
        //
        public List<ExRiskClaimDetails> Claims { get; set; }

        //public PagingInfo PagingInfo { get; set; }
        //
        public ClaimsFilterViewModel claimsFilterViewModel { get; set; }
        //
        //public RouteValueDictionary GetRouteValues(string SortCol)
        //{
        //    RouteValueDictionary oRouteValueDictionary;
        //    //
        //    oRouteValueDictionary = new RouteValueDictionary(new
        //    {
        //        Page = PagingInfo.CurrentPage,
        //        PageSize = PagingInfo.ItemsPerPage,
        //        SortCol = SortCol,
        //        FilterUploadedBy = !String.IsNullOrEmpty(this.claimsFilterViewModel.FilterUploadedBy) ? this.claimsFilterViewModel.FilterUploadedBy : String.Empty,
        //        FilterBatchRef = !String.IsNullOrEmpty(this.claimsFilterViewModel.FilterBatchRef) ? this.claimsFilterViewModel.FilterBatchRef : String.Empty,
        //        FilterClaimNumber = !String.IsNullOrEmpty(this.claimsFilterViewModel.FilterClaimNumber) ? this.claimsFilterViewModel.FilterClaimNumber : String.Empty,
        //        FilterStartDate = this.claimsFilterViewModel.FilterStartDate != null ? this.claimsFilterViewModel.FilterStartDate : null,
        //        FilterEndDate = this.claimsFilterViewModel.FilterEndDate != null ? this.claimsFilterViewModel.FilterEndDate : null,
        //        FilterReportsRequestedOnly = this.claimsFilterViewModel.FilterReportsRequestedOnly ? this.claimsFilterViewModel.FilterReportsRequestedOnly : false,
        //        SelectedClientId = this.claimsFilterViewModel.SelectedClientId != null ? this.claimsFilterViewModel.SelectedClientId : -1,
        //    });
        //    //
        //    return oRouteValueDictionary;
        //}
    }
}