﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models
{
    public class PagingInfo
    {
        public int TotalItems { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }
        //public string SortColumn { get; set; }

        public int PageIndex
        {
            get { return (int) Math.Ceiling((decimal)CurrentPage - 1); }
        }

        public int ItemStart 
        {
            get { return (int)Math.Ceiling((decimal)PageIndex * ItemsPerPage + 1); } 
        }
        public int ItemEnd
        {
            get { return (int)Math.Min(PageIndex * ItemsPerPage + ItemsPerPage, TotalItems); }
        }

        public int TotalPages
        {
            get { return (int) Math.Ceiling((decimal) TotalItems/ItemsPerPage); }
        }

    }
}