﻿
using ExpressiveAnnotations.Attributes;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace KeoghsClientSite.Models
{
    public class SearchFilterViewModel
    {
        public SearchFilterViewModel()
        {
            PagingInfo = new PagingInfo();
        }

        [DisplayName("First Name")]
      
        [RegularExpression(@"^.{1,}$")]
        [RequiredIf("FilterSurname == null")]
        [RequiredIf("FilterAddressLine == null")]
        [RequiredIf("FilterPostcode == null")]
        [RequiredIf("FilterVehicleRegNumber == null")]
        public string FilterFirstName { get; set; }


        [DisplayName("Surname")]
       
        [RegularExpression(@"^.{1,}$")]
        [RequiredIf("FilterFirstName  == null")]
        [RequiredIf("FilterAddressLine == null")]
        [RequiredIf("FilterPostcode == null")]
        [RequiredIf("FilterVehicleRegNumber == null")]
        public string FilterSurname { get; set; }

        [DisplayName("Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [RequiredIf("FilterFirstName  == null")]
        [RequiredIf("FilterSurname  == null")]
        [RequiredIf("FilterAddressLine == null")]
        [RequiredIf("FilterPostcode == null")]
        [RequiredIf("FilterVehicleRegNumber == null")]
        public DateTime? FilterDob { get; set; }

        [DisplayName("Line")]
        [RegularExpression(@"^.{1,}$")]
        [RequiredIf("FilterFirstName  == null")]
        [RequiredIf("FilterSurname == null")]
        [RequiredIf("FilterPostcode == null")]
        [RequiredIf("FilterVehicleRegNumber == null")]
        public string FilterAddressLine { get; set; }

        [DisplayName("Postcode")]
        [RegularExpression(@"^.{1,}$")]
        [RequiredIf("FilterFirstName  == null")]
        [RequiredIf("FilterSurname == null")]
        [RequiredIf("FilterAddressLine == null")]
        [RequiredIf("FilterVehicleRegNumber == null")]
        public string FilterPostcode { get; set; }

        [DisplayName("Registration Number")]
        [RegularExpression(@"^.{1,}$")]
        [RequiredIf("FilterFirstName  == null")]
        [RequiredIf("FilterSurname == null")]
        [RequiredIf("FilterAddressLine == null")]
        [RequiredIf("FilterPostcode == null")]
        public string FilterVehicleRegNumber { get; set; }

        public PagingInfo PagingInfo { get; set; }
    }
}