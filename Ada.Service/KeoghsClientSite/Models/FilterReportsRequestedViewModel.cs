﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models
{
    public class FilterReportsRequestedViewModel
    {
        //public Boolean? IncludeReports { get; set; }
        //
        public IEnumerable<FilterReportsRequested> ReportsRequested { get; set; }

        //
    }
    //
    public class FilterReportsRequested
    {
        public string ReportName { get; set; }
        //
        public bool IsChecked { get; set; }

    }

}