﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.CUE
{
    public class Results
    {
        public List<Dictionary<string, CUE_ReportModel>> ListResultInfo { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime SearchDate { get; set; }
    }
    
    public class CUE_ReportModel
    {
        public List<CUESearchSummary> SearchSummary { get; set; }
        public List<CUEResultsSummary> ResultsSummary { get; set; }
        public CueSearchCriteria SearchCriteria { get; set; }
        public List<CUEIncidentSummary> IncidentSummary { get; set; }
        public List<CUEIncident> CUE_Incidents { get; set; }

    }

    public class CueSearchCriteria
    {
        public string Name { get; set; }
        public string Dob { get; set; }
        public string NI_Number { get; set; }
        public string DrivingLicenceNum { get; set; }
        public string VIN { get; set; }
        public string VehicleReg { get; set; }
        public string AddressHouseName { get; set; }
        public string AddressNumber { get; set; }
        public string AddressStreet { get; set; }
        public string AddressLocality { get; set; }
        public string AddressTown { get; set; }
        public string AddressCity { get; set; }
        public string AddressPostCode { get; set; }
    }

    public class CUEIncident
    {
        public int IncidentId { get; set; }
        public string IncidentType { get; set; }
        public CUEClaimData ClaimData { get; set; }
        public CUEGeneralData GeneralData { get; set; }
        public List<CUEIncidentInvolvement> CUEIncidentInvolvements { get; set; }
        public List<CUEIncidentInvolvementVehicleDetails> CUEIncidentInvolvementVehicleDetails { get; set; }
        public List<CUESupplier> Suppliers { get; set; }
        public CUEIncidentDetails IncidentDetails { get; set; }
    }

    public class CUESupplier
    {
        public int IncidentId { get; set; }
        public string CompanyName { get; set; }
        public string SupplierStatus { get; set; }
        public string Telephone { get; set; }
        public string AddressHouseName { get; set; }
        public string AddressNumber { get; set; }
        public string AddressStreet { get; set; }
        public string AddressLocality { get; set; }
        public string AddressTown { get; set; }
        public string AddressCity { get; set; }
        public string AddressPostCode { get; set; }
        public string AddressIndicator { get; set; }
        public int VAT { get; set; }
        public decimal PaymentAmount { get; set; }
    }

    public class CUEIncidentInvolvement
    {
        public int Incident_Id { get; set; }
        public string Type { get; set; }

        public string Status { get; set; }
        public string Title { get; set; }
        public string Forename { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NI_Number { get; set; }
        public string AddressHouseName { get; set; }
        public string AddressNumber { get; set; }
        public string AddressStreet { get; set; }
        public string AddressLocality { get; set; }
        public string AddressTown { get; set; }
        public string AddressCity { get; set; }
        public string AddressPostCode { get; set; }
        public string AddressIndicator { get; set; }
        public string Sex { get; set; }
        public DateTime Dob { get; set; }
        public int VAT { get; set; }
        public decimal PaymentAmount { get; set; }
        public string Telephone { get; set; }
        public string Occupation { get; set; }
    }

    public class CUEIncidentInvolvementVehicleDetails : CUEIncidentInvolvement
    {
        public string VehicleRegistration { get; set; }
        public string VehicleIdentificationNumber { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Colour { get; set; }
        public string CoverType { get; set; }
        public string VIN { get; set; }
        public string DamageStatus { get; set; }
        public string RecoveredStatus { get; set; }
        public string RegistrationStatus { get; set; }
        public string NewForOldIndicator { get; set; }
    }

    public class CUEGeneralData
    {
        public string ClaimStatus { get; set; }
        public string IncidentDescription { get; set; }
        public string PolicyType { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicyNumberID { get; set; }
        public string CollectivePolicyIndicator { get; set; }
        public DateTime LossSetupDate { get; set; }
        public string InsurerContactName { get; set; }
        public string InsurerContactTel { get; set; }
        public DateTime IncidentDate { get; set; }
        public DateTime PolicyInceptionDate { get; set; }
        public DateTime PolicyPeriodEndDate { get; set; }
        public string CauseOfLoss { get; set; }
        public DateTime ClosedDate { get; set; }
        public string CatastropheRelated { get; set; }
        public string RiskAddressHouseName { get; set; }
        public string RiskAddressNumber { get; set; }
        public string RiskAddressStreet { get; set; }
        public string RiskAddressLocality { get; set; }
        public string RiskAddressTown { get; set; }
        public string RiskAddressCity { get; set; }
        public string RiskAddressPostCode { get; set; }
        public List<CUEPayment> CUEPayments { get; set; }
    }

    public class CUEPayment
    {
        public int IncidentId { get; set; }
        public string PaymentType { get; set; }
        public decimal PaymentAmount { get; set; }
    }


    public class CUEClaimData
    {
        public string Insurer { get; set; }
        public string ClaimNumber { get; set; }
        public List<CUEClaimDataPolicyHolder> PolicyHolders { get; set; }

    }

    public class CUEClaimDataPolicyHolder
    {

        public int IncidentId { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
    }

    public class CUESearchSummary
    {
        public string Subject { get; set; }
        public string Involvement { get; set; }

        public bool Motor { get; set; }

        public bool PI { get; set; }

        public bool Household { get; set; }

        public bool Cross_Enquiry { get; set; }

        public DateTime SearchDate { get; set; }
    }

    public class CUEResultsSummary
    {
        public string Incidents { get; set; }

        public string Involved_PI_Claims { get; set; }

        public string Involved_Motor_Claims { get; set; }

        public string Involved_Household_Claims { get; set; }
    }

    public class CUEIncidentSummary
    {
        public int IncidentId { get; set; }

        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public int PI_Claims { get; set; }
        public int MO_Claims { get; set; }
        public int HH_Claims { get; set; }

        public List<CUESummaryIncident> CUE_Incidents { get; set; }
    }

    public class CUESummaryIncident
    {
        public int IncidentId { get; set; }
        public string IncidentType { get; set; }
        public string Subject { get; set; }
        public string InvolvementType { get; set; }
        public DateTime IncidentDate { get; set; }
        public string ClaimStatus { get; set; }
        public string Insurer { get; set; }
        public string Description { get; set; }
        public string MatchCode { get; set; }
    }

    public class CUEIncidentDetails
    {
        public string InjuryDescription { get; set; }
        public string HospitalAttended { get; set; }
        public string IllnessDiseaseDescription { get; set; }
    }


}