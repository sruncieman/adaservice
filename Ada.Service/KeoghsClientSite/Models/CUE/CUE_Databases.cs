﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.CUE
{
    public class CUE_Databases
    {
        public string Database { get; set; }
        //
        public bool IsChecked { get; set; }

        public bool ReadOnly { get; set; }
    }
}