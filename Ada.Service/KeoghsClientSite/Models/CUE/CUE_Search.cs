﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace KeoghsClientSite.Models.CUE
{
    public class CueSearchEnquiryModel
    {
        public CueSearchEnquiryModel()
        {
            this.CueInvolvements = new List<CueInvolvement>();
        }

        public string ClaimNumber { get; set; }
        public DateTime IncidentDate { get; set; }
       
        public DateTime UploadedDate { get; set; }

        public int RiskClaim_Id { get; set; }
        
        public List<CueInvolvement> CueInvolvements { get; set; }
    }

    public class CueInvolvement
    {
        public int PersonId { get; set; }
        [Display(Name="First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public string Involvement { get; set; }
        public DateTime Dob { get; set; }
        public string NI_Number { get; set; }
        public string DrivingLicenceNumber { get; set; }
        public string HouseName { get; set; }
        public string Number { get; set; }
        public string Street { get; set; }
        public string Locality { get; set; }
        public string Town { get; set; }
        public string PostCode { get; set; }

        public string VehicleReg { get; set; }
        public string VIN { get; set; }

        public IEnumerable<CUE_Databases> CUE_Databases { get; set; }
    }
}