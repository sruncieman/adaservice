﻿using MDA.WCF.WebServices.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models
{
    public class BatchListViewModel
    {
        public BatchListViewModel()
        {
            // 
            PagingInfo=new PagingInfo();
        }
        
        public List<ExWebsiteBatchDetails> BatchList { get; set; }
        //
        public string SortColumn { get; set; }

        // Specifies if the sort order for the results list is in 
        // ascending or descending order
        public bool SortAscending { get; set; }

        public IEnumerable<SelectListItem> Clients { get; set; }
        //
        public int? SelectedClientId { get; set; }
        //
        public int SelectedTeamId { get; set; }
        //
        public int SelectedUserId { get; set; }
        //
        public PagingInfo PagingInfo { get; set; }
        //
        public string ExpectedFileExtension { get; set; }
    }
}