﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MDA.WCF.WebServices.Messages;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Routing;
using System.Web.Mvc;

namespace KeoghsClientSite.Models
{
    public class HomeViewModel
    {
       
        public HomeViewModel()
        {
            claimsListViewModel = new ClaimsListViewModel();
            searchClaimsListViewModel = new SearchClaimsListViewModel();
        }
   
        public ClaimsListViewModel claimsListViewModel { get; set; }

        public SearchClaimsListViewModel searchClaimsListViewModel { get; set; }
    }
}