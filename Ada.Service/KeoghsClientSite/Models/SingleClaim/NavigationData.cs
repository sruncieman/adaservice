﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class NavigationData
    {
        // Holds the data to update the navigation panel with the data just saved in the ajax call
        public string Data { get; set; }
        //
        // Indicates the current form the user is viewing, required for navigation panel
        public string CurrentForm { get; set; }
        //
        // Indicates if the user has returned to the current form by clicking the previous button
        // on another form, required for navigation panel
        public bool Previous { get; set; }

        // The next page in the wizard that the current form goes to
        // this is required when going back through the wizard
        public string NextPage { get; set; }
    }
}