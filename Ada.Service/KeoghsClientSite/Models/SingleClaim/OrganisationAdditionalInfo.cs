﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class OrganisationAdditionalInfo
    {
        [DisplayName("Telephone 1")]
        public string Telephone1 { get; set; }

        [DisplayName("Telephone 2")]
        public string Telephone2 { get; set; }

        [DisplayName("Telephone 3")]
        public string Telephone3 { get; set; }

        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [DisplayName("Website Address")]
        public string WebsiteAddress { get; set; }

        [DisplayName("VAT Number")]
        public string VATNumber { get; set; }

        [DisplayName("MoJ Claims Regulation Number")]
        public string MoJClaimsRegNumber { get; set; }



    }
}