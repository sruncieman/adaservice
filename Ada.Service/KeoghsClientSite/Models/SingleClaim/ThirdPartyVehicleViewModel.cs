﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class ThirdPartyVehicleViewModel : InsuredVehicleViewModel
    {

        public ThirdPartyVehicleViewModel()
        {
            VehicleAdditionalInfo = new VehicleAdditionalInfo();
            Vehicle = new Vehicle();
            this.Vehicle.View = CurrentView.ThirdPartyVehicle;
            NavigationData = new NavigationData();
        }


        public int VehicleID { get; set; }
        public int NumberOfVehicles { get; set; }
        public int CurrentVehicle { get; set; }
        public new CurrentView PreviousPage { get; set; }

        public new NavigationData NavigationData { get; set; }

    }
}