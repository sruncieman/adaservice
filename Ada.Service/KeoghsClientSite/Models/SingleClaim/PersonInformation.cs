﻿using KeoghsClientSite.Models.SingleClaim.Validation;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class PersonInformation
    {
        public PersonInformation()
        {
            this.Salutations = SingleClaimRepository.PopulatePersonSalutations();
            this.Gender = SingleClaimRepository.PopulatePersonGenders();
            this.WitnessInvolvementType = SingleClaimRepository.PopulateWitnessInvolvementType();
        }

        [DisplayName("Salutation")]
        public IEnumerable<SelectListItem> Salutations { get; set; }

        public int? SelectedSalutation { get; set; }

        [DisplayName("First Name")]
        public string Firstname { get; set; }

        [DisplayName("Middle Name")]
        public string Middlename { get; set; }

        [DisplayName("Surname")]
        public string Surname { get; set; }

        [DisplayName("Date of Birth")]
        [PastDate(ErrorMessage = "Date of Birth cannot be in the future.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DateOfBirth { get; set; }

        [DisplayName("Gender")]
        public IEnumerable<SelectListItem> Gender { get; set; }

        public int? SelectedGender { get; set; }

        public CurrentView View { get; set; }

        [DisplayName("Number of suppliers for this person? e.g. Solicitor, Hire Vehicle")]
        [Range(0, 2, ErrorMessage = "Number of suppliers must not exceed 2")]
        public int NumberOfSupportingInvolvements { get; set; }

        [DisplayName("Witness Involvement Type")]
        public IEnumerable<SelectListItem> WitnessInvolvementType { get; set; }

        public int? SelectedWitnessInvolvementType { get; set; }

    }
}