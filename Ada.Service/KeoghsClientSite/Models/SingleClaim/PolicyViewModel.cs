﻿using KeoghsClientSite.Models.SingleClaim.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class PolicyViewModel : ViewModelBase
    {
        [StringLength(50, ErrorMessage = "Policy Number cannot be longer than 50 characters.")]
        [DisplayName("Policy Number")]
        public string PolicyNumber { get; set; }

        [DisplayName("Policy Type")]
        public IEnumerable<SelectListItem> PolicyTypes { get; set; }
        
        [Required(ErrorMessage = "Policy type is required.")]
        public int? SelectedPolicyType { get; set; }

        [DisplayName("Policy Holder details known")]
        [Required(ErrorMessage = "Policy holder details known is required.")]
        public bool? PolicyHolderDetailsKnown { get; set; }

        [DisplayName("Policy Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [PastDate(ErrorMessage = "Policy Start Date cannot be in the future.")]
        public DateTime? PolicyStartDate { get; set; }

        [IsDateAfter("PolicyStartDate", true, ErrorMessage = "Policy end date must be after policy start date.")]
        [DisplayName("Policy End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? PolicyEndDate { get; set; }

        [DisplayName("Cover Type")]
        public IEnumerable<SelectListItem> CoverTypes { get; set; }
        public int? SelectedCoverType { get; set; }

        [RegularExpression(@"^\d+.\d{0,2}$", ErrorMessage = "Policy premium can't have more than 2 decimal places")]
        [Range(0.01, 999999999, ErrorMessage = "Policy premium must be between 0.01 and 999999999.")]
        [DisplayName("Policy Premium (£)")]
        public Decimal? PolicyPremium { get; set; }

        [DisplayName("Broker")]
        public string Broker { get; set; }

        

    }
}