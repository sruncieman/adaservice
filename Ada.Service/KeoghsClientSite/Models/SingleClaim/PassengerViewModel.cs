﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class PassengerViewModel : PersonBase
    {
        public PassengerViewModel()
        {
            this.PersonInformation.View = CurrentView.Passenger;
            this.Address.View = CurrentView.Passenger;
            NavigationData = new NavigationData();
        }

        public int PassengerID { get; set; }
        public int NumberPassengers { get; set; }
        public int CurrentPassenger { get; set; }
        public CurrentView PreviousPage { get; set; }
        public int PreviousPageID { get; set; }
        public int VehicleID { get; set; }
        public PassengerType PassengerType { get; set; }

        public NavigationData NavigationData { get; set; }

    }
}