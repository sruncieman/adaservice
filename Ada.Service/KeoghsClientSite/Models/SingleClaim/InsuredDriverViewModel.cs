﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class InsuredDriverViewModel : PersonBase
    {

        public InsuredDriverViewModel()
        {
            //this.PersonInformation = new PersonInformation();
            //this.Address = new Address();
            //this.BankAccount = new BankAccount();
            //this.PaymentCard = new PaymentCard();
            //this.AdditionalInfo = new AdditionalInfo();
            this.PersonInformation.View = CurrentView.InsuredDriver;
            this.Address.View = CurrentView.InsuredDriver;
            this.NavigationData = new NavigationData();
        }

        public bool? PopulateWithPolicyHolder { get; set; }

        //public PersonInformation PersonInformation { get; set; }

        //public Address Address { get; set; }

        //public BankAccount BankAccount { get; set; }

        //public PaymentCard PaymentCard { get; set; }

        //public AdditionalInfo AdditionalInfo { get; set; }

        //public CurrentView View { get; set; }

        public NavigationData NavigationData { get; set; }

    }
}