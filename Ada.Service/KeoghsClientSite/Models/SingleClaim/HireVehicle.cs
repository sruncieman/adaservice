﻿using KeoghsClientSite.Models.SingleClaim.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class HireVehicle : Vehicle
    {

        [DisplayName("Hire Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? HireStartDate { get; set; }

        [IsDateAfter("HireStartDate", true, ErrorMessage = "Hire end date must be after hire start date.")]
        [DisplayName("Hire End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? HireEndDate { get; set; }


    }
}