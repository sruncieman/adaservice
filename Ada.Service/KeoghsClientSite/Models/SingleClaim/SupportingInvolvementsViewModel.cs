﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class SupportingInvolvementsViewModel : ViewModelBase
    {


        public SupportingInvolvementsViewModel()
        {
            this.OrganisationInvolvement = new OrganisationInvolvement();
            this.Address = new Address();
            this.Vehicle = new HireVehicle();
            this.VehicleAdditionalInfo = new HireVehicleAddInfo();
            this.AdditionalInfo = new OrganisationAdditionalInfo();
        }

        public OrganisationInvolvement OrganisationInvolvement { get; set; }
        
        public Address Address { get; set; }

        public HireVehicle Vehicle { get; set; }

        public HireVehicleAddInfo VehicleAdditionalInfo { get; set; }

        public OrganisationAdditionalInfo AdditionalInfo { get; set; }

        public int NumberSupportingInvolvements { get; set; }
        public int CurrentSupportingInvolvement { get; set; }

        public SupportingInvolvementsType SupportingInvolvementsType { get; set; }

        public int ParentID { get; set; }
        public int VehicleID { get; set; }
    }
}