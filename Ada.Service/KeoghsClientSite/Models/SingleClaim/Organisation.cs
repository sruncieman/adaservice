﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class Organisation
    {
        [DisplayName("Organisation Name")]
        public string Name { get; set; }

        [DisplayName("Registered Number")]
        public string RegisteredNumber { get; set; }

      
    }
}