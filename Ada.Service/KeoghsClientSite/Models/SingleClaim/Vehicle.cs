﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class Vehicle
    {

        [DisplayName("Vehicle Type")]
        public IEnumerable<SelectListItem> VehicleTypes { get; set; }

        public int? SelectedVehicletype { get; set; }

        [DisplayName("Registration Number")]
        public string RegistrationNumber { get; set; }

        [DisplayName("Make")]
        public string Make { get; set; }

        [DisplayName("Model")]
        public string Model { get; set; }

        [DisplayName("Was the Policy holder the driver at the time of the incident?")]
        [Required(ErrorMessage = "Was the Policy holder the driver at the time of the incident?")]
        public bool? WasPolicyHolderTheDriver { get; set; }

        public bool? PolicyCommercial { get; set; }


        public bool? PolicyDetailsKnown { get; set; }

        [DisplayName("How many Passengers were there at the time of the incident? (excludes driver)")]
        [Range(0, 4, ErrorMessage = "Number of passengers must not exceed 4")]
        public int NumberOfPassengers { get; set; }

        public CurrentView View { get; set; }

    }
}