﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class PolicyHolderViewModel
    {

        public PolicyHolderViewModel()
        {

            this.Address = new Address();
            this.BankAccount = new BankAccount();
            this.PaymentCard = new PaymentCard();

        }

        public Address Address { get; set; }

        public BankAccount BankAccount { get; set; }

        public PaymentCard PaymentCard { get; set; }


    }
}