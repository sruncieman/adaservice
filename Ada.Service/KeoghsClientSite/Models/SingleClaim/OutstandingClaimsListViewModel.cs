﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MDA.WCF.WebServices.Messages;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class OutstandingClaimsListViewModel
    {
        public PagingInfo PagingInfo { get; set; }
        public int SelectedTeamId { get; set; }
        public int SelectedUserId { get; set; }
        public List<ExWebsiteSingleClaim> OutstandingClaimsList { get; set; }
        public string SortColumn { get; set; }
        public bool SortAscending { get; set; }
        
        public OutstandingClaimsListViewModel()
        {
            PagingInfo = new PagingInfo();
        }
    }
}