﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{

    public enum CurrentView
    {
        Unknown,
        Claim,
        Incident,
        Policy,
        PolicyHolderPersonal,
        PolicyHolderCommercial,
        InsuredVehicle,
        InsuredDriver,
        SupportingInvolvement,
        SupportingInvolvementInsuredDriver,
        SupportingInvolvementInsuredPassenger,
        SupportingInvolvementThirdPartyDriver,
        SupportingInvolvementThirdPartyPassenger,
        Passenger,
        ThirdPartyVehicle,
        ThirdPartyDriver,
        Witness,
        ReviewAndSubmit,
        UploadProgress
    }

    public enum SupportingInvolvementsType
    {
        InsuredDriver,
        InsuredPassenger,
        ThirdPartyDriver,
        ThirdPartyPassenger
    }

    public enum PassengerType
    {
        InsuredVehiclePassenger,
        ThirdPartyVehiclePassenger
    }

}