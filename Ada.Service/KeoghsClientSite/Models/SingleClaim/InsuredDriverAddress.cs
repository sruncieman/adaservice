﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class InsuredDriverAddress : Address
    {
        [DisplayName("Same address as Policy holder?")]
        [Required(ErrorMessage = "Please indicate if the address is the same as the policy holder")]
        public new bool? SameAddressAsPolicyHolder { get; set; }

    }
}