﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class InsuredDriverPerson : PersonInformation
    {

        //[DisplayName("Number of supporting involvements for this person? e.g. Solicitor, Hire Vehicle")]
        //public IEnumerable<SelectListItem> NumberOfSupportingInvolvements { get; set; }

        [DisplayName("Number of supporting involvements for this person? e.g. Solicitor, Hire Vehicle")]
        [Required(ErrorMessage = "Number of supporting involvements is required.")]
        [Range(0, 99, ErrorMessage = "Number of supporting involvements must be between 0 and 99")]
        public new int? NumberOfSupportingInvolvements { get; set; }

    }
}