﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class WitnessViewModel : ViewModelBase
    {

        public WitnessViewModel()
        {
            this.PersonInformation = new PersonInformation();
            this.PersonInformation.View = CurrentView.Witness;
            this.Address = new Address();
            this.AdditionalInfo = new AdditionalInfo();
        }

        public PersonInformation PersonInformation { get; set; }

        public Address Address { get; set; }

        public AdditionalInfo AdditionalInfo { get; set; }

        public int NumberOfWitnesses { get; set; }
        public int CurrentWitness { get; set; }
        public CurrentView PreviousPage { get; set; }

    }
}