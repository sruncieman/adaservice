﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class VehicleAdditionalInfo
    {

        [DisplayName("VIN")]
        public string VIN { get; set; }

        [DisplayName("Colour")]
        public IEnumerable<SelectListItem> Colours { get; set; }

        public int? SelectedColour { get; set; }

        [DisplayName("Transmission")]
        public IEnumerable<SelectListItem> Transmission { get; set; }

        public int? SelectedTransmission { get; set; }

        [DisplayName("Fuel")]
        public IEnumerable<SelectListItem> Fuel { get; set; }

        public int? SelectedFuel { get; set; }

        [DisplayName("Engine Capacity")]
        public string EngineCapacity { get; set; }

        [DisplayName("Category of Loss")]
        public IEnumerable<SelectListItem> TotalLossCapacity { get; set; }

        public int? SelectedTotalLossCapacity { get; set; }

    }
}