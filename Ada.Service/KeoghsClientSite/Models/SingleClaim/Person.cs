﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class Person
    {

        public Person()
        {
            this.Information = new PersonInformation();
            this.Address = new Address();
            this.BankAccount = new BankAccount();
            this.PaymentCard = new PaymentCard();
            this.AdditionalInformation = new AdditionalInfo();
        }

        public virtual PersonInformation Information { get; set; }

        public Address Address { get; set; }

        public BankAccount BankAccount { get; set; }

        public PaymentCard PaymentCard { get; set; }

        public virtual AdditionalInfo AdditionalInformation { get; set; }

    }
}