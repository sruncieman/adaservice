﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    //public class PolicyHolderPersonalViewModel: IValidatableObject
    public class PolicyHolderPersonalViewModel : ViewModelBase
    {

        public PolicyHolderPersonalViewModel()
        {
            this.PersonInformation = new PersonInformation();
            this.Address = new Address();
            this.BankAccount = new BankAccount();
            this.PaymentCard = new PaymentCard();
            this.AdditionalInfo = new AdditionalInfo();
        }

        public PersonInformation PersonInformation { get; set; }

        public Address Address { get; set; }

        public BankAccount BankAccount { get; set; }

        public PaymentCard PaymentCard { get; set; }

        public AdditionalInfo AdditionalInfo { get; set; }

       
    }
}