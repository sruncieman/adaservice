﻿using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace KeoghsClientSite.Models.SingleClaim
{
    public class AdditionalInfo
    {
        public AdditionalInfo()
        {
            this.Nationality = SingleClaimRepository.PopulateNationalities();  
        }

        [DisplayName("Home Telephone")]
        public string HomeTelephone { get; set; }

        [DisplayName("Work Telephone")]
        public string WorkTelephone { get; set; }

        [DisplayName("Mobile Number")]
        public string MobileNumber { get; set; }

        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [DisplayName("Occupation")]
        public string Occupation { get; set; }

        [DisplayName("Nationality")]
        public IEnumerable<SelectListItem> Nationality { get; set; }

        public string SelectedNationality { get; set; }

        [DisplayName("Passport Number")]
        public string PassportNumber { get; set; }

        [DisplayName("NI Number")]
        public string NINumber { get; set; }

        [DisplayName("Driving License Number")]
        public string DrivingLicenseNumber { get; set; }


    }
}