﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class SingleClaimModel
    {
       
        public ClaimViewModel ClaimViewModel { get; set; }
        public IncidentViewModel IncidentViewModel { get; set; }
        public PolicyViewModel PolicyViewModel { get; set; }
        public PolicyHolderCommercialViewModel PolicyHolderCommercialViewModel { get; set; }
        public PolicyHolderPersonalViewModel PolicyHolderPersonalViewModel { get; set; }
        //public Vehicle Vehicle { get; set; }
        public InsuredVehicleViewModel InsuredVehicleViewModel { get; set; }
        public VehicleAdditionalInfo VehicleAdditionalInfo { get; set; }
        public InsuredDriverPerson InsuredDriverPerson { get; set; }
        public InsuredDriverAddress InsuredDriverAddress { get; set; }
        public InsuredDriverViewModel InsuredDriverViewModel { get; set; }
        public PassengerViewModel PassengerViewModel { get; set; }
        public ThirdPartyVehicleViewModel ThirdPartyVehicleViewModel { get; set; }
        public ThirdPartyDriverViewModel ThirdPartyDriverViewModel { get; set; }
        public WitnessViewModel WitnessViewModel { get; set; }
        public ReviewAndSubmitViewModel ReviewAndSubmitViewModel { get; set; }
    }
}