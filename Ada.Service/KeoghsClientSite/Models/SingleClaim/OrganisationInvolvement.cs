﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class OrganisationInvolvement
    {

        [DisplayName("Organisation Involvement")]
        public IEnumerable<SelectListItem> OrganisationInvolvements { get; set; }

        public int? SelectedOrganisationInvolvement { get; set; }
        
        [DisplayName("Organisation Name")]
        public string OrganisationName { get; set; }

        [DisplayName("Registered Number")]
        public string RegisteredNumber { get; set; }

        

    }
}