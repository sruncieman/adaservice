﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim.Validation
{
    public class CheckDateRangeAttribute : ValidationAttribute
    {
        // Custom Attribute
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime dt = (DateTime) value;
            if(dt <= DateTime.UtcNow)
            {
                return ValidationResult.Success;
            }
            
            return new ValidationResult(string.Format("Make sure the {0} is less or equal to today's date",validationContext.DisplayName));

        }
    }
}