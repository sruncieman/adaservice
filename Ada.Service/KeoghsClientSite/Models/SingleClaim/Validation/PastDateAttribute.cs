﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim.Validation
{
    public class PastDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if(value !=null)
                return value is DateTime && ((DateTime)value) <= DateTime.Now;
            else
                return true;
                    
        }


    }
}