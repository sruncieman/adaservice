﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class ViewModelBase
    {

        public ViewModelBase()
        {
            NavigationData = new NavigationData();
        }

        public NavigationData NavigationData { get; set; }

    }
}