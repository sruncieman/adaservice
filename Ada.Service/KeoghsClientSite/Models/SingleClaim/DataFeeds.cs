﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class DataFeeds
    {
        public string DataFeed { get; set; }
        //
        public bool IsChecked { get; set; }

        public bool ReadOnly { get; set; }

        public string Included { get; set; }
    }
}