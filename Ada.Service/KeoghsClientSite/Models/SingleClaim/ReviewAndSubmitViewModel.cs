﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class ReviewAndSubmitViewModel : PersonBase
    {

        public ReviewAndSubmitViewModel()
        {
            NavigationData = new NavigationData();
        }

        public int VehicleID { get; set; }
        public int NumberOfVehicles { get; set; }
        public int CurrentVehicle { get; set; }
        public CurrentView PreviousPage { get; set; }


        public IEnumerable<DataFeeds> DataFeeds { get; set; }
        public NavigationData NavigationData { get; set; }

    }
}