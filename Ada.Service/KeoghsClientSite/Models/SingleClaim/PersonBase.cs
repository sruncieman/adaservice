﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class PersonBase
    {

        public PersonBase()
        {
            this.PersonInformation = new PersonInformation();
            this.Address = new Address();
            this.BankAccount = new BankAccount();
            this.PaymentCard = new PaymentCard();
            this.AdditionalInfo = new AdditionalInfo();
            //this.View = CurrentView.InsuredDriver;
        }


        public PersonInformation PersonInformation { get; set; }

        public Address Address { get; set; }

        public BankAccount BankAccount { get; set; }

        public PaymentCard PaymentCard { get; set; }

        public AdditionalInfo AdditionalInfo { get; set; }

        //public CurrentView View { get; set; }

    }
}