﻿using KeoghsClientSite.Models.SingleClaim.Validation;
using KeoghsClientSite.Repositories.SingleClaimRepository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class Address
    {
        public Address()
        {
            this.AddressStatus = SingleClaimRepository.PopulateAddressStatuses();
        }
        [DisplayName("Sub Building")]
        public string SubBuilding { get; set; }

        [DisplayName("Building Name")]
        public string BuildingName { get; set; }

        [DisplayName("Building Number")]
        public string BuildingNumber { get; set; }

        [DisplayName("Street")]
        public string Street { get; set; }

        [DisplayName("Locality")]
        public string Locality { get; set; }

        [DisplayName("Town")]
        public string Town { get; set; }

        [DisplayName("County")]
        public string County { get; set; }

        [DisplayName("Postcode")]
        public string Postcode { get; set; }

        [DisplayName("Address Status")]
        public IEnumerable<SelectListItem> AddressStatus { get; set; }

        public int? SelectedAddressStatus { get; set; }

        [PastDate(ErrorMessage = "Start of Residency cannot be in the future.")]
        [DisplayName("Start of Residency")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? StartOfResidency { get; set; }

        [IsDateAfter("StartOfResidency", true, ErrorMessage = "End of residency needs to be after start of residency")]
        [DisplayName("End of Residency")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? EndOfResidency { get; set; }

        [DisplayName("Same address as Policy Holder?")]
        //[Required(ErrorMessage = "Please indicate if the address details are the same as policy holder.")]
        public bool? SameAddressAsPolicyHolder { get; set; }

        public CurrentView View { get; set; }

        public bool PolicyHolderAddressPopulated { get; set; }

        [DisplayName("Policy Holder Sub Building")]
        public string PolicyHolderSubBuilding { get; set; }

        [DisplayName("Policy Holder Building Name")]
        public string PolicyHolderBuildingName { get; set; }

        [DisplayName("Policy Holder Building Number")]
        public string PolicyHolderBuildingNumber { get; set; }

        [DisplayName("Policy Holder Street")]
        public string PolicyHolderStreet { get; set; }

        [DisplayName("Policy Holder Locality")]
        public string PolicyHolderLocality { get; set; }

        [DisplayName("Policy Holder Town")]
        public string PolicyHolderTown { get; set; }

        [DisplayName("Policy Holder County")]
        public string PolicyHolderCounty { get; set; }

        [DisplayName("Policy Holder Postcode")]
        public string PolicyHolderPostcode { get; set; }

    }

}