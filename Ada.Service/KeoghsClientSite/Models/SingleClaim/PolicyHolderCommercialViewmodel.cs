﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class PolicyHolderCommercialViewModel : ViewModelBase
    {

        public PolicyHolderCommercialViewModel()
        {
            this.Organisation = new Organisation();
            this.Address = new Address();
            this.BankAccount = new BankAccount();
            this.PaymentCard = new PaymentCard();
            this.OrganisationAdditionalInfo = new OrganisationAdditionalInfo();
        }

        //public Organisation Organisation { get; set; }

        public Organisation Organisation { get; set; }

        public Address Address { get; set; }

        public BankAccount BankAccount { get; set; }

        public PaymentCard PaymentCard { get; set; }

        public OrganisationAdditionalInfo OrganisationAdditionalInfo { get; set; }

    }
}