﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class ClaimViewModel : ViewModelBase
    {
        [DisplayName("Type of Claim")]
        public IEnumerable<SelectListItem> ClaimTypes { get; set; }

        [Required(ErrorMessage="Please enter Claim Number")]
        [StringLength(50,ErrorMessage="Claim number cannot be longer than 50 characters.")]
        [DisplayName("Claim Number")]
        public string ClaimNumber { get; set; }

        [DisplayName("Claim Status")]
        public IEnumerable<SelectListItem> ClaimStatuses { get; set; }

        [Required(ErrorMessage = "Please select claim type")]
        public int? SelectedClaimTypeID { get; set; }

        //[Required(ErrorMessage = "Please select claim status")]
        public int? SelectedClaimStatusID { get; set; }

        public int? SingleClaimId { get; set; }

       

    }
}