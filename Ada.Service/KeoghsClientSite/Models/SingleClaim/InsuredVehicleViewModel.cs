﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class InsuredVehicleViewModel : ViewModelBase
    {

        public InsuredVehicleViewModel()
        {
            VehicleAdditionalInfo = new VehicleAdditionalInfo();
            Vehicle = new Vehicle();
            this.Vehicle.View = CurrentView.InsuredVehicle;
        }

        //public string PreviousPage { get; set; }


        public CurrentView PreviousPage { get; set; }

        public Vehicle Vehicle { get; set; }

        public VehicleAdditionalInfo VehicleAdditionalInfo { get; set; }

       
    }
}