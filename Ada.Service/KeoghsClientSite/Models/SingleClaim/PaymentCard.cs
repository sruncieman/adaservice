﻿using KeoghsClientSite.Repositories.SingleClaimRepository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class PaymentCard
    {

        public PaymentCard()
        {
            this.CardTypes = SingleClaimRepository.PopulatepaymentCardTypes();  
            this.CardUsedFor = SingleClaimRepository.PopulatePaymentCardUsedFors();
        }

        [DisplayName("Payment Card Number")]
        public string PaymentCardNumber { get; set; }

        [DisplayName("Expiry Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ExpiryDate { get; set; }

        [DisplayName("Card Provider")]
        public string CardProvider { get; set; }

        [DisplayName("Card Type")]
        public IEnumerable<SelectListItem> CardTypes { get; set; }

        public int? SelectedCardType { get; set; }

        [DisplayName("Date details taken")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DateDetailsTaken { get; set; }

        [DisplayName("What was this card used for?")]
        public IEnumerable<SelectListItem> CardUsedFor { get; set; }

        public int? SelectedCardUsedFor { get; set; }
    }
}