﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.Models.SingleClaim.Validation;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class IncidentViewModel : ViewModelBase
    {
        [Required(ErrorMessage = "Date of incident is required.")]
        [PastDate(ErrorMessage = "Date of incident cannot be in the future.")]
        [DisplayName("Date of Incident")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? IncidentDate { get; set; }

        [DisplayName("Time of Incident (HH:MM)")]
        public IEnumerable<SelectListItem> IncidentTimeHours { get; set; }

        public string SelectedIncidentTimeHours { get; set; }

        public IEnumerable<SelectListItem> IncidentTimeMinutes { get; set; }

        public string SelectedIncidentTimeMinutes { get; set; }

        [StringLength(50, ErrorMessage = "Location cannot be longer than 50 characters.")]
        [DisplayName("Location of Incident")]
        public string Location { get; set; }
   
        [DisplayName("Number of Vehicles involved in the Incident")]
        public IEnumerable<SelectListItem> NumberOfVehicles { get; set; }

        [Required(ErrorMessage = "Number of vehicles is required.")]
        [Range(1, 2, ErrorMessage = "Number of vehicles must be greater than 0 but not exceed 2")]
        public int SelectedNumberOfVehicles { get; set; }

        [DisplayName("Total number of Witnesses to the incident")]
        public IEnumerable<SelectListItem> NumberOfWitnesses { get; set; }

        [Required(ErrorMessage = "Number of witnesses is required.")]
        [Range(0, 2, ErrorMessage = "Number of witnesses must not exceed 2")]
        public int SelectedNumberOfWitnesses { get; set; }

        [StringLength(1000, ErrorMessage = "Incident Circumstances cannot be longer than 1000 characters." )]
        [DisplayName("Incident Circumstances")]
        public string Circumstances { get; set; }

        [DisplayName(" Claims notification date")]
        [PastDate(ErrorMessage = "Claim notification date cannot be in the future.")]
        [IsDateAfter("IncidentDate", true, ErrorMessage = "Claim notification date needs to be after incident date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? NotificationDate { get; set; }

        [DisplayName("Claim Code")]
        public IEnumerable<SelectListItem> ClaimCode { get; set; }

        public string SelectedClaimCode { get; set; }

        [RegularExpression(@"^\d+.\d{0,2}$", ErrorMessage = "Total value can't have more than 2 decimal places")]
        [Range(0.01, 999999999, ErrorMessage = "Total value must be between 0.01 and 999999999.")]
        [DisplayName("Total value of the claim (£)")]
        public Decimal? TotalValue { get; set; }

        [RegularExpression(@"^\d+.\d{0,2}$", ErrorMessage = "Total payments made to date can't have more than 2 decimal places")]
        [Range(0.01, 999999999, ErrorMessage = "Total payment made to date must be between 0.01 and 999999999.")]
        [DisplayName("Total of payments made to date (£)")]
        public Decimal? TotalPayments { get; set; }

        [DisplayName("Has a decision been made regarding liability?")]
        public IEnumerable<SelectListItem> Liability { get; set; }
        public string SelectedLiability { get; set; }
        //public bool? SelectedLiability { get; set; }


        [DisplayName("Liability Decision")]
        public IEnumerable<SelectListItem> LiabilityDecision { get; set; }
        public string SelectedLiabilityDecision { get; set; }

        [DisplayName("MoJ Process Stage")]
        public IEnumerable<SelectListItem> MojProcessStage { get; set; }
        public string SelectedMojProcessStage { get; set; }

        [DisplayName("Did the Ambulance Service attend?")]
        public IEnumerable<SelectListItem> AmbulanceAttend { get; set; }
        public int? SelectedAmbulanceAttend { get; set; }

        [DisplayName("Did the Police attend?")]
        public IEnumerable<SelectListItem> PoliceAttend { get; set; }
        public int? SelectedPoliceAttend { get; set; }

        [DisplayName("Name of the Police Force")]
        public string PoliceForce { get; set; }

        [DisplayName("Police Reference")]
        public string PoliceReference { get; set; }

        public bool IncidentFormPosted { get; set; }

       
    }
}