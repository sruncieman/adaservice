﻿using KeoghsClientSite.Repositories.SingleClaimRepository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Models.SingleClaim
{
    public class BankAccount
    {
        public BankAccount()
        {
            this.AccountUsedFor = SingleClaimRepository.PopulateBankAccountUsedFors();  
        }

        [DisplayName("Account Number")]
        public string AccountNumber { get; set; }

        [DisplayName("Sort Code")]
        public string SortCode { get; set; }

        [DisplayName("Bank Name")]
        public string BankName { get; set; }

        [DisplayName("Date details taken")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DateDetailsTaken { get; set; }

        [DisplayName("What was the account used for?")]
        public IEnumerable<SelectListItem> AccountUsedFor { get; set; }

        public int? SelectedAccountUsedFor { get; set; }
    }
}