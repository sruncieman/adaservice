﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Models
{
    public class ReportStatusViewModel
    {
        public string ReportStatus { get; set; }

        public bool IsChecked { get; set; }
    }
}