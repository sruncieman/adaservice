﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KeoghsClientSite.ADATransferServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://www.keoghs.co.uk/ADA", ConfigurationName="ADATransferServiceReference.IADATransferServices")]
    public interface IADATransferServices {
        
        // CODEGEN: Generating message contract since the operation UploadBatchOfClaimsFile is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="http://www.keoghs.co.uk/ADA/IADATransferServices/UploadBatchOfClaimsFile", ReplyAction="http://www.keoghs.co.uk/ADA/IADATransferServices/UploadBatchOfClaimsFileResponse")]
        KeoghsClientSite.ADATransferServiceReference.UploadBatchOfClaimsFileResponse UploadBatchOfClaimsFile(KeoghsClientSite.ADATransferServiceReference.RemoteFileInfo request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.keoghs.co.uk/ADA/IADATransferServices/UploadBatchOfClaimsFile", ReplyAction="http://www.keoghs.co.uk/ADA/IADATransferServices/UploadBatchOfClaimsFileResponse")]
        System.Threading.Tasks.Task<KeoghsClientSite.ADATransferServiceReference.UploadBatchOfClaimsFileResponse> UploadBatchOfClaimsFileAsync(KeoghsClientSite.ADATransferServiceReference.RemoteFileInfo request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.keoghs.co.uk/ADA/IADATransferServices/GetLevel1ReportStream", ReplyAction="http://www.keoghs.co.uk/ADA/IADATransferServices/GetLevel1ReportStreamResponse")]
        System.IO.Stream GetLevel1ReportStream(MDA.WCF.TransferServices.GetLevel1ReportStreamRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.keoghs.co.uk/ADA/IADATransferServices/GetLevel1ReportStream", ReplyAction="http://www.keoghs.co.uk/ADA/IADATransferServices/GetLevel1ReportStreamResponse")]
        System.Threading.Tasks.Task<System.IO.Stream> GetLevel1ReportStreamAsync(MDA.WCF.TransferServices.GetLevel1ReportStreamRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.keoghs.co.uk/ADA/IADATransferServices/GetLevel2ReportStream", ReplyAction="http://www.keoghs.co.uk/ADA/IADATransferServices/GetLevel2ReportStreamResponse")]
        System.IO.Stream GetLevel2ReportStream(MDA.WCF.TransferServices.GetLevel2ReportStreamRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.keoghs.co.uk/ADA/IADATransferServices/GetLevel2ReportStream", ReplyAction="http://www.keoghs.co.uk/ADA/IADATransferServices/GetLevel2ReportStreamResponse")]
        System.Threading.Tasks.Task<System.IO.Stream> GetLevel2ReportStreamAsync(MDA.WCF.TransferServices.GetLevel2ReportStreamRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.keoghs.co.uk/ADA/IADATransferServices/GetBatchReportStream", ReplyAction="http://www.keoghs.co.uk/ADA/IADATransferServices/GetBatchReportStreamResponse")]
        System.IO.Stream GetBatchReportStream(MDA.WCF.TransferServices.GetBatchReportStreamRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.keoghs.co.uk/ADA/IADATransferServices/GetBatchReportStream", ReplyAction="http://www.keoghs.co.uk/ADA/IADATransferServices/GetBatchReportStreamResponse")]
        System.Threading.Tasks.Task<System.IO.Stream> GetBatchReportStreamAsync(MDA.WCF.TransferServices.GetBatchReportStreamRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="RemoteFileInfo", WrapperNamespace="http://www.keoghs.co.uk/ADA", IsWrapped=true)]
    public partial class RemoteFileInfo {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://www.keoghs.co.uk/ADA")]
        public string BatchEntityType;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://www.keoghs.co.uk/ADA")]
        public string ClientBatchReference;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://www.keoghs.co.uk/ADA")]
        public int ClientId;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://www.keoghs.co.uk/ADA")]
        public long FileLength;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://www.keoghs.co.uk/ADA")]
        public string FileName;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://www.keoghs.co.uk/ADA")]
        public string MimeType;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://www.keoghs.co.uk/ADA")]
        public int UserId;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://www.keoghs.co.uk/ADA")]
        public string Who;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.keoghs.co.uk/ADA", Order=0)]
        public System.IO.Stream FileByteStream;
        
        public RemoteFileInfo() {
        }
        
        public RemoteFileInfo(string BatchEntityType, string ClientBatchReference, int ClientId, long FileLength, string FileName, string MimeType, int UserId, string Who, System.IO.Stream FileByteStream) {
            this.BatchEntityType = BatchEntityType;
            this.ClientBatchReference = ClientBatchReference;
            this.ClientId = ClientId;
            this.FileLength = FileLength;
            this.FileName = FileName;
            this.MimeType = MimeType;
            this.UserId = UserId;
            this.Who = Who;
            this.FileByteStream = FileByteStream;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class UploadBatchOfClaimsFileResponse {
        
        public UploadBatchOfClaimsFileResponse() {
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IADATransferServicesChannel : KeoghsClientSite.ADATransferServiceReference.IADATransferServices, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ADATransferServicesClient : System.ServiceModel.ClientBase<KeoghsClientSite.ADATransferServiceReference.IADATransferServices>, KeoghsClientSite.ADATransferServiceReference.IADATransferServices {
        
        public ADATransferServicesClient() {
        }
        
        public ADATransferServicesClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ADATransferServicesClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ADATransferServicesClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ADATransferServicesClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        KeoghsClientSite.ADATransferServiceReference.UploadBatchOfClaimsFileResponse KeoghsClientSite.ADATransferServiceReference.IADATransferServices.UploadBatchOfClaimsFile(KeoghsClientSite.ADATransferServiceReference.RemoteFileInfo request) {
            return base.Channel.UploadBatchOfClaimsFile(request);
        }

        public void UploadBatchOfClaimsFile(string ClientBatchReference, string BatchEntityType, int ClientId, long FileLength, string FileName, string MimeType, int UserId, string Who, System.IO.Stream FileByteStream)
        {
            KeoghsClientSite.ADATransferServiceReference.RemoteFileInfo inValue = new KeoghsClientSite.ADATransferServiceReference.RemoteFileInfo();
            inValue.BatchEntityType = BatchEntityType;
            inValue.ClientBatchReference = ClientBatchReference;
            inValue.ClientId = ClientId;
            inValue.FileLength = FileLength;
            inValue.FileName = FileName;
            inValue.MimeType = MimeType;
            inValue.UserId = UserId;
            inValue.Who = Who;
            inValue.FileByteStream = FileByteStream;
            KeoghsClientSite.ADATransferServiceReference.UploadBatchOfClaimsFileResponse retVal = ((KeoghsClientSite.ADATransferServiceReference.IADATransferServices)(this)).UploadBatchOfClaimsFile(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<KeoghsClientSite.ADATransferServiceReference.UploadBatchOfClaimsFileResponse> KeoghsClientSite.ADATransferServiceReference.IADATransferServices.UploadBatchOfClaimsFileAsync(KeoghsClientSite.ADATransferServiceReference.RemoteFileInfo request) {
            return base.Channel.UploadBatchOfClaimsFileAsync(request);
        }
        
        public System.Threading.Tasks.Task<KeoghsClientSite.ADATransferServiceReference.UploadBatchOfClaimsFileResponse> UploadBatchOfClaimsFileAsync(string BatchEntityType, string ClientBatchReference, int ClientId, long FileLength, string FileName, string MimeType, int UserId, string Who, System.IO.Stream FileByteStream) {
            KeoghsClientSite.ADATransferServiceReference.RemoteFileInfo inValue = new KeoghsClientSite.ADATransferServiceReference.RemoteFileInfo();
            inValue.BatchEntityType = BatchEntityType;
            inValue.ClientBatchReference = ClientBatchReference;
            inValue.ClientId = ClientId;
            inValue.FileLength = FileLength;
            inValue.FileName = FileName;
            inValue.MimeType = MimeType;
            inValue.UserId = UserId;
            inValue.Who = Who;
            inValue.FileByteStream = FileByteStream;
            return ((KeoghsClientSite.ADATransferServiceReference.IADATransferServices)(this)).UploadBatchOfClaimsFileAsync(inValue);
        }
        
        public System.IO.Stream GetLevel1ReportStream(MDA.WCF.TransferServices.GetLevel1ReportStreamRequest request) {
            return base.Channel.GetLevel1ReportStream(request);
        }
        
        public System.Threading.Tasks.Task<System.IO.Stream> GetLevel1ReportStreamAsync(MDA.WCF.TransferServices.GetLevel1ReportStreamRequest request) {
            return base.Channel.GetLevel1ReportStreamAsync(request);
        }
        
        public System.IO.Stream GetLevel2ReportStream(MDA.WCF.TransferServices.GetLevel2ReportStreamRequest request) {
            return base.Channel.GetLevel2ReportStream(request);
        }
        
        public System.Threading.Tasks.Task<System.IO.Stream> GetLevel2ReportStreamAsync(MDA.WCF.TransferServices.GetLevel2ReportStreamRequest request) {
            return base.Channel.GetLevel2ReportStreamAsync(request);
        }
        
        public System.IO.Stream GetBatchReportStream(MDA.WCF.TransferServices.GetBatchReportStreamRequest request) {
            return base.Channel.GetBatchReportStream(request);
        }
        
        public System.Threading.Tasks.Task<System.IO.Stream> GetBatchReportStreamAsync(MDA.WCF.TransferServices.GetBatchReportStreamRequest request) {
            return base.Channel.GetBatchReportStreamAsync(request);
        }
    }
}
