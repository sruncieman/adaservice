﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
//using DotNetOpenAuth.AspNet;
//using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using KeoghsClientSite.Filters;
using KeoghsClientSite.Models;
using KeoghsClientSite.StaticClasses;
using System.Web.Script.Serialization;

#if WEBMEMBERSHIP
using System.Web.Routing;
using KeoghsClientSite.Repositories;
using System.Collections;
using MDA.WCF.WebServices.Messages;
using System.Web.Configuration;
using KeoghsClientSite.ActionFilters;
#endif

namespace KeoghsClientSite.Controllers
{
    [NoCache]
    [RedirectingAction]
    public class AdminController : Controller
    {
#if WEBMEMBERSHIP

        public MemProviderWeb MembershipService { get; set; }
        public MemRoleProviderWeb AuthorizationService { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            // This calls the already instantiated provider
            MembershipService = (MemProviderWeb)Membership.Provider;

            //if (MembershipService == null)
            //    MembershipService = new MemProviderWeb();

            if (AuthorizationService == null)
                AuthorizationService = new MemRoleProviderWeb();

            base.Initialize(requestContext);
        }
#endif
        //
        // GET: /Admin/
         [Authorize]
        public ActionResult Index()
        {
            return View();
        }

         public ActionResult AssignUserToClient()
         {
             AdminModel model = new AdminModel();
             var serializer = new JavaScriptSerializer();

             model.Users = Repository.GetRiskUsers();

             model.AvailableList = new List<SelectListItem>(); //initializing the available list to null
             model.AssignedList = new List<SelectListItem>(); //initializing the assigned list to null

             model.currentAvailableList = serializer.Serialize(model.AvailableList); //Save ViewState
             model.currentAssignedList = serializer.Serialize(model.AssignedList); //Save ViewState


             return View(model);
         }

         [HttpGet]
         public ActionResult CreateUserTemplate(int? Id, string TemplateName)
         {
            TemplateFunctionsModel model = new TemplateFunctionsModel();

            model.Roles = Repository.GetRiskRoles();

             if (TemplateName != null)
             {
                 model.SelectedProfile = TemplateName;
                 model.Profile_Id = Id;

                 model = Repository.LoadTemplateFunctionValues(model);
             }
             
             return View(model);
         }

      



         public enum ManageMessageId
         {
             ChangePasswordSuccess,
             SetPasswordSuccess,
             RemoveLoginSuccess,
         }

         [Authorize]
         public ActionResult ChangePassword(ManageMessageId? message)
         {
             LocalPasswordModel model = new LocalPasswordModel();
             model.Users = Repository.GetRiskUsers();
             ViewBag.StatusMessage =
                 message == ManageMessageId.ChangePasswordSuccess ? "Password has been changed."
                 : message == ManageMessageId.SetPasswordSuccess ? "Password has been set."
                 : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                 : "";
             //ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
             ViewBag.HasLocalPassword = true;
             ViewBag.ReturnUrl = Url.Action("ChangePassword");
             return View(model);
         }

         //
         // POST: /Account/Manage
         [Authorize]
         [HttpPost]
         [ValidateAntiForgeryToken]
         public ActionResult ChangePassword(LocalPasswordModel model)
         {
             //bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
             bool hasLocalAccount = true;
             ViewBag.HasLocalPassword = hasLocalAccount;
             ViewBag.ReturnUrl = Url.Action("ChangePassword");

             if (hasLocalAccount)
             {
                 if (ModelState.IsValid)
                 {
                     // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                     bool changePasswordSucceeded;
                     try
                     {
                         changePasswordSucceeded = MembershipService.ChangePassword(model.UserName, model.OldPassword, model.NewPassword);
                     }
                     catch (Exception)
                     {
                         changePasswordSucceeded = false;
                     }

                     if (changePasswordSucceeded)
                     {
                         return RedirectToAction("ChangePassword", new { Message = ManageMessageId.ChangePasswordSuccess });
                     }
                     else
                     {
                         ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                     }
                 }
             }
             else
             {
                 // User does not have a local password so remove any validation errors caused by a missing
                 // OldPassword field
                 ModelState state = ModelState["OldPassword"];
                 if (state != null)
                 {
                     state.Errors.Clear();
                 }

                 if (ModelState.IsValid)
                 {
                     try
                     {
                         WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                         return RedirectToAction("ChangePassword", new { Message = ManageMessageId.SetPasswordSuccess });
                     }
                     catch (Exception e)
                     {
                         ModelState.AddModelError("", e);
                     }
                 }
             }

             // If we got this far, something failed, redisplay form
             return View(model);
         }

        public JsonResult CreateTemplate(string templateName)
        {
            Repository.CreateRiskRole(templateName);
            return null;
        }

        public JsonResult EditTemplateName(string templateName, string profileId)
        {
            Repository.EditRiskRole(templateName, Convert.ToInt32(profileId));
            return null;
        }


        public JsonResult SaveTemplateData(string strTemplateFunctions, string template_id)
        {
            var json_serializer = new JavaScriptSerializer();
            TemplateFunction functions = new TemplateFunction();

            functions.lstTemplateFunction = json_serializer.Deserialize<List<Dictionary<string, string>>>(strTemplateFunctions);

            var result = Repository.SaveTemplateFunctions(functions.lstTemplateFunction, strTemplateFunctions, template_id);

            return null;
        }

        public JsonResult SaveClientBatchPriority(string batchOrder)
        {
            int[] intArray = batchOrder.Split(',').Select(n => Convert.ToInt32(n)).ToArray();

            var result = Repository.SaveClientBatchPriority(intArray);

            string SubmitResult = null;

            SubmitResult = result;

            ModelState.Clear();

            return Json(new
            {
                nameret = SubmitResult
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveOverriddenBatchPriority(string overrideBatchOrder)
        {
            int[] intArray = overrideBatchOrder.Split(',').Select(n => Convert.ToInt32(n)).ToArray();

            var result = Repository.SaveOverridenBatchPriority(intArray);

            string SubmitResult = null;

            SubmitResult = result;

            ModelState.Clear();

            return Json(new
            {
                nameret = SubmitResult
            }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetBatchQueue()
        {
            var batchQueue = Repository.GetBatchQueue();


            return Json(batchQueue, JsonRequestBehavior.AllowGet);
        }


         public JsonResult DeleteTemplate(string template_id)
         {
             var result = Repository.DeleteTemplate(template_id);

             return Json(result);
         }

         [AjaxAuthorize]
         public JsonResult LoadTemplateData(int? profileId)
         {
             return Json(Repository.LoadUserTemplateData());
             
         }

         [HttpPost]
         public ActionResult AssignUserToClient(AdminModel model)
         {
             //GetAssignedRiskClientsForUser

             var serializer = new JavaScriptSerializer();

             model.Users = Repository.GetRiskUsers();

             //model.AvailableList = new List<SelectListItem>(); //initializing the available list to null
             model.AvailableList = Repository.GetRiskClients().ToList(); // Get all clients from DB
             
             //model.AssignedList = new List<SelectListItem>(); //initializing the assigned list to null
             model.AssignedList = Repository.GetAssignedRiskClientsForUser(model.UserId ?? default(int)).ToList(); // Get all assigned clients for selected user
             
             //model.AvailableList = model.AvailableList.Except(model.AssignedList).ToList(); // remove assigned values from available list

             //var list1 = model.AssignedList;
             //var list2 = model.AvailableList;

             //var newList = list2.Except(list1).ToList();

             foreach (var item in model.AssignedList)
             {

                 model.AvailableList = model.AvailableList.Where(c => c.Value != item.Value).ToList();

             }



             model.currentAvailableList = serializer.Serialize(model.AvailableList); //Save ViewState
             model.currentAssignedList = serializer.Serialize(model.AssignedList); //Save ViewState


                 //bind the list from ViewState
                 if (model.currentAvailableList != null)
                 {
                     model.currentAvailableList = model.currentAvailableList.Replace("<", "");
                     model.currentAvailableList = model.currentAvailableList.Replace(">", "");
                     model.AvailableList = serializer.Deserialize<List<SelectListItem>>(model.currentAvailableList); //Load the ViewState here 
                 }
                 //bind the list from ViewState            
                 if (model.currentAssignedList != null)
                 {
                     model.currentAssignedList = model.currentAssignedList.Replace("<", "");
                     model.currentAssignedList = model.currentAssignedList.Replace(">", "");
                     model.AssignedList = serializer.Deserialize<List<SelectListItem>>(model.currentAssignedList);  //Load the ViewState here
                 }
                 try
                 {
                     switch (model.btnSubmit.ToUpper())
                     {
                         case ("ADD >>"):
                             {
                                 //Update the AvailableList & Assigned list on the basis of our selection
                                 if ((model.SelectedAvailableItems == null) || (model.SelectedAvailableItems.Count <= 0))
                                 {
                                     throw new Exception("No Items Selected in the Available List.");
                                 }
                                 else
                                 {
                                     foreach (string itm in model.SelectedAvailableItems)
                                     {
                                         var slctItm = model.AvailableList.FirstOrDefault(i => i.Value.Equals(itm));
                                         model.AvailableList.Remove(slctItm);
                                         model.AssignedList.Add(slctItm);

                                         int clientId = 0;
                                         
                                         if (Int32.TryParse(itm, out clientId))
                                         {
                                             Repository.CreateRiskUser2Client(model.UserId ?? default(int), clientId);
                                         }

                                     }
                                 }
                             }
                             break;
                         case ("<< REMOVE"):
                             {
                                 //Update the AvailableList & Assigned list on the basis of our selection
                                 if ((model.SelectedAssignedItems == null) || (model.SelectedAssignedItems.Count <= 0))
                                 {
                                     throw new Exception("No Items Selected in the Assigned List.");
                                 }
                                 else
                                 {

                                     foreach (string str in model.SelectedAssignedItems)
                                     {


                                        var slctItm = model.AssignedList.FirstOrDefault(i => i.Value.Equals(str));

                                        if (slctItm.Text.Contains("(Default)"))
                                        {
                                            model.AssignWarningDivIsVisible = true;
                                        }
                                        else
                                        {
                                            model.AssignedList.Remove(slctItm);
                                            model.AvailableList.Add(slctItm);

                                            int clientId = 0;

                                            if (Int32.TryParse(str, out clientId))
                                            {
                                                Repository.DeleteRiskUser2Client(model.UserId ?? default(int), clientId);
                                            }
                                        }

                                     }
                                 }
                             }
                             break;
                         case ("SAVE"):
                             {
                                 var items = model.SelectedAssignedItems;
                                 model.Message = "Save Item here!";
                                 model.IsSuccess = true;
                             }
                             break;
                         default:
                             model.Message = "Un-Identified action!";
                             break;
                     }
                 }
                 catch (Exception exc1)
                 {
                     model.Message = "Sorry an error has occured! =>" + exc1.Message;
                     model.IsSuccess = false;
                     //Log & handle exception
                 }

                 //Updating ViewState
                 if (model.AssignedList != null)
                     model.currentAssignedList = serializer.Serialize(model.AssignedList);
                 else
                     model.currentAssignedList = null;

                 if (model.AvailableList != null)
                     model.currentAvailableList = serializer.Serialize(model.AvailableList);
                 else
                     model.currentAvailableList = null;
                 //Updating ViewState
             //}

             return View(model);
         }
    
         [Authorize]
         [HttpPost]
         [ValidateAntiForgeryToken]
         [ActionName("EditUserDetails")]
         public ActionResult EditUserDetails_Post(EditUserDetails model)
         {
             
             if (ModelState.IsValid)
             {
                 // Attempt to update the user
                 try
                 {
                     IDictionary<string, object> dictionary = new Dictionary<string, object>();
                     //
                     dictionary.Add("firstName", model.FirstName);
                     dictionary.Add("lastName", model.LastName);
                     dictionary.Add("telephoneNumber", model.Telephone);
                     dictionary.Add("username", model.UserName);
                     dictionary.Add("clientId", model.ClientId);
                     //dictionary.Add("teamId", model.TeamId);
                     dictionary.Add("roleId", model.RoleId);
                     dictionary.Add("statusId", model.StatusId);
                     dictionary.Add("autoLoader", model.Autoloader);
                     dictionary.Add("userType", model.UserTypeId);


                     MembershipService.UpdateUser(model.UserName, model.Password, false, dictionary);


                     string SubmitResult = "Success";

                     ModelState.Clear();

                     return Json(new
                     {
                         nameret = SubmitResult
                     }, JsonRequestBehavior.AllowGet);

                 }
                 catch (MembershipCreateUserException e)
                 {
                     ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                 }
             }
             else
             {

                 var errors = ModelState.Values.SelectMany(v => v.Errors);

                 model.Clients = Repository.GetRiskClients();
                 model.Roles = Repository.GetRiskRoles();
                 model.Status = GetStatusSelectList();
                 model.Groups = GetGroupSelectList();
                 model.UserTypes = GetUserTypeSelectList();

             }
             // If we got this far, something failed, redisplay form
             return View(model);
         }

         public ActionResult CreateClient()
         {
             CreateClient model = new CreateClient();

             model.Status = GetClientStatusSelectList();
             model.InsurersClients = Repository.GetInsurersClients();

             return View(model);
         }

         [HttpPost]
         public ActionResult CreateClient(CreateClient model)
         {
             var user = Repository.IsClientNameUnique(model.ClientName);
             
             if (ModelState.IsValid && user.IsUnique == true)
             {

                 // Attempt to create client
                 try
                 {

                     Repository.CreateClient(model.ClientName, model.AmberThreshold, model.RedThreshold, model.FileExtensions, model.StatusId, model.InsurersClientsId);

                     string SubmitResult = "Success";

                     ModelState.Clear();

                     return Json(new
                     {
                         nameret = SubmitResult
                     }, JsonRequestBehavior.AllowGet);

                 }
                 catch (MembershipCreateUserException e)
                 {
                     ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                 }
             }
             else
             {
                 var errors = ModelState.Values.SelectMany(v => v.Errors);

                 model.Status = GetClientStatusSelectList();

             }
             // If we got this far, something failed, redisplay form
             return View(model);
         }

         [HttpPost]
         public JsonResult doesClientNameExist(string ClientName)
         {

             var user = Repository.IsClientNameUnique(ClientName);

             if (user.IsUnique == true)
             {
                 return Json(true, JsonRequestBehavior.AllowGet);
             }

             return Json(ClientName + " is already in use", JsonRequestBehavior.AllowGet);
         }

         [HttpPost]
         public JsonResult doesNewClientNameExist(string ClientName, int Id)
         {

             var existingUser = Repository.GetRiskClientInfo(Id);
             var user = Repository.IsClientNameUnique(ClientName);

             if (existingUser.ClientName != ClientName)
             {
                 if (user.IsUnique == true)
                 {
                     return Json(true, JsonRequestBehavior.AllowGet);
                 }
                 else
                 {
                     return Json(ClientName + " is already in use", JsonRequestBehavior.AllowGet);
                 }
             }

             return Json(true, JsonRequestBehavior.AllowGet);
         }

         private static IEnumerable<SelectListItem> GetStatusSelectList()
         {
             IEnumerable<SelectListItem> ListItems = new List<SelectListItem>{
               new SelectListItem(){Text="Active", Value="1"},
               new SelectListItem(){Text="Inactive",Value="0"},
               new SelectListItem(){Text="Locked", Value="2" },
               };

             return ListItems;
         }

         private static IEnumerable<SelectListItem> GetClientStatusSelectList()
         {
             IEnumerable<SelectListItem> ListItems = new List<SelectListItem>{
               new SelectListItem(){Text="Active", Value="1"},
               new SelectListItem(){Text="Inactive",Value="0"},
               };

             return ListItems;
         }

         private static IEnumerable<SelectListItem> GetGroupSelectList()
         {
             IEnumerable<SelectListItem> ListItems = new List<SelectListItem>{
               new SelectListItem(){Text="Admin",Value="0"},
               new SelectListItem(){Text="Keoghs Intel", Value="1"},
               new SelectListItem(){Text="Liberty", Value="2" },
               };

             return ListItems;
         }

         private static IEnumerable<SelectListItem> GetUserTypeSelectList()
         {
             IEnumerable<SelectListItem> ListItems = new List<SelectListItem>{
               new SelectListItem(){Text="Keoghs", Value="0"},
               new SelectListItem(){Text="Client",Value="1"},
               };

             return ListItems;
         }

         private static string ErrorCodeToString(MembershipCreateStatus createStatus)
         {
             // See http://go.microsoft.com/fwlink/?LinkID=177550 for
             // a full list of status codes.
             switch (createStatus)
             {
                 case MembershipCreateStatus.DuplicateUserName:
                     return "User name already exists. Please enter a different user name.";

                 case MembershipCreateStatus.DuplicateEmail:
                     return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                 case MembershipCreateStatus.InvalidPassword:
                     return "The password provided is invalid. Please enter a valid password value.";

                 case MembershipCreateStatus.InvalidEmail:
                     return "The e-mail address provided is invalid. Please check the value and try again.";

                 case MembershipCreateStatus.InvalidAnswer:
                     return "The password retrieval answer provided is invalid. Please check the value and try again.";

                 case MembershipCreateStatus.InvalidQuestion:
                     return "The password retrieval question provided is invalid. Please check the value and try again.";

                 case MembershipCreateStatus.InvalidUserName:
                     return "The user name provided is invalid. Please check the value and try again.";

                 case MembershipCreateStatus.ProviderError:
                     return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                 case MembershipCreateStatus.UserRejected:
                     return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                 default:
                     return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
             }
         }

         public ActionResult EditUser()
         {
             AdminModel model = new AdminModel();

             model.UserInfo = Repository.GetRiskUsersSummaryInfo();

             return View(model);
         }

         [HttpPost]
         public ActionResult EditUser(AdminModel model, string rowId)
         {

             model.UserInfo = Repository.GetRiskUsersSummaryInfo();

             var user = Repository.GetRiskUserInfo(rowId);

             ModelState.Clear();

             EditUserDetails userDetails = new EditUserDetails();

             model.AvailableList = Repository.GetRiskClients().ToList(); // Get all clients from DB
             model.AssignedList = Repository.GetAssignedRiskClientsForUser(user.Id).ToList(); // Get all assigned clients for selected user

             foreach (var item in model.AssignedList)
             {

                 model.AvailableList = model.AvailableList.Where(c => c.Value != item.Value || item.Text.Contains("(Default)")).ToList();

             }

             userDetails.Clients = model.AvailableList;
             

             userDetails.Roles = Repository.GetRiskRoles();
             userDetails.Status = GetStatusSelectList();
             userDetails.Groups = GetGroupSelectList();
             userDetails.UserTypes = GetUserTypeSelectList();

             userDetails.UserName = user.UserName;
             userDetails.FirstName = user.FirstName;
             userDetails.LastName = user.LastName;
             userDetails.Telephone = user.TelephoneNumber;
             userDetails.Password = user.Password;
             if (user.Status != null)
             {
                 userDetails.StatusId = user.Status;
             }
             else
             {
                 userDetails.StatusId = 0;
             }

             if (user.AutoLoader==true)
             {
                 userDetails.Autoloader = true;
             }

             if (user.UserTypeId != null)
             {
                 userDetails.UserTypeId = user.UserTypeId;
             }
             else
             {
                 userDetails.UserTypeId = 1;
             }

             userDetails.RoleId = user.RiskRoleId;
             userDetails.ClientId = user.RiskClientId;
             //userDetails.Autoloader = user.AutoLoader;


             if (userDetails.UserName != null)
             {
                 return View("EditUserDetails", userDetails);
             }
             else
             {
                 return View(model);
             }

         }

         [HttpGet]
         public ActionResult EditUserDetails(EditUserDetails userDetails)
         {
             return View(userDetails);
         }
        
         public ActionResult EditClient()
         {
             EditClient model = new EditClient();

             model.ClientInfo = Repository.GetAllRiskClientInfo();

             return View(model);
         }

         [HttpPost]
         public ActionResult EditClient(EditClient model, string rowId)
         {

             model.ClientInfo = Repository.GetAllRiskClientInfo();

             int clientId = Int32.Parse(rowId);

             var client = Repository.GetRiskClientInfo(clientId);

             ModelState.Clear();

             EditClientDetails clientDetails = new EditClientDetails();

             clientDetails.ClientName = client.ClientName;
             clientDetails.AmberThreshold = client.AmberThreshold;
             clientDetails.FileExtensions = client.ExpectedFileExtension;
             clientDetails.RedThreshold = client.RedThreshold;
             if (client.Status != null)
             {
                 clientDetails.StatusId = client.Status;
             }
             else
             {
                 clientDetails.StatusId = 0;
             }
             clientDetails.InsurersClientsId = client.InsurersClientsId;
             clientDetails.Id = client.Id;

             if (clientDetails.FileExtensions.Contains("xml"))
             {
                 clientDetails.Xml = true;
             }

             if (clientDetails.FileExtensions.Contains("xls"))
             {
                 clientDetails.Xls = true;
             }

             if (clientDetails.FileExtensions.Contains("csv"))
             {
                 clientDetails.Csv = true;
             }

             if (clientDetails.FileExtensions.Contains("txt"))
             {
                 clientDetails.Txt = true;
             }

             if (clientDetails.FileExtensions.Contains("xlsx"))
             {
                 clientDetails.Xlsx = true;
             }

             if (clientDetails.FileExtensions.Contains("zip"))
             {
                 clientDetails.Zip = true;
             }

             if (client.RecordReserveChanges == true)
             {
                 clientDetails.RecordReserveChanges = true;
             }
             else
             {
                 clientDetails.RecordReserveChanges = false;
             }

             clientDetails.BatchPriority = client.BatchPriority;
             clientDetails.Status = GetClientStatusSelectList();
             clientDetails.InsurersClients = Repository.GetInsurersClients();

             if (clientDetails.ClientName != null)
             {
                 return View("EditClientDetails", clientDetails);
             }
             else
             {
                 return View(model);
             }

         }

         [HttpGet]
         public ActionResult EditClientDetails(EditClientDetails clientDetails)
         {
             return View(clientDetails);
         }

         // POST: /Account/Register
         [Authorize]
         [HttpPost]
         [ValidateAntiForgeryToken]
         [ActionName("EditClientDetails")]
         public ActionResult EditClientDetails_Post(EditClientDetails model)
         {

             #region File Extensions
             if (model.Xls == true)
             {
                 model.FileExtensions = model.FileExtensions + "xls";
             }

             if (model.Xml == true)
             {
                 if (model.FileExtensions != null)
                 {
                     model.FileExtensions = model.FileExtensions + ",xml";
                 }
                 else
                 {
                     model.FileExtensions = model.FileExtensions + "xml";
                 }
             }

             if (model.Csv == true)
             {
                 if (model.FileExtensions != null)
                 {
                     model.FileExtensions = model.FileExtensions + ",csv";
                 }
                 else
                 {
                     model.FileExtensions = model.FileExtensions + "csv";
                 }
             }

             if (model.Txt == true)
             {
                 if (model.FileExtensions != null)
                 {
                     model.FileExtensions = model.FileExtensions + ",txt";
                 }
                 else
                 {
                     model.FileExtensions = model.FileExtensions + "txt";
                 }
             }

             if (model.Xlsx == true)
             {
                 if (model.FileExtensions != null)
                 {
                     model.FileExtensions = model.FileExtensions + ",xlsx";
                 }
                 else
                 {
                     model.FileExtensions = model.FileExtensions + "xlsx";
                 }
             }

             if (model.Zip == true)
             {
                 if (model.FileExtensions != null)
                 {
                     model.FileExtensions = model.FileExtensions + ",zip";
                 }
                 else
                 {
                     model.FileExtensions = model.FileExtensions + "zip";
                 }
             }
             #endregion

             var existingUser = Repository.GetRiskClientInfo(model.Id);
             var user = Repository.IsClientNameUnique(model.ClientName);

             bool clientNameValid = true;

             if (existingUser.ClientName != model.ClientName)
             {
                 if (user.IsUnique == false)
                 {
                     clientNameValid = false;
                 }
             }

             if (ModelState.IsValid && clientNameValid == true)
             {

                 // Attempt to update client
                 try
                 {

                     Repository.UpdateClient(model.Id, model.ClientName, model.AmberThreshold, model.RedThreshold, model.FileExtensions, model.StatusId, model.InsurersClientsId, model.RecordReserveChanges, model.BatchPriority);

                     string SubmitResult = "Success";

                     ModelState.Clear();

                     return Json(new
                     {
                         nameret = SubmitResult,
                     }, JsonRequestBehavior.AllowGet);

                 }
                 catch (MembershipCreateUserException e)
                 {
                     ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                 }
             }
             else
             {
                 var errors = ModelState.Values.SelectMany(v => v.Errors);

                 model.Status = GetClientStatusSelectList();
                 model.InsurersClients = Repository.GetInsurersClients();

             }
             // If we got this far, something failed, redisplay form
             return View(model);
         }


         [HttpGet]
         public ActionResult PrioritiseBatches()
         {
             PriorityClients model = new PriorityClients();
             model.ListPriorityClients = Repository.GetListOfPriorityBatchClients();

             return View(model);

         }


         public ActionResult ViewRiskWords()
         {
             int clientId = 0;

             ViewRiskWords model = new ViewRiskWords();

             model.Words = Repository.GetRiskWords(clientId);

             return View(model);
         }

         [HttpPost]
         public ActionResult ViewRiskWords(ViewRiskWords riskWords)
         {

             int clientId = 0;

             ViewRiskWords model = new ViewRiskWords();

             model.Words = Repository.GetRiskWords(clientId);

             return View(model);

         }

         public ActionResult CreateRiskWord()
         {

             CreateRiskWord riskWordDetails = new CreateRiskWord();             

             riskWordDetails.Fields = RiskWordFields();
             riskWordDetails.SearchType = SearchOptions();
             riskWordDetails.Clients = Repository.GetRiskClients();
            
             return View(riskWordDetails);

         }

         [Authorize]
         [HttpPost]
         [ActionName("CreateRiskWord")]
         //[AllowAnonymous]
         public ActionResult CreateRiskWord(CreateRiskWord riskWordDetails)
         {

             if (ModelState.IsValid)
             {
                 try
                 {
                     string[] words = riskWordDetails.FieldName.Split(' ');

                     riskWordDetails.TableName = words[0];
                     riskWordDetails.FieldName = words[1];

                     riskWordDetails.DateAdded = DateTime.Now;

                     if (riskWordDetails.FieldName == "IncidentDate" || riskWordDetails.FieldName == "DateOfBirth")
                     {
                         riskWordDetails.ReplaceWholeString = true;
                     }

                     Repository.CreateRiskWord(riskWordDetails.RiskClient_Id, riskWordDetails.TableName, riskWordDetails.FieldName, riskWordDetails.LookupWord, riskWordDetails.ReplacementWord, riskWordDetails.ReplaceWholeString, riskWordDetails.DateAdded, riskWordDetails.SearchTypeValue);

                     string SubmitResult = "Success";

                     ModelState.Clear();

                     return Json(new
                     {
                         nameret = SubmitResult
                     }, JsonRequestBehavior.AllowGet);

                 }
                 catch (MembershipCreateUserException e)
                 {
                     ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                 }
             }
             else
             {
                 var errors = ModelState.Values.SelectMany(v => v.Errors);

             }
             // If we got this far, something failed, redisplay form

             return View(riskWordDetails);

         }


         [HttpGet]
         public ActionResult EditRiskWord(int Id)
         {
             return PartialView("_EditRiskWord");
         }

         [Authorize]
         [HttpPost]
         [ValidateAntiForgeryToken]
         [ActionName("EditRiskWord")]
         public ActionResult EditRiskWord(EditRiskWord model)
         {
             return RedirectToAction("Index");
         }


         [HttpGet]
         public ActionResult DeleteRiskWord(int Id)
         {
             DeleteRiskWord riskWordDetails = new DeleteRiskWord();

             return View(riskWordDetails);
         }

         [Authorize]
         [HttpPost]
         [ValidateAntiForgeryToken]
         [ActionName("DeleteRiskWord")]
         public ActionResult DeleteRiskWord(DeleteRiskWord model)
         {

             try
             {
                 Repository.DeleteRiskWord(model.Id);

                 string SubmitResult = "Success";

                 ModelState.Clear();

                 return Json(new
                 {
                     nameret = SubmitResult
                 }, JsonRequestBehavior.AllowGet);

             }
             catch (MembershipCreateUserException e)
             {
                 ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
             }

             int clientId = 0;

             ViewRiskWords viewRiskWords = new ViewRiskWords();

             viewRiskWords.Words = Repository.GetRiskWords(clientId);

             return View(viewRiskWords);
         }

         private static IEnumerable<SelectListItem> RiskWordFields()
         {
             IEnumerable<SelectListItem> ListItems = new List<SelectListItem>{
                //new SelectListItem(){Text="Claim Account Number", Value="Claim AccountNumber"},
                new SelectListItem(){Text="Claim Incident Date", Value="Claim IncidentDate"},
                new SelectListItem(){Text="ClaimInfo Claim Code", Value="ClaimInfo ClaimCode"},
                new SelectListItem(){Text="ClaimInfo Incident Location", Value="ClaimInfo IncidentLocation"},
                new SelectListItem(){Text="ClaimInfo Incident Circs", Value="ClaimInfo IncidentCircs"},
                new SelectListItem(){Text="ClaimInfo Police Force", Value="ClaimInfo PoliceForce"},
                new SelectListItem(){Text="ClaimInfo Police Reference", Value="ClaimInfo PoliceReference"},
                new SelectListItem(){Text="Policy Broker", Value="Policy Broker"},
                new SelectListItem(){Text="Policy Policy Number", Value="Policy PolicyNumber"},
                new SelectListItem(){Text="Policy Insurer Trading Name", Value="Policy InsurerTradingName"},
                new SelectListItem(){Text="Policy Insurer", Value="Policy Insurer"},
                new SelectListItem(){Text="Address SubBuilding", Value="Address SubBuilding"},
                new SelectListItem(){Text="Address Building", Value="Address Building"},
                new SelectListItem(){Text="Address Building Number", Value="Address BuildingNumber"},
                new SelectListItem(){Text="Address County", Value="Address County"},
                new SelectListItem(){Text="Address Street", Value="Address Street"},
                new SelectListItem(){Text="Address Locality", Value="Address Locality"},
                new SelectListItem(){Text="Address Town", Value="Address Town"},
                new SelectListItem(){Text="Address PostCode", Value="Address PostCode"},
                new SelectListItem(){Text="BankAccount Account Number", Value="BankAccount AccountNumber"},
                new SelectListItem(){Text="BankAccount Sort Code", Value="BankAccount SortCode"},
                new SelectListItem(){Text="BankAccount Bank Name", Value="BankAccount BankName"},
                new SelectListItem(){Text="Email Address", Value="Email EmailAddress"},
                new SelectListItem(){Text="NINumber", Value="NINumber NINumber"},
                new SelectListItem(){Text="Organisation Name", Value="Organisation OrganisationName"},
                new SelectListItem(){Text="Organisation Registered Number", Value="Organisation RegisteredNumber"},
                new SelectListItem(){Text="Organisation Vat Number", Value="Organisation VatNumber"},
                new SelectListItem(){Text="Organisation MojCrm Number", Value="Organisation MojCrmNumber"},
                new SelectListItem(){Text="Organisation Website", Value="Website URL"},
                new SelectListItem(){Text="Organisation Telephone", Value="Organisation Telephone"},
                new SelectListItem(){Text="PaymentCard Number", Value="PaymentCard PaymentCardNumber"},
                new SelectListItem(){Text="PaymentCard SortCode", Value="PaymentCard SortCode"},
                new SelectListItem(){Text="PaymentCard Bank Name", Value="PaymentCard BankName"},
                new SelectListItem(){Text="Person First Name", Value="Person FirstName"},
                new SelectListItem(){Text="Person Middle Name", Value="Person MiddleName"},
                new SelectListItem(){Text="Person Last Name", Value="Person LastName"},
                new SelectListItem(){Text="Person Date of Birth", Value="Person DateOfBirth"},
                new SelectListItem(){Text="Person Nationality", Value="Person Nationality"},
                new SelectListItem(){Text="Person Telephone", Value="Person Telephone"},
                new SelectListItem(){Text="Person Driver Number", Value="Person DriverNumber"},
                new SelectListItem(){Text="Person Passport Number", Value="Person PassportNumber"},
                new SelectListItem(){Text="Person Occupation", Value="Person Occupation"},
                new SelectListItem(){Text="Vehicle Registration", Value="Vehicle VehicleRegistration"},
                new SelectListItem(){Text="Vehicle Damage Description", Value="Vehicle DamageDescription"},
                new SelectListItem(){Text="Vehicle Make", Value="Vehicle Make"},
                new SelectListItem(){Text="Vehicle Model", Value="Vehicle Model"},
                new SelectListItem(){Text="Vehicle Engine Capacity", Value="Vehicle EngineCapacity"},
                new SelectListItem(){Text="Vehicle VIN", Value="Vehicle VIN"},
             };

             return ListItems;
         }

         private static IEnumerable<SelectListItem> SearchOptions()
         {
             IEnumerable<SelectListItem> ListItems = new List<SelectListItem>{
                new SelectListItem(){Text="Equal to", Value="="},
                new SelectListItem(){Text="Contains", Value="~"},
                new SelectListItem(){Text="Begins", Value="<"},
                new SelectListItem(){Text="Ends", Value=">"},
             };

             return ListItems;
         }


         public ActionResult ViewRiskDefaultData()
         {
             int clientId = 0;

             ViewRiskDefaultData model = new ViewRiskDefaultData();

             model.DefaultData = Repository.GetRiskDefaultData(clientId);

             return View(model);
         }

         [HttpPost]
         public ActionResult ViewRiskDefaultData(ViewRiskDefaultData riskDefaultData)
         {

             int clientId = 0;

             ViewRiskDefaultData model = new ViewRiskDefaultData();

             model.DefaultData = Repository.GetRiskDefaultData(clientId);

             return View(model);

         }

         public ActionResult CreateRiskDefaultData()
         {

             CreateRiskDefaultData riskDefaultDataDetails = new CreateRiskDefaultData();

             riskDefaultDataDetails.Clients = Repository.GetRiskClients();
             riskDefaultDataDetails.Fields = FieldOptions();

             return View(riskDefaultDataDetails);

         }

         [Authorize]
         [HttpPost]
         [ActionName("CreateRiskDefaultData")]
         //[AllowAnonymous]
         public ActionResult CreateRiskDefaultData(CreateRiskDefaultData riskDefaultDataDetails)
         {

             if (ModelState.IsValid)
             {
                 try
                 {
                     if (riskDefaultDataDetails.RiskConfigurationDescriptionId == 1)
                     {
                         string address = "SB=[" + riskDefaultDataDetails.SubBuilding + "],BN=[" + riskDefaultDataDetails.BuildingNumber + "],BLD=[" + riskDefaultDataDetails.BuildingName + "],ST=[" + riskDefaultDataDetails.Street + "],LOC=[" + riskDefaultDataDetails.Locality + "],TWN=[" + riskDefaultDataDetails.Town + "],CNTY=[" + riskDefaultDataDetails.County + "],PC=[" + riskDefaultDataDetails.Postcode + "]";
                         riskDefaultDataDetails.ConfigurationValue = address;
                     }
                     else if (riskDefaultDataDetails.RiskConfigurationDescriptionId == 2)
                     {

                         string person = "FN=[" + riskDefaultDataDetails.FirstName + "],LN=[" + riskDefaultDataDetails.LastName + "]";
                         riskDefaultDataDetails.ConfigurationValue = person;
                     }
                     else if (riskDefaultDataDetails.RiskConfigurationDescriptionId == 3)
                     {

                         string organisation = "ON=[" + riskDefaultDataDetails.OrganisationName + "]";
                         riskDefaultDataDetails.ConfigurationValue = organisation;
                     }

                     riskDefaultDataDetails.IsActive = true;

                     Repository.CreateRiskDefaultData(riskDefaultDataDetails.RiskClient_Id, riskDefaultDataDetails.ConfigurationValue,riskDefaultDataDetails.IsActive,riskDefaultDataDetails.RiskConfigurationDescriptionId);

                     string SubmitResult = "Success";

                     ModelState.Clear();

                     return Json(new
                     {
                         nameret = SubmitResult
                     }, JsonRequestBehavior.AllowGet);

                 }
                 catch (MembershipCreateUserException e)
                 {
                     ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                 }
             }
             else
             {

                 var errors = new List<string>();

                 foreach (var modelState in ViewData.ModelState.Values)
                 {
                     errors.AddRange(modelState.Errors.Select(error => error.ErrorMessage));
                 }

                 return Json(errors);  

             }
             // If we got this far, something failed, redisplay form

             return View(riskDefaultDataDetails);

         }

         private static IEnumerable<SelectListItem> FieldOptions()
         {
             IEnumerable<SelectListItem> ListItems = new List<SelectListItem>{
                new SelectListItem(){Text="Address", Value="1"},
                new SelectListItem(){Text="Person", Value="2"},
                new SelectListItem(){Text="Organisation", Value="3"},
             };

             return ListItems;
         }

         [HttpGet]
         public ActionResult DeleteRiskDefaultData(int Id)
         {
             DeleteRiskDefaultData riskDefaultDataDetails = new DeleteRiskDefaultData();

             return View(riskDefaultDataDetails);
         }

         [Authorize]
         [HttpPost]
         [ValidateAntiForgeryToken]
         [ActionName("DeleteRiskDefaultData")]
         public ActionResult DeleteRiskDefaultData(DeleteRiskDefaultData model)
         {

             try
             {
                 Repository.DeleteRiskDefaultData(model.Id);

                 string SubmitResult = "Success";

                 ModelState.Clear();

                 return Json(new
                 {
                     nameret = SubmitResult
                 }, JsonRequestBehavior.AllowGet);

             }
             catch (MembershipCreateUserException e)
             {
                 ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
             }

             int clientId = 0;

             ViewRiskDefaultData viewRiskDefaultDatamodel = new ViewRiskDefaultData();

             viewRiskDefaultDatamodel.DefaultData = Repository.GetRiskDefaultData(clientId);

             return View(viewRiskDefaultDatamodel);
         }

         public ActionResult ViewRiskNoteDecisions()
         {
             ViewRiskNoteDecisions model = new ViewRiskNoteDecisions();

             model.RiskNoteDecisions = Repository.GetRiskNoteDecision(null);

             return View(model);
         }

         public ActionResult CreateRiskNoteDecision()
         {

             CreateRiskNoteDecision riskDefaultDataDetails = new CreateRiskNoteDecision();

             riskDefaultDataDetails.Clients = Repository.GetRiskClients();

             return View(riskDefaultDataDetails);

         }

         [Authorize]
         [HttpPost]
         [ActionName("CreateRiskNoteDecision")]
         public ActionResult CreateRiskNoteDecision(CreateRiskNoteDecision riskNoteDecisionDetails)
         {

             if (ModelState.IsValid)
             {
                 try
                 {

                     Repository.CreateRiskNoteDecision(riskNoteDecisionDetails.RiskClient_Id,riskNoteDecisionDetails.Decision.ToUpper(),riskNoteDecisionDetails.IsActive);

                     string SubmitResult = "Success";

                     ModelState.Clear();

                     return Json(new
                     {
                         nameret = SubmitResult
                     }, JsonRequestBehavior.AllowGet);

                 }
                 catch (MembershipCreateUserException e)
                 {
                     ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                 }
             }
             else
             {

                 var errors = new List<string>();

                 foreach (var modelState in ViewData.ModelState.Values)
                 {
                     errors.AddRange(modelState.Errors.Select(error => error.ErrorMessage));
                 }

                 return Json(errors);

             }
             // If we got this far, something failed, redisplay form

             return View(riskNoteDecisionDetails);

         }

         [HttpGet]
         public ActionResult DeleteRiskNoteDecision(int Id)
         {
             DeleteRiskNoteDecision riskNoteDecisionDetails = new DeleteRiskNoteDecision();

             return View(riskNoteDecisionDetails);
         }

         [Authorize]
         [HttpPost]
         [ValidateAntiForgeryToken]
         [ActionName("DeleteRiskNoteDecision")]
         public ActionResult DeleteRiskNoteDecision(DeleteRiskNoteDecision model)
         {

             try
             {
                 Repository.DeleteRiskNoteDecision(model.Id);

                 string SubmitResult = "Success";

                 ModelState.Clear();

                 return Json(new
                 {
                     nameret = SubmitResult
                 }, JsonRequestBehavior.AllowGet);

             }
             catch (MembershipCreateUserException e)
             {
                 ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
             }

             ViewRiskNoteDecisions viewRiskNoteDecisionModel = new ViewRiskNoteDecisions();

             int clientId = 0;

             viewRiskNoteDecisionModel.RiskNoteDecisions = Repository.GetRiskNoteDecision(clientId);

             return View(viewRiskNoteDecisionModel);
         }
    }
}
