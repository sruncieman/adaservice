﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.Models;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.StaticClasses;
using MDA.WCF.WebServices.Messages;
using KeoghsClientSite.FileUpload;
using MDA.Common;
using Newtonsoft.Json;
using KeoghsClientSite.Repositories.SingleClaimRepository; 

namespace KeoghsClientSite.Controllers
{
    [NoCache]
    [RedirectingAction]
    public class UploadController : Controller
    {
        //
        [NoCache]
        [Authorize]
        public ActionResult Index(BatchListViewModel batchListViewModel)
        {
            int totalOutstanding = 0;
      
            if (Request.HttpMethod == "GET")
            {
                BatchListViewModel newBatchListViewModel = new BatchListViewModel();
                batchListViewModel = Repository.PopulateBatchListViewModel(false, newBatchListViewModel);
                
            }
            else
            {
                batchListViewModel = Repository.PopulateBatchListViewModel(true, batchListViewModel);
            }

            totalOutstanding = SingleClaimRepository.GetTotalSingleClaimsForClient();

            ViewData["OutstandingClaimsCount"] = totalOutstanding;

            return View(batchListViewModel);
        }
        //
        [AjaxAuthorize]
        public JsonResult IsClientBatchReferenceUnique(string clientBatchReference)
        {
            //
            return Json(Repository.IsClientBatchReferenceUnique(clientBatchReference)); ;
            //
        }
        //
        [AjaxAuthorize]
        public JsonResult GetBatchProcessingResults(int batchId)
        {
            BatchProcessingResultsAjaxResponse batchProcessingResultsAjaxResponse = new BatchProcessingResultsAjaxResponse();
            GetBatchProcessingResultsResponse batchProcessingResultsResponse;

            batchProcessingResultsResponse = Repository.GetBatchProcessingResults(batchId);

            //
            if (batchProcessingResultsResponse.Error == null)
            {
                //
                List<DynaTreeNode> NodesArray = new List<DynaTreeNode>();

                Repository.CreateTreeNode(NodesArray, null, batchProcessingResultsResponse.ProcessingResults);

                string jsonstring = JsonConvert.SerializeObject(NodesArray.ToArray());

                batchProcessingResultsAjaxResponse.Result = jsonstring;

            }
            else
            {
                batchProcessingResultsAjaxResponse.Error = batchProcessingResultsResponse.Error;
            }

            return Json(batchProcessingResultsAjaxResponse);
        }
    }
}
