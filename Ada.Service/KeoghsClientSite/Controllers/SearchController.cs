﻿using KeoghsClientSite.Models;
using KeoghsClientSite.Repositories.Search;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Controllers
{
    public class SearchController : Controller
    {
       
        // GET: /Search/
        [Authorize]
        public ActionResult Index()
        {
            HomeViewModel oHomeViewModel = new HomeViewModel();
            int CurrentPage = 1;

            int PageSize = Convert.ToInt16(ConfigurationManager.AppSettings["SearchItemsPerPage"]);
            // Populate models
            oHomeViewModel.searchClaimsListViewModel = new SearchClaimsListViewModel();

            // Populate filter with defaults
            oHomeViewModel.searchClaimsListViewModel.searchFilterViewModel = PopulateSearch.PopulateSearchFilterViewModel(CurrentPage, PageSize);
          
            return View(oHomeViewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult SearchFilter(SearchFilterViewModel oModel)  
        {
            return View("Index", PopulateSearch.FilterModel(oModel));
        }
    }
}
