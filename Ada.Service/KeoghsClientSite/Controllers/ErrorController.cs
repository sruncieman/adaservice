﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Controllers
{
    public class ErrorController : Controller
    { 
        public ActionResult Index() 
        { 
            return View();
        } 
        
        public ActionResult NotFound() 
        { 
            return View(); 
        }

        public ActionResult NoReportAccessPermission()
        {
            return View();
        }

        public ActionResult HttpError500()
        {
            return View();
        }

        public ActionResult GeneralHttp()
        {
            return View();
        }


        public ActionResult ServicesError()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult NotAuthenticated()
        {
            return View();
        }

        
    }
}
