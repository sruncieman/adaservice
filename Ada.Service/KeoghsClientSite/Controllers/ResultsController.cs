﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.Models;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.StaticClasses;

namespace KeoghsClientSite.Controllers
{
    public class ResultsController : Controller
    {
        //
        // GET: /Results/

        [Authorize]
        [HttpGet]
        public ActionResult Index()
        {
            int totalCount;
            int CurrentPage = 1;
            int PageSize = 10;
            string SortCol = "UPLOADDATETIME";
            bool SortAscending = false;
            int SelectedClientId = -1;
            //
            ResultsViewModel oResultsViewModel = new ResultsViewModel();
            //
            // Populate models
            oResultsViewModel.claimsListViewModel = Repository.PopulateClaimsListViewModel();
            oResultsViewModel.claimsListViewModel.claimsFilterViewModel = Repository.PopulateClaimsFilterViewModel("Results", CurrentPage,PageSize, SortCol,
                                                                                                                 SortAscending,SelectedClientId);

            //
            // Needed to prevent TextBoxFor from using querystring value
            //ModelState.Remove("FilterStartDate");
            //ModelState.Remove("FilterEndDate");
            //
            oResultsViewModel.claimsListViewModel.Claims = Repository.GetclaimsForClient(oResultsViewModel.claimsListViewModel.claimsFilterViewModel, out totalCount);
            //
            oResultsViewModel.claimsListViewModel.claimsFilterViewModel.PagingInfo.TotalItems = totalCount;
            //
            return View(oResultsViewModel);
        }
        //
        [Authorize]
        [HttpPost]
        public ActionResult Filter(ClaimsFilterViewModel oModel)  // confirmed
        {

            //
            return View("Index", Repository.FilterModel(oModel,true));
        }

    }
}
