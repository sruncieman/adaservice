﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult InsuredDriverPreviousView(InsuredDriverViewModel insuredDriverViewModel)
        {
            SingleClaimRepository.StoreInsuredDriverViewModel(insuredDriverViewModel);
            
            ModelState.Clear();

            InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();

            if (Session["insuredVehicleViewModel"] != null)
            {
                insuredVehicleViewModel = Session["insuredVehicleViewModel"] as InsuredVehicleViewModel;
            }

            return PartialView("_InsuredVehicle", SingleClaimRepository.PopulateInsuredVehicleViewModel(insuredVehicleViewModel.PreviousPage, "", true));
        }
    }
}