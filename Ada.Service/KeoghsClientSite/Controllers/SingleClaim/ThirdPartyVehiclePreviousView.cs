﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult ThirdPartyVehiclePreviousView(ThirdPartyVehicleViewModel thirdPartyVehicleViewModel)
        {
            SingleClaimRepository.StoreThirdPartyVehicleViewModel(thirdPartyVehicleViewModel, thirdPartyVehicleViewModel.CurrentVehicle);
            
            ModelState.Clear();
            if (thirdPartyVehicleViewModel.PreviousPage == CurrentView.Passenger)
            {
                if (thirdPartyVehicleViewModel.CurrentVehicle > 1)
                {
                    // Go back to last Passenger in previous third party vehicle
                    return PartialView("_Passenger",
                        SingleClaimRepository.PopulatePassengerViewModel(SingleClaimRepository.ThirdPartyVehicleNumberPassengers(thirdPartyVehicleViewModel.VehicleID - 1),
                                                                                                     CurrentView.ThirdPartyVehicle, -1,
                                                                                                     PassengerType.ThirdPartyVehiclePassenger, thirdPartyVehicleViewModel.VehicleID - 1, "",
                                                                                                     true, CurrentView.ThirdPartyVehicle.ToString()));
                }
                else
                {
                    // Go back to last Passenger in Insured Vehicle
                    return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(SingleClaimRepository.InsuredVehicleNumberPassengers(),
                                                                                                     CurrentView.ThirdPartyVehicle, -1,
                                                                                                     PassengerType.InsuredVehiclePassenger, -1, "",
                                                                                                     true, CurrentView.ThirdPartyVehicle.ToString()));
                }
            }
            else if (thirdPartyVehicleViewModel.PreviousPage == CurrentView.InsuredDriver)
            {
                // Go back to Insured Driver
                return PartialView("_InsuredDriver", SingleClaimRepository.PopulateInsuredDriverViewModel("", true, CurrentView.ThirdPartyVehicle.ToString()));
            }
            else if (thirdPartyVehicleViewModel.PreviousPage == CurrentView.SupportingInvolvementInsuredPassenger)
            {
                // Go back to last supporting involvement of last Passenger
                int NumSupportingInvolvements = 0;
                int NumPassengers = 0;
                NumPassengers = SingleClaimRepository.InsuredVehicleNumberPassengers();
                NumSupportingInvolvements = SingleClaimRepository.GetNumberofSupportingInvolvementsForPassenger(NumPassengers);

                return PartialView("_SupportingInvolvements", SingleClaimRepository.PopulateSupportingInvolvementsViewModel(NumSupportingInvolvements,
                                                                                            NumSupportingInvolvements,
                                                                                            SupportingInvolvementsType.InsuredPassenger, NumPassengers, -1,
                                                                                            "", true, CurrentView.ThirdPartyVehicle.ToString()));
            }
            else if (thirdPartyVehicleViewModel.PreviousPage == CurrentView.SupportingInvolvementInsuredDriver)
            {
                // Go back to last supporting involvement of Insured Driver
                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                                                                SingleClaimRepository.GetNumberofSupportingInvolvementsForInsuredDriver(),
                                                                                SupportingInvolvementsType.InsuredDriver, -1, -1,
                                                                                "", true, CurrentView.ThirdPartyVehicle.ToString()));
            }
            else if (thirdPartyVehicleViewModel.PreviousPage == CurrentView.SupportingInvolvementThirdPartyPassenger)
            {
                // Go back to last supporting involvement of Passenger
                //if (thirdPartyVehicleViewModel.VehicleID > 1)
                //{
                // get number of supporting involvements for last passenger of previous vehicle

                int SupportingInvolvement;
                int Passenger;

                Passenger = SingleClaimRepository.ThirdPartyVehicleNumberPassengers(thirdPartyVehicleViewModel.VehicleID - 1);
                SupportingInvolvement = SingleClaimRepository.GetNumberofSupportingInvolvementsForThirdPartyPassenger(thirdPartyVehicleViewModel.VehicleID - 1, Passenger, true);

                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                    SupportingInvolvement,
                                    SupportingInvolvementsType.ThirdPartyPassenger, Passenger, thirdPartyVehicleViewModel.VehicleID - 1,
                                    "", true, CurrentView.ThirdPartyVehicle.ToString()));
                // }
            }
            else if (thirdPartyVehicleViewModel.PreviousPage == CurrentView.SupportingInvolvementThirdPartyDriver)
            {
                // Go back to last supporting involvement of Third Party Driver
                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                                                        SingleClaimRepository.GetNumberofSupportingInvolvementsForThirdPartyDriver(thirdPartyVehicleViewModel.VehicleID - 1),
                                                                        SupportingInvolvementsType.ThirdPartyDriver, thirdPartyVehicleViewModel.VehicleID - 1, -1,
                                                                        "", true, CurrentView.ThirdPartyVehicle.ToString()));


            }
            else if (thirdPartyVehicleViewModel.PreviousPage == CurrentView.ThirdPartyDriver)
            {
                return PartialView("_ThirdPartyDriver",
                  SingleClaimRepository.PopulateThirdPartyDriverViewModel(thirdPartyVehicleViewModel.VehicleID - 1,
                                                                          CurrentView.Passenger, "", true, CurrentView.ThirdPartyVehicle.ToString()));
            }
            else
                return null;
        }
    }
}