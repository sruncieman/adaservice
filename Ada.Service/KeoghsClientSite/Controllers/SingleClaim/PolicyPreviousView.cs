﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult PolicyPreviousView(PolicyViewModel policyViewModel)
        {
            SingleClaimRepository.StorePolicyViewModel(policyViewModel);
            
            ModelState.Clear();
            IncidentViewModel incidentViewModel = SingleClaimRepository.PopulateIncidentViewModel("", true);

            return PartialView("_Incident", incidentViewModel);
        }
    }
}