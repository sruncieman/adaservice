﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [Authorize]
        public PartialViewResult GetClaimView()
        {
            ClaimViewModel claimViewModel = SingleClaimRepository.PopulateClaimViewModel();
            claimViewModel.SelectedClaimTypeID = 1;
            return PartialView("_Claim", claimViewModel);
        }
    }
}