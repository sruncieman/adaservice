﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult WitnessPreviousView(WitnessViewModel witnessViewModel)
        {
            SingleClaimRepository.StoreWitnessViewModel(witnessViewModel, witnessViewModel.CurrentWitness);
            
            ModelState.Clear();
            int NumThirdPartyVehicles = SingleClaimRepository.NumberOfThirdPartyVehicles();

            if (witnessViewModel.PreviousPage == CurrentView.Witness)
            {
                return PartialView("_Witness",
                   SingleClaimRepository.PopulateWitnessViewModel(witnessViewModel.CurrentWitness - 1,
                                                                           CurrentView.Witness));
            }
            else if (witnessViewModel.PreviousPage == CurrentView.InsuredDriver)
            {
                return PartialView("_InsuredDriver", SingleClaimRepository.PopulateInsuredDriverViewModel("", true, CurrentView.Witness.ToString()));
            }
            else if (witnessViewModel.PreviousPage == CurrentView.Passenger)
            {
                // if there a third party vehicle with a passenger go back to that
                if (SingleClaimRepository.NumberOfThirdPartyVehicles() > 0)
                {
                    // has last vehicle got a passenger


                    int Passenger = SingleClaimRepository.ThirdPartyVehicleNumberPassengers(NumThirdPartyVehicles);

                    return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(Passenger,
                                                                                                      CurrentView.Witness,
                                                                                                      -1,
                                                                                                      PassengerType.ThirdPartyVehiclePassenger, NumThirdPartyVehicles,
                                                                                                      "", true, CurrentView.Witness.ToString()));
                }
                else
                {
                    int InsuredVehicleNumberPassengers = SingleClaimRepository.InsuredVehicleNumberPassengers();

                    return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(InsuredVehicleNumberPassengers,
                                                                                                        CurrentView.Witness,
                                                                                                        -1,
                                                                                                        PassengerType.InsuredVehiclePassenger, -1,
                                                                                                        "", true, CurrentView.Witness.ToString()));
                }
            }
            else if (witnessViewModel.PreviousPage == CurrentView.ThirdPartyDriver)
            {
                return PartialView("_ThirdPartyDriver",
                  SingleClaimRepository.PopulateThirdPartyDriverViewModel(SingleClaimRepository.NumberOfThirdPartyVehicles(),
                                                                          CurrentView.Witness, "", true, CurrentView.Witness.ToString()));
            }
            else if (witnessViewModel.PreviousPage == CurrentView.SupportingInvolvementInsuredPassenger)
            {
                // Return to last supporting involvement of Insured Passenger
                ModelState.Clear();

                int NumSupportingInvolvements = 0;
                int Passenger = SingleClaimRepository.InsuredVehicleNumberPassengers();
                NumSupportingInvolvements = SingleClaimRepository.GetNumberofSupportingInvolvementsForPassenger(Passenger);


                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(NumSupportingInvolvements,
                                                                                                    NumSupportingInvolvements,
                                                                                                    SupportingInvolvementsType.InsuredPassenger,
                                                                                                    Passenger, -1, "", true, CurrentView.Witness.ToString()));
            }
            else if (witnessViewModel.PreviousPage == CurrentView.SupportingInvolvementInsuredDriver)
            {
                // Go back to last supporting involvement of Insured Driver
                // 
                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                                                                SingleClaimRepository.GetNumberofSupportingInvolvementsForInsuredDriver(),
                                                                                SupportingInvolvementsType.InsuredDriver, -1, -1, "", true, CurrentView.Witness.ToString()));
            }
            else if (witnessViewModel.PreviousPage == CurrentView.SupportingInvolvementThirdPartyDriver)
            {
                // Go back to last supporting involvement of Third Party Driver
                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                                                                SingleClaimRepository.GetNumberofSupportingInvolvementsForThirdPartyDriver(NumThirdPartyVehicles),
                                                                                SupportingInvolvementsType.ThirdPartyDriver, NumThirdPartyVehicles, -1, "", true, CurrentView.Witness.ToString()));
            }
            else if (witnessViewModel.PreviousPage == CurrentView.SupportingInvolvementThirdPartyPassenger)
            {
                // Go back to last supporting involvement of Third Party Passenger
                //return PartialView("_SupportingInvolvements",
                //                   SingleClaimRepository2.PopulateSupportingInvolvementsViewModel(-1,
                //                                                               SingleClaimRepository2.GetNumberofSupportingInvolvementsForThirdPartyPassenger(passengerViewModel.VehicleID, passengerViewModel.CurrentPassenger),
                //                                                               SupportingInvolvementsType.ThirdPartyPassenger, passengerViewModel.CurrentPassenger, passengerViewModel.VehicleID));
                int Passenger = SingleClaimRepository.ThirdPartyVehicleNumberPassengers(NumThirdPartyVehicles);

                return PartialView("_SupportingInvolvements",
                                  SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                                                              SingleClaimRepository.GetNumberofSupportingInvolvementsForThirdPartyPassenger(NumThirdPartyVehicles, Passenger),
                                                                              SupportingInvolvementsType.ThirdPartyPassenger, Passenger, NumThirdPartyVehicles,
                                                                              "", true, CurrentView.Witness.ToString()));
            }
            else
                return null;
        }
    }
}