﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        //
        // Called by clicking Previous button on Incident form
        [AjaxAuthorize]
        public PartialViewResult IncidentPreviousView(IncidentViewModel incidentViewModel)
        {
            SingleClaimRepository.StoreIncidentViewModel(incidentViewModel);

            
            ModelState.Clear();
            ClaimViewModel claimViewModel = SingleClaimRepository.PopulateClaimViewModel(true);

            return PartialView("_Claim", claimViewModel);
        }
    }
}