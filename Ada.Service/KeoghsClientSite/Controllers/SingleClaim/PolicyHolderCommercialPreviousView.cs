﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult PolicyHolderCommercialPreviousView(PolicyHolderCommercialViewModel policyHolderCommercialViewModel)
        {
            SingleClaimRepository.StorePolicyHolderCommercialViewModel(policyHolderCommercialViewModel);
            
            ModelState.Clear();
            return PartialView("_Policy", SingleClaimRepository.PopulatePolicyViewModel("", true));
        }
    }
}