﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult PolicyHolderPersonalPreviousView(PolicyHolderPersonalViewModel policyHolderPersonalViewModel)
        {
            SingleClaimRepository.StorePolicyHolderViewModel(policyHolderPersonalViewModel);

            ModelState.Clear();
            return PartialView("_Policy", SingleClaimRepository.PopulatePolicyViewModel("", true));
        }
    }
}