﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult ThirdPartyDriverPreviousView(ThirdPartyDriverViewModel thirdPartyDriverViewModel)
        {
            SingleClaimRepository.StoreThirdPartyDriverViewModel(thirdPartyDriverViewModel, thirdPartyDriverViewModel.CurrentVehicle);
            
            // Go back to last Passenger in Insured Vehicle
            ModelState.Clear();
            return PartialView("_ThirdPartyVehicle",
                                                    SingleClaimRepository.PopulateThirdPartyVehicleViewModel(thirdPartyDriverViewModel.CurrentVehicle,
                                                    CurrentView.Unknown,
                                                    "", true));
        }
    }
}