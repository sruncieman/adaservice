﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult SupportingInvolvementsPreviousView(SupportingInvolvementsViewModel supportingInvolvementsViewModel)
        {
            if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.InsuredDriver)
                SingleClaimRepository.StoreSupportingInvolvementsViewModel(supportingInvolvementsViewModel,
                                                                            supportingInvolvementsViewModel.SupportingInvolvementsType);
            else if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.InsuredPassenger)
                SingleClaimRepository.StoreSupportingInvolvementsViewModel(supportingInvolvementsViewModel, supportingInvolvementsViewModel.SupportingInvolvementsType,
                                                                                    supportingInvolvementsViewModel.ParentID);
            else if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.ThirdPartyDriver)
                SingleClaimRepository.StoreSupportingInvolvementsViewModel(supportingInvolvementsViewModel, supportingInvolvementsViewModel.SupportingInvolvementsType,
                                                                                            supportingInvolvementsViewModel.ParentID, supportingInvolvementsViewModel.VehicleID);
            else if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.ThirdPartyPassenger)
                SingleClaimRepository.StoreSupportingInvolvementsViewModel(supportingInvolvementsViewModel, supportingInvolvementsViewModel.SupportingInvolvementsType,
                                                                                            supportingInvolvementsViewModel.ParentID, supportingInvolvementsViewModel.VehicleID);
            
            CurrentView CurrentPage = CurrentView.Unknown;
            CurrentView PreviousPage;

            switch (supportingInvolvementsViewModel.SupportingInvolvementsType)
            {
                case SupportingInvolvementsType.InsuredDriver:
                    CurrentPage = CurrentView.SupportingInvolvementInsuredDriver;
                    break;
                case SupportingInvolvementsType.InsuredPassenger:
                    CurrentPage = CurrentView.SupportingInvolvementInsuredPassenger;
                    break;
                case SupportingInvolvementsType.ThirdPartyDriver:
                    CurrentPage = CurrentView.SupportingInvolvementThirdPartyDriver;
                    break;
                case SupportingInvolvementsType.ThirdPartyPassenger:
                    CurrentPage = CurrentView.SupportingInvolvementThirdPartyPassenger;
                    break;
            }

            PreviousPage = SingleClaimRepository.GetPreviousPage(CurrentPage, supportingInvolvementsViewModel.NumberSupportingInvolvements,
                                                                supportingInvolvementsViewModel.CurrentSupportingInvolvement);
            ModelState.Clear();
            if (PreviousPage == CurrentView.InsuredDriver)
            {
                // First Supporting Involvement --> Go back to parent
                return PartialView("_InsuredDriver", SingleClaimRepository.PopulateInsuredDriverViewModel("", true));
            }
            else if (PreviousPage == CurrentView.Passenger)
            {
                // First Supporting Involvement --> Go back to parent
                if (CurrentPage == CurrentView.SupportingInvolvementThirdPartyPassenger)
                    return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(supportingInvolvementsViewModel.ParentID,
                                                                                                      CurrentView.SupportingInvolvementThirdPartyPassenger,
                                                                                                      supportingInvolvementsViewModel.ParentID,
                                                                                                      PassengerType.ThirdPartyVehiclePassenger,
                                                                                                      supportingInvolvementsViewModel.VehicleID,
                                                                                                      "", true));
                else
                    return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(supportingInvolvementsViewModel.ParentID,
                                                                                                        CurrentView.SupportingInvolvementInsuredPassenger,
                                                                                                        supportingInvolvementsViewModel.ParentID,
                                                                                                        PassengerType.InsuredVehiclePassenger, -1, "", true));

            }
            else if (PreviousPage == CurrentView.ThirdPartyDriver)
            {
                // First Supporting Involvement --> Go back to parent
                return PartialView("_ThirdPartyDriver",
                   SingleClaimRepository.PopulateThirdPartyDriverViewModel(supportingInvolvementsViewModel.VehicleID,
                                                                           CurrentPage, "", true));

            }
            else if (PreviousPage == CurrentView.SupportingInvolvementInsuredDriver)
            {
                // Return to previous supporting involvement
                ModelState.Clear();
                return PartialView("_SupportingInvolvements",
                        SingleClaimRepository.PopulateSupportingInvolvementsViewModel(supportingInvolvementsViewModel.NumberSupportingInvolvements,
                                                                                        supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1,
                                                                                        supportingInvolvementsViewModel.SupportingInvolvementsType, -1, -1, "", true));
            }
            else if (PreviousPage == CurrentView.SupportingInvolvementInsuredPassenger)
            {
                //Return to previous Supporting Involvement Insured Passenger
                // 
                ModelState.Clear();
                // Note the parent ID of the parent Passenger is passed in here.
                return PartialView("_SupportingInvolvements",
                        SingleClaimRepository.PopulateSupportingInvolvementsViewModel(supportingInvolvementsViewModel.NumberSupportingInvolvements,
                                                                                        supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1,
                                                                                        supportingInvolvementsViewModel.SupportingInvolvementsType,
                                                                                        supportingInvolvementsViewModel.ParentID, -1, "", true));
            }
            else if (PreviousPage == CurrentView.SupportingInvolvementThirdPartyDriver)
            {
                //Return to previous Supporting Involvement ThirdPartyDriver
                ModelState.Clear();
                //
                return PartialView("_SupportingInvolvements",
                        SingleClaimRepository.PopulateSupportingInvolvementsViewModel(supportingInvolvementsViewModel.NumberSupportingInvolvements,
                                                                                        supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1,
                                                                                        supportingInvolvementsViewModel.SupportingInvolvementsType,
                                                                                        supportingInvolvementsViewModel.ParentID, -1, "", true));
            }
            else if (PreviousPage == CurrentView.SupportingInvolvementThirdPartyPassenger)
            {
                //Return to previous Supporting Involvement ThirdPartyDriver
                ModelState.Clear();
                //
                return PartialView("_SupportingInvolvements",
                        SingleClaimRepository.PopulateSupportingInvolvementsViewModel(supportingInvolvementsViewModel.NumberSupportingInvolvements,
                                                                                        supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1,
                                                                                        supportingInvolvementsViewModel.SupportingInvolvementsType,
                                                                                        supportingInvolvementsViewModel.ParentID, supportingInvolvementsViewModel.VehicleID,
                                                                                        "", true));
            }
            else
                return null;
        }
    }
}