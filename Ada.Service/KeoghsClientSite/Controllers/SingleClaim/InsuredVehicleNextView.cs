﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult InsuredVehicleNextView(InsuredVehicleViewModel insuredVehicleViewModel)
        {
           
            SingleClaimRepository.StoreInsuredVehicleViewModel(insuredVehicleViewModel);
            SingleClaimRepository.Save();

            List<PassengerViewModel> insuredDriverPassengers = new List<PassengerViewModel>();

            insuredDriverPassengers = (List<PassengerViewModel>)Session["InsuredDriverPassengers"];

            if (insuredDriverPassengers != null && insuredDriverPassengers.Count() > 0)
            {
                PassengerViewModel newPassengerViewModel = new PassengerViewModel();
                newPassengerViewModel = insuredDriverPassengers[0];

                int orignialNumPassengers = newPassengerViewModel.NumberPassengers;

                if (newPassengerViewModel.NumberPassengers != insuredVehicleViewModel.Vehicle.NumberOfPassengers)
                {
                    foreach (var insuredDriverPassenger in insuredDriverPassengers)
                    {
                        insuredDriverPassenger.NumberPassengers = insuredVehicleViewModel.Vehicle.NumberOfPassengers;
                    }
                    
                }

                // Check if number of passengers has decreased
                if (insuredVehicleViewModel.Vehicle.NumberOfPassengers > -1 && insuredVehicleViewModel.Vehicle.NumberOfPassengers < orignialNumPassengers)
                {
                    insuredDriverPassengers.RemoveAll(x => x.PassengerID > insuredVehicleViewModel.Vehicle.NumberOfPassengers);

                    // Remove supporting involvements for deleted passenger too
                    Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                    //Get dictionary from session
                    DictionaryPassengerSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)Session["DictionaryPassengerSupportingInvolvements"];
                    if (DictionaryPassengerSupportingInvolvements != null)
                    {
                       
                        var obsoletePassengers = DictionaryPassengerSupportingInvolvements.Where(x=>x.Value.All(y=>y.ParentID>insuredVehicleViewModel.Vehicle.NumberOfPassengers)).FirstOrDefault();

                        DictionaryPassengerSupportingInvolvements.Remove(obsoletePassengers.Key);
                    }

                }


                if (insuredVehicleViewModel.Vehicle.NumberOfPassengers == 0)
                {
                    Session["InsuredDriverPassengers"] = null;
                }

            }

            if (ModelState.IsValid)
                return PartialView("_InsuredDriver",
                    SingleClaimRepository.PopulateInsuredDriverViewModel(insuredVehicleViewModel.Vehicle.RegistrationNumber + "|" +
                                                                         insuredVehicleViewModel.Vehicle.Make + "|" +
                                                                         insuredVehicleViewModel.Vehicle.Model));
            else
                return PartialView("_InsuredVehicle", SingleClaimRepository.PopulateInsuredVehicleViewModel());

        }


        //public PartialViewResult SubmitInsuredVehicleView(InsuredVehicleViewModel insuredVehicleViewModel)
        //{
        //    SingleClaimRepository.StoreInsuredVehicleViewModel(insuredVehicleViewModel);
        //    SingleClaimRepository.Save();

        //    //PolicyViewModel policyViewModel = (PolicyViewModel)System.Web.HttpContext.Current.Session["policyViewModel"];

        //    return PartialView("_InsuredVehicle", SingleClaimRepository.PopulateInsuredVehicleViewModel());
        //}
    }
}