﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult PassengerNextView(PassengerViewModel passengerViewModel)
        {
            // Does previous passenger have a supporting involvement still?
            if (passengerViewModel.CurrentPassenger > 1)
                passengerViewModel.PreviousPage = SingleClaimRepository.CheckPreviousPassengerHasSupportingInvolvement(passengerViewModel);
                
                
                    

           
            SingleClaimRepository.StorePassengerViewModel(passengerViewModel, passengerViewModel.PassengerType,
                                                          passengerViewModel.VehicleID);


            SingleClaimRepository.Save();

            // Are there any supporting involvements
            if (!ModelState.IsValid)
            {
                //validation failed
                return PartialView("_Passenger",
                                   SingleClaimRepository.PopulatePassengerViewModel(passengerViewModel.CurrentPassenger,
                                                                                    CurrentView.Passenger,
                                                                                    passengerViewModel.PreviousPageID,
                                                                                    passengerViewModel.PassengerType,
                                                                                    passengerViewModel.VehicleID));
            }
            else
            {
                PersonInformation myPerson = passengerViewModel.PersonInformation;
                string NavigationData = myPerson.Middlename != ""
                                            ? myPerson.Firstname + " " + myPerson.Middlename + " " + myPerson.Surname
                                            : myPerson.Firstname + " " + myPerson.Surname;

                if (passengerViewModel.PersonInformation.NumberOfSupportingInvolvements == 0)
                {
                    // clear Passenger Supp Involvements if not empty

                    if (passengerViewModel.PassengerType == PassengerType.InsuredVehiclePassenger)
                    {
                        if (Session["DictionaryPassengerSupportingInvolvements"] != null)
                        {
                            Dictionary<int, List<SupportingInvolvementsViewModel>>
                                DictionaryPassengerSupportingInvolvements =
                                    new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                            DictionaryPassengerSupportingInvolvements =
                                (Dictionary<int, List<SupportingInvolvementsViewModel>>)
                                Session["DictionaryPassengerSupportingInvolvements"];

                            if (DictionaryPassengerSupportingInvolvements != null)
                            {
                                int passengerIndex = passengerViewModel.PassengerID;
                                if (passengerIndex > 0)
                                {

                                    if (DictionaryPassengerSupportingInvolvements.ContainsKey(passengerIndex))
                                    {
                                        DictionaryPassengerSupportingInvolvements.Remove(passengerIndex);
                                        Session["DictionaryPassengerSupportingInvolvements"] =
                                            DictionaryPassengerSupportingInvolvements;

                                    }
                                }
                            }
                        }
                    }

                    if (passengerViewModel.PassengerType == PassengerType.ThirdPartyVehiclePassenger)
                    {
                        Dictionary<int, List<SupportingInvolvementsViewModel>>
                            DictionaryThirdPartyPassengerSupportingInvolvements =
                                new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                        Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>
                            DictionaryThirdPartyVehicleSupportingInvolvements =
                                new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();

                        DictionaryThirdPartyVehicleSupportingInvolvements =
                            (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)
                            Session["DictionaryThirdPartyVehicleSupportingInvolvements"];

                        if (DictionaryThirdPartyVehicleSupportingInvolvements != null)
                        {
                            if (
                                DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(
                                    passengerViewModel.VehicleID))
                            {
                                DictionaryThirdPartyPassengerSupportingInvolvements =
                                    DictionaryThirdPartyVehicleSupportingInvolvements[passengerViewModel.VehicleID];
                                if (
                                    DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(
                                        passengerViewModel.PassengerID))
                                {
                                    DictionaryThirdPartyPassengerSupportingInvolvements.Remove(
                                        passengerViewModel.PassengerID);
                                    Session["DictionaryThirdPartyVehicleSupportingInvolvements"] =
                                        DictionaryThirdPartyVehicleSupportingInvolvements;
                
                                }
                            }
                        }
                    }
                }

                if (passengerViewModel.PersonInformation.NumberOfSupportingInvolvements > 0)
                {
                    ModelState.Clear();
                    //Display Supporting Involvements
                    SupportingInvolvementsType supportingInvolvementsType;

                    supportingInvolvementsType = passengerViewModel.PassengerType ==
                                                 PassengerType.InsuredVehiclePassenger
                                                     ? SupportingInvolvementsType.InsuredPassenger
                                                     : SupportingInvolvementsType.ThirdPartyPassenger;

                    return PartialView("_SupportingInvolvements",
                                       SingleClaimRepository.PopulateSupportingInvolvementsViewModel(
                                           passengerViewModel.PersonInformation.NumberOfSupportingInvolvements,
                                           1, supportingInvolvementsType,
                                           passengerViewModel.PassengerID, passengerViewModel.VehicleID,
                                           NavigationData));

                }
                else if (passengerViewModel.CurrentPassenger < passengerViewModel.NumberPassengers)
                {
                    // show next Passenger
                    ModelState.Clear();
                    NavigationData += "|" + passengerViewModel.CurrentPassenger;
                    return PartialView("_Passenger",
                                       SingleClaimRepository.PopulatePassengerViewModel(
                                           passengerViewModel.CurrentPassenger + 1,
                                           CurrentView.Passenger,
                                           passengerViewModel.PreviousPageID,
                                           passengerViewModel.PassengerType, passengerViewModel.VehicleID,
                                           NavigationData));
                }
                else
                {
                    ModelState.Clear();
                    // Store current passenger for use in updating navigation data
                    NavigationData += "|" + passengerViewModel.CurrentPassenger;
                    // Display Third Party Vehicle
                    if (passengerViewModel.PassengerType == PassengerType.InsuredVehiclePassenger)
                    {

                        if (SingleClaimRepository.NumberOfThirdPartyVehicles() > 0)
                            return PartialView("_ThirdPartyVehicle",
                                               SingleClaimRepository.PopulateThirdPartyVehicleViewModel(1,
                                                                                                        CurrentView.
                                                                                                            Passenger,
                                                                                                        NavigationData));
                        else if (SingleClaimRepository.NumberOfWitnesses() > 0)
                        {
                            return PartialView("_Witness",
                                               SingleClaimRepository.PopulateWitnessViewModel(1,
                                                                                              CurrentView.Passenger,
                                                                                              NavigationData));
                        }
                        else
                        {
                            // For now display final review screen
                            return PartialView("_ReviewAndSubmit",
                                               SingleClaimRepository.PopulateReviewAndSubmitViewModel(
                                                   SingleClaimRepository.NumberOfThirdPartyVehicles(),
                                                   passengerViewModel.VehicleID,
                                                   CurrentView.Passenger, NavigationData));
                        }
                    }
                    else
                    {
                        // identify which vehicle for the nav
                        NavigationData += "|" + passengerViewModel.VehicleID;
                        //
                        int NumThirdPartyVehicles = 0;
                        NumThirdPartyVehicles = SingleClaimRepository.NumberOfThirdPartyVehicles();

                        //if (SingleClaimRepository.NumberOfThirdPartyVehicles() > passengerViewModel.VehicleID)
                        if (NumThirdPartyVehicles > 1 && passengerViewModel.VehicleID < NumThirdPartyVehicles)
                            return PartialView("_ThirdPartyVehicle",
                                               SingleClaimRepository.PopulateThirdPartyVehicleViewModel(
                                                   passengerViewModel.VehicleID + 1, CurrentView.Passenger,
                                                   NavigationData));
                        else if (SingleClaimRepository.NumberOfWitnesses() > 0)
                        {
                            return PartialView("_Witness",
                                               SingleClaimRepository.PopulateWitnessViewModel(1,
                                                                                              CurrentView.Passenger,
                                                                                              NavigationData));
                        }
                        else
                        {
                            //return PartialView("_ReviewAndSubmit");
                            // any witnesses?
                            // For now display final review screen
                            return PartialView("_ReviewAndSubmit",
                                               SingleClaimRepository.PopulateReviewAndSubmitViewModel(
                                                   SingleClaimRepository.NumberOfThirdPartyVehicles(),
                                                   passengerViewModel.VehicleID,
                                                   CurrentView.Passenger, NavigationData));
                        }
                    }
                }
            }
        }
    

    //public PartialViewResult SubmitPassengerView(PassengerViewModel passengerViewModel)
    //    {
    //        SingleClaimRepository.StorePassengerViewModel(passengerViewModel, passengerViewModel.PassengerType,
    //                                                      passengerViewModel.VehicleID);
    //        SingleClaimRepository.Save();


    //        return PartialView("_Passenger",
    //                               SingleClaimRepository.PopulatePassengerViewModel(passengerViewModel.CurrentPassenger,
    //                                                                                CurrentView.Passenger,
    //                                                                                passengerViewModel.PreviousPageID,
    //                                                                                passengerViewModel.PassengerType,
    //                                                                                passengerViewModel.VehicleID));

           
    //    }

    }
}