﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult ReviewAndSubmitNextView(ReviewAndSubmitViewModel reviewAndSubmitViewModel)
        {
            // Store review and submit settings
            //SingleClaimRepository.StoreReviewAndSubmitViewModel(reviewAndSubmitViewModel);
            //SingleClaimRepository.Save();
            //
            //ModelState.Clear();
            return PartialView("_UploadProgress");
        }


        public PartialViewResult SubmitReviewAndSubmitView(ReviewAndSubmitViewModel reviewAndSubmitViewModel)
        {
            SingleClaimRepository.StoreReviewAndSubmitViewModel(reviewAndSubmitViewModel);
            SingleClaimRepository.Save();


            return PartialView("_Claim", SingleClaimRepository.PopulateClaimViewModel());
        }
    }
}