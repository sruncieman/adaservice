﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult PolicyHolderCommercialNextView(PolicyHolderCommercialViewModel policyHolderCommercialViewModel)
        {
            SingleClaimRepository.StorePolicyHolderCommercialViewModel(policyHolderCommercialViewModel);
            SingleClaimRepository.Save();

            if (ModelState.IsValid)
            {
                InsuredVehicleViewModel insuredVehicleViewModel =
                    SingleClaimRepository.PopulateInsuredVehicleViewModel(CurrentView.PolicyHolderCommercial, policyHolderCommercialViewModel.Organisation.Name);

                return PartialView("_InsuredVehicle", insuredVehicleViewModel);
            }
            else
                return PartialView("_PolicyHolderPersonal", SingleClaimRepository.PopulatePolicyHolderPersonalViewModel());

        }


        //public PartialViewResult SubmitPolicyHolderCommercialView(PolicyHolderCommercialViewModel policyHolderCommercialViewModel)
        //{
        //    SingleClaimRepository.StorePolicyHolderCommercialViewModel(policyHolderCommercialViewModel);
        //    SingleClaimRepository.Save();

        //    PolicyViewModel policyViewModel = (PolicyViewModel)System.Web.HttpContext.Current.Session["policyViewModel"];

        //    return PartialView("_PolicyHolderCommercial", SingleClaimRepository.PopulatePolicyHolderCommercialViewModel(policyViewModel.PolicyNumber + "|" + SingleClaimRepository.GetPolicyTypeFromID(policyViewModel.SelectedPolicyType)));
        //}

    }
}