﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories.SingleClaimRepository;



namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public ActionResult IncidentNextView(IncidentViewModel incidentViewModel)
        {
            //Store selected values in session for now
            SingleClaimRepository.StoreIncidentViewModel(incidentViewModel);
            SingleClaimRepository.Save();
           
            SingleClaimRepository.CheckThirdPartyVehicles(incidentViewModel);

            if (ModelState.IsValid)
            {
                string IncidentLocation = "";
                //
                IncidentLocation = incidentViewModel.Location;
                DateTime IncidentDate = Convert.ToDateTime(incidentViewModel.IncidentDate).Date;
                if (!string.IsNullOrEmpty(incidentViewModel.SelectedIncidentTimeHours) && incidentViewModel.SelectedIncidentTimeHours!="--")
                    IncidentDate = IncidentDate.AddHours(Convert.ToDouble(incidentViewModel.SelectedIncidentTimeHours));

                if (!string.IsNullOrEmpty(incidentViewModel.SelectedIncidentTimeMinutes) && incidentViewModel.SelectedIncidentTimeMinutes != "--" && incidentViewModel.SelectedIncidentTimeHours != "--")
                    IncidentDate = IncidentDate.AddMinutes(Convert.ToDouble(incidentViewModel.SelectedIncidentTimeMinutes));

                return PartialView("_Policy", SingleClaimRepository.PopulatePolicyViewModel(IncidentDate.ToShortDateString()+ "|" + IncidentLocation));

            }
            else
                return PartialView("_Incident", SingleClaimRepository.PopulateIncidentViewModel(""));
        }

    }
}