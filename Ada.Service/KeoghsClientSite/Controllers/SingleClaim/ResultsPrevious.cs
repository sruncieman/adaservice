﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult ResultsPrevious()
        {
            return PartialView("_UploadProgress");
        }
    }
}