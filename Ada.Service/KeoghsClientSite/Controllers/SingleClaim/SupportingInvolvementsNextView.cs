﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult SupportingInvolvementsNextView(SupportingInvolvementsViewModel supportingInvolvementsViewModel)
        {
            //SingleClaimRepository.StoreSupportingInvolvementsViewModel(supportingInvolvementsViewModel, SupportingInvolvementsType.InsuredDriver);

            SupportingInvolvementStore(supportingInvolvementsViewModel);

            SingleClaimRepository.Save();

            IncidentViewModel incidentViewModel = new IncidentViewModel();

            if (System.Web.HttpContext.Current.Session["incidentViewModel"] != null)
                incidentViewModel = System.Web.HttpContext.Current.Session["incidentViewModel"] as IncidentViewModel;

            DateTime incidentDate;

            incidentDate = incidentViewModel.IncidentDate ?? DateTime.MinValue;

            if (supportingInvolvementsViewModel.Vehicle.HireStartDate < incidentDate)
            {
                ModelState.AddModelError("", "Hire start date must be after incident date");
            }

            if (supportingInvolvementsViewModel.Vehicle.HireEndDate < incidentDate)
            {
                ModelState.AddModelError("", "Hire end date must be after incident date");
            }

            int ParentID = -1;

            if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.InsuredPassenger ||
                supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.ThirdPartyDriver ||
                supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.ThirdPartyPassenger)
                ParentID = supportingInvolvementsViewModel.ParentID;

            if (!ModelState.IsValid)
                return PartialView("_SupportingInvolvements",
                            SingleClaimRepository.PopulateSupportingInvolvementsViewModel(supportingInvolvementsViewModel.NumberSupportingInvolvements,
                                                                                        supportingInvolvementsViewModel.CurrentSupportingInvolvement,
                                                                                        supportingInvolvementsViewModel.SupportingInvolvementsType,
                                                                                        ParentID, supportingInvolvementsViewModel.VehicleID));

            else if (ModelState.IsValid && supportingInvolvementsViewModel.CurrentSupportingInvolvement < supportingInvolvementsViewModel.NumberSupportingInvolvements)
            {   // Further Supporting Involvements to display
                ModelState.Clear();

                SupportingInvolvementsViewModel tmpSupportingInvolvementsViewModel =
                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(supportingInvolvementsViewModel.NumberSupportingInvolvements,
                                                                                supportingInvolvementsViewModel.CurrentSupportingInvolvement + 1,
                                                                                supportingInvolvementsViewModel.SupportingInvolvementsType,
                                                                                ParentID, supportingInvolvementsViewModel.VehicleID,
                                                                                supportingInvolvementsViewModel.OrganisationInvolvement.OrganisationName);

                return PartialView("_SupportingInvolvements", tmpSupportingInvolvementsViewModel);
            }
            else
            {
                ModelState.Clear();
                string NavigationData;

                NavigationData = supportingInvolvementsViewModel.OrganisationInvolvement.OrganisationName + "|" + supportingInvolvementsViewModel.CurrentSupportingInvolvement;

                NavigationData += "|" + ParentID;
                NavigationData += "|" + supportingInvolvementsViewModel.VehicleID;

                // Are there any passengers to display?
                //if (SingleClaimRepository.InsuredVehicleNumberPassengers() > 0)
                if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.InsuredDriver)
                {


                    if (SingleClaimRepository.InsuredVehicleNumberPassengers() > 0)
                    {
                        return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(1,
                                                                                            CurrentView.SupportingInvolvementInsuredDriver,
                                                                                            supportingInvolvementsViewModel.CurrentSupportingInvolvement,
                                                                                            PassengerType.InsuredVehiclePassenger, -1,
                                                                                            NavigationData));
                    }
                    else if (SingleClaimRepository.NumberOfThirdPartyVehicles() > 0)
                    {
                        //display Third Party Vehicle
                        return PartialView("_ThirdPartyVehicle", SingleClaimRepository.PopulateThirdPartyVehicleViewModel(1,
                                                                                                    CurrentView.SupportingInvolvementInsuredDriver,
                                                                                                    NavigationData));

                    }
                    else if (SingleClaimRepository.NumberOfWitnesses() > 0)
                    {
                        return PartialView("_Witness", SingleClaimRepository.PopulateWitnessViewModel(1, CurrentView.SupportingInvolvementInsuredDriver,
                                                                                                    NavigationData));
                    }
                    else
                    {
                        

                        return PartialView("_ReviewAndSubmit", SingleClaimRepository.PopulateReviewAndSubmitViewModel(SingleClaimRepository.NumberOfThirdPartyVehicles(),
                                                                                                                     supportingInvolvementsViewModel.VehicleID,
                                                                                                                     CurrentView.SupportingInvolvementInsuredDriver,
                                                                                                                     NavigationData));
                    }

                }
                else if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.InsuredPassenger)
                {
                    // Need to append last passenger ID to Nav Data, required for determining which insured passenger to add supp involvement data to
                    // NavigationData += "|" + SingleClaimRepository.InsuredVehicleNumberPassengers();
                    //  NavigationData += "|" + ParentID;

                    //display Third Party Vehicle
                    // Any more passengers?
                    if (supportingInvolvementsViewModel.ParentID < SingleClaimRepository.InsuredVehicleNumberPassengers())
                    {
                        return PartialView("_Passenger",
                            SingleClaimRepository.PopulatePassengerViewModel(supportingInvolvementsViewModel.ParentID + 1,
                                                                                CurrentView.SupportingInvolvementInsuredPassenger,
                                                                                supportingInvolvementsViewModel.CurrentSupportingInvolvement,
                                                                                PassengerType.InsuredVehiclePassenger, -1,
                                                                                NavigationData));
                    }
                    else if (SingleClaimRepository.NumberOfThirdPartyVehicles() > 0)
                    {
                        //Display Third Party Vehicle

                        return PartialView("_ThirdPartyVehicle", SingleClaimRepository.PopulateThirdPartyVehicleViewModel(1,
                                                                                                        CurrentView.SupportingInvolvementInsuredPassenger,
                                                                                                        NavigationData));
                    }
                    else if (SingleClaimRepository.NumberOfWitnesses() > 0)
                    {
                        return PartialView("_Witness", SingleClaimRepository.PopulateWitnessViewModel(1, CurrentView.SupportingInvolvementInsuredPassenger,
                                                                                                    NavigationData));
                    }
                    else
                    {
                        return PartialView("_ReviewAndSubmit", SingleClaimRepository.PopulateReviewAndSubmitViewModel(SingleClaimRepository.NumberOfThirdPartyVehicles(),
                                                                                                                     supportingInvolvementsViewModel.VehicleID,
                                                                                                                     CurrentView.SupportingInvolvementInsuredPassenger,
                                                                                                                     NavigationData));
                    }

                }
                else if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.ThirdPartyDriver)
                {
                    // 
                    int NumThirdPartyVehicles = 0;
                    NumThirdPartyVehicles = SingleClaimRepository.NumberOfThirdPartyVehicles();

                    if (SingleClaimRepository.ThirdPartyVehicleNumberPassengers(supportingInvolvementsViewModel.VehicleID) > 0)
                        return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(1,
                                                                                                CurrentView.SupportingInvolvementThirdPartyDriver,
                                                                                                supportingInvolvementsViewModel.CurrentSupportingInvolvement,
                                                                                                PassengerType.ThirdPartyVehiclePassenger, supportingInvolvementsViewModel.VehicleID,
                                                                                                NavigationData));
                    else if (NumThirdPartyVehicles > 1 && supportingInvolvementsViewModel.VehicleID < NumThirdPartyVehicles)
                        return PartialView("_ThirdPartyVehicle", SingleClaimRepository.PopulateThirdPartyVehicleViewModel(supportingInvolvementsViewModel.VehicleID + 1,
                                                                                                    CurrentView.SupportingInvolvementThirdPartyDriver,
                                                                                                    NavigationData));
                    else if (SingleClaimRepository.NumberOfWitnesses() > 0)
                    {
                        return PartialView("_Witness", SingleClaimRepository.PopulateWitnessViewModel(1, CurrentView.SupportingInvolvementThirdPartyDriver,
                                                                                                        NavigationData));
                    }
                    else
                    {

                        

                        return PartialView("_ReviewAndSubmit", SingleClaimRepository.PopulateReviewAndSubmitViewModel(SingleClaimRepository.NumberOfThirdPartyVehicles(),
                                                                                                                     supportingInvolvementsViewModel.VehicleID,
                                                                                                                     CurrentView.SupportingInvolvementThirdPartyDriver,
                                                                                                                     NavigationData));
                    }
                }
                else if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.ThirdPartyPassenger)
                {
                    // Any more passengers?
                    // Any more third party vehicles?
                    int NumThirdPartyVehicles = 0;
                    NumThirdPartyVehicles = SingleClaimRepository.NumberOfThirdPartyVehicles();

                    if (supportingInvolvementsViewModel.ParentID < SingleClaimRepository.ThirdPartyVehicleNumberPassengers(supportingInvolvementsViewModel.VehicleID))
                    {
                        return PartialView("_Passenger",
                            SingleClaimRepository.PopulatePassengerViewModel(supportingInvolvementsViewModel.ParentID + 1,
                                                                                CurrentView.SupportingInvolvementThirdPartyPassenger,
                                                                                supportingInvolvementsViewModel.CurrentSupportingInvolvement,
                                                                                PassengerType.ThirdPartyVehiclePassenger, supportingInvolvementsViewModel.VehicleID,
                                                                                NavigationData));
                    }
                    //else if (SingleClaimRepository.NumberOfThirdPartyVehicles() > supportingInvolvementsViewModel.VehicleID)
                    else if (NumThirdPartyVehicles > 1 && supportingInvolvementsViewModel.VehicleID < NumThirdPartyVehicles)
                    {
                        return PartialView("_ThirdPartyVehicle", SingleClaimRepository.PopulateThirdPartyVehicleViewModel(supportingInvolvementsViewModel.VehicleID + 1,
                                                                                                            CurrentView.SupportingInvolvementThirdPartyPassenger,
                                                                                                            NavigationData));
                    }
                    else if (SingleClaimRepository.NumberOfWitnesses() > 0)
                    {
                        return PartialView("_Witness", SingleClaimRepository.PopulateWitnessViewModel(1, CurrentView.SupportingInvolvementThirdPartyPassenger,
                                                                                                   NavigationData));
                    }
                    else
                    {
                        return PartialView("_ReviewAndSubmit", SingleClaimRepository.PopulateReviewAndSubmitViewModel(SingleClaimRepository.NumberOfThirdPartyVehicles(),
                                                                                                                     supportingInvolvementsViewModel.VehicleID,
                                                                                                                     CurrentView.SupportingInvolvementThirdPartyPassenger,
                                                                                                                     NavigationData));

                    }
                }
                else
                    return null;

            }
            // return null;
        }

        private static void SupportingInvolvementStore(SupportingInvolvementsViewModel supportingInvolvementsViewModel)
        {
            if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.InsuredDriver)
                SingleClaimRepository.StoreSupportingInvolvementsViewModel(supportingInvolvementsViewModel,
                                                                           supportingInvolvementsViewModel.
                                                                               SupportingInvolvementsType);
            else if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.InsuredPassenger)
                SingleClaimRepository.StoreSupportingInvolvementsViewModel(supportingInvolvementsViewModel,
                                                                           supportingInvolvementsViewModel.
                                                                               SupportingInvolvementsType,
                                                                           supportingInvolvementsViewModel.ParentID);
            else if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.ThirdPartyDriver)
                SingleClaimRepository.StoreSupportingInvolvementsViewModel(supportingInvolvementsViewModel,
                                                                           supportingInvolvementsViewModel.
                                                                               SupportingInvolvementsType,
                                                                           supportingInvolvementsViewModel.ParentID,
                                                                           supportingInvolvementsViewModel.VehicleID);
            else if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.ThirdPartyPassenger)
                SingleClaimRepository.StoreSupportingInvolvementsViewModel(supportingInvolvementsViewModel,
                                                                           supportingInvolvementsViewModel.
                                                                               SupportingInvolvementsType,
                                                                           supportingInvolvementsViewModel.ParentID,
                                                                           supportingInvolvementsViewModel.VehicleID);
        }


        //public PartialViewResult SubmitSupportingInvolvementsView(SupportingInvolvementsViewModel supportingInvolvementsViewModel)
        //{
        //    SupportingInvolvementStore(supportingInvolvementsViewModel);
        //    SingleClaimRepository.Save();

        //    int ParentID = -1;

        //    if (supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.InsuredPassenger ||
        //        supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.ThirdPartyDriver ||
        //        supportingInvolvementsViewModel.SupportingInvolvementsType == SupportingInvolvementsType.ThirdPartyPassenger)
        //        ParentID = supportingInvolvementsViewModel.ParentID;

            

        //    return PartialView("_SupportingInvolvements",
        //                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(supportingInvolvementsViewModel.NumberSupportingInvolvements,
        //                                                                                supportingInvolvementsViewModel.CurrentSupportingInvolvement,
        //                                                                                supportingInvolvementsViewModel.SupportingInvolvementsType,
        //                                                                                ParentID, supportingInvolvementsViewModel.VehicleID));

        //}
    }
}