﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult WitnessNextView(WitnessViewModel witnessViewModel)
        {
            SingleClaimRepository.StoreWitnessViewModel(witnessViewModel, witnessViewModel.CurrentWitness);
            SingleClaimRepository.Save();

            int NumThirdPartyVehicles = 0;
            NumThirdPartyVehicles = SingleClaimRepository.NumberOfThirdPartyVehicles();

            if (!ModelState.IsValid)
                return PartialView("_Witness",
                    SingleClaimRepository.PopulateWitnessViewModel(witnessViewModel.CurrentWitness,
                                                                            CurrentView.Witness));
            else
            {
                ModelState.Clear();
                string NavigationData = SingleClaimRepository.FormatPersonName(witnessViewModel.PersonInformation);
                NavigationData += "|" + SingleClaimRepository.GetWitnessInvolvementTypeFromID(witnessViewModel.PersonInformation.SelectedWitnessInvolvementType);

                // any more witnesses?
                if (SingleClaimRepository.NumberOfWitnesses() > witnessViewModel.CurrentWitness)
                {
                    return PartialView("_Witness",
                    SingleClaimRepository.PopulateWitnessViewModel(witnessViewModel.CurrentWitness + 1,
                                                                            CurrentView.Witness, NavigationData));
                }
                else
                {
                    // now display final review screen
                    ModelState.Clear();
                    //Pass WitnessID back to client
                    NavigationData += "|" + witnessViewModel.CurrentWitness;
                    return PartialView("_ReviewAndSubmit", SingleClaimRepository.PopulateReviewAndSubmitViewModel(NumThirdPartyVehicles,
                                                                                                                    NumThirdPartyVehicles,
                                                                                                                    CurrentView.Witness, NavigationData));
                }
            }
        }
    }
}