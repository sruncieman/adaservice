﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {

        [AjaxAuthorize]
        public PartialViewResult ReviewAndSubmitPreviousView(ReviewAndSubmitViewModel reviewAndSubmitViewModel)
        {

            SingleClaimRepository.StoreReviewAndSubmitViewModel(reviewAndSubmitViewModel);
            // 
            //if(reviewAndSubmitViewModel.PreviousPage==
            //return PartialView("_ThirdPartyVehicle", SingleClaimRepository.PopulateThirdPartyVehicleViewModel(reviewAndSubmitViewModel.VehicleID, CurrentView.Unknown));
            ModelState.Clear();

            if (reviewAndSubmitViewModel.PreviousPage == CurrentView.InsuredDriver)
            {
                return PartialView("_InsuredDriver", SingleClaimRepository.PopulateInsuredDriverViewModel("", true, CurrentView.ReviewAndSubmit.ToString()));
            }
            else if (reviewAndSubmitViewModel.PreviousPage == CurrentView.ThirdPartyDriver)
            {
                // First Passenger --> Go back to previous page
                return PartialView("_ThirdPartyDriver", SingleClaimRepository.PopulateThirdPartyDriverViewModel(reviewAndSubmitViewModel.VehicleID, CurrentView.Unknown,
                                                                                                                "", true, CurrentView.ReviewAndSubmit.ToString()));
            }
            else if (reviewAndSubmitViewModel.PreviousPage == CurrentView.Passenger)
            {
                // Go back to Passenger
                if (SingleClaimRepository.NumberOfThirdPartyVehicles() > 0)
                    return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(SingleClaimRepository.ThirdPartyVehicleNumberPassengers(reviewAndSubmitViewModel.VehicleID),
                                                                                                     CurrentView.Passenger, -1,
                                                                                                     PassengerType.ThirdPartyVehiclePassenger, reviewAndSubmitViewModel.VehicleID,
                                                                                                     "", true, CurrentView.ReviewAndSubmit.ToString()));
                else
                    return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(SingleClaimRepository.InsuredVehicleNumberPassengers(),
                                                                                                     CurrentView.Passenger, -1,
                                                                                                     PassengerType.InsuredVehiclePassenger, reviewAndSubmitViewModel.VehicleID,
                                                                                                     "", true, CurrentView.ReviewAndSubmit.ToString()));
            }
            else if (reviewAndSubmitViewModel.PreviousPage == CurrentView.SupportingInvolvementInsuredPassenger)
            {

                int NumSupportingInvolvements = 0;
                int NumPassengers = 0;
                NumPassengers = SingleClaimRepository.InsuredVehicleNumberPassengers();

                NumSupportingInvolvements = SingleClaimRepository.GetNumberofSupportingInvolvementsForPassenger(NumPassengers);

                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(NumSupportingInvolvements,
                                                                                                    NumSupportingInvolvements,
                                                                                                    SupportingInvolvementsType.InsuredPassenger,
                                                                                                    NumPassengers, -1, "", true, CurrentView.ReviewAndSubmit.ToString()));
            }
            else if (reviewAndSubmitViewModel.PreviousPage == CurrentView.SupportingInvolvementInsuredDriver)
            {
                // Go back to last supporting involvement of Insured Driver 
                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                                                                SingleClaimRepository.GetNumberofSupportingInvolvementsForInsuredDriver(),
                                                                                SupportingInvolvementsType.InsuredDriver, -1, -1, "", true, CurrentView.ReviewAndSubmit.ToString()));
            }
            else if (reviewAndSubmitViewModel.PreviousPage == CurrentView.SupportingInvolvementThirdPartyDriver)
            {

                // Go back to last supporting involvement of Third Party Driver
                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                                                                SingleClaimRepository.GetNumberofSupportingInvolvementsForThirdPartyDriver(reviewAndSubmitViewModel.VehicleID),
                                                                                SupportingInvolvementsType.ThirdPartyDriver, reviewAndSubmitViewModel.VehicleID, -1, "", true, CurrentView.ReviewAndSubmit.ToString()));
            }
            else if (reviewAndSubmitViewModel.PreviousPage == CurrentView.SupportingInvolvementThirdPartyPassenger)
            {
                // Go back to last supporting involvement of Third Party Passenger
                int NumPassengers = 0;
                NumPassengers = SingleClaimRepository.ThirdPartyVehicleNumberPassengers(reviewAndSubmitViewModel.VehicleID);

                return PartialView("_SupportingInvolvements",
                                   SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                                                               SingleClaimRepository.GetNumberofSupportingInvolvementsForThirdPartyPassenger(reviewAndSubmitViewModel.VehicleID, NumPassengers, true),
                                                                               SupportingInvolvementsType.ThirdPartyPassenger, NumPassengers, reviewAndSubmitViewModel.VehicleID,
                                                                               "", true, CurrentView.ReviewAndSubmit.ToString()));
            }
            else if (reviewAndSubmitViewModel.PreviousPage == CurrentView.Witness)
            {
                return PartialView("_Witness",
                  SingleClaimRepository.PopulateWitnessViewModel(SingleClaimRepository.NumberOfWitnesses(),
                                                                          CurrentView.Unknown, "", true));
            }
            else
                return null;
        }
    }
}