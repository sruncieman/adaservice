﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult ThirdPartyVehicleNextView(ThirdPartyVehicleViewModel thirdPartyVehicleViewModel)
        {
            SingleClaimRepository.StoreThirdPartyVehicleViewModel(thirdPartyVehicleViewModel, thirdPartyVehicleViewModel.CurrentVehicle);
            SingleClaimRepository.Save();

            SingleClaimRepository.CheckThirdPartyPassengers(thirdPartyVehicleViewModel);

            if (!ModelState.IsValid)
                return PartialView("_ThirdPartyVehicle", SingleClaimRepository.PopulateThirdPartyVehicleViewModel(thirdPartyVehicleViewModel.CurrentVehicle,
                                                                                                                    thirdPartyVehicleViewModel.PreviousPage));
            else
            {
                ModelState.Clear();
                string NavigationData = thirdPartyVehicleViewModel.Vehicle.RegistrationNumber + "|" +
                                        thirdPartyVehicleViewModel.Vehicle.Make + "|" +
                                        thirdPartyVehicleViewModel.Vehicle.Model;
                return PartialView("_ThirdPartyDriver", SingleClaimRepository.PopulateThirdPartyDriverViewModel(thirdPartyVehicleViewModel.CurrentVehicle,
                                                                                                                CurrentView.ThirdPartyVehicle,
                                                                                                                NavigationData));
            }

        }
    }
}