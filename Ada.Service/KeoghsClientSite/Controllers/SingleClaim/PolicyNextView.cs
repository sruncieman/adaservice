﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult PolicyNextView(PolicyViewModel policyViewModel)
        {
            //Store selected values in session for now
            SingleClaimRepository.StorePolicyViewModel(policyViewModel);
            SingleClaimRepository.Save();

            IncidentViewModel incidentViewModel = new IncidentViewModel();

            if (System.Web.HttpContext.Current.Session["incidentViewModel"] != null)
                incidentViewModel = System.Web.HttpContext.Current.Session["incidentViewModel"] as IncidentViewModel;

            DateTime incidentDate;

            incidentDate = incidentViewModel.IncidentDate ?? DateTime.MinValue;

            if (policyViewModel.PolicyStartDate > incidentDate)
            {
                ModelState.AddModelError("","Policy start date must be before incident date");
            }

            if (policyViewModel.PolicyEndDate < incidentDate)
            {
                ModelState.AddModelError("", "Policy end date must be after incident date");
            }

            //
            if (ModelState.IsValid)
            {
                if (!(bool)policyViewModel.PolicyHolderDetailsKnown)
                {
                    // populate data for Policy in navbar
                    InsuredVehicleViewModel insuredVehicleViewModel =
                        SingleClaimRepository.PopulateInsuredVehicleViewModel(CurrentView.Policy, policyViewModel.PolicyNumber + "|" + SingleClaimRepository.GetPolicyTypeFromID(policyViewModel.SelectedPolicyType));

                    insuredVehicleViewModel.Vehicle.PolicyDetailsKnown = false;
                    return PartialView("_InsuredVehicle", insuredVehicleViewModel);
                }
                else if (SingleClaimRepository.GetPolicyTypeFromID(policyViewModel.SelectedPolicyType) == "Personal")
                {
                    InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();
                    if (Session["insuredVehicleViewModel"] != null)
                        insuredVehicleViewModel = Session["insuredVehicleViewModel"] as InsuredVehicleViewModel;

                    insuredVehicleViewModel.Vehicle.PolicyDetailsKnown = true;
                    
                    SingleClaimRepository.StoreInsuredVehicleViewModel(insuredVehicleViewModel);
                    return PartialView("_PolicyHolderPersonal", SingleClaimRepository.PopulatePolicyHolderPersonalViewModel(policyViewModel.PolicyNumber + "|" + SingleClaimRepository.GetPolicyTypeFromID(policyViewModel.SelectedPolicyType)));

                }
                else if (SingleClaimRepository.GetPolicyTypeFromID(policyViewModel.SelectedPolicyType) == "Commercial")
                {
                    InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();
                    if (Session["insuredVehicleViewModel"] != null)
                        insuredVehicleViewModel = Session["insuredVehicleViewModel"] as InsuredVehicleViewModel;

                    insuredVehicleViewModel.Vehicle.PolicyDetailsKnown = true;

                    SingleClaimRepository.StoreInsuredVehicleViewModel(insuredVehicleViewModel);
                    return PartialView("_PolicyHolderCommercial", SingleClaimRepository.PopulatePolicyHolderCommercialViewModel( policyViewModel.PolicyNumber + "|" + SingleClaimRepository.GetPolicyTypeFromID(policyViewModel.SelectedPolicyType)));
                }
                else
                    return null;
            }
            else
                return PartialView("_Policy", SingleClaimRepository.PopulatePolicyViewModel(""));

        }
    }
}