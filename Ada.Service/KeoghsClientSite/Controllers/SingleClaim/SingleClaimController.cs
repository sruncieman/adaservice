﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;
using MDA.WCF.WebServices.Messages;
using MDA.Common.FileModel;
using MDA.Common.Enum;

using vehicleAlias = KeoghsClientSite.Models.SingleClaim.Vehicle;
using System.Collections.ObjectModel;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    [NoCache]
    [RedirectingAction]
    public partial class SingleClaimController : Controller
    {

        //public JsonResult GetSessionValue(string key, string passengerID, string vehicleID)
        //{
        //    JsonResult json = null;
        //    int currentPassengerID = 0;
        //    int currentVehicleID = 0;

        //    currentPassengerID = Convert.ToInt32(passengerID);
        //    currentVehicleID = Convert.ToInt32(vehicleID);

            

        //    switch (key)
        //    {
        //        case "InsuredDriverPassengers":
        //            json = Json(Session[key]);
        //            break;


        //        case "DictionaryThirdPartyVehicleSupportingInvolvements":
        //            var sessionTPData = Json(Session[key]).Data;
        //            if (sessionTPData != null)
        //            {
        //                var DictionaryThirdPartyVehicleSupportingInvolvements = (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)sessionTPData;

        //                if (DictionaryThirdPartyVehicleSupportingInvolvements != null && DictionaryThirdPartyVehicleSupportingInvolvements.Any())
        //                {

        //                    if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(currentVehicleID))
        //                    {
        //                        if (DictionaryThirdPartyVehicleSupportingInvolvements[currentVehicleID].Count() >= currentPassengerID)
        //                            json = Json(DictionaryThirdPartyVehicleSupportingInvolvements[currentVehicleID].Select(x => x.Value));
        //                        //if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(currentPassengerID))
                                    
        //                    }
        //                }
        //            }
        //            break;

        //        case "DictionaryPassengerSupportingInvolvements":
        //            var sessionData = Json(Session[key]).Data;
        //            if (sessionData != null)
        //            {
        //                var DictionaryPassengerSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)sessionData;
        //                if(DictionaryPassengerSupportingInvolvements.Count() >= currentPassengerID) 
        //                    json = Json(DictionaryPassengerSupportingInvolvements.Select(x=>x.Value));

        //            }
        //            break;

        //    }

        //    return json;
        //}      
            

        [Authorize]
        public ActionResult SingleClaimInput(string FormToDisplay)
        {
            int totalOutstanding = 0;

            if (ConfigurationManager.AppSettings["ShowSingleClaim"] == "true")
            {
                totalOutstanding = SingleClaimRepository.GetTotalSingleClaimsForClient();
                ViewData["OutstandingClaimsCount"] = totalOutstanding;
                //ViewBag.OutstandingClaims = totalOutstanding;
                ViewBag.FormToDisplay = FormToDisplay;
                
                SingleClaimRepository.ClearSingleClaim();

                return View();
            }
            else
                return View("UnderDevelopment");
        }

        [Authorize]
        public ActionResult SingleClaimInputFromEditClaim()
        {
            int totalOutstanding = 0;

            if (ConfigurationManager.AppSettings["ShowSingleClaim"] == "true")
            {
                totalOutstanding = SingleClaimRepository.GetTotalSingleClaimsForClient();
                ViewData["OutstandingClaimsCount"] = totalOutstanding;
              
                return View("SingleClaimInput");
            }
            else
                return View("UnderDevelopment");
        }
        


        [Authorize]
        public ActionResult Save()
        {
            //SingleClaimRepository.StoreIncidentViewModel(incidentViewModel);
            
            SingleClaimRepository.Save();

            return RedirectToAction("OutstandingClaims");
        }

        public ActionResult EditSavedClaim(int id)
        {
            // Fetch Saved claim, translate to Single Claim model and populate sessions.
            WebsiteFetchSingleClaimResponse response = SingleClaimRepository.FetchSavedSingleClaim(id);

            MotorClaim savedClaim = new MotorClaim();

            savedClaim = response.SingleClaimDetail.Claim;
            
            SingleClaimModel singleClaimModel = new SingleClaimModel();

            ClaimViewModel claimViewModel = new ClaimViewModel();
            IncidentViewModel incidentViewModel = new IncidentViewModel();
            PolicyViewModel policyViewModel = new PolicyViewModel();
            PolicyHolderCommercialViewModel policyHolderCommercialViewModel = new PolicyHolderCommercialViewModel();
            PolicyHolderPersonalViewModel policyHolderPersonalViewModel = new PolicyHolderPersonalViewModel();
            InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();
            VehicleAdditionalInfo vehicleAdditionalInfo = new VehicleAdditionalInfo();
            InsuredDriverPerson insuredDriverPerson = new InsuredDriverPerson();
            InsuredDriverAddress insuredDriverAddress = new InsuredDriverAddress();
            InsuredDriverViewModel insuredDriverViewModel = new InsuredDriverViewModel();
            //ThirdPartyVehicleViewModel thirdPartyVehicleViewModel = new ThirdPartyVehicleViewModel();
            ThirdPartyDriverViewModel thirdPartyDriverViewModel = new ThirdPartyDriverViewModel();
            
            List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
            
            //List<SupportingInvolvementsViewModel> thirdPartyDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
            //List<SupportingInvolvementsViewModel> thirdPartyDriverPassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

            Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>> DictionaryThirdPartyVehicleSupportingInvolvements = new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
            Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                              


            Dictionary<int, List<SupportingInvolvementsViewModel>> dictionaryPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();



            List<PassengerViewModel> insuredDriverPassengers = new List<PassengerViewModel>();
            Dictionary<int, List<PassengerViewModel>> dictionaryThirdPartyPassengers = new Dictionary<int, List<PassengerViewModel>>();
            
            //List<ThirdPartyVehicleViewModel> thirdPartyVehicles = new List<ThirdPartyVehicleViewModel>();
            Dictionary<int, ThirdPartyVehicleViewModel> thirdPartyVehicles = new Dictionary<int, ThirdPartyVehicleViewModel>();
            Dictionary<int, ThirdPartyDriverViewModel> thirdPartyDriverViewModels = new Dictionary<int, ThirdPartyDriverViewModel>();
            Dictionary<int, WitnessViewModel> witnessViewModels = new Dictionary<int, WitnessViewModel>();

            //Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyDriverSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

            System.Web.HttpContext.Current.Session["claimViewModel"] = claimViewModel;
            System.Web.HttpContext.Current.Session["incidentViewModel"] = incidentViewModel;
            System.Web.HttpContext.Current.Session["policyViewModel"] = policyViewModel;
            System.Web.HttpContext.Current.Session["policyHolderCommercialViewModel"] = policyHolderCommercialViewModel;
            System.Web.HttpContext.Current.Session["policyHolderPersonalViewModel"] = policyHolderPersonalViewModel;
            System.Web.HttpContext.Current.Session["insuredVehicleViewModel"] = insuredVehicleViewModel;
            System.Web.HttpContext.Current.Session["vehicleAdditionalInfo"] = vehicleAdditionalInfo;
            System.Web.HttpContext.Current.Session["insuredDriverPerson"] = insuredDriverPerson;
            System.Web.HttpContext.Current.Session["insuredDriverAddress"] = insuredDriverAddress;
            System.Web.HttpContext.Current.Session["insuredDriverViewModel"] = insuredDriverViewModel;
            //System.Web.HttpContext.Current.Session["thirdPartyVehicleViewModel"] = thirdPartyVehicleViewModel;
            System.Web.HttpContext.Current.Session["thirdPartyDriverViewModel"] = thirdPartyDriverViewModel;
            
            System.Web.HttpContext.Current.Session["InsuredDriverSupportingInvolvements"] = insuredDriverSupportingInvolvements;


            System.Web.HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;
            


            #region Incident

            claimViewModel.SelectedClaimTypeID = (int)savedClaim.ClaimType;
            claimViewModel.ClaimNumber = savedClaim.ClaimNumber;
            

            claimViewModel.SelectedClaimStatusID = 1;
                   
    
            incidentViewModel.IncidentDate = savedClaim.IncidentDate;

            incidentViewModel.SelectedIncidentTimeHours = savedClaim.IncidentDate.ToString("HH");

            incidentViewModel.SelectedIncidentTimeMinutes = savedClaim.IncidentDate.ToString("mm");

            string incidentInfo = savedClaim.MotorClaimInfo.IncidentLocation;

            string[] incidentDetails = incidentInfo.Split('|');

            incidentViewModel.Location = incidentDetails[0];

            int selectedNumberOfvehicles = Convert.ToInt32(incidentDetails[1]);
            int selectedNumberOfWitnesses = Convert.ToInt32(incidentDetails[2]);

            incidentViewModel.SelectedNumberOfVehicles = selectedNumberOfvehicles;
            incidentViewModel.SelectedNumberOfWitnesses = selectedNumberOfWitnesses;

            incidentViewModel.Circumstances = savedClaim.MotorClaimInfo.IncidentCircumstances;


            #region Policy

            if (!string.IsNullOrEmpty(savedClaim.Policy.Broker))
            {
                policyViewModel.Broker = savedClaim.Policy.Broker;
            }

            switch (savedClaim.Policy.CoverType)
            {
                case PolicyCoverType.Comprehensive:
                    policyViewModel.SelectedCoverType = 0;
                    break;
                case PolicyCoverType.ThirdPartyOnly:
                    policyViewModel.SelectedCoverType = 1;
                    break;
                case PolicyCoverType.ThirdPartyFireAndTheft:
                    policyViewModel.SelectedCoverType = 2;
                    break;
            }


            string claimCode = savedClaim.MotorClaimInfo.ClaimCode;

            string[] values = claimCode.Split('|');

            switch (values[3])
            {
                case "False":
                    policyViewModel.PolicyHolderDetailsKnown = false;
                    break;
                case "True":
                    policyViewModel.PolicyHolderDetailsKnown = true;
                    break;
            }

            //policyViewModel.PolicyHolderDetailsKnown = values[3];

            policyViewModel.PolicyEndDate = savedClaim.Policy.PolicyEndDate;

            if (!string.IsNullOrEmpty(savedClaim.Policy.PolicyNumber))
            {
                policyViewModel.PolicyNumber = savedClaim.Policy.PolicyNumber;
            }

            policyViewModel.PolicyStartDate = savedClaim.Policy.PolicyStartDate;

            switch (savedClaim.Policy.PolicyType)
            {
                case PolicyType.CommercialMotor:
                    policyViewModel.SelectedPolicyType = 0;
                    break;
                case PolicyType.PersonalMotor:
                    policyViewModel.SelectedPolicyType = 1;
                    break;
                case PolicyType.Unknown:
                    policyViewModel.SelectedPolicyType = 2;
                    break;
            }

            if (savedClaim.Policy.Premium > 0)
            {
                policyViewModel.PolicyPremium = savedClaim.Policy.Premium;
            }

            //savedClaim.Policy.TradingName

            #endregion

            #region Motor Claim Info

            incidentViewModel.NotificationDate = savedClaim.MotorClaimInfo.ClaimNotificationDate;


            if (values[0] != null)
            {

                switch (values[0])
                {
                    case "Insured damage only":
                        incidentViewModel.SelectedClaimCode = "0";
                        break;
                    case "TP damage only":
                        incidentViewModel.SelectedClaimCode = "1";
                        break;
                    case "Damage & PI":
                        incidentViewModel.SelectedClaimCode = "2";
                        break;
                }

            }



            incidentViewModel.TotalValue = savedClaim.MotorClaimInfo.Reserve;

            incidentViewModel.TotalPayments = savedClaim.MotorClaimInfo.PaymentsToDate;

            if (values[1] != null)
            {
                incidentViewModel.SelectedLiabilityDecision = values[1];
            }

            if (values[2] != null)
            {
                incidentViewModel.SelectedMojProcessStage = values[2];
            }

            if (savedClaim.MotorClaimInfo.AmbulanceAttended != null)
            {
                switch (savedClaim.MotorClaimInfo.AmbulanceAttended)
                {
                    case true:
                        incidentViewModel.SelectedAmbulanceAttend = 0;
                        break;
                    case false:
                        incidentViewModel.SelectedAmbulanceAttend = 1;
                        break;
                }
            }

            if (savedClaim.MotorClaimInfo.PoliceAttended != null)
            {
                switch (savedClaim.MotorClaimInfo.PoliceAttended)
                {
                    case true:
                        incidentViewModel.SelectedPoliceAttend = 0;
                        break;
                    case false:
                        incidentViewModel.SelectedPoliceAttend = 1;
                        break;
                }
            }

            if (!string.IsNullOrEmpty(savedClaim.MotorClaimInfo.PoliceForce))
                incidentViewModel.PoliceForce = savedClaim.MotorClaimInfo.PoliceForce;

            if (!string.IsNullOrEmpty(savedClaim.MotorClaimInfo.PoliceReference))
                incidentViewModel.PoliceReference = savedClaim.MotorClaimInfo.PoliceReference;

            #endregion

            #region Claim Organisations

            foreach (var claimOrg in savedClaim.Organisations)
            {

                if (claimOrg.OrganisationType == OrganisationType.PolicyHolder)
                {
                    if (claimOrg.OrganisationName != null)
                    {
                        policyHolderCommercialViewModel.Organisation.Name = claimOrg.OrganisationName;
                    }

                    if (claimOrg.RegisteredNumber != null)
                    {
                        policyHolderCommercialViewModel.Organisation.RegisteredNumber = claimOrg.RegisteredNumber;
                    }

                    if (claimOrg.VatNumber != null)
                    {
                        policyHolderCommercialViewModel.OrganisationAdditionalInfo.VATNumber = claimOrg.VatNumber;
                    }

                    if (claimOrg.MojCrmNumber != null)
                    {
                        policyHolderCommercialViewModel.OrganisationAdditionalInfo.MoJClaimsRegNumber = claimOrg.MojCrmNumber;
                    }

                    if (claimOrg.Email != null)
                    {
                        policyHolderCommercialViewModel.OrganisationAdditionalInfo.EmailAddress = claimOrg.Email;
                    }

                    if (claimOrg.WebSite != null)
                    {
                        policyHolderCommercialViewModel.OrganisationAdditionalInfo.WebsiteAddress = claimOrg.WebSite;
                    }

                    if (claimOrg.Telephone1 != null)
                    {
                        policyHolderCommercialViewModel.OrganisationAdditionalInfo.Telephone1 = claimOrg.Telephone1;
                    }

                    if (claimOrg.Telephone2 != null)
                    {
                        policyHolderCommercialViewModel.OrganisationAdditionalInfo.Telephone2 = claimOrg.Telephone2;
                    }

                    if (claimOrg.Telephone3 != null)
                    {
                        policyHolderCommercialViewModel.OrganisationAdditionalInfo.Telephone3 = claimOrg.Telephone3;
                    }

                    #region Address

                    if (claimOrg.Addresses != null)
                    {
                        foreach (var claimAddress in claimOrg.Addresses)
                        {

                            if (claimAddress.AddressLinkType != null)
                            {
                                policyHolderCommercialViewModel.Address.SelectedAddressStatus = (int)claimAddress.AddressLinkType;
                            }

                            if (!string.IsNullOrEmpty(claimAddress.Building))
                            {
                                policyHolderCommercialViewModel.Address.BuildingName = claimAddress.Building;
                            }

                            if (!string.IsNullOrEmpty(claimAddress.BuildingNumber))
                            {
                                policyHolderCommercialViewModel.Address.BuildingNumber = claimAddress.BuildingNumber;
                            }

                            if (!string.IsNullOrEmpty(claimAddress.SubBuilding))
                            {
                                policyHolderCommercialViewModel.Address.SubBuilding = claimAddress.SubBuilding;
                            }

                            if (!string.IsNullOrEmpty(claimAddress.Street))
                            {
                                policyHolderCommercialViewModel.Address.Street = claimAddress.Street;
                            }

                            if (!string.IsNullOrEmpty(claimAddress.Town))
                            {
                                policyHolderCommercialViewModel.Address.Town = claimAddress.Town;
                            }

                            if (!string.IsNullOrEmpty(claimAddress.Locality))
                            {
                                policyHolderCommercialViewModel.Address.Locality = claimAddress.Locality;
                            }

                            if (!string.IsNullOrEmpty(claimAddress.PostCode))
                            {
                                policyHolderCommercialViewModel.Address.Postcode = claimAddress.PostCode;
                            }

                        }

                    }

                    #endregion

                    #region Vehicles

                    foreach (var vehicle in claimOrg.Vehicles)
                    {

                    }

                    #endregion

                    #region Bank Account

                    if (claimOrg.BankAccount.AccountNumber != null)
                    {
                        policyHolderCommercialViewModel.BankAccount.AccountNumber = claimOrg.BankAccount.AccountNumber;
                    }

                    if (claimOrg.BankAccount.SortCode != null)
                    {
                        policyHolderCommercialViewModel.BankAccount.SortCode = claimOrg.BankAccount.SortCode;
                    }

                    if (claimOrg.BankAccount.BankName != null)
                    {
                        policyHolderCommercialViewModel.BankAccount.BankName = claimOrg.BankAccount.BankName;
                    }

                    if (claimOrg.BankAccount.DatePaymentDetailsTaken != null)
                    {
                        policyHolderCommercialViewModel.BankAccount.DateDetailsTaken = claimOrg.BankAccount.DatePaymentDetailsTaken;
                    }

                    if (claimOrg.BankAccount.PolicyPaymentType != null)
                    {
                        policyHolderCommercialViewModel.BankAccount.SelectedAccountUsedFor = (int)claimOrg.BankAccount.PolicyPaymentType;
                    }

                    #endregion

                }

            }

            #endregion

            #region Vehicles

            int thirdPartyVehicleId = 1;
            int witnessId = 0;

            foreach (var vehicle in savedClaim.Vehicles)
            {
                #region People

                #region Policyholder

                foreach (var person in vehicle.People)
                {

                    if (person.PartyType == PartyType.Policyholder)
                    {
                        //if (person.Salutation != null)
                        //{
                        policyHolderPersonalViewModel.PersonInformation.SelectedSalutation = (int)person.Salutation;
                        //}

                        if (person.FirstName != null)
                        {
                            policyHolderPersonalViewModel.PersonInformation.Firstname = person.FirstName;
                        }

                        if (person.MiddleName != null)
                        {
                            policyHolderPersonalViewModel.PersonInformation.Middlename = person.MiddleName;
                        }

                        if (person.LastName != null)
                        {
                            policyHolderPersonalViewModel.PersonInformation.Surname = person.LastName;
                        }

                        if (person.DateOfBirth != null)
                        {
                            policyHolderPersonalViewModel.PersonInformation.DateOfBirth = person.DateOfBirth;
                        }

                        policyHolderPersonalViewModel.PersonInformation.SelectedGender = (int)person.Gender;

                        if (person.BankAccount != null)
                        {

                            if (person.BankAccount.AccountNumber != null)
                            {
                                policyHolderPersonalViewModel.BankAccount.AccountNumber = person.BankAccount.AccountNumber;
                            }

                            if (person.BankAccount.SortCode != null)
                            {
                                policyHolderPersonalViewModel.BankAccount.SortCode = person.BankAccount.SortCode;
                            }

                            if (person.BankAccount.BankName != null)
                            {
                                policyHolderPersonalViewModel.BankAccount.BankName = person.BankAccount.BankName;
                            }

                            if (person.BankAccount.DatePaymentDetailsTaken != null)
                            {
                                policyHolderPersonalViewModel.BankAccount.DateDetailsTaken = person.BankAccount.DatePaymentDetailsTaken;
                            }

                            if (person.BankAccount.PolicyPaymentType != null)
                            {
                                policyHolderPersonalViewModel.BankAccount.SelectedAccountUsedFor = (int)person.BankAccount.PolicyPaymentType;
                            }

                            //policyHolderPersonalViewModel.BankAccount.SelectedAccountUsedFor = (int)person.BankAccount.PolicyPaymentType;

                        }

                        if (person.LandlineTelephone != null)
                        {
                            policyHolderPersonalViewModel.AdditionalInfo.HomeTelephone = person.LandlineTelephone;
                        }

                        if (person.WorkTelephone != null)
                        {
                            policyHolderPersonalViewModel.AdditionalInfo.WorkTelephone = person.WorkTelephone;
                        }

                        if (person.MobileTelephone != null)
                        {
                            policyHolderPersonalViewModel.AdditionalInfo.MobileNumber = person.MobileTelephone;
                        }

                        if (person.EmailAddress != null)
                        {
                            policyHolderPersonalViewModel.AdditionalInfo.EmailAddress = person.EmailAddress;
                        }

                        if (person.Occupation != null)
                        {
                            policyHolderPersonalViewModel.AdditionalInfo.Occupation = person.Occupation;
                        }

                        if (person.Nationality != null)
                        {
                            policyHolderPersonalViewModel.AdditionalInfo.SelectedNationality = person.Nationality;
                        }

                        if (person.PassportNumber != null)
                        {
                            policyHolderPersonalViewModel.AdditionalInfo.PassportNumber = person.PassportNumber;
                        }

                        if (person.NINumber != null)
                        {
                            policyHolderPersonalViewModel.AdditionalInfo.NINumber = person.NINumber;
                        }

                        if (person.DrivingLicenseNumber != null)
                        {
                            policyHolderPersonalViewModel.AdditionalInfo.DrivingLicenseNumber = person.DrivingLicenseNumber;
                        }

                        int numberOfSupportingInvolvements = 0;

                        foreach (var org in person.Organisations)
                        {
                            numberOfSupportingInvolvements++;
                        }

                        insuredDriverViewModel.PersonInformation.NumberOfSupportingInvolvements = numberOfSupportingInvolvements;


                        #region Address

                        if (person.Addresses != null)
                        {

                            foreach (var claimAddress in person.Addresses)
                            {

                                if (claimAddress.AddressLinkType != null)
                                {
                                    policyHolderPersonalViewModel.Address.SelectedAddressStatus = (int)claimAddress.AddressLinkType;  
                                    
                                }

                                if (!string.IsNullOrEmpty(claimAddress.SubBuilding))
                                {
                                    policyHolderPersonalViewModel.Address.SubBuilding = claimAddress.SubBuilding;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Building))
                                {
                                    policyHolderPersonalViewModel.Address.BuildingName = claimAddress.Building;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.BuildingNumber))
                                {
                                    policyHolderPersonalViewModel.Address.BuildingNumber = claimAddress.BuildingNumber;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Street))
                                {
                                    policyHolderPersonalViewModel.Address.Street = claimAddress.Street;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Locality))
                                {
                                    policyHolderPersonalViewModel.Address.Locality = claimAddress.Locality;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Town))
                                {
                                    policyHolderPersonalViewModel.Address.Town = claimAddress.Town;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.County))
                                {
                                    policyHolderPersonalViewModel.Address.County = claimAddress.County;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.PostCode))
                                {
                                    policyHolderPersonalViewModel.Address.Postcode = claimAddress.PostCode;
                                }

                                if (claimAddress.StartOfResidency != null)
                                {
                                    policyHolderPersonalViewModel.Address.StartOfResidency = claimAddress.StartOfResidency;
                                }

                                if (claimAddress.EndOfResidency != null)
                                {
                                    policyHolderPersonalViewModel.Address.EndOfResidency = claimAddress.EndOfResidency;
                                }

                                //if (claimAddress.AddressLinkType != null)
                                //{
                                policyHolderPersonalViewModel.Address.SelectedAddressStatus = (int)claimAddress.AddressLinkType;
                                //}

                            }

                        }

                        #endregion

                        #region Organisations

                        int supportingInvolvement = 0;

                        foreach (var organisation in person.Organisations)
                        {

                            supportingInvolvement++;

                            SupportingInvolvementsViewModel supportingInvolvementsViewModel = new SupportingInvolvementsViewModel();

                            supportingInvolvementsViewModel.NumberSupportingInvolvements = person.Organisations.Count();
                            supportingInvolvementsViewModel.CurrentSupportingInvolvement = supportingInvolvement;

                            if (organisation.OrganisationName != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.OrganisationName = organisation.OrganisationName;
                            }

                            if (organisation.RegisteredNumber != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.RegisteredNumber = organisation.RegisteredNumber;
                            }

                            if (organisation.OrganisationType != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.SelectedOrganisationInvolvement = (int)organisation.OrganisationType;
                            }

                            #region Address

                            foreach (var address in organisation.Addresses)
                            {
                                if (address.Building != null)
                                {
                                    supportingInvolvementsViewModel.Address.BuildingName = address.Building;
                                }

                                if (address.BuildingNumber != null)
                                {
                                    supportingInvolvementsViewModel.Address.BuildingNumber = address.BuildingNumber;
                                }

                                if (address.County != null)
                                {
                                    supportingInvolvementsViewModel.Address.County = address.County;
                                }

                                if (address.EndOfResidency != null)
                                {
                                    supportingInvolvementsViewModel.Address.EndOfResidency = address.EndOfResidency;
                                }

                                if (address.Locality != null)
                                {
                                    supportingInvolvementsViewModel.Address.Locality = address.Locality;
                                }

                                if (address.PostCode != null)
                                {
                                    supportingInvolvementsViewModel.Address.Postcode = address.PostCode;
                                }

                                if (address.StartOfResidency != null)
                                {
                                    supportingInvolvementsViewModel.Address.StartOfResidency = address.StartOfResidency;
                                }

                                if (address.Street != null)
                                {
                                    supportingInvolvementsViewModel.Address.Street = address.Street;
                                }

                                if (address.SubBuilding != null)
                                {
                                    supportingInvolvementsViewModel.Address.SubBuilding = address.SubBuilding;
                                }

                                if (address.Town != null)
                                {
                                    supportingInvolvementsViewModel.Address.Town = address.Town;
                                }
                            }

                            #endregion

                            #region Vehicles

                            foreach (var orgVehicle in organisation.Vehicles)
                            {
                                if (orgVehicle.HireEndDate != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.HireEndDate = orgVehicle.HireEndDate;
                                }

                                if (orgVehicle.HireStartDate != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.HireStartDate = orgVehicle.HireStartDate;
                                }

                                if (orgVehicle.Make != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.Make = orgVehicle.Make;
                                }

                                if (orgVehicle.Model != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.Model = orgVehicle.Model;
                                }

                                if (orgVehicle.VehicleRegistration != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.RegistrationNumber = orgVehicle.VehicleRegistration;
                                }

                                //if (orgVehicle.VehicleType != null)
                                //{
                                supportingInvolvementsViewModel.Vehicle.SelectedVehicletype = (int)orgVehicle.VehicleType;
                                //}

                                #region Hire Vehicle Additional Information

                                if (orgVehicle.VIN != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.VIN = orgVehicle.VIN;
                                }

                                if (orgVehicle.Fuel != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedFuel = (int)orgVehicle.Fuel;
                                }

                                if (orgVehicle.Colour != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedColour = (int)orgVehicle.Colour;
                                }

                                if (orgVehicle.EngineCapacity != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.EngineCapacity = orgVehicle.EngineCapacity;
                                }

                                if (orgVehicle.Transmission != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedTransmission = (int)orgVehicle.Transmission;
                                }

                                #endregion
                            }

                            #endregion

                            insuredDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        }

                        #endregion


                    }



                }

                #endregion

                #region Insured Driver

                foreach (var person in vehicle.People)
                {

                    if (person.PartyType == PartyType.Insured && person.SubPartyType == SubPartyType.Driver)
                    {
                        //if (person.Salutation != null)
                        //{
                        insuredDriverViewModel.PersonInformation.SelectedSalutation = (int)person.Salutation;
                        //}

                        if (person.FirstName != null)
                        {
                            insuredDriverViewModel.PersonInformation.Firstname = person.FirstName;
                        }

                        if (person.MiddleName != null)
                        {
                            insuredDriverViewModel.PersonInformation.Middlename = person.MiddleName;
                        }

                        if (person.LastName != null)
                        {
                            insuredDriverViewModel.PersonInformation.Surname = person.LastName;
                        }

                        if (person.DateOfBirth != null)
                        {
                            insuredDriverViewModel.PersonInformation.DateOfBirth = person.DateOfBirth;
                        }

                        //if (person.Gender != null)
                        //{
                        insuredDriverViewModel.PersonInformation.SelectedGender = (int)person.Gender;
                        //}

                        if (person.BankAccount != null)
                        {

                            if (person.BankAccount.AccountNumber != null)
                            {
                                insuredDriverViewModel.BankAccount.AccountNumber = person.BankAccount.AccountNumber;
                            }

                            if (person.BankAccount.SortCode != null)
                            {
                                insuredDriverViewModel.BankAccount.SortCode = person.BankAccount.SortCode;
                            }

                            if (person.BankAccount.BankName != null)
                            {
                                insuredDriverViewModel.BankAccount.BankName = person.BankAccount.BankName;
                            }

                            if (person.BankAccount.DatePaymentDetailsTaken != null)
                            {
                                insuredDriverViewModel.BankAccount.DateDetailsTaken = person.BankAccount.DatePaymentDetailsTaken;
                            }

                            if (person.BankAccount.PolicyPaymentType != null)
                            {
                                insuredDriverViewModel.BankAccount.SelectedAccountUsedFor = (int)person.BankAccount.PolicyPaymentType;
                            }

                        }

                        if (person.LandlineTelephone != null)
                        {
                            insuredDriverViewModel.AdditionalInfo.HomeTelephone = person.LandlineTelephone;
                        }

                        if (person.WorkTelephone != null)
                        {
                            insuredDriverViewModel.AdditionalInfo.WorkTelephone = person.WorkTelephone;
                        }

                        if (person.MobileTelephone != null)
                        {
                            insuredDriverViewModel.AdditionalInfo.MobileNumber = person.MobileTelephone;
                        }

                        if (person.EmailAddress != null)
                        {
                            insuredDriverViewModel.AdditionalInfo.EmailAddress = person.EmailAddress;
                        }

                        if (person.Occupation != null)
                        {
                            insuredDriverViewModel.AdditionalInfo.Occupation = person.Occupation;
                        }

                        if (person.Nationality != null)
                        {
                            insuredDriverViewModel.AdditionalInfo.SelectedNationality = person.Nationality;
                        }

                        if (person.PassportNumber != null)
                        {
                            insuredDriverViewModel.AdditionalInfo.PassportNumber = person.PassportNumber;
                        }

                        if (person.NINumber != null)
                        {
                            insuredDriverViewModel.AdditionalInfo.NINumber = person.NINumber;
                        }

                        if (person.DrivingLicenseNumber != null)
                        {
                            insuredDriverViewModel.AdditionalInfo.DrivingLicenseNumber = person.DrivingLicenseNumber;
                        }

                        int numberOfSupportingInvolvements = 0;

                        foreach (var org in person.Organisations)
                        {
                            numberOfSupportingInvolvements++;
                        }

                        insuredDriverViewModel.PersonInformation.NumberOfSupportingInvolvements = numberOfSupportingInvolvements;



                        #region Address

                        if (person.Addresses != null)
                        {

                            foreach (var claimAddress in person.Addresses)
                            {

                                if (claimAddress.AddressLinkType != null)
                                {
                                    insuredDriverViewModel.Address.SelectedAddressStatus = (int)claimAddress.AddressLinkType;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Building))
                                {
                                    insuredDriverViewModel.Address.BuildingName = claimAddress.Building;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.BuildingNumber))
                                {
                                    insuredDriverViewModel.Address.BuildingNumber = claimAddress.BuildingNumber;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.County))
                                {
                                    insuredDriverViewModel.Address.County = claimAddress.County;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.SubBuilding))
                                {
                                    insuredDriverViewModel.Address.SubBuilding = claimAddress.SubBuilding;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Street))
                                {
                                    insuredDriverViewModel.Address.Street = claimAddress.Street;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Town))
                                {
                                    insuredDriverViewModel.Address.Town = claimAddress.Town;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Locality))
                                {
                                    insuredDriverViewModel.Address.Locality = claimAddress.Locality;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.PostCode))
                                {
                                    insuredDriverViewModel.Address.Postcode = claimAddress.PostCode;
                                }

                                if (claimAddress.StartOfResidency != null)
                                {
                                    insuredDriverViewModel.Address.StartOfResidency = claimAddress.StartOfResidency;
                                }

                                if (claimAddress.EndOfResidency != null)
                                {
                                    insuredDriverViewModel.Address.EndOfResidency = claimAddress.EndOfResidency;
                                }

                                if (InsuredAddressSameAddressAsPolicyHolder(insuredDriverViewModel.Address,policyHolderPersonalViewModel.Address))
                                {
                                    insuredDriverViewModel.Address.SameAddressAsPolicyHolder = true;
                                    //insuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver = true;
                                }
                                else
                                {
                                    insuredDriverViewModel.Address.SameAddressAsPolicyHolder = false;
                                    //insuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver = false;
                                }

                            }

                        }


                        #endregion

                        #region Organisations

                        int supportingInvolvement = 0;

                        foreach (var organisation in person.Organisations)
                        {

                            supportingInvolvement++;

                            SupportingInvolvementsViewModel supportingInvolvementsViewModel = new SupportingInvolvementsViewModel();


                            supportingInvolvementsViewModel.NumberSupportingInvolvements = person.Organisations.Count();
                            supportingInvolvementsViewModel.CurrentSupportingInvolvement = supportingInvolvement;


                            if (organisation.OrganisationType != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.SelectedOrganisationInvolvement = (int)organisation.OrganisationType;
                            }

                            if (organisation.OrganisationName != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.OrganisationName = organisation.OrganisationName;
                            }

                            if (organisation.RegisteredNumber != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.RegisteredNumber = organisation.RegisteredNumber;
                            }


                            #region Additional Info

                            if (organisation.Telephone1 != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.Telephone1 = organisation.Telephone1;
                            }

                            if (organisation.Telephone2 != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.Telephone2 = organisation.Telephone2;
                            }

                            if (organisation.Telephone3 != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.Telephone3 = organisation.Telephone3;
                            }

                            if (organisation.Email != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.EmailAddress = organisation.Email;
                            }

                            if (organisation.WebSite != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.WebsiteAddress = organisation.WebSite;
                            }

                            if (organisation.VatNumber != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.VATNumber = organisation.VatNumber;
                            }

                            if (organisation.MojCrmNumber != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.MoJClaimsRegNumber = organisation.MojCrmNumber;
                            }

                            #endregion


                            #region Address

                            foreach (var address in organisation.Addresses)
                            {
                                if (address.AddressLinkType != null)
                                {
                                    supportingInvolvementsViewModel.Address.SelectedAddressStatus = (int)address.AddressLinkType;
                                }

                                if (address.Building != null)
                                {
                                    supportingInvolvementsViewModel.Address.BuildingName = address.Building;
                                }

                                if (address.BuildingNumber != null)
                                {
                                    supportingInvolvementsViewModel.Address.BuildingNumber = address.BuildingNumber;
                                }

                                if (address.County != null)
                                {
                                    supportingInvolvementsViewModel.Address.County = address.County;
                                }

                                if (address.EndOfResidency != null)
                                {
                                    supportingInvolvementsViewModel.Address.EndOfResidency = address.EndOfResidency;
                                }

                                if (address.Locality != null)
                                {
                                    supportingInvolvementsViewModel.Address.Locality = address.Locality;
                                }

                                if (address.PostCode != null)
                                {
                                    supportingInvolvementsViewModel.Address.Postcode = address.PostCode;
                                }

                                if (address.StartOfResidency != null)
                                {
                                    supportingInvolvementsViewModel.Address.StartOfResidency = address.StartOfResidency;
                                }

                                if (address.Street != null)
                                {
                                    supportingInvolvementsViewModel.Address.Street = address.Street;
                                }

                                if (address.SubBuilding != null)
                                {
                                    supportingInvolvementsViewModel.Address.SubBuilding = address.SubBuilding;
                                }

                                if (address.Town != null)
                                {
                                    supportingInvolvementsViewModel.Address.Town = address.Town;
                                }
                            }

                            #endregion

                            #region Vehicles

                            foreach (var orgVehicle in organisation.Vehicles)
                            {
                                if (orgVehicle.HireEndDate != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.HireEndDate = orgVehicle.HireEndDate;
                                }

                                if (orgVehicle.HireStartDate != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.HireStartDate = orgVehicle.HireStartDate;
                                }

                                if (orgVehicle.Make != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.Make = orgVehicle.Make;
                                }

                                if (orgVehicle.Model != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.Model = orgVehicle.Model;
                                }

                                if (orgVehicle.VehicleRegistration != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.RegistrationNumber = orgVehicle.VehicleRegistration;
                                }

                                //if (orgVehicle.VehicleType != null)
                                //{
                                supportingInvolvementsViewModel.Vehicle.SelectedVehicletype = (int)orgVehicle.VehicleType;
                                //}

                                #region Hire Vehicle Additional Information

                                if (orgVehicle.VIN != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.VIN = orgVehicle.VIN;
                                }

                                if (orgVehicle.Fuel != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedFuel = (int)orgVehicle.Fuel;
                                }

                                if (orgVehicle.Colour != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedColour = (int)orgVehicle.Colour;
                                }

                                if (orgVehicle.EngineCapacity != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.EngineCapacity = orgVehicle.EngineCapacity;
                                }

                                if (orgVehicle.Transmission != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedTransmission = (int)orgVehicle.Transmission;
                                }

                                #endregion
                            }

                            #endregion

                            insuredDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        }

                        #endregion

                    }
                }

                #endregion

                #region Insured Passenger

                int insuredPassenger = 1;

                foreach (var person in vehicle.People)
                {

                    if (person.PartyType == PartyType.Insured && person.SubPartyType == SubPartyType.Passenger)
                    {

                        PassengerViewModel passengerViewModel = new PassengerViewModel();

                        //if (person.Salutation != null)
                        //{
                        passengerViewModel.PersonInformation.SelectedSalutation = (int)person.Salutation;
                        //}

                        if (person.FirstName != null)
                        {
                            passengerViewModel.PersonInformation.Firstname = person.FirstName;
                        }

                        if (person.MiddleName != null)
                        {
                            passengerViewModel.PersonInformation.Middlename = person.MiddleName;
                        }

                        if (person.LastName != null)
                        {
                            passengerViewModel.PersonInformation.Surname = person.LastName;
                        }

                        if (person.DateOfBirth != null)
                        {
                            passengerViewModel.PersonInformation.DateOfBirth = person.DateOfBirth;
                        }

                        //if (person.Gender != null)
                        //{
                        passengerViewModel.PersonInformation.SelectedGender = (int)person.Gender;
                        //}

                        #region Bank Account

                        if (person.BankAccount != null)
                        {

                            if (person.BankAccount.AccountNumber != null)
                            {
                                passengerViewModel.BankAccount.AccountNumber = person.BankAccount.AccountNumber;
                            }

                            if (person.BankAccount.SortCode != null)
                            {
                                passengerViewModel.BankAccount.SortCode = person.BankAccount.SortCode;
                            }

                            if (person.BankAccount.BankName != null)
                            {
                                passengerViewModel.BankAccount.BankName = person.BankAccount.BankName;
                            }

                            if (person.BankAccount.DatePaymentDetailsTaken != null)
                            {
                                passengerViewModel.BankAccount.DateDetailsTaken = person.BankAccount.DatePaymentDetailsTaken;
                            }

                            if (person.BankAccount.PolicyPaymentType != null)
                            {
                                passengerViewModel.BankAccount.SelectedAccountUsedFor = (int)person.BankAccount.PolicyPaymentType;
                            }

                        }

                        #endregion

                        if (person.LandlineTelephone != null)
                        {
                            passengerViewModel.AdditionalInfo.HomeTelephone = person.LandlineTelephone;
                        }

                        if (person.WorkTelephone != null)
                        {
                            passengerViewModel.AdditionalInfo.WorkTelephone = person.WorkTelephone;
                        }

                        if (person.EmailAddress != null)
                        {
                            passengerViewModel.AdditionalInfo.EmailAddress = person.EmailAddress;
                        }

                        if (person.Occupation != null)
                        {
                            passengerViewModel.AdditionalInfo.Occupation = person.Occupation;
                        }

                        if (person.Nationality != null)
                        {
                            passengerViewModel.AdditionalInfo.SelectedNationality = person.Nationality;
                        }

                        if (person.PassportNumber != null)
                        {
                            passengerViewModel.AdditionalInfo.PassportNumber = person.PassportNumber;
                        }

                        if (person.NINumber != null)
                        {
                            passengerViewModel.AdditionalInfo.NINumber = person.NINumber;
                        }

                        if (person.DrivingLicenseNumber != null)
                        {
                            passengerViewModel.AdditionalInfo.DrivingLicenseNumber = person.DrivingLicenseNumber;
                        }

                        int numberOfSupportingInvolvements = 0;

                        foreach (var org in person.Organisations)
                        {
                            numberOfSupportingInvolvements++;
                        }

                        passengerViewModel.PersonInformation.NumberOfSupportingInvolvements = numberOfSupportingInvolvements;

                        passengerViewModel.CurrentPassenger = insuredPassenger;

                        passengerViewModel.PreviousPageID = insuredPassenger - 1;

                        passengerViewModel.PassengerID = insuredPassenger;

                        passengerViewModel.PassengerType = PassengerType.InsuredVehiclePassenger;

                        CurrentView passengerPreviousPage = CurrentView.Unknown;

                        if (passengerViewModel.CurrentPassenger == 1)
                        {
                            passengerViewModel.PreviousPage = CurrentView.InsuredDriver;
                            passengerPreviousPage = SingleClaimRepository.CheckFirstPassengerPreviousPage(passengerViewModel);
                        }
                        else
                        {
                            passengerPreviousPage = SingleClaimRepository.CheckPreviousPassengerHasSupportingInvolvement(passengerViewModel);
                        }

                        passengerViewModel.PreviousPage = passengerPreviousPage;


                        insuredDriverPassengers.Add(passengerViewModel);

                        #region Address

                        if (person.Addresses != null)
                        {

                            foreach (var claimAddress in person.Addresses)
                            {

                                if (claimAddress.AddressLinkType != null)
                                {
                                    passengerViewModel.Address.SelectedAddressStatus = (int)claimAddress.AddressLinkType;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Building))
                                {
                                    passengerViewModel.Address.BuildingName = claimAddress.Building;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.BuildingNumber))
                                {
                                    passengerViewModel.Address.BuildingNumber = claimAddress.BuildingNumber;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.SubBuilding))
                                {
                                    passengerViewModel.Address.SubBuilding = claimAddress.SubBuilding;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Street))
                                {
                                    passengerViewModel.Address.Street = claimAddress.Street;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Town))
                                {
                                    passengerViewModel.Address.Town = claimAddress.Town;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Locality))
                                {
                                    passengerViewModel.Address.Locality = claimAddress.Locality;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.PostCode))
                                {
                                    passengerViewModel.Address.Postcode = claimAddress.PostCode;
                                }

                                if (claimAddress.StartOfResidency != null)
                                {
                                    passengerViewModel.Address.StartOfResidency = claimAddress.StartOfResidency;
                                }

                                if (claimAddress.EndOfResidency != null)
                                {
                                    passengerViewModel.Address.EndOfResidency = claimAddress.EndOfResidency;
                                }

                            }

                        }


                        #endregion

                        #region Organisations

                        int supportingInvolvement = 0;

                        List<SupportingInvolvementsViewModel> insuredDriverPassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                        foreach (var organisation in person.Organisations)
                        {
                            supportingInvolvement++;

                            SupportingInvolvementsViewModel supportingInvolvementsViewModel = new SupportingInvolvementsViewModel();


                            supportingInvolvementsViewModel.NumberSupportingInvolvements = person.Organisations.Count();
                            supportingInvolvementsViewModel.CurrentSupportingInvolvement = supportingInvolvement;
                            supportingInvolvementsViewModel.SupportingInvolvementsType = SupportingInvolvementsType.InsuredPassenger;
                            supportingInvolvementsViewModel.ParentID = insuredPassenger;


                            if (organisation.OrganisationName != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.OrganisationName = organisation.OrganisationName;
                            }

                            if (organisation.RegisteredNumber != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.RegisteredNumber = organisation.RegisteredNumber;
                            }

                            if (organisation.OrganisationType != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.SelectedOrganisationInvolvement = (int)organisation.OrganisationType;
                            }

                            #region Additional Info

                            if (organisation.Telephone1 != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.Telephone1 = organisation.Telephone1;
                            }

                            if (organisation.Telephone2 != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.Telephone2 = organisation.Telephone2;
                            }

                            if (organisation.Telephone3 != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.Telephone3 = organisation.Telephone3;
                            }

                            if (organisation.Email != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.EmailAddress = organisation.Email;
                            }

                            if (organisation.WebSite != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.WebsiteAddress = organisation.WebSite;
                            }

                            if (organisation.VatNumber != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.VATNumber = organisation.VatNumber;
                            }

                            if (organisation.MojCrmNumber != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.MoJClaimsRegNumber = organisation.MojCrmNumber;
                            }

                            #endregion

                            #region Address

                            foreach (var address in organisation.Addresses)
                            {
                                if (address.AddressLinkType != null)
                                {
                                    supportingInvolvementsViewModel.Address.SelectedAddressStatus = (int)address.AddressLinkType;
                                }

                                if (address.Building != null)
                                {
                                    supportingInvolvementsViewModel.Address.BuildingName = address.Building;
                                }

                                if (address.BuildingNumber != null)
                                {
                                    supportingInvolvementsViewModel.Address.BuildingNumber = address.BuildingNumber;
                                }

                                if (address.County != null)
                                {
                                    supportingInvolvementsViewModel.Address.County = address.County;
                                }

                                if (address.EndOfResidency != null)
                                {
                                    supportingInvolvementsViewModel.Address.EndOfResidency = address.EndOfResidency;
                                }

                                if (address.Locality != null)
                                {
                                    supportingInvolvementsViewModel.Address.Locality = address.Locality;
                                }

                                if (address.PostCode != null)
                                {
                                    supportingInvolvementsViewModel.Address.Postcode = address.PostCode;
                                }

                                if (address.StartOfResidency != null)
                                {
                                    supportingInvolvementsViewModel.Address.StartOfResidency = address.StartOfResidency;
                                }

                                if (address.Street != null)
                                {
                                    supportingInvolvementsViewModel.Address.Street = address.Street;
                                }

                                if (address.SubBuilding != null)
                                {
                                    supportingInvolvementsViewModel.Address.SubBuilding = address.SubBuilding;
                                }

                                if (address.Town != null)
                                {
                                    supportingInvolvementsViewModel.Address.Town = address.Town;
                                }
                            }

                            #endregion

                            #region Vehicles

                            foreach (var orgVehicle in organisation.Vehicles)
                            {
                                if (orgVehicle.HireEndDate != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.HireEndDate = orgVehicle.HireEndDate;
                                }

                                if (orgVehicle.HireStartDate != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.HireStartDate = orgVehicle.HireStartDate;
                                }

                                if (orgVehicle.Make != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.Make = orgVehicle.Make;
                                }

                                if (orgVehicle.Model != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.Model = orgVehicle.Model;
                                }

                                if (orgVehicle.VehicleRegistration != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.RegistrationNumber = orgVehicle.VehicleRegistration;
                                }

                                supportingInvolvementsViewModel.Vehicle.SelectedVehicletype = (int)orgVehicle.VehicleType;


                                if (orgVehicle.VIN != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.VIN = orgVehicle.VIN;
                                }

                                if (orgVehicle.Colour != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedColour = (int)orgVehicle.Colour;
                                }

                                if (orgVehicle.Transmission != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedTransmission = (int)orgVehicle.Transmission;
                                }

                                if (orgVehicle.Fuel != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedFuel = (int)orgVehicle.Fuel;
                                }

                                if (orgVehicle.EngineCapacity != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.EngineCapacity = orgVehicle.EngineCapacity;
                                }

                            }

                            #endregion

                            


                            insuredDriverPassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);

                        }

                        #endregion

                        if (insuredDriverPassengerSupportingInvolvements.Count > 0)
                        {
                            dictionaryPassengerSupportingInvolvements.Add(insuredPassenger, insuredDriverPassengerSupportingInvolvements);
                        }

                        insuredPassenger++;

                    }

                }

                #endregion

                #region Third Party Driver

                foreach (var person in vehicle.People)
                {

                    if (person.PartyType == PartyType.ThirdParty && person.SubPartyType == SubPartyType.Driver)
                    {

                        ThirdPartyDriverViewModel thirdPartyDriver = new ThirdPartyDriverViewModel();

                        //if (person.Salutation != null)
                        //{
                        thirdPartyDriver.PersonInformation.SelectedSalutation = (int)person.Salutation;
                        //}

                        if (person.FirstName != null)
                        {
                            thirdPartyDriver.PersonInformation.Firstname = person.FirstName;
                        }

                        if (person.MiddleName != null)
                        {
                            thirdPartyDriver.PersonInformation.Middlename = person.MiddleName;
                        }

                        if (person.LastName != null)
                        {
                            thirdPartyDriver.PersonInformation.Surname = person.LastName;
                        }

                        if (person.DateOfBirth != null)
                        {
                            thirdPartyDriver.PersonInformation.DateOfBirth = person.DateOfBirth;
                        }

                        //if (person.Gender != null)
                        //{
                        thirdPartyDriver.PersonInformation.SelectedGender = (int)person.Gender;
                        //}

                        #region Bank Account

                        if (person.BankAccount != null)
                        {

                            if (person.BankAccount.AccountNumber != null)
                            {
                                thirdPartyDriver.BankAccount.AccountNumber = person.BankAccount.AccountNumber;
                            }

                            if (person.BankAccount.SortCode != null)
                            {
                                thirdPartyDriver.BankAccount.SortCode = person.BankAccount.SortCode;
                            }

                            if (person.BankAccount.BankName != null)
                            {
                                thirdPartyDriver.BankAccount.BankName = person.BankAccount.BankName;
                            }

                            if (person.BankAccount.DatePaymentDetailsTaken != null)
                            {
                                thirdPartyDriver.BankAccount.DateDetailsTaken = person.BankAccount.DatePaymentDetailsTaken;
                            }

                            if (person.BankAccount.PolicyPaymentType != null)
                            {
                                thirdPartyDriver.BankAccount.SelectedAccountUsedFor = (int)person.BankAccount.PolicyPaymentType;
                            }

                        }

                        #endregion

                        if (person.LandlineTelephone != null)
                        {
                            thirdPartyDriver.AdditionalInfo.HomeTelephone = person.LandlineTelephone;
                        }

                        if (person.WorkTelephone != null)
                        {
                            thirdPartyDriver.AdditionalInfo.WorkTelephone = person.WorkTelephone;
                        }

                        if (person.MobileTelephone != null)
                        {
                            thirdPartyDriver.AdditionalInfo.MobileNumber = person.MobileTelephone;
                        }

                        if (person.EmailAddress != null)
                        {
                            thirdPartyDriver.AdditionalInfo.EmailAddress = person.EmailAddress;
                        }

                        if (person.Occupation != null)
                        {
                            thirdPartyDriver.AdditionalInfo.Occupation = person.Occupation;
                        }

                        if (person.Nationality != null)
                        {
                            thirdPartyDriver.AdditionalInfo.SelectedNationality = person.Nationality;
                        }

                        if (person.PassportNumber != null)
                        {
                            thirdPartyDriver.AdditionalInfo.PassportNumber = person.PassportNumber;
                        }

                        if (person.NINumber != null)
                        {
                            thirdPartyDriver.AdditionalInfo.NINumber = person.NINumber;
                        }

                        if (person.DrivingLicenseNumber != null)
                        {
                            thirdPartyDriver.AdditionalInfo.DrivingLicenseNumber = person.DrivingLicenseNumber;
                        }

                        int numberOfSupportingInvolvements = 0;

                        foreach (var org in person.Organisations)
                        {
                            numberOfSupportingInvolvements++;
                        }

                        thirdPartyDriver.PersonInformation.NumberOfSupportingInvolvements = numberOfSupportingInvolvements;

                        #region Address

                        if (person.Addresses != null)
                        {

                            foreach (var claimAddress in person.Addresses)
                            {

                                if (claimAddress.AddressLinkType != null)
                                {
                                    thirdPartyDriver.Address.SelectedAddressStatus = (int)claimAddress.AddressLinkType;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Building))
                                {
                                    thirdPartyDriver.Address.BuildingName = claimAddress.Building;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.BuildingNumber))
                                {
                                    thirdPartyDriver.Address.BuildingNumber = claimAddress.BuildingNumber;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.SubBuilding))
                                {
                                    thirdPartyDriver.Address.SubBuilding = claimAddress.SubBuilding;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Street))
                                {
                                    thirdPartyDriver.Address.Street = claimAddress.Street;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Town))
                                {
                                    thirdPartyDriver.Address.Town = claimAddress.Town;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.County))
                                {
                                    thirdPartyDriver.Address.County = claimAddress.County;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Locality))
                                {
                                    thirdPartyDriver.Address.Locality = claimAddress.Locality;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.PostCode))
                                {
                                    thirdPartyDriver.Address.Postcode = claimAddress.PostCode;
                                }

                                if (claimAddress.StartOfResidency != null)
                                {
                                    thirdPartyDriver.Address.StartOfResidency = claimAddress.StartOfResidency;
                                }

                                if (claimAddress.EndOfResidency != null)
                                {
                                    thirdPartyDriver.Address.EndOfResidency = claimAddress.EndOfResidency;
                                }

                            }

                        }


                        #endregion

                        #region Organisations

                        int supportingInvolvement = 0;

                        Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyDriverSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                        List<SupportingInvolvementsViewModel> thirdPartyDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                        foreach (var organisation in person.Organisations)
                        {

                            supportingInvolvement++;                                                      
                            
                            SupportingInvolvementsViewModel supportingInvolvementsViewModel = new SupportingInvolvementsViewModel();
                            
                            supportingInvolvementsViewModel.NumberSupportingInvolvements = person.Organisations.Count();
                            supportingInvolvementsViewModel.CurrentSupportingInvolvement = supportingInvolvement;
                            supportingInvolvementsViewModel.SupportingInvolvementsType = SupportingInvolvementsType.ThirdPartyDriver;
                            supportingInvolvementsViewModel.ParentID = thirdPartyVehicleId;
                            supportingInvolvementsViewModel.VehicleID = thirdPartyVehicleId;

                            if (organisation.OrganisationName != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.OrganisationName = organisation.OrganisationName;
                            }

                            if (organisation.RegisteredNumber != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.RegisteredNumber = organisation.RegisteredNumber;
                            }

                            if (organisation.OrganisationType != null)
                            {
                                supportingInvolvementsViewModel.OrganisationInvolvement.SelectedOrganisationInvolvement = (int)organisation.OrganisationType;
                            }

                            #region Additional Info

                            if (organisation.Telephone1 != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.Telephone1 = organisation.Telephone1;
                            }

                            if (organisation.Telephone2 != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.Telephone2 = organisation.Telephone2;
                            }

                            if (organisation.Telephone3 != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.Telephone3 = organisation.Telephone3;
                            }

                            if (organisation.Email != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.EmailAddress = organisation.Email;
                            }

                            if (organisation.WebSite != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.WebsiteAddress = organisation.WebSite;
                            }

                            if (organisation.VatNumber != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.VATNumber = organisation.VatNumber;
                            }

                            if (organisation.MojCrmNumber != null)
                            {
                                supportingInvolvementsViewModel.AdditionalInfo.MoJClaimsRegNumber = organisation.MojCrmNumber;
                            }

                            #endregion

                            #region Address

                            foreach (var address in organisation.Addresses)
                            {
                                if (address.AddressLinkType != null)
                                {
                                    supportingInvolvementsViewModel.Address.SelectedAddressStatus = (int)address.AddressLinkType;
                                }

                                if (address.Building != null)
                                {
                                    supportingInvolvementsViewModel.Address.BuildingName = address.Building;
                                }

                                if (address.BuildingNumber != null)
                                {
                                    supportingInvolvementsViewModel.Address.BuildingNumber = address.BuildingNumber;
                                }

                                if (address.County != null)
                                {
                                    supportingInvolvementsViewModel.Address.County = address.County;
                                }

                                if (address.EndOfResidency != null)
                                {
                                    supportingInvolvementsViewModel.Address.EndOfResidency = address.EndOfResidency;
                                }

                                if (address.Locality != null)
                                {
                                    supportingInvolvementsViewModel.Address.Locality = address.Locality;
                                }

                                if (address.PostCode != null)
                                {
                                    supportingInvolvementsViewModel.Address.Postcode = address.PostCode;
                                }

                                if (address.StartOfResidency != null)
                                {
                                    supportingInvolvementsViewModel.Address.StartOfResidency = address.StartOfResidency;
                                }

                                if (address.Street != null)
                                {
                                    supportingInvolvementsViewModel.Address.Street = address.Street;
                                }

                                if (address.SubBuilding != null)
                                {
                                    supportingInvolvementsViewModel.Address.SubBuilding = address.SubBuilding;
                                }

                                if (address.Town != null)
                                {
                                    supportingInvolvementsViewModel.Address.Town = address.Town;
                                }
                            }

                            #endregion

                            #region Vehicles

                            foreach (var orgVehicle in organisation.Vehicles)
                            {
                                if (orgVehicle.HireEndDate != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.HireEndDate = orgVehicle.HireEndDate;
                                }

                                if (orgVehicle.HireStartDate != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.HireStartDate = orgVehicle.HireStartDate;
                                }

                                if (orgVehicle.Make != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.Make = orgVehicle.Make;
                                }

                                if (orgVehicle.Model != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.Model = orgVehicle.Model;
                                }

                                if (orgVehicle.VehicleRegistration != null)
                                {
                                    supportingInvolvementsViewModel.Vehicle.RegistrationNumber = orgVehicle.VehicleRegistration;
                                }

                                #region Hire Vehicle Additional Information

                                if (orgVehicle.VIN != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.VIN = orgVehicle.VIN;
                                }

                                if (orgVehicle.Fuel != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedFuel = (int)orgVehicle.Fuel;
                                }

                                if (orgVehicle.Colour != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedColour = (int)orgVehicle.Colour;
                                }

                                if (orgVehicle.EngineCapacity != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.EngineCapacity = orgVehicle.EngineCapacity;
                                }

                                if (orgVehicle.Transmission != null)
                                {
                                    supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedTransmission = (int)orgVehicle.Transmission;
                                }

                                #endregion

                                //if (orgVehicle.VehicleType != null)
                                //{
                                supportingInvolvementsViewModel.Vehicle.SelectedVehicletype = (int)orgVehicle.VehicleType;
                                //}
                            }

                            #endregion

                            thirdPartyDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);

                        }
                        
                        DictionaryThirdPartyDriverSupportingInvolvements.Add(thirdPartyVehicleId, thirdPartyDriverSupportingInvolvements);

                        if (thirdPartyDriverSupportingInvolvements.Count > 0)
                        {

                            //thirdPartyDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                            //DictionaryThirdPartyDriverSupportingInvolvements.Add(VehicleID, thirdPartyDriverSupportingInvolvements);
                            //HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] = DictionaryThirdPartyDriverSupportingInvolvements;
                            System.Web.HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] = DictionaryThirdPartyDriverSupportingInvolvements;


                        }

                        #endregion


                        CurrentView thirdPartyDriverPreviousPage = CurrentView.Unknown;
                        if (thirdPartyDriver.CurrentVehicle == 0)
                        {
                            thirdPartyDriver.PreviousPage = CurrentView.InsuredDriver;
                        }
                        else
                        {
                            thirdPartyDriver.PreviousPage = CurrentView.ThirdPartyVehicle;
                        }
                        thirdPartyDriver.PreviousPage = thirdPartyDriverPreviousPage;
                        thirdPartyDriver.CurrentVehicle = thirdPartyVehicleId;
                        thirdPartyDriver.NumberOfVehicles = SingleClaimRepository.NumberOfThirdPartyVehicles();
                        thirdPartyDriver.VehicleID = thirdPartyVehicleId;

                        if (!thirdPartyDriverViewModels.ContainsKey(thirdPartyDriver.VehicleID))
                        {
                            thirdPartyDriverViewModels.Add(thirdPartyDriver.VehicleID, thirdPartyDriver);
                        }
                    }

                }

                #endregion

                #region Witness

                foreach (var person in vehicle.People)
                {

                    if (person.PartyType == PartyType.Witness)
                    {

                        witnessId++;

                        WitnessViewModel witnessViewModel = new WitnessViewModel();

                        //if (person.Salutation != null)
                        //{
                        witnessViewModel.PersonInformation.SelectedSalutation = (int)person.Salutation;
                        //}

                        if (person.FirstName != null)
                        {
                            witnessViewModel.PersonInformation.Firstname = person.FirstName;
                        }

                        if (person.MiddleName != null)
                        {
                            witnessViewModel.PersonInformation.Middlename = person.MiddleName;
                        }

                        if (person.LastName != null)
                        {
                            witnessViewModel.PersonInformation.Surname = person.LastName;
                        }

                        if (person.DateOfBirth != null)
                        {
                            witnessViewModel.PersonInformation.DateOfBirth = person.DateOfBirth;
                        }

                        //if (person.Gender != null)
                        //{
                        witnessViewModel.PersonInformation.SelectedGender = (int)person.Gender;
                        //}

                        if (person.LandlineTelephone != null)
                        {
                            witnessViewModel.AdditionalInfo.HomeTelephone = person.LandlineTelephone;
                        }

                        if (person.WorkTelephone != null)
                        {
                            witnessViewModel.AdditionalInfo.WorkTelephone = person.WorkTelephone;
                        }

                        if (person.EmailAddress != null)
                        {
                            witnessViewModel.AdditionalInfo.EmailAddress = person.EmailAddress;
                        }

                        if (person.Occupation != null)
                        {
                            witnessViewModel.AdditionalInfo.Occupation = person.Occupation;
                        }

                        if (person.Nationality != null)
                        {
                            witnessViewModel.AdditionalInfo.SelectedNationality = person.Nationality;
                        }

                        if (person.PassportNumber != null)
                        {
                            witnessViewModel.AdditionalInfo.PassportNumber = person.PassportNumber;
                        }

                        if (person.NINumber != null)
                        {
                            witnessViewModel.AdditionalInfo.NINumber = person.NINumber;
                        }

                        if (person.DrivingLicenseNumber != null)
                        {
                            witnessViewModel.AdditionalInfo.DrivingLicenseNumber = person.DrivingLicenseNumber;
                        }

                        #region Address

                        if (person.Addresses != null)
                        {

                            foreach (var claimAddress in person.Addresses)
                            {

                                if (claimAddress.AddressLinkType != null)
                                {
                                    witnessViewModel.Address.SelectedAddressStatus = (int)claimAddress.AddressLinkType;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Building))
                                {
                                    witnessViewModel.Address.BuildingName = claimAddress.Building;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.BuildingNumber))
                                {
                                    witnessViewModel.Address.BuildingNumber = claimAddress.BuildingNumber;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.SubBuilding))
                                {
                                    witnessViewModel.Address.SubBuilding = claimAddress.SubBuilding;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Street))
                                {
                                    witnessViewModel.Address.Street = claimAddress.Street;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Town))
                                {
                                    witnessViewModel.Address.Town = claimAddress.Town;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Locality))
                                {
                                    witnessViewModel.Address.Locality = claimAddress.Locality;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.PostCode))
                                {
                                    witnessViewModel.Address.Postcode = claimAddress.PostCode;
                                }

                            }

                        }


                        #endregion

                        #region Organisations

                        foreach (var organisation in person.Organisations)
                        {

                            #region Address

                            foreach (var address in organisation.Addresses)
                            {

                            }

                            #endregion

                            #region Vehicles

                            foreach (var orgVehicle in organisation.Vehicles)
                            {

                            }

                            #endregion

                        }
                        #endregion

                        witnessViewModel.CurrentWitness = witnessId;

                        if (witnessViewModel.CurrentWitness == 1)
                        {
                            witnessViewModel.PreviousPage = CurrentView.InsuredDriver;
                        }
                        else
                        {
                            witnessViewModel.PreviousPage = CurrentView.Witness;
                        }

                        witnessViewModel.NumberOfWitnesses = SingleClaimRepository.NumberOfWitnesses();

                        if (!witnessViewModels.ContainsKey(witnessViewModel.CurrentWitness))
                        {
                            witnessViewModels.Add(witnessViewModel.CurrentWitness, witnessViewModel);
                        }

                    }

                }


                #endregion

                #endregion

                #region Vehicles

                #region Insured Vehicle

                if (vehicle.Incident2VehicleLinkType == Incident2VehicleLinkType.InsuredVehicle)
                {
                    //if (vehicle.VehicleType != null)
                    //{
                    insuredVehicleViewModel.Vehicle.SelectedVehicletype = (int)vehicle.VehicleType;
                    //}

                    if (vehicle.VehicleRegistration != null)
                    {
                        insuredVehicleViewModel.Vehicle.RegistrationNumber = vehicle.VehicleRegistration;
                    }

                    if (vehicle.Make != null)
                    {
                        insuredVehicleViewModel.Vehicle.Make = vehicle.Make;
                    }

                    if (vehicle.Model != null)
                    {
                        insuredVehicleViewModel.Vehicle.Model = vehicle.Model;
                    }

                    if (vehicle.DamageDescription != null)
                    {

                        string damageDescription = vehicle.DamageDescription;

                        string[] vehicleValues = damageDescription.Split('|');

                        if (!string.IsNullOrEmpty(vehicleValues[1]))
                        {
                            int numberOfPassengers = Convert.ToInt32(vehicleValues[1]);

                            insuredVehicleViewModel.Vehicle.NumberOfPassengers = numberOfPassengers;
                        }

                        if (!string.IsNullOrEmpty(vehicleValues[0]))
                        {
                            bool wasPolicyHolderTheDriver = Convert.ToBoolean(vehicleValues[0]);
                            insuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver = wasPolicyHolderTheDriver;
                        }

                    }

                    if (vehicle.VIN != null)
                    {
                        insuredVehicleViewModel.VehicleAdditionalInfo.VIN = vehicle.VIN;
                    }

                    if (vehicle.Colour != null)
                    {
                        insuredVehicleViewModel.VehicleAdditionalInfo.SelectedColour = (int)vehicle.Colour;
                    }

                    if (vehicle.Transmission != null)
                    {
                        insuredVehicleViewModel.VehicleAdditionalInfo.SelectedTransmission = (int)vehicle.Transmission;
                    }

                    //if (vehicle.Fuel != null)
                    //{
                    insuredVehicleViewModel.VehicleAdditionalInfo.SelectedFuel = (int)vehicle.Fuel;
                    //}

                    if (vehicle.EngineCapacity != null)
                    {
                        insuredVehicleViewModel.VehicleAdditionalInfo.EngineCapacity = vehicle.EngineCapacity;
                    }

                    if (vehicle.CategoryOfLoss != null)
                    {
                        insuredVehicleViewModel.VehicleAdditionalInfo.SelectedTotalLossCapacity = (int)vehicle.CategoryOfLoss;
                    }

                }
                #endregion

                #region Third Party Vehicle

                if (vehicle.Incident2VehicleLinkType == Incident2VehicleLinkType.ThirdPartyVehicle)
                {

                    ThirdPartyVehicleViewModel thirdPartyVehicle = new ThirdPartyVehicleViewModel();

                    //if (vehicle.VehicleType != null)
                    //{
                    thirdPartyVehicle.Vehicle.SelectedVehicletype = (int)vehicle.VehicleType;
                    //}

                    if (vehicle.VehicleRegistration != null)
                    {
                        thirdPartyVehicle.Vehicle.RegistrationNumber = vehicle.VehicleRegistration;
                    }

                    if (vehicle.Make != null)
                    {
                        thirdPartyVehicle.Vehicle.Make = vehicle.Make;
                    }

                    if (vehicle.Model != null)
                    {
                        thirdPartyVehicle.Vehicle.Model = vehicle.Model;
                    }

                    if (vehicle.EngineCapacity != null)
                    {
                        thirdPartyVehicle.VehicleAdditionalInfo.EngineCapacity = vehicle.EngineCapacity;
                    }

                    if (vehicle.CategoryOfLoss != null)
                    {
                        thirdPartyVehicle.VehicleAdditionalInfo.SelectedTotalLossCapacity = (int)vehicle.CategoryOfLoss;
                    }

                    //if (vehicle.Fuel != null)
                    //{
                    thirdPartyVehicle.VehicleAdditionalInfo.SelectedFuel = (int)vehicle.Fuel;
                    //}

                    //if (vehicle.Colour != null)
                    //{
                    thirdPartyVehicle.VehicleAdditionalInfo.SelectedColour = (int)vehicle.Colour;
                    //}

                    //if (vehicle.Transmission != null)
                    //{
                    thirdPartyVehicle.VehicleAdditionalInfo.SelectedTransmission = (int)vehicle.Transmission;
                    //}

                    if (vehicle.VIN != null)
                    {
                        thirdPartyVehicle.VehicleAdditionalInfo.VIN = vehicle.VIN;
                    }

                    if (vehicle.DamageDescription != null)
                    {

                        string damageDescription = vehicle.DamageDescription;

                        string[] vehicleValues = damageDescription.Split('|');

                        if (!string.IsNullOrEmpty(vehicleValues[1]))
                        {
                            int numberOfPassengers = Convert.ToInt32(vehicleValues[1]);

                            thirdPartyVehicle.Vehicle.NumberOfPassengers = numberOfPassengers;
                        }


                        thirdPartyVehicle.Vehicle.WasPolicyHolderTheDriver = false;
                        

                    }

                    //if (!thirdPartyVehicles.TryGetValue(thirdPartyVehicle.VehicleID, thirdPartyVehicles))
                    //{
                    //    thirdPartyVehicles.Add(thirdPartyVehicle.VehicleID, thirdPartyVehicle);
                    //}

                    thirdPartyVehicle.VehicleID = thirdPartyVehicleId;
                    thirdPartyVehicle.CurrentVehicle = thirdPartyVehicleId;

                    CurrentView thirdPartyDriverPreviousPage = CurrentView.Unknown;
                    if (thirdPartyVehicle.CurrentVehicle == 0)
                    {
                        thirdPartyVehicle.PreviousPage = CurrentView.InsuredDriver;
                    }
                    else
                    {
                        thirdPartyVehicle.PreviousPage = CurrentView.ThirdPartyVehicle;
                    }

                    thirdPartyVehicle.PreviousPage = thirdPartyDriverPreviousPage;

                    thirdPartyVehicle.NumberOfVehicles = SingleClaimRepository.NumberOfThirdPartyVehicles();


                    #region Third Party Passenger

                    int thirdPartyPassenger = 1;

                    int totalNumberOfPassengers = vehicle.People.Where(x => x.PartyType == PartyType.ThirdParty && x.SubPartyType == SubPartyType.Passenger).Count();

                    List<PassengerViewModel> thirdPartyPassengers = new List<PassengerViewModel>();

                    foreach (var p in vehicle.People)
                    {
                        //int currentVehicleKey = Convert.ToInt32(p.OtherTelephone);

                        if (p.PartyType == PartyType.ThirdParty && p.SubPartyType == SubPartyType.Passenger)
                        {
                                                      
                            PassengerViewModel passengerViewModel = new PassengerViewModel();

                            //if (p.Salutation != null)
                            //{
                            passengerViewModel.PersonInformation.SelectedSalutation = (int)p.Salutation;
                            //}

                            if (p.FirstName != null)
                            {
                                passengerViewModel.PersonInformation.Firstname = p.FirstName;
                            }

                            if (p.MiddleName != null)
                            {
                                passengerViewModel.PersonInformation.Middlename = p.MiddleName;
                            }

                            if (p.LastName != null)
                            {
                                passengerViewModel.PersonInformation.Surname = p.LastName;
                            }

                            if (p.DateOfBirth != null)
                            {
                                passengerViewModel.PersonInformation.DateOfBirth = p.DateOfBirth;
                            }

                            //if (p.Gender != null)
                            //{
                            passengerViewModel.PersonInformation.SelectedGender = (int)p.Gender;
                            //}

                            #region Bank Account

                            if (p.BankAccount != null)
                            {

                                if (p.BankAccount.AccountNumber != null)
                                {
                                    passengerViewModel.BankAccount.AccountNumber = p.BankAccount.AccountNumber;
                                }

                                if (p.BankAccount.SortCode != null)
                                {
                                    passengerViewModel.BankAccount.SortCode = p.BankAccount.SortCode;
                                }

                                if (p.BankAccount.BankName != null)
                                {
                                    passengerViewModel.BankAccount.BankName = p.BankAccount.BankName;
                                }

                                if (p.BankAccount.DatePaymentDetailsTaken != null)
                                {
                                    passengerViewModel.BankAccount.DateDetailsTaken = p.BankAccount.DatePaymentDetailsTaken;
                                }

                                if (p.BankAccount.PolicyPaymentType != null)
                                {
                                    passengerViewModel.BankAccount.SelectedAccountUsedFor = (int)p.BankAccount.PolicyPaymentType;
                                }

                            }

                            #endregion

                            if (p.LandlineTelephone != null)
                            {
                                passengerViewModel.AdditionalInfo.HomeTelephone = p.LandlineTelephone;
                            }

                            if (p.WorkTelephone != null)
                            {
                                passengerViewModel.AdditionalInfo.WorkTelephone = p.WorkTelephone;
                            }

                            if (p.EmailAddress != null)
                            {
                                passengerViewModel.AdditionalInfo.EmailAddress = p.EmailAddress;
                            }

                            if (p.Occupation != null)
                            {
                                passengerViewModel.AdditionalInfo.Occupation = p.Occupation;
                            }

                            if (p.Nationality != null)
                            {
                                passengerViewModel.AdditionalInfo.SelectedNationality = p.Nationality;
                            }

                            if (p.PassportNumber != null)
                            {
                                passengerViewModel.AdditionalInfo.PassportNumber = p.PassportNumber;
                            }

                            if (p.NINumber != null)
                            {
                                passengerViewModel.AdditionalInfo.NINumber = p.NINumber;
                            }

                            if (p.DrivingLicenseNumber != null)
                            {
                                passengerViewModel.AdditionalInfo.DrivingLicenseNumber = p.DrivingLicenseNumber;
                            }

                            if (p.MobileTelephone != null)
                            {
                                passengerViewModel.AdditionalInfo.MobileNumber = p.MobileTelephone;
                            }

                            int numberOfSupportingInvolvements = 0;

                            foreach (var org in p.Organisations)
                            {
                                numberOfSupportingInvolvements++;
                            }

                            passengerViewModel.PersonInformation.NumberOfSupportingInvolvements = numberOfSupportingInvolvements;

                            passengerViewModel.CurrentPassenger = thirdPartyPassenger;

                            passengerViewModel.PreviousPageID = thirdPartyPassenger - 1;

                            passengerViewModel.PassengerID = thirdPartyPassenger;

                            passengerViewModel.PassengerType = PassengerType.ThirdPartyVehiclePassenger;

                            passengerViewModel.NumberPassengers = totalNumberOfPassengers;

                            passengerViewModel.VehicleID = thirdPartyVehicle.CurrentVehicle;

                            //passengerViewModel.VehicleID = vehicle

                            CurrentView passengerPreviousPage = CurrentView.Unknown;

                            if (passengerViewModel.CurrentPassenger == 1)
                            {
                                passengerViewModel.PreviousPage = CurrentView.ThirdPartyDriver;
                                passengerPreviousPage = SingleClaimRepository.CheckFirstPassengerPreviousPage(passengerViewModel);
                            }
                            else
                            {
                                passengerViewModel.PreviousPage = CurrentView.Passenger;
                                passengerPreviousPage = SingleClaimRepository.CheckPreviousPassengerHasSupportingInvolvement(passengerViewModel);
                            }

                            passengerViewModel.PreviousPage = passengerPreviousPage;

                            thirdPartyPassengers.Add(passengerViewModel);



                            #region Address

                            foreach (var claimAddress in p.Addresses)
                            {

                                if (claimAddress.AddressLinkType != null)
                                {
                                    passengerViewModel.Address.SelectedAddressStatus = (int)claimAddress.AddressLinkType;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Building))
                                {
                                    passengerViewModel.Address.BuildingName = claimAddress.Building;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.BuildingNumber))
                                {
                                    passengerViewModel.Address.BuildingNumber = claimAddress.BuildingNumber;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.SubBuilding))
                                {
                                    passengerViewModel.Address.SubBuilding = claimAddress.SubBuilding;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Street))
                                {
                                    passengerViewModel.Address.Street = claimAddress.Street;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Town))
                                {
                                    passengerViewModel.Address.Town = claimAddress.Town;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.Locality))
                                {
                                    passengerViewModel.Address.Locality = claimAddress.Locality;
                                }

                                if (!string.IsNullOrEmpty(claimAddress.PostCode))
                                {
                                    passengerViewModel.Address.Postcode = claimAddress.PostCode;
                                }

                                if (claimAddress.StartOfResidency != null)
                                {
                                    passengerViewModel.Address.StartOfResidency = claimAddress.StartOfResidency;
                                }

                                if (claimAddress.EndOfResidency != null)
                                {
                                    passengerViewModel.Address.EndOfResidency = claimAddress.EndOfResidency;
                                }

                            }

                            #endregion

                            #region Organisations

                            int supportingInvolvement = 0;

                            //Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                            List<SupportingInvolvementsViewModel> ThirdPartyPassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                            foreach (var organisation in p.Organisations)
                            {

                                supportingInvolvement++;

                                SupportingInvolvementsViewModel supportingInvolvementsViewModel = new SupportingInvolvementsViewModel();


                                supportingInvolvementsViewModel.NumberSupportingInvolvements = p.Organisations.Count();
                                supportingInvolvementsViewModel.CurrentSupportingInvolvement = supportingInvolvement;
                                supportingInvolvementsViewModel.SupportingInvolvementsType = SupportingInvolvementsType.ThirdPartyPassenger;
                                supportingInvolvementsViewModel.ParentID = thirdPartyPassenger;
                                supportingInvolvementsViewModel.VehicleID = thirdPartyVehicleId;


                                if (organisation.OrganisationName != null)
                                {
                                    supportingInvolvementsViewModel.OrganisationInvolvement.OrganisationName = organisation.OrganisationName;
                                }

                                if (organisation.RegisteredNumber != null)
                                {
                                    supportingInvolvementsViewModel.OrganisationInvolvement.RegisteredNumber = organisation.RegisteredNumber;
                                }

                                if (organisation.OrganisationType != null)
                                {
                                    supportingInvolvementsViewModel.OrganisationInvolvement.SelectedOrganisationInvolvement = (int)organisation.OrganisationType;
                                }

                                #region Additional Info

                                if (organisation.Telephone1 != null)
                                {
                                    supportingInvolvementsViewModel.AdditionalInfo.Telephone1 = organisation.Telephone1;
                                }

                                if (organisation.Telephone2 != null)
                                {
                                    supportingInvolvementsViewModel.AdditionalInfo.Telephone2 = organisation.Telephone2;
                                }

                                if (organisation.Telephone3 != null)
                                {
                                    supportingInvolvementsViewModel.AdditionalInfo.Telephone3 = organisation.Telephone3;
                                }

                                if (organisation.Email != null)
                                {
                                    supportingInvolvementsViewModel.AdditionalInfo.EmailAddress = organisation.Email;
                                }

                                if (organisation.WebSite != null)
                                {
                                    supportingInvolvementsViewModel.AdditionalInfo.WebsiteAddress = organisation.WebSite;
                                }

                                if (organisation.VatNumber != null)
                                {
                                    supportingInvolvementsViewModel.AdditionalInfo.VATNumber = organisation.VatNumber;
                                }

                                if (organisation.MojCrmNumber != null)
                                {
                                    supportingInvolvementsViewModel.AdditionalInfo.MoJClaimsRegNumber = organisation.MojCrmNumber;
                                }

                                #endregion

                                #region Address

                                foreach (var address in organisation.Addresses)
                                {
                                    if (address.AddressLinkType != null)
                                    {
                                        supportingInvolvementsViewModel.Address.SelectedAddressStatus = (int)address.AddressLinkType;
                                    }

                                    if (address.Building != null)
                                    {
                                        supportingInvolvementsViewModel.Address.BuildingName = address.Building;
                                    }

                                    if (address.BuildingNumber != null)
                                    {
                                        supportingInvolvementsViewModel.Address.BuildingNumber = address.BuildingNumber;
                                    }

                                    if (address.County != null)
                                    {
                                        supportingInvolvementsViewModel.Address.County = address.County;
                                    }

                                    if (address.EndOfResidency != null)
                                    {
                                        supportingInvolvementsViewModel.Address.EndOfResidency = address.EndOfResidency;
                                    }

                                    if (address.Locality != null)
                                    {
                                        supportingInvolvementsViewModel.Address.Locality = address.Locality;
                                    }

                                    if (address.PostCode != null)
                                    {
                                        supportingInvolvementsViewModel.Address.Postcode = address.PostCode;
                                    }

                                    if (address.StartOfResidency != null)
                                    {
                                        supportingInvolvementsViewModel.Address.StartOfResidency = address.StartOfResidency;
                                    }

                                    if (address.Street != null)
                                    {
                                        supportingInvolvementsViewModel.Address.Street = address.Street;
                                    }

                                    if (address.SubBuilding != null)
                                    {
                                        supportingInvolvementsViewModel.Address.SubBuilding = address.SubBuilding;
                                    }

                                    if (address.Town != null)
                                    {
                                        supportingInvolvementsViewModel.Address.Town = address.Town;
                                    }
                                }

                                #endregion

                                #region Vehicles

                                foreach (var orgVehicle in organisation.Vehicles)
                                {
                                    if (orgVehicle.HireEndDate != null)
                                    {
                                        supportingInvolvementsViewModel.Vehicle.HireEndDate = orgVehicle.HireEndDate;
                                    }

                                    if (orgVehicle.HireStartDate != null)
                                    {
                                        supportingInvolvementsViewModel.Vehicle.HireStartDate = orgVehicle.HireStartDate;
                                    }

                                    if (orgVehicle.Make != null)
                                    {
                                        supportingInvolvementsViewModel.Vehicle.Make = orgVehicle.Make;
                                    }

                                    if (orgVehicle.Model != null)
                                    {
                                        supportingInvolvementsViewModel.Vehicle.Model = orgVehicle.Model;
                                    }

                                    if (orgVehicle.VehicleRegistration != null)
                                    {
                                        supportingInvolvementsViewModel.Vehicle.RegistrationNumber = orgVehicle.VehicleRegistration;
                                    }

                                    //if (orgVehicle.VehicleType != null)
                                    //{
                                    supportingInvolvementsViewModel.Vehicle.SelectedVehicletype = (int)orgVehicle.VehicleType;
                                    //}

                                    if (orgVehicle.VIN != null)
                                    {
                                        supportingInvolvementsViewModel.VehicleAdditionalInfo.VIN = orgVehicle.VIN;
                                    }

                                    if (orgVehicle.Colour != null)
                                    {
                                        supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedColour = (int)orgVehicle.Colour;
                                    }

                                    if (orgVehicle.Transmission != null)
                                    {
                                        supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedTransmission = (int)orgVehicle.Transmission;
                                    }

                                    if (orgVehicle.Fuel != null)
                                    {
                                        supportingInvolvementsViewModel.VehicleAdditionalInfo.SelectedFuel = (int)orgVehicle.Fuel;
                                    }

                                    if (orgVehicle.EngineCapacity != null)
                                    {
                                        supportingInvolvementsViewModel.VehicleAdditionalInfo.EngineCapacity = orgVehicle.EngineCapacity;
                                    }
                                }

                                #endregion


                                ThirdPartyPassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);


                            }

                            if (ThirdPartyPassengerSupportingInvolvements.Count > 0)
                            {
                                DictionaryThirdPartyPassengerSupportingInvolvements.Add(thirdPartyPassenger, ThirdPartyPassengerSupportingInvolvements);
                            }


                            
                            #endregion


                            if (thirdPartyPassenger == totalNumberOfPassengers)
                            {

                                dictionaryThirdPartyPassengers.Add(thirdPartyVehicle.CurrentVehicle, thirdPartyPassengers);
                            }

                            thirdPartyPassenger++;

                        }


                    }

                    if (!DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(thirdPartyVehicle.CurrentVehicle))
                    {
                        DictionaryThirdPartyVehicleSupportingInvolvements.Add(thirdPartyVehicle.CurrentVehicle, DictionaryThirdPartyPassengerSupportingInvolvements);
                    }
                    #endregion

                    if (!thirdPartyVehicles.ContainsKey(thirdPartyVehicle.CurrentVehicle))
                    {
                        thirdPartyVehicles.Add(thirdPartyVehicle.CurrentVehicle, thirdPartyVehicle);
                    }

                    thirdPartyVehicleId++;

                }
                #endregion

                #endregion

            }

            #endregion

            #endregion



            System.Web.HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = dictionaryPassengerSupportingInvolvements;
            System.Web.HttpContext.Current.Session["InsuredDriverPassengers"] = insuredDriverPassengers;
            System.Web.HttpContext.Current.Session["DictionaryThirdPartyPassengers"] = dictionaryThirdPartyPassengers;
            //System.Web.HttpContext.Current.Session["ThirdPartyPassengers"] = thirdPartyPassengers;
            System.Web.HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] = thirdPartyVehicles;
            System.Web.HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] = thirdPartyDriverViewModels;
            System.Web.HttpContext.Current.Session["DictionaryWitnessViewModels"] = witnessViewModels;

            //thirdPartyVehicles = (Dictionary<int, ThirdPartyVehicleViewModel>)HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"];

            SingleClaimRepository.StoreClaimViewModel(claimViewModel);


            return RedirectToAction("SingleClaimInputFromEditClaim");
        }

        [AjaxAuthorize]
        public JsonResult DeleteOutstandingClaim(int riskClaimId)
        {
            return Json(SingleClaimRepository.DeleteOutstandingClaim(riskClaimId));
        }

        [Authorize]
        public ActionResult OutstandingClaims(OutstandingClaimsListViewModel outstandingClaimsListViewModel)
        {

            int totalOutstanding = 0;

            if (Request.HttpMethod == "GET")
            {
                OutstandingClaimsListViewModel newOutstandingClaimsListViewModel = new OutstandingClaimsListViewModel();
                outstandingClaimsListViewModel = SingleClaimRepository.PopulateOutstandingClaimsListViewModel(false,
                                                                                                              newOutstandingClaimsListViewModel);
            }
            else
            {
                outstandingClaimsListViewModel = SingleClaimRepository.PopulateOutstandingClaimsListViewModel(true,
                                                                                                              outstandingClaimsListViewModel);
            }

            totalOutstanding = SingleClaimRepository.GetTotalSingleClaimsForClient();

            ViewData["OutstandingClaimsCount"] = totalOutstanding;

            return View(outstandingClaimsListViewModel);
        }

        public bool InsuredAddressSameAddressAsPolicyHolder(KeoghsClientSite.Models.SingleClaim.Address insuredAddress, KeoghsClientSite.Models.SingleClaim.Address policyHolderAddress)
        {

            if (insuredAddress.BuildingName == policyHolderAddress.BuildingName && insuredAddress.BuildingNumber == policyHolderAddress.BuildingNumber && insuredAddress.County == policyHolderAddress.County && insuredAddress.Locality == policyHolderAddress.Locality && insuredAddress.Postcode == policyHolderAddress.Postcode && insuredAddress.Street == policyHolderAddress.Street && insuredAddress.SubBuilding == policyHolderAddress.SubBuilding && insuredAddress.Town == policyHolderAddress.Town)
            {
                return true;
            }

            return false;
        }
    }
}
