﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult PolicyHolderPersonalNextView(PolicyHolderPersonalViewModel policyHolderPersonalViewModel)
        {
            SingleClaimRepository.StorePolicyHolderViewModel(policyHolderPersonalViewModel);
            SingleClaimRepository.Save();

            if (ModelState.IsValid)
            {
                string PolicyHolderName = "";
                PersonInformation myPerson = policyHolderPersonalViewModel.PersonInformation;


                PolicyHolderName = myPerson.Middlename != "" ? myPerson.Firstname + " " + myPerson.Middlename + " " + myPerson.Surname : myPerson.Firstname + " " + myPerson.Surname;

                InsuredVehicleViewModel insuredVehicleViewModel = SingleClaimRepository.PopulateInsuredVehicleViewModel(CurrentView.PolicyHolderPersonal, PolicyHolderName);

                return PartialView("_InsuredVehicle", insuredVehicleViewModel);
            }
            else
                return PartialView("_PolicyHolderPersonal", SingleClaimRepository.PopulatePolicyHolderPersonalViewModel());

        }


        //public PartialViewResult SubmitPolicyHolderPersonalView(PolicyHolderPersonalViewModel policyHolderPersonalViewModel)
        //{
        //    SingleClaimRepository.StorePolicyHolderViewModel(policyHolderPersonalViewModel);
        //    SingleClaimRepository.Save();

        //    PolicyViewModel policyViewModel = (PolicyViewModel)System.Web.HttpContext.Current.Session["policyViewModel"];

        //    return PartialView("_PolicyHolderPersonal", SingleClaimRepository.PopulatePolicyHolderPersonalViewModel(policyViewModel.PolicyNumber + "|" + SingleClaimRepository.GetPolicyTypeFromID(policyViewModel.SelectedPolicyType)));
        //}
    }
}