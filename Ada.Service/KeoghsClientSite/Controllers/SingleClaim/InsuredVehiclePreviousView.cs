﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult InsuredVehiclePreviousView(InsuredVehicleViewModel insuredVehicleViewModel)
        {
            SingleClaimRepository.StoreInsuredVehicleViewModel(insuredVehicleViewModel);
            
            ModelState.Clear();

            if (insuredVehicleViewModel.PreviousPage == CurrentView.Policy)
                return PartialView("_Policy", SingleClaimRepository.PopulatePolicyViewModel("", true));
            else if (insuredVehicleViewModel.PreviousPage == CurrentView.PolicyHolderPersonal)
                return PartialView("_PolicyHolderPersonal", SingleClaimRepository.PopulatePolicyHolderPersonalViewModel("", true));
            else if (insuredVehicleViewModel.PreviousPage == CurrentView.PolicyHolderCommercial)
                return PartialView("_PolicyHolderCommercial", SingleClaimRepository.PopulatePolicyHolderCommercialViewModel("", true));
            else
                return null;
        }
    }
}