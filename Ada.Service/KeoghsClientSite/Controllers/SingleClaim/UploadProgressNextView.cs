﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult UploadProgressNextView()
        {
            // Submit Single Claim
            bool submitSuccess = false;

            int riskClaimId = SingleClaimRepository.SubmitSingleClaim();

            if (riskClaimId > 0)
                submitSuccess = true;

            ViewData["SubmissionSuccessful"] = submitSuccess;
            return PartialView("_Results", ViewData["SubmissionSuccessful"]);
        }
    }
}