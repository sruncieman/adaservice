﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult PassengerPreviousView(PassengerViewModel passengerViewModel)
        {
            SingleClaimRepository.StorePassengerViewModel(passengerViewModel, passengerViewModel.PassengerType, passengerViewModel.VehicleID);
            SingleClaimRepository.Save();
            
            //Need to change PassengerViewModel previousPage if it is insured Driver where insured Driver supp involvements exist
            if (passengerViewModel.CurrentPassenger == 1)
                passengerViewModel.PreviousPage = SingleClaimRepository.CheckFirstPassengerPreviousPage(passengerViewModel);
            
            ModelState.Clear();
            if (passengerViewModel.PreviousPage == CurrentView.InsuredDriver)
            {
                // First Passenger --> Go back to previous page
                return PartialView("_InsuredDriver", SingleClaimRepository.PopulateInsuredDriverViewModel());
            }
            else if (passengerViewModel.PreviousPage == CurrentView.Passenger)
            {
                // Go back to previous Passenger
                ModelState.Clear();
                if (passengerViewModel.PassengerType == PassengerType.InsuredVehiclePassenger)
                {
                    return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(passengerViewModel.PassengerID - 1,
                                                                                                      CurrentView.Passenger, -1,
                                                                                                      PassengerType.InsuredVehiclePassenger, -1, "", true));
                }
                else
                {
                    return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(passengerViewModel.PassengerID - 1,
                                                                                                     CurrentView.Passenger, -1,
                                                                                                     passengerViewModel.PassengerType, passengerViewModel.VehicleID, "", true));
                }
            }
            else if (passengerViewModel.PreviousPage == CurrentView.ThirdPartyDriver)
            {
                return PartialView("_ThirdPartyDriver",
                  SingleClaimRepository.PopulateThirdPartyDriverViewModel(passengerViewModel.VehicleID,
                                                                          CurrentView.Passenger, "", true));
            }
            else if (passengerViewModel.PreviousPage == CurrentView.SupportingInvolvementInsuredPassenger)
            {
                // Return to previous supporting involvement of previous Passenger
                ModelState.Clear();

                int NumSupportingInvolvements = 0;
                NumSupportingInvolvements = SingleClaimRepository.GetNumberofSupportingInvolvementsForPassenger(passengerViewModel.PassengerID - 1);

                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(NumSupportingInvolvements,
                                                                                                    NumSupportingInvolvements,
                                                                                                    SupportingInvolvementsType.InsuredPassenger,
                                                                                                    passengerViewModel.PassengerID - 1, -1, "", true));
            }
            else if (passengerViewModel.PreviousPage == CurrentView.SupportingInvolvementInsuredDriver)
            {
                // Go back to last supporting involvement of Insured Driver
                // 
                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                                                                SingleClaimRepository.GetNumberofSupportingInvolvementsForInsuredDriver(),
                                                                                SupportingInvolvementsType.InsuredDriver, -1, -1, "", true));
            }
            else if (passengerViewModel.PreviousPage == CurrentView.SupportingInvolvementThirdPartyDriver)
            {
                // Go back to last supporting involvement of Third Party Driver
                return PartialView("_SupportingInvolvements",
                                    SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                                                                SingleClaimRepository.GetNumberofSupportingInvolvementsForThirdPartyDriver(passengerViewModel.VehicleID),
                                                                                SupportingInvolvementsType.ThirdPartyDriver, passengerViewModel.VehicleID, -1, "", true));
            }
            else if (passengerViewModel.PreviousPage == CurrentView.SupportingInvolvementThirdPartyPassenger)
            {
                // Go back to last supporting involvement of Third Party Passenger
                //return PartialView("_SupportingInvolvements",
                //                   SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                //                                                               SingleClaimRepository.GetNumberofSupportingInvolvementsForThirdPartyPassenger(passengerViewModel.VehicleID, passengerViewModel.CurrentPassenger),
                //                                                               SupportingInvolvementsType.ThirdPartyPassenger, passengerViewModel.CurrentPassenger, passengerViewModel.VehicleID));

                return PartialView("_SupportingInvolvements",
                                  SingleClaimRepository.PopulateSupportingInvolvementsViewModel(-1,
                                                                              SingleClaimRepository.GetNumberofSupportingInvolvementsForThirdPartyPassenger(passengerViewModel.VehicleID, passengerViewModel.CurrentPassenger - 1),
                                                                              SupportingInvolvementsType.ThirdPartyPassenger, passengerViewModel.CurrentPassenger - 1,
                                                                              passengerViewModel.VehicleID, "", true));
            }
            else
                return null;

        }

      
    }
}