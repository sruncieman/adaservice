﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult InsuredDriverNextView(InsuredDriverViewModel insuredDriverViewModel)
        {
           // insuredDriverViewModel = SingleClaimRepository.PopulateInsuredDriverViewModel("", false, "");
            SingleClaimRepository.StoreInsuredDriverViewModel(insuredDriverViewModel);
            SingleClaimRepository.Save();

            if (!ModelState.IsValid)
                return PartialView("_InsuredDriver", SingleClaimRepository.PopulateInsuredDriverViewModel());
            else
            {
                ModelState.Clear();
                string NavigationData;

                insuredDriverViewModel = SingleClaimRepository.CheckInsuredDriverPersonInformation(insuredDriverViewModel);

                PersonInformation myPerson = insuredDriverViewModel.PersonInformation;

                NavigationData = myPerson.Middlename != "" ? myPerson.Firstname + " " + myPerson.Middlename + " " + myPerson.Surname : myPerson.Firstname + " " + myPerson.Surname;

                if (insuredDriverViewModel.PersonInformation.NumberOfSupportingInvolvements == 0)
                {
                    List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
                    // Check First Passenger's previousPage isn't an insured driver supporting involvement as these no longer exist
                    List<PassengerViewModel> insuredDriverPassengers = new List<PassengerViewModel>();
                    insuredDriverPassengers = (List<PassengerViewModel>)Session["InsuredDriverPassengers"];

                    if (insuredDriverPassengers != null && insuredDriverPassengers.Count() > 0)
                        insuredDriverPassengers[0].PreviousPage = CurrentView.InsuredDriver;
                    
                    insuredDriverSupportingInvolvements = (List<SupportingInvolvementsViewModel>)Session["InsuredDriverSupportingInvolvements"];

                    if (insuredDriverSupportingInvolvements != null)
                        Session["InsuredDriverSupportingInvolvements"] = null;

                }

                if (insuredDriverViewModel.PersonInformation.NumberOfSupportingInvolvements > 0)
                {
                    return PartialView("_SupportingInvolvements",
                            SingleClaimRepository.PopulateSupportingInvolvementsViewModel(insuredDriverViewModel.PersonInformation.NumberOfSupportingInvolvements,
                                                                                            1, SupportingInvolvementsType.InsuredDriver, -1, -1, NavigationData));
                }
                else if (SingleClaimRepository.InsuredVehicleNumberPassengers() > 0)
                { // show next Passenger
                    ModelState.Clear();
                    return PartialView("_Passenger", SingleClaimRepository.PopulatePassengerViewModel(1, CurrentView.InsuredDriver,
                                                                                                    -1, PassengerType.InsuredVehiclePassenger, -1, NavigationData));
                }
                else if (SingleClaimRepository.NumberOfThirdPartyVehicles() > 0)
                {
                    return PartialView("_ThirdPartyVehicle", SingleClaimRepository.PopulateThirdPartyVehicleViewModel(1, CurrentView.InsuredDriver, NavigationData));
                }
                else if (SingleClaimRepository.NumberOfWitnesses() > 0)
                {
                    return PartialView("_Witness", SingleClaimRepository.PopulateWitnessViewModel(1, CurrentView.InsuredDriver, NavigationData));
                }
                else
                {
                    return PartialView("_ReviewAndSubmit", SingleClaimRepository.PopulateReviewAndSubmitViewModel(0,
                                                                                                               0,
                                                                                                               CurrentView.InsuredDriver, NavigationData));
                }
            }
        }

        //public PartialViewResult SubmitInsuredDriverView(InsuredDriverViewModel insuredDriverViewModel)
        //{
        //    SingleClaimRepository.StoreInsuredDriverViewModel(insuredDriverViewModel);
        //    SingleClaimRepository.Save();

        //    return PartialView("_InsuredDriver", SingleClaimRepository.PopulateInsuredDriverViewModel());
        //}
    
    }
}