﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult ThirdPartyDriverNextView(ThirdPartyDriverViewModel thirdPartyDriverViewModel)
        {
            SingleClaimRepository.StoreThirdPartyDriverViewModel(thirdPartyDriverViewModel, thirdPartyDriverViewModel.CurrentVehicle);
            SingleClaimRepository.Save();

            int NumThirdPartyVehicles = 0;
            NumThirdPartyVehicles = SingleClaimRepository.NumberOfThirdPartyVehicles();

            if (!ModelState.IsValid)
                return PartialView("_ThirdPartyDriver",
                    SingleClaimRepository.PopulateThirdPartyDriverViewModel(thirdPartyDriverViewModel.CurrentVehicle,
                                                                            CurrentView.ThirdPartyDriver));
            else
            {
                string NavigationData = SingleClaimRepository.FormatPersonName(thirdPartyDriverViewModel.PersonInformation);

                //if (insuredDriverViewModel.PersonInformation.NumberOfSupportingInvolvements == 0)
                //{
                //    List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
                //    // Check First Passenger's previousPage isn't an insured driver supporting involvement as these no longer exist
                //    List<PassengerViewModel> insuredDriverPassengers = new List<PassengerViewModel>();
                //    insuredDriverPassengers = (List<PassengerViewModel>)Session["InsuredDriverPassengers"];

                //    if (insuredDriverPassengers != null)
                //        insuredDriverPassengers[0].PreviousPage = CurrentView.InsuredDriver;

                //    insuredDriverSupportingInvolvements = (List<SupportingInvolvementsViewModel>)Session["InsuredDriverSupportingInvolvements"];

                //    if (insuredDriverSupportingInvolvements != null)
                //        Session["InsuredDriverSupportingInvolvements"] = null;
                //}


                if (thirdPartyDriverViewModel.PersonInformation.NumberOfSupportingInvolvements == 0)
                {
                    Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyDriverSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                    Dictionary<int, List<PassengerViewModel>> DictionaryThirdPartyPassengers = new Dictionary<int, List<PassengerViewModel>>();
                    DictionaryThirdPartyDriverSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)Session["DictionaryThirdPartyDriverSupportingInvolvements"];
                    List<PassengerViewModel> ThirdPartyPassengers = new List<PassengerViewModel>();

                    DictionaryThirdPartyPassengers = (Dictionary<int, List<PassengerViewModel>>)Session["DictionaryThirdPartyPassengers"];

                    if (DictionaryThirdPartyPassengers != null)
                    {
                        if (DictionaryThirdPartyPassengers.ContainsKey(thirdPartyDriverViewModel.VehicleID))
                        {
                            ThirdPartyPassengers =
                                DictionaryThirdPartyPassengers[thirdPartyDriverViewModel.VehicleID];

                            if (ThirdPartyPassengers != null && ThirdPartyPassengers.Count() > 0)
                                ThirdPartyPassengers[0].PreviousPage = CurrentView.ThirdPartyDriver;

                        }
                    }

                    if (DictionaryThirdPartyDriverSupportingInvolvements!=null)
                    {
                        DictionaryThirdPartyDriverSupportingInvolvements.Remove(thirdPartyDriverViewModel.CurrentVehicle); 
                        Session["DictionaryThirdPartyDriverSupportingInvolvements"] = DictionaryThirdPartyDriverSupportingInvolvements;
                    }
                }
                
                if (thirdPartyDriverViewModel.PersonInformation.NumberOfSupportingInvolvements > 0)
                {   // Any supporting Involvements for the third Party Driver?
                    ModelState.Clear();
                   
                    return PartialView("_SupportingInvolvements",
                        SingleClaimRepository.PopulateSupportingInvolvementsViewModel(thirdPartyDriverViewModel.PersonInformation.NumberOfSupportingInvolvements,
                                                                                              1, SupportingInvolvementsType.ThirdPartyDriver, thirdPartyDriverViewModel.CurrentVehicle,
                                                                                              thirdPartyDriverViewModel.CurrentVehicle,
                                                                                              NavigationData));
                }   // Any passengers for Third Party Vehicle
                else if (SingleClaimRepository.ThirdPartyVehicleNumberPassengers(thirdPartyDriverViewModel.CurrentVehicle) > 0)
                {
                    ModelState.Clear();
                    return PartialView("_Passenger",
                        SingleClaimRepository.PopulatePassengerViewModel(1, CurrentView.ThirdPartyDriver, -1, PassengerType.ThirdPartyVehiclePassenger,
                                                                        thirdPartyDriverViewModel.CurrentVehicle, NavigationData));
                } // Any more Third Party Vehicles?
                else if (NumThirdPartyVehicles > 1 && thirdPartyDriverViewModel.CurrentVehicle < NumThirdPartyVehicles)
                {
                    ModelState.Clear();
                    return PartialView("_ThirdPartyVehicle",
                        SingleClaimRepository.PopulateThirdPartyVehicleViewModel(thirdPartyDriverViewModel.CurrentVehicle + 1,
                                                                                    CurrentView.ThirdPartyDriver, NavigationData));
                }
                else if (SingleClaimRepository.NumberOfWitnesses() > 0)
                {
                    ModelState.Clear();
                    NavigationData += "|" + thirdPartyDriverViewModel.VehicleID;
                    return PartialView("_Witness", SingleClaimRepository.PopulateWitnessViewModel(1, CurrentView.ThirdPartyDriver, NavigationData));
                }
                else
                {
                    // any witnesses?
                    // For now display final review screen
                    ModelState.Clear();
                    NavigationData += "|" + thirdPartyDriverViewModel.VehicleID;
                    return PartialView("_ReviewAndSubmit", SingleClaimRepository.PopulateReviewAndSubmitViewModel(thirdPartyDriverViewModel.NumberOfVehicles,
                                                                                                                    thirdPartyDriverViewModel.CurrentVehicle,
                                                                                                                    CurrentView.ThirdPartyDriver, NavigationData));
                }
            }
        }
    }
}