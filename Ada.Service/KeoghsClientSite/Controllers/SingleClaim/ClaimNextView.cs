﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.Repositories.SingleClaimRepository;

namespace KeoghsClientSite.Controllers.SingleClaim
{
    public partial class SingleClaimController : Controller
    {
        [AjaxAuthorize]
        public PartialViewResult ClaimNextView(ClaimViewModel claimViewModel)
        {
            //Store selected values in session for now
            
            
            SingleClaimRepository.StoreClaimViewModel(claimViewModel);


            if (ModelState.IsValid)
                return PartialView("_Incident", SingleClaimRepository.PopulateIncidentViewModel("Motor" + "|" + claimViewModel.ClaimNumber));
            else
                return PartialView("_Claim", SingleClaimRepository.PopulateClaimViewModel());
        }
    }
}