﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
//using DotNetOpenAuth.AspNet;
//using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using KeoghsClientSite.Filters;
using KeoghsClientSite.Models;
using KeoghsClientSite.StaticClasses;
using System.Web.Script.Serialization;

#if WEBMEMBERSHIP
using System.Web.Routing;
using KeoghsClientSite.Repositories;
using System.Collections;
using MDA.WCF.WebServices.Messages;
using System.Web.Configuration;
using KeoghsClientSite.ActionFilters;
using System.Text;
using System.Net.Mail;
using System.Configuration;
#endif

namespace KeoghsClientSite.Controllers
{
    //[Authorize]
    //[InitializeSimpleMembership]
    [NoCache]
    public class AccountController : Controller
    {
#if WEBMEMBERSHIP

        public MemProviderWeb MembershipService { get; set; }
        public MemRoleProviderWeb AuthorizationService { get; set; }

        protected override void Initialize(RequestContext requestContext) 
        {
            // This calls the already instantiated provider
             MembershipService = (MemProviderWeb)Membership.Provider;

            //if (MembershipService == null)
            //    MembershipService = new MemProviderWeb();
           
            if (AuthorizationService == null)
                AuthorizationService = new MemRoleProviderWeb();
            
            base.Initialize(requestContext); 
        }
#endif
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)  //confirmed. Called b4 login
        {
            //if (SiteHelpers.SupportedBrowser()!="")
            //{
                // Do something
            // ViewBag.WarningMessage = SiteHelpers.SupportedBrowser();
            if (HttpContext.IsDebuggingEnabled)  
                ViewBag.BrowserCapabilites = SiteHelpers.BrowserCapabilities();
            //}
            //ViewBag.ReturnUrl = returnUrl;
            return View("Login");
        }



        public ActionResult ChangeClient(string id, string returnUrl)
        {
            // Check if User can change to that client
            var allowedClientIds = SiteHelpers.GetAssignedRiskClientsForUser(SiteHelpers.LoggedInUser.Id);
            if (allowedClientIds != null && allowedClientIds.All(x => x.Value != id))
            {
                throw new UnauthorizedAccessException();
            }

            Session["UserInfo"] = Repository.GetRiskUserInfo(SiteHelpers.LoggedInUser.UserName, Convert.ToInt32(id));
            Session["ClientInfo"] = Repository.GetRiskClientInfo(Convert.ToInt32(id));
            var client = Session["ClientInfo"];
            return RedirectToLocal(returnUrl);
        }
        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)   // confirmed. called after login
        {
            if (ModelState.IsValid)// && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                Session["MyMenu"] = null; 
                Session["UserInfo"] = Repository.GetRiskUserInfo(model.UserName);

                //int? activeClientCount = SiteHelpers.GetAssignedRiskClientsForUser(SiteHelpers.LoggedInUser.Id).Count();

                bool clientStatus = false;

                //if (activeClientCount > 0)
                //    clientStatus = true;

                int? userStatus =  SiteHelpers.LoggedInUser.Status;
                
                // 0 - Inactive, 1 - Active
                if(userStatus==0 || clientStatus == false)
                {
                    ModelState.AddModelError("", "Sorry this user account is currently inactive.");
                    WebSecurity.Logout(); 
                    return View(model);
                }

                DateTime? passwordResetDate = SiteHelpers.LoggedInUser.PasswordResetDate;

                int maxPasswordAgeInDay = Convert.ToInt16(ConfigurationManager.AppSettings["MaxPasswordAgeInDay"]);

                if (passwordResetDate == null || passwordResetDate.Value.AddDays(maxPasswordAgeInDay) < DateTime.Now.Date)
                    return RedirectToAction("ResetPassword", "Account", new { rt = "passwordExpired" + model.UserName });

                var activeClient = SiteHelpers.GetAssignedRiskClientsForUser(SiteHelpers.LoggedInUser.Id).FirstOrDefault();

                int clientId = 0;

                if (Int32.TryParse(activeClient.Value, out clientId))
                {
                    Session["ClientInfo"] = Repository.GetRiskClientInfo(clientId);
                    Session["UserInfo"] = Repository.GetRiskUserInfo(SiteHelpers.LoggedInUser.UserName, clientId);
                }
              
                return RedirectToLocal(returnUrl);
            }
            // If we got this far, something failed, redisplay form
            if (MembershipService.GetPasswordFailuresSinceLastSuccess(model.UserName) >= MembershipService.MaxInvalidPasswordAttempts)
                ModelState.AddModelError("", "Sorry you have exceeded the number of allowed login attempts and your account is now locked. Please email: support-ada@keoghs.co.uk");
            else
                ModelState.AddModelError("", "The user name or password provided is incorrect.");

            return View(model);
        }

        //
        // POST: /Account/LogOff

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();
            Session["MyMenu"] = null;
            Session.Clear();
            SiteHelpers.LoggedInUser = null;
            return RedirectToAction("Index", "Home");
        }


        //public JsonResult ResetPassword(string userName)
        //{
        //    var result = Repository.ResetPassword(userName);
            
        //    return Json(result);
        //}

        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string rt)
        {

            var token = Repository.ValidatePasswordResetToken(rt);

            if (token.Result == 1) // valid
            {
                ResetPasswordModel model = new ResetPasswordModel();
                model.ReturnToken = rt;
                model.UserName = token.UserName;
                return View(model);
            }

            if (rt.Contains("passwordExpired"))
            {
                rt = rt.Replace("passwordExpired", "");

                ResetPasswordModel model = new ResetPasswordModel();
                ModelState.AddModelError("", "Your password has expired and must be changed.");
                model.ReturnToken = "passwordExpired";
                model.UserName = rt;
                return View(model);
            }

            WebSecurity.Logout();
            return RedirectToAction("login", "Account");
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var token = Repository.ValidatePasswordResetToken(model.ReturnToken);

                if (token == null || token.Result == 0)
                {
                    // invalid token sent, need to cancel reset request
                    WebSecurity.Logout();
                    return RedirectToAction("login", "Account");
                }

                if (IsPreviousPassword(model.UserName,model.Password) == false)
                {
                    ModelState.AddModelError("", "The password you have entered is invalid. The new password entered has been used previously.");

                    return View(model);  
                }

                bool changePasswordSucceeded;
                try
                {
                    changePasswordSucceeded = MembershipService.ChangePassword(model.UserName, "", model.Password);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                WebSecurity.Logout();
                return RedirectToAction("login", "Account");
            }
            else
            {

                var errors = new List<string>();

                foreach (var modelState in ViewData.ModelState.Values)
                {
                    errors.AddRange(modelState.Errors.Select(error => error.ErrorMessage));
                }

                return View(model);

            }

            
        }


        public ActionResult RecoverPassword()
        {
            RecoverPassword model = new RecoverPassword();

            return View(model);
        }

        [HttpPost]
        [ActionName("RecoverPassword")]
        public ActionResult RecoverPassword(RecoverPassword model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string SubmitResult = "UserNameNotFound";

                    var result = Repository.FindUserName(model.UserName);
                    
                    if(result == 0) // Username exists so generate token and send password recovery email
                    {

                        var scheme = Request.Url.Scheme;

                        string token = Guid.NewGuid().ToString();
                        DateTime tokenExpiration = DateTime.Now.AddDays(1);
                        string resetLink = Url.Action("ResetPassword", "Account", new { rt = token }, scheme);

                        // Generae password token that will be used in the email link to authenticate user
                        var generatePasswordResetToken = Repository.GeneratePasswordResetToken(model.UserName, token, tokenExpiration, resetLink);

                        if (generatePasswordResetToken.Result == 1)
                        {
                            SubmitResult = "Success";

                            ModelState.Clear();   
                        }

                        return Json(new
                        {
                            nameret = SubmitResult,
                        }, JsonRequestBehavior.AllowGet);

                    }

                    return Json(new
                    {
                        nameret = SubmitResult,
                    }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }
            else
            {

                var errors = new List<string>();

                foreach (var modelState in ViewData.ModelState.Values)
                {
                    errors.AddRange(modelState.Errors.Select(error => error.ErrorMessage));
                }

                return Json(errors);

            }
            // If we got this far, something failed, redisplay form

            return View(model);
        }

        //
        // GET: /Account/Register

        [Authorize]
        public ActionResult Register()
        {
            bool IsAdminEnabled = false;
            
            foreach (var item in SiteHelpers.LoggedInUser.TemplateFunctions)
            {
                if(item.Key=="chkEnabled_25_ADAMAINTENANCE"){
                        IsAdminEnabled = true;
                        break;
                }
            }

            if (IsAdminEnabled)
            {
                RegisterModel model = new RegisterModel();
                //
                model.Clients = Repository.GetRiskClients();
                //
                //
                model.Roles = Repository.GetRiskRoles();

                model.Status = GetStatusSelectList();

                model.UserTypes = GetUserTypeSelectList();

                //model.Groups = GetGroupSelectList();

                //
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        [Authorize]
        [HttpGet]
        public ActionResult Unlock()
        {
            var isAdminEnabled = SiteHelpers.LoggedInUser.TemplateFunctions.Any(item => item.Key == "chkEnabled_25_ADAMAINTENANCE") || User.IsInRole("Keoghs Administrator");
            if (!isAdminEnabled) throw new UnauthorizedAccessException();


            if (Request.UrlReferrer != null)
            {
                UnlockModel model = new UnlockModel();
                model.Users = Repository.GetLockedUsers();
                return View(model);
            }

            return RedirectToAction("login", "Account");
        }

        [Authorize]
        [HttpPost]
        public ActionResult Unlock(UnlockModel unlockModel, string rowId)
        {
            var isAdminEnabled = SiteHelpers.LoggedInUser.TemplateFunctions.Any(item => item.Key == "chkEnabled_25_ADAMAINTENANCE") || User.IsInRole("Keoghs Administrator");
            if (!isAdminEnabled) throw new UnauthorizedAccessException();

            bool unlockSuccess = false;
                        
            if (!string.IsNullOrEmpty(rowId))
            {
                int userId = Int32.Parse(rowId);

                unlockSuccess = Repository.UnlockUser(userId);
                if (unlockSuccess)
                    unlockModel.UnlockSuccess = 0;
                else
                    unlockModel.UnlockSuccess = -1;

                string SubmitResult = "Success";

                ModelState.Clear();

                return Json(new
                {
                    nameret = SubmitResult,
                }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                unlockModel.UnlockSuccess = -2;
                unlockModel.Users = Repository.GetLockedUsers();
                return View(unlockModel);
            }

        }


    

        //
        // POST: /Account/Register
        [Authorize]
        [HttpPost]
        //[AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            var isAdminEnabled = SiteHelpers.LoggedInUser.TemplateFunctions.Any(item => item.Key == "chkEnabled_25_ADAMAINTENANCE") || User.IsInRole("Keoghs Administrator");
            if (!isAdminEnabled) throw new UnauthorizedAccessException();

            var user = Repository.IsUserNameUnique(model.UserName);

            if (ModelState.IsValid && user.IsUnique == true)
            {
                // Attempt to register the user
                try
                {
                    //WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    //WebSecurity.Login(model.UserName, model.Password);
                    // MembershipService.CreateUser(model.UserName, model.Password,model.Email
                    // Get the Teams for the client
                    //if (model.ClientId != null)
                    //    model.Teams = Repository.GetTeamsForClient((int)model.ClientId);

                    // IDictionary<string, object> oDictionary;
                    IDictionary<string, object> dictionary = new Dictionary<string, object>();
                    //
                    dictionary.Add("firstName", model.FirstName);
                    dictionary.Add("lastName", model.LastName);
                    dictionary.Add("telephoneNumber", model.Telephone);
                    dictionary.Add("username", model.UserName);
                    dictionary.Add("clientId", model.ClientId);
                    //dictionary.Add("teamId", model.TeamId);
                    dictionary.Add("roleId", model.RoleId);
                    dictionary.Add("statusId", model.StatusId);
                    dictionary.Add("autoLoader", model.Autoloader);
                    dictionary.Add("userType", model.UserTypeId);

                    //oDictionary=new Dictionary
                    //oDictionary.
                    //
                    MembershipService.CreateUserAndAccount(model.UserName, model.Password, false, dictionary);
                    //return RedirectToAction("Index", "Admin");
                    //return RedirectToAction("Register", new { Message = ManageMessageId.ChangePasswordSuccess });

                    string SubmitResult = "Success";

                    ModelState.Clear();

                    return Json(new
                    {
                        nameret = SubmitResult
                    }, JsonRequestBehavior.AllowGet);

                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }
            else
            {

                model.Clients = Repository.GetRiskClients();
                model.Roles = Repository.GetRiskRoles();
                model.Status = GetStatusSelectList();
                model.UserTypes = GetUserTypeSelectList();
                //model.Groups = GetGroupSelectList();

            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }
       
        //
        // POST: /Account/Disassociate

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Disassociate(string provider, string providerUserId)
        //{
        //    string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
        //    ManageMessageId? message = null;

        //    // Only disassociate the account if the currently logged in user is the owner
        //    if (ownerAccount == User.Identity.Name)
        //    {
        //        // Use a transaction to prevent the user from deleting their last login credential
        //        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
        //        {
        //            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
        //            if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
        //            {
        //                OAuthWebSecurity.DeleteAccount(provider, providerUserId);
        //                scope.Complete();
        //                message = ManageMessageId.RemoveLoginSuccess;
        //            }
        //        }
        //    }

        //    return RedirectToAction("Manage", new { Message = message });
        //}

        //
        // GET: /Account/Manage

        [Authorize]
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            //ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = true;
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            //bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            bool hasLocalAccount = true;
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");

            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        //changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                        changePasswordSucceeded = MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);

                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Authorize]
        public ActionResult ProtectWebConfigUserAccountSection()
        {
            bool IsAdminEnabled = false;

            foreach (var item in SiteHelpers.LoggedInUser.TemplateFunctions)
            {
                if(item.Key=="chkEnabled_25_ADAMAINTENANCE"){
                        IsAdminEnabled = true;
                        break;
                }
            }

            if (IsAdminEnabled)
            {
                try
                {
                    var config = WebConfigurationManager.OpenWebConfiguration("/");

                    var configSection = config.GetSection("userAccountSection");

                    configSection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");

                    config.Save();

                    ViewBag.ReturnMessage = "Web config user account section protected.";
                }
                catch (Exception excep)
                {
                    ViewBag.ReturnMessage = "Error = " + excep.Message;
                }

                //
                return View();
            }
            else
            {
                //Log user out
                WebSecurity.Logout();
                return RedirectToAction("login", "Account");
            }
        }

     

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)  // confirmed. called after login
        {
            bool checkResultsEnabled = false;
            foreach (var item in SiteHelpers.LoggedInUser.TemplateFunctions)
            {
                if(item.Key=="chkEnabled_5_RESULTS"){
                    checkResultsEnabled = true;
                }
            }
            
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                if (checkResultsEnabled)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return RedirectToAction("Index", "Upload");
                }
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        //internal class ExternalLoginResult : ActionResult
        //{
        //    public ExternalLoginResult(string provider, string returnUrl)
        //    {
        //        Provider = provider;
        //        ReturnUrl = returnUrl;
        //    }

        //    public string Provider { get; private set; }
        //    public string ReturnUrl { get; private set; }

        //    public override void ExecuteResult(ControllerContext context)
        //    {
        //        OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
        //    }
        //}

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        private static IEnumerable<SelectListItem> GetStatusSelectList()
        {
            IEnumerable<SelectListItem> ListItems = new List<SelectListItem>{
               new SelectListItem(){Text="Active", Value="1"},
               new SelectListItem(){Text="Inactive",Value="0"},
               };

            return ListItems;
        }

        private static IEnumerable<SelectListItem> GetUserTypeSelectList()
        {
            IEnumerable<SelectListItem> ListItems = new List<SelectListItem>{
               new SelectListItem(){Text="Keoghs", Value="0"},
               new SelectListItem(){Text="Client",Value="1"},
               };

            return ListItems;
        }

        private static IEnumerable<SelectListItem> GetGroupSelectList()
        {
            IEnumerable<SelectListItem> ListItems = new List<SelectListItem>{
               new SelectListItem(){Text="Admin",Value="0"},
               new SelectListItem(){Text="Keoghs Intel", Value="1"},
               new SelectListItem(){Text="Liberty", Value="2" },
               };

            return ListItems;
        }

        #endregion

        [HttpPost]
        public JsonResult doesUserNameExist(string UserName)
        {

            var user = Repository.GetRiskUserInfo(UserName);

            if (user.UserName != UserName)
            {
                return Json(user);
            }

            return Json(user == null);
        }

        [HttpPost]
        public bool IsPreviousPassword(string UserName, string Password)
        {

            var result = Repository.IsPasswordUnique(UserName, Password);

            if (result.Error == null)
            {
                return result.IsUnique;
            }

            return false;
        }
    }
}
