﻿using System;
using System.Web.Mvc;
using KeoghsClientSite.Models;  
using KeoghsClientSite.Repositories;
using System.Configuration;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.StaticClasses;
using System.Web.Security;

namespace KeoghsClientSite.Controllers
{
    [NoCache]
    [RedirectingAction]
    public class HomeController : Controller
    {
        [Authorize]
        [HttpGet]
       
        public ActionResult Index()  
        {
            int totalCount;
            int CurrentPage=1;
           
            //
            int PageSize = Convert.ToInt16(ConfigurationManager.AppSettings["ItemsPerPage"]);
            
            string SortCol="";
            bool SortAscending = false;
            int SelectedClientId = -1; //All clients
            //
            HomeViewModel oHomeViewModel = new HomeViewModel();
            // Populate models
            oHomeViewModel.claimsListViewModel = Repository.PopulateClaimsListViewModel();
            // Populate filter with defaults
            oHomeViewModel.claimsListViewModel.claimsFilterViewModel = Repository.PopulateClaimsFilterViewModel("Home", CurrentPage, PageSize, SortCol,
                                                                                                                 SortAscending,SelectedClientId);

            //
            oHomeViewModel.claimsListViewModel.Claims = Repository.GetclaimsForClient(oHomeViewModel.claimsListViewModel.claimsFilterViewModel, out totalCount);
            //
            oHomeViewModel.claimsListViewModel.claimsFilterViewModel.PagingInfo.TotalItems = totalCount;

            ViewData["LevelOneReportDelay"] = SiteHelpers.LoggedInClient.LevelOneReportDelay;

            //
            return View(oHomeViewModel);
        }
        //
        [Authorize]
        [HttpPost]
        public ActionResult Filter(ClaimsFilterViewModel oModel)  // confirmed
        {
            return View("Index", Repository.FilterModel(oModel));
        }
     
        [AjaxAuthorize]
        public JsonResult RequestLevelOneReport(int riskClaimId)
        {

            return Json(Repository.RequestLevelOneReport(riskClaimId));
            //
        }
        //
        //[Authorize]
        [AjaxAuthorize]
        public JsonResult UpdateClaimReadStatus(int riskClaimId, int riskClaimReadStatus)
        {

            return Json(Repository.UpdateClaimReadStatus(riskClaimId,riskClaimReadStatus));
            //
        }


        [AjaxAuthorize]
        public JsonResult UpdateResultsBatchListDropDown(int riskClientId)
        {
            return Json(Repository.UpdateResultBatchListDropDown(riskClientId));
        }


        public ContentResult SessionReset()
        {
            return Content("OK");
        }

        public ActionResult Font()
        {
            return View();
        }



    }
}
