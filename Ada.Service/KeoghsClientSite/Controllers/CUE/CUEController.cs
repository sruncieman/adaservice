﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.Models.CUE;
using KeoghsClientSite.Repositories.CUE;
using MDA.Pipeline.Model;
using System.Threading;
using System.IO;

namespace KeoghsClientSite.Controllers.CUE
{
    public class CUEController : Controller
    {
        public ActionResult Index(int riskClaimId)
        {
            var claimInvolvementDetails = new CueSearchEnquiryModel();

            claimInvolvementDetails = CUE_Repository.PopulateCUEInvolvements(riskClaimId);
            
            return View(claimInvolvementDetails);
        }

        [HttpPost]
        public ActionResult CUESearch(CueSearchEnquiryModel cueSearchEnquiryModel)
        {
            Thread.Sleep(1000);
           
            var personIds = new List<int>();
            
            var involvements = cueSearchEnquiryModel.CueInvolvements;

            foreach (var involvement in involvements)
            {
                foreach (var cue_database in involvement.CUE_Databases)
                {
                    if (cue_database.Database == "Cross Enquiry")
                    {
                        if (cue_database.IsChecked == true)
                        {
                            personIds.Add(involvement.PersonId);
                        }
                    }
                }
            }
            

            string SubmitResult = CUE_Repository.SubmitCUESearch(personIds, cueSearchEnquiryModel.RiskClaim_Id);



            //FileStream fs = new FileStream("CUE Search Report.pdf", FileMode.Create, FileAccess.Write, FileShare.None);

            //Document doc = new Document();

            //PdfWriter writer = PdfWriter.GetInstance(doc, fs);

            //doc.Open();

            //doc.Add(new Paragraph("Hello World"));

            //doc.Close();


            return Json(new
            {
                nameret = SubmitResult
            }, JsonRequestBehavior.AllowGet);
            //return null;

        }

        public ActionResult CUEReport(int riskClaimId)
        {
            // Return report data
            var cueReportModel = new Results();
            //var cueReportModel = new CUESearchSummary();

            cueReportModel = CUE_Repository.GetCUEReport(riskClaimId);

            //claimInvolvementDetails = CUE_Repository.PopulateCUEInvolvements(riskClaimId);

            //return View(claimInvolvementDetails);
            return View(cueReportModel);
        }
    }
}
