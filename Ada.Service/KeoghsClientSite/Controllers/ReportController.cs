﻿using System;
using System.Collections.Generic;
using System.Configuration;
//using System.Data;
//using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.Models;
using System.Web.UI.WebControls;
using KeoghsClientSite.Interfaces;
using KeoghsClientSite.StaticClasses;
using MDA.WCF.WebServices;
using MDA.WCF.WebServices.Interface;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Repositories;



namespace KeoghsClientSite.Controllers
{
    [NoCache]
    [Authorize]
    public class ReportController : Controller
    {
        [HttpGet]
        public ActionResult GetBatchReport(string Id, string ClientBatchRef)
        {

            string application = ConfigurationManager.AppSettings["ReportApplication"];
            string extn = ConfigurationManager.AppSettings["ReportExtn"];
            string reportFormat = ConfigurationManager.AppSettings["ReportFormat"];
            bool checkUserHasAccessToReport;

            Stream output;
            var ms = new MemoryStream();
            Response.ContentType = application; // "application/msword"; "application/pdf";

            checkUserHasAccessToReport = Repository.CheckUserHasAccessToReport(Convert.ToInt32(Id),
                                                                               SiteHelpers.LoggedInUser.Id, SiteHelpers.LoggedInUser.RiskClientId);

            if (!checkUserHasAccessToReport)
            {
                // User does not have permission to view this batch report
                return RedirectToAction("NoReportAccessPermission", "Error");
            }
                
            output = Repository.GetBatchReportStream(Convert.ToInt32(Id)); //, reportFormat);
         
            string mimeType = application; // "application/msword"; // "application/pdf"
            string extension = extn; // "doc"; // "pdf";
            Response.AddHeader("content-disposition", string.Format("attachment;filename=BatchLevelReport{0}.{1}", ClientBatchRef, extension));
            
            return new FileStreamResult(output, mimeType);
            
        }

        //
        [HttpGet]
        public FileResult GetLevelOneReport(string ClaimId)
        {

            string application = ConfigurationManager.AppSettings["ReportApplication"];
            string extn = ConfigurationManager.AppSettings["ReportExtn"];
            //string reportFormat = ConfigurationManager.AppSettings["ReportFormat"];

            Stream output;
            var ms = new MemoryStream();
            Response.ContentType = application; // "application/msword"; "application/pdf";

            output = Repository.GetLevelOneReportStream(Convert.ToInt32(ClaimId));

            string mimeType = application; // "application/msword"; // "application/pdf"
            string extension = extn; // "doc"; // "pdf";
            Response.AddHeader("content-disposition", string.Format("attachment;filename=LevelOneReport{0:yyyyMMdd}.{1}", DateTime.Today, extension));

            return new FileStreamResult(output, mimeType);

        }

        //[HttpPost]
        //public FileContentResult ExportExcel(string Id)
        //{
        //    Byte[] output;
        //    string extension, mimeType, encoding;
        //    extension = "pdf";
        //    ADATransferService adaTransferService = new ADATransferService();
            
        //    //string reportName = "http://ukbolintel-tst3/ReportServer/Pages/ReportViewer.aspx?%2fadaOne2&rs:Command=Render";
        //    //return File(new System.Text.UTF8Encoding().GetBytes(result), "application/pdf", reportName);
        //    output = adaTransferService.GetReportBytes(Convert.ToInt32(Id));
        //    Response.AddHeader("content-disposition", string.Format("attachment;filename=GeneratedExcelFile{0:yyyyMMdd}.{1}", DateTime.Today, extension));
        //    return new FileContentResult(output, mimeType);
        //}
        //{
        //    //IList<ParameterValue> parameters = new List<ParameterValue>();
        //    //parameters.Add(new ParameterValue { Name = "Id", Value = Id });

        //    byte[] output;
        //    string extension, mimeType, encoding;
        //    //Warning[] warnings;
        //    //string[] streamIds;
        //    //extension = "pdf";
        //    //mimeType = "application/pdf";
        //    //string reportName = "http://ukbolintel-tst3/ReportServer/Pages/ReportViewer.aspx?%2fadaOne2&rs:Command=Render";
           

        //    //ReportServices  rs = new ReportServices();
        //    //output = rs.RunReport(Id, reportName);

        //    ////ReportExporter.Export(
        //    ////    "basicHttpEndpoint" /* name of the WCF endpoint from Web.config */,
        //    ////    new NetworkCredential("userName", "secretPassword", "activeDirectoryDomainName"),
        //    ////    reportName,
        //    ////    parameters.ToArray(),
        //    ////    ExportFormat.Excel,
        //    ////    out output,
        //    ////    out extension,
        //    ////    out mimeType,
        //    ////    out encoding,
        //    ////    out warnings,
        //    ////    out streamIds
        //    ////);

        //    ////-------------------------------------------------------------
        //    //// Set HTTP Response Header to show download dialog popup
        //    ////-------------------------------------------------------------
        //    //Response.AddHeader("content-disposition", string.Format("attachment;filename=GeneratedExcelFile{0:yyyyMMdd}.{1}", DateTime.Today, extension));
        //    return new FileContentResult(output, mimeType);
        //    //
        //}

        //#region Administration Methods

        ////
        //// GET: /Reports/

        //public ActionResult Index()
        //{
        //    return View(db.Reports.ToList());
        //}

        ////
        //// GET: /Reports/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    KeoghsClientSite.Models.Report reports = db.Reports.Find(id);
        //    if (reports == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(reports);
        //}

        ////
        //// GET: /Reports/Create

        //public ActionResult Create()
        //{
        //    return View();
        //}

        ////
        //// POST: /Reports/Create

        //[HttpPost]
        //public ActionResult Create(MDA.WCF.WebServices.Reports reports)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        KeoghsClientSite.Models.Report rpt = new Models.Report()
        //        {
        //             CreateDate = reports.CreateDate,
        //             CreatedBy = reports.CreatedBy,
        //             Reference = reports.Reference,
        //             ReportId = reports.ReportId,
        //             ReportName = reports.ReportName
        //        };

        //        db.Reports.Add(rpt);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(reports);
        //}

        ////
        //// GET: /Reports/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    KeoghsClientSite.Models.Report reports = db.Reports.Find(id);
        //    if (reports == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(reports);
        //}

        ////
        //// POST: /Reports/Edit/5

        //[HttpPost]
        //public ActionResult Edit(MDA.WCF.WebServices.Reports reports)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(reports).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(reports);
        //}

        ////
        //// GET: /Reports/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    KeoghsClientSite.Models.Report reports = db.Reports.Find(id);
        //    if (reports == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(reports);
        //}

        ////
        //// POST: /Reports/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    KeoghsClientSite.Models.Report reports = db.Reports.Find(id);
        //    db.Reports.Remove(reports);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //#endregion

        protected override void Dispose(bool disposing)   // confirmed
        {
            //db.Dispose();
            base.Dispose(disposing);
        }
    }
}