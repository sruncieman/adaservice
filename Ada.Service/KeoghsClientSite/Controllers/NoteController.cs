﻿using KeoghsClientSite.Interfaces;
using KeoghsClientSite.Models.Note;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.StaticClasses;
using System.IO;


namespace KeoghsClientSite.Controllers
{
    public class NoteController : Controller
    {
        private IRepository repository;

        public NoteController(IRepository repository)
        {
            this.repository = repository;
        }

        public NoteController()
        {
            this.repository = new WorkingNoteRepository();
        }

        public ViewResult GetAll()
        {
            var notes = repository.GetAll();
            return View(notes);
        }
        
        public ActionResult Index(string claimsSelected)
        {
            Note model = new Note();
            
            claimsSelected = claimsSelected.Replace("on,", string.Empty);
            
            List<int> riskCaimIds = claimsSelected.Split(',').Select(int.Parse).ToList();

            model.RiskClaim_Id = riskCaimIds.ToArray();
            model.CreatedBy = SiteHelpers.LoggedInUser.FirstName + " " + SiteHelpers.LoggedInUser.LastName;
            model.CreatedDate = DateTime.Now;
            model.Decisions = this.repository.GetAllDecisions(SiteHelpers.LoggedInUser.RiskClientId);
            model.Visibilty = this.repository.GetAllVisibilityStatus();
            model.Deleted = false;

            model.UserTypeId = SiteHelpers.LoggedInUser.UserTypeId;

            if (riskCaimIds.Count() == 1)
            {
                model.History = this.repository.GetAll(Convert.ToInt32(claimsSelected));
            }

            if (riskCaimIds.Count() > 1)
                ViewBag.Output += "Multiple";
            else
                ViewBag.Output += "Single";

            return View(model);

        }

        [Authorize]
        [HttpPost]
        [ActionName("CreateRiskNote")]
        public ActionResult Index(Note model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if ((model.DocumentUpload != null) && (model.DocumentUpload.ContentLength > 0) && !string.IsNullOrEmpty(model.DocumentUpload.FileName))
                    {
                        //model.OriginalFile = new byte[model.DocumentUpload.ContentLength];

                        byte[] data;
                        using (Stream inputStream = model.DocumentUpload.InputStream)
                        {
                            MemoryStream memoryStream = inputStream as MemoryStream;
                            if (memoryStream == null)
                            {
                                memoryStream = new MemoryStream();
                                inputStream.CopyTo(memoryStream);
                            }
                            data = memoryStream.ToArray();
                        }

                        model.OriginalFile = data;
                        model.FileName = model.DocumentUpload.FileName;
                        model.FileType = model.DocumentUpload.ContentType;
                    }

                    this.repository.CreateNote(model);

                    string SubmitResult = "SingleSuccess";

                    if (model.RiskClaim_Id.Count() > 1)
                    {
                        SubmitResult = "MultiSuccess";
                    }                   

                    ModelState.Clear();

                    var redirectUrl = new UrlHelper(Request.RequestContext).Action("Index", "Note", new { claimsSelected = model.RiskClaim_Id[0] });
                    //return Json(new { Url = redirectUrl });

                    return Json(new
                    {
                        nameret = SubmitResult,
                        Url = redirectUrl
                    }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }
            else
            {

                var errors = new List<string>();

                foreach (var modelState in ViewData.ModelState.Values)
                {
                    errors.AddRange(modelState.Errors.Select(error => error.ErrorMessage));
                }

                return Json(errors);

            }
            // If we got this far, something failed, redisplay form

            return View(model);
        }


        public ActionResult EditRiskNote(string rowId)
        {
            if (!string.IsNullOrEmpty(rowId))
            {
                int noteId = Int32.Parse(rowId);

                //EditNote model = new EditNote();
                //return View(model);

                var riskNote = this.repository.GetRiskNote(noteId);

                Note riskNoteDetails = new Note();

                riskNoteDetails.CreatedBy = riskNote.CreatedBy;
                riskNoteDetails.CreatedDate = riskNote.CreatedDate;
                
                riskNoteDetails.History = this.repository.GetAll(Convert.ToInt32(riskNote.BaseRiskClaim_Id));
                riskNoteDetails.NoteDesc = riskNote.NoteDesc;
                riskNoteDetails.NoteId = riskNote.Id;
                List<int> riskClaimIds = new List<int>();
                riskClaimIds.Add(riskNote.RiskClaim_Id);
                riskNoteDetails.RiskClaim_Id = riskClaimIds.ToArray();
                riskNoteDetails.UserTypeId = riskNote.UserId;
                

                riskNoteDetails.Decisions = this.repository.GetAllDecisions(SiteHelpers.LoggedInUser.RiskClientId);
                riskNoteDetails.Decision_Id = riskNote.DecisionId;

                riskNoteDetails.Visibilty = this.repository.GetAllVisibilityStatus();
                riskNoteDetails.Visibilty_Id = riskNote.Visibilty;

                riskNoteDetails.UpdatedBy = SiteHelpers.LoggedInUser.FirstName + " " + SiteHelpers.LoggedInUser.LastName;
                riskNoteDetails.UpdatedDate = DateTime.Now;
                
                return View(riskNoteDetails);

            }

            return View();

        }

        [Authorize]
        [HttpPost]
        [ActionName("EditRiskNote")]
        public ActionResult EditRiskNote(Note model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    this.repository.UpdateNote(model);

                    string SubmitResult = "Success";
                    
                    ModelState.Clear();

                    var redirectUrl = new UrlHelper(Request.RequestContext).Action("Index", "Note", new { claimsSelected = model.RiskClaim_Id[0] });
                    //return Json(new { Url = redirectUrl });

                    return Json(new
                    {
                        nameret = SubmitResult,
                        Url = redirectUrl
                    }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }
            else
            {

                var errors = new List<string>();

                foreach (var modelState in ViewData.ModelState.Values)
                {
                    errors.AddRange(modelState.Errors.Select(error => error.ErrorMessage));
                }

                return Json(errors);

            }
            // If we got this far, something failed, redisplay form

            return View(model);
        }


        public ActionResult DeleteRiskNote(string rowId)
        {
            if (!string.IsNullOrEmpty(rowId))
            {
                int noteId = Int32.Parse(rowId);

                var riskNote = this.repository.GetRiskNote(noteId);

                Note riskNoteDetails = new Note();

                riskNoteDetails.CreatedBy = riskNote.CreatedBy;
                riskNoteDetails.CreatedDate = riskNote.CreatedDate;

                riskNoteDetails.History = this.repository.GetAll(Convert.ToInt32(riskNote.BaseRiskClaim_Id));
                riskNoteDetails.NoteDesc = riskNote.NoteDesc;
                riskNoteDetails.NoteId = riskNote.Id;
                List<int> riskClaimIds = new List<int>();
                riskClaimIds.Add(riskNote.RiskClaim_Id);
                riskNoteDetails.RiskClaim_Id = riskClaimIds.ToArray();
                riskNoteDetails.UserTypeId = riskNote.UserId;


                riskNoteDetails.Decisions = this.repository.GetAllDecisions(SiteHelpers.LoggedInUser.RiskClientId);
                riskNoteDetails.Decision_Id = riskNote.DecisionId;

                riskNoteDetails.Visibilty = this.repository.GetAllVisibilityStatus();
                riskNoteDetails.Visibilty_Id = riskNote.Visibilty;

                riskNoteDetails.UpdatedBy = SiteHelpers.LoggedInUser.FirstName + " " + SiteHelpers.LoggedInUser.LastName;
                riskNoteDetails.UpdatedDate = DateTime.Now;

                riskNoteDetails.Deleted = true;

                try
                {
                    this.repository.UpdateNote(riskNoteDetails);

                    string SubmitResult = "Success";

                    ModelState.Clear();

                    var redirectUrl = new UrlHelper(Request.RequestContext).Action("Index", "Note", new { claimsSelected = riskNoteDetails.RiskClaim_Id[0] });
                    //return Json(new { Url = redirectUrl });

                    return Json(new
                    {
                        nameret = SubmitResult,
                        Url = redirectUrl
                    }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }


            }

            return View();

        }

        public ActionResult OpenRiskNoteFile(string rowId)
        {
            if (!string.IsNullOrEmpty(rowId))
            {
                int noteId = Int32.Parse(rowId);

                var riskNote = this.repository.GetRiskNote(noteId);
                
                try
                {
                    return File(riskNote.OriginalFile, riskNote.FileType, riskNote.FileName);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }


            }

            return View();

        }

        //[HttpGet]
        //public ViewResult InsertNote()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ViewResult InsertNote(Note note)
        //{
        //    var noteToInsert = repository.CreateNote(note);

        //    if(noteToInsert.Result == 1)
        //        return View(1);
        //    else
        //        return View(-1);
        //}
    }
}
