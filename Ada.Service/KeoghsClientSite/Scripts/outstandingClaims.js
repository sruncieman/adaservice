﻿$(document).ready(function () {
  
    // Submit filter form when sort cols clicked
    $('.SortCol').click(function() {
        var col = $(this).attr('id');
        $('#SortColumn').val(col);
        //
        var SortAscending = $('#SortAscending').val();
        //alert('Sort Ascending =' + SortAscending);
        //
        if (SortAscending == "True")
            $('#SortAscending').val("False");
        else
            $('#SortAscending').val("True");

        $(this).closest('form')[0].submit();
    });
    // Submit filter form when page links clicked
    //$('.pagelink').click(function () {
    //    //var col = $(this).attr('id');
    //    var page = $(this).text();
    //    //alert('You clicked page ' + page);
    //    $('#PagingInfo_CurrentPage').val(page);
    //    $(this).closest('form')[0].submit();
    //});
    $('.pageprev').click(function () {
        // Get current page
        var Currpage = parseInt($('#PagingInfo_CurrentPage').val(), 10);
        if (Currpage != 1) {
            $('#PagingInfo_CurrentPage').val(Currpage - 1);
            $(this).closest('form')[0].submit();
        } else {
            return false;
        }
    });
    $('.pagenext').click(function(){
        // Get current page
        var Currpage = parseInt($('#PagingInfo_CurrentPage').val(), 10);
        var TotalPages = parseInt($('#PagingInfo_TotalPages').val(), 10);
        if (Currpage != TotalPages) {
            $('#PagingInfo_CurrentPage').val(Currpage + 1);
            $(this).closest('form')[0].submit();
        } else {
            return false;
        }
    });

    $(".EditSavedClaim").click(function () {
        var riskClaimId = $(this).attr('id');
        window.location.href = "/SingleClaim/EditSavedClaim/" + riskClaimId;
        return true;
    });
    
    $(".DeleteSavedClaim")
        .click(function () {
            riskClaimId = $(this).attr('id');
            $("#dialog-confirm_del").dialog({
                resizable: false,
                height: 140,
                modal: true,
                buttons: [
                    {
                        text: "Delete",
                        click: function () {
                            // Call ajax method 
                                $.ajax({
                                    type: "POST",
                                    url: "/SingleClaim/DeleteOutstandingClaim",
                                    dataType: 'json',
                                    contentType: "application/json; charset=utf-8",
                                    data: JSON.stringify({
                                        riskClaimId: riskClaimId
                                    }),
                                    success: function (data) {
                                        //
                                        // console.log(data);
                                        //
                                        var Result = data.Result;
                                        var Error = data.Error;
                                        //
                                        if (Result == 0) {
                                            window.location.href = "/SingleClaim/OutstandingClaims";
                                        }
                                    },
                                    error: function (result) {
                                        // alert('Error: ' + result.responseText);
                                    }
                                });
                            $(this).dialog("close");
                        }, "class": "add"
                    },
                    {
                        text: "Cancel",
                        click: function () {
                            $(this).dialog("close");
                        }, "class": "cancel"
                    }
                ]
            });
        });
});