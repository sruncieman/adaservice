﻿// Fileupload code for home page
$(document).ready(function () {
    //$("#rotate").rotate(90);
    $(function () {
        // Interval for rotating image
        var interval;
        //
        $('#fileupload')
             .bind('fileuploadchange', function (e, data) {
                 var filename = data.files[0].name;
                 filename = filename.split('\\').pop();
                 //
                 $('#filehometext').val(filename);
                 //
             })
            .bind('fileuploadsubmit', function (e, data) {

               

            })
            .bind('fileuploadstart', function (e) {
                // Display upload in progress

                $("#UploadDialog").dialog({
                    modal: true,
                    resizable: false,
                    autoResize: true,
                    width: 830,
                    //
                    open: function (event, ui) {
                        // alert('dialog open');
                        //$('#UploadDialog').find('.ui-dialog-titlebar').hide();
                        $('.ui-dialog-titlebar').hide();

                        // Disable OK button
                        $('#UploadHeader > h2').text('UPLOAD IN PROGRESS');
                        // Add h2 tag
                        $('#UploadIcon').append('<h2>LOADING </h2>');

                        //$('#UploadIcon > i').addClass('icon-x-large loading');
                        //$('#UploadIcon > img').css("visibility", "visible");
                        $('#UploadIcon > i').removeClass();
                        $('#UploadIcon > img').css("display", "inline");

                        $('#UploadFooter > h2').text('Please do not navigate away from this page');

                        // Animate loading image
                        var angle = 0;
                        interval = setInterval(function () {
                            angle += 3;
                            $("#rotate").rotate(angle);
                        }, 10);
                        //
                        e.cancelBubble = true; // for IE
                        e.stopPropagation();
                    }
                }).on("dialogclose", function (event, ui) {

                    //alert('dialog closed');
                    // Refresh the page
                    location.reload(true);
                });

            })
            .bind('fileuploaddone', function (e, data) {
                //

                e.cancelBubble = true; // for IE
                e.stopPropagation();

                // Check status of batch upload
                var BatchStatusError = data.result[0].BatchError;
                var BatchStatus = data.result[0].BatchStatus;
                var BatchStatusAsString = data.result[0].BatchStatusAsString;

                if (BatchStatusError == "") {
                    // Display success message
                    $('#UploadHeader > h2').text('FILE UPLOADED SUCCESSFULLY');
                    $('#UploadIcon > i').removeClass().addClass('icon-xxx-large uploadsuccess');
                    $('#UploadFooter > h2').text('Your batch status is: ' + BatchStatusAsString + '.');

                }
                else {
                    // An error occurred
                    if (BatchStatusAsString == "NotLoggedIn") {
                        var url = getBaseURL();
                        window.location.replace(url + 'Account/Login');
                    }
                    else {
                        if (BatchStatusError == 'Maximum request length exceeded.') {
                            //var maxSize = $('#FileUploadMaxSize').val();
                            var maxSize = parseInt($('#FileUploadMaxSize').val(), 10);
                            var maxSizeMegaBytes = maxSize / 1024;
                            BatchStatusError = 'Your file was too big to be processed, maximum size is ' + maxSizeMegaBytes.toString() + ' Megabytes.';
                        }
                        //
                        $('#UploadHeader > h2').text('FILE UPLOAD ERROR').css("margin-right","40px");
                        $('#UploadIcon > i').removeClass().addClass('icon-xx-large error');
                        // Un handled error
                        $('#UploadFooter > h2').text('Your batch upload has failed.');
                        $('#BatchError > h2').text('Error: ' + BatchStatusError);
                        $('#BatchError').show();
                    }
                }

                // Remove h2 tag
                $('#UploadIcon > h2').remove();
                // Hide loading icon
                $('#UploadIcon > img').css("display", "none");
                clearInterval(interval);

                $('#UploadIcon').removeClass().addClass('aligncenter paddingtop_12 paddingbottom_10');
                // 
                $('#FileDialogOK').css("display", "inline");

            })
            .fileupload({
                dataType: 'json',
                progressInterval: 100,
                dropZone: null, // disable drag and drop
                pasteZone: null, // Disable paste
                singleFileUploads: true,
                sequentialUploads: true,
                maxNumberOfFiles: 1,
                add: function (e, data) {
                    // Disable submit button
                    $("#FileSubmit").attr("disabled", "disabled");
                    //
                    // Validate file before submission
                    // this gets called when the file is picked
                    var FileValid = true;
                    var uploadFile = data.files[0];
                    var FileExtensions = $('#ExpectedFileExtension').val();
                    var fileExt = new RegExp('(\\.(' + FileExtensions + ')$)', "i");
                    var invalidFileMessage = FileExtensions.replace(/\|/g, ', ');

                    if (!fileExt.test(uploadFile.name)) {
                        alert('Invalid file type. Only the following file types are accepted: ' + invalidFileMessage);
                        FileValid = false;
                        // Clear file name from text box
                        $('#filehometext').val('');
                        return false;
                    }
                    else {
                        // Valid file, enable submit button
                        $("#FileSubmit").removeAttr("disabled");

                    }
                    // Magic to prevent multiple files being uploaded if any are previously selected.
                    $('#FileSubmit').unbind('click');
                    data.context = $('#FileSubmit').bind('click', function () {

                        e.preventDefault();
                        //
                        // Disable subsequent button click
                        $("#FileSubmit").attr("disabled", "disabled");
                        //
                        var IsValid = true;
                        var sError = '';
                        //
                        if (data.files.length == 0 || data.files[0].name == '') {
                            //alert('Batch file is required');
                            sError = 'Batch file is required. \r\n';
                            IsValid = false;
                        }
                        //
                        // Check that batch ref entered
                        var batchref = $('#batchref').val();
                        if (batchref == '') {
                            //alert('Batch reference is required');
                            sError += 'Batch reference is required. \r\n';
                            // submitted = false;
                            IsValid = false;
                        }
                        // Check for Unique Batch Reference
                        // Call ajax method in homeandresults.js
                        var BatchError = CheckBatchRefIsUnique(batchref);
                        //
                        if (BatchError != '') {
                            if (BatchError == 'SessionTimeout' || BatchError == 'UnknownError') {
                                sError = BatchError;
                            }
                            else {
                                sError += BatchError;
                            }
                            IsValid = false;
                        }
                        //
                        if (IsValid) {
                            data.submit();
                        }
                        else {
                            var me = $(this);
                            if (sError != 'SessionTimeout' && sError != 'UnknownError') {
                                alert(sError);
                                // Re-enable submit button
                                $("#FileSubmit").removeAttr("disabled");
                            }
                            // Display error message
                            e.cancelBubble = true; // for IE
                            e.stopPropagation();
                            return false;
                        }
                    });

                }//,

            });
        $('.NFFileButton').click(function () {
            //
            // Clear text boxes
            $('#batchref').val('');
            $('#filehometext').val('');
        });
        //
        $('#FileDialogOK').click(function () {
            $("#UploadDialog").dialog("close");
            $('#batchref').val('');
            $('#filehometext').val('');

        });
        //
        $('#FileSubmit').click(function () {
            alert("Please click the 'Browse Button' to select a file to upload and enter a 'Batch Reference'");
        });

    });
    //
    $('#filehometext').keydown(function (event) {
            if (event.which == 8) {
                event.preventDefault();
            } 
    });

});