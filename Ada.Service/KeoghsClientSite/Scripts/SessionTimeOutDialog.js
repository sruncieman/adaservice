﻿
// setup the dialog
$("#dialog").dialog({
    autoOpen: false,
    modal: true,
    height: 140,
    closeOnEscape: false,
    draggable: false,
    resizable: false,
    buttons: {
        'Yes, Keep Working': function () {
            $(this).dialog('close');
        },
        'No, Logoff': function () {
            // fire whatever the configured onTimeout callback is.
            // using .call(this) keeps the default behavior of "this" being the warning
            // element (the dialog in this case) inside the callback.
            $.idleTimeout.options.onTimeout.call(this);
        }
    }
});





// cache a reference to the countdown element so we don't have to query the DOM for it on each ping.
var $countdown = $("#dialog-countdown");
var warningLength = 60;

var webConfigTimeout = $('input#timeoutConfig').val();
var timeOutInSeconds = (webConfigTimeout * 60);
var timeOut = timeOutInSeconds - (warningLength+5); // Timeout - countdown of 60 seconds + 2 second interval


// start the idle timer plugin
$.idleTimeout('#dialog', 'div.ui-dialog-buttonpane button:first', {
    idleAfter: timeOut,
    warningLength: warningLength,
    pollingInterval: 2,
    keepAliveURL: '/Home/SessionReset',
    serverResponseEquals: 'OK',
    onTimeout: function () {
        window.location = '/Account/LogOff';
    },
    onIdle: function () {
        $(this).dialog("open");
    },
    onCountdown: function (counter) {
        $countdown.html(counter); // update the counter
    }
});

