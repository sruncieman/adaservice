﻿function UpdateNavigation() {
    // Code here to populate the Nav bar with updated data
    // Determine nav section to update
    var NavigationData = $('#NavigationData');
    var theCurrentForm = $('#CurrentForm');
    var CurrentForm = $('#CurrentForm').html();
    var Previous = $('#Previous').html();
    var PolicyHolderKnown = false;

    if (CurrentForm != null) {

        var NavData = $('#Data').html();

        var DataCols = NavData.split('|');
        var SectionID;
        //
        switch (CurrentForm) {

            case "Claim":
                ClaimForm(Previous);
                break;
            case "Incident":
                IncidentForm(DataCols,Previous);
                break;
            case "Policy":
                PolicyForm(DataCols,Previous);
                break;
            case "PolicyHolderPersonal":
                PolicyHolderForms(DataCols,Previous);
                break;
            case "PolicyHolderCommercial":
                PolicyHolderForms(DataCols,Previous);
                break;
            case "InsuredVehicle":
                InsuredVehicleForm(DataCols,Previous);
                break;
            case "InsuredDriver":
                InsuredDriverForm(DataCols,Previous);
                break;
            case "SupportingInvolvementInsuredDriver":
                SupportingInvolvementInsuredDriver(DataCols, Previous);
                break;
            case "InsuredVehiclePassenger":
                InsuredVehiclePassenger(DataCols, Previous);
                break;
            case "SupportingInvolvementInsuredPassenger":
                SupportingInvolvementInsuredPassenger(DataCols, Previous);
                break;
            case "ThirdPartyVehicle":
                ThirdPartyVehicle(DataCols, Previous);
                break;
            case "ThirdPartyDriver":
                ThirdPartyDriver(DataCols, Previous);
                break;
            case "SupportingInvolvementThirdPartyDriver":
                SupportingInvolvementThirdPartyDriver(DataCols, Previous);
                break;
            case "ThirdPartyVehiclePassenger":
                ThirdPartyVehiclePassenger(DataCols, Previous);
                break;
            case "SupportingInvolvementThirdPartyPassenger":
                SupportingInvolvementThirdPartyPassenger(DataCols, Previous);
                break;
            case "Witness":
                Witness(DataCols, Previous);
                break;
            case "ReviewAndSubmit":
                ReviewAndSubmit(DataCols, Previous);
                break;
        }
        //
        // Collapse previous nav section
        var CurrentNavSectionID = parseInt(SectionID, 10);
        //

    }
}

//
// ************************* Code to run for each form *****************************************//
//
function ClaimForm(Previous) {

    if (Previous == 'True') {

        MakeSectionActive("ClaimDiv",1);
        //
        // Collapse section after current section (first four sections always shown)
        MakeSectionInActive("IncidentDiv",2, false);
    }
}

function IncidentForm(DataCols,Previous) {

    if (Previous == 'True') {
        // 
        MakeSectionActive("IncidentDiv",2);
        //
        // Collapse section after current section (first four sections always shown)
        MakeSectionInActive("PolicyDiv",3, false);
    }
    else {
        //
        MakeSectionActive("IncidentDiv",2);
        //
        MakeSectionInActive("ClaimDiv",1, false);

        // Update claim data
        $('#claimsummary-1' + ' .DataCol1').html(DataCols[0]);
        $('#claimsummary-1' + ' .DataCol2').html(DataCols[1]);
    } 
}

function PolicyForm(DataCols,Previous) {

    MakeSectionActive("PolicyDiv",3);

    if (Previous == 'True') {
        //
        // Collapse section after current section (first four sections always shown)
        // Do we have a Policy Holder form?
        if ($('#PolicyHolderDetailsKnownYes').is(':checked')) {
            MakeSectionInActive("PolicyHolderDiv",4, false);
        }
        else {
            MakeSectionInActive("InsuredVehicleDiv",5, true);
        }
    }
    else {
        //
        MakeSectionInActive("IncidentDiv",2, false);

        // Update incident data
        $('#claimsummary-2' + ' .DataCol1').html(DataCols[0]);
        $('#claimsummary-2' + ' .DataCol2').html(DataCols[1]);
    }

}

function PolicyHolderForms(DataCols,Previous) {

    MakeSectionActive("PolicyHolderDiv", 4);
    // Display Policy holder section (this is to fix bug 9064 - Policy Holder nav section not re-appearing)
    $('.content_wrapper .claimsummary_container').eq(3).css('display', 'block');
    if (Previous == 'True') {
        //
        // Collapse section after current section 
        MakeSectionInActive("InsuredVehicleDiv",5, true);
    }
    else {
        //
        MakeSectionInActive("PolicyDiv",3, false);

        // Update policy data
        $('#claimsummary-3 .DataCol1').html(DataCols[0]);
        $('#claimsummary-3 .DataCol2').html(DataCols[1]);
    }

}

function InsuredVehicleForm(DataCols,Previous) {

    MakeSectionActive("InsuredVehicleDiv",5);

    if (Previous == 'True') {
        // Do nothing, section hasn't changed
    }
    else {
        // Collapse first four sections
        CollapseSections(4);

        if ($('#PreviousPage').val() == "PolicyHolderPersonal" || $('#PreviousPage').val() == "PolicyHolderCommercial") {
            // Update previous section Policy Holder
            $('#claimsummary-4 .DataCol1').html(DataCols[0]);
            $('#claimsummary-4 .DataCol2').html(DataCols[1]);
            //
            MakeSectionInActive("PolicyHolderDiv", 4, false);
           
        }
        else {
            // Hide PolicyHolder section
            $('.content_wrapper .claimsummary_container').eq(3).css('display', 'none');
            // Update previous section Policy 
            $('#claimsummary-3 .DataCol1').html(DataCols[0]);
            $('#claimsummary-3 .DataCol2').html(DataCols[1]);
            //
            MakeSectionInActive("PolicyDiv",3, false);
        }
    }
}

function InsuredDriverForm(DataCols,Previous) {

    if (Previous == 'True') {
        // If next page is ThirdPartyVehicle or Witness or Review --> expand and highlight section
        MakeSectionActive("InsuredVehicleDiv",5);

        MakeInsuredVehicleNextSectionInactive($('#NextPage').html());
    }
    else {
        // Update the Insured Vehicle row
        $('#InsuredVehicle .DataCol1').html(DataCols[0]);
        $('#InsuredVehicle .DataCol2').html(DataCols[1]);
        $('#InsuredVehicle .DataCol3').html(DataCols[2]);
        // Display the Supporting Involvements Header and next section Insured Driver
        //
        $('#InvolvementHeader').removeClass('display_none');
        //
        $('#InsuredDriver').removeClass('display_none');
        //
    }
}

function SupportingInvolvementInsuredDriver(DataCols, Previous) {

    if (Previous == 'True') {
        MakeInsuredVehicleNextSectionInactive($('#NextPage').html());
        MakeSectionActive("InsuredVehicleDiv", 5);
    }
    else {
        // UpdateNavData(DataCols, "SupportingInvolvementInsuredDriver");

        var DriverNameDiv = '#InsuredDriverName';
        var SupportingInvolvementsDiv = '#InsuredDriverSupportingInvolvements';

        NavDriverSupportingInvolvements(DataCols[0], DriverNameDiv, SupportingInvolvementsDiv, 'IDSupp');
    }
}

function InsuredVehiclePassenger(DataCols, Previous) {

    if (Previous == 'True') {
        // Make current section active
        MakeSectionActive("InsuredVehicleDiv",5);
        //
        MakeInsuredVehicleNextSectionInactive($('#NextPage').html());

    }
    else {
        ////NavPassenger(DataCols, $('#PassengerType').val(), "");
        UpdateNavData(DataCols, "InsuredVehiclePassenger");
    }

}

function SupportingInvolvementInsuredPassenger(DataCols, Previous) {
                    
    if (Previous == 'True') {
        MakeInsuredVehicleNextSectionInactive($('#NextPage').html());
        MakeSectionActive("InsuredVehicleDiv", 5);
    }
    else {
        //
        var CurrentSupportingInvolvement = parseInt($('#CurrentSupportingInvolvement').val(), 10);
        var NumberSupportingInvolvements = parseInt($('#NumberSupportingInvolvements').val(), 10);

        if (CurrentSupportingInvolvement == 1) {
            // Update the name of the parent Passenger
            var ParentID = $('#ParentID').val();
            NavInsuredPassengerName(ParentID, DataCols[0]);
        }
        else if (CurrentSupportingInvolvement <= NumberSupportingInvolvements) {
            // Add supporting involvement org. name for previous supporting involvement
            var ParentID = $('#ParentID').val();
            var SuppInvolvementParentDiv = '#InsuredPassengerSupportingInvolvements_Passenger' + ParentID;
            var PreviousSupportingInvolvement = CurrentSupportingInvolvement - 1;
            NavSupportingInvolvements(DataCols[0], PreviousSupportingInvolvement, SuppInvolvementParentDiv, 'IPSupp');
        }

        NavPassengerSupportingInvolvements(DataCols, $('#ParentID').val(), "", false);
    }
}

function ThirdPartyVehicle(DataCols, Previous) {

    if (Previous == 'True') {

        //if($('#VehicleID').val()==
        //MakeSectionInActive(8, false);
        //MakeSectionActive(6, $('#VehicleID').val());

    }
    else {
        //
        UpdateNavData(DataCols, "ThirdPartyVehicle");
        // Expand third Party Vehicle Section
        MakeSectionActive("ThirdPartyVehicleDiv",6, $('#VehicleID').val());

        // Collapse previous section
        if ($('#VehicleID').val() == "1") {
            //Collapse Insured Vehicle Section
            MakeSectionInActive("InsuredVehicleDiv",5, true);
        }
        else {
            // Collapse previous third party vehicle section
            var VehicleID = parseInt($('#VehicleID').val(), 10);
            var PreviousVehicleID = VehicleID - 1;
            MakeSectionInActive("ThirdPartyVehicleDiv",6, true, PreviousVehicleID);
        }
    }
}

function ThirdPartyDriver(DataCols, Previous) {

    if (Previous == 'True') {
        MakeThirdPartyVehicleNextSectionInactive($('#NextPage').html());
        MakeSectionActive("ThirdPartyVehicleDiv",6, $('#VehicleID').val());
    }
    else {
        UpdateNavData(DataCols, "ThirdPartyDriver");
    }
}

function SupportingInvolvementThirdPartyDriver(DataCols, Previous) {

    if (Previous == 'True') {
        MakeThirdPartyVehicleNextSectionInactive($('#NextPage').html());
        MakeSectionActive("ThirdPartyVehicleDiv",6, $('#VehicleID').val());
    }
    else {
        // If first supporting involvement update Third Party driver else update previous supp involvement

        var DriverNameDiv = '#ThirdPartyDriverName_Vehicle' + $('#VehicleID').val();
        var SupportingInvolvementsDiv = '#ThirdPartyDriverSupportingInvolvements_Vehicle' + $('#VehicleID').val();

        NavDriverSupportingInvolvements(DataCols[0], DriverNameDiv, SupportingInvolvementsDiv, 'TPDSupp');
    }
}

function ThirdPartyVehiclePassenger(DataCols, Previous) {

    if (Previous == 'True') {
        MakeThirdPartyVehicleNextSectionInactive($('#NextPage').html());
        MakeSectionActive("ThirdPartyVehicleDiv",6, $('#VehicleID').val());
    }
    else {
        NavPassenger(DataCols, $('#PassengerType').val(), $('#VehicleID').val());
    }
}

function SupportingInvolvementThirdPartyPassenger(DataCols, Previous) {

    if (Previous == 'True') {
        MakeThirdPartyVehicleNextSectionInactive($('#NextPage').html());
        MakeSectionActive("ThirdPartyVehicleDiv",6, $('#VehicleID').val());
    }
    else {
        NavPassengerSupportingInvolvements(DataCols, $('#ParentID').val(), $('#VehicleID').val(), true);
    }
}

function Witness(DataCols, Previous) { // Update name of previous witness in nav

    // Expand Witness Section
    MakeSectionActive("WitnessDiv", 7);

    if (Previous == 'True') {
        //
        // Make review and submit section inactive
        $('#ReviewAndSubmitDiv').removeClass('claimsummary_container_active');
        //
    }
    else {
        // Update Navigation Data
        if ($('#PreviousPage').val() == "Witness") { // Update name of previous witness in nav
            var CurrentWitness = parseInt($('#CurrentWitness').val(), 10);
            var NumberOfWitnesses = parseInt($('#NumberOfWitnesses').val(), 10);

            // Add supporting involvement org. name for previous supporting involvement

            var PreviousWitness = CurrentWitness - 1;
            var WitnessName = DataCols[0];
            var WitnessInvolvementType = "Witness";

            NavUpdateWitness(WitnessName, WitnessInvolvementType, PreviousWitness);
            //
            if (CurrentWitness <= NumberOfWitnesses) {
                if ($('#Witness' + CurrentWitness).length == 0) {
                    //
                    var Template = $('#WitnessTemplate').html();
                    Template = Template.replace('WitnessX', 'Witness' + CurrentWitness);
                    Template = Template.replace('WitnessInvolvementTypeX', 'WitnessInvolvementType' + CurrentWitness);
                    Template = Template.replace('WitnessNameX', 'WitnessName' + CurrentWitness);
                    //
                    $('#claimsummary-7').append(Template);
                    //
                }
            }
        }
        else {
            UpdateNavData(DataCols, "Witness");
        }
        //
        CollapseWitnessOrReviewPreviousSection();

        // Witness is last div in SingleClaimNav
        // Collapse previous one
        // If there is a Third Party Vehicle visible Collapse Section 
        // Need to collapse the last third party vehicle entered
        //var ThirdPartyVehicles = $('div[id*="ThirdPartyVehicleDiv_Vehicle"]');

        //// Get last third party vehicle div
        //// last vehicle will be the template so get penultimate one
        //var tempVehicle = ThirdPartyVehicles.eq(-2);

        //// Get ID of last vehicle
        //var tempVehicleId = tempVehicle.attr('id');

        //// Get VehicleId
        //var VehicleID = tempVehicleId.charAt(tempVehicleId.length - 1)

        //// Is third party vehicle div visible?
        //if (IsSectionVisible(tempVehicleId)) {
        //    // We have a third party vehicle, collapse section
        //    MakeSectionInActive("ThirdPartyVehicleDiv",6, true, VehicleID);
        //}
        //else{
        //    // Make Insured vehicle section inactive
        //    MakeSectionInActive("InsuredVehicleDiv",5, true);
        //}

    }
}

function ReviewAndSubmit(DataCols, Previous) {

    // Highlight Review Section
    $('#ReviewAndSubmitDiv').addClass('claimsummary_container_active');

    if (Previous == 'True') {
        // 
    }
    else {
        // Update Nav data
        if ($('#PreviousPage').val() == "Witness") {
            // Update last witness 
            var WitnessID = DataCols[2];
            var WitnessName = DataCols[0];
            var WitnessInvolvementType = DataCols[1];

            NavUpdateWitness(WitnessName, WitnessInvolvementType, WitnessID);
        }
        else {
            UpdateNavData(DataCols, "ReviewAndSubmit");
        }
        //
        // Collapse previous section
        // Can be Witness section, Third Party Vehicle or Insured vehicle

        // if there is a Witness section, make it inactive
        if (IsSectionVisible("WitnessDiv")) {
            MakeSectionInActive("WitnessDiv",7, true);
        }
        else {
            // Collapse Third Party Vehicle or Insured Vehicle Section 
            CollapseWitnessOrReviewPreviousSection();
        }


    }
}

//
// ************************* Miscellaneous functions *****************************************//
//

function UpdateNavData(DataCols, CurrentForm) {

    //
    // ************************* Update data Nav for previous form *****************************************//
    //
    if ($('#PreviousPage').val() == "InsuredDriver") {
        $('#InsuredDriverName p').html(DataCols[0]);
        
    }
    else if ($('#PreviousPage').val() == "SupportingInvolvementInsuredDriver") {
        // add or update last supporting involvement for insured driver
        var LastInsuredDriverSupportingInvolvement = DataCols[1];

        NavSupportingInvolvements(DataCols[0], LastInsuredDriverSupportingInvolvement, '#InsuredDriverSupportingInvolvements', 'IDSupp'); 

    }
    else if ($('#PreviousPage').val() == "Passenger") {
        // Update last passenger 
        // If there is a third party vehicle update third party vehicle passenger 
        // else update insured vehicle passenger

        if (DataCols.length == 3) {
            // We have a VehicleID
            NavThirdPartyPassengerName(DataCols[1], DataCols[2], DataCols[0]);
        } else {
            NavInsuredPassengerName(DataCols[1], DataCols[0]);
        }
        //
    }
    else if ($('#PreviousPage').val() == "SupportingInvolvementInsuredPassenger") {
        // Add supporting involvement org. name for last supporting involvement of last passenger
        // DataCols[1]  holds supporting involvement ID
        // DataCols[2] holds the Passenger ID
        var SuppInvolvementParentDiv = '#InsuredPassengerSupportingInvolvements_Passenger' + DataCols[2];
        NavSupportingInvolvements(DataCols[0], DataCols[1], SuppInvolvementParentDiv, 'IPSupp');
        //
       
    }
    else if ($('#PreviousPage').val() == "ThirdPartyDriver") {
        // Update Third Party Driver name
        // Are we updating the driver in the current vehicle or previous one??

        var VehicleID;
        //
        if (CurrentForm == "Witness" || CurrentForm == "ReviewAndSubmit") {
            VehicleID= DataCols[1];
        }
        else{
            VehicleID= parseInt($('#VehicleID').val(), 10);
        }
        //
        if (CurrentForm == "ThirdPartyVehicle") {
            // We are in next vehicle need to update previous
            VehicleID -= 1;
        }
        //
        var ThirdPartydriverName = DataCols[0];
        //var VehicleID = DataCols[1];
        //var VehicleID = $('#VehicleID').val();

        NavUpdateThirdPartyDriverName(ThirdPartydriverName, VehicleID);
        //
    }
    else if ($('#PreviousPage').val() == "SupportingInvolvementThirdPartyDriver") {
        // Update last supporting involvement of last third party vehicel

        // var DriverNameDiv = '#ThirdPartyDriverName_Vehicle' + $('#VehicleID').val();

        var SupportingInvolvementsDiv = '#ThirdPartyDriverSupportingInvolvements_Vehicle' + DataCols[3];

        //NavDriverSupportingInvolvements(DataCols[0], DriverNameDiv, SupportingInvolvementsDiv, 'TPDSupp');
        var SuppInvolvementNumber = DataCols[1];

        NavSupportingInvolvements(DataCols[0], SuppInvolvementNumber, SupportingInvolvementsDiv, 'TPDSupp')

    }
    else if ($('#PreviousPage').val() == "SupportingInvolvementThirdPartyPassenger") {

        var SuppInvolvementNumber = DataCols[1];
        var PassengerID = DataCols[2];
        var VehicleID = DataCols[3];

        //ThirdPartyPassengerSupportingInvolvements_Passenger1_Vehicle1

        var SupportingInvolvementsDiv = '#ThirdPartyPassengerSupportingInvolvements_Passenger' + PassengerID + '_Vehicle' + VehicleID;

        NavSupportingInvolvements(DataCols[0], SuppInvolvementNumber, SupportingInvolvementsDiv, 'TPPSupp')

        //NavPassengerSupportingInvolvements(DataCols, $('#ParentID').val(), $('#VehicleID').val(), true);
    }
    else if ($('#PreviousPage').val() == "ThirdPartyVehicle") {
        var VehicleID = $('#VehicleID').val();

        // Update third party vehicle
        $('#ThirdPartyVehicleReg_Vehicle' + VehicleID).html(DataCols[0]);
        $('#ThirdPartyVehicleMake_Vehicle' + VehicleID).html(DataCols[1]);
        $('#ThirdPartyVehicleModel_Vehicle' + VehicleID).html(DataCols[2]);
        // Display the Supporting Involvements Header and next section Insured Driver
        //
        $('#ThirdPartyVehicleInvolvementHeader_Vehicle' + VehicleID).removeClass('display_none');
        //
        $('#ThirdPartyDriver_Vehicle' + VehicleID).removeClass('display_none');
    }
    //
    // ************************* Create any extra nav elements *****************************************//
    //
    if (CurrentForm == "ThirdPartyVehiclePassenger") {
        // Show icon for Passenger
        $('#ThirdPartyPassenger_Passenger1' + '_Vehicle' + $('#VehicleID').val()).removeClass('display_none');
    }
    // Create html for next Passenger row
    if (CurrentForm == "InsuredVehiclePassenger") {

        // Show icon for Passenger
        $('#InsuredPassenger_Passenger1').removeClass('display_none');

        var CurrentPassenger = parseInt($('#CurrentPassenger').val(), 10);

        if ($('#InsuredPassenger_Passenger' + CurrentPassenger).length == 0) {
            //
            var Template = $('#InsuredPassengerTemplate').html();
            Template = Template.replace('_InsuredPassenger_PassengerX', 'InsuredPassenger_Passenger' + CurrentPassenger);
            Template = Template.replace('_InsuredPassengerName_PassengerX', 'InsuredPassengerName_Passenger' + CurrentPassenger);
            Template = Template.replace('_InsuredPassengerSupportingInvolvements_PassengerX', 'InsuredPassengerSupportingInvolvements_Passenger' + CurrentPassenger);
            //
            $('#claimsummary-5').append(Template);
            //
        }
    }
    else if(CurrentForm == "ThirdPartyPassenger"){
        // Third party passenger
        var CurrentPassenger = parseInt($('#CurrentPassenger').val(), 10);
        var VehicleID = $('#VehicleID').val();

        if ($('#ThirdPartyPassenger_Passenger' + CurrentPassenger + '_Vehicle' + VehicleID).length == 0) {
            //
            var Template = $('#InsuredPassengerTemplate').html();
            Template = Template.replace('_InsuredPassenger_PassengerX', 'ThirdPartyPassenger_Passenger' + CurrentPassenger + '_Vehicle' + VehicleID);
            Template = Template.replace('_InsuredPassengerName_PassengerX', 'ThirdPartyPassengerName_Passenger' + CurrentPassenger + '_Vehicle' + VehicleID);
            Template = Template.replace('_InsuredPassengerSupportingInvolvements_PassengerX', 'ThirdPartyPassengerSupportingInvolvements_Passenger' + CurrentPassenger + '_Vehicle' + VehicleID);
            //
            $('#claimsummary-6_Vehicle' + VehicleID).append(Template);
            //
        }
    }
    else if (CurrentForm == "ThirdPartyVehicle") {

        var VehicleID = $('#VehicleID').val();

        if ($('#claimsummary-6_Vehicle' + VehicleID).length == 0 && VehicleID != "1") {
            //
            // Add new section from template
            $('#ThirdPartyVehicleTemplate .ThirdPartyVehicleHeading').html('THIRD PARTY VEHICLE ' + VehicleID);
            var Template = $('#ThirdPartyVehicleTemplate').html();

            Template = Template.replace(/_VehicleX/g, '_Vehicle' + VehicleID);

            // Find index of last third party vehicle
            var intVehicleID = parseInt(VehicleID, 10);
            var PreviousVehicleID = intVehicleID - 1;

            var ClaimSummary;
            var IndexClaimSummaryContainer;

            IndexClaimSummaryContainer = $('#claimsummary-6_Vehicle' + PreviousVehicleID).parent().index();

            $(Template).insertAfter($('#claimsummary-6_Vehicle' + PreviousVehicleID).parent());

            //
        }
    }


}

// Makes the next nav section after the Insured vehicle inactive.
// This is required when going backwards in the wizard
function MakeInsuredVehicleNextSectionInactive(NextPage) {

    if (NextPage == "ThirdPartyVehicle") {
        // Collapse Third Party Vehicle Section
        MakeSectionInActive("ThirdPartyVehicleDiv",6, true,'1'); // Pass VehicleID=1 for first third party vehicle
    }
    if (NextPage == "Witness") {
        // Collapse Third Party Vehicle Section
        MakeSectionInActive("WitnessDiv",7, true);
    }
    if (NextPage == "ReviewAndSubmit") {
        // Collapse Third Party Vehicle Section
        MakeSectionInActive("ReviewAndSubmitDiv",8, false);
    }

}

// Makes the next nav section after the Third Party Vehicle inactive.
// This is required when going backwards in the wizard
function MakeThirdPartyVehicleNextSectionInactive(NextPage) {

    if (NextPage == "Witness") {
        // Collapse Third Party Vehicle Section
        MakeSectionInActive("WitnessDiv",7, true);
    }
    else if (NextPage == "ThirdPartyVehicle") {

        // Collapse next third party vehicle in list
        var CurrentVehicleID = parseInt($('#VehicleID').val(), 10);
        var NextVehicleID = CurrentVehicleID + 1;

        MakeSectionInActive("ThirdPartyVehicleDiv", 6, true, NextVehicleID);

    }
    else if (NextPage == "ReviewAndSubmit") {
        // Make Review and Submit section inactive
        MakeSectionInActive("ReviewAndSubmitDiv", 7, false, null);

    }

}

// Collapse the Nav section before the Witness or review pages
// When the user navigates to the Witness or Review forms this function
// Collapses the nav section for the previous page
// This will be either an third party vehicle section ior insured vehicle section
function CollapseWitnessOrReviewPreviousSection() {

    // Collapse previous one
    // If there is a Third Party Vehicle visible Collapse Section 
    // Need to collapse the last third party vehicle entered
    var ThirdPartyVehicles = $('div[id*="ThirdPartyVehicleDiv_Vehicle"]');

    // Get last third party vehicle div
    // last vehicle will be the template so get penultimate one
    var tempVehicle = ThirdPartyVehicles.eq(-2);

    // Get ID of last vehicle
    var tempVehicleId = tempVehicle.attr('id');

    // Get VehicleId
    var VehicleID = tempVehicleId.charAt(tempVehicleId.length - 1)

    // Is third party vehicle div visible?
    if (IsSectionVisible(tempVehicleId)) {
        // We have a third party vehicle, collapse section
        MakeSectionInActive("ThirdPartyVehicleDiv", 6, true, VehicleID);
    }
    else {
        // Make Insured vehicle section inactive
        MakeSectionInActive("InsuredVehicleDiv", 5, true);
    }

}

function IsSectionVisible(SectionDiv) {

    if ($('#' + SectionDiv).css('display') == 'block') {
        return true;
    } else {

        return false;
    }
}

function CollapseSections(SectionMax) {

    // Collapse all sections prior to current section
    for (i = 1; i <= SectionMax; i++) {
        if ($('#' + 'claimsummary-' + i).css('display') == 'block') {
            divShowHide('claim_accordion-' + i, 'claimsummary-' + i, 'accordion_uparrow', 'accordion_downarrow');
        }
    }

}
function MakeSectionActive(SectionDiv,SectionID,VehicleID) {

    // Display current section
    if (VehicleID != null) {
        SectionID = SectionID + '_Vehicle' + VehicleID;
        SectionDiv = SectionDiv + '_Vehicle' + VehicleID;
    }
    //
    if ($('#' + 'claimsummary-' + SectionID).css('display') == 'none') {
        divShowHide('claim_accordion-' + SectionID, 'claimsummary-' + SectionID, 'accordion_uparrow', 'accordion_downarrow');
    }
    // Add class to display active section
    $('#' + SectionDiv).addClass('claimsummary_container_active');

}

function MakeSectionInActive(SectionDiv, SectionID, bCollapse, VehicleID) {

    if (VehicleID != null) {
        SectionID = SectionID + '_Vehicle' + VehicleID;
        SectionDiv = SectionDiv + '_Vehicle' + VehicleID;
    }

    if (bCollapse) {
        if ($('#' + 'claimsummary-' + SectionID).css('display') == 'block') {
            divShowHide('claim_accordion-' + SectionID, 'claimsummary-' + SectionID, 'accordion_uparrow', 'accordion_downarrow');
        }
    }
    //Remove active class from previous section ( the one after the current one )
    $('#' + SectionDiv).removeClass('claimsummary_container_active');

}

function NavPassengerSupportingInvolvements(DataCols,PassengerID,VehicleID,ThirdPartyPassenger) {

    var CurrentSupportingInvolvement = parseInt($('#CurrentSupportingInvolvement').val(), 10);
    var NumberSupportingInvolvements = parseInt($('#NumberSupportingInvolvements').val(), 10);
    var Data = DataCols[0];
    var CssClassStub;

    if (CurrentSupportingInvolvement == 1) {
        // Update the name of the parent Passenger
        //var ParentID = $('#ParentID').val();
        if (ThirdPartyPassenger) {
            NavThirdPartyPassengerName(PassengerID, VehicleID, Data);
        } else {
            NavInsuredPassengerName(PassengerID, Data);
        }
    }
    else if (CurrentSupportingInvolvement <= NumberSupportingInvolvements) {
        // Add supporting involvement name for previous supporting involvement
        //var ParentID = $('#ParentID').val();

        if (ThirdPartyPassenger) {
            CssClassStub = 'TPPSupp';
            var SuppInvolvementParentDiv = '#ThirdPartyPassengerSupportingInvolvements_Passenger' + PassengerID + '_Vehicle' + VehicleID;
        } else {
            CssClassStub = 'IPSupp';
            var SuppInvolvementParentDiv = '#InsuredPassengerSupportingInvolvements_Passenger' + PassengerID;
        }

        var PreviousSupportingInvolvement = CurrentSupportingInvolvement - 1;

        NavSupportingInvolvements(Data, PreviousSupportingInvolvement, SuppInvolvementParentDiv, CssClassStub);
    }

}

function NavDriverSupportingInvolvements(NavData,DriverNameDiv, SupportingInvolvementsDiv, CssClassStub) {

    var CurrentSupportingInvolvement = parseInt($('#CurrentSupportingInvolvement').val(), 10);
    var NumberSupportingInvolvements = parseInt($('#NumberSupportingInvolvements').val(), 10);

    if (CurrentSupportingInvolvement == 1) {
        // Update the name of the Insured Driver
        $(DriverNameDiv + ' p').html(NavData);
        //NavSupportingInvolvements(NavData, PreviousSupportingInvolvement, SupportingInvolvementsDiv, CssClassStub);
    }
    else if (CurrentSupportingInvolvement <= NumberSupportingInvolvements) {
        // Add supporting involvement org. name for previous supp involvement
        var PreviousSupportingInvolvement = CurrentSupportingInvolvement - 1;
        // 
        NavSupportingInvolvements(NavData, PreviousSupportingInvolvement, SupportingInvolvementsDiv, CssClassStub);
    }

}

function NavSupportingInvolvements(NavData, SuppInvolvementNumber, SuppInvolvementParentDiv, CssClassStub) {

    if ($(SuppInvolvementParentDiv + ' .' + CssClassStub + SuppInvolvementNumber).length == 0) {
        $(SuppInvolvementParentDiv).append('<p class="' + CssClassStub + SuppInvolvementNumber + '">' + NavData + '</p>');
    }
    else {
        $(SuppInvolvementParentDiv + ' .' + CssClassStub + SuppInvolvementNumber).html(NavData);
    }

}

function NavUpdateWitness(WitnessName, InvolvementType, WitnessID) {

    $('#WitnessName' + WitnessID + ' p').html(WitnessName);
    $('#WitnessInvolvementType' + WitnessID + ' p').html(InvolvementType);

}

function NavUpdateThirdPartyDriverName(DriverName, VehicleID) {

    $('#ThirdPartyDriverName_Vehicle' + VehicleID + ' p').html(DriverName);

}

function NavInsuredPassengerName(PassengerID, PassengerName) {

    if ($('#InsuredPassengerName_Passenger' + PassengerID).length == 0) {
        $('#InsuredPassengerName_Passenger' + PassengerID).append('<p>' + PassengerName + '</p>');
    }
    else {
        $('#InsuredPassengerName_Passenger' + PassengerID).html('<p>' + PassengerName + '</p>');
    }

}

function NavThirdPartyPassengerName(PassengerID, VehicleID, PassengerName) {

    if ($('#ThirdPartyPassengerName_Passenger' + PassengerID + '_Vehicle' + VehicleID).length == 0) {
        $('#ThirdPartyPassengerName_Passenger' + PassengerID + '_Vehicle' + VehicleID).append('<p>' + PassengerName + '</p>');
    }
    else {
        $('#ThirdPartyPassengerName_Passenger' + PassengerID + '_Vehicle' + VehicleID).html('<p>' + PassengerName + '</p>');
    }

}

function NavPassenger(DataCols, PassengerType, VehicleID) {

    var PrevPageDriver;
    var PrevPageSupportingInvolvementDriver;
    var PrevPagePassenger;
    var PrevPageSupportingInvolvementPassenger;

    var PassengerSupportingInvolvementsDiv;

    var DriverSuppInvolvementClassStub;
    var PassengerSuppInvolvementClassStub;

    if (PassengerType == "InsuredVehiclePassenger") {

        PrevPageDriver = "InsuredDriver";
        PrevPageSupportingInvolvementDriver = "SupportingInvolvementInsuredDriver";
        PrevPagePassenger = "Passenger";
        PrevPageSupportingInvolvementPassenger = "SupportingInvolvementInsuredPassenger";

        PassengerSupportingInvolvementsDiv = "#InsuredPassengerSupportingInvolvements_Passenger";

        DriverSuppInvolvementClassStub = 'IDSupp';
        PassengerSuppInvolvementClassStub = 'IPSupp';

    }
    else {
        // Third party vehicle
        PrevPageDriver = "ThirdPartyDriver";
        PrevPageSupportingInvolvementDriver = "SupportingInvolvementThirdPartyDriver";
        PrevPagePassenger = "Passenger";
        PrevPageSupportingInvolvementPassenger = "SupportingInvolvementThirdPartyPassenger";

        PassengerSupportingInvolvementsDiv = "#ThirdPartyPassengerSupportingInvolvements";

        DriverSuppInvolvementClassStub = 'TPDSupp';
        PassengerSuppInvolvementClassStub = 'TPPSupp';

    }

    var Data = DataCols[0];

    var CurrentPassenger = parseInt($('#CurrentPassenger').val(), 10);
    var NumberPassengers = parseInt($('#NumberPassengers').val(), 10);

    if (CurrentPassenger == 1) {
        if ($('#PreviousPage').val() == PrevPageDriver) {

            if (PassengerType == "InsuredVehiclePassenger") {
                // Update Insured Driver name
                $('#' + PrevPageDriver + 'Name' + ' p').html(Data);
                // Show icon for Passenger
                $('#InsuredPassenger_Passenger1').removeClass('display_none');
            }
            else {
                // Update third party driver name
                $('#' + PrevPageDriver + 'Name' + '_Vehicle' + VehicleID + ' p').html(Data);
                // Show icon for Passenger
                $('#ThirdPartyPassenger_Passenger1' + '_Vehicle' + VehicleID).removeClass('display_none');
            }
        }
        else if ($('#PreviousPage').val() == PrevPageSupportingInvolvementDriver) {
            
            var SuppInvolvementNumber = DataCols[1];
            var SupportingInvolvementDiv;
            if (PassengerType == "InsuredVehiclePassenger") {
                // add or update last supporting involvement for insured driver
                SupportingInvolvementDiv = '#' + PrevPageDriver + 'SupportingInvolvements';
                // Show icon for Passenger
                $('#InsuredPassenger_Passenger1').removeClass('display_none');
            }
            else { // Third Party Driver
                SupportingInvolvementDiv = '#' + PrevPageDriver + 'SupportingInvolvements' + '_Vehicle' + VehicleID;
                // Show icon for Passenger
                $('#ThirdPartyPassenger_Passenger1' + '_Vehicle' + VehicleID).removeClass('display_none');
            }
            NavSupportingInvolvements(Data, SuppInvolvementNumber, SupportingInvolvementDiv, DriverSuppInvolvementClassStub);
        }
        
    }
    else if (CurrentPassenger <= NumberPassengers) {
        // 
        var PreviousPassenger = CurrentPassenger - 1;

        if ($('#PreviousPage').val() == PrevPagePassenger) {
            // Add data for previous Passenger Name to nav
            if (PassengerType == "InsuredVehiclePassenger") {

                NavInsuredPassengerName(PreviousPassenger, Data);
            }
            else {
                NavThirdPartyPassengerName(PreviousPassenger, VehicleID, Data);
            }
        }
        else if ($('#PreviousPage').val() == PrevPageSupportingInvolvementPassenger) {
            // Add/Update previous supporting involvement for previous passenger
           
            var SuppInvolvementNumber = DataCols[1];

            if (PassengerType == "InsuredVehiclePassenger") {

                PassengerSupportingInvolvementsDiv = PassengerSupportingInvolvementsDiv + PreviousPassenger;
            }
            else {
                PassengerSupportingInvolvementsDiv = PassengerSupportingInvolvementsDiv + '_Passenger' + PreviousPassenger + '_Vehicle' + VehicleID;
            }

            NavSupportingInvolvements(Data, SuppInvolvementNumber, PassengerSupportingInvolvementsDiv, PassengerSuppInvolvementClassStub);
        }

        // Create html for next Passenger row
        if (PassengerType == "InsuredVehiclePassenger") {
            if ($('#InsuredPassenger' + CurrentPassenger).length == 0) {
                //
                var Template = $('#InsuredPassengerTemplate').html();
                Template = Template.replace('_InsuredPassenger_PassengerX', 'InsuredPassenger_Passenger' + CurrentPassenger);
                Template = Template.replace('_InsuredPassengerName_PassengerX', 'InsuredPassengerName_Passenger' + CurrentPassenger);
                Template = Template.replace('_InsuredPassengerSupportingInvolvements_PassengerX', 'InsuredPassengerSupportingInvolvements_Passenger' + CurrentPassenger);
                //
                $('#claimsummary-5').append(Template);
                //
            }
        }
        else {

            if ($('#ThirdPartyPassenger_Passenger' + CurrentPassenger + '_Vehicle' + VehicleID).length == 0) {
                //
                var Template = $('#InsuredPassengerTemplate').html();
                Template = Template.replace('_InsuredPassenger_PassengerX', 'ThirdPartyPassenger_Passenger' + CurrentPassenger + '_Vehicle' + VehicleID);
                Template = Template.replace('_InsuredPassengerName_PassengerX', 'ThirdPartyPassengerName_Passenger' + CurrentPassenger + '_Vehicle' + VehicleID);
                Template = Template.replace('_InsuredPassengerSupportingInvolvements_PassengerX', 'ThirdPartyPassengerSupportingInvolvements_Passenger' + CurrentPassenger + '_Vehicle' + VehicleID);
                //
                $('#claimsummary-6_Vehicle' + VehicleID).append(Template);
                //
            }

        }
        // }
    }
}
