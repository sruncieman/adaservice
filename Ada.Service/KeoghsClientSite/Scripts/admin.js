﻿
$(document).ready(function () {
    
    //$("#SubmitButton").live("click", function (e) {
    $("#registrationForm").submit(function (e) {
        e.preventDefault();
        $("#registrationForm").validate();

            $.ajax({
                type: "POST",
                url: "/Account/Register",
                data: $("form").serializeArray(),
                success: function (data) {
                    var retName = data.nameret;
                    if (retName == "Success") {
                        $('#registrationForm')[0].reset();
                        $('#CreateSuccess').show();
                    }
                },

                error: function (data) {
                    $('#CreateError').show();
                }
            });

    });

    $("#edituserdetailsForm").submit(function (e) {
        e.preventDefault();
        $("#edituserdetailsForm").validate();

        $.ajax({
            type: "POST",
            url: "/Admin/EditUserDetails",
            data: $("form").serializeArray(),
            success: function (data) {
                var retName = data.nameret;
                if (retName == "Success") {
                    $('#myModal').modal('show');
                    $('#myModal').on('hidden.bs.modal', function () {
                        window.location.href = '/Admin/EditUser';
                    })
                    
                }
            },

            error: function (data) {
                $('#CreateError').show();
            }
        });

    });

    $('#deleteFunctions').click(function () {
        var templateId = $('#saveFunctions').attr("name");
        $('#confirmDelete').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#confirm', function () {
            $.ajax({
                type: "POST",
                url: "/Admin/DeleteTemplate",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    template_id: templateId
                }),
                success: function (data) {
                    switch (data) {
                        case 0:
                            $(document).scrollTop(0);
                            $('#confirmDelete').modal('hide');
                            $('#confirmDeleteSuccessful').modal('show');
                            $('#confirmDeleteSuccessful').on('hidden.bs.modal', function () {
                                window.location.href = '/Admin/CreateUserTemplate';
                            })
                            break;
                        case 1:
                            $(document).scrollTop(0);
                            $('#confirmDelete').modal('hide');
                            $('#riskRoleHasUsers').modal('show');
                            $('#riskRoleHasUsers').on('hidden.bs.modal', function () {
                                window.location.href = '/Admin/CreateUserTemplate';
                            })
                            break;
                    }
                },

                error: function (data) {
                    // $('#CreateError').show();
                }
            });
            //window.location.href = '/Admin/EditUser';
            
        });
    });
      
    $('#cancelFunctions').click(function () {
        var templateId = $('#saveFunctions').attr("name");
        $('#confirmCancel').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#confirm', function () {
            location.reload();
        });
        
    });

    $('#saveFunctions').click(function () {
        var templateId = $('#saveFunctions').attr("name");
        var form = $("#frmTemplate");
        
     
        var jsonFormData = JSON.stringify(form.serializeArray());
        var checkUploadEnabled = jsonFormData.indexOf("chkEnabled_1_UPLOAD");
        var checkResultsEnabled = jsonFormData.indexOf("chkEnabled_5_RESULTS");
        if (checkUploadEnabled == -1 && checkResultsEnabled == -1) {
            $('#myModalFunctionError').modal('show');

            ('#myModalFunctionError').on('hidden.bs.modal', function () {
                location.reload();
                return false;
            })
        }

   
        $.ajax({
            type: "POST",
            url: "/Admin/SaveTemplateData",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                strTemplateFunctions: jsonFormData,
                template_id: templateId
            }),
            success: function (data) {
                $(document).scrollTop(0);
                //$('#myModal').modal('toggle');
                $('#myModal').modal('show');
                //$('#myModal').modal('hide');
               
            },

            error: function (data) {
               // $('#CreateError').show();
            }
        });
    });
    
    $('.edit-TemplateName').click(function () {
        $('#templateHeading').hide();
        $('.edit-TemplateName').hide();
        $("#templateNameSpan").toggleClass("hide");

    });

    $("#treeToDict").click(function () {
        var tree = $("#tree").fancytree("getTree");
        var d = tree.toDict();
        $("#debugTxt").val(JSON.stringify(d));
    });
           
    $(".save-TemplateName").click(function () {
        var templateName = $('#templateName').val();

        var breakOut = false;
        $('#templateRoleNames tr').each(function () {

            $.each(this.cells, function (e) {
                var cellText = $(this).text();
                if ($.trim(cellText) == templateName) {
                    $('#myModalDuplicateTemplateNameRename').modal('show');
                    breakOut = true;
                    return false;
                }
            });
        });


        var profileId = $('#profileId').val();
        if (!breakOut) {
            $.ajax({
                type: "POST",
                url: "/Admin/EditTemplateName",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    templateName: templateName,
                    profileId: profileId
                }),
                success: function (data) {

                    $('#myModalEditTemplateName').modal('show');
                    $('#myModalLabelEditTemplateName').text('Template name changed to ' + templateName);
                    $('#myModalEditTemplateName').on('hidden.bs.modal', function () {
                        window.location.href = '/Admin/CreateUserTemplate';
                    })
                },
                error: function (data) {
                }
            });
        }
    });

    $("#btnCreate").click(function () {
        var templateName = $("#TemplateName").val();
        var breakOut = false;
        $('#templateRoleNames tr').each(function () {

            $.each(this.cells, function (e) {
                var cellText = $(this).text();
                if ($.trim(cellText) == templateName) {
                    $('#myModalDuplicateTemplateName').modal('show');
                    breakOut = true;
                    return false;
                }
            });
        });

        if (!breakOut) {
            $.ajax({
                type: "POST",
                url: "/Admin/CreateTemplate",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    templateName: templateName
                }),
                success: function (data) {

                    $('#myModalCreateSuccess').modal('show');
                    $('#myModalCreateSuccess').on('hidden.bs.modal', function () {
                        location.reload();
                    })

                    //$('#myModal').modal('hide');

                },

                error: function (data) {
                    alert(data);
                    // $('#CreateError').show();
                }
            });
        }
    });

    $('.edit-user').click(function () {
        $('#rowId').val($(this).parents('tr:first').children('td:first').html());
        $('#EditUser').show();
    });

    $(function () {
        var spinner = $("#spinnerAmberThreshold").spinner();
    });

    $(function () {
        var spinner = $("#spinnerRedThreshold").spinner();
    });
    
    $("#createclientForm").submit(function (e) {
        e.preventDefault();
        $("#createclientForm").validate();

        $.ajax({
            type: "POST",
            url: "/Admin/CreateClient",
            data: $("form").serializeArray(),
            success: function (data) {
                var retName = data.nameret;
                if (retName == "Success") {
                    $('#myModal').modal('show');
                    $('#createclientForm')[0].reset();
                }
            },

            error: function (data) {
                $('#CreateError').show();
            }
        });

    });

    $('.edit-client').click(function () {
        $('#rowId').val($(this).parents('tr:first').children('td:first').html());
        $('#EditClient').show();
    });
        
    $("#editclientdetailsForm").submit(function (e) {
        e.preventDefault();
        $("#editclientdetailsForm").validate();

        $.ajax({
            type: "POST",
            url: "/Admin/EditClientDetails",
            data: $("form").serializeArray(),
            success: function (data) {
                var retName = data.nameret;
                if (retName == "Success") {
                    $('#myModal').modal('show');
                    $('#myModal').on('hidden.bs.modal', function () {
                        window.location.href = '/Admin/EditClient';
                    })

                }
            },

            error: function (data) {
                $('#CreateError').show();
            }
        });

    });

    $('.unlock-user').click(function (e) {
        $('#rowId').val($(this).parents('tr:first').children('td:first').html());
        $('#Unlock').show();
    });

    $("#unlockuserForm").submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "/Account/Unlock",
            data: $("form").serializeArray(),
            success: function (data) {
                var retName = data.nameret;
                if (retName == "Success") {
                    $('#myModal').modal('show');
                    $('#myModal').on('hidden.bs.modal', function () {
                        window.location.href = '/Account/Unlock';
                    })

                }
            },

            error: function (data) {
                $('#CreateError').show();
            }
        });

    });

    $("#createRiskWordForm").submit(function (e) {
        e.preventDefault();
        $("#createRiskWordForm").validate();

        $.ajax({
            type: "POST",
            url: "/Admin/CreateRiskWord",
            data: $("form").serializeArray(),
            success: function (data) {
                var retName = data.nameret;
                if (retName == "Success") {
                    $('#myModal').modal('show');
                    $('#myModal').on('hidden.bs.modal', function () {
                        window.location.href = '/Admin/ViewRiskWords';
                    })

                }
            },

            error: function (data) {
                $('#CreateError').show();
            }
        });

    });

    $("#editRiskWordForm").submit(function (e) {
        e.preventDefault();
        $("#editRiskWordForm").validate();

        $.ajax({
            type: "POST",
            url: "/Admin/EditRiskWord",
            data: $("form").serializeArray(),
            success: function (data) {
                var retName = data.nameret;
                if (retName == "Success") {
                    $('#myModal').modal('show');
                    $('#editRiskWordForm')[0].reset();
                }
            },

            error: function (data) {
                $('#CreateError').show();
            }
        });

    });

    $("#deleteRiskWordForm").submit(function (e) {
        e.preventDefault();
        $("#deleteRiskWordForm").validate();

        $.ajax({
            type: "POST",
            url: "/Admin/DeleteRiskWord",
            data: $("form").serializeArray(),
            success: function (data) {
                var retName = data.nameret;
                if (retName == "Success") {
                    $('#myModal').modal('show');
                    $('#myModal').on('hidden.bs.modal', function () {
                        window.location.href = '/Admin/ViewRiskWords';
                    })

                }
            },

            error: function (data) {
                $('#CreateError').show();
            }
        });

    });

    $("#createRiskDefaultDataForm").submit(function (e) {
        e.preventDefault();
        $("#createRiskDefaultDataForm").validate();

        $.ajax({
            type: "POST",
            url: "/Admin/CreateRiskDefaultData",
            data: $("form").serializeArray(),
            success: function (data) {
                var retName = data.nameret;
                if (retName == "Success") {
                    $('#myModal').modal('show');
                    $('#myModal').on('hidden.bs.modal', function () {
                        window.location.href = '/Admin/ViewRiskDefaultData';
                    })
                }
                else {
                    fillErrorList(data);
                    showValidationErrors(true);
                }
            },

            error: function (data) {
                $('#errorContainer').show();
            }
        });

    });

    $("#createRiskNoteDecisionForm").submit(function (e) {
        e.preventDefault();
        $("#createRiskNoteDecisionForm").validate();

        $.ajax({
            type: "POST",
            url: "/Admin/CreateRiskNoteDecision",
            data: $("form").serializeArray(),
            success: function (data) {
                var retName = data.nameret;
                if (retName == "Success") {
                    $('#myModal').modal('show');
                    $('#myModal').on('hidden.bs.modal', function () {
                        window.location.href = '/Admin/ViewRiskNoteDecisions';
                    })
                }
                else {
                    fillErrorList(data);
                    showValidationErrors(true);
                }
            },

            error: function (data) {
                $('#errorContainer').show();
            }
        });

    });

    function showValidationErrors(isShown) {
        if (isShown) {
            $("#errorContainer").show();
        } else {
            $("#errorContainer").hide();
        }
    }

    function fillErrorList(errors) {
        $("#errors").html("");

        var list = document.createElement('ul');

        for (var i = 0; i < errors.length; i++) {
            var item = document.createElement('li');
            item.appendChild(document.createTextNode(errors[i]));
            list.appendChild(item);
        }
        $("#errors").html(list);
    }

    $("#deleteRiskDefaultDataForm").submit(function (e) {
        e.preventDefault();
        $("#deleteRiskDefaultDataForm").validate();

        $.ajax({
            type: "POST",
            url: "/Admin/DeleteRiskDefaultData",
            data: $("form").serializeArray(),
            success: function (data) {
                var retName = data.nameret;
                if (retName == "Success") {
                    $('#myModal').modal('show');
                    $('#myModal').on('hidden.bs.modal', function () {
                        window.location.href = '/Admin/ViewRiskDefaultData';
                    })

                }
            },

            error: function (data) {
                $('#CreateError').show();
            }
        });

    });

    $("#deleteRiskNoteDecisionForm").submit(function (e) {
        e.preventDefault();
        $("#deleteRiskNoteDecisionForm").validate();
        $.ajax({
            type: "POST",
            url: "/Admin/DeleteRiskNoteDecision",
            data: $("form").serializeArray(),
            success: function (data) {
                var retName = data.nameret;
                if (retName == "Success") {
                    $('#myModal').modal('show');
                    $('#myModal').on('hidden.bs.modal', function () {
                        window.location.href = '/Admin/ViewRiskNoteDecision';
                    })

                }
            },

            error: function (data) {
                $('#CreateError').show();
            }
        });

    });

});


