﻿$(document).ready(function () {
    function GetDialogBox(DialogType, MessageText, Title, height, width, buttons) {
        var dialogName = "";
        switch (DialogType) {
            case "IncorrectUsername":
                $('#dialog-message-usernameerror').html(MessageText);
                dialogName = "#ui-pwdResetUserNameError-dialog";
                break;
            case "ResetSuccessful":
                $('#dialog-message-success').html(MessageText);
                dialogName = "#ui-pwdResetSuccess-dialog";
                break;
            case "EmailError": 
                $('#dialog-message-emailerror').html(MessageText);
                dialogName = "#ui-pwdResetEmailError-dialog";
                break;
        }

        // Configure dialog
        var myDialog = $(dialogName).dialog({
            autoOpen: false,
            resizable: false,
            height: height,
            width: width,
            maxHeight: 500,
            maxWidth: 1200,
            modal: true,
            title: Title,
            buttons: buttons,
            create: function () {
                $('.ui-dialog-buttonset').children('button').
                        removeClass("ui-button ui-widget ui-state-default ui-state-active ui-state-focus ui-corner-all").
                        mouseover(function () { $(this).removeClass('ui-state-hover'); }).
                        mousedown(function () { $(this).removeClass('ui-state-active'); }).
                        focus(function () { $(this).removeClass('ui-state-focus'); }).addClass("buttonStyle").corner("5px");
            },
            open: function (e, ui) {
                $('#ajax-loader').css('display', 'none');
                $('#dialog-message-emailRequired').html('');
            },
        });

        $(myDialog).dialog("open");       

    }

    $("#toggle").click(function () {
        $("p:contains('Protection')").toggle("drop");
    });

    if (window.PIE) {
        $('.border-radius, .login_container, .styled-button, .blue_bg, .menu_navigation li a').each(function () {
            PIE.attach(this);
        });
    }
    
    $('#pwdResetUsername').click(function () {
        $('#dialog-message-emailRequired').html('');
    });


    //$("#resetPwd").click(function () {
    //    $('#pwdResetUsername').val('');
    //    var MessageText = '';
    //    var Title = 'Request Password Reset';
    //    GetDialogBox("ResetPassword",MessageText, Title, 165, 500, null);
    //});

    
    //$("#cancel-btn").click(function (e) {

    //    $(this).closest('.ui-dialog-content').dialog("close");
    //});

    $("#resetPwd").click(function (e) {

        e.preventDefault();
        $("<div></div>")
        .addClass("dialog")
        .attr("id", $(this).attr("data-dialog-title"))
        .appendTo("body")
        .dialog({
            close: function () {
                $(this).remove()
            },
            cache: false,
            height: 300,
            width: 380,
            title: $(this).attr("data-dialog-title"),
            modal: true
        })
        .empty().load(this.href).css('overflow', 'hidden');

    });

    $('.styled-button-cancel').on('click', function () {
        $(this).closest(".dialog").dialog("close");
        location.reload();
    });

});

$('#login').corner("5px");


$("#recoverPasswordForm").submit(function (e) {
    e.preventDefault();
    showLoader(true);
    showSuccess(false);
    showValidationErrors(false);
    $("#recoverPasswordForm").validate();
    $.ajax({
        cache: false,
        type: "POST",
        url: "/Account/RecoverPassword",
        data: $("#recoverPasswordForm").serializeArray(),
        context: $(this),
        success: function (data) {
            var retName = data.nameret;
            if (retName == "Success") {
                showLoader(false);
                $("#cancel-btn").prop('value', 'Close');
                $('#approve-btn').hide();
                $("#userName").prop("readonly", true);
                showSuccess(true);
                showValidationErrors(false);
                //$(this).closest(".dialog").dialog("close");
                //location.reload();
            }
            else if (retName == "UserNameNotFound") {
                showLoader(false);
                $("#cancel-btn").prop('value', 'Close');
                $('#approve-btn').hide();
                $("#userName").prop("readonly", true);
                showSuccess(true);
                showValidationErrors(false);
                //$(this).closest(".dialog").dialog("close");
                //location.reload();
            }
            else {
                showLoader(false);
                fillErrorList(data);
                showValidationErrors(true);
            }
        },

        error: function (data) {
            $('#CreateError').show();
        }
    });

    function fillErrorList(errors) {
        $("#errors").html("");

        var list = document.createElement('ul');

        for (var i = 0; i < errors.length; i++) {
            var item = document.createElement('li');
            item.appendChild(document.createTextNode(errors[i]));
            list.appendChild(item);
        }
        $("#errors").html(list);
    }

    function showValidationErrors(isShown) {
        if (isShown) {
            $("#errorContainer").show();
        } else {
            $("#errorContainer").hide();
        }
    }

    function showLoader(isShown) {
        if (isShown) {
            $("#ajaxloader").show();
            $("#userName").prop("readonly", true);
            $('#approve-btn').attr("disabled", true);
            //$("#cancel-btn").attr("disabled", true);
        } else {
            $("#ajaxloader").hide();
            $("#userName").prop("readonly", false);
            $('#approve-btn').attr("disabled", false);
            //$("#cancel-btn").attr("disabled", false);

        }
    }

    function showSuccess(isShown) {
        if (isShown) {
            $("#sucessContainer").show();
        } else {
            $("#sucessContainer").hide();
        }
    }

});