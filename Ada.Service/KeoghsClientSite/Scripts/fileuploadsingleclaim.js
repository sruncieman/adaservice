﻿// Fileupload code for batch upload page
$(document).ready(function () {
   
    $(function () {
        // Interval for rotating image
        var interval;
        alert('1');
        //var submitted; // Used to check if form submitted
        //
        $('#fileupload')
             .bind('fileuploadchange', function (e, data) {
                 alert('2');
                 var filename = data.files[0].name;
                 filename = filename.split('\\').pop();
                 //
                 $('#filetext').val(filename);
                 //
                 //var fileSize = data.files[0].size;
                 //alert('File size is ' + fileSize);
                 //if (window.File) {
                 //    alert('File API is supported');
                 //}
             })
            .bind('fileuploadstart', function (e) {
                // Display upload in progress
                //$('#uploadprogress').removeClass('progress-hide');
                $('#UploadHeader > h2').text('UPLOAD IN PROGRESS');
                // Add h2 tag
                $('#UploadIcon').append('<h2>LOADING </h2>');

                //$('#UploadIcon > i').addClass('icon-x-large loading');
                //$('#UploadIcon > img').css("visibility", "visible");
                $('#UploadIcon > i').removeClass();
                $('#UploadIcon > img').css("display", "inline");

                //$('#UploadFooter > h2').text('Please do not navigate away from this page');
                $('#FooterText').text('Please do not navigate away from this page');

                // Animate loading image
                var angle = 0;
                interval = setInterval(function () {
                    angle += 3;
                    $("#rotate").rotate(angle);
                }, 10);

            })
            .bind('fileuploaddone', function (e, data) {
                //
                // Check status of batch upload
                var BatchStatusError = data.result[0].BatchError;
                var BatchStatus = data.result[0].BatchStatus;
                var BatchStatusAsString = data.result[0].BatchStatusAsString;

                if (BatchStatusError == "") {
                    // No error
                    // Display success message
                    $('#UploadHeader > h2').text('FILE UPLOADED SUCCESSFULLY');
                    $('#UploadIcon > i').removeClass().addClass('icon-xxx-large uploadsuccess');
                    $('#FooterText').text('Your batch status is: ' + BatchStatusAsString + '.');
                }
                else {
                    // An error occurred
                    if (BatchStatusAsString == "NotLoggedIn") {
                        // User not logged in --> log them out
                        var url = getBaseURL();
                        window.location.replace(url + 'Account/Login');
                    }
                    else {
                        if (BatchStatusError == 'Maximum request length exceeded.') {
                            var maxSize = parseInt($('#FileUploadMaxSize').val(), 10);
                            var maxSizeMegaBytes = maxSize / 1024;
                            BatchStatusError = 'Your file was too big to be processed, maximum size is ' + maxSizeMegaBytes.toString() + ' Megabytes.';
                        }
                        //
                        $('#UploadHeader > h2').text('FILE UPLOAD ERROR');
                        $('#UploadIcon > i').removeClass().addClass('icon-xx-large error');
                        // Un handled error
                        $('#FooterText').text('Your batch upload has failed.');
                        $('#BatchError > h2').text('Error: ' + BatchStatusError);
                    }
                }
                // Remove h2 tag
                $('#UploadIcon > h2').remove();
                // Hide loading icon
                $('#UploadIcon > img').css("display", "none");
                clearInterval(interval);

                $('#UploadIcon').removeClass().addClass('aligncenter paddingtop_12 paddingbottom_10');
                //
                if (BatchStatusError == "") {
                    $('#ShowErrorButton').hide();
                    $('#ContinueButton').show();
                }
                else {
                    $('#ContinueButton').hide();
                    $('#ShowErrorButton').show();
                }

            })
            .fileupload({
                dataType: 'json',
                progressInterval: 100,
                maxNumberOfFiles: 1,
                add: function (e, data) {
                    // Disable submit button
                    $("#FileSubmit").attr("disabled", "disabled");
                    //
                    // Validate file before submission
                    // this gets called when the file is picked
                    var FileValid = true;
                    var uploadFile = data.files[0];
                    var FileExtensions = $('#ExpectedFileExtension').val();
                    var fileExt = new RegExp('(\\.(' + FileExtensions + ')$)', "i");
                    var invalidFileMessage = FileExtensions.replace(/\|/g, ', ');

                    if (!fileExt.test(uploadFile.name)) {
                        alert('Invalid file type. Only the following file types are accepted: ' + invalidFileMessage);
                        FileValid = false;
                        // Clear file name from text box
                        $('#filetext').val('');
                        return false;
                    }
                    else {
                        // Valid file, enable submit button
                        $("#FileSubmit").removeAttr("disabled");

                    }

                    // Magic to prevent multiple files being uploaded if any are previously selected.
                    $('#FileSubmit').unbind('click');
                    data.context = $('#FileSubmit').bind('click', function () {
                        e.preventDefault();
                        //
                        // Disable subsequent button click
                        $("#FileSubmit").attr("disabled", "disabled");
                        //
                        var IsValid = true;
                        var sError = '';
                        //
                        if (data.files.length == 0 || data.files[0].name == '') {
                            //alert('Batch file is required');
                            sError = 'Batch file is required. <br>';
                            IsValid = false;
                        }
                        //
                        // Check that batch ref entered
                        var batchref = $('#batchref').val();
                        if (batchref == '') {
                            //alert('Batch reference is required');
                            sError += 'Batch reference is required. <br>';
                            // submitted = false;
                            IsValid = false;
                        }
                        // Check for Unique Batch Reference
                        // Call ajax method in homeandresults.js
                        var BatchError = CheckBatchRefIsUnique(batchref);
                        //
                        if (BatchError != '') {
                            if (BatchError == 'SessionTimeout' || BatchError == 'UnknownError') {
                                sError = BatchError;
                            }
                            else {
                                sError += BatchError;
                            }
                            IsValid = false;
                        }
                        //
                        if (IsValid) {
                            data.submit();
                        }
                        else {
                            var me = $(this);
                            //alert(sError);
                           
                            //$('#UploadFooter > h2').text(sError);
                            if (sError != 'SessionTimeout' || sError != 'UnknownError') {
                                // Display error message
                                $('#UploadHeader > h2').text('FILE UPLOAD ERROR');
                                $('#UploadIcon > i').removeClass().addClass('icon-xx-large error');
                                $('#FooterText').html(sError);
                                // Re-enable submit button
                                $("#FileSubmit").removeAttr("disabled");
                            }
                            e.cancelBubble = true; // for IE
                            e.stopPropagation();
                            return false;
                        }
                        // }

                    });


                }//,

            });
        $('.NFFileButton').click(function () {
            //console.log('hello')
            // Remove any messages/icons from previous upload
            $('#UploadHeader > h2').text('');
            // 
            $('#UploadIcon > i').removeClass()
            //$('#UploadFooter > h2').text('');
            $('#FooterText').text('');
            // Clear text boxes
            $('#batchref').val('');
            $('#filetext').val('');
            $('#ContinueButton').hide();
            $('#ShowErrorButton').hide();
        });
        //
        $('#ContinueButton').click(function () {
            // Refresh the page
            location.reload(true);
        });
        $('#ShowErrorButton').click(function () {
            // Open dialog to show error

           // $("#BatchError").dialog("open");
            $('#BatchError').dialog({
                modal: true,
                resizable: false,
                autoResize: true,
                width: 630,
                title: 'Batch upload error.'

            });
            return false;

        });
        //
        //
        $('#FileSubmit').click(function () {
            alert("Please click the 'Browse Button' to select a file to upload and enter a 'Batch Reference'");
        });
        //
        $('#BatchErrorDialogOK').click(function () {
            $("#BatchError").dialog("close");

        });

    });

    $('#filetext').keydown(function (event) {
        if (event.which == 8) {
            event.preventDefault();
        }
    });

});
