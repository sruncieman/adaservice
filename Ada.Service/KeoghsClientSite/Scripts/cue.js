﻿
$.ajaxSetup({ cache: false });

//$(document).ajaxError(function (event, request, options) {
//    if (request.status == 401) {
//        $("#session-timeout-dialog").dialog({
//            width: 500,
//            height: 200,
//            modal: true,
//            title: "Session timeout",
//            resizable: false,
//            open: function (event, ui) {
//                $('.ui-dialog-titlebar > a').remove();
//            }
//        });
//    }
//});



$(document).ready(function () {
    $(".openDialog").live("click", function (e) {
        e.preventDefault();
        $("<div></div>")
        .addClass("dialog")
        .attr("id", $(this).attr("data-dialog-title"))
        .appendTo("body")
        .dialog({
            height: 480,
            width: 1060,
            title: $(this).attr("data-dialog-title"),
            close: function () { $(this).remove() },
            modal: true
        })
        .load(this.href);
    });
    
    $(".openReportDialog").live("click", function (e) {
        e.preventDefault();
        window.open(this.href, "_blank")
        $(this).closest(".dialog").dialog("close");
    });
    
    $(".cue-button-submit").live("click", function (e) {
        e.preventDefault();

        var checkboxesCount = $(":checkbox:checked").length;
        if (checkboxesCount == 0) {
            $("#noneSelected").dialog({
                modal: true
            });
            return false;
        }
        else {
            $.ajax({
                type: "POST",
                url: "/CUE/CUESearch",
                data: $("form").serializeArray(),
                success: function (data) {
                    var retName = data.nameret;
                    if (retName == "Success") {
                        $('#UploadHeader > h2').text('CUE Search Complete');
                        $('#FooterText').html('The Cue Search has been successfully processed.');
                        $('#UploadIcon > img').css("display", "none");
                        $('#UploadIcon').remove();
                        $('#rotate').parent().remove();
                        $('#submitCUEReport').show();
                        $('#UploadSuccessIcon').show();
                    }
                },

                error: function (data) {
                    alert("There was an error processing this");
                    $(this).closest(".dialog").dialog("close");
                }
            });

            $('#cueInvolvements').css("display", "none");
            $('#UploadHeader > h2').text('CUE Search In Progress');
            $('#UploadIcon > i').removeClass();
            $('#FooterText').html('A CUE Search is currently being processed. A CUE Report will be available on completion of the search. <br/><br/>Please do not navigate away from this page while the search is in progress');
            $('#UploadFooter').show();
            $('#UploadIcon > img').css("display", "inline");
        }
    });

});


