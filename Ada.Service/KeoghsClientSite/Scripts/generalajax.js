﻿
function AjaxError(request, error) {
    if (request.status == 401) {
        $("#session-timeout-dialog").dialog({
            width: 500,
            height: 200,
            modal: true,
            title: "Session timeout",
            resizable: false,
            open: function () {
                $('.ui-dialog-titlebar > a').remove();
            }
        });
    }
    else {

        //TODO: Another error occurred, which we could handle globally here.
        $("#ajax-error-dialog").dialog({
            width: 500,
            height: 200,
            modal: true,
            title: "Error",
            resizable: false,
            buttons: {
                "Ok": function () {
                    $(this).dialog("close");
                }
            },
            open: function () {
                //
                $('.ui-dialog-titlebar-close').hide();
                $('.ui-dialog :button').blur();

            }
        });
    }
}
