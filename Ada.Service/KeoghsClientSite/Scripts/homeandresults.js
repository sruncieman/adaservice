﻿$(document).ready(function () {
    $(document).ajaxError(function (event, request, options) {
        if (request.status == 401) {
            $("#session-timeout-dialog").dialog({
                width: 500,
                height: 200,
                modal: true,
                title: "Session timeout",
                resizable: false,
                open: function (event, ui) {
                    $('.ui-dialog-titlebar > a').remove();
                }
            });
        }
        //else {
        //    //TODO: Another error occurred, which we could handle globally here.
        //    $("#ajax-error-dialog").dialog({
        //        width: 500,
        //        height: 200,
        //        modal: true,
        //        title: "Error",
        //        resizable: false,
        //        buttons: {
        //            "Ok": function () {
        //                $(this).dialog("close");
        //            }
        //        },
        //        open: function () {
        //            //
        //            $('.ui-dialog-titlebar-close').hide();
        //            $('.ui-dialog :button').blur();

        //        }
        //    });
        //}
    });
    //
    if (window.PIE) {
        $('.border-radius, .login_container, .styled-button, .action_border, .NFFileButton, .blue_bg, .action-arrow, .welcome, .menu_navigation li a').each(function () {
            PIE.attach(this);
        });
    }
    // Display calendar when icon calendar clicked
    $(".calendarIconStartDate").click(function () {
        //alert('Calendar clicked');
        $("#FilterStartDate").datepicker('show');
        return false;
    });
    // Display calendar when icon calendar clicked
    $(".calendarIconEndDate").click(function () {
        $("#FilterEndDate").datepicker('show');
        return false;
    });
    //
    $(function () {

        // Filter datepickers
        $("#FilterStartDate").datepicker({ dateFormat: 'dd/mm/yy' });
        $("#FilterEndDate").datepicker({ dateFormat: 'dd/mm/yy' });

        // Filter client drop down
        $('#SelectedClientId').change(function () {
            // Set PageSize in hidden field
            $('#PagingInfo_CurrentPage').val(1);
            // reset Team and User drop downs
            $('#SelectedTeamId').val("");
            $('#SelectedUserId').val("");
            this.form.submit();
        });
        // Filter Team drop down
        $('#SelectedTeamId').change(function () {
            // Set PageSize in hidden field
            $('#PagingInfo_CurrentPage').val(1);
            // Reset user drop down
            $('#SelectedUserId').val("");
            this.form.submit();
        });
        // Filter User drop down
        $('#SelectedUserId').change(function () {
            // Set PageSize in hidden field
            $('#PagingInfo_CurrentPage').val(1);
            this.form.submit();
        });
        //
        var TotalPages = parseInt($('#PagingInfo_TotalPages').val(), 10);
        if (TotalPages == 0)
        {
            $('#PagingLinks').hide();
        }
    });
    //

    


    var batchReportsSectionReadOnly = $('#BatchReportsSectionReadOnly').val();

    
    $('.col-7').each(function () {
        if (batchReportsSectionReadOnly != 'true') {
            $(this).find('i').click(function () {

                var clickedDivId = $(this).attr('data-field');

                $('.dropdownlist').each(function () {
                    if ($(this).css('display') == 'block')
                        var divVisibleId = $(this).attr('id');

                    if (clickedDivId != divVisibleId) {
                        $(this).css('display', 'none');
                        $('i[data-field*="' + divVisibleId + '"]').removeClass('action_border').removeClass('downarrowblue');
                    }

                });
                if ($('#' + $(this).attr('data-field')).css('display') == 'none') {
                    $('#' + $(this).attr('data-field')).css('display', 'block');
                    $(this).addClass('action_border').addClass('downarrowblue');
                }
                else {
                    $('#' + $(this).attr('data-field')).css('display', 'none');
                    $(this).removeClass('action_border').removeClass('downarrowblue');
                }
            });
        }
    });


   
    //
    // Submit filter form when sort cols clicked
    $('.SortCol').click(function () {
        //
        var col = $(this).attr('id');
       
        $('#SortColumn').val(col);

        var SortColumn = $('#SortColumn').val(col);

        var SortAscending = $('#SortAscending').val();
       
        if (SortAscending == "True")
            $('#SortAscending').val("False");
        else
            $('#SortAscending').val("True");

        $(this).closest('form')[0].submit();
    });
    // Submit filter form when page links clicked
    //$('.pagelink').click(function () {
    //    //var col = $(this).attr('id');
    //    var page = $(this).text();
    //    //alert('You clicked page ' + page);
    //    $('#PagingInfo_CurrentPage').val(page);
    //    $(this).closest('form')[0].submit();
    //});

    $('.pagefirst').click(function () {
        
      
        var Currpage = parseInt($('#PagingInfo_CurrentPage').val(), 10);
        if (Currpage != 1) {
            if ($("#clientbatchreference input:checked").length < 1) {
                //alert($("#clientbatchreference input:checked").length);
                $("#checkallbatches").prop('checked', true);
                $("#clientbatchreference input").each(function () {
                    $(this).prop("checked", true);
                });
            }
            $('#PagingInfo_CurrentPage').val(1);
            $(this).closest('form')[0].submit();
            return true;
        }
        else { return false; }
    });
    
    $('.pagelast').click(function () {
        var TotalPages = parseInt($('#PagingInfo_TotalPages').val(), 10);
            $('#PagingInfo_CurrentPage').val(TotalPages);
            $(this).closest('form')[0].submit();
            return true;
    });


    $('.pageprev').click(function () {
        // Get current page
        var Currpage = parseInt($('#PagingInfo_CurrentPage').val(), 10);
        if (Currpage != 1) {
            $('#PagingInfo_CurrentPage').val(Currpage - 1);
            $(this).closest('form')[0].submit();
        }
        else { return false; }
    });
    $('.pagenext').click(function () {
        // Get current page
        var Currpage = parseInt($('#PagingInfo_CurrentPage').val(), 10);
        var TotalPages = parseInt($('#PagingInfo_TotalPages').val(), 10);

    
        if (Currpage != TotalPages) {
            $('#PagingInfo_CurrentPage').val(Currpage + 1);
            $(this).closest('form')[0].submit();
        }
        else { return false; }
    });
    // Re-set current page when filter button clicked
    // ensures results start again at page 1
    // Submit filter form when page links clicked
    $('#FilterButton').click(function () {
        if ($("#clientbatchreference input:checked").length < 1) {
            //alert($("#clientbatchreference input:checked").length);
            $("#checkallbatches").prop('checked', true);
            $("#clientbatchreference input").each(function () {
                $(this).prop("checked", true);
            });
        }        
        
        $('#PagingInfo_CurrentPage').val(1);
        $(this).closest('form')[0].submit();
    });


    $('.selectedClaim').change(function () {
        $("#checkall").prop('checked', false);
        if ($("#checkboxes input:checked").length > 1) {
            $('#hidedivlevel1').hide();
            $('#hidedivcue').hide();
            $('#hidedivsinglenote').hide();
            $('#hidedivmultinote').show();
        }
        else {
            $('#hidedivlevel1').show();
            $('#hidedivcue').show();
            $('#hidedivsinglenote').show();
            $('#hidedivmultinote').hide();
        }
        
    });

    $('.selectedBatch').change(function () {
        $("#checkallbatches").prop('checked', false);
    });

    $('.selectedDecision').change(function () {
        $("#checkalldecisions").prop('checked', false);
    });

    // Filter Clear button
    // Clear all filter entries back to default
    $('#ClearButton').click(function () {
        //alert('clicked');
        //$('#FilterClaimNumber').val('');
        //$('#FilterBatchRef').val('');
        //$('#FilterStartDate').val('');
        //$('#FilterEndDate').val('');
        //// Clear all risk rating check boxes
        //$('#riskrating input:checkbox').removeAttr('checked');
        //// Clear all reports requested options
        //$('#reportsrequested input:checkbox').removeAttr('checked');
        //$('#reportsrequested input:radio').removeAttr('checked');
        //return false;

        location.href = '/';
        return false;

    });
    //
    $(".GetBatchProcessingResults").on("click", (function () {

        var batchId = $(this).attr('id');
        //
        var submitCallback = function () {
            // alert('Submit Clicked');
            $("#tree").dynatree("destroy");

            jQuery(this).dialog('destroy');
        }; //
        var expandTree = function () {
            $("#tree").dynatree("getRoot").visit(function (node) {
                node.expand(true);
                //node.toggleExpand();
            });
            //var node = $("#tree").dynatree("getRoot");
            //node.expand(true);
            //alert(e.target);
            return false;

        }; //
        var contractTree = function () {
            $("#tree").dynatree("getRoot").visit(function (node) {
                node.expand(false);
            });
            return false;
        }; //
        var MessageText='';
        var Title = 'Batch processing results';
        var buttons = {};
        buttons['OK'] = submitCallback;
        buttons['Expand tree'] = expandTree;
        buttons['Contract tree'] = contractTree;
        ////buttons['Cancel'] = function () {
        ////   // alert('Cancel clicked');
        ////    jQuery(this).dialog('destroy');
        ////};
        //
        $(function () {
            $.ajax({
                type: "POST",
                url: "/Upload/GetBatchProcessingResults",
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    batchId: batchId
                }),
                success: function (data) {

                    if (data.Error == null) {
                        var obj = jQuery.parseJSON(data.Result);

                        //treeData = data;
                        $("#tree").dynatree({
                            checkbox: false,
                            children: obj
                        });
                        //
                        GetDialogBox(MessageText, Title, 500, 700, buttons);
                    }
                    else {
                        alert('There was a problem accessing your results');
                    }
                },
                error: function (result) {
                    // un-handled error, handled by global error handler
                }
            });
        });
        return false;
    }));
    //
    $(".GetLevel1Report").click(function () {
        var claimId = $(this).attr('id');
      
       
        var url = getBaseURL();
        url += 'Report/GetLevelOneReport?Id=' + claimId;
       // alert(url);

        location.href = url;

        return false;
    });


    

    $(".UpdateClaimStatus").click(function () {
        var originalId = '#' + $(this).attr('id');


        var name = $(originalId).attr("name");
        var idData = name.split("|");
        var riskClaimId = idData[0];
        var readStatus = idData[1];
        var riskClaimReadStatus;
        var nameValue = "";
        
        if (readStatus == "0") {
            //Unread
            riskClaimReadStatus = 1;
        }

        else if (readStatus == "1") {
            //Read
            riskClaimReadStatus = 0;
        }
        
        else {
            riskClaimReadStatus = 1;
        }

        nameValue = riskClaimId + "|" + riskClaimReadStatus;


        

        //
        // Call ajax method 
        $.ajax({
            type: "POST",
            url: "/Home/UpdateClaimReadStatus",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({
                riskClaimId: riskClaimId,
                riskClaimReadStatus: riskClaimReadStatus
            }),
            success: function (data) {
                //
               // console.log(data);
                //
                var Result = data.Result;
                var Error = data.Error;
                var CurrentReadStatus = data.CurrentReadStatus;
                
                

                //
                //alert('CurrentReadStatus: ' + CurrentReadStatus);
                if (Result == 0) {
                    if (CurrentReadStatus == 2 || CurrentReadStatus == 3) {
                        if (CurrentReadStatus == 2) {
                            $('#Status' + riskClaimId).removeClass('icon-small UnreadIncrease-status left UpdateClaimStatus').addClass('icon-small Read-status left UpdateClaimStatus');
                            $("#ClaimRow" + riskClaimId).css("font-weight", "normal");
                        }
                        else if (CurrentReadStatus == 3) {
                            $('#Status' + riskClaimId).removeClass('icon-small UnreadDecrease-status left UpdateClaimStatus').addClass('icon-small Read-status left UpdateClaimStatus');
                            $("#ClaimRow" + riskClaimId).css("font-weight", "normal");
                        }
                    }
                    
                    else if (readStatus == 1) {
                        $('#Status' + riskClaimId).removeClass('icon-small Read-status left UpdateClaimStatus').addClass('icon-small Unread-status left UpdateClaimStatus');
                        $('#Status' + riskClaimId).attr('title', 'Unread Claim');
                        $("#ClaimRow" + riskClaimId).css("font-weight", "bold");
                    }
                    else if (readStatus == 0) {
                        $('#Status' + riskClaimId).removeClass('icon-small Unread-status left UpdateClaimStatus').addClass('icon-small Read-status left UpdateClaimStatus');
                        $('#Status' + riskClaimId).attr('title', 'Read Claim');
                        $("#ClaimRow" + riskClaimId).css("font-weight", "normal");
                    }
                    $('#Status' + riskClaimId).attr('name', nameValue);
                }
            },
            error: function (result) {
               // alert('Error: ' + result.responseText);
            }
        });
        return false;
    });
    //
    // Request Level 1 Report
    $(".View-Level-1").on("click", (function () {

        if ($("#checkboxes input:checked").length > 0) {
            var riskClaimId = new Array();

            $("#checkboxes input:checked").each(function () {
                riskClaimId.push($(this).val());
            });

            var url = getBaseURL();
            url += 'Report/GetLevelOneReport?ClaimId=' + riskClaimId;

            $(this).addClass('View-Level-1');

            location.href = url;

            return false;
        }

    }));



    //$(".Add-Note").on("click", (function () {

    //    if ($("#checkboxes input:checked").length > 0) {
    //        var riskClaimId = new Array();

    //        $("#checkboxes input:checked").each(function () {
    //            riskClaimId.push($(this).val());
    //        });

    //        var url = getBaseURL();
    //        url += 'Note/Index?claimsSelected=' + riskClaimId;

    //        $(this).addClass('View-Level-1');

    //        location.href = url;

    //        return false;
    //    }

    //}));

  
    $(".openNotesDialog").live("click", function (e) {

        if ($("#checkboxes input:checked").not('#checkall').length > 0) {

            var riskClaimId = new Array();

            $("#checkboxes input:checked").each(function () {
                riskClaimId.push($(this).val());                              
            });

            var url = getBaseURL();
            url += 'Note/Index?claimsSelected=' + riskClaimId;

            e.preventDefault();
            $("<div></div>")
            .addClass("dialog")
            .attr("id", $(this).attr("data-dialog-title"))
            .appendTo("body")
            .dialog({
                close: function () { $(this).remove() },
                cache: false,
                height: 530,
                width: 1060,
                title: $(this).attr("data-dialog-title"),
                modal: true
            })
            .empty().load(url);
        }

        return false;
    });

    $(".icon-small.edit").click(function (e) {

        $('#rowId').val($(this).parents('tr:first').children('td:first').html());

        if ($('#rowId').val().length > 0) {

            var url = getBaseURL();
            url += 'Note/EditRiskNote?rowId=' + $('#rowId').val();

            $(this).closest('.ui-dialog-content').dialog("close");

            e.preventDefault();
            $("<div></div>")
            .addClass("dialog")
            .attr("id", $(this).attr("data-dialog-title"))
            .appendTo("body")
            .dialog({
                close: function () { $(this).remove() },
                cache: false,
                height: 530,
                width: 1060,
                title: $(this).attr("data-dialog-title"),
                modal: true
            })
            .empty().load(url);
        }

        return false;
    });

    $(".icon-small.delete").click(function (e) {

        if (window.confirm("Are you sure that you want to permanently delete the selected record?")) {

            $('#rowId').val($(this).parents('tr:first').children('td:first').html());

            if ($('#rowId').val().length > 0) {

                var url = getBaseURL();
                url += 'Note/DeleteRiskNote?rowId=' + $('#rowId').val();

                //$(this).closest('.ui-dialog-content').dialog("close");

                $.ajax({
                    cache: false,
                    type: "POST",
                    url: '/Note/DeleteRiskNote?rowId=' + $('#rowId').val(),
                    data: $("form").serializeArray(),
                    context: $(this),
                    success: function (data) {
                        var retName = data.nameret;
                        if (retName == "Success") {
                            $(this).closest(".dialog").dialog("close");
                            $('.dialog').remove();
                            $("<div></div>")
                            .addClass("dialog")
                            .attr("id", "Claim Notes")
                            .appendTo("body")
                            .dialog({
                                close: function () { $(this).remove() },
                                cache: false,
                                height: 530,
                                width: 1060,
                                title: "Claim Notes",
                                modal: true
                            })
                            .empty().load(data.Url);
                        }
                        else {
                            fillErrorList(data);
                            showValidationErrors(true);
                        }
                    },

                    error: function (data) {
                    }
                });

            }

            return false;
        }

        return false;

    });

    //$(".icon-small.paperclip").click(function (e) {

    //    $('#rowId').val($(this).parents('tr:first').children('td:first').html());

    //    if ($('#rowId').val().length > 0) {

    //        var url = getBaseURL();
    //        url += 'Note/OpenRiskNoteFile?rowId=' + $('#rowId').val();

    //        //$(this).closest('.ui-dialog-content').dialog("close");

    //        $.ajax({
    //            cache: false,
    //            type: "POST",
    //            url: '/Note/OpenRiskNoteFile?rowId=' + $('#rowId').val(),
    //            data: $("form").serializeArray(),
    //            context: $(this),
    //            success: function (data) {
    //                var retName = data.nameret;
    //                if (retName == "Success") {
    //                    $(this).closest(".dialog").dialog("close");
    //                    $('.dialog').remove();
    //                    $("<div></div>")
    //                    .addClass("dialog")
    //                    .attr("id", "Claim Notes")
    //                    .appendTo("body")
    //                    .dialog({
    //                        close: function () { $(this).remove() },
    //                        cache: false,
    //                        height: 510,
    //                        width: 1060,
    //                        title: "Claim Notes",
    //                        modal: true
    //                    })
    //                    .empty().load(data.Url);
    //                }
    //                else {
    //                    fillErrorList(data);
    //                    showValidationErrors(true);
    //                }
    //            },

    //            error: function (data) {
    //            }
    //        });

    //    }

    //    return false;
    //});

    $(".openMultiNotesDialog").live("click", function (e) {

        if ($("#checkboxes input:checked").length > 0) {

            var riskClaimId = new Array();

            $("#checkboxes input:checked").each(function () {
                riskClaimId.push($(this).val());
            });

            var url = getBaseURL();
            url += 'Note/Index?claimsSelected=' + riskClaimId;

            e.preventDefault();
            $("<div></div>")
            .addClass("dialog")
            .attr("id", $(this).attr("data-dialog-title"))
            .appendTo("body")
            .dialog({
                height: 230,
                width: 700,
                title: $(this).attr("data-dialog-title"),
                modal: true
            })
            .empty().load(url);

        }

        return false;
    });

    
    $(".icon-small.add-note.left").click(function(e) {

        e.preventDefault();
        $("<div></div>")
        .addClass("dialog")
        .attr("id", $(this).attr("data-dialog-title"))
        .appendTo("body")
        .dialog({
            close: function () { $(this).remove()
            },
            cache: false,
            height: 530,
                width: 1060,
            title: $(this).attr("data-dialog-title"),
                modal: true
            })
        .empty().load(this.href);
            
    });

    $(".icon-small.paperclip.left").click(function (e) {

        e.preventDefault();
        $("<div></div>")
        .addClass("dialog")
        .attr("id", $(this).attr("data-dialog-title"))
        .appendTo("body")
        .dialog({
            close: function () {
                $(this).remove()
            },
            cache: false,
            height: 530,
            width: 1060,
            title: $(this).attr("data-dialog-title"),
            modal: true
        })
        .empty().load(this.href);

    });

    $(".icon-small.decision.left").click(function(e) {

        e.preventDefault();
        $("<div></div>")
        .addClass("dialog")
        .attr("id", $(this).attr("data-dialog-title"))
        .appendTo("body")
        .dialog({
            close: function () { $(this).remove()
        },
                cache : false,
            height: 530,
            width: 1060,
            title: $(this).attr("data-dialog-title"),
                modal: true
        })
        .empty().load(this.href);

    });

    $(".icon-small.decision2.left").click(function(e) {

        e.preventDefault();
        $("<div></div>")
        .addClass("dialog")
        .attr("id", $(this).attr("data-dialog-title"))
        .appendTo("body")
        .dialog({
                close: function () {
            $(this).remove()
            },
                cache : false,
            height: 530,
            width: 1060,
            title: $(this).attr("data-dialog-title"),
                modal: true
        })
        .empty().load(this.href);

    });

    $(".styled-button-cancel-edit").click(function(e) {

            $(this).closest('.ui-dialog-content').dialog("close");
            e.preventDefault();
            $("<div></div>")
            .addClass("dialog")
            .attr("id", $(this).attr("data-dialog-title"))
            .appendTo("body")
            .dialog({
                close: function () {
                    $(this).remove()
                },
                cache: false,
                height: 530,
                width: 1060,
                title: $(this).attr("data-dialog-title"),
                modal: true
            })
        .empty().load(this.href);
                    

    });



    //
    // Request Level 1 Report
    $(".Request-Level-1").on("click", (function () {
        // Prevent further clicks
        var linkId = $(this);

        var $this = this;

        $(linkId).removeClass('Request-Level-1').addClass('not-clickable');

        var Id = $(this).attr('id');
        riskClaimId = Id.replace('Level1', '');
   
        //Display confirm message box for user
        $('#dialog-message').html('Please confirm your request.');
        //
        // Configure dialog
        var myDialog = $("#ui-message-dialog").dialog({
            autoOpen: false ,
            resizable: false,
            height: 200,
            width: 300,
            modal: true,
            title: "Request Level 1 Report",
            buttons: {
                "Confirm": function () {
                    // Call ajax method 

                    RequestLevelOneReport($this, myDialog, riskClaimId);

                    //$.ajax({
                    //    type: "POST",
                    //    url: "/Home/RequestLevelOneReport",
                    //    dataType: 'json',
                    //    contentType: "application/json; charset=utf-8",
                    //    data: JSON.stringify({
                    //        riskClaimId: riskClaimId
                    //    }),
                    //    context: $this,
                    //    success: function (data) {
                            
                    //        var Result = data.Result;
                    //        var Error = data.Error;
                            
                    //        if (Result == 0) {
                    //            // Turn off click
                    //            $($this).off("click");
                    //             //Update action link text and class to Level 1 Pending
                    //            //$('#' + Id + 'Text').html('Level 1 Pending');
                    //            $('#' + riskClaimId + 'Text').html('Level 1 Pending');
                    //            //
                    //            $($this).removeClass().addClass('Level-1-Pending');
                    //        }
                    //        else {
                    //             //An error occurred , display message
                    //             //Add request level 1 class back in to allow further clicks
                    //            $(linkId).removeClass('not-clickable').addClass('Request-Level-1');
                    //            alert('There was a problem processing your request');
                    //        }
                    //        $(myDialog).dialog("close");
                    //    },
                    //    error: function (result) {
                    //        // un-handled error, handled by global error handler
                    //         //Add request level 1 class back in to allow further clicks
                    //        $(linkId).removeClass('not-clickable').addClass('Request-Level-1');
                    //        alert('There was a problem processing your request');
                    //        $(myDialog).dialog("close");
                    //    }
                    //});
                    $('#Level1Status' + riskClaimId).removeClass('icon-large RequestLevel1-status left').addClass('icon-large PendingLevel1-status left');
                    $('#Level1Status' + riskClaimId).attr('title', 'Level 1 Report Pending');
                },
                "Cancel": function () {
                    // Add request level 1 class back in to allow further clicks
                    $(linkId).removeClass('not-clickable').addClass('Request-Level-1');
                    $(myDialog).dialog("close");
                    
                }
    
            },
            open: function () {
                //
                $('.ui-dialog-titlebar-close').hide();
                $('.ui-dialog :button').blur();

            }
        });
        // Open dialog
        $(myDialog).dialog("open");
        //
        return false;
    }));

    $(".GetBatchReport").live("click", function () {
        var claimId = $(this).attr('id');
        var clientBatchRef = $("#" + claimId).attr("name");
        var url = getBaseURL();
        url += 'Report/GetBatchReport?Id=' + claimId + '&ClientBatchRef=' + clientBatchRef;
        location.href = url;
        return false;
    });

});

function getBaseURL() {
    return location.protocol + "//" + location.hostname +
       (location.port && ":" + location.port) + "/";
}

function divShowHide(ListId, divId, ArrowUp, ArrowDown) {
    if ($('#' + divId).css('display') == 'block') {
        $('#' + divId).css('display', 'none');
        $('#' + ListId).addClass(ArrowDown);
        $('#' + ListId).removeClass(ArrowUp);

        switch (divId) {
            //case "uploadedby":
            //    document.getElementById('UploadedByCollapsedState').value = "Hide";
            //    break;
            case "claimnumbersubtext":
                document.getElementById('ClaimNumberCollapsedState').value = "Hide";
                break;
            case "clientbatchreference":
                document.getElementById('BatchReferenceCollapsedState').value = "Hide";
                break;
            case "uploaddaterange":
                document.getElementById('UploadDateRangeCollapsedState').value = "Hide";
                break;
            case "riskrating":
                document.getElementById('RiskRatingCollapsedState').value = "Hide";
                break;
            case "filterStatus":
                document.getElementById('StatusCollapsedState').value = "Hide";
                break;
            case "decisions":
                document.getElementById('DecisionCollapsedState').value = "Hide";
                break;
        }
    }
    else {
        $('#' + divId).css('display', 'block');
        $('#' + ListId).addClass(ArrowUp);
        $('#' + ListId).removeClass(ArrowDown);

        switch (divId) {
            //case "uploadedby":
            //    document.getElementById('UploadedByCollapsedState').value = "Show";
            //    break;
            case "claimnumbersubtext":
                document.getElementById('ClaimNumberCollapsedState').value = "Show";
                break;
            case "clientbatchreference":
                document.getElementById('BatchReferenceCollapsedState').value = "Show";
                break;
            case "uploaddaterange":
                document.getElementById('UploadDateRangeCollapsedState').value = "Show";
                break;
            case "riskrating":
                document.getElementById('RiskRatingCollapsedState').value = "Show";
                break;
            case "filterStatus":
                document.getElementById('StatusCollapsedState').value = "Show";
                break;
            case "decisions":
                document.getElementById('DecisionCollapsedState').value = "Show";
                break;
        }
    }
    return false;
}

function CheckBatchRefIsUnique(batchref) {

    var sError = '';

    // Check for Unique Batch Reference
    // Call ajax method 
    $.ajax({
        type: 'POST',
        url: '/Upload/IsClientBatchReferenceUnique',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            clientBatchReference: batchref
        }),
        async: false,
        success: function (data) {
            //
            //alert('Success');
            if (data.Error != null) {
                if (data.Error == 'The underlying provider failed on Open.') {
                    sError = 'Error connecting to ADA database.<br/><br/>Please email us at support-ada@keoghs.co.uk to report this error. \r\n';
                }
                else {
                    sError = 'Error checking batch reference. \r\n';
                }
            }
            else if (data.IsUnique == false) {
                sError = 'Batch reference must be unique. \r\n';
            }
        },
        error: function (result) {
            //alert('Fail');
             //Check if user session has timed out
             //If session has timed out the ajax call will return the html from the login screen
            //var sLoginText = 'LoginWarningMessage';
            //
            if (result.status == 401) {
                sError = 'SessionTimeout';
            }
            else {
                sError = 'UnknownError';
            }
        }
    });
    return sError;
}

function RequestLevelOneReport(clickedLink, myDialog, riskClaimId) {

    $.ajax({
        type: "POST",
        url: "/Home/RequestLevelOneReport",
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            riskClaimId: riskClaimId
        }),
        context: clickedLink,
        success: function (data) {

            var Result = data.Result;
            var Error = data.Error;

            if (Result == 0) {
                // Turn off click
                $(clickedLink).off("click");
                //Update action link text and class to Level 1 Pending
                //$('#' + Id + 'Text').html('Level 1 Pending');
                var Id = $(clickedLink).attr('id');
                var linkText = $('#' + Id + 'Text');
                linkText.html('Level 1 Pending');
                
                //$('#' + riskClaimId + 'Text').html('Level 1 Pending');
                //
                $(clickedLink).removeClass().addClass('Level-1-Pending');
            }
            else {
                //An error occurred , display message
                //Add request level 1 class back in to allow further clicks
                $(clickedLink).removeClass('not-clickable').addClass('Request-Level-1');
                alert('There was a problem processing your request');
            }
            $(myDialog).dialog("close");
        },
        error: function (result) {
            // un-handled error, handled by global error handler
            //Add request level 1 class back in to allow further clicks
            $(clickedLink).removeClass('not-clickable').addClass('Request-Level-1');
            //alert('There was a problem processing your request');
            $(myDialog).dialog("close");
        }
    });

}

function GetDialogBox(MessageText, Title, height, width, buttons) {
    //
    //var buttons = {};

    //buttons['OK'] = submitCallback;

    //buttons['Cancel'] = function () {
    //             jQuery(this).dialog('destroy').remove();
    //         };

    //Display confirm message box for user
    $('#dialog-message').html(MessageText);
    //
    // Configure dialog
    var myDialog = $("#ui-message-dialog").dialog({
        autoOpen: false,
        resizable: true,
        height: height,
        width: width,
        //height: "auto",
        //width: "auto",
        maxHeight: 1000,
        //minWidth: 500,
        maxWidth:1200,
        modal: true,
        title: Title,
        buttons: buttons,
        open: function () {
            //
            $('.ui-dialog-titlebar-close').hide();
            $('.ui-dialog :button').blur();

        }
    });
    $(myDialog).dialog("open");
}

//$(document).click(function (e) { //button click class name is myDiv
//    $('.col-7').each(function () {
//        // Check if click was triggered on or within #menu_content
//        if ($(e.target).closest(".downarrowblue").length > 0) {
//            return false;
//        } else {
//            $('.downarrowblue').each(function () {
//                if ($('#' + $(this).attr('data-field')).css('display') != 'none') {

//                    $('#' + $(this).attr('data-field')).css('display', 'none');
//                    $(this).removeClass('action_border').removeClass('downarrowblue');
//                }
//            });
//        }
//        return false;
//    });
//});


$('.styled-button-cancel').on('click', function () {
    var dropdownvalue = $(".decisionDropDown-control").val();
    var textvalue = $(".textarea").val();
    if (dropdownvalue == '' && textvalue == '')
    {
        $(this).closest('.ui-dialog-content').dialog("close");
        if ($("#clientbatchreference input:checked").length < 1) {
            //alert($("#clientbatchreference input:checked").length);
            $("#checkallbatches").prop('checked', true);
            $("#clientbatchreference input").each(function () {
                $(this).prop("checked", true);
            });
        }
        $('#ClaimsForm').submit();
    }
    else {
        if (window.confirm("Are you sure you want to discard these changes?")) {
            $(this).closest('.ui-dialog-content').dialog("close");
            if ($("#clientbatchreference input:checked").length < 1) {
                //alert($("#clientbatchreference input:checked").length);
                $("#checkallbatches").prop('checked', true);
                $("#clientbatchreference input").each(function () {
                    $(this).prop("checked", true);
                });
            }
            $('#ClaimsForm').submit();
        }
    }

});


$("#createRiskNoteForm").submit(function (e) {
    var formData = new FormData($(this)[0]);    
    e.preventDefault();
    $("#createRiskNoteForm").validate();
    $.ajax({
        cache: false,
        type: "POST",
        url: "/Note/CreateRiskNote",
        data: formData,
        contentType: false,
        processData: false,
        context: $(this),
        success: function (data) {
            var retName = data.nameret;
            if (retName == "SingleSuccess") {
                $('#createRiskNoteForm')[0].reset();
                $(this).closest(".dialog").dialog("close");
                $('.dialog').remove();
                $("<div></div>")
                .addClass("dialog")
                .attr("id", "Claim Notes")
                .appendTo("body")
                .dialog({
                    close: function () { $(this).remove() },
                    cache: false,
                    height: 530,
                    width: 1060,
                    title: "Claim Notes",
                    modal: true
                })
                .empty().load(data.Url);
            }
            else if (retName == "MultiSuccess") {
                $(this).closest(".dialog").dialog("close");
                custom_alert("New note successfully created", "Success");                
            }
            else  {
                fillErrorList(data);
                showValidationErrors(true);
            }
        },

        error: function (data) {
        }
    });

    function showValidationErrors(isShown) {
        if (isShown) {
            $("#errorContainer").show();
        } else {
            $("#errorContainer").hide();
        }
    }

    function fillErrorList(errors) {
        $("#errors").html("");

        var list = document.createElement('ul');

        for (var i = 0; i < errors.length; i++) {
            var item = document.createElement('li');
            item.appendChild(document.createTextNode(errors[i]));
            list.appendChild(item);
        }
        $("#errors").html(list);
    }

    function custom_alert(output_msg, title_msg) {
        if (!title_msg)
            title_msg = 'Alert';

        if (!output_msg)
            output_msg = 'No Message to Display.';

        $("<div></div>").html(output_msg).dialog({
            title: title_msg,
            resizable: false,
            modal: true,
            buttons: {
                "Ok": function () {
                    $(this).dialog("close");
                    if ($("#clientbatchreference input:checked").length < 1) {
                        //alert($("#clientbatchreference input:checked").length);
                        $("#checkallbatches").prop('checked', true);
                        $("#clientbatchreference input").each(function () {
                            $(this).prop("checked", true);
                        });
                    }

                    $('#ClaimsForm').submit();
                }
            }
        });
    }

});


$("#editRiskNoteForm").submit(function (e) {
    e.preventDefault();
    $("#editRiskNoteForm").validate();
    $.ajax({
        cache: false,
        type: "POST",
        url: "/Note/EditRiskNote",
        data: $("form").serializeArray(),
        context: $(this),
        success: function (data) {
            var retName = data.nameret;
            if (retName == "Success") {
                $('#editRiskNoteForm')[0].reset();
                $(this).closest(".dialog").dialog("close");
                $('.dialog').remove();
                $("<div></div>")
                .addClass("dialog")
                .attr("id", "Claim Notes")
                .appendTo("body")
                .dialog({
                    close: function () { $(this).remove() },
                    cache: false,
                    height: 530,
                    width: 1060,
                    title: "Claim Notes",
                    modal: true
                })
                .empty().load(data.Url);
            }
            else {
                fillErrorList(data);
                showValidationErrors(true);
            }
        },

        error: function (data) {
        }
    });

    function showValidationErrors(isShown) {
        if (isShown) {
            $("#errorContainer").show();
        } else {
            $("#errorContainer").hide();
        }
    }

    function fillErrorList(errors) {
        $("#errors").html("");

        var list = document.createElement('ul');

        for (var i = 0; i < errors.length; i++) {
            var item = document.createElement('li');
            item.appendChild(document.createTextNode(errors[i]));
            list.appendChild(item);
        }
        $("#errors").html(list);
    }

    function custom_alert(output_msg, title_msg) {
        if (!title_msg)
            title_msg = 'Alert';

        if (!output_msg)
            output_msg = 'No Message to Display.';

        $("<div></div>").html(output_msg).dialog({
            title: title_msg,
            resizable: false,
            modal: true,
            buttons: {
                "Ok": function () {
                    $(this).dialog("close");
                    if ($("#clientbatchreference input:checked").length < 1) {
                        //alert($("#clientbatchreference input:checked").length);
                        $("#checkallbatches").prop('checked', true);
                        $("#clientbatchreference input").each(function () {
                            $(this).prop("checked", true);
                        });
                    }

                    $('#ClaimsForm').submit();
                }
            }
        });
    }

});

