﻿using MDA.WCF.WebServices.Interface;
using KeoghsClientSite.Models;
using MDA.WCF.WebServices.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.StaticClasses;
using System.Globalization;

namespace KeoghsClientSite.Repositories.Search
{
    public static class PopulateSearch
    {
        internal static Models.SearchFilterViewModel PopulateSearchFilterViewModel(int currentPage = 1, int pageSize = 5)
        {
            SearchFilterViewModel searchFilterViewModel = new SearchFilterViewModel();

            searchFilterViewModel.PagingInfo.ItemsPerPage = pageSize;
            searchFilterViewModel.PagingInfo.CurrentPage = currentPage;

            return searchFilterViewModel;
        }
        internal static HomeViewModel FilterModel(SearchFilterViewModel oModel)
        {
            var searchClaimsListViewModel = new SearchClaimsListViewModel();

            int totalCount;
            searchClaimsListViewModel.searchFilterViewModel = oModel;
            searchClaimsListViewModel.Results = PopulateSearch.GetResultsFromSearch(searchClaimsListViewModel.searchFilterViewModel, out totalCount);
            searchClaimsListViewModel.searchFilterViewModel.PagingInfo.TotalItems = totalCount;
         
            HomeViewModel homeViewModel = new HomeViewModel();
            homeViewModel.searchClaimsListViewModel = searchClaimsListViewModel;
            return (homeViewModel);
        }

        private static List<ExSearchDetails> GetResultsFromSearch(SearchFilterViewModel searchFilterViewModel, out int totalCount)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebSearchResultsResponse response = proxy.FindSearchResults(new WebSearchResultsRequest(){
                ClientId = SiteHelpers.LoggedInClient.Id,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                Page = searchFilterViewModel.PagingInfo.CurrentPage,
                PageSize = searchFilterViewModel.PagingInfo.ItemsPerPage,
                FilterFirstName = searchFilterViewModel.FilterFirstName,
                FilterSurname = searchFilterViewModel.FilterSurname,
                FilterDob = searchFilterViewModel.FilterDob,
                FilterAddressLine = searchFilterViewModel.FilterAddressLine,
                FilterAddressPostcode = searchFilterViewModel.FilterPostcode,
                FilterVehicleRegNumber = searchFilterViewModel.FilterVehicleRegNumber
            });

            totalCount = response.TotalRows;

            return response.SearchResultsList.ToList();
        }
    }
}