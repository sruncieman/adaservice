﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.StaticClasses;
using MDA.Common.FileModel;
using MDA.WCF.WebServices.Interface;
using MDA.WCF.WebServices.Messages;
using System.Configuration;
using MDA.Common.Enum;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        public static int GetTotalSingleClaimsForClient()
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            int clientId = SiteHelpers.LoggedInUser.RiskClientId;

            WebsiteGetTotalSingleClaimsForClientResponse response =
                proxy.GetTotalSingleClaimForClient(new WebsiteGetTotalSingleClaimsForClientRequest
                {
                    ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                });

            return response.TotalRows;
        }


        public static OutstandingClaimsListViewModel PopulateOutstandingClaimsListViewModel(bool bPosted,
                                                                                            OutstandingClaimsListViewModel
                                                                                                outstandingClaimsListViewModel)
        {
            if (!bPosted)
            {
                // Defaults
                outstandingClaimsListViewModel.PagingInfo.ItemsPerPage =
                    Convert.ToInt16(ConfigurationManager.AppSettings["OutstandingItemsPerPage"]);
                outstandingClaimsListViewModel.PagingInfo.CurrentPage = 1;
                outstandingClaimsListViewModel.SortColumn = "UPLOADDATETIME";
                outstandingClaimsListViewModel.SortAscending = false;
            }

            // Get the single claims
            int totalcount = 0;

            outstandingClaimsListViewModel.OutstandingClaimsList =
                GetSingleClaimsForClient(outstandingClaimsListViewModel, out totalcount);

            outstandingClaimsListViewModel.PagingInfo.TotalItems = totalcount;

            return outstandingClaimsListViewModel;

        }

        public static List<ExWebsiteSingleClaim> GetSingleClaimsForClient(OutstandingClaimsListViewModel outstandingClaimsListViewModel, out int totalCount)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteSingleClaimsListResponse response = proxy.GetSingleClaimsList(new WebsiteSingleClaimsListRequest
            {
                UserId = SiteHelpers.LoggedInUser.Id,
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                Page = outstandingClaimsListViewModel.PagingInfo.CurrentPage,
                PageSize = outstandingClaimsListViewModel.PagingInfo.ItemsPerPage,
                SortColumn = outstandingClaimsListViewModel.SortColumn,
                SortAscending = outstandingClaimsListViewModel.SortAscending,
                TeamId = outstandingClaimsListViewModel.SelectedTeamId,
                StatusFilter = 0
            });

            totalCount = response.TotalRows;

            return response.WebsiteSingleClaimsList.ToList();
        }

        public static WebsiteDeleteSingleClaimResponse DeleteOutstandingClaim(int riskClaimId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteDeleteSingleClaimRequest request = new WebsiteDeleteSingleClaimRequest();

            request.ClientId = SiteHelpers.LoggedInUser.RiskClientId;
            request.EditClaimId = riskClaimId;
            request.UserId = SiteHelpers.LoggedInUser.Id;

            WebsiteDeleteSingleClaimResponse response = proxy.DeleteSingleClaim(request);

            return response;
        }

        public static WebsiteFetchSingleClaimResponse FetchSavedSingleClaim(int id)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteFetchSingleClaimRequest request = new WebsiteFetchSingleClaimRequest();

            request.ClientId = SiteHelpers.LoggedInUser.RiskClientId;
            request.EditClaimId = id;
            request.UserId = SiteHelpers.LoggedInUser.Id;

            WebsiteFetchSingleClaimResponse response = proxy.FetchSingleClaimDetail(request);

            return response;
        }

        public static WebsiteSaveSingleClaimResponse SaveSingleClaim(MDA.Common.FileModel.IClaim claim, int? singleClaimId, string claimStatus)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteSaveSingleClaimRequest request = new WebsiteSaveSingleClaimRequest();

            request.ClaimToSave = claim;
            switch (claim.ClaimType)
            {
                case ClaimType.Motor:
                    request.ClaimType = "Motor";
                    break;
            }

            request.ClientId = SiteHelpers.LoggedInUser.RiskClientId;

            if (singleClaimId != null)
                request.EditClaimId = singleClaimId;

            request.IncidentDate = claim.IncidentDate;

            String fullUserName = SiteHelpers.LoggedInUser.FirstName;
            fullUserName += " ";
            fullUserName += SiteHelpers.LoggedInUser.LastName;
            request.ModifiedBy = fullUserName;
            request.ModifiedDate = DateTime.Now;
            request.RiskClaimNumber = claim.ClaimNumber;
            request.UserId = SiteHelpers.LoggedInUser.Id;
            request.EditClaimId = singleClaimId;
            request.Status = claimStatus;
            request.Who = SiteHelpers.LoggedInUser.UserName;

            WebsiteSaveSingleClaimResponse response = proxy.SaveSingleClaim(request);

            return response;
        }

        public static SingleClaimModel Save()
        {
            SingleClaimModel singleClaimModel = new SingleClaimModel();

            //Check session data
            ClaimViewModel claimViewModel = new ClaimViewModel();
            IncidentViewModel incidentViewModel = new IncidentViewModel();
            PolicyViewModel policyViewModel = new PolicyViewModel();
            PolicyHolderCommercialViewModel policyHolderCommercialViewModel = new PolicyHolderCommercialViewModel();
            PolicyHolderPersonalViewModel policyHolderPersonalViewModel = new PolicyHolderPersonalViewModel();
            //ObservableCollection<vehicleAlias> claimVehicles = new ObservableCollection<vehicleAlias>();
            //vehicleAlias claimVehicles = new vehicleAlias();
            InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();
            VehicleAdditionalInfo vehicleAdditionalInfo = new VehicleAdditionalInfo();
            InsuredDriverPerson insuredDriverPerson = new InsuredDriverPerson();
            InsuredDriverAddress insuredDriverAddress = new InsuredDriverAddress();
            InsuredDriverViewModel insuredDriverViewModel = new InsuredDriverViewModel();
            PassengerViewModel passengerViewModel = new PassengerViewModel();
            ThirdPartyVehicleViewModel thirdPartyVehicleViewModel = new ThirdPartyVehicleViewModel();
            //ThirdPartyDriverViewModel thirdPartyDriverViewModel = new ThirdPartyDriverViewModel();
            WitnessViewModel witnessViewModel = new WitnessViewModel();
            ReviewAndSubmitViewModel reviewAndSubmitViewModel = new ReviewAndSubmitViewModel();
            Dictionary<int, ThirdPartyDriverViewModel> dictionaryThirdPartyDriverViewModels = new Dictionary<int, ThirdPartyDriverViewModel>();





            if (System.Web.HttpContext.Current.Session["claimViewModel"] != null)
                claimViewModel = System.Web.HttpContext.Current.Session["claimViewModel"] as ClaimViewModel;

            if (System.Web.HttpContext.Current.Session["incidentViewModel"] != null)
                incidentViewModel = System.Web.HttpContext.Current.Session["incidentViewModel"] as IncidentViewModel;

            if (System.Web.HttpContext.Current.Session["policyViewModel"] != null)
                policyViewModel = System.Web.HttpContext.Current.Session["policyViewModel"] as PolicyViewModel;

            if (System.Web.HttpContext.Current.Session["policyHolderCommercialViewModel"] != null)
                policyHolderCommercialViewModel = System.Web.HttpContext.Current.Session["policyHolderCommercialViewModel"] as PolicyHolderCommercialViewModel;

            if (System.Web.HttpContext.Current.Session["policyHolderPersonalViewModel"] != null)
                policyHolderPersonalViewModel = System.Web.HttpContext.Current.Session["policyHolderPersonalViewModel"] as PolicyHolderPersonalViewModel;

            //if (System.Web.HttpContext.Current.Session["claimVehicles"] != null)
            //    claimVehicles = System.Web.HttpContext.Current.Session["claimVehicles"] as vehicleAlias;

            if (System.Web.HttpContext.Current.Session["insuredVehicleViewModel"] != null)
                insuredVehicleViewModel = System.Web.HttpContext.Current.Session["insuredVehicleViewModel"] as InsuredVehicleViewModel;

            if (System.Web.HttpContext.Current.Session["vehicleAdditionalInfo"] != null)
                vehicleAdditionalInfo = System.Web.HttpContext.Current.Session["vehicleAdditionalInfo"] as VehicleAdditionalInfo;

            if (System.Web.HttpContext.Current.Session["insuredDriverPerson"] != null)
                insuredDriverPerson = System.Web.HttpContext.Current.Session["insuredDriverPerson"] as InsuredDriverPerson;

            if (System.Web.HttpContext.Current.Session["insuredDriverAddress"] != null)
                insuredDriverAddress = System.Web.HttpContext.Current.Session["insuredDriverAddress"] as InsuredDriverAddress;

            if (System.Web.HttpContext.Current.Session["insuredDriverViewModel"] != null)
                insuredDriverViewModel = System.Web.HttpContext.Current.Session["insuredDriverViewModel"] as InsuredDriverViewModel;

            if (System.Web.HttpContext.Current.Session["passengerViewModel"] != null)
                passengerViewModel = System.Web.HttpContext.Current.Session["passengerViewModel"] as PassengerViewModel;

            if (System.Web.HttpContext.Current.Session["thirdPartyVehicleViewModel"] != null)
                thirdPartyVehicleViewModel = System.Web.HttpContext.Current.Session["thirdPartyVehicleViewModel"] as ThirdPartyVehicleViewModel;

            //if (System.Web.HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] != null)
            //    dictionaryThirdPartyDriverViewModels = (Dictionary<int, ThirdPartyDriverViewModel>)HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"];

            if (System.Web.HttpContext.Current.Session["witnessViewModel"] != null)
                witnessViewModel = System.Web.HttpContext.Current.Session["witnessViewModel"] as WitnessViewModel;


            if (HttpContext.Current.Session["ReviewAndSubmitViewModel"] != null)
                reviewAndSubmitViewModel = HttpContext.Current.Session["ReviewAndSubmitViewModel"] as ReviewAndSubmitViewModel;



            singleClaimModel.ClaimViewModel = claimViewModel;
            singleClaimModel.IncidentViewModel = incidentViewModel;
            singleClaimModel.PolicyViewModel = policyViewModel;
            singleClaimModel.PolicyHolderCommercialViewModel = policyHolderCommercialViewModel;
            singleClaimModel.PolicyHolderPersonalViewModel = policyHolderPersonalViewModel;
            //singleClaimModel.Vehicle = claimVehicles;
            singleClaimModel.InsuredVehicleViewModel = insuredVehicleViewModel;
            singleClaimModel.VehicleAdditionalInfo = vehicleAdditionalInfo;
            singleClaimModel.InsuredDriverPerson = insuredDriverPerson;
            singleClaimModel.InsuredDriverAddress = insuredDriverAddress;
            singleClaimModel.InsuredDriverViewModel = insuredDriverViewModel;
            singleClaimModel.PassengerViewModel = passengerViewModel;
            singleClaimModel.ThirdPartyVehicleViewModel = thirdPartyVehicleViewModel;
            //singleClaimModel.ThirdPartyDriverViewModel = thirdPartyDriverViewModel;
            singleClaimModel.WitnessViewModel = witnessViewModel;
            singleClaimModel.ReviewAndSubmitViewModel = reviewAndSubmitViewModel;

            MotorClaim claim = new MotorClaim();

            claim = TranslateToClaim(singleClaimModel, claim);

            if (claim.ClaimNumber != null)
            {
                WebsiteSaveSingleClaimResponse response = SaveSingleClaim(claim, claimViewModel.SingleClaimId, "Uncommitted");
                claimViewModel.SingleClaimId = response.EditClaimId;
                StoreClaimViewModel(claimViewModel);
            }
            return singleClaimModel;
        }

        public static int SubmitSingleClaim()
        {
            SingleClaimModel singleClaimModel = Save();

            MotorClaim claim = new MotorClaim();

            claim = TranslateToClaim(singleClaimModel, claim);

            claim = TrimClaim(claim);

            IADAWebServices proxy = Helper.Services.CreateADAServices();

            ProcessClaimRequest request = new ProcessClaimRequest();

            request.BatchEntityType = "Motor";
            request.Claim = claim;
            request.ClientId = SiteHelpers.LoggedInUser.RiskClientId;
            request.UserId = SiteHelpers.LoggedInUser.Id;

            DateTime strDateTime = DateTime.Now;

            string clientName = "SCIF";
            string date;

            date = DateTime.Now.ToString("dd/MM/yy HH:mm:ss");
            date = date.Replace("/", "");
            date = date.Replace(":", "");
            date = date.Replace(" ", "");

            request.ClientBatchReference = clientName + date;

            ProcessClaimResponse response = new ProcessClaimResponse();

            response = proxy.ProcessClaim(request);


            // Update Unsubmitted claim to Committed
            if (response.RiskClaimId > 0)
                SaveSingleClaim(claim, null, "Committed");

            return response.RiskClaimId;
        }

        public static MotorClaim TranslateToClaim(SingleClaimModel singleClaimModel, MotorClaim claim)
        {

            //Save

            #region Claim

            if (singleClaimModel.ClaimViewModel.SelectedClaimTypeID != null)
                claim.ClaimType = (MDA.Common.Enum.ClaimType)singleClaimModel.ClaimViewModel.SelectedClaimTypeID;//ClaimType.Motor;
            else
            {
                // Session time out. 
                return claim;
            }

            claim.ClaimNumber = singleClaimModel.ClaimViewModel.ClaimNumber;

            if (singleClaimModel.ReviewAndSubmitViewModel.DataFeeds != null)
            {
                foreach (var dataFeed in singleClaimModel.ReviewAndSubmitViewModel.DataFeeds)
                {
                    if (dataFeed.DataFeed == "Tracesmart" && dataFeed.Included == "Additional Fee")
                        claim.ExternalServices = "Tracesmart";
                }
            }


            claim.MotorClaimInfo.ClaimStatus = ClaimStatus.Open;

            #endregion

            #region Incident

            DateTime incidentDate;

            incidentDate = singleClaimModel.IncidentViewModel.IncidentDate ?? DateTime.MinValue;

            if (!string.IsNullOrEmpty(singleClaimModel.IncidentViewModel.SelectedIncidentTimeHours) && singleClaimModel.IncidentViewModel.SelectedIncidentTimeHours != "--")
            {
                incidentDate = incidentDate.AddHours(Convert.ToDouble(singleClaimModel.IncidentViewModel.SelectedIncidentTimeHours));
            }

            if (!string.IsNullOrEmpty(singleClaimModel.IncidentViewModel.SelectedIncidentTimeMinutes) && singleClaimModel.IncidentViewModel.SelectedIncidentTimeMinutes != "--")
            {
                incidentDate = incidentDate.AddMinutes(Convert.ToDouble(singleClaimModel.IncidentViewModel.SelectedIncidentTimeMinutes));
            }

            if (incidentDate.Hour == 00 && incidentDate.Minute == 00)
            {
                incidentDate = incidentDate.AddMinutes(1);
            }

            claim.IncidentDate = incidentDate;

            claim.MotorClaimInfo.IncidentLocation = singleClaimModel.IncidentViewModel.Location;
            claim.MotorClaimInfo.IncidentCircumstances = singleClaimModel.IncidentViewModel.Circumstances;

            claim.MotorClaimInfo.IncidentLocation += "|" + singleClaimModel.IncidentViewModel.SelectedNumberOfVehicles;
            claim.MotorClaimInfo.IncidentLocation += "|" + singleClaimModel.IncidentViewModel.SelectedNumberOfWitnesses;


            #region Additional Information

            claim.MotorClaimInfo.ClaimNotificationDate = singleClaimModel.IncidentViewModel.NotificationDate;

            //if (!string.IsNullOrEmpty(singleClaimModel.IncidentViewModel.SelectedClaimCode))
            //claim.MotorClaimInfo.ClaimCode = singleClaimModel.IncidentViewModel.SelectedClaimCode;

            if (!string.IsNullOrEmpty(singleClaimModel.IncidentViewModel.SelectedClaimCode))
            {

                switch (singleClaimModel.IncidentViewModel.SelectedClaimCode)
                {
                    case "0":
                        claim.MotorClaimInfo.ClaimCode = "Insured damage only";
                        break;
                    case "1":
                        claim.MotorClaimInfo.ClaimCode = "TP damage only";
                        break;
                    case "2":
                        claim.MotorClaimInfo.ClaimCode = "Damage & PI";
                        break;
                }

            }

            claim.MotorClaimInfo.Reserve = singleClaimModel.IncidentViewModel.TotalValue;
            claim.MotorClaimInfo.PaymentsToDate = singleClaimModel.IncidentViewModel.TotalPayments;

            //if (!string.IsNullOrEmpty(singleClaimModel.IncidentViewModel.SelectedLiabilityDecision))
            claim.MotorClaimInfo.ClaimCode += "|" + singleClaimModel.IncidentViewModel.SelectedLiabilityDecision;


            claim.MotorClaimInfo.ClaimCode += "|" + singleClaimModel.IncidentViewModel.SelectedMojProcessStage;


            switch (singleClaimModel.IncidentViewModel.SelectedAmbulanceAttend)
            {
                case 0:
                    claim.MotorClaimInfo.AmbulanceAttended = true;
                    break;
                case 1:
                    claim.MotorClaimInfo.AmbulanceAttended = false;
                    break;
                default:
                    claim.MotorClaimInfo.AmbulanceAttended = null;
                    break;
            }

            switch (singleClaimModel.IncidentViewModel.SelectedPoliceAttend)
            {
                case 0:
                    claim.MotorClaimInfo.PoliceAttended = true;
                    break;
                case 1:
                    claim.MotorClaimInfo.PoliceAttended = false;
                    break;
                default:
                    claim.MotorClaimInfo.PoliceAttended = null;
                    break;
            }

            if (!string.IsNullOrEmpty(singleClaimModel.IncidentViewModel.PoliceForce))
                claim.MotorClaimInfo.PoliceForce = singleClaimModel.IncidentViewModel.PoliceForce;

            if (!string.IsNullOrEmpty(singleClaimModel.IncidentViewModel.PoliceReference))
                claim.MotorClaimInfo.PoliceReference = singleClaimModel.IncidentViewModel.PoliceReference;


            #endregion

            #endregion

            #region Policy

            if (!string.IsNullOrEmpty(singleClaimModel.PolicyViewModel.PolicyNumber))
                claim.Policy.PolicyNumber = singleClaimModel.PolicyViewModel.PolicyNumber;

            switch (singleClaimModel.PolicyViewModel.SelectedPolicyType)
            {
                case 0:
                    claim.Policy.PolicyType = PolicyType.CommercialMotor;
                    break;
                case 1:
                    claim.Policy.PolicyType = PolicyType.PersonalMotor;
                    break;
                case 2:
                    claim.Policy.PolicyType = PolicyType.Unknown;
                    break;
            }

            claim.MotorClaimInfo.ClaimCode += "|" + singleClaimModel.PolicyViewModel.PolicyHolderDetailsKnown;

            #region Additional Information

            claim.Policy.PolicyStartDate = singleClaimModel.PolicyViewModel.PolicyStartDate;
            claim.Policy.PolicyEndDate = singleClaimModel.PolicyViewModel.PolicyEndDate;

            switch (singleClaimModel.PolicyViewModel.SelectedCoverType)
            {
                case 0:
                    claim.Policy.CoverType = PolicyCoverType.Comprehensive;
                    break;
                case 1:
                    claim.Policy.CoverType = PolicyCoverType.ThirdPartyOnly;
                    break;
                case 2:
                    claim.Policy.CoverType = PolicyCoverType.ThirdPartyFireAndTheft;
                    break;
            }

            if (singleClaimModel.PolicyViewModel.PolicyPremium > 0)
                claim.Policy.Premium = singleClaimModel.PolicyViewModel.PolicyPremium;

            if (!string.IsNullOrEmpty(singleClaimModel.PolicyViewModel.Broker))
                claim.Policy.Broker = singleClaimModel.PolicyViewModel.Broker;

            //claim.ExtraClaimInfo.ClaimCode += "|" + singleClaimModel.PolicyViewModel.PolicyHolderDetailsKnown;

            #endregion

            #endregion

            #region Policy Holder (Commercial)

            MDA.Common.FileModel.Organisation claimOrg = new MDA.Common.FileModel.Organisation();

            MDA.Common.FileModel.Address claimAddress = new MDA.Common.FileModel.Address();

            if (claim.Policy.PolicyType == PolicyType.CommercialMotor)
            {
                claimOrg.OrganisationType = OrganisationType.PolicyHolder;

                if (singleClaimModel.PolicyHolderCommercialViewModel.Organisation.Name != null)
                {
                    claimOrg.OrganisationName = singleClaimModel.PolicyHolderCommercialViewModel.Organisation.Name;
                }

                if (singleClaimModel.PolicyHolderCommercialViewModel.Organisation.RegisteredNumber != null)
                {
                    claimOrg.RegisteredNumber = singleClaimModel.PolicyHolderCommercialViewModel.Organisation.RegisteredNumber;
                }

                #region Address

                if (singleClaimModel.PolicyHolderCommercialViewModel.Address != null)
                {

                    if (singleClaimModel.PolicyHolderCommercialViewModel.Address.SelectedAddressStatus != null)
                    {

                        switch (singleClaimModel.PolicyHolderCommercialViewModel.Address.SelectedAddressStatus)
                        {
                            case 1:
                                claimAddress.AddressLinkType = AddressLinkType.PreviousAddress;
                                break;
                            case 2:
                                claimAddress.AddressLinkType = AddressLinkType.LinkedAddress;
                                break;
                            case 3:
                                claimAddress.AddressLinkType = AddressLinkType.CurrentAddress;
                                break;
                            case 4:
                                claimAddress.AddressLinkType = AddressLinkType.RegisteredOffice;
                                break;
                            case 5:
                                claimAddress.AddressLinkType = AddressLinkType.TradingAddress;
                                break;
                            case 6:
                                claimAddress.AddressLinkType = AddressLinkType.StorageAddress;
                                break;
                            default:
                                claimAddress.AddressLinkType = AddressLinkType.Unknown;
                                break;
                        }

                    }
                    else
                    {
                        claimAddress.AddressLinkType = AddressLinkType.Unknown;
                    }

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderCommercialViewModel.Address.BuildingName))
                        claimAddress.Building = singleClaimModel.PolicyHolderCommercialViewModel.Address.BuildingName;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderCommercialViewModel.Address.BuildingNumber))
                        claimAddress.BuildingNumber = singleClaimModel.PolicyHolderCommercialViewModel.Address.BuildingNumber;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderCommercialViewModel.Address.County))
                        claimAddress.County = singleClaimModel.PolicyHolderCommercialViewModel.Address.County;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderCommercialViewModel.Address.SubBuilding))
                        claimAddress.SubBuilding = singleClaimModel.PolicyHolderCommercialViewModel.Address.SubBuilding;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderCommercialViewModel.Address.Street))
                        claimAddress.Street = singleClaimModel.PolicyHolderCommercialViewModel.Address.Street;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderCommercialViewModel.Address.Town))
                        claimAddress.Town = singleClaimModel.PolicyHolderCommercialViewModel.Address.Town;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderCommercialViewModel.Address.Locality))
                        claimAddress.Locality = singleClaimModel.PolicyHolderCommercialViewModel.Address.Locality;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderCommercialViewModel.Address.Postcode))
                        claimAddress.PostCode = singleClaimModel.PolicyHolderCommercialViewModel.Address.Postcode;

                    if (singleClaimModel.PolicyHolderCommercialViewModel.Address.StartOfResidency != null)
                        claimAddress.StartOfResidency = singleClaimModel.PolicyHolderCommercialViewModel.Address.StartOfResidency;

                    if (singleClaimModel.PolicyHolderCommercialViewModel.Address.EndOfResidency != null)
                        claimAddress.EndOfResidency = singleClaimModel.PolicyHolderCommercialViewModel.Address.EndOfResidency;


                    if (claimAddress.Street != null || claimAddress.SubBuilding != null || claimAddress.Building != null || claimAddress.BuildingNumber != null || claimAddress.Locality != null || claimAddress.PostCode != null)
                        claimOrg.Addresses.Add(claimAddress);
                }

                #endregion

                #region Bank Account

                if (singleClaimModel.PolicyHolderCommercialViewModel.BankAccount.AccountNumber != null)
                {
                    claimOrg.BankAccount.AccountNumber = singleClaimModel.PolicyHolderCommercialViewModel.BankAccount.AccountNumber;
                }

                if (singleClaimModel.PolicyHolderCommercialViewModel.BankAccount.SortCode != null)
                {
                    claimOrg.BankAccount.SortCode = singleClaimModel.PolicyHolderCommercialViewModel.BankAccount.SortCode;
                }

                if (singleClaimModel.PolicyHolderCommercialViewModel.BankAccount.BankName != null)
                {
                    claimOrg.BankAccount.BankName = singleClaimModel.PolicyHolderCommercialViewModel.BankAccount.BankName;
                }

                if (singleClaimModel.PolicyHolderCommercialViewModel.BankAccount.DateDetailsTaken != null)
                {
                    claimOrg.BankAccount.DatePaymentDetailsTaken = singleClaimModel.PolicyHolderCommercialViewModel.BankAccount.DateDetailsTaken;
                }

                if (singleClaimModel.PolicyHolderCommercialViewModel.BankAccount.SelectedAccountUsedFor != null)
                {
                    claimOrg.BankAccount.PolicyPaymentType = (PolicyPaymentType)singleClaimModel.PolicyHolderCommercialViewModel.BankAccount.SelectedAccountUsedFor;
                }

                #endregion

                #region Additional Information

                if (singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.VATNumber != null)
                {
                    claimOrg.VatNumber = singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.VATNumber;
                }

                if (singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.MoJClaimsRegNumber != null)
                {
                    claimOrg.MojCrmNumber = singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.MoJClaimsRegNumber;
                }

                //"MojCrmStatus": 0,

                if (singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.EmailAddress != null)
                {
                    claimOrg.Email = singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.EmailAddress;
                }

                if (singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.WebsiteAddress != null)
                {
                    claimOrg.WebSite = singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.WebsiteAddress;
                }

                if (singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.Telephone1 != null)
                {
                    claimOrg.Telephone1 = singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.Telephone1;
                }

                if (singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.Telephone2 != null)
                {
                    claimOrg.Telephone2 = singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.Telephone2;
                }

                if (singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.Telephone3 != null)
                {
                    claimOrg.Telephone3 = singleClaimModel.PolicyHolderCommercialViewModel.OrganisationAdditionalInfo.Telephone3;
                }

                #endregion

                if (claimOrg != null)
                {
                    claim.Organisations.Add(claimOrg);
                }

            }

            #endregion

            #region Policy Holder (Private)

            MDA.Common.FileModel.Person policyHolder = new MDA.Common.FileModel.Person();

            if (claim.Policy.PolicyType == PolicyType.PersonalMotor && singleClaimModel.PolicyViewModel.PolicyHolderDetailsKnown == true)
            {
                policyHolder.PartyType = PartyType.Policyholder;

                if (singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.SelectedSalutation != null)
                {
                    policyHolder.Salutation = (Salutation)singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.SelectedSalutation;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.Firstname != null)
                {
                    policyHolder.FirstName = singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.Firstname;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.Middlename != null)
                {
                    policyHolder.MiddleName = singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.Middlename;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.Surname != null)
                {
                    policyHolder.LastName = singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.Surname;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.DateOfBirth != null)
                {
                    policyHolder.DateOfBirth = singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.DateOfBirth;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.SelectedGender != null)
                {
                    policyHolder.Gender = (Gender)singleClaimModel.PolicyHolderPersonalViewModel.PersonInformation.SelectedGender;
                }

                #region Address

                if (singleClaimModel.PolicyHolderPersonalViewModel.Address != null)
                {

                    if (singleClaimModel.PolicyHolderPersonalViewModel.Address.SelectedAddressStatus != null)
                    {

                        switch (singleClaimModel.PolicyHolderPersonalViewModel.Address.SelectedAddressStatus)
                        {
                            case 1:
                                claimAddress.AddressLinkType = AddressLinkType.PreviousAddress;
                                break;
                            case 2:
                                claimAddress.AddressLinkType = AddressLinkType.LinkedAddress;
                                break;
                            case 3:
                                claimAddress.AddressLinkType = AddressLinkType.CurrentAddress;
                                break;
                            case 4:
                                claimAddress.AddressLinkType = AddressLinkType.RegisteredOffice;
                                break;
                            case 5:
                                claimAddress.AddressLinkType = AddressLinkType.TradingAddress;
                                break;
                            case 6:
                                claimAddress.AddressLinkType = AddressLinkType.StorageAddress;
                                break;
                            default:
                                claimAddress.AddressLinkType = AddressLinkType.Unknown;
                                break;
                        }

                    }
                    else
                    {
                        claimAddress.AddressLinkType = AddressLinkType.Unknown;
                    }

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderPersonalViewModel.Address.BuildingName))
                        claimAddress.Building = singleClaimModel.PolicyHolderPersonalViewModel.Address.BuildingName;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderPersonalViewModel.Address.BuildingNumber))
                        claimAddress.BuildingNumber = singleClaimModel.PolicyHolderPersonalViewModel.Address.BuildingNumber;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderPersonalViewModel.Address.SubBuilding))
                        claimAddress.SubBuilding = singleClaimModel.PolicyHolderPersonalViewModel.Address.SubBuilding;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderPersonalViewModel.Address.Street))
                        claimAddress.Street = singleClaimModel.PolicyHolderPersonalViewModel.Address.Street;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderPersonalViewModel.Address.Town))
                        claimAddress.Town = singleClaimModel.PolicyHolderPersonalViewModel.Address.Town;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderPersonalViewModel.Address.Locality))
                        claimAddress.Locality = singleClaimModel.PolicyHolderPersonalViewModel.Address.Locality;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderPersonalViewModel.Address.County))
                        claimAddress.County = singleClaimModel.PolicyHolderPersonalViewModel.Address.County;

                    if (!string.IsNullOrEmpty(singleClaimModel.PolicyHolderPersonalViewModel.Address.Postcode))
                        claimAddress.PostCode = singleClaimModel.PolicyHolderPersonalViewModel.Address.Postcode;

                    if (singleClaimModel.PolicyHolderPersonalViewModel.Address.StartOfResidency != null)
                        claimAddress.StartOfResidency = singleClaimModel.PolicyHolderPersonalViewModel.Address.StartOfResidency;

                    if (singleClaimModel.PolicyHolderPersonalViewModel.Address.EndOfResidency != null)
                        claimAddress.EndOfResidency = singleClaimModel.PolicyHolderPersonalViewModel.Address.EndOfResidency;


                    if (claimAddress.Street != null || claimAddress.SubBuilding != null || claimAddress.Building != null || claimAddress.BuildingNumber != null || claimAddress.Locality != null || claimAddress.PostCode != null)
                        policyHolder.Addresses.Add(claimAddress);
                }

                #endregion

                #region Bank Account

                if (singleClaimModel.PolicyHolderPersonalViewModel.BankAccount != null)
                {
                    MDA.Common.FileModel.BankAccount bankAccount = new MDA.Common.FileModel.BankAccount();

                    if (singleClaimModel.PolicyHolderPersonalViewModel.BankAccount.AccountNumber != null)
                    {
                        bankAccount.AccountNumber = singleClaimModel.PolicyHolderPersonalViewModel.BankAccount.AccountNumber;
                    }

                    if (singleClaimModel.PolicyHolderPersonalViewModel.BankAccount.SortCode != null)
                    {
                        bankAccount.SortCode = singleClaimModel.PolicyHolderPersonalViewModel.BankAccount.SortCode;
                    }

                    if (singleClaimModel.PolicyHolderPersonalViewModel.BankAccount.BankName != null)
                    {
                        bankAccount.BankName = singleClaimModel.PolicyHolderPersonalViewModel.BankAccount.BankName;
                    }

                    if (singleClaimModel.PolicyHolderPersonalViewModel.BankAccount.DateDetailsTaken != null)
                    {
                        bankAccount.DatePaymentDetailsTaken = singleClaimModel.PolicyHolderPersonalViewModel.BankAccount.DateDetailsTaken;
                    }

                    if (singleClaimModel.PolicyHolderPersonalViewModel.BankAccount.SelectedAccountUsedFor != null)
                    {
                        bankAccount.PolicyPaymentType = (PolicyPaymentType)singleClaimModel.PolicyHolderPersonalViewModel.BankAccount.SelectedAccountUsedFor;
                    }

                    policyHolder.BankAccount = bankAccount;
                }

                #endregion

                #region Additional Information

                if (singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.HomeTelephone != null)
                {
                    policyHolder.LandlineTelephone = singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.HomeTelephone;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.WorkTelephone != null)
                {
                    policyHolder.WorkTelephone = singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.WorkTelephone;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.MobileNumber != null)
                {
                    policyHolder.MobileTelephone = singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.MobileNumber;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.EmailAddress != null)
                {
                    policyHolder.EmailAddress = singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.EmailAddress;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.Occupation != null)
                {
                    policyHolder.Occupation = singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.Occupation;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.SelectedNationality != null)
                {
                    policyHolder.Nationality = singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.SelectedNationality;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.PassportNumber != null)
                {
                    policyHolder.PassportNumber = singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.PassportNumber;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.NINumber != null)
                {
                    policyHolder.NINumber = singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.NINumber;
                }

                if (singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.DrivingLicenseNumber != null)
                {
                    policyHolder.DrivingLicenseNumber = singleClaimModel.PolicyHolderPersonalViewModel.AdditionalInfo.DrivingLicenseNumber;
                }

                #endregion

                if (policyHolder.FirstName != null || policyHolder.MiddleName != null || policyHolder.LastName != null || policyHolder.DateOfBirth != null || policyHolder.EmailAddress != null || policyHolder.LandlineTelephone != null || policyHolder.MobileTelephone != null || policyHolder.WorkTelephone != null)
                {

                    if (singleClaimModel.InsuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver == false || singleClaimModel.InsuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver == null)
                    {
                        MDA.Common.FileModel.Vehicle noVehicleInvolved = new MDA.Common.FileModel.Vehicle();

                        noVehicleInvolved.Incident2VehicleLinkType = Incident2VehicleLinkType.NoVehicleInvolved;

                        policyHolder.SubPartyType = SubPartyType.Unknown;

                        noVehicleInvolved.People.Add(policyHolder);

                        claim.Vehicles.Add(noVehicleInvolved);

                    }

                }

            }

            #endregion

            #region Insured Vehicle

            MDA.Common.FileModel.Vehicle insuredVehicle = new MDA.Common.FileModel.Vehicle();

            insuredVehicle.DamageDescription = singleClaimModel.InsuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver.ToString();
            insuredVehicle.DamageDescription += "|" + singleClaimModel.InsuredVehicleViewModel.Vehicle.NumberOfPassengers.ToString();
            insuredVehicle.DamageDescription += "|" + "99";

            if (singleClaimModel.InsuredVehicleViewModel.Vehicle.SelectedVehicletype != null)
            {
                insuredVehicle.VehicleType = (VehicleType)singleClaimModel.InsuredVehicleViewModel.Vehicle.SelectedVehicletype;
            }

            if (singleClaimModel.InsuredVehicleViewModel.Vehicle.RegistrationNumber != null)
            {
                insuredVehicle.VehicleRegistration = singleClaimModel.InsuredVehicleViewModel.Vehicle.RegistrationNumber;
            }

            if (singleClaimModel.InsuredVehicleViewModel.Vehicle.Make != null)
            {
                insuredVehicle.Make = singleClaimModel.InsuredVehicleViewModel.Vehicle.Make;
            }

            if (singleClaimModel.InsuredVehicleViewModel.Vehicle.Model != null)
            {
                insuredVehicle.Model = singleClaimModel.InsuredVehicleViewModel.Vehicle.Model;
            }

            if (singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.EngineCapacity != null)
            {
                insuredVehicle.EngineCapacity = singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.EngineCapacity;
            }

            if (singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.SelectedFuel != null)
            {
                insuredVehicle.Fuel = (VehicleFuelType)singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.SelectedFuel;
            }

            if (singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.SelectedColour != null)
            {
                insuredVehicle.Colour = (VehicleColour)singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.SelectedColour;
            }

            if (singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.SelectedTransmission != null)
            {
                insuredVehicle.Transmission = (VehicleTransmission)singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.SelectedTransmission;
            }

            if (singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.VIN != null)
            {
                insuredVehicle.VIN = singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.VIN;
            }

            if (singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.SelectedTotalLossCapacity != null)
            {
                insuredVehicle.CategoryOfLoss = (VehicleCategoryOfLoss)singleClaimModel.InsuredVehicleViewModel.VehicleAdditionalInfo.SelectedTotalLossCapacity;
            }

            //if (insuredVehicle.Make != null || insuredVehicle.Model != null || insuredVehicle.VehicleRegistration != null || insuredVehicle.VIN != null)
            //{

                insuredVehicle.Incident2VehicleLinkType = Incident2VehicleLinkType.InsuredVehicle;

            //}


            if (singleClaimModel.InsuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver == true)
            {
                policyHolder.SubPartyType = SubPartyType.Driver;

                insuredVehicle.People.Add(policyHolder);
            }


            #endregion

            #region Insured Driver

            MDA.Common.FileModel.Person insuredDriver = new MDA.Common.FileModel.Person();

            if (singleClaimModel.InsuredDriverViewModel.PersonInformation.SelectedSalutation != null)
            {
                insuredDriver.Salutation = (Salutation)singleClaimModel.InsuredDriverViewModel.PersonInformation.SelectedSalutation;
            }

            if (singleClaimModel.InsuredDriverViewModel.PersonInformation.Firstname != null)
            {
                insuredDriver.FirstName = singleClaimModel.InsuredDriverViewModel.PersonInformation.Firstname;
            }

            if (singleClaimModel.InsuredDriverViewModel.PersonInformation.Middlename != null)
            {
                insuredDriver.MiddleName = singleClaimModel.InsuredDriverViewModel.PersonInformation.Middlename;
            }

            if (singleClaimModel.InsuredDriverViewModel.PersonInformation.Surname != null)
            {
                insuredDriver.LastName = singleClaimModel.InsuredDriverViewModel.PersonInformation.Surname;
            }

            if (singleClaimModel.InsuredDriverViewModel.PersonInformation.DateOfBirth != null)
            {
                insuredDriver.DateOfBirth = singleClaimModel.InsuredDriverViewModel.PersonInformation.DateOfBirth;
            }

            if (singleClaimModel.InsuredDriverViewModel.PersonInformation.SelectedGender != null)
            {
                insuredDriver.Gender = (Gender)singleClaimModel.InsuredDriverViewModel.PersonInformation.SelectedGender;
            }

            if (singleClaimModel.InsuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver == false || claim.Policy.PolicyType == PolicyType.CommercialMotor || singleClaimModel.InsuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver == null)
            {
                insuredVehicle.People.Add(insuredDriver);
            }


            if (singleClaimModel.InsuredDriverViewModel != null)
            {
                insuredDriver.PartyType = PartyType.Insured;
                insuredDriver.SubPartyType = SubPartyType.Driver;
            }

            #region Address

            if (singleClaimModel.InsuredDriverViewModel.Address != null)
            {

                MDA.Common.FileModel.Address insuredDriverAddress = new MDA.Common.FileModel.Address();


                if (singleClaimModel.InsuredDriverViewModel.Address.SelectedAddressStatus != null)
                {

                    switch (singleClaimModel.InsuredDriverViewModel.Address.SelectedAddressStatus)
                    {
                        case 1:
                            insuredDriverAddress.AddressLinkType = AddressLinkType.PreviousAddress;
                            break;
                        case 2:
                            insuredDriverAddress.AddressLinkType = AddressLinkType.LinkedAddress;
                            break;
                        case 3:
                            insuredDriverAddress.AddressLinkType = AddressLinkType.CurrentAddress;
                            break;
                        case 4:
                            insuredDriverAddress.AddressLinkType = AddressLinkType.RegisteredOffice;
                            break;
                        case 5:
                            insuredDriverAddress.AddressLinkType = AddressLinkType.TradingAddress;
                            break;
                        case 6:
                            insuredDriverAddress.AddressLinkType = AddressLinkType.StorageAddress;
                            break;
                        default:
                            insuredDriverAddress.AddressLinkType = AddressLinkType.Unknown;
                            break;
                    }

                }
                else
                {
                    insuredDriverAddress.AddressLinkType = AddressLinkType.Unknown;
                }

                if (singleClaimModel.InsuredDriverViewModel.Address.SameAddressAsPolicyHolder == true)
                {

                    if (singleClaimModel.PolicyHolderPersonalViewModel.Address.SubBuilding != null)
                    {
                        insuredDriverAddress.SubBuilding = singleClaimModel.PolicyHolderPersonalViewModel.Address.SubBuilding;
                    }

                    if (singleClaimModel.PolicyHolderPersonalViewModel.Address.BuildingName != null)
                    {
                        insuredDriverAddress.Building = singleClaimModel.PolicyHolderPersonalViewModel.Address.BuildingName;
                    }

                    if (singleClaimModel.PolicyHolderPersonalViewModel.Address.BuildingNumber != null)
                    {
                        insuredDriverAddress.BuildingNumber = singleClaimModel.PolicyHolderPersonalViewModel.Address.BuildingNumber;
                    }

                    if (singleClaimModel.PolicyHolderPersonalViewModel.Address.Street != null)
                    {
                        insuredDriverAddress.Street = singleClaimModel.PolicyHolderPersonalViewModel.Address.Street;
                    }

                    if (singleClaimModel.PolicyHolderPersonalViewModel.Address.Locality != null)
                    {
                        insuredDriverAddress.Locality = singleClaimModel.PolicyHolderPersonalViewModel.Address.Locality;
                    }

                    if (singleClaimModel.PolicyHolderPersonalViewModel.Address.Town != null)
                    {
                        insuredDriverAddress.Town = singleClaimModel.PolicyHolderPersonalViewModel.Address.Town;
                    }

                    if (singleClaimModel.PolicyHolderPersonalViewModel.Address.County != null)
                    {
                        insuredDriverAddress.County = singleClaimModel.PolicyHolderPersonalViewModel.Address.County;
                    }

                    if (singleClaimModel.PolicyHolderPersonalViewModel.Address.Postcode != null)
                    {
                        insuredDriverAddress.PostCode = singleClaimModel.PolicyHolderPersonalViewModel.Address.Postcode;
                    }
                }
                else
                {
                    if (singleClaimModel.InsuredDriverViewModel.Address.SubBuilding != null)
                    {
                        insuredDriverAddress.SubBuilding = singleClaimModel.InsuredDriverViewModel.Address.SubBuilding;
                    }

                    if (singleClaimModel.InsuredDriverViewModel.Address.BuildingName != null)
                    {
                        insuredDriverAddress.Building = singleClaimModel.InsuredDriverViewModel.Address.BuildingName;
                    }

                    if (singleClaimModel.InsuredDriverViewModel.Address.BuildingNumber != null)
                    {
                        insuredDriverAddress.BuildingNumber = singleClaimModel.InsuredDriverViewModel.Address.BuildingNumber;
                    }

                    if (singleClaimModel.InsuredDriverViewModel.Address.Street != null)
                    {
                        insuredDriverAddress.Street = singleClaimModel.InsuredDriverViewModel.Address.Street;
                    }

                    if (singleClaimModel.InsuredDriverViewModel.Address.Locality != null)
                    {
                        insuredDriverAddress.Locality = singleClaimModel.InsuredDriverViewModel.Address.Locality;
                    }

                    if (singleClaimModel.InsuredDriverViewModel.Address.Town != null)
                    {
                        insuredDriverAddress.Town = singleClaimModel.InsuredDriverViewModel.Address.Town;
                    }

                    if (singleClaimModel.InsuredDriverViewModel.Address.County != null)
                    {
                        insuredDriverAddress.County = singleClaimModel.InsuredDriverViewModel.Address.County;
                    }

                    if (singleClaimModel.InsuredDriverViewModel.Address.Postcode != null)
                    {
                        insuredDriverAddress.PostCode = singleClaimModel.InsuredDriverViewModel.Address.Postcode;
                    }
                }

                if (singleClaimModel.InsuredDriverViewModel.Address.StartOfResidency != null)
                {
                    insuredDriverAddress.StartOfResidency = singleClaimModel.InsuredDriverViewModel.Address.StartOfResidency;
                }

                if (singleClaimModel.InsuredDriverViewModel.Address.EndOfResidency != null)
                {
                    insuredDriverAddress.EndOfResidency = singleClaimModel.InsuredDriverViewModel.Address.EndOfResidency;
                }

                insuredDriver.Addresses.Add(insuredDriverAddress);


            }

            #endregion

            #region Additional Information

            if (singleClaimModel.InsuredDriverViewModel.AdditionalInfo.HomeTelephone != null)
            {
                insuredDriver.LandlineTelephone = singleClaimModel.InsuredDriverViewModel.AdditionalInfo.HomeTelephone;
            }

            if (singleClaimModel.InsuredDriverViewModel.AdditionalInfo.WorkTelephone != null)
            {
                insuredDriver.WorkTelephone = singleClaimModel.InsuredDriverViewModel.AdditionalInfo.WorkTelephone;
            }

            if (singleClaimModel.InsuredDriverViewModel.AdditionalInfo.MobileNumber != null)
            {
                insuredDriver.MobileTelephone = singleClaimModel.InsuredDriverViewModel.AdditionalInfo.MobileNumber;
            }

            if (singleClaimModel.InsuredDriverViewModel.AdditionalInfo.EmailAddress != null)
            {
                insuredDriver.EmailAddress = singleClaimModel.InsuredDriverViewModel.AdditionalInfo.EmailAddress;
            }

            if (singleClaimModel.InsuredDriverViewModel.AdditionalInfo.Occupation != null)
            {
                insuredDriver.Occupation = singleClaimModel.InsuredDriverViewModel.AdditionalInfo.Occupation;
            }

            if (singleClaimModel.InsuredDriverViewModel.AdditionalInfo.SelectedNationality != null)
            {
                insuredDriver.Nationality = singleClaimModel.InsuredDriverViewModel.AdditionalInfo.SelectedNationality;
            }

            if (singleClaimModel.InsuredDriverViewModel.AdditionalInfo.PassportNumber != null)
            {
                insuredDriver.PassportNumber = singleClaimModel.InsuredDriverViewModel.AdditionalInfo.PassportNumber;
            }

            if (singleClaimModel.InsuredDriverViewModel.AdditionalInfo.NINumber != null)
            {
                insuredDriver.NINumber = singleClaimModel.InsuredDriverViewModel.AdditionalInfo.NINumber;
            }

            if (singleClaimModel.InsuredDriverViewModel.AdditionalInfo.DrivingLicenseNumber != null)
            {
                insuredDriver.DrivingLicenseNumber = singleClaimModel.InsuredDriverViewModel.AdditionalInfo.DrivingLicenseNumber;
            }

            #endregion

            #region Supporting Involvements

            List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

            insuredDriverSupportingInvolvements = (List<SupportingInvolvementsViewModel>)HttpContext.Current.Session["InsuredDriverSupportingInvolvements"];

            if (insuredDriverSupportingInvolvements != null)
            {

                foreach (var s in insuredDriverSupportingInvolvements)
                {

                    MDA.Common.FileModel.Organisation supportingInvolvement = new MDA.Common.FileModel.Organisation();

                    if (s.OrganisationInvolvement.OrganisationName != null)
                    {
                        supportingInvolvement.OrganisationName = s.OrganisationInvolvement.OrganisationName;
                    }

                    if (s.OrganisationInvolvement.RegisteredNumber != null)
                    {
                        supportingInvolvement.RegisteredNumber = s.OrganisationInvolvement.RegisteredNumber;
                    }

                    if (s.OrganisationInvolvement.SelectedOrganisationInvolvement != null)
                    {
                        supportingInvolvement.OrganisationType = (OrganisationType)s.OrganisationInvolvement.SelectedOrganisationInvolvement;


                        switch (s.OrganisationInvolvement.SelectedOrganisationInvolvement)
                        {
                            case 0:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                break;
                            case 1:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                break;
                            case 2:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.UndefinedSupplier;
                                break;
                            case 3:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.AccidentManagement;
                                break;
                            case 4:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Hire;
                                break;
                            case 5:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Solicitor;
                                break;
                            case 6:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Engineer;
                                break;
                            case 7:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Recovery;
                                break;
                            case 8:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Storage;
                                break;
                            case 9:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Broker;
                                break;
                            case 10:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.MedicalExaminer;
                                break;
                            case 11:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Repairer;
                                break;
                            case 12:
                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                break;
                        }



                    }

                    #region Address

                    if (s.Address != null)
                    {

                        MDA.Common.FileModel.Address address = new MDA.Common.FileModel.Address();

                        if (s.Address.SelectedAddressStatus != null)
                        {

                            switch (s.Address.SelectedAddressStatus)
                            {
                                case 1:
                                    address.AddressLinkType = AddressLinkType.PreviousAddress;
                                    break;
                                case 2:
                                    address.AddressLinkType = AddressLinkType.LinkedAddress;
                                    break;
                                case 3:
                                    address.AddressLinkType = AddressLinkType.CurrentAddress;
                                    break;
                                case 4:
                                    address.AddressLinkType = AddressLinkType.RegisteredOffice;
                                    break;
                                case 5:
                                    address.AddressLinkType = AddressLinkType.TradingAddress;
                                    break;
                                case 6:
                                    address.AddressLinkType = AddressLinkType.StorageAddress;
                                    break;
                                default:
                                    address.AddressLinkType = AddressLinkType.Unknown;
                                    break;
                            }

                        }
                        else
                        {
                            address.AddressLinkType = AddressLinkType.Unknown;
                        }

                        if (s.Address.SubBuilding != null)
                        {
                            address.SubBuilding = s.Address.SubBuilding;
                        }

                        if (s.Address.BuildingName != null)
                        {
                            address.Building = s.Address.BuildingName;
                        }

                        if (s.Address.BuildingNumber != null)
                        {
                            address.BuildingNumber = s.Address.BuildingNumber;
                        }

                        if (s.Address.Street != null)
                        {
                            address.Street = s.Address.Street;
                        }

                        if (s.Address.Locality != null)
                        {
                            address.Locality = s.Address.Locality;
                        }

                        if (s.Address.Town != null)
                        {
                            address.Town = s.Address.Town;
                        }

                        if (s.Address.County != null)
                        {
                            address.County = s.Address.County;
                        }

                        if (s.Address.Postcode != null)
                        {
                            address.PostCode = s.Address.Postcode;
                        }


                        if (s.Address.StartOfResidency != null)
                        {
                            address.StartOfResidency = s.Address.StartOfResidency;
                        }

                        if (s.Address.EndOfResidency != null)
                        {
                            address.EndOfResidency = s.Address.EndOfResidency;
                        }


                        supportingInvolvement.Addresses.Add(address);

                    }

                    #endregion

                    #region Hire Vehicle

                    if (s.OrganisationInvolvement.SelectedOrganisationInvolvement == 3 || s.OrganisationInvolvement.SelectedOrganisationInvolvement == 4)
                    {
                        if (s.Vehicle.RegistrationNumber != null || s.Vehicle.Make != null || s.Vehicle.Model != null || s.Vehicle.HireStartDate != null || s.Vehicle.HireEndDate != null)
                        {
                            MDA.Common.FileModel.OrganisationVehicle hireVehicle = new MDA.Common.FileModel.OrganisationVehicle();

                            if (s.Vehicle.SelectedVehicletype != null)
                            {
                                hireVehicle.VehicleType = (VehicleType)s.Vehicle.SelectedVehicletype;
                            }

                            if (s.Vehicle.RegistrationNumber != null)
                            {
                                hireVehicle.VehicleRegistration = s.Vehicle.RegistrationNumber;
                            }

                            if (s.Vehicle.Make != null)
                            {
                                hireVehicle.Make = s.Vehicle.Make;
                            }

                            if (s.Vehicle.Model != null)
                            {
                                hireVehicle.Model = s.Vehicle.Model;
                            }

                            if (s.Vehicle.HireStartDate != null)
                            {
                                hireVehicle.HireStartDate = s.Vehicle.HireStartDate;
                            }

                            if (s.Vehicle.HireEndDate != null)
                            {
                                hireVehicle.HireEndDate = s.Vehicle.HireEndDate;
                            }

                            #region Hire Vehicle Additional Information

                            if (s.VehicleAdditionalInfo.VIN != null)
                            {
                                hireVehicle.VIN = s.VehicleAdditionalInfo.VIN;
                            }

                            if (s.VehicleAdditionalInfo.SelectedFuel != null)
                            {
                                hireVehicle.Fuel = (VehicleFuelType)s.VehicleAdditionalInfo.SelectedFuel;
                            }

                            if (s.VehicleAdditionalInfo.SelectedColour != null)
                            {
                                hireVehicle.Colour = (VehicleColour)s.VehicleAdditionalInfo.SelectedColour;
                            }

                            if (s.VehicleAdditionalInfo.EngineCapacity != null)
                            {
                                hireVehicle.EngineCapacity = s.VehicleAdditionalInfo.EngineCapacity;
                            }

                            if (s.VehicleAdditionalInfo.SelectedTransmission != null)
                            {
                                hireVehicle.Transmission = (VehicleTransmission)s.VehicleAdditionalInfo.SelectedTransmission;
                            }

                            #endregion

                            hireVehicle.Incident2VehicleLinkType = Incident2VehicleLinkType.InsuredHireVehicle;

                            supportingInvolvement.Vehicles.Add(hireVehicle);

                        }
                    }

                    #endregion

                    #region Additional Information

                    if (s.AdditionalInfo.Telephone1 != null)
                    {
                        supportingInvolvement.Telephone1 = s.AdditionalInfo.Telephone1;
                    }

                    if (s.AdditionalInfo.Telephone2 != null)
                    {
                        supportingInvolvement.Telephone2 = s.AdditionalInfo.Telephone2;
                    }

                    if (s.AdditionalInfo.Telephone3 != null)
                    {
                        supportingInvolvement.Telephone3 = s.AdditionalInfo.Telephone3;
                    }

                    if (s.AdditionalInfo.EmailAddress != null)
                    {
                        supportingInvolvement.Email = s.AdditionalInfo.EmailAddress;
                    }

                    if (s.AdditionalInfo.WebsiteAddress != null)
                    {
                        supportingInvolvement.WebSite = s.AdditionalInfo.WebsiteAddress;
                    }

                    if (s.AdditionalInfo.VATNumber != null)
                    {
                        supportingInvolvement.VatNumber = s.AdditionalInfo.VATNumber;
                    }

                    if (s.AdditionalInfo.MoJClaimsRegNumber != null)
                    {
                        supportingInvolvement.MojCrmNumber = s.AdditionalInfo.MoJClaimsRegNumber;
                    }

                    #endregion

                    if (singleClaimModel.InsuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver == true)
                    {
                        policyHolder.Organisations.Add(supportingInvolvement);
                    }
                    else
                    {
                        insuredDriver.Organisations.Add(supportingInvolvement);
                    }
                }
            }


            #endregion


            #endregion

            #region Insured Passenger

            List<PassengerViewModel> insuredDriverPassengers = new List<PassengerViewModel>();

            insuredDriverPassengers = (List<PassengerViewModel>)System.Web.HttpContext.Current.Session["InsuredDriverPassengers"];

            if (insuredDriverPassengers != null)
            {

                int passengerId = 1;

                foreach (var p in insuredDriverPassengers)
                {

                    if (passengerId <= singleClaimModel.InsuredVehicleViewModel.Vehicle.NumberOfPassengers)
                    {

                        MDA.Common.FileModel.Person passenger = new MDA.Common.FileModel.Person();

                        if (p.PersonInformation.SelectedSalutation != null)
                        {
                            passenger.Salutation = (Salutation)p.PersonInformation.SelectedSalutation;
                        }

                        if (p.PersonInformation.Firstname != null)
                        {
                            passenger.FirstName = p.PersonInformation.Firstname;
                        }

                        if (p.PersonInformation.Middlename != null)
                        {
                            passenger.MiddleName = p.PersonInformation.Middlename;
                        }

                        if (p.PersonInformation.Surname != null)
                        {
                            passenger.LastName = p.PersonInformation.Surname;
                        }

                        if (p.PersonInformation.DateOfBirth != null)
                        {
                            passenger.DateOfBirth = p.PersonInformation.DateOfBirth;
                        }

                        if (p.PersonInformation.SelectedGender != null)
                        {
                            passenger.Gender = (Gender)p.PersonInformation.SelectedGender;
                        }

                        #region Address

                        if (p.Address != null)
                        {

                            MDA.Common.FileModel.Address address = new MDA.Common.FileModel.Address();

                            if (p.Address.SelectedAddressStatus != null)
                            {

                                switch (p.Address.SelectedAddressStatus)
                                {
                                    case 1:
                                        address.AddressLinkType = AddressLinkType.PreviousAddress;
                                        break;
                                    case 2:
                                        address.AddressLinkType = AddressLinkType.LinkedAddress;
                                        break;
                                    case 3:
                                        address.AddressLinkType = AddressLinkType.CurrentAddress;
                                        break;
                                    case 4:
                                        address.AddressLinkType = AddressLinkType.RegisteredOffice;
                                        break;
                                    case 5:
                                        address.AddressLinkType = AddressLinkType.TradingAddress;
                                        break;
                                    case 6:
                                        address.AddressLinkType = AddressLinkType.StorageAddress;
                                        break;
                                    default:
                                        address.AddressLinkType = AddressLinkType.Unknown;
                                        break;
                                }

                            }
                            else
                            {
                                address.AddressLinkType = AddressLinkType.Unknown;
                            }

                            if (p.Address.SubBuilding != null)
                            {
                                address.SubBuilding = p.Address.SubBuilding;
                            }

                            if (p.Address.BuildingName != null)
                            {
                                address.Building = p.Address.BuildingName;
                            }

                            if (p.Address.BuildingNumber != null)
                            {
                                address.BuildingNumber = p.Address.BuildingNumber;
                            }

                            if (p.Address.Street != null)
                            {
                                address.Street = p.Address.Street;
                            }

                            if (p.Address.Locality != null)
                            {
                                address.Locality = p.Address.Locality;
                            }

                            if (p.Address.Town != null)
                            {
                                address.Town = p.Address.Town;
                            }

                            if (p.Address.County != null)
                            {
                                address.County = p.Address.County;
                            }

                            if (p.Address.Postcode != null)
                            {
                                address.PostCode = p.Address.Postcode;
                            }

                            if (p.Address.StartOfResidency != null)
                            {
                                address.StartOfResidency = p.Address.StartOfResidency;
                            }

                            if (p.Address.EndOfResidency != null)
                            {
                                address.EndOfResidency = p.Address.EndOfResidency;
                            }

                            passenger.Addresses.Add(address);

                        }

                        #endregion

                        #region Additional Information

                        if (p.AdditionalInfo.HomeTelephone != null)
                        {
                            passenger.LandlineTelephone = p.AdditionalInfo.HomeTelephone;
                        }

                        if (p.AdditionalInfo.WorkTelephone != null)
                        {
                            passenger.WorkTelephone = p.AdditionalInfo.WorkTelephone;
                        }

                        if (p.AdditionalInfo.MobileNumber != null)
                        {
                            passenger.MobileTelephone = p.AdditionalInfo.MobileNumber;
                        }

                        if (p.AdditionalInfo.EmailAddress != null)
                        {
                            passenger.EmailAddress = p.AdditionalInfo.EmailAddress;
                        }

                        if (p.AdditionalInfo.Occupation != null)
                        {
                            passenger.Occupation = p.AdditionalInfo.Occupation;
                        }

                        if (p.AdditionalInfo.SelectedNationality != null)
                        {
                            passenger.Nationality = p.AdditionalInfo.SelectedNationality;
                        }

                        if (p.AdditionalInfo.PassportNumber != null)
                        {
                            passenger.PassportNumber = p.AdditionalInfo.PassportNumber;
                        }

                        if (p.AdditionalInfo.NINumber != null)
                        {
                            passenger.NINumber = p.AdditionalInfo.NINumber;
                        }

                        if (p.AdditionalInfo.DrivingLicenseNumber != null)
                        {
                            passenger.DrivingLicenseNumber = p.AdditionalInfo.DrivingLicenseNumber;
                        }

                        #endregion

                        #region Supporting Involvements


                        if (HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] != null)
                        {


                            List<SupportingInvolvementsViewModel> passengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                            Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                            DictionaryPassengerSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"];

                            if (DictionaryPassengerSupportingInvolvements != null)
                            {
                                if (DictionaryPassengerSupportingInvolvements.ContainsKey(passengerId))
                                {

                                    passengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements[passengerId];

                                    foreach (var s in passengerSupportingInvolvements)
                                    {

                                        MDA.Common.FileModel.Organisation supportingInvolvement = new MDA.Common.FileModel.Organisation();

                                        if (s.OrganisationInvolvement.OrganisationName != null)
                                        {
                                            supportingInvolvement.OrganisationName = s.OrganisationInvolvement.OrganisationName;
                                        }

                                        if (s.OrganisationInvolvement.RegisteredNumber != null)
                                        {
                                            supportingInvolvement.RegisteredNumber = s.OrganisationInvolvement.RegisteredNumber;
                                        }

                                        if (s.OrganisationInvolvement.SelectedOrganisationInvolvement != null)
                                        {
                                            supportingInvolvement.OrganisationType = (OrganisationType)s.OrganisationInvolvement.SelectedOrganisationInvolvement;

                                            switch (s.OrganisationInvolvement.SelectedOrganisationInvolvement)
                                            {
                                                case 0:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                                    break;
                                                case 1:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                                    break;
                                                case 2:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.UndefinedSupplier;
                                                    break;
                                                case 3:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.AccidentManagement;
                                                    break;
                                                case 4:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Hire;
                                                    break;
                                                case 5:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Solicitor;
                                                    break;
                                                case 6:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Engineer;
                                                    break;
                                                case 7:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Recovery;
                                                    break;
                                                case 8:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Storage;
                                                    break;
                                                case 9:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Broker;
                                                    break;
                                                case 10:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.MedicalExaminer;
                                                    break;
                                                case 11:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Repairer;
                                                    break;
                                                case 12:
                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                                    break;
                                            }
                                        }

                                        #region Address

                                        if (s.Address != null)
                                        {

                                            MDA.Common.FileModel.Address address = new MDA.Common.FileModel.Address();

                                            if (s.Address.SelectedAddressStatus != null)
                                            {

                                                switch (s.Address.SelectedAddressStatus)
                                                {
                                                    case 1:
                                                        address.AddressLinkType = AddressLinkType.PreviousAddress;
                                                        break;
                                                    case 2:
                                                        address.AddressLinkType = AddressLinkType.LinkedAddress;
                                                        break;
                                                    case 3:
                                                        address.AddressLinkType = AddressLinkType.CurrentAddress;
                                                        break;
                                                    case 4:
                                                        address.AddressLinkType = AddressLinkType.RegisteredOffice;
                                                        break;
                                                    case 5:
                                                        address.AddressLinkType = AddressLinkType.TradingAddress;
                                                        break;
                                                    case 6:
                                                        address.AddressLinkType = AddressLinkType.StorageAddress;
                                                        break;
                                                    default:
                                                        address.AddressLinkType = AddressLinkType.Unknown;
                                                        break;
                                                }

                                            }
                                            else
                                            {
                                                address.AddressLinkType = AddressLinkType.Unknown;
                                            }

                                            if (s.Address.SubBuilding != null)
                                            {
                                                address.SubBuilding = s.Address.SubBuilding;
                                            }

                                            if (s.Address.BuildingName != null)
                                            {
                                                address.Building = s.Address.BuildingName;
                                            }

                                            if (s.Address.BuildingNumber != null)
                                            {
                                                address.BuildingNumber = s.Address.BuildingNumber;
                                            }

                                            if (s.Address.Street != null)
                                            {
                                                address.Street = s.Address.Street;
                                            }

                                            if (s.Address.Locality != null)
                                            {
                                                address.Locality = s.Address.Locality;
                                            }

                                            if (s.Address.Town != null)
                                            {
                                                address.Town = s.Address.Town;
                                            }

                                            if (s.Address.County != null)
                                            {
                                                address.County = s.Address.County;
                                            }

                                            if (s.Address.Postcode != null)
                                            {
                                                address.PostCode = s.Address.Postcode;
                                            }

                                            if (s.Address.StartOfResidency != null)
                                            {
                                                address.StartOfResidency = s.Address.StartOfResidency;
                                            }

                                            if (s.Address.EndOfResidency != null)
                                            {
                                                address.EndOfResidency = s.Address.EndOfResidency;
                                            }


                                            supportingInvolvement.Addresses.Add(address);

                                        }

                                        #endregion

                                        #region Hire Vehicle

                                        if (s.OrganisationInvolvement.SelectedOrganisationInvolvement == 3 || s.OrganisationInvolvement.SelectedOrganisationInvolvement == 4)
                                        {
                                            if (s.Vehicle.RegistrationNumber != null || s.Vehicle.Make != null || s.Vehicle.Model != null || s.Vehicle.HireStartDate != null || s.Vehicle.HireEndDate != null)
                                            {
                                                MDA.Common.FileModel.OrganisationVehicle hireVehicle = new MDA.Common.FileModel.OrganisationVehicle();

                                                if (s.Vehicle.SelectedVehicletype != null)
                                                {
                                                    hireVehicle.VehicleType = (VehicleType)s.Vehicle.SelectedVehicletype;
                                                }

                                                if (s.Vehicle.RegistrationNumber != null)
                                                {
                                                    hireVehicle.VehicleRegistration = s.Vehicle.RegistrationNumber;
                                                }

                                                if (s.Vehicle.Make != null)
                                                {
                                                    hireVehicle.Make = s.Vehicle.Make;
                                                }

                                                if (s.Vehicle.Model != null)
                                                {
                                                    hireVehicle.Model = s.Vehicle.Model;
                                                }

                                                if (s.Vehicle.HireStartDate != null)
                                                {
                                                    hireVehicle.HireStartDate = s.Vehicle.HireStartDate;
                                                }

                                                if (s.Vehicle.HireEndDate != null)
                                                {
                                                    hireVehicle.HireEndDate = s.Vehicle.HireEndDate;
                                                }

                                                #region Hire Vehicle Additional Information

                                                if (s.VehicleAdditionalInfo.VIN != null)
                                                {
                                                    hireVehicle.VIN = s.VehicleAdditionalInfo.VIN;
                                                }

                                                if (s.VehicleAdditionalInfo.SelectedFuel != null)
                                                {
                                                    hireVehicle.Fuel = (VehicleFuelType)s.VehicleAdditionalInfo.SelectedFuel;
                                                }

                                                if (s.VehicleAdditionalInfo.SelectedColour != null)
                                                {
                                                    hireVehicle.Colour = (VehicleColour)s.VehicleAdditionalInfo.SelectedColour;
                                                }

                                                if (s.VehicleAdditionalInfo.EngineCapacity != null)
                                                {
                                                    hireVehicle.EngineCapacity = s.VehicleAdditionalInfo.EngineCapacity;
                                                }

                                                if (s.VehicleAdditionalInfo.SelectedTransmission != null)
                                                {
                                                    hireVehicle.Transmission = (VehicleTransmission)s.VehicleAdditionalInfo.SelectedTransmission;
                                                }

                                                #endregion

                                                hireVehicle.Incident2VehicleLinkType = Incident2VehicleLinkType.InsuredHireVehicle;

                                                supportingInvolvement.Vehicles.Add(hireVehicle);

                                            }
                                        }

                                        #endregion


                                        #region Additional Information

                                        if (s.AdditionalInfo.EmailAddress != null)
                                        {
                                            supportingInvolvement.Email = s.AdditionalInfo.EmailAddress;
                                        }

                                        if (s.AdditionalInfo.MoJClaimsRegNumber != null)
                                        {
                                            supportingInvolvement.MojCrmNumber = s.AdditionalInfo.MoJClaimsRegNumber;
                                        }

                                        if (s.AdditionalInfo.Telephone1 != null)
                                        {
                                            supportingInvolvement.Telephone1 = s.AdditionalInfo.Telephone1;
                                        }

                                        if (s.AdditionalInfo.Telephone2 != null)
                                        {
                                            supportingInvolvement.Telephone2 = s.AdditionalInfo.Telephone2;
                                        }

                                        if (s.AdditionalInfo.Telephone3 != null)
                                        {
                                            supportingInvolvement.Telephone3 = s.AdditionalInfo.Telephone3;
                                        }

                                        if (s.AdditionalInfo.VATNumber != null)
                                        {
                                            supportingInvolvement.VatNumber = s.AdditionalInfo.VATNumber;
                                        }

                                        if (s.AdditionalInfo.WebsiteAddress != null)
                                        {
                                            supportingInvolvement.WebSite = s.AdditionalInfo.WebsiteAddress;
                                        }

                                        #endregion

                                        passenger.Organisations.Add(supportingInvolvement);

                                    }
                                }
                            }

                        }

                        #endregion

                        if (passenger != null)
                        {
                            passenger.PartyType = PartyType.Insured;
                            passenger.SubPartyType = SubPartyType.Passenger;
                        }

                        passengerId++;

                        insuredVehicle.People.Add(passenger);

                    }

                }

            }

            #endregion

            claim.Vehicles.Add(insuredVehicle);

            #region Third Party Vehicle

            Dictionary<int, ThirdPartyVehicleViewModel> thirdPartyVehicles = new Dictionary<int, ThirdPartyVehicleViewModel>();

            thirdPartyVehicles = (Dictionary<int, ThirdPartyVehicleViewModel>)HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"];

            if (thirdPartyVehicles != null)
            {
                int vehicleCount = 1;

                foreach (var tpv in thirdPartyVehicles)
                {

                    MDA.Common.FileModel.Vehicle thirdPartyVehicle = new MDA.Common.FileModel.Vehicle();

                    thirdPartyVehicle.DamageDescription = tpv.Value.Vehicle.WasPolicyHolderTheDriver.ToString();
                    thirdPartyVehicle.DamageDescription += "|" + tpv.Value.Vehicle.NumberOfPassengers.ToString();
                    thirdPartyVehicle.DamageDescription += "|" + tpv.Value.CurrentVehicle.ToString();

                    if (tpv.Value.Vehicle.SelectedVehicletype != null)
                    {
                        thirdPartyVehicle.VehicleType = (MDA.Common.Enum.VehicleType)tpv.Value.Vehicle.SelectedVehicletype;
                    }

                    if (tpv.Value.Vehicle.RegistrationNumber != null)
                    {
                        thirdPartyVehicle.VehicleRegistration = tpv.Value.Vehicle.RegistrationNumber;
                    }

                    if (tpv.Value.Vehicle.Make != null)
                    {
                        thirdPartyVehicle.Make = tpv.Value.Vehicle.Make;
                    }

                    if (tpv.Value.Vehicle.Model != null)
                    {
                        thirdPartyVehicle.Model = tpv.Value.Vehicle.Model;
                    }

                    if (tpv.Value.VehicleAdditionalInfo.EngineCapacity != null)
                    {
                        thirdPartyVehicle.EngineCapacity = tpv.Value.VehicleAdditionalInfo.EngineCapacity;
                    }

                    if (tpv.Value.VehicleAdditionalInfo.SelectedFuel != null)
                    {
                        thirdPartyVehicle.Fuel = (MDA.Common.Enum.VehicleFuelType)tpv.Value.VehicleAdditionalInfo.SelectedFuel;
                    }

                    if (tpv.Value.VehicleAdditionalInfo.SelectedColour != null)
                    {
                        thirdPartyVehicle.Colour = (MDA.Common.Enum.VehicleColour)tpv.Value.VehicleAdditionalInfo.SelectedColour;
                    }

                    if (tpv.Value.VehicleAdditionalInfo.SelectedTransmission != null)
                    {
                        thirdPartyVehicle.Transmission = (MDA.Common.Enum.VehicleTransmission)tpv.Value.VehicleAdditionalInfo.SelectedTransmission;
                    }

                    if (tpv.Value.VehicleAdditionalInfo.VIN != null)
                    {
                        thirdPartyVehicle.VIN = tpv.Value.VehicleAdditionalInfo.VIN;
                    }

                    if (tpv.Value.VehicleAdditionalInfo.SelectedTotalLossCapacity != null)
                    {
                        thirdPartyVehicle.CategoryOfLoss = (VehicleCategoryOfLoss)tpv.Value.VehicleAdditionalInfo.SelectedTotalLossCapacity;
                    }

                    //if (thirdPartyVehicle.Make != null || thirdPartyVehicle.Model != null || thirdPartyVehicle.VehicleRegistration != null || thirdPartyVehicle.VIN != null)
                    //{
                        thirdPartyVehicle.Incident2VehicleLinkType = Incident2VehicleLinkType.ThirdPartyVehicle;
                    //}

                    #region Third Party Driver

                    Dictionary<int, ThirdPartyDriverViewModel> DictionaryThirdPartyDriverViewModels = new Dictionary<int, ThirdPartyDriverViewModel>();

                    DictionaryThirdPartyDriverViewModels = HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] as Dictionary<int, ThirdPartyDriverViewModel>;

                    if (DictionaryThirdPartyDriverViewModels != null)
                    {
                        if (DictionaryThirdPartyDriverViewModels.Count() > 0)
                        {

                            if (DictionaryThirdPartyDriverViewModels.ContainsKey(vehicleCount))
                            {
                                var tpd = DictionaryThirdPartyDriverViewModels[vehicleCount];

                                MDA.Common.FileModel.Person thirdPartyDriver = new MDA.Common.FileModel.Person();

                                thirdPartyDriver.PartyType = PartyType.ThirdParty;
                                thirdPartyDriver.SubPartyType = SubPartyType.Driver;


                                if (tpd.PersonInformation.SelectedSalutation != null)
                                    thirdPartyDriver.Salutation = (Salutation)tpd.PersonInformation.SelectedSalutation;

                                if (tpd.PersonInformation.Firstname != null)
                                    thirdPartyDriver.FirstName = tpd.PersonInformation.Firstname;


                                if (tpd.PersonInformation.Middlename != null)
                                    thirdPartyDriver.MiddleName = tpd.PersonInformation.Middlename;

                                if (tpd.PersonInformation.Surname != null)
                                    thirdPartyDriver.LastName = tpd.PersonInformation.Surname;

                                if (tpd.PersonInformation.DateOfBirth != null)
                                    thirdPartyDriver.DateOfBirth = tpd.PersonInformation.DateOfBirth;

                                if (tpd.PersonInformation.SelectedGender != null)
                                    thirdPartyDriver.Gender = (Gender)tpd.PersonInformation.SelectedGender;

                                #region Additional Information

                                if (tpd.AdditionalInfo.HomeTelephone != null)
                                    thirdPartyDriver.LandlineTelephone = tpd.AdditionalInfo.HomeTelephone;

                                if (tpd.AdditionalInfo.WorkTelephone != null)
                                    thirdPartyDriver.WorkTelephone = tpd.AdditionalInfo.WorkTelephone;

                                if (tpd.AdditionalInfo.MobileNumber != null)
                                    thirdPartyDriver.MobileTelephone = tpd.AdditionalInfo.MobileNumber;

                                if (tpd.AdditionalInfo.EmailAddress != null)
                                    thirdPartyDriver.EmailAddress = tpd.AdditionalInfo.EmailAddress;

                                if (tpd.AdditionalInfo.Occupation != null)
                                    thirdPartyDriver.Occupation = tpd.AdditionalInfo.Occupation;


                                if (tpd.AdditionalInfo.SelectedNationality != null)
                                    thirdPartyDriver.Nationality = tpd.AdditionalInfo.SelectedNationality;

                                if (tpd.AdditionalInfo.PassportNumber != null)
                                    thirdPartyDriver.PassportNumber = tpd.AdditionalInfo.PassportNumber;

                                if (tpd.AdditionalInfo.NINumber != null)
                                    thirdPartyDriver.NINumber = tpd.AdditionalInfo.NINumber;

                                if (tpd.AdditionalInfo.DrivingLicenseNumber != null)
                                    thirdPartyDriver.DrivingLicenseNumber = tpd.AdditionalInfo.DrivingLicenseNumber;

                                #endregion


                                #region Address

                                if (tpd.Address != null)
                                {

                                    MDA.Common.FileModel.Address thirdPartyDriverAddress = new MDA.Common.FileModel.Address();

                                    if (tpd.Address.SelectedAddressStatus != null)
                                    {

                                        switch (tpd.Address.SelectedAddressStatus)
                                        {
                                            case 1:
                                                thirdPartyDriverAddress.AddressLinkType = AddressLinkType.PreviousAddress;
                                                break;
                                            case 2:
                                                thirdPartyDriverAddress.AddressLinkType = AddressLinkType.LinkedAddress;
                                                break;
                                            case 3:
                                                thirdPartyDriverAddress.AddressLinkType = AddressLinkType.CurrentAddress;
                                                break;
                                            case 4:
                                                thirdPartyDriverAddress.AddressLinkType = AddressLinkType.RegisteredOffice;
                                                break;
                                            case 5:
                                                thirdPartyDriverAddress.AddressLinkType = AddressLinkType.TradingAddress;
                                                break;
                                            case 6:
                                                thirdPartyDriverAddress.AddressLinkType = AddressLinkType.StorageAddress;
                                                break;
                                            default:
                                                thirdPartyDriverAddress.AddressLinkType = AddressLinkType.Unknown;
                                                break;
                                        }

                                    }
                                    else
                                    {
                                        thirdPartyDriverAddress.AddressLinkType = AddressLinkType.Unknown;
                                    }

                                    if (tpd.Address.SubBuilding != null)
                                    {
                                        thirdPartyDriverAddress.SubBuilding = tpd.Address.SubBuilding;
                                    }

                                    if (tpd.Address.BuildingName != null)
                                    {
                                        thirdPartyDriverAddress.Building = tpd.Address.BuildingName;
                                    }

                                    if (tpd.Address.BuildingNumber != null)
                                    {
                                        thirdPartyDriverAddress.BuildingNumber = tpd.Address.BuildingNumber;
                                    }

                                    if (tpd.Address.Street != null)
                                    {
                                        thirdPartyDriverAddress.Street = tpd.Address.Street;
                                    }

                                    if (tpd.Address.Locality != null)
                                    {
                                        thirdPartyDriverAddress.Locality = tpd.Address.Locality;
                                    }

                                    if (tpd.Address.Town != null)
                                    {
                                        thirdPartyDriverAddress.Town = tpd.Address.Town;
                                    }

                                    if (tpd.Address.County != null)
                                    {
                                        thirdPartyDriverAddress.County = tpd.Address.County;
                                    }

                                    if (tpd.Address.Postcode != null)
                                    {
                                        thirdPartyDriverAddress.PostCode = tpd.Address.Postcode;
                                    }

                                    if (tpd.Address.StartOfResidency != null)
                                    {
                                        thirdPartyDriverAddress.StartOfResidency = tpd.Address.StartOfResidency;
                                    }

                                    if (tpd.Address.EndOfResidency != null)
                                    {
                                        thirdPartyDriverAddress.EndOfResidency = tpd.Address.EndOfResidency;
                                    }


                                    thirdPartyDriver.Addresses.Add(thirdPartyDriverAddress);

                                }

                                #endregion


                                #region Supporting Involvements

                                if (HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] != null)
                                {
                                    List<SupportingInvolvementsViewModel> thirdPartyDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                                    Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyDriverSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                                    DictionaryThirdPartyDriverSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"];

                                    if (DictionaryThirdPartyDriverSupportingInvolvements != null)
                                    {

                                        if (DictionaryThirdPartyDriverSupportingInvolvements.Count() > 0)
                                        {

                                            if (DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(vehicleCount))
                                            {
                                                thirdPartyDriverSupportingInvolvements = DictionaryThirdPartyDriverSupportingInvolvements[vehicleCount];

                                                foreach (var s in thirdPartyDriverSupportingInvolvements)
                                                {
                                                    MDA.Common.FileModel.Organisation supportingInvolvement = new MDA.Common.FileModel.Organisation();

                                                    if (s.OrganisationInvolvement.OrganisationName != null)
                                                    {
                                                        supportingInvolvement.OrganisationName = s.OrganisationInvolvement.OrganisationName;
                                                    }

                                                    if (s.OrganisationInvolvement.RegisteredNumber != null)
                                                    {
                                                        supportingInvolvement.RegisteredNumber = s.OrganisationInvolvement.RegisteredNumber;
                                                    }

                                                    if (s.OrganisationInvolvement.SelectedOrganisationInvolvement != null)
                                                    {
                                                        supportingInvolvement.OrganisationType = (OrganisationType)s.OrganisationInvolvement.SelectedOrganisationInvolvement;

                                                        switch (s.OrganisationInvolvement.SelectedOrganisationInvolvement)
                                                        {
                                                            case 0:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                                                break;
                                                            case 1:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                                                break;
                                                            case 2:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.UndefinedSupplier;
                                                                break;
                                                            case 3:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.AccidentManagement;
                                                                break;
                                                            case 4:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Hire;
                                                                break;
                                                            case 5:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Solicitor;
                                                                break;
                                                            case 6:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Engineer;
                                                                break;
                                                            case 7:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Recovery;
                                                                break;
                                                            case 8:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Storage;
                                                                break;
                                                            case 9:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Broker;
                                                                break;
                                                            case 10:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.MedicalExaminer;
                                                                break;
                                                            case 11:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Repairer;
                                                                break;
                                                            case 12:
                                                                supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                                                break;
                                                        }
                                                    }

                                                    #region Address

                                                    if (s.Address != null)
                                                    {

                                                        MDA.Common.FileModel.Address address = new MDA.Common.FileModel.Address();

                                                        if (s.Address.SelectedAddressStatus != null)
                                                        {

                                                            switch (s.Address.SelectedAddressStatus)
                                                            {
                                                                case 1:
                                                                    address.AddressLinkType = AddressLinkType.PreviousAddress;
                                                                    break;
                                                                case 2:
                                                                    address.AddressLinkType = AddressLinkType.LinkedAddress;
                                                                    break;
                                                                case 3:
                                                                    address.AddressLinkType = AddressLinkType.CurrentAddress;
                                                                    break;
                                                                case 4:
                                                                    address.AddressLinkType = AddressLinkType.RegisteredOffice;
                                                                    break;
                                                                case 5:
                                                                    address.AddressLinkType = AddressLinkType.TradingAddress;
                                                                    break;
                                                                case 6:
                                                                    address.AddressLinkType = AddressLinkType.StorageAddress;
                                                                    break;
                                                                default:
                                                                    address.AddressLinkType = AddressLinkType.Unknown;
                                                                    break;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            address.AddressLinkType = AddressLinkType.Unknown;
                                                        }


                                                        if (s.Address.SubBuilding != null)
                                                        {
                                                            address.SubBuilding = s.Address.SubBuilding;
                                                        }

                                                        if (s.Address.BuildingName != null)
                                                        {
                                                            address.Building = s.Address.BuildingName;
                                                        }

                                                        if (s.Address.BuildingNumber != null)
                                                        {
                                                            address.BuildingNumber = s.Address.BuildingNumber;
                                                        }

                                                        if (s.Address.Street != null)
                                                        {
                                                            address.Street = s.Address.Street;
                                                        }

                                                        if (s.Address.Locality != null)
                                                        {
                                                            address.Locality = s.Address.Locality;
                                                        }

                                                        if (s.Address.Town != null)
                                                        {
                                                            address.Town = s.Address.Town;
                                                        }

                                                        if (s.Address.County != null)
                                                        {
                                                            address.County = s.Address.County;
                                                        }

                                                        if (s.Address.Postcode != null)
                                                        {
                                                            address.PostCode = s.Address.Postcode;
                                                        }

                                                        if (s.Address.StartOfResidency != null)
                                                        {
                                                            address.StartOfResidency = s.Address.StartOfResidency;
                                                        }

                                                        if (s.Address.EndOfResidency != null)
                                                        {
                                                            address.EndOfResidency = s.Address.EndOfResidency;
                                                        }


                                                        supportingInvolvement.Addresses.Add(address);
                                                    }

                                                    #endregion

                                                    #region Hire Vehicle

                                                    if (s.OrganisationInvolvement.SelectedOrganisationInvolvement == 3 || s.OrganisationInvolvement.SelectedOrganisationInvolvement == 4)
                                                    {

                                                        if (s.Vehicle.RegistrationNumber != null || s.Vehicle.Make != null || s.Vehicle.Model != null || s.Vehicle.HireStartDate != null || s.Vehicle.HireEndDate != null)
                                                        {
                                                            MDA.Common.FileModel.OrganisationVehicle hireVehicle =
                                                                new MDA.Common.FileModel.OrganisationVehicle();

                                                            hireVehicle.Incident2VehicleLinkType = Incident2VehicleLinkType.ThirdPartyHireVehicle;

                                                            if (s.Vehicle.SelectedVehicletype != null)
                                                            {
                                                                hireVehicle.VehicleType = (VehicleType)s.Vehicle.SelectedVehicletype;
                                                            }

                                                            if (s.Vehicle.RegistrationNumber != null)
                                                            {
                                                                hireVehicle.VehicleRegistration = s.Vehicle.RegistrationNumber;
                                                            }

                                                            if (s.Vehicle.Make != null)
                                                            {
                                                                hireVehicle.Make = s.Vehicle.Make;
                                                            }

                                                            if (s.Vehicle.Model != null)
                                                            {
                                                                hireVehicle.Model = s.Vehicle.Model;
                                                            }

                                                            if (s.Vehicle.HireStartDate != null)
                                                            {
                                                                hireVehicle.HireStartDate = s.Vehicle.HireStartDate;
                                                            }

                                                            if (s.Vehicle.HireEndDate != null)
                                                            {
                                                                hireVehicle.HireEndDate = s.Vehicle.HireEndDate;
                                                            }

                                                    #endregion

                                                    #region Hire Vehicle Additional Information

                                                    if (s.VehicleAdditionalInfo.VIN != null)
                                                    {
                                                        hireVehicle.VIN = s.VehicleAdditionalInfo.VIN;
                                                    }

                                                    if (s.VehicleAdditionalInfo.SelectedFuel != null)
                                                    {
                                                        hireVehicle.Fuel = (VehicleFuelType)s.VehicleAdditionalInfo.SelectedFuel;
                                                    }

                                                    if (s.VehicleAdditionalInfo.SelectedColour != null)
                                                    {
                                                        hireVehicle.Colour = (VehicleColour)s.VehicleAdditionalInfo.SelectedColour;
                                                    }

                                                    if (s.VehicleAdditionalInfo.EngineCapacity != null)
                                                    {
                                                        hireVehicle.EngineCapacity = s.VehicleAdditionalInfo.EngineCapacity;
                                                    }

                                                    if (s.VehicleAdditionalInfo.SelectedTransmission != null)
                                                    {
                                                        hireVehicle.Transmission = (VehicleTransmission)s.VehicleAdditionalInfo.SelectedTransmission;
                                                    }

                                                    

                                                            supportingInvolvement.Vehicles.Add(hireVehicle);

                                                        }

                                                    }

                                                        #endregion


                                                    #region Additional Information

                                                    if (s.AdditionalInfo.EmailAddress != null)
                                                    {
                                                        supportingInvolvement.Email = s.AdditionalInfo.EmailAddress;
                                                    }

                                                    if (s.AdditionalInfo.MoJClaimsRegNumber != null)
                                                    {
                                                        supportingInvolvement.MojCrmNumber = s.AdditionalInfo.MoJClaimsRegNumber;
                                                    }

                                                    if (s.AdditionalInfo.Telephone1 != null)
                                                    {
                                                        supportingInvolvement.Telephone1 = s.AdditionalInfo.Telephone1;
                                                    }

                                                    if (s.AdditionalInfo.Telephone2 != null)
                                                    {
                                                        supportingInvolvement.Telephone2 = s.AdditionalInfo.Telephone2;
                                                    }

                                                    if (s.AdditionalInfo.Telephone3 != null)
                                                    {
                                                        supportingInvolvement.Telephone3 = s.AdditionalInfo.Telephone3;
                                                    }

                                                    if (s.AdditionalInfo.VATNumber != null)
                                                    {
                                                        supportingInvolvement.VatNumber = s.AdditionalInfo.VATNumber;
                                                    }

                                                    if (s.AdditionalInfo.WebsiteAddress != null)
                                                    {
                                                        supportingInvolvement.WebSite = s.AdditionalInfo.WebsiteAddress;
                                                    }

                                                    #endregion

                                                    thirdPartyDriver.Organisations.Add(supportingInvolvement);
                                                }

                                            }

                                        }
                                    }

                                }
                                #endregion

                                thirdPartyVehicle.People.Add(thirdPartyDriver);

                            }

                        }

                    }

                    #endregion

                    #region Third Party Vehicle Passenger


                    List<PassengerViewModel> thirdPartyPassengers = new List<PassengerViewModel>();

                    Dictionary<int, List<PassengerViewModel>> DictionaryThirdPartyPassengers = new Dictionary<int, List<PassengerViewModel>>();


                    DictionaryThirdPartyPassengers = (Dictionary<int, List<PassengerViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyPassengers"];

                    if (DictionaryThirdPartyPassengers != null)
                    {
                        if (DictionaryThirdPartyPassengers.Count() > 0)
                        {

                            if (DictionaryThirdPartyPassengers.ContainsKey(tpv.Value.CurrentVehicle))
                            {
                                thirdPartyPassengers = DictionaryThirdPartyPassengers[tpv.Value.CurrentVehicle];

                                foreach (var p in thirdPartyPassengers)
                                {

                                    MDA.Common.FileModel.Person passenger = new MDA.Common.FileModel.Person();

                                    if (p.PersonInformation.SelectedSalutation != null)
                                    {
                                        passenger.Salutation = (Salutation)p.PersonInformation.SelectedSalutation;
                                    }


                                    if (p.PersonInformation.Firstname != null)
                                    {
                                        passenger.FirstName = p.PersonInformation.Firstname;
                                    }

                                    if (p.PersonInformation.Middlename != null)
                                    {
                                        passenger.MiddleName = p.PersonInformation.Middlename;
                                    }

                                    if (p.PersonInformation.Surname != null)
                                    {
                                        passenger.LastName = p.PersonInformation.Surname;
                                    }

                                    if (p.PersonInformation.DateOfBirth != null)
                                    {
                                        passenger.DateOfBirth = p.PersonInformation.DateOfBirth;
                                    }

                                    if (p.PersonInformation.SelectedGender != null)
                                    {
                                        passenger.Gender = (Gender)p.PersonInformation.SelectedGender;
                                    }

                                    #region Address

                                    if (p.Address != null)
                                    {

                                        MDA.Common.FileModel.Address address = new MDA.Common.FileModel.Address();

                                        if (p.Address.SelectedAddressStatus != null)
                                        {

                                            switch (p.Address.SelectedAddressStatus)
                                            {
                                                case 1:
                                                    address.AddressLinkType = AddressLinkType.PreviousAddress;
                                                    break;
                                                case 2:
                                                    address.AddressLinkType = AddressLinkType.LinkedAddress;
                                                    break;
                                                case 3:
                                                    address.AddressLinkType = AddressLinkType.CurrentAddress;
                                                    break;
                                                case 4:
                                                    address.AddressLinkType = AddressLinkType.RegisteredOffice;
                                                    break;
                                                case 5:
                                                    address.AddressLinkType = AddressLinkType.TradingAddress;
                                                    break;
                                                case 6:
                                                    address.AddressLinkType = AddressLinkType.StorageAddress;
                                                    break;
                                                default:
                                                    address.AddressLinkType = AddressLinkType.Unknown;
                                                    break;
                                            }

                                        }
                                        else
                                        {
                                            address.AddressLinkType = AddressLinkType.Unknown;
                                        }

                                        if (p.Address.SubBuilding != null)
                                        {
                                            address.SubBuilding = p.Address.SubBuilding;
                                        }

                                        if (p.Address.BuildingName != null)
                                        {
                                            address.Building = p.Address.BuildingName;
                                        }

                                        if (p.Address.BuildingNumber != null)
                                        {
                                            address.BuildingNumber = p.Address.BuildingNumber;
                                        }

                                        if (p.Address.Street != null)
                                        {
                                            address.Street = p.Address.Street;
                                        }

                                        if (p.Address.Locality != null)
                                        {
                                            address.Locality = p.Address.Locality;
                                        }

                                        if (p.Address.Town != null)
                                        {
                                            address.Town = p.Address.Town;
                                        }

                                        if (p.Address.County != null)
                                        {
                                            address.County = p.Address.County;
                                        }

                                        if (p.Address.Postcode != null)
                                        {
                                            address.PostCode = p.Address.Postcode;
                                        }

                                        if (p.Address.StartOfResidency != null)
                                        {
                                            address.StartOfResidency = p.Address.StartOfResidency;
                                        }

                                        if (p.Address.EndOfResidency != null)
                                        {
                                            address.EndOfResidency = p.Address.EndOfResidency;
                                        }

                                        passenger.Addresses.Add(address);

                                    }

                                    #endregion

                                    #region Additional Information

                                    if (p.AdditionalInfo.HomeTelephone != null)
                                    {
                                        passenger.LandlineTelephone = p.AdditionalInfo.HomeTelephone;
                                    }

                                    if (p.AdditionalInfo.WorkTelephone != null)
                                    {
                                        passenger.WorkTelephone = p.AdditionalInfo.WorkTelephone;
                                    }

                                    if (p.AdditionalInfo.MobileNumber != null)
                                    {
                                        passenger.MobileTelephone = p.AdditionalInfo.MobileNumber;
                                    }

                                    if (p.AdditionalInfo.EmailAddress != null)
                                    {
                                        passenger.EmailAddress = p.AdditionalInfo.EmailAddress;
                                    }

                                    if (p.AdditionalInfo.Occupation != null)
                                    {
                                        passenger.Occupation = p.AdditionalInfo.Occupation;
                                    }

                                    if (p.AdditionalInfo.SelectedNationality != null)
                                    {
                                        passenger.Nationality = p.AdditionalInfo.SelectedNationality;
                                    }

                                    if (p.AdditionalInfo.PassportNumber != null)
                                    {
                                        passenger.PassportNumber = p.AdditionalInfo.PassportNumber;
                                    }

                                    if (p.AdditionalInfo.NINumber != null)
                                    {
                                        passenger.NINumber = p.AdditionalInfo.NINumber;
                                    }

                                    if (p.AdditionalInfo.DrivingLicenseNumber != null)
                                    {
                                        passenger.DrivingLicenseNumber = p.AdditionalInfo.DrivingLicenseNumber;
                                    }

                                    #endregion

                                    #region Supporting Involvements

                                    if (HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] != null)
                                    {
                                        Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>> DictionaryThirdPartyVehicleSupportingInvolvements = new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
                                        Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                                        List<SupportingInvolvementsViewModel> ThirdPartyPassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                                        DictionaryThirdPartyVehicleSupportingInvolvements = (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];

                                        if (DictionaryThirdPartyVehicleSupportingInvolvements != null)
                                        {

                                            if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(tpv.Value.CurrentVehicle))
                                            {
                                                DictionaryThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyVehicleSupportingInvolvements[tpv.Value.CurrentVehicle];

                                                if (DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(p.CurrentPassenger))
                                                {

                                                    ThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyPassengerSupportingInvolvements[p.CurrentPassenger];

                                                    foreach (var s in ThirdPartyPassengerSupportingInvolvements)
                                                    {
                                                        MDA.Common.FileModel.Organisation supportingInvolvement = new MDA.Common.FileModel.Organisation();

                                                        if (s.OrganisationInvolvement.OrganisationName != null)
                                                        {
                                                            supportingInvolvement.OrganisationName = s.OrganisationInvolvement.OrganisationName;
                                                        }

                                                        if (s.OrganisationInvolvement.RegisteredNumber != null)
                                                        {
                                                            supportingInvolvement.RegisteredNumber = s.OrganisationInvolvement.RegisteredNumber;
                                                        }

                                                        if (s.OrganisationInvolvement.SelectedOrganisationInvolvement != null)
                                                        {
                                                            supportingInvolvement.OrganisationType = (OrganisationType)s.OrganisationInvolvement.SelectedOrganisationInvolvement;

                                                            switch (s.OrganisationInvolvement.SelectedOrganisationInvolvement)
                                                            {
                                                                case 0:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                                                    break;
                                                                case 1:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                                                    break;
                                                                case 2:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.UndefinedSupplier;
                                                                    break;
                                                                case 3:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.AccidentManagement;
                                                                    break;
                                                                case 4:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Hire;
                                                                    break;
                                                                case 5:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Solicitor;
                                                                    break;
                                                                case 6:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Engineer;
                                                                    break;
                                                                case 7:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Recovery;
                                                                    break;
                                                                case 8:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Storage;
                                                                    break;
                                                                case 9:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Broker;
                                                                    break;
                                                                case 10:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.MedicalExaminer;
                                                                    break;
                                                                case 11:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Repairer;
                                                                    break;
                                                                case 12:
                                                                    supportingInvolvement.Person2OrganisationLinkType = Person2OrganisationLinkType.Unknown;
                                                                    break;
                                                            }
                                                        }

                                                        #region Address

                                                        if (s.Address != null)
                                                        {

                                                            MDA.Common.FileModel.Address address = new MDA.Common.FileModel.Address();

                                                            if (s.Address.SelectedAddressStatus != null)
                                                            {

                                                                switch (s.Address.SelectedAddressStatus)
                                                                {
                                                                    case 1:
                                                                        address.AddressLinkType = AddressLinkType.PreviousAddress;
                                                                        break;
                                                                    case 2:
                                                                        address.AddressLinkType = AddressLinkType.LinkedAddress;
                                                                        break;
                                                                    case 3:
                                                                        address.AddressLinkType = AddressLinkType.CurrentAddress;
                                                                        break;
                                                                    case 4:
                                                                        address.AddressLinkType = AddressLinkType.RegisteredOffice;
                                                                        break;
                                                                    case 5:
                                                                        address.AddressLinkType = AddressLinkType.TradingAddress;
                                                                        break;
                                                                    case 6:
                                                                        address.AddressLinkType = AddressLinkType.StorageAddress;
                                                                        break;
                                                                    default:
                                                                        address.AddressLinkType = AddressLinkType.Unknown;
                                                                        break;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                address.AddressLinkType = AddressLinkType.Unknown;
                                                            }


                                                            if (s.Address.SubBuilding != null)
                                                            {
                                                                address.SubBuilding = s.Address.SubBuilding;
                                                            }

                                                            if (s.Address.BuildingName != null)
                                                            {
                                                                address.Building = s.Address.BuildingName;
                                                            }

                                                            if (s.Address.BuildingNumber != null)
                                                            {
                                                                address.BuildingNumber = s.Address.BuildingNumber;
                                                            }

                                                            if (s.Address.Street != null)
                                                            {
                                                                address.Street = s.Address.Street;
                                                            }

                                                            if (s.Address.Locality != null)
                                                            {
                                                                address.Locality = s.Address.Locality;
                                                            }

                                                            if (s.Address.Town != null)
                                                            {
                                                                address.Town = s.Address.Town;
                                                            }

                                                            if (s.Address.County != null)
                                                            {
                                                                address.County = s.Address.County;
                                                            }

                                                            if (s.Address.Postcode != null)
                                                            {
                                                                address.PostCode = s.Address.Postcode;
                                                            }

                                                            if (s.Address.StartOfResidency != null)
                                                            {
                                                                address.StartOfResidency = s.Address.StartOfResidency;
                                                            }

                                                            if (s.Address.EndOfResidency != null)
                                                            {
                                                                address.EndOfResidency = s.Address.EndOfResidency;
                                                            }


                                                            supportingInvolvement.Addresses.Add(address);
                                                        }

                                                        #endregion


                                                        #region Hire Vehicle

                                                        if (s.OrganisationInvolvement.SelectedOrganisationInvolvement == 3 || s.OrganisationInvolvement.SelectedOrganisationInvolvement == 4)
                                                        {

                                                            if (s.Vehicle.RegistrationNumber != null || s.Vehicle.Make != null || s.Vehicle.Model != null || s.Vehicle.HireStartDate != null || s.Vehicle.HireEndDate != null)
                                                            {
                                                                MDA.Common.FileModel.OrganisationVehicle hireVehicle =
                                                                    new MDA.Common.FileModel.OrganisationVehicle();

                                                                hireVehicle.Incident2VehicleLinkType = Incident2VehicleLinkType.ThirdPartyHireVehicle;

                                                                if (s.Vehicle.SelectedVehicletype != null)
                                                                {
                                                                    hireVehicle.VehicleType = (VehicleType)s.Vehicle.SelectedVehicletype;
                                                                }

                                                                if (s.Vehicle.RegistrationNumber != null)
                                                                {
                                                                    hireVehicle.VehicleRegistration = s.Vehicle.RegistrationNumber;
                                                                }

                                                                if (s.Vehicle.Make != null)
                                                                {
                                                                    hireVehicle.Make = s.Vehicle.Make;
                                                                }

                                                                if (s.Vehicle.Model != null)
                                                                {
                                                                    hireVehicle.Model = s.Vehicle.Model;
                                                                }

                                                                if (s.Vehicle.HireStartDate != null)
                                                                {
                                                                    hireVehicle.HireStartDate = s.Vehicle.HireStartDate;
                                                                }

                                                                if (s.Vehicle.HireEndDate != null)
                                                                {
                                                                    hireVehicle.HireEndDate = s.Vehicle.HireEndDate;
                                                                }

                                                        #endregion

                                                                #region Hire Vehicle Additional Information

                                                                if (s.VehicleAdditionalInfo.EngineCapacity != null)
                                                                    hireVehicle.EngineCapacity = s.VehicleAdditionalInfo.EngineCapacity;

                                                                if (s.VehicleAdditionalInfo.SelectedFuel != null)
                                                                {
                                                                    switch (s.VehicleAdditionalInfo.SelectedFuel)
                                                                    {
                                                                        case 0:
                                                                            hireVehicle.Fuel = VehicleFuelType.Diesel;
                                                                            break;
                                                                        case 1:
                                                                            hireVehicle.Fuel = VehicleFuelType.LPG;
                                                                            break;
                                                                        case 2:
                                                                            hireVehicle.Fuel = VehicleFuelType.Petrol;
                                                                            break;
                                                                        case 3:
                                                                            hireVehicle.Fuel = VehicleFuelType.Unknown;
                                                                            break;
                                                                    }
                                                                }


                                                                if (s.VehicleAdditionalInfo.SelectedTransmission != null)
                                                                {
                                                                    switch (s.VehicleAdditionalInfo.SelectedTransmission)
                                                                    {
                                                                        case 0:
                                                                            hireVehicle.Transmission = VehicleTransmission.Automatic;
                                                                            break;
                                                                        case 1:
                                                                            hireVehicle.Transmission = VehicleTransmission.Manual;
                                                                            break;
                                                                        case 2:
                                                                            hireVehicle.Transmission = VehicleTransmission.Unknown;
                                                                            break;

                                                                    }
                                                                }

                                                                if (s.VehicleAdditionalInfo.VIN != null)
                                                                    hireVehicle.VIN = s.VehicleAdditionalInfo.VIN;


                                                                // Note more fields to add here

                                                                supportingInvolvement.Vehicles.Add(hireVehicle);

                                                            }
                                                        }

                                                            #endregion


                                                        #region Additional Information

                                                        if (s.AdditionalInfo.EmailAddress != null)
                                                        {
                                                            supportingInvolvement.Email = s.AdditionalInfo.EmailAddress;
                                                        }

                                                        if (s.AdditionalInfo.MoJClaimsRegNumber != null)
                                                        {
                                                            supportingInvolvement.MojCrmNumber = s.AdditionalInfo.MoJClaimsRegNumber;
                                                        }

                                                        if (s.AdditionalInfo.Telephone1 != null)
                                                        {
                                                            supportingInvolvement.Telephone1 = s.AdditionalInfo.Telephone1;
                                                        }

                                                        if (s.AdditionalInfo.Telephone2 != null)
                                                        {
                                                            supportingInvolvement.Telephone2 = s.AdditionalInfo.Telephone2;
                                                        }

                                                        if (s.AdditionalInfo.Telephone3 != null)
                                                        {
                                                            supportingInvolvement.Telephone3 = s.AdditionalInfo.Telephone3;
                                                        }

                                                        if (s.AdditionalInfo.VATNumber != null)
                                                        {
                                                            supportingInvolvement.VatNumber = s.AdditionalInfo.VATNumber;
                                                        }

                                                        if (s.AdditionalInfo.WebsiteAddress != null)
                                                        {
                                                            supportingInvolvement.WebSite = s.AdditionalInfo.WebsiteAddress;
                                                        }

                                                        #endregion

                                                        passenger.Organisations.Add(supportingInvolvement);
                                                    }

                                                }
                                            }
                                        }
                                    }

                                                                       

                                    #endregion


                                    passenger.PartyType = PartyType.ThirdParty;
                                    passenger.SubPartyType = SubPartyType.Passenger;


                                    thirdPartyVehicle.People.Add(passenger);

                                }
                            }
                        }

                    }

                    #endregion

                    claim.Vehicles.Add(thirdPartyVehicle);


                    vehicleCount++;

                }

            }

            #endregion
            
            #region Witness

            Dictionary<int, WitnessViewModel> witnesses = new Dictionary<int, WitnessViewModel>();

            witnesses = HttpContext.Current.Session["DictionaryWitnessViewModels"] as Dictionary<int, WitnessViewModel>;

            if (witnesses != null)
            {

                foreach (var p in witnesses)
                {

                    MDA.Common.FileModel.Person witness = new MDA.Common.FileModel.Person();

                    if (p.Value.PersonInformation.SelectedSalutation != null)
                    {
                        witness.Salutation = (Salutation)p.Value.PersonInformation.SelectedSalutation;
                    }

                    if (p.Value.PersonInformation.Firstname != null)
                    {
                        witness.FirstName = p.Value.PersonInformation.Firstname;
                    }

                    if (p.Value.PersonInformation.Middlename != null)
                    {
                        witness.MiddleName = p.Value.PersonInformation.Middlename;
                    }

                    if (p.Value.PersonInformation.Surname != null)
                    {
                        witness.LastName = p.Value.PersonInformation.Surname;
                    }

                    if (p.Value.PersonInformation.DateOfBirth != null)
                    {
                        witness.DateOfBirth = p.Value.PersonInformation.DateOfBirth;
                    }

                    if (p.Value.PersonInformation.SelectedGender != null)
                    {
                        witness.Gender = (Gender)p.Value.PersonInformation.SelectedGender;
                    }


                    #region Address

                    if (p.Value.Address != null)
                    {

                        MDA.Common.FileModel.Address address = new MDA.Common.FileModel.Address();

                        if (p.Value.Address.SelectedAddressStatus != null)
                        {

                            switch (p.Value.Address.SelectedAddressStatus)
                            {
                                case 1:
                                    address.AddressLinkType = AddressLinkType.PreviousAddress;
                                    break;
                                case 2:
                                    address.AddressLinkType = AddressLinkType.LinkedAddress;
                                    break;
                                case 3:
                                    address.AddressLinkType = AddressLinkType.CurrentAddress;
                                    break;
                                case 4:
                                    address.AddressLinkType = AddressLinkType.RegisteredOffice;
                                    break;
                                case 5:
                                    address.AddressLinkType = AddressLinkType.TradingAddress;
                                    break;
                                case 6:
                                    address.AddressLinkType = AddressLinkType.StorageAddress;
                                    break;
                                default:
                                    address.AddressLinkType = AddressLinkType.Unknown;
                                    break;
                            }

                        }
                        else
                        {
                            address.AddressLinkType = AddressLinkType.Unknown;
                        }

                        if (p.Value.Address.SubBuilding != null)
                        {
                            address.SubBuilding = p.Value.Address.SubBuilding;
                        }

                        if (p.Value.Address.BuildingName != null)
                        {
                            address.Building = p.Value.Address.BuildingName;
                        }

                        if (p.Value.Address.BuildingNumber != null)
                        {
                            address.BuildingNumber = p.Value.Address.BuildingNumber;
                        }

                        if (p.Value.Address.Street != null)
                        {
                            address.Street = p.Value.Address.Street;
                        }

                        if (p.Value.Address.Locality != null)
                        {
                            address.Locality = p.Value.Address.Locality;
                        }

                        if (p.Value.Address.Town != null)
                        {
                            address.Town = p.Value.Address.Town;
                        }

                        if (p.Value.Address.County != null)
                        {
                            address.County = p.Value.Address.County;
                        }

                        if (p.Value.Address.Postcode != null)
                        {
                            address.PostCode = p.Value.Address.Postcode;
                        }

                        witness.Addresses.Add(address);

                    }

                    #endregion

                    #region Additional Information

                    if (p.Value.AdditionalInfo.HomeTelephone != null)
                    {
                        insuredDriver.LandlineTelephone = p.Value.AdditionalInfo.HomeTelephone;
                    }

                    if (p.Value.AdditionalInfo.WorkTelephone != null)
                    {
                        insuredDriver.WorkTelephone = p.Value.AdditionalInfo.WorkTelephone;
                    }

                    if (p.Value.AdditionalInfo.MobileNumber != null)
                    {
                        insuredDriver.MobileTelephone = p.Value.AdditionalInfo.MobileNumber;
                    }

                    if (p.Value.AdditionalInfo.EmailAddress != null)
                    {
                        insuredDriver.EmailAddress = p.Value.AdditionalInfo.EmailAddress;
                    }

                    if (p.Value.AdditionalInfo.Occupation != null)
                    {
                        insuredDriver.Occupation = p.Value.AdditionalInfo.Occupation;
                    }

                    if (p.Value.AdditionalInfo.SelectedNationality != null)
                    {
                        insuredDriver.Nationality = p.Value.AdditionalInfo.SelectedNationality;
                    }

                    if (p.Value.AdditionalInfo.PassportNumber != null)
                    {
                        insuredDriver.PassportNumber = p.Value.AdditionalInfo.PassportNumber;
                    }

                    if (p.Value.AdditionalInfo.NINumber != null)
                    {
                        insuredDriver.NINumber = p.Value.AdditionalInfo.NINumber;
                    }

                    if (p.Value.AdditionalInfo.DrivingLicenseNumber != null)
                    {
                        insuredDriver.DrivingLicenseNumber = p.Value.AdditionalInfo.DrivingLicenseNumber;
                    }

                    #endregion

                    MDA.Common.FileModel.Vehicle noVehicleInvolved = new MDA.Common.FileModel.Vehicle();
                    noVehicleInvolved.Incident2VehicleLinkType = Incident2VehicleLinkType.NoVehicleInvolved;
                    witness.PartyType = PartyType.Witness;
                    noVehicleInvolved.People.Add(witness);
                    claim.Vehicles.Add(noVehicleInvolved);




                }
            }

            #endregion


            return claim;
        }

        public static MotorClaim TrimClaim(MotorClaim claim)
        {
            if (claim.MotorClaimInfo.IncidentLocation != null)
            {
                string[] incidentDetails = claim.MotorClaimInfo.IncidentLocation.Split('|');
                claim.MotorClaimInfo.IncidentLocation = incidentDetails[0];
            }

            if (claim.MotorClaimInfo.ClaimCode != null)
            {
                string[] claimCode = claim.MotorClaimInfo.ClaimCode.Split('|');
                claim.MotorClaimInfo.ClaimCode = claimCode[0];
            }


            foreach (var v in claim.Vehicles)
            {
                if (v.DamageDescription != null)
                {
                    //string[] damageDescription = v.DamageDescription.Split('|');
                    //v.DamageDescription = damageDescription[0];
                    v.DamageDescription = null;
                }

            }

            return claim;
        }

    }

}