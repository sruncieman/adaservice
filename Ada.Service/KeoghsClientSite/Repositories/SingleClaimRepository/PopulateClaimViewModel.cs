﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.Models.SingleClaim;
using KeoghsClientSite.StaticClasses;
using MDA.WCF.WebServices.Interface;
using MDA.WCF.WebServices.Messages;


namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        public static ClaimViewModel PopulateClaimViewModel(bool Previous = false)
        {
            ClaimViewModel claimViewModel = new ClaimViewModel();

            if (HttpContext.Current.Session["claimViewModel"] != null)
            {
                claimViewModel = HttpContext.Current.Session["claimViewModel"] as ClaimViewModel;
            }

            IEnumerable<SelectListItem> ClaimTypeListItems = new List<SelectListItem>{
               new SelectListItem(){Text="Motor",Value="1"}
               };

            //ClaimType
            claimViewModel.ClaimTypes = ClaimTypeListItems;

            IEnumerable<SelectListItem> ClaimStatusListItems = new List<SelectListItem>{
               new SelectListItem(){Text="Open",Value="1"},
               new SelectListItem(){Text="Closed",Value="2"}
               };

            claimViewModel.ClaimStatuses = ClaimStatusListItems;

            claimViewModel.NavigationData.CurrentForm = CurrentView.Claim.ToString();
            claimViewModel.NavigationData.Previous = Previous;

            return claimViewModel;
        }
    }
}