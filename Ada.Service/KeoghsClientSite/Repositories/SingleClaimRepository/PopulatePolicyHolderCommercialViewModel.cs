﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        public static PolicyHolderCommercialViewModel PopulatePolicyHolderCommercialViewModel(string NavigationData = "", bool Previous = false)
        {
            PolicyHolderCommercialViewModel policyHolderCommercialViewmodel = new PolicyHolderCommercialViewModel();

            if (HttpContext.Current.Session["policyHolderCommercialViewmodel"] != null)
            {
                policyHolderCommercialViewmodel = HttpContext.Current.Session["policyHolderCommercialViewmodel"] as PolicyHolderCommercialViewModel;
            }

            InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();

            if (HttpContext.Current.Session["insuredVehicleViewmodel"] != null)
            {
                insuredVehicleViewModel = HttpContext.Current.Session["insuredVehicleViewmodel"] as InsuredVehicleViewModel;
            }

            insuredVehicleViewModel.Vehicle.PolicyCommercial = true;
            insuredVehicleViewModel.PreviousPage = CurrentView.Policy;
            StoreInsuredVehicleViewModel(insuredVehicleViewModel);
            

            //
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                policyHolderCommercialViewmodel.NavigationData.Data = NavigationData;

            policyHolderCommercialViewmodel.NavigationData.CurrentForm = CurrentView.PolicyHolderPersonal.ToString();
            policyHolderCommercialViewmodel.NavigationData.Previous = Previous;

            return policyHolderCommercialViewmodel;
        }
    }
}