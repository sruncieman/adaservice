﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        public static PolicyHolderPersonalViewModel PopulatePolicyHolderPersonalViewModel( string NavigationData = "", bool Previous = false)
        {
            PolicyHolderPersonalViewModel policyHolderPersonalViewModel = new PolicyHolderPersonalViewModel();

            if (HttpContext.Current.Session["policyHolderPersonalViewModel"] != null)
            {
                policyHolderPersonalViewModel = HttpContext.Current.Session["policyHolderPersonalViewModel"] as PolicyHolderPersonalViewModel;
            }

            InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();

            if (HttpContext.Current.Session["insuredVehicleViewmodel"] != null)
            {
                insuredVehicleViewModel = HttpContext.Current.Session["insuredVehicleViewmodel"] as InsuredVehicleViewModel;
            }

            PolicyViewModel policyViewModel = new PolicyViewModel();

            if (HttpContext.Current.Session["policyViewmodel"] != null)
            {
                policyViewModel = HttpContext.Current.Session["policyViewmodel"] as PolicyViewModel;
            }

            if (policyViewModel.PolicyHolderDetailsKnown==true)
                insuredVehicleViewModel.PreviousPage = CurrentView.PolicyHolderPersonal;
            else
                insuredVehicleViewModel.PreviousPage = CurrentView.Policy;
          
           
            insuredVehicleViewModel.Vehicle.PolicyCommercial = false;
            
          
            StoreInsuredVehicleViewModel(insuredVehicleViewModel);

            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                policyHolderPersonalViewModel.NavigationData.Data = NavigationData;

            policyHolderPersonalViewModel.NavigationData.CurrentForm = CurrentView.PolicyHolderPersonal.ToString();
            policyHolderPersonalViewModel.NavigationData.Previous = Previous;

            return policyHolderPersonalViewModel;
        }
    }
}