﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        public static WitnessViewModel PopulateWitnessViewModel(int CurrentWitnessID, CurrentView PreviousPage, string NavigationData = "", bool Previous = false)
        {
            bool WitnessFoundInSession = false;
            WitnessViewModel witnessViewModel = new WitnessViewModel();

            Dictionary<int, WitnessViewModel> DictionaryWitnessViewModels = new Dictionary<int, WitnessViewModel>();

            if (HttpContext.Current.Session["DictionaryWitnessViewModels"] != null)
            {
                DictionaryWitnessViewModels = HttpContext.Current.Session["DictionaryWitnessViewModels"] as Dictionary<int, WitnessViewModel>;

                if (DictionaryWitnessViewModels.ContainsKey(CurrentWitnessID))
                {
                    //
                    witnessViewModel = DictionaryWitnessViewModels[CurrentWitnessID];


                    WitnessFoundInSession = true;
                    witnessViewModel.NumberOfWitnesses = NumberOfWitnesses();
                }
            }
            if (!WitnessFoundInSession)
            {
                witnessViewModel.PreviousPage = PreviousPage;
                witnessViewModel.CurrentWitness = CurrentWitnessID;
                witnessViewModel.NumberOfWitnesses = NumberOfWitnesses();
                // witnessViewModel.VehicleID = CurrentVehicleID;
                witnessViewModel.NavigationData.Data = NavigationData;
            }
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                witnessViewModel.NavigationData.Data = NavigationData;

            witnessViewModel.NavigationData.CurrentForm = CurrentView.Witness.ToString();
            witnessViewModel.NavigationData.Previous = Previous;
            //
            return witnessViewModel;
        }
    }
}