﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        public static InsuredDriverViewModel PopulateInsuredDriverViewModel(string NavigationData = "", bool Previous = false, string NextPage = "")
        {
            InsuredDriverViewModel insuredDriverViewModel = new InsuredDriverViewModel();

            // bool InsuredDriverFoundInSession = false;

            if (HttpContext.Current.Session["insuredDriverViewModel"] != null)
            {
                insuredDriverViewModel = HttpContext.Current.Session["insuredDriverViewModel"] as InsuredDriverViewModel;
                // InsuredDriverFoundInSession = true;
            }

            PolicyViewModel policyViewModel = new PolicyViewModel();

            if (HttpContext.Current.Session["policyViewModel"] != null)
            {
                policyViewModel = HttpContext.Current.Session["policyViewModel"] as PolicyViewModel;
            }

            InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();

            if (HttpContext.Current.Session["insuredVehicleViewModel"] != null)
            {
                insuredVehicleViewModel = HttpContext.Current.Session["insuredVehicleViewModel"] as InsuredVehicleViewModel;
            }

            if (policyViewModel.SelectedPolicyType == 1 && policyViewModel.PolicyHolderDetailsKnown == true && insuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver == true)
            {
                // Populate Insured driver with policy holder details
                PolicyHolderPersonalViewModel policyHolderPersonalViewModel = new PolicyHolderPersonalViewModel();

                insuredDriverViewModel.PopulateWithPolicyHolder = true;

                if (HttpContext.Current.Session["policyHolderPersonalViewModel"] != null)
                {
                    policyHolderPersonalViewModel = HttpContext.Current.Session["policyHolderPersonalViewModel"] as PolicyHolderPersonalViewModel;
                }

                PopulateInsuredDriverFromPolicyHolder(insuredDriverViewModel, policyHolderPersonalViewModel);

                StoreInsuredDriverViewModel(insuredDriverViewModel);
            }

            if (policyViewModel.SelectedPolicyType == 1 && policyViewModel.PolicyHolderDetailsKnown == true)
            {

                PolicyHolderPersonalViewModel policyHolderPersonalViewModel = new PolicyHolderPersonalViewModel();

                policyHolderPersonalViewModel = HttpContext.Current.Session["policyHolderPersonalViewModel"] as PolicyHolderPersonalViewModel;

                if (policyHolderPersonalViewModel != null)
                {

                    if (policyHolderPersonalViewModel.Address.BuildingName != null || policyHolderPersonalViewModel.Address.BuildingNumber != null || policyHolderPersonalViewModel.Address.County != null || policyHolderPersonalViewModel.Address.Locality != null || policyHolderPersonalViewModel.Address.Postcode != null || policyHolderPersonalViewModel.Address.Street != null || policyHolderPersonalViewModel.Address.SubBuilding != null || policyHolderPersonalViewModel.Address.Town != null)
                    {
                        insuredDriverViewModel.Address.PolicyHolderAddressPopulated = true;
                    }
                    else
                    {
                        insuredDriverViewModel.Address.PolicyHolderAddressPopulated = false;
                    }

                }
                else
                {
                    insuredDriverViewModel.Address.PolicyHolderAddressPopulated = false;
                }
            }

            if (policyViewModel.SelectedPolicyType == 1 && policyViewModel.PolicyHolderDetailsKnown == true)
            {
                PolicyHolderPersonalViewModel policyHolderPersonalViewModel = new PolicyHolderPersonalViewModel();

                if (policyHolderPersonalViewModel != null)
                {
                    insuredDriverViewModel.PopulateWithPolicyHolder = true;

                    if (HttpContext.Current.Session["policyHolderPersonalViewModel"] != null)
                    {
                        policyHolderPersonalViewModel = HttpContext.Current.Session["policyHolderPersonalViewModel"] as PolicyHolderPersonalViewModel;
                    }

                    insuredDriverViewModel.Address.PolicyHolderSubBuilding = policyHolderPersonalViewModel.Address.SubBuilding;
                    insuredDriverViewModel.Address.PolicyHolderBuildingName = policyHolderPersonalViewModel.Address.BuildingName;
                    insuredDriverViewModel.Address.PolicyHolderBuildingNumber = policyHolderPersonalViewModel.Address.BuildingNumber;
                    insuredDriverViewModel.Address.PolicyHolderStreet = policyHolderPersonalViewModel.Address.Street;
                    insuredDriverViewModel.Address.PolicyHolderTown = policyHolderPersonalViewModel.Address.Town;
                    insuredDriverViewModel.Address.PolicyHolderCounty = policyHolderPersonalViewModel.Address.County;
                    insuredDriverViewModel.Address.PolicyHolderLocality = policyHolderPersonalViewModel.Address.Locality;
                    insuredDriverViewModel.Address.PolicyHolderPostcode = policyHolderPersonalViewModel.Address.Postcode;

                    if (insuredDriverViewModel.Address.SameAddressAsPolicyHolder == true)
                    {
                        insuredDriverViewModel.Address.SubBuilding = policyHolderPersonalViewModel.Address.SubBuilding;
                        insuredDriverViewModel.Address.BuildingName = policyHolderPersonalViewModel.Address.BuildingName;
                        insuredDriverViewModel.Address.BuildingNumber = policyHolderPersonalViewModel.Address.BuildingNumber;
                        insuredDriverViewModel.Address.Street = policyHolderPersonalViewModel.Address.Street;
                        insuredDriverViewModel.Address.Town = policyHolderPersonalViewModel.Address.Town;
                        insuredDriverViewModel.Address.County = policyHolderPersonalViewModel.Address.County;
                        insuredDriverViewModel.Address.Locality = policyHolderPersonalViewModel.Address.Locality;
                        insuredDriverViewModel.Address.Postcode = policyHolderPersonalViewModel.Address.Postcode;
                    }
                }
            }



            if (policyViewModel.SelectedPolicyType == 1 && policyViewModel.PolicyHolderDetailsKnown == true && (insuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver == false || insuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver == null))
            {
                insuredDriverViewModel.PopulateWithPolicyHolder = false;

                StoreInsuredDriverViewModel(insuredDriverViewModel);
            }

            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                insuredDriverViewModel.NavigationData.Data = NavigationData;

            insuredDriverViewModel.NavigationData.CurrentForm = CurrentView.InsuredDriver.ToString();
            insuredDriverViewModel.NavigationData.Previous = Previous;
            insuredDriverViewModel.NavigationData.NextPage = NextPage;

            //
            return insuredDriverViewModel;

        }

        private static void PopulateInsuredDriverFromPolicyHolder(InsuredDriverViewModel insuredDriverViewModel, PolicyHolderPersonalViewModel policyHolderPersonalViewModel)
        {
            insuredDriverViewModel.PersonInformation.DateOfBirth = policyHolderPersonalViewModel.PersonInformation.DateOfBirth;
            insuredDriverViewModel.PersonInformation.Firstname = policyHolderPersonalViewModel.PersonInformation.Firstname;
            insuredDriverViewModel.PersonInformation.Middlename = policyHolderPersonalViewModel.PersonInformation.Middlename;
            insuredDriverViewModel.PersonInformation.SelectedGender = policyHolderPersonalViewModel.PersonInformation.SelectedGender;
            insuredDriverViewModel.PersonInformation.SelectedSalutation = policyHolderPersonalViewModel.PersonInformation.SelectedSalutation;
            insuredDriverViewModel.PersonInformation.SelectedWitnessInvolvementType = policyHolderPersonalViewModel.PersonInformation.SelectedWitnessInvolvementType;
            insuredDriverViewModel.PersonInformation.Surname = policyHolderPersonalViewModel.PersonInformation.Surname;
            insuredDriverViewModel.AdditionalInfo.DrivingLicenseNumber = policyHolderPersonalViewModel.AdditionalInfo.DrivingLicenseNumber;
            insuredDriverViewModel.AdditionalInfo.EmailAddress = policyHolderPersonalViewModel.AdditionalInfo.EmailAddress;
            insuredDriverViewModel.AdditionalInfo.HomeTelephone = policyHolderPersonalViewModel.AdditionalInfo.HomeTelephone;
            insuredDriverViewModel.AdditionalInfo.MobileNumber = policyHolderPersonalViewModel.AdditionalInfo.MobileNumber;
            insuredDriverViewModel.AdditionalInfo.SelectedNationality = policyHolderPersonalViewModel.AdditionalInfo.SelectedNationality;
            insuredDriverViewModel.AdditionalInfo.WorkTelephone = policyHolderPersonalViewModel.AdditionalInfo.WorkTelephone;
            insuredDriverViewModel.AdditionalInfo.PassportNumber = policyHolderPersonalViewModel.AdditionalInfo.PassportNumber;
            insuredDriverViewModel.AdditionalInfo.NINumber = policyHolderPersonalViewModel.AdditionalInfo.NINumber;
            insuredDriverViewModel.Address.SelectedAddressStatus = policyHolderPersonalViewModel.Address.SelectedAddressStatus;
            insuredDriverViewModel.Address.SubBuilding = policyHolderPersonalViewModel.Address.SubBuilding;
            insuredDriverViewModel.Address.BuildingName = policyHolderPersonalViewModel.Address.BuildingName;
            insuredDriverViewModel.Address.BuildingNumber = policyHolderPersonalViewModel.Address.BuildingNumber;
            insuredDriverViewModel.Address.Street = policyHolderPersonalViewModel.Address.Street;
            insuredDriverViewModel.Address.Town = policyHolderPersonalViewModel.Address.Town;
            insuredDriverViewModel.Address.County = policyHolderPersonalViewModel.Address.County;
            insuredDriverViewModel.Address.EndOfResidency = policyHolderPersonalViewModel.Address.EndOfResidency;
            insuredDriverViewModel.Address.Locality = policyHolderPersonalViewModel.Address.Locality;
            insuredDriverViewModel.Address.Postcode = policyHolderPersonalViewModel.Address.Postcode;
            insuredDriverViewModel.Address.StartOfResidency = policyHolderPersonalViewModel.Address.StartOfResidency;
            //insuredDriverViewModel.BankAccount.AccountNumber = policyHolderPersonalViewModel.BankAccount.AccountNumber;
            //insuredDriverViewModel.BankAccount.BankName = policyHolderPersonalViewModel.BankAccount.BankName;
            //insuredDriverViewModel.BankAccount.DateDetailsTaken = policyHolderPersonalViewModel.BankAccount.DateDetailsTaken;
            //insuredDriverViewModel.BankAccount.SelectedAccountUsedFor = policyHolderPersonalViewModel.BankAccount.SelectedAccountUsedFor;
            //insuredDriverViewModel.BankAccount.SortCode = policyHolderPersonalViewModel.BankAccount.SortCode;
        }
    }
}