﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;
using CurrentView = KeoghsClientSite.Models.SingleClaim.CurrentView;
using PassengerViewModel = KeoghsClientSite.Models.SingleClaim.PassengerViewModel;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        //
        //
        //
        public static PassengerViewModel PopulatePassengerViewModel(int CurrentPassenger,
                                                                    CurrentView PreviousPage,
                                                                    int PreviousPageID,
                                                                    Models.SingleClaim.PassengerType PassengerType,
                                                                    int VehicleID, string NavigationData = "", bool Previous = false, string NextPage = "")
        {
            PassengerViewModel passengerViewModel = new PassengerViewModel();
            bool PassengerFoundInSession = false;
            int NumberPassengers = 0;

            if (PassengerType == Models.SingleClaim.PassengerType.InsuredVehiclePassenger)
            {
                NumberPassengers = InsuredVehicleNumberPassengers();

                List<PassengerViewModel> insuredDriverPassengers = new List<PassengerViewModel>();

                insuredDriverPassengers = (List<PassengerViewModel>)HttpContext.Current.Session["InsuredDriverPassengers"];
                // subtract one from CurrSupportingInvolvement to account for zero based List index
                if (insuredDriverPassengers != null)
                {
                    if (CurrentPassenger - 1 < insuredDriverPassengers.Count())
                    {
                        passengerViewModel = insuredDriverPassengers[CurrentPassenger - 1];

                        passengerViewModel.PreviousPage = SingleClaimRepository.CheckPreviousPassengerHasSupportingInvolvement(passengerViewModel);
                        
                        PassengerFoundInSession = true;
                    }
                }
            }
            else if (PassengerType == Models.SingleClaim.PassengerType.ThirdPartyVehiclePassenger)
            {
                NumberPassengers = ThirdPartyVehicleNumberPassengers(VehicleID);

                Dictionary<int, List<PassengerViewModel>> DictionaryThirdPartyPassengers = new Dictionary<int, List<PassengerViewModel>>();
                List<PassengerViewModel> ThirdPartyPassengers = new List<PassengerViewModel>();

                DictionaryThirdPartyPassengers = (Dictionary<int, List<PassengerViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyPassengers"];
               
                if (DictionaryThirdPartyPassengers != null)
                {
                    // Check if model already exists in List
                    if (DictionaryThirdPartyPassengers.ContainsKey(VehicleID))
                    {
                        ThirdPartyPassengers = DictionaryThirdPartyPassengers[VehicleID];
                        if (CurrentPassenger - 1 < ThirdPartyPassengers.Count())
                        {
                            passengerViewModel = ThirdPartyPassengers[CurrentPassenger - 1];

                            passengerViewModel.PreviousPage = SingleClaimRepository.CheckPreviousPassengerHasSupportingInvolvement(passengerViewModel);

                            passengerViewModel.NumberPassengers = NumberPassengers;

                            PassengerFoundInSession = true;
                        }
                    }
                }
            }
            //
            if (!PassengerFoundInSession)
            {
                passengerViewModel.CurrentPassenger = CurrentPassenger;
                passengerViewModel.NumberPassengers = NumberPassengers;
                passengerViewModel.PassengerID = CurrentPassenger;

                passengerViewModel.PreviousPage = PreviousPage;
                passengerViewModel.PreviousPageID = PreviousPageID;
                passengerViewModel.VehicleID = VehicleID;
                passengerViewModel.PassengerType = PassengerType;
                //

            }
            //

            //
            if (PassengerType == Models.SingleClaim.PassengerType.InsuredVehiclePassenger)
                passengerViewModel.NavigationData.CurrentForm = Models.SingleClaim.PassengerType.InsuredVehiclePassenger.ToString();
            else
                passengerViewModel.NavigationData.CurrentForm = PassengerType.ThirdPartyVehiclePassenger.ToString();
            //
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                passengerViewModel.NavigationData.Data = NavigationData;

            passengerViewModel.NavigationData.Previous = Previous;
            passengerViewModel.NavigationData.NextPage = NextPage;

            return passengerViewModel;
        }
    }
}