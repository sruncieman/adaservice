﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        public static ReviewAndSubmitViewModel PopulateReviewAndSubmitViewModel(int NumberOfVehicles, int CurrentVehicleID,
                                                                               CurrentView PreviousPage, string NavigationData = "", bool Previous = false)
        {
            ReviewAndSubmitViewModel reviewAndSubmitViewModel = new ReviewAndSubmitViewModel();
            bool ReviewAndSubmitFoundInSession = false;

            if (HttpContext.Current.Session["ReviewAndSubmitViewModel"] != null)
            {
                reviewAndSubmitViewModel = HttpContext.Current.Session["ReviewAndSubmitViewModel"] as ReviewAndSubmitViewModel;
                reviewAndSubmitViewModel.PreviousPage = PreviousPage;

                //List<SupportingInvolvementsViewModel> thirdPartyDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
                //Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyDriverSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                ////Get dictionary from session
                //DictionaryThirdPartyDriverSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"];
                //// Check if model already exists in List, if not add it

                //Dictionary<int, List<PassengerViewModel>> DictionaryThirdPartyPassengers = new Dictionary<int, List<PassengerViewModel>>();
                //List<PassengerViewModel> ThirdPartyPassengers = new List<PassengerViewModel>();

                //Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>> DictionaryThirdPartyVehicleSupportingInvolvements = new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
                //Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();


                ////if(HttpContext.Current.Session["InsuredDriverSupportingInvolvements"]==null)
                ////    reviewAndSubmitViewModel.PreviousPage = CurrentView.InsuredDriver;
                ////else
                ////    reviewAndSubmitViewModel.PreviousPage = CurrentView.SupportingInvolvementInsuredDriver;

                ////if (HttpContext.Current.Session["InsuredDriverPassengers"] != null)
                ////    reviewAndSubmitViewModel.PreviousPage = CurrentView.Passenger;



                //if (DictionaryThirdPartyDriverSupportingInvolvements != null)
                //{
                //    if (DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(CurrentVehicleID))
                //    {
                //        thirdPartyDriverSupportingInvolvements =
                //            DictionaryThirdPartyDriverSupportingInvolvements[CurrentVehicleID];
                //        //
                //        if (thirdPartyDriverSupportingInvolvements != null)
                //        {
                //            reviewAndSubmitViewModel.PreviousPage =
                //                CurrentView.SupportingInvolvementThirdPartyDriver;
                //        }
                //        else
                //        {
                //            reviewAndSubmitViewModel.PreviousPage = CurrentView.ThirdPartyDriver;
                //        }
                //    }
                //    else
                //    {
                //        reviewAndSubmitViewModel.PreviousPage = CurrentView.ThirdPartyDriver;
                //    }
                //}

                //DictionaryThirdPartyPassengers = (Dictionary<int, List<PassengerViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyPassengers"];

                //if (DictionaryThirdPartyPassengers != null)
                //{
                //    // Check if model already exists in List
                //    if (DictionaryThirdPartyPassengers.ContainsKey(CurrentVehicleID))
                //    {
                //        ThirdPartyPassengers = DictionaryThirdPartyPassengers[CurrentVehicleID];
                //    }

                //    if (ThirdPartyPassengers != null && ThirdPartyPassengers.Count() > 0)
                //        reviewAndSubmitViewModel.PreviousPage = CurrentView.Passenger;

                //}

                //DictionaryThirdPartyVehicleSupportingInvolvements = (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];

                //if (DictionaryThirdPartyVehicleSupportingInvolvements != null)
                //{
                //    if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(CurrentVehicleID))
                //    {
                //        DictionaryThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyVehicleSupportingInvolvements[CurrentVehicleID];
                //    }

                //    if (DictionaryThirdPartyPassengerSupportingInvolvements != null && DictionaryThirdPartyPassengerSupportingInvolvements.Count() > 0)
                //        reviewAndSubmitViewModel.PreviousPage = CurrentView.SupportingInvolvementThirdPartyPassenger;

                //}



                ReviewAndSubmitFoundInSession = true;
            }
            //
            if (!ReviewAndSubmitFoundInSession)
            {
                reviewAndSubmitViewModel.PreviousPage = PreviousPage;
                reviewAndSubmitViewModel.CurrentVehicle = CurrentVehicleID;
                reviewAndSubmitViewModel.NumberOfVehicles = NumberOfVehicles;
                reviewAndSubmitViewModel.VehicleID = CurrentVehicleID;
                //
            }

            var dataFeeds = new[] 
                {
                    new DataFeeds { DataFeed = "Keoghs Fraud Database", IsChecked = true, ReadOnly = true, Included="Included" }
                    //,new DataFeeds { DataFeed = "Companies House", IsChecked = true , Included="Included"},
                    ,new DataFeeds { DataFeed = "Tracesmart", IsChecked = false ,ReadOnly = false, Included="Additional Fee"}
                    //,new DataFeeds { DataFeed = "CUE", IsChecked = false , Included="Additional Fee" }
                };
            //
            reviewAndSubmitViewModel.DataFeeds = dataFeeds;
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                reviewAndSubmitViewModel.NavigationData.Data = NavigationData;

            reviewAndSubmitViewModel.NavigationData.CurrentForm = CurrentView.ReviewAndSubmit.ToString();
            reviewAndSubmitViewModel.NavigationData.Previous = Previous;
            // 
            return reviewAndSubmitViewModel;
        }
    }
}