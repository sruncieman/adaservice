﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        public static ThirdPartyVehicleViewModel PopulateThirdPartyVehicleViewModel(int CurrentVehicleID, CurrentView PreviousPage, string NavigationData = "", bool Previous = false)
        {
            bool VehicleFoundInSession = false;
            ThirdPartyVehicleViewModel thirdPartyVehicleViewModel = new ThirdPartyVehicleViewModel();
            Dictionary<int, ThirdPartyVehicleViewModel> DictionaryThirdPartyVehicleViewModels = new Dictionary<int, ThirdPartyVehicleViewModel>();


            if (HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] != null)
            {
                DictionaryThirdPartyVehicleViewModels = HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] as Dictionary<int, ThirdPartyVehicleViewModel>;

                if (DictionaryThirdPartyVehicleViewModels.ContainsKey(CurrentVehicleID))
                {
                    //
                    thirdPartyVehicleViewModel = DictionaryThirdPartyVehicleViewModels[CurrentVehicleID];

                    if (PreviousPage != CurrentView.Unknown)
                        thirdPartyVehicleViewModel.PreviousPage = PreviousPage;

                    VehicleFoundInSession = true;
                }
            }
            if (!VehicleFoundInSession)
            {
                thirdPartyVehicleViewModel.PreviousPage = PreviousPage;
                thirdPartyVehicleViewModel.CurrentVehicle = CurrentVehicleID;
                thirdPartyVehicleViewModel.NumberOfVehicles = NumberOfThirdPartyVehicles();
                thirdPartyVehicleViewModel.VehicleID = CurrentVehicleID;
                //
                //

            }

            thirdPartyVehicleViewModel.Vehicle.VehicleTypes = PopulateInsuredVehicleTypes();
            thirdPartyVehicleViewModel.VehicleAdditionalInfo.Colours = PopulateInsuredVehicleColours();
            thirdPartyVehicleViewModel.VehicleAdditionalInfo.Transmission = PopulateInsuredVehicleTransmissions();
            thirdPartyVehicleViewModel.VehicleAdditionalInfo.Fuel = PopulateFuels();
            thirdPartyVehicleViewModel.VehicleAdditionalInfo.TotalLossCapacity = PopulateInsuredVehicleTotalLossCapacity();

            thirdPartyVehicleViewModel.Vehicle.WasPolicyHolderTheDriver = false;

            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                thirdPartyVehicleViewModel.NavigationData.Data = NavigationData;
            //
            thirdPartyVehicleViewModel.NavigationData.CurrentForm = CurrentView.ThirdPartyVehicle.ToString();
            thirdPartyVehicleViewModel.NavigationData.Previous = Previous;
            //

            return thirdPartyVehicleViewModel;
        }
    }
}