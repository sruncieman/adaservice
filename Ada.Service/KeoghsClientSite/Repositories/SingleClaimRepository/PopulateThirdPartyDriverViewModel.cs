﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    partial class SingleClaimRepository
    {
        public static ThirdPartyDriverViewModel PopulateThirdPartyDriverViewModel(int CurrentVehicleID, CurrentView PreviousPage,
                                                                               string NavigationData = "", bool Previous = false, string NextPage = "")
        {
            ThirdPartyDriverViewModel thirdPartyDriverViewModel = new ThirdPartyDriverViewModel();

            bool DriverFoundInSession = false;

            Dictionary<int, ThirdPartyDriverViewModel> DictionaryThirdPartyDriverViewModels = new Dictionary<int, ThirdPartyDriverViewModel>();

            if (HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] != null)
            {
                DictionaryThirdPartyDriverViewModels = HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] as Dictionary<int, ThirdPartyDriverViewModel>;

                if (DictionaryThirdPartyDriverViewModels.ContainsKey(CurrentVehicleID))
                {
                    //
                    thirdPartyDriverViewModel = DictionaryThirdPartyDriverViewModels[CurrentVehicleID];
                    DriverFoundInSession = true;
                }
            }
            if (!DriverFoundInSession)
            {
                thirdPartyDriverViewModel.PreviousPage = PreviousPage;
                thirdPartyDriverViewModel.CurrentVehicle = CurrentVehicleID;
                thirdPartyDriverViewModel.NumberOfVehicles = NumberOfThirdPartyVehicles();
                thirdPartyDriverViewModel.VehicleID = CurrentVehicleID;
                //


            }
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                thirdPartyDriverViewModel.NavigationData.Data = NavigationData;

            thirdPartyDriverViewModel.NavigationData.CurrentForm = CurrentView.ThirdPartyDriver.ToString();
            thirdPartyDriverViewModel.NavigationData.Previous = Previous;
            thirdPartyDriverViewModel.NavigationData.NextPage = NextPage;

            return thirdPartyDriverViewModel;

        }
    }
}