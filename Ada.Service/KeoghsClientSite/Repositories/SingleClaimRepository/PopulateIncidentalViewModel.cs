﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {

        public static IncidentViewModel PopulateIncidentViewModel(string NavigationData, bool Previous = false)
        {

            IncidentViewModel incidentViewModel = new IncidentViewModel();

            if (HttpContext.Current.Session["incidentViewModel"] != null)
            {
                incidentViewModel = HttpContext.Current.Session["incidentViewModel"] as IncidentViewModel;
            }
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                incidentViewModel.NavigationData.Data = NavigationData;

            incidentViewModel.NavigationData.CurrentForm = CurrentView.Incident.ToString();
            incidentViewModel.NavigationData.Previous = Previous;
            //
            //Incident time hours
            List<SelectListItem> IncidentTimeHours = new List<SelectListItem>();

            IncidentTimeHours.Add(new SelectListItem() { Text = "--", Value = "--" });

            for (int i = 0; i < 24; i++)
                IncidentTimeHours.Add(new SelectListItem() { Text = i.ToString("D2"), Value = i.ToString("D2") });
            //
            incidentViewModel.IncidentTimeHours = IncidentTimeHours as IEnumerable<SelectListItem>;

            //Incident time minutes
            List<SelectListItem> IncidentTimeMinutes = new List<SelectListItem>();

            IncidentTimeMinutes.Add(new SelectListItem() { Text = "--", Value = "--" });

            for (int i = 0; i < 60; i++)
                IncidentTimeMinutes.Add(new SelectListItem() { Text = i.ToString("D2"), Value = i.ToString("D2") });
            //
            incidentViewModel.IncidentTimeMinutes = IncidentTimeMinutes as IEnumerable<SelectListItem>;

            // Number of vehicles
            IEnumerable<SelectListItem> NumberOfVehicles = new List<SelectListItem>{
               new SelectListItem(){Text="1",Value="0"},
               new SelectListItem(){Text="2",Value="1"},
               new SelectListItem(){Text="3",Value="2"}
               };
            incidentViewModel.NumberOfVehicles = NumberOfVehicles;

            //Number of witnesses
            IEnumerable<SelectListItem> NumberOfWitnesses = new List<SelectListItem>{
               new SelectListItem(){Text="1",Value="0"},
               new SelectListItem(){Text="2",Value="1"},
               new SelectListItem(){Text="3",Value="2"}
               };
            incidentViewModel.NumberOfWitnesses = NumberOfWitnesses;

            // Claim Code
            IEnumerable<SelectListItem> ClaimCode = new List<SelectListItem>{
               new SelectListItem(){Text="Insured damage only",Value="0"},
               new SelectListItem(){Text="TP damage only",Value="1"},
               new SelectListItem(){Text="Damage & PI",Value="2"}
               };
            incidentViewModel.ClaimCode = ClaimCode;

            //Liability
            IEnumerable<SelectListItem> Liability = new List<SelectListItem>{
               new SelectListItem(){Text="Yes",Value="1"},
               new SelectListItem(){Text="No",Value="0"}
               };

            incidentViewModel.Liability = Liability;

            //Liability Decision
            IEnumerable<SelectListItem> LiabilityDecision = new List<SelectListItem>(){
               new SelectListItem(){Text="Insured at fault",Value="0"},
               new SelectListItem(){Text="TP at fault",Value="1"},
               new SelectListItem(){Text="Split",Value="2"},
               new SelectListItem(){Text="Disputed",Value="3"},
               new SelectListItem(){Text="No Decision",Value="4"},
               new SelectListItem(){Text="Unknown",Value="5"}
               };

            incidentViewModel.LiabilityDecision = LiabilityDecision;

            //Moj Process Stage
            IEnumerable<SelectListItem> MojProcessStage = new List<SelectListItem>(){
               new SelectListItem(){Text="Stage 1",Value="0"},
               new SelectListItem(){Text="Stage 2",Value="1"},
               new SelectListItem(){Text="Stage 3",Value="2"},
               new SelectListItem(){Text="Out of process",Value="3"}
               };

            incidentViewModel.MojProcessStage = MojProcessStage;

            //Ambulance attend?
            IEnumerable<SelectListItem> AmbulanceAttend = new List<SelectListItem>{
               new SelectListItem(){Text="Yes",Value="0"},
               new SelectListItem(){Text="No",Value="1"},
               new SelectListItem(){Text="Unknown",Value="2"}
               };
            incidentViewModel.AmbulanceAttend = AmbulanceAttend;

            //Police Attend
            IEnumerable<SelectListItem> PoliceAttend = new List<SelectListItem>{
               new SelectListItem(){Text="Yes",Value="0"},
               new SelectListItem(){Text="No",Value="1"}
               };
            incidentViewModel.PoliceAttend = PoliceAttend;

            return incidentViewModel;
        }
    }
}