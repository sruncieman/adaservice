﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        public static InsuredVehicleViewModel PopulateInsuredVehicleViewModel(CurrentView PreviousPage = CurrentView.Unknown,
                                                                            string NavigationData = "",
                                                                            bool Previous = false)
        {
            InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();
            //bool InsuredVehicleFoundInSession = false;

            if (HttpContext.Current.Session["insuredVehicleViewModel"] != null)
            {
                insuredVehicleViewModel = HttpContext.Current.Session["insuredVehicleViewModel"] as InsuredVehicleViewModel;
                //InsuredVehicleFoundInSession = true;
            }
            else
            {

                insuredVehicleViewModel.PreviousPage = PreviousPage;
            }

            if (NavigationData != null)
            {

                if (NavigationData.Contains("Commercial"))
                {
                    insuredVehicleViewModel.Vehicle.PolicyCommercial = true;
                }
                else
                {
                    insuredVehicleViewModel.Vehicle.PolicyCommercial = false;
                }
            }


            if (PreviousPage == CurrentView.PolicyHolderCommercial)
            {
                insuredVehicleViewModel.Vehicle.PolicyCommercial = true;
            }

            insuredVehicleViewModel.PreviousPage = PreviousPage;
            StoreInsuredVehicleViewModel(insuredVehicleViewModel);
           

            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                insuredVehicleViewModel.NavigationData.Data = NavigationData;

            insuredVehicleViewModel.NavigationData.CurrentForm = CurrentView.InsuredVehicle.ToString();
            // Always set previous independent of session
            insuredVehicleViewModel.NavigationData.Previous = Previous;
            //
            //
            insuredVehicleViewModel.Vehicle.VehicleTypes = PopulateInsuredVehicleTypes();
            insuredVehicleViewModel.VehicleAdditionalInfo.Colours = PopulateInsuredVehicleColours();
            insuredVehicleViewModel.VehicleAdditionalInfo.Transmission = PopulateInsuredVehicleTransmissions();
            insuredVehicleViewModel.VehicleAdditionalInfo.Fuel = PopulateFuels();
            insuredVehicleViewModel.VehicleAdditionalInfo.TotalLossCapacity = PopulateInsuredVehicleTotalLossCapacity();

            if (insuredVehicleViewModel.Vehicle.PolicyDetailsKnown == false || insuredVehicleViewModel.Vehicle.PolicyDetailsKnown == null)
            {
                insuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver = false;
            }

            return insuredVehicleViewModel;
        }
    }
}