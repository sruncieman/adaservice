﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        public static SupportingInvolvementsViewModel PopulateSupportingInvolvementsViewModel(int NumSupportingInvolvements, int CurrSupportingInvolvement,
                                                                                              SupportingInvolvementsType supportingInvolvementsType,
                                                                                              int ParentID, int VehicleID = -1, string NavigationData = "",
                                                                                              bool Previous = false, string NextPage = "")
        {
            bool SuppInvolvementFoundInSession = false;
            SupportingInvolvementsViewModel supportingInvolvementsViewModel = new SupportingInvolvementsViewModel();

            //if (PopulateFromSession)
            //{
            if (supportingInvolvementsType == SupportingInvolvementsType.InsuredDriver)
            {
                List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                insuredDriverSupportingInvolvements = (List<SupportingInvolvementsViewModel>)HttpContext.Current.Session["InsuredDriverSupportingInvolvements"];

                if (insuredDriverSupportingInvolvements != null)
                {
                    // Check if number of involvements has increased
                    if (NumSupportingInvolvements > insuredDriverSupportingInvolvements.Count())
                    {
                        //add new model to end of list
                        supportingInvolvementsViewModel.SupportingInvolvementsType = supportingInvolvementsType;
                        supportingInvolvementsViewModel.ParentID = ParentID;
                        supportingInvolvementsViewModel.NumberSupportingInvolvements = NumSupportingInvolvements;
                        supportingInvolvementsViewModel.CurrentSupportingInvolvement = CurrSupportingInvolvement;
                        supportingInvolvementsViewModel.VehicleID = VehicleID;
                       

                        insuredDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        HttpContext.Current.Session["InsuredDriverSupportingInvolvements"] = insuredDriverSupportingInvolvements;

                        // Update collection counts
                        int count = 1;
                        foreach (var item in insuredDriverSupportingInvolvements)
                        {
                            item.NumberSupportingInvolvements = NumSupportingInvolvements;
                            item.CurrentSupportingInvolvement = count;
                            count++;
                        }
                    }

                    // Check if number of involvements has decreased
                    if (NumSupportingInvolvements > -1 && NumSupportingInvolvements < insuredDriverSupportingInvolvements.Count())
                    {
                        insuredDriverSupportingInvolvements.RemoveAll(x => x.CurrentSupportingInvolvement > NumSupportingInvolvements);

                        // Update collection counts
                        int count = 1;
                        foreach (var item in insuredDriverSupportingInvolvements)
                        {
                            item.NumberSupportingInvolvements = NumSupportingInvolvements;
                            item.CurrentSupportingInvolvement = count;
                            count++;
                        }
                    }

                    // subtract one from CurrSupportingInvolvement to account for zero based List index
                    if (CurrSupportingInvolvement - 1 < insuredDriverSupportingInvolvements.Count())
                    {
                        // supportingInvolvementsViewModel.SupportingInvolvementsType = supportingInvolvementsType;
                        supportingInvolvementsViewModel = insuredDriverSupportingInvolvements[CurrSupportingInvolvement - 1];
                        SuppInvolvementFoundInSession = true;
                    }
                }
            }
            else if (supportingInvolvementsType == SupportingInvolvementsType.InsuredPassenger)
            {

                Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                List<SupportingInvolvementsViewModel> PassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                //Get dictionary from session
                DictionaryPassengerSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"];

                if (DictionaryPassengerSupportingInvolvements != null && DictionaryPassengerSupportingInvolvements.Count() > 0)
                {
                    int numDictionaryPassengerSupportingInvolvements = 0;

                    int numDictionaryPassengers = DictionaryPassengerSupportingInvolvements.Count();

                    if (DictionaryPassengerSupportingInvolvements.ContainsKey(ParentID))
                    {
                        PassengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements[ParentID];
                        if(PassengerSupportingInvolvements.Count() > 0)
                            numDictionaryPassengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements[ParentID][0].NumberSupportingInvolvements;
                    }
                    else
                    {
                        numDictionaryPassengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements.FirstOrDefault().Value[0].NumberSupportingInvolvements;
                    }

                    //// Check if number of involvements has increased
                    if (NumSupportingInvolvements > numDictionaryPassengerSupportingInvolvements)
                    {
                        //    //add new model to end of list
                        supportingInvolvementsViewModel.SupportingInvolvementsType = supportingInvolvementsType;
                        supportingInvolvementsViewModel.ParentID = ParentID;
                        supportingInvolvementsViewModel.NumberSupportingInvolvements = NumSupportingInvolvements;
                        supportingInvolvementsViewModel.CurrentSupportingInvolvement = CurrSupportingInvolvement;
                        supportingInvolvementsViewModel.VehicleID = VehicleID;

                        PassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);

                        // Update collection counts
                        int count = 1;
                        foreach (var item in PassengerSupportingInvolvements)
                        {
                            item.NumberSupportingInvolvements = NumSupportingInvolvements;
                            item.CurrentSupportingInvolvement = count;
                            count++;
                        }
                    }

                    // Check if number of involvements has decreased
                    if (NumSupportingInvolvements > -1 && NumSupportingInvolvements < numDictionaryPassengerSupportingInvolvements)
                    {
                        PassengerSupportingInvolvements.RemoveAll(x => x.CurrentSupportingInvolvement > NumSupportingInvolvements);

                        // Update collection counts
                        int count = 1;
                        foreach (var item in PassengerSupportingInvolvements)
                        {
                            item.NumberSupportingInvolvements = NumSupportingInvolvements;
                            item.CurrentSupportingInvolvement = count;
                            count++;
                        }
                    }

                    HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = DictionaryPassengerSupportingInvolvements;

                    // Check if model already exists in List
                    if (DictionaryPassengerSupportingInvolvements.ContainsKey(ParentID))
                    {
                        PassengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements[ParentID];
                        if (CurrSupportingInvolvement - 1 < PassengerSupportingInvolvements.Count())
                        {
                            if (CurrSupportingInvolvement > 0)
                            {
                                supportingInvolvementsViewModel = PassengerSupportingInvolvements[CurrSupportingInvolvement - 1];
                                SuppInvolvementFoundInSession = true;
                            }
                        }
                    }

                }
            }
            else if (supportingInvolvementsType == SupportingInvolvementsType.ThirdPartyDriver)
            {
                Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyDriverSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                List<SupportingInvolvementsViewModel> ThirdPartyDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                DictionaryThirdPartyDriverSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"];

                if (DictionaryThirdPartyDriverSupportingInvolvements != null)
                {
                    int numDictionaryPassengerSupportingInvolvements = 0;

                    int numVehicles = DictionaryThirdPartyDriverSupportingInvolvements.Count();

                    if (numVehicles > 1 && DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(ParentID))
                    {
                        numDictionaryPassengerSupportingInvolvements = DictionaryThirdPartyDriverSupportingInvolvements[ParentID].FirstOrDefault().NumberSupportingInvolvements;
                    }
                    else
                    {
                        // check vehicle contains any supp involvements first
                        if (DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(ParentID))
                        {
                            var listSuppInv = DictionaryThirdPartyDriverSupportingInvolvements[ParentID].ToList();
                            if (listSuppInv.Count() > 0)
                                numDictionaryPassengerSupportingInvolvements =
                                    DictionaryThirdPartyDriverSupportingInvolvements.FirstOrDefault().Value[0].
                                        NumberSupportingInvolvements;
                        }
                    }
                    if (DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(ParentID))
                    {
                        ThirdPartyDriverSupportingInvolvements = DictionaryThirdPartyDriverSupportingInvolvements[ParentID];
                    }

                    if (ThirdPartyDriverSupportingInvolvements != null)
                    {

                        //// Check if number of involvements has increased
                        if (NumSupportingInvolvements > numDictionaryPassengerSupportingInvolvements)
                        {
                            //    //add new model to end of list
                            supportingInvolvementsViewModel.SupportingInvolvementsType = supportingInvolvementsType;
                            supportingInvolvementsViewModel.ParentID = ParentID;
                            supportingInvolvementsViewModel.NumberSupportingInvolvements = NumSupportingInvolvements;
                            supportingInvolvementsViewModel.CurrentSupportingInvolvement = CurrSupportingInvolvement;
                            supportingInvolvementsViewModel.VehicleID = VehicleID;


                            ThirdPartyDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);

                            HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] =
                                DictionaryThirdPartyDriverSupportingInvolvements;

                            // Update collection counts
                            int count = 1;
                            foreach (var item in ThirdPartyDriverSupportingInvolvements)
                            {
                                item.NumberSupportingInvolvements = NumSupportingInvolvements;
                                item.CurrentSupportingInvolvement = count;
                                count++;
                            }
                        }
                    }

                    // Check if number of involvements has decreased
                    if (NumSupportingInvolvements > -1 && NumSupportingInvolvements < numDictionaryPassengerSupportingInvolvements)
                    {
                        ThirdPartyDriverSupportingInvolvements.RemoveAll(x => x.CurrentSupportingInvolvement > NumSupportingInvolvements);

                        // Update collection counts
                        int count = 1;
                        foreach (var item in ThirdPartyDriverSupportingInvolvements)
                        {
                            item.NumberSupportingInvolvements = NumSupportingInvolvements;
                            item.CurrentSupportingInvolvement = count;
                            count++;
                        }
                    }
                    
                    // Check if model already exists in List
                    if (DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(ParentID))
                    {
                        ThirdPartyDriverSupportingInvolvements = DictionaryThirdPartyDriverSupportingInvolvements[ParentID];
                        if (ThirdPartyDriverSupportingInvolvements != null)
                        {
                            if (CurrSupportingInvolvement - 1 < ThirdPartyDriverSupportingInvolvements.Count())
                            {
                                if (CurrSupportingInvolvement > 0)
                                {
                                    supportingInvolvementsViewModel =
                                        ThirdPartyDriverSupportingInvolvements[CurrSupportingInvolvement - 1];
                                    SuppInvolvementFoundInSession = true;
                                }
                            }
                        }
                    }
                }
                //
            }
            else if (supportingInvolvementsType == SupportingInvolvementsType.ThirdPartyPassenger)
            {
                Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>> DictionaryThirdPartyVehicleSupportingInvolvements = new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
                Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                List<SupportingInvolvementsViewModel> ThirdPartyPassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
                
                DictionaryThirdPartyVehicleSupportingInvolvements = (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];
                
                if (DictionaryThirdPartyVehicleSupportingInvolvements != null)
                {
                    if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(VehicleID))
                    {
                        DictionaryThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyVehicleSupportingInvolvements[VehicleID];

                        if (DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(ParentID))
                        {
                            // Check if number of involvements has increased
                            ThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyPassengerSupportingInvolvements[ParentID];

                            int numThirdPartyPassengerDictionary = ThirdPartyPassengerSupportingInvolvements.Count();
                            int numThirdPartyPassengerSupportingInvolvements = 0;

                            if (numThirdPartyPassengerDictionary > 0)
                            {

                                if (numThirdPartyPassengerDictionary > 1)
                                {
                                    //numThirdPartyPassengerSupportingInvolvements =
                                    //    DictionaryThirdPartyPassengerSupportingInvolvements.Skip(ParentID - 1).
                                    //        FirstOrDefault().Value[0].NumberSupportingInvolvements;
                                   
                                    //numThirdPartyPassengerSupportingInvolvements =
                                    //    DictionaryThirdPartyPassengerSupportingInvolvements.FirstOrDefault().Value[
                                    //        ParentID - 1].NumberSupportingInvolvements;

                                    var involvementsViewModel = ThirdPartyPassengerSupportingInvolvements.FirstOrDefault();
                                    if (involvementsViewModel != null)
                                        numThirdPartyPassengerSupportingInvolvements =
                                            involvementsViewModel.
                                                NumberSupportingInvolvements;
                                }
                                else
                                    numThirdPartyPassengerSupportingInvolvements =
                                        DictionaryThirdPartyPassengerSupportingInvolvements.FirstOrDefault().Value[0].
                                            NumberSupportingInvolvements;
                            }

                            // Check if number of involvements has increased
                            if (NumSupportingInvolvements > numThirdPartyPassengerSupportingInvolvements)
                            {
                                //add new model to end of list
                                supportingInvolvementsViewModel.SupportingInvolvementsType = supportingInvolvementsType;
                                supportingInvolvementsViewModel.ParentID = ParentID;
                                supportingInvolvementsViewModel.NumberSupportingInvolvements = NumSupportingInvolvements;
                                supportingInvolvementsViewModel.CurrentSupportingInvolvement = CurrSupportingInvolvement;
                                supportingInvolvementsViewModel.VehicleID = VehicleID;

                                ThirdPartyPassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);

                                //HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;

                                // Update collection counts
                                int count = 1;
                                foreach (var item in ThirdPartyPassengerSupportingInvolvements)
                                {
                                    item.NumberSupportingInvolvements = NumSupportingInvolvements;
                                    item.CurrentSupportingInvolvement = count;
                                    count++;
                                }

                            }

                            // Check if number of involvements has decreased
                            if (NumSupportingInvolvements > -1 && NumSupportingInvolvements < numThirdPartyPassengerSupportingInvolvements)
                            {
                                ThirdPartyPassengerSupportingInvolvements.RemoveAll(x => x.CurrentSupportingInvolvement > NumSupportingInvolvements);

                                // Update collection counts
                                int count = 1;
                                foreach (var item in ThirdPartyPassengerSupportingInvolvements)
                                {
                                    item.NumberSupportingInvolvements = NumSupportingInvolvements;
                                    item.CurrentSupportingInvolvement = count;
                                    count++;
                                }
                            }

                            HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;

                        }
                    }
                    
                    
                    // Check if model already exists in List
                    if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(VehicleID))
                    {
                        DictionaryThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyVehicleSupportingInvolvements[VehicleID];

                        if (DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(ParentID))
                        {
                            ThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyPassengerSupportingInvolvements[ParentID];

                            if (CurrSupportingInvolvement - 1 < ThirdPartyPassengerSupportingInvolvements.Count())
                            {
                                supportingInvolvementsViewModel = ThirdPartyPassengerSupportingInvolvements[CurrSupportingInvolvement - 1];
                                SuppInvolvementFoundInSession = true;
                            }
                        }
                    }



                }
            }
            //
            if (!SuppInvolvementFoundInSession)
            {
                supportingInvolvementsViewModel.SupportingInvolvementsType = supportingInvolvementsType;
                supportingInvolvementsViewModel.ParentID = ParentID;
                supportingInvolvementsViewModel.NumberSupportingInvolvements = NumSupportingInvolvements;
                supportingInvolvementsViewModel.CurrentSupportingInvolvement = CurrSupportingInvolvement;
                supportingInvolvementsViewModel.VehicleID = VehicleID;
            }
            //else
            //{
            //    supportingInvolvementsViewModel.NumberSupportingInvolvements = NumSupportingInvolvements;

            //}

            // Populate lookups
            supportingInvolvementsViewModel.OrganisationInvolvement.OrganisationInvolvements = PopulateOrganisationInvolvements();
            //supportingInvolvementsViewModel.Address.AddressStatus = PopulateAddressStatuses();
            supportingInvolvementsViewModel.Vehicle.VehicleTypes = PopulateInsuredVehicleTypes();
            supportingInvolvementsViewModel.Vehicle.WasPolicyHolderTheDriver = false;
            supportingInvolvementsViewModel.VehicleAdditionalInfo.Colours = PopulateInsuredVehicleColours();
            supportingInvolvementsViewModel.VehicleAdditionalInfo.Transmission = PopulateInsuredVehicleTransmissions();
            supportingInvolvementsViewModel.VehicleAdditionalInfo.Fuel = PopulateFuels();
            supportingInvolvementsViewModel.VehicleAdditionalInfo.TotalLossCapacity = PopulateInsuredVehicleTotalLossCapacity();



            switch (supportingInvolvementsViewModel.SupportingInvolvementsType)
            {
                case SupportingInvolvementsType.InsuredDriver:
                    supportingInvolvementsViewModel.NavigationData.CurrentForm = CurrentView.SupportingInvolvementInsuredDriver.ToString();
                    break;
                case SupportingInvolvementsType.InsuredPassenger:
                    supportingInvolvementsViewModel.NavigationData.CurrentForm = CurrentView.SupportingInvolvementInsuredPassenger.ToString();
                    break;
                case SupportingInvolvementsType.ThirdPartyDriver:
                    supportingInvolvementsViewModel.NavigationData.CurrentForm = CurrentView.SupportingInvolvementThirdPartyDriver.ToString();
                    break;
                case SupportingInvolvementsType.ThirdPartyPassenger:
                    supportingInvolvementsViewModel.NavigationData.CurrentForm = CurrentView.SupportingInvolvementThirdPartyPassenger.ToString();
                    break;

            }

            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                supportingInvolvementsViewModel.NavigationData.Data = NavigationData;

            supportingInvolvementsViewModel.NavigationData.Previous = Previous;
            supportingInvolvementsViewModel.NavigationData.NextPage = NextPage;

            return supportingInvolvementsViewModel;

        }
    }
}