﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        #region populate lookups

        internal static string GetWitnessInvolvementTypeFromID(int? InvolvementTypeID)
        {
            string sReturn = "";

            if (InvolvementTypeID == 0)
                sReturn = "Independent Witness";
            else if (InvolvementTypeID == 1)
                sReturn = "Third Party Witness";
            else if (InvolvementTypeID == 2)
                sReturn = "Insured Witness";

            return sReturn;
        }

        internal static string GetPolicyTypeFromID(int? PolicyTypeID)
        {

            if (PolicyTypeID == 0)
                return "Commercial";
            else if (PolicyTypeID == 1)
                return "Personal";
            else
                return "Unknown";           

        }

        internal static IEnumerable<SelectListItem> PopulateAddressStatuses()
        {
            // Address Status
            IEnumerable<SelectListItem> AddressStatuses = new List<SelectListItem>{
               new SelectListItem(){Text="Previous Address",Value="1"},
               new SelectListItem(){Text="Linked Address",Value="2"},
               new SelectListItem(){Text="Current Address",Value="3"},
               new SelectListItem(){Text="Registered Address",Value="4"},
               new SelectListItem(){Text="Trading Address",Value="5"},
               new SelectListItem(){Text="Storage Address",Value="6"}
               };
            return AddressStatuses;
        }

        internal static IEnumerable<SelectListItem> PopulateBankAccountUsedFors()
        {
            // Bank Account : Account Used For
            IEnumerable<SelectListItem> AccountUsedFors = new List<SelectListItem>{
               new SelectListItem(){Text="Unknown",Value="0"},
               new SelectListItem(){Text="Deposit",Value="1"},
               new SelectListItem(){Text="Direct Debit",Value="2"},
               new SelectListItem(){Text="Single Payment",Value="3"}
               };
            return AccountUsedFors;
        }

        internal static IEnumerable<SelectListItem> PopulatepaymentCardTypes()
        {
            // Card Types
            IEnumerable<SelectListItem> CardTypes = new List<SelectListItem>{
               new SelectListItem(){Text="Credit Card",Value="0"},
               new SelectListItem(){Text="Debit card",Value="1"}
               };

            return CardTypes;
        }

        internal static IEnumerable<SelectListItem> PopulateInsuredVehicleColours()
        {
            IEnumerable<SelectListItem> Colours = new List<SelectListItem>{
            new SelectListItem(){Text="Unknown",Value="0"},
            new SelectListItem(){Text="Beige",Value="1"},
            new SelectListItem(){Text="Black",Value="2"},
            new SelectListItem(){Text="Blue",Value="3"},
            new SelectListItem(){Text="Bronze",Value="4"},
            new SelectListItem(){Text="Brown",Value="5"},
            new SelectListItem(){Text="Cream",Value="6"},
            new SelectListItem(){Text="Gold",Value="7"},
            new SelectListItem(){Text="Graphite",Value="8"},
            new SelectListItem(){Text="Green",Value="9"},
            new SelectListItem(){Text="Grey",Value="10"},
            new SelectListItem(){Text="Lilac",Value="11"},
            new SelectListItem(){Text="Maroon",Value="12"},
            new SelectListItem(){Text="Mauve",Value="13"},
            new SelectListItem(){Text="Orange",Value="14"},
            new SelectListItem(){Text="Pink",Value="15"},
            new SelectListItem(){Text="Purple",Value="16"},
            new SelectListItem(){Text="Red",Value="17"},
            new SelectListItem(){Text="Silver",Value="18"},
            new SelectListItem(){Text="Turquoise",Value="19"},
            new SelectListItem(){Text="White",Value="20"},
            new SelectListItem(){Text="Yellow",Value="21"}
               };
            return Colours;
        }

        internal static IEnumerable<SelectListItem> PopulatePaymentCardUsedFors()
        {
            // Payment Card : Card used for
            IEnumerable<SelectListItem> CardUsedFors = new List<SelectListItem>{
               new SelectListItem(){Text="Credit Card",Value="0"},
               new SelectListItem(){Text="Debit card",Value="1"}
               };
            return CardUsedFors;
        }

        internal static IEnumerable<SelectListItem> PopulateFuels()
        {
            // Bank Account : Account Used For
            IEnumerable<SelectListItem> Fuels = new List<SelectListItem>{
               new SelectListItem(){Text="Petrol",Value="1"},
               new SelectListItem(){Text="Diesel",Value="2"},
               new SelectListItem(){Text="Electric",Value="3"},
               new SelectListItem(){Text="Hybrid",Value="4"},
               new SelectListItem(){Text="LPG",Value="5"}
               };
            return Fuels;
        }

        internal static IEnumerable<SelectListItem> PopulatePersonGenders()
        {
            IEnumerable<SelectListItem> Genders = new List<SelectListItem>{
               new SelectListItem(){Text="Male",Value="1"},
               new SelectListItem(){Text="Female",Value="2"}
               };
            return Genders;
        }

        internal static IEnumerable<SelectListItem> PopulateNationalities()
        {
            // Person : nationality
            IEnumerable<SelectListItem> Nationalities = new List<SelectListItem>{
                new SelectListItem() {Text=" British",Value=" British"},
                new SelectListItem() {Text="------------",Value="------------"},
                new SelectListItem() {Text="Afghan",Value="Afghan"},
                new SelectListItem() {Text=" Albanian",Value=" Albanian"},
                new SelectListItem() {Text=" Algerian",Value=" Algerian"},
                new SelectListItem() {Text=" American",Value=" American"},
                new SelectListItem() {Text=" Andorran",Value=" Andorran"},
                new SelectListItem() {Text=" Angolan",Value=" Angolan"},
                new SelectListItem() {Text=" Antiguans",Value=" Antiguans"},
                new SelectListItem() {Text=" Argentinean",Value=" Argentinean"},
                new SelectListItem() {Text=" Armenian",Value=" Armenian"},
                new SelectListItem() {Text=" Australian",Value=" Australian"},
                new SelectListItem() {Text=" Austrian",Value=" Austrian"},
                new SelectListItem() {Text=" Azerbaijani",Value=" Azerbaijani"},
                new SelectListItem() {Text=" Bahamian",Value=" Bahamian"},
                new SelectListItem() {Text=" Bahraini",Value=" Bahraini"},
                new SelectListItem() {Text=" Bangladeshi",Value=" Bangladeshi"},
                new SelectListItem() {Text=" Barbadian",Value=" Barbadian"},
                new SelectListItem() {Text=" Barbudans",Value=" Barbudans"},
                new SelectListItem() {Text=" Batswana",Value=" Batswana"},
                new SelectListItem() {Text=" Belarusian",Value=" Belarusian"},
                new SelectListItem() {Text=" Belgian",Value=" Belgian"},
                new SelectListItem() {Text=" Belizean",Value=" Belizean"},
                new SelectListItem() {Text=" Beninese",Value=" Beninese"},
                new SelectListItem() {Text=" Bhutanese",Value=" Bhutanese"},
                new SelectListItem() {Text=" Bolivian",Value=" Bolivian"},
                new SelectListItem() {Text=" Bosnian",Value=" Bosnian"},
                new SelectListItem() {Text=" Brazilian",Value=" Brazilian"},
                new SelectListItem() {Text=" British",Value=" British"},
                new SelectListItem() {Text=" Bruneian",Value=" Bruneian"},
                new SelectListItem() {Text=" Bulgarian",Value=" Bulgarian"},
                new SelectListItem() {Text=" Burkinabe",Value=" Burkinabe"},
                new SelectListItem() {Text=" Burmese",Value=" Burmese"},
                new SelectListItem() {Text=" Burundian",Value=" Burundian"},
                new SelectListItem() {Text=" Cambodian",Value=" Cambodian"},
                new SelectListItem() {Text=" Cameroonian",Value=" Cameroonian"},
                new SelectListItem() {Text=" Canadian",Value=" Canadian"},
                new SelectListItem() {Text=" Cape Verdean",Value=" Cape Verdean"},
                new SelectListItem() {Text=" Central African",Value=" Central African"},
                new SelectListItem() {Text=" Chadian",Value=" Chadian"},
                new SelectListItem() {Text=" Chilean",Value=" Chilean"},
                new SelectListItem() {Text=" Chinese",Value=" Chinese"},
                new SelectListItem() {Text=" Colombian",Value=" Colombian"},
                new SelectListItem() {Text=" Comoran",Value=" Comoran"},
                new SelectListItem() {Text=" Congolese",Value=" Congolese"},
                new SelectListItem() {Text=" Costa Rican",Value=" Costa Rican"},
                new SelectListItem() {Text=" Croatian",Value=" Croatian"},
                new SelectListItem() {Text=" Cuban",Value=" Cuban"},
                new SelectListItem() {Text=" Cypriot",Value=" Cypriot"},
                new SelectListItem() {Text=" Czech",Value=" Czech"},
                new SelectListItem() {Text=" Danish",Value=" Danish"},
                new SelectListItem() {Text=" Djibouti",Value=" Djibouti"},
                new SelectListItem() {Text=" Dominican",Value=" Dominican"},
                new SelectListItem() {Text=" Dutch",Value=" Dutch"},
                new SelectListItem() {Text=" East Timorese",Value=" East Timorese"},
                new SelectListItem() {Text=" Ecuadorean",Value=" Ecuadorean"},
                new SelectListItem() {Text=" Egyptian",Value=" Egyptian"},
                new SelectListItem() {Text=" Emirian",Value=" Emirian"},
                new SelectListItem() {Text=" Equatorial Guinean",Value=" Equatorial Guinean"},
                new SelectListItem() {Text=" Eritrean",Value=" Eritrean"},
                new SelectListItem() {Text=" Estonian",Value=" Estonian"},
                new SelectListItem() {Text=" Ethiopian",Value=" Ethiopian"},
                new SelectListItem() {Text=" Fijian",Value=" Fijian"},
                new SelectListItem() {Text=" Filipino",Value=" Filipino"},
                new SelectListItem() {Text=" Finnish",Value=" Finnish"},
                new SelectListItem() {Text=" French",Value=" French"},
                new SelectListItem() {Text=" Gabonese",Value=" Gabonese"},
                new SelectListItem() {Text=" Gambian",Value=" Gambian"},
                new SelectListItem() {Text=" Georgian",Value=" Georgian"},
                new SelectListItem() {Text=" German",Value=" German"},
                new SelectListItem() {Text=" Ghanaian",Value=" Ghanaian"},
                new SelectListItem() {Text=" Greek",Value=" Greek"},
                new SelectListItem() {Text=" Grenadian",Value=" Grenadian"},
                new SelectListItem() {Text=" Guatemalan",Value=" Guatemalan"},
                new SelectListItem() {Text=" Guinea-Bissauan",Value=" Guinea-Bissauan"},
                new SelectListItem() {Text=" Guinean",Value=" Guinean"},
                new SelectListItem() {Text=" Guyanese",Value=" Guyanese"},
                new SelectListItem() {Text=" Haitian",Value=" Haitian"},
                new SelectListItem() {Text=" Herzegovinian",Value=" Herzegovinian"},
                new SelectListItem() {Text=" Honduran",Value=" Honduran"},
                new SelectListItem() {Text=" Hungarian",Value=" Hungarian"},
                new SelectListItem() {Text=" I-Kiribati",Value=" I-Kiribati"},
                new SelectListItem() {Text=" Icelander",Value=" Icelander"},
                new SelectListItem() {Text=" Indian",Value=" Indian"},
                new SelectListItem() {Text=" Indonesian",Value=" Indonesian"},
                new SelectListItem() {Text=" Iranian",Value=" Iranian"},
                new SelectListItem() {Text=" Iraqi",Value=" Iraqi"},
                new SelectListItem() {Text=" Irish",Value=" Irish"},
                new SelectListItem() {Text=" Israeli",Value=" Israeli"},
                new SelectListItem() {Text=" Italian",Value=" Italian"},
                new SelectListItem() {Text=" Ivorian",Value=" Ivorian"},
                new SelectListItem() {Text=" Jamaican",Value=" Jamaican"},
                new SelectListItem() {Text=" Japanese",Value=" Japanese"},
                new SelectListItem() {Text=" Jordanian",Value=" Jordanian"},
                new SelectListItem() {Text=" Kazakhstani",Value=" Kazakhstani"},
                new SelectListItem() {Text=" Kenyan",Value=" Kenyan"},
                new SelectListItem() {Text=" Kittian and Nevisian",Value=" Kittian and Nevisian"},
                new SelectListItem() {Text=" Kuwaiti",Value=" Kuwaiti"},
                new SelectListItem() {Text=" Kyrgyz",Value=" Kyrgyz"},
                new SelectListItem() {Text=" Laotian",Value=" Laotian"},
                new SelectListItem() {Text=" Latvian",Value=" Latvian"},
                new SelectListItem() {Text=" Lebanese",Value=" Lebanese"},
                new SelectListItem() {Text=" Liberian",Value=" Liberian"},
                new SelectListItem() {Text=" Libyan",Value=" Libyan"},
                new SelectListItem() {Text=" Liechtensteiner",Value=" Liechtensteiner"},
                new SelectListItem() {Text=" Lithuanian",Value=" Lithuanian"},
                new SelectListItem() {Text=" Luxembourger",Value=" Luxembourger"},
                new SelectListItem() {Text=" Macedonian",Value=" Macedonian"},
                new SelectListItem() {Text=" Malagasy",Value=" Malagasy"},
                new SelectListItem() {Text=" Malawian",Value=" Malawian"},
                new SelectListItem() {Text=" Malaysian",Value=" Malaysian"},
                new SelectListItem() {Text=" Maldivan",Value=" Maldivan"},
                new SelectListItem() {Text=" Malian",Value=" Malian"},
                new SelectListItem() {Text=" Maltese",Value=" Maltese"},
                new SelectListItem() {Text=" Marshallese",Value=" Marshallese"},
                new SelectListItem() {Text=" Mauritanian",Value=" Mauritanian"},
                new SelectListItem() {Text=" Mauritian",Value=" Mauritian"},
                new SelectListItem() {Text=" Mexican",Value=" Mexican"},
                new SelectListItem() {Text=" Micronesian",Value=" Micronesian"},
                new SelectListItem() {Text=" Moldovan",Value=" Moldovan"},
                new SelectListItem() {Text=" Monacan",Value=" Monacan"},
                new SelectListItem() {Text=" Mongolian",Value=" Mongolian"},
                new SelectListItem() {Text=" Moroccan",Value=" Moroccan"},
                new SelectListItem() {Text=" Mosotho",Value=" Mosotho"},
                new SelectListItem() {Text=" Motswana",Value=" Motswana"},
                new SelectListItem() {Text=" Mozambican",Value=" Mozambican"},
                new SelectListItem() {Text=" Namibian",Value=" Namibian"},
                new SelectListItem() {Text=" Nauruan",Value=" Nauruan"},
                new SelectListItem() {Text=" Nepalese",Value=" Nepalese"},
                new SelectListItem() {Text=" New Zealander",Value=" New Zealander"},
                new SelectListItem() {Text=" Nicaraguan",Value=" Nicaraguan"},
                new SelectListItem() {Text=" Nigerian",Value=" Nigerian"},
                new SelectListItem() {Text=" Nigerien",Value=" Nigerien"},
                new SelectListItem() {Text=" North Korean",Value=" North Korean"},
                new SelectListItem() {Text=" Northern Irish",Value=" Northern Irish"},
                new SelectListItem() {Text=" Norwegian",Value=" Norwegian"},
                new SelectListItem() {Text=" Omani",Value=" Omani"},
                new SelectListItem() {Text=" Pakistani",Value=" Pakistani"},
                new SelectListItem() {Text=" Palauan",Value=" Palauan"},
                new SelectListItem() {Text=" Panamanian",Value=" Panamanian"},
                new SelectListItem() {Text=" Papua New Guinean",Value=" Papua New Guinean"},
                new SelectListItem() {Text=" Paraguayan",Value=" Paraguayan"},
                new SelectListItem() {Text=" Peruvian",Value=" Peruvian"},
                new SelectListItem() {Text=" Polish",Value=" Polish"},
                new SelectListItem() {Text=" Portuguese",Value=" Portuguese"},
                new SelectListItem() {Text=" Qatari",Value=" Qatari"},
                new SelectListItem() {Text=" Romanian",Value=" Romanian"},
                new SelectListItem() {Text=" Russian",Value=" Russian"},
                new SelectListItem() {Text=" Rwandan",Value=" Rwandan"},
                new SelectListItem() {Text=" Saint Lucian",Value=" Saint Lucian"},
                new SelectListItem() {Text=" Salvadoran",Value=" Salvadoran"},
                new SelectListItem() {Text=" Samoan",Value=" Samoan"},
                new SelectListItem() {Text=" San Marinese",Value=" San Marinese"},
                new SelectListItem() {Text=" Sao Tomean",Value=" Sao Tomean"},
                new SelectListItem() {Text=" Saudi",Value=" Saudi"},
                new SelectListItem() {Text=" Scottish",Value=" Scottish"},
                new SelectListItem() {Text=" Senegalese",Value=" Senegalese"},
                new SelectListItem() {Text=" Serbian",Value=" Serbian"},
                new SelectListItem() {Text=" Seychellois",Value=" Seychellois"},
                new SelectListItem() {Text=" Sierra Leonean",Value=" Sierra Leonean"},
                new SelectListItem() {Text=" Singaporean",Value=" Singaporean"},
                new SelectListItem() {Text=" Slovakian",Value=" Slovakian"},
                new SelectListItem() {Text=" Slovenian",Value=" Slovenian"},
                new SelectListItem() {Text=" Solomon Islander",Value=" Solomon Islander"},
                new SelectListItem() {Text=" Somali",Value=" Somali"},
                new SelectListItem() {Text=" South African",Value=" South African"},
                new SelectListItem() {Text=" South Korean",Value=" South Korean"},
                new SelectListItem() {Text=" Spanish",Value=" Spanish"},
                new SelectListItem() {Text=" Sri Lankan",Value=" Sri Lankan"},
                new SelectListItem() {Text=" Sudanese",Value=" Sudanese"},
                new SelectListItem() {Text=" Surinamer",Value=" Surinamer"},
                new SelectListItem() {Text=" Swazi",Value=" Swazi"},
                new SelectListItem() {Text=" Swedish",Value=" Swedish"},
                new SelectListItem() {Text=" Swiss",Value=" Swiss"},
                new SelectListItem() {Text=" Syrian",Value=" Syrian"},
                new SelectListItem() {Text=" Taiwanese",Value=" Taiwanese"},
                new SelectListItem() {Text=" Tajik",Value=" Tajik"},
                new SelectListItem() {Text=" Tanzanian",Value=" Tanzanian"},
                new SelectListItem() {Text=" Thai",Value=" Thai"},
                new SelectListItem() {Text=" Togolese",Value=" Togolese"},
                new SelectListItem() {Text=" Tongan",Value=" Tongan"},
                new SelectListItem() {Text=" Trinidadian or Tobagonian",Value=" Trinidadian or Tobagonian"},
                new SelectListItem() {Text=" Tunisian",Value=" Tunisian"},
                new SelectListItem() {Text=" Turkish",Value=" Turkish"},
                new SelectListItem() {Text=" Tuvaluan",Value=" Tuvaluan"},
                new SelectListItem() {Text=" Ugandan",Value=" Ugandan"},
                new SelectListItem() {Text=" Ukrainian",Value=" Ukrainian"},
                new SelectListItem() {Text=" Uruguayan",Value=" Uruguayan"},
                new SelectListItem() {Text=" Uzbekistani",Value=" Uzbekistani"},
                new SelectListItem() {Text=" Venezuelan",Value=" Venezuelan"},
                new SelectListItem() {Text=" Vietnamese",Value=" Vietnamese"},
                new SelectListItem() {Text=" Welsh",Value=" Welsh"},
                new SelectListItem() {Text=" Yemenite",Value=" Yemenite"},
                new SelectListItem() {Text=" Zambian",Value=" Zambian"},
                new SelectListItem() {Text=" Zimbabwean",Value=" Zimbabwean"},
    
            };
            return Nationalities;

        }

        internal static IEnumerable<SelectListItem> PopulatePersonSalutations()
        {
            IEnumerable<SelectListItem> Salutations = new List<SelectListItem>{
               new SelectListItem(){Text="Mr",Value="1"},
               new SelectListItem(){Text="Mrs",Value="2"},
               new SelectListItem(){Text="Ms",Value="3"},
               new SelectListItem(){Text="Miss",Value="4"},
               new SelectListItem(){Text="Master",Value="5"},
               new SelectListItem(){Text="Dr",Value="6"},
               new SelectListItem(){Text="Professor",Value="7"},
               new SelectListItem(){Text="Reverend",Value="8"},
               new SelectListItem(){Text="Sir",Value="9"},
               new SelectListItem(){Text="Lord",Value="10"},
               new SelectListItem(){Text="Baroness",Value="11"},
               new SelectListItem(){Text="Right honourable",Value="12"}
               };
            return Salutations;
        }

        internal static IEnumerable<SelectListItem> PopulateInsuredVehicleTransmissions()
        {
            // Person : nationality
            IEnumerable<SelectListItem> Transmissions = new List<SelectListItem>{
               new SelectListItem(){Text="Unknown",Value="0"},
               new SelectListItem(){Text="Manual",Value="1"},
               new SelectListItem(){Text="Automatic",Value="2"}
               };
            return Transmissions;
        }

        internal static IEnumerable<SelectListItem> PopulateInsuredVehicleTotalLossCapacity()
        {
            // Person : nationality
            IEnumerable<SelectListItem> CategoryOfLoss = new List<SelectListItem>{
               new SelectListItem(){Text="Unknown",Value="0"},
               new SelectListItem(){Text="CatA",Value="1"},
               new SelectListItem(){Text="CatB",Value="2"},
               new SelectListItem(){Text="CatC",Value="3"},
               new SelectListItem(){Text="CatD",Value="4"}

               };
            return CategoryOfLoss;
        }

        internal static IEnumerable<SelectListItem> PopulateInsuredVehicleTypes()
        {
            // Person : nationality
            IEnumerable<SelectListItem> VehicleTypes = new List<SelectListItem>{
               new SelectListItem(){Text="Bicycle",Value="1"},
               new SelectListItem(){Text="Car",Value="2"},
               new SelectListItem(){Text="Coach",Value="3"},
               new SelectListItem(){Text="Lorry",Value="4"},
               new SelectListItem(){Text="Minibus",Value="5"},
               new SelectListItem(){Text="Motorcycle",Value="6"},
               new SelectListItem(){Text="Pickup",Value="7"},
               new SelectListItem(){Text="Van",Value="8"}
               };
            return VehicleTypes;
        }

        internal static IEnumerable<SelectListItem> PopulateOrganisationInvolvements()
        {
            // Person : nationality
            IEnumerable<SelectListItem> OrganisationInvolvements = new List<SelectListItem>{
               new SelectListItem(){Text="Unknown",Value="0"},
               new SelectListItem(){Text="Accident Management",Value="3"},
               new SelectListItem(){Text="Credit Hire",Value="4"},
               new SelectListItem(){Text="Solicitor",Value="5"},
               new SelectListItem(){Text="Vehicle Engineer",Value="6"},
               new SelectListItem(){Text="Recovery",Value="7"},
               new SelectListItem(){Text="Storage",Value="8"},
               new SelectListItem(){Text="Broker",Value="9"},
               new SelectListItem(){Text="Medical Examiner",Value="10"}
               };
            return OrganisationInvolvements;
        }

        internal static IEnumerable<SelectListItem> PopulateWitnessInvolvementType()
        {
            // Person : nationality
            IEnumerable<SelectListItem> WitnessInvolvementTypes = new List<SelectListItem>{
               new SelectListItem(){Text="Independent Witness",Value="0"},
               new SelectListItem(){Text="Third Party Witness",Value="1"},
               new SelectListItem(){Text="Insured Witness",Value="2"}
               };
            return WitnessInvolvementTypes;
        }

        #endregion
    }
}