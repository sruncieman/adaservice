﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        public static PolicyViewModel PopulatePolicyViewModel(string NavigationData, bool Previous = false)
        {
            PolicyViewModel policyViewModel = new PolicyViewModel();

            if (HttpContext.Current.Session["policyViewModel"] != null)
            {
                policyViewModel = HttpContext.Current.Session["policyViewModel"] as PolicyViewModel;
            }
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                policyViewModel.NavigationData.Data = NavigationData;

            policyViewModel.NavigationData.CurrentForm = CurrentView.Policy.ToString();
            policyViewModel.NavigationData.Previous = Previous;

            IEnumerable<SelectListItem> PolicyTypes = new List<SelectListItem>{
               new SelectListItem(){Text="Commercial",Value="0"},
               new SelectListItem(){Text="Personal",Value="1"},
               new SelectListItem(){Text="Unknown",Value="2"}
               };
            policyViewModel.PolicyTypes = PolicyTypes;

            IEnumerable<SelectListItem> CoverTypes = new List<SelectListItem>{
               new SelectListItem(){Text="Comprehensive",Value="0"},
               new SelectListItem(){Text="Third Party Only",Value="1"},
               new SelectListItem(){Text="Third Party, Fire and Theft",Value="2"}
               };
            policyViewModel.CoverTypes = CoverTypes;

            return policyViewModel;
        }
    }
}