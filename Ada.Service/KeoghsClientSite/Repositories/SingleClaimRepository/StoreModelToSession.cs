﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        #region store view models
        //
        //
        public static void StoreClaimViewModel(ClaimViewModel claimViewModel)
        {
            HttpContext.Current.Session["claimViewModel"] = claimViewModel;
        }
        //
        //
        public static void StoreIncidentViewModel(IncidentViewModel incidentViewModel)
        {
            HttpContext.Current.Session["incidentViewModel"] = incidentViewModel;
        }
        //
        //
        public static void StorePolicyViewModel(PolicyViewModel policyViewModel)
        {
            HttpContext.Current.Session["policyViewModel"] = policyViewModel;
        }
        //
        //
        public static void StorePolicyHolderViewModel(PolicyHolderPersonalViewModel policyHolderPersonalViewModel)
        {
            HttpContext.Current.Session["policyHolderPersonalViewModel"] = policyHolderPersonalViewModel;
        }
        //
        //
        public static void StorePolicyHolderCommercialViewModel(PolicyHolderCommercialViewModel policyHolderCommercialViewModel)
        {
            HttpContext.Current.Session["policyHolderCommercialViewModel"] = policyHolderCommercialViewModel;
        }
        //
        //
        public static void StoreInsuredVehicleViewModel(InsuredVehicleViewModel insuredVehicleViewModel)
        {
            HttpContext.Current.Session["insuredVehicleViewModel"] = insuredVehicleViewModel;
        }
        //
        //
        public static void StoreInsuredDriverViewModel(InsuredDriverViewModel insuredDriverViewModel)
        {
            HttpContext.Current.Session["insuredDriverViewModel"] = insuredDriverViewModel;
        }
        //
        //
        public static void StoreSupportingInvolvementsViewModel(SupportingInvolvementsViewModel supportingInvolvementsViewModel,
                                                                SupportingInvolvementsType supportingInvolvementsType,
                                                                int PassengerID = -1,
                                                                int VehicleID = -1)
        {
            if (supportingInvolvementsType == SupportingInvolvementsType.InsuredDriver)
            {
                List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                if (HttpContext.Current.Session["InsuredDriverSupportingInvolvements"] == null)
                {
                    //List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
                    insuredDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                    HttpContext.Current.Session["InsuredDriverSupportingInvolvements"] = insuredDriverSupportingInvolvements;
                }
                else
                {
                    //Get List from Session
                    insuredDriverSupportingInvolvements = (List<SupportingInvolvementsViewModel>)HttpContext.Current.Session["InsuredDriverSupportingInvolvements"];
                    // Check if model already exists in List if it is update it
                    if (supportingInvolvementsViewModel.CurrentSupportingInvolvement > insuredDriverSupportingInvolvements.Count())
                    {
                        //add new model to end of list
                        insuredDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        HttpContext.Current.Session["InsuredDriverSupportingInvolvements"] = insuredDriverSupportingInvolvements;
                    }
                    else //if (insuredDriverSupportingInvolvements[supportingInvolvementsViewModel.CurrentSupportingInvolvement-1] != null)
                    {
                        //an update of the model
                        insuredDriverSupportingInvolvements[supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1] = supportingInvolvementsViewModel;
                    }
                }
            }
            else if (supportingInvolvementsType == SupportingInvolvementsType.InsuredPassenger)
            {
                List<SupportingInvolvementsViewModel> passengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                if (HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] == null)
                {
                    //
                    passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                    DictionaryPassengerSupportingInvolvements.Add(PassengerID, passengerSupportingInvolvements);
                    HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = DictionaryPassengerSupportingInvolvements;
                }
                else
                {
                    //Get dictionary from session
                    DictionaryPassengerSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"];
                    // Check if model already exists in List, if not add it
                    if (DictionaryPassengerSupportingInvolvements.ContainsKey(PassengerID))
                    {
                        passengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements[PassengerID];
                        //
                        // Do we need to add the Supporting Involvement?
                        if (supportingInvolvementsViewModel.CurrentSupportingInvolvement > passengerSupportingInvolvements.Count())
                        {
                            passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                            HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = DictionaryPassengerSupportingInvolvements;
                        }
                        else
                        {
                            // do update
                            //an update of the model
                            passengerSupportingInvolvements[supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1] = supportingInvolvementsViewModel;
                        }
                    }
                    else
                    {
                        //need to add supporting involvement for new passenger
                        passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        DictionaryPassengerSupportingInvolvements.Add(PassengerID, passengerSupportingInvolvements);
                        HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = DictionaryPassengerSupportingInvolvements;
                    }
                }
            }
            else if (supportingInvolvementsType == SupportingInvolvementsType.ThirdPartyDriver)
            {
                List<SupportingInvolvementsViewModel> thirdPartyDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyDriverSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                if (HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] == null)
                {
                    //
                    thirdPartyDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                    DictionaryThirdPartyDriverSupportingInvolvements.Add(VehicleID, thirdPartyDriverSupportingInvolvements);
                    HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] = DictionaryThirdPartyDriverSupportingInvolvements;
                }
                else
                {
                    //Get dictionary from session
                    DictionaryThirdPartyDriverSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"];
                    // Check if model already exists in List, if not add it
                    if (DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(VehicleID))
                    {
                        thirdPartyDriverSupportingInvolvements = DictionaryThirdPartyDriverSupportingInvolvements[VehicleID];

                        if (thirdPartyDriverSupportingInvolvements != null)
                        {
                            //
                            // Do we need to add the Supporting Involvement?
                            if (supportingInvolvementsViewModel.CurrentSupportingInvolvement >
                                thirdPartyDriverSupportingInvolvements.Count())
                            {
                                thirdPartyDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                                HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] =
                                    DictionaryThirdPartyDriverSupportingInvolvements;
                            }
                            else
                            {
                                // do update
                                //an update of the model
                                thirdPartyDriverSupportingInvolvements[
                                    supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1] =
                                    supportingInvolvementsViewModel;
                            }
                        }

                    }
                    else
                    {
                        // Add supp involvements for new vehicle
                        thirdPartyDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        DictionaryThirdPartyDriverSupportingInvolvements.Add(VehicleID, thirdPartyDriverSupportingInvolvements);
                        HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] = DictionaryThirdPartyDriverSupportingInvolvements;

                    }
                }
            }
            else if (supportingInvolvementsType == SupportingInvolvementsType.ThirdPartyPassenger)
            {
                Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>> DictionaryThirdPartyVehicleSupportingInvolvements = new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
                Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                List<SupportingInvolvementsViewModel> ThirdPartyPassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                if (HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] == null)
                {
                    ThirdPartyPassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                    DictionaryThirdPartyPassengerSupportingInvolvements.Add(PassengerID, ThirdPartyPassengerSupportingInvolvements);

                    DictionaryThirdPartyVehicleSupportingInvolvements.Add(VehicleID, DictionaryThirdPartyPassengerSupportingInvolvements);

                    HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;
                }
                else
                {
                    //Get dictionary from session
                    DictionaryThirdPartyVehicleSupportingInvolvements = (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];

                    // Check if model already exists in List, if not add it
                    if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(VehicleID))
                    {
                        DictionaryThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyVehicleSupportingInvolvements[VehicleID];

                        if (DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(PassengerID))
                        {
                            ThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyPassengerSupportingInvolvements[PassengerID];
                            //
                            // Do we need to add the Supporting Involvement?
                            if (supportingInvolvementsViewModel.CurrentSupportingInvolvement > ThirdPartyPassengerSupportingInvolvements.Count())
                            {
                                ThirdPartyPassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);

                                // DictionaryThirdPartyVehicleSupportingInvolvements.Add(VehicleID, ThirdPartyPassengerSupportingInvolvements);
                                HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;
                            }
                            else
                            {
                                // do update
                                //an update of the model
                                ThirdPartyPassengerSupportingInvolvements[supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1] = supportingInvolvementsViewModel;
                                HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;
                            }
                        }
                        else
                        {
                            // add dictionary for new passenger
                            ThirdPartyPassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                            DictionaryThirdPartyPassengerSupportingInvolvements.Add(PassengerID, ThirdPartyPassengerSupportingInvolvements);
                            // update dictionary of passengers for vehicle.
                            DictionaryThirdPartyVehicleSupportingInvolvements[VehicleID] = DictionaryThirdPartyPassengerSupportingInvolvements;

                            // DictionaryThirdPartyVehicleSupportingInvolvements.Add(VehicleID, DictionaryThirdPartyPassengerSupportingInvolvements);

                            HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;

                        }
                    }
                    else
                    {
                        // Add supp involvements for new vehicle
                        ThirdPartyPassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        //DictionaryThirdPartyPassengerSupportingInvolvements.Add(VehicleID, ThirdPartyPassengerSupportingInvolvements);

                        DictionaryThirdPartyPassengerSupportingInvolvements.Add(PassengerID, ThirdPartyPassengerSupportingInvolvements);

                        DictionaryThirdPartyVehicleSupportingInvolvements.Add(VehicleID, DictionaryThirdPartyPassengerSupportingInvolvements);

                        HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;

                    }
                }

            }

        }

        //public static void StorePassengerSupportingInvolvementsViewModel(SupportingInvolvementsViewModel supportingInvolvementsViewModel, int PassengerID)
        //{

        //        List<SupportingInvolvementsViewModel> passengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

        //        Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

        //        if (HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] == null)
        //        {
        //            //
        //            passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
        //            DictionaryPassengerSupportingInvolvements.Add(PassengerID, passengerSupportingInvolvements);
        //            HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = DictionaryPassengerSupportingInvolvements;
        //        }
        //        else
        //        {
        //            //Get dictionary from session
        //            DictionaryPassengerSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"];
        //            // Check if model already exists in List, if not add it
        //            if (DictionaryPassengerSupportingInvolvements.ContainsKey(PassengerID))
        //            {
        //                passengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements[PassengerID];
        //                //
        //                // Do we need to add the Supporting Involvement?
        //                if (supportingInvolvementsViewModel.CurrentSupportingInvolvement > passengerSupportingInvolvements.Count())
        //                {
        //                    passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
        //                    HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = DictionaryPassengerSupportingInvolvements;
        //                }


        //            }


        //        }

        //}

        public static void StorePassengerViewModel(PassengerViewModel passengerViewModel, PassengerType PassengerType, int VehicleID = -1)
        {
            List<PassengerViewModel> insuredDriverPassengers = new List<PassengerViewModel>();
            
            if (PassengerType == PassengerType.InsuredVehiclePassenger)
            {
                if (HttpContext.Current.Session["InsuredDriverPassengers"] == null)
                {
                    //List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
                    insuredDriverPassengers.Add(passengerViewModel);
                    HttpContext.Current.Session["InsuredDriverPassengers"] = insuredDriverPassengers;
                }
                else
                {
                    //Get List from Session
                    insuredDriverPassengers = (List<PassengerViewModel>)HttpContext.Current.Session["InsuredDriverPassengers"];
                    // Check if model already exists in List
                    if (passengerViewModel.CurrentPassenger > insuredDriverPassengers.Count())
                    {
                        insuredDriverPassengers.Add(passengerViewModel);
                        HttpContext.Current.Session["InsuredDriverPassengers"] = insuredDriverPassengers;
                    }
                    else
                    {
                        // Update the model in session
                        insuredDriverPassengers[passengerViewModel.CurrentPassenger - 1] = passengerViewModel;
                    }
                }
            }
            else if (PassengerType == PassengerType.ThirdPartyVehiclePassenger)
            {
                // NumberPassengers = ThirdPartyVehicleNumberPassengers(VehicleID);

                Dictionary<int, List<PassengerViewModel>> DictionaryThirdPartyPassengers = new Dictionary<int, List<PassengerViewModel>>();
                List<PassengerViewModel> ThirdPartyPassengers = new List<PassengerViewModel>();

                if (HttpContext.Current.Session["DictionaryThirdPartyPassengers"] == null)
                {
                    ThirdPartyPassengers.Add(passengerViewModel);
                    DictionaryThirdPartyPassengers.Add(VehicleID, ThirdPartyPassengers);
                    HttpContext.Current.Session["DictionaryThirdPartyPassengers"] = DictionaryThirdPartyPassengers;

                }
                else
                {
                    //Get List from Session
                    DictionaryThirdPartyPassengers = (Dictionary<int, List<PassengerViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyPassengers"];

                    // Check if model already exists in List
                    if (DictionaryThirdPartyPassengers.ContainsKey(VehicleID))
                    {
                        ThirdPartyPassengers = DictionaryThirdPartyPassengers[VehicleID];

                        if (passengerViewModel.CurrentPassenger > ThirdPartyPassengers.Count())
                        {
                            ThirdPartyPassengers.Add(passengerViewModel);
                            DictionaryThirdPartyPassengers[VehicleID] = ThirdPartyPassengers;
                            HttpContext.Current.Session["DictionaryThirdPartyPassengers"] = DictionaryThirdPartyPassengers;

                        }
                        else
                        {
                            // Update the model in session
                            ThirdPartyPassengers[passengerViewModel.CurrentPassenger - 1] = passengerViewModel;
                        }
                    }
                    else
                    {
                        // Add Passenger for new vehicle
                        ThirdPartyPassengers.Add(passengerViewModel);
                        DictionaryThirdPartyPassengers.Add(VehicleID, ThirdPartyPassengers);
                        HttpContext.Current.Session["DictionaryThirdPartyPassengers"] = DictionaryThirdPartyPassengers;
                    }
                }
            }
        }

        public static void StoreThirdPartyVehicleViewModel(ThirdPartyVehicleViewModel thirdPartyVehicleViewModel, int VehicleID)
        {

            Dictionary<int, ThirdPartyVehicleViewModel> DictionaryThirdPartyVehicleViewModels = new Dictionary<int, ThirdPartyVehicleViewModel>();

            if (HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] == null)
            {
                //
                // passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                DictionaryThirdPartyVehicleViewModels.Add(VehicleID, thirdPartyVehicleViewModel);
                HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] = DictionaryThirdPartyVehicleViewModels;
            }
            else
            {
                //Get dictionary from session
                DictionaryThirdPartyVehicleViewModels = (Dictionary<int, ThirdPartyVehicleViewModel>)HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"];
                // Check if model already exists in List, if not add it
                if (DictionaryThirdPartyVehicleViewModels.ContainsKey(VehicleID))
                {
                    //Update the model
                    DictionaryThirdPartyVehicleViewModels[VehicleID] = thirdPartyVehicleViewModel;
                    HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] = DictionaryThirdPartyVehicleViewModels;
                }
                else
                {
                    // Add new model
                    DictionaryThirdPartyVehicleViewModels.Add(VehicleID, thirdPartyVehicleViewModel);
                    HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] = DictionaryThirdPartyVehicleViewModels;
                }
            }
        }

        public static void StoreThirdPartyDriverViewModel(ThirdPartyDriverViewModel thirdPartyDriverViewModel, int VehicleID)
        {

            Dictionary<int, ThirdPartyDriverViewModel> DictionaryThirdPartyDriverViewModels = new Dictionary<int, ThirdPartyDriverViewModel>();

            if (HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] == null)
            {
                //
                // passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                DictionaryThirdPartyDriverViewModels.Add(VehicleID, thirdPartyDriverViewModel);
                HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] = DictionaryThirdPartyDriverViewModels;
            }
            else
            {
                //Get dictionary from session
                DictionaryThirdPartyDriverViewModels = (Dictionary<int, ThirdPartyDriverViewModel>)HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"];
                // Check if model already exists in List, if not add it
                if (DictionaryThirdPartyDriverViewModels.ContainsKey(VehicleID))
                {
                    //Update the model
                    DictionaryThirdPartyDriverViewModels[VehicleID] = thirdPartyDriverViewModel;
                }
                else
                {
                    // Add new model
                    DictionaryThirdPartyDriverViewModels.Add(VehicleID, thirdPartyDriverViewModel);
                    HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] = DictionaryThirdPartyDriverViewModels;
                }
            }

        }

        public static void StoreWitnessViewModel(WitnessViewModel witnessViewModel, int WitnessID)
        {
            Dictionary<int, WitnessViewModel> DictionaryWitnessViewModels = new Dictionary<int, WitnessViewModel>();

            if (HttpContext.Current.Session["DictionaryWitnessViewModels"] == null)
            {
                //
                // passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                DictionaryWitnessViewModels.Add(WitnessID, witnessViewModel);
                HttpContext.Current.Session["DictionaryWitnessViewModels"] = DictionaryWitnessViewModels;
            }
            else
            {
                //Get dictionary from session
                DictionaryWitnessViewModels = (Dictionary<int, WitnessViewModel>)HttpContext.Current.Session["DictionaryWitnessViewModels"];
                // Check if model already exists in List, if not add it
                if (DictionaryWitnessViewModels.ContainsKey(WitnessID))
                {
                    //Update the model
                    DictionaryWitnessViewModels[WitnessID] = witnessViewModel;
                }
                else
                {
                    // Add new model
                    DictionaryWitnessViewModels.Add(WitnessID, witnessViewModel);
                    HttpContext.Current.Session["DictionaryWitnessViewModels"] = DictionaryWitnessViewModels;
                }
            }
        }

        public static void StoreReviewAndSubmitViewModel(ReviewAndSubmitViewModel reviewAndSubmitViewModel)
        {
            HttpContext.Current.Session["ReviewAndSubmitViewModel"] = reviewAndSubmitViewModel;
        }

        #endregion
    }
}