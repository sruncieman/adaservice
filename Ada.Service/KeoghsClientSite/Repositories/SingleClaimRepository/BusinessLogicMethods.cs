﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.SingleClaim;

namespace KeoghsClientSite.Repositories.SingleClaimRepository
{
    public partial class SingleClaimRepository
    {
        #region business logic methods

        public static CurrentView GetPreviousPage(CurrentView CurrentPageType, int TotalNumPages, int CurrentPage)
        {
            // 
            switch (CurrentPageType)
            {
                case CurrentView.Incident:

                    break;
                case CurrentView.Policy:

                    break;
                case CurrentView.PolicyHolderPersonal:

                    break;
                case CurrentView.PolicyHolderCommercial:

                    break;

                case CurrentView.SupportingInvolvementInsuredDriver:
                    if (CurrentPage == 1)
                        return CurrentView.InsuredDriver;
                    else
                        return CurrentView.SupportingInvolvementInsuredDriver;

                case CurrentView.SupportingInvolvementInsuredPassenger:
                    if (CurrentPage == 1)
                        return CurrentView.Passenger;
                    else
                        return CurrentView.SupportingInvolvementInsuredPassenger;
                case CurrentView.SupportingInvolvementThirdPartyDriver:
                    if (CurrentPage == 1)
                        return CurrentView.ThirdPartyDriver;
                    else
                        return CurrentView.SupportingInvolvementThirdPartyDriver;
                case CurrentView.SupportingInvolvementThirdPartyPassenger:
                    if (CurrentPage == 1)
                        return CurrentView.Passenger;
                    else
                        return CurrentView.SupportingInvolvementThirdPartyPassenger;
                default:
                    return CurrentView.Unknown;
                    //break;
            }
            return CurrentView.Unknown;
        }

        public static int InsuredVehicleNumberPassengers()
        {
            var insuredVehicleViewModel = new InsuredVehicleViewModel();

            if (HttpContext.Current.Session["insuredVehicleViewModel"] != null)
            {
                insuredVehicleViewModel =
                    HttpContext.Current.Session["insuredVehicleViewModel"] as InsuredVehicleViewModel;
            }
            return insuredVehicleViewModel.Vehicle.NumberOfPassengers;
        }

        public static int ThirdPartyVehicleNumberPassengers(int VehicleID)
        {
            var thirdPartyVehicleViewModel = new ThirdPartyVehicleViewModel();

            var DictionaryThirdPartyVehicleViewModels = new Dictionary<int, ThirdPartyVehicleViewModel>();

            //Get dictionary from session
            DictionaryThirdPartyVehicleViewModels =
                (Dictionary<int, ThirdPartyVehicleViewModel>)
                HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"];
            // Check if model already exists in List, if not add it
            if (DictionaryThirdPartyVehicleViewModels.ContainsKey(VehicleID))
            {
                //Update the model
                thirdPartyVehicleViewModel = DictionaryThirdPartyVehicleViewModels[VehicleID];
            }

            return thirdPartyVehicleViewModel.Vehicle.NumberOfPassengers;
        }

        public static int NumberOfThirdPartyVehicles()
        {
            var incidentViewModel = new IncidentViewModel();

            if (HttpContext.Current.Session["incidentViewModel"] != null)
            {
                incidentViewModel = HttpContext.Current.Session["incidentViewModel"] as IncidentViewModel;
            }

            return incidentViewModel.SelectedNumberOfVehicles - 1;
        }

        public static int NumberOfWitnesses()
        {
            var incidentViewModel = new IncidentViewModel();

            if (HttpContext.Current.Session["incidentViewModel"] != null)
            {
                incidentViewModel = HttpContext.Current.Session["incidentViewModel"] as IncidentViewModel;
            }

            return incidentViewModel.SelectedNumberOfWitnesses;
        }

        public static int GetNumberofSupportingInvolvementsForPassenger(int PassengerID)
        {
            var DictionaryPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();
            var PassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

            //if (HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] != null)
            //{
            //Get dictionary from session
            DictionaryPassengerSupportingInvolvements =
                (Dictionary<int, List<SupportingInvolvementsViewModel>>)
                HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"];

            if (DictionaryPassengerSupportingInvolvements != null)
            {
                // Check if model already exists in List
                if (DictionaryPassengerSupportingInvolvements.ContainsKey(PassengerID))
                {
                    PassengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements[PassengerID];
                    return PassengerSupportingInvolvements.Count();
                }
                else
                    return -1;
            }
            else
            {
                return -1;
            }
        }

        //
        //
        //
        public static int GetNumberofSupportingInvolvementsForThirdPartyPassenger(int VehicleID, int PassengerID,
                                                                                  bool GetLastPassenger = false)
        {
            var DictionaryThirdPartyVehicleSupportingInvolvements =
                new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
            var DictionaryThirdPartyPassengerSupportingInvolvements =
                new Dictionary<int, List<SupportingInvolvementsViewModel>>();

            var ThirdPartyPassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

            //Get dictionary from session
            DictionaryThirdPartyVehicleSupportingInvolvements =
                (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)
                HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];

            // Check if model already exists in List, if not add it
            if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(VehicleID))
            {
                DictionaryThirdPartyPassengerSupportingInvolvements =
                    DictionaryThirdPartyVehicleSupportingInvolvements[VehicleID];

                if (DictionaryThirdPartyPassengerSupportingInvolvements != null &&
                    DictionaryThirdPartyPassengerSupportingInvolvements.Count() > 0)
                {
                    if (GetLastPassenger && DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(PassengerID))
                    {
                        SupportingInvolvementsViewModel lastPassenger =
                            DictionaryThirdPartyPassengerSupportingInvolvements[PassengerID].Last();

                        return lastPassenger.NumberSupportingInvolvements;
                    }
                    else if (DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(PassengerID))
                    {
                        ThirdPartyPassengerSupportingInvolvements =
                            DictionaryThirdPartyPassengerSupportingInvolvements[PassengerID];
                        //
                        // 
                        return ThirdPartyPassengerSupportingInvolvements.Count();
                    }

                    else
                        return -1;
                }
                else
                {
                    return -1;
                }
            }
            else
                return -1;
        }

        //
        //
        //
        public static int GetNumberofSupportingInvolvementsForInsuredDriver()
        {
            int numSupportingInvolvementsForInsuredDriver = 0;

            var insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
            //insuredDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);

            insuredDriverSupportingInvolvements =
                (List<SupportingInvolvementsViewModel>)
                HttpContext.Current.Session["InsuredDriverSupportingInvolvements"];
            if (insuredDriverSupportingInvolvements != null)
                numSupportingInvolvementsForInsuredDriver = insuredDriverSupportingInvolvements.Count();

            return numSupportingInvolvementsForInsuredDriver;
            //return insuredDriverSupportingInvolvements.Count();
        }

        //
        //
        //
        public static int GetNumberofSupportingInvolvementsForThirdPartyDriver(int VehicleID)
        {
            var thirdPartyDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

            var DictionaryThirdPartyDriverSupportingInvolvements =
                new Dictionary<int, List<SupportingInvolvementsViewModel>>();

            //Get dictionary from session
            DictionaryThirdPartyDriverSupportingInvolvements =
                (Dictionary<int, List<SupportingInvolvementsViewModel>>)
                HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"];
            // Check if model already exists in List, if not add it

            if (DictionaryThirdPartyDriverSupportingInvolvements != null)
            {
                if (DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(VehicleID))
                {
                    thirdPartyDriverSupportingInvolvements = DictionaryThirdPartyDriverSupportingInvolvements[VehicleID];
                    //
                    if (thirdPartyDriverSupportingInvolvements != null)
                    {
                        return thirdPartyDriverSupportingInvolvements.Count();
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                    return -1;
            }
            else
            {
                return -1;
            }
        }

        //
        //
        //
        public static string FormatPersonName(PersonInformation myPerson)
        {
            string PersonName = myPerson.Middlename != ""
                                    ? myPerson.Firstname + " " + myPerson.Middlename + " " + myPerson.Surname
                                    : myPerson.Firstname + " " + myPerson.Surname;

            return PersonName;
        }

        #endregion

        public static CurrentView CheckPreviousPassengerHasSupportingInvolvement(PassengerViewModel passengerViewModel)
        {
            CurrentView currentView = passengerViewModel.PreviousPage;

            bool previousPassengerHasSuppInvolvments = false;

            if (passengerViewModel.PassengerType == PassengerType.InsuredVehiclePassenger)
            {
                var DictionaryPassengerSupportingInvolvements =
                    new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                DictionaryPassengerSupportingInvolvements =
                    (Dictionary<int, List<SupportingInvolvementsViewModel>>)
                    HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"];

                if (DictionaryPassengerSupportingInvolvements != null)
                {
                    if (DictionaryPassengerSupportingInvolvements.ContainsKey(passengerViewModel.CurrentPassenger - 1))
                        previousPassengerHasSuppInvolvments = true;
                }

                if (passengerViewModel.CurrentPassenger > 1)
                {
                    if (previousPassengerHasSuppInvolvments)
                        currentView = CurrentView.SupportingInvolvementInsuredPassenger;
                    else
                        currentView = CurrentView.Passenger;
                }

                if (passengerViewModel.CurrentPassenger == 1)
                {
                    currentView = CheckFirstPassengerPreviousPage(passengerViewModel);
                }
            }


            if (passengerViewModel.PassengerType == PassengerType.ThirdPartyVehiclePassenger)
            {
                var DictionaryThirdPartyVehicleSupportingInvolvements =
                    new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
                var DictionaryThirdPartyPassengerSupportingInvolvements =
                    new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                if (HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] != null)
                {
                    DictionaryThirdPartyVehicleSupportingInvolvements =
                        (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)
                        HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];

                    if (DictionaryThirdPartyVehicleSupportingInvolvements != null)
                    {
                        if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(passengerViewModel.VehicleID))
                        {
                            DictionaryThirdPartyPassengerSupportingInvolvements =
                                DictionaryThirdPartyVehicleSupportingInvolvements[passengerViewModel.VehicleID];

                            if (
                                DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(
                                    passengerViewModel.PassengerID-1))
                            {
                                previousPassengerHasSuppInvolvments = true;
                            }
                        }
                    }

                    if (passengerViewModel.CurrentPassenger > 1)
                    {
                        if (previousPassengerHasSuppInvolvments)
                            currentView = CurrentView.SupportingInvolvementThirdPartyPassenger;
                        else
                            currentView = CurrentView.Passenger;
                    }

                    if (passengerViewModel.CurrentPassenger == 1)
                    {
                        currentView = CheckFirstPassengerPreviousPage(passengerViewModel);
                    }
                }
            }


            return currentView;
        }

        public static CurrentView CheckFirstPassengerPreviousPage(PassengerViewModel passengerViewModel)
        {
            CurrentView currentView = passengerViewModel.PreviousPage;

            if (passengerViewModel.PassengerType == PassengerType.InsuredVehiclePassenger)
            {
                var insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
                insuredDriverSupportingInvolvements =
                    (List<SupportingInvolvementsViewModel>)
                    HttpContext.Current.Session["InsuredDriverSupportingInvolvements"];

                if (insuredDriverSupportingInvolvements != null)
                {
                    var insuredDriverPassengers = new List<PassengerViewModel>();
                    insuredDriverPassengers =
                        (List<PassengerViewModel>) HttpContext.Current.Session["InsuredDriverPassengers"];

                    if (insuredDriverPassengers != null)
                    {
                        if (insuredDriverPassengers.Count > 0)
                        {
                            PassengerViewModel firstPassenger = insuredDriverPassengers[0];

                            if (firstPassenger.PreviousPage == CurrentView.InsuredDriver)
                            {
                                currentView = CurrentView.SupportingInvolvementInsuredDriver;
                            }
                        }
                    }
                }
            }

            if (passengerViewModel.PassengerType == PassengerType.ThirdPartyVehiclePassenger)
            {
                var DictionaryThirdPartyPassengers = new Dictionary<int, List<PassengerViewModel>>();
                var ThirdPartyPassengers = new List<PassengerViewModel>();
                var DictionaryThirdPartyDriverSupportingInvolvements =
                    new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                DictionaryThirdPartyDriverSupportingInvolvements =
                    (Dictionary<int, List<SupportingInvolvementsViewModel>>)
                    HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"];

                if (DictionaryThirdPartyDriverSupportingInvolvements != null)
                {
                    if (DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(passengerViewModel.VehicleID))
                    {
                        if (DictionaryThirdPartyDriverSupportingInvolvements[passengerViewModel.VehicleID] != null &&
                            DictionaryThirdPartyDriverSupportingInvolvements[passengerViewModel.VehicleID].Any())
                        {
                            currentView = CurrentView.SupportingInvolvementThirdPartyDriver;

                            DictionaryThirdPartyPassengers =
                                (Dictionary<int, List<PassengerViewModel>>)
                                HttpContext.Current.Session["DictionaryThirdPartyPassengers"];

                            if (DictionaryThirdPartyPassengers != null)
                            {
                                if (DictionaryThirdPartyPassengers.ContainsKey(passengerViewModel.VehicleID))
                                {
                                    ThirdPartyPassengers = DictionaryThirdPartyPassengers[passengerViewModel.VehicleID];

                                    if (passengerViewModel.PassengerID == 1)
                                    {
                                        PassengerViewModel firstPassenger = ThirdPartyPassengers[0];

                                        if (firstPassenger.PreviousPage == CurrentView.ThirdPartyDriver ||
                                            firstPassenger.PreviousPage ==
                                            CurrentView.SupportingInvolvementThirdPartyDriver)
                                            currentView = CurrentView.SupportingInvolvementThirdPartyDriver;
                                    }
                                    else
                                        currentView = CurrentView.SupportingInvolvementThirdPartyPassenger;
                                }
                            }
                        }
                    }
                }
            }

            return currentView;
        }

        public static void CheckThirdPartyVehicles(IncidentViewModel incidentViewModel)
        {
            var DictionaryThirdPartyVehicleViewModels = new Dictionary<int, ThirdPartyVehicleViewModel>();
            var DictionaryThirdPartyDriverViewModels = new Dictionary<int, ThirdPartyDriverViewModel>();
            var DictionaryThirdPartyDriverSupportingInvolvements =
                new Dictionary<int, List<SupportingInvolvementsViewModel>>();

            int numSelectedTPVehicles = incidentViewModel.SelectedNumberOfVehicles - 1;

            if (HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] != null)
            {
                DictionaryThirdPartyVehicleViewModels =
                    (Dictionary<int, ThirdPartyVehicleViewModel>)
                    HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"];

                int currentNumThirdPartyVehicleViewModels = DictionaryThirdPartyVehicleViewModels.Count();

                if (numSelectedTPVehicles > currentNumThirdPartyVehicleViewModels)
                {
                    int numTPVehicleDiff = numSelectedTPVehicles - currentNumThirdPartyVehicleViewModels;

                    for (int i = 0; i < numTPVehicleDiff; i++)
                    {
                        int countV = 1;
                        if (!DictionaryThirdPartyVehicleViewModels.ContainsKey(countV))
                            DictionaryThirdPartyVehicleViewModels.Add(currentNumThirdPartyVehicleViewModels + countV,
                                                                  new ThirdPartyVehicleViewModel
                                                                      {NumberOfVehicles = numSelectedTPVehicles});
                        countV++;
                    }

                    // Update collection counts
                    int count = 1;
                    foreach (var item in DictionaryThirdPartyVehicleViewModels)
                    {
                        item.Value.NumberOfVehicles = numSelectedTPVehicles;
                        item.Value.CurrentVehicle = count;
                        item.Value.VehicleID = count;
                        count++;
                    }

                    HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] =
                        DictionaryThirdPartyVehicleViewModels;
                }

                if (numSelectedTPVehicles > -1 && numSelectedTPVehicles < currentNumThirdPartyVehicleViewModels)
                {
                    DictionaryThirdPartyDriverSupportingInvolvements =
                        (Dictionary<int, List<SupportingInvolvementsViewModel>>)
                        HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"];
                    //DictionaryThirdPartyDriverSupportingInvolvements[thirdPartyDriverViewModel.CurrentVehicle].RemoveAll(x => x.NumberSupportingInvolvements > 0);
                    //Session["DictionaryThirdPartyDriverSupportingInvolvements"] = DictionaryThirdPartyDriverSupportingInvolvements;


                    if (HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] != null)
                        DictionaryThirdPartyDriverViewModels =
                            (Dictionary<int, ThirdPartyDriverViewModel>)
                            HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"];

                    for (int i = currentNumThirdPartyVehicleViewModels; i > numSelectedTPVehicles; i--)
                    {
                        DictionaryThirdPartyVehicleViewModels.Remove(i);

                        if (DictionaryThirdPartyDriverViewModels != null &&
                            DictionaryThirdPartyDriverViewModels.ContainsKey(i))
                        {
                            DictionaryThirdPartyDriverViewModels.Remove(i);

                            if (DictionaryThirdPartyDriverSupportingInvolvements != null && DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(i))
                            {
                                DictionaryThirdPartyDriverSupportingInvolvements[i].RemoveAll(
                                    x => x.NumberSupportingInvolvements > 0);
                            }
                        }
                    }

                    // Update collection counts
                    int count = 1;
                    foreach (var item in DictionaryThirdPartyVehicleViewModels)
                    {
                        item.Value.NumberOfVehicles = numSelectedTPVehicles;
                        item.Value.CurrentVehicle = count;
                        item.Value.VehicleID = count;
                        count++;
                    }

                    HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] =
                        DictionaryThirdPartyVehicleViewModels;
                    HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] =
                        DictionaryThirdPartyDriverSupportingInvolvements;
                    HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] =
                        DictionaryThirdPartyDriverViewModels;
                }
            }
        }

        public static void CheckThirdPartyPassengers(ThirdPartyVehicleViewModel thirdPartyVehicleViewModel)
        {
            int numSelectedTpPassengers = thirdPartyVehicleViewModel.Vehicle.NumberOfPassengers;
            int Vehicle_ID = thirdPartyVehicleViewModel.VehicleID;

            var DictionaryThirdPartyPassengers =
                new Dictionary<int, List<PassengerViewModel>>();
            //Dictionary<int, ThirdPartyVehicleViewModel> DictionaryThirdPartyVehicleViewModels = new Dictionary<int, ThirdPartyVehicleViewModel>();
            var ThirdPartyPassengers = new List<PassengerViewModel>();

            if (HttpContext.Current.Session["DictionaryThirdPartyPassengers"] != null && numSelectedTpPassengers > 0)
            {
                DictionaryThirdPartyPassengers =
                    (Dictionary<int, List<PassengerViewModel>>)
                    HttpContext.Current.Session["DictionaryThirdPartyPassengers"];

                // Check if model already exists in List
                if (DictionaryThirdPartyPassengers.ContainsKey(Vehicle_ID))
                {
                    ThirdPartyPassengers = DictionaryThirdPartyPassengers[Vehicle_ID];

                    if (numSelectedTpPassengers > ThirdPartyPassengers.Count())
                    {
                        int numTpPassengerDiff = numSelectedTpPassengers - ThirdPartyPassengers.Count();

                        for (int i = 0; i < numTpPassengerDiff; i++)
                        {
                            int countP = 1;
                            ThirdPartyPassengers.Add(new PassengerViewModel {});

                            countP++;
                        }

                        // Update collection counts
                        int count = 1;
                        foreach (PassengerViewModel item in ThirdPartyPassengers)
                        {
                            item.VehicleID = Vehicle_ID;
                            item.NumberPassengers = numSelectedTpPassengers;
                            item.PassengerID = count;
                            item.CurrentPassenger = count;
                            item.PassengerType = PassengerType.ThirdPartyVehiclePassenger;
                            if (count == 1)
                                item.PreviousPage = CurrentView.ThirdPartyDriver;
                            else
                                item.PreviousPage = CurrentView.Passenger;
                            item.PreviousPageID = 0;
                            count++;
                        }

                        DictionaryThirdPartyPassengers[Vehicle_ID] = ThirdPartyPassengers;

                        HttpContext.Current.Session["DictionaryThirdPartyPassengers"] = DictionaryThirdPartyPassengers;
                    }

                    if (numSelectedTpPassengers > -1 && numSelectedTpPassengers < ThirdPartyPassengers.Count())
                    {
                        var removedPassengers = new List<int>();

                        foreach (PassengerViewModel item in ThirdPartyPassengers)
                        {
                            if (item.PassengerID > numSelectedTpPassengers)
                                removedPassengers.Add(item.PassengerID);
                        }


                        int numTpPassengerDiff = numSelectedTpPassengers - ThirdPartyPassengers.Count();

                        ThirdPartyPassengers.RemoveAll(x => x.PassengerID > numSelectedTpPassengers);

                        DictionaryThirdPartyPassengers[Vehicle_ID] = ThirdPartyPassengers;

                        // Update collection counts
                        int count = 1;
                        foreach (PassengerViewModel item in ThirdPartyPassengers)
                        {
                            item.VehicleID = Vehicle_ID;
                            item.NumberPassengers = numSelectedTpPassengers;
                            item.PassengerID = count;
                            item.CurrentPassenger = count;
                            item.PassengerType = PassengerType.ThirdPartyVehiclePassenger;
                            if (count == 1)
                                item.PreviousPage = CurrentView.ThirdPartyDriver;
                            else
                                item.PreviousPage = CurrentView.Passenger;
                            item.PreviousPageID = 0;
                            count++;
                        }

                        HttpContext.Current.Session["DictionaryThirdPartyPassengers"] = DictionaryThirdPartyPassengers;

                        // Remove any 3rd party Vehicle Supporting Involvements for this vehicle
                        var DictionaryThirdPartyVehicleSupportingInvolvements =
                            new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
                        var DictionaryThirdPartyPassengerSupportingInvolvements =
                            new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                        DictionaryThirdPartyVehicleSupportingInvolvements =
                            (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)
                            HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];

                        if (DictionaryThirdPartyVehicleSupportingInvolvements != null)
                        {
                            if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(Vehicle_ID))
                            {
                                DictionaryThirdPartyPassengerSupportingInvolvements =
                                    DictionaryThirdPartyVehicleSupportingInvolvements[Vehicle_ID];
                            }

                            if (DictionaryThirdPartyPassengerSupportingInvolvements != null)
                            {
                                foreach (int passenger in removedPassengers)
                                {
                                    DictionaryThirdPartyPassengerSupportingInvolvements.Remove(passenger);
                                }


                                if (!DictionaryThirdPartyPassengerSupportingInvolvements.Any())
                                    HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] =
                                        null;

                                else
                                {
                                    HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] =
                                        DictionaryThirdPartyVehicleSupportingInvolvements;
                                }
                            }
                        }
                    }
                }
            }

            if (numSelectedTpPassengers == 0)
            {
                // delete TP passengers and any related tpPassengerSuppInvs
                if (HttpContext.Current.Session["DictionaryThirdPartyPassengers"] != null)
                {
                    DictionaryThirdPartyPassengers =
                        (Dictionary<int, List<PassengerViewModel>>)
                        HttpContext.Current.Session["DictionaryThirdPartyPassengers"];

                    // Check if model already exists in List
                    if (DictionaryThirdPartyPassengers.ContainsKey(Vehicle_ID))
                    {
                        DictionaryThirdPartyPassengers[Vehicle_ID] = null;
                        DictionaryThirdPartyPassengers.Remove(Vehicle_ID);
                        HttpContext.Current.Session["DictionaryThirdPartyPassengers"] = DictionaryThirdPartyPassengers;
                    }
                }

                // remove any tpPassengerSuppInvs
                var DictionaryThirdPartyVehicleSupportingInvolvements =
                    new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
                var DictionaryThirdPartyPassengerSupportingInvolvements =
                    new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                DictionaryThirdPartyVehicleSupportingInvolvements =
                    (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)
                    HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];

                if (DictionaryThirdPartyVehicleSupportingInvolvements != null)
                {
                    if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(Vehicle_ID))
                    {
                        DictionaryThirdPartyPassengerSupportingInvolvements =
                            DictionaryThirdPartyVehicleSupportingInvolvements[Vehicle_ID];
                    }

                    if (DictionaryThirdPartyPassengerSupportingInvolvements != null)
                    {
                        if (DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(Vehicle_ID))
                            DictionaryThirdPartyPassengerSupportingInvolvements[Vehicle_ID].RemoveAll(
                                x => x.NumberSupportingInvolvements > 0);

                        HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] =
                            DictionaryThirdPartyVehicleSupportingInvolvements;
                    }
                }
            }
        }


        public static void ClearSingleClaim()
        {
            HttpContext.Current.Session["claimViewModel"] = null;
            HttpContext.Current.Session["incidentViewModel"] = null;
            HttpContext.Current.Session["policyViewModel"] = null;
            HttpContext.Current.Session["policyHolderCommercialViewModel"] = null;
            HttpContext.Current.Session["policyHolderPersonalViewModel"] = null;
            HttpContext.Current.Session["insuredVehicleViewModel"] = null;
            HttpContext.Current.Session["insuredVehicleViewModel"] = null;
            HttpContext.Current.Session["vehicleAdditionalInfo"] = null;
            HttpContext.Current.Session["insuredDriverPerson"] = null;
            HttpContext.Current.Session["insuredDriverAddress"] = null;
            HttpContext.Current.Session["insuredDriverViewModel"] = null;
            HttpContext.Current.Session["passengerViewModel"] = null;
            HttpContext.Current.Session["thirdPartyVehicleViewModel"] = null;
            HttpContext.Current.Session["thirdPartyDriverViewModel"] = null;
            HttpContext.Current.Session["witnessViewModel"] = null;
            HttpContext.Current.Session["InsuredDriverSupportingInvolvements"] = null;
            HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = null;
            HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] = null;
            HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = null;
            HttpContext.Current.Session["DictionaryThirdPartyPassengers"] = null;
            HttpContext.Current.Session["InsuredDriverPassengers"] = null;
            HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] = null;
            HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] = null;
            HttpContext.Current.Session["DictionaryWitnessViewModels"] = null;
            HttpContext.Current.Session["ReviewAndSubmitViewModel"] = null;
        }




        private static void PopulateInsuredDriver(InsuredDriverViewModel insuredDriverViewModel, PolicyHolderPersonalViewModel policyHolderPersonalViewModel)
        {
            insuredDriverViewModel.PersonInformation.DateOfBirth = policyHolderPersonalViewModel.PersonInformation.DateOfBirth;
            insuredDriverViewModel.PersonInformation.Firstname = policyHolderPersonalViewModel.PersonInformation.Firstname;
            insuredDriverViewModel.PersonInformation.Middlename = policyHolderPersonalViewModel.PersonInformation.Middlename;
            insuredDriverViewModel.PersonInformation.SelectedGender = policyHolderPersonalViewModel.PersonInformation.SelectedGender;
            insuredDriverViewModel.PersonInformation.SelectedSalutation = policyHolderPersonalViewModel.PersonInformation.SelectedSalutation;
            insuredDriverViewModel.PersonInformation.SelectedWitnessInvolvementType = policyHolderPersonalViewModel.PersonInformation.SelectedWitnessInvolvementType;
            insuredDriverViewModel.PersonInformation.Surname = policyHolderPersonalViewModel.PersonInformation.Surname;
            insuredDriverViewModel.AdditionalInfo.DrivingLicenseNumber = policyHolderPersonalViewModel.AdditionalInfo.DrivingLicenseNumber;
            insuredDriverViewModel.AdditionalInfo.EmailAddress = policyHolderPersonalViewModel.AdditionalInfo.EmailAddress;
            insuredDriverViewModel.AdditionalInfo.HomeTelephone = policyHolderPersonalViewModel.AdditionalInfo.HomeTelephone;
            insuredDriverViewModel.AdditionalInfo.MobileNumber = policyHolderPersonalViewModel.AdditionalInfo.MobileNumber;
            insuredDriverViewModel.AdditionalInfo.SelectedNationality = policyHolderPersonalViewModel.AdditionalInfo.SelectedNationality;
            insuredDriverViewModel.AdditionalInfo.WorkTelephone = policyHolderPersonalViewModel.AdditionalInfo.WorkTelephone;
            insuredDriverViewModel.AdditionalInfo.PassportNumber = policyHolderPersonalViewModel.AdditionalInfo.PassportNumber;
            insuredDriverViewModel.AdditionalInfo.NINumber = policyHolderPersonalViewModel.AdditionalInfo.NINumber;
            insuredDriverViewModel.Address.SelectedAddressStatus = policyHolderPersonalViewModel.Address.SelectedAddressStatus;
            insuredDriverViewModel.Address.SubBuilding = policyHolderPersonalViewModel.Address.SubBuilding;
            insuredDriverViewModel.Address.BuildingName = policyHolderPersonalViewModel.Address.BuildingName;
            insuredDriverViewModel.Address.BuildingNumber = policyHolderPersonalViewModel.Address.BuildingNumber;
            insuredDriverViewModel.Address.Street = policyHolderPersonalViewModel.Address.Street;
            insuredDriverViewModel.Address.Town = policyHolderPersonalViewModel.Address.Town;
            insuredDriverViewModel.Address.County = policyHolderPersonalViewModel.Address.County;
            insuredDriverViewModel.Address.EndOfResidency = policyHolderPersonalViewModel.Address.EndOfResidency;
            insuredDriverViewModel.Address.Locality = policyHolderPersonalViewModel.Address.Locality;
            insuredDriverViewModel.Address.Postcode = policyHolderPersonalViewModel.Address.Postcode;
            insuredDriverViewModel.Address.StartOfResidency = policyHolderPersonalViewModel.Address.StartOfResidency;
            //insuredDriverViewModel.BankAccount.AccountNumber = policyHolderPersonalViewModel.BankAccount.AccountNumber;
            //insuredDriverViewModel.BankAccount.BankName = policyHolderPersonalViewModel.BankAccount.BankName;
            //insuredDriverViewModel.BankAccount.DateDetailsTaken = policyHolderPersonalViewModel.BankAccount.DateDetailsTaken;
            //insuredDriverViewModel.BankAccount.SelectedAccountUsedFor = policyHolderPersonalViewModel.BankAccount.SelectedAccountUsedFor;
            //insuredDriverViewModel.BankAccount.SortCode = policyHolderPersonalViewModel.BankAccount.SortCode;
        }


        internal static InsuredDriverViewModel CheckInsuredDriverPersonInformation(InsuredDriverViewModel insuredDriverViewModel)
        {
            PolicyViewModel policyViewModel = new PolicyViewModel();

            if (HttpContext.Current.Session["policyViewModel"] != null)
                policyViewModel = HttpContext.Current.Session["policyViewModel"] as PolicyViewModel;
            

            InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();

            if (HttpContext.Current.Session["insuredVehicleViewModel"] != null)
                insuredVehicleViewModel = HttpContext.Current.Session["insuredVehicleViewModel"] as InsuredVehicleViewModel;


            if (policyViewModel.SelectedPolicyType == 1 && insuredVehicleViewModel.Vehicle.WasPolicyHolderTheDriver == true && policyViewModel.PolicyHolderDetailsKnown == true)
            {
                // Populate Insured driver with policy holder details
                PolicyHolderPersonalViewModel policyHolderPersonalViewModel = new PolicyHolderPersonalViewModel();

                insuredDriverViewModel.PopulateWithPolicyHolder = true;

                if (HttpContext.Current.Session["policyHolderPersonalViewModel"] != null)
                {
                    policyHolderPersonalViewModel = HttpContext.Current.Session["policyHolderPersonalViewModel"] as PolicyHolderPersonalViewModel;
                }

                PopulateInsuredDriver(insuredDriverViewModel, policyHolderPersonalViewModel);

                StoreInsuredDriverViewModel(insuredDriverViewModel);
            }

            return insuredDriverViewModel;
        }
    }
}