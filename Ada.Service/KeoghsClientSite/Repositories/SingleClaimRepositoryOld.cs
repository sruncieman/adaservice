﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using KeoghsClientSite.ActionFilters;
using KeoghsClientSite.Models.SingleClaim;
using System.Web.Mvc;
using KeoghsClientSite.StaticClasses;
using MDA.Common.Enum;
using MDA.WCF.WebServices.Interface;
using MDA.WCF.WebServices.Messages;

namespace KeoghsClientSite.Repositories
{
    public static class SingleClaimRepositoryOld
    {

        #region populate view models


        public static ClaimViewModel PopulateClaimViewModel(bool Previous=false)
        {
            ClaimViewModel claimViewModel = new ClaimViewModel();

            if (HttpContext.Current.Session["claimViewModel"] != null)
            {
                claimViewModel =HttpContext.Current.Session["claimViewModel"] as ClaimViewModel;
            }

            IEnumerable<SelectListItem> ClaimTypeListItems = new List<SelectListItem>{
               new SelectListItem(){Text="Motor",Value="1"}
               };

            //ClaimType
            claimViewModel.ClaimTypes = ClaimTypeListItems;

            IEnumerable<SelectListItem> ClaimStatusListItems = new List<SelectListItem>{
               new SelectListItem(){Text="Open",Value="1"},
               new SelectListItem(){Text="Closed",Value="2"}
               };

            claimViewModel.ClaimStatuses = ClaimStatusListItems;

            claimViewModel.NavigationData.CurrentForm = CurrentView.Claim.ToString();
            claimViewModel.NavigationData.Previous = Previous;

            return claimViewModel;
        }

        public static IncidentViewModel PopulateIncidentViewModel(string NavigationData,bool Previous=false)
        {

            IncidentViewModel incidentViewModel = new IncidentViewModel();

            if (HttpContext.Current.Session["incidentViewModel"] != null)
            {
                incidentViewModel = HttpContext.Current.Session["incidentViewModel"] as IncidentViewModel;
            }
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                incidentViewModel.NavigationData.Data = NavigationData;

            incidentViewModel.NavigationData.CurrentForm = CurrentView.Incident.ToString();
            incidentViewModel.NavigationData.Previous = Previous;
            //
            //Incident time hours
            List<SelectListItem> IncidentTimeHours = new List<SelectListItem>();

            IncidentTimeHours.Add(new SelectListItem() { Text = "--", Value = "--" });

            for( int i=0; i<25; i++)
                IncidentTimeHours.Add(new SelectListItem() { Text = i.ToString("D2"), Value = i.ToString("D2") });
            //
            incidentViewModel.IncidentTimeHours = IncidentTimeHours as IEnumerable<SelectListItem>;
            
            //Incident time minutes
            List<SelectListItem> IncidentTimeMinutes = new List<SelectListItem>();

            IncidentTimeMinutes.Add(new SelectListItem() { Text = "--", Value = "--" });

            for (int i = 0; i < 61; i++)
                IncidentTimeMinutes.Add(new SelectListItem() { Text = i.ToString("D2"), Value = i.ToString("D2") });
            //
            incidentViewModel.IncidentTimeMinutes = IncidentTimeMinutes as IEnumerable<SelectListItem>;

            // Number of vehicles
            IEnumerable<SelectListItem> NumberOfVehicles = new List<SelectListItem>{
               new SelectListItem(){Text="1",Value="0"},
               new SelectListItem(){Text="2",Value="1"},
               new SelectListItem(){Text="3",Value="2"}
               };
            incidentViewModel.NumberOfVehicles = NumberOfVehicles;

            //Number of witnesses
            IEnumerable<SelectListItem> NumberOfWitnesses = new List<SelectListItem>{
               new SelectListItem(){Text="1",Value="0"},
               new SelectListItem(){Text="2",Value="1"},
               new SelectListItem(){Text="3",Value="2"}
               };
            incidentViewModel.NumberOfWitnesses = NumberOfWitnesses;

            // Claim Code
            IEnumerable<SelectListItem> ClaimCode = new List<SelectListItem>{
               new SelectListItem(){Text="Insured damage only",Value="0"},
               new SelectListItem(){Text="TP damage only",Value="1"},
               new SelectListItem(){Text="Damage & PI",Value="2"}
               };
            incidentViewModel.ClaimCode = ClaimCode;

            //Liability
            IEnumerable<SelectListItem> Liability = new List<SelectListItem>{
               new SelectListItem(){Text="Yes",Value="1"},
               new SelectListItem(){Text="No",Value="0"}
               };

            incidentViewModel.Liability = Liability;

            //Liability Decision
            IEnumerable<SelectListItem> LiabilityDecision = new List<SelectListItem>(){
               new SelectListItem(){Text="Insured at fault",Value="0"},
               new SelectListItem(){Text="TP at fault",Value="1"},
               new SelectListItem(){Text="Split",Value="2"},
               new SelectListItem(){Text="Disputed",Value="3"}
               };

            incidentViewModel.LiabilityDecision=LiabilityDecision;

            //Moj Process Stage
            IEnumerable<SelectListItem> MojProcessStage = new List<SelectListItem>(){
               new SelectListItem(){Text="Stage 1",Value="0"},
               new SelectListItem(){Text="Stage 2",Value="1"},
               new SelectListItem(){Text="Stage 3",Value="2"},
               new SelectListItem(){Text="Out of process",Value="3"}
               };

            incidentViewModel.MojProcessStage = MojProcessStage;

            //Ambulance attend?
            IEnumerable<SelectListItem> AmbulanceAttend = new List<SelectListItem>{
               new SelectListItem(){Text="Yes",Value="0"},
               new SelectListItem(){Text="No",Value="1"},
               new SelectListItem(){Text="Unknown",Value="2"}
               };
            incidentViewModel.AmbulanceAttend = AmbulanceAttend;

            //Police Attend
            IEnumerable<SelectListItem> PoliceAttend = new List<SelectListItem>{
               new SelectListItem(){Text="Yes",Value="0"},
               new SelectListItem(){Text="No",Value="1"},
               new SelectListItem(){Text="Unknown",Value="2"}
               };
            incidentViewModel.PoliceAttend = PoliceAttend;

            return incidentViewModel;
        }

        public static PolicyViewModel PopulatePolicyViewModel(string NavigationData, bool Previous = false)
        {
            PolicyViewModel policyViewModel = new PolicyViewModel();

            if (HttpContext.Current.Session["policyViewModel"] != null)
            {
                policyViewModel = HttpContext.Current.Session["policyViewModel"] as PolicyViewModel;
            }
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                policyViewModel.NavigationData.Data = NavigationData;

            policyViewModel.NavigationData.CurrentForm = CurrentView.Policy.ToString();
            policyViewModel.NavigationData.Previous = Previous;

            IEnumerable<SelectListItem> PolicyTypes = new List<SelectListItem>{
               new SelectListItem(){Text="Commercial",Value="0"},
               new SelectListItem(){Text="Personal",Value="1"},
               new SelectListItem(){Text="Unknown",Value="2"}
               };
            policyViewModel.PolicyTypes = PolicyTypes;

            IEnumerable<SelectListItem> CoverTypes = new List<SelectListItem>{
               new SelectListItem(){Text="Comprehensive",Value="0"},
               new SelectListItem(){Text="Third Party Only",Value="1"},
               new SelectListItem(){Text="Third Party, Fire and Theft",Value="2"}
               };
            policyViewModel.CoverTypes = CoverTypes;

            return policyViewModel;
        }

        public static PolicyHolderPersonalViewModel PopulatePolicyHolderPersonalViewModel(string NavigationData = "", bool Previous = false)
        {
            PolicyHolderPersonalViewModel policyHolderPersonalViewModel = new PolicyHolderPersonalViewModel();

            if (HttpContext.Current.Session["policyHolderPersonalViewModel"] != null)
            {
                policyHolderPersonalViewModel = HttpContext.Current.Session["policyHolderPersonalViewModel"] as PolicyHolderPersonalViewModel;
            }
                
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                policyHolderPersonalViewModel.NavigationData.Data = NavigationData;

            policyHolderPersonalViewModel.NavigationData.CurrentForm = CurrentView.PolicyHolderPersonal.ToString();
            policyHolderPersonalViewModel.NavigationData.Previous = Previous;

            return policyHolderPersonalViewModel;
        }

        public static PolicyHolderCommercialViewModel PopulatePolicyHolderCommercialViewModel(string NavigationData = "", bool Previous = false)
        {
            PolicyHolderCommercialViewModel policyHolderCommercialViewmodel = new PolicyHolderCommercialViewModel();

            if (HttpContext.Current.Session["policyHolderCommercialViewmodel"] != null)
            {
                policyHolderCommercialViewmodel = HttpContext.Current.Session["policyHolderCommercialViewmodel"] as PolicyHolderCommercialViewModel;
            }
            //
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                policyHolderCommercialViewmodel.NavigationData.Data = NavigationData;

            policyHolderCommercialViewmodel.NavigationData.CurrentForm = CurrentView.PolicyHolderPersonal.ToString();
            policyHolderCommercialViewmodel.NavigationData.Previous = Previous;

            return policyHolderCommercialViewmodel;
        }

        public static InsuredVehicleViewModel PopulateInsuredVehicleViewModel(CurrentView PreviousPage = CurrentView.Unknown, 
                                                                            string NavigationData = "", 
                                                                            bool Previous = false)
        {
            InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();
            //bool InsuredVehicleFoundInSession = false;

            if (HttpContext.Current.Session["insuredVehicleViewModel"] != null)
            {
                insuredVehicleViewModel = HttpContext.Current.Session["insuredVehicleViewModel"] as InsuredVehicleViewModel;
                //InsuredVehicleFoundInSession = true;
            }
            else
            {

                insuredVehicleViewModel.PreviousPage = PreviousPage;
            }
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                insuredVehicleViewModel.NavigationData.Data = NavigationData;

            insuredVehicleViewModel.NavigationData.CurrentForm = CurrentView.InsuredVehicle.ToString();
            // Always set previous independent of session
            insuredVehicleViewModel.NavigationData.Previous = Previous;
            //
            //
            insuredVehicleViewModel.Vehicle.VehicleTypes = PopulateInsuredVehicleTypes();
            insuredVehicleViewModel.VehicleAdditionalInfo.Colours = PopulateInsuredVehicleColours();
            insuredVehicleViewModel.VehicleAdditionalInfo.Transmission = PopulateInsuredVehicleTransmissions();
            insuredVehicleViewModel.VehicleAdditionalInfo.Fuel = PopulateFuels();
            insuredVehicleViewModel.VehicleAdditionalInfo.TotalLossCapacity = PopulateInsuredVehicleTotalLossCapacity();

            return insuredVehicleViewModel;
        }

        public static InsuredDriverViewModel PopulateInsuredDriverViewModel(string NavigationData = "", bool Previous = false,string NextPage="")
        {
            InsuredDriverViewModel insuredDriverViewModel = new InsuredDriverViewModel();

           // bool InsuredDriverFoundInSession = false;

            if (HttpContext.Current.Session["insuredDriverViewModel"] != null)
            {
                insuredDriverViewModel = HttpContext.Current.Session["insuredDriverViewModel"] as InsuredDriverViewModel;
               // InsuredDriverFoundInSession = true;
            }

            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                insuredDriverViewModel.NavigationData.Data = NavigationData;

            insuredDriverViewModel.NavigationData.CurrentForm = CurrentView.InsuredDriver.ToString();
            insuredDriverViewModel.NavigationData.Previous = Previous;
            insuredDriverViewModel.NavigationData.NextPage = NextPage;
            
            //
            return insuredDriverViewModel;

        }

        public static SupportingInvolvementsViewModel PopulateSupportingInvolvementsViewModel(int NumSupportingInvolvements, int CurrSupportingInvolvement, 
                                                                                                SupportingInvolvementsType supportingInvolvementsType,
                                                                                                int ParentID, int VehicleID = -1, string NavigationData = "",
                                                                                                bool Previous = false, string NextPage = "")
        {
            bool SuppInvolvementFoundInSession=false;
            SupportingInvolvementsViewModel supportingInvolvementsViewModel = new SupportingInvolvementsViewModel();

            //if (PopulateFromSession)
            //{
                if (supportingInvolvementsType == SupportingInvolvementsType.InsuredDriver)
                {

                    List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                    insuredDriverSupportingInvolvements = (List<SupportingInvolvementsViewModel>)HttpContext.Current.Session["InsuredDriverSupportingInvolvements"];

                    if (insuredDriverSupportingInvolvements != null)
                    {
                        //if (insuredDriverSupportingInvolvements.Count() < NumSupportingInvolvements)
                        //{
                        //    int numSupportInvolvements = NumSupportingInvolvements - insuredDriverSupportingInvolvements.Count();

                            

                        //    for (int i = 0; i < numSupportInvolvements; i++)
                        //    {
                        //        //add new model to end of list
                        //        insuredDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        //        HttpContext.Current.Session["InsuredDriverSupportingInvolvements"] = insuredDriverSupportingInvolvements;
                        //    }
                        //}
                        
                        // subtract one from CurrSupportingInvolvement to account for zero based List index
                        if (CurrSupportingInvolvement - 1 < insuredDriverSupportingInvolvements.Count())
                        {
                            // supportingInvolvementsViewModel.SupportingInvolvementsType = supportingInvolvementsType;
                            supportingInvolvementsViewModel = insuredDriverSupportingInvolvements[CurrSupportingInvolvement - 1];
                            SuppInvolvementFoundInSession = true;
                        }
                    }
                }
                else if (supportingInvolvementsType == SupportingInvolvementsType.InsuredPassenger)
                {
                    Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                    List<SupportingInvolvementsViewModel> PassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                    //if (HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] != null)
                    //{
                        //Get dictionary from session
                        DictionaryPassengerSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"];

                        if (DictionaryPassengerSupportingInvolvements != null)
                        {
                            // Check if model already exists in List
                            if (DictionaryPassengerSupportingInvolvements.ContainsKey(ParentID))
                            {
                                PassengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements[ParentID];
                                if (CurrSupportingInvolvement - 1 < PassengerSupportingInvolvements.Count())
                                {

                                    supportingInvolvementsViewModel = PassengerSupportingInvolvements[CurrSupportingInvolvement - 1];
                                    SuppInvolvementFoundInSession = true;
                                }
                            }
                        }
                }
                else if (supportingInvolvementsType == SupportingInvolvementsType.ThirdPartyDriver)
                {
                    Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyDriverSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();
                    List<SupportingInvolvementsViewModel> ThirdPartyDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                    DictionaryThirdPartyDriverSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"];

                    if (DictionaryThirdPartyDriverSupportingInvolvements != null)
                    {
                        // Check if model already exists in List
                        if (DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(ParentID))
                        {
                            ThirdPartyDriverSupportingInvolvements = DictionaryThirdPartyDriverSupportingInvolvements[ParentID];
                            if (CurrSupportingInvolvement - 1 < ThirdPartyDriverSupportingInvolvements.Count())
                            {
                                supportingInvolvementsViewModel = ThirdPartyDriverSupportingInvolvements[CurrSupportingInvolvement - 1];
                                SuppInvolvementFoundInSession = true;
                            }
                        }
                    }
                    //
                }
                else if (supportingInvolvementsType == SupportingInvolvementsType.ThirdPartyPassenger)
                {
                    Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>> DictionaryThirdPartyVehicleSupportingInvolvements = new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
                    Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                    List<SupportingInvolvementsViewModel> ThirdPartyPassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                    var myObject = HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];

                    if (myObject is Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)
                    {
                        DictionaryThirdPartyVehicleSupportingInvolvements = (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)myObject;
                    }

                    DictionaryThirdPartyVehicleSupportingInvolvements = (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];


                    if (DictionaryThirdPartyVehicleSupportingInvolvements != null)
                    {
                        // Check if model already exists in List
                        if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(VehicleID))
                        {

                            DictionaryThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyVehicleSupportingInvolvements[VehicleID];

                            if (DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(ParentID))
                            {
                                ThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyPassengerSupportingInvolvements[ParentID];

                                if (CurrSupportingInvolvement - 1 < ThirdPartyPassengerSupportingInvolvements.Count())
                                {
                                    supportingInvolvementsViewModel = ThirdPartyPassengerSupportingInvolvements[CurrSupportingInvolvement - 1];
                                    SuppInvolvementFoundInSession = true;
                                }
                            }
                        }
                    }

                }
                //
            if (!SuppInvolvementFoundInSession)
            {
                supportingInvolvementsViewModel.SupportingInvolvementsType = supportingInvolvementsType;
                supportingInvolvementsViewModel.ParentID = ParentID;
                supportingInvolvementsViewModel.NumberSupportingInvolvements = NumSupportingInvolvements;
                supportingInvolvementsViewModel.CurrentSupportingInvolvement = CurrSupportingInvolvement;
                supportingInvolvementsViewModel.VehicleID = VehicleID;
            }
            //else
            //{
            //    supportingInvolvementsViewModel.NumberSupportingInvolvements = NumSupportingInvolvements;

            //}

            // Populate lookups
            supportingInvolvementsViewModel.OrganisationInvolvement.OrganisationInvolvements = PopulateOrganisationInvolvements();
            //supportingInvolvementsViewModel.Address.AddressStatus = PopulateAddressStatuses();
            supportingInvolvementsViewModel.Vehicle.VehicleTypes = PopulateInsuredVehicleTypes();
            supportingInvolvementsViewModel.VehicleAdditionalInfo.Colours = PopulateInsuredVehicleColours();
            supportingInvolvementsViewModel.VehicleAdditionalInfo.Transmission = PopulateInsuredVehicleTransmissions();
            supportingInvolvementsViewModel.VehicleAdditionalInfo.Fuel = PopulateFuels();
            supportingInvolvementsViewModel.VehicleAdditionalInfo.TotalLossCapacity = PopulateInsuredVehicleTotalLossCapacity();

            

            switch (supportingInvolvementsViewModel.SupportingInvolvementsType)
            {
                case SupportingInvolvementsType.InsuredDriver:
                    supportingInvolvementsViewModel.NavigationData.CurrentForm = CurrentView.SupportingInvolvementInsuredDriver.ToString();
                    break;
                case SupportingInvolvementsType.InsuredPassenger:
                    supportingInvolvementsViewModel.NavigationData.CurrentForm = CurrentView.SupportingInvolvementInsuredPassenger.ToString();
                    break;
                case SupportingInvolvementsType.ThirdPartyDriver:
                    supportingInvolvementsViewModel.NavigationData.CurrentForm = CurrentView.SupportingInvolvementThirdPartyDriver.ToString();
                    break;
                case SupportingInvolvementsType.ThirdPartyPassenger:
                    supportingInvolvementsViewModel.NavigationData.CurrentForm = CurrentView.SupportingInvolvementThirdPartyPassenger.ToString();
                    break;
               
            }
            
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                supportingInvolvementsViewModel.NavigationData.Data = NavigationData;

            supportingInvolvementsViewModel.NavigationData.Previous = Previous;
            supportingInvolvementsViewModel.NavigationData.NextPage = NextPage;

            return supportingInvolvementsViewModel;

        }

        

        //
        //
        //
        public static PassengerViewModel PopulatePassengerViewModel(int CurrentPassenger, 
                                                                    CurrentView PreviousPage, 
                                                                    int PreviousPageID,
                                                                    PassengerType PassengerType,
                                                                    int VehicleID, string NavigationData = "", bool Previous = false,string NextPage="")
        {
            PassengerViewModel passengerViewModel = new PassengerViewModel();
            bool PassengerFoundInSession = false;
            int NumberPassengers=0;

            if (PassengerType == PassengerType.InsuredVehiclePassenger)
            {
                NumberPassengers = InsuredVehicleNumberPassengers();

                List<PassengerViewModel> insuredDriverPassengers = new List<PassengerViewModel>();

                insuredDriverPassengers = (List<PassengerViewModel>)HttpContext.Current.Session["InsuredDriverPassengers"];
                // subtract one from CurrSupportingInvolvement to account for zero based List index
                if (insuredDriverPassengers != null)
                {
                    if (CurrentPassenger - 1 < insuredDriverPassengers.Count())
                    {
                        passengerViewModel = insuredDriverPassengers[CurrentPassenger - 1];
                        PassengerFoundInSession = true;
                    }
                }
            }
            else if (PassengerType == PassengerType.ThirdPartyVehiclePassenger)
            {
                NumberPassengers = ThirdPartyVehicleNumberPassengers(VehicleID);

                Dictionary<int, List<PassengerViewModel>> DictionaryThirdPartyPassengers = new Dictionary<int, List<PassengerViewModel>>();
                List<PassengerViewModel> ThirdPartyPassengers = new List<PassengerViewModel>();

                DictionaryThirdPartyPassengers = (Dictionary<int, List<PassengerViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyPassengers"];

                if (DictionaryThirdPartyPassengers != null)
                {
                    // Check if model already exists in List
                    if (DictionaryThirdPartyPassengers.ContainsKey(VehicleID))
                    {
                        ThirdPartyPassengers = DictionaryThirdPartyPassengers[VehicleID];
                        if (CurrentPassenger - 1 < ThirdPartyPassengers.Count())
                        {
                            passengerViewModel = ThirdPartyPassengers[CurrentPassenger - 1];
                            PassengerFoundInSession = true;
                        }
                    }
                }
            }
            //
            if(!PassengerFoundInSession)
            {
                passengerViewModel.CurrentPassenger = CurrentPassenger;
                passengerViewModel.NumberPassengers = NumberPassengers;
                passengerViewModel.PassengerID = CurrentPassenger;

                passengerViewModel.PreviousPage = PreviousPage;
                passengerViewModel.PreviousPageID = PreviousPageID;
                passengerViewModel.VehicleID = VehicleID;
                passengerViewModel.PassengerType = PassengerType;
                //

            }
            //
           
            //
            if (PassengerType == PassengerType.InsuredVehiclePassenger)
                passengerViewModel.NavigationData.CurrentForm = PassengerType.InsuredVehiclePassenger.ToString();
            else
                passengerViewModel.NavigationData.CurrentForm = PassengerType.ThirdPartyVehiclePassenger.ToString();
            //
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                passengerViewModel.NavigationData.Data = NavigationData;

            passengerViewModel.NavigationData.Previous = Previous;
            passengerViewModel.NavigationData.NextPage = NextPage;

            return passengerViewModel;
        }
        //
        //
        //
        public static ThirdPartyVehicleViewModel PopulateThirdPartyVehicleViewModel(int CurrentVehicleID, CurrentView PreviousPage, string NavigationData = "", bool Previous = false)
        {
            bool VehicleFoundInSession = false;
            ThirdPartyVehicleViewModel thirdPartyVehicleViewModel = new ThirdPartyVehicleViewModel();
            Dictionary<int, ThirdPartyVehicleViewModel> DictionaryThirdPartyVehicleViewModels = new Dictionary<int, ThirdPartyVehicleViewModel>();

            if (HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] != null)
            {
                DictionaryThirdPartyVehicleViewModels = HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] as Dictionary<int, ThirdPartyVehicleViewModel>;

                if (DictionaryThirdPartyVehicleViewModels.ContainsKey(CurrentVehicleID))
                {
                    //
                    thirdPartyVehicleViewModel = DictionaryThirdPartyVehicleViewModels[CurrentVehicleID];
                    VehicleFoundInSession = true;
                }
            }
            if (!VehicleFoundInSession)
            {
                thirdPartyVehicleViewModel.PreviousPage = PreviousPage;
                thirdPartyVehicleViewModel.CurrentVehicle = CurrentVehicleID;
                thirdPartyVehicleViewModel.NumberOfVehicles = NumberOfThirdPartyVehicles();
                thirdPartyVehicleViewModel.VehicleID = CurrentVehicleID;
                //
                //

            }

            thirdPartyVehicleViewModel.Vehicle.VehicleTypes = PopulateInsuredVehicleTypes();
            thirdPartyVehicleViewModel.VehicleAdditionalInfo.Colours = PopulateInsuredVehicleColours();
            thirdPartyVehicleViewModel.VehicleAdditionalInfo.Transmission = PopulateInsuredVehicleTransmissions();
            thirdPartyVehicleViewModel.VehicleAdditionalInfo.Fuel = PopulateFuels();
            thirdPartyVehicleViewModel.VehicleAdditionalInfo.TotalLossCapacity = PopulateInsuredVehicleTotalLossCapacity();

            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                thirdPartyVehicleViewModel.NavigationData.Data = NavigationData;
            //
            thirdPartyVehicleViewModel.NavigationData.CurrentForm = CurrentView.ThirdPartyVehicle.ToString();
            thirdPartyVehicleViewModel.NavigationData.Previous = Previous;
            //

            return thirdPartyVehicleViewModel;
        }
        //
        //
        //
        public static WitnessViewModel PopulateWitnessViewModel(int CurrentWitnessID, CurrentView PreviousPage, string NavigationData = "", bool Previous = false)
        {
            bool WitnessFoundInSession = false;
            WitnessViewModel witnessViewModel = new WitnessViewModel();

            Dictionary<int, WitnessViewModel> DictionaryWitnessViewModels = new Dictionary<int, WitnessViewModel>();

            if (HttpContext.Current.Session["DictionaryWitnessViewModels"] != null)
            {
                DictionaryWitnessViewModels = HttpContext.Current.Session["DictionaryWitnessViewModels"] as Dictionary<int, WitnessViewModel>;

                if (DictionaryWitnessViewModels.ContainsKey(CurrentWitnessID))
                {
                    //
                    witnessViewModel = DictionaryWitnessViewModels[CurrentWitnessID];
                    WitnessFoundInSession = true;
                    witnessViewModel.NumberOfWitnesses = NumberOfWitnesses();
                }
            }
            if (!WitnessFoundInSession)
            {
                witnessViewModel.PreviousPage = PreviousPage;
                witnessViewModel.CurrentWitness = CurrentWitnessID;
                witnessViewModel.NumberOfWitnesses = NumberOfWitnesses();
               // witnessViewModel.VehicleID = CurrentVehicleID;
                witnessViewModel.NavigationData.Data = NavigationData;
            }
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                witnessViewModel.NavigationData.Data = NavigationData;
           
            witnessViewModel.NavigationData.CurrentForm = CurrentView.Witness.ToString();
            witnessViewModel.NavigationData.Previous = Previous;
            //
            return witnessViewModel;
        }
        //
        //
        //
        public static ThirdPartyDriverViewModel PopulateThirdPartyDriverViewModel(int CurrentVehicleID, CurrentView PreviousPage, 
                                                                                string NavigationData = "", bool Previous = false,string NextPage="")
        {
            ThirdPartyDriverViewModel thirdPartyDriverViewModel = new ThirdPartyDriverViewModel();

            bool DriverFoundInSession = false;

            Dictionary<int, ThirdPartyDriverViewModel> DictionaryThirdPartyDriverViewModels = new Dictionary<int, ThirdPartyDriverViewModel>();

            if (HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] != null)
            {
                DictionaryThirdPartyDriverViewModels = HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] as Dictionary<int, ThirdPartyDriverViewModel>;

                if (DictionaryThirdPartyDriverViewModels.ContainsKey(CurrentVehicleID))
                {
                    //
                    thirdPartyDriverViewModel = DictionaryThirdPartyDriverViewModels[CurrentVehicleID];
                    DriverFoundInSession = true;
                }
            }
            if (!DriverFoundInSession)
            {
                thirdPartyDriverViewModel.PreviousPage = PreviousPage;
                thirdPartyDriverViewModel.CurrentVehicle = CurrentVehicleID;
                thirdPartyDriverViewModel.NumberOfVehicles=NumberOfThirdPartyVehicles();
                thirdPartyDriverViewModel.VehicleID = CurrentVehicleID;
                //

               
            }
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                thirdPartyDriverViewModel.NavigationData.Data = NavigationData;

            thirdPartyDriverViewModel.NavigationData.CurrentForm = CurrentView.ThirdPartyDriver.ToString();
            thirdPartyDriverViewModel.NavigationData.Previous = Previous;
            thirdPartyDriverViewModel.NavigationData.NextPage = NextPage;

            return thirdPartyDriverViewModel;

        }

        public static ReviewAndSubmitViewModel PopulateReviewAndSubmitViewModel(int NumberOfVehicles, int CurrentVehicleID, 
                                                                                CurrentView PreviousPage, string NavigationData = "", bool Previous = false)
        {
            ReviewAndSubmitViewModel reviewAndSubmitViewModel = new ReviewAndSubmitViewModel();
            bool ReviewAndSubmitFoundInSession = false;

            if (HttpContext.Current.Session["ReviewAndSubmitViewModel"] != null)
            {
                reviewAndSubmitViewModel = HttpContext.Current.Session["ReviewAndSubmitViewModel"] as ReviewAndSubmitViewModel;
                ReviewAndSubmitFoundInSession = true;
            }
            //
            if (!ReviewAndSubmitFoundInSession)
            {
                reviewAndSubmitViewModel.PreviousPage = PreviousPage;
                reviewAndSubmitViewModel.CurrentVehicle = CurrentVehicleID;
                reviewAndSubmitViewModel.NumberOfVehicles = NumberOfVehicles;
                reviewAndSubmitViewModel.VehicleID = CurrentVehicleID;
                //

                var dataFeeds = new[] 
                {
                    new DataFeeds { DataFeed = "Keoghs Fraud Database", IsChecked = true, Included="Included" },
                    new DataFeeds { DataFeed = "Companies House", IsChecked = true , Included="Included"},
                    new DataFeeds { DataFeed = "Tracesmart", IsChecked = false , Included="Additional Fee"},
                    new DataFeeds { DataFeed = "CUE", IsChecked = false , Included="Additional Fee" }
                };
                //
                reviewAndSubmitViewModel.DataFeeds = dataFeeds;
            }
            //
            // Always update Navigation Data if Previous is false
            // in case the user has changed data on previous form
            if (Previous == false)
                reviewAndSubmitViewModel.NavigationData.Data = NavigationData;

            reviewAndSubmitViewModel.NavigationData.CurrentForm = CurrentView.ReviewAndSubmit.ToString();
            reviewAndSubmitViewModel.NavigationData.Previous = Previous;
            // 
            return reviewAndSubmitViewModel;
        }

        #endregion

        #region business logic methods

        public static CurrentView GetPreviousPage(CurrentView CurrentPageType, int TotalNumPages, int CurrentPage)
        {

            // 
            switch (CurrentPageType)
            {
                case CurrentView.Incident:

                    break;
                case CurrentView.Policy:

                    break;
                case CurrentView.PolicyHolderPersonal:

                    break;
                case CurrentView.PolicyHolderCommercial:

                    break;
                case CurrentView.SupportingInvolvementInsuredDriver:
                    if (CurrentPage == 1)
                        return CurrentView.InsuredDriver;
                    else
                        return CurrentView.SupportingInvolvementInsuredDriver;

                case CurrentView.SupportingInvolvementInsuredPassenger:
                    if (CurrentPage == 1)
                        return CurrentView.Passenger;
                    else
                        return CurrentView.SupportingInvolvementInsuredPassenger;
                case CurrentView.SupportingInvolvementThirdPartyDriver:
                    if (CurrentPage == 1)
                        return CurrentView.ThirdPartyDriver;
                    else
                        return CurrentView.SupportingInvolvementThirdPartyDriver;
                case CurrentView.SupportingInvolvementThirdPartyPassenger:
                    if (CurrentPage == 1)
                        return CurrentView.Passenger;
                    else
                        return CurrentView.SupportingInvolvementThirdPartyPassenger;
                default:
                    return CurrentView.Unknown;
                    //break;
            }
            return CurrentView.Unknown;

        }

        public static int InsuredVehicleNumberPassengers()
        {
            InsuredVehicleViewModel insuredVehicleViewModel = new InsuredVehicleViewModel();

            if (HttpContext.Current.Session["insuredVehicleViewModel"] != null)
            {
                insuredVehicleViewModel = HttpContext.Current.Session["insuredVehicleViewModel"] as InsuredVehicleViewModel;
            }
            return insuredVehicleViewModel.Vehicle.NumberOfPassengers;
        }

        public static int ThirdPartyVehicleNumberPassengers(int VehicleID)
        {
            ThirdPartyVehicleViewModel thirdPartyVehicleViewModel = new ThirdPartyVehicleViewModel();

            Dictionary<int, ThirdPartyVehicleViewModel> DictionaryThirdPartyVehicleViewModels = new Dictionary<int, ThirdPartyVehicleViewModel>();

            //Get dictionary from session
            DictionaryThirdPartyVehicleViewModels = (Dictionary<int, ThirdPartyVehicleViewModel>)HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"];
            // Check if model already exists in List, if not add it
            if (DictionaryThirdPartyVehicleViewModels.ContainsKey(VehicleID))
            {
                //Update the model
                thirdPartyVehicleViewModel = DictionaryThirdPartyVehicleViewModels[VehicleID];
            }

            return thirdPartyVehicleViewModel.Vehicle.NumberOfPassengers;
        }

        public static int NumberOfThirdPartyVehicles()
        {
            IncidentViewModel incidentViewModel = new IncidentViewModel();

            if (HttpContext.Current.Session["incidentViewModel"] != null)
            {
                incidentViewModel = HttpContext.Current.Session["incidentViewModel"] as IncidentViewModel;
            }

            return incidentViewModel.SelectedNumberOfVehicles - 1;

        }

        public static int NumberOfWitnesses()
        {
            IncidentViewModel incidentViewModel = new IncidentViewModel();

            if (HttpContext.Current.Session["incidentViewModel"] != null)
            {
                incidentViewModel = HttpContext.Current.Session["incidentViewModel"] as IncidentViewModel;
            }

            return incidentViewModel.SelectedNumberOfWitnesses;

        }

        public static int GetNumberofSupportingInvolvementsForPassenger(int PassengerID)
        {
            Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();
            List<SupportingInvolvementsViewModel> PassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

            //if (HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] != null)
            //{
            //Get dictionary from session
            DictionaryPassengerSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"];
            // Check if model already exists in List
            if (DictionaryPassengerSupportingInvolvements.ContainsKey(PassengerID))
            {
                PassengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements[PassengerID];
                return PassengerSupportingInvolvements.Count();
            }
            else
                return -1;
        }
        //
        //
        //
        public static int GetNumberofSupportingInvolvementsForThirdPartyPassenger(int VehicleID, int PassengerID, bool GetLastPassenger=false)
        {
            
            Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>> DictionaryThirdPartyVehicleSupportingInvolvements = new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
            Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

            List<SupportingInvolvementsViewModel> ThirdPartyPassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

            //Get dictionary from session
            DictionaryThirdPartyVehicleSupportingInvolvements = (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];

            // Check if model already exists in List, if not add it
            if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(VehicleID))
            {
                DictionaryThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyVehicleSupportingInvolvements[VehicleID];

                if (GetLastPassenger)
                {
                    return DictionaryThirdPartyPassengerSupportingInvolvements.Count();
                }
                else if (DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(PassengerID))
                {
                    ThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyPassengerSupportingInvolvements[PassengerID];
                    //
                    // 
                    return ThirdPartyPassengerSupportingInvolvements.Count();
                }
                else
                    return -1;
            }
            else
                return -1;
        }
        //
        //
        //
        public static int GetNumberofSupportingInvolvementsForInsuredDriver()
        {
            List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
            //insuredDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);

            insuredDriverSupportingInvolvements = (List<SupportingInvolvementsViewModel>)HttpContext.Current.Session["InsuredDriverSupportingInvolvements"];
            return insuredDriverSupportingInvolvements.Count();
        }
        //
        //
        //
        public static int GetNumberofSupportingInvolvementsForThirdPartyDriver(int VehicleID)
        {
            List<SupportingInvolvementsViewModel> thirdPartyDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

            Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyDriverSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

            //Get dictionary from session
            DictionaryThirdPartyDriverSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"];
            // Check if model already exists in List, if not add it
            if (DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(VehicleID))
            {
                thirdPartyDriverSupportingInvolvements = DictionaryThirdPartyDriverSupportingInvolvements[VehicleID];
                //
                return thirdPartyDriverSupportingInvolvements.Count();
            }
            else
                return -1;
           
        }
        //
        //
        //
        public static string FormatPersonName(PersonInformation myPerson)
        {
            
            string PersonName = myPerson.Middlename != "" ? myPerson.Firstname + " " + myPerson.Middlename + " " + myPerson.Surname : myPerson.Firstname + " " + myPerson.Surname;

            return PersonName;

        }
        #endregion

        #region store view models
        //
        //
        public static void StoreClaimViewModel(ClaimViewModel claimViewModel)
        {
            HttpContext.Current.Session["claimViewModel"] = claimViewModel;
        }
        //
        //
        public static void StoreIncidentViewModel(IncidentViewModel incidentViewModel)
        {
            HttpContext.Current.Session["incidentViewModel"] = incidentViewModel;
        }
        //
        //
        public static void StorePolicyViewModel(PolicyViewModel policyViewModel)
        {
            HttpContext.Current.Session["policyViewModel"] = policyViewModel;
        }
        //
        //
        public static void StorePolicyHolderViewModel(PolicyHolderPersonalViewModel policyHolderPersonalViewModel)
        {
            HttpContext.Current.Session["policyHolderPersonalViewModel"] = policyHolderPersonalViewModel;
        }
        //
        //
        public static void StorePolicyHolderCommercialViewModel(PolicyHolderCommercialViewModel policyHolderCommercialViewModel)
        {
            HttpContext.Current.Session["policyHolderCommercialViewModel"] = policyHolderCommercialViewModel;
        }
        //
        //
        public static void StoreInsuredVehicleViewModel(InsuredVehicleViewModel insuredVehicleViewModel)
        {
            HttpContext.Current.Session["insuredVehicleViewModel"] = insuredVehicleViewModel;
        }
        //
        //
        public static void StoreInsuredDriverViewModel(InsuredDriverViewModel insuredDriverViewModel)
        {
            HttpContext.Current.Session["insuredDriverViewModel"] = insuredDriverViewModel;
        }
        //
        //
        public static void StoreSupportingInvolvementsViewModel(SupportingInvolvementsViewModel supportingInvolvementsViewModel, 
                                                                SupportingInvolvementsType supportingInvolvementsType,
                                                                int PassengerID=-1,
                                                                int VehicleID = -1)
        {
            if (supportingInvolvementsType == SupportingInvolvementsType.InsuredDriver)
            {
                List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                if (HttpContext.Current.Session["InsuredDriverSupportingInvolvements"] == null)
                {
                    //List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
                    insuredDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                    HttpContext.Current.Session["InsuredDriverSupportingInvolvements"] = insuredDriverSupportingInvolvements;
                }
                else
                {
                    //Get List from Session
                    insuredDriverSupportingInvolvements = (List<SupportingInvolvementsViewModel> )HttpContext.Current.Session["InsuredDriverSupportingInvolvements"];
                    // Check if model already exists in List if it is update it
                    if (supportingInvolvementsViewModel.CurrentSupportingInvolvement > insuredDriverSupportingInvolvements.Count())
                    {
                        //add new model to end of list
                        insuredDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        HttpContext.Current.Session["InsuredDriverSupportingInvolvements"] = insuredDriverSupportingInvolvements;
                    }
                    else //if (insuredDriverSupportingInvolvements[supportingInvolvementsViewModel.CurrentSupportingInvolvement-1] != null)
                    {
                        //an update of the model
                        insuredDriverSupportingInvolvements[supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1] = supportingInvolvementsViewModel;
                    }
                }
            }
            else if (supportingInvolvementsType == SupportingInvolvementsType.InsuredPassenger)
            {
                  List<SupportingInvolvementsViewModel> passengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                if (HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] == null)
                {
                    //
                    passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                    DictionaryPassengerSupportingInvolvements.Add(PassengerID, passengerSupportingInvolvements);
                    HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = DictionaryPassengerSupportingInvolvements;
                }
                else
                {
                    //Get dictionary from session
                    DictionaryPassengerSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"];
                    // Check if model already exists in List, if not add it
                    if (DictionaryPassengerSupportingInvolvements.ContainsKey(PassengerID))
                    {
                        passengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements[PassengerID];
                        //
                        // Do we need to add the Supporting Involvement?
                        if (supportingInvolvementsViewModel.CurrentSupportingInvolvement > passengerSupportingInvolvements.Count())
                        {
                            passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                            HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = DictionaryPassengerSupportingInvolvements;
                        }
                        else
                        {
                            // do update
                            //an update of the model
                            passengerSupportingInvolvements[supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1] = supportingInvolvementsViewModel;
                        }
                    }
                    else
                    {
                        //need to add supporting involvement for new passenger
                        passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        DictionaryPassengerSupportingInvolvements.Add(PassengerID, passengerSupportingInvolvements);
                        HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = DictionaryPassengerSupportingInvolvements;
                    }
                }
            }
            else if (supportingInvolvementsType == SupportingInvolvementsType.ThirdPartyDriver)
            {
                 List<SupportingInvolvementsViewModel> thirdPartyDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyDriverSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                if (HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] == null)
                {
                    //
                    thirdPartyDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                    DictionaryThirdPartyDriverSupportingInvolvements.Add(VehicleID, thirdPartyDriverSupportingInvolvements);
                    HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] = DictionaryThirdPartyDriverSupportingInvolvements;
                }
                else
                {
                    //Get dictionary from session
                    DictionaryThirdPartyDriverSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"];
                    // Check if model already exists in List, if not add it
                    if (DictionaryThirdPartyDriverSupportingInvolvements.ContainsKey(VehicleID))
                    {
                        thirdPartyDriverSupportingInvolvements = DictionaryThirdPartyDriverSupportingInvolvements[VehicleID];
                        //
                        // Do we need to add the Supporting Involvement?
                        if (supportingInvolvementsViewModel.CurrentSupportingInvolvement > thirdPartyDriverSupportingInvolvements.Count())
                        {
                            thirdPartyDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                            HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] = DictionaryThirdPartyDriverSupportingInvolvements;
                        }
                        else
                        {
                            // do update
                            //an update of the model
                            thirdPartyDriverSupportingInvolvements[supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1] = supportingInvolvementsViewModel;
                        }
                    }
                    else
                    {
                        // Add supp involvements for new vehicle
                        thirdPartyDriverSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        DictionaryThirdPartyDriverSupportingInvolvements.Add(VehicleID, thirdPartyDriverSupportingInvolvements);
                        HttpContext.Current.Session["DictionaryThirdPartyDriverSupportingInvolvements"] = DictionaryThirdPartyDriverSupportingInvolvements;

                    }
                }
            }
            else if (supportingInvolvementsType == SupportingInvolvementsType.ThirdPartyPassenger)
            {
                Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>> DictionaryThirdPartyVehicleSupportingInvolvements = new Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>();
                Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryThirdPartyPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

                List<SupportingInvolvementsViewModel> ThirdPartyPassengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

                if (HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] == null)
                {
                    ThirdPartyPassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                    DictionaryThirdPartyPassengerSupportingInvolvements.Add(PassengerID, ThirdPartyPassengerSupportingInvolvements);

                    DictionaryThirdPartyVehicleSupportingInvolvements.Add(VehicleID, DictionaryThirdPartyPassengerSupportingInvolvements);

                    HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;
                }
                else
                {
                    //Get dictionary from session
                    DictionaryThirdPartyVehicleSupportingInvolvements = (Dictionary<int, Dictionary<int, List<SupportingInvolvementsViewModel>>>)HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"];
                    
                    // Check if model already exists in List, if not add it
                    if (DictionaryThirdPartyVehicleSupportingInvolvements.ContainsKey(VehicleID))
                    {
                        DictionaryThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyVehicleSupportingInvolvements[VehicleID];

                        if (DictionaryThirdPartyPassengerSupportingInvolvements.ContainsKey(PassengerID))
                        {
                            ThirdPartyPassengerSupportingInvolvements = DictionaryThirdPartyPassengerSupportingInvolvements[PassengerID];
                            //
                            // Do we need to add the Supporting Involvement?
                            if (supportingInvolvementsViewModel.CurrentSupportingInvolvement > ThirdPartyPassengerSupportingInvolvements.Count())
                            {
                                ThirdPartyPassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);

                                // DictionaryThirdPartyVehicleSupportingInvolvements.Add(VehicleID, ThirdPartyPassengerSupportingInvolvements);
                                HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;
                            }
                            else
                            {
                                // do update
                                //an update of the model
                                ThirdPartyPassengerSupportingInvolvements[supportingInvolvementsViewModel.CurrentSupportingInvolvement - 1] = supportingInvolvementsViewModel;
                                HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;
                            }
                        }
                        else
                        {
                            // add dictionary for new passenger
                            ThirdPartyPassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                            DictionaryThirdPartyPassengerSupportingInvolvements.Add(PassengerID, ThirdPartyPassengerSupportingInvolvements);
                            // update dictionary of passengers for vehicle.
                            DictionaryThirdPartyVehicleSupportingInvolvements[VehicleID] = DictionaryThirdPartyPassengerSupportingInvolvements;

                           // DictionaryThirdPartyVehicleSupportingInvolvements.Add(VehicleID, DictionaryThirdPartyPassengerSupportingInvolvements);

                            HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;

                        }
                    }
                    else
                    {
                        // Add supp involvements for new vehicle
                        ThirdPartyPassengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                        //DictionaryThirdPartyPassengerSupportingInvolvements.Add(VehicleID, ThirdPartyPassengerSupportingInvolvements);

                        DictionaryThirdPartyPassengerSupportingInvolvements.Add(PassengerID, ThirdPartyPassengerSupportingInvolvements);

                        DictionaryThirdPartyVehicleSupportingInvolvements.Add(VehicleID, DictionaryThirdPartyPassengerSupportingInvolvements);

                        HttpContext.Current.Session["DictionaryThirdPartyVehicleSupportingInvolvements"] = DictionaryThirdPartyVehicleSupportingInvolvements;

                    }
                }

            }
           
        }

        //public static void StorePassengerSupportingInvolvementsViewModel(SupportingInvolvementsViewModel supportingInvolvementsViewModel, int PassengerID)
        //{
           
        //        List<SupportingInvolvementsViewModel> passengerSupportingInvolvements = new List<SupportingInvolvementsViewModel>();

        //        Dictionary<int, List<SupportingInvolvementsViewModel>> DictionaryPassengerSupportingInvolvements = new Dictionary<int, List<SupportingInvolvementsViewModel>>();

        //        if (HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] == null)
        //        {
        //            //
        //            passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
        //            DictionaryPassengerSupportingInvolvements.Add(PassengerID, passengerSupportingInvolvements);
        //            HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = DictionaryPassengerSupportingInvolvements;
        //        }
        //        else
        //        {
        //            //Get dictionary from session
        //            DictionaryPassengerSupportingInvolvements = (Dictionary<int, List<SupportingInvolvementsViewModel>>)HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"];
        //            // Check if model already exists in List, if not add it
        //            if (DictionaryPassengerSupportingInvolvements.ContainsKey(PassengerID))
        //            {
        //                passengerSupportingInvolvements = DictionaryPassengerSupportingInvolvements[PassengerID];
        //                //
        //                // Do we need to add the Supporting Involvement?
        //                if (supportingInvolvementsViewModel.CurrentSupportingInvolvement > passengerSupportingInvolvements.Count())
        //                {
        //                    passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
        //                    HttpContext.Current.Session["DictionaryPassengerSupportingInvolvements"] = DictionaryPassengerSupportingInvolvements;
        //                }
                        
                       
        //            }
                    

        //        }
            
        //}

        public static void StorePassengerViewModel(PassengerViewModel passengerViewModel, PassengerType PassengerType, int VehicleID=-1)
        {
            List<PassengerViewModel> insuredDriverPassengers = new List<PassengerViewModel>();

            if (PassengerType == PassengerType.InsuredVehiclePassenger)
            {
                if (HttpContext.Current.Session["InsuredDriverPassengers"] == null)
                {
                    //List<SupportingInvolvementsViewModel> insuredDriverSupportingInvolvements = new List<SupportingInvolvementsViewModel>();
                    insuredDriverPassengers.Add(passengerViewModel);
                    HttpContext.Current.Session["InsuredDriverPassengers"] = insuredDriverPassengers;
                }
                else
                {
                    //Get List from Session
                    insuredDriverPassengers = (List<PassengerViewModel>)HttpContext.Current.Session["InsuredDriverPassengers"];
                    // Check if model already exists in List
                    if (passengerViewModel.CurrentPassenger > insuredDriverPassengers.Count())
                    {
                        insuredDriverPassengers.Add(passengerViewModel);
                        HttpContext.Current.Session["InsuredDriverPassengers"] = insuredDriverPassengers;
                    }
                    else
                    {
                        // Update the model in session
                        insuredDriverPassengers[passengerViewModel.CurrentPassenger - 1] = passengerViewModel;
                    }
                }
            }
            else if (PassengerType == PassengerType.ThirdPartyVehiclePassenger)
            {
               // NumberPassengers = ThirdPartyVehicleNumberPassengers(VehicleID);

                Dictionary<int, List<PassengerViewModel>> DictionaryThirdPartyPassengers = new Dictionary<int, List<PassengerViewModel>>();
                List<PassengerViewModel> ThirdPartyPassengers = new List<PassengerViewModel>();

                if (HttpContext.Current.Session["DictionaryThirdPartyPassengers"] == null)
                {
                    ThirdPartyPassengers.Add(passengerViewModel);
                    DictionaryThirdPartyPassengers.Add(VehicleID, ThirdPartyPassengers);
                    HttpContext.Current.Session["DictionaryThirdPartyPassengers"] = DictionaryThirdPartyPassengers;

                }
                else
                {
                    //Get List from Session
                    DictionaryThirdPartyPassengers = (Dictionary<int, List<PassengerViewModel>>)HttpContext.Current.Session["DictionaryThirdPartyPassengers"];

                    // Check if model already exists in List
                    if (DictionaryThirdPartyPassengers.ContainsKey(VehicleID))
                    {
                        ThirdPartyPassengers = DictionaryThirdPartyPassengers[VehicleID];

                        if (passengerViewModel.CurrentPassenger > ThirdPartyPassengers.Count())
                        {
                            ThirdPartyPassengers.Add(passengerViewModel);
                            DictionaryThirdPartyPassengers[VehicleID] = ThirdPartyPassengers;
                            HttpContext.Current.Session["DictionaryThirdPartyPassengers"] = DictionaryThirdPartyPassengers;

                        }
                        else
                        {
                            // Update the model in session
                            ThirdPartyPassengers[passengerViewModel.CurrentPassenger - 1] = passengerViewModel;
                        }
                    }
                    else
                    {
                        // Add Passenger for new vehicle
                        ThirdPartyPassengers.Add(passengerViewModel);
                        DictionaryThirdPartyPassengers.Add(VehicleID, ThirdPartyPassengers);
                        HttpContext.Current.Session["DictionaryThirdPartyPassengers"] = DictionaryThirdPartyPassengers;
                    }
                }
            }
        }

        public static void StoreThirdPartyVehicleViewModel(ThirdPartyVehicleViewModel thirdPartyVehicleViewModel, int VehicleID)
        {

            Dictionary<int, ThirdPartyVehicleViewModel> DictionaryThirdPartyVehicleViewModels = new Dictionary<int, ThirdPartyVehicleViewModel>();

            if (HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] == null)
            {
                //
               // passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                DictionaryThirdPartyVehicleViewModels.Add(VehicleID, thirdPartyVehicleViewModel);
                HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] = DictionaryThirdPartyVehicleViewModels;
            }
            else
            {
                //Get dictionary from session
                DictionaryThirdPartyVehicleViewModels = (Dictionary<int, ThirdPartyVehicleViewModel>)HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"];
                // Check if model already exists in List, if not add it
                if (DictionaryThirdPartyVehicleViewModels.ContainsKey(VehicleID))
                {
                    //Update the model
                    DictionaryThirdPartyVehicleViewModels[VehicleID] = thirdPartyVehicleViewModel;
                    HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] = DictionaryThirdPartyVehicleViewModels;
                }
                else
                {
                    // Add new model
                    DictionaryThirdPartyVehicleViewModels.Add(VehicleID, thirdPartyVehicleViewModel);
                    HttpContext.Current.Session["DictionaryThirdPartyVehicleViewModels"] = DictionaryThirdPartyVehicleViewModels;
                }
            }

        }

        public static void StoreThirdPartyDriverViewModel(ThirdPartyDriverViewModel thirdPartyDriverViewModel, int VehicleID)
        {

            Dictionary<int, ThirdPartyDriverViewModel> DictionaryThirdPartyDriverViewModels = new Dictionary<int, ThirdPartyDriverViewModel>();

            if (HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] == null)
            {
                //
                // passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                DictionaryThirdPartyDriverViewModels.Add(VehicleID, thirdPartyDriverViewModel);
                HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] = DictionaryThirdPartyDriverViewModels;
            }
            else
            {
                //Get dictionary from session
                DictionaryThirdPartyDriverViewModels = (Dictionary<int, ThirdPartyDriverViewModel>)HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"];
                // Check if model already exists in List, if not add it
                if (DictionaryThirdPartyDriverViewModels.ContainsKey(VehicleID))
                {
                    //Update the model
                    DictionaryThirdPartyDriverViewModels[VehicleID] = thirdPartyDriverViewModel;
                }
                else
                {
                    // Add new model
                    DictionaryThirdPartyDriverViewModels.Add(VehicleID, thirdPartyDriverViewModel);
                    HttpContext.Current.Session["DictionaryThirdPartyDriverViewModels"] = DictionaryThirdPartyDriverViewModels;
                }
            }

        }

        public static void StoreWitnessViewModel(WitnessViewModel witnessViewModel, int WitnessID)
        {
            Dictionary<int, WitnessViewModel> DictionaryWitnessViewModels = new Dictionary<int, WitnessViewModel>();
            
            if (HttpContext.Current.Session["DictionaryWitnessViewModels"] == null)
            {
                //
                // passengerSupportingInvolvements.Add(supportingInvolvementsViewModel);
                DictionaryWitnessViewModels.Add(WitnessID, witnessViewModel);
                HttpContext.Current.Session["DictionaryWitnessViewModels"] = DictionaryWitnessViewModels;
            }
            else
            {
                //Get dictionary from session
                DictionaryWitnessViewModels = (Dictionary<int, WitnessViewModel>)HttpContext.Current.Session["DictionaryWitnessViewModels"];
                // Check if model already exists in List, if not add it
                if (DictionaryWitnessViewModels.ContainsKey(WitnessID))
                {
                    //Update the model
                    DictionaryWitnessViewModels[WitnessID] = witnessViewModel;
                }
                else
                {
                    // Add new model
                    DictionaryWitnessViewModels.Add(WitnessID, witnessViewModel);
                    HttpContext.Current.Session["DictionaryWitnessViewModels"] = DictionaryWitnessViewModels;
                }
            }
        }

        public static void StoreReviewAndSubmitViewModel(ReviewAndSubmitViewModel reviewAndSubmitViewModel)
        {
            HttpContext.Current.Session["ReviewAndSubmitViewModel"] = reviewAndSubmitViewModel;
        }

        #endregion

        #region populate lookups

        internal static string GetWitnessInvolvementTypeFromID(int? InvolvementTypeID)
        {
            string sReturn = "";

            if (InvolvementTypeID == 0)
               sReturn= "Independent Witness";
            else if(InvolvementTypeID == 1)
                sReturn= "Third Party Witness";
            else if(InvolvementTypeID == 2)
                sReturn= "Insured Witness";

            return sReturn;
        }

        internal static string GetPolicyTypeFromID(int? PolicyTypeID)
        {

              if(PolicyTypeID==0)
                  return "Commercial";
              else
                  return "Personal";

        }

        internal static IEnumerable<SelectListItem> PopulateAddressStatuses()
        {
        // Address Status
            IEnumerable<SelectListItem> AddressStatuses = new List<SelectListItem>{
               new SelectListItem(){Text="Previous Address",Value="0"},
               new SelectListItem(){Text="Linked Address",Value="1"},
               new SelectListItem(){Text="Current Address",Value="2"},
               new SelectListItem(){Text="Registered Address",Value="3"},
               new SelectListItem(){Text="trading Address",Value="4"},
               new SelectListItem(){Text="Storage Address",Value="5"}
               };
            return AddressStatuses;
        }

        internal static IEnumerable<SelectListItem> PopulateBankAccountUsedFors()
        {
            // Bank Account : Account Used For
            IEnumerable<SelectListItem> AccountUsedFors = new List<SelectListItem>{
               new SelectListItem(){Text="Unknown",Value="0"},
               new SelectListItem(){Text="Deposit",Value="1"},
               new SelectListItem(){Text="Direct Debit",Value="2"},
               new SelectListItem(){Text="Single Payment",Value="3"}
               };
            return AccountUsedFors;
        }

        internal static IEnumerable<SelectListItem> PopulatepaymentCardTypes()
        {
            // Card Types
            IEnumerable<SelectListItem> CardTypes = new List<SelectListItem>{
               new SelectListItem(){Text="Credit Card",Value="0"},
               new SelectListItem(){Text="Debit card",Value="1"}
               };

            return CardTypes;
        }

        internal static IEnumerable<SelectListItem> PopulateInsuredVehicleColours()
        {
            // Person : nationality
            IEnumerable<SelectListItem> Colours = new List<SelectListItem>{
            new SelectListItem(){Text="Beige",Value="0"},
            new SelectListItem(){Text="Black",Value="1"},
            new SelectListItem(){Text="Blue",Value="2"},
            new SelectListItem(){Text="Bronze",Value="3"},
            new SelectListItem(){Text="Brown",Value="4"},
            new SelectListItem(){Text="Cream",Value="5"},
            new SelectListItem(){Text="Gold",Value="6"},
            new SelectListItem(){Text="Graphite",Value="7"},
            new SelectListItem(){Text="Green",Value="8"},
            new SelectListItem(){Text="Grey",Value="9"},
            new SelectListItem(){Text="Lilac",Value="10"},
            new SelectListItem(){Text="Maroon",Value="11"},
            new SelectListItem(){Text="Mauve",Value="12"},
            new SelectListItem(){Text="Orange",Value="13"},
            new SelectListItem(){Text="Pink",Value="14"},
            new SelectListItem(){Text="Purple",Value="15"},
            new SelectListItem(){Text="Red",Value="16"},
            new SelectListItem(){Text="Silver",Value="17"},
            new SelectListItem(){Text="Turquoise",Value="18"},
            new SelectListItem(){Text="White",Value="19"},
            new SelectListItem(){Text="Yellow",Value="20"}
               };
            return Colours;
        }

        internal static IEnumerable<SelectListItem> PopulatePaymentCardUsedFors()
        {
            // Payment Card : Card used for
            IEnumerable<SelectListItem> CardUsedFors = new List<SelectListItem>{
               new SelectListItem(){Text="Credit Card",Value="0"},
               new SelectListItem(){Text="Debit card",Value="1"}
               };
            return CardUsedFors;
        }

        internal static IEnumerable<SelectListItem> PopulateFuels()
        {
            // Bank Account : Account Used For
            IEnumerable<SelectListItem> Fuels = new List<SelectListItem>{
               new SelectListItem(){Text="Petrol",Value="0"},
               new SelectListItem(){Text="Diesel",Value="1"},
               new SelectListItem(){Text="LPG Debit",Value="2"},
               new SelectListItem(){Text="Hybrid",Value="3"},
               new SelectListItem(){Text="Electric",Value="4"}
               };
            return Fuels;
        }

        internal static IEnumerable<SelectListItem> PopulatePersonGenders()
        {
            IEnumerable<SelectListItem> Genders = new List<SelectListItem>{
               new SelectListItem(){Text="Male",Value="0"},
               new SelectListItem(){Text="Female",Value="1"}
               };
            return Genders;
        }

        internal static IEnumerable<SelectListItem> PopulateNationalities()
        {
            // Person : nationality
            IEnumerable<SelectListItem> Nationalities = new List<SelectListItem>{
               new SelectListItem(){Text="British",Value="0"},
               new SelectListItem(){Text="------------",Value="1"},
               new SelectListItem(){Text="Afghan",Value="2"},
               new SelectListItem(){Text="Albanian",Value="3"},
               new SelectListItem(){Text="French",Value="4"},
               new SelectListItem(){Text="Spanish",Value="5"},
               new SelectListItem(){Text="German",Value="6"}
               };
            return Nationalities;
        }

        internal static IEnumerable<SelectListItem> PopulatePersonSalutations()
        {
            IEnumerable<SelectListItem> Salutations = new List<SelectListItem>{
               new SelectListItem(){Text="Mr",Value="0"},
               new SelectListItem(){Text="Mrs",Value="1"},
               new SelectListItem(){Text="Ms",Value="2"},
               new SelectListItem(){Text="Miss",Value="3"},
               new SelectListItem(){Text="Master",Value="1"},
               new SelectListItem(){Text="Dr",Value="5"},
               new SelectListItem(){Text="Professor",Value="6"},
               new SelectListItem(){Text="Reverend",Value="7"},
               new SelectListItem(){Text="Sir",Value="8"},
               new SelectListItem(){Text="Lord",Value="9"},
               new SelectListItem(){Text="Baroness",Value="10"},
               new SelectListItem(){Text="Right honourable",Value="11"}
               };
            return Salutations;
        }

        internal static IEnumerable<SelectListItem> PopulateInsuredVehicleTransmissions()
        {
            // Person : nationality
            IEnumerable<SelectListItem> Transmissions = new List<SelectListItem>{
               new SelectListItem(){Text="Unknown",Value="0"},
               new SelectListItem(){Text="Manual",Value="1"},
               new SelectListItem(){Text="Automatic",Value="2"}
               };
            return Transmissions;
        }

        internal static IEnumerable<SelectListItem> PopulateInsuredVehicleTotalLossCapacity()
        {
            // Person : nationality
            IEnumerable<SelectListItem> Transmissions = new List<SelectListItem>{
               new SelectListItem(){Text="CatA",Value="0"},
               new SelectListItem(){Text="CatB",Value="1"},
               new SelectListItem(){Text="CatC",Value="2"},
               new SelectListItem(){Text="CatD",Value="3"}
               };
            return Transmissions;
        }

        internal static IEnumerable<SelectListItem> PopulateInsuredVehicleTypes()
        {
            // Person : nationality
            IEnumerable<SelectListItem> VehicleTypes = new List<SelectListItem>{
               new SelectListItem(){Text="Unknown",Value="0"},
               new SelectListItem(){Text="Bicycle",Value="1"},
               new SelectListItem(){Text="Car",Value="2"},
               new SelectListItem(){Text="Coach",Value="3"},
               new SelectListItem(){Text="Lorry",Value="4"},
               new SelectListItem(){Text="Minibus",Value="5"},
               new SelectListItem(){Text="Motorcycle",Value="6"},
               new SelectListItem(){Text="Pickup",Value="7"},
               new SelectListItem(){Text="van",Value="8"}
               };
            return VehicleTypes;
        }

        internal static IEnumerable<SelectListItem> PopulateOrganisationInvolvements()
        {
            // Person : nationality
            IEnumerable<SelectListItem> OrganisationInvolvements = new List<SelectListItem>{
               new SelectListItem(){Text="Unknown",Value="0"},
               new SelectListItem(){Text="Accident",Value="1"},
               new SelectListItem(){Text="Management",Value="2"},
               new SelectListItem(){Text="Credit Hire",Value="3"},
               new SelectListItem(){Text="Solicitor",Value="4"},
               new SelectListItem(){Text="Vehicle Engineer",Value="5"},
               new SelectListItem(){Text="Recovery and/or Storage",Value="6"},
               new SelectListItem(){Text="Broker",Value="7"},
               new SelectListItem(){Text="Medical Examiner",Value="8"}
               };
            return OrganisationInvolvements;
        }

        internal static IEnumerable<SelectListItem> PopulateWitnessInvolvementType()
        {
            // Person : nationality
            IEnumerable<SelectListItem> WitnessInvolvementTypes = new List<SelectListItem>{
               new SelectListItem(){Text="Independent Witness",Value="0"},
               new SelectListItem(){Text="Third Party Witness",Value="1"},
               new SelectListItem(){Text="Insured Witness",Value="2"}
               };
            return WitnessInvolvementTypes;
        }

        #endregion

        public static List<ExWebsiteSingleClaim> GetSingleClaimsForClient(OutstandingClaimsListViewModel outstandingClaimsListViewModel, out int totalCount)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteSingleClaimsListResponse response = proxy.GetSingleClaimsList(new WebsiteSingleClaimsListRequest
                {
                    UserId = SiteHelpers.LoggedInUser.Id,
                    ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                    Page = outstandingClaimsListViewModel.PagingInfo.CurrentPage,
                    PageSize = outstandingClaimsListViewModel.PagingInfo.ItemsPerPage,
                    SortColumn = outstandingClaimsListViewModel.SortColumn,
                    SortAscending = outstandingClaimsListViewModel.SortAscending,
                    TeamId = outstandingClaimsListViewModel.SelectedTeamId,
                    StatusFilter = 0
                });

            totalCount = response.TotalRows;

            return response.WebsiteSingleClaimsList.ToList();
        }

        public static int GetTotalSingleClaimsForClient()
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            int clientId = SiteHelpers.LoggedInUser.RiskClientId;


            WebsiteGetTotalSingleClaimsForClientResponse response =
                proxy.GetTotalSingleClaimForClient(new WebsiteGetTotalSingleClaimsForClientRequest
                    {
                        ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                    });

            return response.TotalRows;
        }


        public static OutstandingClaimsListViewModel PopulateOutstandingClaimsListViewModel(bool bPosted,
                                                                                            OutstandingClaimsListViewModel
                                                                                                outstandingClaimsListViewModel)
        {
            if (!bPosted)
            {
                // Defaults
                outstandingClaimsListViewModel.PagingInfo.ItemsPerPage =
                    Convert.ToInt16(ConfigurationManager.AppSettings["OutstandingItemsPerPage"]);
                outstandingClaimsListViewModel.PagingInfo.CurrentPage = 1;
                outstandingClaimsListViewModel.SortColumn = "UPLOADDATETIME";
                outstandingClaimsListViewModel.SortAscending = false;
            }

            // Get the single claims
            int totalcount = 0;

            outstandingClaimsListViewModel.OutstandingClaimsList =
                GetSingleClaimsForClient(outstandingClaimsListViewModel, out totalcount);

            outstandingClaimsListViewModel.PagingInfo.TotalItems = totalcount;

            return outstandingClaimsListViewModel;

        }

       

        public  static int GetTotalOutstandingClaims(OutstandingClaimsListViewModel selectedOutstandingClaimsModel)
        {
            int totalcount = 0;
            OutstandingClaimsListViewModel outstandingClaimsListViewModel = new OutstandingClaimsListViewModel();

            outstandingClaimsListViewModel.OutstandingClaimsList =
                GetSingleClaimsForClient(outstandingClaimsListViewModel, out totalcount);

            return totalcount;
        }

        public static List<ExRiskClaimEdit> GetBatchesForClient(OutstandingClaimsListViewModel outstandingClaimsListViewModel, out int totalCount)
        {
            
            //IADAWebServices proxy = Helper.Services.CreateADAServices();

            //ClaimsForUserResponse response = proxy.GetBatchList()

            //WebsiteBatchListResponse response = proxy.GetBatchList(new WebsiteBatchListRequest()
            //{
            //    ClientId = batchListViewModel.SelectedClientId != null ? (int)batchListViewModel.SelectedClientId : -1,
            //    TeamId = batchListViewModel.SelectedTeamId,
            //    UserId = batchListViewModel.SelectedUserId,
            //    SortColumn = batchListViewModel.SortColumn,
            //    SortAscending = batchListViewModel.SortAscending,
            //    Page = batchListViewModel.PagingInfo.CurrentPage,
            //    PageSize = batchListViewModel.PagingInfo.ItemsPerPage,
            //    StatusFilter = 2
            //});
            ////
            //totalCount = response.TotalRows;
            //// 

            totalCount = 0;
            return new List<ExRiskClaimEdit>();
        }

        public static WebsiteSaveSingleClaimResponse SaveSingleClaim(MDA.Common.FileModel.IClaim claim, int? singleClaimId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteSaveSingleClaimRequest request = new WebsiteSaveSingleClaimRequest();

            request.ClaimToSave = claim;
            switch (claim.ClaimType)
            {
                case ClaimType.Motor:
                    request.ClaimType = "Motor";
                    break;
            }

            request.ClientId = SiteHelpers.LoggedInUser.RiskClientId;

            if (singleClaimId != null)
                request.EditClaimId = singleClaimId;

            request.IncidentDate = claim.IncidentDate;

            String fullUserName = SiteHelpers.LoggedInUser.FirstName;
            fullUserName += " ";
            fullUserName += SiteHelpers.LoggedInUser.LastName;
            request.ModifiedBy = fullUserName;
            request.ModifiedDate = DateTime.Now;
            request.RiskClaimNumber = claim.ClaimNumber;
            request.UserId = SiteHelpers.LoggedInUser.Id;
            request.EditClaimId = singleClaimId;

            WebsiteSaveSingleClaimResponse response = proxy.SaveSingleClaim(request);

            return response;
        }


        public static WebsiteFetchSingleClaimResponse FetchSavedSingleClaim(int id)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteFetchSingleClaimRequest request = new WebsiteFetchSingleClaimRequest();

            request.ClientId = SiteHelpers.LoggedInUser.RiskClientId;
            request.EditClaimId = id;
            request.UserId = SiteHelpers.LoggedInUser.Id;

            WebsiteFetchSingleClaimResponse response = proxy.FetchSingleClaimDetail(request);

            return response;
        }


        public static WebsiteDeleteSingleClaimResponse DeleteOutstandingClaim(int riskClaimId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteDeleteSingleClaimRequest request = new WebsiteDeleteSingleClaimRequest();

            request.ClientId = SiteHelpers.LoggedInUser.RiskClientId;
            request.EditClaimId = riskClaimId;
            request.UserId = SiteHelpers.LoggedInUser.Id;

            WebsiteDeleteSingleClaimResponse response = proxy.DeleteSingleClaim(request);

            return response;
        }





        
    }
}