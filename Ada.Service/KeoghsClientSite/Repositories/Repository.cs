﻿using KeoghsClientSite.FileUpload;
using KeoghsClientSite.Models;
using KeoghsClientSite.StaticClasses;
using MDA.Common;
using MDA.WCF.TransferServices;
using MDA.WCF.WebServices.Entities;
using MDA.WCF.WebServices.Interface;
using MDA.WCF.WebServices.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeoghsClientSite.Repositories
{
    public static class Repository
    {
        #region Web services

        //
        public static void CreateBatchFromUpload(HttpPostedFile ADAFile, string ClientBatchReference, string FileName, string MimeType)
        {
            IADATransferServices p = Helper.Services.CreateADATransferServices();

            string filePath = Path.Combine(ConfigurationManager.AppSettings["BatchUploadFolderPath"], Guid.NewGuid().ToString());

            // Create path if not present
            if (!Directory.Exists(ConfigurationManager.AppSettings["BatchUploadFolderPath"]))
                Directory.CreateDirectory(ConfigurationManager.AppSettings["BatchUploadFolderPath"]);
            //
            if (ADAFile != null)
            {
                ADAFile.SaveAs(filePath);

                using (Stream upFile = System.IO.File.OpenRead(filePath))
                {
                    p.UploadBatchOfClaimsFile(new RemoteFileInfo()
                    {
                        ClientId = SiteHelpers.LoggedInClient.Id,
                        UserId = SiteHelpers.LoggedInUser.Id,
                        Who = SiteHelpers.LoggedInUser.UserName,

                        BatchEntityType = "Motor",
                        ClientBatchReference = ClientBatchReference,
                        FileByteStream = upFile,
                        FileLength = upFile.Length,
                        FileName = FileName,
                        MimeType = MimeType
                    });
                }

                System.IO.File.Delete(filePath);
            }
            else
            {
                // File not posted correctly Return error ??
            }
        }

        //
        public static List<ExWebsiteBatchDetails> GetBatchesForClient(BatchListViewModel batchListViewModel, out int totalCount)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteBatchListResponse response = proxy.GetBatchList(new WebsiteBatchListRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                TeamId = SiteHelpers.LoggedInUser.RiskTeamId,

                FilterClientId = batchListViewModel.SelectedClientId != null ? (int)batchListViewModel.SelectedClientId : -1,
                FilterUserId = batchListViewModel.SelectedUserId,
                SortColumn = batchListViewModel.SortColumn,
                SortAscending = batchListViewModel.SortAscending,
                Page = batchListViewModel.PagingInfo.CurrentPage,
                PageSize = batchListViewModel.PagingInfo.ItemsPerPage,
                StatusFilter = 2
            });
            //
            totalCount = response.TotalRows;
            //
            return response.BatchList.ToList();
        }

        internal static bool CheckUserHasAccessToReport(int riskBatchId, int loggedInUserId, int clientId)
        {
            IADAWebServices p = Helper.Services.CreateADAServices();

            return p.CheckUserHasAccessToReport(new CheckUserHasAccessToReportRequest()
            {
                //clientId = SiteHelpers.LoggedInUser.RiskClientId,
                //UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                RiskBatchId = riskBatchId,
                LoggedInUserId = loggedInUserId,
                ClientId = clientId
            });
        }

        //
        public static GetBatchProcessingResultsResponse GetBatchProcessingResults(int batchId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            GetBatchProcessingResultsResponse response = proxy.GetBatchProcessingResults(new GetBatchProcessingResultsRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                RiskBatchId = batchId
            });
            //
            return response;
        }

        //

        public static Stream GetBatchReportStream(int riskBatchId)
        {
            IADATransferServices p = Helper.Services.CreateADATransferServices();

            var request = new GetBatchReportStreamRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                RiskBatchId = riskBatchId,
            };
            return p.GetBatchReportStream(request);
        }

        //
        public static GetBatchStatusResponse GetBatchStatus(string clientBatchReference)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            GetBatchStatusResponse response = proxy.GetBatchStatus(new GetBatchStatusRequest()
            {
                ClientId = SiteHelpers.LoggedInClient.Id,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                ClientBatchReference = clientBatchReference,
            });
            //
            return response;
        }

        //
        //
        public static List<ExRiskClaimDetails> GetclaimsForClient(ClaimsFilterViewModel claimsFilterViewModel, out int totalCount)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            bool filterNoReportsRequested = false;
            bool filterLevel1ReportsRequested = false;
            bool filterLevel2ReportsRequested = false;
            bool filterCallbackReportsRequested = false;
            bool filterHighRisk = false;
            bool filterMediumRisk = false;
            bool filterLowRisk = false;
            bool filterKeoghsCFS = false;
            bool filterPreviousVersion = false;
            bool filterError = false;
            bool filterStatusUnread = false;
            bool filterStatusRead = false;
            bool filterStatusUnreadScoreChanges = false;
            bool filterStatusUnrequestedReports = false;
            bool filterStatusPendingReports = false;
            bool filterStatusAvailableReports = false;
            bool filterStatusDecisions = false;
            bool filterStatusReserveChanges = false;

            bool defaultHigh = false;
            bool defaultMedium = false;
            bool defaultLow = false;
            bool defaultKeoghsCFS = false;
            bool defaultPreviousVersion = false;
            bool defaultError = false;
            bool defaultUnread = false;
            bool defaultRead = false;
            bool defaultUnreadScoreChange = false;
            bool riskRatingReadOnly = false;
            bool statusReadOnly = false;

            foreach (var item in SiteHelpers.LoggedInUser.TemplateFunctions)
            {
                switch (item.Key)
                {
                    case "chkDefault_12_High":
                        defaultHigh = true;
                        break;

                    case "chkDefault_13_Medium":
                        defaultMedium = true;
                        break;

                    case "chkDefault_14_Low":
                        defaultLow = true;
                        break;

                    case "chkDefault_15_KeoghsCFS":
                        defaultKeoghsCFS = true;
                        break;

                    case "chkDefault_16_PreviousVersion":
                        defaultPreviousVersion = true;
                        break;

                    case "chkDefault_17_Error":
                        defaultError = true;
                        break;

                    case "chkDefault_19_Unread":
                        defaultUnread = true;
                        break;

                    case "chkDefault_20_Read":
                        defaultRead = true;
                        break;

                    case "chkDefault_21_UnreadScoreChange":
                        defaultUnreadScoreChange = true;
                        break;

                    case "chkReadOnly_11_RiskRating":
                        riskRatingReadOnly = true;
                        break;

                    case "chkReadOnly_18_Status":
                        statusReadOnly = true;
                        break;
                    //case "NotesFilter_??_Status":
                    //    defaultNotes = true;
                    //    break;
                }
            }

            // Populate Risk Rating checkboxes
            if (riskRatingReadOnly)
            {
                var riskRatings = new[]
                {
                    new FilterRiskRatingViewModel { Rating = "High", IsChecked = (defaultHigh)? true : false },
                    new FilterRiskRatingViewModel { Rating = "Medium", IsChecked = (defaultMedium) ? true : false },
                    new FilterRiskRatingViewModel { Rating = "Low", IsChecked = (defaultLow) ? true : false },
                    new FilterRiskRatingViewModel { Rating = "Keoghs CFS", IsChecked = (defaultKeoghsCFS) ? true : false },
                    new FilterRiskRatingViewModel { Rating = "Previous Version", IsChecked = (defaultPreviousVersion) ? true : false},
                    new FilterRiskRatingViewModel { Rating = "Error", IsChecked = (defaultError) ? true : false}
                };

                //
                claimsFilterViewModel.RiskRatings = riskRatings;
            }

            bool? reserveChanges = SiteHelpers.LoggedInClient.RecordReserveChanges;

            bool addReserveFilter = false;

            if (reserveChanges == true)
            {
                addReserveFilter = true;
            }

            if (statusReadOnly)
            {
                var statusFilters = new[]
                {
                    new FilterStatusViewModel {FilterStatus = "Unread", IsChecked = (defaultUnread) ? true : false},
                    new FilterStatusViewModel {FilterStatus = "Read", IsChecked = (defaultRead) ? true : false},
                    new FilterStatusViewModel {FilterStatus = "Unread Score Change", IsChecked = (defaultUnreadScoreChange) ? true : false},
                };

                if (addReserveFilter)
                {
                    var reserveStatusFilters = new[]
                    {
                        new FilterStatusViewModel {FilterStatus = "Reserve Change", IsChecked = false },
                    };

                    statusFilters = statusFilters.Concat(reserveStatusFilters).ToArray();
                }

                claimsFilterViewModel.FilterStatus = statusFilters;
            }

            foreach (FilterReportsRequested report in claimsFilterViewModel.ReportsRequested.ReportsRequested)
            {
                switch (report.ReportName)
                {
                    case "No requests":
                        filterNoReportsRequested = report.IsChecked;
                        break;

                    case "Level 1":
                        filterLevel1ReportsRequested = report.IsChecked;
                        break;

                    case "Level 2":
                        filterLevel2ReportsRequested = report.IsChecked;
                        break;

                    case "Callback":
                        filterCallbackReportsRequested = report.IsChecked;
                        break;
                }
            }

            foreach (FilterRiskRatingViewModel rating in claimsFilterViewModel.RiskRatings)
            {
                switch (rating.Rating)
                {
                    case "High":
                        filterHighRisk = rating.IsChecked;
                        break;

                    case "Medium":
                        filterMediumRisk = rating.IsChecked;
                        break;

                    case "Low":
                        filterLowRisk = rating.IsChecked;
                        break;

                    case "Keoghs CFS":
                        filterKeoghsCFS = rating.IsChecked;
                        break;

                    case "Previous Version":
                        filterPreviousVersion = rating.IsChecked;
                        break;

                    case "Error":
                        filterError = rating.IsChecked;
                        break;
                }
            }

            foreach (FilterStatusViewModel filterStatus in claimsFilterViewModel.FilterStatus)
            {
                switch (filterStatus.FilterStatus)
                {
                    case "Unread":
                        filterStatusUnread = filterStatus.IsChecked;
                        break;

                    case "Read":
                        filterStatusRead = filterStatus.IsChecked;
                        break;

                    case "Unread Score Change":
                        filterStatusUnreadScoreChanges = filterStatus.IsChecked;
                        break;

                    case "Reserve Change":
                        filterStatusReserveChanges = filterStatus.IsChecked;
                        break;
                }
            }

            foreach (ReportStatusViewModel reportStatus in claimsFilterViewModel.ReportStatus)
            {
                switch (reportStatus.ReportStatus)
                {
                    case "Unrequested Reports":
                        filterStatusUnrequestedReports = reportStatus.IsChecked;
                        break;

                    case "Pending Reports":
                        filterStatusPendingReports = reportStatus.IsChecked;
                        break;

                    case "Available Reports":
                        filterStatusAvailableReports = reportStatus.IsChecked;
                        break;
                }
            }

            ClaimsForClientResponse response = proxy.GetClaimsForClient(new ClaimsForClientRequest()
            {
                ClientId = SiteHelpers.LoggedInClient.Id,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                ////clientId                   = claimsFilterViewModel.selectedClientId!=null? (int)claimsFilterViewModel.selectedClientId:SiteHelpers.LoggedInUser.RiskClientId,
                ////TeamId                     = claimsFilterViewModel.SelectedTeamId != null ? (int)claimsFilterViewModel.SelectedTeamId : SiteHelpers.LoggedInUser.RiskTeamId,
                ////UserId                     = claimsFilterViewModel.SelectedUserId != null ? (int)claimsFilterViewModel.SelectedUserId : SiteHelpers.LoggedInUser.Id,
                FilterClientId = (int)claimsFilterViewModel.SelectedClientId,
                TeamId = (int)claimsFilterViewModel.SelectedTeamId,
                FilterUserId = (int)claimsFilterViewModel.SelectedUserId,
                Page = claimsFilterViewModel.PagingInfo.CurrentPage,
                PageSize = claimsFilterViewModel.PagingInfo.ItemsPerPage,
                SortColumn = claimsFilterViewModel.SortColumn,
                FilterUploadedBy = claimsFilterViewModel.FilterUploadedBy,
                FilterBatchRef = claimsFilterViewModel.SelectedBatchId,
                FilterClaimNumber = claimsFilterViewModel.FilterClaimNumber,
                FilterStartDate = claimsFilterViewModel.FilterStartDate,
                FilterEndDate = claimsFilterViewModel.FilterEndDate,
                FilterHighRisk = filterHighRisk,
                FilterMediumRisk = filterMediumRisk,
                FilterLowRisk = filterLowRisk,
                FilterKeoghsCFS = filterKeoghsCFS,
                FilterPreviousVersion = filterPreviousVersion,
                FilterError = filterError,
                FilterStatusRead = filterStatusRead,
                FilterStatusUnread = filterStatusUnread,
                FilterStatusUnreadScoreChanges = filterStatusUnreadScoreChanges,
                FilterStatusReserveChange  = filterStatusReserveChanges,
                FilterStatusUnrequestedReports = filterStatusUnrequestedReports,
                FilterStatusPendingReports = filterStatusPendingReports,
                FilterStatusAvailableReports = filterStatusAvailableReports,
                FilterNoReportsRequested = filterNoReportsRequested,
                FilterLevel1ReportsRequested = filterLevel1ReportsRequested,
                FilterLevel2ReportsRequested = filterLevel2ReportsRequested,
                FilterCallbackReportsRequested = filterCallbackReportsRequested,
                FilterNotes = claimsFilterViewModel.FilterNotes,
                FilterIntel = claimsFilterViewModel.FilterIntel,
                FilterDecisions = filterStatusDecisions,
                FilterDecisionIds = claimsFilterViewModel.SelectedDecisionId,
                //FilterReportsRequestedOnly   = (claimsFilterViewModel.FilterReportsRequestedOnly == false ? null : (bool?)true),
                SortAscending = claimsFilterViewModel.SortAscending
            });
            //
            totalCount = response.TotalRows;
            //
            return response.ClaimsList.ToList();
        }

        //
        public static Stream GetLevelOneReportStream(int riskClaimId)
        {
            IADATransferServices p = Helper.Services.CreateADATransferServices();

            var request = new GetLevel1ReportStreamRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                RiskClaimId = riskClaimId,
            };

            return p.GetLevel1ReportStream(request);
        }

        //
        // Returns expected file extension for client
        public static string GetUserExpectedFileExtension()
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RiskClientsResponse response = proxy.GetRiskClients(new RiskClientsRequest()
            {
                ClientId = SiteHelpers.LoggedInClient.Id,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
            });
            //
            //
            //string expectedFileExtension = response.ClientList.FirstOrDefault().ExpectedFileExtension;

            ////Replace commas with |
            //expectedFileExtension = ExpectedFileExtension.Replace(",", "|");
            //expectedFileExtension = ExpectedFileExtension.Replace(" " ,"");

            var firstClient = response.ClientList.FirstOrDefault();

            return (firstClient == null) ? string.Empty : firstClient.ExpectedFileExtension.Replace(",", "|").Replace(" ", "");
        }

        public static ExRiskClient GetRiskClientInfo(int clientId)
        {
            var proxy = Helper.Services.CreateADAServices();

            RiskClientsResponse response = proxy.GetRiskClients(new RiskClientsRequest()
            {
                ClientId = clientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
            });

            var firstClient = response.ClientList.FirstOrDefault();

            return firstClient;
        }

        public static List<ExRiskClient> GetAllRiskClientInfo()
        {
            var proxy = Helper.Services.CreateADAServices();

            RiskClientsResponse response = proxy.GetRiskClients(new RiskClientsRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                FilterClientId = -1
            });

            var clientList = response.ClientList.OrderBy(x => x.ClientName);

            return clientList.ToList();
        }

        public static ExRiskUser GetRiskUserInfo(string userName, int clientId)
        {
            var proxy = Helper.Services.CreateADAServices();

            int userId = proxy.GetUserId(new GetUserIdRequest()
            {
                ClientId = 0,        //SiteHelpers.LoggedInUser.RiskClientId,
                UserId = -1,       //SiteHelpers.LoggedInUser.Id,
                Who = userName, //SiteHelpers.LoggedInUser.UserName,

                UserName = userName,
            });

            GetRiskUserInfoResponse response = proxy.GetRiskUserInfo(new GetRiskUserInfoRequest()
            {
                ClientId = clientId, //SiteHelpers.LoggedInUser.RiskClientId,
                Who = userName,      //SiteHelpers.LoggedInUser.UserName,
                UserId = userId,
            });

            return response.UserInfo;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static ExRiskUser GetRiskUserInfo(string userName)
        {
            var proxy = Helper.Services.CreateADAServices();

            int userId = proxy.GetUserId(new GetUserIdRequest()
            {
                ClientId = 0,        //SiteHelpers.LoggedInUser.RiskClientId,
                UserId = -1,       //SiteHelpers.LoggedInUser.Id,
                Who = userName, //SiteHelpers.LoggedInUser.UserName,

                UserName = userName,
            });

            GetRiskUserInfoResponse response = proxy.GetRiskUserInfo(new GetRiskUserInfoRequest()
            {
                ClientId = 0,
                Who = userName,
                UserId = userId,
            });

            return response.UserInfo;
        }

        public static IsClientBatchReferenceUniqueResponse IsClientBatchReferenceUnique(string clientBatchReference)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            IsClientBatchReferenceUniqueResponse response = proxy.IsClientBatchReferenceUnique(new IsClientBatchReferenceUniqueRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                ClientBatchReference = clientBatchReference,
            });

            return response;
        }

        public static IsPasswordUniqueResponse IsPasswordUnique(string userName, string password)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            IsPasswordUniqueResponse response = proxy.IsPasswordUnique(new IsPasswordUniqueRequest()
            {
                Username = userName,
                Password = password,
            });

            return response;
        }

        public static IsClientNameUniqueResponse IsClientNameUnique(string clientName)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            IsClientNameUniqueResponse response = proxy.IsClientNameUnique(new IsClientNameUniqueRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                ClientName = clientName,
            });

            return response;
        }

        public static IsUserNameUniqueResponse IsUserNameUnique(string userName)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            IsUserNameUniqueResponse response = proxy.IsUserNameUnique(new IsUserNameUniqueRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                UserName = userName,
            });

            return response;
        }

        //
        public static RequestLevelOneReportResponse RequestLevelOneReport(int riskClaimId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RequestLevelOneReportResponse response = proxy.RequestLevelOneReport(new RequestLevelOneReportRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                RiskClaimId = riskClaimId,
            });

            return response;
        }

        //
        public static SetClaimReadStatusResponse UpdateClaimReadStatus(int riskClaimId, int riskClaimReadStatus)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            SetClaimReadStatusResponse response = proxy.SetClaimReadStatus(new SetClaimReadStatusRequest()
            {
                ClientId = SiteHelpers.LoggedInClient.Id,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                RiskClaimId = riskClaimId,
                RiskClaimReadStatus = riskClaimReadStatus,
            });

            return response;
        }

        //public static SaveClaimForUserResponse SaveClaimForUser(int userId, MDA.Common.FileModel.Claim claim)
        //{
        //    IADAWebServices proxy = Helper.Services.CreateADAServices();

        //    SaveClaimForUserResponse response = proxy.SaveClaimForUser(new SaveClaimForUserRequest()
        //        {
        //            UserId = userId,
        //            ClaimToSave = claim
        //        });

        //    return response;
        //}
        //

        #endregion Web services

        #region membership web services

        public static List<ExRiskUserLocked> GetLockedUsers()
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RiskUsersLockedResponse response = proxy.GetRiskUsersLocked(new RiskUsersLockedRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
            });

            return response.LockedUserList.OrderBy(x => x.Username).ToList();
        }

        public static bool UnlockUser(int selectedUserId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RiskUserUnlockResponse response = proxy.UnlockRiskUser(new RiskUserUnlockRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                UnlockUserId = selectedUserId
            });

            var result = response.Result;

            if (result == 0)
                return true;
            else
                return false;
        }

        // Returns list of clients
        // Pass -1 to return all clients
        public static IEnumerable<SelectListItem> GetRiskClients(int clientId = -1)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RiskClientsResponse response = proxy.GetRiskClients(new RiskClientsRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                FilterClientId = clientId
            });

            var clientList = response.ClientList.OrderBy(client => client.ClientName).Select(client =>
                      new SelectListItem
                      {
                          Text = client.ClientName,
                          Value = client.Id.ToString()
                      });
            //
            return clientList;
        }

        public static IEnumerable<SelectListItem> GetAssignedRiskClientsForUser(int userId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            GetAssignedRiskClientsForUserResponse response = proxy.GetAssignedRiskClientsForUser(new GetAssignedRiskClientsForUserRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                FilterUserId = userId
            });

            var clientList = response.ClientList.OrderBy(client => client.ClientName).Select(client =>
                      new SelectListItem
                      {
                          Text = client.ClientName,
                          Value = client.Id.ToString()
                      });
            //
            return clientList;
        }

        public static IEnumerable<SelectListItem> GetRiskUsers()
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //

            RiskUsersResponse response = proxy.GetRiskUsers(new RiskUserRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
            });

            var userList = response.RiskUserList.OrderBy(user => user.UserName).Select(user =>
                                    new SelectListItem
                                    {
                                        Text = user.UserName,
                                        Value = user.Id.ToString()
                                    });

            return userList;
        }

        public static IEnumerable<SelectListItem> GetRiskRoles()
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            RiskRolesResponse response = proxy.GetRiskRoles(new RiskRolesRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
            });

            var roleList = response.RoleList.OrderBy(role => role.RoleName).Select(role =>
                       new SelectListItem
                       {
                           Text = role.RoleName,
                           Value = role.Id.ToString()
                       });
            //
            return roleList;
        }

        public static IEnumerable<SelectListItem> GetInsurersClients()
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            InsurersClientsResponse response = proxy.GetInsurersClients(new InsurersClientsRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
            });

            var roleList = response.ClientList.OrderBy(role => role.ClientName).Select(role =>
                       new SelectListItem
                       {
                           Text = role.ClientName,
                           Value = role.Id.ToString()
                       });
            //
            return roleList;
        }

        public static void CreateClient(string clientName, int amberThreshold, int redThreshold, string fileExtensions, int? statusId, int insurersClientsId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //

            var request = new CreateClientRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                ClientName = clientName,
                AmberThreshold = amberThreshold,
                RedThreshold = redThreshold,
                FileExtensions = fileExtensions,
                StatusId = statusId,
                InsurersClientsId = insurersClientsId
            };

            proxy.CreateClient(request);
            //
        }

        public static void UpdateClient(int iD, string clientName, int amberThreshold, int redThreshold, string fileExtensions, int? statusId, int insurersClientsId, bool? recordReserveChanges, int? batchPriority)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //

            var request = new UpdateClientRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                ClientName = clientName,
                AmberThreshold = amberThreshold,
                RedThreshold = redThreshold,
                FileExtensions = fileExtensions,
                StatusId = statusId,
                InsurersClientsId = insurersClientsId,
                Id = iD,
                RecordReserveChanges = recordReserveChanges,
                BatchPriority = batchPriority
            };

            proxy.UpdateClient(request);
            //
        }

        public static void UpdateUser(string userName, string password, string email, string firstName,
                        string lastName, string telephoneNumber, int clientId, int roleId, int teamId, int? status, bool? autolaoder)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //
            var request = new UpdateUserRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                UserName = userName,
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                Password = password,
                RoleId = roleId,
                TeamId = teamId,
                TelephoneNumber = telephoneNumber,
                Status = status,
                Autoloader = autolaoder
            };

            proxy.UpdateUser(request);
            //
        }

        public static void CreateRiskUser2Client(int userId, int clientId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //
            var request = new CreateRiskUser2ClientRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                SelectedUserId = userId,
                SelectedClientId = clientId,
            };

            proxy.CreateRiskUser2Client(request);
            //
        }

        public static void DeleteRiskUser2Client(int userId, int clientId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //
            var request = new DeleteRiskUser2ClientRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                SelectedUserId = userId,
                SelectedClientId = clientId,
            };

            proxy.DeleteRiskUser2Client(request);
            //
        }

        #endregion membership web services

        #region populate view models

        public static HomeViewModel FilterModel(ClaimsFilterViewModel oModel, Boolean bResults = false)
        {
            var claimsListViewModel = new ClaimsListViewModel();

            int totalCount;

            oModel.SelectedClientId = SiteHelpers.LoggedInUser.RiskClientId;
            oModel.SelectedTeamId = SiteHelpers.LoggedInUser.RiskTeamId;
            oModel.SelectedUserId = SiteHelpers.LoggedInUser.Id;

            oModel.Batches = Repository.GetBatchNumbers("ClaimsHandler", null);

            oModel.Decisions = Repository.GetRiskNoteDecision(SiteHelpers.LoggedInUser.RiskClientId);

            // Add one day to filter end date, this is done to support entering start date and end date to be the same
            // The linq query now uses createdate >= Start date and createdate < End date
            if (oModel.FilterEndDate != null)
                oModel.FilterEndDate = oModel.FilterEndDate.Value.AddDays(1);

            claimsListViewModel.claimsFilterViewModel = oModel;

            claimsListViewModel.claimsFilterViewModel.RiskRatings = oModel.RiskRatings;

            claimsListViewModel.claimsFilterViewModel.ReportsRequested = oModel.ReportsRequested;

            claimsListViewModel.Claims = Repository.GetclaimsForClient(claimsListViewModel.claimsFilterViewModel, out totalCount);

            claimsListViewModel.claimsFilterViewModel.PagingInfo.TotalItems = totalCount;

            if (bResults)
            {
                ResultsViewModel resultsViewModel = new ResultsViewModel();
                resultsViewModel.claimsListViewModel = claimsListViewModel;
                return (resultsViewModel);
            }
            else
            {
                HomeViewModel homeViewModel = new HomeViewModel();
                homeViewModel.claimsListViewModel = claimsListViewModel;
                return (homeViewModel);
            }
        }

        public static BatchListViewModel PopulateBatchListViewModel(bool bPosted, BatchListViewModel batchListViewModel)
        {
            batchListViewModel.Clients = Repository.GetRiskClients();

            if (!bPosted)
            {
                // Defaults
                batchListViewModel.PagingInfo.ItemsPerPage = Convert.ToInt16(ConfigurationManager.AppSettings["ItemsPerPage"]);
                batchListViewModel.PagingInfo.CurrentPage = 1;
                batchListViewModel.SortColumn = "UPLOADDATETIME";
                batchListViewModel.SortAscending = false;
                batchListViewModel.ExpectedFileExtension = Repository.GetUserExpectedFileExtension();
                //
            }

            //all batches uploaded for by all teams
            batchListViewModel.SelectedClientId = SiteHelpers.LoggedInClient.Id;
            batchListViewModel.SelectedTeamId = -1;
            batchListViewModel.SelectedUserId = -1;

            // Get the batches
            int totalcount = 0;
            batchListViewModel.BatchList = GetBatchesForClient(batchListViewModel, out totalcount);
            //
            batchListViewModel.PagingInfo.TotalItems = totalcount;
            //
            return batchListViewModel;
        }

        public static ClaimsFilterViewModel PopulateClaimsFilterViewModel(string controller, int currentPage = 1, int pageSize = 5,
                                                                         string sortColumn = "", bool sortAscending = true,
                                                                         int selectedClientId = -1)
        {
            ClaimsFilterViewModel claimsFilterViewModel = new ClaimsFilterViewModel();

            var uploadedDateRangeInMonths = 0;

            foreach (var item in SiteHelpers.LoggedInUser.TemplateFunctions)
            {
                switch (item.Key)
                {
                    case "txt_10_UploadedDateRange":
                        uploadedDateRangeInMonths = (item.Value != "") ? Convert.ToInt32(item.Value) : 0;
                        break;
                }
            }

            // Get the expected file extension for the client
            claimsFilterViewModel.ExpectedFileExtension = Repository.GetUserExpectedFileExtension();
            claimsFilterViewModel.Controller = controller;
            claimsFilterViewModel.PagingInfo.ItemsPerPage = pageSize;
            claimsFilterViewModel.PagingInfo.CurrentPage = currentPage;
            claimsFilterViewModel.SortColumn = sortColumn;
            claimsFilterViewModel.SortAscending = sortAscending;
            claimsFilterViewModel.SelectedClientId = SiteHelpers.LoggedInUser.RiskClientId;
            claimsFilterViewModel.SelectedTeamId = SiteHelpers.LoggedInUser.RiskTeamId;
            claimsFilterViewModel.SelectedUserId = SiteHelpers.LoggedInUser.Id;

            if (uploadedDateRangeInMonths > 0)
            {
                var dateAndTime = DateTime.Now;
                var date = dateAndTime.Date;
                claimsFilterViewModel.FilterStartDate = date.AddMonths(-uploadedDateRangeInMonths);
                claimsFilterViewModel.FilterEndDate = date;
            }

            var batchNumberListItems = Repository.GetBatchNumbers("ClaimsHandler", null);
            SelectLastBatchNumber(batchNumberListItems, claimsFilterViewModel);
            claimsFilterViewModel.Batches = batchNumberListItems;

            claimsFilterViewModel.Decisions = Repository.GetRiskNoteDecision(SiteHelpers.LoggedInUser.RiskClientId);

            //claimsFilterViewModel.SelectedBatchId = new String[0];

            bool defaultHigh = false;
            bool defaultMedium = false;
            bool defaultLow = false;
            bool defaultKeoghsCFS = false;
            bool defaultPreviousVersion = false;
            bool defaultError = false;
            bool defaultUnread = false;
            bool defaultRead = false;
            bool defaultUnreadScoreChange = false;
            bool riskRatingsEnabled = false;
            bool riskStatusEnabled = false;

            foreach (var item in SiteHelpers.LoggedInUser.TemplateFunctions)
            {
                switch (item.Key)
                {
                    case "chkDefault_12_High":
                        defaultHigh = true;
                        break;

                    case "chkDefault_13_Medium":
                        defaultMedium = true;
                        break;

                    case "chkDefault_14_Low":
                        defaultLow = true;
                        break;

                    case "chkDefault_15_KeoghsCFS":
                        defaultKeoghsCFS = true;
                        break;

                    case "chkDefault_16_PreviousVersion":
                        defaultPreviousVersion = true;
                        break;

                    case "chkDefault_17_Error":
                        defaultError = true;
                        break;

                    case "chkDefault_19_Unread":
                        defaultUnread = true;
                        break;

                    case "chkDefault_20_Read":
                        defaultRead = true;
                        break;

                    case "chkDefault_21_UnreadScoreChange":
                        defaultUnreadScoreChange = true;
                        break;

                    case "chkEnabled_18_Status":
                        riskStatusEnabled = true;
                        break;

                    case "chkEnabled_11_RiskRating":
                        riskRatingsEnabled = true;
                        break;
                    //case "NotesFilter_??_Status":
                    //    defaultNotes = true;
                    //    break;
                }
            }

            // Populate Risk Rating checkboxes
            if (!riskRatingsEnabled)
            {
                var riskRatings = new[]
                {
                    new FilterRiskRatingViewModel { Rating = "High", IsChecked =  true  },
                    new FilterRiskRatingViewModel { Rating = "Medium", IsChecked =  true  },
                    new FilterRiskRatingViewModel { Rating = "Low", IsChecked =  true  },
                    new FilterRiskRatingViewModel { Rating = "Keoghs CFS", IsChecked =  true },
                    new FilterRiskRatingViewModel { Rating = "Previous Version", IsChecked = true},
                    new FilterRiskRatingViewModel { Rating = "Error", IsChecked =  true }
                };
                claimsFilterViewModel.RiskRatings = riskRatings;
            }
            else
            {
                var riskRatings = new[]
                {
                    new FilterRiskRatingViewModel { Rating = "High", IsChecked = (defaultHigh)? true : false },
                    new FilterRiskRatingViewModel { Rating = "Medium", IsChecked = (defaultMedium) ? true : false },
                    new FilterRiskRatingViewModel { Rating = "Low", IsChecked = (defaultLow) ? true : false },
                    new FilterRiskRatingViewModel { Rating = "Keoghs CFS", IsChecked = (defaultKeoghsCFS) ? true : false },
                    new FilterRiskRatingViewModel { Rating = "Previous Version", IsChecked = (defaultPreviousVersion) ? true : false},
                    new FilterRiskRatingViewModel { Rating = "Error", IsChecked = (defaultError) ? true : false}
                };
                claimsFilterViewModel.RiskRatings = riskRatings;
            }
            //

            bool? recordReserveChanges = SiteHelpers.LoggedInClient.RecordReserveChanges;

            bool addReserveFilter = false;

            if (recordReserveChanges == true)
            {
                addReserveFilter = true;
            }

            if (!riskStatusEnabled)
            {
                var statusFilters = new[]
                {
                    new FilterStatusViewModel {FilterStatus = "Unread", IsChecked = true },
                    new FilterStatusViewModel {FilterStatus = "Read", IsChecked = true },
                    new FilterStatusViewModel {FilterStatus = "Unread Score Change", IsChecked = true},
                };

                if (addReserveFilter)
                {
                    var reserveStatusFilters = new[]
                    {
                        new FilterStatusViewModel {FilterStatus = "Reserve Change", IsChecked = false },
                    };

                    statusFilters = statusFilters.Concat(reserveStatusFilters).ToArray();
                }

                claimsFilterViewModel.FilterStatus = statusFilters;
            }
            else
            {
                var statusFilters = new[]
                {
                    new FilterStatusViewModel {FilterStatus = "Unread", IsChecked = (defaultUnread) ? true : false},
                    new FilterStatusViewModel {FilterStatus = "Read", IsChecked = (defaultRead) ? true : false},
                    new FilterStatusViewModel {FilterStatus = "Unread Score Change", IsChecked = (defaultUnreadScoreChange) ? true : false},
                };

                if (addReserveFilter)
                {
                    var reserveStatusFilters = new[]
                    {
                        new FilterStatusViewModel {FilterStatus = "Reserve Change", IsChecked = false },
                    };

                    statusFilters = statusFilters.Concat(reserveStatusFilters).ToArray();
                }

                claimsFilterViewModel.FilterStatus = statusFilters;
            }

            var reportFilters = new[]
            {
                new ReportStatusViewModel {ReportStatus = "Unrequested Reports", IsChecked = false},
                new ReportStatusViewModel {ReportStatus = "Pending Reports", IsChecked = false},
                new ReportStatusViewModel {ReportStatus = "Available Reports", IsChecked = false}
            };

            claimsFilterViewModel.ReportStatus = reportFilters;

            //
            var reportsRequested = new[]
                {
                    new FilterReportsRequested{ReportName="No requests",IsChecked=false},
                    new FilterReportsRequested{ReportName="Level 1",IsChecked=false},
                    new FilterReportsRequested{ReportName="Level 2",IsChecked=false},
                    new FilterReportsRequested{ReportName="Callback",IsChecked=false}
                };
            FilterReportsRequestedViewModel filterReportsRequestedViewModel = new FilterReportsRequestedViewModel
            {
                ReportsRequested = reportsRequested
            };
            claimsFilterViewModel.ReportsRequested = filterReportsRequestedViewModel;
            //
            return claimsFilterViewModel;
        }

        private static void SelectLastBatchNumber(IEnumerable<SelectListItem> batchNumberListItems, ClaimsFilterViewModel claimsFilterViewModel)
        {
            List<string> selectBatchReferences = new List<string>();

            foreach (var selectListItem in batchNumberListItems)
            {
                if (selectListItem.Selected)
                {
                    selectBatchReferences.Add(selectListItem.Value);
                }
            }

            claimsFilterViewModel.SelectedBatchId = selectBatchReferences.ToArray();
        }

        //private static void SelectLastBatchNumber(IEnumerable<SelectListItem> batchNumberListItems, ClaimsFilterViewModel claimsFilterViewModel)
        //{
        //    foreach (var selectListItem in batchNumberListItems)
        //    {
        //        if (selectListItem.Selected)
        //        {
        //            string test = selectListItem.Value;
        //            break;
        //        }
        //    }
        //}

        private static IEnumerable<SelectListItem> GetBatchNumbers(string role, int? selectedClientId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteFilterBatchNumbersResponse response = proxy.FilterBatchNumbers(new WebsiteFilterBatchNumbersRequest()
                                                                                    {
                                                                                        ClientId = SiteHelpers.LoggedInClient.Id,
                                                                                        UserId = SiteHelpers.LoggedInUser.Id,
                                                                                        Who = SiteHelpers.LoggedInUser.UserName,
                                                                                        Role = role,
                                                                                        SelectedClientId = selectedClientId
                                                                                    });

            string lastBatchNumber = null;

            foreach (var batchNumberListItem in response.BatchNumbersList)
            {
                lastBatchNumber = batchNumberListItem;
                break;
            }

            var result = response.BatchNumbersList.Select(x => new SelectListItem { Text = x, Value = x });

            var selectListItems = result as SelectListItem[] ?? result.ToArray();

            foreach (var selectListItem in selectListItems)
            {
                if (selectListItem.Text == lastBatchNumber)
                    selectListItem.Selected = true;
                break;
            }

            return selectListItems;
        }

        public static ClaimsListViewModel PopulateClaimsListViewModel()
        {
            return new ClaimsListViewModel();
        }

        #endregion populate view models

        public static void CreateTreeNode(List<DynaTreeNode> nodesList, DynaTreeNode parentNode, MessageNode node)
        {
            //System.Diagnostics.Debug.WriteLine(node.Text);

            var myNode = new DynaTreeNode();

            myNode.title = node.Text;

            if (parentNode == null)
            {
                //first root node
                myNode.isFolder = true;
                nodesList.Add(myNode);
            }
            else
            {
                if (node.Nodes.Any())
                    myNode.isFolder = true;
                parentNode.children.Add(myNode);
            }

            foreach (MessageNode child in node.Nodes)
            {
                CreateTreeNode(nodesList, myNode, child); //<-- recursive
            }
        }

        internal static object UpdateResultBatchListDropDown(int riskClientId)
        {
            //SetClaimReadStatusResponse

            //IADAWebServices proxy = Helper.Services.CreateADAServices();

            //SetClaimReadStatusResponse response = proxy.SetClaimReadStatus(new SetClaimReadStatusRequest()
            //{
            //    ClientId = SiteHelpers.LoggedInUser.RiskClientId,
            //    UserId = SiteHelpers.LoggedInUser.Id,
            //    Who = SiteHelpers.LoggedInUser.UserName,

            //    RiskClaimId = riskClaimId,
            //    RiskClaimReadStatus = riskClaimReadStatus,
            //});

            return null;
            //return response;
        }

        internal static List<FancyTreeItem> LoadUserTemplateData()
        {
            FancyTreeItem treeItem = new FancyTreeItem();
            FancyTreeItem treeChildItem1 = new FancyTreeItem();
            FancyTreeItem treeChildItem2 = new FancyTreeItem();
            FancyTreeItem treeChildItem3 = new FancyTreeItem();
            FancyTreeItem treeItem2 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_1 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_2 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_3 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_4 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_5 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_5_1 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_5_2 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_5_3 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_5_4 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_5_5 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_5_6 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_6 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_6_1 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_6_2 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_6_3 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_7 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_7_1 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_7_2 = new FancyTreeItem();
            FancyTreeItem treeChildItem2_1_7_3 = new FancyTreeItem();
            FancyTreeItem treeItem3 = new FancyTreeItem();
            FancyTreeItem treeItem4 = new FancyTreeItem();

            treeItem.title = "UPLOAD";
            treeItem.folder = false;
            treeItem.expanded = true;
            treeItem.key = "_1_UPLOAD";
            //treeItem.data = new Data { Icon = "" };
            treeChildItem1.title = "Batch Upload";
            treeChildItem1.key = "_2_BatchUpload";
            treeChildItem2.title = "Batch Level Reports";
            treeChildItem2.key = "_3_BatchLevelReports";
            treeChildItem3.title = "Single Claim Input";
            treeChildItem3.key = "_4_SingleClaimInput";
            treeItem.children.Add(treeChildItem1);
            treeItem.children.Add(treeChildItem2);
            treeItem.children.Add(treeChildItem3);

            treeItem2.title = "RESULTS";
            treeItem2.folder = false;
            treeItem2.expanded = true;
            treeItem2.key = "_5_RESULTS";

            treeChildItem2_1.title = "Refinement";
            treeChildItem2_1.key = "_6_Refinement";
            treeChildItem2_1.expanded = true;

            treeChildItem2_1_1.title = "Claim Number";
            treeChildItem2_1_1.key = "_7_ClaimNumber";
            treeChildItem2_1_2.title = "Batch Reference";
            treeChildItem2_1_2.key = "_8_BatchReference";
            //treeChildItem2_1_3.title = "Uploaded By";
            //treeChildItem2_1_3.key = "_9_UploadedBy";
            treeChildItem2_1_4.title = "Uploaded Date Range (M)";
            treeChildItem2_1_4.key = "_10_UploadedDateRange";
            treeChildItem2_1_5.title = "Risk Rating";
            treeChildItem2_1_5.expanded = true;
            treeChildItem2_1_5.key = "_11_RiskRating";
            treeChildItem2_1_5_1.title = "High";
            treeChildItem2_1_5_1.key = "_12_High";
            treeChildItem2_1_5_2.title = "Medium";
            treeChildItem2_1_5_2.key = "_13_Medium";
            treeChildItem2_1_5_3.title = "Low";
            treeChildItem2_1_5_3.key = "_14_Low";
            treeChildItem2_1_5_4.title = "Keoghs CFS";
            treeChildItem2_1_5_4.key = "_15_KeoghsCFS";
            treeChildItem2_1_5_5.title = "Previous Version";
            treeChildItem2_1_5_5.key = "_16_PreviousVersion";
            treeChildItem2_1_5_6.title = "Error";
            treeChildItem2_1_5_6.key = "_17_Error";

            treeChildItem2_1_5.children.Add(treeChildItem2_1_5_1);
            treeChildItem2_1_5.children.Add(treeChildItem2_1_5_2);
            treeChildItem2_1_5.children.Add(treeChildItem2_1_5_3);
            treeChildItem2_1_5.children.Add(treeChildItem2_1_5_4);
            treeChildItem2_1_5.children.Add(treeChildItem2_1_5_5);
            treeChildItem2_1_5.children.Add(treeChildItem2_1_5_6);

            treeChildItem2_1_6.title = "Status";
            treeChildItem2_1_6.key = "_18_Status";
            treeChildItem2_1_6.expanded = true;

            treeChildItem2_1.children.Add(treeChildItem2_1_1);
            treeChildItem2_1.children.Add(treeChildItem2_1_2);
            //treeChildItem2_1.children.Add(treeChildItem2_1_3);
            treeChildItem2_1.children.Add(treeChildItem2_1_4);
            treeChildItem2_1.children.Add(treeChildItem2_1_5);
            treeChildItem2_1.children.Add(treeChildItem2_1_6);
            //treeChildItem2_1.children.Add(treeChildItem2_1_7);
            treeItem2.children.Add(treeChildItem2_1);
            treeItem2.children.Add(treeChildItem2_1_7);

            treeChildItem2_1_6_1.title = "Unread";
            treeChildItem2_1_6_1.key = "_19_Unread";
            treeChildItem2_1_6_2.title = "Read";
            treeChildItem2_1_6_2.key = "_20_Read";
            treeChildItem2_1_6_3.title = "Unread Score Change";
            treeChildItem2_1_6_3.key = "_21_UnreadScoreChange";
            treeChildItem2_1_6.children.Add(treeChildItem2_1_6_1);
            treeChildItem2_1_6.children.Add(treeChildItem2_1_6_2);
            treeChildItem2_1_6.children.Add(treeChildItem2_1_6_3);

            treeChildItem2_1_7.title = "Actions";
            treeChildItem2_1_7.key = "_22_Actions";
            treeChildItem2_1_7.expanded = true;
            treeChildItem2_1_7.folder = false;

            treeChildItem2_1_7_1.title = "Level 1";
            treeChildItem2_1_7_1.key = "_23_Level 1";
            treeChildItem2_1_7_1.folder = false;
            treeChildItem2_1_7_2.title = "CUE";
            treeChildItem2_1_7_2.key = "_24_CUE";
            treeChildItem2_1_7_2.folder = false;
            treeChildItem2_1_7_3.title = "Notes/Decisions";
            treeChildItem2_1_7_3.key = "_24a_Notes";
            treeChildItem2_1_7_3.folder = false;
            treeChildItem2_1_7.children.Add(treeChildItem2_1_7_1);
            treeChildItem2_1_7.children.Add(treeChildItem2_1_7_2);
            treeChildItem2_1_7.children.Add(treeChildItem2_1_7_3);

            treeItem3.title = "ADA MAINTENANCE";
            treeItem3.folder = false;
            treeItem3.expanded = true;
            treeItem3.key = "_25_ADAMAINTENANCE";

            treeItem4.title = "SEARCH";
            treeItem4.folder = false;
            treeItem4.expanded = true;
            treeItem4.key = "_26_SEARCH";

            var FancyTreeItems = new List<FancyTreeItem>();

            FancyTreeItems.Add(treeItem);
            FancyTreeItems.Add(treeItem2);
            FancyTreeItems.Add(treeItem3);
            FancyTreeItems.Add(treeItem4);

            string response = "[" + treeItem.JsonToFancyTree() + "]";

            return FancyTreeItems;
        }

        internal static string SaveTemplateFunctions(List<Dictionary<string, string>> templateFunctions, string templateFunctionsJson, string templateId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteSaveTemplateFunctionsResponse response = proxy.SaveTemplateFunctions(new WebsiteSaveTemplateFunctionsRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                ListTemplateFunctionsJson = templateFunctions,
                TemplateFunctionsJson = templateFunctionsJson,
                Template_Id = Convert.ToInt32(templateId)
            });

            return "";
        }

        internal static TemplateFunctionsModel LoadTemplateFunctionValues(TemplateFunctionsModel model)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteLoadTemplateFunctionsResponse response = proxy.LoadTemplateFunctions(new WebsiteLoadTemplateFunctionsRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                Template_Id = Convert.ToInt32(model.Profile_Id)
            });

            model.FunctionValues = response.TemplateFunctionsJson;

            return model;
        }

        public static List<ExRiskUser> GetRiskUsersSummaryInfo()
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //

            RiskUsersResponse response = proxy.GetRiskUsers(new RiskUserRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
            });

            return response.RiskUserList.OrderBy(user => user.UserName).ToList();
        }

        internal static void CreateRiskRole(string templateName)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            WebsiteRiskRoleCreateResponse response = proxy.CreateRiskRole(new WebsiteRiskRoleCreateRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                TemplateName = templateName
            });
        }

        internal static void EditRiskRole(string templateName, int profileId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            WebsiteRiskRoleEditResponse response = proxy.EditRiskRole(new WebsiteRiskRoleEditRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                TemplateName = templateName,
                Profile_Id = profileId
            });
        }

        internal static int DeleteTemplate(string template_id)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            WebsiteRiskRoleDeleteResponse response = proxy.DeleteRiskRole(new WebsiteRiskRoleDeleteRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                Profile_Id = Convert.ToInt32(template_id)
            });

            return response.Result;
        }

        internal static int FindUserName(string userName)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            FindUserNameResponse response = proxy.FindUserName(new FindUserNameRequest()
            {
                UserName = userName
            });

            return response.Result;
        }

        public static List<ExRiskWord> GetRiskWords(int clientId)
        {
            var proxy = Helper.Services.CreateADAServices();

            RiskWordsResponse response = proxy.GetRiskWords(new RiskWordsRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                FilterClientId = clientId
            });

            var wordList = response.WordList.OrderBy(x => x.TableName);

            return wordList.ToList();
        }

        public static void CreateRiskWord(int riskClientId, string tableName, string fieldName, string lookupWord, string replacementWord, bool replaceWholeString, DateTime dateAdded, string searchType)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //

            var request = new CreateRiskWordRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                DateAdded = dateAdded,
                FieldName = fieldName,
                LookupWord = lookupWord,
                ReplacementWord = replacementWord,
                ReplaceWholeString = replaceWholeString,
                RiskClient_Id = riskClientId,
                SearchType = searchType,
                TableName = tableName,
            };

            proxy.CreateRiskWord(request);
            //
        }

        public static RiskWordDeleteResponse DeleteRiskWord(int riskWordId)
        {
            var proxy = Helper.Services.CreateADAServices();

            RiskWordDeleteResponse response = proxy.DeleteRiskWord(new RiskWordDeleteRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                RiskWordId = riskWordId
            });

            return response;
        }

        public static List<ExRiskDefaultData> GetRiskDefaultData(int clientId)
        {
            var proxy = Helper.Services.CreateADAServices();

            RiskDefaultDataResponse response = proxy.GetRiskDefaultData(new RiskDefaultDataRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                FilterClientId = clientId
            });

            var wordList = response.DefaultDataList.OrderBy(x => x.ConfigurationValue);

            return wordList.ToList();
        }

        public static List<ExRiskNoteDecision> GetRiskNoteDecision(int? clientId)
        {
            var proxy = Helper.Services.CreateADAServices();

            RiskNoteDecisionsResponse response = proxy.GetRiskNoteDecisions(new RiskNoteDecisionsRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,
                FilterRiskClientId = clientId
            });

            var list = response.DecisionList.OrderBy(x => x.Decision);

            return list.ToList();
        }

        public static void CreateRiskDefaultData(int riskClient_Id, string configurationValue, bool isActive, int riskConfigurationDescriptionId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //

            var request = new CreateRiskDefaultDataRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                ConfigurationValue = configurationValue,
                IsActive = isActive,
                RiskClient_Id = riskClient_Id,
                RiskConfigurationDescriptionId = riskConfigurationDescriptionId
            };

            proxy.CreateRiskDefaultData(request);
            //
        }

        public static void CreateRiskNoteDecision(int riskClient_Id, string decision, byte isActive)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //

            var request = new CreateRiskNoteDecisionRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                IsActive = isActive,
                RiskClient_Id = riskClient_Id,
                Decision = decision,
            };

            proxy.CreateRiskNoteDecision(request);
            //
        }

        public static void DeleteRiskDefaultData(int id)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //

            var request = new DeleteRiskDefaultDataRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                Id = id,
            };

            proxy.DeleteRiskDefaultData(request);
            //
        }

        public static void DeleteRiskNoteDecision(int id)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();
            //

            var request = new DeleteRiskNoteDecisionRequest()
            {
                ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName,

                Id = id,
            };

            proxy.DeleteRiskNoteDecision(request);
            //
        }

        public static GeneratePasswordResetTokenResponse GeneratePasswordResetToken(string userName, string token, DateTime tokenExpiration, string resetLink)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            GeneratePasswordResetTokenResponse response = proxy.GeneratePasswordResetToken(new GeneratePasswordResetTokenRequest()
            {
                UserName = userName,
                Token = token,
                TokenExpiration = tokenExpiration,
                ResetLink = resetLink,
            });

            return response;
        }

        public static ValidatePasswordResetTokenResponse ValidatePasswordResetToken(string token)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            ValidatePasswordResetTokenResponse response = proxy.ValidatePasswordResetToken(new ValidatePasswordResetTokenRequest()
            {
                Token = token,
            });

            return response;
        }

        internal static SendElmahErrorResponse SendElmahError(Elmah.ErrorMailEventArgs e)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            SendElmahErrorResponse response = proxy.SendElmahError(new SendElmahErrorRequest()
            {
                Error = e.Mail.Body
            });

            return response;
        }

        internal static List<PriorityClient> GetListOfPriorityBatchClients()
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            List<PriorityClient> listPriorityClients = new List<PriorityClient>();

            GetListOfBatchPriorityClientsResponse response = proxy.GetListOfBatchPriorityClients(new GetListOfBatchPriorityClientsRequest()
            {
                ClientId = SiteHelpers.LoggedInClient.Id,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName
            });

            foreach (var listItem in response.ListRiskClientBatchPriority)
            {
                listPriorityClients.Add(new PriorityClient { Client = listItem.Client, PriorityId = listItem.PriorityId });
            }

            return listPriorityClients;
        }

        internal static string SaveClientBatchPriority(int[] intArray)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            SaveBatchPriorityClientsResponse response = proxy.SaveBatchPriorityClients(new SaveBatchPriorityClientsRequest()
            {
                ClientBatchPriorityArray = intArray,
                ClientId = SiteHelpers.LoggedInClient.Id,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName 
            });

            return response.Error;
        }

        internal static List<BatchQueue> GetBatchQueue()
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            List<BatchQueue> BatchQueue = new List<Models.BatchQueue>();


            GetBatchQueueResponse response = proxy.GetBatchQueue(new GetBatchQueueRequest()
            {
                ClientId = SiteHelpers.LoggedInClient.Id,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName
            });

            foreach (var batch in response.BatchQueue)
            {
                BatchQueue.Add(new BatchQueue { BatchId = batch.BatchId, BatchRef = batch.BatchRef, ClaimsReceieved = batch.ClaimsReceieved, Client = batch.Client, Uploaded = Convert.ToDateTime(batch.Uploaded) });
            }

            return BatchQueue;
        }

        internal static string SaveOverridenBatchPriority(int[] intArray)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            SaveOverriddenBatchPriorityResponse response = proxy.SaveOverriddenBatchPriority(new SaveOverriddenBatchPriorityRequest()
            {
                OverriddenBatchPriorityArray = intArray,
                ClientId = SiteHelpers.LoggedInClient.Id,
                UserId = SiteHelpers.LoggedInUser.Id,
                Who = SiteHelpers.LoggedInUser.UserName
            });

            return response.Error;
        }
    }
}