﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.StaticClasses;
using MDA.WCF.WebServices.Interface;
using MDA.WCF.WebServices.Messages;
using KeoghsClientSite.Models.CUE;
using MDA.Pipeline.Model;

namespace KeoghsClientSite.Repositories.CUE
{
    public class CUE_Repository
    {
        internal static CueSearchEnquiryModel PopulateCUEInvolvements(int riskClaimId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            CueSearchEnquiryModel cueSearchEnquiryModel = new CueSearchEnquiryModel();

            WebsiteCueInvolvementsResponse response = proxy.GetCueInvolvements(
                new WebsiteCueInvolvementsRequest()
                {
                    RiskClaim_Id = riskClaimId,
                    ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                    UserId = SiteHelpers.LoggedInUser.Id,
                    Who = SiteHelpers.LoggedInUser.UserName
                });


            cueSearchEnquiryModel = Translate(response, riskClaimId);

            Validate(cueSearchEnquiryModel);

            return cueSearchEnquiryModel;
        }

        private static void Validate(CueSearchEnquiryModel CueInvolvements)
        {
            
            foreach (var involvement in CueInvolvements.CueInvolvements)
            {
                bool firstNameExists = false;
                bool lastNameExists = false;
                bool vehicleRegExists = false;


                if (involvement.FirstName != "Data not supplied")
                    firstNameExists = true;

                if (involvement.LastName != "Data not supplied")
                    lastNameExists = true;

                if (involvement.VehicleReg != "Data not supplied")
                    vehicleRegExists = true;

                var CueDatabaseAll = involvement.CUE_Databases.Where(x => x.Database == "Cross Enquiry").FirstOrDefault();

                if (firstNameExists && lastNameExists && vehicleRegExists)
                    CueDatabaseAll.IsChecked = false;
                else
                    CueDatabaseAll.ReadOnly = true;
            }
        }

        private static CueSearchEnquiryModel Translate(WebsiteCueInvolvementsResponse response, int riskClaimId)
        {
            CueSearchEnquiryModel cueSearchEnquiryModel = new CueSearchEnquiryModel();

            cueSearchEnquiryModel.ClaimNumber = response.ClaimNumber;
            cueSearchEnquiryModel.IncidentDate = response.IncidentDate;
            cueSearchEnquiryModel.UploadedDate = response.UploadedDate;
            cueSearchEnquiryModel.RiskClaim_Id = riskClaimId;

            string noDataSupplied = "Data not supplied";

            List<CueInvolvement> involvements = new List<CueInvolvement>();
            int count = 0;
            foreach (var i in response.CueInvolvements)
            {
                
                CueInvolvement involvement = new CueInvolvement();
                involvement.PersonId = response.CueInvolvements[count].CuePerson.PersonId;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CuePerson.FirstName))
                    involvement.FirstName = noDataSupplied;
                else
                    involvement.FirstName = response.CueInvolvements[count].CuePerson.FirstName;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CuePerson.LastName))
                    involvement.LastName = noDataSupplied;
                else
                    involvement.LastName = response.CueInvolvements[count].CuePerson.LastName;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CuePerson.NI_Number))
                    involvement.NI_Number = noDataSupplied;
                else
                    involvement.NI_Number = response.CueInvolvements[count].CuePerson.NI_Number;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CuePerson.DrivingLicenceNumber))
                    involvement.DrivingLicenceNumber = noDataSupplied;
                else
                    involvement.DrivingLicenceNumber = response.CueInvolvements[count].CuePerson.DrivingLicenceNumber;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CuePerson.Involvement))
                    involvement.Involvement = noDataSupplied;
                else
                    involvement.Involvement = response.CueInvolvements[count].CuePerson.Involvement;
                
                involvement.Dob = response.CueInvolvements[count].CuePerson.Dob;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CueAddress.HouseName))
                    involvement.HouseName = noDataSupplied;
                else
                    involvement.HouseName = response.CueInvolvements[count].CueAddress.HouseName;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CueAddress.Number))
                    involvement.Number = noDataSupplied;
                else
                    involvement.Number = response.CueInvolvements[count].CueAddress.Number;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CueAddress.Street))
                    involvement.Street = noDataSupplied;
                else
                    involvement.Street = response.CueInvolvements[count].CueAddress.Street;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CueAddress.Locality))
                    involvement.Locality = noDataSupplied;
                else
                    involvement.Locality = response.CueInvolvements[count].CueAddress.Locality;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CueAddress.Town))
                    involvement.Town = noDataSupplied;
                else
                    involvement.Town = response.CueInvolvements[count].CueAddress.Town;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CueAddress.PostCode))
                    involvement.PostCode = noDataSupplied;
                else
                    involvement.PostCode = response.CueInvolvements[count].CueAddress.PostCode;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CueVehicle.VehicleReg))
                    involvement.VehicleReg = noDataSupplied;
                else
                    involvement.VehicleReg = response.CueInvolvements[count].CueVehicle.VehicleReg;

                if (string.IsNullOrEmpty(response.CueInvolvements[count].CueVehicle.VIN))
                    involvement.VIN = noDataSupplied;
                else
                    involvement.VIN = response.CueInvolvements[count].CueVehicle.VIN;
               
              
                //involvement.CueDatabaseAll = i.CueDatabaseAll;

                var cueDatabases = new[] 
                {
                    new CUE_Databases { Database = "Cross Enquiry", IsChecked = false, ReadOnly=false },
                    new CUE_Databases { Database = "Motor", IsChecked = false , ReadOnly=true},
                    new CUE_Databases { Database = "PI", IsChecked = false , ReadOnly=true},
                    new CUE_Databases { Database = "Household", IsChecked = false , ReadOnly=true}
                };
              
                involvement.CUE_Databases = cueDatabases;

                involvements.Add(involvement);
                count++;
            }

            cueSearchEnquiryModel.CueInvolvements = involvements;

            return cueSearchEnquiryModel;
        }

        internal static string SubmitCUESearch(List<int> personIds, int riskClaimId )
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            WebsiteCueSearchResponse response = proxy.SubmitCueSearch(new WebsiteCueSearchRequest
                                                                          {
                                                                              RiskClaim_Id = riskClaimId,
                                                                              ListPerson_Id = personIds,
                                                                              ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                                                                              UserId = SiteHelpers.LoggedInUser.Id,
                                                                              Who = SiteHelpers.LoggedInUser.UserName
                                                                          });



            if (response.Result == 0)
                return "Success";
            else
                return response.Error;
           

            
        }

        internal static Results GetCUEReport(int riskClaimId)
        {
            IADAWebServices proxy = Helper.Services.CreateADAServices();

            Results reportResults = new Results();

            WebsiteCueReportResponse response = proxy.GetCueReport(
                new WebsiteCueReportRequest()
                {
                    ClientId = SiteHelpers.LoggedInUser.RiskClientId,
                    UserId = SiteHelpers.LoggedInUser.Id,
                    Who = SiteHelpers.LoggedInUser.UserName,
                    RiskClaim_Id = riskClaimId
                });

            reportResults = Translator.Translate(response.CueReportDetails);

            #region Old Mock data

            //reportModel.ClaimNumber = "AB123456789";
            //reportModel.SearchDate = DateTime.Now;

            //List<CUESearchSummary> lstSearchSummary = new List<CUESearchSummary>();
            //List<CUEResultsSummary> resultsSummary = new List<CUEResultsSummary>();
            //CueSearchCriteria searchCriteria = new CueSearchCriteria();
            //List<CUEIncidentSummary> lstIncidentSummary = new List<CUEIncidentSummary>();
            //List<CUESummaryIncident> lstCUESummary_Incidents = new List<CUESummaryIncident>();
            //List<CUEIncident> lstCUEIncidents = new List<CUEIncident>();
            //List<CUEClaimDataPolicyHolder> lstClaimDataPolicyHolders = new List<CUEClaimDataPolicyHolder>();
            //List<CUEPayment> lstClaimDataPayments = new List<CUEPayment>();

            //List<CUEIncidentInvolvement> lstCUEIncidentInvolvements = new List<CUEIncidentInvolvement>();
            //List<CUESupplier> lstCUESuppliers = new List<CUESupplier>();
             

            //lstSearchSummary.Add(new CUESearchSummary { Subject = "David Compton", Involvement = "Insured Driver", Motor = false, PI = false, Household = false, Cross_Enquiry = true, SearchDate = Convert.ToDateTime("01/06/2014 14:18") });
            //lstSearchSummary.Add(new CUESearchSummary { Subject = "Anne Compton", Involvement = "Insured Passenger", Motor = true, PI = true, Household = false, Cross_Enquiry = false, SearchDate = Convert.ToDateTime("01/06/2014 14:18") });
            //lstSearchSummary.Add(new CUESearchSummary { Subject = "James Gayle", Involvement = "Third Party Driver", Motor = false, PI = false, Household = false, Cross_Enquiry = true, SearchDate = Convert.ToDateTime("27/05/2014 09:16") });

            //resultsSummary.Add(new CUEResultsSummary { Incidents = "8", Involved_Household_Claims = "4", Involved_Motor_Claims = "2", Involved_PI_Claims = "3" });

            //searchCriteria.Name = "David Compton";
            //searchCriteria.Dob = "29/05/1964";
            //searchCriteria.NI_Number = "AB123456C";
            //searchCriteria.DrivingLicenceNum = "45621452";
            //searchCriteria.AddressNumber = "26";
            //searchCriteria.AddressTown= "Lancaster";
            //searchCriteria.AddressCity = "Lancashire";
            //searchCriteria.AddressPostCode = "LA1 2JE";
            //searchCriteria.VehicleReg = "MK59 FQL";
            //searchCriteria.VIN = "5GZCZ43D13S812715";

            //lstCUESummary_Incidents.Add(new CUESummaryIncident{ IncidentId = 1, Subject = "David Compton", IncidentType = "HH", InvolvementType="Policyholder", IncidentDate=Convert.ToDateTime("01/08/2012"), ClaimStatus="Outstanding", Insurer="AXA", Description="", MatchCode=""});

            //lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 2, Subject = "David Compton", IncidentType = "HH", InvolvementType = "Policyholder", IncidentDate = Convert.ToDateTime("12/05/2011"), ClaimStatus = "Breach of Condition", Insurer = "LV=", Description = "", MatchCode = "" });

            //lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 3, Subject = "Anne Compton", IncidentType = "PI", InvolvementType = "Claimant", IncidentDate = Convert.ToDateTime("24/02/2010"), ClaimStatus = "", Insurer = "DLG", Description = "", MatchCode = "" });

            //lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 4, Subject = "Anne Compton", IncidentType = "M", InvolvementType = "PolicyHolder", IncidentDate = Convert.ToDateTime("17/03/2009"), ClaimStatus = "Settled", Insurer = "Aviva", Description = "Struck TP Vehicle in read", MatchCode = "" });

            //lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 4, Subject = "Anne Compton", IncidentType = "PI", InvolvementType = "Claimant", IncidentDate = Convert.ToDateTime("17/03/2009"), ClaimStatus = "Outstanding", Insurer = "Aviva", Description = "Whiplash", MatchCode = "" });

            //lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 5, Subject = "David Compton", IncidentType = "PI", InvolvementType = "Claimant", IncidentDate = Convert.ToDateTime("09/08/2008"), ClaimStatus = "Settled", Insurer = "RBS", Description = "Broken Leg at workplace", MatchCode = "" });

            //lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 6, Subject = "James Compton", IncidentType = "PI", InvolvementType = "Claimant", IncidentDate = Convert.ToDateTime("26/12/2007"), ClaimStatus = "Settled", Insurer = "Liberty", Description = "", MatchCode = "" });

            //lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 7, Subject = "Henry Winter", IncidentType = "HH", InvolvementType = "PolicyHolder", IncidentDate = Convert.ToDateTime("04/03/2002"), ClaimStatus = "Settled", Insurer = "DLG", Description = "", MatchCode = "" });

            //lstCUESummary_Incidents.Add(new CUESummaryIncident { IncidentId = 8, Subject = "David Compton", IncidentType = "M", InvolvementType = "Third Party", IncidentDate = Convert.ToDateTime("19/11/1999"), ClaimStatus = "Settled", Insurer = "LV=", Description = "", MatchCode = "" });


            //lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 1, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 0, MO_Claims = 0, HH_Claims = 1, CUE_Incidents = lstCUESummary_Incidents.Where(x=>x.IncidentId==1).ToList() });

            //lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 2, From = Convert.ToDateTime("12/05/2011"), To = Convert.ToDateTime("12/05/2011"), PI_Claims = 0, MO_Claims = 0, HH_Claims = 1, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 2).ToList() });

            //lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 3, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 1, MO_Claims = 0, HH_Claims = 0, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 3).ToList() });

            //lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 4, From = Convert.ToDateTime("17/03/2009"), To = Convert.ToDateTime("17/03/2009"), PI_Claims = 1, MO_Claims = 1, HH_Claims = 0, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 4).ToList() });

            //lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 5, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 1, MO_Claims = 0, HH_Claims = 0, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 5).ToList() });

            //lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 6, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 1, MO_Claims = 0, HH_Claims = 0, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 6).ToList() });

            //lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 7, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 0, MO_Claims = 0, HH_Claims = 1, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 7).ToList() });

            //lstIncidentSummary.Add(new CUEIncidentSummary { IncidentId = 8, From = Convert.ToDateTime("01/08/2012"), To = Convert.ToDateTime("01/08/2012"), PI_Claims = 0, MO_Claims = 1, HH_Claims = 0, CUE_Incidents = lstCUESummary_Incidents.Where(x => x.IncidentId == 8).ToList() });


            //#region Incident 1

            //CUEIncident incident1 = new CUEIncident();
            //lstClaimDataPolicyHolders.Add(new CUEClaimDataPolicyHolder { IncidentId=1, FullName = "David Compton", Address = "26 Wood Road, Lancaster, Lancashire, LA1 2JE" });
            //lstClaimDataPolicyHolders.Add(new CUEClaimDataPolicyHolder { IncidentId=1, FullName = "Anne Compton", Address = "26 Wood Road, Lancaster, Lancashire, LA1 2JE" });

            //lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "Buildings", PaymentAmount = 0.00M });
            //lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "Contents", PaymentAmount = 0.00M });
            //lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "All Risks", PaymentAmount = 0.00M });
            //lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "Freezer", PaymentAmount = 0.00M });
            //lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "Caravan", PaymentAmount = 0.00M });
            //lstClaimDataPayments.Add(new CUEPayment { IncidentId = 1, PaymentType = "Other", PaymentAmount = 0.00M });

            //CUEClaimData cueClaimData = new CUEClaimData();
            //CUEGeneralData cueGeneralData = new CUEGeneralData();

            //cueClaimData.ClaimNumber = "5796679W";
            //cueClaimData.Insurer = "AXA";
            //cueClaimData.PolicyHolders = lstClaimDataPolicyHolders.Where(x => x.IncidentId == 1).ToList();

            //cueGeneralData.ClaimStatus = "Outstanding";
            //cueGeneralData.IncidentDescription = "Not Provided";
            //cueGeneralData.PolicyType = "Buildings";
            //cueGeneralData.PolicyNumber = "AB6054987";
            //cueGeneralData.PolicyNumberID = "Standard";
            //cueGeneralData.CollectivePolicyIndicator = "Non Collective Policy";
            //cueGeneralData.LossSetupDate = Convert.ToDateTime("20/08/2012");
            //cueGeneralData.InsurerContactName = "Jamie Bull";
            //cueGeneralData.InsurerContactTel = "0845 166 5177";
            //cueGeneralData.IncidentDate = Convert.ToDateTime("01/08/2012");
            //cueGeneralData.PolicyInceptionDate = Convert.ToDateTime("01/08/2009");
            //cueGeneralData.PolicyPeriodEndDate = Convert.ToDateTime("01/08/2011");
            //cueGeneralData.CauseOfLoss = "Storm";
            //cueGeneralData.CatastropheRelated = "Unknown";
            //cueGeneralData.RiskAddressNumber = "26";
            //cueGeneralData.RiskAddressStreet = "Wood Road";
            //cueGeneralData.RiskAddressTown = "Lancaster";
            //cueGeneralData.RiskAddressCity = "Lancashire";
            //cueGeneralData.RiskAddressPostCode = "LA1 2JE";
            //cueGeneralData.CUEPayments = lstClaimDataPayments.Where(x => x.IncidentId == 1).ToList();

            //incident1.IncidentId = 1;
            //incident1.IncidentType = "HH";
            //incident1.ClaimData = cueClaimData;
            //incident1.GeneralData = cueGeneralData;
          
            //CUEIncidentInvolvement incident1CUEIncidentInvolvement = new CUEIncidentInvolvement();
            //incident1CUEIncidentInvolvement.Incident_Id = 1;
            //incident1CUEIncidentInvolvement.Type = "Policyholder 1 (Claimant)";
            //incident1CUEIncidentInvolvement.Status = "Personal";
            //incident1CUEIncidentInvolvement.Title = "Mr";
            //incident1CUEIncidentInvolvement.Forename = "David";
            //incident1CUEIncidentInvolvement.MiddleName = "";
            //incident1CUEIncidentInvolvement.LastName = "Compton";
            //incident1CUEIncidentInvolvement.NI_Number = "LW021290C";
            //incident1CUEIncidentInvolvement.AddressNumber = "26";
            //incident1CUEIncidentInvolvement.AddressStreet = "Wood Road";
            //incident1CUEIncidentInvolvement.AddressTown = "Lancaster";
            //incident1CUEIncidentInvolvement.AddressCity = "Lancashire";
            //incident1CUEIncidentInvolvement.AddressPostCode = "LA1 2JE";
            //incident1CUEIncidentInvolvement.AddressIndicator = "PAF Valid";
            //incident1CUEIncidentInvolvement.Sex = "Male";
            //incident1CUEIncidentInvolvement.Dob = Convert.ToDateTime("29/05/1964");
            //incident1CUEIncidentInvolvement.PaymentAmount = 700.58M;
            //incident1CUEIncidentInvolvement.Telephone = "01789 469106";
            //incident1CUEIncidentInvolvement.Occupation = "Unknown";

            //CUEIncidentInvolvement incident2CUEIncidentInvolvement = new CUEIncidentInvolvement();
            //incident2CUEIncidentInvolvement.Incident_Id = 1;
            //incident2CUEIncidentInvolvement.Type = "Policyholder 2";
            //incident2CUEIncidentInvolvement.Status = "Personal";
            //incident2CUEIncidentInvolvement.Title = "Mrs";
            //incident2CUEIncidentInvolvement.Forename = "Anne";
            //incident2CUEIncidentInvolvement.MiddleName = "";
            //incident2CUEIncidentInvolvement.LastName = "Compton";
            //incident2CUEIncidentInvolvement.NI_Number = "LW021290C";
            //incident2CUEIncidentInvolvement.AddressNumber = "26";
            //incident2CUEIncidentInvolvement.AddressStreet = "Wood Road";
            //incident2CUEIncidentInvolvement.AddressTown = "Lancaster";
            //incident2CUEIncidentInvolvement.AddressCity = "Lancashire";
            //incident2CUEIncidentInvolvement.AddressPostCode = "LA1 2JE";
            //incident2CUEIncidentInvolvement.AddressIndicator = "PAF Valid";
            //incident2CUEIncidentInvolvement.Sex = "Female";
            //incident2CUEIncidentInvolvement.Dob = Convert.ToDateTime("14/09/1967");
            //incident2CUEIncidentInvolvement.PaymentAmount = 0.00M;
            //incident2CUEIncidentInvolvement.Telephone = "01789 469106";
            //incident2CUEIncidentInvolvement.Occupation = "Unknown";

            //CUESupplier supplier1 = new CUESupplier();
            //supplier1.IncidentId = 1;
            //supplier1.CompanyName = "Metric Consulatants";
            //supplier1.SupplierStatus = "Business";
            //supplier1.Telephone = "0845 401 8644";
            //supplier1.AddressHouseName = "Metric House";
            //supplier1.AddressStreet = "Gibson Street";
            //supplier1.AddressCity = "Blackburn";
            //supplier1.AddressPostCode = "BB4 8JL";
            //supplier1.AddressIndicator = "PAF Valid";


            //lstCUEIncidentInvolvements.Add(incident1CUEIncidentInvolvement);
            //lstCUEIncidentInvolvements.Add(incident2CUEIncidentInvolvement);

            //lstCUESuppliers.Add(supplier1);
            //incident1.Suppliers = lstCUESuppliers;
            //incident1.CUEIncidentInvolvements = lstCUEIncidentInvolvements;

            //#endregion

            //#region Incident 2

            //CUEIncident incident2 = new CUEIncident();
            //incident2.IncidentId = 2;
            //incident2.IncidentType = "M";
            //lstClaimDataPolicyHolders.Add(new CUEClaimDataPolicyHolder { IncidentId = 2, FullName = "David Compton", Address = "26 Wood Road, Lancaster, Lancashire, LA1 2JE" });

            //CUEClaimData cueClaimData2 = new CUEClaimData();
            //cueClaimData2.ClaimNumber = "48662001479892";
            //cueClaimData2.Insurer = "LV=";
            //cueClaimData2.PolicyHolders = lstClaimDataPolicyHolders.Where(x => x.IncidentId == 2).ToList();

            //incident2.ClaimData = cueClaimData2;

            //List<CUEPayment> ClaimDataPayments = new List<CUEPayment>();
            //ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "Buildings", PaymentAmount = 0.00M });
            //ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "Contents", PaymentAmount = 0.00M });
            //ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "All Risks", PaymentAmount = 0.00M });
            //ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "Freezer", PaymentAmount = 0.00M });
            //ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "Caravan", PaymentAmount = 0.00M });
            //ClaimDataPayments.Add(new CUEPayment { IncidentId = 2, PaymentType = "Other", PaymentAmount = 0.00M });

            //CUEGeneralData cueGeneralData2 = new CUEGeneralData();
            //cueGeneralData2.ClaimStatus = "Outstanding";
            //cueGeneralData2.IncidentDescription = "Struck third party vehicle in rear";
            //cueGeneralData2.PolicyType = "Motor";
            //cueGeneralData2.PolicyNumber = "AB6054987";
            //cueGeneralData2.PolicyNumberID = "Standard";
            //cueGeneralData2.CollectivePolicyIndicator = "Non Collective Policy";
            //cueGeneralData2.LossSetupDate = Convert.ToDateTime("20/08/2012");
            //cueGeneralData2.InsurerContactName = "Jamie Bull";
            //cueGeneralData2.InsurerContactTel = "0845 166 5177";
            //cueGeneralData2.IncidentDate = Convert.ToDateTime("01/08/2012");
            //cueGeneralData2.PolicyInceptionDate = Convert.ToDateTime("01/08/2009");
            //cueGeneralData2.PolicyPeriodEndDate = Convert.ToDateTime("01/08/2011");
            //cueGeneralData2.CauseOfLoss = "Storm";
            //cueGeneralData2.CatastropheRelated = "Unknown";
            //cueGeneralData2.RiskAddressNumber = "26";
            //cueGeneralData2.RiskAddressStreet = "Wood Road";
            //cueGeneralData2.RiskAddressTown = "Lancaster";
            //cueGeneralData2.RiskAddressCity = "Lancashire";
            //cueGeneralData2.RiskAddressPostCode = "LA1 2JE";
            //cueGeneralData2.CUEPayments = ClaimDataPayments.Where(x => x.IncidentId == 2).ToList();

            //incident2.GeneralData = cueGeneralData2;

            //CUEIncidentInvolvementVehicleDetails policyHolder1 = new CUEIncidentInvolvementVehicleDetails();
            //policyHolder1.Incident_Id = 1;
            //policyHolder1.Type = "Policyholder 1";
            //policyHolder1.Status = "Personal";
            //policyHolder1.Title = "Mr";
            //policyHolder1.Forename = "David";
            //policyHolder1.MiddleName = "";
            //policyHolder1.LastName = "Compton";
            //policyHolder1.NI_Number = "LW021290C";
            //policyHolder1.AddressNumber = "26";
            //policyHolder1.AddressStreet = "Wood Road";
            //policyHolder1.AddressTown = "Lancaster";
            //policyHolder1.AddressCity = "Lancashire";
            //policyHolder1.AddressPostCode = "LA1 2JE";
            //policyHolder1.AddressIndicator = "PAF Valid";
            //policyHolder1.Sex = "Male";
            //policyHolder1.Dob = Convert.ToDateTime("29/05/1964");
            //policyHolder1.PaymentAmount = 700.58M;
            //policyHolder1.Telephone = "01789 469106";
            //policyHolder1.Occupation = "Unknown";

            //policyHolder1.VehicleRegistration = "MA04 DFP";
            //policyHolder1.VehicleIdentificationNumber = "5GZCZ43D13S812715";
            //policyHolder1.Make = "Audi";
            //policyHolder1.DamageStatus = "Cat 1";
            //policyHolder1.Model = "A1";
            //policyHolder1.RecoveredStatus = "n/a";
            //policyHolder1.Colour = "White";
            //policyHolder1.RegistrationStatus = "n/a";
            //policyHolder1.CoverType = "Comprehensive";

            //CUEIncidentInvolvementVehicleDetails thirdParty1 = new CUEIncidentInvolvementVehicleDetails();
            //thirdParty1.Incident_Id = 1;
            //thirdParty1.Type = "Third Party 1";
            //thirdParty1.Status = "Personal";
            //thirdParty1.Title = "Mrs";
            //thirdParty1.Forename = "Janet";
            //thirdParty1.MiddleName = "Laura";
            //thirdParty1.LastName = "Heywood";
            //thirdParty1.NI_Number = "LW021290C";
            //thirdParty1.AddressNumber = "543";
            //thirdParty1.AddressStreet = "Church Road";
            //thirdParty1.AddressTown = "Morecambe";
            //thirdParty1.AddressCity = "Lancashire";
            //thirdParty1.AddressPostCode = "MC1 8WN";
            //thirdParty1.AddressIndicator = "Not PAF Valid";
            //thirdParty1.Sex = "Female";
            //thirdParty1.Dob = Convert.ToDateTime("16/11/1983");
            //thirdParty1.PaymentAmount = 0.00M;
            //thirdParty1.Telephone = "01789 746004";
            //thirdParty1.Occupation = "Unknown";

            //thirdParty1.VehicleRegistration = "MA04 DFP";
            //thirdParty1.VehicleIdentificationNumber = "5GZCZ43D13S812715";
            //thirdParty1.Make = "Audi";
            //thirdParty1.DamageStatus = "Cat 1";
            //thirdParty1.Model = "A1";
            //thirdParty1.RecoveredStatus = "n/a";
            //thirdParty1.Colour = "White";
            //thirdParty1.RegistrationStatus = "n/a";
            //thirdParty1.CoverType = "Comprehensive";

            //CUESupplier supplier2 = new CUESupplier();
            //supplier2.IncidentId = 2;
            //supplier2.CompanyName = "Coles Law Ltd.";
            //supplier2.SupplierStatus = "Business";
            //supplier2.Telephone = "0845 559 1430";
            //supplier2.AddressHouseName = "121";
            //supplier2.AddressStreet = "Stump Street";
            //supplier2.AddressCity = "Harrogate";
            //supplier2.AddressPostCode = "HA12 7HW";
            //supplier2.AddressIndicator = "PAF Valid";
            //supplier2.PaymentAmount = 1450.55M;

            //List<CUEIncidentInvolvement> CUEIncident2Involvements = new List<CUEIncidentInvolvement>();
            //CUEIncident2Involvements.Add(policyHolder1);
            //CUEIncident2Involvements.Add(thirdParty1);

            //List<CUESupplier> CUESuppliers2 = new List<CUESupplier>();
            //CUESuppliers2.Add(supplier2);
            //incident2.Suppliers = CUESuppliers2;
            //incident2.CUEIncidentInvolvements = CUEIncident2Involvements;

            //#endregion

            //#region Incident 3

            //CUEIncident incident3 = new CUEIncident();
            //incident3.IncidentId = 3;
            //incident3.IncidentType = "PI";
            //lstClaimDataPolicyHolders.Add(new CUEClaimDataPolicyHolder { IncidentId = 3, FullName = "David Compton", Address = "26 Wood Road, Lancaster, Lancashire, LA1 2JE" });

            //CUEClaimData cueClaimData3 = new CUEClaimData();
            //cueClaimData3.ClaimNumber = "48662001479892";
            //cueClaimData3.Insurer = "LV=";
            //cueClaimData3.PolicyHolders = lstClaimDataPolicyHolders.Where(x => x.IncidentId == 3).ToList();

            //incident3.ClaimData = cueClaimData3;

            //CUEGeneralData cueGeneralData3 = new CUEGeneralData();
            //cueGeneralData3.ClaimStatus = "Outstanding";
            //cueGeneralData3.IncidentDescription = "Struck third party vehicle in rear";
            //cueGeneralData3.PolicyType = "Motor";
            //cueGeneralData3.PolicyNumber = "AB6054987";
            //cueGeneralData3.PolicyNumberID = "Standard";
            //cueGeneralData3.CollectivePolicyIndicator = "Non Collective Policy";
            //cueGeneralData3.LossSetupDate = Convert.ToDateTime("20/08/2012");
            //cueGeneralData3.InsurerContactName = "Jamie Bull";
            //cueGeneralData3.InsurerContactTel = "0845 166 5177";
            //cueGeneralData3.IncidentDate = Convert.ToDateTime("01/08/2012");
            //cueGeneralData3.PolicyInceptionDate = Convert.ToDateTime("01/08/2009");
            //cueGeneralData3.PolicyPeriodEndDate = Convert.ToDateTime("01/08/2011");
            //cueGeneralData3.CauseOfLoss = "Storm";
            //cueGeneralData3.CatastropheRelated = "Unknown";
            //cueGeneralData3.RiskAddressNumber = "26";
            //cueGeneralData3.RiskAddressStreet = "Wood Road";
            //cueGeneralData3.RiskAddressTown = "Lancaster";
            //cueGeneralData3.RiskAddressCity = "Lancashire";
            //cueGeneralData3.RiskAddressPostCode = "LA1 2JE";
            //cueGeneralData3.CUEPayments = lstClaimDataPayments.Where(x => x.IncidentId == 3).ToList();

            //incident3.GeneralData = cueGeneralData3;

            //CUEIncidentInvolvement claimant1 = new CUEIncidentInvolvement();
            //claimant1.Incident_Id = 3;
            //claimant1.Type = "Claimant 1";
            //claimant1.Status = "Personal";
            //claimant1.Title = "Mr";
            //claimant1.Forename = "David";
            //claimant1.MiddleName = "";
            //claimant1.LastName = "Compton";
            //claimant1.NI_Number = "LW021290C";
            //claimant1.AddressNumber = "26";
            //claimant1.AddressStreet = "Wood Road";
            //claimant1.AddressTown = "Lancaster";
            //claimant1.AddressCity = "Lancashire";
            //claimant1.AddressPostCode = "LA1 2JE";
            //claimant1.AddressIndicator = "PAF Valid";
            //claimant1.Sex = "Male";
            //claimant1.Dob = Convert.ToDateTime("29/05/1964");
            //claimant1.PaymentAmount = 700.58M;
            //claimant1.Telephone = "01789 469106";
            //claimant1.Occupation = "Unknown";

            //CUEIncidentDetails incidentDetails = new CUEIncidentDetails();

            //incidentDetails.InjuryDescription = "Whiplash";
            //incidentDetails.HospitalAttended = "Yes";

            //incident3.IncidentDetails = incidentDetails;

            //List<CUEIncidentInvolvement> CUEIncident3Involvements = new List<CUEIncidentInvolvement>();
            //CUEIncident3Involvements.Add(claimant1);
            //incident3.CUEIncidentInvolvements = CUEIncident3Involvements;

            //#endregion

            //lstCUEIncidents.Add(incident1);
            //lstCUEIncidents.Add(incident2);
            //lstCUEIncidents.Add(incident3);


            //reportModel.SearchSummary = lstSearchSummary;
            //reportModel.ResultsSummary = resultsSummary;
            //reportModel.SearchCriteria = searchCriteria;
            //reportModel.IncidentSummary = lstIncidentSummary;
            //reportModel.CUE_Incidents = lstCUEIncidents;
            #endregion

            return reportResults;
        }
    }
}