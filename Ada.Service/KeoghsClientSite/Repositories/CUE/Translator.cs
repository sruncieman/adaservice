﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeoghsClientSite.Models.CUE;
using MDA.WCF.WebServices.Messages;

namespace KeoghsClientSite.Repositories.CUE
{
    public class Translator
    {
        internal static Results Translate(ExCueReportDetails exCueReportDetails)
        {
            return new Results()
            {
                ClaimNumber = exCueReportDetails.ClaimNumber,
                SearchDate = exCueReportDetails.SearchDate,
                ListResultInfo =  Translator.Translate(exCueReportDetails.ListResultInfo)
            };
        }

        private static List<Dictionary<string, CUE_ReportModel>> Translate(List<Dictionary<string, ExCueReportResult>> lstResultInfo)
        {
            List<Dictionary<string, CUE_ReportModel>> lstReport = new List<Dictionary<string, CUE_ReportModel>>();

            foreach (var item in lstResultInfo)
            {

                Dictionary<string, CUE_ReportModel> dictExCUE_ReportModel = new Dictionary<string, CUE_ReportModel>();

                foreach (var dictItem in item)
                {
                    dictExCUE_ReportModel.Add(dictItem.Key, Translator.Translate(dictItem.Value));
                }

                lstReport.Add(dictExCUE_ReportModel);
            }

            return lstReport;
        }

        private static CUE_ReportModel Translate(ExCueReportResult exCueReportResult)
        {
            CUE_ReportModel reportModel = new CUE_ReportModel();

           
            reportModel.SearchSummary = Translator.Translate(exCueReportResult.SearchSummary);
            reportModel.ResultsSummary = Translator.Translate(exCueReportResult.ResultsSummary);
            reportModel.SearchCriteria = Translator.Translate(exCueReportResult.SearchCriteria);
            reportModel.IncidentSummary = Translator.Translate(exCueReportResult.IncidentSummary);
            reportModel.CUE_Incidents = Translator.Translate(exCueReportResult.CUE_Incidents);

            return reportModel;
        }

        

       

        private static List<CUEIncident> Translate(List<ExCUEIncident> cueLstIncident)
        {
            List<CUEIncident> lstIncident = new List<CUEIncident>();

            foreach (var item in cueLstIncident)
            {
                CUEIncident incident = new CUEIncident();
                incident.ClaimData = Translator.Translate(item.ClaimData);
                incident.GeneralData = Translator.Translate(item.GeneralData);
                incident.IncidentId = item.IncidentId;
                incident.IncidentType = item.IncidentType;


                if (incident.IncidentDetails != null)
                {
                    var iDetails = incident.IncidentDetails = new CUEIncidentDetails();
                    iDetails.HospitalAttended = item.IncidentDetails.HospitalAttended;
                    iDetails.IllnessDiseaseDescription = item.IncidentDetails.IllnessDiseaseDescription;
                    iDetails.InjuryDescription = item.IncidentDetails.InjuryDescription;

                    incident.IncidentDetails = iDetails;
                }
                incident.CUEIncidentInvolvements = Translator.Translate(item.CUEIncidentInvolvements);
                if (item.CUEIncidentInvolvementVehicleDetails != null)
                    incident.CUEIncidentInvolvementVehicleDetails = Translator.Translate(item.CUEIncidentInvolvementVehicleDetails);

                if (item.Suppliers != null)
                    incident.Suppliers = Translator.Translate(item.Suppliers);

                if (item.IncidentDetails != null)
                    incident.IncidentDetails = Translator.Translate(item.IncidentDetails);

                lstIncident.Add(incident);
            }

            return lstIncident;
        }

        private static CUEGeneralData Translate(ExCUEGeneralData cueGeneralData)
        {
            CUEGeneralData cueGeneralDataInfo = new CUEGeneralData();

            cueGeneralDataInfo.CatastropheRelated = cueGeneralData.CatastropheRelated;
            cueGeneralDataInfo.CauseOfLoss = cueGeneralData.CauseOfLoss;
            cueGeneralDataInfo.ClaimStatus = cueGeneralData.ClaimStatus;
            cueGeneralDataInfo.ClosedDate = cueGeneralData.ClosedDate;
            cueGeneralDataInfo.CollectivePolicyIndicator = cueGeneralData.CollectivePolicyIndicator;
            cueGeneralDataInfo.CUEPayments = Translator.Translate(cueGeneralData.CUEPayments);
            cueGeneralDataInfo.IncidentDescription = cueGeneralData.IncidentDescription;
            cueGeneralDataInfo.InsurerContactName = cueGeneralData.InsurerContactName;
            cueGeneralDataInfo.InsurerContactTel = cueGeneralData.InsurerContactTel;
            cueGeneralDataInfo.LossSetupDate = cueGeneralData.LossSetupDate;
            cueGeneralDataInfo.PolicyInceptionDate = cueGeneralData.PolicyInceptionDate;
            cueGeneralDataInfo.PolicyNumber = cueGeneralData.PolicyNumber;
            cueGeneralDataInfo.PolicyNumberID = cueGeneralData.PolicyNumberID;
            cueGeneralDataInfo.PolicyPeriodEndDate = cueGeneralData.PolicyPeriodEndDate;
            cueGeneralDataInfo.PolicyType = cueGeneralData.PolicyType;
            cueGeneralDataInfo.RiskAddressCity = cueGeneralData.RiskAddressCity;
            cueGeneralDataInfo.RiskAddressHouseName = cueGeneralData.RiskAddressHouseName;
            cueGeneralDataInfo.RiskAddressLocality = cueGeneralData.RiskAddressLocality;
            cueGeneralDataInfo.RiskAddressNumber = cueGeneralData.RiskAddressNumber;
            cueGeneralDataInfo.RiskAddressPostCode = cueGeneralData.RiskAddressPostCode;
            cueGeneralDataInfo.RiskAddressStreet = cueGeneralData.RiskAddressStreet;
            cueGeneralDataInfo.RiskAddressTown = cueGeneralData.RiskAddressTown;
            cueGeneralDataInfo.IncidentDate = cueGeneralData.IncidentDate;

            return cueGeneralDataInfo;
        }

        private static List<CUEPayment> Translate(List<ExCUEPayment> cuePaymentList)
        {
            List<CUEPayment> lstPayments = new List<CUEPayment>();

            foreach (var item in cuePaymentList)
            {
                CUEPayment cuePayment = new CUEPayment();
                cuePayment.IncidentId = item.IncidentId;
                cuePayment.PaymentAmount = item.PaymentAmount;
                cuePayment.PaymentType = item.PaymentType;
                lstPayments.Add(cuePayment);
            }

            return lstPayments;
        }

        private static CUEIncidentDetails Translate(ExCUEIncidentDetails cueIncidentDetails)
        {
            CUEIncidentDetails cueIncidentDetail = new CUEIncidentDetails();

            cueIncidentDetail.HospitalAttended = cueIncidentDetails.HospitalAttended;
            cueIncidentDetail.IllnessDiseaseDescription = cueIncidentDetails.IllnessDiseaseDescription;
            cueIncidentDetail.InjuryDescription = cueIncidentDetails.InjuryDescription;

            return cueIncidentDetail;
        }
        private static List<CUESupplier> Translate(List<ExCUESupplier> cueListSuppliers)
        {
            List<CUESupplier> lstSuppliers = new List<CUESupplier>();

            foreach (var item in cueListSuppliers)
            {
                CUESupplier cueSupplier = new CUESupplier();
                cueSupplier.AddressCity = item.AddressCity;
                cueSupplier.AddressHouseName = item.AddressHouseName;
                cueSupplier.AddressIndicator = item.AddressIndicator;
                cueSupplier.AddressLocality = item.AddressLocality;
                cueSupplier.AddressNumber = item.AddressNumber;
                cueSupplier.AddressPostCode = item.AddressPostCode;
                cueSupplier.AddressStreet = item.AddressStreet;
                cueSupplier.AddressTown = item.AddressTown;
                cueSupplier.CompanyName = item.CompanyName;
                cueSupplier.IncidentId = item.IncidentId;
                cueSupplier.PaymentAmount = item.PaymentAmount;
                cueSupplier.SupplierStatus = item.SupplierStatus;
                cueSupplier.Telephone = item.Telephone;
                cueSupplier.VAT = item.VAT;

                lstSuppliers.Add(cueSupplier);
            }

            return lstSuppliers;
        }

        private static List<CUEIncidentInvolvementVehicleDetails> Translate(List<ExCUEIncidentInvolvementVehicleDetails> cueListInvolvementVehicleDetails)
        {
            List<CUEIncidentInvolvementVehicleDetails> lstIncidentInvolvementVehicleDetails = new List<CUEIncidentInvolvementVehicleDetails>();

            foreach (var item in cueListInvolvementVehicleDetails)
            {
                CUEIncidentInvolvementVehicleDetails incidentInvolvementVehicleDetail = new CUEIncidentInvolvementVehicleDetails();
                incidentInvolvementVehicleDetail.AddressCity = item.AddressCity;
                incidentInvolvementVehicleDetail.AddressHouseName = item.AddressHouseName;
                incidentInvolvementVehicleDetail.AddressIndicator = item.AddressIndicator;
                incidentInvolvementVehicleDetail.AddressLocality = item.AddressLocality;
                incidentInvolvementVehicleDetail.AddressNumber = item.AddressNumber;
                incidentInvolvementVehicleDetail.AddressPostCode = item.AddressPostCode;
                incidentInvolvementVehicleDetail.AddressStreet = item.AddressStreet;
                incidentInvolvementVehicleDetail.AddressTown = item.AddressTown;
                incidentInvolvementVehicleDetail.Colour = item.Colour;
                incidentInvolvementVehicleDetail.CoverType = item.CoverType;
                incidentInvolvementVehicleDetail.DamageStatus = item.DamageStatus;
                incidentInvolvementVehicleDetail.Dob = item.Dob;
                incidentInvolvementVehicleDetail.Forename = item.Forename;
                incidentInvolvementVehicleDetail.Incident_Id = item.Incident_Id;
                incidentInvolvementVehicleDetail.LastName = item.LastName;
                incidentInvolvementVehicleDetail.Make = item.Make;
                incidentInvolvementVehicleDetail.MiddleName = item.MiddleName;
                incidentInvolvementVehicleDetail.Model = item.Model;
                incidentInvolvementVehicleDetail.NewForOldIndicator = item.NewForOldIndicator;
                incidentInvolvementVehicleDetail.NI_Number = item.NI_Number;
                incidentInvolvementVehicleDetail.Occupation = item.Occupation;
                incidentInvolvementVehicleDetail.PaymentAmount = item.PaymentAmount;
                incidentInvolvementVehicleDetail.RecoveredStatus = item.RecoveredStatus;
                incidentInvolvementVehicleDetail.RegistrationStatus = item.RegistrationStatus;
                incidentInvolvementVehicleDetail.Sex = item.Sex;
                incidentInvolvementVehicleDetail.Status = item.Status;
                incidentInvolvementVehicleDetail.Telephone = item.Telephone;
                incidentInvolvementVehicleDetail.Title = item.Title;
                incidentInvolvementVehicleDetail.Type = item.Type;
                incidentInvolvementVehicleDetail.VAT = item.VAT;
                incidentInvolvementVehicleDetail.VehicleIdentificationNumber = item.VehicleIdentificationNumber;
                incidentInvolvementVehicleDetail.VehicleRegistration = item.VehicleRegistration;
                incidentInvolvementVehicleDetail.VIN = item.VIN;

                lstIncidentInvolvementVehicleDetails.Add(incidentInvolvementVehicleDetail);
            }

            return lstIncidentInvolvementVehicleDetails;
        }

        private static List<CUEIncidentInvolvement> Translate(List<ExCUEIncidentInvolvement> cueListIncidentInvolvements)
        {
            List<CUEIncidentInvolvement> lstIncidentInvolvements = new List<CUEIncidentInvolvement>();

            foreach (var item in cueListIncidentInvolvements)
            {
                CUEIncidentInvolvement incidentInvolvement = new CUEIncidentInvolvement();
                incidentInvolvement.AddressCity = item.AddressCity;
                incidentInvolvement.AddressHouseName = item.AddressHouseName;
                incidentInvolvement.AddressIndicator = item.AddressIndicator;
                incidentInvolvement.AddressLocality = item.AddressLocality;
                incidentInvolvement.AddressNumber = item.AddressNumber;
                incidentInvolvement.AddressPostCode = item.AddressPostCode;
                incidentInvolvement.AddressStreet = item.AddressStreet;
                incidentInvolvement.AddressTown = item.AddressTown;
                incidentInvolvement.Dob = item.Dob;
                incidentInvolvement.Forename = item.Forename;
                incidentInvolvement.Incident_Id = item.Incident_Id;
                incidentInvolvement.LastName = item.LastName;
                incidentInvolvement.MiddleName = item.MiddleName;
                incidentInvolvement.NI_Number = item.NI_Number;
                incidentInvolvement.Occupation = item.Occupation;
                incidentInvolvement.PaymentAmount = item.PaymentAmount;
                incidentInvolvement.Sex = item.Sex;
                incidentInvolvement.Status = item.Status;
                incidentInvolvement.Telephone = item.Telephone;
                incidentInvolvement.Title = item.Title;
                incidentInvolvement.Type = item.Type;
                incidentInvolvement.VAT = item.VAT;

                lstIncidentInvolvements.Add(incidentInvolvement);

            }

            return lstIncidentInvolvements;
        }

        private static CUEClaimData Translate(ExCUEClaimData cueClaimData)
        {
            CUEClaimData claimData = new CUEClaimData();

            claimData.ClaimNumber = cueClaimData.ClaimNumber;
            claimData.Insurer = cueClaimData.Insurer;
            claimData.PolicyHolders = Translator.Translate(cueClaimData.PolicyHolders);

            return claimData;

        }

        private static List<CUEClaimDataPolicyHolder> Translate(List<ExCUEClaimDataPolicyHolder> cueLstClaimDataPolicyHolders)
        {
            List<CUEClaimDataPolicyHolder> lstClaimDataPolicyHolders = new List<CUEClaimDataPolicyHolder>();

            foreach (var item in cueLstClaimDataPolicyHolders)
            {
                CUEClaimDataPolicyHolder claimDataPolicyHolder = new CUEClaimDataPolicyHolder();
                claimDataPolicyHolder.Address = item.Address;
                claimDataPolicyHolder.FullName = item.FullName;
                claimDataPolicyHolder.IncidentId = item.IncidentId;

                lstClaimDataPolicyHolders.Add(claimDataPolicyHolder);
            }

            return lstClaimDataPolicyHolders;
        }

        private static List<CUEIncidentSummary> Translate(List<ExCUEIncidentSummary> cueLstIncidentSummary)
        {
            List<CUEIncidentSummary> lstIncidentSummary = new List<CUEIncidentSummary>();

            foreach (var item in cueLstIncidentSummary)
            {
                CUEIncidentSummary incidentSummary = new CUEIncidentSummary();
                incidentSummary.From = item.From;
                incidentSummary.HH_Claims = item.HH_Claims;
                incidentSummary.IncidentId = item.IncidentId;
                incidentSummary.MO_Claims = item.MO_Claims;
                incidentSummary.PI_Claims = item.PI_Claims;
                incidentSummary.To = item.To;
                incidentSummary.CUE_Incidents = Translator.Translate(item.CUE_Incidents);

                lstIncidentSummary.Add(incidentSummary);
            }

            return lstIncidentSummary;
        }

      

        private static List<CUESummaryIncident> Translate(List<ExCUESummaryIncident> cueLstSummaryIncident)
        {
            List<CUESummaryIncident> lstSummaryIncidents = new List<CUESummaryIncident>();

            foreach (var item in cueLstSummaryIncident)
            {
                CUESummaryIncident summaryIncident = new CUESummaryIncident();
                summaryIncident.ClaimStatus = item.ClaimStatus;
                summaryIncident.Description = item.Description;
                summaryIncident.IncidentDate = item.IncidentDate;
                summaryIncident.IncidentId = item.IncidentId;
                summaryIncident.IncidentType = item.IncidentType;
                summaryIncident.Insurer = item.Insurer;
                summaryIncident.InvolvementType = item.InvolvementType;
                summaryIncident.MatchCode = item.MatchCode;
                summaryIncident.Subject = item.Subject;

                lstSummaryIncidents.Add(summaryIncident);
            }

            return lstSummaryIncidents;
        }

        private static CueSearchCriteria Translate(ExCueSearchCriteria cueSearchCriteria)
        {
            return new CueSearchCriteria()
            {
                AddressCity = cueSearchCriteria.AddressCity,
                AddressHouseName = cueSearchCriteria.AddressHouseName,
                AddressLocality = cueSearchCriteria.AddressLocality,
                AddressNumber = cueSearchCriteria.AddressNumber,
                AddressPostCode = cueSearchCriteria.AddressPostCode,
                AddressStreet = cueSearchCriteria.AddressCity,
                AddressTown = cueSearchCriteria.AddressTown,
                Dob = cueSearchCriteria.Dob,
                DrivingLicenceNum = cueSearchCriteria.DrivingLicenceNum,
                Name = cueSearchCriteria.Name,
                NI_Number = cueSearchCriteria.NI_Number,
                VehicleReg = cueSearchCriteria.VehicleReg,
                VIN = cueSearchCriteria.VIN
            };
        }

        private static List<CUEResultsSummary> Translate(List<ExCUEResultsSummary> cueLstResultsSummary)
        {
            List<CUEResultsSummary> lstResultsSummary = new List<CUEResultsSummary>();
            
            foreach (var item in cueLstResultsSummary)
            {
                CUEResultsSummary resultsSummary = new CUEResultsSummary();
                resultsSummary.Incidents = item.Incidents;
                resultsSummary.Involved_Household_Claims = item.Involved_Household_Claims;
                resultsSummary.Involved_Motor_Claims = item.Involved_Motor_Claims;
                resultsSummary.Involved_PI_Claims = item.Involved_PI_Claims;

                lstResultsSummary.Add(resultsSummary);
            }

            return lstResultsSummary;
        }


        

        private static List<CUESearchSummary> Translate(List<ExCUESearchSummary> cueLstSearchSummary)
        {
            List<CUESearchSummary> lstSearchSummary = new List<CUESearchSummary>();

            foreach (var item in cueLstSearchSummary)
            {
                CUESearchSummary searchSummary = new CUESearchSummary();
                searchSummary.Cross_Enquiry = item.Cross_Enquiry;
                searchSummary.Household = item.Household;
                searchSummary.Involvement = item.Involvement;
                searchSummary.Motor = item.Motor;
                searchSummary.PI = item.PI;
                searchSummary.SearchDate = item.SearchDate;
                searchSummary.Subject = item.Subject;

                lstSearchSummary.Add(searchSummary);
            }

            return lstSearchSummary;
        }
    }
}