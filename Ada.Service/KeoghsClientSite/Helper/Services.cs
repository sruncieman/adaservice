﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace KeoghsClientSite.Helper
{
    public class Services
    {
        public static MDA.WCF.WebServices.Interface.IADAWebServices CreateADAServices()
        {
            //if (Convert.ToBoolean(ConfigurationManager.AppSettings["UseLocalDb"]))
            //    return new KeoghsClientSite.Interfaces.ADADBService();
            //else
                return new KeoghsClientSite.Interfaces.ADAWebService();
        }

        public static MDA.WCF.TransferServices.IADATransferServices CreateADATransferServices()
        {
            //if (Convert.ToBoolean(ConfigurationManager.AppSettings["UseLocalDb"]))
            //    return new KeoghsClientSite.Interfaces.ADADBTransferService();
            //else
                return new KeoghsClientSite.Interfaces.ADATransferService();
        }
    }
}