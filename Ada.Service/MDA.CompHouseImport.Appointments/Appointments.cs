﻿using System;
using FileHelpers;

namespace MDA.CompHouseImport.Appointments
{
    [IgnoreFirst(1)]
    [IgnoreLast(1)]

    [FixedLengthRecord(FixedMode.AllowVariableLength)]
    public sealed class Appointments
    {
        [FieldFixedLength(8)]
        public String CompanyNumber;
        [FieldFixedLength(1)]
        public String RecordType;
        [FieldFixedLength(1)]
        public String AppDateOrigin;
        [FieldFixedLength(2)]
        public String AppointmentType;
        [FieldFixedLength(12)]
        public String PersonNumber;
        [FieldFixedLength(1)]
        public String CorporateIndicator;
        [FieldFixedLength(7)]
        public String Filler;
        [FieldFixedLength(8)]
        public String AppointmentDate;
        [FieldFixedLength(8)]
        public String ResignationDate;
        [FieldFixedLength(8)]
        public String PersonPostCode;
        [FieldFixedLength(8)]
        public String PersonDateOfBirth;
        [FieldFixedLength(1125)]
        public String VariableLengthData;
        [FieldOptional]
        [FieldFixedLength(50)]
        public String Length;
        [FieldOptional]
        [FieldFixedLength(50)]
        public String Title;
        [FieldOptional]
        [FieldFixedLength(50)]
        public String Forenames;
        [FieldOptional]
        [FieldFixedLength(160)]
        public String Surname;
        [FieldOptional]
        [FieldFixedLength(10)]
        public String POBox;
        [FieldOptional]
        [FieldFixedLength(251)]
        public String AddressLine1;
        [FieldOptional]
        [FieldFixedLength(50)]
        public String AddressLine2;
        [FieldOptional]
        [FieldFixedLength(50)]
        public String PostTown;
        [FieldOptional]
        [FieldFixedLength(50)]
        public String County;
        [FieldOptional]
        [FieldFixedLength(40)]
        public String Occupation;
        [FieldOptional]
        [FieldFixedLength(40)]
        public String Nationality;
    }

    [FixedLengthRecord(FixedMode.AllowVariableLength)]
    public sealed class Companies
    {
        [FieldFixedLength(50)]
        public String Field1;
    }
}
