﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.CompHouseImport.Appointments
{
    class Program
    {
        public static string AppointmentNum = string.Empty;

        static void Main(string[] args)
        {
            Int32 ImportId = 0;
            string path = @"C:\Dev\Projects\MDA\MDASolution\MDA.CompHouseImport.Appointments\Resource\";
                           
            foreach (var file in Directory.GetFiles(path, "ProdTest.dat", SearchOption.AllDirectories))
            {
                Stopwatch stopwatch = new Stopwatch();

                MultiRecordEngine engine;

                engine = new MultiRecordEngine(typeof(Appointments), typeof(Companies));
                engine.RecordSelector = new RecordTypeSelector(CustomSelector);
                DateTime? dateTime;

                engine.BeginReadFile(file);

                string connectionString = GetNewConnection();
                var conn = new SqlConnection(connectionString);

                stopwatch.Start();

                string sqlQuery = "Insert INTO CH_ImportLog (FileUploaded, CH_Type, UploadDate)";
                sqlQuery += "OUTPUT INSERTED.ID  VALUES (@Filename, @CH_Type, @UploadDate)";

                using (SqlConnection dataConnection = new SqlConnection(connectionString))
                {
                    using (var dataCommand = new SqlCommand(sqlQuery, conn))
                    {
                        dataConnection.Open();
                        dataCommand.CommandType = CommandType.Text;
                        dataCommand.CommandText = sqlQuery;
                        dataCommand.Parameters.AddWithValue("@Filename", Path.GetFileName(file));
                        dataCommand.Parameters.AddWithValue("@CH_Type", "Appointment");
                        dataCommand.Parameters.AddWithValue("@UploadDate", DateTime.Now);
                        conn.Open();
                        ImportId = (Int32)dataCommand.ExecuteScalar();
                        dataConnection.Close();
                        conn.Close();
                    }
                }

                try
                {
                    conn.Open();
                    using (conn)
                    {
                        foreach (Appointments appointments in engine)
                        {
                            AppointmentNum = appointments.CompanyNumber;

                            var sqlCmd = new SqlCommand();
                            sqlCmd.CommandText = "CH_InsertAppointments";
                            sqlCmd.CommandType = CommandType.StoredProcedure;
                            sqlCmd.Parameters.AddWithValue("@ImportId", ImportId);
                            sqlCmd.Parameters.AddWithValue("@CompanyNumber", appointments.CompanyNumber);
                            sqlCmd.Parameters.AddWithValue("@AppointmentType", appointments.AppointmentType);
                            sqlCmd.Parameters.AddWithValue("@PersonNumber", appointments.PersonNumber);
                            sqlCmd.Parameters.AddWithValue("@Postcode", appointments.PersonPostCode);

                            if (appointments.VariableLengthData.Trim() != "")
                            {
                                var variableDataArray = appointments.VariableLengthData.Split('<');

                                appointments.Length = variableDataArray[0];

                                if (variableDataArray[0].Length > 4)
                                {
                                    appointments.Title = variableDataArray[0].Substring(4,
                                                                                        variableDataArray[0].Length -
                                                                                        4);
                                    appointments.Forenames = variableDataArray[1];
                                    appointments.Surname = variableDataArray[2];
                                    appointments.POBox = variableDataArray[5];
                                    appointments.AddressLine1 = variableDataArray[6];
                                    appointments.AddressLine2 = variableDataArray[7];
                                    appointments.PostTown = variableDataArray[8];
                                    appointments.County = variableDataArray[9];
                                    appointments.Occupation = variableDataArray[11];
                                    appointments.Nationality = variableDataArray[12];
                                }

                                else
                                {
                                    appointments.Forenames = variableDataArray[1];
                                    appointments.Surname = variableDataArray[2];
                                    appointments.POBox = variableDataArray[5];
                                    appointments.AddressLine1 = variableDataArray[6];
                                    appointments.AddressLine2 = variableDataArray[7];
                                    appointments.PostTown = variableDataArray[8];
                                    appointments.County = variableDataArray[9];
                                    appointments.Occupation = variableDataArray[11];
                                    appointments.Nationality = variableDataArray[12];
                                }
                            }

                            sqlCmd.Parameters.AddWithValue("@Title", appointments.Title);
                            sqlCmd.Parameters.AddWithValue("@Forenames", appointments.Forenames);
                            sqlCmd.Parameters.AddWithValue("@Surname", appointments.Surname);
                            sqlCmd.Parameters.AddWithValue("@POBox", appointments.POBox);
                            sqlCmd.Parameters.AddWithValue("@AddressLine1", appointments.AddressLine1);
                            sqlCmd.Parameters.AddWithValue("@AddressLine2", appointments.AddressLine2);
                            sqlCmd.Parameters.AddWithValue("@PostTown", appointments.PostTown);
                            sqlCmd.Parameters.AddWithValue("@County", appointments.County);
                            sqlCmd.Parameters.AddWithValue("@Occupation", appointments.Occupation);
                            sqlCmd.Parameters.AddWithValue("@Nationality", appointments.Nationality);

                            sqlCmd.Parameters.AddWithValue("@AppointmentDate", SqlDbType.DateTime).Value =
                                DateTimeTryParse(appointments.AppointmentDate, out dateTime)
                                    ? (object)dateTime
                                    : DBNull.Value;

                            appointments.AppointmentDate = dateTime.ToString();

                            sqlCmd.Parameters.AddWithValue("@ResignationDate", SqlDbType.DateTime).Value =
                                DateTimeTryParse(appointments.ResignationDate, out dateTime)
                                    ? (object)dateTime
                                    : DBNull.Value;

                            appointments.ResignationDate = dateTime.ToString();

                            sqlCmd.Parameters.AddWithValue("@Dob", SqlDbType.DateTime).Value =
                                DateTimeTryParse(appointments.PersonDateOfBirth, out dateTime)
                                    ? (object)dateTime
                                    : DBNull.Value;

                            appointments.PersonDateOfBirth = dateTime.ToString();

                            sqlCmd.Connection = conn;
                            sqlCmd.ExecuteNonQuery();
                        }
                        stopwatch.Stop();

                        // Get the elapsed time as a TimeSpan value.
                        TimeSpan ts = stopwatch.Elapsed;

                        // Format and display the TimeSpan value. 
                        string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                                           ts.Hours, ts.Minutes, ts.Seconds,
                                                           ts.Milliseconds / 10);
                        Console.WriteLine("RunTime " + elapsedTime);

                        SendEmail(true, null, file, elapsedTime, null);
                    }
                }

                catch (Exception ex)
                {
                    Console.WriteLine(AppointmentNum);
                    SendEmail(false, ex, file, null, AppointmentNum);
                }

                finally
                {
                    conn.Close();
                    engine.Close();
                }
            }
        }

        private static Type CustomSelector(MultiRecordEngine engine, string recordString)
        {
            if (recordString.Substring(8, 1) == "2")
            {
                return typeof(Appointments);
            }

            return null;
        }

        private static void SendEmail(bool result, Exception ex, string fileName, string elapsedTime, string appointmentNumber)
        {
            MailMessage mail = new MailMessage("admin@CompaniesAppointments.co.uk", "jonathanwalker@keoghs.co.uk");
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Host = ConfigurationManager.AppSettings["Smtp"];

            if (result)
            {
                mail.Subject = "Companies House Appointments Import Success.";
                mail.Body = "Companies House Appointments Import was successful. " + Environment.NewLine + Environment.NewLine + "File: " + fileName + Environment.NewLine;
                mail.Body += "Runtime: " + elapsedTime;
            }
            else
            {
                mail.Subject = "Companies House Appointments Import Failure.";
                mail.Body = "Companies House Appointments Import was unsucessful. File: " + fileName + Environment.NewLine;
                mail.Body += ex;
                mail.Body += Environment.NewLine + Environment.NewLine + " Company Appointment Number: " + appointmentNumber;
            }

            client.Send(mail);
        }


        public static string GetNewConnection()
        {
            return ConfigurationManager.ConnectionStrings["CompaniesHouseDbContext"].ConnectionString;
        }

        public static bool DateTimeTryParse(string text, out DateTime? result)
        {
            result = null;
            // We allow an empty string for null (could also use IsNullOrWhitespace)
            if (String.IsNullOrEmpty(text.Trim())) return true;

            string modifiedDateTxt;

            modifiedDateTxt = text.Substring(6, 2) + "/" + text.Substring(4, 2) + "/" + text.Substring(0, 4);

            DateTime d;
            if (!DateTime.TryParse(modifiedDateTxt, out d)) return false;
            result = d;

            if (d.Year < 1753) return false;

            return true;
        }
    
    }
}
