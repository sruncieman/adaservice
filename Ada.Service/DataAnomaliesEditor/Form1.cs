﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace DataAnomaliesEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the '_MDA_Link_UnitTestDataSet.RiskWordFilter' table. You can move, or remove it, as needed.
            this.riskWordFilterTableAdapter.Fill(this._MDA_Link_UnitTestDataSet.RiskWordFilter);

        }


        private void button1_Click(object sender, EventArgs e)
        {


            foreach (DataGridViewRow row in dataGridView1.Rows)
            {


                if (!string.IsNullOrEmpty(textBox1.Text) && row.Cells["lookupWordDataGridViewTextBoxColumn"].Value != null)
                {
                    if (row.Cells["lookupWordDataGridViewTextBoxColumn"].Value.ToString().Equals(textBox1.Text))
                    {
                        //row.Selected = true;
                        row.Visible = true;
                    }
                    else
                    {
                        CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dataGridView1.DataSource];
                        currencyManager1.SuspendBinding();
                        dataGridView1.Rows[row.Index].Visible = false;
                        currencyManager1.ResumeBinding();
                    }
                }
                else
                {
                    if(string.IsNullOrEmpty(textBox1.Text))
                    {
                        row.Visible = true;
                    }
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
