﻿namespace DataAnomaliesEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.riskWordFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._MDA_Link_UnitTestDataSet = new DataAnomaliesEditor.DataAnomaliesDataSet();
            this.riskWordFilterTableAdapter = new DataAnomaliesEditor._MDA_Link_UnitTestDataSetTableAdapters.RiskWordFilterTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.riskClientIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fieldNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lookupWordDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.replacementWordDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.searchTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.replaceWholeStringDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dateAddedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riskWordFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._MDA_Link_UnitTestDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.riskClientIdDataGridViewTextBoxColumn,
            this.tableNameDataGridViewTextBoxColumn,
            this.fieldNameDataGridViewTextBoxColumn,
            this.lookupWordDataGridViewTextBoxColumn,
            this.replacementWordDataGridViewTextBoxColumn,
            this.searchTypeDataGridViewTextBoxColumn,
            this.replaceWholeStringDataGridViewCheckBoxColumn,
            this.dateAddedDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.riskWordFilterBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 39);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(946, 448);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // riskWordFilterBindingSource
            // 
            this.riskWordFilterBindingSource.DataMember = "RiskWordFilter";
            this.riskWordFilterBindingSource.DataSource = this._MDA_Link_UnitTestDataSet;
            // 
            // _MDA_Link_UnitTestDataSet
            // 
            this._MDA_Link_UnitTestDataSet.DataSetName = "_MDA_Link_UnitTestDataSet";
            this._MDA_Link_UnitTestDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // riskWordFilterTableAdapter
            // 
            this.riskWordFilterTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(93, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(152, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // riskClientIdDataGridViewTextBoxColumn
            // 
            this.riskClientIdDataGridViewTextBoxColumn.DataPropertyName = "RiskClient_Id";
            this.riskClientIdDataGridViewTextBoxColumn.HeaderText = "RiskClient_Id";
            this.riskClientIdDataGridViewTextBoxColumn.Name = "riskClientIdDataGridViewTextBoxColumn";
            // 
            // tableNameDataGridViewTextBoxColumn
            // 
            this.tableNameDataGridViewTextBoxColumn.DataPropertyName = "TableName";
            this.tableNameDataGridViewTextBoxColumn.HeaderText = "TableName";
            this.tableNameDataGridViewTextBoxColumn.Name = "tableNameDataGridViewTextBoxColumn";
            this.tableNameDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tableNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // fieldNameDataGridViewTextBoxColumn
            // 
            this.fieldNameDataGridViewTextBoxColumn.DataPropertyName = "FieldName";
            this.fieldNameDataGridViewTextBoxColumn.HeaderText = "FieldName";
            this.fieldNameDataGridViewTextBoxColumn.Name = "fieldNameDataGridViewTextBoxColumn";
            this.fieldNameDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fieldNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // lookupWordDataGridViewTextBoxColumn
            // 
            this.lookupWordDataGridViewTextBoxColumn.DataPropertyName = "LookupWord";
            this.lookupWordDataGridViewTextBoxColumn.HeaderText = "LookupWord";
            this.lookupWordDataGridViewTextBoxColumn.Name = "lookupWordDataGridViewTextBoxColumn";
            // 
            // replacementWordDataGridViewTextBoxColumn
            // 
            this.replacementWordDataGridViewTextBoxColumn.DataPropertyName = "ReplacementWord";
            this.replacementWordDataGridViewTextBoxColumn.HeaderText = "ReplacementWord";
            this.replacementWordDataGridViewTextBoxColumn.Name = "replacementWordDataGridViewTextBoxColumn";
            // 
            // searchTypeDataGridViewTextBoxColumn
            // 
            this.searchTypeDataGridViewTextBoxColumn.DataPropertyName = "SearchType";
            this.searchTypeDataGridViewTextBoxColumn.HeaderText = "SearchType";
            this.searchTypeDataGridViewTextBoxColumn.Items.AddRange(new object[] {
            "=",
            "~",
            "<",
            ">"});
            this.searchTypeDataGridViewTextBoxColumn.Name = "searchTypeDataGridViewTextBoxColumn";
            this.searchTypeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.searchTypeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // replaceWholeStringDataGridViewCheckBoxColumn
            // 
            this.replaceWholeStringDataGridViewCheckBoxColumn.DataPropertyName = "ReplaceWholeString";
            this.replaceWholeStringDataGridViewCheckBoxColumn.HeaderText = "ReplaceWholeString";
            this.replaceWholeStringDataGridViewCheckBoxColumn.Name = "replaceWholeStringDataGridViewCheckBoxColumn";
            // 
            // dateAddedDataGridViewTextBoxColumn
            // 
            this.dateAddedDataGridViewTextBoxColumn.DataPropertyName = "DateAdded";
            dataGridViewCellStyle2.NullValue = "dd/mm/yyyy";
            this.dateAddedDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.dateAddedDataGridViewTextBoxColumn.HeaderText = "DateAdded";
            this.dateAddedDataGridViewTextBoxColumn.Name = "dateAddedDataGridViewTextBoxColumn";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 499);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Data Anomalies";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riskWordFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._MDA_Link_UnitTestDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private DataAnomaliesDataSet _MDA_Link_UnitTestDataSet;
        private System.Windows.Forms.BindingSource riskWordFilterBindingSource;
        private _MDA_Link_UnitTestDataSetTableAdapters.RiskWordFilterTableAdapter riskWordFilterTableAdapter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn riskClientIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lookupWordDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn replacementWordDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn searchTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn replaceWholeStringDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateAddedDataGridViewTextBoxColumn;
    }
}

