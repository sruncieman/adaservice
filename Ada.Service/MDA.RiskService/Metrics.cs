﻿using MDA.ExternalServices.Model;
using MDA.RiskService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService
{
    public partial class RiskServices
    {
        public RunningMetrics GetRunningMetrics()
        {
            var m = new RunningMetrics();

            try
            {
                m.BatchesWaiting = (from x in _db.RiskBatches where x.BatchStatus == (int)RiskBatchStatus.Initialising select x).Count();
                m.BatchesLoading = (from x in _db.RiskBatches where x.BatchStatus == (int)RiskBatchStatus.ClaimsBeingLoaded select x).Count();
                m.BatchesTotal = (from x in _db.RiskBatches select x).Count();
                m.BatchesError = (from x in _db.RiskBatches where x.BatchStatus < (int)RiskBatchStatus.Initialising select x).Count();

                m.ClaimsWaitingToBeLoaded = (from x in _db.RiskClaims
                                             where x.Id > 0
                                                 && (x.ClaimStatus == (int)RiskClaimStatus.QueuedForLoading
                                                     || x.ClaimStatus == (int)RiskClaimStatus.IsNew)
                                                 &&
                                                 (x.RiskBatch.BatchStatus >= (int)RiskBatchStatus.ClaimsQueuedForLoading
                                                     && x.RiskBatch.BatchStatus <= (int)RiskBatchStatus.ClaimsBeingLoaded)
                                             select x).Count();

                m.ClaimsWaitingToBeScored = (from x in _db.RiskClaims
                                             where x.Id > 0
                                                 &&
                                                 (x.ClaimStatus == (int)RiskClaimStatus.QueuedForScoring
                                                     || x.ClaimStatus == (int)RiskClaimStatus.ThirdPartyComplete)
                                                 && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
                                             select x).Count();

                m.ClaimsBeingScored = (from x in _db.RiskClaims
                                       where x.Id > 0
                                          && x.ClaimStatus == (int)RiskClaimStatus.BeingScored
                                          && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
                                       select x).Count();

                m.ClaimsWaitingOnExternal = (from x in _db.RiskExternalServicesRequests
                                             where
                                                 x.CallStatus == (int)RiskExternalServiceCallStatus.Created
                                             select x).Count();

                m.ExternalRetriesRequired = (from x in _db.RiskExternalServicesRequests
                                             where
                                                 x.CallStatus == (int)RiskExternalServiceCallStatus.UnavailableNeedsRetry
                                                 || x.CallStatus == (int)RiskExternalServiceCallStatus.FailedNeedsRetry
                                             select x).Count();

                m.ExternalErrors = (from x in _db.RiskExternalServicesRequests
                                    where x.CallStatus < 0
                                    select x).Count();


                var AverageLoadingTime = ((from x in _db.RiskClaims where x.ClaimStatus == 3 orderby x.Id descending select x).Take(20).Select(a => a.LoadingDurationInMs).Average());

                var AverageScoringTime = ((from x in _db.RiskClaimRuns orderby x.Id descending select x).Take(20).Select(a => a.ScoringDurationInMs).Average());

                m.AverageLoadingTime = (int)(AverageLoadingTime);

                m.AverageScoringTime = (int)(AverageScoringTime);
            }
            catch (Exception)
            {
            }
            return m;
        }
    }
}
