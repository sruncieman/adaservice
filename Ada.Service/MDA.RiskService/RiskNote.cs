﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Data.Entity.Core;
using System.Xml;
using MDA.Pipeline.Model;
using MDA.RiskService.Model;
using MDA.DAL;
using Newtonsoft.Json;
using MDA.Common;

using System.Xml.Serialization;
using LinqKit;
using RiskEngine.Model;
using RiskEngine.Scoring.Entities;
using RiskEngine.Scoring.Model;
using MDA.Common.Server;
using System.Data.Entity.Validation;

namespace MDA.RiskService
{
    public partial class RiskServices
    {
        public void CreateRiskNote(int clientId, string createdBy, DateTime createdDate, int? decision_Id, bool deleted, string noteDesc, int[] riskClaim_Id, int userId, int? visibilty, int? userTypeId, byte[] originalFile, string fileName, string fileType)
        {
            foreach (var claim in riskClaim_Id)
            {
                var baseRiskClaim_Id = GetRiskClaim(claim);

                var n = _db.RiskNotes.Create();

                n.BaseRiskClaim_Id = baseRiskClaim_Id.BaseRiskClaim_Id;
                n.CreatedBy = createdBy;
                n.CreatedDate = createdDate;
                n.DecisionId = decision_Id;
                n.Deleted = deleted;
                n.Note = noteDesc;
                n.RiskClaim_Id = claim;
                n.Visibility = visibilty;
                n.UserType = userTypeId;
                //n.RiskNoteFile_Id = 0;

                _db.RiskNotes.Add(n);

                _db.SaveChanges();

                if (originalFile != null)
                {
                    SaveFileIntoRiskNoteFilesRecord(fileName, fileType, n.Id, originalFile);
                }                
            }
        }

        public void UpdateRiskNote(int riskNoteId, int? decision_Id, bool deleted, string noteDesc, int? visibilty, int? userTypeId, string updatedBy, DateTime updatedDate)
        {
            var riskNote = _db.RiskNotes.Where(x => x.Id == riskNoteId).FirstOrDefault();
            riskNote.Visibility = visibilty;
            riskNote.Note = noteDesc;
            riskNote.Deleted = deleted;
            riskNote.DecisionId = decision_Id;
            riskNote.UserType = userTypeId;
            riskNote.UpdatedBy = updatedBy;
            riskNote.UpdatedDate = updatedDate;
            _db.SaveChanges();

        }

        public List<ERiskNote> GetAllRiskNotes(int baseRiskClaimId, int userId)
        {

            var oldClaimVersions = from x in _db.RiskClaims
                                   where x.Id == baseRiskClaimId
                                   select x.BaseRiskClaim_Id;

            var publicNotes = from pn in _db.RiskNotes
                              where oldClaimVersions.Contains(pn.BaseRiskClaim_Id)
                              && pn.Visibility == 1 //Public
                              select pn;

            var privateNotes = from prn in _db.RiskNotes
                               join ru in _db.RiskUsers on userId equals ru.Id
                               where oldClaimVersions.Contains(prn.BaseRiskClaim_Id)
                               && prn.Visibility == 2 //Restricted
                               && prn.UserType == ru.UserType
                               select prn;

            var fullList = publicNotes.Union(privateNotes);

            return (from x in fullList
                    where oldClaimVersions.Contains(x.BaseRiskClaim_Id)
                    && x.Deleted == false
                    select new ERiskNote
                    {
                        BaseRiskClaim_Id = x.BaseRiskClaim_Id,
                        CreatedBy = x.CreatedBy,
                        CreatedDate = x.CreatedDate,
                        DecisionId = x.DecisionId,
                        Deleted = x.Deleted,
                        Id = x.Id,
                        NoteDesc = x.Note,
                        RiskClaim_Id = x.RiskClaim_Id,
                        Visibilty = x.Visibility,
                        UpdatedBy = x.UpdatedBy,
                        UpdatedDate = x.UpdatedDate,
                        Decision = x.RiskNoteDecision.Decision,
                        OriginalFile = x.RiskNoteFiles.Where(rn => rn.RiskNote_ID == x.Id).Select(rn => rn.FileData).FirstOrDefault(),
                        FileName = x.RiskNoteFiles.Where(rn => rn.RiskNote_ID == x.Id).Select(rn => rn.Description).FirstOrDefault(),
                        FileType = x.RiskNoteFiles.Where(rn => rn.RiskNote_ID == x.Id).Select(rn => rn.FileType).FirstOrDefault(),
                    }).ToList();
        }

        public ERiskNote GetRiskNote(int riskNoteId)
        {
            return (from x in _db.RiskNotes
                    where x.Id == riskNoteId
                    select new ERiskNote
                    {
                        BaseRiskClaim_Id = x.BaseRiskClaim_Id,
                        CreatedBy = x.CreatedBy,
                        CreatedDate = x.CreatedDate,
                        DecisionId = x.DecisionId,
                        Deleted = x.Deleted,
                        Id = x.Id,
                        NoteDesc = x.Note,
                        RiskClaim_Id = x.RiskClaim_Id,
                        Visibilty = x.Visibility,
                        Decision = x.RiskNoteDecision.Decision,
                        OriginalFile = x.RiskNoteFiles.Where(rn => rn.RiskNote_ID == x.Id).Select(rn => rn.FileData).FirstOrDefault(),
                        FileName = x.RiskNoteFiles.Where(rn => rn.RiskNote_ID == x.Id).Select(rn => rn.Description).FirstOrDefault(),
                        FileType = x.RiskNoteFiles.Where(rn => rn.RiskNote_ID == x.Id).Select(rn => rn.FileType).FirstOrDefault(),
                    }).FirstOrDefault();
        }

        public void SaveFileIntoRiskNoteFilesRecord(string fileName, string fileType, int riskNoteId, byte[] originalFile)
        {
            var note = GetRiskNote(riskNoteId);

            if (note != null)
            {
                var oFile = _db.RiskNoteFiles.Create();

                oFile.Id = Guid.NewGuid();
                oFile.Description = Path.GetFileName(fileName);
                oFile.FileData = originalFile;
                oFile.FileType = fileType;
                oFile.RiskNote_ID = riskNoteId;

                _db.RiskNoteFiles.Add(oFile);
                _db.SaveChanges();
            }

        }
    }
}
