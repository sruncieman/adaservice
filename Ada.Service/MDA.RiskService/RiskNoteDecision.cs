﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Data.Entity.Core;
using System.Xml;
using MDA.Pipeline.Model;
using MDA.RiskService.Model;
using MDA.DAL;
using Newtonsoft.Json;
using MDA.Common;

using System.Xml.Serialization;
using LinqKit;
using RiskEngine.Model;
using RiskEngine.Scoring.Entities;
using RiskEngine.Scoring.Model;
using MDA.Common.Server;

namespace MDA.RiskService
{
    public partial class RiskServices
    {

        public List<RiskNoteDecision> GetAllRiskNoteDecisions(int? riskClientId)
        {
            if (riskClientId == null)
            {
                return (from x in _db.RiskNoteDecisions select x).ToList();
            }

            return (from x in _db.RiskNoteDecisions
                    where x.RiskClient_Id == 0
                    && x.ADARecordStatus == 0
                    select x)
                .Union(
                    from x in _db.RiskNoteDecisions
                    where x.RiskClient_Id == riskClientId
                    && x.ADARecordStatus == 0
                    select x).ToList();
        }


        public void CreateRiskNoteDecision(string decision, byte isActive, int riskClient_Id)
        {
            var c = _db.RiskNoteDecisions.Create();

            c.ADARecordStatus = isActive;
            c.RiskClient_Id = riskClient_Id;
            c.Decision = decision;

            _db.RiskNoteDecisions.Add(c);

            _db.SaveChanges();
        }

        public void DeleteRiskNoteDecision(int id)
        {
            var riskNoteDecision = _db.RiskNoteDecisions.Where(x => x.Id == id).FirstOrDefault();

            riskNoteDecision.ADARecordStatus = 3;

            _db.SaveChanges();
        }
    }
}
