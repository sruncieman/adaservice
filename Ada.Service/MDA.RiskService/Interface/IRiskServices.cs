﻿using System;
using System.Collections.Generic;
using MDA.DAL;
//using RiskEngine.Scoring.Entities;
using MDA.RiskService.Model;
using MDA.Common;
using System.IO;
using RiskEngine.Scoring.Model;
using MDA.Common.Server;
using MDA.ExternalServices.Model;
using MDA.Common.Enum;

namespace MDA.RiskService.Interface
{
    public interface IRiskServices
    {
        CurrentContext CurrentContext { set; }

        /// <summary>
        /// Call IBASE Sync
        /// </summary>
        /// <returns>0 - Complete, -1 - Busy, try again later, -2 - Sync Error needs manual intervention, -3 - Migration Error needs manual intervention</returns>
        int SynchroniseDb();

        List<int> GetBatchesForClientToScore(int? riskBatchId, bool scoreDirect);
        RiskClaimsToProcess GetClaimsToScoreFromBatch(int? batchId, bool scoreDirect);
        bool IsABatchToLoadForClient(int riskClientId, bool submitDirect);
        void SetAllClaimStatusInAllClientBatches(int riskClientId, RiskClaimStatus statusToFind, RiskClaimStatus statusToSet);

        //MDA.Pipeline.Model.PipelineMotorClaim RetrieveTheClaim(RiskClaim riskClaim);

        MDA.Pipeline.Model.IPipelineClaim RetrieveTheIClaim(RiskClaim riskClaim);



        //string SerializeClaimBatch(MDA.Pipeline.Model.PipelineClaimBatch batch);

        int CreateRiskClaimRun(int riskClaimId, ClaimScoreResults resultsEntity, string clientBatchRef, string who, long scoringTimeInMs);
        RiskClaim CreateRiskClaimUpdateRecord(MDA.Pipeline.Model.IPipelineClaim claimXml, MDA.Common.Enum.ClaimStatus claimStatus, int parentRiskClaimId, int riskBatchId, int existingIncidentId, string clientClaimRefNumber, string sourceRef, int servicesMask, ProcessingResults validationResults, string who);
        RiskClaim CreateRiskClaimRecord(MDA.Pipeline.Model.IPipelineClaim claimXml, MDA.Common.Enum.ClaimStatus claimStatus, int riskBatchId, string clientClaimRefNumber, string sourceRef, int servicesMask, ProcessingResults validationResults, string who);

        int CountRiskClaimStatusForBatch(int riskBatchId, RiskClaimStatus claimStatusToCount);

        void ResetExternalServiceCallStatus();

        RiskExternalServicesRequest InsertRiskExternalServicesRequest(int riskClaimId, int RiskExternalServiceId, int UniqueId, string who);

      
        void RemoveExistingServicesRequest(int riskClaimId, int RiskExternalServiceId, int UniqueId);
        void SetClaimStatusToThirdPartyCompleteIfNoThirdParty(int riskBatchId, RiskClaimStatus statusToFind);
        void SetClaimStatusIfClosed(int riskBatchId, RiskClaimStatus statusToFind, RiskClaimStatus statusToSet);
        void SetAllClaimStatusInBatch(int riskBatchId, RiskClaimStatus statusToFind, RiskClaimStatus statusToSet);
        void SetAllRiskClaimStatusAsMemberOfBadBatch(int riskBatchId);

        List<int> FindBatchesStuckOnStatus(RiskClaimStatus statusToFind);

        RiskBatch GetRiskClaimParentBatch(int riskClaimId);
        void SetRiskBatchDirectFlags(int riskBatchId, bool submitDirect, bool scoreDirect);

        int CreateRiskBatchRecord(RiskBatchStatus batchStatus, string batchEntityType, int riskClientId, int riskUserId, string batchRef, string clientBatchReference, bool submitDirest, bool scoreDirect, string who);
        int SaveFileIntoRiskBatchRecord(string fileName, int riskBatchId, byte[] originalFile);

        byte[] GetOriginalFile(int riskBatchId);

        int FindLastBatchPriority();

        void SetRiskClaimIncidentId(int riskClaimId, int incidentId);

        bool AnyExternalServicesUntried(int riskClaimId);

        /// <summary>
        /// Check if an external service (specified by the service Id) is available to be called. Checks the CurrentStatus of the service and also whether the
        /// ServiceEnabled flag is true. If the service has been auto disabled and an elapsed period has expired it will reset to "enabled" so that the caller
        /// can attempt to call the service again.  This is to cover a service going offline and getting disabled, coming back online.
        /// </summary>
        /// <param name="riskExternalServiceId">ID of the service to check</param>
        /// <returns>Whether it can be called</returns>
        bool IsRiskExternalServiceAvailable(int riskExternalServiceId);

        RiskExternalService GetRiskExternalService(int riskExternalServiceId);
        void SetRiskExternalServiceStatus(int riskExternalServiceId, RiskExternalServiceCurrentStatus status, string message);
        void SetRiskExternalServiceEnabled(int riskExternalServiceId, bool enabled, string message);

        void SetRiskExternalRequestCallStatusForAllClaimsInBatch(int riskBatchId, int riskExternalServiceId, RiskExternalServiceCallStatus oldStatus, RiskExternalServiceCallStatus newStatus);
 
        void SetRiskExternalRequestCallStatus(int riskClaimId, int riskExternalServiceId, int uniqueId, RiskExternalServiceCallStatus callStatus);
        void ResetRetriesForServicesBeingUnavailable(int? riskBatchId, int riskExternalServiceId);

      
        List<RiskClaim> GetClaimsInBatch(int riskBatchId);

        List<RiskClaimRunDetails> GetRiskClaimRunDetailsInBatch(int riskBatchId);
        List<RiskClaimRunDetails> GetRiskClaimRunDetailsForClient(int filterClientId, int teamId, int filterUserId, string FilterUploadedBy, string[] FilterBatchRef, string FilterClaimNumber, DateTime? FilterStartDate,
                                DateTime? FilterEndDate, bool FilterHighRisk, bool FilterMediumRisk, bool FilterLowRisk, bool FilterKeoghsCFS, bool FilterPreviousVersion, bool FilterError, bool FilterStatusUnrequestedReports,
                                bool FilterStatusAvailableReports, bool FilterStatusPendingReports, bool FilterStatusRead, bool FilterStatusUnread, bool FilterStatusUnreadScoreChanges, bool FilterStatusReserveChange, bool FilterNoReportsRequested, bool FilterLevel1ReportsRequested,
                                bool FilterLevel2ReportsRequested, bool FilterCallbackReportsRequested, bool FilterNotes, bool FilterIntel, int?[] FilterDecisionId, bool FilterDecisions, string SortColumn, bool SortAscending, int page, int pageSize, out int rowCount);


        List<ERiskClaimEdit> GetClaimEditDetailsForUser(int userId);

        ERiskClaimEdit GetSingleClaimEditDetailsForUser(int userId, int editClaimId);

        List<ERiskSingleClaim> GetSingleClaimsForClient(int clientId, int teamId, int UserId, string SortColumn, bool SortAscending, int Page, int PageSize, out int totalRows);
        int GetTotalSingleClaimsForClient(int clientId, out int totalRows);
        //void SaveSingleClaimForUser(Common.FileModel.Claim claim, int filterUserId, int? editClaimId);
        int SaveSingleClaim(int? editClaimId, MDA.Common.FileModel.IClaim claim, string claimType, int clientId, int userId, DateTime incidentDate, string modifiedBy, DateTime modifiedDate, string riskClaimNumber, string status, out int editClaimIdRes);
        

        MessageNode GetScoresAsNodeTree(RiskClaimRun riskClaimRun);

        RiskClaim GetRiskClaim(int RiskClaimId);
        RiskBatch GetRiskBatch(int RiskBatchId);
        RiskBatch GetRiskBatch(int clientId, string ClientBatchReference);

        bool IsAClaimToScore(bool scoreDirect);
        //RiskClaimToProcess GetAClaimToScore(int riskClaimId);
        //RiskClaimToProcess GetAClaimToScore();
        RiskClaimToProcess GetAClaimToScoreFromBatch(int? batchId, bool scoreDirect);

        bool IsAClaimToLoad(bool submitDirect);
        RiskClaimToProcess GetAClaimToLoadFromBatch(int batchId);
        //RiskClaimToProcess GetAClaimToProcessForClient(int filterClientId);
        //RiskClaimToProcess GetAClaimToLoad(int riskClaimId);
        //RiskClaimToProcess GetAClaimToProcess();

        RiskBatch GetABatchWithClaimsToLoad(int? batchId, bool submitDirect);
        List<RiskBatch> GetListOfBatchesWithClaimsBeingLoaded();

        bool IsAClaimToProcessThirdParties(bool submitDirect);
        //RiskClaimToProcess GetAClaimToProcessThirdPartiesForClient(int filterClientId);
        //RiskClaimToProcess GetAClaimToProcessThirdParties(int riskClaimId);
        //RiskClaimToProcess GetAClaimToProcessThirdParties();
        RiskClaimToProcess GetAClaimToProcessThirdPartiesFromBatch(int? batchId, bool submitDirect);

        bool IsABatchToProcess(bool submitDirect);

        bool IsNextBatchToProcessForInternalClient();

        //RiskBatch GetABatchToProcessForClient(int riskClientId);
        RiskBatch GetABatchToLoad(int? riskBatchId, bool submitDirect);

        /// <summary>
        /// Find all Batches where Status is CreatingClaims.  For each one found delete all the RiskClaims created so far and set Batch back to QueuedForCreating
        /// </summary>
        void CleanUpPartiallyCreatedBatchClaims();

        //void CreateBatchReportMessages(int riskBatchId);

        //void CreateTracesmartServiceCallHistory(MDA.TracesmartService.Model.ResultInfo results, int riskClaimId,
        //                                                                    int riskClientId, int person2AddressId, string who);

        void CreateServiceRequestHistory(int riskClaimId, int riskExternalServiceId, int uniqueId, RiskExternalServiceCallStatus status, int servicesCalledMask = 0, string errorMsg = null, object requestData = null, object resultData = null, bool useJson = true);
        void CreateExternalRequestRecords(int riskBatchId, string who);
        List<RiskExternalServicesRequest> GetExternalRequestRecords(int riskClaimId, RiskExternalServiceCallStatus status);

        void SetRiskClaimStatus(int riskClaimId, RiskClaimStatus status);

        void SetRiskClaimStatusToBeingScored(RiskClaim riskClaim);

        void SetRiskBatchStatus(int riskBatchId, RiskBatchStatus status);
        void SaveRiskBatchCounters(int riskBatchId, long loadingTimeInMs, int countTotal, int countInsert, int countUpdate, int countErrors, int countSkipped);

        void SaveDeduplicationTiming(long dedupeTimeInMs, SyncOrDedupeType syncOrDedupeType);

        bool CheckToRunDeduplication(); 

        void SaveBackupTiming(long backupTiming, BackupType backupType);

        void SaveSyncronisationTiming(long dedupeTimeInMs, SyncOrDedupeType syncOrDedupeType);

        void SaveRiskClaimLoadingTime(int riskClaimId, long loadingTimeInMs);

        List<RiskClaimToProcess> GetClaimsToProcess();
        List<RiskBatch> GetBatchesForClient(int riskClientId);
        List<RiskBatchDetails> GetBatchesForClient(int clientId, int teamId, int userId, string sortColumn, bool sortAscending, int page, int pageSize, out int rowCount);

        RiskClaim FindLatestClaim(int clientId, string refNumber);
        //RiskClaim FindLatestClaim(int filterClientId, string refNumber);
        bool AlreadyHaveNewerVersionOfSameClaim(int clientId, string refNumber, DateTime IncidentDate);

        RiskClaim FindExistingBaseClaim(int clientId, string refNumber);
        bool FindExistingIdenticalClaim(int clientId, MDA.Pipeline.Model.IPipelineClaim pipelineClaim);
        //RiskClaim FindExistingBaseClaim(int filterClientId, string refNumber);
        RiskClient GetRiskClient(int clientId);
        List<RiskClient> GetAssignedRiskClientsForUser(int userId);
        //RiskClient GetClientRecord(int filterClientId);
        List<RiskClientServicesResult> GetClientExternalServices(int clientId);
        int GetServiceId(string serviceName);
        RiskClaimRun GetLatestRiskClaimRun(int riskClaimId);
        //void PrepareLevelOneData(int riskClaimRunId);

        //void SaveRiskResults(int riskClaimRunId, int ruleGroupId, Dictionary<string, Dictionary<string, RiskEngine.Model.RiskResult>> results, int score);
        //CalculateRiskResponse ScoreEntity(IRiskEntity entity, RuleGroupData ruleGroupData);
        //ClaimScoreResults ScoreClaim(int riskClaimId, int orgId, bool scoreWithLiveRules, string who);
        //ClaimScoreResults ScoreClaim(FullClaimToScore claim, int filterClientId, bool scoreWithLiveRules, string who);

        void SaveCleansingResults(int riskClaimId, ProcessingResults results);
        void SaveVerificationResults(int riskBatchId, ProcessingResults results);
        void SaveValidationResults(int riskClaimId, ProcessingResults results);
        void SaveMappingResults(int riskBatchId, ProcessingResults results);

        List<RiskClaim> GetListAllClaims();

        List<RiskClient> GetAllRiskClients();

        List<ERiskUser> GetAllRiskUsers();

        List<RiskWordFilter> GetRiskWordsForClient(int clientId);

        List<RiskWordFilter> GetAllRiskWords();

        List<ERiskNote> GetAllRiskNotes(int baseRiskClaimId, int userId);

        ERiskNote GetRiskNote(int riskNoteId);

        List<RiskNoteDecision> GetAllRiskNoteDecisions(int? riskClientId);

        void DeleteRiskWord(int id);

        void CreateRiskNote(int ClientId, string CreatedBy, DateTime CreatedDate, int? Decision_Id, bool Deleted, string NoteDesc, int[] RiskClaim_Id, int UserId, int? Visibilty, int? UserTypeId, byte[] originalFile, string fileName, string fileType);

        void UpdateRiskNote(int riskNoteId, int? decision_Id, bool deleted, string noteDesc, int? visibilty, int? UserTypeId, string UpdatedBy, DateTime UpdatedDate);

        List<RiskConfigurationValue> GetRiskDefaultData();

        //List<EntityScoreMessageList> GetClaimSummaryMessages(int riskClaimId);
        //List<EntityScoreMessageList> GetClaimSummaryMessages(RiskClaimRun rcr);

        RiskClient FindRiskClientByName(string clientName);

        bool IsPasswordUnique(string userName, string password);

        RiskUser FindRiskUserByName(string clientName);

        RiskClient CreatNewRiskClient(string clientName);

        int RequestLevelOneReport(int RiskClaimId, int userId);

        int RequestLevelTwoReport(int RiskClaimId, int userId);

        int RequestCallback(int RiskClaimId, int userId);

        int ClearCallbackRequest(int RiskClaimId, int userId);

        int SetClaimReadStatus(int RiskClaimId, int RiskClaimReadStatus, int userId, out int CurrentClaimReadStatus);

        void SetClaimReadStatus(int RiskClaimId, int RiskClaimReadStatus);

        RiskUserDetails GetUserDetails(int userId, int? clientId);
        
        int GetPasswordFailuresSinceLastSuccess(string userName);
        bool ValidateUser(string username, string password, string ipAddress);
        bool IsUserInRole(string userName, string roleName);
        int GetSingleClaimRiskClaimId(int riskBatchId);
        string GetUserName(int userId);
        string GetFullName(int userId);
        string[] GetRolesForUser(string userName);
        void CreateUser(string userName, string password, string email,  string firstName, string lastName, string telephoneNumber, int clientId, int roleId, int teamId, int? status, bool? autoloader, int? userTypeId);
        void UpdateUser(string userName, string password, string email, string firstName, string lastName, string telephoneNumber, int clientId, int roleId, int teamId, int? status, bool? autoloader, int? userTypeId);
        void CreateClient(string clientName, int? statusId, string fileExtensions, int amberThreshold, int redThreshold, int insurersClienId);
        void UpdateClient(int iD, string clientName, int? statusId, string fileExtensions, int amberThreshold, int redThreshold, int insurersClienId, bool? recordReserveChanges, int? batchPriority);
        void CreateRiskUser2Client(int clientId, int userId);
        void DeleteRiskUser2Client(int clientId, int userId);
        bool ChangePassword(string username, string newHashPassword);
        int GetUserId(string UserName);

        void CreateRiskWord(int riskClientId, string tableName, string fieldName, string lookupWord, string replacementWord, bool replaceWholeString, DateTime dateAdded, string searchType);

        void CreateRiskDefaultData(string configurationValue, bool isActive, int riskClient_Id, int riskConfigurationDescriptionId);

        void CreateRiskNoteDecision(string decision, byte isActive, int riskClient_Id);

        void DeleteRiskDefaultData(int id);

        void DeleteRiskNoteDecision(int id);

        ERiskSingleClaimDetail FetchSingleClaimDetail(int p1, int p2, int p3);

        void DeleteSingleClaim(int clientId, int userId, int deleteId);

        List<string> GetFilteredClientBatchRefs(string role, int clientId, int userId, int? selectedClientId);

        //void SavePipelineClaim(int filterClientId, MDA.Pipeline.Model.PipelineClaim claim);
        //MDA.Pipeline.Model.PipelineClaimBatch GetBatchFromStaging(int filterClientId, int count);
        //void DeleteClaimFromStaging(int filterClientId);
     
        ECueSearchEnquiryModel FetchCueInvolvements(int RiskClaim_Id, int ClientId);

        void BackupTheDatabase(string SaveSetName, int backupType, string path, string folder);

        //byte[] GetRiskReport(int riskReportId);
        byte[] GetBatchReport(int riskBatchId);
        byte[] GetLevel1Report(int riskClaimId);
        byte[] GetLevel2Report(int riskClaimId);

        int SaveBatchReportIntoRiskBatchRecord(int riskBatchId, byte[] originalFile);
        int SaveLevel1ReportIntoRiskClaimRecord(int riskClaimId, byte[] originalFile);
        int SaveLevel2ReportIntoRiskClaimRecord(int riskClaimId, byte[] originalFile);

        List<RiskBatch> GetRiskBatchesNeedingReport();

        bool CheckUserBelongsToClient(int clientId, int userId);
        bool CheckUserHasAccessToReport(int riskBatchId, int loggedInUser, int clientId);


        string CheckScoring(int riskBatchId);

        RunningMetrics GetRunningMetrics();

        byte[] GetRiskReportFromRiskExternalServiceRecord(int riskClaimId, int riskExternalServiceId);
        byte[] GetRiskReportFromRiskExternalServiceRecord(int riskClaimId, int riskExternalServiceId, int unique_Id);

        List<RiskExternalServicesRequest> GetExternalRequestRecords(int riskClaimId);
        List<RiskExternalServicesRequest> GetExternalRequestRecords(int riskClaimId, int riskExternalServiceId);
        RiskExternalServicesRequest GetExternalRequestRecord(int riskClaimId, int riskExternalServiceId, int uniqueId);
        List<string> GetExternalRequestRecordsAsListString(int riskClaimId, int riskExternalServiceId);
        int SaveCueReportIntoRiskExternalTable(int riskClaimId, int riskExternalRequestId, int uniqueId, byte[] reportBytes);
        int SaveCueReportIntoRiskExternalTable(int riskClaimId, int riskExternalRequestId, byte[] reportBytes, MDA.CueService.Model.Results lstResults);

        string GetRiskReportStructure(int riskClaimId, int riskExternalServiceId);

        List<ERiskUserLocked> GetLockedUsers(int clientId, string userName, int userId);

        List<ERiskClientBatchPriority> GetRiskClientBatchPriority(int clientId, string userName, int userId);

       
        int UnlockRiskUser(int clientId, string userName, int userId, int unlockUserId);

        int GeneratePasswordResetToken(string userName, string token, DateTime tokenExpiry);

        string ValidatePasswordResetToken(string token);

        List<RiskRole> GetRoles();

        List<InsurersClient> GetAllInsurersClients();

        void SaveTemplateFunctions(string userName, int roleId, int templateId, string templateFunctionsJson, List<Dictionary<string, string>> ListTemplateFunctionsJson);
        
        string LoadTemplateFunctions(string userName, int roleId, int clientId, int userId);

        void CreateRiskRole(int clientId, int userId, string who, string templateName);

        void EditRiskRole(int clientId, int userId, string who, string templateName, int profileId);

        int DeleteRiskRole(int clientId, int userId, string who,  int profileId);

        int FindUserName(string userName);

        int SaveBatchPriorityClients(int ClientId, int UserId, string Who, int[] ClientBatchPriorityArray);

        int SaveOverriddenBatchPriority(int ClientId, int UserId, string Who, int[] OverriddenBatchPriorityArray);

        List<EBatchQueue> GetRiskBatchQueue();

        List<ESearchDetails> GetWebSearchResults(int clientId, int userId, string who, string filterFirstName, string filterSurname, DateTime? filterDob, string filterAddressLine, string filterAddressPostcode, string filterVehicleRegNum, int page, int pageSize, out int totalRows);
    }
}
