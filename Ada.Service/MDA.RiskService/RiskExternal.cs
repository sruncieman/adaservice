﻿
using System;
using System.Collections.Generic;
using System.Linq;
using MDA.RiskService.Model;
using MDA.DAL;
using Newtonsoft.Json;
using MDA.ExternalServices.Model;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;



namespace MDA.RiskService
{
    public partial class RiskServices
    {
        /// <summary>
        /// Get all the Risk External Service details for a given client.  Returns a structure containing a list of the ServiceUsage, ServiceName and MaskValue
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<RiskClientServicesResult> GetClientExternalServices(int clientId)
        {
            return (from x in _db.RiskClient2RiskExternalServices
                    where x.RiskClient_Id == clientId
                    select new RiskClientServicesResult
                    {
                        ServiceUsage = x.ServiceUsage,
                        ServiceName = x.RiskExternalService.ServiceName,
                        MaskValue = x.RiskExternalService.MaskValue
                    }).ToList();
        }

        /// <summary>
        /// Get Risk Service Id from service name identified from config. Returns an int of Service Id
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        public int GetServiceId(string serviceName)
        {
            return _db.RiskExternalServices.Where(x => x.ServiceName == serviceName).Select(x => x.Id).FirstOrDefault();
        }
           

        /// <summary>
        /// This resets ALL external service requests to 0 which are currently set at RETRY (2 or 3).  It does this for EVERY external service that is enabled and whose current
        /// status is also enabled.
        /// </summary>
        public void ResetExternalServiceCallStatus()
        {
            if (ReEnableRiskExternalServices())
            {
                var reList = (from x in _db.RiskExternalServices
                                where x.ServiceEnabled == true
                                    && x.CurrentStatus == (int)RiskExternalServiceCurrentStatus.Enabled
                                select x).ToList();

                foreach (var res in reList)
                {
                    _db.Database.ExecuteSqlCommand("update RiskExternalServiceRequests set CallStatus = 0 where RiskExternalService_Id = " + res.Id.ToString() + 
                        " and (CallStatus = 2 or CallStatus = 3)");
                }
            }
        }

        public RiskExternalServicesRequest InsertRiskExternalServicesRequest(int riskClaimId, int RiskExternalServiceId, int UniqueId, string who)
        {
            var esr = _db.RiskExternalServicesRequests.Create();

            esr.RiskClaim_Id = riskClaimId;
            esr.RiskExternalService_Id = RiskExternalServiceId;
            esr.Unique_Id = UniqueId;
            esr.ServicesCalledMask = 0;
            esr.RequestedBy = who;
            esr.RequestedWhen = DateTime.Now;
            esr.IsRepeatCall = false;
            esr.ErrorMessage = null;
            esr.CallStatus = 0;

            _db.RiskExternalServicesRequests.Add(esr);

            _db.SaveChanges();

            return esr;
        }

        /// <summary>
        /// Remove existing CUE request for the same risk claim and person id's. Duplicates will break ref. integrity of composite primary key. 
        /// </summary>
        /// <param name="riskClaimId"></param>
        /// <param name="RiskExternalServiceId"></param>
        /// <param name="UniqueId"></param>
        public void RemoveExistingServicesRequest(int riskClaimId, int RiskExternalServiceId, int UniqueId)
        {

            var existingRequests = _db.RiskExternalServicesRequests.Where(x => x.RiskClaim_Id == riskClaimId && x.RiskExternalService_Id == RiskExternalServiceId && x.Unique_Id == UniqueId).ToList();

            if (existingRequests != null)
            {
                foreach (var item in existingRequests)
                {
                    _db.RiskExternalServicesRequests.Remove(item);
                }
            }
           _db.SaveChanges();
        }


        /// <summary>
        /// Create ALL the external service request records for a batch.  This cycles through all the RiskClaim records in the batch examining the MASK of external 
        /// services required. For each external service a claim requires a record is created. Implemented as an SP as it can be A LOT!
        /// </summary>
        /// <param name="riskBatchId"></param>
        /// <param name="who"></param>
        public void CreateExternalRequestRecords(int riskBatchId, string who)
        {
            _db.uspCreateExternalRequests(riskBatchId, who, DateTime.Now);
        }

        /// <summary>
        /// Reset External Request Record Call Status where the value is currently set to a RETRY value.  Called when a service comes back online.  You can limit the 
        /// effect by passing a Batch ID. If Batch ID is null EVERY record in EVERY batch to modified.
        /// </summary>
        /// <param name="riskBatchId">Risk Batch ID</param>
        /// <param name="riskExternalServiceId">Risk External Service ID</param>
        public void ResetRetriesForServicesBeingUnavailable(int? riskBatchId, int riskExternalServiceId)
        {
            if (riskBatchId == null)
                _db.Database.ExecuteSqlCommand("update RiskExternalServicesRequests set CallStatus = " + ((int)RiskExternalServiceCallStatus.Created).ToString() + 
                    " where (RiskExternalService_Id = " + riskExternalServiceId.ToString() + " and CallStatus = " + ((int)RiskExternalServiceCallStatus.FailedNeedsRetry).ToString() + 
                    " or CallStatus = " + ((int)RiskExternalServiceCallStatus.UnavailableNeedsRetry).ToString() + ")");
            else
                _db.Database.ExecuteSqlCommand("update RiskExternalServicesRequests set CallStatus = " + ((int)RiskExternalServiceCallStatus.Created).ToString() + 
                    " where (RiskExternalService_Id = " + riskExternalServiceId.ToString() + " and CallStatus = " + ((int)RiskExternalServiceCallStatus.FailedNeedsRetry).ToString() + 
                    " or CallStatus = " + ((int)RiskExternalServiceCallStatus.UnavailableNeedsRetry).ToString() + ") and (RiskClaim_id in (select id from RiskClaim where RiskBatch_Id = " +
                    riskBatchId.ToString());

            _db.SaveChanges();
        }

        /// <summary>
        /// Set the Call Status for the given External Request Record
        /// </summary>
        /// <param name="riskClaimId">Risk Claim ID</param>
        /// <param name="riskExternalServiceId">Risk External Service ID</param>
        /// <param name="uniqueId">Unique ID for this service/record combination</param>
        /// <param name="callStatus">The new status to set</param>
        public void SetRiskExternalRequestCallStatus(int riskClaimId, int riskExternalServiceId, int uniqueId, RiskExternalServiceCallStatus callStatus)
        {
            var resr = (from x in _db.RiskExternalServicesRequests
                        where x.RiskClaim_Id == riskClaimId
                          && x.RiskExternalService_Id == riskExternalServiceId
                          && x.Unique_Id == uniqueId
                        select x).FirstOrDefault();

            if (resr != null)
            {
                resr.CallStatus = (int)callStatus;

                _db.SaveChanges();
            }
        }

        /// <summary>
        /// Set the External Service Request Call Status for a given External Service for all claims in a batch.  You pass in the Call Status to look for and the method changes
        /// these records to the new Call Status.
        /// </summary>
        /// <param name="riskBatchId">The Batch ID</param>
        /// <param name="riskExternalServiceId">The External Service ID</param>
        /// <param name="oldStatus">The Call Status to look for</param>
        /// <param name="newStatus">The Call Status to set too</param>
        public void SetRiskExternalRequestCallStatusForAllClaimsInBatch(int riskBatchId, int riskExternalServiceId, RiskExternalServiceCallStatus oldStatus, RiskExternalServiceCallStatus newStatus)
        {
            _db.Database.ExecuteSqlCommand("update RiskExternalServicesRequests set CallStatus = " + ((int)newStatus).ToString() + 
                " where (RiskExternalService_Id = " + riskExternalServiceId.ToString() + " and CallStatus = " + 
                ((int)oldStatus).ToString() + ") and (RiskClaim_id in (select id from RiskClaim where RiskBatch_Id = " + riskBatchId.ToString() + "))");

            _db.SaveChanges();
        }

        /// <summary>
        /// Get all the external request record for the given RiskClaim ID who have never been called (status = 0)
        /// </summary>
        /// <param name="riskClaimId">Risk Claim ID</param>
        /// <param name="status">Status to look for</param>
        /// <returns></returns>
        public List<RiskExternalServicesRequest> GetExternalRequestRecords(int riskClaimId, RiskExternalServiceCallStatus status)
        {
            return (from x in _db.RiskExternalServicesRequests
                    where x.RiskClaim_Id == riskClaimId && x.CallStatus == (int)status
                    select x).ToList();
        }

        public RiskExternalServicesRequest GetExternalRequestRecord(int riskClaimId, int riskExternalServiceId, int uniqueId)
        {
            return (from x in _db.RiskExternalServicesRequests
                    where x.RiskClaim_Id == riskClaimId && x.RiskExternalService_Id == riskExternalServiceId && x.Unique_Id == uniqueId
                    select x).FirstOrDefault();
        }

        public List<RiskExternalServicesRequest> GetExternalRequestRecords(int riskClaimId, int riskExternalServiceId)
        {
            return (from x in _db.RiskExternalServicesRequests
                    where x.RiskClaim_Id == riskClaimId && x.RiskExternalService_Id == riskExternalServiceId
                    select x).ToList();
        }

        public List<string> GetExternalRequestRecordsAsListString(int riskClaimId, int riskExternalServiceId)
        {
            return (from x in _db.RiskExternalServicesRequests
                    where x.RiskClaim_Id == riskClaimId && x.RiskExternalService_Id == riskExternalServiceId
                    select x.ReturnedData).ToList();
        }

        public List<RiskExternalServicesRequest> GetExternalRequestRecords(int riskClaimId)
        {
            return (from x in _db.RiskExternalServicesRequests
                    where x.RiskClaim_Id == riskClaimId
                    select x).ToList();
        }

        public byte[] GetRiskReportFromRiskExternalServiceRecord(int riskClaimId, int riskExternalServiceId, int unique_Id)
        {
            var rb = GetExternalRequestRecord(riskClaimId, riskExternalServiceId, unique_Id);

            if (rb != null && rb.RiskReport_Id != null)
            {
                return GetRiskReport((int)rb.RiskReport_Id);
            }

            return null;
        }

        public byte[] GetRiskReportFromRiskExternalServiceRecord(int riskClaimId, int riskExternalServiceId)
        {
            var rb = GetExternalRequestRecords(riskClaimId, riskExternalServiceId);

            if (rb != null && rb.Count > 0 && rb[0].RiskReport_Id != null)
            {
                return GetRiskReport((int)rb[0].RiskReport_Id);
            }

            return null;
        }

        public string GetRiskReportStructure(int riskClaimId, int riskExternalServiceId)
        {
            var reportId = _db.RiskExternalServicesRequests.Where(x => x.RiskClaim_Id == riskClaimId && x.RiskExternalService_Id == riskExternalServiceId).Select(x => x.RiskReport_Id).FirstOrDefault();

            return (_db.RiskReports.Where(x => x.PkId == reportId).Select(x => x.ReturnedData).FirstOrDefault());
        }

        public int SaveCueReportIntoRiskExternalTable(int riskClaimId, int riskExternalServiceId, int uniqueId, byte[] reportBytes)
        {
            var err = GetExternalRequestRecord(riskClaimId, riskExternalServiceId, uniqueId);

            if (err != null)
            {
                var id = SaveRiskReportIntoRiskReportTable(reportBytes, null, riskExternalServiceId, "Cue-PDF");

                err.RiskReport_Id = id;

                _db.SaveChanges();

                return id;
            }

            return -1;
        }

        public int SaveCueReportIntoRiskExternalTable(int riskClaimId, int riskExternalServiceId, byte[] reportBytes, MDA.CueService.Model.Results lstResults)
        {
            var errList = GetExternalRequestRecords(riskClaimId, riskExternalServiceId);

            if (errList != null)
            {

                #region XmlSerializing
                //var ms = new MemoryStream();
                //var ser = new DataContractSerializer(lstResults.GetType());
                //ser.WriteObject(ms, lstResults);
                //ms.Position = 0;
                //var sr = new StreamReader(ms);
                //var serializedResults = sr.ReadToEnd();
                //resr.ReturnedData = JsonConvert.SerializeObject(resultData, Newtonsoft.Json.Formatting.None);
                #endregion

                var serializedResults = JsonConvert.SerializeObject(lstResults, Newtonsoft.Json.Formatting.None);

                var id = SaveRiskReportIntoRiskReportTable(reportBytes, serializedResults, riskExternalServiceId, "Cue-PDF");

                foreach (var err in errList)
                {
                    err.RiskReport_Id = id;
                }

                _db.SaveChanges();

                return id;
            }

            return -1;
        }

        /// <summary>
        /// Check if an external service (specified by the service Id) is available to be called. Checks the CurrentStatus of the service and also whether the
        /// ServiceEnabled flag is true. If the service has been auto disabled and an elapsed period has expired it will reset to "enabled" so that the caller
        /// can attempt to call the service again.  This is to cover a service going offline and getting disabled, coming back online.
        /// </summary>
        /// <param name="riskExternalServiceId">ID of the service to check</param>
        /// <returns>Whether it can be called</returns>
        public bool IsRiskExternalServiceAvailable(int riskExternalServiceId)
        {
            var re = (from x in _db.RiskExternalServices
                      where x.Id == riskExternalServiceId
                      select x).FirstOrDefault();

            if (re == null) return false;

            if (re.CurrentStatus == (int)RiskExternalServiceCurrentStatus.AdaAutoDisabled)
            {
                if (!re.StatusLastChecked.HasValue || re.StatusLastChecked.Value.AddMinutes(re.StatusTimeout) < DateTime.Now)
                {
                    re.CurrentStatus = (int)RiskExternalServiceCurrentStatus.Enabled;
                    re.StatusLastChecked = DateTime.Now;

                    _db.SaveChanges();
                }
            }

            return re.CurrentStatus == (int)RiskExternalServiceCurrentStatus.Enabled && re.ServiceEnabled;
        }

        /// <summary>
        /// Enable any external services that have been AdaAutoDisabled provided the timeout has expired
        /// </summary>
        /// <returns>Did any status values get changed</returns>
        public bool ReEnableRiskExternalServices()
        {
            bool changedAnything = false;

            var reList = (from x in _db.RiskExternalServices where x.CurrentStatus == (int)RiskExternalServiceCurrentStatus.AdaAutoDisabled select x).ToList();

            foreach (var re in reList)
            {
                if (!re.StatusLastChecked.HasValue || re.StatusLastChecked.Value.AddMinutes(re.StatusTimeout) < DateTime.Now)
                {
                    re.CurrentStatus = (int)RiskExternalServiceCurrentStatus.Enabled;
                    re.StatusLastChecked = DateTime.Now;

                    changedAnything = true;
                }
            }

            _db.SaveChanges();

            return changedAnything;
        }

        /// <summary>
        /// Return the RiskExternalService record for the given ID
        /// </summary>
        /// <param name="riskExternalServiceId"></param>
        /// <returns></returns>
        public RiskExternalService GetRiskExternalService(int riskExternalServiceId)
        {
            return (from x in _db.RiskExternalServices
                    where x.Id == riskExternalServiceId
                    select x).FirstOrDefault();
        }

        /// <summary>
        /// Set the Risk External Service CurrentStatus (and StatusMessage) values for the given Risk External Service ID
        /// </summary>
        /// <param name="riskExternalServiceId"></param>
        /// <param name="status"></param>
        /// <param name="message"></param>
        public void SetRiskExternalServiceStatus(int riskExternalServiceId, RiskExternalServiceCurrentStatus status, string message)
        {
            var re = GetRiskExternalService(riskExternalServiceId);

            if (re != null)
            {
                re.CurrentStatus = (int)status;
                re.StatusMessage = message;
                re.StatusLastChecked = DateTime.Now;

                _db.SaveChanges();
            }
        }

        /// <summary>
        /// Set the Risk External Service ServiceEnabled value (and StatusMessage) for the given Risk External Service ID
        /// </summary>
        /// <param name="riskExternalServiceId"></param>
        /// <param name="enabled"></param>
        /// <param name="message"></param>
        public void SetRiskExternalServiceEnabled(int riskExternalServiceId, bool enabled, string message)
        {
            var re = GetRiskExternalService(riskExternalServiceId);

            if (re != null)
            {
                re.ServiceEnabled = enabled;
                re.StatusMessage = message;
                re.StatusLastChecked = DateTime.Now;

                _db.SaveChanges();
            }
        }

        /// <summary>
        /// Returns whether any of the external services for the Given Risk Claim ID have not been tried (status = 0)
        /// </summary>
        /// <param name="riskClaimId"></param>
        /// <returns></returns>
        public bool AnyExternalServicesUntried(int riskClaimId)
        {
            return _db.RiskExternalServicesRequests.Any(x => x.RiskClaim_Id == riskClaimId && x.CallStatus == 0);
        }

        /// <summary>
        /// Populate the Service Request History fields in the Risk External Service Request record.
        /// </summary>
        /// <param name="requestData">The "object" sent to the 3rd party call.  Serialised and saved in table in JSON</param>
        /// <param name="resultData">The "object" received from the 3rd party call.  Serialised and saved in table in JSON</param>
        /// <param name="riskClaimId">Risk Claim ID</param>
        /// <param name="riskExternalServiceId">Risk External Service ID</param>
        /// <param name="uniqueId">Unique ID</param>
        /// <param name="status">Call Status</param>
        /// <param name="servicesCalledMask">Services Called Mask</param>
        /// <param name="errorMsg">Any error message</param>
        public void CreateServiceRequestHistory(int riskClaimId, int riskExternalServiceId, int uniqueId, RiskExternalServiceCallStatus status, int servicesCalledMask = 0, string errorMsg = null, object requestData = null, object resultData = null, bool useJson = true)
        {
            var resr = (from x in _db.RiskExternalServicesRequests
                        where x.RiskClaim_Id == riskClaimId
                           && x.RiskExternalService_Id == riskExternalServiceId
                           && x.Unique_Id == uniqueId
                        select x).First();

            resr.CallStatus = (int)status;
            resr.ServicesCalledMask = servicesCalledMask;
            resr.ErrorMessage = errorMsg;

            if (requestData != null)
            {
                if (useJson)
                {
                    resr.RequestDataFormat = "json";
                    resr.RequestData = JsonConvert.SerializeObject(requestData, Newtonsoft.Json.Formatting.None);
                }
                else
                {
                    resr.RequestDataFormat = "xml";

                    var ms = new MemoryStream();

                    var ser = new DataContractSerializer(requestData.GetType());

                    ser.WriteObject(ms, requestData);
                    ms.Position = 0;
                    var sr = new StreamReader(ms);
                    resr.RequestData = sr.ReadToEnd();
                }
            }

            if (resultData != null)
            {
                if (useJson)
                {
                    resr.ReturnedDataFormat = "json";
                    resr.ReturnedData = JsonConvert.SerializeObject(resultData, Newtonsoft.Json.Formatting.None);
                }
                else
                {
                    resr.ReturnedDataFormat = "xml";

                    var ms = new MemoryStream();

                    var ser = new DataContractSerializer(resultData.GetType());
                    ser.WriteObject(ms, resultData);
                    ms.Position = 0;
                    var sr = new StreamReader(ms);
                    resr.ReturnedData = sr.ReadToEnd();
                }
            }
        }
    }
}
