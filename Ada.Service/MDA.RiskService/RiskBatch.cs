﻿
using System;
using System.Collections.Generic;
using System.Linq;
using MDA.RiskService.Model;
using MDA.DAL;
using MDA.Common;
using Newtonsoft.Json;
using System.Data.Entity.Core;


namespace MDA.RiskService
{
    public partial class RiskServices
    {
        public RiskBatch GetRiskBatch(int riskBatchId)
        {
            return (from x in _db.RiskBatches
                    join y in _db.RiskClients on x.RiskClient_Id equals y.Id
                    where x.Id == riskBatchId
                    orderby y.BatchPriority
                    select x).FirstOrDefault();
        }

        public RiskBatch GetRiskBatch(int clientId, string clientBatchReference)
        {
            var batch = from x in _db.RiskBatches
                        where x.ClientBatchReference == clientBatchReference
                            && x.RiskClient_Id == clientId
                        select x;

            return batch.FirstOrDefault();
        }

        public List<int> FindBatchesStuckOnStatus(RiskClaimStatus statusToFind)
        {
            // Find all the batches where their Claims.Status == statusToFind || 10 || < 0

            return (from x in _db.RiskBatches where x.RiskClaims.Count > 0 && x.RiskClaims.Count(y => y.ClaimStatus == (int)statusToFind) > 0 && x.RiskClaims.All(y => y.Id > 0 && (y.ClaimStatus == (int)statusToFind || y.ClaimStatus == (int)RiskClaimStatus.Archived || y.ClaimStatus < 0)) select x.Id).ToList();
        }

        /// <summary>
        /// Find all Batches where Status is CreatingClaims.  For each one found delete all the RiskClaims created so far and set Batch back to QueuedForCreating
        /// </summary>
        public void CleanUpPartiallyCreatedBatchClaims()
        {
            var batches = (from x in _db.RiskBatches
                           where x.BatchStatus == (int)RiskBatchStatus.CreatingClaims
                           select x).ToList();

            foreach (var b in batches)
            {
                _db.Database.ExecuteSqlCommand("delete from RiskClaim where RiskBatch_Id = " + b.Id.ToString());

                b.BatchStatus = (int)RiskBatchStatus.QueuedForCreating;

                _db.SaveChanges();
            }
        }

        public void SaveVerificationResults(int riskBatchId, ProcessingResults results)
        {
            string xml = JsonConvert.SerializeObject(results, Newtonsoft.Json.Formatting.None);

            var riskBatch = GetRiskBatch(riskBatchId);

            if (riskBatch != null)
            {
                riskBatch.VerificationResults = xml;

                if (!results.IsValid)
                    riskBatch.BatchStatus = (int)RiskBatchStatus.Bad;

                _db.SaveChanges();
            }
        }

        public void SaveMappingResults(int riskBatchId, ProcessingResults results)
        {
            string xml = JsonConvert.SerializeObject(results, Newtonsoft.Json.Formatting.None);

            var riskBatch = GetRiskBatch(riskBatchId);

            if (riskBatch != null)
            {
                riskBatch.MappingResults = xml;

                if (!results.IsValid)
                    riskBatch.BatchStatus = (int)RiskBatchStatus.Bad;

                _db.SaveChanges();
            }
        }

        public RiskBatch GetRiskClaimParentBatch(int riskClaimId)
        {
            return GetRiskClaim(riskClaimId).RiskBatch;
        }

        public void SetRiskBatchDirectFlags(int riskBatchId, bool submitDirect, bool scoreDirect)
        {
            var batch = GetRiskBatch(riskBatchId);

            if (batch != null)
            {

                batch.SubmitDirect = submitDirect;
                batch.ScoreDirect = scoreDirect;

                _db.SaveChanges();
            }
        }

        public int CreateRiskBatchRecord(RiskBatchStatus batchStatus, string batchEntityType, int riskClientId, int riskUserId, string batchRef, string clientBatchReference, 
                                                            bool submitDirect, bool scoreDirect, string who)
        {
            var batch = _db.RiskBatches.Create();

            batch.RiskClient_Id           = riskClientId;
            batch.BatchReference          = batchRef;
            batch.ClientBatchReference    = clientBatchReference;
            batch.BatchStatus             = (int)batchStatus;
            batch.BatchStatusLastModified = DateTime.Now;
            batch.CreatedBy               = who;
            batch.CreatedDate             = DateTime.Now;
            //batch.RiskOriginalFile_Id   = oFile.PkId;
            batch.BatchEntityType         = batchEntityType;
            batch.RiskUser_Id             = riskUserId;
            batch.SubmitDirect            = submitDirect;
            batch.ScoreDirect             = scoreDirect;

            _db.RiskBatches.Add(batch);

            _db.SaveChanges();

            return batch.Id;
        }

        public int SaveFileIntoRiskBatchRecord(string fileName, int riskBatchId, byte[] originalFile)
        {
            var batch = GetRiskBatch(riskBatchId);

            if (batch != null)
            {
                var oFile = _db.RiskOriginalFiles.Create();

                oFile.Id          = Guid.NewGuid();
                oFile.Description = fileName;
                oFile.FileData    = originalFile;

                _db.RiskOriginalFiles.Add(oFile);

                _db.SaveChanges();

                batch.RiskOriginalFile_Id = oFile.PkId;

                _db.SaveChanges();
            }

            return batch.Id;
        }

        public int SaveBatchReportIntoRiskBatchRecord(int riskBatchId, byte[] reportBytes)
        {
            var batch = GetRiskBatch(riskBatchId);

            if (batch != null)
            {
                var riskReport = _db.RiskReports.Create();

                riskReport.Id              = Guid.NewGuid();
                riskReport.Description     = "Batch";
                riskReport.FileData        = reportBytes;
                riskReport.ParentEntity_Id = riskBatchId;

                _db.RiskReports.Add(riskReport);

                _db.SaveChanges();

                batch.RiskBatchReport_Id = riskReport.PkId;

                _db.SaveChanges();
            }


            return batch.Id;
        }

        public List<RiskBatch> GetRiskBatchesNeedingReport()
        {
            return (from x in _db.RiskBatches
                            where x.RiskBatchReport_Id == null
                                && x.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
                                && IsBatchComplete(x.Id)
                            select x).Distinct().ToList();
        }

        public void SetRiskBatchStatus(int riskBatchId, RiskBatchStatus status)
        {
            var riskBatch = GetRiskBatch(riskBatchId); // (from x in db.RiskBatches where x.Id == riskBatchId select x).FirstOrDefault();

            if (riskBatch != null)
            {
                riskBatch.BatchStatus = (int)status;

                _db.SaveChanges();
            }
        }

        public void SaveRiskBatchCounters(int riskBatchId, long loadingTimeInMs, int countTotal,
                                                        int countInsert, int countUpdate, int countErrors, int countSkipped)
        {
            var riskBatch = GetRiskBatch(riskBatchId); 

            if (riskBatch != null)
            {
                riskBatch.TotalClaimsReceived = countTotal;
                riskBatch.TotalNewClaims      = countInsert;
                riskBatch.TotalModifiedClaims = countUpdate;
                riskBatch.TotalClaimsInError  = countErrors;
                riskBatch.TotalClaimsLoaded   = countInsert + countUpdate;
                riskBatch.TotalClaimsSkipped  = countSkipped;
                riskBatch.LoadingDurationInMs = (int)loadingTimeInMs;

                _db.SaveChanges();
            }
        }


        public byte[] GetOriginalFile(int riskBatchId)
        {
            return (from x in _db.RiskBatches
                        where x.Id == riskBatchId
                        select x.RiskOriginalFile.FileData).FirstOrDefault();
        }

        public byte[] GetBatchReport(int riskBatchId)
        {
            var rb = GetRiskBatch(riskBatchId);

            if (rb != null)
                return (from x in _db.RiskReports
                        where x.PkId == rb.RiskBatchReport_Id
                        select x.FileData).FirstOrDefault();

            return null;
        }



        public List<RiskBatch> GetBatchesForClient(int riskClientId)
        {
            return (from x in _db.RiskBatches where x.RiskClient_Id == riskClientId && x.BatchEntityType != "None" select x).ToList();
        }

        public List<int> GetBatchesForClientToScore(int? riskBatchId, bool scoreDirect)
        {
            DateTime timeout = DateTime.Now; // DateTime.Now.AddMinutes(-10);

            if (riskBatchId == null)
            {
                return (from x in _db.RiskBatches
                        join y in _db.RiskClients on x.RiskClient_Id equals y.Id
                        where x.ScoreDirect == scoreDirect &&
                            x.RiskClaims.Any(d => d.ClaimStatus == (int)RiskClaimStatus.QueuedForScoring ||
                            (d.ClaimStatus == (int)RiskClaimStatus.BeingScored && d.ClaimStatusLastModified <= timeout))
                            orderby y.BatchPriority
                        select x.Id).ToList();
            }
            else
            {
                var z = GetRiskBatch((int)riskBatchId);

                return (from x in _db.RiskBatches
                        join y in _db.RiskClients on x.RiskClient_Id equals y.Id
                        where x.ScoreDirect == scoreDirect && x.RiskClient_Id == z.RiskClient_Id &&
                            x.RiskClaims.Any(d => d.ClaimStatus == (int)RiskClaimStatus.QueuedForScoring ||
                            (d.ClaimStatus == (int)RiskClaimStatus.BeingScored && d.ClaimStatusLastModified <= timeout))
                        orderby y.BatchPriority
                        select x.Id).ToList();
            }
        }

        public bool IsABatchToLoadForClient(int riskClientId, bool submitDirect)
        {
            return (from x in _db.RiskBatches where x.SubmitDirect == submitDirect && x.RiskClient_Id == riskClientId && x.Id > 1 && x.BatchStatus == (int)RiskBatchStatus.QueuedForCreating select x).Any();
        } 

        public List<RiskBatch> GetListOfBatchesWithClaimsBeingLoaded()
        {
            return (from x in _db.RiskBatches
                    where x.BatchStatus == (int)RiskBatchStatus.ClaimsBeingLoaded
                    select x).ToList();
        }

        public RiskBatch GetABatchWithClaimsToLoad(int? batchId, bool submitDirect)
        {
            RiskBatch ret = null;
            bool done = false;

            DateTime timeout = DateTime.Now.AddMinutes(-10);

            while (!done)
            {
                using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                {
                    try
                    {
                        if (batchId == null)
                        {
                            ret = (from x in _db.RiskBatches
                                   join y in _db.RiskClients on x.RiskClient_Id equals y.Id
                                   where x.SubmitDirect == submitDirect && x.BatchStatus == (int)RiskBatchStatus.ClaimsQueuedForLoading ||
                                      (x.RiskClaims.Any(d => d.ClaimStatus == (int)RiskClaimStatus.QueuedForLoading || (d.ClaimStatus == (int)RiskClaimStatus.BeingLoaded && d.ClaimStatusLastModified <= timeout)))
                                   orderby x.BatchPriorityOverride, y.BatchPriority
                                   select x).FirstOrDefault();
                        }
                        else
                        {
                            ret = (from x in _db.RiskBatches
                                   join y in _db.RiskClients on x.RiskClient_Id equals y.Id
                                   where x.SubmitDirect == submitDirect && x.BatchStatus == (int)RiskBatchStatus.ClaimsQueuedForLoading ||
                                      (x.RiskClaims.Any(d => d.ClaimStatus == (int)RiskClaimStatus.QueuedForLoading || (d.ClaimStatus == (int)RiskClaimStatus.BeingLoaded && d.ClaimStatusLastModified <= timeout)))
                                       && x.Id == batchId
                                   orderby x.BatchPriorityOverride, y.BatchPriority
                                   select x).FirstOrDefault();
                        }

                        if (ret != null)
                        {
                            try
                            {
                                ret.BatchStatus = (int)RiskBatchStatus.ClaimsBeingLoaded;

                                _db.SaveChanges();

                                transaction.Commit();

                                done = true;
                            }
                            catch (OptimisticConcurrencyException)
                            {
                                ret = null;

                                // and go around again
                            }
                        }
                        else done = true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// Look for batch with QUEUED status and set status to CREATING CLAIMS
        /// </summary>
        /// <param name="riskBatchId"></param>
        /// <returns></returns>
        public RiskBatch GetABatchToLoad(int? riskBatchId, bool submitDirect)
        {
            RiskBatch ret = null;
            bool done = false;

            while (!done)
            {
                using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                {
                    try
                    {
                        if (riskBatchId != null)
                        {
                            ret = (from x in _db.RiskBatches
                                   join y in _db.RiskClients on x.RiskClient_Id equals y.Id
                                   where x.BatchStatus == (int)RiskBatchStatus.QueuedForCreating
                                       && x.Id == riskBatchId && x.SubmitDirect == submitDirect 
                                       orderby y.BatchPriority
                                   select x).FirstOrDefault();
                        }
                        else
                        {
                            ret = (from x in _db.RiskBatches
                                   join y in _db.RiskClients on x.RiskClient_Id equals y.Id
                                   where x.BatchStatus == (int)RiskBatchStatus.QueuedForCreating && x.SubmitDirect == submitDirect 
                                   orderby y.BatchPriority, x.Id ascending
                                   select x).FirstOrDefault();
                        }

                        if (ret != null)
                        {
                            try
                            {
                                ret.BatchStatus = (int)RiskBatchStatus.CreatingClaims;

                                _db.SaveChanges();

                                transaction.Commit();

                                done = true;
                            }
                            catch (OptimisticConcurrencyException)
                            {
                                ret = null;

                                // and go around again
                            }
                        }
                        else done = true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// Find a BATCH which has CLAIMS to load (Status = QUEUED).  Lock and change status to CREATING CLAIMS
        /// </summary>
        /// <param name="riskClientId"></param>
        /// <returns></returns>
        //public RiskBatch GetABatchToProcessForClient(int riskClientId)
        //{
        //    RiskBatch ret = null;
        //    bool done = false;

        //    while (!done)
        //    {
        //        using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
        //        {
        //            try
        //            {
        //                ret = (from x in _db.RiskBatches
        //                       where x.BatchStatus == (int)RiskBatchStatus.QueuedForCreating
        //                           && x.RiskClient.Id == riskClientId
        //                       orderby x.Id ascending
        //                       select x).FirstOrDefault();

        //                if (ret != null)
        //                {
        //                    try
        //                    {
        //                        ret.BatchStatus = (int)RiskBatchStatus.CreatingClaims;

        //                        _db.SaveChanges();

        //                        transaction.Commit();

        //                        done = true;
        //                    }
        //                    catch (OptimisticConcurrencyException)
        //                    {
        //                        ret = null;

        //                        // and go around again
        //                    }
        //                }
        //                else done = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                throw ex;
        //            }
        //        }
        //    }
        //    return ret;
        //}

        public bool IsBatchComplete(int riskBatchId)
        {
            return !(from x in _db.RiskClaims
                     where x.RiskBatch_Id == riskBatchId
                         && x.ClaimStatus >= (int)RiskClaimStatus.IsNew && x.ClaimStatus < (int)RiskClaimStatus.Scored
                     select x).Any();
        }
        public bool IsABatchToProcess(bool submitDirect)
        {
            return (from x in _db.RiskBatches
                    where x.SubmitDirect == submitDirect && x.BatchStatus == (int)RiskBatchStatus.QueuedForCreating
                    select x).Any();
        }

        public bool IsNextBatchToProcessForInternalClient()
        {

            var currentBatch = (from x in _db.RiskBatches
                                where x.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete 
                                orderby x.Id descending
                                select x).FirstOrDefault();

            var nextBatch = (from x in _db.RiskBatches
                             where x.BatchStatus == (int)RiskBatchStatus.ClaimsQueuedForLoading || x.BatchStatus == (int)RiskBatchStatus.QueuedForCreating
                             orderby x.Id
                             select x).FirstOrDefault();

            if (nextBatch != null)
            {
                if (currentBatch.RiskClient.InternalClient == 1 && nextBatch.RiskClient.InternalClient == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }


        public List<EBatchQueue> GetRiskBatchQueue()
        {
            List<EBatchQueue> ListBatchQueue = new List<EBatchQueue>();

            ListBatchQueue = (from x in _db.RiskBatches
                            join y in _db.RiskClients on x.RiskClient_Id equals y.Id
                            where x.BatchStatus == 3 && y.Status == 1
                            orderby x.BatchPriorityOverride, y.BatchPriority
                            select new EBatchQueue{
                                BatchId = x.Id, 
                                BatchRef = x.ClientBatchReference,
                                Client = y.ClientName,
                                ClaimsReceieved = x.TotalClaimsReceived,
                                Uploaded = x.CreatedDate
                            }).ToList();

            return ListBatchQueue;
        }



        public int SaveOverriddenBatchPriority(int ClientId, int UserId, string Who, int[] OverriddenBatchPriorityArray)
        {
            var rb = _db.RiskBatches.Create();

            int returnId = 0;

            int currentIndex = 0;

            try
            {
                foreach (int BatchId in OverriddenBatchPriorityArray)
                {
                    currentIndex++;

                    rb = _db.RiskBatches.Where(x => x.Id == BatchId).FirstOrDefault();

                    rb.BatchPriorityOverride = currentIndex;
                }

                _db.SaveChanges();

                returnId = 1;
            }
            catch (Exception ex)
            {
                returnId = -1;
            }

            return returnId;
        }
    }
}
