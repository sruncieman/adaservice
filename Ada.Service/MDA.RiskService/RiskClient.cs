﻿using MDA.DAL;
using MDA.RiskService.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MDA.RiskService
{
    public partial class RiskServices
    {
        public List<RiskClient> GetAllRiskClients()
        {
            return (from x in _db.RiskClients select x).ToList();
        }

        public RiskClient GetRiskClient(int clientId)
        {
            RiskClient riskClient = null;

            if (clientId == -1)
            {
                riskClient = (from x in _db.RiskClients
                              select x).FirstOrDefault();
            }
            else
            {
                riskClient = (from x in _db.RiskClients
                              where x.Id == clientId
                              select x).FirstOrDefault();
            }
            if (riskClient == null)
                throw new Exception("Client [" + clientId.ToString() + "] does not exist in RiskClient table");

            return riskClient;
        }


        public int FindLastBatchPriority()
        {
            var batchPriorityId = _db.RiskClients.Where(x => x.BatchPriority != null).OrderByDescending(y => y.BatchPriority)
                                        .Select(z => z.BatchPriority).FirstOrDefault();
            
            return Convert.ToInt32(batchPriorityId+1);
        }

        public List<RiskClient> GetAssignedRiskClientsForUser(int userId)
        {
            var ru2c = (from x in _db.RiskUser2Client
                        where x.RiskUser_Id == userId
                        select x.RiskClient).OrderBy(x => x.ClientName).ToList();

            var rc = (from x in _db.RiskUsers
                      where x.Id == userId
                      select x.RiskClient).ToList();

            foreach (var x in rc)
            {
                x.ClientName = x.ClientName + " (Default)";
            }

            rc.AddRange(ru2c);

            return rc;
        }

        public RiskClient FindRiskClientByName(string clientName)
        {
            string cName = clientName.ToUpper();

            return (from x in _db.RiskClients where x.ClientName == cName select x).FirstOrDefault();
        }

        public RiskClient CreatNewRiskClient(string clientName)
        {
            var rc = _db.RiskClients.Create();

            rc.ClientName = clientName;

            _db.RiskClients.Add(rc);

            _db.SaveChanges();

            return rc;
        }

        public List<InsurersClient> GetAllInsurersClients()
        {
            return (from x in _db.InsurersClients select x).ToList();
        }

        public List<ERiskClientBatchPriority> GetRiskClientBatchPriority(int clientId, string userName, int userId)
        {
            List<ERiskClientBatchPriority> listRiskClientBatchPriority = new List<ERiskClientBatchPriority>();

            var rclients = from rc in _db.RiskClients
                           where rc.BatchPriority != null && rc.Status == 1
                           orderby rc.BatchPriority

                           select new ERiskClientBatchPriority
                           {
                               Client = rc.ClientName,
                               PriorityId = (int)rc.BatchPriority
                           };

            foreach (var item in rclients)
            {
                listRiskClientBatchPriority.Add(item);
            }

            return listRiskClientBatchPriority;
        }

        public int SaveBatchPriorityClients(int ClientId, int UserId, string Who, int[] ClientBatchPriorityArray)
        {
            var rc = _db.RiskClients.Create();

            int returnId = 0;

            int currentIndex = 0;

            try
            {
                foreach (int clientBatchPriority in ClientBatchPriorityArray)
                {
                    currentIndex++;

                    rc = _db.RiskClients.Where(x => x.BatchPriority != null && x.BatchPriority == clientBatchPriority).FirstOrDefault();

                    rc.BatchPriority = currentIndex;
                }

                _db.SaveChanges();

                returnId = 1;
            }
            catch (Exception ex)
            {
                returnId = -1;
            }

            return returnId;
        }
    }
}