﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class EBatchQueue
    {
        public int BatchId { get; set; }
        public string Client { get; set; }
        public string BatchRef { get; set; }
        public DateTime? Uploaded { get; set; }
        public int ClaimsReceieved { get; set; }
    }
}
