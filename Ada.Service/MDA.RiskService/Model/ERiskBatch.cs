﻿using MDA.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskBatch
    {
        public int Id { get; set; }
        public int RiskClient_Id { get; set; }
        public string BatchReference { get; set; }
        public string ClientBatchReference { get; set; }
        public int BatchStatus { get; set; }
        public string BatchStatusAsString { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public MessageNode VerificationResults { get; set; }
        public MessageNode MappingResults { get; set; }
    }
}
