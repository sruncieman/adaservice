﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskClient
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public int AmberThreshold { get; set; }
        public int RedThreshold { get; set; }
        public string ExpectedFileExtension { get; set; }
        public int LevelOneReportDelay { get; set; }
        public int LevelTwoReportDelay { get; set; }
        public string InsurerName { get; set; }
        public int? Status { get; set; }
        public int InsuersClientsId { get; set; }
        public bool? RecordReserveChanges { get; set; }
        public int? BatchPriority { get; set; }
    }
}
