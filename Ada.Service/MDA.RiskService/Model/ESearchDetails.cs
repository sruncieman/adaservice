﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ESearchDetails
    {
        public string MatchedOn { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string VehicleReg { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleModel { get; set; }
        public string Involvement { get; set; }
        public string ClaimType { get; set; }
        public string Status { get; set; }
        public DateTime? Date { get; set; }
        public string Source { get; set; }
        public string Reference { get; set; }
    }
}