﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class RiskClientServicesResult
    {
         public int ServiceUsage { get; set; }
         public string ServiceName { get; set; }
         public int MaskValue { get; set; }
    }
}
