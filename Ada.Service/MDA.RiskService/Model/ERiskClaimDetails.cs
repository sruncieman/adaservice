﻿using MDA.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class EEntityScoreMessageList
    {
        public string EntityHeader { get; set; }
        public List<string> Messages { get; set; }
    }

    public class ERiskClaimDetails
    {
        public int RiskBatch_Id { get; set; }
        public string BatchReference { get; set; }
        public string ClientBatchReference { get; set; }
        public int BatchStatus { get; set; }
        public string BatchStatusAsString { get; set; }

        public int RiskClaim_Id { get; set; }
        public int BaseRiskClaim_Id { get; set; }
        public string MDAClaimRef { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ClientClaimRefNumber { get; set; }
        public int LevelOneRequestedCount { get; set; }
        public int LevelTwoRequestedCount { get; set; }
        public DateTime? LevelOneRequestedWhen { get; set; }
        public DateTime? LevelTwoRequestedWhen { get; set; }
        public string LevelOneRequestedWho { get; set; }
        public string LevelTwoRequestedWho { get; set; }
        public int LevelOneRequestedStatus { get; set; }
        public int LevelTwoRequestedStatus { get; set; }
        public string LevelOneRequestedStatusAsString { get; set; }
        public string LevelTwoRequestedStatusAsString { get; set; }
        public int ClaimStatus { get; set; }
        public int ClaimReadStatus { get; set; }
        public string ClaimStatusAsString { get; set; }

        public MessageNode ValidationResults { get; set; }
        public MessageNode CleansingResults { get; set; }

        public int Incident_Id { get; set; }
        public DateTime IncidentDate { get; set; }

        public decimal? PaymentsToDate { get; set; }
        public decimal? Reserve { get; set; }
        public int RiskClaimRun_Id { get; set; }
        public int TotalScore { get; set; }
        public int TotalKeyAttractorCount { get; set; }

        public string RiskRatingAsString { get; set; }
        public bool LatestVersion { get; set; }
        public bool LatestScored { get; set; }

        public bool NoteAdded { get; set; }
        public bool IntelAdded { get; set; }
        public bool DecisionAdded { get; set; }
        public int? DecisionId { get; set; }

        public int? ReserveChanged { get; set; }

        public List<EEntityScoreMessageList> MessageList { get; set; }
    }
}
