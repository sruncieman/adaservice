﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskSingleClaimDetail
    {
        public int RiskClaimId { get; set; }
        public MDA.Common.FileModel.MotorClaim Claim { get; set; }    
    }
}
