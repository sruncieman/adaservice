﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TelephoneNumber { get; set; }
        public string Password { get; set; }
        public DateTime? PasswordResetDate { get; set; }
        public int RiskClientId { get; set; }
        public string RiskClientName { get; set; }
        public int RiskTeamId { get; set; }
        public string RiskTeamName { get; set; }
        public int RiskRoleId { get; set; }
        public string RiskRoleName { get; set; }
        public int? Status { get; set; }
        public bool? AutoLoader { get; set; }
        public Dictionary<string, string> TemplateFunctions { get; set; }
        public int? UserTypeId { get; set; }
    }
}
