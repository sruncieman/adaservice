﻿
namespace MDA.RiskService.Model
{
    public enum RiskClaimStatus
    {
        FailedUnableToScoreKeoghsCFSCase = -8, // Keoghs CFS Case
        FailedAsMemberOfBadBatch         = -7,
        FailedOnValidation               = -6,
        FailedOnCleansing                = -5,
        FailedToScore                    = -4,
        FailedOnExternalService          = -3,
        FailedToLoad                     = -2,
        FailedToCreate                   = -1,
        IsNew                            = 0,   // record created
        QueuedForLoading                 = 1,   // record created and xml saved. Reading to be processed into db
        BeingLoaded                      = 2,  
        LoadingComplete                  = 3,
        QueuedForThirdParty              = 4,
        BeingThirdPartyProcessed         = 5,   // third party processing
        ThirdPartyComplete               = 6,
        QueuedForScoring                 = 7,
        BeingScored                      = 8,   // being scored
        Scored                           = 9,   // done
        Archived                         = 10   // done, no scores
    }

    public enum RiskBatchStatus
    {
        NoModifiedClaims                = -6,
        BadTooManyConsecutiveErrors     = -5,
        UnableToProcessMappingFailed    = -4,
        UnableToProcessNoMappingDefined = -3,
        UnableToProcessNoFileFound      = -2,
        Bad                             = -1,
        Initialising                    = 0,
        QueuedForCreating               = 1,
        CreatingClaims                  = 2,
        ClaimsQueuedForLoading          = 3,
        ClaimsBeingLoaded               = 4,
        BatchLoadComplete               = 5
    }

    public enum RiskClaimRunFinalScoreStatus
    {
        UnableToScore = -2,
        Error         = -1,
        Incomplete    = 0,
        Preliminary   = 1,
        Final         = 2
    }

    public enum RiskClaimReadStatus
    {
        Unread = 0,
        Read = 1,
        UnreadIncrease = 2,
        UnreadDecrease = 3,
        UnreadReserveChange = 4,
    }

    public enum RiskClaimReserveStatus
    {
        Unknown = 0,
        Increase = 1,
        Decrease = 2
    }

}
