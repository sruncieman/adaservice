﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskClientBatchPriority
    {
        public string Client { get; set; }
     
        public int PriorityId { get; set; }
    }
}
