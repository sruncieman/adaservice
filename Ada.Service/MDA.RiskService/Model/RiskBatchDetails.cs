﻿using MDA.DAL;
using System;
using System.Collections.Generic;

namespace MDA.RiskService.Model
{
    public class RiskClaimToProcess
    {
        public RiskClient RiskClient { get; set; }
        public RiskClaim RiskClaim { get; set; }

        public object CleansedPipelineClaim { get; set; }
        public int CleanseProcessingTime { get; set; }
        public int LoadProcessingTime { get; set; }
    }

    public class RiskClaimsToProcess
    {
        public RiskClient RiskClient { get; set; }
        public List<RiskClaim> RiskClaims { get; set; }

        public object CleansedPipelineClaim { get; set; }
        public int CleanseProcessingTime { get; set; }
        public int LoadProcessingTime { get; set; }
    }

    public class RiskBatchDetails
    {
        public int Id { get; set; }
        public int RiskClientId { get; set; }
        public string BatchReference { get; set; }
        public string BatchEntityType { get; set; }
        public string ClientBatchReference { get; set; }
        public int BatchStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string VerificationResults { get; set; }
        public string MappingResults { get; set; }
        public int TotalNumberOfClaims { get; set; }
    }
}