﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskClaimRun
    {

        public int Id { get; set; }

        public int RiskClaim_Id { get; set; }

        public string EntityType { get; set; }
        //public string ScoreEntityJson { get; set; }
        //public string ScoreEntityXml { get; set; }

        public int TotalScore { get; set; }

        public int TotalKeyAttractorCount { get; set; }
        //public string ReportOneVersion { get; set; }
        //public string ReportTwoVersion { get; set; }

        public string ExecutedBy { get; set; }

        public System.DateTime ExecutedWhen { get; set; }


        public List<EEntityScoreMessageList> MessageList { get; set; }
    }
}
