﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class RunningMetrics
    {
        public int BatchesWaiting { get; set; }
        public int BatchesLoading { get; set; }
        public int BatchesTotal { get; set; }
        public int BatchesError { get; set; }
        public int ExternalErrors { get; set; }
        public int ExternalRetriesRequired { get; set; }

        public int ClaimsWaitingToBeLoaded { get; set; }

        public int ClaimsBeingScored { get; set; }
        public int ClaimsWaitingOnExternal { get; set; }

        public int ClaimsWaitingToBeScored { get; set; }

        public int AverageLoadingTime { get; set; }

        public int AverageScoringTime { get; set; }
    }
}
