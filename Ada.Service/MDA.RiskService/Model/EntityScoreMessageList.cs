﻿using MDA.Common;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MDA.RiskService.Model
{
    class XmlMessages
    {
        public const string Namespace = "http://keo/MsgsV1";
    }


    [CollectionDataContract(ItemName = "I", Namespace = XmlMessages.Namespace)]
    public class ListOfMsgStrings : List<string> { }

    [DataContract(Name = "ML", Namespace = XmlMessages.Namespace)]
    public class EntityScoreMessageList
    {
        [DataMember(Name = "H")]
        public string EntityHeader { get; set; }
        [DataMember(Name = "M")]
        public ListOfMsgStrings Messages { get; set; }
    }

    public class RiskClaimRunDetails
    {
        public int RiskBatch_Id { get; set; }
        public string BatchReference { get; set; }
        public string ClientBatchReference { get; set; }
        public int BatchStatus { get; set; }

        public int RiskClaim_Id { get; set; }
        public int BaseRiskClaim_Id { get; set; }
        public string MDAClaimRef { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ClientClaimRefNumber { get; set; }
        public int LevelOneRequestedCount { get; set; }
        public int LevelTwoRequestedCount { get; set; }
        public bool CallbackRequested { get; set; }
        public DateTime? LevelOneRequestedWhen { get; set; }
        public DateTime? LevelTwoRequestedWhen { get; set; }
        public DateTime? CallbackRequestedWhen { get; set; }
        public string LevelOneRequestedWho { get; set; }
        public string LevelTwoRequestedWho { get; set; }
        public string CallbackRequestedWho { get; set; }
        public int LevelOneRequestedStatus { get; set; }
        public int LevelTwoRequestedStatus { get; set; }
        public int ClaimStatus { get; set; }
        public int ClaimReadStatus { get; set; }

        public string ValidationResults { get; set; }
        public string CleansingResults { get; set; }

        public int Incident_Id { get; set; }
        public DateTime IncidentDate { get; set; }
        public decimal? Reserve { get; set; }
        public decimal? PaymentsToDate { get; set; }

        public int RedThreshold { get; set; }
        public int AmberThreshold { get; set; }

        public int RiskClaimRun_Id { get; set; }
        public int TotalScore { get; set; }
        public int TotalKeyAttractorCount { get; set; }
        public string ScoreXml { get; set; }
        public string ScoreEntityEncodeFormat { get; set; }
        public string SummaryMessagesXml { get; set; }
        public string SummaryMessagesEncodeFormat { get; set; }
        public bool LatestVersion { get; set; }
        public bool LatestScored { get; set; }

        public bool NoteAdded { get; set; }
        public bool IntelAdded { get; set; }
        public bool DecisionAdded { get; set; }
        public int? DecisionId { get; set; }

        public int? ReserveChanged { get; set; }

        public List<EntityScoreMessageList> MessageList { get; set; }
    }


}
