﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ECueSearchEnquiryModel
    {
        public string ClaimNumber { get; set; }
        public DateTime IncidentDate { get; set; }
        public DateTime UploadedDate { get; set; }
        public List<ECueInvolvement> CueInvolvements { get; set; }
    }

    public class ECueInvolvement
    {
        public ECuePerson CuePerson { get; set; }
        public ECueAddress CueAddress { get; set; }
        public ECueVehicle CueVehicle { get; set; }
        public bool CueDatabaseAll { get; set; }
    }

    public class ECuePerson
    {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Involvement { get; set; }
        public DateTime Dob { get; set; }
        public string NI_Number { get; set; }
        public string DrivingLicenceNumber { get; set; }
    }

    public class ECueAddress
    {
        public string HouseName { get; set; }
        public string Number { get; set; }
        public string Street { get; set; }
        public string Locality { get; set; }
        public string Town { get; set; }
        public string PostCode { get; set; }
    }

    public class ECueVehicle
    {
        public string VehicleReg { get; set; }
        public string VIN { get; set; }
    }
}
