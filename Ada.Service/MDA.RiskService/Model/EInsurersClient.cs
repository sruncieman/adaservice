﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class EInsurersClient
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
    }
}
