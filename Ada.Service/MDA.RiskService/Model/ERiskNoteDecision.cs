﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskNoteDecision
    {
        public int Id { get; set; }
        public string Decision { get; set; }
        public int RiskClient_Id { get; set; }
        public int ADARecordStatus { get; set; }
    }
}
