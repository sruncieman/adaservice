﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskTeam
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
    }
}
