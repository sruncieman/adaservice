﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskWord
    {
        public int Id { get; set; }
        public int RiskClient_Id { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string LookupWord { get; set; }
        public string ReplacementWord { get; set; }
        public bool ReplaceWholeString { get; set; }
        public System.DateTime DateAdded { get; set; }
        public string SearchType { get; set; }
    }
}
