﻿using MDA.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskClaim
    {

        public int Id { get; set; }

        public int RiskBatch_Id { get; set; }

        public int Incident_Id { get; set; }

        public string MdaClaimRef { get; set; }

        public int BaseRiskClaim_Id { get; set; }

        public int ClaimStatus { get; set; }

        public int ClaimReadStatus { get; set; }

        public string ClientClaimRefNumber { get; set; }

        public int LevelOneRequestedCount { get; set; }

        public int LevelTwoRequestedCount { get; set; }

        public Nullable<System.DateTime> LevelOneRequestedWhen { get; set; }

        public Nullable<System.DateTime> LevelTwoRequestedWhen { get; set; }

        public string LevelOneRequestedWho { get; set; }

        public string LevelTwoRequestedWho { get; set; }

        public string SourceReference { get; set; }
        //public string SourceEntityXML { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public int TotalScore { get; set; }

        public MessageNode ValidationResults { get; set; }

        public MessageNode CleansingResults { get; set; }
    }
}
