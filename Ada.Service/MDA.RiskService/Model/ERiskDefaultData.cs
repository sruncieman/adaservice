﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskDefaultData
    {
        public int Id { get; set; }
        public int RiskClient_Id { get; set; }
        public string ConfigurationValue { get; set; }
        public bool IsActive { get; set; }
        public string RiskConfigurationDescription { get; set; }

    }
}
