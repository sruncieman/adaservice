﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskNote
    {

        public int Id { get; set; }
        public int BaseRiskClaim_Id { get; set; }
        public int ClientId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Decision { get; set; }
        public int? DecisionId { get; set; }
        public bool Deleted { get; set; }
        public string NoteDesc { get; set; }
        public int RiskClaim_Id { get; set; }
        public int UserId { get; set; }
        public int? Visibilty { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public byte[] OriginalFile { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
    }
}
