﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService.Model
{
    public class ERiskSingleClaim
    {
        public int Id { get; set; }
        public string ClaimType { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime IncidentDate { get; set; }
        public string SavedBy { get; set; }
        public DateTime DateSaved { get; set; }
    }
}
