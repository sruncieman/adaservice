﻿
using System;
using System.Linq;
using System.Data.Linq;
using MDA.RiskService.Model;
using MDA.DAL;
using MDA.Common.Server;
using System.Collections.Generic;

namespace MDA.RiskService
{
    internal class Membership
    {
        private bool _trace = false;
        private CurrentContext _ctx;
        private MdaDbContext _db;

        public Membership(CurrentContext ctx)
        {
            _ctx = ctx;
            _db = ctx.db as MdaDbContext;
            _trace = ctx.TraceLoad;
        }

        public int GetPasswordFailuresSinceLastSuccess(string userName)
        {
            var riskUser = _db.RiskUsers.Where(y => y.UserName == userName.Trim()).FirstOrDefault();

            if (riskUser != null)
            {
                return riskUser.BadLoginCount;
            }
            return -1;
        }

        public bool ValidateUser(string username, string hashedPassword, string ipAddress)
        {
            bool valid = false;

            var riskUser = _db.RiskUsers.Where(y => y.UserName == username.Trim()).FirstOrDefault();
            var riskUserAudit = _db.RiskUserAudits.Create();

            if ( riskUser != null )
            {
                valid = (riskUser.BadLoginCount < 5 && riskUser.Status == 1 && riskUser.Password == hashedPassword);
                riskUserAudit.UserId = riskUser.Id;

                if (valid)
                {
                    riskUser.LastGoodLoginDate = DateTime.Now;
                    riskUser.BadLoginCount = 0;
                    riskUser.Token = null;
                    riskUser.TokenExpiry = null;
                    riskUserAudit.SuccessfulLogin = true;
                }
                else
                {
                    riskUser.BadLoginCount += 1;
                    if (riskUser.BadLoginCount >= 5)
                    {
                        riskUser.Status = 2;
                    }
                    riskUserAudit.SuccessfulLogin = false;
                }

                riskUserAudit.IPAddress = ipAddress;
                riskUserAudit.CreatedDate = DateTime.Now;
            }
            else
            {
                riskUserAudit.CreatedDate = DateTime.Now;
                riskUserAudit.IPAddress = ipAddress;
                riskUserAudit.UserId = -1;
                riskUserAudit.SuccessfulLogin = false;
            }

            _db.RiskUserAudits.Add(riskUserAudit);
            _db.SaveChanges();

            return valid;
        }

        public bool IsUserInRole(string userName, string roleName)
        {
            return (from roles in _db.RiskRoles 
                        join users in _db.RiskUsers on roles.Id equals users.RiskRole_Id
                        where users.UserName == userName select roles).Any();
        }

        public string[] GetRolesForUser(string userName)
        {
            return (from roles in _db.RiskRoles
                    join users in _db.RiskUsers on roles.Id equals users.RiskRole_Id
                    where users.UserName == userName
                    select roles.Name).ToArray();
        }

        public RiskUserDetails GetUserDetails(int userId, int? clientId)
        {
            RiskUserDetails riskUserDetails = new RiskUserDetails();
            RiskServices ser = new RiskServices(_ctx);
            if (clientId!=0)
            {
                int client_Id = Convert.ToInt32(clientId);
                var changedClient = ser.GetRiskClient(client_Id);

                riskUserDetails = (from x in _db.RiskUsers
                        where x.Id == userId
                        select new RiskUserDetails
                        {
                            Id = x.Id,
                            UserName = x.UserName,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            TelephoneNumber = x.TelephoneNumber,
                            RiskClientId = client_Id,
                            RiskClientName = changedClient.ClientName ,
                            RiskTeamId = x.RiskTeam_Id,
                            RiskTeamName = x.RiskTeam.TeamName,
                            RiskRoleId = x.RiskRole_Id,
                            RiskRoleName = x.RiskRole.Name,
                            Status = x.Status,
                            AutoLoader = x.AutoLoader,
                            Password = x.Password,
                            PasswordResetDate = x.PasswordResetDate,
                            UserTypeId = x.UserType

                        }).FirstOrDefault();
            }

            else
            {
                riskUserDetails = (from x in _db.RiskUsers
                        where x.Id == userId
                        select new RiskUserDetails
                        {
                            Id = x.Id,
                            UserName = x.UserName,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            TelephoneNumber = x.TelephoneNumber,
                            RiskClientId = x.RiskClient_Id,
                            RiskClientName = x.RiskClient.ClientName,
                            RiskTeamId = x.RiskTeam_Id,
                            RiskTeamName = x.RiskTeam.TeamName,
                            RiskRoleId = x.RiskRole_Id,
                            RiskRoleName = x.RiskRole.Name,
                            Status = x.Status,
                            AutoLoader = x.AutoLoader,
                            Password = x.Password,
                            PasswordResetDate = x.PasswordResetDate,
                            UserTypeId = x.UserType,

                        }).FirstOrDefault();
            }


            int riskRoleId = ser.GetRoleByUserId(userId);

            var templateFunctions = (from x in _db.RiskTemplateFunctions
                                     where x.RiskRole_Id == riskRoleId
                                     select x).ToList();

            Dictionary<string, string> DictFunctionDetails = new Dictionary<string, string>();

            foreach (var item in templateFunctions)
            {
                DictFunctionDetails.Add(item.Name, item.Value);
            }

            riskUserDetails.TemplateFunctions = DictFunctionDetails;

            return riskUserDetails;
        }


        public void CreateUser(string userName, string hashPassword, string email, string firstName, string lastName, string telephoneNumber, int clientId, int roleId, int teamId, int? status, bool? autoloader, int? userTypeId)
        {
            var p = _db.RiskUsers.Create();

            p.UserName = userName;
            p.FirstName = firstName;
            p.LastName = lastName;
            p.TelephoneNumber = telephoneNumber;
            p.Password = hashPassword;
            p.PasswordResetDate = DateTime.Now;
            p.RiskRole_Id = roleId;
            p.RiskClient_Id = clientId;
            p.RiskTeam_Id = teamId;
            p.Status = status;
            p.AutoLoader = autoloader;
            p.UserType = userTypeId;

            _db.RiskUsers.Add(p);

            _db.SaveChanges();
        }

        public void CreateClient(string clientName, int? statusId, string fileExtensions, int amberThreshold, int redThreshold, int insurersClienId)
        {
            var c = _db.RiskClients.Create();

            c.ClientName = clientName;
            c.AmberThreshold = amberThreshold;
            c.Status = statusId;
            c.ExpectedFileExtension = fileExtensions;
            c.RedThreshold = redThreshold;
            c.InsurersClients_Id = insurersClienId;

            if(c.Status==1)
            {
                //Find last batchpriority number, add one and add.
                RiskServices ser = new RiskServices(_ctx);
                
                c.BatchPriority = ser.FindLastBatchPriority();
            }

            _db.RiskClients.Add(c);

            _db.SaveChanges();
        }

        public void CreateRiskWord(int riskClientId, string tableName, string fieldName, string lookupWord, string replacementWord, bool replaceWholeString, DateTime dateAdded, string searchType)
        {
            var c = _db.RiskWordFilters.Create();

            c.DateAdded = dateAdded;
            c.FieldName = fieldName;
            c.LookupWord = lookupWord;
            c.ReplacementWord = replacementWord;
            c.ReplaceWholeString = replaceWholeString;
            //c.RiskClient = riskClientId;
            c.RiskClient_Id = riskClientId;
            c.SearchType = searchType;
            c.TableName = tableName;

            _db.RiskWordFilters.Add(c);

            _db.SaveChanges();
        }

        public void CreateRiskDefaultData(string configurationValue, bool isActive, int riskClient_Id, int riskConfigurationDescriptionId)
        {
            var c = _db.RiskConfigurationValues.Create();

            c.ConfigurationValue = configurationValue;
            c.IsActive = isActive;
            c.RiskClient_Id = riskClient_Id;
            c.RiskConfigurationDescription_Id = riskConfigurationDescriptionId;

            _db.RiskConfigurationValues.Add(c);

            _db.SaveChanges();
        }

        public void DeleteRiskDefaultData(int id)
        {
            var deleteRdd =
                (from ru2c in _db.RiskConfigurationValues
                 where ru2c.Id == id
                 select ru2c).ToList();

            foreach (var detail in deleteRdd)
            {
                _db.RiskConfigurationValues.Remove(detail);
            }

            try
            {
                _db.SaveChanges();
            }
            catch (Exception e)
            {
                // Provide for exceptions.
            }
        }

        public void UpdateClient(int iD, string clientName, int? statusId, string fileExtensions, int amberThreshold, int redThreshold, int insurersClienId, bool? recordReserveChanges, int? batchPriority)
        {
            var client = (from x in _db.RiskClients
                          where x.Id == iD
                          select x).FirstOrDefault();

            if (client != null)
            {
                client.ClientName = clientName;
                client.AmberThreshold = amberThreshold;

                if (client.Status == 1 && statusId == 0)
                    client.BatchPriority = null;

                if (client.Status == 0 && statusId == 1)
                {
                    //Find last batchpriority number, add one and add.
                    RiskServices ser = new RiskServices(_ctx);
                    client.BatchPriority = ser.FindLastBatchPriority();
                }
                client.Status = statusId;
                client.ExpectedFileExtension = fileExtensions;
                client.RedThreshold = redThreshold;
                client.InsurersClients_Id = insurersClienId;
                client.RecordReserveChanges = recordReserveChanges;
                client.BatchPriority = batchPriority;
            }

            _db.SaveChanges();
        }

        public void UpdateUser(string userName, string hashPassword, string email, string firstName, string lastName, string telephoneNumber, int clientId, int roleId, int teamId, int? status, bool? autoloader, int? userTypeId)
        {

            var riskUser = (from x in _db.RiskUsers
                            where x.UserName == userName
                            select x).FirstOrDefault();

            if (riskUser != null)
            {

                riskUser.UserName = userName;
                riskUser.FirstName = firstName;
                riskUser.LastName = lastName;
                riskUser.TelephoneNumber = telephoneNumber;
                riskUser.RiskRole_Id = roleId;
                riskUser.RiskClient_Id = clientId;
                riskUser.RiskTeam_Id = teamId;
                riskUser.Status = status;
                riskUser.AutoLoader = autoloader;
                riskUser.UserType = userTypeId;


                _db.SaveChanges();

            }
        }

        public void CreateRiskUser2Client(int clientId, int userId)
        {
            var p = _db.RiskUser2Client.Create();

            p.RiskClient_Id = clientId;
            p.RiskUser_Id = userId;

            _db.RiskUser2Client.Add(p);

            _db.SaveChanges();
        }

        public void DeleteRiskUser2Client(int clientId, int userId)
        {
             //Query the database for the rows to be deleted. 
            var deleteru2c =
                (from ru2c in _db.RiskUser2Client
                where ru2c.RiskClient_Id == clientId
                && ru2c.RiskUser_Id == userId
                select ru2c).ToList();

            foreach (var detail in deleteru2c)
            {
                _db.RiskUser2Client.Remove(detail);
            }

            try
            {
                _db.SaveChanges();
            }
            catch (Exception e)
            {
                // Provide for exceptions.
            }
        }

        public bool ChangePassword(string username, string newHashPassword)
        {
            var u = (from x in _db.RiskUsers where x.UserName == username select x).FirstOrDefault();

            if (u != null)
            {              
                var p = _db.RiskPasswordHistories.Create();

                p.RiskUser_Id = u.Id;
                p.Password = u.Password;
                p.DateChanged = DateTime.Now;

                _db.RiskPasswordHistories.Add(p);

                u.Password = newHashPassword;
                u.PasswordResetDate = DateTime.Now;
                u.Token = null;
                u.TokenExpiry = null;

                _db.SaveChanges();

                return true;
            }

            return false;
        }

        public bool IsPasswordUnique(string userName, string password)
        {
            var u = (from x in _db.RiskUsers where x.UserName == userName select x).FirstOrDefault();          

            int numberOfReuse = 11;  // or configurable

            var historiesQuery = (from histories in _db.RiskPasswordHistories
                                  where histories.RiskUser_Id == u.Id
                                  orderby histories.DateChanged descending
                                  select histories.Password).Take<string>(numberOfReuse);

            if (historiesQuery.Contains(password) || u.Password == password)
            {
                return false;
            }
            
            return true;
        }

    }
}
