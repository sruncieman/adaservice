﻿
using MDA.RiskService.Model;
using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using System;


namespace MDA.RiskService
{
    public partial class RiskServices
    {
       public int GetUserId(string UserName)
        {
            var userId = (from ru in _db.RiskUsers
                            where ru.UserName == UserName
                            select ru).FirstOrDefault();

            if (userId != null)
                return userId.Id;
            return 0;
        }
        
       public ERiskUser GetAutoloaderFromClientId(int clientId)
       {
           var autoLoaders = (from ru in _db.RiskUsers
                   where ru.RiskClient_Id == clientId && ru.AutoLoader == true
                   select ru).ToList();


           ERiskUser eru = new ERiskUser();

           if (autoLoaders.Count()>0)
           {
               var loader = autoLoaders.FirstOrDefault();

               eru.Id = loader.Id;
               eru.UserName = loader.UserName;
           }

           return eru;
       }

        public bool CheckUserBelongsToClient(int clientId, int userId)
        {
            if (userId == 0)
                return true;
            else
            {
                return (from ru in _db.RiskUsers
                        join ru2c in _db.RiskUser2Client on ru.Id equals ru2c.RiskUser_Id into j1
                        from leftRiskUsers2Client in j1.DefaultIfEmpty()
                        where (ru.Id == userId && ru.RiskClient_Id == clientId) || (leftRiskUsers2Client.RiskClient_Id == clientId && leftRiskUsers2Client.RiskUser_Id == userId) && ru.Status == 1
                        select ru).Any();
            }
        }

        public bool CheckUserHasAccessToReport(int riskBatchId, int loggedInUser, int clientId)
        {
            var result = (from rb in _db.RiskBatches
                            where rb.Id == riskBatchId && rb.RiskClient_Id == clientId
                            select rb).FirstOrDefault();

            if (result != null)
            {
                // check batch number belongs to client and user belongs to client
                return CheckUserBelongsToClient(clientId, loggedInUser);
            }

            return false;
        }

        public string GetFullName(int userId)
        {
            var user = (from ru in _db.RiskUsers
                        where ru.Id == userId
                        select ru).FirstOrDefault();

            if (user != null)
                return (user.FirstName + " " + user.LastName);

            return "";
        }

        public string GetUserName(int userId)
        {
            var user = (from ru in _db.RiskUsers
                        where ru.Id == userId
                        select ru).FirstOrDefault();

            if (user != null)
                return (user.UserName);

            return "";
        }

        public List<ERiskUserLocked> GetLockedUsers(int clientId, string userName, int userId)
        {
            List<ERiskUserLocked> lstERiskUserLocked = new List<ERiskUserLocked>();

            var query = (from ru in _db.RiskUsers
                         where ru.BadLoginCount >= 5 || ru.Status == 2
                         select ru).ToList();


            foreach (var item in query)
            {
                ERiskUserLocked erl = new ERiskUserLocked();

                erl.Id = item.Id;
                erl.Username = item.UserName;
                erl.Forename = item.FirstName;
                erl.Surname = item.LastName;
                erl.Template = item.RiskRole.Name;
                

                lstERiskUserLocked.Add(erl);
            }

            return lstERiskUserLocked;
        }

        public int FindUserName(string userName)
        {
            var riskUserWithUserNameCount = _db.RiskUsers.Where(x => x.UserName == userName).Count();

            if (riskUserWithUserNameCount == 0)
                return 1;
            else
            {
                return 0;
            }
        }

        public int UnlockRiskUser(int clientId, string userName, int userId, int unlockUserId)
        {
            var user = _db.RiskUsers.Where(x => x.Id == unlockUserId).FirstOrDefault();
            user.BadLoginCount = 0;
            user.Status = 1;
            _db.SaveChanges();

            int userBadLoginCount = _db.RiskUsers.Where(x => x.Id == unlockUserId).Select(x => x.BadLoginCount).FirstOrDefault();

            if (userBadLoginCount == 0)
                return 1; //success
            else
                return 0; //failure
        }

        public int GeneratePasswordResetToken(string userName, string token, DateTime tokenExpiry)
        {

            var user = (from x in _db.RiskUsers where x.UserName == userName select x).FirstOrDefault();
            //var user = _db.RiskUsers.Where(x => x.UserName == userName.Trim()).FirstOrDefault();

            if (user != null)
            {
                user.Token = token;
                user.TokenExpiry = tokenExpiry;
                _db.SaveChanges();

                return 1;
            }

            return 0;
            
        }

        public string ValidatePasswordResetToken(string token)
        {

            var user = _db.RiskUsers.Where(x => x.Token == token && x.TokenExpiry > DateTime.Now).FirstOrDefault();

            if (user != null)
            {
                return user.UserName;
            }

            return null;
        }

        public IEnumerable<string> GetUsers(int clientId, int teamId, int userId)
        {
            if (clientId == -1) // Get all users for all clients
            {
                return (from riskUser in _db.RiskUsers
                        select riskUser.UserName).ToList();
            }
            else if (teamId == -1 && userId == -1) // All users for client
            {
                return (from riskUser in _db.RiskUsers
                        where riskUser.RiskClient_Id == clientId
                        select riskUser.UserName).ToList();
            }
            else if (userId == -1) // All users for team
            {
                return (from riskUser in _db.RiskUsers
                        where riskUser.RiskTeam_Id == teamId
                            && riskUser.RiskClient_Id == clientId
                        select riskUser.UserName).ToList();
            }
            else // Just the user
            {
                return (from riskUser in _db.RiskUsers
                        where riskUser.Id == userId
                        && riskUser.RiskClient_Id == clientId
                        && riskUser.RiskTeam_Id == teamId
                        select riskUser.UserName).ToList();
            }

        }

        public IEnumerable<int> GetUsersById(int clientId, int? teamId, int userId)
        {
            // 1, 4 , -1

            if (clientId == -1) // Get all users for all clients
            {
                return (from riskUser in _db.RiskUsers
                        select riskUser.Id).ToList();
            }

            else if (teamId == -1 && userId == -1) // All users for client
            {
                return (from riskUser in _db.RiskUsers
                        where riskUser.RiskClient_Id == clientId
                        select riskUser.Id).ToList();
            }

            else if (userId == -1) // All users for team
            {
                //return (from riskUser in _db.RiskUsers
                //        where riskUser.RiskTeam_Id == teamId
                //            && riskUser.RiskClient_Id == clientId
                //        select riskUser.Id).ToList();

                return (from riskUser in _db.RiskUsers
                        where riskUser.RiskClient_Id == clientId
                        select riskUser.Id).ToList();
            }

            else // Just the user
            {
                return (from riskUser in _db.RiskUsers
                        where riskUser.Id == userId
                        && riskUser.RiskClient_Id == clientId
                        && riskUser.RiskTeam_Id == teamId
                        select riskUser.Id).ToList();
            }
        }

        public IEnumerable<string> GetUsersByRole(int UserId)
        {
            IEnumerable<string> users = null;

            var userDetails = (from riskUser in _db.RiskUsers
                               join riskRole in _db.RiskRoles on riskUser.RiskRole_Id equals riskRole.Id
                               where riskUser.Id == UserId
                               select new
                               {
                                   RiskClient_Id = riskUser.RiskClient_Id,
                                   RiskTeam_Id = riskUser.RiskTeam_Id,
                                   Riskuser_ID = riskUser.Id,
                                   RiskRole = riskRole.Name
                               }).FirstOrDefault();
            //
            switch (userDetails.RiskRole)
            {
                case "Keoghs Administrator":
                    // Get all users for all clients
                    users = from riskUser in _db.RiskUsers
                            select riskUser.UserName;

                    //return users.ToList();
                    break;
                case "Account Manager":
                    // All users for client
                    users = from riskUser in _db.RiskUsers
                            where riskUser.RiskClient_Id == userDetails.RiskClient_Id
                            select riskUser.UserName;

                    //return users.ToList();
                    break;
                case "Team Manager":
                    // All users for team
                    users = from riskUser in _db.RiskUsers
                            where riskUser.RiskTeam_Id == userDetails.RiskTeam_Id
                            && riskUser.RiskClient_Id == userDetails.RiskClient_Id
                            select riskUser.UserName;

                    //return users.ToList();
                    break;
                case "Claims Handler":
                    users = from riskUser in _db.RiskUsers
                            where riskUser.Id == userDetails.Riskuser_ID
                            && riskUser.RiskClient_Id == userDetails.RiskClient_Id
                            && riskUser.RiskTeam_Id == userDetails.RiskTeam_Id
                            select riskUser.UserName;

                    //return users.ToList();
                    break;

            }
            return users.ToList();
        }

        public List<ERiskUser> GetAllRiskUsers()
        {
            List<ERiskUser> riskUsers = new List<ERiskUser>();

            var users = (_db.RiskUsers).ToList();

            if (users.Count() > 0)
            {

                foreach (var ru in users)
                {
                    ERiskUser eru = new ERiskUser();

                    eru.UserName = ru.UserName;
                    eru.Id = ru.Id;
                    eru.RiskClientId = ru.RiskClient_Id;

                    eru.FirstName = ru.FirstName;
                    eru.LastName = ru.LastName;
                    eru.RiskRoleName = ru.RiskRole.Name;
                    eru.TelephoneNumber = ru.TelephoneNumber;
                    eru.Status = ru.Status;
                    eru.AutoLoader = ru.AutoLoader;
                    eru.Password = ru.Password;
                    eru.PasswordResetDate = ru.PasswordResetDate;
                    

                    riskUsers.Add(eru);

                }

            }

            return riskUsers;
        }

        public RiskUser FindRiskUserByName(string userName)
        {
            string uName = userName.ToUpper();

            return (from x in _db.RiskUsers where x.UserName == uName select x).FirstOrDefault();
        }
    }
}
