﻿using MDA.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService
{
    public partial class RiskServices
    {
        public void SaveSyncronisationTiming(long syncTimeInMs, SyncOrDedupeType syncOrDedupeType)
        {
            var syncTiming = _db.Timings.Create();

            syncTiming.SyncTimeInMs = (int)syncTimeInMs;
            syncTiming.SyncDateTime = DateTime.Now;
            syncTiming.SyncType = (int)syncOrDedupeType;

            _db.Timings.Add(syncTiming);

            _db.SaveChanges();
        }
    }
}
