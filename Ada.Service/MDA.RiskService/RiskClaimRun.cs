﻿
using System;
using System.Configuration;
using System.Collections;
using System.Linq;
using MDA.RiskService.Model;
using MDA.DAL;
using MDA.Common;

using RiskEngine.Scoring.Model;
using System.Collections.Generic;
using RiskEngine.Model;
using RiskEngine.Scoring.Entities;
using MDA.ExternalServices.Model;

namespace MDA.RiskService
{
    public partial class RiskServices
    {

        public RiskClaimRun GetRiskClaimRun(int riskClaimRunId)
        {
            return (from x in _db.RiskClaimRuns
                    where x.Id == riskClaimRunId
                    select x).FirstOrDefault();
        }

        //private MessageNode _GetScoresAsNodeTree(IRiskEntity entity)
        //{
        //    string name = entity.GetType().Name;

        //    MessageNode node = (entity._ScoreResults == null) ? new MessageNode("No " + name)
        //               : entity._ScoreResults.ScoresAsNodes(entity.DisplayText + " SCORE=[" + entity.TotalEntityScore + ", " + entity.TotalChildScore + "]");

        //    // Now find all the properties with the SCOREABLE attribute on THIS entity
        //    var propList = from p in entity.GetType().GetProperties()
        //                   let attr = p.GetCustomAttributes(typeof(ScoreableItemAttribute), true)
        //                   where attr.Length == 1
        //                   select new { Property = p, Attribute = attr.First() as ScoreableItemAttribute };

        //    // Now cycle through each property
        //    foreach (var p in propList)
        //    {
        //        // Now check if the property is a collection.  If so score each item in the collection otherwise score the single property value
        //        if (p.Attribute.IsCollection)
        //        {
        //            IEnumerable items = (IEnumerable)p.Property.GetValue(entity);   // Get the list to cycle through

        //            foreach (var item in items)  // Score each list item
        //            {
        //                if (item != null) 
        //                    node.AddNode(_GetScoresAsNodeTree((IRiskEntity)item));
        //            }
        //        }
        //        else
        //        {
        //            var item = (IRiskEntity)p.Property.GetValue(entity);  // Get the single value

        //            if ( item != null )
        //                node.AddNode(_GetScoresAsNodeTree((IRiskEntity)item));
        //        }
        //    }

        //    return node;
        //}

        public MessageNode GetScoresAsNodeTree(RiskClaimRun rcr)
        {
            if (rcr == null || rcr.ScoreEntityXml == null) return new MessageNode("Nothing");

            bool useJson = rcr.ScoreEntityEncodeFormat == "json";

            var v = DeserializeScores(rcr.ScoreEntityXml, useJson);

            MessageNode node = new MessageNode("ClaimScoreResults");

            MessageNode n1 = new MessageNode("Total Score " + v.TotalScore.ToString());
            MessageNode n2 = new MessageNode("Total KA " + v.TotalKeyAttractorCount.ToString());

            //MessageNode n3 = _GetScoresAsNodeTree((IRiskEntity)v.CompleteScoreResults);
            MessageNode n3 = v.CompleteScoreResults.GetScoresAsNodeTree(true);

            node.AddNode(n1);
            node.AddNode(n2);
            node.AddNode(n3);

            return node;
        }


       

        /// <summary>
        /// Create new Risk Run
        /// </summary>
        /// <param name="riskClaimId">Claim ID</param>
        /// <param name="entity">Entity being risked. Can be null</param>
        /// <param name="who">Logged in user</param>
        /// <returns>RiskClaimRun.Id</returns>
        public int CreateRiskClaimRun(int riskClaimId, ClaimScoreResults resultsEntity, string clientBatchRef, string who, long scoringTimeInMs)
        {

            #region Mark all older score runs as old
            var oldVersions = from x in _db.RiskClaimRuns
                              where x.LatestVersion == true && x.RiskClaim_Id == riskClaimId
                              select x;

            foreach (var r in oldVersions)
                r.LatestVersion = false;

            _db.SaveChanges();
            #endregion

            #region Mark all older RiskClaims as NOT the latest scored version

            var riskClaim = GetRiskClaim(riskClaimId);

            var oldClaimVersions = from x in _db.RiskClaims
                                   where x.LatestScored == true && x.BaseRiskClaim_Id == riskClaim.BaseRiskClaim_Id
                                   select x;

            foreach (var r in oldClaimVersions)
                r.LatestScored = false;


            riskClaim.LatestScored = true;

            _db.SaveChanges();

            #endregion

            #region Remove any old PDF reports against this BATCH
            var riskBatch = GetRiskBatch(riskClaim.RiskBatch_Id);

            if (riskBatch.RiskBatchReport_Id != null)
            {
                //var riskReport = (from x in db.RiskReports where x.PkId == (int)riskBatch.RiskBatchReport_Id select x).First();

                //db.RiskReports.Remove(riskReport);

                riskBatch.RiskBatchReport_Id = null;

                _db.SaveChanges();
            }

            #endregion

            // Check to see if any external records still need processing

            var anyExternalReqsOutstanding = (from x in _db.RiskExternalServicesRequests
                                              where x.RiskClaim_Id == riskClaimId
                                                && (x.CallStatus == (int)RiskExternalServiceCallStatus.Created
                                                 || x.CallStatus == (int)RiskExternalServiceCallStatus.FailedNeedsRetry
                                                 || x.CallStatus == (int)RiskExternalServiceCallStatus.UnavailableNeedsRetry)
                                              select x).Any();

            var rcr = _db.RiskClaimRuns.Create();

            rcr.FinalScoreStatus = (int)((anyExternalReqsOutstanding) ? RiskClaimRunFinalScoreStatus.Preliminary
                                                                      : RiskClaimRunFinalScoreStatus.Final);

            if (resultsEntity.StatusReturnCode < 0) // -1 error. -99 incident
            {
                switch (resultsEntity.StatusReturnCode)
                {
                    case -99:
                        rcr.FinalScoreStatus = (int)(RiskClaimRunFinalScoreStatus.UnableToScore);
                        break;

                    default:
                        rcr.FinalScoreStatus = (int)(RiskClaimRunFinalScoreStatus.Error);
                        break;
                }

                resultsEntity.TotalScore = 0;
                resultsEntity.TotalKeyAttractorCount = 0;
            }

            bool useJson = Convert.ToBoolean(ConfigurationManager.AppSettings["UseJsonForScores"]);

            rcr.ScoringDurationInMs         = (int)scoringTimeInMs;
            rcr.ExecutedBy                  = who;
            rcr.ExecutedWhen                = DateTime.Now;
            rcr.RiskClaim_Id                = riskClaimId;
            rcr.EntityType                  = resultsEntity.GetType().ToString();
            rcr.ScoreEntityEncodeFormat     = (useJson) ? "json" : "xml";
            rcr.ScoreEntityXml              = SerializeScores(resultsEntity, useJson);
            rcr.SummaryMessagesEncodeFormat = (useJson) ? "json" : "xml";
            rcr.SummaryMessagesXml          = SerializeLowMessages(GetClaimSummaryMessages(resultsEntity), useJson);
            rcr.TotalScore                  = resultsEntity.TotalScore;
            rcr.TotalKeyAttractorCount      = resultsEntity.TotalKeyAttractorCount;
            rcr.LatestVersion               = true;

            if (rcr.TotalScore == 0)
                rcr.ScoreEntityXml = null;

            var rc = GetRiskClaim(riskClaimId);

            var prevRiskClaim = GetPreviousRiskClaimVersion(rc);
            if ( prevRiskClaim != null )
            {
                var prcr = GetLatestRiskClaimRun(prevRiskClaim.Id);

                if (prcr != null)
                {
                    //var previousTotalScore = prcr.TotalScore;// 15
                  
                    //var currentTotalScore = rcr.TotalScore; // 45

                    var previousRAG = GetRAGStatus(prcr, prcr.AmberThreshold, prcr.RedThreshold);
                    var currentRAG = GetRAGStatus(rcr, prcr.AmberThreshold, prcr.RedThreshold);
                    var unreadStatus = HasRiskStatusChanged(previousRAG, currentRAG);

                    if(unreadStatus!="NoChange")
                    {
                        var claimToUpdate = (from x in _db.RiskClaims
                                             where x.Id == rcr.RiskClaim_Id
                                             select x).FirstOrDefault();
                        
                        if(unreadStatus=="Increase")
                        {
                            if (claimToUpdate != null) claimToUpdate.ClaimReadStatus = 2;
                        }
                        else
                        {
                            if (claimToUpdate != null) claimToUpdate.ClaimReadStatus = 3;
                        }

                        _db.SaveChanges();
                    }

                    var riskClient = GetRiskClient(rc.RiskBatch.RiskClient_Id);

                    bool? reserveChanges = riskClient.RecordReserveChanges;

                    if (reserveChanges == true)
                    {
                        //if (unreadStatus == "NoChange" && (rc.ReserveChanged == 1 || rc.ReserveChanged == 2))
                        // LV= Only want to see increase in reserve
                        if (unreadStatus == "NoChange" && (rc.ReserveChanged == 1))
                        {
                            var claimToUpdate = (from x in _db.RiskClaims
                                                 where x.Id == rcr.RiskClaim_Id
                                                 select x).FirstOrDefault();

                            if (claimToUpdate != null) claimToUpdate.ClaimReadStatus = 4;

                            _db.SaveChanges();
                        }
                    }

                    rcr.PrevTotalScore             = prcr.TotalScore;
                    rcr.PrevTotalKeyAttractorCount = prcr.TotalKeyAttractorCount;
                }
            }

            var rb = GetRiskBatch(riskBatch.Id);

            var rclient = GetRiskClient(rb.RiskClient_Id);

            rcr.AmberThreshold       = rclient.AmberThreshold;
            rcr.RedThreshold         = rclient.RedThreshold;
            rcr.UploadedWhen         = rb.CreatedDate;
            rcr.ClientBatchReference = clientBatchRef;

            //rcr.ExternalServicesOutstanding = rc.ExternalServicesOutstanding;
            //rcr.ExternalServicesRequired = rc.ExternalServicesRequired;

            _db.RiskClaimRuns.Add(rcr);

            _db.SaveChanges();

            return rcr.Id;
        }

        private static string HasRiskStatusChanged(string previousRAG, string currentRAG)
        {
            string unreadStatus = "NoChange";
            
            if (previousRAG == "Low" && (currentRAG == "Medium" || currentRAG == "High"))
                unreadStatus = "Increase";

            if (previousRAG == "Medium" && currentRAG == "High")
                unreadStatus = "Increase";

            if (previousRAG == "High" && (currentRAG == "Medium" || currentRAG == "Low"))
                unreadStatus = "Decrease";

            if (previousRAG == "Medium" && currentRAG == "Low")
                unreadStatus = "Decrease";

            return unreadStatus;
        }

        private static string GetRAGStatus(RiskClaimRun rcr, int? amberRating, int? redRating)
        {
            string returnedRAG;
            if (rcr.TotalScore < amberRating)
            {
                returnedRAG = "Low";
            }
            else if (rcr.TotalScore < redRating)
            {
                returnedRAG = "Medium";
            }
            else
                returnedRAG = "High";

            return returnedRAG;
        }

        public RiskClaimRun GetLatestRiskClaimRun(int riskClaimId)
        {
            return (from x in _db.RiskClaimRuns
                    where x.RiskClaim_Id == riskClaimId && x.LatestVersion == true
                    select x).FirstOrDefault();
        }

        public void UpdateClaimRunReportOneVersion(int riskClaimRunId, string version)
        {
            var rcr = GetRiskClaimRun(riskClaimRunId); // (from x in db.RiskClaimRuns where x.Id == riskClaimRunId select x).FirstOrDefault();

            if (rcr != null)
            {
                rcr.ReportOneVersion = version;

                _db.SaveChanges();
            }
        }

        public void UpdateClaimRunReportTwoVersion(int riskClaimRunId, string version)
        {
            var rcr = GetRiskClaimRun(riskClaimRunId); // (from x in db.RiskClaimRuns where x.Id == riskClaimRunId select x).FirstOrDefault();

            if (rcr != null)
            {
                rcr.ReportTwoVersion = version;

                _db.SaveChanges();
            }
        }


        //public static void EnsureAllClaimScoresAreEncodedInXml(int riskBatchId)
        //{
        //    using (MdaDbContext db = new MdaDbContext())
        //    {
        //        var claims = GetClaimsInBatch(db, riskBatchId); //from x in db.RiskClaims where x.RiskBatch_Id == riskBatchId select x;

        //        foreach (var riskClaim in claims)
        //        {
        //            var rcr = GetLatestRiskClaimRun(db, riskClaim.Id);

        //            if (rcr != null)
        //            {
        //                if (rcr.ScoreEntityEncodeFormat == "json")
        //                {
        //                    ClaimScoreResults csr = RiskDataAccess.DeserializeScores(rcr.ScoreEntityXml, true);

        //                    rcr.ScoreEntityXml = SerializeScores(csr, false);
        //                }
        //            }
        //        }

        //        db.SaveChanges();
        //    }
        //}


        #region Scan Messages Methods

        private int ProcessMessages(int n1, int n2, int n3, string header, ListOfEntityResultMessages msgs, List<EntityScoreMessageList> lst)
        {
            if (msgs.Count > 0)
            {
                var esml = lst.Where(x => x.EntityHeader == header).FirstOrDefault();

                if (esml == null)
                {
                    esml = new EntityScoreMessageList();
                    esml.Messages = new ListOfMsgStrings();
                    esml.EntityHeader = header;

                    lst.Add(esml);
                }

                foreach (var msg in msgs)
                {
                    foreach (var msgLine in msg.LowMessageLines)
                    {
                        if (!esml.Messages.Contains(msgLine))    // SKIP identical message strings
                            esml.Messages.Add(msgLine);
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// This method scans the ScoreResults in a ClaimScoreRun and for EVERY entity it finds it makes a callback to the HandleMsgs function.
        /// This is a general purpose method that can callback to any method with the correct signature.
        /// It is used currently to scan every entity and extract the LOW level messages into a SUMMARY.
        /// </summary>
        /// <param name="riskClaimRunId">Risk Claim Run Id</param>
        /// <param name="csr">Claim Score Results structure</param>
        /// <returns></returns>
        private List<EntityScoreMessageList> GetClaimSummaryMessages(int riskClaimRunId, ClaimScoreResults csr)
        {
            return _GetClaimSummaryMessages(0, 0, 0, csr, ProcessMessages);
        }

        /// <summary>
        /// This method scans the ScoreResults in a ClaimScoreRun and for EVERY entity it finds it makes a callback to the HandleMsgs function.
        /// This is a general purpose method that can callback to any method with the correct signature.
        /// It is used currently to scan every entity and extract the LOW level messages into a SUMMARY.
        /// </summary>
        /// <param name="csr"></param>
        /// <returns></returns>
        private List<EntityScoreMessageList> GetClaimSummaryMessages(ClaimScoreResults csr)
        {
            return _GetClaimSummaryMessages(0, 0, 0, csr, ProcessMessages);
        }

        /// <summary>
        /// This method scans the ScoreResults in a ClaimScoreRun and for EVERY entity it finds it makes a callback to the HandleMsgs function.
        /// This is a general purpose method that can callback to any method with the correct signature.
        /// It is used currently to scan every entity and extract the LOW level messages into a SUMMARY.
        /// </summary>
        /// <param name="batchId">Not used by this method but passed through to the callback function</param>
        /// <param name="claimId">Not used by this method but passed through to the callback function</param>
        /// <param name="runId">Not used by this method but passed through to the callback function</param>
        /// <param name="csr">The ClaimScoreResults structure being scanned</param>
        /// <param name="HandleMsgs">the CallBack method, called for each entity in the ClaimScoreResults structure</param>
        /// <returns></returns>
        private List<EntityScoreMessageList> _GetClaimSummaryMessages(int batchId, int claimId, int runId, ClaimScoreResults csr, Func<int, int, int, string, ListOfEntityResultMessages, List<EntityScoreMessageList>, int> HandleMsgs)
        {
            List<EntityScoreMessageList> ret = new List<EntityScoreMessageList>();

            //MotorClaimIncidentRisk i = (MotorClaimIncidentRisk)csr.CompleteScoreResults; // The Incident is always top of the list

            _GetClaimSummaryMessages(csr.CompleteScoreResults, batchId, claimId, runId, ret, HandleMsgs);

            //if (i != null && i._ScoreResults != null)
            //{
            //    HandleMsgs(batchId, claimId, runId, i.ReportHeading1, i._ScoreResults.GetScoreMessages(), ret);

            //    #region Policy
            //    PolicyRisk ip = i.IncidentPolicy;

            //    if (ip != null && ip._ScoreResults != null)
            //        HandleMsgs(batchId, claimId, runId, ip.ReportHeading1, ip._ScoreResults.GetScoreMessages(), ret);
            //    #endregion

            //    #region Vehicles
            //    if (i.IncidentVehicles != null)
            //    {
            //        foreach (var v in i.IncidentVehicles)
            //        {
            //            if (v != null && v._ScoreResults != null)
            //                HandleMsgs(batchId, claimId, runId, v.ReportHeading1, v._ScoreResults.GetScoreMessages(), ret);

            //            #region People
            //            if (v.VehiclePeople != null)
            //            {
            //                foreach (var p in v.VehiclePeople)
            //                {
            //                    if (p != null && p._ScoreResults != null)
            //                        HandleMsgs(batchId, claimId, runId, p.ReportHeading1, p._ScoreResults.GetScoreMessages(), ret);

            //                    #region Person Confirmed Address
            //                    if (p.PersonsAddresses != null)
            //                    {
            //                        foreach (var pa in p.PersonsAddresses)
            //                        {
            //                            if (pa != null && pa._ScoreResults != null)
            //                                HandleMsgs(batchId, claimId, runId, pa.ReportHeading1, pa._ScoreResults.GetScoreMessages(), ret);
            //                        }
            //                    }
            //                    #endregion Person Confirmed Address

            //                    #region Person Unconfirmed Address
            //                    if (p.PersonsUnconfirmedAddresses != null)
            //                    {
            //                        foreach (var pa in p.PersonsUnconfirmedAddresses)
            //                        {
            //                            if (pa != null && pa._ScoreResults != null)
            //                                HandleMsgs(batchId, claimId, runId, pa.ReportHeading1, pa._ScoreResults.GetScoreMessages(), ret);
            //                        }
            //                    }
            //                    #endregion Person Unconfirmed Address

            //                    #region Person Orgs
            //                    if (p.PersonsOrganisations != null)
            //                    {
            //                        foreach (var po in p.PersonsOrganisations)
            //                        {
            //                            if (po != null && po._ScoreResults != null)
            //                                HandleMsgs(batchId, claimId, runId, po.ReportHeading1, po._ScoreResults.GetScoreMessages(), ret);

            //                            #region Person Orgs Addresses
            //                            if (po.OrganisationsAddresses != null)
            //                            {
            //                                foreach (var poa in po.OrganisationsAddresses)
            //                                {
            //                                    if (poa != null && poa._ScoreResults != null)
            //                                        HandleMsgs(batchId, claimId, runId, poa.ReportHeading1, poa._ScoreResults.GetScoreMessages(), ret);
            //                                }
            //                            }
            //                            #endregion Person Orgs Addresses

            //                            #region Person Orgs Vehicles
            //                            if (po.OrganisationsVehicles != null)
            //                            {
            //                                foreach (var pov in po.OrganisationsVehicles)
            //                                {
            //                                    if (pov != null && pov._ScoreResults != null)
            //                                        HandleMsgs(batchId, claimId, runId, pov.ReportHeading1, pov._ScoreResults.GetScoreMessages(), ret);
            //                                }
            //                            }
            //                            #endregion Person Orgs Vehicles
            //                        }
            //                    }
            //                    #endregion Person Orgs
            //                }
            //            }
            //            #endregion People
            //        }
            //    }
            //    #endregion Vehicles

            //    #region Incident Orgs
            //    if (i.IncidentOrganisations != null)
            //    {
            //        foreach (var io in i.IncidentOrganisations)
            //        {
            //            if (io != null && io._ScoreResults != null)
            //                HandleMsgs(batchId, claimId, runId, io.ReportHeading1, io._ScoreResults.GetScoreMessages(), ret);

            //            #region Incident Org Addresses
            //            if (io.OrganisationsAddresses != null)
            //            {
            //                foreach (var ioa in io.OrganisationsAddresses)
            //                {
            //                    if (ioa != null && ioa._ScoreResults != null)
            //                        HandleMsgs(batchId, claimId, runId, ioa.ReportHeading1, ioa._ScoreResults.GetScoreMessages(), ret);
            //                }
            //            }
            //            #endregion Incident Org Addresses

            //            #region Incident Orgs Vehicles
            //            if (io.OrganisationsVehicles != null)
            //            {
            //                foreach (var iov in io.OrganisationsVehicles)
            //                {
            //                    if (iov != null && iov._ScoreResults != null)
            //                        HandleMsgs(batchId, claimId, runId, iov.ReportHeading1, iov._ScoreResults.GetScoreMessages(), ret);
            //                }
            //            }
            //            #endregion Incident Orgs Vehicles
            //        }
            //    }
            //    #endregion Incident Orgs
            //}

            return ret;
        }

        protected virtual void _GetClaimSummaryMessages(IRiskEntity entity, int batchId, int claimId, int runId, List<EntityScoreMessageList> ret, Func<int, int, int, string, ListOfEntityResultMessages, List<EntityScoreMessageList>, int> HandleMsgs)
        {
            // If we have been passed nothing return
            if (entity == null) return;

            if (entity._ScoreResults != null)
                HandleMsgs(batchId, claimId, runId, entity.ReportHeading1, entity._ScoreResults.GetScoreMessages(), ret);

            // Now find all the properties with the SCOREABLE attribute on THIS entity
            var propList = from p in entity.GetType().GetProperties()
                           let attr = p.GetCustomAttributes(typeof(ScoreableItemAttribute), true)
                           where attr.Length == 1
                           select new { Property = p, Attribute = attr.First() as ScoreableItemAttribute };

            // Now cycle through each property
            foreach (var p in propList)
            {
                // Now check if the property is a collection.  If so score each item in the collection otherwise score the single property value
                if (p.Attribute.IsCollection)
                {
                    IEnumerable items = (IEnumerable)p.Property.GetValue(entity);   // Get the list to cycle through

                    if (items != null)
                    {
                        foreach (var item in items)  // Score each list item
                        {
                            _GetClaimSummaryMessages((IRiskEntity)item, batchId, claimId, runId, ret, HandleMsgs);
                        }
                    }
                }
                else
                {
                    var item = (IRiskEntity)p.Property.GetValue(entity);  // Get the single value

                    _GetClaimSummaryMessages((IRiskEntity)item, batchId, claimId, runId, ret, HandleMsgs);
                }
            }
        }
        #endregion

    }
}
