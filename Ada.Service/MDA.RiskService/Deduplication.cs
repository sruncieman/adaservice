﻿using MDA.Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.RiskService
{
    public partial class RiskServices
    {

        public void SaveDeduplicationTiming(long dedupeTimeInMs, SyncOrDedupeType syncOrDedupeType)
        {
            var dedupeTiming = _db.Timings.Create();

            dedupeTiming.DeduplicationTimeInMs = (int)dedupeTimeInMs;
            dedupeTiming.DeduplicationDateTime = DateTime.Now;
            dedupeTiming.DedupeType = (int)syncOrDedupeType;

            _db.Timings.Add(dedupeTiming);

            _db.SaveChanges();
        }

        public bool CheckToRunDeduplication()
        {
            System.Data.Entity.Core.Objects.ObjectParameter pp = new System.Data.Entity.Core.Objects.ObjectParameter("RunDedupe", typeof(int));

            var boolRunDeupe = _db.uspRunDedupe(pp);

            if (boolRunDeupe == 1)
                return true;
            else
                return false;
        }
    }
}
