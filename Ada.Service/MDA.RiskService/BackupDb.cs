﻿
using MDA.Common.Enum;
using System;
using System.Configuration;

namespace MDA.RiskService
{
    public partial class RiskServices
    {
         public void BackupTheDatabase(string SaveSetName, int backupType, string path, string folder)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["PerformDbBackups"]))
                return;

            //string path = ConfigurationManager.AppSettings["BackupDbPath"];
            //string folder = ConfigurationManager.AppSettings["BackupDbFolder"];

            if (path == "") path = null;
            if (folder == "") folder = null;

            string sql = "exec usp_pipeline_backup {0}, {1}, {2}, {3}";

            int? t = _db.Database.CommandTimeout;

            _db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["BackupCommandTimeout"]);

            _db.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.DoNotEnsureTransaction, sql, path,
                folder, SaveSetName, backupType);

            _db.Database.CommandTimeout = t;
        }

         public void SaveBackupTiming(long backupTimeInMs, BackupType backupType)
         {
             var backupTiming = _db.Timings.Create();

             backupTiming.BackupTimeInMs = (int)backupTimeInMs;
             backupTiming.BackupDateTime = DateTime.Now;
             backupTiming.BackupType = (int)backupType;

             _db.Timings.Add(backupTiming);

             _db.SaveChanges();
         }

    }
}
