﻿using MDA.RiskService.Model;
using System.Collections.Generic;
using System.Linq;
using MDA.DAL;
using System;



namespace MDA.RiskService
{
    public partial class RiskServices
    {
        public List<RiskRole> GetRoles()
        {
            return (_db.RiskRoles).ToList();
        }


        public int GetRoleByUserId(int userId)
        {
            int riskRoleId = (from x in _db.RiskUsers
                            join y in _db.RiskRoles on x.RiskRole_Id equals y.Id
                            where x.Id == userId
                            select y.Id).FirstOrDefault();

            return riskRoleId;
        }

        public void CreateRiskRole(int clientId, int userId, string who, string templateName)
        {
            var rr = _db.RiskRoles.Create();
            var rtUpload = _db.RiskTemplateFunctions.Create();
            var rtResults = _db.RiskTemplateFunctions.Create();

            rr.Name = templateName;

            _db.RiskRoles.Add(rr);
            _db.SaveChanges();


            rtUpload.RiskRole_Id = rr.Id;
            rtUpload.TemplateStructure_Id = 1;
            rtUpload.TemplateFunctionsJson = "[{\"name\":\"chkEnabled_1_UPLOAD\",\"value\":\"_1_UPLOAD\"},{\"name\":\"chkEnabled_5_RESULTS\",\"value\":\"_5_RESULTS\"}]";
            rtUpload.Name = "chkEnabled_1_UPLOAD";
            rtUpload.Value = "_1_UPLOAD";
            rtUpload.CreatedBy = who;
            rtUpload.ModifiedDate = DateTime.Now;

            rtResults.RiskRole_Id = rr.Id;
            rtResults.TemplateStructure_Id = 1;
            rtResults.TemplateFunctionsJson = "[{\"name\":\"chkEnabled_1_UPLOAD\",\"value\":\"_1_UPLOAD\"},{\"name\":\"chkEnabled_5_RESULTS\",\"value\":\"_5_RESULTS\"}]";
            rtResults.Name = "chkEnabled_5_RESULTS";
            rtResults.Value = "_5_RESULTS";
            rtResults.CreatedBy = who;
            rtResults.ModifiedDate = DateTime.Now;

            
            _db.RiskTemplateFunctions.Add(rtUpload);
            _db.RiskTemplateFunctions.Add(rtResults);

            _db.SaveChanges();
        }

        public void EditRiskRole(int clientId, int userId, string who, string templateName, int profileId)
        {
            var riskRole = _db.RiskRoles.Where(x => x.Id == profileId).FirstOrDefault();
            riskRole.Name = templateName;
            _db.SaveChanges();
        }

        public int DeleteRiskRole(int clientId, int userId, string who, int profileId)
        {
            // Check if any users are associated with this role first.
            // If no count remove risk role.
            var riskUsersWithRoleCount = _db.RiskUsers.Where(x => x.RiskRole_Id == profileId).Count();

            if (riskUsersWithRoleCount > 0)
                return 1;
            else
            {
                var riskRole = _db.RiskRoles.Where(x => x.Id == profileId).FirstOrDefault();
                _db.RiskRoles.Remove(riskRole);
                _db.SaveChanges();
                return 0;
            }
        }
    }
}
