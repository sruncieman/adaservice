﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Data.Entity.Core;
using MDA.RiskService.Model;
using MDA.DAL;
using Newtonsoft.Json;
using MDA.Common;
using LinqKit;
using RiskEngine.Model;
using RiskEngine.Scoring.Entities;
using RiskEngine.Scoring.Model;
using System.Data;
using System.Globalization;

namespace MDA.RiskService
{
    public partial class RiskServices
    {
        public List<string> GetFilteredClientBatchRefs(string role, int clientId, int userId, int? selectedClientId)
        {
            var batches = new List<string>();
           

            batches = (from riskBatch in _db.RiskBatches
                       where riskBatch.BatchStatus == 5 && riskBatch.VisibilityStatus == 0 && riskBatch.RiskClient_Id == clientId
                       orderby riskBatch.Id descending
                       select riskBatch.ClientBatchReference).ToList();

            return batches;
        }

       public string CheckScoring(int riskBatchId)
       {
           string scoringStatus;
           
           var countScoring = (from rc in _db.RiskClaims
                        where rc.RiskBatch_Id == riskBatchId && (rc.ClaimStatus == 7 || rc.ClaimStatus == 8)
                        select rc).Count();

           scoringStatus = countScoring > 0 ? "Scoring" : "Complete";

           return scoringStatus;
       }


       public List<ESearchDetails> GetWebSearchResults(int clientId, int userId, string who, string filterFirstName, string filterSurname, DateTime? filterDob, string filterAddressLine, string filterAddressPostcode, string filterVehicleRegNumber, int page, int pageSize, out int totalRows)
       {
           totalRows = -1;

           List<ESearchDetails> ListSearchDetails = new List<ESearchDetails>();

           string convertedDob = null;

           if (filterDob != null)
               convertedDob = String.Format("{0:dd/MM/yyyy}", filterDob); 

           System.Data.Entity.Core.Objects.ObjectParameter pp = new System.Data.Entity.Core.Objects.ObjectParameter("outputRowCount", typeof(int));

           var searchResults = _db.uspADASearch(userId, filterFirstName, filterSurname, convertedDob, filterAddressLine, filterAddressPostcode, filterVehicleRegNumber,  Convert.ToInt16(page), Convert.ToInt16(pageSize), pp).ToList();

           if (searchResults.Count > 0)
           {
               foreach (var item in searchResults)
               {
                   ListSearchDetails.Add(new ESearchDetails { ClaimType = item.Claim, Source = item.Source, Reference = item.Reference, Date = item.Date, Involvement = item.Involvement, MatchedOn = item.MatchOn, Status = item.Status, Address = item.Address, DateOfBirth = item.DateOfBirth, FirstName = item.FirstName, MiddleName = item.MiddleName, LastName = item.LastName, Postcode = item.PostCode, Title = item.Title, VehicleMake = item.VehicleMake, VehicleModel = item.VehicleModel, VehicleReg = item.VehicleRegistration });
               }
           }

           totalRows = (int)pp.Value;

           return ListSearchDetails;
       }
  

        /// <summary>
        /// Returns the claims for a given client.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="teamId"></param>
        /// <param name="userId"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="rowCount"></param>
        /// <returns>List of claims for the client</returns>
        public List<RiskBatchDetails> GetBatchesForClient(int clientId, int teamId, int userId,
                                                          string sortColumn, bool sortAscending, int page, int pageSize, out int rowCount)
        {
            rowCount = -1;

            IEnumerable<int> users = GetUsersById(clientId, null, userId);
            IQueryable<RiskBatchDetails> batches;

            if (clientId == -1)
            {
                batches = //from riskClient in db.RiskClients
                                (from riskUser in _db.RiskUsers
                                 join riskBatch in _db.RiskBatches on riskUser.Id equals riskBatch.RiskUser_Id
                                 //join riskClaim in db.RiskClaims on riskBatch.Id equals riskClaim.RiskBatch_Id
                                 where users.Contains(riskUser.Id) && riskBatch.VisibilityStatus == 0 //&& riskClaim.LatestVersion == true
                                
                                 select new RiskBatchDetails
                                 {
                                     Id = riskBatch.Id,
                                     RiskClientId = riskBatch.RiskClient_Id,
                                     BatchReference = riskBatch.BatchReference,
                                     BatchEntityType = riskBatch.BatchEntityType,
                                     ClientBatchReference = riskBatch.ClientBatchReference,
                                     BatchStatus = riskBatch.BatchStatus,
                                     CreatedDate = riskBatch.CreatedDate,
                                     CreatedBy = riskUser.FirstName + " " + riskUser.LastName,
                                     VerificationResults = riskBatch.VerificationResults,
                                     MappingResults = riskBatch.MappingResults,
                                     //TotalNumberOfClaims = (from x in db.RiskClaims where x.RiskBatch_Id == riskBatch.Id select x.Id).Count(),
                                     TotalNumberOfClaims = riskBatch.TotalClaimsReceived
                                 }).Distinct();
            }
            else
            {
                batches = //from riskClient in db.RiskClients
                                (from riskUser in _db.RiskUsers
                                 join riskBatch in _db.RiskBatches on riskUser.Id equals riskBatch.RiskUser_Id
                                 //join riskClaim in db.RiskClaims on riskBatch.Id equals riskClaim.RiskBatch_Id
                                 //where users.Contains(riskUser.Id) && riskBatch.VisibilityStatus == 0 //&& riskClaim.LatestVersion == true
                                 where riskBatch.RiskClient_Id == clientId && riskBatch.VisibilityStatus == 0
                                 select new RiskBatchDetails
                                 {
                                     Id = riskBatch.Id,
                                     RiskClientId = riskBatch.RiskClient_Id,
                                     BatchReference = riskBatch.BatchReference,
                                     BatchEntityType = riskBatch.BatchEntityType,
                                     ClientBatchReference = riskBatch.ClientBatchReference,
                                     BatchStatus = riskBatch.BatchStatus,
                                     CreatedDate = riskBatch.CreatedDate,
                                     CreatedBy = riskUser.FirstName + " " + riskUser.LastName,
                                     VerificationResults = riskBatch.VerificationResults,
                                     MappingResults = riskBatch.MappingResults,
                                     //TotalNumberOfClaims = (from x in db.RiskClaims where x.RiskBatch_Id == riskBatch.Id select x.Id).Count(),
                                     TotalNumberOfClaims = riskBatch.TotalClaimsReceived
                                 }).Distinct();
            }

            switch (sortColumn.ToUpper())
            {

                case "CLIENTBATCHREFERENCE":
                    //
                    batches = sortAscending ? batches.OrderBy(x => x.ClientBatchReference) :
                                                        batches.OrderByDescending(x => x.ClientBatchReference);
                    break;

                case "TOTALNUMBEROFCLAIMS":
                    //
                    batches = sortAscending ? batches.OrderBy(x => x.TotalNumberOfClaims) :
                                                        batches.OrderByDescending(x => x.TotalNumberOfClaims);
                    break;

                case "UPLOADEDBY":
                    //
                    batches = sortAscending ? batches.OrderBy(x => x.CreatedBy) :
                                                        batches.OrderByDescending(x => x.CreatedBy);
                    break;

                case "UPLOADDATETIME":
                    //
                    batches = sortAscending ? batches.OrderBy(x => x.CreatedDate) :
                                                        batches.OrderByDescending(x => x.CreatedDate);

                    break;

                case "STATUS":
                    //
                    batches = sortAscending ? batches.OrderBy(x => x.BatchStatus) :
                                                        batches.OrderByDescending(x => x.BatchStatus);

                    break;

                default:
                    batches = sortAscending ? batches.OrderBy(x => x.CreatedDate) :
                                                        batches.OrderByDescending(x => x.CreatedDate);
                    break;
            }

            rowCount = batches.Count();

            var batchList = batches.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return batchList.ToList();
        }
        public int GetTotalSingleClaimsForClient(int clientId,out int totalClaims)
        {
            var countSingleClaims = (from sClaims in _db.RiskClaimEditors
                                where sClaims.ClientId == clientId && sClaims.Status == "Uncommitted"
                                select sClaims).Count();

            totalClaims = countSingleClaims;

            return totalClaims;
        }

        public ERiskSingleClaimDetail FetchSingleClaimDetail(int clientId, int userId, int editClaimId)
        {
            ERiskSingleClaimDetail eRiskSingleClaimDetail = new ERiskSingleClaimDetail();

            var singleClaimDetail = (from sClaimDetail in _db.RiskClaimEditors
                                     where
                                         sClaimDetail.ClientId == clientId &&
                                         sClaimDetail.Id == editClaimId
                                     select sClaimDetail).FirstOrDefault();

            eRiskSingleClaimDetail.RiskClaimId = singleClaimDetail.Id;
            eRiskSingleClaimDetail.Claim = JsonConvert.DeserializeObject<MDA.Common.FileModel.MotorClaim>(singleClaimDetail.JSON);

            return eRiskSingleClaimDetail;
        }

        public ECueSearchEnquiryModel FetchCueInvolvements(int riskClaimId, int ClientId)
        {
            ECueSearchEnquiryModel claim = new ECueSearchEnquiryModel();
            var riskClaim = (from x in _db.RiskClaims where x.Id == riskClaimId && x.LatestVersion == true select x).First();

            claim.ClaimNumber = riskClaim.ClientClaimRefNumber;
            var incidentDt = _db.Incidents.Where(x => x.Id == riskClaim.Incident_Id).Select(y => y.IncidentDate).FirstOrDefault();
            claim.IncidentDate = incidentDt;
            claim.UploadedDate = riskClaim.CreatedDate;
           
            // Get all people on this incident 
            List<int> peopleOnIncident = (from i2p in _db.Incident2Person.AsNoTracking()
                                          where i2p.Incident_Id == riskClaim.Incident_Id && i2p.ADARecordStatus == 0
                                          select i2p.Person_Id).ToList();

            List<int> vehicleOnIncident = (from i2v in _db.Incident2Vehicle.AsNoTracking()
                                          where i2v.Incident_Id == riskClaim.Incident_Id && i2v.ADARecordStatus == 0
                                          select i2v.Vehicle_Id).ToList();

            List<ECueInvolvement> involvements = new List<ECueInvolvement>();
            
            foreach(var personId in peopleOnIncident)
            {
                var person = _db.People.Where(x => x.Id == personId).FirstOrDefault();
                var addressEntity = _db.Person2Address.Where(x => x.RiskClaim_Id == riskClaimId && x.Person_Id == personId && x.ADARecordStatus==0).FirstOrDefault();

                Address address = new Address();

                if (addressEntity != null)
                    address = _db.Addresses.Where(x => x.Id == addressEntity.Address_Id).FirstOrDefault();

                ECueInvolvement involvement = new ECueInvolvement();
                ECuePerson cuePerson = new ECuePerson();
                cuePerson.FirstName = person.FirstName;
                cuePerson.LastName = person.LastName;
                cuePerson.Dob = Convert.ToDateTime(person.DateOfBirth);
                var niNumber = _db.Person2NINumber.Where(x => x.Person_Id == personId && x.RiskClaim_Id == riskClaimId).Select(x => x.NINumber.NINumber1).FirstOrDefault();
                var drivingNumber = _db.Person2DrivingLicense.Where(x => x.Person_Id == personId && x.RiskClaim_Id == riskClaimId).Select(x => x.DrivingLicense.DriverNumber).FirstOrDefault();
                if (niNumber != null)
                    cuePerson.NI_Number = niNumber;
                if (drivingNumber != null)
                    cuePerson.DrivingLicenceNumber = drivingNumber;

                var partyType = _db.Incident2Person.Where(x => x.Incident_Id == riskClaim.Incident_Id).Select(x=>x.PartyType).FirstOrDefault();
                var subPartyType = _db.Incident2Person.Where(x => x.Incident_Id == riskClaim.Incident_Id).Select(x => x.SubPartyType).FirstOrDefault();
                string party = partyType.PartyTypeText;
                string subParty = subPartyType.SubPartyText;
                cuePerson.Involvement = party + " " + subParty;
                cuePerson.PersonId = personId;
                involvement.CuePerson = cuePerson;

                ECueAddress cueAddress = new ECueAddress();
                cueAddress.HouseName = address.Building;
                cueAddress.Number = address.BuildingNumber;
                cueAddress.Street = address.Street;
                cueAddress.Locality = address.Locality;
                cueAddress.Town = address.Town;
                cueAddress.PostCode = address.PostCode;
                
                involvement.CueAddress = cueAddress;
                involvements.Add(involvement);
            }

            if (involvements.Any())
            {
                foreach (var involvement in involvements)
                {
                    //var vehicleEntity = _db.Incident2Vehicle.Where(x => x.RiskClaim_Id == riskClaimId && x.ADARecordStatus == 0).FirstOrDefault();
                    var v2p = _db.Vehicle2Person.Where(x => x.Person_Id == involvement.CuePerson.PersonId).FirstOrDefault();
                    var v = _db.Vehicles.Where(x => x.Id  == v2p.Vehicle_Id).FirstOrDefault();
                    Vehicle vehicle = new Vehicle();
                    if (v != null)
                        vehicle = v;

                    ECueVehicle cueVehicle = new ECueVehicle();
                    cueVehicle.VehicleReg = vehicle.VehicleRegistration;
                    cueVehicle.VIN = vehicle.VIN;

                    involvement.CueVehicle = cueVehicle;
                }
            }
            else
            {
                ECueInvolvement involvement = new ECueInvolvement();
                ECueAddress cueAddress = new ECueAddress();
                ECuePerson cuePerson = new ECuePerson();
                ECueVehicle cueVehicle = new ECueVehicle();

                //var vehicleEntity = _db.Incident2Vehicle.Where(x => x.RiskClaim_Id == riskClaimId && x.ADARecordStatus == 0).FirstOrDefault();
                var v2p = _db.Vehicle2Person.Where(x => x.Person_Id == involvement.CuePerson.PersonId).FirstOrDefault();
                var v = _db.Vehicles.Where(x => x.Id == v2p.Vehicle_Id).FirstOrDefault();
                Vehicle vehicle = new Vehicle();
                if (v != null)
                    vehicle = v;

                cueVehicle.VehicleReg = vehicle.VehicleRegistration;
                cueVehicle.VIN = vehicle.VIN;

                involvement.CueVehicle = cueVehicle;
                involvement.CuePerson = cuePerson;
                involvement.CueAddress = cueAddress;

                involvements.Add(involvement);
            }

            claim.CueInvolvements = involvements;
            
            return claim;
        }

        public void DeleteSingleClaim(int clientId, int userId, int deleteId)
        {
            var singleClaimToDelete =
                _db.RiskClaimEditors.FirstOrDefault(sc => sc.Id == deleteId && sc.ClientId == clientId && sc.UserId == userId);

            _db.RiskClaimEditors.Remove(singleClaimToDelete);

            _db.SaveChanges();
        }

        public List<ERiskSingleClaim> GetSingleClaimsForClient(int clientId, int teamId, int userId, string sortColumn, bool sortAscending, int page, int pageSize, out int rowCount)
        {
            rowCount = -1;

            var singleClaims = (from sClaims in _db.RiskClaimEditors
                                where sClaims.ClientId == clientId && sClaims.Status == "Uncommitted"
                                select new ERiskSingleClaim
                                    {
                                        Id           = sClaims.Id,
                                        ClaimNumber  = sClaims.RiskClaimNumber,
                                        ClaimType    = "Motor",
                                        DateSaved    = sClaims.ModifiedDate,
                                        IncidentDate = sClaims.IncidentDate,
                                        SavedBy      = sClaims.ModifiedBy
                                    }).OrderBy(x => x.ClaimNumber);

            switch (sortColumn.ToUpper())
            {

                case "CLAIMNUMBER":
                    singleClaims = sortAscending ? singleClaims.OrderBy(x => x.ClaimNumber) :
                                                        singleClaims.OrderByDescending(x => x.ClaimNumber);
                    break;


                case "CLAIMTYPE":
                    singleClaims = sortAscending ? singleClaims.OrderBy(x => x.ClaimType) :
                                                        singleClaims.OrderByDescending(x => x.ClaimType);
                    break;

                case "INCIDENTDATE":
                    singleClaims = sortAscending ? singleClaims.OrderBy(x => x.IncidentDate) :
                                                        singleClaims.OrderByDescending(x => x.IncidentDate);
                    break;

                case "SAVEDBY":
                    singleClaims = sortAscending ? singleClaims.OrderBy(x => x.SavedBy) :
                                                        singleClaims.OrderByDescending(x => x.SavedBy);
                    break;

                case "DATESAVED":
                    singleClaims = sortAscending ? singleClaims.OrderBy(x => x.DateSaved) :
                                                        singleClaims.OrderByDescending(x => x.DateSaved);
                    break;

                default:
                    singleClaims = sortAscending ? singleClaims.OrderBy(x => x.DateSaved) :
                                                        singleClaims.OrderByDescending(x => x.DateSaved);
                    break;
            }

            rowCount = singleClaims.Count();

            var singleClaimsList = singleClaims.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return singleClaimsList.ToList();
        }

        public List<RiskClaimRunDetails> GetRiskClaimRunDetailsInBatch(int riskBatchId)
        {
            var claims = from riskClient in _db.RiskClients
                            join riskBatch  in _db.RiskBatches on riskClient.Id equals riskBatch.RiskClient_Id
                            join riskClaim  in _db.RiskClaims on riskBatch.Id equals riskClaim.RiskBatch_Id
                            join incident   in _db.Incidents on riskClaim.Incident_Id equals incident.Id
                            join riskScore  in _db.RiskClaimRuns on riskClaim.Id equals riskScore.RiskClaim_Id
                            where riskBatch.Id == riskBatchId
                            select new RiskClaimRunDetails
                            {
                                RiskClaim_Id                = riskClaim.Id,
                                MDAClaimRef                 = riskClaim.MdaClaimRef,
                                BaseRiskClaim_Id            = riskClaim.BaseRiskClaim_Id,
                                CreatedDate                 = riskClaim.CreatedDate,
                                CreatedBy                   = riskClaim.CreatedBy,
                                ClientClaimRefNumber        = riskClaim.ClientClaimRefNumber,
                                LevelOneRequestedCount      = riskClaim.LevelOneRequestedCount,
                                LevelOneRequestedWhen       = riskClaim.LevelOneRequestedWhen,
                                LevelOneRequestedWho        = riskClaim.LevelOneRequestedWho,
                                LevelTwoRequestedCount      = riskClaim.LevelTwoRequestedCount,
                                LevelTwoRequestedWhen       = riskClaim.LevelTwoRequestedWhen,
                                LevelTwoRequestedWho        = riskClaim.LevelTwoRequestedWho,
                                ClaimStatus                 = riskClaim.ClaimStatus,
                                ClaimReadStatus             = riskClaim.ClaimReadStatus,
                                RiskBatch_Id                = riskClaim.RiskBatch.Id,
                                BatchStatus                 = riskClaim.RiskBatch.BatchStatus,
                                BatchReference              = riskClaim.RiskBatch.BatchReference,
                                ClientBatchReference        = riskClaim.RiskBatch.ClientBatchReference,
                                Incident_Id                 = riskClaim.Incident.Id,
                                IncidentDate                = riskClaim.Incident.IncidentDate,
                                PaymentsToDate              = riskClaim.Incident.PaymentsToDate,
                                Reserve                     = riskClaim.Incident.Reserve,
                                RiskClaimRun_Id             = riskScore.Id,
                                TotalKeyAttractorCount      = riskScore.TotalKeyAttractorCount,
                                TotalScore                  = riskScore.TotalScore,
                                ScoreEntityEncodeFormat     = riskScore.ScoreEntityEncodeFormat,
                                ScoreXml                    = riskScore.ScoreEntityXml,
                                SummaryMessagesEncodeFormat = riskScore.SummaryMessagesEncodeFormat,
                                SummaryMessagesXml          = riskScore.SummaryMessagesXml,
                                RedThreshold                = riskClient.RedThreshold,
                                AmberThreshold              = riskClient.AmberThreshold,
                                CleansingResults            = riskClaim.CleansingResults,
                                ValidationResults           = riskClaim.ValidationResults,

                            };

            return claims.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="teamId"></param>
        /// <param name="filterUserId"></param>
        /// <param name="FilterUploadedBy"></param>
        /// <param name="FilterBatchRef"></param>
        /// <param name="FilterClaimNumber"></param>
        /// <param name="FilterStartDate"></param>
        /// <param name="FilterEndDate"></param>
        /// <param name="FilterHighRisk"></param>
        /// <param name="FilterMediumRisk"></param>
        /// <param name="FilterLowRisk"></param>
        /// <param name="FilterKeoghsCFS"></param>
        /// <param name="FilterPreviousVersion"></param>
        /// <param name="FilterError"></param>
        /// <param name="FilterNoReportsRequested"></param>
        /// <param name="FilterLevel1ReportsRequested"></param>
        /// <param name="FilterLevel2ReportsRequested"></param>
        /// <param name="FilterCallbackReportsRequested"></param>
        /// <param name="SortColumn"></param>
        /// <param name="SortAscending"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        public List<RiskClaimRunDetails> GetRiskClaimRunDetailsForClient(int clientId, int teamId, int filterUserId,
                                string FilterUploadedBy, string[] FilterBatchRef, string FilterClaimNumber, DateTime? FilterStartDate,
                                DateTime? FilterEndDate, bool FilterHighRisk, bool FilterMediumRisk, bool FilterLowRisk, bool FilterKeoghsCFS, 
                                bool FilterPreviousVersion, bool FilterError,  bool FilterStatusUnrequestedReports,
                                bool FilterStatusAvailableReports, bool FilterStatusPendingReports, bool FilterStatusRead, bool FilterStatusUnread,
                                bool FilterStatusUnreadScoreChanges, bool FilterStatusReserveChange, bool FilterNoReportsRequested, bool FilterLevel1ReportsRequested,
                                bool FilterLevel2ReportsRequested, bool FilterCallbackReportsRequested, bool FilterNotes, bool FilterIntel, int?[] FilterDecisionId, bool FilterDecisions,
                                string SortColumn, bool SortAscending, int page, int pageSize, out int rowCount)
        {
          
            
            rowCount = -1;

            int? userType = FindRiskUserByName(_ctx.Who).UserType;

                //var claims = from riskUser in _db.RiskUsers
                //             join riskBatch in _db.RiskBatches on riskUser.Id equals riskBatch.RiskUser_Id
                //             join riskClient in _db.RiskClients on riskBatch.RiskClient_Id equals riskClient.Id
                //             join riskClaim in _db.RiskClaims on riskBatch.Id equals riskClaim.RiskBatch_Id
                //             join incident in _db.Incidents on riskClaim.Incident_Id equals incident.Id
                //             join riskClaimRuns in _db.RiskClaimRuns on riskClaim.Id equals riskClaimRuns.RiskClaim_Id into j1
                //             from leftriskClaimRuns in j1.DefaultIfEmpty()
                //             where riskUser.RiskClient_Id == clientId && riskBatch.BatchStatus == 5 && riskClaim.ClaimStatus < 10

                    var claims = from riskBatch in _db.RiskBatches
                                 join riskClient in _db.RiskClients on riskBatch.RiskClient_Id equals riskClient.Id
                                 join riskClaim in _db.RiskClaims on riskBatch.Id equals riskClaim.RiskBatch_Id
                                 join incident in _db.Incidents on riskClaim.Incident_Id equals incident.Id
                                 join riskClaimRuns in _db.RiskClaimRuns on riskClaim.Id equals riskClaimRuns.RiskClaim_Id into j1
                                 from leftriskClaimRuns in j1.DefaultIfEmpty()
                         where riskBatch.RiskClient_Id == clientId && riskBatch.BatchStatus == 5 && riskClaim.ClaimStatus < 10

                             select new  RiskClaimRunDetails
                             {

                                   RiskClaim_Id                                 = riskClaim.Id,
                                   MDAClaimRef                                  = riskClaim.MdaClaimRef,
                                   BaseRiskClaim_Id                             = riskClaim.BaseRiskClaim_Id,
                                   CreatedDate                                  = riskClaim.CreatedDate,
                                   CreatedBy                                    = riskClaim.CreatedBy,
                                   ClientClaimRefNumber                         = riskClaim.ClientClaimRefNumber,
                                   LevelOneRequestedCount                       = riskClaim.LevelOneRequestedCount,
                                   LevelOneRequestedWhen                        = riskClaim.LevelOneRequestedWhen,
                                   LevelOneRequestedWho                         = riskClaim.LevelOneRequestedWho,
                                   LevelTwoRequestedCount                       = riskClaim.LevelTwoRequestedCount,
                                   LevelTwoRequestedWhen                        = riskClaim.LevelTwoRequestedWhen,
                                   LevelTwoRequestedWho                         = riskClaim.LevelTwoRequestedWho,
                                   CallbackRequested                            = riskClaim.CallbackRequested,
                                   CallbackRequestedWhen                        = riskClaim.CallbackRequestedWhen,
                                   CallbackRequestedWho                         = riskClaim.CallbackRequestedWho,
                                   ClaimStatus                                  = riskClaim.ClaimStatus,
                                   ClaimReadStatus                              = riskClaim.ClaimReadStatus,
                                   RiskBatch_Id                                 = riskClaim.RiskBatch.Id,
                                   BatchStatus                                  = riskClaim.RiskBatch.BatchStatus,
                                   BatchReference                               = riskClaim.RiskBatch.BatchReference,
                                   ClientBatchReference                         = riskClaim.RiskBatch.ClientBatchReference,
                                   Incident_Id                                  = riskClaim.Incident.Id,
                                   IncidentDate                                 = riskClaim.Incident.IncidentDate,
                                   PaymentsToDate                               = _db.Incident2Person.Where(i2p => i2p.Incident_Id == incident.Id && i2p.PartyType_Id == (int)MDA.Common.Enum.PartyType.Insured && i2p.SubPartyType_Id == (int)MDA.Common.Enum.SubPartyType.Driver && i2p.ADARecordStatus == 0).Select(i2p => i2p.PaymentsToDate).FirstOrDefault(),
                                   Reserve                                      = _db.Incident2Person.Where(i2p => i2p.Incident_Id == incident.Id && i2p.PartyType_Id == (int)MDA.Common.Enum.PartyType.Insured && i2p.SubPartyType_Id == (int)MDA.Common.Enum.SubPartyType.Driver && i2p.ADARecordStatus == 0).Select(i2p => i2p.Reserve).FirstOrDefault(),
                                   RiskClaimRun_Id                              = leftriskClaimRuns.Id == null ? 0 : leftriskClaimRuns.Id,
                                   TotalKeyAttractorCount                       = 0,
                                   TotalScore                                   = leftriskClaimRuns.TotalScore != null ? leftriskClaimRuns.TotalScore : 0,
                                   ScoreEntityEncodeFormat                      = leftriskClaimRuns.ScoreEntityEncodeFormat ?? "",
                                   ScoreXml                                     = leftriskClaimRuns.ScoreEntityXml ?? "",
                                   SummaryMessagesEncodeFormat                  = leftriskClaimRuns.SummaryMessagesEncodeFormat ?? "",
                                   SummaryMessagesXml                           = leftriskClaimRuns.SummaryMessagesXml ?? "",
                                   RedThreshold                                 = riskClient.RedThreshold,
                                   AmberThreshold                               = riskClient.AmberThreshold,
                                   CleansingResults                             = riskClaim.CleansingResults,
                                   ValidationResults                            = riskClaim.ValidationResults,

                                   LevelOneRequestedStatus =
                                       riskClaim.LevelOneUnavailable == true
                                           ? 3
                                           : riskClaim.LevelOneRequestedCount == 0
                                                 ? 0
                                                 : riskClaim.LevelOneRequestedCount > 0 &&
                                                   DateTime.Now <
                                                   System.Data.Entity.DbFunctions.AddMinutes(
                                                       riskClaim.LevelOneRequestedWhen,
                                                       riskClient.LevelOneReportDelay)
                                                       ? 1
                                                       : riskClaim.LevelOneRequestedCount > 0 &&
                                                         DateTime.Now >
                                                         System.Data.Entity.DbFunctions.AddMinutes(
                                                             riskClaim.LevelOneRequestedWhen,
                                                             riskClient.LevelOneReportDelay)
                                                             ? 2
                                                             : -1,
                                   LatestScored                                 = riskClaim.LatestScored,
                                   LatestVersion                                = riskClaim.LatestVersion,
                                   NoteAdded =     _db.RiskNotes.Where(rn => rn.BaseRiskClaim_Id == riskClaim.BaseRiskClaim_Id && rn.Note != null && rn.Visibility == 1 && rn.Deleted == false).Any() || _db.RiskNotes.Where(rn => rn.BaseRiskClaim_Id == riskClaim.BaseRiskClaim_Id && rn.Note != null && rn.Visibility == 2 && rn.UserType == userType && rn.Deleted == false).Any(),
                                   IntelAdded =    _db.RiskNotes.Where(rn => rn.BaseRiskClaim_Id == riskClaim.BaseRiskClaim_Id && rn.Visibility == 1 && rn.Deleted == false && rn.RiskNoteFiles.Any()).Any() || _db.RiskNotes.Where(rn => rn.BaseRiskClaim_Id == riskClaim.BaseRiskClaim_Id && rn.Visibility == 2 && rn.UserType == userType && rn.Deleted == false && rn.RiskNoteFiles.Any()).Any(),
                                   DecisionAdded = _db.RiskNotes.Where(rn => rn.BaseRiskClaim_Id == riskClaim.BaseRiskClaim_Id && rn.DecisionId != null && rn.Visibility == 1 && rn.Deleted == false).Any() || _db.RiskNotes.Where(rn => rn.BaseRiskClaim_Id == riskClaim.BaseRiskClaim_Id && rn.DecisionId != null && rn.Visibility == 2 && rn.UserType == userType && rn.Deleted == false).Any(),
                                   ReserveChanged                               = riskClient.RecordReserveChanges == true ? riskClaim.ReserveChanged : 0,
                                   DecisionId = _db.RiskNotes.Where(rn => rn.BaseRiskClaim_Id == riskClaim.BaseRiskClaim_Id && rn.DecisionId != null && rn.Visibility == 1 && rn.Deleted == false).Select(rn => rn).OrderByDescending(n => n.Id).Select(rn => rn.DecisionId).FirstOrDefault(),

                             };


                if (SortColumn == null)
                    SortColumn = "";


                switch (SortColumn.ToUpper())
                {
                    case "INCIDENTDATE":
                        claims = SortAscending
                                     ? claims.OrderBy(x => x.IncidentDate)
                                     .ThenBy(y=>y.ClaimStatus)
                                     .ThenBy(y => y.TotalScore)
                                     : claims.OrderByDescending(x => x.IncidentDate)
                                     .ThenByDescending(y => y.ClaimStatus)
                                     .ThenByDescending(y => y.TotalScore);
                        break;
                    case "CLIENTCLAIMREFNUMBER":
                        claims = SortAscending
                                     ? claims.OrderBy(x => x.ClientClaimRefNumber)
                                     .ThenBy(y => y.ClaimStatus)
                                     .ThenBy(y => y.TotalScore)
                                     : claims.OrderByDescending(x => x.ClientClaimRefNumber)
                                     .ThenByDescending(y => y.ClaimStatus)
                                     .ThenByDescending(y => y.TotalScore);
                        break;
                    case "CLIENTBATCHREFERENCE":
                        claims = SortAscending
                                     ? claims.OrderBy(x => x.ClientBatchReference)
                                     .ThenBy(y => y.ClaimStatus)
                                     .ThenBy(y => y.TotalScore)
                                     : claims.OrderByDescending(x => x.ClientBatchReference)
                                     .ThenByDescending(y => y.ClaimStatus)
                                     .ThenByDescending(y => y.TotalScore);
                        break;
                    case "RESERVE":
                        claims = SortAscending
                                     ? claims.OrderBy(x => x.Reserve)
                                     .ThenBy(y => y.ClaimStatus)
                                     .ThenBy(y => y.TotalScore)
                                     : claims.OrderByDescending(x => x.Reserve)
                                     .ThenByDescending(y => y.ClaimStatus)
                                     .ThenByDescending(y => y.TotalScore);
                        break;
                    case "PAYMENTSTODATE":
                        claims = SortAscending
                                     ? claims.OrderBy(x => x.PaymentsToDate)
                                     .ThenBy(y => y.ClaimStatus)
                                     .ThenBy(y => y.TotalScore)
                                     : claims.OrderByDescending(x => x.PaymentsToDate)
                                     .ThenByDescending(y => y.ClaimStatus)
                                     .ThenByDescending(y => y.TotalScore);
                        
                        break;
                    case "UPLOADDATETIME":
                        claims = SortAscending
                                     ? claims.OrderBy(x => x.CreatedDate)
                                      .ThenBy(y => y.ClaimStatus)
                                     .ThenBy(y => y.TotalScore)
                                     : claims.OrderByDescending(x => x.CreatedDate)
                                     .ThenByDescending(y => y.ClaimStatus)
                                     .ThenByDescending(y => y.TotalScore);

                        break;

                    case "STATUS":
                        claims = SortAscending
                                     ? claims.OrderBy(x => x.ClaimReadStatus)
                                     .ThenBy(y => y.ClaimStatus)
                                     .ThenBy(y => y.TotalScore)
                                     : claims.OrderByDescending(x => x.ClaimReadStatus)
                                     .ThenByDescending(y => y.ClaimStatus)
                                     .ThenByDescending(y => y.TotalScore);

                        break;
                    default:
                        claims = SortAscending
                                     ? claims.OrderBy(x=>x.ClaimStatus)
                                     .ThenBy(x => x.TotalScore).ThenBy(x => x.CreatedDate)
                                     : claims.OrderByDescending(x=>x.ClaimStatus)
                                     .ThenByDescending(x => x.TotalScore)
                                     .ThenByDescending(x => x.CreatedDate);
                                       
                        break;
                }

                if (!string.IsNullOrEmpty(FilterUploadedBy))
                    claims = claims.Where(x => x.CreatedBy == FilterUploadedBy);

                if (!string.IsNullOrEmpty(FilterClaimNumber))
                    claims = claims.Where(x => x.ClientClaimRefNumber == FilterClaimNumber);

                //if (FilterBatchRef.Count > 0)
                    claims = claims.Where(x => FilterBatchRef.Contains(x.ClientBatchReference));
                    //claims = claims.Where(x => x.ClientBatchReference == FilterBatchRef);

                if (FilterStartDate != null)
                    claims = claims.Where(x => x.CreatedDate >= FilterStartDate);
             
                if (FilterEndDate != null)
                    claims = claims.Where(x => x.CreatedDate < FilterEndDate);

                if (FilterNotes == true)
                    claims = claims.Where(x => x.NoteAdded == true);

                if (FilterIntel == true)
                    claims = claims.Where(x => x.IntelAdded == true);

                if (FilterDecisions == true)
                    claims = claims.Where(x => x.DecisionAdded == true);

                if (FilterDecisionId != null)
                    claims = claims.Where(x => FilterDecisionId.Contains(x.DecisionId));


                var inner = PredicateBuilder.False<RiskClaimRunDetails>();
                var outer = PredicateBuilder.True<RiskClaimRunDetails>();

                List<int> lstFilteredStatus = new List<int>();
                lstFilteredStatus.Add(-1); // FailedToCreate
                lstFilteredStatus.Add(-2); // FailedToLoad
                lstFilteredStatus.Add(-3); // FailedOnExternalService
                lstFilteredStatus.Add(-4); // FailedToScore
                lstFilteredStatus.Add(-5); // FailedOnCleansing
                lstFilteredStatus.Add(-6); // FailedOnValidation
                lstFilteredStatus.Add(-7); // FailedAsMemberOfBadBatch

               if (FilterHighRisk)
                    inner = inner.Or(x => x.TotalScore >= x.RedThreshold && x.ClaimStatus == 9 && x.LatestVersion); 

               if (FilterMediumRisk)
                    inner = inner.Or(x => x.TotalScore >= x.AmberThreshold && x.TotalScore < x.RedThreshold && x.ClaimStatus == 9 && x.LatestVersion);

               if(FilterLowRisk)
                    inner = inner.Or(x => x.TotalScore < x.AmberThreshold && x.ClaimStatus == 9 && x.LatestVersion);

               if(FilterKeoghsCFS)
                   inner = inner.Or(x => x.ClaimStatus == -8 && x.LatestVersion);
                
               if(FilterPreviousVersion)
                   inner = inner.Or(x => x.LatestVersion == false);

               if (FilterError)
                   inner = inner.Or(x => lstFilteredStatus.Contains(x.ClaimStatus) && x.LatestVersion);


               if(FilterStatusUnread)
               {
                   if (!FilterStatusRead && !FilterStatusUnreadScoreChanges && !FilterStatusReserveChange)
                       outer = outer.And(x => x.ClaimReadStatus == 0); // Unread Only

                   else if (FilterStatusRead && !FilterStatusUnreadScoreChanges && !FilterStatusReserveChange)
                       outer = outer.And(x => x.ClaimReadStatus == 1 || x.ClaimReadStatus == 0); // Unread And Read

                   else if (FilterStatusUnreadScoreChanges && !FilterStatusRead && !FilterStatusReserveChange)
                       outer = outer.And(x => x.ClaimReadStatus == 0 || x.ClaimReadStatus > 1); // Unread And FilterStatusUnreadScoreChange

                   else if (FilterStatusReserveChange && !FilterStatusUnreadScoreChanges && !FilterStatusRead && !FilterStatusReserveChange)
                       outer = outer.And(x => x.ReserveChanged == 1  || x.ClaimReadStatus == 0); // Unread And FilterStatusReserveChange
               }

               else if (FilterStatusRead)
               {
                   if (!FilterStatusUnread && !FilterStatusUnreadScoreChanges && !FilterStatusReserveChange)
                       outer = outer.And(x => x.ClaimReadStatus == 1); // Read Only

                   else if (FilterStatusUnreadScoreChanges && !FilterStatusUnread && !FilterStatusReserveChange)
                       outer = outer.And(x => x.ClaimReadStatus == 1 || x.ClaimReadStatus > 1); // Read And UnreadScoreChange

                   else if (FilterStatusReserveChange && !FilterStatusUnread && !FilterStatusUnreadScoreChanges)
                       outer = outer.And(x => x.ReserveChanged == 1 || x.ClaimReadStatus == 1 ); // Read And ReserveChange Increment
               }

               else if (FilterStatusUnreadScoreChanges)
               {
                   if (!FilterStatusUnread && !FilterStatusUnreadScoreChanges && !FilterStatusRead)
                       outer = outer.And(x => x.ReserveChanged == 1); // Reserve Change Increment Only

                   else if (FilterStatusReserveChange && !FilterStatusUnreadScoreChanges && !FilterStatusReserveChange)
                       outer = outer.And(x => x.ReserveChanged == 1 || x.ClaimReadStatus > 1); // FilterStatusUnreadScoreChanges And FilterStatusReserveChange
               }

               else if (FilterStatusReserveChange)
               {
                   if (!FilterStatusUnread && !FilterStatusRead && !FilterStatusUnreadScoreChanges)
                        outer = outer.And(x => x.ReserveChanged == 1); // Reserve Change Increment Only
               }

               outer = outer.And(inner.Expand());

               claims = claims.AsExpandable().Where(outer);

               rowCount = claims.Count();

               List<RiskClaimRunDetails> claimList = claims.Skip((page - 1) * pageSize).Take(pageSize).ToList();

               return claimList.Select(claim => AddMessageList(claim)).ToList();
        }
       

        public ERiskClaimEdit GetSingleClaimEditDetailsForUser(int userId, int editClaimId)
        {
            var singleEditClaim = (from riskClaimEdit in _db.RiskClaimEditors
                                    where riskClaimEdit.UserId == userId && riskClaimEdit.Id == editClaimId
                                    select new ERiskClaimEdit
                                    {

                                    }).FirstOrDefault();

            return singleEditClaim;
        }

        public List<ERiskClaimEdit> GetClaimEditDetailsForUser(int userId)
        {
            var claimsEdit = from riskClaimEdit in _db.RiskClaimEditors
                                where riskClaimEdit.UserId == userId
                                select new ERiskClaimEdit
                                {

                                };


            return claimsEdit.ToList();
        }


        public void SaveTemplateFunctions(string userName, int roleId, int templateId, string templateFunctionsJson, System.Collections.Generic.List<System.Collections.Generic.Dictionary<string, string>> ListTemplateFunctionsJson)
        {
            
            // Remove Existing template functions for role selected
            var existingTemplateFunctions = _db.RiskTemplateFunctions.Where(x => x.RiskRole_Id == roleId).ToList();

            foreach(var templateFunction in existingTemplateFunctions){
                _db.RiskTemplateFunctions.Remove(templateFunction);
            }

            foreach (var item in ListTemplateFunctionsJson)
            {
                RiskTemplateFunction rtf = _db.RiskTemplateFunctions.Create();
                
                rtf.CreatedBy = userName;
                rtf.RiskRole_Id = roleId;
                rtf.TemplateStructure_Id = templateId;
                rtf.TemplateFunctionsJson = templateFunctionsJson;
                rtf.Name = item["name"];
                rtf.Value = item["value"];
                rtf.ModifiedDate = DateTime.Now;

                _db.RiskTemplateFunctions.Add(rtf);
            }

            _db.SaveChanges();
        }


        public string LoadTemplateFunctions(string userName, int roleId, int clientId, int userId)
        {
            var strTemplateFunctionsJson =  _db.RiskTemplateFunctions.Where(x => x.RiskRole_Id == roleId).Select(x => x.TemplateFunctionsJson).FirstOrDefault();

            return strTemplateFunctionsJson;
        }

        public int SaveSingleClaim(int? editClaimId, MDA.Common.FileModel.IClaim claim, string claimType, int clientId, int userId, DateTime incidentDate, string modifiedBy, DateTime modifiedDate, string riskClaimNumber,string status, out int editClaimIdSaved)
        {
            bool useJson = Convert.ToBoolean(ConfigurationManager.AppSettings["UseJsonForClaims"]);

            string xml = null;

            if (!useJson)
            {
                MemoryStream ms = new MemoryStream();

                DataContractSerializer ser = new DataContractSerializer(claim.GetType());
                ser.WriteObject(ms, claim);
                ms.Position = 0;
                var sr = new StreamReader(ms);
                xml = sr.ReadToEnd();
            }
            else
            {
                xml = JsonConvert.SerializeObject(claim, Newtonsoft.Json.Formatting.None);
            }


            var editSingleClaim = _db.RiskClaimEditors.FirstOrDefault(x => x.RiskClaimNumber == claim.ClaimNumber);

            if (editSingleClaim != null)
            {                  
                editSingleClaim.JSON            = xml;
                editSingleClaim.ClientId        = clientId;
                editSingleClaim.UserId          = userId;
                editSingleClaim.IncidentDate    = incidentDate;
                editSingleClaim.ModifiedBy      = modifiedBy;
                editSingleClaim.ModifiedDate    = modifiedDate;
                editSingleClaim.ClaimType       = claimType;
                editSingleClaim.RiskClaimNumber = riskClaimNumber;
                editSingleClaim.Status          = status;

                _db.SaveChanges();

                editClaimIdSaved = editSingleClaim.Id;
            }

            else
            {
                RiskClaimEditor riskClaimEditor = _db.RiskClaimEditors.Create();

                riskClaimEditor.JSON            = xml;
                riskClaimEditor.ClientId        = clientId;
                riskClaimEditor.UserId          = userId;
                riskClaimEditor.IncidentDate    = incidentDate;
                riskClaimEditor.ModifiedBy      = modifiedBy;
                riskClaimEditor.ModifiedDate    = modifiedDate;
                riskClaimEditor.ClaimType       = claimType;
                riskClaimEditor.RiskClaimNumber = riskClaimNumber;
                riskClaimEditor.Status          = status;

                _db.RiskClaimEditors.Add(riskClaimEditor);

                _db.SaveChanges();

                editClaimIdSaved = riskClaimEditor.Id;
            }

                
            return editClaimIdSaved;
        }

        private RiskClaimRunDetails AddMessageList(RiskClaimRunDetails oExtendedClaimsInBatchDetails)
        {

            if (oExtendedClaimsInBatchDetails.TotalScore == null)
                oExtendedClaimsInBatchDetails.TotalScore = 0;

            
            bool useJson = oExtendedClaimsInBatchDetails.SummaryMessagesEncodeFormat == "json";

            if (oExtendedClaimsInBatchDetails.SummaryMessagesXml == null)
            {
                ClaimScoreResults csr = RiskServices.DeserializeScores(oExtendedClaimsInBatchDetails.ScoreXml, useJson);

                oExtendedClaimsInBatchDetails.MessageList = GetClaimSummaryMessages(oExtendedClaimsInBatchDetails.RiskClaimRun_Id, csr);
            }
            else
            {
                if (oExtendedClaimsInBatchDetails.SummaryMessagesXml!="")
                oExtendedClaimsInBatchDetails.MessageList = DeserializeListEntityScoreMessages(oExtendedClaimsInBatchDetails.SummaryMessagesXml, useJson);
            }

            return oExtendedClaimsInBatchDetails;
        }

       
    }
}
