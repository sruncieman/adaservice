﻿
using System;
using System.Linq;


namespace MDA.RiskService
{
    public partial class RiskServices
    {

        public byte[] GetRiskReport(int riskReportId)
        {
            return (from x in _db.RiskReports
                        where x.PkId == riskReportId
                        select x.FileData).FirstOrDefault();
        }



        public int SaveRiskReportIntoRiskReportTable(byte[] reportBytes, string reportData,  int? parentEntityId, string description)
        {
            var riskReport = _db.RiskReports.Create();

            riskReport.Id = Guid.NewGuid();
            riskReport.Description = description;
            riskReport.FileData = reportBytes;
            riskReport.ParentEntity_Id = parentEntityId;
            riskReport.ReturnedData = reportData;

            _db.RiskReports.Add(riskReport);

            _db.SaveChanges();

            return riskReport.PkId;
        }
    }
}
