﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using MDA.RiskService.Model;
using MDA.DAL;
using Newtonsoft.Json;
using MDA.Common;

using RiskEngine.Scoring.Model;
using System.Data.Entity.Core;
using MDA.Pipeline.Model;

namespace MDA.RiskService
{
    public partial class RiskServices
    {
        public static bool ClaimsIdentical(MDA.Pipeline.Model.IPipelineClaim a, MDA.Pipeline.Model.IPipelineClaim b)
        {
            string xml1 = XmlSerializeClaim(a);
            string xml2 = XmlSerializeClaim(b);

            return string.Compare(xml1, xml2, false) == 0;
        }

        public MDA.Pipeline.Model.IPipelineClaim RetrieveTheIClaim(RiskClaim riskClaim)
        {
            bool useJson = riskClaim.SourceEntityEncodeFormat == "json";

            return DeserializeIClaim(riskClaim.SourceEntityXML, useJson, riskClaim.EntityClassType);
        }


        public bool FindExistingIdenticalClaim(int clientId, MDA.Pipeline.Model.IPipelineClaim pipelineClaim)
        {
            bool useJson = Convert.ToBoolean(ConfigurationManager.AppSettings["UseJsonForClaims"]);

            var riskClaim = FindLatestClaim(clientId, pipelineClaim.ClaimNumber);

            if (riskClaim != null)
            {

                IPipelineClaim oldClaim = null;

                oldClaim = DeserializeIClaim(riskClaim.SourceEntityXML, useJson, riskClaim.EntityClassType);

                if (oldClaim != null)
                {
                    return ClaimsIdentical(oldClaim, pipelineClaim);
                }
                
                
            }
            return false;
        }

        public RiskClaim FindExistingBaseClaim(int clientId, string refNumber)
        {
            var c = from x in _db.RiskClaims
                    where x.ClientClaimRefNumber == refNumber && x.RiskBatch.RiskClient_Id == clientId
                    orderby x.Id ascending
                    select x;

            return c.FirstOrDefault();
        }
        

        public int CountRiskClaimStatusForBatch(int riskBatchId, RiskClaimStatus claimStatusToCount)
        {
            return _db.RiskClaims.Where(x => x.RiskBatch_Id == riskBatchId && x.ClaimStatus == (int)claimStatusToCount).Count();
        }

        public RiskClaim GetRiskClaim(int riskClaimId)
        {
            return (from x in _db.RiskClaims
                    where x.Id == riskClaimId
                    select x).FirstOrDefault();
        }

        public RiskClaim GetPreviousRiskClaimVersion(RiskClaim rc)
        {
            return (from x in _db.RiskClaims
                    where x.BaseRiskClaim_Id == rc.BaseRiskClaim_Id && x.LatestVersion == false
                    orderby x.Id descending
                    select x).FirstOrDefault();
        }



        //public RiskClaimToProcess GetAClaimToScore()
        //{
        //    RiskClaimToProcess ret = new RiskClaimToProcess();
        //    bool done = false;

        //    while (!done)
        //    {
        //        using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
        //        {
        //            try
        //            {
        //                DateTime timeout = DateTime.Now.AddMinutes(-10);

        //                ret.RiskClaim = (from x in _db.RiskClaims
        //                                 where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForScoring
        //                                 || (x.ClaimStatus == (int)RiskClaimStatus.BeingScored && x.ClaimStatusLastModified <= timeout))
        //                                 && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
        //                                 select x).FirstOrDefault();

        //                if (ret.RiskClaim != null)
        //                {
        //                    ret.RiskClaim.ClaimStatus = (int)RiskClaimStatus.BeingScored;
        //                    ret.RiskClaim.ClaimStatusLastModified = DateTime.Now;

        //                    try
        //                    {
        //                        _db.SaveChanges();

        //                        ret.RiskClient = ret.RiskClaim.RiskBatch.RiskClient;

        //                        transaction.Commit();

        //                        done = true;
        //                    }
        //                    catch (OptimisticConcurrencyException)
        //                    {
        //                        ret = new RiskClaimToProcess();

        //                        // and go around again
        //                    }
        //                }
        //                else done = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                throw ex;
        //            }
        //        }
        //    }
        //    return ret;
        //}

        //public RiskClaimToProcess GetAClaimToScore(int riskClaimId)
        //{
        //    RiskClaimToProcess ret = new RiskClaimToProcess();
        //    bool done = false;

        //    while (!done)
        //    {
        //        using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
        //        {
        //            try
        //            {
        //                DateTime timeout = DateTime.Now.AddMinutes(-10);

        //                ret.RiskClaim = (from x in _db.RiskClaims
        //                                 where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForScoring
        //                                 || (x.ClaimStatus == (int)RiskClaimStatus.BeingScored && x.ClaimStatusLastModified <= timeout))
        //                                 && x.Id == riskClaimId
        //                                 && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
        //                                 select x).FirstOrDefault();

        //                if (ret.RiskClaim != null)
        //                {
        //                    try
        //                    {
        //                        ret.RiskClaim.ClaimStatus = (int)RiskClaimStatus.BeingScored;
        //                        ret.RiskClaim.ClaimStatusLastModified = DateTime.Now;

        //                        _db.SaveChanges();

        //                        ret.RiskClient = ret.RiskClaim.RiskBatch.RiskClient;

        //                        transaction.Commit();

        //                        done = true;
        //                    }
        //                    catch (OptimisticConcurrencyException)
        //                    {
        //                        ret = new RiskClaimToProcess();

        //                        // and go around again
        //                    }
        //                }
        //                else done = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                throw ex;
        //            }
        //        }
        //    }
        //    return ret;

        //}

        public RiskClaimsToProcess GetClaimsToScoreFromBatch(int? batchId, bool scoreDirect)
        {
            RiskClaimsToProcess ret = new RiskClaimsToProcess();
            bool done = false;

            while (!done)
            {
                using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                {
                    try
                    {
                        DateTime timeout = DateTime.Now;//.AddDays(-30);

                        if (batchId != null)
                            ret.RiskClaims = (from x in _db.RiskClaims
                                              where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForScoring
                                              || (x.ClaimStatus == (int)RiskClaimStatus.BeingScored && x.ClaimStatusLastModified <= timeout))
                                              && x.RiskBatch_Id == batchId && x.RiskBatch.ScoreDirect == scoreDirect
                                              && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
                                              select x).ToList();
                        else
                            ret.RiskClaims = (from x in _db.RiskClaims
                                              where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForScoring
                                              || (x.ClaimStatus == (int)RiskClaimStatus.BeingScored && x.ClaimStatusLastModified <= timeout))
                                              && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
                                              && x.RiskBatch.ScoreDirect == scoreDirect
                                              select x).ToList();

                        if (ret.RiskClaims != null)
                        {
                            try
                            {
                                //foreach (var riskClaim in ret.RiskClaims)
                                //{
                                //    riskClaim.ClaimStatus = (int)RiskClaimStatus.BeingScored;
                                //    riskClaim.ClaimStatusLastModified = DateTime.Now;

                                //    _db.SaveChanges();
                                //}

                                ret.RiskClient = ret.RiskClaims.FirstOrDefault().RiskBatch.RiskClient;

                                transaction.Commit();

                                done = true;
                            }
                            catch (OptimisticConcurrencyException)
                            {
                                ret = new RiskClaimsToProcess();

                                // and go around again
                            }
                            catch (Exception ex)
                            {
                                ret = new RiskClaimsToProcess();
                                throw ex;
                            }
                        }
                        else done = true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
            return ret;
        }

        public RiskClaimToProcess GetAClaimToScoreFromBatch(int? batchId, bool scoreDirect)
        {
            RiskClaimToProcess ret = new RiskClaimToProcess();
            bool done = false;

            while (!done)
            {
                using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                {
                    try
                    {
                        DateTime timeout = DateTime.Now; // DateTime.Now.AddMinutes(-10);

                        if (batchId != null)
                            ret.RiskClaim = (from x in _db.RiskClaims
                                             where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForScoring
                                             || (x.ClaimStatus == (int)RiskClaimStatus.BeingScored && x.ClaimStatusLastModified <= timeout))
                                             && x.RiskBatch_Id == batchId && x.RiskBatch.ScoreDirect == scoreDirect
                                             && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
                                             select x).FirstOrDefault();
                        else
                            ret.RiskClaim = (from x in _db.RiskClaims
                                             where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForScoring
                                             || (x.ClaimStatus == (int)RiskClaimStatus.BeingScored && x.ClaimStatusLastModified <= timeout))
                                             && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
                                             && x.RiskBatch.ScoreDirect == scoreDirect
                                             select x).FirstOrDefault();

                        if (ret.RiskClaim != null)
                        {
                            try
                            {
                                ret.RiskClaim.ClaimStatus = (int)RiskClaimStatus.BeingScored;
                                ret.RiskClaim.ClaimStatusLastModified = DateTime.Now;

                                _db.SaveChanges();

                                ret.RiskClient = ret.RiskClaim.RiskBatch.RiskClient;

                                transaction.Commit();

                                done = true;
                            }
                            catch (OptimisticConcurrencyException)
                            {
                                ret = new RiskClaimToProcess();

                                // and go around again
                            }
                        }
                        else done = true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
            return ret;
        }

        public bool IsAClaimToScore(bool scoreDirect)
        {
            DateTime timeout = DateTime.Now.AddMinutes(-10);

            bool any = (from x in _db.RiskClaims
                        where x.RiskBatch.ScoreDirect == scoreDirect && (x.ClaimStatus == (int)RiskClaimStatus.QueuedForScoring
                            || (x.ClaimStatus == (int)RiskClaimStatus.BeingScored && x.ClaimStatusLastModified <= timeout))
                        select x).Any();

            return any;
        }

        public List<RiskClaim> GetClaimsInBatch(int riskBatchId)
        {
            return (from x in _db.RiskClaims
                    where x.RiskBatch_Id == riskBatchId
                    orderby x.Id ascending
                    select x).ToList();

        }


        public void SaveCleansingResults(int riskClaimId, ProcessingResults results)
        {
            string xml = JsonConvert.SerializeObject(results, Newtonsoft.Json.Formatting.None);

            var riskClaim = GetRiskClaim(riskClaimId); // = (from x in db.RiskClaims where x.Id == riskClaimId select x).FirstOrDefault();

            if (riskClaim != null)
            {
                riskClaim.CleansingResults = xml;

                if (!results.IsValid)
                    riskClaim.ClaimStatus = (int)RiskClaimStatus.FailedOnCleansing;

                _db.SaveChanges();
            }
        }

        public void SaveValidationResults(int riskClaimId, ProcessingResults results)
        {
            string xml = JsonConvert.SerializeObject(results, Newtonsoft.Json.Formatting.None);

            var riskClaim = GetRiskClaim(riskClaimId);

            if (riskClaim != null)
            {
                riskClaim.ValidationResults = xml;

                if (!results.IsValid)
                    riskClaim.ClaimStatus = (int)RiskClaimStatus.FailedOnValidation;

                _db.SaveChanges();
            }
        }

        

        public RiskClaim FindPreviousVersionOfClaim(int riskClaimId)
        {

            var c = (from x in _db.RiskClaims
                where x.Id == riskClaimId
                select x);

            return c.FirstOrDefault();
        }

        public RiskClaim FindLatestClaim(int clientId, string refNumber)
        {
            var c = from x in _db.RiskClaims
                    where x.ClientClaimRefNumber == refNumber && x.RiskBatch.RiskClient_Id == clientId && x.RiskBatch.BatchStatus > 0 && x.LatestVersion == true
                    select x;

            return c.FirstOrDefault();
        }

        public List<RiskClaimToProcess> GetClaimsToProcess()
        {
            var claims = from rc in _db.RiskClaims
                         join rb in _db.RiskBatches on rc.RiskBatch_Id equals rb.Id
                         join rcl in _db.RiskClients on rb.RiskClient_Id equals rcl.Id
                         where rc.ClaimStatus == (int)RiskClaimStatus.QueuedForLoading
                         select new RiskClaimToProcess
                         {
                             RiskClaim = rc,
                             RiskClient = rcl
                         };

            return claims.ToList();
        }

        public void SetRiskClaimStatus(int riskClaimId, RiskClaimStatus status)
        {
            var riskClaim = GetRiskClaim(riskClaimId);

            if (riskClaim != null)
            {
                riskClaim.ClaimStatus = (int)status;

                _db.SaveChanges();
            }
        }


        public void SetRiskClaimStatusToBeingScored(RiskClaim riskClaim)
        {
            if (riskClaim != null)
            {
                riskClaim.ClaimStatus = (int)RiskClaimStatus.BeingScored;
                riskClaim.ClaimStatusLastModified = DateTime.Now;

                _db.SaveChanges();
            }
        }

        public void SetClaimReadStatus(int riskClaimId, int riskClaimReadStatus)
        {

            var claim = (from x in _db.RiskClaims
                         where x.Id == riskClaimId
                         select x).FirstOrDefault();

            claim.ClaimReadStatus = riskClaimReadStatus;

            _db.SaveChanges();
        }

        public void SetRiskClaimIncidentId(int riskClaimId, int incidentId)
        {
            var riskClaim = GetRiskClaim(riskClaimId);

            if (riskClaim != null)
            {
                riskClaim.Incident_Id = incidentId;

                _db.SaveChanges();
            }
        }




        //public RiskClaimToProcess GetAClaimToProcessThirdParties()
        //{
        //    return GetAClaimToProcessThirdParties(null);

            //RiskClaimToProcess ret = new RiskClaimToProcess();
            //bool done = false;

            //while (!done)
            //{
            //    DateTime timeout = DateTime.Now.AddMinutes(-10);

            //    using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
            //    {
            //        try
            //        {
            //            ret.RiskClaim = (from x in _db.RiskClaims
            //                             where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForThirdParty
            //                             || (x.ClaimStatus == (int)RiskClaimStatus.BeingThirdPartyProcessed && x.ClaimStatusLastModified <= timeout))
            //                             && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
            //                             select x).FirstOrDefault();

            //            if (ret.RiskClaim != null)
            //            {
            //                var olderRiskClaim = (from x in _db.RiskClaims
            //                                      where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForThirdParty
            //                                          || (x.ClaimStatus == (int)RiskClaimStatus.BeingThirdPartyProcessed && x.ClaimStatusLastModified <= timeout))
            //                                          && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
            //                                          && x.BaseRiskClaim_Id == ret.RiskClaim.BaseRiskClaim_Id
            //                                      select x).FirstOrDefault();

            //                if (olderRiskClaim != null)
            //                    ret.RiskClaim = olderRiskClaim;
            //            }

            //            if (ret.RiskClaim != null)
            //            {
            //                try
            //                {
            //                    ret.RiskClaim.ClaimStatus = (int)RiskClaimStatus.BeingThirdPartyProcessed;
            //                    ret.RiskClaim.ClaimStatusLastModified = DateTime.Now;

            //                    _db.SaveChanges();

            //                    ret.RiskClient = ret.RiskClaim.RiskBatch.RiskClient;

            //                    transaction.Commit();

            //                    done = true;
            //                }
            //                catch (OptimisticConcurrencyException)
            //                {
            //                    ret = new RiskClaimToProcess();

            //                    // and go around again
            //                }
            //            }
            //            else done = true;
            //        }
            //        catch (Exception ex)
            //        {
            //            transaction.Rollback();
            //            throw ex;
            //        }
            //    }
            //}
            //return ret;


        //}

        public bool IsAClaimToProcessThirdParties(bool submitDirect)
        {
            DateTime timeout = DateTime.Now.AddMinutes(-10);

            return (from x in _db.RiskClaims
                    where x.RiskBatch.SubmitDirect == submitDirect && (x.ClaimStatus == (int)RiskClaimStatus.QueuedForThirdParty
                        || (x.ClaimStatus == (int)RiskClaimStatus.BeingThirdPartyProcessed && x.ClaimStatusLastModified <= timeout))
                    select x).Any();
        }

        public RiskClaimToProcess GetAClaimToProcessThirdPartiesFromBatch(int? batchId, bool submitDirect)
        {
            RiskClaimToProcess ret = new RiskClaimToProcess();
            bool done = false;

            while (!done)
            {
                DateTime timeout = DateTime.Now.AddMinutes(-10);

                using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                {
                    try
                    {
                        if (batchId != null)
                        {
                            ret.RiskClaim = (from x in _db.RiskClaims
                                             where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForThirdParty
                                             || (x.ClaimStatus == (int)RiskClaimStatus.BeingThirdPartyProcessed && x.ClaimStatusLastModified <= timeout))
                                             && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
                                             && x.RiskBatch_Id == batchId && x.RiskBatch.SubmitDirect == submitDirect
                                             select x).FirstOrDefault();

                            if (ret.RiskClaim != null)  // Look for older (lower ID) claim in the same batch with same base risk claim ID
                            {
                                var olderRiskClaim = (from x in _db.RiskClaims
                                                      where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForThirdParty
                                                      || (x.ClaimStatus == (int)RiskClaimStatus.BeingThirdPartyProcessed && x.ClaimStatusLastModified <= timeout))
                                                      && x.RiskBatch_Id == batchId && x.RiskBatch.SubmitDirect == submitDirect
                                                      && x.BaseRiskClaim_Id == ret.RiskClaim.BaseRiskClaim_Id
                                                      && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
                                                      select x).FirstOrDefault();

                                if (olderRiskClaim != null)
                                    ret.RiskClaim = olderRiskClaim;
                            }
                        }
                        else
                        {
                            ret.RiskClaim = (from x in _db.RiskClaims
                                             where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForThirdParty
                                             || (x.ClaimStatus == (int)RiskClaimStatus.BeingThirdPartyProcessed && x.ClaimStatusLastModified <= timeout))
                                             && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete && x.RiskBatch.SubmitDirect == submitDirect
                                             select x).FirstOrDefault();

                            if (ret.RiskClaim != null)
                            {
                                var olderRiskClaim = (from x in _db.RiskClaims
                                                      where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForThirdParty
                                                      || (x.ClaimStatus == (int)RiskClaimStatus.BeingThirdPartyProcessed && x.ClaimStatusLastModified <= timeout))
                                                      && x.BaseRiskClaim_Id == ret.RiskClaim.BaseRiskClaim_Id && x.RiskBatch.SubmitDirect == submitDirect
                                                      && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
                                                      select x).FirstOrDefault();

                                if (olderRiskClaim != null)
                                    ret.RiskClaim = olderRiskClaim;
                            }
                        }

                        if (ret.RiskClaim != null)
                        {
                            try
                            {
                                ret.RiskClaim.ClaimStatus = (int)RiskClaimStatus.BeingThirdPartyProcessed;
                                ret.RiskClaim.ClaimStatusLastModified = DateTime.Now;

                                _db.SaveChanges();

                                ret.RiskClient = ret.RiskClaim.RiskBatch.RiskClient;

                                transaction.Commit();

                                done = true;
                            }
                            catch (OptimisticConcurrencyException)
                            {
                                ret = new RiskClaimToProcess();

                                // and go around again
                            }
                        }
                        else done = true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }

            return ret;
        }

        //public RiskClaimToProcess GetAClaimToProcessThirdParties(int riskClaimId)
        //{
        //    RiskClaimToProcess ret = new RiskClaimToProcess();
        //    bool done = false;

        //    while (!done)
        //    {
        //        DateTime timeout = DateTime.Now.AddMinutes(-10);

        //        using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
        //        {
        //            try
        //            {
        //                ret.RiskClaim = (from x in _db.RiskClaims
        //                                 where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForThirdParty
        //                                 || (x.ClaimStatus == (int)RiskClaimStatus.BeingThirdPartyProcessed && x.ClaimStatusLastModified <= timeout))
        //                                 && x.Id == riskClaimId
        //                                 && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
        //                                 select x).FirstOrDefault();

        //                if (ret.RiskClaim != null)
        //                {
        //                    var olderRiskClaim = (from x in _db.RiskClaims
        //                                          where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForThirdParty
        //                                              || (x.ClaimStatus == (int)RiskClaimStatus.BeingThirdPartyProcessed && x.ClaimStatusLastModified <= timeout))
        //                                              && x.Id == riskClaimId
        //                                              && x.BaseRiskClaim_Id == ret.RiskClaim.BaseRiskClaim_Id
        //                                              && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.BatchLoadComplete
        //                                          select x).FirstOrDefault();

        //                    if (olderRiskClaim != null)
        //                        ret.RiskClaim = olderRiskClaim;
        //                }

        //                if (ret.RiskClaim != null)
        //                {
        //                    try
        //                    {
        //                        ret.RiskClaim.ClaimStatus = (int)RiskClaimStatus.BeingThirdPartyProcessed;
        //                        ret.RiskClaim.ClaimStatusLastModified = DateTime.Now;

        //                        _db.SaveChanges();

        //                        ret.RiskClient = ret.RiskClaim.RiskBatch.RiskClient;

        //                        transaction.Commit();

        //                        done = true;
        //                    }
        //                    catch (OptimisticConcurrencyException)
        //                    {
        //                        ret = new RiskClaimToProcess();

        //                        // and go around again
        //                    }
        //                }
        //                else done = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                throw ex;
        //            }
        //        }
        //    }
        //    return ret;
        //}

        public bool IsAClaimToLoad(bool submitDirect)
        {
            DateTime timeout = DateTime.Now.AddMinutes(-10);

            return (from x in _db.RiskClaims
                    where x.RiskBatch.SubmitDirect == submitDirect && (x.ClaimStatus == (int)RiskClaimStatus.QueuedForLoading
                        || (x.ClaimStatus == (int)RiskClaimStatus.BeingLoaded && x.ClaimStatusLastModified <= timeout))
                    select x).Any();
        }

        public RiskClaimToProcess GetAClaimToLoadFromBatch(int batchId)
        {
            RiskClaimToProcess ret = new RiskClaimToProcess();
            bool done = false;

            while (!done)
            {
                DateTime timeout = DateTime.Now.AddMinutes(-10);

                using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
                {
                    try
                    {
                        ret.RiskClaim = (from x in _db.RiskClaims
                                         where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForLoading
                                         || (x.ClaimStatus == (int)RiskClaimStatus.BeingLoaded && x.ClaimStatusLastModified <= timeout))
                                         && x.RiskBatch.Id == batchId
                                         && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.ClaimsBeingLoaded
                                         select x).FirstOrDefault();

                        if (ret.RiskClaim != null)
                        {
                            var olderRiskClaim = (from x in _db.RiskClaims
                                                  where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForLoading
                                                      || (x.ClaimStatus == (int)RiskClaimStatus.BeingLoaded && x.ClaimStatusLastModified <= timeout))
                                                      && x.RiskBatch.Id == batchId
                                                      && x.RiskBatch.BatchStatus == (int)RiskBatchStatus.ClaimsBeingLoaded
                                                      && x.BaseRiskClaim_Id == ret.RiskClaim.BaseRiskClaim_Id
                                                  select x).FirstOrDefault();

                            if (olderRiskClaim != null)
                                ret.RiskClaim = olderRiskClaim;
                        }

                        if (ret.RiskClaim != null)
                        {
                            try
                            {
                                ret.RiskClaim.ClaimStatus = (int)RiskClaimStatus.BeingLoaded;
                                ret.RiskClaim.ClaimStatusLastModified = DateTime.Now;

                                _db.SaveChanges();

                                ret.RiskClient = ret.RiskClaim.RiskBatch.RiskClient;

                                transaction.Commit();

                                done = true;
                            }
                            catch (OptimisticConcurrencyException)
                            {
                                ret = new RiskClaimToProcess();

                                // and go around again
                            }
                        }
                        else done = true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }

            }

            return ret;
        }

        //public RiskClaimToProcess GetAClaimToLoad(int riskClaimId)
        //{
        //    RiskClaimToProcess ret = new RiskClaimToProcess();
        //    bool done = false;

        //    while (!done)
        //    {
        //        DateTime timeout = DateTime.Now.AddMinutes(-10);

        //        using (var transaction = _db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
        //        {
        //            try
        //            {
        //                ret.RiskClaim = (from x in _db.RiskClaims
        //                                 where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForLoading
        //                                 || (x.ClaimStatus == (int)RiskClaimStatus.BeingLoaded && x.ClaimStatusLastModified <= timeout))
        //                                 && x.Id == riskClaimId
        //                                 select x).FirstOrDefault();

        //                if (ret.RiskClaim != null)
        //                {
        //                    var olderRiskClaim = (from x in _db.RiskClaims
        //                                          where (x.ClaimStatus == (int)RiskClaimStatus.QueuedForLoading
        //                                              || (x.ClaimStatus == (int)RiskClaimStatus.BeingLoaded && x.ClaimStatusLastModified <= timeout))
        //                                          && x.BaseRiskClaim_Id == ret.RiskClaim.BaseRiskClaim_Id
        //                                          select x).FirstOrDefault();

        //                    if (olderRiskClaim != null)
        //                        ret.RiskClaim = olderRiskClaim;
        //                }

        //                if (ret.RiskClaim != null)
        //                {
        //                    try
        //                    {
        //                        ret.RiskClaim.ClaimStatus = (int)RiskClaimStatus.BeingLoaded;
        //                        ret.RiskClaim.ClaimStatusLastModified = DateTime.Now;

        //                        _db.SaveChanges();

        //                        ret.RiskClient = ret.RiskClaim.RiskBatch.RiskClient;

        //                        transaction.Commit();

        //                        done = true;
        //                    }
        //                    catch (OptimisticConcurrencyException)
        //                    {
        //                        ret = new RiskClaimToProcess();

        //                        // and go around again
        //                    }
        //                }
        //                else done = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                throw ex;
        //            }
        //        }
        //    }
        //    return ret;
        //}


        public bool AlreadyHaveNewerVersionOfSameClaim(int clientId, string refNumber, DateTime incidentDate)
        {
            return false;
        }

        public List<RiskClaim> GetListAllClaims()
        {
            return (from x in _db.RiskClaims select x).ToList();
        }



        public int SaveLevel1ReportIntoRiskClaimRecord(int riskClaimId, byte[] reportBytes)
        {
            var claim = GetRiskClaim(riskClaimId);

            if (claim != null)
            {
                var riskReport = _db.RiskReports.Create();

                riskReport.Id              = Guid.NewGuid();
                riskReport.Description     = "Level 1";
                riskReport.FileData        = reportBytes;
                riskReport.ParentEntity_Id = riskClaimId;

                _db.RiskReports.Add(riskReport);

                _db.SaveChanges();

                claim.RiskReportLevel1_Id = riskReport.PkId;

                _db.SaveChanges();
            }

            return claim.Id;
        }

        public int SaveLevel2ReportIntoRiskClaimRecord(int riskClaimId, byte[] reportBytes)
        {
            var claim = GetRiskClaim(riskClaimId);

            if (claim != null)
            {
                var riskReport = _db.RiskReports.Create();

                riskReport.Id              = Guid.NewGuid();
                riskReport.Description     = "Level 2";
                riskReport.FileData        = reportBytes;
                riskReport.ParentEntity_Id = riskClaimId;

                _db.RiskReports.Add(riskReport);

                _db.SaveChanges();

                claim.RiskReportLevel2_Id = riskReport.PkId;

                _db.SaveChanges();
            }

            return claim.Id;
        }



        public void SaveRiskClaimLoadingTime(int riskClaimId, long loadingDurationInMs)
        {
            var riskClaim = GetRiskClaim(riskClaimId);

            if (riskClaim != null)
            {
                riskClaim.LoadingDurationInMs = (int)loadingDurationInMs;

                _db.SaveChanges();
            }
        }

        public byte[] GetLevel1Report(int riskClaimId)
        {
            var rc = GetRiskClaim(riskClaimId);

            if (rc != null)
                return (from x in _db.RiskReports
                        where x.PkId == rc.RiskReportLevel1_Id
                        select x.FileData).FirstOrDefault();

            return null;
        }

        public byte[] GetLevel2Report(int riskClaimId)
        {
            var rc = GetRiskClaim(riskClaimId);

            if (rc != null)
                return (from x in _db.RiskReports
                        where x.PkId == rc.RiskReportLevel2_Id
                        select x.FileData).FirstOrDefault();

            return null;
        }

        public void SetClaimStatusToThirdPartyCompleteIfNoThirdParty(int riskBatchId, RiskClaimStatus statusToFind)
        {
            string oldStatus = ((int)statusToFind).ToString();
            string newStatus = ((int)RiskClaimStatus.ThirdPartyComplete).ToString();
            
            _db.Database.ExecuteSqlCommand("update RiskClaim set ClaimStatus = " + newStatus + ", ExternalServicesRequestsRequired = 0 where ExternalServicesRequired = 0 and ClaimStatus = " + oldStatus + " and RiskBatch_Id = " + riskBatchId.ToString());

            _db.SaveChanges();
        }

        public void SetClaimStatusIfClosed(int riskBatchId, RiskClaimStatus statusToFind, RiskClaimStatus statusToSet)
        {
            string oldStatus = ((int)statusToFind).ToString();
            string newStatus = ((int)statusToSet).ToString();

            string include = ((int)MDA.Common.Enum.ClaimStatus.Delete).ToString() + ", " +
                             ((int)MDA.Common.Enum.ClaimStatus.Settled).ToString() + ", " +
                             ((int)MDA.Common.Enum.ClaimStatus.Withdrawn).ToString();

            _db.Database.ExecuteSqlCommand("update RiskClaim set ClaimStatus = " + newStatus + " where SuppliedClaimStatus in (" + include + ") and ClaimStatus = " + oldStatus + " and RiskBatch_Id = " + riskBatchId.ToString());

            _db.SaveChanges();
        }

        public void SetAllClaimStatusInBatch(int riskBatchId, RiskClaimStatus statusToFind, RiskClaimStatus statusToSet)
        {
            string oldStatus = ((int)statusToFind).ToString();
            string newStatus = ((int)statusToSet).ToString();

            _db.Database.ExecuteSqlCommand("update RiskClaim set ClaimStatus = " + newStatus + " where ClaimStatus = " + oldStatus + " and RiskBatch_Id = " + riskBatchId.ToString());

            _db.SaveChanges();
        }

        public void SetAllClaimStatusInAllClientBatches(int riskClientId, RiskClaimStatus statusToFind, RiskClaimStatus statusToSet)
        {
            string oldStatus = ((int)statusToFind).ToString();
            string newStatus = ((int)statusToSet).ToString();

            _db.Database.ExecuteSqlCommand("update rc set ClaimStatus = " + newStatus + " from RiskClaim rc join RiskBatch rb on rb.Id = rc.RiskBatch_Id where rb.RiskClient_Id = " + riskClientId.ToString() + " and rc.ClaimStatus = " + oldStatus);

            _db.SaveChanges();
        }

        public void SetAllRiskClaimStatusAsMemberOfBadBatch(int riskBatchId)
        {
            string newStatus = ((int)RiskClaimStatus.FailedAsMemberOfBadBatch).ToString();

            _db.Database.ExecuteSqlCommand("update RiskClaim set ClaimStatus = " + newStatus + " where RiskBatch_Id = " + riskBatchId.ToString());

            _db.SaveChanges();
        }


        /// <summary>
        /// Create a Risk Claim record within a given Batch
        /// </summary>
        /// <param name="riskBatchId">Risk Batch ID</param>
        /// <param name="mdaClaimRef">MDA reference</param>
        /// <param name="who">Logged in User</param>
        /// <returns>RiskClaim.Id</returns>
        public RiskClaim CreateRiskClaimRecord(MDA.Pipeline.Model.IPipelineClaim claimXml, MDA.Common.Enum.ClaimStatus claimStatus, int riskBatchId, string clientClaimRefNumber, string sourceRef, int servicesMask, ProcessingResults validationResults, string who)
        {
            bool useJson = Convert.ToBoolean(ConfigurationManager.AppSettings["UseJsonForClaims"]);

            var riskClaim = _db.RiskClaims.Create();

            bool ClaimIsValid = validationResults.IsValid;

            riskClaim.CreatedBy                        = who;
            riskClaim.CreatedDate                      = DateTime.Now;
            riskClaim.ClientClaimRefNumber             = clientClaimRefNumber;
            riskClaim.SuppliedClaimStatus              = (int)claimStatus;
            riskClaim.ClaimStatus                      = (ClaimIsValid) ? (int)RiskClaimStatus.IsNew : (int)RiskClaimStatus.FailedOnValidation;
            riskClaim.ClaimStatusLastModified          = DateTime.Now;
            riskClaim.MdaClaimRef                      = "";
            riskClaim.RiskBatch_Id                     = riskBatchId;
            riskClaim.SourceReference                  = sourceRef;
            riskClaim.SourceEntityEncodeFormat         = (useJson) ? "json" : "xml";
            riskClaim.SourceEntityXML                  = SerializePipelineClaim(claimXml, useJson);
            riskClaim.Incident_Id                      = 0;   // This is a dummy value and relies on an Incident ID = 0
            riskClaim.LatestVersion                    = true;
            riskClaim.EntityClassType                  = claimXml.GetType().ToString();
            riskClaim.ExternalServicesRequired = 0; // servicesMask;
            riskClaim.ExternalServicesRequestsRequired = 0; //servicesMask;
            riskClaim.ValidationResults                = JsonConvert.SerializeObject(validationResults, Newtonsoft.Json.Formatting.None);
            riskClaim.ReserveChanged                   = (int)RiskClaimReserveStatus.Unknown;

            _db.RiskClaims.Add(riskClaim);

            _db.SaveChanges();

            riskClaim.BaseRiskClaim_Id = riskClaim.Id;
            riskClaim.MdaClaimRef      = "ADA-" + string.Format("{0:d7}", riskClaim.Id);

            _db.SaveChanges();

            return riskClaim;
        }

        public RiskClaim CreateRiskClaimUpdateRecord(MDA.Pipeline.Model.IPipelineClaim claimXml, MDA.Common.Enum.ClaimStatus claimStatus, int baseRiskClaimId, int riskBatchId, int existingIncidentId, string clientClaimRefNumber, string sourceRef, int servicesMask, ProcessingResults validationResults, string who)
        {           

            bool useJson = Convert.ToBoolean(ConfigurationManager.AppSettings["UseJsonForClaims"]);
            int readClaimStatus = -1;

            var oldVersions = from x in _db.RiskClaims where x.LatestVersion == true && x.BaseRiskClaim_Id == baseRiskClaimId select x;

            foreach (var r in oldVersions)
            {
                r.LatestVersion = false;
                readClaimStatus = r.ClaimReadStatus;
            }

            

            _db.SaveChanges();

            var riskClaim = _db.RiskClaims.Create();

            bool ClaimIsValid = validationResults.IsValid;

            riskClaim.CreatedBy                        = who;
            riskClaim.CreatedDate                      = DateTime.Now;
            riskClaim.ClientClaimRefNumber             = clientClaimRefNumber;
            riskClaim.SuppliedClaimStatus              = (int)claimStatus;
            riskClaim.ClaimStatus                      = (ClaimIsValid) ? (int)RiskClaimStatus.IsNew : (int)RiskClaimStatus.FailedOnValidation;
            riskClaim.ClaimStatusLastModified          = DateTime.Now;
            riskClaim.RiskBatch_Id                     = riskBatchId;
            riskClaim.SourceReference                  = sourceRef;
            riskClaim.BaseRiskClaim_Id                 = baseRiskClaimId;
            riskClaim.SourceEntityEncodeFormat         = (useJson) ? "json" : "xml";
            riskClaim.SourceEntityXML                  = SerializePipelineClaim(claimXml, useJson);
            riskClaim.Incident_Id                      = existingIncidentId;
            riskClaim.LevelOneRequestedCount           = 0;
            riskClaim.LevelTwoRequestedCount           = 0;
            riskClaim.MdaClaimRef                      = "";
            riskClaim.LatestVersion                    = true;
            riskClaim.EntityClassType                  = claimXml.GetType().ToString();
            riskClaim.ExternalServicesRequired = 0; // servicesMask;
            riskClaim.ExternalServicesRequestsRequired = 0; // servicesMask;

            if(readClaimStatus==1)
                riskClaim.ClaimReadStatus              = readClaimStatus;

            riskClaim.ReserveChanged                   = ReserveChanged(riskClaim, claimXml.ExtraClaimInfo.Reserve);

            _db.RiskClaims.Add(riskClaim);

            _db.SaveChanges();

            riskClaim.MdaClaimRef = "ADA-" + string.Format("{0:d7}", riskClaim.Id);
            riskClaim.ClaimStatus = (int)RiskClaimStatus.QueuedForLoading;

            _db.SaveChanges();

            SaveValidationResults(riskClaim.Id, validationResults);

            return riskClaim;
        }


        public void IncrementLevelOneCount(int riskClaimId, string version, string who)
        {
            var riskClaim = GetRiskClaim(riskClaimId);

            if (riskClaim != null)
            {
                riskClaim.LevelOneRequestedCount += 1;

                if (riskClaim.LevelOneRequestedCount == 1)
                {
                    riskClaim.LevelOneRequestedWhen = DateTime.Now;
                    riskClaim.LevelOneRequestedWho = who;
                }
                _db.SaveChanges();
        }
        }

        public void IncrementLevelTwoCount(int riskClaimId, string version, string who)
        {
            var rcr = GetRiskClaim(riskClaimId);

            if (rcr != null)
            {
                rcr.LevelTwoRequestedCount += 1;

                if (rcr.LevelTwoRequestedCount == 1)
                {
                    rcr.LevelTwoRequestedWhen = DateTime.Now;
                    rcr.LevelTwoRequestedWho = who;
                }

                _db.SaveChanges();
            }
        }

        //public void EnsureLatestClaimScoreIsEncodedInXml(int riskClaimId)
        //{
        //    var rcr = GetLatestRiskClaimRun(riskClaimId);

        //    if (rcr != null)
        //    {
        //        if (rcr.ScoreEntityEncodeFormat == "json")
        //        {
        //            ClaimScoreResults csr = RiskServices.DeserializeScores(rcr.ScoreEntityXml, true);

        //            rcr.ScoreEntityXml = SerializeScores(csr, false);
        //        }
        //    }

        //    _db.SaveChanges();
        //}


        public int GetSingleClaimRiskClaimId(int riskBatchId)
        {
            var riskClaimId = (from ru in _db.RiskClaims
                               where ru.RiskBatch_Id == riskBatchId
                               select ru).FirstOrDefault();

            if (riskClaimId != null)
                return (riskClaimId.Id);

            return 0;
        }

        public int RequestLevelOneReport(int riskClaimId, int userId)
        {
            IEnumerable<string> users = GetUsersByRole(userId);

            var claim = (from x in _db.RiskClaims
                         where x.Id == riskClaimId && x.ClaimStatus == (int)RiskClaimStatus.Scored
                         && users.Contains(x.CreatedBy)
                         select x).FirstOrDefault();

            if (claim == null) return -1;

            if (claim.LevelOneRequestedWhen == null)
            {
                claim.LevelOneRequestedWhen = DateTime.Now;
                claim.LevelOneRequestedWho = GetFullName(userId);
                claim.LevelOneRequestedCount = 0;
            }

            claim.LevelOneRequestedCount += 1;

            _db.SaveChanges();

            return 0;
        }

        public int RequestLevelTwoReport(int riskClaimId, int userId)
        {
            IEnumerable<string> users = GetUsersByRole(userId);

            var claim = (from x in _db.RiskClaims
                         where x.Id == riskClaimId && x.ClaimStatus == (int)RiskClaimStatus.Scored
                         && users.Contains(x.CreatedBy)
                         select x).FirstOrDefault();

            if (claim == null) return -1;

            if (claim.LevelTwoRequestedWhen == null)
            {
                claim.LevelTwoRequestedWhen = DateTime.Now;
                claim.LevelTwoRequestedWho = GetFullName(userId);
                claim.LevelTwoRequestedCount = 0;
            }

            claim.LevelOneRequestedCount += 1;

            _db.SaveChanges();
            return 0;
        }

        public int RequestCallback(int riskClaimId, int userId)
        {
            IEnumerable<string> users = GetUsersByRole(userId);

            var claim = (from x in _db.RiskClaims
                         where x.Id == riskClaimId
                         && users.Contains(x.CreatedBy)
                         select x).FirstOrDefault();

            if (claim == null) return -1;

            if (claim.CallbackRequestedWhen == null)
            {
                claim.CallbackRequestedWhen = DateTime.Now;
                claim.CallbackRequestedWho = GetFullName(userId);
                claim.CallbackRequested = true;
            }

            _db.SaveChanges();
            return 0;
        }

        public int ClearCallbackRequest(int riskClaimId, int userId)
        {
            IEnumerable<string> users = GetUsersByRole(userId);

            var claim = (from x in _db.RiskClaims
                         where x.Id == riskClaimId
                         && users.Contains(x.CreatedBy)
                         select x).FirstOrDefault();

            if (claim == null) return -1;

            if (claim.CallbackRequestedWhen == null)
            {
                claim.CallbackRequestedWhen = null;
                claim.CallbackRequestedWho = null;
                claim.CallbackRequested = false;
            }

            _db.SaveChanges();
            return 0;
        }

        public int SetClaimReadStatus(int riskClaimId, int riskClaimReadStatus, int userId, out int riskClaimReadStatusCurrent)
        {
           //IEnumerable<string> users = GetUsersByRole(userId);

            var claim = (from x in _db.RiskClaims
                         where x.Id == riskClaimId
                         //&& users.Contains(x.CreatedBy)
                         select x).FirstOrDefault();

            if (claim == null)
            {
                riskClaimReadStatusCurrent = -1;
                return -1;
            }

            var tempClaimReadStatus = claim.ClaimReadStatus;

            claim.ClaimReadStatus = riskClaimReadStatus;
            riskClaimReadStatusCurrent = tempClaimReadStatus;

            _db.SaveChanges();
            return 0;
        }

        public int ReserveChanged(RiskClaim rc, decimal? currentReserve)
        {
            var prevRiskClaim = GetPreviousRiskClaimVersion(rc);

            if (prevRiskClaim != null)
            {
                var previousReserve = prevRiskClaim.Incident2Person.Where(i2p => i2p.Incident_Id == rc.Incident_Id && i2p.PartyType_Id == (int)MDA.Common.Enum.PartyType.Insured && i2p.SubPartyType_Id == (int)MDA.Common.Enum.SubPartyType.Driver).Select(i2p => i2p.Reserve).FirstOrDefault();

                if (previousReserve != null)
                {
                    //if (previousReserve > currentReserve)
                    //{
                    //    return (int)RiskClaimReserveStatus.Decrease;
                    //}
                    if (previousReserve < currentReserve)
                    {
                        return (int)RiskClaimReserveStatus.Increase;
                    }
                    else
                    {
                        return (int)RiskClaimReserveStatus.Unknown;
                    }
                }
                else
                {
                    if (currentReserve != null && currentReserve > 0)
                    {
                        return (int)RiskClaimReserveStatus.Increase;
                    }
                }
            }

            return (int)RiskClaimReserveStatus.Unknown;
        }

    }
}
