﻿
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using MDA.RiskService.Model;
using Newtonsoft.Json;

using System.Xml.Serialization;
using RiskEngine.Scoring.Model;
using System;
using System.Reflection;

namespace MDA.RiskService
{
    public partial class RiskServices
    {
        private static string XmlSerializeClaim(MDA.Pipeline.Model.IPipelineClaim claimXml)
        {
            var ms = new MemoryStream();

            var ser = new XmlSerializer(claimXml.GetType()); //typeof(MDA.Pipeline.Model.PipelineMotorClaim));

            ser.Serialize(ms, claimXml);

            ms.Position = 0;
            var sr = new StreamReader(ms);

            string xml = sr.ReadToEnd();

            return xml;
        }

        private static T Deserialize<T>(string rawXml)
        {
            using (XmlReader reader = XmlReader.Create(new StringReader(rawXml)))
            {
                var formatter = new DataContractSerializer(typeof(T));
                return (T)formatter.ReadObject(reader);
            }
        }

        private static List<EntityScoreMessageList> DeserializeListEntityScoreMessages(string xml, bool useJson)
        {
            if (!useJson)
            {
                var result = System.Text.Encoding.UTF8.GetBytes(xml);

                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(result, new XmlDictionaryReaderQuotas());
                var ser = new DataContractSerializer(typeof(List<EntityScoreMessageList>));

                return (List<EntityScoreMessageList>)ser.ReadObject(reader, true);
            }
            else
            {
                return JsonConvert.DeserializeObject<List<EntityScoreMessageList>>(xml);
            }
        }

        private static ClaimScoreResults DeserializeScores(string xml, bool useJson)
        {
            if (!useJson)
            {
                var result = System.Text.Encoding.UTF8.GetBytes(xml);

                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(result, new XmlDictionaryReaderQuotas());
                var ser = new DataContractSerializer(typeof(ClaimScoreResults));

                return (ClaimScoreResults)ser.ReadObject(reader, true);
            }
            else
            {
                return JsonConvert.DeserializeObject<ClaimScoreResults>(xml);
            }
        }

        private static MDA.Pipeline.Model.IPipelineClaim DeserializeIClaim(string rawXml, bool useJson, string entityClassType)
        {
            MDA.Pipeline.Model.IPipelineClaim IPipelineClaim = null;

            if (!useJson)
            {
                switch (entityClassType)
                {
                    case "MDA.Pipeline.Model.PipelineMobileClaim":
                        IPipelineClaim = RiskServices.Deserialize<MDA.Pipeline.Model.PipelineMobileClaim>(rawXml);
                        break;
                    case "MDA.Pipeline.Model.PipelineMotorClaim":
                        IPipelineClaim = RiskServices.Deserialize<MDA.Pipeline.Model.PipelineMotorClaim>(rawXml);
                        break;
                    case "MDA.Pipeline.Model.PipelinePIClaim":
                        IPipelineClaim = RiskServices.Deserialize<MDA.Pipeline.Model.PipelinePIClaim>(rawXml);
                        break;

                }
            }

            else
            {
                switch (entityClassType)
                {
                    case "MDA.Pipeline.Model.PipelineMobileClaim":
                        IPipelineClaim = JsonConvert.DeserializeObject<MDA.Pipeline.Model.PipelineMobileClaim>(rawXml);
                        break;
                    case "MDA.Pipeline.Model.PipelineMotorClaim":
                        IPipelineClaim = JsonConvert.DeserializeObject<MDA.Pipeline.Model.PipelineMotorClaim>(rawXml);
                        break;
                    case "MDA.Pipeline.Model.PipelinePIClaim":
                        IPipelineClaim = JsonConvert.DeserializeObject<MDA.Pipeline.Model.PipelinePIClaim>(rawXml);
                        break;

                }
            }
            
            return IPipelineClaim;
        }

        private static string SerializePipelineClaim(MDA.Pipeline.Model.IPipelineClaim claimXml, bool useJson)
        {
            string xml = null;

            if (!useJson)
            {
                var ms = new MemoryStream();

                var ser = new DataContractSerializer(claimXml.GetType());

                ser.WriteObject(ms, claimXml);
                ms.Position = 0;
                var sr = new StreamReader(ms);
                xml = sr.ReadToEnd();
            }
            else
            {
                xml = JsonConvert.SerializeObject(claimXml, Newtonsoft.Json.Formatting.None);
            }

            return xml;
        }

        public static string SerializePipelineClaimBatch(MDA.Pipeline.Model.PipelineClaimBatch batch)
        {
            string xml = null;

            var ms = new MemoryStream();

            var ser = new DataContractSerializer(batch.GetType());
            ser.WriteObject(ms, batch);
            ms.Position = 0;
            var sr = new StreamReader(ms);
            xml = sr.ReadToEnd();

            return xml;
        }

        private static string SerializeScores(ClaimScoreResults resultsEntity, bool useJson)
        {
            string xml = null;

            if (resultsEntity != null)
            {
                if (!useJson)
                {
                    var ms = new MemoryStream();

                    var ser = new DataContractSerializer(resultsEntity.GetType());

                    ser.WriteObject(ms, resultsEntity);
                    ms.Position = 0;
                    var sr = new StreamReader(ms);
                    xml = sr.ReadToEnd();
                }
                else
                {
                    xml = JsonConvert.SerializeObject(resultsEntity, Newtonsoft.Json.Formatting.None);
                }
            }

            return xml;
        }

        private static string SerializeLowMessages(List<EntityScoreMessageList> lm, bool useJson)
        {
            string xml = null;

            if (lm != null)
            {
                if (!useJson)
                {
                    var ms = new MemoryStream();

                    var ser = new DataContractSerializer(lm.GetType());
                    ser.WriteObject(ms, lm);
                    ms.Position = 0;
                    var sr = new StreamReader(ms);
                    xml = sr.ReadToEnd();
                }
                else
                {
                    xml = JsonConvert.SerializeObject(lm, Newtonsoft.Json.Formatting.None);
                }
            }

            return xml;
        }

    }
}
