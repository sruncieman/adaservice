﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Data.Entity.Core;
using System.Xml;
using MDA.Pipeline.Model;
using MDA.RiskService.Model;
using MDA.DAL;
using Newtonsoft.Json;
using MDA.Common;

using System.Xml.Serialization;
using LinqKit;
using RiskEngine.Model;
using RiskEngine.Scoring.Entities;
using RiskEngine.Scoring.Model;
using MDA.Common.Server;

namespace MDA.RiskService
{
    public partial class RiskServices
    {

        public List<RiskConfigurationValue> GetRiskDefaultData()
        {
            return (from x in _db.RiskConfigurationValues select x).ToList();
        }

        public List<string> GetRiskDefaultDataAddressForClient(int clientId)
        {
            return (from x in _db.RiskConfigurationValues 
                    where x.RiskConfigurationDescription_Id == 1
                    && x.IsActive
                    && x.RiskClient_Id == clientId
                    select x.ConfigurationValue).ToList();
        }

        public List<string> GetRiskDefaultDataPeopleForClient(int clientId)
        {
            return (from x in _db.RiskConfigurationValues
                    where x.RiskConfigurationDescription_Id == 2
                    && x.IsActive
                    && x.RiskClient_Id == clientId
                    select x.ConfigurationValue).ToList();
        }

        public List<string> GetRiskDefaultDataOrganisationsForClient(int clientId)
        {
            return (from x in _db.RiskConfigurationValues
                    where x.RiskConfigurationDescription_Id == 3
                    && x.IsActive
                    && x.RiskClient_Id == clientId
                    select x.ConfigurationValue).ToList();
        }
    }
}
