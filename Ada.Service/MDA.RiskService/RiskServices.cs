﻿using MDA.DAL;
using MDA.RiskService.Model;
using MDA.Common.Server;
using MDA.RiskService.Interface;

namespace MDA.RiskService
{
    public partial class RiskServices : IRiskServices
    {
        private bool _trace;
        private CurrentContext _ctx;
        private MdaDbContext _db;

        public RiskServices(CurrentContext ctx)
        {
            _ctx = ctx;
            _db = ctx.db as MdaDbContext;
            _trace = ctx.TraceLoad;
        }

        public CurrentContext CurrentContext
        {
            set
            {
                _ctx = value;
                _db = _ctx.db as MdaDbContext;
            }
        }

        #region Membership

        public int GetPasswordFailuresSinceLastSuccess(string userName)
        {
            return new Membership(_ctx).GetPasswordFailuresSinceLastSuccess(userName);
        }

        public bool ValidateUser(string username, string password, string ipAddress)
        {
            return new Membership(_ctx).ValidateUser(username, password, ipAddress);
        }

        public bool IsUserInRole(string userName, string roleName)
        {
            return new Membership(_ctx).IsUserInRole(userName, roleName);
        }

        public string[] GetRolesForUser(string userName)
        {
            return new Membership(_ctx).GetRolesForUser(userName);
        }

        public RiskUserDetails GetUserDetails(int userId, int? clientId)
        {
            return new Membership(_ctx).GetUserDetails(userId, clientId);
        }

        public void CreateUser(string userName, string password, string email, string firstName, string lastName, string telephoneNumber, int clientId, int roleId, int teamId, int? status, bool? autoloader, int? userTypeId)
        {
            new Membership(_ctx).CreateUser(userName, password, email, firstName, lastName, telephoneNumber, clientId, roleId, teamId, status, autoloader, userTypeId);
        }

        public void CreateClient(string clientName, int? statusId, string fileExtensions, int amberThreshold, int redThreshold, int insurersClienId)
        {
            new Membership(_ctx).CreateClient(clientName, statusId, fileExtensions, amberThreshold, redThreshold, insurersClienId);
        }

        public void CreateRiskWord(int riskClientId, string tableName, string fieldName, string lookupWord, string replacementWord, bool replaceWholeString, System.DateTime dateAdded, string searchType)
        {
            new Membership(_ctx).CreateRiskWord(riskClientId, tableName, fieldName, lookupWord, replacementWord, replaceWholeString, dateAdded, searchType);
        }

        public void CreateRiskDefaultData(string ConfigurationValue, bool IsActive, int RiskClient_Id, int RiskConfigurationDescriptionId)
        {
            new Membership(_ctx).CreateRiskDefaultData(ConfigurationValue, IsActive, RiskClient_Id, RiskConfigurationDescriptionId);
        }

        public void DeleteRiskDefaultData(int Id)
        {
            new Membership(_ctx).DeleteRiskDefaultData(Id);
        }

        public void UpdateClient( int iD, string clientName, int? statusId, string fileExtensions, int amberThreshold, int redThreshold, int insurersClienId, bool? recordReserveChanges, int? batchPriority)
        {
            new Membership(_ctx).UpdateClient(iD, clientName, statusId, fileExtensions, amberThreshold, redThreshold, insurersClienId, recordReserveChanges, batchPriority);
        }

        public void UpdateUser(string userName, string password, string email, string firstName, string lastName, string telephoneNumber, int clientId, int roleId, int teamId, int? status, bool? autoloader, int? userTypeId)
        {
            new Membership(_ctx).UpdateUser(userName, password, email, firstName, lastName, telephoneNumber, clientId, roleId, teamId, status, autoloader, userTypeId);
        }

        public void CreateRiskUser2Client(int clientId, int userId)
        {
            new Membership(_ctx).CreateRiskUser2Client(clientId, userId);
        }

        public void DeleteRiskUser2Client(int clientId, int userId)
        {
            new Membership(_ctx).DeleteRiskUser2Client(clientId, userId);
        }

        public bool ChangePassword(string username, string newHashPassword)
        {
            return new Membership(_ctx).ChangePassword(username, newHashPassword);
        }

        public bool IsPasswordUnique(string username, string password)
        {
            return new Membership(_ctx).IsPasswordUnique(username, password);
        }

        #endregion












      
    }
}

                                                                               