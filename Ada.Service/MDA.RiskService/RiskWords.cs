﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Data.Entity.Core;
using System.Xml;
using MDA.Pipeline.Model;
using MDA.RiskService.Model;
using MDA.DAL;
using Newtonsoft.Json;
using MDA.Common;

using System.Xml.Serialization;
using LinqKit;
using RiskEngine.Model;
using RiskEngine.Scoring.Entities;
using RiskEngine.Scoring.Model;
using MDA.Common.Server;

namespace MDA.RiskService
{
    public partial class RiskServices
    {
        public List<RiskWordFilter> GetRiskWordsForClient(int clientId)
        {
            return (from x in _db.RiskWordFilters
                where x.RiskClient_Id == 0
                select x)
                .Union(
                    from x in _db.RiskWordFilters
                    where x.RiskClient_Id == clientId
                    select x).ToList();
        }


        public List<RiskWordFilter> GetAllRiskWords()
        {
            return (from x in _db.RiskWordFilters select x).ToList();
        }

        public void DeleteRiskWord(int id)
        {
            var riskWordToDelete = _db.RiskWordFilters.FirstOrDefault(rw => rw.Id == id);

            _db.RiskWordFilters.Remove(riskWordToDelete);

            _db.SaveChanges();
        }
    }
}
