﻿
using System;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;


namespace MDA.RiskService
{
    public partial class RiskServices
    {
        /// <summary>
        /// Call IBASE Sync
        /// </summary>
        /// <returns>0 - Complete, -1 - Busy, try again later, -2 - Sync Error needs manual intervention, -3 - Migration Error needs manual intervention</returns>
        public int SynchroniseDb()
        {
            //var res = new SqlParameter("SyncSuccess", typeof(int));

            SqlParameter res = new SqlParameter()
            {
                ParameterName = "@SyncSuccess",
                SqlDbType = System.Data.SqlDbType.SmallInt,
                Direction = System.Data.ParameterDirection.Output
            };

            //_db.uspRunSync(res);

            string sql = "exec uspRunSync @SyncSuccess OUTPUT";

            int? t = _db.Database.CommandTimeout;

            _db.Database.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["SyncCommandTimeout"]);

            _db.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.DoNotEnsureTransaction, sql ,res );

            _db.Database.CommandTimeout = t;

            return Convert.ToInt32(res.Value);
        }
    }
}
