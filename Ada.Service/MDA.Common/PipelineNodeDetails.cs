﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.Common
{
    public class PipelineNodeDetails
    {
        public object PipelineEntity { get; set; }
        public string Text { get; set; }

        public List<PipelineNodeDetails> Nodes { get; set; }

        public PipelineNodeDetails(string text, object entity)
        {
            Nodes = new List<PipelineNodeDetails>();

            PipelineEntity = entity;
            Text = text;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
