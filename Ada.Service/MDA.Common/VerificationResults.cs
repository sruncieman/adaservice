﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace MDA.Common
{
    [DataContract]
    public enum MessageType
    {
        [EnumMember]
        Normal = 0,
        [EnumMember]
        Warning,
        [EnumMember]
        Error
    }

    [DataContract]
    public class MessageNode
    {
        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public List<MessageNode> Nodes { get; set; }

        [DataMember]
        public MessageType MessageType { get; set; }

        public MessageNode()
        {
            Text = "init";
            Nodes = new List<MessageNode>();
            MessageType = Common.MessageType.Normal;
        }

        public MessageNode(string text)
        {
            Text = text;
            Nodes = new List<MessageNode>();
            MessageType = Common.MessageType.Normal;
        }

        public MessageNode(string text, MessageType messageType)
        {
            Text = text;
            Nodes = new List<MessageNode>();
            MessageType = messageType;
        }


        public void AddNode(MessageNode node)
        {
            Nodes.Add(node);
        }

        public void AddError(string text)
        {
            Nodes.Add(new MessageNode(text, Common.MessageType.Error));
        }

        public void AddWarning(string text)
        {
            Nodes.Add(new MessageNode(text, Common.MessageType.Warning));
        }

        public void AddNormal(string text)
        {
            Nodes.Add(new MessageNode(text, Common.MessageType.Normal));
        }

        public int CountNodes(Common.MessageType messageType)
        {
            int cnt = (this.MessageType == messageType) ? 1 : 0;

            foreach (var n in this.Nodes)
                cnt += n.CountNodes(messageType);

            return cnt;
        }
    }

    [DataContract]
    public class ProcessingResults
    {
        [DataMember]
        public MessageNode Message { get; set; }

        public bool IsValid 
        { 
            get { return Message.CountNodes(Common.MessageType.Error) == 0; }
        }  

        public int ErrorCount
        {
            get { return Message.CountNodes(Common.MessageType.Error); }
        }

        public int WarningCount
        {
            get { return Message.CountNodes(Common.MessageType.Warning); }
        }

        public ProcessingResults(string rootText)
        {
            Message = new MessageNode(rootText);
        }
    }

    //[DataContract]
    //public class ValidationResults
    //{
    //    [DataMember]
    //    public MessageNode Message { get; set; }

    //    //[DataMember]
    //    //public MessageNode Warnings { get; set; }

    //    [DataMember]
    //    public int success { get; set; }    // 0 = success, any other value is error

    //    public ValidationResults()
    //    {
    //        Message = new MessageNode();
    //        //Warnings = new MessageNode();

    //        success = 0;
    //    }
    //}
}
