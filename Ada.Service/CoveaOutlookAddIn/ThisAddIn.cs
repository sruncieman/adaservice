﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Windows.Forms;
using System.Threading;
using Ionic.Zip;
using System.IO;
using System.Configuration;

namespace CoveaOutlookAddIn
{
    public partial class ThisAddIn
    {
        private string coveaPickupPath = @"\\ukboldat05\projectvf\ADAClients\Covea\";
        
        private void ThisAddIn_Startup(object sender, System.EventArgs e){
            
            this.Application.NewMailEx += new Microsoft.Office.Interop.Outlook
                .ApplicationEvents_11_NewMailExEventHandler(ThisApplication_NewMail);
        }

        private void ThisApplication_NewMail(string EntryID)
        {
            Outlook.MAPIFolder coveaArchiveFolder = null;
            Outlook.MAPIFolder inBox = this.Application.ActiveExplorer()
                .Session.GetDefaultFolder(Outlook
                .OlDefaultFolders.olFolderInbox);

            coveaArchiveFolder = inBox.Folders["CoveaFilesArchive"];
            string[] expectedSenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].Split(',').Select(s => s.Trim()).ToArray();

            // get MailItem for this new mail
            Outlook.Explorers explorers = this.Application.Explorers;
            Outlook.MailItem newMail =
                (Outlook.MailItem)explorers.Application.Session.GetItemFromID(EntryID, System.Reflection.Missing.Value);


            //if (newMail.Subject.Contains("Keoghs daily files part"))
            if (newMail.Subject.Contains(ConfigurationManager.AppSettings["Subject"]) && expectedSenderEmailAddress.Contains(getSenderEmailAddress(newMail)))
            {
                // Covea emails
                if (newMail.Attachments.Count > 0)
                {
                    // Attachments
                    for (int i = 1; i <= newMail
                                       .Attachments.Count; i++)
                    {
                        string fileName = newMail.Attachments[i].FileName;

                        if (!fileName.Contains("mma_files"))
                        {

                            if (fileName.Substring(0, 4) == "MMA1" && fileName.Length == 29)
                            {
                                newMail.Attachments[i].SaveAsFile(coveaPickupPath + fileName);
                            }
                        }
                    }
                }
                
                newMail.Move(coveaArchiveFolder);
            }

            int filesCount = Directory.GetFiles(coveaPickupPath).Count();

            int coveaFileAmount = Convert.ToInt32(ConfigurationManager.AppSettings["CoveaFilesAmount"]);

            if (filesCount == coveaFileAmount)
            {
                bool canZip = true;

                string[] expectedFileNames = ConfigurationManager.AppSettings["CoveaFiles"].Split(',').Select(s => s.Trim()).ToArray();

                // Make sure string date parts of filenames all match
                var firstFile = Directory.GetFiles(coveaPickupPath).FirstOrDefault();
                string[] firstFileDateParts = firstFile.Split('_');
                string firstFileDatePart1 = firstFileDateParts[2];
                string firstFileDatePart2 = firstFileDateParts[3];

                foreach (string file in Directory.GetFiles(coveaPickupPath))
                {
                    string[] dateParts = file.Split('_');
                    string datePart1 = dateParts[2];
                    string datePart2 = dateParts[3];

                    if (datePart1 != firstFileDatePart1 || datePart2 != firstFileDatePart2)
                    {
                        canZip = false;
                        break;
                    }
                    
                   if (!expectedFileNames.Contains(file.Substring(file.Length-29 ,9)))
                   {
                       canZip = false;
                       break;
                   }
                }

                if (canZip)
                {
                    using (ZipFile zip = new ZipFile())
                    {

                        foreach (string file in Directory.GetFiles(coveaPickupPath))
                        {
                            zip.AddFile(file, @"\");
                        }

                        if (zip.Any())
                        {

                            string zipPath = coveaPickupPath + "Covea" + DateTime.Now.ToString("ddMMyyyy") + ".zip";

                            zip.Save(zipPath);
                        }
                    }

                    //Clean up files
                    foreach (string file in Directory.GetFiles(coveaPickupPath))
                    {
                        string temp = file.Substring(file.Length - 4);
                        if (temp.ToUpper() != ".ZIP")
                        {
                            File.Delete(file);
                        }
                    }
                }
            }
        }

        private string getSenderEmailAddress(Outlook.MailItem mail)
        {
            Outlook.AddressEntry sender = mail.Sender;
            string SenderEmailAddress = "";

            if (sender.AddressEntryUserType == Outlook.OlAddressEntryUserType.olExchangeUserAddressEntry || sender.AddressEntryUserType == Outlook.OlAddressEntryUserType.olExchangeRemoteUserAddressEntry)
            {
                Outlook.ExchangeUser exchUser = sender.GetExchangeUser();
                if (exchUser != null)
                {
                    SenderEmailAddress = exchUser.PrimarySmtpAddress;
                }
            }
            else
            {
                SenderEmailAddress = mail.SenderEmailAddress;
            }

            return SenderEmailAddress;
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
