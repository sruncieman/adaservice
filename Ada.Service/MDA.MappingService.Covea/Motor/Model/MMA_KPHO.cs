﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Covea.Motor.Model
{
    [DelimitedRecord("|")]
    public class MMA_KPHO
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Entity;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Telephone;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TelephoneType;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PartyType;
    }
}
