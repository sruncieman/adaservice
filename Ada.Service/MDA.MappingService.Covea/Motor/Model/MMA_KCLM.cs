﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Covea.Motor.Model
{
  
    [DelimitedRecord("|")]
    public class MMA_KCLM
    {

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Entity;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNumber;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? IncidentDt;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FieldIgnored1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FieldIgnored2;
    }
}
