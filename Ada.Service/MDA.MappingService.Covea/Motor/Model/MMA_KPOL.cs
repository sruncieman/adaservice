﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Covea.Motor.Model
{
    [DelimitedRecord("|")]
    public class MMA_KPOL
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Entity;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyNum;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNumber;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PolicyStartDt;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PolicyEndDt;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimType;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string BlankField;
    }
}
