﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Covea.Motor.Model
{
    [DelimitedRecord("|")]
    public class MMA_KVEH
    {
       [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Entity;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehicleReg;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Make;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Model;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FieldIgnored;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehicleInvolvementGroup;
    }
}
