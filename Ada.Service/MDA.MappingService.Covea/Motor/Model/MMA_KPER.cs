﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Covea.Motor.Model
{
    [DelimitedRecord("|")]
    public class MMA_KPER
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Entity;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PartyType;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IgnoredField1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LastName;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DOBDt;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IgnoredField2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IgnoredField3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NI;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehicleInvolvementId;
       
    }
}
