﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Covea.Motor.Model
{
    [DelimitedRecord("|")]
   
    public class MMA_KADD
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Entity;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Street;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Locality;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Town;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string County;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IgnoredField;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PartyType;
    }
}
