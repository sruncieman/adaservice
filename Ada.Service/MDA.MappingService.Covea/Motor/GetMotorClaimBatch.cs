﻿using FileHelpers;
using Ionic.Zip;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.Covea.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Covea.Motor
{
    public class GetMotorClaimBatch
    {
        private static bool boolDebug = false;
        private static string filePath;
        private static string folderPath;
        public void RetrieveMotorClaimBatch(Common.Server.CurrentContext ctx, System.IO.Stream filem, string clientFolder, bool DeleteUploadedFiles, object statusTracking, Func<Common.Server.CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn, Common.ProcessingResults processingResults)
        {

            #region Initialise variables
            DateTime minDate = new DateTime(2007, 1, 1);
            string CoveaAddressFile = string.Empty;
            string CoveaClaimFile = string.Empty;
            string CoveaPersonFile = string.Empty;
            string CoveaTelFile = string.Empty;
            string CoveaPolicyFile = string.Empty;
            string CoveaVehicleFile = string.Empty;
            string filePartDateString = string.Empty;
            string fileDateString = string.Empty;

            //string filePath = null;

            MMA_KADD[] Covea_Address;
            MMA_KCLM[] Covea_Claim;
            MMA_KPER[] Covea_Person;
            MMA_KPHO[] Covea_Tel;
            MMA_KPOL[] Covea_Policy;
            MMA_KVEH[] Covea_Vehicle;

            #endregion

            #region Extract Files from Zip
            if (filem != null)
            {
                try
                {
                    boolDebug = false;

                    using (ZipFile zip1 = ZipFile.Read(filem)) //_filePath))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            var extractTmpPath = string.Concat(clientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);


                            string unpackDirectory = clientFolder;
                            e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);


                            fileDateString = e.FileName;


                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }

                        string[] fileDatePartStringArray = fileDateString.Split('_');
                        filePartDateString = fileDatePartStringArray[2] + "_" + fileDatePartStringArray[3];

                        CoveaAddressFile = clientFolder + @"\MMA1_KADD_" + filePartDateString;
                        CoveaClaimFile = clientFolder + @"\MMA1_KCLM_" + filePartDateString;
                        CoveaPersonFile = clientFolder + @"\MMA1_KPER_" + filePartDateString;
                        CoveaTelFile = clientFolder + @"\MMA1_KPHO_" + filePartDateString;
                        CoveaPolicyFile = clientFolder + @"\MMA1_KPOL_" + filePartDateString;
                        CoveaVehicleFile = clientFolder + @"\MMA1_KVEH_" + filePartDateString;
                    }

                    
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in extraction of files: " + ex);
                }
            }
            else
            {
                
                    boolDebug = true;
                    folderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Covea\Resource\";
                    CoveaAddressFile = folderPath + @"\MMA1_KADD_20140912_040017.txt";
                    CoveaClaimFile = folderPath + @"\MMA1_KCLM_20140912_040017.txt";
                    CoveaPersonFile = folderPath + @"\MMA1_KPER_20140912_040017.txt";
                    CoveaTelFile = folderPath + @"\MMA1_KPHO_20140912_040017.txt";
                    CoveaPolicyFile = folderPath + @"\MMA1_KPOL_20140912_040017.txt";
                    CoveaVehicleFile = folderPath + @"\MMA1_KVEH_20140912_040017.txt";
            }
            #endregion

            DeleteExtractedFiles(clientFolder, DeleteUploadedFiles);
         
          

            #region FileHelper Engines
            try
            {

                FileHelperEngine CoveaAddressEngine = new FileHelperEngine(typeof(MMA_KADD));
                CoveaAddressEngine.BeforeReadRecord += BeforeEvent;
                FileHelperEngine CoveaClaimEngine = new FileHelperEngine(typeof(MMA_KCLM));
                CoveaClaimEngine.BeforeReadRecord += BeforeEvent;
                FileHelperEngine CoveaPersonEngine = new FileHelperEngine(typeof(MMA_KPER));
                CoveaPersonEngine.BeforeReadRecord += BeforeEvent;
                FileHelperEngine CoveaTelephoneEngine = new FileHelperEngine(typeof(MMA_KPHO));
                CoveaTelephoneEngine.BeforeReadRecord += BeforeEvent;
                FileHelperEngine CoveaPolicyEngine = new FileHelperEngine(typeof(MMA_KPOL));
                CoveaPolicyEngine.BeforeReadRecord += BeforeEvent;
                FileHelperEngine CoveaVehicleEngine = new FileHelperEngine(typeof(MMA_KVEH));
                CoveaVehicleEngine.BeforeReadRecord += BeforeEvent;


                

            #endregion

                #region Populate FileHelper Engines with data from files

                Covea_Address = CoveaAddressEngine.ReadFile(CoveaAddressFile) as MMA_KADD[];
                Covea_Claim = CoveaClaimEngine.ReadFile(CoveaClaimFile) as MMA_KCLM[];
                Covea_Person = CoveaPersonEngine.ReadFile(CoveaPersonFile) as MMA_KPER[];
                Covea_Policy = CoveaPolicyEngine.ReadFile(CoveaPolicyFile) as MMA_KPOL[];
                Covea_Vehicle = CoveaVehicleEngine.ReadFile(CoveaVehicleFile) as MMA_KVEH[];
                Covea_Tel = CoveaTelephoneEngine.ReadFile(CoveaTelFile) as MMA_KPHO[];

            }

            catch (Exception ex)
            {
                throw new CustomException("Error in initialising FileHelper Engines: " + ex);
            }

                #endregion

            #region Translate to XML

            List<MMA_KCLM> lstCovea_Claim = new List<MMA_KCLM>();

            if (Covea_Claim != null)
                lstCovea_Claim = Covea_Claim
                    .GroupBy(i => i.ClaimNumber)
                    .Select(g => g.First())
                    .ToList();


            foreach (var record in lstCovea_Claim)
            {
                bool organisationFlag = false;
                var policyData = Covea_Policy.Where(x => x.ClaimNumber == record.ClaimNumber).FirstOrDefault();
                var organisationData = Covea_Person.Where(x => x.ClaimNumber == record.ClaimNumber && x.LastName.ToUpper().Contains("LTD")).ToList();
                var vehicleData = Covea_Vehicle.Where(x => x.ClaimNumber == record.ClaimNumber).ToList();

                List<MMA_KPER> peopleWithNoVehicle = new List<MMA_KPER>();

                #region GeneralClaimData
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                try
                {

                    motorClaim.ClaimNumber = Convert.ToString(record.ClaimNumber);
                    motorClaim.IncidentDate = Convert.ToDateTime(record.IncidentDt);
                    motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general claim data: " + ex);
                }

                #endregion

                #region Policy

                try
                {
                    //motorClaim.Policy.PolicyNumber = record.InsPol;
                    motorClaim.Policy.Insurer = "Covea";
                    if (!string.IsNullOrWhiteSpace(policyData.PolicyNum))
                        motorClaim.Policy.PolicyNumber = policyData.PolicyNum;

                    motorClaim.Policy.PolicyStartDate = Convert.ToDateTime(policyData.PolicyStartDt);
                    motorClaim.Policy.PolicyEndDate = Convert.ToDateTime(policyData.PolicyEndDt);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating policy: " + ex);
                }

                #endregion

                #region Claim Organisations
                if (organisationData.Any())
                {
                    organisationFlag = true;
                    foreach (var orgItem in organisationData)
                    {
                        var organisation = new PipelineOrganisation();
                        var organisationAddress = new PipelineAddress();

                        if (!string.IsNullOrWhiteSpace(orgItem.LastName))
                            organisation.OrganisationName = orgItem.LastName;

                        if (orgItem.PartyType == "CLM0001")
                            organisation.OrganisationType_Id = (int)OrganisationType.PolicyHolder;

                        #region Org Telephone
                        var telephoneData = Covea_Tel.Where(x => x.ClaimNumber == orgItem.ClaimNumber).ToList();

                        foreach (var telephoneNum in telephoneData)
                        {
                            organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = telephoneNum.Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                        }


                        #endregion

                        #region Org Address
                        var addressData = Covea_Address.Where(x => x.ClaimNumber == orgItem.ClaimNumber).FirstOrDefault();
                        if (addressData != null)
                        {

                            organisationAddress.AddressType_Id = (int)AddressLinkType.TradingAddress;
                            
                            if (!string.IsNullOrWhiteSpace(addressData.Street))
                                organisationAddress.Street = addressData.Street;

                            if (!string.IsNullOrWhiteSpace(addressData.Locality))
                                organisationAddress.Locality = addressData.Locality;

                            if (!string.IsNullOrWhiteSpace(addressData.Town))
                                organisationAddress.Locality = addressData.Town;

                            if (!string.IsNullOrWhiteSpace(addressData.County))
                                organisationAddress.County = addressData.County;

                            if (!string.IsNullOrWhiteSpace(addressData.Postcode))
                                organisationAddress.PostCode = addressData.Postcode;

                        }

                        organisation.Addresses.Add(organisationAddress);

                        motorClaim.Organisations.Add(organisation);

                        #endregion

                    }
                }
                #endregion

                #region Vehicle

                try
                {

                    #region People Not In Vehicle
                    var lstInvolvementTypes = new List<string>();

                    foreach (var item in vehicleData)
                    {
                        lstInvolvementTypes.Add(item.VehicleInvolvementGroup);

                        var personDetails = Covea_Person.Where(x => x.ClaimNumber == item.ClaimNumber).ToList();

                        foreach (var personD in personDetails)
                        {
                            if (!lstInvolvementTypes.Contains(personD.VehicleInvolvementId) && !personD.LastName.ToUpper().Contains("LTD"))
                            {
                                peopleWithNoVehicle.Add(personD);
                            }
                        }
                    }
                    #endregion

                    foreach (var vItem in vehicleData)
                    {
                        var personDetails = Covea_Person.Where(x => x.ClaimNumber == vItem.ClaimNumber).ToList();

                        var insuredPeople = personDetails.Where(x => x.VehicleInvolvementId == "1").ToList();
                        var TPPeople = personDetails.Where(x => x.VehicleInvolvementId == "2").ToList();

                        PipelineVehicle insuredVehicle = new PipelineVehicle();
                        PipelineVehicle TPVehicle = new PipelineVehicle();

                        #region Insured Vehicle Details
                        if (vItem.VehicleInvolvementGroup == "1")
                        {
                            if (!string.IsNullOrWhiteSpace(vItem.VehicleReg))
                                insuredVehicle.VehicleRegistration = vItem.VehicleReg;

                            if (!string.IsNullOrWhiteSpace(vItem.Make))
                                insuredVehicle.VehicleMake = vItem.Make;

                            if (!string.IsNullOrWhiteSpace(vItem.Model))
                                insuredVehicle.VehicleModel = vItem.Model;

                            insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id =
                                (int)Incident2VehicleLinkType.InsuredVehicle;
                        }
                        #endregion

                        #region TP Vehicle Details
                        if (vItem.VehicleInvolvementGroup == "2")
                        {
                            if (!string.IsNullOrWhiteSpace(vItem.VehicleReg))
                                TPVehicle.VehicleRegistration = vItem.VehicleReg;

                            if (!string.IsNullOrWhiteSpace(vItem.Make))
                                TPVehicle.VehicleMake = vItem.Make;

                            if (!string.IsNullOrWhiteSpace(vItem.Model))
                                TPVehicle.VehicleModel = vItem.Model;

                            TPVehicle.I2V_LinkData.Incident2VehicleLinkType_Id =
                                (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                        }
                        #endregion


                        if (insuredPeople.Any() && vItem.VehicleInvolvementGroup == "1")
                        {
                            foreach (var insuredP in insuredPeople)
                            {
                                PipelinePerson insuredPerson = new PipelinePerson();
                                PipelineAddress insuredPAddress = new PipelineAddress();

                                var telephoneDetails = Covea_Tel.Where(x => x.ClaimNumber == insuredP.ClaimNumber && x.PartyType == insuredP.PartyType).ToList();
                                var addressDetails = Covea_Address.Where(x => x.ClaimNumber == insuredP.ClaimNumber && x.PartyType == insuredP.PartyType).FirstOrDefault();

                                #region Insured Vehicle Person Details

                                int partyTypeNumber = Convert.ToInt32(insuredP.PartyType.Substring(insuredP.PartyType.Length - 1));

                                if (insuredP.PartyType.Contains("CLM") && partyTypeNumber == 2 && organisationFlag)
                                {
                                    insuredPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    insuredPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (insuredP.PartyType.Contains("CLM") && partyTypeNumber > 2 && organisationFlag)
                                {
                                    insuredPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    insuredPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                }

                                if (insuredP.PartyType.Contains("CLM") && partyTypeNumber == 1 && !organisationFlag)
                                {
                                    insuredPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    insuredPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (insuredP.PartyType.Contains("CLM") && partyTypeNumber > 1 && !organisationFlag)
                                {
                                    insuredPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    insuredPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                }

                                if (insuredP.PartyType.Contains("CLR"))
                                {
                                    insuredPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.LitigationFriend;
                                }

                                if (!string.IsNullOrWhiteSpace(insuredP.FirstName))
                                    insuredPerson.FirstName = insuredP.FirstName;

                                if (!string.IsNullOrWhiteSpace(insuredP.LastName))
                                    insuredPerson.LastName = insuredP.LastName;

                                insuredPerson.DateOfBirth = insuredP.DOBDt;

                                if (!string.IsNullOrWhiteSpace(insuredP.NI))
                                    insuredPerson.NINumbers.Add(new PipelineNINumber() { NINumber1 = insuredP.NI });


                                foreach (var tel in telephoneDetails)
                                {
                                    insuredPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = tel.Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                                }


                                #region Insured Vehicle Person Address
                                if (addressDetails != null)
                                {
                                    insuredPAddress.AddressType_Id = (int)AddressLinkType.CurrentAddress;
                                    if (!string.IsNullOrWhiteSpace(addressDetails.Street))
                                        insuredPAddress.Street = addressDetails.Street;

                                    if (!string.IsNullOrWhiteSpace(addressDetails.Locality))
                                        insuredPAddress.Locality = addressDetails.Locality;

                                    if (!string.IsNullOrWhiteSpace(addressDetails.Town))
                                        insuredPAddress.Town = addressDetails.Town;

                                    if (!string.IsNullOrWhiteSpace(addressDetails.County))
                                        insuredPAddress.County = addressDetails.County;

                                    if (!string.IsNullOrWhiteSpace(addressDetails.Postcode))
                                        insuredPAddress.PostCode = addressDetails.Postcode;

                                #endregion

                                    insuredPerson.Addresses.Add(insuredPAddress);
                                }
                                insuredVehicle.People.Add(insuredPerson);

                                #endregion
                            }
                            
                        }

                        if(vItem.VehicleInvolvementGroup=="1")
                            motorClaim.Vehicles.Add(insuredVehicle);

                        if (TPPeople.Any() && vItem.VehicleInvolvementGroup == "2")
                        {
                            foreach (var TPP in TPPeople)
                            {
                                PipelinePerson TPPerson = new PipelinePerson();
                                PipelineAddress TPPAddress = new PipelineAddress();

                                var telephoneDetails = Covea_Tel.Where(x => x.ClaimNumber == TPP.ClaimNumber && x.PartyType == TPP.PartyType).ToList();
                                var addressDetails = Covea_Address.Where(x => x.ClaimNumber == TPP.ClaimNumber && x.PartyType == TPP.PartyType).FirstOrDefault();

                                #region TP Vehicle Person Details

                                int partyTypeNumber = Convert.ToInt32(TPP.PartyType.Substring(TPP.PartyType.Length - 1));

                                if (TPP.PartyType.Contains("THP") && partyTypeNumber == 1)
                                {
                                    TPPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    TPPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                }

                                if (TPP.PartyType.Contains("THP") && partyTypeNumber > 1)
                                {
                                    TPPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    TPPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                }

                                if (TPP.PartyType.Contains("CLR"))
                                {
                                    TPPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.LitigationFriend;
                                }


                                if (!string.IsNullOrWhiteSpace(TPP.FirstName))
                                    TPPerson.FirstName = TPP.FirstName;

                                if (!string.IsNullOrWhiteSpace(TPP.LastName))
                                    TPPerson.LastName = TPP.LastName;

                                TPPerson.DateOfBirth = TPP.DOBDt;

                                if (!string.IsNullOrWhiteSpace(TPP.NI))
                                    TPPerson.NINumbers.Add(new PipelineNINumber() { NINumber1 = TPP.NI });

                                foreach (var tel in telephoneDetails)
                                {
                                    TPPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = tel.Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                                }


                                #region TP Vehicle Person Address
                                if (addressDetails != null)
                                {
                                    TPPAddress.AddressType_Id = (int)AddressLinkType.CurrentAddress;
                                    if (!string.IsNullOrWhiteSpace(addressDetails.Street))
                                        TPPAddress.Street = addressDetails.Street;

                                    if (!string.IsNullOrWhiteSpace(addressDetails.Locality))
                                        TPPAddress.Locality = addressDetails.Locality;

                                    if (!string.IsNullOrWhiteSpace(addressDetails.Town))
                                        TPPAddress.Town = addressDetails.Town;

                                    if (!string.IsNullOrWhiteSpace(addressDetails.County))
                                        TPPAddress.County = addressDetails.County;

                                    if (!string.IsNullOrWhiteSpace(addressDetails.Postcode))
                                        TPPAddress.PostCode = addressDetails.Postcode;

                                #endregion

                                    TPPerson.Addresses.Add(TPPAddress);
                                }
                                TPVehicle.People.Add(TPPerson);

                                #endregion
                            }
                            
                        }
                        if (vItem.VehicleInvolvementGroup == "2")
                            motorClaim.Vehicles.Add(TPVehicle);
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating vehicle: " + ex);
                }

                #endregion

                #region PeopleWithNoVehicle

                if (!vehicleData.Any())
                {
                    var peopleNoVehicle = Covea_Person.Where(x => x.ClaimNumber == record.ClaimNumber && !x.LastName.ToUpper().Contains("LTD")).ToList();

                    foreach (var item in peopleNoVehicle)
                    {
                        peopleWithNoVehicle.Add(item);
                    }
                }

                PipelineVehicle insuredVeh = new PipelineVehicle();
                foreach (var person in peopleWithNoVehicle.Where(x => x.VehicleInvolvementId == "1"))
                {

                    
                    PipelinePerson personp = new PipelinePerson();
                    PipelineAddress personAddress = new PipelineAddress();
                    // Add Insured Person to Insured Vehicle
                    insuredVeh.I2V_LinkData.Incident2VehicleLinkType_Id =
                        (int)Incident2VehicleLinkType.NoVehicleInvolved;

                    var telephoneDetails = Covea_Tel.Where(x => x.ClaimNumber == person.ClaimNumber && x.PartyType == person.PartyType).ToList();
                    var addressDetails = Covea_Address.Where(x => x.ClaimNumber == person.ClaimNumber && x.PartyType == person.PartyType).FirstOrDefault();

                    #region Insured Vehicle Person Details

                    int partyTypeNumber = Convert.ToInt32(person.PartyType.Substring(person.PartyType.Length - 1));

                    if (person.PartyType.Contains("CLM") && partyTypeNumber == 2 && organisationFlag)
                    {
                        personp.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        personp.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    }

                    if (person.PartyType.Contains("CLM") && partyTypeNumber > 2 && organisationFlag)
                    {
                        personp.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        personp.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    }

                    if (person.PartyType.Contains("CLM") && partyTypeNumber == 1 && !organisationFlag)
                    {
                        personp.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        personp.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    }

                    if (person.PartyType.Contains("CLM") && partyTypeNumber > 1 && !organisationFlag)
                    {
                        personp.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        personp.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    }

                    if (person.PartyType.Contains("CLR"))
                    {
                        personp.I2Pe_LinkData.PartyType_Id = (int)PartyType.LitigationFriend;
                    }

                    if (!string.IsNullOrWhiteSpace(person.FirstName))
                        personp.FirstName = person.FirstName;

                    if (!string.IsNullOrWhiteSpace(person.LastName))
                        personp.LastName = person.LastName;

                    personp.DateOfBirth = person.DOBDt;

                    if (!string.IsNullOrWhiteSpace(person.NI))
                        personp.NINumbers.Add(new PipelineNINumber() { NINumber1 = person.NI });


                    foreach (var tel in telephoneDetails)
                    {
                        personp.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = tel.Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                    }


                    #region Insured Vehicle Person Address
                    if (addressDetails != null)
                    {
                        personAddress.AddressType_Id = (int)AddressLinkType.CurrentAddress;
                        if (!string.IsNullOrWhiteSpace(addressDetails.Street))
                            personAddress.Street = addressDetails.Street;

                        if (!string.IsNullOrWhiteSpace(addressDetails.Locality))
                            personAddress.Locality = addressDetails.Locality;

                        if (!string.IsNullOrWhiteSpace(addressDetails.Town))
                            personAddress.Town = addressDetails.Town;

                        if (!string.IsNullOrWhiteSpace(addressDetails.County))
                            personAddress.County = addressDetails.County;

                        if (!string.IsNullOrWhiteSpace(addressDetails.Postcode))
                            personAddress.PostCode = addressDetails.Postcode;

                    #endregion

                        personp.Addresses.Add(personAddress);
                    }

                    insuredVeh.People.Add(personp);
                    
                }

                if(insuredVeh.People.Any())
                    motorClaim.Vehicles.Add(insuredVeh);

                PipelineVehicle TPVeh = new PipelineVehicle();
                foreach (var person in peopleWithNoVehicle.Where(x => x.VehicleInvolvementId == "2"))
                {

                    
                    PipelinePerson TPPerson = new PipelinePerson();
                    PipelineAddress TPPAddress = new PipelineAddress();

                    // Add TP Person to TP Vehicle

                    TPVeh.I2V_LinkData.Incident2VehicleLinkType_Id =
                        (int)Incident2VehicleLinkType.NoVehicleInvolved;



                    var telephoneDetails = Covea_Tel.Where(x => x.ClaimNumber == person.ClaimNumber && x.PartyType == person.PartyType).ToList();
                    var addressDetails = Covea_Address.Where(x => x.ClaimNumber == person.ClaimNumber && x.PartyType == person.PartyType).FirstOrDefault();

                    #region TP Vehicle Person Details

                    int partyTypeNumber = Convert.ToInt32(person.PartyType.Substring(person.PartyType.Length - 1));

                    if (person.PartyType.Contains("THP") && partyTypeNumber == 1)
                    {
                        TPPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                        TPPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                    }

                    if (person.PartyType.Contains("THP") && partyTypeNumber > 1)
                    {
                        TPPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                        TPPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                    }

                    if (person.PartyType.Contains("CLR"))
                    {
                        TPPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.LitigationFriend;
                    }


                    if (!string.IsNullOrWhiteSpace(person.FirstName))
                        TPPerson.FirstName = person.FirstName;

                    if (!string.IsNullOrWhiteSpace(person.LastName))
                        TPPerson.LastName = person.LastName;

                    TPPerson.DateOfBirth = person.DOBDt;

                    if (!string.IsNullOrWhiteSpace(person.NI))
                        TPPerson.NINumbers.Add(new PipelineNINumber() { NINumber1 = person.NI });

                    foreach (var tel in telephoneDetails)
                    {
                        TPPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = tel.Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });
                    }


                    #region TP Vehicle Person Address
                    if (addressDetails != null)
                    {
                        TPPAddress.AddressType_Id = (int)AddressLinkType.CurrentAddress;
                        if (!string.IsNullOrWhiteSpace(addressDetails.Street))
                            TPPAddress.Street = addressDetails.Street;

                        if (!string.IsNullOrWhiteSpace(addressDetails.Locality))
                            TPPAddress.Locality = addressDetails.Locality;

                        if (!string.IsNullOrWhiteSpace(addressDetails.Town))
                            TPPAddress.Town = addressDetails.Town;

                        if (!string.IsNullOrWhiteSpace(addressDetails.County))
                            TPPAddress.County = addressDetails.County;

                        if (!string.IsNullOrWhiteSpace(addressDetails.Postcode))
                            TPPAddress.PostCode = addressDetails.Postcode;

                    #endregion

                        TPPerson.Addresses.Add(TPPAddress);
                    }
                    TPVeh.People.Add(TPPerson);
                   
                }
                if (TPVeh.People.Any())
                    motorClaim.Vehicles.Add(TPVeh);


                    #endregion
                    #endregion

                #endregion

                #region Insured Vehicle and Driver Check

                bool insuredVehicleCheck = false;
                bool insuredDriverCheck = false;

                foreach (var vehicle in motorClaim.Vehicles)
                {
                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                    {
                        insuredVehicleCheck = true;

                        foreach (var person in vehicle.People)
                        {
                            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                            {
                                insuredDriverCheck = true;
                            }
                        }
                    }
                }

                if (insuredVehicleCheck == false)
                {
                    PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                    defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                    }

                    motorClaim.Vehicles.Add(defaultInsuredVehicle);
                }
                else
                {
                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        foreach (var vehicle in motorClaim.Vehicles)
                        {
                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                            {

                                vehicle.People.Add(defaultInsuredDriver);

                            }

                        }

                    }
                }

                #endregion

                if (ProcessClaimFn(ctx, motorClaim, statusTracking) == -1) return;
            }

            #endregion


            // Remove extracted zip files
            DeleteExtractedZipFiles(clientFolder, true);
        }

        



        private void BeforeEvent(EngineBase engine, FileHelpers.Events.BeforeReadEventArgs<object> e)
        {
            // skip any bad lines
            if (!e.RecordLine.Contains("|"))
                e.SkipThisRecord = true;

        }

        private static void DeleteExtractedFiles(string clientFolder, bool deleteExtractedFiles)
        {
            if (deleteExtractedFiles)
            {
                try
                {
                    string[] filePaths = Directory.GetFiles(clientFolder);
                    foreach (string file in filePaths)
                    {
                        File.Delete(file);
                    }
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in deleting extracted files: " + ex);
                }
            }
        }

        private void DeleteExtractedZipFiles(string clientFolder, bool deleteFiles)
        {
            if (deleteFiles)
            {
                try
                {
                    string[] filePaths = Directory.GetFiles(clientFolder);
                    foreach (string file in filePaths)
                    {
                        string extension = Path.GetExtension(file);
                        if (extension.ToUpper() != ".ZIP")
                        {
                            File.SetAttributes(file, FileAttributes.Normal);
                            File.Delete(file);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in deleting extracted files: " + ex);
                }
            }
        }


    }
}
