﻿CREATE TABLE [dbo].[_AuditData] (
    [Audit_ID]      UNIQUEIDENTIFIER NOT NULL,
    [field_ID]      INT              NOT NULL,
    [value]         NVARCHAR (MAX)   NULL,
    [SCC]           NVARCHAR (255)   NULL,
    [SCC_New]       NVARCHAR (255)   NULL,
    [Unique_ID]     NVARCHAR (50)    NOT NULL,
    [External_ID]   NVARCHAR (50)    NULL,
    [Table_ID_End1] INT              NULL,
    [Table_ID_End2] INT              NULL,
    CONSTRAINT [PK__AuditData] PRIMARY KEY CLUSTERED ([Audit_ID] ASC, [field_ID] ASC, [Unique_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [_dta_index__AuditData_16_5575058__K5_K1]
    ON [dbo].[_AuditData]([Unique_ID] ASC, [Audit_ID] ASC);

