﻿CREATE TABLE [dbo].[_AuditCodes] (
    [Audit_ID]     UNIQUEIDENTIFIER NOT NULL,
    [Unique_ID]    NVARCHAR (50)    NOT NULL,
    [CodeGroup_ID] INT              NOT NULL,
    [Field_ID]     INT              NOT NULL,
    [Value]        NVARCHAR (550)   NULL,
    CONSTRAINT [PK__AuditCodes] PRIMARY KEY CLUSTERED ([Audit_ID] ASC, [Unique_ID] ASC, [Field_ID] ASC)
);

