﻿CREATE TABLE [dbo].[_AuditLog] (
    [Action_Type]   INT            NULL,
    [User_Name]     NVARCHAR (255) NULL,
    [Network_Login] NVARCHAR (255) NULL,
    [Detail]        NTEXT          NULL,
    [Date_Time]     DATETIME       NULL,
    [Record_ID]     NVARCHAR (50)  NULL,
    [External_ID]   NVARCHAR (50)  NULL,
    [Location]      NVARCHAR (50)  NULL
);


GO
CREATE CLUSTERED INDEX [idxDateTime]
    ON [dbo].[_AuditLog]([Date_Time] ASC);

