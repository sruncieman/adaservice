﻿CREATE TABLE [dbo].[_Audit] (
    [Audit_ID]             UNIQUEIDENTIFIER NOT NULL,
    [table_ID]             INT              NOT NULL,
    [user_name]            NVARCHAR (255)   NOT NULL,
    [machine_Name]         NVARCHAR (255)   NOT NULL,
    [OS_user_name]         NVARCHAR (255)   NOT NULL,
    [reason]               NVARCHAR (255)   NULL,
    [IBase_user]           BIT              NOT NULL,
    [date_time]            DATETIME         NOT NULL,
    [location]             NVARCHAR (50)    NULL,
    [Database_Action_Type] TINYINT          NOT NULL,
    [IsLink]               BIT              NOT NULL,
    [Batch_ID]             UNIQUEIDENTIFIER NULL,
    [Action_Type]          INT              NOT NULL,
    CONSTRAINT [PK__Audit] PRIMARY KEY CLUSTERED ([Audit_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [_dta_index__Audit_16_2121058592__K3_K8_K2]
    ON [dbo].[_Audit]([user_name] ASC, [date_time] ASC, [table_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [idxUserName]
    ON [dbo].[_Audit]([user_name] ASC);


GO
CREATE NONCLUSTERED INDEX [idxDateTime]
    ON [dbo].[_Audit]([date_time] ASC, [Audit_ID] ASC);

