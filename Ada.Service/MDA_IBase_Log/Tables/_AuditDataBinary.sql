﻿CREATE TABLE [dbo].[_AuditDataBinary] (
    [Audit_ID]      UNIQUEIDENTIFIER NOT NULL,
    [field_ID]      INT              NOT NULL,
    [value]         VARBINARY (MAX)  NULL,
    [SCC]           NVARCHAR (255)   NULL,
    [SCC_New]       NVARCHAR (255)   NULL,
    [Unique_ID]     NVARCHAR (50)    NOT NULL,
    [External_ID]   NVARCHAR (50)    NULL,
    [Binding]       NVARCHAR (50)    NULL,
    [Table_ID_End1] INT              NULL,
    [Table_ID_End2] INT              NULL,
    CONSTRAINT [PK__AuditDataBinary] PRIMARY KEY CLUSTERED ([Audit_ID] ASC, [field_ID] ASC, [Unique_ID] ASC)
);

