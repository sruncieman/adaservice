﻿CREATE TABLE [dbo].[_AuditLinkTableID] (
    [Audit_ID]      UNIQUEIDENTIFIER NOT NULL,
    [Unique_ID]     NVARCHAR (50)    NOT NULL,
    [Table_ID_End1] INT              NOT NULL,
    [Table_ID_End2] INT              NOT NULL,
    CONSTRAINT [PK__AuditLinkTableID] PRIMARY KEY CLUSTERED ([Audit_ID] ASC, [Unique_ID] ASC)
);

