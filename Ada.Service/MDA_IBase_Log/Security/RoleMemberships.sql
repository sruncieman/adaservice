﻿EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'i2iBaseSSE';


GO
EXECUTE sp_addrolemember @rolename = N'db_ddladmin', @membername = N'KNET\SQL-ADA-ReadAccess';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'KNET\SQL-ADA-ReadAccess';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'guest';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'KNET\SQL-ADA-ReadAccess';

