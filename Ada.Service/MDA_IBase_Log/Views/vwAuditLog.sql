﻿CREATE VIEW dbo.vwAuditLog 
AS  
SELECT     Action_Type, User_Name, Network_Login, Detail, Date_Time, Record_ID, External_ID, Location, NULL AS Audit_ID  
FROM         _AuditLog  
Union All  
SELECT     Action_Type, user_name, Network_Login, Detail, date_time, Record_ID, External_ID, location, Audit_ID  
FROM         vwAudit