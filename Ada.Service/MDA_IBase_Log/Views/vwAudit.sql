﻿CREATE VIEW dbo.vwAudit AS  SELECT  DISTINCT    dbo.vw_GroupedAudits.Action_Type, dbo._Audit.user_name, ISNULL(dbo._Audit.machine_Name, '') + ':' + ISNULL(dbo._Audit.OS_user_name, '') AS Network_Login,
          CASE vw_GroupedAudits.Action_Type WHEN 408 THEN NULL ELSE reason END AS Detail, dbo._Audit.date_time,
          CASE vw_GroupedAudits.Action_Type WHEN 408 THEN NULL ELSE Unique_ID END AS Record_ID, dbo._Audit.location,
          CASE vw_GroupedAudits.Action_Type WHEN 408 THEN NULL ELSE External_ID END AS External_ID, dbo._Audit.Audit_ID
 FROM         dbo._Audit INNER JOIN
          dbo.vw_GroupedAudits ON dbo._Audit.Audit_ID = dbo.vw_GroupedAudits.Group_Audit_ID
