﻿CREATE VIEW [dbo].[vwSCC]
 AS
 SELECT     _AuditData.Audit_ID, _AuditData.SCC, _AuditData.SCC_New, _AuditData.field_ID, _Field.Table_ID
 FROM         _AuditData INNER JOIN
          _Field ON _AuditData.field_ID = _Field.Field_ID
 Union
 SELECT     _AuditDataBinary.Audit_ID, _AuditDataBinary.SCC, _AuditDataBinary.SCC_New, _AuditDataBinary.field_ID, _Field_1.Table_ID
 FROM         _AuditDataBinary INNER JOIN
          _Field AS _Field_1 ON _AuditDataBinary.field_ID = _Field_1.Field_ID