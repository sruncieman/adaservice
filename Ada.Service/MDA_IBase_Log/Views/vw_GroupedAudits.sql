﻿CREATE VIEW [dbo].vw_GroupedAudits AS SELECT  _Audit.Batch_ID, CASE _Audit.Action_Type WHEN 208 THEN '' ELSE _AuditData.Unique_ID END As Unique_ID, MIN(CAST(_Audit.Audit_ID AS varchar(50))) AS Group_Audit_ID, MIN(_Audit.Action_Type) AS Action_Type,  
     MIN(_AuditData.External_ID) AS External_ID
 FROM         _Audit INNER JOIN
       _AuditData ON _Audit.Audit_ID = _AuditData.Audit_ID
   GROUP BY _Audit.Batch_ID, _AuditData.Unique_ID,_Audit.Action_Type
 HAVING      (NOT (_Audit.Batch_ID IS NULL))
 UNION
 SELECT     _Audit.Batch_ID, _AuditData.Unique_ID, _Audit.Audit_ID AS Group_Audit_ID, _Audit.Action_Type, _AuditData.External_ID
 FROM         _Audit INNER JOIN
      _AuditData ON _Audit.Audit_ID = _AuditData.Audit_ID
 WHERE     (_Audit.Batch_ID IS NULL)
 UNION 
 SELECT     _Audit.Batch_ID, CAST(_AuditCodes.CodeGroup_ID As nvarchar(255)) AS Unique_ID, MIN(CAST(_Audit.Audit_ID AS varchar(50))) AS Group_Audit_ID, _Audit.Action_Type, '' AS External_ID
 FROM         _Audit INNER JOIN
          _AuditCodes ON _Audit.Audit_ID = _AuditCodes.Audit_ID
 GROUP BY _Audit.Batch_ID, _AuditCodes.CodeGroup_ID, _Audit.Batch_ID, _Audit.Action_Type
 HAVING      (_Audit.Action_Type = 409)
