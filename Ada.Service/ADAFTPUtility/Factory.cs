﻿using ADAFTPUtility.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADAFTPUtility
{
    public static class Factory
    {
        public static BaseClientFTP Get(string client)
        {
            string clientName;

            clientName = client.Replace("-", "");

            switch (clientName.ToUpper())
            {
                case "MULSANNE":
                    return new MulsanneFTP(clientName);
                case "ACE":
                    return new AceFTP(clientName);
                case "ADMIRAL":
                    return new AdmiralFTP(clientName);
                case "LIBERTY":
                    return new LibertyFTP(clientName);
                case "First Central":
                    return new FirstCentralFTP(clientName);
                case "DIRECTLINE":
                    return new DirectLineFTP(clientName);
                case "Eldon":
                    return new EldonFTP(clientName);
                case "LV=":
                    return new LVFTP(clientName);
                case "HAVEN":
                    return new HavenFTP(clientName);
                case "ESURE":
                    return new EsureFTP(clientName);
                case "BROKERDIRECT":
                    return new BrokerDirectFTP(clientName);
                case "HASTINGSDIRECT":
                    return new HastingsFTP(clientName);
                default:
                    return null;
            }
        }
    }
}
