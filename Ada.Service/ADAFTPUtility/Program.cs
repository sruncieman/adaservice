﻿using System;
using System.Configuration;
using WinSCP;

namespace ADAFTPUtility
{
    public class Program
    {
        static void Main(string[] args)
        {
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["ActiveClientName"]))
            {
                BaseClientFTP.SendEmail(false, new Exception("No active client set", null), null);
                return;
            }
            args = new string[] { ConfigurationManager.AppSettings["ActiveClientName"] };

            try
            {
                if (args == null || args.Length != 1) { throw new ArgumentException("Client name not found in Command Argument");}
                
                    var ftpClient = Factory.Get(args[0]);

                    if (ftpClient == null) {  throw new ArgumentException("Client [" + args[0] + "] not found.");}

                    var sessionOptions = BaseClientFTP.ConfigureSetupOptions();

                    using (var session = new Session())
                    {
                        session.SessionLogPath = BaseClientFTP.LogFileLocation + "Log.txt";
                        session.Open(sessionOptions);
                 
                        ftpClient.DownloadNewFiles(session);
                    
                        if (ConfigurationManager.AppSettings["DeleteArchiveFiles"]=="1")
                            ftpClient.DeleteArchiveFiles(ftpClient.ClientMonitoredArchiveFolder);
                    }

                BaseClientFTP.SendEmail(true, null, args[0].Replace("-", ""));
            }

            catch (Exception ex)
            {
                BaseClientFTP.SendEmail(false, ex, args[0].Replace("-", ""));
            }
        }
    }
}
