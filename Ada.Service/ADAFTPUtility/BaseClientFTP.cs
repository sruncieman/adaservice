﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using WinSCP;

namespace ADAFTPUtility
{
    public abstract class BaseClientFTP
    {
        public string MonitoredFolderTxt { get; set; }
        public string ArchiveFolderTxt { get; set; }

        public string NetworkPath { get; set; }
        public string FtpClient { get; set; }
        public string ClientMonitoredArchiveFolder { get; set; }
        public string StrRemoteDirectory { get; set; }
        public string DestinationPath { get; set; }

        public static string ProtocolType { get; set; }

        public static string HostKeyFingerprint { get; set; }

        public static string LogFileLocation { get; set; }
        
        public BaseClientFTP(string client)
        {
            this.FtpClient = client;
        }

        public static SessionOptions ConfigureSetupOptions()
        {
            var protocolConfigValue = ProtocolType;

            var sessionOptions = new SessionOptions
            {
                Protocol = (Protocol)Enum.Parse(typeof(Protocol), protocolConfigValue),
                HostName = ConfigurationManager.AppSettings["HostName"],
                UserName = ConfigurationManager.AppSettings["UserName"],
                Password = ConfigurationManager.AppSettings["Password"],
                PortNumber = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                SshHostKeyFingerprint = HostKeyFingerprint
            };
            return sessionOptions;
        }

        public static string GetNewConnection()
        {
            return ConfigurationManager.ConnectionStrings["MobileConn"].ConnectionString;
        }

        public virtual void DownloadNewFiles(Session session)
        {
            try
            {
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary;
                session.FileTransferred += FilesTransferred;
                SynchronizationResult synchronizationResult;
                synchronizationResult = session.SynchronizeDirectories(SynchronizationMode.Local, this.DestinationPath, this.StrRemoteDirectory, false, false, SynchronizationCriteria.Time, transferOptions);

                foreach (var download in synchronizationResult.Downloads.AsQueryable())
                {
                    if (download.Error == null)
                    {
                        Console.WriteLine("Download of {0} succeeded, removing from source", download.FileName);
                        var removalResult = session.RemoveFiles(session.EscapeFileMask(download.FileName));

                        if (removalResult.IsSuccess)
                        {
                            Console.WriteLine("Removing of file {0} succeeded", download.FileName);
                        }
                        else
                        {
                            Console.WriteLine("Removing of file {0} failed: {1}", download.FileName, removalResult.Failures.First());
                        }
                    }

                    else
                    {
                        Console.WriteLine("Download of {0} failed: {1}", download.FileName, download.Error.Message);
                    }
                }
               
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error: {0}", ex);
            }

            

        }

        public virtual void FilesTransferred(object sender, TransferEventArgs e)
        {
            if (e.Error == null)
            {
                Console.WriteLine("Upload of {0} succeeded", e.FileName);
            }
            else
            {
                Console.WriteLine("Upload of {0} failed: {1}", e.FileName, e.Error);
            }

            if (e.Chmod != null)
            {
                if (e.Chmod.Error == null)
                {
                    Console.WriteLine("Permisions of {0} set to {1}", e.Chmod.FileName, e.Chmod.FilePermissions);
                }
                else
                {
                    Console.WriteLine("Setting permissions of {0} failed: {1}", e.Chmod.FileName, e.Chmod.Error);
                }
            }
            else
            {
                Console.WriteLine("Permissions of {0} kept with their defaults", e.Destination);
            }

            if (e.Touch != null)
            {
                if (e.Touch.Error == null)
                {
                    Console.WriteLine("Timestamp of {0} set to {1}", e.Touch.FileName, e.Touch.LastWriteTime);
                }
                else
                {
                    Console.WriteLine("Setting timestamp of {0} failed: {1}", e.Touch.FileName, e.Touch.Error);
                }
            }
            else
            {
                // This should never happen with Session.SynchronizeDirectories
                Console.WriteLine("Timestamp of {0} kept with its default (current time)", e.Destination);
            }
        }

        public static void SendEmail(bool result, Exception ex, string clientName)
        {
            MailMessage mail = result ? new MailMessage("ADAClients@keoghs.co.uk", ConfigurationManager.AppSettings["SuccessRecipients"]) : new MailMessage("ADAClients@keoghs.co.uk", ConfigurationManager.AppSettings["ErrorRecipients"]);
            
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Host = ConfigurationManager.AppSettings["Smtp"];

            if (result)
            {
                mail.Subject = string.Format("{0} FTP Success.",clientName);
                mail.Body = "FTP sync was successful." + Environment.NewLine + Environment.NewLine;
            }
            else
            {
                mail.Subject = string.Format("{0} FTP Failure.",clientName);
                mail.Body = "FTP sync was unsuccessful."  + Environment.NewLine;
                mail.Body += ex;
            }

            client.Send(mail);
        }

        public virtual void DeleteArchiveFiles(string clientArchiveFolder)
        {
            int retentionPeriod =  Convert.ToInt32(ConfigurationManager.AppSettings["ArchiveRetentionPeriod"]);
            DateTime retentionDate = DateTime.Now.AddDays(-retentionPeriod);
            DirectoryInfo dir = new DirectoryInfo(clientArchiveFolder);
            
            foreach (FileInfo fi in dir.GetFiles())
            {
                string path = fi.Directory + @"\" + fi.Name;

                var fileCreated = fi.CreationTime;
                if (fileCreated < retentionDate)
                {
                    fi.Delete();
                }
            }
        }
    }
}
