﻿using System.Configuration;

namespace ADAFTPUtility.Clients
{
    class HavenFTP : BaseClientFTP
    {
        public HavenFTP(string client) : base(client)
        {
            NetworkPath = ConfigurationManager.AppSettings["HavenNetworkAddress"];
            MonitoredFolderTxt = ConfigurationManager.AppSettings["HavenMonitoredFolderText"];
            ArchiveFolderTxt = ConfigurationManager.AppSettings["HavenArchivedFolderText"];
            DestinationPath = NetworkPath + @"\" + MonitoredFolderTxt;
            StrRemoteDirectory = ConfigurationManager.AppSettings["HavenRemoteDir"];
            ClientMonitoredArchiveFolder = NetworkPath + @"\" + ArchiveFolderTxt;
            ProtocolType = ConfigurationManager.AppSettings["HavenFtpProctocol"];
            HostKeyFingerprint = ConfigurationManager.AppSettings["HavenHostKeyFingerprint"];
            LogFileLocation = ConfigurationManager.AppSettings["HavenLogFileLocation"];
        }

    }
}
