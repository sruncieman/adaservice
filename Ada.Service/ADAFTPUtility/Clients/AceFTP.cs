﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WinSCP;

namespace ADAFTPUtility.Clients
{
    public class AceFTP : BaseClientFTP
    {
        public AceFTP(string client) : base (client)
        {
            this.NetworkPath = ConfigurationManager.AppSettings["ACENetworkAddress"];
            this.MonitoredFolderTxt = ConfigurationManager.AppSettings["ACEMonitoredFolderText"];
            this.ArchiveFolderTxt =  ConfigurationManager.AppSettings["ACEArchivedFolderText"];
            this.DestinationPath = NetworkPath + @"\" + client + this.MonitoredFolderTxt;
            this.StrRemoteDirectory = ConfigurationManager.AppSettings["ACERemoteDir"];
            this.ClientMonitoredArchiveFolder = NetworkPath + @"\" + client + this.ArchiveFolderTxt;
            ProtocolType = ConfigurationManager.AppSettings["ACEFtpProctocol"];
            HostKeyFingerprint = ConfigurationManager.AppSettings["ACEHostKeyFingerprint"];
            LogFileLocation = ConfigurationManager.AppSettings["ACELogFileLocation"];
        }

        public override void DownloadNewFiles(WinSCP.Session session)
        {
            try
            {
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary;
                session.FileTransferred += FilesTransferred;
                SynchronizationResult synchronizationResult;
                synchronizationResult = session.SynchronizeDirectories(SynchronizationMode.Local, this.DestinationPath, this.StrRemoteDirectory, false, false, SynchronizationCriteria.Time, transferOptions);
                List<string> transferredFiles = new List<string>();

                foreach (var download in synchronizationResult.Downloads.AsQueryable())
                {
                    if (download.Error == null)
                    {
                        Console.WriteLine("Download of {0} succeeded, removing from source", download.FileName);
                        var removalResult = session.RemoveFiles(session.EscapeFileMask(download.FileName));

                        if (removalResult.IsSuccess)
                        {
                            Console.WriteLine("Removing of file {0} succeeded", download.FileName);
                            transferredFiles.Add(download.FileName);
                        }
                        else
                        {
                            Console.WriteLine("Removing of file {0} failed: {1}", download.FileName, removalResult.Failures.First());
                        }
                    }

                    else
                    {
                        Console.WriteLine("Download of {0} failed: {1}", download.FileName, download.Error.Message);
                    }
                }

                int aceFileAmount = Convert.ToInt32(ConfigurationManager.AppSettings["AceFilesAmount"]);

                if (transferredFiles.Count() == aceFileAmount)
                {
                    Console.WriteLine("Attempt to run SQL Job - ADA - ACEO2DataExtractionAndFileGeneration");

                    try
                    {
                        string connectionString = GetNewConnection();

                        using (SqlConnection conn = new SqlConnection(connectionString))
                        {
                            SqlCommand cmd = new SqlCommand("msdb.dbo.sp_start_job", conn);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@Job_name", "ADA - ACEO2DataExtractionAndFileGeneration");

                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }

                        Console.WriteLine("Successfully ran SQL Job - ADA - ACEO2DataExtractionAndFileGeneration");
                        Thread.Sleep(2000);
                    }

                    catch(Exception ex)
                    {
                        throw new ArgumentException(ex.InnerException.ToString());
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex);
            }
           
        }

        public override void FilesTransferred(object sender, TransferEventArgs e)
        {
            base.FilesTransferred(sender, e);
        }
    }
}
