﻿using System.Configuration;

namespace ADAFTPUtility.Clients
{
    class HastingsFTP : BaseClientFTP
    {
        public HastingsFTP(string client)
            : base(client)
        {
            NetworkPath = ConfigurationManager.AppSettings["HastingsNetworkAddress"];
            MonitoredFolderTxt = ConfigurationManager.AppSettings["HastingsMonitoredFolderText"];
            ArchiveFolderTxt = ConfigurationManager.AppSettings["HastingsArchivedFolderText"];
            DestinationPath = NetworkPath + @"\" + MonitoredFolderTxt;
            StrRemoteDirectory = ConfigurationManager.AppSettings["HastingsRemoteDir"];
            ClientMonitoredArchiveFolder = NetworkPath + @"\" + ArchiveFolderTxt;
            ProtocolType = ConfigurationManager.AppSettings["HastingsFtpProctocol"];
            HostKeyFingerprint = ConfigurationManager.AppSettings["HastingsHostKeyFingerprint"];
            LogFileLocation = ConfigurationManager.AppSettings["HastingsLogFileLocation"];
        }
    }
}
