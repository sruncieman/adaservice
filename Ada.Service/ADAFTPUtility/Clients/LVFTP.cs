﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using WinSCP;

namespace ADAFTPUtility.Clients
{
    // ReSharper disable once InconsistentNaming
    public class LVFTP : BaseClientFTP
    {
        public LVFTP(string client) : base(client)
        {
            NetworkPath = ConfigurationManager.AppSettings["LVNetworkAddress"];
            StrRemoteDirectory = ConfigurationManager.AppSettings["LVRemoteDir"];            
            ProtocolType = ConfigurationManager.AppSettings["LVFtpProctocol"];
            HostKeyFingerprint = ConfigurationManager.AppSettings["LVHostKeyFingerprint"];
            LogFileLocation = ConfigurationManager.AppSettings["LVLogFileLocation"];
        }

        public override void DownloadNewFiles(Session session)
        {
            if (session == null) throw new ArgumentNullException("session");

            try
            {
                var transferOptions = new TransferOptions {TransferMode = TransferMode.Binary};

                session.FileTransferred += FilesTransferred;
               
                var directory = session.ListDirectory(StrRemoteDirectory);

                if (directory != null && directory.Files != null)
                foreach (RemoteFileInfo fileInfo in directory.Files)
                {
                    if (fileInfo != null)
                    {
                        var extension = Path.GetExtension(fileInfo.Name);
                        if (string.Compare(extension, ".txt", StringComparison.OrdinalIgnoreCase) == 0)
                        {
                            if (fileInfo.Name.Contains("LVFS_MBC2_GIOS"))
                            {
                                MonitoredFolderTxt = ConfigurationManager.AppSettings["LVGiosMonitoredFolderText"];
                                ArchiveFolderTxt = ConfigurationManager.AppSettings["LVGiosArchivedFolderText"];
                     
                                DestinationPath = NetworkPath + @"\" + MonitoredFolderTxt;
                            
                                transferOptions.FileMask = "LVFS_MBC2_GIOS_????????.txt";

                                SyncFiles(session, transferOptions);
                            }
                            else if (fileInfo.Name.Contains("LVFS_MDC4_CLCENTER"))
                            {
                                MonitoredFolderTxt = ConfigurationManager.AppSettings["LVCCMonitoredFolderText"];
                                ArchiveFolderTxt = ConfigurationManager.AppSettings["LVCCArchivedFolderText"];

                                DestinationPath = NetworkPath + @"\" + MonitoredFolderTxt;

                                transferOptions.FileMask = "LVFS_MDC4_CLCENTER_????????.txt";

                                SyncFiles(session, transferOptions);
                            }
                        }
                    }
                }
            }
          
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex);
            }
        }

        private void SyncFiles(Session session, TransferOptions transferOptions)
        {
            if (session == null) throw new ArgumentNullException("session");
            if (transferOptions == null) throw new ArgumentNullException("transferOptions");

            var synchronizationResult = session.SynchronizeDirectories(SynchronizationMode.Local, DestinationPath, StrRemoteDirectory, false, false, SynchronizationCriteria.Time, transferOptions);

            if (synchronizationResult == null || synchronizationResult.Downloads == null) return;

            foreach (var download in synchronizationResult.Downloads.AsQueryable())
            {
                if (download != null && download.Error == null)
                {
                    Console.WriteLine("Download of {0} succeeded, removing from source", download.FileName);
                    var removalResult = session.RemoveFiles(session.EscapeFileMask(download.FileName));

                    if (removalResult != null && removalResult.IsSuccess)
                    {
                        Console.WriteLine("Removing of file {0} succeeded", download.FileName);
                    }
                    else
                    {
                        if (removalResult != null && removalResult.Failures != null)
                            Console.WriteLine("Removing of file {0} failed: {1}", download.FileName, removalResult.Failures.First());
                        else
                            Console.WriteLine("Removing of file {0} failed.", download.FileName);
                    }
                }

                else
                {
                    if (download != null)
                        Console.WriteLine("Download of {0} failed: {1}", download.FileName, download.Error.Message);
                    else
                        Console.WriteLine("Download failed - no download: {0}", DateTime.Now);
                }
            }
        }
    }
}
