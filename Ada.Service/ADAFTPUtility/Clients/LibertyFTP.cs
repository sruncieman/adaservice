﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinSCP;

namespace ADAFTPUtility.Clients
{
    public class LibertyFTP : BaseClientFTP
    {
        public LibertyFTP(string client) : base(client)
        {
            this.NetworkPath = ConfigurationManager.AppSettings["LibertyNetworkAddress"];
            this.MonitoredFolderTxt = ConfigurationManager.AppSettings["LibertyMonitoredFolderText"];
            this.ArchiveFolderTxt = ConfigurationManager.AppSettings["LibertyArchivedFolderText"];
            this.DestinationPath = NetworkPath + @"\" + client + this.MonitoredFolderTxt;
            this.StrRemoteDirectory = ConfigurationManager.AppSettings["LibertyRemoteDir"];
            this.ClientMonitoredArchiveFolder = NetworkPath + @"\" + client + this.ArchiveFolderTxt;
            ProtocolType = ConfigurationManager.AppSettings["LibertyFtpProctocol"];
            HostKeyFingerprint = ConfigurationManager.AppSettings["LibertyHostKeyFingerprint"];
            LogFileLocation = ConfigurationManager.AppSettings["LibertyLogFileLocation"];
        }
    }
}
