﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADAFTPUtility.Clients
{
    public class AdmiralFTP : BaseClientFTP
    {
        public AdmiralFTP(string client) : base (client)
        {
            this.NetworkPath = ConfigurationManager.AppSettings["AdmiralNetworkAddress"];
            this.MonitoredFolderTxt = ConfigurationManager.AppSettings["AdmiralMonitoredFolderText"];
            this.ArchiveFolderTxt = ConfigurationManager.AppSettings["AdmiralArchivedFolderText"];
            this.DestinationPath = NetworkPath + @"\" + client + this.MonitoredFolderTxt;
            this.StrRemoteDirectory = ConfigurationManager.AppSettings["AdmiralRemoteDir"];
            this.ClientMonitoredArchiveFolder = NetworkPath + @"\" + client + this.ArchiveFolderTxt;
            ProtocolType = ConfigurationManager.AppSettings["AdmiralFtpProctocol"];
            HostKeyFingerprint = ConfigurationManager.AppSettings["AdmiralHostKeyFingerprint"];
            LogFileLocation = ConfigurationManager.AppSettings["AdmiralLogFileLocation"];
        }
    }
}
