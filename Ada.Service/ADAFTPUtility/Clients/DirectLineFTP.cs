﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using WinSCP;

// ReSharper disable InconsistentNaming

namespace ADAFTPUtility.Clients
{
    /// <summary>
    /// Direct Line Custom Constructor to map to App.Config
    /// </summary>
    public class DirectLineFTP : BaseClientFTP
    {
        public DirectLineFTP(string client) : base (client)
        {
            NetworkPath = ConfigurationManager.AppSettings["DlgNetworkAddress"];
            MonitoredFolderTxt = ConfigurationManager.AppSettings["DlgMonitoredFolderText"];
            ArchiveFolderTxt = ConfigurationManager.AppSettings["DlgArchivedFolderText"];
            DestinationPath = NetworkPath + @"\" + MonitoredFolderTxt;
            StrRemoteDirectory = ConfigurationManager.AppSettings["DlgRemoteDir"];
            ClientMonitoredArchiveFolder = NetworkPath + @"\" + ArchiveFolderTxt;
            ProtocolType = ConfigurationManager.AppSettings["DlgFtpProctocol"];
            HostKeyFingerprint = ConfigurationManager.AppSettings["DlgHostKeyFingerprint"];
            LogFileLocation = ConfigurationManager.AppSettings["DlgLogFileLocation"];
        }

        public override void DownloadNewFiles(Session session)
        {
            if (session == null) throw new ArgumentNullException("session");

            try
            {
                var transferOptions = new TransferOptions { TransferMode = TransferMode.Binary };

                session.FileTransferred += FilesTransferred;
    
                transferOptions.FileMask = "DLG_datawash_?????????.zip";

                SyncFiles(session, transferOptions);
            }

            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex);
            }
        }

        private void SyncFiles(Session session, TransferOptions transferOptions)
        {
            if (session == null) throw new ArgumentNullException("session");
            if (transferOptions == null) throw new ArgumentNullException("transferOptions");

            var synchronizationResult = session.SynchronizeDirectories(SynchronizationMode.Local, DestinationPath, StrRemoteDirectory, false, false, SynchronizationCriteria.Time, transferOptions);

            if (synchronizationResult == null || synchronizationResult.Downloads == null) return;

            foreach (var download in synchronizationResult.Downloads.AsQueryable())
            {
                if (download != null && download.Error == null)
                {
                    Console.WriteLine("Download of {0} succeeded, removing from source", download.FileName);
                    var removalResult = session.RemoveFiles(session.EscapeFileMask(download.FileName));

                    if (removalResult != null && removalResult.IsSuccess)
                    {
                        Console.WriteLine("Removing of file {0} succeeded", download.FileName);
                    }
                    else
                    {
                        if (removalResult != null && removalResult.Failures != null)
                            Console.WriteLine("Removing of file {0} failed: {1}", download.FileName, removalResult.Failures.First());
                        else
                            Console.WriteLine("Removing of file {0} failed.", download.FileName);
                    }
                }

                else
                {
                    if (download != null)
                        Console.WriteLine("Download of {0} failed: {1}", download.FileName, download.Error.Message);
                    else
                        Console.WriteLine("Download failed - no download: {0}", DateTime.Now);
                }
            }
        }
    }
}
