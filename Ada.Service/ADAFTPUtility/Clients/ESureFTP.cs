﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using WinSCP;

namespace ADAFTPUtility.Clients
{
    /// <summary>
    /// Esure Custom Constructor to map to App.Config
    /// </summary>
    public class EsureFTP : BaseClientFTP
    {
        public EsureFTP(string client)
            : base(client)
        {
            NetworkPath = ConfigurationManager.AppSettings["EsureNetworkAddress"];
            MonitoredFolderTxt = ConfigurationManager.AppSettings["EsureMonitoredFolderText"];
            ArchiveFolderTxt = ConfigurationManager.AppSettings["EsureArchivedFolderText"];
            DestinationPath = NetworkPath + @"\" + MonitoredFolderTxt;
            StrRemoteDirectory = ConfigurationManager.AppSettings["EsureRemoteDir"];
            ClientMonitoredArchiveFolder = NetworkPath + @"\" + ArchiveFolderTxt;
            ProtocolType = ConfigurationManager.AppSettings["EsureFtpProctocol"];
            HostKeyFingerprint = ConfigurationManager.AppSettings["EsureHostKeyFingerprint"];
            LogFileLocation = ConfigurationManager.AppSettings["EsureLogFileLocation"];
        }

        public override void DownloadNewFiles(Session session)
        {
            if (session == null) throw new ArgumentNullException("session");

            try
            {
                var transferOptions = new TransferOptions { TransferMode = TransferMode.Binary };

                session.FileTransferred += FilesTransferred;                
                transferOptions.ResumeSupport.State = TransferResumeSupportState.Off;
                SyncFiles(session, transferOptions);
            }

            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex);
            }
        }

        private void SyncFiles(Session session, TransferOptions transferOptions)
        {
            if (session == null) throw new ArgumentNullException("session");
            if (transferOptions == null) throw new ArgumentNullException("transferOptions");

            // Only transfer files after 11am incase filepart not complete
            if (DateTime.Now.Hour < 11) { return; }

            // additional check - only older than 1 hour
            transferOptions.FileMask = "*<1H";

            var transferResult = session.GetFiles(StrRemoteDirectory, DestinationPath, false, transferOptions);

            // Throw on any error
            transferResult.Check();

            // Print results
            foreach (TransferEventArgs transfer in transferResult.Transfers)
            {
                // if filename contains filepart, rename
                if (transfer.Destination.Contains(".filepart"))
                {
                    var newFilename = transfer.Destination.Replace(".filepart", "");

                    //first, delete target file if exists, as File.Move() does not support overwrite
                    if (File.Exists(newFilename))
                    {
                        File.Delete(newFilename);                        
                    } 

                    File.Move(transfer.Destination, newFilename);    
                }

                session.RemoveFiles(transfer.FileName);
                Console.WriteLine("Download of {0} succeeded", transfer.FileName);
            }

            Console.WriteLine("done!");
            
        }
    }
}
