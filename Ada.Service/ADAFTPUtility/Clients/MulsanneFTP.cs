﻿using System.Configuration;

namespace ADAFTPUtility.Clients
{
    class MulsanneFTP : BaseClientFTP
    {
        public MulsanneFTP(string client) : base(client)
        {
            NetworkPath = ConfigurationManager.AppSettings["MulsanneNetworkAddress"];
            MonitoredFolderTxt = ConfigurationManager.AppSettings["MulsanneMonitoredFolderText"];
            ArchiveFolderTxt = ConfigurationManager.AppSettings["MulsanneArchivedFolderText"];
            DestinationPath = NetworkPath + @"\" + MonitoredFolderTxt;
            StrRemoteDirectory = ConfigurationManager.AppSettings["MulsanneRemoteDir"];
            ClientMonitoredArchiveFolder = NetworkPath + @"\" + ArchiveFolderTxt;
            ProtocolType = ConfigurationManager.AppSettings["MulsanneFtpProctocol"];
            HostKeyFingerprint = ConfigurationManager.AppSettings["MulsanneHostKeyFingerprint"];
            LogFileLocation = ConfigurationManager.AppSettings["MulsanneLogFileLocation"];
        }
    }
}
