﻿using System.Configuration;

namespace ADAFTPUtility.Clients
{
    class BrokerDirectFTP : BaseClientFTP
    {
        public BrokerDirectFTP(string client)
            : base(client)
        {
            NetworkPath = ConfigurationManager.AppSettings["BrokerDirectNetworkAddress"];
            MonitoredFolderTxt = ConfigurationManager.AppSettings["BrokerDirectMonitoredFolderText"];
            ArchiveFolderTxt = ConfigurationManager.AppSettings["BrokerDirectArchivedFolderText"];
            DestinationPath = NetworkPath + @"\" + MonitoredFolderTxt;
            StrRemoteDirectory = ConfigurationManager.AppSettings["BrokerDirectRemoteDir"];
            ClientMonitoredArchiveFolder = NetworkPath + @"\" + ArchiveFolderTxt;
            ProtocolType = ConfigurationManager.AppSettings["BrokerDirectFtpProctocol"];
            HostKeyFingerprint = ConfigurationManager.AppSettings["BrokerDirectHostKeyFingerprint"];
            LogFileLocation = ConfigurationManager.AppSettings["BrokerDirectLogFileLocation"];
        }
    }
}
