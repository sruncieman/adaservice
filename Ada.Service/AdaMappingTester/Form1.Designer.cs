﻿namespace AdaMappingTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.LoadBtn = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkMatch = new System.Windows.Forms.CheckBox();
            this.btnAbort2 = new System.Windows.Forms.Button();
            this.btnFullMatch = new System.Windows.Forms.Button();
            this.btnCollAll = new System.Windows.Forms.Button();
            this.btnCollapse = new System.Windows.Forms.Button();
            this.btnExpand = new System.Windows.Forms.Button();
            this.chkScan = new System.Windows.Forms.CheckBox();
            this.chkValidate = new System.Windows.Forms.CheckBox();
            this.chkCleanse = new System.Windows.Forms.CheckBox();
            this.btnAbort = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.propertyGrid2 = new System.Windows.Forms.PropertyGrid();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.HideSelection = false;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Margin = new System.Windows.Forms.Padding(4);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(658, 591);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.CategoryForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Top;
            this.propertyGrid1.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(423, 283);
            this.propertyGrid1.TabIndex = 1;
            this.propertyGrid1.SelectedGridItemChanged += new System.Windows.Forms.SelectedGridItemChangedEventHandler(this.propertyGrid1_SelectedGridItemChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // LoadBtn
            // 
            this.LoadBtn.Location = new System.Drawing.Point(12, 36);
            this.LoadBtn.Name = "LoadBtn";
            this.LoadBtn.Size = new System.Drawing.Size(59, 23);
            this.LoadBtn.TabIndex = 2;
            this.LoadBtn.Text = "Load";
            this.LoadBtn.UseVisualStyleBackColor = true;
            this.LoadBtn.Click += new System.EventHandler(this.LoadBtn_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Keoghs",
            "1st Central",
            "CH1",
            "Co-Op Pre-Lit Credit Hire",
            "Sabre",
            "Liberty",
            "Cue",
            "Eldon",
            "First Group",
            "Allianz",
            "DLG",
            "Covea",
            "Endsleigh",
            "Aviva Pre-Lit Credit Hire",
            "Service Pre-Lit Credit Hire",
            "Service Underwriting Agency",
            "Co-Op Litigated Credit Hire",
            "Service Litigated Credit Hire",
            "Aviva Litigated Credit Hire",
            "LV=",
            "Qudos",
            "Allianz Litigated Credit Hire",
            "AXA Litigated Credit Hire",
            "Churchill Litigated Credit Hire",
            "DLG Litigated Credit Hire",
            "eSure Litigated Credit Hire",
            "Hastings Litigated Credit Hire",
            "Haven Litigated Credit Hire",
            "LV= Litigated Credit Hire",
            "NFUM Litigated Credit Hire",
            "Privilege Litigated Credit Hire",
            "Prospect Legal Litigated Credit Hire",
            "UKI Litigated Credit Hire",
            "Zurich Commercial Litigated Credit Hire",
            "Zurich Global Corporate Litigated Credit Hire",
            "Zurich Municipal Litigated Credit Hire",
            "Zurich UK PL Motor Litigated Credit Hire",
            "Aviva",
            "eSure Pre-Lit Credit Hire",
            "ACE",
            "Tradex",
            "Haven",
            "LV= Pre-Lit Credit Hire",
            "Antilo",
            "Co-Operative Insurance",
            "Excel",
            "Keoghs_Non_Default",
            "LV= Claim Centre",
            "Covea Pre-Lit Credit Hire",
            "Admiral Pre-Lit Credit Hire",
            "Hastings Pre-Lit Credit Hire",
            "Covea Litigated Credit Hire",
            "Admiral Litigated Credit Hire",
            "Sixt",
            "Hertz",
            "Liberty Pre-Lit Credit Hire",
            "Liberty Litigated Credit Hire",
            "Generic Client",
            "Valexa",
            "Mulsanne",
            "Canopius",
            "Key Claims Pre-Lit Credit Hire",
            "BT Pre-Lit Credit Hire",
            "LV Legacy Pre-Lit Credit Hire",
            "XSDirect",
            "Broadspire",
            "Saga",
            "ESure"});
            this.comboBox1.Location = new System.Drawing.Point(12, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(133, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.chkMatch);
            this.panel1.Controls.Add(this.btnAbort2);
            this.panel1.Controls.Add(this.btnFullMatch);
            this.panel1.Controls.Add(this.btnCollAll);
            this.panel1.Controls.Add(this.btnCollapse);
            this.panel1.Controls.Add(this.btnExpand);
            this.panel1.Controls.Add(this.chkScan);
            this.panel1.Controls.Add(this.chkValidate);
            this.panel1.Controls.Add(this.chkCleanse);
            this.panel1.Controls.Add(this.btnAbort);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.LoadBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1221, 68);
            this.panel1.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.AcceptsReturn = true;
            this.textBox1.Location = new System.Drawing.Point(350, 22);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(137, 35);
            this.textBox1.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(378, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Claim Numbers";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // chkMatch
            // 
            this.chkMatch.AutoSize = true;
            this.chkMatch.Location = new System.Drawing.Point(257, 42);
            this.chkMatch.Name = "chkMatch";
            this.chkMatch.Size = new System.Drawing.Size(70, 17);
            this.chkMatch.TabIndex = 15;
            this.chkMatch.Text = "Match All";
            this.chkMatch.UseVisualStyleBackColor = true;
            // 
            // btnAbort2
            // 
            this.btnAbort2.Location = new System.Drawing.Point(1016, 32);
            this.btnAbort2.Name = "btnAbort2";
            this.btnAbort2.Size = new System.Drawing.Size(75, 23);
            this.btnAbort2.TabIndex = 14;
            this.btnAbort2.Text = "STOP!";
            this.btnAbort2.UseVisualStyleBackColor = true;
            this.btnAbort2.Visible = false;
            this.btnAbort2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnFullMatch
            // 
            this.btnFullMatch.Location = new System.Drawing.Point(1097, 33);
            this.btnFullMatch.Name = "btnFullMatch";
            this.btnFullMatch.Size = new System.Drawing.Size(116, 23);
            this.btnFullMatch.TabIndex = 13;
            this.btnFullMatch.Text = "Full Match Report";
            this.btnFullMatch.UseVisualStyleBackColor = true;
            this.btnFullMatch.Click += new System.EventHandler(this.btnFullMatch_Click);
            // 
            // btnCollAll
            // 
            this.btnCollAll.Location = new System.Drawing.Point(77, 36);
            this.btnCollAll.Name = "btnCollAll";
            this.btnCollAll.Size = new System.Drawing.Size(31, 23);
            this.btnCollAll.TabIndex = 12;
            this.btnCollAll.Text = "^";
            this.btnCollAll.UseVisualStyleBackColor = true;
            this.btnCollAll.Click += new System.EventHandler(this.btnCollAll_Click);
            // 
            // btnCollapse
            // 
            this.btnCollapse.Location = new System.Drawing.Point(150, 36);
            this.btnCollapse.Name = "btnCollapse";
            this.btnCollapse.Size = new System.Drawing.Size(31, 23);
            this.btnCollapse.TabIndex = 11;
            this.btnCollapse.Text = "-";
            this.btnCollapse.UseVisualStyleBackColor = true;
            this.btnCollapse.Click += new System.EventHandler(this.btnCollapse_Click);
            // 
            // btnExpand
            // 
            this.btnExpand.Location = new System.Drawing.Point(114, 35);
            this.btnExpand.Name = "btnExpand";
            this.btnExpand.Size = new System.Drawing.Size(31, 23);
            this.btnExpand.TabIndex = 10;
            this.btnExpand.Text = "+";
            this.btnExpand.UseVisualStyleBackColor = true;
            this.btnExpand.Click += new System.EventHandler(this.btnExpand_Click);
            // 
            // chkScan
            // 
            this.chkScan.AutoSize = true;
            this.chkScan.Checked = true;
            this.chkScan.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkScan.Location = new System.Drawing.Point(187, 7);
            this.chkScan.Name = "chkScan";
            this.chkScan.Size = new System.Drawing.Size(153, 17);
            this.chkScan.TabIndex = 9;
            this.chkScan.Text = "Add Standard Fields (scan)";
            this.chkScan.UseVisualStyleBackColor = true;
            // 
            // chkValidate
            // 
            this.chkValidate.AutoSize = true;
            this.chkValidate.Checked = true;
            this.chkValidate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkValidate.Location = new System.Drawing.Point(187, 40);
            this.chkValidate.Name = "chkValidate";
            this.chkValidate.Size = new System.Drawing.Size(64, 17);
            this.chkValidate.TabIndex = 8;
            this.chkValidate.Text = "Validate";
            this.chkValidate.UseVisualStyleBackColor = true;
            // 
            // chkCleanse
            // 
            this.chkCleanse.AutoSize = true;
            this.chkCleanse.Checked = true;
            this.chkCleanse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCleanse.Location = new System.Drawing.Point(187, 23);
            this.chkCleanse.Name = "chkCleanse";
            this.chkCleanse.Size = new System.Drawing.Size(64, 17);
            this.chkCleanse.TabIndex = 7;
            this.chkCleanse.Text = "Cleanse";
            this.chkCleanse.UseVisualStyleBackColor = true;
            // 
            // btnAbort
            // 
            this.btnAbort.Location = new System.Drawing.Point(1016, 6);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(75, 23);
            this.btnAbort.TabIndex = 6;
            this.btnAbort.Text = "STOP!";
            this.btnAbort.UseVisualStyleBackColor = true;
            this.btnAbort.Visible = false;
            this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1097, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Match Entity";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(500, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "label1";
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(658, 68);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(10, 591);
            this.splitter1.TabIndex = 5;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.propertyGrid2);
            this.panel2.Controls.Add(this.splitter2);
            this.panel2.Controls.Add(this.propertyGrid1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(668, 68);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(423, 591);
            this.panel2.TabIndex = 6;
            // 
            // propertyGrid2
            // 
            this.propertyGrid2.CategoryForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.propertyGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid2.Location = new System.Drawing.Point(0, 294);
            this.propertyGrid2.Name = "propertyGrid2";
            this.propertyGrid2.Size = new System.Drawing.Size(423, 297);
            this.propertyGrid2.TabIndex = 3;
            // 
            // splitter2
            // 
            this.splitter2.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 283);
            this.splitter2.Name = "splitter2";
            this.splitter2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitter2.Size = new System.Drawing.Size(423, 11);
            this.splitter2.TabIndex = 2;
            this.splitter2.TabStop = false;
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(1101, 68);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 591);
            this.listBox1.TabIndex = 8;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter3.Location = new System.Drawing.Point(1091, 68);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(10, 591);
            this.splitter3.TabIndex = 9;
            this.splitter3.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.treeView1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 68);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(658, 591);
            this.panel3.TabIndex = 10;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // backgroundWorker2
            // 
            this.backgroundWorker2.WorkerReportsProgress = true;
            this.backgroundWorker2.WorkerSupportsCancellation = true;
            this.backgroundWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork);
            this.backgroundWorker2.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker2_ProgressChanged);
            this.backgroundWorker2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker2_RunWorkerCompleted);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1221, 659);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.splitter3);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "ADA Mapping Tester V1.2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button LoadBtn;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PropertyGrid propertyGrid2;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnAbort;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox chkValidate;
        private System.Windows.Forms.CheckBox chkCleanse;
        private System.Windows.Forms.Button btnCollapse;
        private System.Windows.Forms.Button btnExpand;
        private System.Windows.Forms.CheckBox chkScan;
        private System.Windows.Forms.Button btnCollAll;
        private System.Windows.Forms.Button btnFullMatch;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.Button btnAbort2;
        private System.Windows.Forms.CheckBox chkMatch;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
    }
}

