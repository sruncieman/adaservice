﻿using MDA.Common;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using MDA.Pipeline;
using MDA.Common.Server;
using MDA.ClaimService.Interface;
using MDA.ClaimService;
using MDA.MatchingService.Interface;

namespace AdaMappingTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            label1.Text = "";

            //PipelineService.ADAServiceClient p = new PipelineService.ADAServiceClient();

            //var clients = p.GetRiskClients(new RiskClientsRequest() { ClientId = 0, UserId = 0, Who = "admin", FilterClientId = -1 });
            //foreach (var c in clients.ClientList)
            //{
            //    comboBox1.Items.Add(new ComboItem() { Name = c.ClientName, Id = c.Id });
            //}

            this.comboBox1.SelectedIndex = this.comboBox1.Items.Count - 1;
        }

        //private T Deserialize<T>(string rawXml)
        //{
        //    using (XmlReader reader = XmlReader.Create(new StringReader(rawXml)))
        //    {
        //        var formatter = new DataContractSerializer(typeof(T));
        //        return (T)formatter.ReadObject(reader);
        //    }
        //}

        private TreeNode AssignMyNodeToTreeNode(PipelineNodeDetails root)
        {
            TreeNode node = new TreeNode(root.Text);
            node.Tag = root.PipelineEntity;

            if ( node.Tag != null )
                ((PipeEntityBase)root.PipelineEntity).Tag = node;

            foreach (PipelineNodeDetails n in root.Nodes)
                node.Nodes.Add(AssignMyNodeToTreeNode(n));

            return node;
        }

        private TreeNode AssignMyNodeToTreeNode(MessageNode root)
        {
            TreeNode node = new TreeNode(root.Text);

            foreach (MessageNode n in root.Nodes)
                node.Nodes.Add(AssignMyNodeToTreeNode(n));

            return node;
        }

        private void PopulateTreeView(List<PipelineMotorClaim> batch, ProcessingResults mappingResults)
        {
            treeView1.Nodes.Clear();

            if (mappingResults != null)
            {
                TreeNode n0 = new TreeNode("Mapping Results");
                n0.Nodes.Add(AssignMyNodeToTreeNode(mappingResults.Message));
                treeView1.Nodes.Add(n0);
            }

            string search = textBox1.Text;

            string[] result = search.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (!string.IsNullOrEmpty(search))
	        {
		        batch = batch.Where(x => result.Contains(x.ClaimNumber)).ToList();
	        }

            foreach(var c in batch)
            {
                TreeNode n3 = AssignMyNodeToTreeNode(c.ToPipelineNodeDetails());

                string err = "";

                if (c.CleanseResults != null && (c.CleanseResults.ErrorCount > 0 || c.CleanseResults.WarningCount > 0))
                {
                    TreeNode n1 = new TreeNode("Cleansing Results");
                    n1.Nodes.Add(AssignMyNodeToTreeNode(c.CleanseResults.Message));

                    err +=  "[Cleanse";

                    if (c.CleanseResults.ErrorCount > 0)
                        err += " ERR:" + c.CleanseResults.ErrorCount;

                    if (c.CleanseResults.WarningCount > 0)
                        err += " WRN:" + c.CleanseResults.WarningCount;

                    err += "]";

                    n3.Nodes.Add(n1);
                }

                if (c.ValidationResults != null && (c.ValidationResults.ErrorCount > 0 || c.ValidationResults.WarningCount > 0))
                {
                    TreeNode n2 = new TreeNode("Validation Results");
                    n2.Nodes.Add(AssignMyNodeToTreeNode(c.ValidationResults.Message));

                    err += "[Valid";

                    if (c.ValidationResults.ErrorCount > 0)
                        err += " ERR:" + c.ValidationResults.ErrorCount;

                    if (c.ValidationResults.WarningCount > 0)
                        err += " WRN:" + c.ValidationResults.WarningCount;

                    err += "]";

                    n3.Nodes.Add(n2);
                }

                if (err.Length > 0)
                    n3.Text += ">>>>  " + err;

                treeView1.Nodes.Add(n3);
            }
        }

        private Pipeline p = null;
        private IMatchingService MatchingService;

        private List<PipelineMotorClaim> batch;

        public int BuildClaimsIntoBatch(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim, object o)
        {
            if ( chkScan.Checked )
                p.CreateClaimService(ctx).TestScanPipelineRecord(ctx, claim);

            batch.Add((PipelineMotorClaim)claim);

            mapCnt = batch.Count;

            backgroundWorker1.ReportProgress(0);

            if (backgroundWorker1.CancellationPending == true)
            {
                return -1;
            }

            //DisplayLabel();

            return 0;
        }

        int mapCnt = 0;
        int cleanCnt = 0;
        int validCnt = 0;
        int matchCnt = 0;

        FileStream fs;
        ProcessingResults mappingResults;
        string clientName;
        int comboSelectedIndex;

        private void DisplayLabel()
        {
            label1.Text = string.Format("Mapping[{0}] - Cleanse[{1}] - Validate[{2}] - Match[{3}]", mapCnt, cleanCnt, validCnt, matchCnt);
            panel1.Refresh();
        }

        private void LoadBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var x = openFileDialog1.ShowDialog();

                fs = (FileStream)openFileDialog1.OpenFile();

                if (fs == null) return;

                label1.Text = "";
                treeView1.Nodes.Clear();

                //XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                //DataContractSerializer ser = new DataContractSerializer(typeof(PipelineClaimBatch));

                //PipelineClaimBatch batch = (PipelineClaimBatch)ser.ReadObject(reader, true);
                //reader.Close();
                //fs.Close();

                mappingResults = null;

                mapCnt = 0;
                cleanCnt = 0;
                validCnt = 0;
                matchCnt = 0;

                clientName = comboBox1.SelectedItem.ToString();
                comboSelectedIndex = comboBox1.SelectedIndex;

                btnAbort.Visible = true;

                if (backgroundWorker1.IsBusy != true)
                {
                    backgroundWorker1.RunWorkerAsync();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Error loading sample");
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            propertyGrid1.SelectedObject = treeView1.SelectedNode.Tag;

            propertyGrid2.SelectedObject = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode.Tag is PipelineVehicle)
            {
                PipelineVehicle v = (PipelineVehicle)treeView1.SelectedNode.Tag;
                List<PipelineAddress> addresses = new List<PipelineAddress>();

                foreach (var person in v.People)
                {
                    foreach (var address in person.Addresses)
                    {
                        addresses.Add(address);
                    }
                }

                var r = MatchingService.MatchVehicle((PipelineVehicle)treeView1.SelectedNode.Tag, addresses, null);

                propertyGrid2.SelectedObject = r;
            }
            else if ( treeView1.SelectedNode.Tag is PipelinePerson )
            {
                var r = MatchingService.MatchPerson((PipelinePerson)treeView1.SelectedNode.Tag, (PipelineVehicle)treeView1.SelectedNode.Parent.Parent.Tag,
                         (PipelineMotorClaim)treeView1.SelectedNode.Parent.Parent.Parent.Parent.Tag, null);

                propertyGrid2.SelectedObject = r;
            }
            else if ( treeView1.SelectedNode.Tag is PipelineAddress )
            {
                string orgName = null;

                if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineOrganisation)
                    orgName = ((PipelineOrganisation)treeView1.SelectedNode.Parent.Parent.Tag).OrganisationName;

                var r = MatchingService.MatchAddress((PipelineAddress)treeView1.SelectedNode.Tag, orgName, null);

                propertyGrid2.SelectedObject = r;
            }
            else if (treeView1.SelectedNode.Tag is PipelineBankAccount)
            {
                var r = MatchingService.MatchBankAccount((PipelineBankAccount)treeView1.SelectedNode.Tag, null);

                propertyGrid2.SelectedObject = r;
            }
            else if (treeView1.SelectedNode.Tag is PipelinePaymentCard)
            {
                var r = MatchingService.MatchPaymentCard((PipelinePaymentCard)treeView1.SelectedNode.Tag, null);

                propertyGrid2.SelectedObject = r;
            }
            else if (treeView1.SelectedNode.Tag is PipelineMotorClaim)
            {
                var r = MatchingService.MatchClaim((PipelineMotorClaim)treeView1.SelectedNode.Tag, null, null);

                propertyGrid2.SelectedObject = r;
            }
            else if (treeView1.SelectedNode.Tag is PipelineDrivingLicense)
            {
                var r = MatchingService.MatchLicenseNumber((PipelineDrivingLicense)treeView1.SelectedNode.Tag, null);

                propertyGrid2.SelectedObject = r;
            }
            else if (treeView1.SelectedNode.Tag is PipelinePassport)
            {
                var r = MatchingService.MatchPassportNumber((PipelinePassport)treeView1.SelectedNode.Tag, null);

                propertyGrid2.SelectedObject = r;
            }
            else if (treeView1.SelectedNode.Tag is PipelineEmail)
            {
                var r = MatchingService.MatchEmailAddress((PipelineEmail)treeView1.SelectedNode.Tag, null);

                propertyGrid2.SelectedObject = r;
            }
            else if (treeView1.SelectedNode.Tag is PipelineNINumber)
            {
                var r = MatchingService.MatchNINumber((PipelineNINumber)treeView1.SelectedNode.Tag, null);

                propertyGrid2.SelectedObject = r;
            }
            else if (treeView1.SelectedNode.Tag is PipelineOrganisation)
            {
                var r = MatchingService.MatchOrganisation((PipelineOrganisation)treeView1.SelectedNode.Tag, null);

                propertyGrid2.SelectedObject = r;
            }
            else if (treeView1.SelectedNode.Tag is PipelinePolicy)
            {
                var r = MatchingService.MatchPolicy((PipelinePolicy)treeView1.SelectedNode.Tag, null, null);

                propertyGrid2.SelectedObject = r;
            }
            else if (treeView1.SelectedNode.Tag is PipelineTelephone)
            {
                var r = MatchingService.MatchTelephoneNumber((PipelineTelephone)treeView1.SelectedNode.Tag, null);

                propertyGrid2.SelectedObject = r;
            }
            else if (treeView1.SelectedNode.Tag is PipelineWebSite)
            {
                var r = MatchingService.MatchWebSite((PipelineWebSite)treeView1.SelectedNode.Tag, null);

                propertyGrid2.SelectedObject = r;
            }
        }

        private ListItemDetails ProcessEntity(object o, string prop)
        {
            try
            {    
                var val = o.GetType().GetProperty(prop).GetValue(o, null);

                string s = ((val == null) ? "null" : val.ToString());

                return new ListItemDetails( ((TreeNode)((PipeEntityBase)o).Tag), s);
            }
            catch (Exception ex)
            {
                return new ListItemDetails( ((TreeNode)((PipeEntityBase)o).Tag), "ERR:" + ex.Message);
            }
        }

        private void propertyGrid1_SelectedGridItemChanged(object sender, SelectedGridItemChangedEventArgs e)
        {
            var prop = propertyGrid1.SelectedGridItem.Label;

            listBox1.Items.Clear();

            if (treeView1.SelectedNode == null) return;

            #region PipelineClaim
            if (treeView1.SelectedNode.Tag is PipelineMotorClaim)
            {
                foreach( PipelineMotorClaim c in batch )
                {
                    listBox1.Items.Add(ProcessEntity(c, prop));
                }
            }
            #endregion

            #region PipelineVehicle
            else if (treeView1.SelectedNode.Tag is PipelineVehicle)
            {
                if( treeView1.SelectedNode.Parent.Parent.Tag is PipelineMotorClaim )  // veh under claim
                {
                    foreach (PipelineVehicle v in from c in batch from v in c.Vehicles select v)
                    {
                        listBox1.Items.Add(ProcessEntity(v, prop));
                    }
                }
            }
            #endregion

            #region PipelineOrganisationVehicle
            else if (treeView1.SelectedNode.Tag is PipelineOrganisationVehicle)
            {
                if (treeView1.SelectedNode.Parent.Parent.Parent.Parent.Tag is PipelineMotorClaim) // vehicle under org under claim
                {
                    foreach (PipelineOrganisationVehicle ov in from c in batch from o in c.Organisations from ov in o.Vehicles select ov)
                    {
                        listBox1.Items.Add(ProcessEntity(ov, prop));
                    }

                }
                else if (treeView1.SelectedNode.Parent.Parent.Parent.Parent.Tag is PipelinePerson) // vehicle under org under person
                {
                    foreach (PipelineOrganisationVehicle ov in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations from ov in o.Vehicles select ov)
                    {
                        listBox1.Items.Add(ProcessEntity(ov, prop));
                    }
                }
            }
            #endregion

            #region PipelineOrganisation
            else if (treeView1.SelectedNode.Tag is PipelineOrganisation)
            {
                if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineMotorClaim)
                {
                    foreach (PipelineOrganisation o in from c in batch from o in c.Organisations select o)
                    {
                        listBox1.Items.Add(ProcessEntity(o, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Tag is PipelinePerson)
                {
                    foreach (PipelineOrganisation o in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations select o)
                    {
                        listBox1.Items.Add(ProcessEntity(o, prop));
                    }
                }
            }
            #endregion

            #region PipelinePerson
            else if (treeView1.SelectedNode.Tag is PipelinePerson)
            {
                foreach (PipelinePerson p in from c in batch from v in c.Vehicles from p in v.People select p)
                {
                    listBox1.Items.Add(ProcessEntity(p, prop));
                }
            }
            #endregion

            #region PipelineAddress
            else if (treeView1.SelectedNode.Tag is PipelineAddress)
            {
                if ( treeView1.SelectedNode.Parent.Parent.Tag is PipelinePerson)
                {
                    foreach (PipelineAddress a in from c in batch from v in c.Vehicles from p in v.People from a in p.Addresses select a)
                    {
                        listBox1.Items.Add(ProcessEntity(a, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineOrganisation)
                {
                    if (treeView1.SelectedNode.Parent.Parent.Parent.Parent.Tag is PipelinePerson)
                    {
                        foreach (PipelineAddress a in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations from a in o.Addresses select a)
                        {
                            listBox1.Items.Add(ProcessEntity(a, prop));
                        }
                    }
                    else
                    {
                        foreach (PipelineAddress a in from c in batch from o in c.Organisations from a in o.Addresses select a)
                        {
                            listBox1.Items.Add(ProcessEntity(a, prop));
                        }
                    }
                }
            }
            #endregion

            #region PipelinePolicy
            else if (treeView1.SelectedNode.Tag is PipelinePolicy)
            {
                foreach (PipelinePolicy po in from c in batch select c.Policy)
                {
                    listBox1.Items.Add(ProcessEntity(po, prop));
                }
            }
            #endregion

            #region PipelineTelephone
            else if (treeView1.SelectedNode.Tag is PipelineTelephone)
            {
                if (treeView1.SelectedNode.Parent.Parent.Tag is PipelinePerson)
                {
                    foreach (PipelineTelephone t in from c in batch from v in c.Vehicles from p in v.People from t in p.Telephones select t)
                    {
                        listBox1.Items.Add(ProcessEntity(t, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineOrganisation)
                {
                    if (treeView1.SelectedNode.Parent.Parent.Parent.Parent.Tag is PipelinePerson)
                    {
                        foreach (PipelineTelephone t in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations from t in o.Telephones select t)
                        {
                            listBox1.Items.Add(ProcessEntity(t, prop));
                        }
                    }
                    else
                    {
                        foreach (PipelineTelephone t in from c in batch from o in c.Organisations from t in o.Telephones select t)
                        {
                            listBox1.Items.Add(ProcessEntity(t, prop));
                        }
                    }
                }
            }
            #endregion

            #region PipelineWebSite
            else if (treeView1.SelectedNode.Tag is PipelineWebSite)
            {
                if (treeView1.SelectedNode.Parent.Parent.Parent.Parent.Tag is PipelinePerson)
                {
                    foreach (PipelineWebSite w in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations from w in o.WebSites select w)
                    {
                        listBox1.Items.Add(ProcessEntity(w, prop));
                    }
                }
                else
                {
                    foreach (PipelineWebSite w in from c in batch from o in c.Organisations from w in o.WebSites select w)
                    {
                        listBox1.Items.Add(ProcessEntity(w, prop));
                    }
                }
            }
            #endregion

            #region PipelineBankAccount
            else if (treeView1.SelectedNode.Tag is PipelineBankAccount)
            {
                if (treeView1.SelectedNode.Parent.Parent.Tag is PipelinePerson)
                {
                    foreach (PipelineBankAccount b in from c in batch from v in c.Vehicles from p in v.People from b in p.BankAccounts select b)
                    {
                        listBox1.Items.Add(ProcessEntity(b, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineOrganisation)
                {
                    foreach (PipelineBankAccount b in from c in batch from o in c.Organisations from b in o.BankAccounts select b)
                    {
                        listBox1.Items.Add(ProcessEntity(b, prop));
                    }
                }
            }
            #endregion

            #region PipelinePaymentCard
            else if (treeView1.SelectedNode.Tag is PipelinePaymentCard)
            {
                if (treeView1.SelectedNode.Parent.Parent.Tag is PipelinePerson)
                {
                    foreach (PipelinePaymentCard pc in from c in batch from v in c.Vehicles from p in v.People from pc in p.PaymentCards select pc)
                    {
                        listBox1.Items.Add(ProcessEntity(pc, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineOrganisation)
                {
                    foreach (PipelinePaymentCard pc in from c in batch from o in c.Organisations from pc in o.PaymentCards select pc)
                    {
                        listBox1.Items.Add(ProcessEntity(pc, prop));
                    }
                }
            }
            #endregion

            #region PipelineNINumber
            else if (treeView1.SelectedNode.Tag is PipelineNINumber)
            {
                foreach (PipelineNINumber ni in from c in batch from v in c.Vehicles from p in v.People from ni in p.NINumbers select ni)
                {
                    listBox1.Items.Add(ProcessEntity(ni, prop));
                }
            }
            #endregion

            #region PipelineDrivingLicense
            else if (treeView1.SelectedNode.Tag is PipelineDrivingLicense)
            {
                foreach (PipelineDrivingLicense dl in from c in batch from v in c.Vehicles from p in v.People from dl in p.DrivingLicenseNumbers select dl)
                {
                    listBox1.Items.Add(ProcessEntity(dl, prop));
                }
            }
            #endregion

            #region PipelineEmail
            else if (treeView1.SelectedNode.Tag is PipelineEmail)
            {
                if (treeView1.SelectedNode.Parent.Parent.Tag is PipelinePerson)
                {
                    foreach (PipelineEmail em in from c in batch from v in c.Vehicles from p in v.People from em in p.EmailAddresses select em)
                    {
                        listBox1.Items.Add(ProcessEntity(em, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineOrganisation)
                {
                    foreach (PipelineEmail em in from c in batch from o in c.Organisations from em in o.EmailAddresses select em)
                    {
                        listBox1.Items.Add(ProcessEntity(em, prop));
                    }
                }
            }
            #endregion

            #region PipelinePassport
            else if (treeView1.SelectedNode.Tag is PipelinePassport)   // under person
            {
                foreach (PipelinePassport pa in from c in batch from v in c.Vehicles from p in v.People from pa in p.PassportNumbers select pa)
                {
                    listBox1.Items.Add(ProcessEntity(pa, prop));
                }
            }
            #endregion

            #region PipelineClaimInfo
            else if (treeView1.SelectedNode.Tag is PipelineClaimInfo)   // under claim, also i2p and i2o links
            {
                if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineMotorClaim)
                {
                    foreach (PipelineClaimInfo mc in from c in batch select c.ExtraClaimInfo)
                    {
                        listBox1.Items.Add(ProcessEntity(mc, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Tag is PipelinePerson)
                {
                    foreach (PipelineClaimInfo mc in from c in batch from v in c.Vehicles from p in v.People select p.I2Pe_LinkData.ClaimInfo)
                    {
                        listBox1.Items.Add(ProcessEntity(mc, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineOrganisation)
                {
                    foreach (PipelineClaimInfo mc in from c in batch select c.ExtraClaimInfo)
                    {
                        listBox1.Items.Add(ProcessEntity(mc, prop));
                    }
                }
            }
            #endregion

            #region PipelineEmailLink
            else if (treeView1.SelectedNode.Tag is PipelineEmailLink)  // under org or person
            {
                if (treeView1.SelectedNode.Parent.Parent.Parent.Tag is PipelinePerson)
                {
                    foreach (PipelineEmailLink ld in from c in batch from v in c.Vehicles from p in v.People from em in p.EmailAddresses select em.PO2E_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Parent.Tag is PipelineOrganisation)
                {
                    if (treeView1.SelectedNode.Parent.Parent.Parent.Parent.Parent.Tag is PipelineMotorClaim) // org under claim
                    {
                        foreach (PipelineEmailLink ld in from c in batch from o in c.Organisations from em in o.EmailAddresses select em.PO2E_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }
                    }
                    else  //Org under person
                    {
                        foreach (PipelineEmailLink ld in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations from em in o.EmailAddresses select em.PO2E_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }
                    }
                }
            }
            #endregion

            #region PipelineAddressLink
            else if (treeView1.SelectedNode.Tag is PipelineAddressLink) 
            {
                if (treeView1.SelectedNode.Parent.Parent.Parent.Tag is PipelinePerson)  // Address under person
                {
                    foreach (PipelineAddressLink ld in from c in batch from v in c.Vehicles from p in v.People from a in p.Addresses select a.PO2A_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Parent.Tag is PipelineOrganisation)  // Address under Org
                {
                    if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineMotorClaim)
                    {
                        foreach (PipelineAddressLink ld in from c in batch from o in c.Organisations from a in o.Addresses select a.PO2A_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }
                    }
                    else  // Address under Org under person
                    {
                        foreach (PipelineAddressLink ld in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations from a in o.Addresses select a.PO2A_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }
                    }
                }
            }
            #endregion

            #region PipelineIncident2OrganisationLink
            else if (treeView1.SelectedNode.Tag is PipelineIncident2OrganisationLink)
            {
                if (treeView1.SelectedNode.Parent.Parent.Parent.Tag is PipelineMotorClaim)  // Org under Claim
                {
                    foreach (PipelineIncident2OrganisationLink ld in from c in batch from o in c.Organisations select o.I2O_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
                else // Org under Person
                {
                    foreach (PipelineIncident2OrganisationLink ld in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations select o.I2O_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
            }
            #endregion

            #region PipelineAddressPipelineIncident2PersonLink
            else if (treeView1.SelectedNode.Tag is PipelineIncident2PersonLink)  // all under person
            {
                foreach (PipelineIncident2PersonLink ld in from c in batch from v in c.Vehicles from p in v.People select p.I2Pe_LinkData)
                {
                    listBox1.Items.Add(ProcessEntity(ld, prop));
                }
            }
            #endregion

            #region PipelineIncident2VehicleLink

            else if (treeView1.SelectedNode.Tag is PipelineIncident2VehicleLink)  // all under vehicle
            {
                foreach (PipelineIncident2VehicleLink ld in from c in batch from v in c.Vehicles select v.I2V_LinkData)
                {
                    listBox1.Items.Add(ProcessEntity(ld, prop));
                }
            }
            #endregion

            #region PipelineIncident2PolicyLink

            else if (treeView1.SelectedNode.Tag is PipelineIncident2PolicyLink)  // all under policy
            {
                foreach (PipelineIncident2PolicyLink ld in from c in batch select c.Policy.I2Po_LinkData)
                {
                    listBox1.Items.Add(ProcessEntity(ld, prop));
                }
            }
            #endregion

            #region PipelineOrganisation2OrganisationLink

            else if (treeView1.SelectedNode.Tag is PipelineOrganisation2OrganisationLink)  
            {
                if (treeView1.SelectedNode.Parent.Parent.Parent.Tag is PipelinePerson)  // Org under Person
                {
                    foreach (PipelineOrganisation2OrganisationLink ld in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations select o.O2O_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
                else // Org under Claim
                {
                    foreach (PipelineOrganisation2OrganisationLink ld in from c in batch from o in c.Organisations select o.O2O_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
            }
            #endregion

            #region PipelineOrgVehicleLink

            else if (treeView1.SelectedNode.Tag is PipelineOrgVehicleLink)  // pV2O in Org, V2O in OrgVehicle
            {
                if (treeView1.SelectedNode.Parent.Tag is PipelineOrganisationVehicle) 
                {
                    foreach (PipelineOrgVehicleLink ld in from c in batch from o in c.Organisations from ov in o.Vehicles select ov.V2O_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Tag is PipelineOrganisation) 
                {
                    if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineMotorClaim)  // Org under Claim
                    {
                        foreach (PipelineOrgVehicleLink ld in from c in batch from o in c.Organisations select o.pV2O_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }
                    }
                    else // Org under Claim
                    {
                        foreach (PipelineOrgVehicleLink ld in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations select o.pV2O_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }
                    }
                }
            }
            #endregion

            #region PipelinePerson2OrganisationLink

            else if (treeView1.SelectedNode.Tag is PipelinePerson2OrganisationLink)  // in Org
            {
                if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineMotorClaim)  // Org under Claim
                {
                    foreach (PipelinePerson2OrganisationLink ld in from c in batch from o in c.Organisations select o.P2O_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
                else // Org under Claim
                {
                    foreach (PipelinePerson2OrganisationLink ld in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations select o.P2O_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
            }
            #endregion

            #region PipelinePerson2PersonLink

            else if (treeView1.SelectedNode.Tag is PipelinePerson2PersonLink)  // in Person
            {
                foreach (PipelinePerson2PersonLink ld in from c in batch from v in c.Vehicles from p in v.People select p.P2P_LinkData)
                {
                    listBox1.Items.Add(ProcessEntity(ld, prop));
                }
            }
            #endregion

            #region PipelineAddressPipelinePolicyLink
            else if (treeView1.SelectedNode.Tag is PipelinePolicyLink)  // in org and person
            {
                if (treeView1.SelectedNode.Parent.Tag is PipelinePerson)  
                {
                    foreach (PipelinePolicyLink ld in from c in batch from v in c.Vehicles from p in v.People select p.Pe2Po_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Parent.Tag is PipelineMotorClaim)  
                {
                    foreach (PipelinePolicyLink ld in from c in batch from o in c.Organisations select o.O2Po_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
                else
                {
                    foreach (PipelinePolicyLink ld in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations select o.O2Po_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
            }
            #endregion

            #region PipelinePolicy2BankAccountLink
            else if (treeView1.SelectedNode.Tag is PipelinePolicy2BankAccountLink)  // in BA in Person and claim orgs
            {
                if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineMotorClaim)  // Org under Claim
                {
                    foreach (PipelinePolicy2BankAccountLink ld in from c in batch from o in c.Organisations from ba in o.BankAccounts select ba.Po2Ba_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
                else // person ba
                {
                    foreach (PipelinePolicy2BankAccountLink ld in from c in batch from v in c.Vehicles from p in v.People from ba in p.BankAccounts select ba.Po2Ba_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
            }
            #endregion

            #region PipelineAddressPipelinePolicy2PaymentCardLink
            else if (treeView1.SelectedNode.Tag is PipelinePolicy2PaymentCardLink)  // in PC in Person and claim orgs
            {
                if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineMotorClaim)  // Org under Claim
                {
                    foreach (PipelinePolicy2PaymentCardLink ld in from c in batch from o in c.Organisations from pc in o.PaymentCards select pc.Po2Pc_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
                else // person ba
                {
                    foreach (PipelinePolicy2PaymentCardLink ld in from c in batch from v in c.Vehicles from p in v.People from pc in p.PaymentCards select pc.Po2Pc_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
            }
            #endregion

            #region PipelineVehicle2PersonLink
            else if (treeView1.SelectedNode.Tag is PipelineVehicle2PersonLink)  // in person
            {
                if (treeView1.SelectedNode.Parent.Tag is PipelinePerson)
                {
                    foreach (PipelineVehicle2PersonLink ld in from c in batch from v in c.Vehicles from p in v.People select p.V2Pe_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(ld, prop));
                    }
                }
            }
            #endregion

            #region PipelineVehicle2PolicyLink
            else if (treeView1.SelectedNode.Tag is PipelineVehicle2PolicyLink)  // in vehicle
            {
                if (treeView1.SelectedNode.Parent.Tag is PipelineVehicle)
                {
                    if( treeView1.SelectedNode.Parent.Parent.Parent.Tag is PipelineMotorClaim )  // veh under claim
                    {
                        foreach (PipelineVehicle2PolicyLink ld in from c in batch from v in c.Vehicles select v.V2Po_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }
                    }
                    else if ( treeView1.SelectedNode.Parent.Parent.Parent.Parent.Parent.Tag is PipelineMotorClaim ) // vehicle under org under claim
                    {
                        foreach (PipelineVehicle2PolicyLink ld in from c in batch from o in c.Organisations from v in o.Vehicles select v.V2Po_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }

                    }
                    else  if ( treeView1.SelectedNode.Parent.Parent.Parent.Parent.Parent.Tag is PipelinePerson ) // vehicle under org under person
                    {
                        foreach (PipelineVehicle2PolicyLink ld in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations from ov in o.Vehicles select ov.V2Po_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }
                    }
                }
            }
            #endregion

            #region PipelineVehicle2AddressLink
            else if (treeView1.SelectedNode.Tag is PipelineVehicle2AddressLink)  // in vehicle
            {
                if (treeView1.SelectedNode.Parent.Tag is PipelineVehicle)
                {
                    if (treeView1.SelectedNode.Parent.Parent.Parent.Tag is PipelineMotorClaim)  // veh under claim
                    {
                        foreach (PipelineVehicle2AddressLink ld in from c in batch from v in c.Vehicles select v.V2A_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }
                    }
                }
            }
            #endregion

            #region PipelineVehicle2VehicleLink
            else if (treeView1.SelectedNode.Tag is PipelineVehicle2VehicleLink)  // in vehicle
            {
                if (treeView1.SelectedNode.Parent.Tag is PipelineVehicle)
                {
                    if( treeView1.SelectedNode.Parent.Parent.Parent.Tag is PipelineMotorClaim )  // veh under claim
                    {
                        foreach (PipelineVehicle2VehicleLink ld in from c in batch from v in c.Vehicles select v.V2V_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }
                    }
                    else if ( treeView1.SelectedNode.Parent.Parent.Parent.Parent.Parent.Tag is PipelineMotorClaim ) // vehicle under org under claim
                    {
                        foreach (PipelineVehicle2VehicleLink ld in from c in batch from o in c.Organisations from v in o.Vehicles select v.V2V_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }

                    }
                    else  if ( treeView1.SelectedNode.Parent.Parent.Parent.Parent.Parent.Tag is PipelinePerson ) // vehicle under org under person
                    {
                        foreach (PipelineVehicle2VehicleLink ld in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations from ov in o.Vehicles select ov.V2V_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(ld, prop));
                        }
                    }
                }
            }
            #endregion

            #region PipelineTelephoneLink
            else if (treeView1.SelectedNode.Tag is PipelineTelephoneLink)
            {
                if ( treeView1.SelectedNode.Parent.Parent.Parent.Tag is PipelinePerson)
                {
                    foreach (PipelineTelephoneLink t in from c in batch from v in c.Vehicles from p in v.People from t in p.Telephones select t.PO2T_LinkData)
                    {
                        listBox1.Items.Add(ProcessEntity(t, prop));
                    }
                }
                else if (treeView1.SelectedNode.Parent.Parent.Tag is PipelineOrganisation)
                {
                    if (treeView1.SelectedNode.Parent.Parent.Parent.Parent.Tag is PipelineMotorClaim)
                    {
                        foreach (PipelineTelephoneLink t in from c in batch from o in c.Organisations from t in o.Telephones select t.PO2T_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(t, prop));
                        }
                    }
                    else
                    {
                        foreach (PipelineTelephoneLink t in from c in batch from v in c.Vehicles from p in v.People from o in p.Organisations from t in o.Telephones select t.PO2T_LinkData)
                        {
                            listBox1.Items.Add(ProcessEntity(t, prop));
                        }
                    }
                }
            }
            #endregion
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            treeView1.CollapseAll();

            treeView1.SelectedNode = ((ListItemDetails)listBox1.SelectedItem).ownerNode;

            treeView1.SelectedNode.EnsureVisible();

            treeView1.Refresh();
        }

        private void btnAbort_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.WorkerSupportsCancellation == true)
            {
                // Cancel the asynchronous operation.
                backgroundWorker1.CancelAsync();

                chkMatch.Checked = false;
            }

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            if (worker.CancellationPending == true)
            {
                e.Cancel = true;
                return;
            }
            else
            {
                try
                {
                    using (CurrentContext ctx = new CurrentContext(comboSelectedIndex, 0, "Admin"))
                    {
                        ctx.ClientName = clientName;

                        p = (Pipeline)MDA.Pipeline.Pipeline.CreatePipeline("Motor", ctx.RiskClientId);

                        MatchingService = p.CreateMatchingService(ctx);

                        //if (_trace) ADATrace.SwitchToNewUniqueLogFile(comboSelectedIndex, "Mapping Util", "Process-" +
                        //         MDA.Common.Helpers.FileNameHelper.CreateCleanFileName(claimToProcess.RiskClaim.ClientClaimRefNumber));

                        ClaimCreationStatus currentCreateStatus = new ClaimCreationStatus() { RiskBatchId = 0 };

                        batch = new List<PipelineMotorClaim>();

                        p.CreateMappingService().ConvertToClaimBatch(ctx, fs, currentCreateStatus, BuildClaimsIntoBatch, out mappingResults);

                        if (chkCleanse.Checked)
                        {
                            foreach (var c in batch)
                            {
                                cleanCnt++;
                                c.CleanseResults = p.CreateCleansingService(ctx).CleanseTheClaim(c);
                                worker.ReportProgress(0);

                                if (worker.CancellationPending == true)
                                {
                                    e.Cancel = true;
                                    return;
                                }
                            }
                        }

                        if (chkValidate.Checked)
                        {
                            foreach (var c in batch)
                            {
                                validCnt++;
                                c.ValidationResults = p.CreateValidationService().ValidateClaim(c);
                                worker.ReportProgress(0);

                                if (worker.CancellationPending == true)
                                {
                                    e.Cancel = true;
                                    return;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string m = ex.Message;

                    while (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                        m += " INNER: " + ex.Message;
                    }

                    MessageBox.Show(m);
                }

                System.Threading.Thread.Sleep(500);
                worker.ReportProgress(0);
            }

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            DisplayLabel();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnAbort.Visible = false;

            PopulateTreeView(batch, mappingResults);

            if ( chkMatch.Checked )
                btnFullMatch.PerformClick();
        }

        private void btnCollapse_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
                treeView1.SelectedNode.Collapse();
        }

        private void btnExpand_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
                treeView1.SelectedNode.ExpandAll();
        }

        private void btnCollAll_Click(object sender, EventArgs e)
        {
            treeView1.CollapseAll();
        }

        MatchReport matchReport;
        PipelineVehicle _currentVehicle;
        PipelineMotorClaim _currentClaim;
        PipelineOrganisation _currentOrg;

        private int CallBackEntityRecord(CurrentContext ctx, MDA.Pipeline.Model.PipeEntityBase pr)
        {
            if (pr == null) return 0;

            if (pr is PipelineVehicle)
            {
                PipelineVehicle v = (PipelineVehicle)pr;
                _currentVehicle = v;

                List<PipelineAddress> addresses = new List<PipelineAddress>();

                foreach (var person in v.People)
                {
                    foreach (var address in person.Addresses)
                    {
                        addresses.Add(address);
                    }
                }

                var r = MatchingService.MatchVehicle((PipelineVehicle)pr, addresses, null);

                matchReport.VehicleMatchType[((int)r.MatchTypeFound)+1] += 1;
            }
            else if (pr is PipelinePerson)
            {
                _currentOrg = null;

                var r = MatchingService.MatchPerson((PipelinePerson)pr, (PipelineVehicle)_currentVehicle,
                         (PipelineMotorClaim)_currentClaim, null);

                matchReport.PersonMatchType[((int)r.MatchTypeFound)+1] += 1;
            }
            else if (pr is PipelineAddress)
            {
                string orgName = null;

                if (_currentOrg != null)
                    orgName = _currentOrg.OrganisationName;

                var r = MatchingService.MatchAddress((PipelineAddress)pr, orgName, null);

                matchReport.AddressMatchType[((int)r.MatchTypeFound)+1] += 1;
            }
            else if (pr is PipelineBankAccount)
            {
                var r = MatchingService.MatchBankAccount((PipelineBankAccount)pr, null);

                matchReport.BankAccountMatchType[((int)r.MatchTypeFound) + 1] += 1;
            }
            else if (pr is PipelinePaymentCard)
            {
                var r = MatchingService.MatchPaymentCard((PipelinePaymentCard)pr, null);

                matchReport.PaymentCardMatchType[((int)r.MatchTypeFound) + 1] += 1;
            }
            else if (pr is PipelineMotorClaim)
            {
                _currentClaim = (PipelineMotorClaim)pr;

                var r = MatchingService.MatchClaim((PipelineMotorClaim)pr, null, null);

                matchReport.ClaimMatchType[((int)r.MatchTypeFound) + 1] += 1;
            }
            else if (pr is PipelineDrivingLicense)
            {
                var r = MatchingService.MatchLicenseNumber((PipelineDrivingLicense)pr, null);

                matchReport.DrivingLicenseMatchType[((int)r.MatchTypeFound) + 1] += 1;
            }
            else if (pr is PipelinePassport)
            {
                var r = MatchingService.MatchPassportNumber((PipelinePassport)pr, null);

                matchReport.PassportMatchType[((int)r.MatchTypeFound) + 1] += 1;
            }
            else if (pr is PipelineEmail)
            {
                var r = MatchingService.MatchEmailAddress((PipelineEmail)pr, null);

                matchReport.EmailMatchType[((int)r.MatchTypeFound) + 1] += 1;
            }
            else if (pr is PipelineNINumber)
            {
                var r = MatchingService.MatchNINumber((PipelineNINumber)pr, null);

                matchReport.NINumberMatchType[((int)r.MatchTypeFound) + 1] += 1;
            }
            else if (pr is PipelineOrganisation)
            {
                _currentOrg = (PipelineOrganisation)pr;

                var r = MatchingService.MatchOrganisation((PipelineOrganisation)pr, null);

                matchReport.OrganisationMatchType[((int)r.MatchTypeFound) + 1] += 1;
            }
            else if (pr is PipelinePolicy)
            {
                var r = MatchingService.MatchPolicy((PipelinePolicy)pr, null, null);

                matchReport.PolicyMatchType[((int)r.MatchTypeFound) + 1] += 1;
            }
            else if (pr is PipelineTelephone)
            {
                var r = MatchingService.MatchTelephoneNumber((PipelineTelephone)pr, null);

                matchReport.TelephoneMatchType[((int)r.MatchTypeFound) + 1] += 1;
            }
            else if (pr is PipelineWebSite)
            {
                var r = MatchingService.MatchWebSite((PipelineWebSite)pr, null);

                matchReport.WebSiteMatchType[((int)r.MatchTypeFound) + 1] += 1;
            }

            return 0;
        }

        private int CallBackLinkRecord(CurrentContext ctx, MDA.Pipeline.Model.PipeLinkBase r)
        {
            if (r == null) return 0;

            return 0;
        }

        private void btnFullMatch_Click(object sender, EventArgs e)
        {
            btnAbort2.Visible = true;
            LoadBtn.Enabled = false;

            matchCnt = 0;

            matchReport = new MatchReport();

            if (backgroundWorker2.IsBusy != true)
            {
                backgroundWorker2.RunWorkerAsync();
            }
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            string CurrentClaimRef = "";

            if (backgroundWorker2.CancellationPending == true)
            {
                e.Cancel = true;
                return;
            }

            try
            {
                using (CurrentContext ctx = new CurrentContext(comboSelectedIndex, 0, "Admin"))
                {
                    IClaimService svc = new MDA.ClaimService.Motor.ClaimService(ctx);

                    foreach (var c in batch)
                    {
                        CurrentClaimRef = c.ClaimNumber;

                        svc.ScanAndCallBackPipelineClaim(ctx, c, CallBackEntityRecord, CallBackLinkRecord);
                        matchCnt++;

                        backgroundWorker2.ReportProgress(0);

                        if (backgroundWorker2.CancellationPending == true)
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                string m = "Error: Didn't like claim [" + CurrentClaimRef + "] : " + ex.Message;

                while( ex.InnerException != null )
                {
                    ex = ex.InnerException;
                    m += " INNER: " + ex.Message;
                }

                MessageBox.Show(m);
            }
        }

        private void backgroundWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            DisplayLabel();
        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnAbort2.Visible = false;
            LoadBtn.Enabled = true;

            ShowMatches dlg = new ShowMatches();
            dlg.AddMessages(matchReport.BuildMessages());

            dlg.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (backgroundWorker2.WorkerSupportsCancellation == true)
            {
                // Cancel the asynchronous operation.
                backgroundWorker2.CancelAsync();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }

    public class MatchReport
    {
        public int[] VehicleMatchType { get; set; }
        public int[] PersonMatchType { get; set; }
        public int[] AddressMatchType { get; set; }
        public int[] BankAccountMatchType { get; set; }
        public int[] PaymentCardMatchType { get; set; }
        public int[] ClaimMatchType { get; set; }
        public int[] DrivingLicenseMatchType { get; set; }
        public int[] PassportMatchType { get; set; }
        public int[] EmailMatchType { get; set; }
        public int[] NINumberMatchType { get; set; }
        public int[] OrganisationMatchType { get; set; }
        public int[] PolicyMatchType { get; set; }
        public int[] TelephoneMatchType { get; set; }
        public int[] WebSiteMatchType { get; set; }

        List<string> outputMessages { get; set; }

        public MatchReport()
        {
            VehicleMatchType        = new int[4];
            PersonMatchType         = new int[4];
            AddressMatchType        = new int[4];
            BankAccountMatchType    = new int[4];
            PaymentCardMatchType    = new int[4];
            ClaimMatchType          = new int[4];
            DrivingLicenseMatchType = new int[4];
            PassportMatchType       = new int[4];
            EmailMatchType          = new int[4];
            NINumberMatchType       = new int[4];
            OrganisationMatchType   = new int[4];
            PolicyMatchType         = new int[4];
            TelephoneMatchType      = new int[4];
            WebSiteMatchType        = new int[4];

            outputMessages = new List<string>();
        }

        public List<string> BuildMessages()
        {
            outputMessages.Clear();

            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,04}],\tTentative[{4,4}]\n", "Claim", ClaimMatchType[0], ClaimMatchType[1], ClaimMatchType[2], ClaimMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,04}],\tTentative[{4,4}]\n", "Policy", PolicyMatchType[0], PolicyMatchType[1], PolicyMatchType[2], PolicyMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,04}],\tTentative[{4,4}]\n", "Vehicle", VehicleMatchType[0], VehicleMatchType[1], VehicleMatchType[2], VehicleMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,04}],\tTentative[{4,4}]\n", "Person", PersonMatchType[0], PersonMatchType[1], PersonMatchType[2], PersonMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,04}],\tTentative[{4,4}]\n", "Orgs", OrganisationMatchType[0], OrganisationMatchType[1], OrganisationMatchType[2], OrganisationMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,4}],\tTentative[{4,4}]\n", "Address", AddressMatchType[0], AddressMatchType[1], AddressMatchType[2], AddressMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,4}],\tTentative[{4,4}]\n", "Tele", TelephoneMatchType[0], TelephoneMatchType[1], TelephoneMatchType[2], TelephoneMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,4}],\tTentative[{4,4}]\n", "Bank", BankAccountMatchType[0], BankAccountMatchType[1], BankAccountMatchType[2], BankAccountMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,4}],\tTentative[{4,4}]\n", "Card", PaymentCardMatchType[0], PaymentCardMatchType[1], PaymentCardMatchType[2], PaymentCardMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,4}],\tTentative[{4,4}]\n", "License", DrivingLicenseMatchType[0], DrivingLicenseMatchType[1], DrivingLicenseMatchType[2], DrivingLicenseMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,4}],\tTentative[{4,4}]\n", "Passport", PassportMatchType[0], PassportMatchType[1], PassportMatchType[2], PassportMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,4}],\tTentative[{4,4}]\n", "Email", EmailMatchType[0], EmailMatchType[1], EmailMatchType[2], EmailMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,4}],\tTentative[{4,4}]\n", "NI", NINumberMatchType[0], NINumberMatchType[1], NINumberMatchType[2], NINumberMatchType[3]));
            outputMessages.Add( string.Format( "{0}\t: Not Found[{1,4}],\tConfirmed[{2,4}],\tUnConfirmed[{3,4}],\tTentative[{4,4}]\n", "WebSite", WebSiteMatchType[0], WebSiteMatchType[1], WebSiteMatchType[2], WebSiteMatchType[3]));

            return outputMessages;
        }
    }

    public class ListItemDetails
    {
        public TreeNode ownerNode { get; set; }
        public string Text { get; set; }

        public ListItemDetails(TreeNode ownerNode, string txt)
        {
            this.ownerNode = ownerNode;
            this.Text = txt;
        }

        public override string ToString()
        {
            return Text;
        }
    }
    public class NodeDetails : TreeNode
    {
        public object PipelineEntity { get; set; }
        public string Text { get; set; }

        public NodeDetails(string text, object entity)
        {
            PipelineEntity = entity;
            Text = text;
            ((PipeEntityBase)entity).Tag = this;
        }

        public override string ToString()
        {
            return Text;
        }
    }

    public class ComboItem
    {
        public string Name { get; set; }
        public int Id { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
