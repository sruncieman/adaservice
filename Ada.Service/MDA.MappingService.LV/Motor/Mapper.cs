﻿using FileHelpers;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Common.Server;
using MDA.MappingService.LV.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace MDA.MappingService.LV.Motor
{
    public class Mapper
    {
        private Stream _fs;
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        private object statusTracking;
        private static bool _boolDebug = true;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        public bool BoolDebug { get; set; }
        public string FilePath { get; set; }
        public string FolderPath { get; set; }
        public string ClaimFile { get; set; }
        public string PolicyFile { get; set; }
        public FileHelperEngine ClaimEngine { get; set; }
        public FileHelperEngine PolicyEngine { get; set; }

        private string _coreClaimDataFile;
        private string _vehicleClaimDataFile;
        private bool _debug;
        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private List<string> _uniqueClaimNumberList;
        private string _claimCode;
        private IEnumerable<ClaimData> _policyItemRows;
        private IEnumerable<ClaimData> _claimItemRows;
        private string _filePath = null;
        private bool _deleteExtractedFiles;
        private int _claimCount = 0;
        private string _clientFolder;
        private StreamReader _claimDataFileStreamReader;


        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking)
        {
            this._fs = fs;
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _uniqueClaimNumberList = new List<string>();
        }

        public void AssignFiles()
        {

            if (_fs == null)
            {
                FolderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\LV=\Data\Data Received 21.04";
                ClaimFile = FolderPath + @"\LVFS_MBC2_GIOS_20150415.txt";
            }
            else
            {
                try
                {
                    _claimDataFileStreamReader = new StreamReader(_fs);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in creating csv file: " + ex);
                }

            }


        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {

                if (_fs == null)
                {
                    _coreClaimData = _coreClaimDataEngine.ReadFile(ClaimFile) as ClaimData[];
                }
                else
                {
                    _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
                }


            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        private void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            try
            {
                if (_debug)
                {
                    Console.WriteLine("Start");
                }

                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Populate File Helper Engine (core) : ", false);
                }

                _coreClaimData = _coreClaimDataEngine.ReadFile(_coreClaimDataFile) as ClaimData[];

            }

            catch (Exception ex)
            {
                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "PopulateFileHelpersEngines Error : " + ex.Message, true);
                }

                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers 
                _uniqueClaimNumberList = _coreClaimData.Where(x => x.Enquiry_External_UID != "").Select(x => x.Enquiry_External_UID).Distinct().ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }

        }

        public void Translate()
        {


            int _skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["LVSkipNum"]);
            int _takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["LVTakeNum"]);


            foreach (var claim in _uniqueClaimNumberList) //.Skip(_skipValue).Take(_takeValue))
            {

                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _coreClaimData.Where(x => x.Enquiry_External_UID == claim).ToList();


                if (uniqueClaim != null)
                {

                    #region Claim

                    var claimInfo = uniqueClaim.Where(x => x.Row_Type == "CLAIM_MOTOR").FirstOrDefault();

                    try
                    {

                        if (!string.IsNullOrEmpty(claimInfo.Enquiry_External_UID))
                        {
                            motorClaim.ClaimNumber = claimInfo.Enquiry_External_UID;
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_6))
                        {
                            DateTime incidentDate = _ConvertDate(claimInfo.Data_Column_6);
                            motorClaim.IncidentDate = incidentDate;
                        }

                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;

                        #region Claim Status

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_9))
                        {
                            switch (claimInfo.Data_Column_9.ToUpper())
                            {
                                case "CLAIM FNOL":
                                case "CLAIM UPDATE":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                    break;
                                case "CLAIM CLOSED":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                    break;
                                case "CLAIM REOPENED":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                                    break;
                                default:
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                                    break;
                            }
                        }

                        #endregion

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_4))
                        {
                            motorClaim.ExtraClaimInfo.ClaimNotificationDate = _ConvertDate(claimInfo.Data_Column_4);
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_15))
                        {
                            motorClaim.ExtraClaimInfo.ClaimCode = claimInfo.Data_Column_15;
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_26))
                        {
                            motorClaim.ExtraClaimInfo.IncidentLocation = claimInfo.Data_Column_26;
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_25))
                        {
                            motorClaim.ExtraClaimInfo.IncidentCircumstances = claimInfo.Data_Column_25;
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_17))
                        {
                            motorClaim.ExtraClaimInfo.Reserve = Convert.ToDecimal(claimInfo.Data_Column_17);
                        }

                        if (!string.IsNullOrEmpty(claimInfo.Data_Column_18))
                        {
                            motorClaim.ExtraClaimInfo.PaymentsToDate = Convert.ToDecimal(claimInfo.Data_Column_18);
                        }

                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion

                    #region Policy

                    if (!string.IsNullOrEmpty(claimInfo.Data_Column_12))
                    {
                        motorClaim.Policy.PolicyNumber = claimInfo.Data_Column_12;
                    }

                    if (!string.IsNullOrEmpty(claimInfo.Data_Column_1))
                    {
                        if (claimInfo.Data_Column_1 == "MOTOR BROKER")
                        {
                            motorClaim.Policy.PolicyType_Id = (int)PolicyType.PersonalMotor;
                        }
                        else if (claimInfo.Data_Column_1 == "COMMERCIAL")
                        {
                            motorClaim.Policy.PolicyType_Id = (int)PolicyType.CommercialMotor;
                        }

                    }

                    var additionalClaimInfo = uniqueClaim.Where(x => x.Row_Type == "ENQUIRY_ADDITIONAL_STRING").FirstOrDefault();

                    motorClaim.Policy.Insurer = "LV=";

                    if (!string.IsNullOrEmpty(additionalClaimInfo.Data_Column_6))
                    {
                        motorClaim.Policy.InsurerTradingName = additionalClaimInfo.Data_Column_6;
                    }

                    if (!string.IsNullOrEmpty(additionalClaimInfo.Data_Column_15))
                    {
                        motorClaim.Policy.Broker = additionalClaimInfo.Data_Column_15;
                    }

                    #endregion

                    #region Vehicles

                    var vehicleInfo = uniqueClaim.Where(x => x.Row_Type == "VEHICLE").ToList();

                    foreach (var v in vehicleInfo)
                    {
                        PipelineVehicle vehicle = new PipelineVehicle();

                        try
                        {

                            #region Vehicle Involvement Groups (Incident2VehcileLinkType)

                            if (v.Entity_Type.Contains("INV"))
                            {
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                            }

                            if (v.Entity_Type.Contains("TPV"))
                            {
                                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                            }

                            #endregion

                            if (v.Data_Column_1 != null)
                            {
                                vehicle.VehicleRegistration = v.Data_Column_1;
                            }

                            if (v.Data_Column_8 != null)
                            {
                                vehicle.VehicleMake = v.Data_Column_8;
                            }

                            if (v.Data_Column_9 != null)
                            {
                                vehicle.VehicleModel = v.Data_Column_9;
                            }

                            if (v.Data_Column_23 != null)
                            {
                                vehicle.DamageDescription = v.Data_Column_23;
                            }

                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping vehicle information: " + ex);
                        }

                        #region Vehicle People

                        var assetRelationshipInfo = (from a in uniqueClaim
                                                     where a.Row_Type == "ASSET_RELATIONSHIP"
                                                     && a.Data_Column_1 == v.Data_Column_1
                                                     select a.Party_External_UID).ToList();


                        var personInfo = (from pm in uniqueClaim
                                          where pm.Row_Type == "PARTY_MOTOR"
                                          && assetRelationshipInfo.Contains(pm.Party_External_UID)
                                          && pm.Data_Column_20 == ""
                                          select pm).ToList();

                        foreach (var p in personInfo)
                        {
                            bool ignorePersonFlag = false;

                            PipelinePerson person = new PipelinePerson();

                            try
                            {



                                #region PartyTypes

                                var partyTypeInfo = (from pti in uniqueClaim
                                                     where pti.Row_Type == "PARTY_ADDITIONAL_NVP"
                                                     && pti.Party_External_UID == p.Party_External_UID
                                                     select pti.Data_Column_5).ToList();

                                if (partyTypeInfo != null)
                                {

                                    if (p.Party_Type == "CLM" && partyTypeInfo.Contains("DRIVER"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                    }

                                    if (p.Party_Type == "CLM" && partyTypeInfo.Contains("CLAIMANT"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "CLMPAS" && partyTypeInfo.Contains("PASSENGER"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                    }

                                    if (p.Party_Type == "CLR" && partyTypeInfo.Contains("REPRESENTATIVE"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "OWN" && partyTypeInfo.Contains("OWNER"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "THP" && partyTypeInfo.Contains("DRIVER"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                    }

                                    if (p.Party_Type == "THP" && partyTypeInfo.Contains("INSURER"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "THP" && partyTypeInfo.Contains("RELATIVE"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "THP" && partyTypeInfo.Contains("REPRESENTATIVE"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "THP" && partyTypeInfo.Contains("SOLICITOR"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "THP" && partyTypeInfo.Contains("THIRD PARTY"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "THP" && partyTypeInfo.Contains("UNKNOWN"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "WIT" && partyTypeInfo.Contains("WITNESS"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Witness;
                                    }

                                    if (p.Party_Type == "THP" && partyTypeInfo.Contains("PASSENGER"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                    }

                                    if (p.Party_Type == "THP" && partyTypeInfo.Contains("INVESTIGATOR"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "THP" && partyTypeInfo.Contains("PEDESTRIAN"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "THP" && partyTypeInfo.Contains("LOSS ADJUSTER"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "CLR" && partyTypeInfo.Contains("POLICYHOLDER EMPLOYER"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "CLR" && partyTypeInfo.Contains("SOLICITOR"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                    if (p.Party_Type == "CLR" && partyTypeInfo.Contains("RELATIVE"))
                                    {
                                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    }

                                }

                                #endregion

                                #region Saltutation

                                if (!string.IsNullOrEmpty(p.Data_Column_1))
                                {
                                    switch (p.Data_Column_1.ToUpper())
                                    {
                                        case "MR":
                                            person.Salutation_Id = (int)Salutation.Mr;
                                            person.Gender_Id = (int)Gender.Male;
                                            break;
                                        case "MRS":
                                            person.Salutation_Id = (int)Salutation.Mrs;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                        case "MS":
                                            person.Salutation_Id = (int)Salutation.Ms;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                        case "MISS":
                                            person.Salutation_Id = (int)Salutation.Miss;
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                        case "MASTER":
                                            person.Salutation_Id = (int)Salutation.Master;
                                            person.Gender_Id = (int)Gender.Male;
                                            break;
                                        case "DR":
                                            person.Salutation_Id = (int)Salutation.Dr;
                                            person.Gender_Id = (int)Gender.Unknown;
                                            break;
                                        case "REV":
                                            person.Salutation_Id = (int)Salutation.Reverend;
                                            person.Gender_Id = (int)Gender.Unknown;
                                            break;
                                        default:
                                            person.Salutation_Id = (int)Salutation.Unknown;
                                            person.Gender_Id = (int)Gender.Unknown;
                                            break;
                                    }
                                }

                                #endregion

                                if (!string.IsNullOrEmpty(p.Data_Column_3))
                                {
                                    person.FirstName = p.Data_Column_3;
                                }

                                if (!string.IsNullOrEmpty(p.Data_Column_5))
                                {
                                    person.LastName = p.Data_Column_5;
                                }

                                if (!string.IsNullOrEmpty(p.Data_Column_6))
                                {
                                    DateTime dateOfBirth = _ConvertDate(p.Data_Column_6);
                                    person.DateOfBirth = dateOfBirth;
                                }

                                #region Gender

                                if (!string.IsNullOrEmpty(p.Data_Column_9))
                                {
                                    switch (p.Data_Column_9.ToUpper())
                                    {
                                        case "M":
                                            person.Gender_Id = (int)Gender.Male;
                                            break;
                                        case "F":
                                            person.Gender_Id = (int)Gender.Female;
                                            break;
                                    }
                                }

                                #endregion

                                #region Address

                                var addresses = (from a in uniqueClaim
                                                 where a.Row_Type == "ADDRESS"
                                                 && a.Party_External_UID == p.Party_External_UID
                                                 select a).ToList();

                                foreach (var a in addresses)
                                {
                                    bool ignoreAddressFlag = false;
                                    PipelineAddress address = new PipelineAddress();

                                    address.SubBuilding = a.Data_Column_1;
                                    address.Building = a.Data_Column_2;
                                    address.BuildingNumber = a.Data_Column_3;
                                    address.Street = a.Data_Column_4;
                                    address.Town = a.Data_Column_6;
                                    address.PostCode = a.Data_Column_8;

                                    if (a.Entity_Type == "CORR")
                                    {
                                        address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                                    }

                                    if (address.PostCode.ToUpper() == "WS11 6XH" && address.BuildingNumber == "35" && address.Street.ToUpper() == "HANNAFORD WAY" && address.Town.ToUpper() == "CANNOCK")
                                    {
                                        ignoreAddressFlag = true;
                                        if (person.FirstName.ToUpper() == "DIANNE" && person.LastName.ToUpper() == "BROOME")
                                        {
                                            person.FirstName = "";
                                            person.LastName = "";
                                        }
                                    }

                                    if (address.PostCode.ToUpper() == "PE9 4RU" && address.Street.ToUpper() == "CHURCH LANE" && address.Town.ToUpper() == "TALLINGTON")
                                    {
                                        ignoreAddressFlag = true;
                                        if (person.FirstName.ToUpper() == "HEATHER" && person.LastName.ToUpper() == "GASPON")
                                        {
                                            person.FirstName = "";
                                            person.LastName = "";
                                        }
                                    }

                                    if (address.PostCode.ToUpper().Contains("IP1 3HS") && (address.Building.ToUpper().Contains("CROWN HOUSE") && address.Building.ToUpper().Contains("CROWN STREET")))
                                        ignoreAddressFlag = true;

                                    if (address.PostCode.ToUpper().Contains("SS14 1NT") && address.Building.ToUpper().Contains("KELTING HOUSE"))
                                        ignoreAddressFlag = true;

                                    if (address.BuildingNumber == "37" && address.PostCode.ToUpper().Contains("AL4 0UN") && address.Street.ToUpper().Contains("CENTRAL DRIVE"))
                                    {
                                        ignoreAddressFlag = true;
                                        if (person.LastName.ToUpper() == "KALIN" && person.FirstName == null)
                                        {
                                            person.FirstName = "";
                                            person.LastName = "";
                                        }
                                        if ((person.FirstName.ToUpper() == "ALAN" && person.LastName.ToUpper() == "HAVERS") || (person.FirstName.ToUpper() == "THOMAS" && person.LastName.ToUpper() == "HILL"))
                                        {
                                            person.FirstName = "";
                                            person.LastName = "";
                                        }
                                    }


                                    if (!ignoreAddressFlag)
                                        person.Addresses.Add(address);


                        


                                }

                                #endregion

                                #region Contact Details

                                var partyContactInfo = (from pci in uniqueClaim
                                                        where pci.Row_Type == "CONTACT"
                                                        && pci.Party_External_UID == p.Party_External_UID
                                                        select pci).ToList();

                                foreach (var pci in partyContactInfo)
                                {

                                    if (pci.Entity_Type == "CORRESTEL1")
                                    {
                                        PipelineTelephone telephone = new PipelineTelephone();
                                        telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                        person.Telephones.Add(telephone);

                                    }
                                    else if (pci.Entity_Type == "CORRESTEL2")
                                    {
                                        PipelineTelephone telephone = new PipelineTelephone();
                                        telephone.TelephoneType_Id = (int)TelephoneType.Unknown;
                                        telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                        person.Telephones.Add(telephone);
                                    }
                                    else if (pci.Entity_Type == "GENEML")
                                    {
                                        PipelineEmail email = new PipelineEmail();
                                        email.EmailAddress = pci.Data_Column_1;
                                        person.EmailAddresses.Add(email);
                                    }

                                }

                                #endregion

                                if (person.FirstName != null && person.LastName != null)
                                {
                                    if (person.FirstName.ToUpper() == "ANNA" && person.LastName.ToUpper() == "DIVALL")
                                    { ignorePersonFlag = true; }

                                    if (person.FirstName.ToUpper() == "ROBERT" && person.LastName.ToUpper() == "HOPKIRK")
                                    { ignorePersonFlag = true; }

                                    if (person.FirstName.ToUpper() == "KAYLEIGH" && person.LastName.ToUpper() == "WEEDON")
                                    { ignorePersonFlag = true; }

                                    if (person.FirstName.ToUpper() == "JOHN" && person.LastName.ToUpper() == "BUNDOCK")
                                    { ignorePersonFlag = true; }

                                    if (person.FirstName.ToUpper() == "JACQUELINE" && person.LastName.ToUpper() == "SINCLAIR")
                                    { ignorePersonFlag = true; }

                                    if (person.FirstName.ToUpper() == "LORNA" && person.LastName.ToUpper() == "ROBINSON")
                                    { ignorePersonFlag = true; }

                                    if (person.FirstName.ToUpper() == "VANESSA" && person.LastName.ToUpper() == "BUTLER")
                                    { ignorePersonFlag = true; }

                                    if (person.FirstName.ToUpper() == "EMMA" && person.LastName.ToUpper() == "MEAD")
                                    { ignorePersonFlag = true; }

                                    if (person.FirstName.ToUpper() == "JOANNE" && person.LastName.ToUpper() == "FRENCH")
                                    { ignorePersonFlag = true; }

                                }




                                if (ignorePersonFlag)
                                {
                                    person.FirstName = "";
                                    person.LastName = "";
                                }

                                vehicle.People.Add(person);

                            }
                            catch (Exception ex)
                            {
                                throw new CustomException("Error in mapping person information: " + ex);
                            }

                        }

                        #endregion

                        motorClaim.Vehicles.Add(vehicle);
                    }

                    #endregion

                    #region Organisations

                    var organisations = (from p in uniqueClaim
                                         where p.Row_Type == "PARTY_MOTOR"
                                         && p.Data_Column_20 != ""
                                         select p).ToList();


                    foreach (var org in organisations)
                    {
                        PipelineOrganisation organisation = new PipelineOrganisation();

                        try
                        {

                            #region Party Type
                            var partyTypeInfo = (from pti in uniqueClaim
                                                 where pti.Row_Type == "PARTY_ADDITIONAL_NVP"
                                                 && pti.Party_External_UID == org.Party_External_UID
                                                 select pti.Data_Column_5).ToList();

                            if (partyTypeInfo != null)
                            {

                                if (org.Party_Type == "CHO" && partyTypeInfo.Contains("CAR HIRE"))
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.CreditHire;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire;
                                }

                                if (org.Party_Type == "CLM" && partyTypeInfo.Contains("DRIVER")) //Policy Holder
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                    organisation.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                }

                                if (org.Party_Type == "CLM" && partyTypeInfo.Contains("CLAIMANT")) //Policy Holder
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                    organisation.I2O_LinkData.OrganisationType_Id = (int)OrganisationType.PolicyHolder;
                                }

                                if (org.Party_Type == "CLR" && partyTypeInfo.Contains("REPRESENTATIVE"))
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                }

                                if (org.Party_Type == "OWN" && partyTypeInfo.Contains("OWNER"))
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                }

                                if (org.Party_Type == "THP" && partyTypeInfo.Contains("DRIVER"))
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                }

                                if (org.Party_Type == "THP" && partyTypeInfo.Contains("INSURER"))
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.Insurer;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                }

                                if (org.Party_Type == "THP" && partyTypeInfo.Contains("RELATIVE"))
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                }

                                if (org.Party_Type == "THP" && partyTypeInfo.Contains("REPRESENTATIVE"))
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                }

                                if (org.Party_Type == "THP" && partyTypeInfo.Contains("SOLICITOR"))
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                                }

                                if (org.Party_Type == "THP" && partyTypeInfo.Contains("THIRD PARTY"))
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                }

                                if (org.Party_Type == "THP" && partyTypeInfo.Contains("UNKNOWN"))
                                {
                                    organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                                    organisation.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Unknown;
                                }

                            }

                            #endregion

                            if (org.Data_Column_20 != null)
                            {
                                organisation.OrganisationName = org.Data_Column_20;
                            }

                            #region Address

                            var addresses = (from a in uniqueClaim
                                             where a.Row_Type == "ADDRESS"
                                             && a.Party_External_UID == org.Party_External_UID
                                             select a).ToList();

                            foreach (var a in addresses)
                            {
                                bool ignoreAddressFlag = false;
                                PipelineAddress address = new PipelineAddress();

                                address.SubBuilding = a.Data_Column_1;
                                address.Building = a.Data_Column_2;
                                address.BuildingNumber = a.Data_Column_3;
                                address.Street = a.Data_Column_4;
                                address.Town = a.Data_Column_6;
                                address.PostCode = a.Data_Column_8;

                                if (a.Entity_Type == "CORR")
                                {
                                    address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;
                                }


                                if (address.PostCode.ToUpper().Contains("IP1 3HS") && (address.Building.ToUpper().Contains("CROWN HOUSE") && address.Building.ToUpper().Contains("CROWN STREET")))
                                    ignoreAddressFlag = true;

                                if (address.PostCode.ToUpper().Contains("SS14 1NT") && address.Building.ToUpper().Contains("KELTING HOUSE"))
                                    ignoreAddressFlag = true;

                                if (address.BuildingNumber == "37" && address.PostCode.ToUpper().Contains("AL4 0UN") && address.Street.ToUpper().Contains("CENTRAL DRIVE"))
                                {
                                    ignoreAddressFlag = true;
                                    
                                }

                                if (!ignoreAddressFlag)
                                    organisation.Addresses.Add(address);


                            }

                            #endregion

                            #region Contact Details

                            var partyContactInfo = (from pci in uniqueClaim
                                                    where pci.Row_Type == "CONTACT"
                                                    && pci.Party_External_UID == org.Party_External_UID
                                                    select pci).ToList();

                            foreach (var pci in partyContactInfo)
                            {

                                if (pci.Entity_Type == "CORRESTEL1")
                                {
                                    PipelineTelephone telephone = new PipelineTelephone();
                                    telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                    organisation.Telephones.Add(telephone);

                                }
                                else if (pci.Entity_Type == "CORRESTEL2")
                                {
                                    PipelineTelephone telephone = new PipelineTelephone();
                                    telephone.TelephoneType_Id = (int)TelephoneType.Unknown;
                                    telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                    organisation.Telephones.Add(telephone);
                                }
                                else if (pci.Entity_Type == "GENEML")
                                {
                                    PipelineEmail email = new PipelineEmail();
                                    email.EmailAddress = pci.Data_Column_1;
                                    organisation.EmailAddresses.Add(email);
                                }

                            }

                            #endregion

                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping organisation information: " + ex);
                        }

                        motorClaim.Organisations.Add(organisation);

                    }


                    #endregion

                    #region NoneVehiclePeople

                    var assetRelationships = (from a in uniqueClaim
                                              where a.Row_Type == "ASSET_RELATIONSHIP"
                                              select a.Party_External_UID).ToList();

                    var people = (from p in uniqueClaim
                                  where p.Row_Type == "PARTY_MOTOR"
                                  && !assetRelationships.Contains(p.Party_External_UID)
                                  && p.Data_Column_20 == ""
                                  select p).ToList();

                    if (people != null)
                    {

                        if (people.Count > 0)
                        {

                            PipelineVehicle noVehicleInvolved = new PipelineVehicle();
                            noVehicleInvolved.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;

                            foreach (var p in people)
                            {
                                bool ignorePersonFlag = false;

                                PipelinePerson person = new PipelinePerson();

                                try
                                {

                                    #region PartyTypes

                                    var partyTypeInfo = (from pti in uniqueClaim
                                                         where pti.Row_Type == "PARTY_ADDITIONAL_NVP"
                                                         && pti.Party_External_UID == p.Party_External_UID
                                                         select pti.Data_Column_5).ToList();

                                    if (partyTypeInfo != null)
                                    {

                                        if (p.Party_Type == "CLM" && partyTypeInfo.Contains("DRIVER"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                        }

                                        if (p.Party_Type == "CLM" && partyTypeInfo.Contains("CLAIMANT"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "CLMPAS" && partyTypeInfo.Contains("PASSENGER"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                        }

                                        if (p.Party_Type == "CLR" && partyTypeInfo.Contains("REPRESENTATIVE"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "OWN" && partyTypeInfo.Contains("OWNER"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "THP" && partyTypeInfo.Contains("DRIVER"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                        }

                                        if (p.Party_Type == "THP" && partyTypeInfo.Contains("INSURER"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "THP" && partyTypeInfo.Contains("RELATIVE"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "THP" && partyTypeInfo.Contains("REPRESENTATIVE"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "THP" && partyTypeInfo.Contains("SOLICITOR"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "THP" && partyTypeInfo.Contains("THIRD PARTY"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "THP" && partyTypeInfo.Contains("UNKNOWN"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "WIT" && partyTypeInfo.Contains("WITNESS"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Witness;
                                        }

                                        if (p.Party_Type == "THP" && partyTypeInfo.Contains("PASSENGER"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                        }

                                        if (p.Party_Type == "THP" && partyTypeInfo.Contains("INVESTIGATOR"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "THP" && partyTypeInfo.Contains("PEDESTRIAN"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "THP" && partyTypeInfo.Contains("LOSS ADJUSTER"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "CLR" && partyTypeInfo.Contains("POLICYHOLDER EMPLOYER"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "CLR" && partyTypeInfo.Contains("SOLICITOR"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                        if (p.Party_Type == "CLR" && partyTypeInfo.Contains("RELATIVE"))
                                        {
                                            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                            person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                        }

                                    }

                                    #endregion

                                    #region Saltutation

                                    if (!string.IsNullOrEmpty(p.Data_Column_1))
                                    {
                                        switch (p.Data_Column_1.ToUpper())
                                        {
                                            case "MR":
                                                person.Salutation_Id = (int)Salutation.Mr;
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "MRS":
                                                person.Salutation_Id = (int)Salutation.Mrs;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MS":
                                                person.Salutation_Id = (int)Salutation.Ms;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MISS":
                                                person.Salutation_Id = (int)Salutation.Miss;
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                            case "MASTER":
                                                person.Salutation_Id = (int)Salutation.Master;
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "DR":
                                                person.Salutation_Id = (int)Salutation.Dr;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                            case "REV":
                                                person.Salutation_Id = (int)Salutation.Reverend;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                            default:
                                                person.Salutation_Id = (int)Salutation.Unknown;
                                                person.Gender_Id = (int)Gender.Unknown;
                                                break;
                                        }
                                    }

                                    #endregion

                                    if (!string.IsNullOrEmpty(p.Data_Column_3))
                                    {
                                        person.FirstName = p.Data_Column_3;
                                    }

                                    if (!string.IsNullOrEmpty(p.Data_Column_5))
                                    {
                                        person.LastName = p.Data_Column_5;
                                    }

                                    if (!string.IsNullOrEmpty(p.Data_Column_6))
                                    {
                                        DateTime dateOfBirth = _ConvertDate(p.Data_Column_6);
                                        person.DateOfBirth = dateOfBirth;
                                    }

                                    #region Gender

                                    if (!string.IsNullOrEmpty(p.Data_Column_9))
                                    {
                                        switch (p.Data_Column_9.ToUpper())
                                        {
                                            case "M":
                                                person.Gender_Id = (int)Gender.Male;
                                                break;
                                            case "F":
                                                person.Gender_Id = (int)Gender.Female;
                                                break;
                                        }
                                    }

                                    #endregion

                                    #region Address


                                    var addresses = (from a in uniqueClaim
                                                     where a.Row_Type == "ADDRESS"
                                                     && a.Party_External_UID == p.Party_External_UID
                                                     select a).ToList();



                                    foreach (var a in addresses)
                                    {
                                        bool ignoreAddressFlag = false;

                                        PipelineAddress address = new PipelineAddress();

                                        address.SubBuilding = a.Data_Column_1;
                                        address.Building = a.Data_Column_2;
                                        address.BuildingNumber = a.Data_Column_3;
                                        address.Street = a.Data_Column_4;
                                        address.Town = a.Data_Column_6;
                                        address.PostCode = a.Data_Column_8;

                                        if (a.Entity_Type == "CORR")
                                        {
                                            address.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
                                        }


                                        if (address.PostCode.ToUpper() == "WS11 6XH" && address.BuildingNumber == "35" && address.Street.ToUpper() == "HANNAFORD WAY" && address.Town.ToUpper() == "CANNOCK")
                                        {
                                            ignoreAddressFlag = true;
                                            if (person.FirstName.ToUpper() == "DIANNE" && person.LastName.ToUpper() == "BROOME")
                                            {
                                                person.FirstName = "";
                                                person.LastName = "";
                                            }
                                        }

                                        if (address.PostCode.ToUpper() == "PE9 4RU" && address.Street.ToUpper() == "CHURCH LANE" && address.Town.ToUpper() == "TALLINGTON")
                                        {
                                            ignoreAddressFlag = true;
                                            if (person.FirstName.ToUpper() == "HEATHER" && person.LastName.ToUpper() == "GASPON")
                                            {
                                                person.FirstName = "";
                                                person.LastName = "";
                                            }
                                        }

                                        if (address.PostCode.ToUpper().Contains("IP1 3HS") && (address.Building.ToUpper().Contains("CROWN HOUSE") && address.Building.ToUpper().Contains("CROWN STREET")))
                                            ignoreAddressFlag = true;

                                        if (address.PostCode.ToUpper().Contains("SS14 1NT") && address.Building.ToUpper().Contains("KELTING HOUSE"))
                                            ignoreAddressFlag = true;

                                        if (address.BuildingNumber == "37" && address.PostCode.ToUpper().Contains("AL4 0UN") && address.Street.ToUpper().Contains("CENTRAL DRIVE"))
                                        {
                                            ignoreAddressFlag = true;
                                            if (person.LastName.ToUpper() == "KALIN" && person.FirstName == null)
                                            {
                                                person.FirstName = "";
                                                person.LastName = "";
                                            }
                                            if ((person.FirstName.ToUpper() == "ALAN" && person.LastName.ToUpper() == "HAVERS") || (person.FirstName.ToUpper() == "THOMAS" && person.LastName.ToUpper() == "HILL"))
                                            {
                                                person.FirstName = "";
                                                person.LastName = "";
                                            }
                                        }


                                        if (!ignoreAddressFlag)
                                            person.Addresses.Add(address);
                                    }

                                    #endregion

                                    #region Contact Details

                                    var partyContactInfo = (from pci in uniqueClaim
                                                            where pci.Row_Type == "CONTACT"
                                                            && pci.Party_External_UID == p.Party_External_UID
                                                            select pci).ToList();

                                    foreach (var pci in partyContactInfo)
                                    {

                                        if (pci.Entity_Type == "CORRESTEL1")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.Landline;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);

                                        }
                                        else if (pci.Entity_Type == "CORRESTEL2")
                                        {
                                            PipelineTelephone telephone = new PipelineTelephone();
                                            telephone.TelephoneType_Id = (int)TelephoneType.Unknown;
                                            telephone.ClientSuppliedNumber = pci.Data_Column_1;
                                            person.Telephones.Add(telephone);
                                        }
                                        else if (pci.Entity_Type == "GENEML")
                                        {
                                            PipelineEmail email = new PipelineEmail();
                                            email.EmailAddress = pci.Data_Column_1;
                                            person.EmailAddresses.Add(email);
                                        }

                                    }

                                    #endregion

                                    if (person.FirstName != null && person.LastName != null)
                                    {
                                        if (person.FirstName.ToUpper() == "ANNA" && person.LastName.ToUpper() == "DIVALL")
                                        { ignorePersonFlag = true; }

                                        if (person.FirstName.ToUpper() == "ROBERT" && person.LastName.ToUpper() == "HOPKIRK")
                                        { ignorePersonFlag = true; }

                                        if (person.FirstName.ToUpper() == "KAYLEIGH" && person.LastName.ToUpper() == "WEEDON")
                                        { ignorePersonFlag = true; }

                                        if (person.FirstName.ToUpper() == "JOHN" && person.LastName.ToUpper() == "BUNDOCK")
                                        { ignorePersonFlag = true; }

                                        if (person.FirstName.ToUpper() == "JACQUELINE" && person.LastName.ToUpper() == "SINCLAIR")
                                        { ignorePersonFlag = true; }

                                        if (person.FirstName.ToUpper() == "LORNA" && person.LastName.ToUpper() == "ROBINSON")
                                        { ignorePersonFlag = true; }

                                        if (person.FirstName.ToUpper() == "VANESSA" && person.LastName.ToUpper() == "BUTLER")
                                        { ignorePersonFlag = true; }

                                        if (person.FirstName.ToUpper() == "EMMA" && person.LastName.ToUpper() == "MEAD")
                                        { ignorePersonFlag = true; }

                                        if (person.FirstName.ToUpper() == "JOANNE" && person.LastName.ToUpper() == "FRENCH")
                                        { ignorePersonFlag = true; }
                                    }


                                    if (ignorePersonFlag)
                                    {
                                        person.FirstName = "";
                                        person.LastName = "";
                                    }

                                    if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured || person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Claimant)
                                    {
                                        foreach (var vehicle in motorClaim.Vehicles)
                                        {

                                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                            {
                                                vehicle.People.Add(person);
                                            }
                                        }

                                    }
                                    else
                                    {
                                        noVehicleInvolved.People.Add(person);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw new CustomException("Error in mapping person information: " + ex);
                                }

                            }

                            if (noVehicleInvolved.People.Count() > 0)
                            {
                                motorClaim.Vehicles.Add(noVehicleInvolved);
                            }

                        }
                    }

                    #endregion

                    #region Insured Vehicle and Driver Check

                    bool insuredVehicle = false;
                    bool insuredDriver = false;

                    foreach (var vehicle in motorClaim.Vehicles)
                    {
                        if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                        {
                            insuredVehicle = true;

                            foreach (var person in vehicle.People)
                            {
                                if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                                {
                                    insuredDriver = true;
                                }

                                if (insuredDriver == false && vehicle.People.Count() == 1)
                                {
                                    person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                                    insuredDriver = true;
                                }
                            }
                        }
                    }

                    if (insuredVehicle == false)
                    {
                        PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                        defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                        }

                        motorClaim.Vehicles.Add(defaultInsuredVehicle);
                    }
                    else
                    {
                        if (insuredDriver == false)
                        {
                            PipelinePerson defaultInsuredDriver = new PipelinePerson();
                            defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                            defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                            foreach (var vehicle in motorClaim.Vehicles)
                            {
                                if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                                {

                                    vehicle.People.Add(defaultInsuredDriver);

                                }

                            }

                        }
                    }

                    #endregion

                    if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;

                }
            }


        }

        private DateTime _ConvertDate(string s)
        {
            int year = 0;
            int month = 0;
            int day = 0;

            try
            {
                year = Convert.ToInt32(s.Substring(0, 4));
                month = Convert.ToInt32(s.Substring(4, 2));
                day = Convert.ToInt32(s.Substring(6, 2));
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in method _ConvertDate: " + ex);
            }

            return new DateTime(year, month, day);
        }

    }
}
