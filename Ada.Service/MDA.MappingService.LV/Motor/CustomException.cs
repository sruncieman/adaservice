﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace MDA.MappingService.LV.Motor
{
    [Serializable]
    public class CustomException : Exception
    {
        public CustomException() { }

        public CustomException(string message) : base(message) { }

        public CustomException(string message, Exception innerException) : base(message, innerException) { }

        protected CustomException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
