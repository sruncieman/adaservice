﻿using MDA.Common.Server;
using MDA.DAL;
using RiskEngine.Model;
using RiskEngine.Scoring.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.ClaimService.Interface
{
    public interface IClaimService
    {
        void ScanAndCallBackPipelineClaim(CurrentContext ctx, MDA.Pipeline.Model.PipelineMotorClaim claim,
                                        Func<CurrentContext, MDA.Pipeline.Model.PipeEntityBase, int> ProcessEntity,
                                        Func<CurrentContext, MDA.Pipeline.Model.PipeLinkBase, int> ProcessLink);

        void ScanAndCallBackPipelineClaim(CurrentContext ctx, MDA.Pipeline.Model.PipelineMobileClaim claim,
                                Func<CurrentContext, MDA.Pipeline.Model.PipeEntityBase, int> ProcessEntity,
                                Func<CurrentContext, MDA.Pipeline.Model.PipeLinkBase, int> ProcessLink);

        void TestScanPipelineRecord(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim claim);

        Incident LoadClaimIntoDatabase(MDA.Pipeline.Model.IPipelineClaim pipeClaim, RiskClaim newRiskClaim);

        /// <summary>
        /// This is the callback method (delegate) that is called from within the Mapping code. This function is passed to mapping
        /// and is called for each CLAIM found in the batch file being processed by the S2-CREATE pipeline
        /// </summary>
        /// <param name="ctx">Current Context</param>
        /// <param name="newClaim">Claim to be loaded (serialised) into the RiskClaim table</param>
        /// <param name="o">Mapping is passed an OBJECT and this gets passed onto here. Normally used to track process (or something)</param>
        /// <returns>Returns 0 as a delegate FUNC<> call MUST return something</returns>
        int ProcessClaimIntoRiskClaimTable(CurrentContext ctx, MDA.Pipeline.Model.IPipelineClaim pipeClaim, object o);

        void Deduplicate(CurrentContext ctx);

        /// <summary>
        /// Retrieves all entities associated with a claim and score them
        /// </summary>
        /// <param name="riskClaimId">Claim to score</param>
        /// <param name="clientId">Client ID</param>
        /// <param name="scoreWithLiveRules">Use live rulegroups?</param>
        /// <param name="who">user name</param>
        /// <returns></returns>
        ClaimScoreResults ScoreRiskEntityMap(int riskClaimId, int clientId, bool scoreWithLiveRules, string who);
        ClaimScoreResults ScoreRiskEntityMap(FullClaimToScore claim, int clientId, bool scoreWithLiveRules, string who);

        /// <summary>
        /// This method is calledby ScoreClaim in the base class. You override this method to extract the RiskEntity map that will be recursively scored.
        /// This method should return a FullClaimToScore structure and populate the RootRiskEntity field in that structure.
        /// </summary>
        /// <param name="riskClaimId">The ID of the RiskClaim to be scored</param>
        /// <returns>A class that holds the RiskEntity Map and fields to hold the score</returns>
        FullClaimToScore FetchRiskEntityMapForScoring(int riskClaimId);
     }

    public class TooManyErrorsException : Exception
    {
        public TooManyErrorsException(string msg)
            : base(msg)
        { }
    }

    /// <summary>
    /// Class used to track progress in Callback from Mapping
    /// </summary>
    public class ClaimCreationStatus
    {
        public int RiskBatchId { get; set; }

        public int CountInsert { get; set; }
        public int CountUpdate { get; set; }
        public int CountSkipped { get; set; }
        public int CountErrors { get; set; }
        public int CountTotal { get; set; }

        public int ConsecutiveErrorCount { get; set; }

        public ClaimCreationStatus()
        {
            RiskBatchId = -1;

            CountInsert = 0;
            CountUpdate = 0;
            CountSkipped = 0;
            CountErrors = 0;
            CountTotal = 0;
            ConsecutiveErrorCount = 0;
        }
    }
}
