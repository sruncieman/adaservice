﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MDA.RiskService;
using MDA.DAL;
using MDA.Common.Server;

namespace RiskEngine.TestEditorDb
{
    public partial class PickClaim : Form
    {
        public PickClaim()
        {
            InitializeComponent();

            CurrentContext ctx = new CurrentContext(0, 0, "GUI");

            MDA.RiskService.RiskServices svc = new RiskServices(ctx);

            List<RiskClaim> claims = svc.GetListAllClaims();

            foreach (var x in claims)
                listBox1.Items.Add(new ListItem() { claim = x, id = x.BaseRiskClaim_Id });

            //listBox1.DataSource = claims;
            listBox1.DisplayMember = "ToString()";
        }

        public List<int> SelectedClaims
        {
            get
            {
                List<int> ids = new List<int>();

                foreach (var i in listBox1.SelectedItems)
                    ids.Add(((ListItem)(i)).id);

                return ids;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }

    public class ListItem
    {
        public int id { get; set; }
        public RiskClaim claim { get; set; }

        public override string ToString()
        {
            return claim.Id + " : " + claim.ClientClaimRefNumber;
        }
    }
}
