﻿namespace RiskEngine.TestEditorDb
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.incidentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.claimBatchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.vehiclesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.peopleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.addressesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBox6 = new System.Windows.Forms.ListBox();
            this.addressesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.organisationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBox5 = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lbPersonOrgAddrVeh = new System.Windows.Forms.ListBox();
            this.orgVehBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.lbClaimOrg = new System.Windows.Forms.ListBox();
            this.orgsClaimBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lbClaimOrgAddr = new System.Windows.Forms.ListBox();
            this.addressesOrgBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lbClaimOrgAddrVeh = new System.Windows.Forms.ListBox();
            this.orgVehBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listBox7 = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cboClient = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.incidentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimBatchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehiclesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peopleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.organisationsBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orgVehBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orgsClaimBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesOrgBindingSource1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orgVehBindingSource1)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.DataSource = this.incidentsBindingSource;
            this.listBox1.DisplayMember = "DisplayText";
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(10, 32);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(151, 368);
            this.listBox1.TabIndex = 1;
            this.listBox1.DoubleClick += new System.EventHandler(this.ClaimEdit_Click);
            // 
            // incidentsBindingSource
            // 
            this.incidentsBindingSource.DataMember = "Incidents";
            this.incidentsBindingSource.DataSource = this.claimBatchBindingSource;
            // 
            // claimBatchBindingSource
            // 
            this.claimBatchBindingSource.DataSource = typeof(RiskEngine.Scoring.Entities.ClaimBatchTest);
            this.claimBatchBindingSource.CurrentChanged += new System.EventHandler(this.claimBatchBindingSource_CurrentChanged);
            // 
            // listBox2
            // 
            this.listBox2.DataSource = this.vehiclesBindingSource;
            this.listBox2.DisplayMember = "DisplayText";
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(6, 19);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(171, 446);
            this.listBox2.TabIndex = 6;
            this.listBox2.DoubleClick += new System.EventHandler(this.btnVehicleEdit_Click);
            // 
            // vehiclesBindingSource
            // 
            this.vehiclesBindingSource.DataMember = "IncidentVehicles";
            this.vehiclesBindingSource.DataSource = this.incidentsBindingSource;
            // 
            // listBox3
            // 
            this.listBox3.DataSource = this.peopleBindingSource;
            this.listBox3.DisplayMember = "DisplayText";
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Location = new System.Drawing.Point(9, 35);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(188, 160);
            this.listBox3.TabIndex = 11;
            this.listBox3.DoubleClick += new System.EventHandler(this.btnPeopleEdit_Click);
            // 
            // peopleBindingSource
            // 
            this.peopleBindingSource.DataMember = "VehiclePeople";
            this.peopleBindingSource.DataSource = this.vehiclesBindingSource;
            // 
            // listBox4
            // 
            this.listBox4.DataSource = this.addressesBindingSource;
            this.listBox4.DisplayMember = "DisplayText";
            this.listBox4.FormattingEnabled = true;
            this.listBox4.Location = new System.Drawing.Point(203, 35);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(216, 160);
            this.listBox4.TabIndex = 21;
            this.listBox4.DoubleClick += new System.EventHandler(this.btnPerAddrEdit_Click);
            // 
            // addressesBindingSource
            // 
            this.addressesBindingSource.DataMember = "PersonsAddresses";
            this.addressesBindingSource.DataSource = this.peopleBindingSource;
            // 
            // listBox6
            // 
            this.listBox6.DataSource = this.addressesBindingSource1;
            this.listBox6.DisplayMember = "DisplayText";
            this.listBox6.FormattingEnabled = true;
            this.listBox6.Location = new System.Drawing.Point(203, 215);
            this.listBox6.Name = "listBox6";
            this.listBox6.Size = new System.Drawing.Size(218, 121);
            this.listBox6.TabIndex = 26;
            this.listBox6.DoubleClick += new System.EventHandler(this.btnOrgAddrEdit_Click);
            // 
            // addressesBindingSource1
            // 
            this.addressesBindingSource1.DataMember = "OrganisationsAddresses";
            this.addressesBindingSource1.DataSource = this.organisationsBindingSource;
            // 
            // organisationsBindingSource
            // 
            this.organisationsBindingSource.DataMember = "PersonsOrganisations";
            this.organisationsBindingSource.DataSource = this.peopleBindingSource;
            // 
            // listBox5
            // 
            this.listBox5.DataSource = this.organisationsBindingSource;
            this.listBox5.DisplayMember = "DisplayText";
            this.listBox5.FormattingEnabled = true;
            this.listBox5.Location = new System.Drawing.Point(9, 215);
            this.listBox5.Name = "listBox5";
            this.listBox5.Size = new System.Drawing.Size(188, 121);
            this.listBox5.TabIndex = 16;
            this.listBox5.DoubleClick += new System.EventHandler(this.btnOrgEdit_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 199);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Organisations";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(200, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Addresses";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(200, 199);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Addresses";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(12, 531);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 30;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(92, 531);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 31;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xml";
            this.saveFileDialog1.Filter = "XML Files|*.xml";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "XML Files|*.xml";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lbPersonOrgAddrVeh);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.listBox3);
            this.groupBox1.Controls.Add(this.listBox4);
            this.groupBox1.Controls.Add(this.listBox5);
            this.groupBox1.Controls.Add(this.listBox6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(183, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(431, 451);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "People";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 344);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 48;
            this.label9.Text = "3rd Party Vehicles";
            // 
            // lbPersonOrgAddrVeh
            // 
            this.lbPersonOrgAddrVeh.DataSource = this.orgVehBindingSource2;
            this.lbPersonOrgAddrVeh.DisplayMember = "DisplayText";
            this.lbPersonOrgAddrVeh.FormattingEnabled = true;
            this.lbPersonOrgAddrVeh.Location = new System.Drawing.Point(11, 360);
            this.lbPersonOrgAddrVeh.Name = "lbPersonOrgAddrVeh";
            this.lbPersonOrgAddrVeh.Size = new System.Drawing.Size(199, 82);
            this.lbPersonOrgAddrVeh.TabIndex = 45;
            // 
            // orgVehBindingSource2
            // 
            this.orgVehBindingSource2.DataMember = "OrganisationsVehicles";
            this.orgVehBindingSource2.DataSource = this.organisationsBindingSource;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Person";
            // 
            // lbClaimOrg
            // 
            this.lbClaimOrg.DataSource = this.orgsClaimBindingSource;
            this.lbClaimOrg.DisplayMember = "DisplayText";
            this.lbClaimOrg.FormattingEnabled = true;
            this.lbClaimOrg.Location = new System.Drawing.Point(6, 16);
            this.lbClaimOrg.Name = "lbClaimOrg";
            this.lbClaimOrg.Size = new System.Drawing.Size(216, 160);
            this.lbClaimOrg.TabIndex = 37;
            this.lbClaimOrg.DoubleClick += new System.EventHandler(this.btnClaimOrgEdit_Click);
            // 
            // orgsClaimBindingSource
            // 
            this.orgsClaimBindingSource.DataMember = "IncidentOrganisations";
            this.orgsClaimBindingSource.DataSource = this.incidentsBindingSource;
            // 
            // lbClaimOrgAddr
            // 
            this.lbClaimOrgAddr.DataSource = this.addressesOrgBindingSource1;
            this.lbClaimOrgAddr.DisplayMember = "DisplayText";
            this.lbClaimOrgAddr.FormattingEnabled = true;
            this.lbClaimOrgAddr.Location = new System.Drawing.Point(5, 206);
            this.lbClaimOrgAddr.Name = "lbClaimOrgAddr";
            this.lbClaimOrgAddr.Size = new System.Drawing.Size(216, 121);
            this.lbClaimOrgAddr.TabIndex = 39;
            this.lbClaimOrgAddr.DoubleClick += new System.EventHandler(this.btnClaimOrgAddrEdit_Click);
            // 
            // addressesOrgBindingSource1
            // 
            this.addressesOrgBindingSource1.DataMember = "OrganisationsAddresses";
            this.addressesOrgBindingSource1.DataSource = this.orgsClaimBindingSource;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 187);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "Addresses";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listBox2);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Location = new System.Drawing.Point(243, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(623, 470);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vehicles";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox2);
            this.groupBox3.Location = new System.Drawing.Point(189, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(875, 499);
            this.groupBox3.TabIndex = 41;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Incident / Claim";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.lbClaimOrgAddrVeh);
            this.groupBox5.Controls.Add(this.lbClaimOrg);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.lbClaimOrgAddr);
            this.groupBox5.Location = new System.Drawing.Point(10, 20);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(227, 469);
            this.groupBox5.TabIndex = 41;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Organisations";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 334);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "3rd Party Vehicles";
            // 
            // lbClaimOrgAddrVeh
            // 
            this.lbClaimOrgAddrVeh.DataSource = this.orgVehBindingSource1;
            this.lbClaimOrgAddrVeh.DisplayMember = "DisplayText";
            this.lbClaimOrgAddrVeh.FormattingEnabled = true;
            this.lbClaimOrgAddrVeh.Location = new System.Drawing.Point(5, 350);
            this.lbClaimOrgAddrVeh.Name = "lbClaimOrgAddrVeh";
            this.lbClaimOrgAddrVeh.Size = new System.Drawing.Size(216, 108);
            this.lbClaimOrgAddrVeh.TabIndex = 36;
            // 
            // orgVehBindingSource1
            // 
            this.orgVehBindingSource1.DataMember = "OrganisationsVehicles";
            this.orgVehBindingSource1.DataSource = this.orgsClaimBindingSource;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.listBox7);
            this.groupBox4.Controls.Add(this.listBox1);
            this.groupBox4.Location = new System.Drawing.Point(12, 13);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(171, 498);
            this.groupBox4.TabIndex = 42;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Batch";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Incidents";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 400);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Policy";
            // 
            // listBox7
            // 
            this.listBox7.DataSource = this.incidentsBindingSource;
            this.listBox7.DisplayMember = "DisplayTextPolicy";
            this.listBox7.FormattingEnabled = true;
            this.listBox7.Location = new System.Drawing.Point(10, 416);
            this.listBox7.Name = "listBox7";
            this.listBox7.Size = new System.Drawing.Size(151, 69);
            this.listBox7.TabIndex = 5;
            this.listBox7.DoubleClick += new System.EventHandler(this.PolicyEdit_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(432, 517);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(177, 23);
            this.button1.TabIndex = 43;
            this.button1.Text = "Score selected against Live";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // cboClient
            // 
            this.cboClient.FormattingEnabled = true;
            this.cboClient.Location = new System.Drawing.Point(837, 531);
            this.cboClient.Name = "cboClient";
            this.cboClient.Size = new System.Drawing.Size(146, 21);
            this.cboClient.TabIndex = 44;
            this.cboClient.SelectedIndexChanged += new System.EventHandler(this.cboClient_SelectedIndexChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(432, 546);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(177, 23);
            this.button2.TabIndex = 45;
            this.button2.Text = "Score selected against Dev";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(223, 531);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(147, 23);
            this.button3.TabIndex = 46;
            this.button3.Text = "Fetch claim(s) from Db";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(615, 517);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(177, 23);
            this.button4.TabIndex = 47;
            this.button4.Text = "Score ALL against Live";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(615, 546);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(177, 23);
            this.button5.TabIndex = 48;
            this.button5.Text = "Score ALL against Dev";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 574);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.cboClient);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnLoad);
            this.Name = "MainForm";
            this.Text = "ADA Rules Test Editor";
            ((System.ComponentModel.ISupportInitialize)(this.incidentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.claimBatchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehiclesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peopleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.organisationsBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orgVehBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orgsClaimBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesOrgBindingSource1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orgVehBindingSource1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource claimBatchBindingSource;
        private System.Windows.Forms.BindingSource incidentsBindingSource;
        private System.Windows.Forms.BindingSource vehiclesBindingSource;
        private System.Windows.Forms.BindingSource peopleBindingSource;
        private System.Windows.Forms.BindingSource addressesBindingSource;
        private System.Windows.Forms.BindingSource addressesBindingSource1;
        private System.Windows.Forms.BindingSource organisationsBindingSource;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.ListBox listBox4;
        private System.Windows.Forms.ListBox listBox6;
        private System.Windows.Forms.ListBox listBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lbClaimOrg;
        private System.Windows.Forms.BindingSource orgsClaimBindingSource;
        private System.Windows.Forms.ListBox lbClaimOrgAddr;
        private System.Windows.Forms.BindingSource addressesOrgBindingSource1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cboClient;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox listBox7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox lbPersonOrgAddrVeh;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox lbClaimOrgAddrVeh;
        private System.Windows.Forms.BindingSource orgVehBindingSource1;
        private System.Windows.Forms.BindingSource orgVehBindingSource2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
    }
}

