﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using RiskEngine.Engine;
using RiskEngine.Interfaces;
using RiskEngine.Scoring.Entities;
using RiskEngine.Model;
using RiskEngine.Service.Components;
using MDA.DataService;
using MDA.DataService.PropertyMethods.Model;
using MDA.RiskService;
using MDA.RiskService.Model;
using MDA.Common.Server;
using MDA.ClaimService.Interface;
using MDA.ClaimService;
using RiskEngine.Scoring.Model;
using MDA.Pipeline;
using System.Diagnostics;

namespace RiskEngine.TestEditorDb
{
    public partial class MainForm : Form
    {
        public ClaimBatchTest Batch = new ClaimBatchTest();

        private RiskEngineService reService;
        private List<ListValues> orgs;
        private int _currentOrg = 0;
        private string _currentOrgText = "Default";
        

        public MainForm()
        {
            InitializeComponent();
            
            claimBatchBindingSource.DataSource = Batch;

            reService = new RiskEngineService();

            PopulateOrgs();
        }

        private void PopulateOrgs()
        {
            Dictionary<int, string> o = reService.GetClientList();

            orgs = new List<ListValues>();

            foreach (KeyValuePair<int, String> kvp in o)
            {
                ListValues lv = new ListValues() { lookup = kvp.Key, label = kvp.Value };

                orgs.Add(lv);
            }

            cboClient.DataSource = orgs;
            cboClient.SelectedIndex = _currentOrg;
            cboClient.ValueMember = "lookup";
            cboClient.DisplayMember = "label";
        }

        //static public string SerialiseToXml(System.Object o, System.Type type)
        //{
        //    StringWriter writer = new StringWriter();
        //    XmlSerializer ser = new XmlSerializer(type);
        //    ser.Serialize(writer, o);
        //    string s = writer.ToString();
        //    writer.Close();
        //    return s;
        //}

        //static public System.Object DeserialiseFromXmlString(string xmlString, System.Type type)
        //{
        //    StringReader s = new StringReader(xmlString);

        //    XmlSerializer xml = new XmlSerializer(type);
        //    object o = xml.Deserialize(s);
        //    return o;
        //}

        private void ClaimEdit_Click(object sender, EventArgs e)
        {
            EntityExplore ee = new EntityExplore();

            MotorClaimIncidentRisk eo = (MotorClaimIncidentRisk)this.incidentsBindingSource.Current;

            ee.AssignEntity(eo,"");

            ee.Show();
        }


        private void PolicyEdit_Click(object sender, EventArgs e)
        {
            EntityExplore ee = new EntityExplore();

            MotorClaimIncidentRisk eo = (MotorClaimIncidentRisk)this.incidentsBindingSource.Current;

            ee.AssignEntity(eo.IncidentPolicy, ((MotorClaimIncidentRisk)this.incidentsBindingSource.Current).DisplayText);

            ee.Show();

        }

        private void btnVehicleEdit_Click(object sender, EventArgs e)
        {
            EntityExplore ee = new EntityExplore();

            MotorClaimVehicleRisk eo = (MotorClaimVehicleRisk)this.vehiclesBindingSource.Current;

            ee.AssignEntity(eo, ((MotorClaimIncidentRisk)this.incidentsBindingSource.Current).DisplayText);

            ee.Show();
        }



        private void btnPeopleEdit_Click(object sender, EventArgs e)
        {
            EntityExplore ee = new EntityExplore();

            PersonRisk eo = (PersonRisk)this.peopleBindingSource.Current;

            ee.AssignEntity(eo, ((MotorClaimIncidentRisk)this.incidentsBindingSource.Current).DisplayText);

            ee.Show();
        }


        private void btnOrgEdit_Click(object sender, EventArgs e)
        {
            EntityExplore ee = new EntityExplore();

            OrganisationRisk eo = (OrganisationRisk)this.organisationsBindingSource.Current;

            ee.AssignEntity(eo, ((MotorClaimIncidentRisk)this.incidentsBindingSource.Current).DisplayText);

            ee.Show();
        }



        private void btnPerAddrEdit_Click(object sender, EventArgs e)
        {
            EntityExplore ee = new EntityExplore();

            AddressRisk eo = (AddressRisk)this.addressesBindingSource.Current;

            ee.AssignEntity(eo, ((MotorClaimIncidentRisk)this.incidentsBindingSource.Current).DisplayText);

            ee.Show();
        }


        private void btnOrgAddrEdit_Click(object sender, EventArgs e)
        {
            EntityExplore ee = new EntityExplore();

            AddressRisk eo = (AddressRisk)this.addressesBindingSource1.Current;

            ee.AssignEntity(eo, ((MotorClaimIncidentRisk)this.incidentsBindingSource.Current).DisplayText);

            ee.Show();
        }

        private void btnClaimOrgEdit_Click(object sender, EventArgs e)
        {
            EntityExplore ee = new EntityExplore();

            OrganisationRisk eo = (OrganisationRisk)this.orgsClaimBindingSource.Current;

            ee.AssignEntity(eo, ((MotorClaimIncidentRisk)this.incidentsBindingSource.Current).DisplayText);

            ee.Show();
        }



        private void btnClaimOrgAddrEdit_Click(object sender, EventArgs e)
        {
            EntityExplore ee = new EntityExplore();

            AddressRisk eo = (AddressRisk)this.addressesOrgBindingSource1.Current;

            ee.AssignEntity(eo, ((MotorClaimIncidentRisk)this.incidentsBindingSource.Current).DisplayText);

            ee.Show();
        }

        private void btnClaimOrgVehEdit_Click(object sender, EventArgs e)
        {
            EntityExplore ee = new EntityExplore();

            OrganisationVehicleRisk eo = (OrganisationVehicleRisk)this.orgVehBindingSource1.Current;

            ee.AssignEntity(eo, ((MotorClaimIncidentRisk)this.incidentsBindingSource.Current).DisplayText);

            ee.Show();
        }

        private void btnPersonOrgVehEdit_Click(object sender, EventArgs e)
        {
            EntityExplore ee = new EntityExplore();

            OrganisationVehicleRisk eo = (OrganisationVehicleRisk)this.orgVehBindingSource2.Current;

            ee.AssignEntity(eo, ((MotorClaimIncidentRisk)this.incidentsBindingSource.Current).DisplayText);

            ee.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FileStream fs = null;
            XmlDictionaryReader reader = null;

            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                    DataContractSerializer ser = new DataContractSerializer(typeof(ClaimBatchTest));

                    // Deserialize the data and read it from the instance.
                    ClaimBatchTest Batch = (ClaimBatchTest)ser.ReadObject(reader, true);

                    claimBatchBindingSource.DataSource = Batch;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                if ( reader != null ) reader.Close();
                if ( fs != null ) fs.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            FileStream writer = null;

            try
            {
                if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    writer = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    DataContractSerializer ser = new DataContractSerializer(typeof(ClaimBatchTest));
                    ser.WriteObject(writer, Batch);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                if (writer != null) writer.Close();
            }

        }

        private void cboClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            _currentOrg = ((ListValues)cboClient.SelectedItem).lookup;
            _currentOrgText = ((ListValues)cboClient.SelectedItem).label;
        }

        private void ScoreClaim(bool scoreWithLiveRules, string orgName, int clientId)
        {
            FullClaimToScore fullClaimToScore = new FullClaimToScore()
            {
                RiskEntityMap = (IRiskEntity)this.incidentsBindingSource.Current
            };

            CurrentContext ctx = new CurrentContext(clientId, 0, "Fred");

            Pipeline p = (Pipeline)Pipeline.CreatePipeline("Motor", clientId);

            IClaimService claimService = p.CreateClaimService(ctx);

            Stopwatch sw = Stopwatch.StartNew();

            var claimScoreResults = claimService.ScoreRiskEntityMap(fullClaimToScore, clientId, scoreWithLiveRules, "fred");

            ResetAllBindings();

            ShowScores dlg = new ShowScores();
            dlg.PopulateTreeView(claimScoreResults.CompleteScoreResults);
            dlg.Text = "Score: " + ((IRiskEntity)fullClaimToScore.RiskEntityMap).DisplayText + "[" + orgName + "] : " + sw.ElapsedMilliseconds + "ms";
            dlg.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ScoreClaim(false, cboClient.Text, cboClient.SelectedIndex);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            ScoreClaim(true, cboClient.SelectedText, cboClient.SelectedIndex);
        }


        private void button3_Click(object sender, EventArgs e)
        {
            PickClaim pc = new PickClaim();

            pc.ShowDialog();

            CurrentContext ctx = new CurrentContext(0, 0, "Fred");

            Batch.Incidents.Clear();

            foreach (int claimId in pc.SelectedClaims)
            {
                var _fullClaimToScore = new MDA.ClaimService.Motor.ClaimService(ctx).FetchRiskEntityMapForScoring(claimId);

                Batch.Incidents.Add((MotorClaimIncidentRisk)_fullClaimToScore.RiskEntityMap);
            }

            claimBatchBindingSource.DataSource = Batch;

            ResetAllBindings();
        }

        private void ResetAllBindings()
        {
            claimBatchBindingSource.ResetBindings(false);
            incidentsBindingSource.ResetBindings(false);
            vehiclesBindingSource.ResetBindings(false);
            peopleBindingSource.ResetBindings(false);
            organisationsBindingSource.ResetBindings(false);
            orgsClaimBindingSource.ResetBindings(false);
            addressesBindingSource.ResetBindings(false);
            addressesBindingSource1.ResetBindings(false);
            addressesOrgBindingSource1.ResetBindings(false);
            orgVehBindingSource1.ResetBindings(false);
            orgVehBindingSource2.ResetBindings(false);
        }

        private void claimBatchBindingSource_CurrentChanged(object sender, EventArgs e)
        {
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            int cnt = 0;
            this.incidentsBindingSource.MoveFirst();

            while( cnt < this.incidentsBindingSource.Count )
            {
                ScoreClaim(true, cboClient.SelectedText, cboClient.SelectedIndex);

                this.incidentsBindingSource.MoveNext();

                cnt++;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int cnt = 0;
            this.incidentsBindingSource.MoveFirst();

            while (cnt < this.incidentsBindingSource.Count)
            {
                ScoreClaim(false, cboClient.SelectedText, cboClient.SelectedIndex);

                this.incidentsBindingSource.MoveNext();

                cnt++;
            }
        }

    }

    public class ListValues
    {
        public int lookup { get; set; }
        public string label { get; set; }
    }
}
