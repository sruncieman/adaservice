﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RiskEngine.Scoring.Entities;
using RiskEngine.Model;
using RiskEngine.Interfaces;
using MDA.Common;

namespace RiskEngine.TestEditorDb
{
    public partial class ShowScores : Form
    {
        public ShowScores()
        {
            InitializeComponent();
        }

        private TreeNode AssignMyNodeToTreeNode(MessageNode root)
        {
            TreeNode node = new TreeNode(root.Text);

            foreach (MessageNode n in root.Nodes)
                node.Nodes.Add(AssignMyNodeToTreeNode(n));

            return node;
        }

        public void PopulateTreeView(IRiskEntity ve)
        {
            treeView1.Nodes.Clear();

            treeView1.Nodes.Add(AssignMyNodeToTreeNode(ve.GetScoresAsNodeTree(true)));

            treeView1.ExpandAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
