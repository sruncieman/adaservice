﻿using MDA.Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Common
{
    [TestClass]
    public class VehicleColourResolverTests
    {
        [TestMethod]
        public void VehicleColourIsNullCausesNullReferenceException()
        {
            // Arrange
            string colourNull = null;

            // Act
            var unknown = colourNull.DeriveVehicleColour();

            // Assert
            Assert.AreEqual(unknown, VehicleColour.Unknown);
        }

        [TestMethod]
        public void VehicleColourIsNullOrStringEmptyReturnsUnknown()
        { 
            // Arrange
            string colourNull = null;
            string colourEmpty = string.Empty;

            // Act
            var vehicleColourFromNull = colourNull.DeriveVehicleColour();
            var vehicleColourFromEmpty = colourEmpty.DeriveVehicleColour();

            //Assert
            Assert.AreEqual(vehicleColourFromNull, VehicleColour.Unknown);
            Assert.AreEqual(vehicleColourFromEmpty, VehicleColour.Unknown);
        }

        [TestMethod]
        public void VehicleColourIsInEnumsReturnsSameEnum()
        {
            // Arrange
            var black = VehicleColour.Black;
            string colour = black.ToString();

            // Act
            var vehicleColour = colour.DeriveVehicleColour();

            //Assert
            Assert.AreEqual(vehicleColour, black);
        }
    }
}
