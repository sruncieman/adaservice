﻿using System;
using ADAFtpPushReportToClient;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Utilities.ADAFtpPushReportToClient
{
    [TestClass]
    public class StandardTests
    {
        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_Factory_Create_WhenNoParam1_ThrowsException()
        {
            // Arrange
            
            // Act
            Action action = () => Factory.Create(null);

            // Assert
            action.ShouldThrow<ArgumentException>();
        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_Factory_Create_WhenInvalidParam1_ThrowsException()
        {
            // Arrange

            // Act
            Action action = () => Factory.Create("WrongInsurer");

            // Assert
            action.ShouldThrow<ArgumentException>();
        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_Factory_Create_WhenValidParam1_Returns_ReportService()
        {
            // Arrange

            // Act
            var sut = Factory.Create("DIRECTLINE");

            // Assert
            sut.Should().BeOfType<ReportService>();            
        }


        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_SmtpService_ctor_WhenInvalidParam1_ThrowsException()
        {
            // Arrange

            // Act
            Action action = () => new SmtpService(null);

            // Assert
            action.ShouldThrow<ArgumentException>();
        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_Extensions_Output_WhenNull_ReturnsStringEmpty()
        {
            // Arrange
            Exception ex = null;
            
            // Act
            var result = ex.Output();
            
            // Assert
            result.ShouldBeEquivalentTo(string.Empty);            
        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_Extensions_Output_WhenInnerException_ReturnsBoth()
        {
            // Arrange
            var ex = new Exception("OuterError", new Exception("Inner Exception"));

            // Act
            var result = ex.Output();

            // Assert
            result.Should().ContainEquivalentOf("Inner Exception");
        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_ConfigService_ctor_WhenInvalidParam1_ThrowsException()
        {
            // Arrange

            // Act
            Action action = () => ConfigService.Get(null);

            // Assert
            action.ShouldThrow<ArgumentException>();
        }

    }
}
