﻿using System;
using ADAFtpPushReportToClient;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Utilities.ADAFtpPushReportToClient
{
    [TestClass]
    public class FtpServiceTests
    {
        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FtpService_ctor_WhenNoParam_ThrowsException()
        {
            // Arrange
                        
            // Act
            Action action = () => new FtpService(null);

            // Assert
            action.ShouldThrow<ArgumentNullException>();

        }
    }
}
