﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions.TestingHelpers;
using ADAFtpPushReportToClient;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Utilities.ADAFtpPushReportToClient
{
    [TestClass]
    public class FileCheckServiceTests
    {
        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_GetFileNamesInFolder_WhenNoPath_ThrowsException()
        {
            // Arrange
            var sut = new FileCheckService();
            
            // Act
            Action action = () => sut.GetFileNamesInFolder(null);

            // Assert
            action.ShouldThrow<ArgumentNullException>();

        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_GetFileNamesInFolder_WhenNoFiles_ReturnsNull()
        {
            // Arrange 
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>());
            var folder = "C:\\temp\\";
            
            fileSystem.AddDirectory(folder);
            var sut = new FileCheckService(fileSystem);

            // Act            
            var result = sut.GetFileNamesInFolder(folder);

            // Assert
            result.Should().BeNull();

        }


        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_GetFileNamesInFolder_When3Files_Returns3FileNames()
        {
            // Arrange 
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { "c:\\temp\\file1.txt", new MockFileData("Testing File1.") },
                { "c:\\temp\\file2.txt", new MockFileData("Testing File2.") },
                { "c:\\temp\\file3.txt", new MockFileData("Testing File3.") },
            });

            var folder = "C:\\temp\\";

            fileSystem.AddDirectory(folder);
            var sut = new FileCheckService(fileSystem);

            // Act            
            var result = sut.GetFileNamesInFolder(folder);

            // Assert
            result.Should().NotBeNull();
            result.Length.ShouldBeEquivalentTo(3);

        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_DeleteFilesInFolderOlderThanXDays_Given3Files_When2OldFiles_Deletes2Files()
        {
            // Arrange 

            var folder = "c:\\temp\\";

            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { folder + "file1.txt", new MockFileData("Testing File1.") },
                { folder + "file2.txt", new MockFileData("Testing File2.") },
                { folder + "file3.txt", new MockFileData("Testing File3.") },
            });
            

            var file = new MockFile(fileSystem);            
            file.SetCreationTime(folder + "file1.txt", DateTime.Now.AddDays(-35));
            file.SetCreationTime(folder + "file2.txt", DateTime.Now.AddDays(-35));
            file.SetCreationTime(folder + "file3.txt", DateTime.Now.AddDays(-5));

                        
            var sut = new FileCheckService(fileSystem);

            // Act            
            var result = sut.DeleteFilesInFolderOlderThanXDays(folder, 30);
            var remainingFiles = sut.GetFileNamesInFolder(folder);

            // Assert
            result.Should().BeTrue();
            remainingFiles.Should().NotBeNull();
            remainingFiles.Length.ShouldBeEquivalentTo(1);            
        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_DeleteFilesInFolderOlderThanXDays_When0Files_ReturnsTrue()
        {
            // Arrange 

            var folder = "c:\\temp";

            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
        
            });
            
            fileSystem.AddDirectory(folder);

            var sut = new FileCheckService(fileSystem);

            // Act            
            var result = sut.DeleteFilesInFolderOlderThanXDays(folder, 30);
            var remainingFiles = sut.GetFileNamesInFolder(folder);

            // Assert
            result.Should().BeTrue();
            remainingFiles.Should().BeNull();            
        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_DeleteFilesInFolderOlderThanXDays_WhenNoPath_ThrowsException()
        {
            // Arrange
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>());
            var sut = new FileCheckService(fileSystem);

            // Act
            Action action = () => sut.DeleteFilesInFolderOlderThanXDays(null, 0);

            // Assert
            action.ShouldThrow<ArgumentNullException>();

        }


        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_DeleteFilesInFolderOlderThanXDays_WhenDaysInvalid_ThrowsException()
        {
            // Arrange
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>());
            var sut = new FileCheckService(fileSystem);

            // Act
            Action action = () => sut.DeleteFilesInFolderOlderThanXDays("C:\\Temp123", -20);

            // Assert
            action.ShouldThrow<ArgumentOutOfRangeException>();

        }


        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_MoveFile_WhenPathFromNull_ThrowsException()
        {
            // Arrange
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>());
            var sut = new FileCheckService(fileSystem);

            // Act
            Action action = () => sut.MoveFile(null, "c:\\path");

            // Assert
            action.ShouldThrow<ArgumentNullException>();

        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_MoveFile_WhenPathToNull_ThrowsException()
        {
            // Arrange
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>());
            var sut = new FileCheckService(fileSystem);

            // Act
            Action action = () => sut.MoveFile("c:\\path", null);

            // Assert
            action.ShouldThrow<ArgumentNullException>();

        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_MoveFile_WhenPathFromInvalid_ThrowsException()
        {
            // Arrange
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>());
            var sut = new FileCheckService(fileSystem);

            // Act
            Action action = () => sut.MoveFile("c:\\path", "C:\\path2");

            // Assert
            action.ShouldThrow<ArgumentException>();

        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_MoveFile_WhenPathToInvalid_ThrowsException()
        {
            // Arrange
            var file = "c:\\temp\\file1.txt";

            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { file, new MockFileData("Testing File1.") }, 
            });

            var sut = new FileCheckService(fileSystem);            

            // Act
            Action action = () => sut.MoveFile(file, "C:\\path2");

            // Assert
            action.ShouldThrow<ArgumentException>();

        }


        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_MoveFile_WhenOk_MovesFile()
        {
            // Arrange 

            var file = "c:\\temp\\file1.txt";
            var folder = "C:\\temp2\\";

            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { file, new MockFileData("Testing File1.") },                
            });

            

            fileSystem.AddDirectory(folder);
            var sut = new FileCheckService(fileSystem);

            // Act            
            var result = sut.MoveFile(file, folder);
            var filesMoved = sut.GetFileNamesInFolder(folder);

            // Assert
            result.Should().BeTrue();
            filesMoved.Should().NotBeNull();
            filesMoved.Length.ShouldBeEquivalentTo(1);            

        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_FileCheckService_MoveFile_WhenFileAlreadyExists_StillMovesFileAndRenames()
        {
            // Arrange 

            var file1 = "c:\\temp\\file1.txt";
            var file2 = "c:\\temp2\\file1.txt";

            var folder = "C:\\temp2\\";

            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { file1, new MockFileData("Testing File1.") },                
                { file2, new MockFileData("Testing File1.") },                
            });

            var sut = new FileCheckService(fileSystem);

            // Act            
            var result = sut.MoveFile(file1, folder);
            var filesMoved = sut.GetFileNamesInFolder(folder);

            // Assert
            result.Should().BeTrue();
            filesMoved.Should().NotBeNull();
            filesMoved.Length.ShouldBeEquivalentTo(2);
            filesMoved[1].Should().NotContainEquivalentOf(file1);

        }


    }
}
