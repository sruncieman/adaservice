﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions.TestingHelpers;
using ADAFtpPushReportToClient;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Tests.Utilities.ADAFtpPushReportToClient
{
    [TestClass]
    public class ReportServiceTests
    {
        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_ReportService_ctor_WhenNoParam1_ThrowsException()
        {
            // Arrange
            var param1 = new FileCheckService();            
            var param2 = new FtpService(new Config());
            var param3 = new Mock<ISmtpService>().Object;
            var param4 = new Config();            
            
            // Act
            Action action = () => new ReportService(null, param2, param3, param4);

            // Assert
            action.ShouldThrow<ArgumentNullException>();

        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_ReportService_ctor_WhenNoParam2_ThrowsException()
        {
            // Arrange
            var param1 = new FileCheckService();
            var param2 = new FtpService(new Config());
            var param3 = new Mock<ISmtpService>().Object;
            var param4 = new Config();

            // Act
            Action action = () => new ReportService(param1, null, param3, param4);

            // Assert
            action.ShouldThrow<ArgumentNullException>();

        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_ReportService_ctor_WhenNoParam3_ThrowsException()
        {
            // Arrange
            var param1 = new FileCheckService();
            var param2 = new FtpService(new Config());
            var param3 = new Mock<ISmtpService>().Object;
            var param4 = new Config();

            // Act
            Action action = () => new ReportService(param1, param2, null, param4);

            // Assert
            action.ShouldThrow<ArgumentNullException>();

        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_ReportService_ctor_WhenNoParam4_ThrowsException()
        {
            // Arrange
            var param1 = new FileCheckService();
            var param2 = new FtpService(new Config());
            var param3 = new Mock<ISmtpService>().Object;
            var param4 = new Config();

            // Act
            Action action = () => new ReportService(param1, param2, param3, null);

            // Assert
            action.ShouldThrow<ArgumentNullException>();

        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_ReportService_ProcessFiles_WhenNone_Returns()
        {
            // Arrange
            var fakeFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>());
            
            var folderFrom = "c:\\temp1";
            var folderTo = "c:\\temp2";
            fakeFileSystem.AddDirectory(folderFrom);
            fakeFileSystem.AddDirectory(folderTo);

            var fileCheckService = new FileCheckService(fakeFileSystem);
            var ftpService = new FakeFtpService();
            var config = new Config
            {
                MonitorFolderNetworkAddress = folderFrom, 
                ArchiveFolderNetworkAddress = folderTo,
                KeoghsSuccessRecipients = "sreynolds@keoghs.co.uk",
                KeoghsErrorRecipients = "sreynolds@keoghs.co.uk"
            };
            var smtp = new Mock<ISmtpService>().Object;

            var sut = new ReportService(fileCheckService, ftpService, smtp, config);

            // Act
            Action action = () => sut.ProcessFilesDirToKeoghsFTP();
            var filesMoved = fileCheckService.GetFileNamesInFolder(folderTo);

            // Assert
            action.ShouldNotThrow<Exception>();
            filesMoved.Should().BeNull();
        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_ReportService_ProcessFiles_When3_Returns()
        {
            // Arrange
            var folderFrom = "c:\\temp1";
            var folderTo = "c:\\temp2";
            
            var fakeFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { folderFrom + "\\file1.txt", new MockFileData("Testing File1.") },
                { folderFrom + "\\file2.txt", new MockFileData("Testing File2.") },
                { folderFrom + "\\file3.txt", new MockFileData("Testing File3.") },
            });

            
            fakeFileSystem.AddDirectory(folderFrom);
            fakeFileSystem.AddDirectory(folderTo);

            var fileCheckService = new FileCheckService(fakeFileSystem);
            var ftpService = new FakeFtpService();
            var config = new Config
            {
                MonitorFolderNetworkAddress = folderFrom, 
                ArchiveFolderNetworkAddress = folderTo,
                KeoghsSuccessRecipients = "sreynolds@keoghs.co.uk",
                KeoghsErrorRecipients = "sreynolds@keoghs.co.uk"
            };
            var smtp = new Mock<ISmtpService>().Object;

            var sut = new ReportService(fileCheckService, ftpService, smtp, config);

            // Act
            sut.ProcessFilesDirToKeoghsFTP();
            var filesMoved = fileCheckService.GetFileNamesInFolder(folderTo);

            // Assert            
            filesMoved.Length.ShouldBeEquivalentTo(3);
        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_ReportService_ProcessFiles_WhenSubComponentErrors_CatchesErrorAndReturns()
        {
            // Arrange
            var folderFrom = "c:\\temp1";
            var folderTo = "c:\\temp2";

            var fakeFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { folderFrom + "\\file1.txt", new MockFileData("Testing File1.") },
                { folderFrom + "\\file2.txt", new MockFileData("Testing File2.") },
                { folderFrom + "\\file3.txt", new MockFileData("Testing File3.") },
            });


            fakeFileSystem.AddDirectory(folderFrom);
            fakeFileSystem.AddDirectory(folderTo);

            var fileCheckService = new FileCheckService(fakeFileSystem);
            var ftpService = new FakeFtpService();
            var config = new Config
            {
                MonitorFolderNetworkAddress = folderFrom,
                ArchiveFolderNetworkAddress = null,
                KeoghsSuccessRecipients = "sreynolds@keoghs.co.uk",
                KeoghsErrorRecipients = "sreynolds@keoghs.co.uk"
            };
            var smtp = new Mock<ISmtpService>().Object;

            var sut = new ReportService(fileCheckService, ftpService, smtp, config);

            // Act
            Action action = () => sut.ProcessFilesDirToKeoghsFTP();            

            // Assert
            action.ShouldNotThrow<Exception>();            
        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_ReportService_TidyUp_WhenCalled_Returns()
        {
            // Arrange
            var folderFrom = "c:\\temp1";
            var folderTo = "c:\\temp2";

            var fakeFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { folderFrom + "\\file1.txt", new MockFileData("Testing File1.") },
                { folderFrom + "\\file2.txt", new MockFileData("Testing File2.") },
                { folderFrom + "\\file3.txt", new MockFileData("Testing File3.") },
            });


            fakeFileSystem.AddDirectory(folderFrom);
            fakeFileSystem.AddDirectory(folderTo);

            var fileCheckService = new FileCheckService(fakeFileSystem);
            var ftpService = new FakeFtpService();
            var config = new Config
            {
                MonitorFolderNetworkAddress = folderFrom,
                ArchiveFolderNetworkAddress = folderTo,
                ArchiveFolderDeletePeriodDays = 10,
                KeoghsSuccessRecipients = "sreynolds@keoghs.co.uk",
                KeoghsErrorRecipients = "sreynolds@keoghs.co.uk"
            };

            var smtp = new Mock<ISmtpService>().Object;

            var sut = new ReportService(fileCheckService, ftpService, smtp, config);

            // Act
            Action action = () => sut.TidyUp();

            // Assert
            action.ShouldNotThrow<Exception>();
        }

        [TestMethod]
        [TestCategory("Standard")]
        public void T1_Utilities_AdaFtpPushReportToClient_ReportService_TidyUp_WhenSubComponentErrors_CatchesErrorAndReturns()
        {
            // Arrange
            var folderFrom = "c:\\temp1";
            var folderTo = "c:\\temp2";

            var fakeFileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { folderFrom + "\\file1.txt", new MockFileData("Testing File1.") },
                { folderFrom + "\\file2.txt", new MockFileData("Testing File2.") },
                { folderFrom + "\\file3.txt", new MockFileData("Testing File3.") },
            });


            fakeFileSystem.AddDirectory(folderFrom);
            fakeFileSystem.AddDirectory(folderTo);

            var fileCheckService = new FileCheckService(fakeFileSystem);
            var ftpService = new FakeFtpService();
            var config = new Config
            {
                MonitorFolderNetworkAddress = folderFrom,
                ArchiveFolderNetworkAddress = null,
                ArchiveFolderDeletePeriodDays = -10,
                KeoghsSuccessRecipients = "sreynolds@keoghs.co.uk",
                KeoghsErrorRecipients = "sreynolds@keoghs.co.uk"
            };
            var smtp = new Mock<ISmtpService>().Object;

            var sut = new ReportService(fileCheckService, ftpService, smtp, config);

            // Act
            Action action = () => sut.TidyUp();

            // Assert
            action.ShouldNotThrow<Exception>(); 
        }

        public class FakeFtpService : IFtpService
        {
            public void SendFileToKeoghs(string fileName)
            {
                return;
            }

            public void GetFilesFromKeoghs()
            {
                return;
            }


            public void SendFilesToClient()
            {
                return;
            }
        }
    }
}
