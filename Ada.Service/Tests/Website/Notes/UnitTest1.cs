﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KeoghsClientSite.Controllers;
using System.Web.Mvc;
using KeoghsClientSite;

namespace Tests.Website.Notes
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GivenManyReturnMultiple()
        {
            NoteController noteController = new NoteController();
            
            int[] selectedValues = new int[] { 1, 4, 3 };

            //ViewResult result = noteController.AddNote(selectedValues) as ViewResult;

            string expected = "Multiple";

            //string actual = result.ViewBag.Output;

            //Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GivenOneReturnSingle()
        {
            NoteController noteController = new NoteController();

            int[] selectedValues = new int[] { 1 };

            //ViewResult result = noteController.AddNote(selectedValues) as ViewResult;

            string expected = "Single";

            //string actual = result.ViewBag.Output;

            //Assert.AreEqual(expected, actual);
        }
    }
}
