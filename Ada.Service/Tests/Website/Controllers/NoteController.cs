﻿using KeoghsClientSite.Controllers;
using KeoghsClientSite.Interfaces;
using KeoghsClientSite.Models.Note;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.JustMock;

namespace Tests.Website.Controllers
{
    [TestClass]
    public class NoteControllerTest
    {

        [TestMethod]
        public void InsertNoteIntoDBGet()
        {
            // Arrange
            NoteController controller = new NoteController();

            // Act
            //ViewResult result = controller.InsertNote() as ViewResult;

            // Assert
            //Assert.IsNotNull(result);
        }

        [TestMethod]
        public void InsertNoteIntoDBSuccessPost()
        {
            //Arrange
            //var noteRepository = Mock.Create<IRepository>();

            //var note = new Note{RiskClaim_Id = 1234, 
            //    BaseRiskClaim_Id = 1234, 
            //    CreatedBy = "JonathanWalker@keoghs.co.uk", 
            //    CreatedDate = DateTime.Now, 
            //    Decision = null,
            //    Deleted = false, 
            //    NoteDesc = "Inserting a new note,",
            //    Visibilty = 0};

            //Mock.Arrange(() => noteRepository.InsertNote(note)).Returns(1).MustBeCalled();

            ////Act
            //NoteController controller = new NoteController(noteRepository);
            //ViewResult viewResult = controller.InsertNote(note);
            //var model = viewResult.Model;

            ////Assert
            //Assert.AreEqual(1, model);
        }

        [TestMethod]
        public void InsertNoteIntoDBErrorPost()
        {
            //Arrange
            //var noteRepository = Mock.Create<IRepository>();

            //var note = new Note
            //{
            //    RiskClaim_Id = 1234,
            //    BaseRiskClaim_Id = 1234,
            //    CreatedBy = "JonathanWalker@keoghs.co.uk",
            //    CreatedDate = DateTime.Now,
            //    Decision = null,
            //    Deleted = false,
            //    NoteDesc = "Inserting a new note,",
            //    Visibilty = 0
            //};

            //Mock.Arrange(() => noteRepository.InsertNote(note)).Returns(-1).MustBeCalled();

            ////Act
            //NoteController controller = new NoteController(noteRepository);
            //ViewResult viewResult = controller.InsertNote(note);
            //var model = viewResult.Model;

            ////Assert
            //Assert.AreEqual(-1, model);
        }
        
        
        [TestMethod]
        public void Index_Return_All_Notes_In_DB()
        {
            //Arrange
            var noteRepository = Mock.Create<IRepository>();

            Mock.Arrange(() => noteRepository.GetAll()).
                Returns(new List<Note>()
                {
                    new Note{ NoteId = 1, NoteDesc = "Note Added"},
                    new Note{ NoteId = 2, NoteDesc = "Note 1 Amended"}
                }).MustBeCalled();

            //Act
            NoteController controller = new NoteController(noteRepository);
            ViewResult viewResult = controller.GetAll();
            var model = viewResult.Model as IEnumerable<Note>;

            //Assert
            Assert.AreEqual(2, model.Count());
        }

        [TestMethod]
        public void Index()
        {
            // Arrange
            //NoteController controller = new NoteController();

            //// Act
            //ViewResult result = controller.Index(new int[] { 1, 4, 3 }) as ViewResult;

            //// Assert
            //Assert.IsNotNull(result);
        }
    }
}