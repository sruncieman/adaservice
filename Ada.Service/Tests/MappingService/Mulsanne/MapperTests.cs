﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using MDA.ClaimService.Interface;
using MDA.Common.Server;
using MDA.Pipeline.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.MappingService.Interface;

namespace Tests.MappingService.Mulsanne
{

    /// <summary>
    /// Test Mulsanne Mappings
    /// </summary> 
    class MapperTests
    {

        private static IMapper _mapper;
        private static Stream _fileStream;
        private static List<PipelineMotorClaim> _batch;
        private static CurrentContext _ctx;


        //[ClassInitialize]
        //public static void ClassInitialise(TestContext context)
        //{
        //    const int riskClientId = 59;
        //    const int riskUserId = 0;
        //    const string riskUserName = "Admin1";

        //    var clientFolder = MappingTestHelper.GetFolderPath();
        //    var filePath = string.Format("{0}\\{1}", clientFolder, "ADA_Data_Wash_Tets.xlsx");

        //    _ctx = new CurrentContext(riskClientId, riskUserId, riskUserName);
        //    _batch = new List<PipelineMotorClaim>();
        //    _fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
        //    var currentCreateStatus = new ClaimCreationStatus() { RiskBatchId = 0 };
        //    var defaultAddresses = new List<string>();
        //    var defaultPeople = new List<string>();
        //    var defaultOrgs = new List<string>();

        //    _mapper = new MDA.MappingService.Impl.Mulsanne.Motor.Mapper(_fileStream,
        //                                                                clientFolder,
        //                                                                _ctx,
        //                                                                //BuildClaimsIntoBatch,
        //                                                                currentCreateStatus,
        //                                                                defaultAddresses,
        //                                                                defaultPeople,
        //                                                                defaultOrgs);
        //    // Arrange
        //    Debug.Assert(_mapper != null, "_mapper != null");
        //    _mapper.AssignFiles();
        //    _mapper.InitialiseFileHelperEngines();
        //    _mapper.PopulateFileHelperEngines();
        //    _mapper.RetrieveDistinctClaims();
        //    // Act
        //    _mapper.Translate();
        //}

    }
}
