﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using MDA.ClaimService.Interface;
using MDA.Common.Server;
using MDA.Pipeline.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.MappingService.Interface;

namespace Tests.MappingService.DLGNew
{
    /// <summary>
    /// Test DLGNew Mappings
    /// </summary>       
    [TestClass]
    public class MapperTests
    {

        private static IMapper _mapper;
        private static Stream _fileStream;
        private static List<PipelineMotorClaim> _batch;
        private static CurrentContext _ctx;


        [ClassInitialize]
        public static void ClassInitialise(TestContext context)
        {
            const int riskClientId = 10;
            const int riskUserId = 0;
            const string riskUserName = "Admin1";

            var clientFolder = MappingTestHelper.GetFolderPath();
            var filePath = string.Format("{0}\\{1}", clientFolder, "DLG_datawash_Test.zip");

            _ctx = new CurrentContext(riskClientId, riskUserId, riskUserName);
            _batch = new List<PipelineMotorClaim>();
            _fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var currentCreateStatus = new ClaimCreationStatus() { RiskBatchId = 0 };
            var defaultAddresses = new List<string>();
            var defaultPeople = new List<string>();
            var defaultOrgs = new List<string>();

            _mapper = new MDA.MappingService.DLGNew.Motor.Mapper(_fileStream,
                                                                        clientFolder,
                                                                        _ctx,
                                                                        BuildClaimsIntoBatch,
                                                                        currentCreateStatus,
                                                                        defaultAddresses,
                                                                        defaultPeople,
                                                                        defaultOrgs);
            // Arrange
            Debug.Assert(_mapper != null, "_mapper != null");
            _mapper.AssignFiles();
            _mapper.InitialiseFileHelperEngines();
            _mapper.PopulateFileHelperEngines();
            _mapper.RetrieveDistinctClaims();
            // Act
            _mapper.Translate();
        }



        [ClassCleanup]
        public static void ClassTeardown()
        {
            _ctx = null;
            _mapper = null;
            _batch = null;

            Debug.Assert(_fileStream != null, "_fileStream != null");
            _fileStream.Dispose();
        }

        public static int BuildClaimsIntoBatch(CurrentContext ctx, IPipelineClaim claim, object o)
        {
            //_claimService.TestScanPipelineRecord(ctx, claim);
            Debug.Assert(_batch != null, "_batch != null");
            _batch.Add((PipelineMotorClaim)claim);
            return 0;
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_FirstClaimHasExpectedIncidentDetails()
        {

            // Assert General
            Assert.IsNotNull(_batch, "_batch != null");
            Assert.AreEqual(5, _batch.Count, "Batch must contain 5 claims");

            var claim = _batch[0];
            Assert.IsNotNull(claim, "claim !- null");

            // Assert Claim
            Assert.AreEqual("35647775", claim.ClaimNumber, "Claim number must match");
            Assert.AreEqual(new DateTime(2016, 03, 09).ToShortDateString(), claim.IncidentDate.ToShortDateString(), "Incident Date must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_FirstClaimHasExpectedClaimInfoDetails()
        {

            // Assert General
            var claim = _batch[0];

            // Assert ClaimInfo
            Assert.AreEqual(1, claim.ExtraClaimInfo.ClaimStatus_Id, "Claim status must match");
            Assert.AreEqual(11826, claim.ExtraClaimInfo.Reserve, "Claim Reserve must match");
            Assert.AreEqual(new DateTime(2016, 07, 19).ToShortDateString(), claim.ExtraClaimInfo.ClaimNotificationDate.Value.ToShortDateString(), "Claim notification date must match");
            Assert.AreEqual("Not Involved Incident | ", claim.ExtraClaimInfo.IncidentCircumstances, "Incident circumstances must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_FirstClaimHasExpectedPolicyDetails()
        {

            // Assert General
            var claim = _batch[0];

            // Assert Policy
            Assert.AreEqual("DLG", claim.Policy.Insurer, "Insurer must match");
            Assert.AreEqual(new DateTime(2014, 10, 02, 00, 00, 00).ToShortDateString(), claim.Policy.PolicyStartDate.Value.ToShortDateString(), "Policy Start date must match");
            Assert.AreEqual(new DateTime(2016, 10, 02).ToShortDateString(), claim.Policy.PolicyEndDate.Value.ToShortDateString(), "Policy end date must match");
            Assert.AreEqual("Churchill", claim.Policy.InsurerTradingName, "Trading name must match");
            Assert.AreEqual(1, claim.Policy.PolicyCoverType_Id, "cover type must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_FirstClaimHasBlankVehicle()
        {
            // Assert General
            var claim = _batch[0];
            Assert.AreEqual(1, claim.Vehicles.Count, "claim must have 1 vehicle");

            var vehicle = claim.Vehicles.FirstOrDefault(x => x.I2V_LinkData.Incident2VehicleLinkType_Id == 6);
            Assert.IsNotNull(vehicle, "vehicle != null");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_FirstClaimHasExpectedInsuredPersonDetails()
        {
            // Assert General
            var claim = _batch[0];

            var vehicle = claim.Vehicles.FirstOrDefault(x => x.I2V_LinkData.Incident2VehicleLinkType_Id == 6);
            Assert.AreEqual(4, vehicle.People.Count, "blank vehicle must have 4 persons");

            var person = vehicle.People.FirstOrDefault(x => x.I2Pe_LinkData.PartyType_Id == 3 && x.I2Pe_LinkData.SubPartyType_Id == 1);
            Assert.IsNotNull(person, "insured driver != null");

            // Assert Insured Driver
            Assert.AreEqual("WALSHAW", person.LastName, "Insured driver last name must match");
            Assert.AreEqual("MICHAEL", person.FirstName, "Insured driver first name must match");
            Assert.AreEqual(null, person.MiddleName, "Insured driver middle name must match");
            Assert.AreEqual(new DateTime(1934, 08, 09).ToShortDateString(), person.DateOfBirth.Value.ToShortDateString(), "Insured Driver date of Birth must match");
            Assert.AreEqual("7901908934", person.Telephones.FirstOrDefault(x => x.TelephoneType_Id == 1).ClientSuppliedNumber, "Insured Driver Home Tel must match");

            var personAddresses = person.Addresses;
            Assert.AreEqual("UPRN[] BN[4 Fishers Farm] ST[Limes Avenue] PC[RH6 9DQ] TYPE[CurrentAddress]", personAddresses[0].DisplayText, "Person Address must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_FirstClaimHasExpectedPolicyholderPersonDetails()
        {
            // Assert General
            var claim = _batch[0];

            var vehicle = claim.Vehicles.FirstOrDefault(x => x.I2V_LinkData.Incident2VehicleLinkType_Id == 6);
            var person = vehicle.People.FirstOrDefault(x => x.I2Pe_LinkData.PartyType_Id == 5);
            Assert.IsNotNull(person, "policyholder != null");

            // Assert Policyholder
            Assert.AreEqual("WALSHAW", person.LastName, "Policyholder last name must match");
            Assert.AreEqual("JACQUELINE", person.FirstName, "Policyholder first name must match");
            Assert.AreEqual(null, person.MiddleName, "Policyholder middle name must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_FirstClaimHasExpectedClaimantDetails()
        {
            // Assert General
            var claim = _batch[0];

            var vehicle = claim.Vehicles.FirstOrDefault(x => x.I2V_LinkData.Incident2VehicleLinkType_Id == 6);
            var people = vehicle.People.Where(x => x.I2Pe_LinkData.PartyType_Id == 1);
            Assert.AreEqual(2, people.Count(), "vehicle must contain 2 claimants");

            var claimant1 = vehicle.People[0];
            var claimant2 = vehicle.People[1];

            // Assert Claimant1
            Assert.AreEqual("CHOWDHURY", claimant1.LastName, "Claimant1 last name must match");
            Assert.AreEqual("ASISH", claimant1.FirstName, "Claimant1 first name must match");
            Assert.AreEqual(null, claimant1.MiddleName, "Claimant1 middle name must match");

            // Assert Claimant2
            Assert.AreEqual("CHOWDHURY", claimant2.LastName, "Claimant2 last name must match");
            Assert.AreEqual("ASISH", claimant2.FirstName, "Claimant2 first name must match");
            Assert.AreEqual(null, claimant2.MiddleName, "Claimant2 middle name must match");

            var claimantAddresses = claimant2.Addresses;
            Assert.AreEqual("UPRN[] BN[3 wooden bridge terrace] ST[] PC[SE270NR] TYPE[CurrentAddress]", claimantAddresses[0].DisplayText, "Claimant2 Address must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_SecondClaimHasExpectedIncidentDetails()
        {

            // Assert General
            var claim = _batch[2];
            Assert.IsNotNull(claim, "claim !- null");

            // Assert Claim
            Assert.AreEqual("38699336", claim.ClaimNumber, "Claim number must match");
            Assert.AreEqual(new DateTime(2016, 07, 14).ToShortDateString(), claim.IncidentDate.ToShortDateString(), "Incident Date must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_SecondClaimHasExpectedClaimInfoDetails()
        {

            // Assert General
            var claim = _batch[2];

            // Assert ClaimInfo
            Assert.AreEqual(1, claim.ExtraClaimInfo.ClaimStatus_Id, "Claim status must match");
            Assert.AreEqual(422, claim.ExtraClaimInfo.Reserve, "Claim Reserve must match");
            Assert.AreEqual(new DateTime(2016, 07, 19).ToShortDateString(), claim.ExtraClaimInfo.ClaimNotificationDate.Value.ToShortDateString(), "Claim notification date must match");
            Assert.AreEqual("Single Vehicle | Social | 0-  5 mph | Left Rear Door | Left Quarter Panel", claim.ExtraClaimInfo.IncidentCircumstances, "Incident circumstances must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_SecondClaimHasExpectedPolicyDetails()
        {

            // Assert General
            var claim = _batch[2];

            // Assert Policy
            Assert.AreEqual("DLG", claim.Policy.Insurer, "Insurer must match");
            Assert.AreEqual(new DateTime(2016, 04, 26, 00, 00, 00).ToShortDateString(), claim.Policy.PolicyStartDate.Value.ToShortDateString(), "Policy Start date must match");
            Assert.AreEqual(new DateTime(2017, 04, 26).ToShortDateString(), claim.Policy.PolicyEndDate.Value.ToShortDateString(), "Policy end date must match");
            Assert.AreEqual("Direct Line", claim.Policy.InsurerTradingName, "Trading name must match");
            Assert.AreEqual(1, claim.Policy.PolicyCoverType_Id, "cover type must match");
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_SecondClaimHasExpectedVehicleDetails()
        {
            // Assert General
            var claim = _batch[2];
            Assert.AreEqual(2, claim.Vehicles.Count, "claim must have 2 vehicles");

            var vehicle = claim.Vehicles[0];
            Assert.IsNotNull(vehicle, "vehicle != null");

            // Assert Vehicle      
            Assert.AreEqual(0, vehicle.I2V_LinkData.Incident2VehicleLinkType_Id, "vehicle involvement must be unknown");
            Assert.AreEqual("PEUGEOT", vehicle.VehicleMake, "Insured Vehicle make must match");
            Assert.AreEqual("308 1.6D (11ON) 5D ACTIVE 92", vehicle.VehicleModel, "Insured Vehicle model must match");
            Assert.AreEqual("WV63VVO", vehicle.VehicleRegistration, "Insured Vehicle registation must match");
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_SecondClaimHasExpectedVehiclePersonDetails()
        {
            // Assert General
            var claim = _batch[2];
            var vehicle = claim.Vehicles[0];
            Assert.AreEqual(1, vehicle.People.Count, "vehicle must contain 1 person");

            var person = vehicle.People[0];
            Assert.IsNotNull(person, "person != null");

            // Assert Vehicle Person
            Assert.AreEqual("LYNN", person.LastName, "VehiclePerson last name must match");
            Assert.AreEqual("KAREN", person.FirstName, "VehiclePerson first name must match");
            Assert.AreEqual(null, person.MiddleName, "VehiclePerson middle name must match");
            Assert.AreEqual(new DateTime(1968, 12, 17).ToShortDateString(), person.DateOfBirth.Value.ToShortDateString(), "VehiclePerson date of Birth must match");
            Assert.AreEqual("1575564957", person.Telephones.FirstOrDefault(x => x.TelephoneType_Id == 1).ClientSuppliedNumber, "VehiclePerson Home Tel must match");
            Assert.AreEqual("7933912459", person.Telephones.FirstOrDefault(x => x.TelephoneType_Id == 0).ClientSuppliedNumber, "VehiclePerson Work Tel must match");
            Assert.AreEqual("7734663299", person.Telephones.FirstOrDefault(x => x.TelephoneType_Id == 2).ClientSuppliedNumber, "VehiclePerson Mob Tel must match");
            Assert.AreEqual("CUSTOMER ADVISOR", person.Occupation, "VehiclePerson occupation must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_DLGNew_GivenSample_WhenMapped_FithClaimHasExpectedVehicleDetails()
        {
            // Assert General
            var claim = _batch[4];
            Assert.IsNotNull(claim, "claim != null");
            Assert.AreEqual(4, claim.Vehicles.Count, "Claim must contain 4 vehicles");

            var vehicle1 = claim.Vehicles[0];
            Assert.AreEqual(1, vehicle1.People.Count, "Vehicle1 must contain 1 person");
            var vehicle2 = claim.Vehicles[1];
            Assert.AreEqual(1, vehicle2.People.Count, "Vehicle2 must contain 1 person");
            var vehicle3 = claim.Vehicles[2];
            Assert.AreEqual(1, vehicle3.People.Count, "vehicle3 must contain 1 people");
            var vehicle4 = claim.Vehicles[3];
            Assert.AreEqual(6, vehicle4.People.Count, "vehicle4 must contain 5 people");

        }

    }
}