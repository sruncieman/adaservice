﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using MDA.ClaimService.Interface;
using MDA.Common.Server;
using MDA.Pipeline.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.MappingService.Interface;

namespace Tests.MappingService.LV
{
    /// <summary>
    /// Test LV Mappings
    /// </summary> 
    [TestClass]
    public class MapperTestsGIOS
    {
        
        private static IMapper _mapper;
        private static Stream _fileStream;
        private static List<PipelineMotorClaim> _batch;
        private static CurrentContext _ctx;


        [ClassInitialize]
        public static void ClassInitialise(TestContext context)
        {
            // To do: change this to LV file
            const int riskClientId = 19;
            const int riskUserId = 0;
            const string riskUserName = "Admin1";

            // To do: change to these for TFS
            //var clientFolder = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory + "\\..\\..\\Files\\");
            var clientFolder = MappingTestHelper.GetFolderPath();
            var filePath = string.Format("{0}\\{1}", clientFolder, "LVFS_MBC2_GIOS_TEST.txt");

            //const string filePath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\LV=\Data\Test\LVFS_MBC2_GIOS_TEST.txt";
            //const string clientFolder = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\LV=\Data\Test";

            _ctx = new CurrentContext(riskClientId, riskUserId, riskUserName);
            _batch = new List<PipelineMotorClaim>();
            _fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var currentCreateStatus = new ClaimCreationStatus() { RiskBatchId = 0 };
            List<string> defaultAddresses = new List<string> { };
            List<string> defaultPeople = new List<string> { };
            List<string> defaultOrgs = new List<string> { };

            _mapper = new MDA.MappingService.LV.Motor.Mapper(_fileStream,
                                                                        clientFolder,
                                                                        _ctx,
                                                                        BuildClaimsIntoBatch,
                                                                        currentCreateStatus,
                                                                        defaultAddresses,
                                                                        defaultPeople,
                                                                        defaultOrgs);

            // Arrange
            Debug.Assert(_mapper != null, "_mapper != null");
            _mapper.AssignFiles();
            _mapper.InitialiseFileHelperEngines();
            _mapper.PopulateFileHelperEngines();
            _mapper.RetrieveDistinctClaims();
            // Act
            _mapper.Translate();

        }

        [ClassCleanup]
        public static void ClassTeardown()
        {
            _ctx = null;
            _mapper = null;
            _batch = null;

            Debug.Assert(_fileStream != null, "_fileStream != null");
            _fileStream.Dispose();
        }

        public static int BuildClaimsIntoBatch(CurrentContext ctx, IPipelineClaim claim, object o)
        {
            Debug.Assert(_batch != null, "_batch != null");
            _batch.Add((PipelineMotorClaim)claim);
            return 0;
        }


        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedIncidentDetails()
        {

            // Assert General
            Assert.IsNotNull(_batch, "_batch != null");
            Assert.AreEqual(3, _batch.Count, "Batch must contain 3 claims");

            var claim = _batch[0];
            Assert.IsNotNull(claim, "claim !- null");

            // Assert Claim
            Assert.AreEqual("8000794437GIOS", claim.ClaimNumber, "Claim number must match");
            Assert.AreEqual(new DateTime(2013, 09, 29).ToShortDateString(), claim.IncidentDate.ToShortDateString(), "Incident Date must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedClaimInfoDetails()
        {

            // Assert General
            var claim = _batch[0];

            // Assert Claim Info
            Assert.AreEqual(new DateTime(2013, 09, 29).ToShortDateString(), claim.ExtraClaimInfo.ClaimNotificationDate.Value.ToShortDateString(), "Claim Notifcation Date must match");
            Assert.AreEqual("ACCIDENT", claim.ExtraClaimInfo.ClaimCode, "Claim Code must match");
            Assert.AreEqual(null, claim.ExtraClaimInfo.IncidentLocation, "Incident Location must match");
            Assert.AreEqual("ph and other vehicle making room for passing ambulance, as ph has swerved to avoid, tp has collided into the rear of ph"
                            ,claim.ExtraClaimInfo.IncidentCircumstances, "Incident Circumstances must match");
            Assert.AreEqual(6000.00M, claim.ExtraClaimInfo.Reserve, "Reserve must match");
            Assert.AreEqual(6380, claim.ExtraClaimInfo.PaymentsToDate, "Payments to Date must match");
            Assert.AreEqual(2, claim.ExtraClaimInfo.ClaimStatus_Id, "Claim Status must match");
        }


        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedPolicyDetails()
        {

            // Assert General
            var claim = _batch[0];
            var policy = claim.Policy;
            Assert.IsNotNull(policy, "policy != null");

            // Assert Policy            
            Assert.AreEqual("7070081449GIOS", policy.PolicyNumber, "Policy Number must match");
            Assert.AreEqual(1, policy.PolicyType_Id, "Policy Type must match");
            Assert.AreEqual("LV=", policy.Insurer, "Insurer must match");
            Assert.AreEqual("HIGHWAY", policy.InsurerTradingName, "Trading name must match");
            Assert.AreEqual("UNKNOWN", policy.Broker, "Broker must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedVehicleDetails()
        {

            // Assert General
            var claim = _batch[0];
            Assert.AreEqual(3, claim.Vehicles.Count, "Claim must contain 3 vehicles");
            var vehicle1 = claim.Vehicles[0];
            Assert.IsNotNull(vehicle1, "vehicle1 != null");
            var vehicle2 = claim.Vehicles[1];
            Assert.IsNotNull(vehicle2, "vehicle2 != null");

            // Assert Vehicle 1
            Assert.AreEqual(1, vehicle1.I2V_LinkData.Incident2VehicleLinkType_Id, "Vehicle involvement must match");
            Assert.AreEqual("WG53WXR", vehicle1.VehicleRegistration, "vehicle Registration must match");
            Assert.AreEqual("", vehicle1.VIN, "Vehicle VIN must match");
            Assert.AreEqual("FORD", vehicle1.VehicleMake, "VehicleMake must match");
            Assert.AreEqual("FOCUS LX", vehicle1.VehicleModel, "VehicleModel must match");
            Assert.AreEqual("TOTAL LOSS", vehicle1.DamageDescription, "VehicleDamageDecs must match");
            Assert.AreEqual("", vehicle1.EngineCapacity, "Engine Capacity must match");
            Assert.AreEqual(0, vehicle1.VehicleColour_Id, "vehicleColour must match");
            Assert.AreEqual(0, vehicle1.VehicleType_Id, "vehicleType must match");
            Assert.AreEqual(0, vehicle1.I2V_LinkData.VehicleCategoryOfLoss_Id, "VehicleLossCat must match");

            // Assert Vehicle 2
            Assert.AreEqual(2, vehicle2.I2V_LinkData.Incident2VehicleLinkType_Id, "Vehicle involvement must match");
            Assert.AreEqual("CK52FCX", vehicle2.VehicleRegistration, "vehicle Registration must match");
            Assert.AreEqual("", vehicle2.VIN, "Vehicle VIN must match");
            Assert.AreEqual("", vehicle2.VehicleMake, "VehicleMake must match");
            Assert.AreEqual("", vehicle2.VehicleModel, "VehicleModel must match");
            Assert.AreEqual("", vehicle2.DamageDescription, "VehicleDamageDecs must match");
            Assert.AreEqual("", vehicle2.EngineCapacity, "Engine Capacity must match");
            Assert.AreEqual(0, vehicle2.VehicleColour_Id, "vehicleColour must match");
            Assert.AreEqual(0, vehicle2.VehicleType_Id, "vehicleType must match");
            Assert.AreEqual(0, vehicle2.I2V_LinkData.VehicleCategoryOfLoss_Id, "VehicleLossCat must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedVehiclePerson1Details()
        {
            // Assert General
            var claim = _batch[0];
            var vehicle1 = claim.Vehicles[0];
            Assert.AreEqual(1, vehicle1.People.Count, "Vehicle1 must contain 1 person");
            var person1 = vehicle1.People[0];
            Assert.IsNotNull(person1, "person1 != null");

            // Assert person1
            Assert.AreEqual(null, person1.Occupation, "occupation must match");
            Assert.AreEqual(0, person1.NINumbers.Count, "NINOCount must match");
            //Assert.AreEqual(null, person1.NINumbers[0], "NINO must match");
            Assert.AreEqual(3, person1.I2Pe_LinkData.PartyType_Id, "PartyType must match");
            Assert.AreEqual(1, person1.I2Pe_LinkData.SubPartyType_Id, "SubPartyType must match");
            Assert.AreEqual(2, person1.Salutation_Id, "Salutation must match");
            Assert.AreEqual(2, person1.Gender_Id, "Gender must match");
            Assert.AreEqual("PAULINE E", person1.FirstName, "FirstName must match");
            Assert.AreEqual(null, person1.MiddleName, "MiddleName must match");
            Assert.AreEqual("ALLEN", person1.LastName, "LastName must match");
            Assert.AreEqual(new DateTime(1954, 09, 25).ToShortDateString(), person1.DateOfBirth.Value.ToShortDateString(), "DoB must match");

            // Assert Person1 Address
            Assert.AreEqual(1, person1.Addresses.Count, "person1 must have 1 address");
            var address1 = person1.Addresses[0];
            Assert.IsNotNull(address1, "address1 != null");

            Assert.AreEqual("", address1.SubBuilding, "SubBuilding must match");
            Assert.AreEqual("", address1.Building, "Building must match");
            Assert.AreEqual("19", address1.BuildingNumber, "BuildingNumber must match");
            Assert.AreEqual("MEADOW PARK", address1.Street, "Street must match");
            Assert.AreEqual("", address1.Town, "Town must match");
            Assert.AreEqual("PL9 9NX", address1.PostCode, "PostCode must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedVehiclePerson2Details()
        {
            // Assert General
            var claim = _batch[0];
            var vehicle2 = claim.Vehicles[1];
            Assert.AreEqual(1, vehicle2.People.Count, "Vehicle2 must contain 1 person");
            var person2 = vehicle2.People[0];
            Assert.IsNotNull(person2, "person2 != null");

            // Assert person2
            Assert.AreEqual(null, person2.Occupation, "occupation must match");
            Assert.AreEqual(0, person2.NINumbers.Count, "NINOCount must match");
            //Assert.AreEqual("", person2.NINumbers[0], "NINO must match");
            Assert.AreEqual(6, person2.I2Pe_LinkData.PartyType_Id, "PartyType must match");
            Assert.AreEqual(1, person2.I2Pe_LinkData.SubPartyType_Id, "SubPartyType must match");
            Assert.AreEqual(1, person2.Salutation_Id, "Salutation must match");
            Assert.AreEqual(1, person2.Gender_Id, "Gender must match");
            Assert.AreEqual("G", person2.FirstName, "FirstName must match");
            Assert.AreEqual(null, person2.MiddleName, "MiddleName must match");
            Assert.AreEqual("IKAMBA", person2.LastName, "LastName must match");
            Assert.AreEqual(new DateTime(1981, 09, 11).ToShortDateString(), person2.DateOfBirth.Value.ToShortDateString(), "DoB must match");

            // Assert Person2 Address
            Assert.AreEqual(1, person2.Addresses.Count, "person1 must have 1 address");
            var address2 = person2.Addresses[0];
            Assert.IsNotNull(address2, "address1 != null");

            Assert.AreEqual("", address2.SubBuilding, "SubBuilding must match");
            Assert.AreEqual("", address2.Building, "Building must match");
            Assert.AreEqual("1", address2.BuildingNumber, "BuildingNumber must match");
            Assert.AreEqual("MOOREDALE ROAD", address2.Street, "Street must match");
            Assert.AreEqual("CARDIFF", address2.Town, "Town must match");
            Assert.AreEqual("CF11 7DY", address2.PostCode, "PostCode must match");
        
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedOrgDetails()
        {

            // Assert General
            var claim = _batch[0];
            Assert.AreEqual(1, claim.Organisations.Count, "Claim must have 1 org");
            var org = claim.Organisations[0];
            Assert.IsNotNull(org, "Org != null");

            // Assert Org
            Assert.AreEqual(5, org.OrganisationType_Id, "OrgType must match");
            Assert.AreEqual(12, org.P2O_LinkData.Person2OrganisationLinkType_Id, "Per2OrgLinkType must match");
            Assert.AreEqual("SPICKETTS BATTRICK LAW PRACTICE", org.OrganisationName, "OrgName must match");

            // Assert Org Address
            Assert.AreEqual(1, org.Addresses.Count, "Org must have 1 address");
            var orgAddress = org.Addresses[0];
            Assert.IsNotNull(orgAddress, "OrgAddress != null");

            Assert.AreEqual("", orgAddress.SubBuilding, "SubBuilding must match");
            Assert.AreEqual("SPICKETTS SOLICITORS", orgAddress.Building, "Building must match");
            Assert.AreEqual("", orgAddress.BuildingNumber, "BuildingNumber must match");
            Assert.AreEqual("", orgAddress.Street, "Street must match");
            Assert.AreEqual("PONTYPRIDD", orgAddress.Town, "Town must match");
            Assert.AreEqual("CF37 2AU", orgAddress.PostCode, "PostCode must match");
            Assert.AreEqual(5, orgAddress.PO2A_LinkData.AddressLinkType_Id, "AddressLinkType must match");
           
           
        }
       
        
        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedNoneVehiclePersonDetails()
        {
            // Assert General
            var claim = _batch[0];
            var vehicle3 = claim.Vehicles[2];
            Assert.AreEqual(1, vehicle3.People.Count, "Vehicle3 must contain 1 person");
            var person3 = vehicle3.People[0];
            Assert.IsNotNull(person3, "person3 != null");

            // Assert person3
            Assert.AreEqual(null, person3.Occupation, "occupation must match");
            Assert.AreEqual(0, person3.NINumbers.Count, "NINOCount must match");
            //Assert.AreEqual("", person3.NINumbers[0], "NINO must match");
            Assert.AreEqual(3, person3.I2Pe_LinkData.PartyType_Id, "PartyType must match");
            Assert.AreEqual(1, person3.I2Pe_LinkData.SubPartyType_Id, "SubPartyType must match");
            Assert.AreEqual(1, person3.Salutation_Id, "Salutation must match");
            Assert.AreEqual(1, person3.Gender_Id, "Gender must match");
            Assert.AreEqual("JEFFERY S", person3.FirstName, "FirstName must match");
            Assert.AreEqual(null, person3.MiddleName, "MiddleName must match");
            Assert.AreEqual("ALLEN", person3.LastName, "LastName must match");
            Assert.AreEqual(new DateTime(1956, 02, 18).ToShortDateString(), person3.DateOfBirth.Value.ToShortDateString(), "DoB must match");

            // Assert Person3 Address
            Assert.AreEqual(1, person3.Addresses.Count, "person1 must have 1 address");
            var address3 = person3.Addresses[0];
            Assert.IsNotNull(address3, "address1 != null");

            Assert.AreEqual("", address3.SubBuilding, "SubBuilding must match");
            Assert.AreEqual("", address3.Building, "Building must match");
            Assert.AreEqual("19", address3.BuildingNumber, "BuildingNumber must match");
            Assert.AreEqual("MEADOW PARK", address3.Street, "Street must match");
            Assert.AreEqual("PLYMOUTH", address3.Town, "Town must match");
            Assert.AreEqual("PL9 9NX", address3.PostCode, "PostCode must match");

            // Assert person3 Contact
            Assert.AreEqual(1, person3.Telephones.Count, "org must have 1 tel");
            var person3tel = person3.Telephones[0];
            Assert.IsNotNull(person3tel, "orgTel != null");

            Assert.AreEqual("01752248924", person3tel.ClientSuppliedNumber, "OrgTelNumber must match");
            Assert.AreEqual(1, person3tel.TelephoneType_Id, "OrgTelType must match");

        }
  

    }
}
