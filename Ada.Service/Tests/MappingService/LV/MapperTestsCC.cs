﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using MDA.ClaimService.Interface;
using MDA.Common.Server;
using MDA.Pipeline.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.MappingService.Interface;

namespace Tests.MappingService.LV
{
    [TestClass]   
    public class MapperTestsCC
    {
        private static IMapper _mapper;
        private static Stream _fileStream;
        private static List<PipelineMotorClaim> _batch;
        private static CurrentContext _ctx;


        [ClassInitialize]
        public static void ClassInitialise(TestContext context)
        {
            // To do: change this to LV file
            const int riskClientId = 47;
            const int riskUserId = 0;
            const string riskUserName = "Admin1";

            // To do: change to these for TFS
            var clientFolder = MappingTestHelper.GetFolderPath();
            var filePath = string.Format("{0}\\{1}", clientFolder, "LVFS_MDC4_CLCENTER_TEST.txt");

            //const string filePath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\LV=\Data\Test\LVFS_MBC2_GIOS_TEST.txt";
            //const string clientFolder = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\LV=\Data\Test";

            _ctx = new CurrentContext(riskClientId, riskUserId, riskUserName);
            _batch = new List<PipelineMotorClaim>();
            _fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var currentCreateStatus = new ClaimCreationStatus() { RiskBatchId = 0 };
            List<string> defaultAddresses = new List<string> { };
            List<string> defaultPeople = new List<string> { };
            List<string> defaultOrgs = new List<string> { };

            _mapper = new MDA.MappingService.LV.Motor.Mapper(_fileStream,
                                                                        clientFolder,
                                                                        _ctx,
                                                                        BuildClaimsIntoBatch,
                                                                        currentCreateStatus,
                                                                        defaultAddresses,
                                                                        defaultPeople,
                                                                        defaultOrgs);

            // Arrange
            Debug.Assert(_mapper != null, "_mapper != null");
            _mapper.AssignFiles();
            _mapper.InitialiseFileHelperEngines();
            _mapper.PopulateFileHelperEngines();
            _mapper.RetrieveDistinctClaims();
            // Act
            _mapper.Translate();

        }

        [ClassCleanup]
        public static void ClassTeardown()
        {
            _ctx = null;
            _mapper = null;
            _batch = null;

            Debug.Assert(_fileStream != null, "_fileStream != null");
            _fileStream.Dispose();
        }

        public static int BuildClaimsIntoBatch(CurrentContext ctx, IPipelineClaim claim, object o)
        {
            Debug.Assert(_batch != null, "_batch != null");
            _batch.Add((PipelineMotorClaim)claim);
            return 0;
        }


        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedIncidentDetails()
        {

            // Assert General
            Assert.IsNotNull(_batch, "_batch != null");
            Assert.AreEqual(53, _batch.Count, "Batch must contain 53 claims");

            var claim = _batch[0];
            Assert.IsNotNull(claim, "claim !- null");

            // Assert Claim
            Assert.AreEqual("200-21-001133CC", claim.ClaimNumber, "Claim number must match");
            Assert.AreEqual(new DateTime(2015, 12, 04).ToShortDateString(), claim.IncidentDate.ToShortDateString(), "Incident Date must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedClaimInfoDetails()
        {

            // Assert General
            var claim = _batch[0];

            // Assert Claim Info
            Assert.AreEqual(1, claim.ExtraClaimInfo.ClaimStatus_Id, "Claim Status must match");
            Assert.AreEqual(new DateTime(2015, 12, 04).ToShortDateString(), claim.ExtraClaimInfo.ClaimNotificationDate.Value.ToShortDateString(), "Claim Notifcation Date must match");
            Assert.AreEqual("INSURED HIT TP", claim.ExtraClaimInfo.ClaimCode, "Claim Code must match");
            Assert.AreEqual("Hurn Rd Christchurch BH23 2RP, UK", claim.ExtraClaimInfo.IncidentLocation, "Incident Location must match");
            Assert.AreEqual("Customer was in stationary traffic, about to set off. As she has done, the traffic has stopped and she has collided with the rear a third party vehicle.  CCTV - NO Dashcam - NO Photos - NO Police - NO Ambulance - NO Witnesses - NO  Details exchanged at the scene"
                            , claim.ExtraClaimInfo.IncidentCircumstances, "Incident Circumstances must match");
            Assert.AreEqual(14004.00M, claim.ExtraClaimInfo.Reserve, "Reserve must match");
            Assert.AreEqual(null, claim.ExtraClaimInfo.PaymentsToDate, "Payments to Date must match");
        }


        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedPolicyDetails()
        {

            // Assert General
            var claim = _batch[0];
            var policy = claim.Policy;
            Assert.IsNotNull(policy, "policy != null");

            // Assert Policy            
            Assert.AreEqual("21034965575choice", policy.PolicyNumber, "Policy Number must match");
            Assert.AreEqual(1, policy.PolicyType_Id, "Policy Type must match");
            Assert.AreEqual("LV=", policy.Insurer, "Insurer must match");
            Assert.AreEqual("LV", policy.InsurerTradingName, "Trading name must match");
            Assert.AreEqual(null, policy.Broker, "Broker must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedVehicleDetails()
        {

            // Assert General
            var claim = _batch[0];
            Assert.AreEqual(3, claim.Vehicles.Count, "Claim must contain 3 vehicles");
            var vehicle1 = claim.Vehicles[0];
            Assert.IsNotNull(vehicle1, "vehicle1 != null");
            var vehicle2 = claim.Vehicles[1];
            Assert.IsNotNull(vehicle2, "vehicle2 != null");            
            var vehicle3 = claim.Vehicles[2];
            Assert.IsNotNull(vehicle3, "vehicle3 != null");


            // Assert Vehicle 1
            Assert.AreEqual(1, vehicle1.I2V_LinkData.Incident2VehicleLinkType_Id, "Vehicle involvement must match");
            Assert.AreEqual("J999MBH", vehicle1.VehicleRegistration, "vehicle Registration must match");
            Assert.AreEqual("", vehicle1.VIN, "Vehicle VIN must match");
            Assert.AreEqual("MAZDA", vehicle1.VehicleMake, "VehicleMake must match");
            Assert.AreEqual("323 GSI AUTO", vehicle1.VehicleModel, "VehicleModel must match");
            Assert.AreEqual("AWAITING AUTHORISATION", vehicle1.DamageDescription, "VehicleDamageDecs must match");
            Assert.AreEqual("1840", vehicle1.EngineCapacity, "Engine Capacity must match");
            Assert.AreEqual(3, vehicle1.VehicleColour_Id, "vehicleColour must match");
            Assert.AreEqual(0, vehicle1.VehicleType_Id, "vehicleType must match");
            Assert.AreEqual(0, vehicle1.I2V_LinkData.VehicleCategoryOfLoss_Id, "VehicleLossCat must match");

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedVehiclePerson1Details()
        {
            // Assert General
            var claim = _batch[0];
            var vehicle1 = claim.Vehicles[0];
            Assert.AreEqual(1, vehicle1.People.Count, "Vehicle1 must contain 1 person");
            var person1 = vehicle1.People[0];
            Assert.IsNotNull(person1, "person1 != null");

            // Assert person1
            Assert.AreEqual("RETIRED ", person1.Occupation, "occupation must match");
            Assert.AreEqual(0, person1.NINumbers.Count, "NINOCount must match");
            //Assert.AreEqual(null, person1.NINumbers[0], "NINO must match");
            Assert.AreEqual(3, person1.I2Pe_LinkData.PartyType_Id, "PartyType must match");
            Assert.AreEqual(1, person1.I2Pe_LinkData.SubPartyType_Id, "SubPartyType must match");
            Assert.AreEqual(2, person1.Salutation_Id, "Salutation must match");
            Assert.AreEqual(2, person1.Gender_Id, "Gender must match");
            Assert.AreEqual("MARGARET", person1.FirstName, "FirstName must match");
            Assert.AreEqual(null, person1.MiddleName, "MiddleName must match");
            Assert.AreEqual("HALL", person1.LastName, "LastName must match");
            Assert.AreEqual(new DateTime(1927, 02, 08).ToShortDateString(), person1.DateOfBirth.Value.ToShortDateString(), "DoB must match");

            // Assert Person1 Address
            Assert.AreEqual(2, person1.Addresses.Count, "person1 must have 2 addresses");
            var address1 = person1.Addresses[0];
            Assert.IsNotNull(address1, "address1 != null");

            Assert.AreEqual("FLAT 6 SUNNINGDALE", address1.SubBuilding, "SubBuilding must match");
            Assert.AreEqual("FAIRWAY DRIVE", address1.Building, "Building must match");
            Assert.AreEqual("", address1.BuildingNumber, "BuildingNumber must match");
            Assert.AreEqual("", address1.Street, "Street must match");
            Assert.AreEqual("CHRISTCHURCH", address1.Town, "Town must match");
            Assert.AreEqual("BH23 1JY", address1.PostCode, "PostCode must match");

        }
                
        //[TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_LV_GivenSample_WhenMapped_FirstClaimHasExpectedOrgDetails()
        {

            // Assert General
            var claim = _batch[0];
            Assert.AreEqual(3, claim.Organisations.Count, "Claim must have 3 orgs");
            var org = claim.Organisations[1];
            Assert.IsNotNull(org, "Org != null");

            // Assert Org
            Assert.AreEqual(0, org.OrganisationType_Id, "OrgType must match");
            Assert.AreEqual(0, org.P2O_LinkData.Person2OrganisationLinkType_Id, "Per2OrgLinkType must match");
            Assert.AreEqual("LV REPAIR SERVICES LTD", org.OrganisationName, "OrgName must match");
            Assert.AreEqual("groupfinsys@lv.com", org.EmailAddresses[0].EmailAddress, "Email Address must match");

            // Assert Org Address
            Assert.AreEqual(1, org.Addresses.Count, "Org must have 1 address");
            var orgAddress = org.Addresses[0];
            Assert.IsNotNull(orgAddress, "OrgAddress != null");

            Assert.AreEqual("PO BOX 7740", orgAddress.SubBuilding, "SubBuilding must match");
            Assert.AreEqual("", orgAddress.Building, "Building must match");
            Assert.AreEqual("", orgAddress.BuildingNumber, "BuildingNumber must match");
            Assert.AreEqual("", orgAddress.Street, "Street must match");
            Assert.AreEqual("BOURNEMOUTH", orgAddress.Town, "Town must match");
            Assert.AreEqual("BH1 9XD", orgAddress.PostCode, "PostCode must match");
            Assert.AreEqual(3, orgAddress.PO2A_LinkData.AddressLinkType_Id, "AddressLinkType must match");
            
        }
        
    }
}
