﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using MDA.ClaimService.Interface;
using MDA.Common.Server;
using MDA.Pipeline.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.MappingService.Interface;
using FluentAssertions;

namespace Tests.MappingService.Broadspire
{
    /// <summary>
    /// Test Broadspire Mappings
    /// </summary>       
    [TestClass]
    public class MapperTests
    {

        private static IMapper _mapper;
        private static Stream _fileStream;
        private static List<PipelineMotorClaim> _batch;
        private static CurrentContext _ctx;


        [ClassInitialize]
        public static void ClassInitialise(TestContext context)
        {
            const int riskClientId = 65;
            const int riskUserId = 0;
            const string riskUserName = "Admin1";

            var clientFolder = MappingTestHelper.GetFolderPath();
            var filePath = string.Format("{0}\\{1}", clientFolder, "Broadspire-01012017.zip");

            _ctx = new CurrentContext(riskClientId, riskUserId, riskUserName);
            _batch = new List<PipelineMotorClaim>();
            _fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var currentCreateStatus = new ClaimCreationStatus() { RiskBatchId = 0 };
            var defaultAddresses = new List<string>();
            var defaultPeople = new List<string>();
            var defaultOrgs = new List<string>();

            _mapper = new MDA.MappingService.Impl.Broadspire.Motor.Mapper(_fileStream,
                                                                        clientFolder,
                                                                        _ctx,
                                                                        BuildClaimsIntoBatch,
                                                                        currentCreateStatus,
                                                                        defaultAddresses,
                                                                        defaultPeople,
                                                                        defaultOrgs);
            // Arrange
            Debug.Assert(_mapper != null, "_mapper != null");
            _mapper.AssignFiles();
            _mapper.InitialiseFileHelperEngines();
            _mapper.PopulateFileHelperEngines();
            _mapper.RetrieveDistinctClaims();
            // Act
            _mapper.Translate();
        }



        [ClassCleanup]
        public static void ClassTeardown()
        {
            _ctx = null;
            _mapper = null;
            _batch = null;

            Debug.Assert(_fileStream != null, "_fileStream != null");
            _fileStream.Dispose();
        }

        public static int BuildClaimsIntoBatch(CurrentContext ctx, IPipelineClaim claim, object o)
        {
            //_claimService.TestScanPipelineRecord(ctx, claim);
            Debug.Assert(_batch != null, "_batch != null");
            _batch.Add((PipelineMotorClaim)claim);
            return 0;
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Broadspire_GivenSample_WhenMapped_FirstClaimHasExpectedIncidentDetails()
        {
            // Arrange
            // Act 
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "1000001");

            // Assert General
            _batch.Should().NotBeNull("_batch != null");
            _batch.Count.Should().Be(3, "Batch must contain 3 claims");

            // Assert Claim
            claim.Should().Should().NotBeNull("claim !- null");
            claim.ClaimNumber.Should().BeEquivalentTo("1000001");
            claim.IncidentDate.ShouldBeEquivalentTo(new DateTime(2014, 01, 01));
            claim.ClaimType_Id.Should().Be(1);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Broadspire_GivenSample_WhenMapped_FirstClaimHasExpectedClaimInfoDetails()
        {
            // Arrange
            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "1000001");

            // Assert
            claim.ExtraClaimInfo.ClaimStatus_Id.Should().Be(2);
            claim.ExtraClaimInfo.Reserve.Should().Be(100);
            claim.ExtraClaimInfo.PaymentsToDate.Should().Be(50);
            claim.ExtraClaimInfo.ClaimNotificationDate.ShouldBeEquivalentTo(new DateTime(2014, 01, 02));
            claim.ExtraClaimInfo.IncidentCircumstances.ShouldBeEquivalentTo("Description"); ;
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Broadspire_GivenSample_WhenMapped_FirstClaimHasExpectedPolicyDetails()
        {
            // Arrange
            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "1000001");

            // Assert Policy
            claim.Policy.Insurer.Should().BeEquivalentTo("Carrot Insurance");
            claim.Policy.PolicyStartDate.ShouldBeEquivalentTo(new DateTime(2014, 01, 01));
            claim.Policy.PolicyEndDate.ShouldBeEquivalentTo(new DateTime(2014, 01, 02));

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Broadspire_GivenSample_WhenMapped_FirstClaimHasVehicle()
        {
            // Arrange
            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "1000001");
            var vehicle = claim.Vehicles.FirstOrDefault(x => x.VehicleRegistration == "ML01 AAA");

            // Assert
            claim.Vehicles.Count.Should().Be(2);
            vehicle.Should().NotBeNull();
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Broadspire_GivenSample_WhenMapped_FirstClaimHasExpectedInsuredPersonDetails()
        {
            // Arrange
            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "1000001");
            var vehicle = claim.Vehicles.FirstOrDefault(x => x.VehicleRegistration == "ML01 AAA");
            var person = vehicle.People.FirstOrDefault(x => x.I2Pe_LinkData.PartyType_Id == 3 && x.I2Pe_LinkData.SubPartyType_Id == 1);

            vehicle.People.Count.Should().Be(4);
            person.Should().NotBeNull();

            person.FirstName.ShouldBeEquivalentTo("Test");
            person.LastName.ShouldBeEquivalentTo("Man1");
            person.DateOfBirth.ShouldBeEquivalentTo(new DateTime(1996, 01, 01));
            person.Telephones.Count.ShouldBeEquivalentTo(1);
            person.Addresses.Count.ShouldBeEquivalentTo(1);

        }


        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Broadspire_GivenSample_WhenMapped_FirstClaimHasExpectedClaimantDetails()
        {
            // Arrange

            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "1000001");
            var vehicle = claim.Vehicles.FirstOrDefault(x => x.VehicleRegistration == "ML04 AAA");
            var claimant1 = vehicle.People.FirstOrDefault();


            // Assert
            vehicle.People.Count().ShouldBeEquivalentTo(1);

            claimant1.FirstName.ShouldBeEquivalentTo("TestTP");
            claimant1.LastName.ShouldBeEquivalentTo("Man1");
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Broadspire_GivenSample_WhenMapped_SecondClaimHasExpectedIncidentDetails()
        {

            // Arrange
            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "1000002");
            var vehicle = claim.Vehicles.FirstOrDefault(x => x.VehicleRegistration == "ML02 AAA");
            var person = vehicle.People.FirstOrDefault(x => x.I2Pe_LinkData.PartyType_Id == 3 && x.I2Pe_LinkData.SubPartyType_Id == 1);

            vehicle.People.Count.Should().Be(1);
            person.Should().NotBeNull();

            person.FirstName.ShouldBeEquivalentTo("Test");
            person.LastName.ShouldBeEquivalentTo("Woman2");
            person.DateOfBirth.ShouldBeEquivalentTo(new DateTime(1996, 01, 01));
            person.Telephones.Count.ShouldBeEquivalentTo(1);
            person.Addresses.Count.ShouldBeEquivalentTo(1);

        }



    }
}
