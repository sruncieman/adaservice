﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using MDA.ClaimService.Interface;
using MDA.Common.Server;
using MDA.Pipeline.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.MappingService.Interface;
using FluentAssertions;

namespace Tests.MappingService.Saga
{
    /// <summary>
    /// Test Broadspire Mappings
    /// </summary>       
    [TestClass]
    public class MapperTests
    {

        private static IMapper _mapper;
        private static Stream _fileStream;
        private static List<PipelineMotorClaim> _batch;
        private static CurrentContext _ctx;


        [ClassInitialize]
        public static void ClassInitialise(TestContext context)
        {
            const int riskClientId = 66;
            const int riskUserId = 0;
            const string riskUserName = "Admin1";

            var clientFolder = MappingTestHelper.GetFolderPath();
            var filePath = string.Format("{0}\\{1}", clientFolder, "Saga-20160915_000500.zip");

            _ctx = new CurrentContext(riskClientId, riskUserId, riskUserName);
            _batch = new List<PipelineMotorClaim>();
            _fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var currentCreateStatus = new ClaimCreationStatus() { RiskBatchId = 0 };
            var defaultAddresses = new List<string>();
            var defaultPeople = new List<string>();
            var defaultOrgs = new List<string>();

            _mapper = new MDA.MappingService.Impl.Saga.Motor.Mapper(_fileStream,
                                                                        clientFolder,
                                                                        _ctx,
                                                                        BuildClaimsIntoBatch,
                                                                        currentCreateStatus,
                                                                        defaultAddresses,
                                                                        defaultPeople,
                                                                        defaultOrgs);
            // Arrange
            Debug.Assert(_mapper != null, "_mapper != null");
            _mapper.AssignFiles();
            _mapper.InitialiseFileHelperEngines();
            _mapper.PopulateFileHelperEngines();
            _mapper.RetrieveDistinctClaims();
            // Act
            _mapper.Translate();
        }



        [ClassCleanup]
        public static void ClassTeardown()
        {
            _ctx = null;
            _mapper = null;
            _batch = null;

            Debug.Assert(_fileStream != null, "_fileStream != null");
            _fileStream.Dispose();
        }

        public static int BuildClaimsIntoBatch(CurrentContext ctx, IPipelineClaim claim, object o)
        {
            //_claimService.TestScanPipelineRecord(ctx, claim);
            Debug.Assert(_batch != null, "_batch != null");
            _batch.Add((PipelineMotorClaim)claim);
            return 0;
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Saga_GivenSample_WhenMapped_FirstClaimHasExpectedIncidentDetails()
        {
            // Arrange
            // Act 
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "MA983665");

            // Assert General
            _batch.Should().NotBeNull("_batch != null");
            _batch.Count.Should().Be(4, "Test Batch must contain 4 claims");

            // Assert Claim
            claim.Should().Should().NotBeNull("claim !- null");
            claim.ClaimNumber.Should().BeEquivalentTo("MA983665");
            claim.IncidentDate.ShouldBeEquivalentTo(new DateTime(2016, 09, 13));
            claim.ClaimType_Id.Should().Be(1);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Saga_GivenSample_WhenMapped_FirstClaimHasExpectedClaimInfoDetails()
        {
            // Arrange
            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "MA983665");

            // Assert
            claim.ExtraClaimInfo.ClaimStatus_Id.Should().Be(1);
            claim.ExtraClaimInfo.Reserve.Should().Be(2509.00m);
            claim.ExtraClaimInfo.PaymentsToDate.Should().Be(0m);
            claim.ExtraClaimInfo.ClaimNotificationDate.ShouldBeEquivalentTo(new DateTime(2016, 09, 14));
            claim.ExtraClaimInfo.IncidentCircumstances.ShouldBeEquivalentTo("Collided with pedestrian"); ;
            claim.ExtraClaimInfo.IncidentLocation.ShouldBeEquivalentTo("Unknown, Pitstone, Berkshire "); ;

        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Saga_GivenSample_WhenMapped_FirstClaimHasExpectedPolicyDetails()
        {
            // Arrange
            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "MA983665");

            // Assert Policy
            claim.Policy.Insurer.Should().BeEquivalentTo("Saga");
            claim.Policy.InsurerTradingName.Should().BeEquivalentTo("Acromas Insurance Company Ltd");
            claim.Policy.PolicyStartDate.ShouldBeEquivalentTo(new DateTime(2011, 07, 30));
            claim.Policy.PolicyEndDate.ShouldBeEquivalentTo(new DateTime(2017, 07, 29));
            claim.Policy.Broker.ShouldBeEquivalentTo("AICL Underwritten");
            claim.Policy.Premium.ShouldBeEquivalentTo(435.09m);
            
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Saga_GivenSample_WhenMapped_FirstClaimHasVehicle()
        {
            // Arrange
            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "MA983675");
            var vehicle1 = claim.Vehicles.FirstOrDefault(x => x.VehicleRegistration == "DV62DFF");
            var vehicle2 = claim.Vehicles.FirstOrDefault(x => x.VehicleRegistration == "AY64BUH");

            // Assert
            claim.Vehicles.Count.Should().Be(2);
            vehicle1.Should().NotBeNull();
            vehicle1.VehicleMake = "FORD";
            vehicle1.VehicleModel = "FOCUS";

            vehicle2.Should().NotBeNull();
            vehicle2.VehicleMake = "MERCEDES-BENZ";
            vehicle2.VehicleModel = "SPRINTER 519 CDI";
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Saga_GivenSample_WhenMapped_FirstClaimHasExpectedInsuredPersonDetails()
        {
            // Arrange
            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "MA983675");
            var vehicle = claim.Vehicles.FirstOrDefault(x => x.VehicleRegistration == "DV62DFF");

            var person = vehicle.People.FirstOrDefault(x => x.I2Pe_LinkData.PartyType_Id == 3 && x.I2Pe_LinkData.SubPartyType_Id == 1);

            vehicle.People.Count.Should().Be(4);
            person.Should().NotBeNull();

            person.FirstName.ShouldBeEquivalentTo("Stephen");
            person.LastName.ShouldBeEquivalentTo("Rutter");
            person.DateOfBirth.ShouldBeEquivalentTo(new DateTime(1952, 10, 26));
            person.EmailAddresses.Count.ShouldBeEquivalentTo(1);
            person.Addresses.Count.ShouldBeEquivalentTo(1);

        }


        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Saga_GivenSample_WhenMapped_ThirdPartyHasExpectedVehicleDetails()
        {
            // Arrange

            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "MA983667");
            var vehicle = claim.Vehicles.FirstOrDefault(x => x.VehicleRegistration == "YG03HZT");
            var claimant1 = vehicle.People.FirstOrDefault();


            // Assert
            vehicle.People.Count().ShouldBeEquivalentTo(1);
            vehicle.VehicleMake.ShouldBeEquivalentTo("RENAULT");
            vehicle.VehicleModel.ShouldBeEquivalentTo("MEGANE SCENIC FIDJI 16V");
            claimant1.Should().NotBeNull();            
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void MappingTests_Saga_GivenSample_WhenMapped_ThirdPartyHasExpectedPersonDetails()
        {

            // Arrange
            // Act
            var claim = _batch.FirstOrDefault(x => x.ClaimNumber == "MA983667");
            var vehicle = claim.Vehicles.FirstOrDefault(x => x.VehicleRegistration == "YG03HZT");
            var person = vehicle.People.FirstOrDefault(x => x.I2Pe_LinkData.PartyType_Id == 6 && x.I2Pe_LinkData.SubPartyType_Id == 1);

            vehicle.People.Count.Should().Be(1);
            person.Should().NotBeNull();

            person.FirstName.ShouldBeEquivalentTo("John");
            person.LastName.ShouldBeEquivalentTo("Swinden");
            person.DateOfBirth.ShouldBeEquivalentTo(new DateTime(1935, 03, 21));
            person.Telephones.Count.ShouldBeEquivalentTo(1);
            person.Addresses.Count.ShouldBeEquivalentTo(1);

        }
    }
}
