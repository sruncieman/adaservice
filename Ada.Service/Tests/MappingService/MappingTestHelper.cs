﻿using System;
using System.IO;

namespace Tests.MappingService
{
    public static class MappingTestHelper
    {
        // Keep trying to find 'Files' folder where test data is
        public static string GetFolderPath()
        {
            var path = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory + "\\..\\..\\Files\\");

            if (!Directory.Exists(path))
            {
                // Might not be in a 'bin/Release' folder
                path = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory + "\\..\\Files\\");
            }

            if (!Directory.Exists(path))
            {
                // Might be executing in the bin folder on TFS
                path = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory + "\\Files\\");
            }

            if (!Directory.Exists(path))
            {
                // Might be executing in the bin folder on TFS
                path = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory + "\\..\\Tests\\Files\\");
            }

            return path;
        }
    }
}
