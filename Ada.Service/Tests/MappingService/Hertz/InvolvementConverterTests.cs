﻿using MDA.Common.Enum;
using MDA.MappingService.Impl.Hertz.Motor;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.MappingService.Hertz
{
    /// <summary>
    /// Re : Field Value Mappings in Hertz Doc 
    /// \\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Hertz\Mapping\Hertz UK Data Mapping Analysis.docx
    /// </summary>
    [TestClass]
    public class InvolvementConverterTests
    {
        [TestCategory("MappingTests")]
        [TestMethod]
        public void RenterIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Renter",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.InsuredVehicle);
            Assert.AreEqual(partyType, PartyType.Insured);
            Assert.AreEqual(subPartyType, SubPartyType.Driver);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void ThirdPartyDriverIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Third Party Driver",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.ThirdPartyVehicle);
            Assert.AreEqual(partyType, PartyType.ThirdParty);
            Assert.AreEqual(subPartyType, SubPartyType.Driver);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void PassengerThirdPartyIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Passenger (Third Party)",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.ThirdPartyVehicle);
            Assert.AreEqual(partyType, PartyType.ThirdParty);
            Assert.AreEqual(subPartyType, SubPartyType.Passenger);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void ThirdPartyOwnerIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Third Party Owner",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.ThirdPartyVehicle);
            Assert.AreEqual(partyType, PartyType.ThirdParty);
            Assert.AreEqual(subPartyType, SubPartyType.Unknown);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void PassengerFirstPartyIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Passenger (First Party)",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.InsuredVehicle);
            Assert.AreEqual(partyType, PartyType.Insured);
            Assert.AreEqual(subPartyType, SubPartyType.Passenger);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void ThirdPartyCyclistIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Third Party Cyclist",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.ThirdPartyVehicle);
            Assert.AreEqual(partyType, PartyType.ThirdParty);
            Assert.AreEqual(subPartyType, SubPartyType.Driver);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void ThirdPartyResponsiblePartyIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Third Party (Responsible Party)",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.ThirdPartyVehicle);
            Assert.AreEqual(partyType, PartyType.ThirdParty);
            Assert.AreEqual(subPartyType, SubPartyType.Unknown);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void ThirdPartyPedestrianIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Third Party Pedestrian",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.ThirdPartyVehicle);
            Assert.AreEqual(partyType, PartyType.ThirdParty);
            Assert.AreEqual(subPartyType, SubPartyType.Unknown);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void DriverFirstPartyIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Driver (First Party)",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.InsuredVehicle);
            Assert.AreEqual(partyType, PartyType.Insured);
            Assert.AreEqual(subPartyType, SubPartyType.Driver);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void ThirdPartyMotorcyclistIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Third Party Motorcyclist",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.ThirdPartyVehicle);
            Assert.AreEqual(partyType, PartyType.ThirdParty);
            Assert.AreEqual(subPartyType, SubPartyType.Driver);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void AllegedpartyIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Alleged party",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.ThirdPartyVehicle);
            Assert.AreEqual(partyType, PartyType.ThirdParty);
            Assert.AreEqual(subPartyType, SubPartyType.Unknown);
        }

        [TestCategory("MappingTests")]
        [TestMethod]
        public void ThirdPartyAnimalOwnerIsCorrect()
        {
            //Arrange
            var claimData = new MDA.MappingService.Impl.Hertz.Motor.Model.ClaimData
            {
                Involvement = "Third Party Animal Owner",
            };

            //Act
            var vehicleLinkType = claimData.VehicleLinkType;
            var partyType = claimData.PartyType;
            var subPartyType = claimData.SubPartyType;

            //Assert
            Assert.AreEqual(vehicleLinkType, Incident2VehicleLinkType.ThirdPartyVehicle);
            Assert.AreEqual(partyType, PartyType.ThirdParty);
            Assert.AreEqual(subPartyType, SubPartyType.Driver);
        }
    }
}
