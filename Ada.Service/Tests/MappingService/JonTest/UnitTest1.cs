﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.MappingService.Interface;
using MDA.Common;
using MDA.Common.Server;
using System.IO;
using MDA.ClaimService.Motor;
using MDA.CleansingService.Motor;
using MDA.AddressService.KeoghsPAF;
using MDA.AddressService.Interface;
using MDA.ClaimService.Interface;
using MDA.CleansingService.Interface;
using MDA.MatchingService.Interface;
using MDA.MatchingService.SQL;
using MDA.Pipeline.Model;
using MDA.RiskService;
using MDA.RiskService.Interface;
using MDA.VerificationService;
using MDA.VerificationService.Interface;
using RiskEngine.Interfaces;

namespace Tests.MappingService.JonTest
{
    [TestClass]
    public class UnitTest1
    {
        private IMappingService _mappingService;
        private IAddressService _addressService;
        private ICleansingService _cleansingService;
        private IMatchingService _matchingService;
        private IValidationService _validationService;
        private IRiskServices _riskService;
        private IRiskEngine _riskEngine;
        private IClaimService _claimService;
        private Stream _fileStream;
        private List<PipelineMotorClaim> _batch;
        private CurrentContext _ctx;


        [TestInitialize]
        public void TestInitialise()
        {
            const int riskClientId = 50;
            const int riskUserId = 50;
            const string riskUserName = "Admin";
            const string filePath = @"D:\temp\ADAClients\Canopius\sample.csv";

            _ctx = new CurrentContext(riskClientId, riskUserId, riskUserName);
            
            //todo: swap back to JonTest not Canopius - missing mapper source
            _mappingService = new MDA.MappingService.Impl.Canopius.Motor.FileMapping();

            _fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);

            _addressService  = new AddressService(false);
            _cleansingService = new CleansingService(_ctx, _addressService);
            _matchingService = new MatchingService(_ctx);
            _validationService = new DetailedValidation();
            _riskService = new RiskServices(_ctx);

            _riskEngine = new RiskEngine.Engine.RiskEngine();

            _claimService = new ClaimService(_ctx, 
                                            _cleansingService, 
                                            _matchingService, 
                                            _validationService, 
                                            _riskService, 
                                            _riskEngine);


        }

        [TestCleanup]
        public void TestTeardown()
        {
            _addressService = null;
            _cleansingService = null;
            _matchingService = null;
            _validationService = null;
            _riskService = null;
            _ctx = null;
            _riskEngine = null;
            _claimService = null;
            _mappingService = null;

            _batch = null;

            _fileStream.Dispose();
        }

        public ProcessingResults GetResults()
        {
            ProcessingResults mappingResults;
            ClaimCreationStatus currentCreateStatus = new ClaimCreationStatus() { RiskBatchId = 0 };

            _mappingService.ConvertToClaimBatch(_ctx, _fileStream, currentCreateStatus, BuildClaimsIntoBatch, out mappingResults);

            return mappingResults;
        }


        public int BuildClaimsIntoBatch(CurrentContext ctx, IPipelineClaim claim, object o)
        {
            //_claimService.TestScanPipelineRecord(ctx, claim);
            _batch.Add((PipelineMotorClaim)claim);
            return 0;
        }


        //[TestMethod]
        //public void TestMethod1()
        //{
        //    // Arrange + Act
        //    var actual = GetResults();
          
        //    // Assert
        //    Assert.IsNotNull(actual);

        //}
    }
}
