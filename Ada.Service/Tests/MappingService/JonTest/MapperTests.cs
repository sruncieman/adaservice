﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using MDA.ClaimService.Interface;
using MDA.Common.Server;
using MDA.Pipeline.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MDA.MappingService.Interface;

namespace Tests.MappingService.JonTest
{
    /// <summary>
    /// Test Jon Mappings
    /// </summary>
    [TestClass]
    public class MapperTests
    {

        private IMapper _mapper;        
        private Stream _fileStream;
        private List<PipelineMotorClaim> _batch;
        private CurrentContext _ctx;


        [TestInitialize]
        public void TestInitialise()
        {
            const int riskClientId = 50;
            const int riskUserId = 50;
            const string riskUserName = "Admin";
            //todo: swap filePath to Jon Sample File
            const string filePath = @"D:\temp\ADAClients\JonTest\sample.csv";

            _ctx = new CurrentContext(riskClientId, riskUserId, riskUserName);
            _batch = new List<PipelineMotorClaim>();
            _fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var currentCreateStatus = new ClaimCreationStatus() { RiskBatchId = 0 };
            
            _mapper = new MDA.MappingService.Impl.JonTest.Motor.Mapper(_fileStream, string.Empty, _ctx, BuildClaimsIntoBatch, currentCreateStatus);

        }

        [TestCleanup]
        public void TestTeardown()
        {
            _ctx = null;            
            _mapper = null;
            _batch = null;

            Debug.Assert(_fileStream != null, "_fileStream != null");
            _fileStream.Dispose();
        }
        

        /// <summary>
        /// Helper method, Mapper passes each claim into this method, we collate the claims for assertions later
        /// </summary>
        /// <param name="ctx"><see cref="CurrentContext" />Current DB Context</param>
        /// <param name="claim"><see cref="IPipelineClaim" />mapped claim</param>
        /// <param name="o"><see cref="object"/>Status Tracking Object (not needed here as we're not using the pipelins, so nothing is changing the status)</param>
        /// <returns></returns>
        public int BuildClaimsIntoBatch(CurrentContext ctx, IPipelineClaim claim, object o)
        {
            //_claimService.TestScanPipelineRecord(ctx, claim);
            Debug.Assert(_batch != null, "_batch != null");
            _batch.Add((PipelineMotorClaim)claim);
            return 0;
        }

        [TestMethod]
        public void MappingTests_JonTest__GivenSample_WhenMapped_FirstClaimHasExpectedIncidentDetails()
        {
            // Arrange
            Debug.Assert(_mapper != null, "_mapper != null");
            _mapper.AssignFiles();
            _mapper.InitialiseFileHelperEngines();
            _mapper.PopulateFileHelperEngines();
            _mapper.RetrieveDistinctClaims();
            
            // Act
            _mapper.Translate();
            
            // Assert
            Assert.IsNotNull(_batch, "_batch != null");
            Assert.AreEqual(11, _batch.Count, "Batch must contain 11 items");

            var claim = _batch[0];
            Assert.IsNotNull(claim, "claim !- null");
            
            Assert.AreEqual("196586", claim.ClaimNumber, "Claim number must match");
            Assert.AreEqual(new DateTime(2013,10,24).ToShortDateString(), claim.IncidentDate.ToShortDateString(), "Incident Date must match");

        }


        [TestMethod]
        public void MappingTests_JonTest__GivenSample_WhenMapped_FirstItemHasExpectedPolicyDetails()
        {
            // Arrange
            Debug.Assert(_mapper != null, "_mapper != null");
            _mapper.AssignFiles();
            _mapper.InitialiseFileHelperEngines();
            _mapper.PopulateFileHelperEngines();
            _mapper.RetrieveDistinctClaims();
            
            // Act
            _mapper.Translate();

            // Assert General
            Assert.IsNotNull(_batch, "_batch != null");
            
            var claim = _batch[0];
            Assert.IsNotNull(claim, "claim !- null");

            var policy = claim.Policy;
            Assert.IsNotNull(policy, "policy != null");

            Assert.AreEqual("MT3750020", policy.PolicyNumber, "Policy number must match");
            Assert.AreEqual("Canopius", policy.Insurer, "Policy insurer must match");
        }

        [TestMethod]
        public void MappingTests_JonTest__GivenSample_WhenMapped_FirstItemHasExpectedVehicleDetails()
        {
            // Arrange
            Debug.Assert(_mapper != null, "_mapper != null");
            _mapper.AssignFiles();
            _mapper.InitialiseFileHelperEngines();
            _mapper.PopulateFileHelperEngines();
            _mapper.RetrieveDistinctClaims();

            // Act
            _mapper.Translate();

            // Assert General
            Assert.IsNotNull(_batch, "_batch != null");

            var claim = _batch[0];
            Assert.IsNotNull(claim, "claim !- null");

            Assert.IsNotNull(claim.Vehicles != null, "claim.Vehicles != null");
            
            var vehicle = claim.Vehicles.FirstOrDefault();
            Assert.IsNotNull(vehicle, "vehicle != null");

            Assert.AreEqual("WX02 XUA", vehicle.VehicleRegistration, "Vehicle Reg must match");
        }


    }
}
