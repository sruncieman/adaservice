﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileChopper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string FileName( string fname, int indx )
        {
            string p = Path.GetDirectoryName(fname);

            string f = p + "\\" + Path.GetFileNameWithoutExtension(fname);

            f += "_" + indx.ToString() + Path.GetExtension(fname);

            return f;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            FileStream fs = null;

            try
            {
                int indx = 1;
                button1.Enabled = false;

                
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fName = FileName(openFileDialog1.FileName, indx);
                    int count = 0;
                    bool gotFirstLine = false;
                    string firstLine = "";

                    fs = File.Create(fName);

                    StreamWriter sw = new StreamWriter(fs);

                    foreach (string line in File.ReadLines(openFileDialog1.FileName))
                    {
                        if (!gotFirstLine)
                        {
                            firstLine = line;
                            gotFirstLine = true;
                        }

                        count += line.Count();

                        if (count + line.Count() > numericUpDown1.Value * 1024 * 1024)
                        {
                            indx += 1;
                            count = 0;

                            fs.Close();

                            fName = FileName(openFileDialog1.FileName, indx);
                            fs = File.Create(fName);

                            sw = new StreamWriter(fs);

                            sw.WriteLine(firstLine);
                            count += firstLine.Count();
                        }

                        sw.WriteLine(line);
                    }
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                if (fs != null) fs.Close();

                MessageBox.Show("Done");

                button1.Enabled = true;
            }
        }
    }
}
