﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Globalization;
using RiskEngine.Service.Components;

namespace RiskEngine.Editor
{
    public partial class RuleSetEdit : Form
    {
        public RuleSetEdit()
        {
            InitializeComponent();
        }

        internal static Assembly ResolveAssembly(string assemblyPath, string failedAssemblyName)
        {
            try
            {
                string assemblyName;
                if (failedAssemblyName.Contains(",")) //strong name; need to strip off everything but the name
                {
                    assemblyName = failedAssemblyName.Substring(0, failedAssemblyName.IndexOf(",", StringComparison.Ordinal));
                }
                else
                {
                    assemblyName = failedAssemblyName;
                }
                string tempPath = Path.HasExtension(assemblyPath) ? Path.GetDirectoryName(assemblyPath) : assemblyPath;
                string assemblyPathToTry = tempPath + Path.DirectorySeparatorChar + assemblyName + ".dll";

                FileInfo assemblyFileInfo = new FileInfo(assemblyPathToTry);
                if (assemblyFileInfo.Exists)
                {
                    return Assembly.LoadFile(assemblyPathToTry);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception) //no luck in resolving the assembly
            {
                return null;
            }
        }

        Assembly AssemblyResolve(object sender, ResolveEventArgs args)
        {
            string a = System.Configuration.ConfigurationManager.AppSettings["AssemblyName"].ToString();

            if (!String.IsNullOrEmpty(a))
            {
                return RuleSetEdit.ResolveAssembly(a, args.Name);
            }
            else
            {
                return null;
            }
        }

        internal static Assembly LoadAssembly(string assemblyPath)
        {
            Assembly assembly = null;
            if (!String.IsNullOrEmpty(assemblyPath))
            {
                try
                {
                    FileInfo assemblyFileInfo = new FileInfo(assemblyPath);
                    if (assemblyFileInfo.Exists)
                    {
                        assembly = Assembly.LoadFile(assemblyPath);
                    }
                    else
                    {
                        //try to load from the application directory
                        AssemblyName assemblyName = new AssemblyName(Path.GetFileNameWithoutExtension(assemblyPath));
                        assembly = Assembly.Load(assemblyName);
                    }
                }
                catch (FileLoadException ex)
                {
                    MessageBox.Show(String.Format(CultureInfo.InvariantCulture, "Error loading assembly for the referenced Type at: \r\n\n'{0}' \r\n\n{1}", assemblyPath, ex.Message), "Assembly Load Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show(String.Format(CultureInfo.InvariantCulture, "Error loading assembly for the referenced Type at: \r\n\n'{0}'", assemblyPath), "Assembly Load Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            return assembly;
        }

        private void PopulateTreeView()
        {
            string a = System.Configuration.ConfigurationManager.AppSettings["AssemblyName"].ToString();

            Assembly assembly = LoadAssembly(a);


            treeView1.Nodes.Clear();

            if (assembly != null)
            {
                var type = assembly.GetType("RiskEntitySetup");

                var item = Activator.CreateInstance(type);

                Dictionary<string, string> entityData = (Dictionary<string, string>)type.InvokeMember("GetEntityStructure", 
                    BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.Public, null, null, null);

                var types =
                    assembly.GetTypes()
                        .Where(m => m.IsClass && m.GetInterface("IRiskEntity") != null);

                foreach( var n in types )
                {
                    TreeNode entityNode = new TreeNode();
                    entityNode.Tag = n;
                    entityNode.Text = n.Name.Substring(0, n.Name.Length - 4);

                    // Now add RuleSets that are based on this entity

                    var x = new RiskEngineService(entityData).GetRuleSetsByType(n.FullName);
                    foreach (var n2 in x)
                    {
                        entityNode.Nodes.Add(n2.Name);
                    }

                    treeView1.Nodes.Add(entityNode);
                }
            }

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(AssemblyResolve);

            PopulateTreeView();
        }

        private void treeView1_Click(object sender, EventArgs e)
        {

        }
    }
}
