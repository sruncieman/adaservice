﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using System.Workflow.Activities.Rules;
using System.Workflow.Activities.Rules.Design;
using System.Workflow.ComponentModel.Serialization;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using DoddleReport;
using DoddleReport.Writers;
using RiskEngine.DAL;
using RiskEngine.Model;
using RiskEngine.Service.Components;

namespace RiskEngine.Editor
{
    public partial class OrganisationRuleSets : Form
    {
        private string _currentRuleGroupPath = "";
        private int _currentOrg = 0;
        private RuleScoreData _currentScore = null;
        private RuleGroupData _currentGroup;

        private string _currentUser = "FRED";

        private Dictionary<string, string> EntityStructure;

        private RiskEngineService reService;

        private List<ListValues> orgs; // { new ListValues() { lookup = 0, label = "All" } };

        public OrganisationRuleSets()
        {
            InitializeComponent();

            _currentUser = Environment.UserName; //System.Threading.Thread.CurrentPrincipal.Identity.Name;
            this.Text = "Risk Engine Configuration - Logged in user : " + _currentUser;

            string assemblyName = System.Configuration.ConfigurationManager.AppSettings["AssemblyName"].ToString();
            string entityClass = System.Configuration.ConfigurationManager.AppSettings["EntityStructureClass"].ToString();
            string entityClassMethod = System.Configuration.ConfigurationManager.AppSettings["EntityStructureMethod"].ToString();

            Assembly assembly = LoadAssembly(assemblyName);

            if (assembly != null)
            {
                var type = assembly.GetType(entityClass);

                EntityStructure = (Dictionary<string, string>)type.InvokeMember(entityClassMethod,
                    BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.Public, null, null, null);

                PopulateTreeView();

                reService = new RiskEngineService(EntityStructure);

                reService.CreateAllInitialRuleSetGroups(_currentUser);

                reService.CleanUpAllRuleGroupScores(_currentUser);

                PopulateOrgs();
            }

            //reService.CreateNewClientRuleGroups("1st Central", "admin");
            //reService.CreateNewClientRuleGroups("CH1", "admin");
            //reService.CreateNewClientRuleGroups("CoOp", "admin");
            //reService.CreateNewClientRuleGroups("Sabre", "admin");
        }

        private void PopulateOrgs()
        {
            Dictionary<int, string> o = reService.GetClientList();

            orgs = new List<ListValues>();

            foreach (KeyValuePair<int, String> kvp in o)
            {
                ListValues lv = new ListValues() { lookup = kvp.Key, label = kvp.Value };

                orgs.Add(lv);
            }

            cboClient.DataSource = orgs;
            cboClient.SelectedIndex = _currentOrg;
            cboClient.ValueMember = "lookup";
            cboClient.DisplayMember = "label";
        }

        private void PopulateTreeView()
        {
            treeView1.Nodes.Clear();

            TreeNode lastNode = null;
            string subPathAgg;

            foreach (KeyValuePair<string, String> kvp in EntityStructure)
            {
                string key = kvp.Key;
                string v = kvp.Value;

                subPathAgg = string.Empty;
                foreach (string subPath in key.Split('\\'))
                {
                    subPathAgg += subPath + "\\";
                    TreeNode[] nodes = treeView1.Nodes.Find(subPathAgg, true);
                    if (nodes.Length == 0)
                    {
                        if (lastNode == null)
                            lastNode = treeView1.Nodes.Add(subPathAgg, subPath);
                        else
                            lastNode = lastNode.Nodes.Add(subPathAgg, subPath);

                        lastNode.Tag = key;
                    }
                    else
                        lastNode = nodes[0];
                }

            }

            treeView1.Nodes[0].ExpandAll();
        }

        private bool RuleInUse(List<RuleSetData> list, int id)
        {
            if (list == null) return false;

            foreach (RuleSetData rsd in list)
            {
                if (rsd.RootRuleSetId == id || rsd.RuleSetId == id) return true;
            }
            return false;
        }

        private void PopulateVars()
        {
            lbDefVar.Items.Clear();
            lbClientVar.Items.Clear();

            if (_currentGroup == null) return;

            if (_currentGroup.DefRuleVariables != null)
            {
                foreach (RuleVariableData rvd in _currentGroup.DefRuleVariables)
                {
                    lbDefVar.Items.Add(new VarListValues() { Data = rvd });
                }
            }

            if (_currentGroup.RuleVariables != null)
            {
                foreach (RuleVariableData rvd in _currentGroup.RuleVariables)
                {
                    lbClientVar.Items.Add(new VarListValues() { Data = rvd });
                }
            }

            SetDefVarButtons();
            SetClientVarButtons();
        }

        private void RePopulate()
        {
            ResetVarAndScoreLists();

            Refresh_RuleSets();

            PopulateVars();
        }

        private void ResetVarAndScoreLists()
        {
            lbDefVar.Items.Clear();
            lbClientVar.Items.Clear();

            ClearScores();

            cbRules.SelectedIndex = -1;
            lblRule.Text = "";
        }

        private void Refresh_RuleSets()
        {
            btnDefEdit.Enabled = false;
            btnDefDelete.Enabled = false;
            btnClientEdit.Enabled = false;
            btnClientDelete.Enabled = false;
            btnAdd.Enabled = false;
            btnRemove.Enabled = false;

            lbRuleSets.Items.Clear();
            lbScoreGroups.Items.Clear();

            if (_currentRuleGroupPath.Length == 0) return;

            _currentGroup = reService.GetWorkingRuleGroupByPath(_currentRuleGroupPath, _currentOrg);

            if (_currentGroup == null)
                _currentGroup = reService.GetLiveRuleGroupByPath(_currentRuleGroupPath, _currentOrg);

            if (_currentGroup == null)
                _currentGroup = reService.CreateInitialRuleSetGroup(_currentRuleGroupPath, _currentOrg, _currentUser);

            if (_currentGroup != null)
            {
                //Get all rulesets for given type to populate listbox
                List<RuleSetData> allRuleSets = reService.GetListRuleSets(_currentGroup.EntityClassType);

                if (_currentOrg == 0)
                {
                    foreach (RuleSetData rsd in allRuleSets)
                    {
                        if (RuleInUse(_currentGroup.DefRuleSets, rsd.RuleSetId))
                            lbScoreGroups.Items.Add(new RuleSetListValues() { Data = rsd, RuleSetId = rsd.RootRuleSetId, isDefault = false });
                        else
                            lbRuleSets.Items.Add(new RuleSetListValues() { Data = rsd, RuleSetId = rsd.RootRuleSetId, isDefault = false });
                    }
                }
                else
                {
                    foreach (RuleSetData rsd in allRuleSets)
                    {
                        bool found = false;

                        if (RuleInUse(_currentGroup.RuleSets, rsd.RuleSetId))
                        {
                            lbScoreGroups.Items.Add(new RuleSetListValues() { Data = rsd, RuleSetId = rsd.RootRuleSetId, isDefault = false });
                            found = true;
                        }

                        if (RuleInUse(_currentGroup.DefRuleSets, rsd.RuleSetId))
                        {
                            lbScoreGroups.Items.Add(new RuleSetListValues() { Data = rsd, RuleSetId = rsd.RootRuleSetId, isDefault = true });
                            found = true;
                        }

                        if (!found)
                            lbRuleSets.Items.Add(new RuleSetListValues() { Data = rsd, RuleSetId = rsd.RootRuleSetId, isDefault = false });
                    }
                }

                lbScoreGroups.SelectedIndex = -1;
                lbRuleSets.SelectedIndex = -1;

                btnAdd.Enabled = lbRuleSets.SelectedIndex >= 0 && _currentGroup.Status == 0;
                btnRemove.Enabled = lbRuleSets.SelectedIndex >= 0 && _currentGroup.Status == 0;
                grpScores.Enabled = lbRuleSets.SelectedIndex >= 0 && _currentGroup.Status == 0;
                btnNewRuleset.Enabled = true;
                btnEditRuleset.Enabled = lbRuleSets.SelectedIndex >= 0;
                btnEditRuleset2.Enabled = lbScoreGroups.SelectedIndex >= 0;
                btnExportRuleset.Enabled = lbRuleSets.SelectedIndex >= 0;
                btnExportRuleset2.Enabled = lbRuleSets.SelectedIndex >= 0;
                btnCustom.Enabled = lbDefVar.SelectedIndex >= 0;

                //PopulateVars();

                grpClientVars.Text = ((_currentOrg == 0) ? "Client" : ((ListValues)cboClient.SelectedItem).label) + " Variables";
                btnCreateScores.Text = ((_currentOrg == 0) ? "Create Scores" : "Create " + ((ListValues)cboClient.SelectedItem).label) + " Custom Scores";
                grpScoreGroup.Text = ((_currentOrg == 0) ? "Default Score Group" : ((ListValues)cboClient.SelectedItem).label + " Score Group");

                grpClientVars.Enabled = true;
                grpDefVars.Enabled = true;

                pnlVarButtons.Enabled = (_currentOrg == 0 && _currentGroup.Status == 0);
                pnlCVarButtons.Enabled = _currentGroup.Status == 0;

                btnCustom.Enabled = (_currentOrg != 0 && _currentGroup.DefRuleVariables.Count > 0 && _currentGroup.Status == 0);

                gbRuleSets.Enabled = true;

                btnWorkingVersion.Enabled = (_currentGroup.Status != 0);
            }
            else
            {
                gbRuleSets.Enabled = false;
                btnWorkingVersion.Enabled = true;

                grpClientVars.Enabled = false;
                grpDefVars.Enabled = false;

                pnlVarButtons.Enabled = false;
                pnlCVarButtons.Enabled = false;

            }
        }


        private void cboClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            _currentOrg = ((ListValues)cboClient.SelectedItem).lookup;

            RePopulate();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // TAG holds path to Correct RuleGroup
            _currentRuleGroupPath = treeView1.SelectedNode.Tag.ToString();

            RePopulate();
        }

        private void bntWorkingVersion_Click(object sender, EventArgs e)
        {
            RuleGroupData _liveGroup = reService.GetLiveRuleGroupByPath(_currentRuleGroupPath, _currentOrg);

            DialogResult result = MessageBox.Show("Are you sure you want to Create a Working Version?", "Create a Working Session", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (result.Equals(DialogResult.OK) && _liveGroup != null)
            {
                _currentGroup = reService.CreateWorkingRuleGroup(_liveGroup.RuleGroupId, _currentUser);

                reService.CleanUpAllRuleGroupScores(_currentUser);

                RePopulate();
            }

            else
            {
                return;
            }
        }

        private void SetDefVarButtons()
        {
            btnDefEdit.Enabled = lbDefVar.SelectedIndex >= 0;
            btnDefDelete.Enabled = lbDefVar.SelectedIndex >= 0;
            btnCustom.Enabled = lbDefVar.SelectedIndex >= 0;
        }

        private void SetClientVarButtons()
        {
            btnClientEdit.Enabled = lbClientVar.SelectedIndex >= 0;
            btnClientDelete.Enabled = lbClientVar.SelectedIndex >= 0;
        }
        private void lbDefVar_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDefVarButtons();
        }

        private void lbClientVar_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetClientVarButtons();
        }

        private void btnDefEdit_Click(object sender, EventArgs e)
        {
            if (_currentGroup == null || lbDefVar.SelectedItem == null) return;

            VarEdit editor = new VarEdit(((VarListValues)lbDefVar.SelectedItem).Data.VariableName, ((VarListValues)lbDefVar.SelectedItem).Data.VariableValue);

            if (editor.ShowDialog() == DialogResult.OK)
            {
                RuleVariableData rvd = reService.UpdateVarValue(((VarListValues)lbDefVar.SelectedItem).Data.RuleVariableId, editor.VarName, editor.VarValue, _currentUser);

                if (rvd != null)
                {
                    RuleVariableData old = (from x in _currentGroup.DefRuleVariables where x.VariableName == editor.VarName select x).FirstOrDefault();

                    if (old != null)
                        old.VariableValue = editor.VarValue;

                    PopulateVars();
                };
            }
        }

        private void btnClientEdit_Click(object sender, EventArgs e)
        {
            if (_currentGroup == null || lbClientVar.SelectedItem == null) return;

            VarEdit editor = new VarEdit(((VarListValues)lbClientVar.SelectedItem).Data.VariableName, ((VarListValues)lbClientVar.SelectedItem).Data.VariableValue);

            if (editor.ShowDialog() == DialogResult.OK)
            {
                RuleVariableData rvd = reService.UpdateVarValue(((VarListValues)lbClientVar.SelectedItem).Data.RuleVariableId, editor.VarName, editor.VarValue, _currentUser);

                if (rvd != null)
                {
                    RuleVariableData old = (from x in _currentGroup.RuleVariables where x.VariableName == editor.VarName select x).FirstOrDefault();

                    if (old != null)
                        old.VariableValue = editor.VarValue;

                    PopulateVars();
                }
            }
        }

        private void btnDefNew_Click(object sender, EventArgs e)
        {
            if (_currentGroup == null) return;

            VarEdit editor = new VarEdit();

            if (editor.ShowDialog() == DialogResult.OK)
            {
                RuleVariableData rvd = reService.InsertVarValue(_currentGroup.RuleGroupId, editor.VarName, editor.VarValue, _currentUser);

                var r = (from x in _currentGroup.DefRuleVariables where x.RuleVariableId == rvd.RuleVariableId select x).FirstOrDefault();

                if (r == null)
                    _currentGroup.DefRuleVariables.Add(rvd);
                else
                    r.VariableValue = editor.VarValue;

                PopulateVars();
            }
        }

        private void btnClientNew_Click(object sender, EventArgs e)
        {
            if (_currentGroup == null) return;

            VarEdit editor = new VarEdit();

            if (editor.ShowDialog() == DialogResult.OK)
            {
                RuleVariableData rvd = reService.InsertVarValue(_currentGroup.RuleGroupId, editor.VarName, editor.VarValue, _currentUser);

                var r = (from x in _currentGroup.RuleVariables where x.RuleVariableId == rvd.RuleVariableId select x).FirstOrDefault();

                if (r == null)
                    _currentGroup.RuleVariables.Add(rvd);
                else
                    r.VariableValue = editor.VarValue;

                PopulateVars();
            }
        }

        private void btnDefDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to Delete this variable?", "Delete Variable", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (result.Equals(DialogResult.OK))
            {
                if (_currentGroup == null || lbDefVar.SelectedItem == null) return;

                reService.DeleteVarValue(((VarListValues)lbDefVar.SelectedItem).Data.RuleVariableId);

                _currentGroup.DefRuleVariables.Remove(((VarListValues)lbDefVar.SelectedItem).Data);

                PopulateVars();
            }
        }

        private void btnClientDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to Delete this custom variable?", "Delete Custom Variable", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (result.Equals(DialogResult.OK))
            {

                if (_currentGroup == null || lbClientVar.SelectedItem == null) return;

                reService.DeleteVarValue(((VarListValues)lbClientVar.SelectedItem).Data.RuleVariableId);

                _currentGroup.RuleVariables.Remove(((VarListValues)lbClientVar.SelectedItem).Data);

                PopulateVars();
            }
        }

        private void lbRuleSets_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnAdd.Enabled = false;

            if (lbRuleSets.SelectedItem == null) return;

            RuleSetData rsd = ((RuleSetListValues)lbRuleSets.SelectedItem).Data;

            btnAdd.Enabled = (_currentGroup.Status == 0);
            btnEditRuleset.Enabled = true;
            btnExportRuleset.Enabled = true;
        }

        private void lbScoreGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_currentGroup == null || lbScoreGroups.SelectedItem == null) return;

            btnRemove.Enabled = false;

            if (lbScoreGroups.SelectedItem == null) return;

            RuleSetData rsd = ((RuleSetListValues)lbScoreGroups.SelectedItem).Data;

            btnRemove.Enabled = !((RuleSetListValues)lbScoreGroups.SelectedItem).isDefault && (_currentGroup.Status == 0);
            btnEditRuleset2.Enabled = (_currentGroup.Status == 0);

            cbRules.Items.Clear();
            lblRule.Text = "";

            RuleSet rs = rsd.RuleSet;

            if (rs != null)
            {
                foreach (System.Workflow.Activities.Rules.Rule r in rs.Rules)
                {
                    cbRules.Items.Add(new RuleListValues() { Data = r });
                }
            }

            ClearScores();

            cbRules.SelectedIndex = -1;
            grpThen.Enabled = false;
            grpElse.Enabled = false;

            grpScores.Enabled = lbScoreGroups.SelectedIndex >= 0;
            btnExportRuleset2.Enabled = true;

        }

        private void DisplayScores(RuleScoreData Scores)
        {
            nThenScore.Value = Scores.ThenScore;
            txtThenLow.Text = Scores.ThenMessageLow;
            txtThenMedium.Text = Scores.ThenMessageMedium;
            txtThenHigh.Text = Scores.ThenMessageHigh;
            txtThenError.Text = Scores.ThenError;

            nElseScore.Value = Scores.ElseScore;
            txtElseLow.Text = Scores.ElseMessageLow;
            txtElseMedium.Text = Scores.ElseMessageMedium;
            txtElseHigh.Text = Scores.ElseMessageHigh;
            txtElseError.Text = Scores.ElseError;

            _currentScore = Scores;
        }

        private void RenderScores()
        {
            if (_currentGroup == null || cbRules.SelectedItem == null || lbScoreGroups.SelectedItem == null) return;

            ClearScores();

            if (cbRules.SelectedItem == null) return;

            btnCreateScores.Visible = false;

            // Display Rule in text box
            if (((RuleListValues)cbRules.SelectedItem).Data.Condition == null)
                lblRule.Text = "No IF defined";
            else
                lblRule.Text = ((RuleListValues)cbRules.SelectedItem).Data.Condition.ToString();

            string ruleName = ((RuleListValues)cbRules.SelectedItem).Data.Name;
            int rootRuleSetId = ((RuleSetListValues)lbScoreGroups.SelectedItem).Data.RootRuleSetId;

            var DefaultScores = (from x in _currentGroup.DefRuleScores where x.RuleName == ruleName && x.RootRuleSetId == rootRuleSetId select x).FirstOrDefault();
            var CustomScores = (from x in _currentGroup.RuleScores where x.RuleName == ruleName && x.RootRuleSetId == rootRuleSetId select x).FirstOrDefault();

            if (DefaultScores == null)
            {
                // Create a set of default scores
                DefaultScores = reService.CreateNewScore(_currentGroup.RuleGroupId, rootRuleSetId, ruleName, _currentUser);

                _currentGroup.DefRuleScores.Add(DefaultScores);
            }

            if (_currentOrg == 0)
            {
                grpThen.Enabled = (_currentGroup.Status == 0);
                grpElse.Enabled = (_currentGroup.Status == 0);

                DisplayScores(DefaultScores);
            }
            else // custom scores
            {
                if (CustomScores == null)
                {
                    grpThen.Enabled = false;
                    grpElse.Enabled = false;

                    DisplayScores(DefaultScores);

                    btnCreateScores.Visible = true;

                }
                else
                {
                    grpThen.Enabled = (_currentGroup.Status == 0);
                    grpElse.Enabled = (_currentGroup.Status == 0);

                    DisplayScores(CustomScores);
                }
            }
        }

        private void cbRules_SelectedIndexChanged(object sender, EventArgs e)
        {
            RenderScores();
        }

        private void ClearScores()
        {
            nThenScore.Value = 0;
            txtThenLow.Text = "";
            txtThenHigh.Text = "";
            txtThenMedium.Text = "";
            txtThenError.Text = "";

            nElseScore.Value = 0;
            txtElseLow.Text = "";
            txtElseHigh.Text = "";
            txtElseMedium.Text = "";
            txtElseError.Text = "";

            _currentScore = null;
        }

        private void SaveScores()
        {
            if (_currentScore == null) return;

            _currentScore.RuleScoreId = _currentScore.RuleScoreId;
            _currentScore.ThenScore = (int)nThenScore.Value;
            _currentScore.ThenMessageLow = txtThenLow.Text;
            _currentScore.ThenMessageMedium = txtThenMedium.Text;
            _currentScore.ThenMessageHigh = txtThenHigh.Text;
            _currentScore.ThenError = txtThenError.Text;

            _currentScore.ElseScore = (int)nElseScore.Value;
            _currentScore.ElseMessageLow = txtElseLow.Text;
            _currentScore.ElseMessageMedium = txtElseMedium.Text;
            _currentScore.ElseMessageHigh = txtElseHigh.Text;
            _currentScore.ElseError = txtElseError.Text;

            reService.UpdateScore(_currentScore, _currentUser);
        }

        private void ScoreFieldLeave(object sender, EventArgs e)
        {
            if (_currentGroup.Status == 0)
                SaveScores();
        }

        private void btnCreateScores_Click(object sender, EventArgs e)
        {
            if (_currentGroup == null || cbRules.SelectedItem == null || lbScoreGroups.SelectedItem == null) return;

            string ruleName = ((RuleListValues)cbRules.SelectedItem).Data.Name;

            int rootRuleSetId = ((RuleSetListValues)lbScoreGroups.SelectedItem).Data.RootRuleSetId;

            // Create a set of custom scores
            var CustomScores = reService.CreateNewScore(_currentGroup.RuleGroupId, rootRuleSetId, ruleName, _currentUser);

            _currentGroup.RuleScores.Add(CustomScores);

            btnCreateScores.Visible = false;

            RenderScores();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (_currentGroup == null || lbRuleSets.SelectedItem == null) return;

            if (lbRuleSets.SelectedIndex >= 0)
            {
                #region Assign Ruleset to Group
                int ruleSetId = ((RuleSetListValues)lbRuleSets.SelectedItem).Data.RuleSetId;

                reService.AssignRuleSetToGroup(_currentGroup.RuleGroupId, ruleSetId, _currentUser);

                RePopulate();
                #endregion
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {

            DialogResult result = MessageBox.Show("Are you sure you want to Remove the Ruleset from the rule group?", "Remove Ruleset", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (result.Equals(DialogResult.OK))
            {
                if (_currentGroup == null || lbScoreGroups.SelectedItem == null) return;

                if (lbScoreGroups.SelectedIndex >= 0)
                {
                    #region Remove Ruleset from group
                    int ruleSetId = ((RuleSetListValues)lbScoreGroups.SelectedItem).Data.RuleSetId;

                    reService.RemoveRuleSetFromGroup(_currentGroup.RuleGroupId, ruleSetId);

                    RePopulate();
                    #endregion
                }
            }
        }

        private void txtClientName_TextChanged(object sender, EventArgs e)
        {
            bool exists = (from x in orgs where x.label.ToUpper() == txtClientName.Text.ToUpper() select x).FirstOrDefault() != null;

            lblExists.Visible = exists;

            btnNewClient.Enabled = txtClientName.Text.Length > 0 && !exists;
        }

        private void btnNewClient_Click(object sender, EventArgs e)
        {
            if (txtClientName.Text.Length > 0)
            {
                if (reService.ClientExists(txtClientName.Text))
                {
                    MessageBox.Show("Client with that name exists already", "Error", MessageBoxButtons.OK);
                }
                else
                {
                    reService.CreateNewClientRuleGroups(txtClientName.Text, _currentUser);

                    txtClientName.Text = "";

                    PopulateOrgs();
                }
            }
        }

        private void btnCustom_Click(object sender, EventArgs e)
        {
            if (_currentGroup == null || lbDefVar.SelectedItem == null) return;

            if (lbDefVar.SelectedIndex >= 0)
            {
                var varExists = (from x in _currentGroup.RuleVariables where x.VariableName == ((VarListValues)lbDefVar.SelectedItem).Data.VariableName select x).FirstOrDefault();

                if (varExists != null)
                {
                    #region Update Variable Value
                    VarEdit editor = new VarEdit(varExists.VariableName, varExists.VariableValue);

                    if (editor.ShowDialog() == DialogResult.OK)
                    {
                        RuleVariableData rvd = reService.UpdateVarValue(varExists.RuleVariableId, editor.VarName, editor.VarValue, _currentUser);

                        varExists.VariableValue = rvd.VariableValue;

                        PopulateVars();
                    }
                    #endregion
                }
                else
                {
                    #region Insert Variable Value and add to current group
                    VarEdit editor = new VarEdit(((VarListValues)lbDefVar.SelectedItem).Data.VariableName, ((VarListValues)lbDefVar.SelectedItem).Data.VariableValue);

                    if (editor.ShowDialog() == DialogResult.OK)
                    {
                        RuleVariableData rvd = reService.InsertVarValue(_currentGroup.RuleGroupId, editor.VarName, editor.VarValue, _currentUser);

                        _currentGroup.RuleVariables.Add(rvd);

                        PopulateVars();
                    }
                    #endregion
                }
            }
            else
            {
                #region Insert New Variable Value and add to current group
                VarEdit editor = new VarEdit();

                if (editor.ShowDialog() == DialogResult.OK)
                {
                    RuleVariableData rvd = reService.InsertVarValue(_currentGroup.RuleGroupId, editor.VarName, editor.VarValue, _currentUser);

                    _currentGroup.RuleVariables.Add(rvd);

                    PopulateVars();
                }
                #endregion
            }

        }

        private void btnNewRuleset_Click(object sender, EventArgs e)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(AssemblyResolve);

            string a = System.Configuration.ConfigurationManager.AppSettings["AssemblyName"].ToString();

            Assembly assembly = LoadAssembly(a);

            if (assembly != null)
            {
                RuleSetName d = new RuleSetName();

                if (d.ShowDialog() == DialogResult.OK)
                {
                    Type t = assembly.CreateInstance(_currentGroup.EntityClassType).GetType();

                    RuleSet rs = new RuleSet();

                    RuleSetDialog ruleSetDialog = new RuleSetDialog(t, null, rs);

                    MakeRuleSetDialogResizable(ruleSetDialog);

                    DialogResult result = ruleSetDialog.ShowDialog();

                    if (result == DialogResult.OK)
                    {
                        #region Insert New Ruleset
                        RuleSetData rsd = new RuleSetData();

                        rsd.AssemblyPath = a;
                        rsd.EntityClassType = _currentGroup.EntityClassType;
                        rsd.Version = 1;
                        rsd.Name = d.RulesetName;
                        rsd.RuleSetXML = this.SerializeRuleSet(ruleSetDialog.RuleSet);
                        rsd.Status = 0;

                        reService.InsertRuleSet(rsd, _currentUser);
                        #endregion

                        Refresh_RuleSets();
                    }
                }
            }
        }

        private void EditRuleSet(int ruleSetId)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(AssemblyResolve);

            string a = System.Configuration.ConfigurationManager.AppSettings["AssemblyName"].ToString();

            Assembly assembly = LoadAssembly(a);

            if (assembly != null)
            {
                RuleSetData current_ruleset = reService.GetLatestRuleSetByRootId(ruleSetId);

                RuleSetName d = new RuleSetName(current_ruleset.Name);

                if (d.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        object o = assembly.CreateInstance(_currentGroup.EntityClassType);
                        if (o != null)
                        {
                            Type t = o.GetType();

                            RuleSetDialog ruleSetDialog = new RuleSetDialog(t, null, current_ruleset.RuleSet);

                            MakeRuleSetDialogResizable(ruleSetDialog);

                            DialogResult result = ruleSetDialog.ShowDialog();

                            if (result == DialogResult.OK)
                            {
                                if (current_ruleset.Status == 1)
                                {
                                    #region We have LIVE ruleset so need to clone and update ruleset
                                    RuleSetData new_ruleset = reService.CloneRuleSet(current_ruleset.RuleSetId, _currentUser);

                                    new_ruleset.Name = d.RulesetName;
                                    ruleSetDialog.RuleSet.Name = d.RulesetName;
                                    new_ruleset.RuleSetXML = this.SerializeRuleSet(ruleSetDialog.RuleSet);
                                    new_ruleset.Status = 0;

                                    reService.UpdateRuleSet(new_ruleset);

                                    // Now we need to assign all working rule groups to this new ruleset. If no working rule group create one.
                                    reService.AssignNewWorkingRuleSetToAllRuleGroups(current_ruleset.RuleSetId, new_ruleset.RuleSetId, _currentUser);
                                    #endregion
                                }
                                else
                                {
                                    #region we have a working copy of the ruleset so just update it
                                    RuleSetData rsd = new RuleSetData();

                                    rsd.RuleSetId = current_ruleset.RuleSetId;
                                    rsd.RootRuleSetId = current_ruleset.RootRuleSetId;
                                    rsd.Name = d.RulesetName;
                                    ruleSetDialog.RuleSet.Name = d.RulesetName;
                                    rsd.RuleSetXML = this.SerializeRuleSet(ruleSetDialog.RuleSet);
                                    rsd.Status = 0;

                                    reService.UpdateRuleSet(rsd);
                                    #endregion
                                }

                                Refresh_RuleSets();
                            }
                        }
                        else
                            MessageBox.Show("Failed to create Risk Entity");
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Failed to create Risk Entity : " + ex.Message);
                    }
                }
            }
        }

        private void BtnEditRuleset_Click(object sender, EventArgs e)
        {
            EditRuleSet(((RuleSetListValues)lbRuleSets.SelectedItem).Data.RootRuleSetId);

            Refresh_RuleSets();
        }

        private void btnEdit2_Click(object sender, EventArgs e)
        {
            EditRuleSet(((RuleSetListValues)lbScoreGroups.SelectedItem).Data.RootRuleSetId);

            Refresh_RuleSets();
        }

        #region Serialize Ruleset
        private readonly WorkflowMarkupSerializer serializer = new WorkflowMarkupSerializer();

        private string SerializeRuleSet(RuleSet ruleSet)
        {
            StringBuilder ruleDefinition = new StringBuilder();

            if (ruleSet != null)
            {
                try
                {
                    StringWriter stringWriter = new StringWriter(ruleDefinition, CultureInfo.InvariantCulture);
                    XmlTextWriter writer = new XmlTextWriter(stringWriter);
                    serializer.Serialize(writer, ruleSet);
                    writer.Flush();
                    writer.Close();
                    stringWriter.Flush();
                    stringWriter.Close();
                }
                catch (Exception)
                {
                }
            }
            else
            {
            }

            return ruleDefinition.ToString();
        }
        #endregion

        #region ASSEMBLY Code
        internal static Assembly ResolveAssembly(string assemblyPath, string failedAssemblyName)
        {
            try
            {
                string assemblyName;
                if (failedAssemblyName.Contains(",")) //strong name; need to strip off everything but the name
                {
                    assemblyName = failedAssemblyName.Substring(0, failedAssemblyName.IndexOf(",", StringComparison.Ordinal));
                }
                else
                {
                    assemblyName = failedAssemblyName;
                }
                string tempPath = Path.HasExtension(assemblyPath) ? Path.GetDirectoryName(assemblyPath) : assemblyPath;
                string assemblyPathToTry = tempPath + Path.DirectorySeparatorChar + assemblyName + ".dll";

                FileInfo assemblyFileInfo = new FileInfo(assemblyPathToTry);
                if (assemblyFileInfo.Exists)
                {
                    return Assembly.LoadFile(assemblyPathToTry);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception) //no luck in resolving the assembly
            {
                return null;
            }
        }

        Assembly AssemblyResolve(object sender, ResolveEventArgs args)
        {
            string a = System.Configuration.ConfigurationManager.AppSettings["AssemblyName"].ToString();

            if (!String.IsNullOrEmpty(a))
            {
                return RuleSetEdit.ResolveAssembly(a, args.Name);
            }
            else
            {
                return null;
            }
        }

        [DebuggerNonUserCode]
        internal static Assembly LoadAssembly(string assemblyPath)
        {
            Assembly assembly = null;
            if (!String.IsNullOrEmpty(assemblyPath))
            {
                try
                {
                    try
                    {
                        FileInfo assemblyFileInfo = new FileInfo(assemblyPath);
                        if (assemblyFileInfo.Exists)
                        {
                            assembly = Assembly.LoadFile(assemblyPath);
                        }
                    }
                    catch (Exception)
                    {
                        //try to load from the application directory
                        AssemblyName assemblyName = new AssemblyName(Path.GetFileNameWithoutExtension(assemblyPath));
                        assembly = Assembly.Load(assemblyName);
                    }
                }
                catch (FileLoadException ex)
                {
                    MessageBox.Show(String.Format(CultureInfo.InvariantCulture, "Error loading assembly for the referenced Type at: \r\n\n'{0}' \r\n\n{1}", assemblyPath, ex.Message), "Assembly Load Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show(String.Format(CultureInfo.InvariantCulture, "Error loading assembly for the referenced Type at: \r\n\n'{0}'", assemblyPath), "Assembly Load Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            return assembly;
        }
        #endregion

        #region RESIZE Code
        private static void MakeRuleSetDialogResizable(RuleSetDialog ruleSetDialog)
        {
            ruleSetDialog.FormBorderStyle = FormBorderStyle.Sizable;
            ruleSetDialog.HelpButton = false;
            ruleSetDialog.MaximizeBox = true;
            ruleSetDialog.MinimumSize = new System.Drawing.Size(580, 440);
            ruleSetDialog.Controls["okCancelTableLayoutPanel"].Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            ruleSetDialog.Controls["rulesGroupBox"].Controls["panel1"].Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
            ruleSetDialog.Controls["rulesGroupBox"].Controls["panel1"].Controls["chainingBehaviourComboBox"].Anchor = AnchorStyles.Top | AnchorStyles.Right;
            ruleSetDialog.Controls["rulesGroupBox"].Controls["panel1"].Controls["chainingLabel"].Anchor = AnchorStyles.Top | AnchorStyles.Right;
            ruleSetDialog.Controls["rulesGroupBox"].Controls["panel1"].Controls["rulesListView"].Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
            ruleSetDialog.Resize += new EventHandler(RuleSetDialog_Resize);
        }

        private static void RuleSetDialog_Resize(object sender, EventArgs e)
        {
            Control ruleSetDialog = sender as Control;
            if (ruleSetDialog != null)
            {
                LayoutRuleSetDialog(ruleSetDialog);
            }
        }

        private static void LayoutRuleSetDialog(Control ruleSetDialog)
        {
            const int TopOffset = 50;
            const int BottomOffset = 50;
            const int Padding = 20;
            const int LeftMargin = 10;

            int availableHeight = ruleSetDialog.Size.Height - TopOffset - BottomOffset;
            int rulesGroupBoxHeight = availableHeight / 3;
            int ruleGroupBoxHeight = availableHeight - rulesGroupBoxHeight;
            int groupBoxWidth = ruleSetDialog.Size.Width - 40;

            Control rulesGroupBox = ruleSetDialog.Controls["rulesGroupBox"];
            if (rulesGroupBox != null)
            {
                rulesGroupBox.SetBounds(LeftMargin, TopOffset, groupBoxWidth, rulesGroupBoxHeight - Padding);
            }

            Control ruleGroupBox = ruleSetDialog.Controls["ruleGroupBox"];
            if (ruleGroupBox != null)
            {
                ruleGroupBox.SetBounds(LeftMargin, rulesGroupBoxHeight + TopOffset, groupBoxWidth, ruleGroupBoxHeight - Padding);
                LayoutRuleGroupBox(ruleGroupBox);
            }
        }

        private static void LayoutRuleGroupBox(Control ruleGroupBox)
        {
            const int TopOffset = 80;
            const int Padding = 20;
            const int LeftMargin = 10;
            const int LabelHeight = 17;

            int textBoxHeight = (ruleGroupBox.Size.Height - TopOffset) / 3;
            int textBoxWidth = ruleGroupBox.Size.Width - 20;

            Control conditionTextBox = ruleGroupBox.Controls["conditionTextBox"];
            if (conditionTextBox != null)
            {
                conditionTextBox.SetBounds(LeftMargin, TopOffset, textBoxWidth, textBoxHeight - Padding);
            }

            Control conditionLabel = ruleGroupBox.Controls["conditionLabel"];
            if (conditionLabel != null)
            {
                conditionLabel.SetBounds(LeftMargin, TopOffset - LabelHeight, textBoxWidth, LabelHeight);
            }

            Control thenTextBox = ruleGroupBox.Controls["thenTextBox"];
            if (thenTextBox != null)
            {
                thenTextBox.SetBounds(LeftMargin, textBoxHeight + TopOffset, textBoxWidth, textBoxHeight - Padding);
            }

            Control thenLabel = ruleGroupBox.Controls["thenLabel"];
            if (thenLabel != null)
            {
                thenLabel.SetBounds(LeftMargin, textBoxHeight + TopOffset - LabelHeight, textBoxWidth, LabelHeight);
            }

            Control elseTextBox = ruleGroupBox.Controls["elseTextBox"];
            if (elseTextBox != null)
            {
                elseTextBox.SetBounds(LeftMargin, (textBoxHeight * 2) + TopOffset, textBoxWidth, textBoxHeight - Padding);
            }

            Control elseLabel = ruleGroupBox.Controls["elseLabel"];
            if (elseLabel != null)
            {
                elseLabel.SetBounds(LeftMargin, (textBoxHeight * 2) + TopOffset - LabelHeight, textBoxWidth, LabelHeight);
            }
        }
        #endregion

        private void btnReport_Click(object sender, EventArgs e)
        {
            // foreach ruleset group for this client
        }

        private void btnRelease_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to Release All Change to Live?", "Release All Changes to Live", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (result.Equals(DialogResult.OK))
            {
                reService.ReleaseWorkingSetsToLive();
                RePopulate();
            }
            else
            {
                return;
            }
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    Tester t = new Tester();

        //    t.ShowDialog();
        //}

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ExportRuleSet(int ruleSetId)
        {
            List<RuleSetDataExport> history = reService.GetRuleSetHistoryByRootId(ruleSetId);

            FileStream writer = null;

            try
            {
                if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    writer = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    DataContractSerializer ser = new DataContractSerializer(typeof(List<RuleSetDataExport>));
                    ser.WriteObject(writer, history);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                if (writer != null) writer.Close();
            }
        }

        private void btnExportRuleset_Click(object sender, EventArgs e)
        {
            ExportRuleSet(((RuleSetListValues)lbRuleSets.SelectedItem).Data.RootRuleSetId);
        }

        private void btnRuleSetImport_Click(object sender, EventArgs e)
        {
            FileStream fs = null;
            XmlDictionaryReader reader = null;

            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas() { MaxStringContentLength = 128000 });
                    DataContractSerializer ser = new DataContractSerializer(typeof(List<RuleSetDataExport>));

                    // Deserialize the data and read it from the instance.
                    List<RuleSetDataExport> c = (List<RuleSetDataExport>)ser.ReadObject(reader, true);

                    reService.ImportRuleSetHistory(c, _currentUser);

                    Refresh_RuleSets();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                if (reader != null) reader.Close();
                if (fs != null) fs.Close();
            }
        }

        private void btnExportRuleset2_Click(object sender, EventArgs e)
        {
            ExportRuleSet(((RuleSetListValues)lbScoreGroups.SelectedItem).Data.RootRuleSetId);
        }

        private void btnViewRules_Click(object sender, EventArgs e)
        {
            var query = GetAllRules();

            //var report = new Report(query.ToReportSource(), new DoddleReport.OpenXml.ExcelReportWriter());
            var report = new Report(query.ToReportSource(), new HtmlReportWriter());
            // Customize the Text Fields
            report.TextFields.Title = "Rules Report";
            //report.TextFields.SubTitle = "This is a sample report showing how Doddle Report works";
            //report.TextFields.Footer = "Copyright 2011 &copy; The Doddle Project";
            //            report.TextFields.Header = string.Format(@"
            //    Report Generated: {0}
            //    Total Products: {1}
            //    Total Orders: {2}
            //    Total Sales: {3:c}", DateTime.Now, totalProducts, totalOrders, totalProducts * totalOrders);

            // Render hints allow you to pass additional hints to the reports as they are being rendered


            report.RenderHints.BooleanCheckboxes = true;

            // Customize the data fields
            //report.DataFields["Id"].Hidden = true;
            //report.DataFields["Price"].DataFormatString = "{0:c}";
            //report.DataFields["LastPurchase"].DataFormatString = "{0:d}";

            using (var stream = File.Create("Report.htm"))
            {
                report.WriteReport(stream);
            }


            Process.Start("Report.htm");

        }

        public List<ReportRulesData> GetAllRules()
        {
            var rrd = new List<ReportRulesData>();
            RuleSetEntity rulesetEntity = new RuleSetEntity();
            ReportRulesData reportRulesData = new ReportRulesData();
            RuleSet ruleSet = new RuleSet();
            List<string> lstElseActions = new List<string>();
            List<string> lstThenActions = new List<string>();

            foreach (KeyValuePair<string, String> kvp in EntityStructure)
            {
                string key = kvp.Key;
                string v = kvp.Value;

                if (v != string.Empty)
                {
                    string path = key;

                    _currentGroup = reService.GetWorkingRuleGroupByPath(path, _currentOrg);

                    if (_currentGroup == null)
                        _currentGroup = reService.GetLiveRuleGroupByPath(path, _currentOrg);

                    foreach (var itemDefRuleSets in _currentGroup.DefRuleSets)
                    {
                        rulesetEntity.RuleSetXML = itemDefRuleSets.RuleSetXML;

                        RuleSetData rulesetData = new RuleSetData(rulesetEntity);
                        ruleSet = rulesetData.RuleSet;

                        foreach (var ruleSetItem in ruleSet.Rules)
                        {
                            lstElseActions.AddRange(ruleSetItem.ElseActions.Select(elseActionsItem => elseActionsItem.ToString()));
                            lstThenActions.AddRange(ruleSetItem.ThenActions.Select(thenActionsItem => thenActionsItem.ToString()));

                            foreach (var itemElseActions in lstElseActions)
                            {
                                reportRulesData.RuleElseActions = itemElseActions;
                                reportRulesData.RuleElseActions += "<Br>";
                            }

                            foreach (var itemThenActions in lstThenActions)
                            {
                                reportRulesData.RuleThenActions = itemThenActions;
                                reportRulesData.RuleThenActions += "<Br>";
                            }

                            rrd.Add(new ReportRulesData
                                {
                                    ClientId = _currentGroup.ClientId,
                                    ClientName = reService.GetClientName(_currentGroup.ClientId),
                                    RuleGroupEntityClassType = _currentGroup.EntityClassType,
                                    RuleGroupId = _currentGroup.RuleGroupId,
                                    RuleGroupName = _currentGroup.RuleGroupName,
                                    RuleGroupStatus = _currentGroup.Status,
                                    RuleGroupVersion = _currentGroup.Version,
                                    RuleSetAssemblyPath = itemDefRuleSets.AssemblyPath,
                                    RuleSetCreatedBy = itemDefRuleSets.CreatedBy,
                                    RuleSetCreatedDate = itemDefRuleSets.CreatedDate,
                                    RuleSetEntityClassType = itemDefRuleSets.EntityClassType,
                                    RuleSetName = itemDefRuleSets.Name,
                                    RuleSetRootRuleSetId =itemDefRuleSets.RootRuleSetId,
                                    RuleSetId = itemDefRuleSets.RuleSetId,
                                    RuleSetStatus =itemDefRuleSets.Status,
                                    RuleSetVersion =itemDefRuleSets.Version,
                                    RuleName = ruleSetItem.Name,
                                    RuleActive = ruleSetItem.Active.ToString(),
                                    RuleDescription = ruleSetItem.Description,
                                    RuleCondition = ruleSetItem.Condition.ToString(),
                                    RulePriority = ruleSetItem.Priority.ToString(),
                                    RuleReevaluationBehaviour = ruleSetItem.ReevaluationBehavior.ToString(),
                                    RuleThenActions = reportRulesData.RuleThenActions,
                                    RuleElseActions = reportRulesData.RuleElseActions
                                });
                        }
                    }
                }
            }
            return rrd.ToList();
        }
    }

    public class ListValues
    {
        public int lookup { get; set; }
        public string label { get; set; }
    }

    public class RuleSetListValues
    {
        public int RuleSetId { get; set; }
        public RuleSetData Data { get; set; }
        public bool isDefault { get; set; }

        public override string ToString()
        {
            return ((isDefault) ? "<< " : "") + Data.Name + ((isDefault) ? " >>" : "");
        }
    }

    public class RuleListValues
    {
        public System.Workflow.Activities.Rules.Rule Data { get; set; }

        public override string ToString()
        {
            return Data.Name;
        }
    }

    public class VarListValues
    {
        public RuleVariableData Data { get; set; }

        public override string ToString()
        {
            return string.Format("{0} = {1}", Data.VariableName, Data.VariableValue);
        }
    }
}
