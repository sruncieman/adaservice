﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RiskEngine.Service.Components;

namespace RiskEngine.Editor
{
    public partial class RuleSetName : Form
    {
        private List<string> ruleSetNames;
        private bool checkExists = false;

        public RuleSetName()
        {
            InitializeComponent();

            ruleSetNames = new RiskEngineService().FullListRuleSetsNames();

            checkExists = true;
        }

        public RuleSetName(string name)
        {
            InitializeComponent();

            RulesetName = name;

            checkExists = false;
        }

        public string RulesetName
        {
            get
            {
                return txtName.Text.Trim();
            }

            private set
            {
                txtName.Text = value.Trim();
            }
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            bool exists = false;
            
            if ( checkExists )
                exists = (from x in ruleSetNames where x.ToUpper() == txtName.Text.ToUpper() select x).FirstOrDefault() != null;

            lblExists.Visible = exists;

            btnOK.Enabled = txtName.TextLength > 0 && !exists;
        }
    }
}
