﻿namespace RiskEngine.Editor
{
    partial class OrganisationRuleSets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Policy");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Address");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("PolicyHolder", new System.Windows.Forms.TreeNode[] {
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Address");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Driver", new System.Windows.Forms.TreeNode[] {
            treeNode4});
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Address");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("3rd Party", new System.Windows.Forms.TreeNode[] {
            treeNode6});
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("People", new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode5,
            treeNode7});
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Address");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Companies", new System.Windows.Forms.TreeNode[] {
            treeNode9});
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Vehicles");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Claim", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode8,
            treeNode10,
            treeNode11});
            this.cboClient = new System.Windows.Forms.ComboBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.grpScores = new System.Windows.Forms.GroupBox();
            this.btnCreateScores = new System.Windows.Forms.Button();
            this.grpElse = new System.Windows.Forms.GroupBox();
            this.txtElseError = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtElseHigh = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtElseMedium = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.nElseScore = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.txtElseLow = new System.Windows.Forms.TextBox();
            this.grpThen = new System.Windows.Forms.GroupBox();
            this.txtThenError = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtThenHigh = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtThenMedium = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.nThenScore = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.txtThenLow = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblRule = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbRules = new System.Windows.Forms.ComboBox();
            this.grpDefVars = new System.Windows.Forms.GroupBox();
            this.pnlVarButtons = new System.Windows.Forms.Panel();
            this.btnDefEdit = new System.Windows.Forms.Button();
            this.btnDefDelete = new System.Windows.Forms.Button();
            this.btnDefNew = new System.Windows.Forms.Button();
            this.lbDefVar = new System.Windows.Forms.ListBox();
            this.btnCustom = new System.Windows.Forms.Button();
            this.lbClientVar = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pnlClient = new System.Windows.Forms.Panel();
            this.lblExists = new System.Windows.Forms.Label();
            this.btnReport = new System.Windows.Forms.Button();
            this.txtClientName = new System.Windows.Forms.TextBox();
            this.btnNewClient = new System.Windows.Forms.Button();
            this.btnWorkingVersion = new System.Windows.Forms.Button();
            this.gbRuleSets = new System.Windows.Forms.GroupBox();
            this.grpScoreGroup = new System.Windows.Forms.GroupBox();
            this.btnExportRuleset2 = new System.Windows.Forms.Button();
            this.btnEditRuleset2 = new System.Windows.Forms.Button();
            this.lbScoreGroups = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRuleSetImport = new System.Windows.Forms.Button();
            this.btnExportRuleset = new System.Windows.Forms.Button();
            this.btnEditRuleset = new System.Windows.Forms.Button();
            this.btnNewRuleset = new System.Windows.Forms.Button();
            this.lbRuleSets = new System.Windows.Forms.ListBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpClientVars = new System.Windows.Forms.GroupBox();
            this.pnlCVarButtons = new System.Windows.Forms.Panel();
            this.btnClientEdit = new System.Windows.Forms.Button();
            this.btnClientDelete = new System.Windows.Forms.Button();
            this.btnClientNew = new System.Windows.Forms.Button();
            this.pnlOverride = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRelease = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnViewRules = new System.Windows.Forms.Button();
            this.grpScores.SuspendLayout();
            this.grpElse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nElseScore)).BeginInit();
            this.grpThen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nThenScore)).BeginInit();
            this.panel1.SuspendLayout();
            this.grpDefVars.SuspendLayout();
            this.pnlVarButtons.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.pnlClient.SuspendLayout();
            this.gbRuleSets.SuspendLayout();
            this.grpScoreGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpClientVars.SuspendLayout();
            this.pnlCVarButtons.SuspendLayout();
            this.pnlOverride.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboClient
            // 
            this.cboClient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboClient.FormattingEnabled = true;
            this.cboClient.Items.AddRange(new object[] {
            "DEFAULT",
            "Aviva"});
            this.cboClient.Location = new System.Drawing.Point(6, 21);
            this.cboClient.Name = "cboClient";
            this.cboClient.Size = new System.Drawing.Size(159, 21);
            this.cboClient.TabIndex = 0;
            this.cboClient.SelectedIndexChanged += new System.EventHandler(this.cboClient_SelectedIndexChanged);
            // 
            // treeView1
            // 
            this.treeView1.FullRowSelect = true;
            this.treeView1.HideSelection = false;
            this.treeView1.Location = new System.Drawing.Point(13, 21);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Node5";
            treeNode1.Tag = "Claim\\Policy";
            treeNode1.Text = "Policy";
            treeNode2.Name = "Node7";
            treeNode2.Tag = "Claim\\People\\PolicyHolder\\Address";
            treeNode2.Text = "Address";
            treeNode3.Name = "Node4";
            treeNode3.Tag = "Claim\\People\\PolicyHolder";
            treeNode3.Text = "PolicyHolder";
            treeNode4.Name = "Node9";
            treeNode4.Tag = "Claim\\People\\Driver\\Address";
            treeNode4.Text = "Address";
            treeNode5.Name = "Node8";
            treeNode5.Tag = "Claim\\People\\Driver";
            treeNode5.Text = "Driver";
            treeNode6.Name = "Node11";
            treeNode6.Tag = "Claim\\People\\3rd Party\\Address";
            treeNode6.Text = "Address";
            treeNode7.Name = "Node10";
            treeNode7.Tag = "Claim\\People\\3rd Party";
            treeNode7.Text = "3rd Party";
            treeNode8.Name = "Node2";
            treeNode8.Tag = "Claim\\People";
            treeNode8.Text = "People";
            treeNode9.Name = "Node6";
            treeNode9.Tag = "Claim\\Company\\Address";
            treeNode9.Text = "Address";
            treeNode10.Name = "Node3";
            treeNode10.Tag = "Claim\\Company";
            treeNode10.Text = "Companies";
            treeNode11.Name = "Node3";
            treeNode11.Tag = "Claim\\Vehicles";
            treeNode11.Text = "Vehicles";
            treeNode12.Name = "Node0";
            treeNode12.Tag = "Claim";
            treeNode12.Text = "Claim";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode12});
            this.treeView1.Size = new System.Drawing.Size(165, 284);
            this.treeView1.TabIndex = 8;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Client";
            // 
            // grpScores
            // 
            this.grpScores.Controls.Add(this.btnCreateScores);
            this.grpScores.Controls.Add(this.grpElse);
            this.grpScores.Controls.Add(this.grpThen);
            this.grpScores.Controls.Add(this.panel1);
            this.grpScores.Controls.Add(this.label9);
            this.grpScores.Controls.Add(this.cbRules);
            this.grpScores.Location = new System.Drawing.Point(504, 14);
            this.grpScores.Name = "grpScores";
            this.grpScores.Size = new System.Drawing.Size(389, 464);
            this.grpScores.TabIndex = 22;
            this.grpScores.TabStop = false;
            this.grpScores.Text = "Scores/Messages";
            // 
            // btnCreateScores
            // 
            this.btnCreateScores.Location = new System.Drawing.Point(53, 435);
            this.btnCreateScores.Name = "btnCreateScores";
            this.btnCreateScores.Size = new System.Drawing.Size(288, 23);
            this.btnCreateScores.TabIndex = 30;
            this.btnCreateScores.Text = "Create Scores";
            this.btnCreateScores.UseVisualStyleBackColor = true;
            this.btnCreateScores.Visible = false;
            this.btnCreateScores.Click += new System.EventHandler(this.btnCreateScores_Click);
            // 
            // grpElse
            // 
            this.grpElse.Controls.Add(this.txtElseError);
            this.grpElse.Controls.Add(this.label6);
            this.grpElse.Controls.Add(this.txtElseHigh);
            this.grpElse.Controls.Add(this.label7);
            this.grpElse.Controls.Add(this.txtElseMedium);
            this.grpElse.Controls.Add(this.label8);
            this.grpElse.Controls.Add(this.label10);
            this.grpElse.Controls.Add(this.nElseScore);
            this.grpElse.Controls.Add(this.label11);
            this.grpElse.Controls.Add(this.txtElseLow);
            this.grpElse.Location = new System.Drawing.Point(12, 288);
            this.grpElse.Name = "grpElse";
            this.grpElse.Size = new System.Drawing.Size(363, 144);
            this.grpElse.TabIndex = 29;
            this.grpElse.TabStop = false;
            this.grpElse.Text = "Else/False";
            // 
            // txtElseError
            // 
            this.txtElseError.Location = new System.Drawing.Point(72, 118);
            this.txtElseError.Name = "txtElseError";
            this.txtElseError.Size = new System.Drawing.Size(279, 20);
            this.txtElseError.TabIndex = 21;
            this.txtElseError.Leave += new System.EventHandler(this.ScoreFieldLeave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Error";
            // 
            // txtElseHigh
            // 
            this.txtElseHigh.Location = new System.Drawing.Point(72, 93);
            this.txtElseHigh.Name = "txtElseHigh";
            this.txtElseHigh.Size = new System.Drawing.Size(279, 20);
            this.txtElseHigh.TabIndex = 19;
            this.txtElseHigh.Leave += new System.EventHandler(this.ScoreFieldLeave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "High";
            // 
            // txtElseMedium
            // 
            this.txtElseMedium.Location = new System.Drawing.Point(72, 68);
            this.txtElseMedium.Name = "txtElseMedium";
            this.txtElseMedium.Size = new System.Drawing.Size(279, 20);
            this.txtElseMedium.TabIndex = 17;
            this.txtElseMedium.Leave += new System.EventHandler(this.ScoreFieldLeave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Medium";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Score";
            // 
            // nElseScore
            // 
            this.nElseScore.Location = new System.Drawing.Point(72, 18);
            this.nElseScore.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nElseScore.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.nElseScore.Name = "nElseScore";
            this.nElseScore.Size = new System.Drawing.Size(69, 20);
            this.nElseScore.TabIndex = 12;
            this.nElseScore.Leave += new System.EventHandler(this.ScoreFieldLeave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Low";
            // 
            // txtElseLow
            // 
            this.txtElseLow.Location = new System.Drawing.Point(72, 43);
            this.txtElseLow.Name = "txtElseLow";
            this.txtElseLow.Size = new System.Drawing.Size(279, 20);
            this.txtElseLow.TabIndex = 14;
            this.txtElseLow.Leave += new System.EventHandler(this.ScoreFieldLeave);
            // 
            // grpThen
            // 
            this.grpThen.Controls.Add(this.txtThenError);
            this.grpThen.Controls.Add(this.label12);
            this.grpThen.Controls.Add(this.txtThenHigh);
            this.grpThen.Controls.Add(this.label13);
            this.grpThen.Controls.Add(this.txtThenMedium);
            this.grpThen.Controls.Add(this.label14);
            this.grpThen.Controls.Add(this.label15);
            this.grpThen.Controls.Add(this.nThenScore);
            this.grpThen.Controls.Add(this.label16);
            this.grpThen.Controls.Add(this.txtThenLow);
            this.grpThen.Location = new System.Drawing.Point(12, 122);
            this.grpThen.Name = "grpThen";
            this.grpThen.Size = new System.Drawing.Size(363, 157);
            this.grpThen.TabIndex = 28;
            this.grpThen.TabStop = false;
            this.grpThen.Text = "Then/True";
            // 
            // txtThenError
            // 
            this.txtThenError.Location = new System.Drawing.Point(72, 123);
            this.txtThenError.Name = "txtThenError";
            this.txtThenError.Size = new System.Drawing.Size(279, 20);
            this.txtThenError.TabIndex = 11;
            this.txtThenError.Leave += new System.EventHandler(this.ScoreFieldLeave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Error";
            // 
            // txtThenHigh
            // 
            this.txtThenHigh.Location = new System.Drawing.Point(72, 98);
            this.txtThenHigh.Name = "txtThenHigh";
            this.txtThenHigh.Size = new System.Drawing.Size(279, 20);
            this.txtThenHigh.TabIndex = 9;
            this.txtThenHigh.Leave += new System.EventHandler(this.ScoreFieldLeave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 101);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "High";
            // 
            // txtThenMedium
            // 
            this.txtThenMedium.Location = new System.Drawing.Point(72, 73);
            this.txtThenMedium.Name = "txtThenMedium";
            this.txtThenMedium.Size = new System.Drawing.Size(279, 20);
            this.txtThenMedium.TabIndex = 7;
            this.txtThenMedium.Leave += new System.EventHandler(this.ScoreFieldLeave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Medium";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Score";
            // 
            // nThenScore
            // 
            this.nThenScore.Location = new System.Drawing.Point(72, 23);
            this.nThenScore.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nThenScore.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.nThenScore.Name = "nThenScore";
            this.nThenScore.Size = new System.Drawing.Size(69, 20);
            this.nThenScore.TabIndex = 0;
            this.nThenScore.Leave += new System.EventHandler(this.ScoreFieldLeave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 51);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(27, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Low";
            // 
            // txtThenLow
            // 
            this.txtThenLow.Location = new System.Drawing.Point(72, 48);
            this.txtThenLow.Name = "txtThenLow";
            this.txtThenLow.Size = new System.Drawing.Size(279, 20);
            this.txtThenLow.TabIndex = 4;
            this.txtThenLow.Leave += new System.EventHandler(this.ScoreFieldLeave);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblRule);
            this.panel1.Location = new System.Drawing.Point(12, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(363, 64);
            this.panel1.TabIndex = 26;
            // 
            // lblRule
            // 
            this.lblRule.Location = new System.Drawing.Point(4, 4);
            this.lblRule.Name = "lblRule";
            this.lblRule.Size = new System.Drawing.Size(346, 52);
            this.lblRule.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(81, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Rule";
            // 
            // cbRules
            // 
            this.cbRules.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRules.FormattingEnabled = true;
            this.cbRules.Location = new System.Drawing.Point(116, 19);
            this.cbRules.Name = "cbRules";
            this.cbRules.Size = new System.Drawing.Size(192, 21);
            this.cbRules.TabIndex = 23;
            this.cbRules.SelectedIndexChanged += new System.EventHandler(this.cbRules_SelectedIndexChanged);
            // 
            // grpDefVars
            // 
            this.grpDefVars.Controls.Add(this.pnlVarButtons);
            this.grpDefVars.Controls.Add(this.lbDefVar);
            this.grpDefVars.Location = new System.Drawing.Point(247, 16);
            this.grpDefVars.Name = "grpDefVars";
            this.grpDefVars.Size = new System.Drawing.Size(251, 242);
            this.grpDefVars.TabIndex = 23;
            this.grpDefVars.TabStop = false;
            this.grpDefVars.Text = "Default Variables";
            // 
            // pnlVarButtons
            // 
            this.pnlVarButtons.Controls.Add(this.btnDefEdit);
            this.pnlVarButtons.Controls.Add(this.btnDefDelete);
            this.pnlVarButtons.Controls.Add(this.btnDefNew);
            this.pnlVarButtons.Location = new System.Drawing.Point(7, 207);
            this.pnlVarButtons.Name = "pnlVarButtons";
            this.pnlVarButtons.Size = new System.Drawing.Size(231, 31);
            this.pnlVarButtons.TabIndex = 0;
            // 
            // btnDefEdit
            // 
            this.btnDefEdit.Location = new System.Drawing.Point(24, 3);
            this.btnDefEdit.Name = "btnDefEdit";
            this.btnDefEdit.Size = new System.Drawing.Size(57, 23);
            this.btnDefEdit.TabIndex = 29;
            this.btnDefEdit.Text = "Edit";
            this.btnDefEdit.UseVisualStyleBackColor = true;
            this.btnDefEdit.Click += new System.EventHandler(this.btnDefEdit_Click);
            // 
            // btnDefDelete
            // 
            this.btnDefDelete.Location = new System.Drawing.Point(150, 3);
            this.btnDefDelete.Name = "btnDefDelete";
            this.btnDefDelete.Size = new System.Drawing.Size(57, 23);
            this.btnDefDelete.TabIndex = 25;
            this.btnDefDelete.Text = "Delete";
            this.btnDefDelete.UseVisualStyleBackColor = true;
            this.btnDefDelete.Click += new System.EventHandler(this.btnDefDelete_Click);
            // 
            // btnDefNew
            // 
            this.btnDefNew.Location = new System.Drawing.Point(87, 3);
            this.btnDefNew.Name = "btnDefNew";
            this.btnDefNew.Size = new System.Drawing.Size(57, 23);
            this.btnDefNew.TabIndex = 28;
            this.btnDefNew.Text = "New";
            this.btnDefNew.UseVisualStyleBackColor = true;
            this.btnDefNew.Click += new System.EventHandler(this.btnDefNew_Click);
            // 
            // lbDefVar
            // 
            this.lbDefVar.FormattingEnabled = true;
            this.lbDefVar.Location = new System.Drawing.Point(7, 20);
            this.lbDefVar.Name = "lbDefVar";
            this.lbDefVar.Size = new System.Drawing.Size(232, 186);
            this.lbDefVar.Sorted = true;
            this.lbDefVar.TabIndex = 16;
            this.lbDefVar.SelectedIndexChanged += new System.EventHandler(this.lbDefVar_SelectedIndexChanged);
            this.lbDefVar.DoubleClick += new System.EventHandler(this.btnDefEdit_Click);
            // 
            // btnCustom
            // 
            this.btnCustom.Location = new System.Drawing.Point(342, 265);
            this.btnCustom.Name = "btnCustom";
            this.btnCustom.Size = new System.Drawing.Size(143, 23);
            this.btnCustom.TabIndex = 24;
            this.btnCustom.Text = "Create Custom Variable";
            this.btnCustom.UseVisualStyleBackColor = true;
            this.btnCustom.Click += new System.EventHandler(this.btnCustom_Click);
            // 
            // lbClientVar
            // 
            this.lbClientVar.FormattingEnabled = true;
            this.lbClientVar.Location = new System.Drawing.Point(0, 0);
            this.lbClientVar.Name = "lbClientVar";
            this.lbClientVar.Size = new System.Drawing.Size(231, 134);
            this.lbClientVar.Sorted = true;
            this.lbClientVar.TabIndex = 17;
            this.lbClientVar.SelectedIndexChanged += new System.EventHandler(this.lbClientVar_SelectedIndexChanged);
            this.lbClientVar.DoubleClick += new System.EventHandler(this.btnClientEdit_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.pnlClient);
            this.groupBox5.Controls.Add(this.btnWorkingVersion);
            this.groupBox5.Controls.Add(this.treeView1);
            this.groupBox5.Location = new System.Drawing.Point(12, 21);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(194, 490);
            this.groupBox5.TabIndex = 24;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Master Structure";
            // 
            // pnlClient
            // 
            this.pnlClient.Controls.Add(this.lblExists);
            this.pnlClient.Controls.Add(this.btnReport);
            this.pnlClient.Controls.Add(this.label1);
            this.pnlClient.Controls.Add(this.cboClient);
            this.pnlClient.Controls.Add(this.txtClientName);
            this.pnlClient.Controls.Add(this.btnNewClient);
            this.pnlClient.Location = new System.Drawing.Point(13, 356);
            this.pnlClient.Name = "pnlClient";
            this.pnlClient.Size = new System.Drawing.Size(175, 111);
            this.pnlClient.TabIndex = 31;
            // 
            // lblExists
            // 
            this.lblExists.AutoSize = true;
            this.lblExists.ForeColor = System.Drawing.Color.Red;
            this.lblExists.Location = new System.Drawing.Point(101, 49);
            this.lblExists.Name = "lblExists";
            this.lblExists.Size = new System.Drawing.Size(43, 13);
            this.lblExists.TabIndex = 27;
            this.lblExists.Text = "Exists...";
            this.lblExists.Visible = false;
            // 
            // btnReport
            // 
            this.btnReport.Enabled = false;
            this.btnReport.Location = new System.Drawing.Point(107, 76);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(58, 23);
            this.btnReport.TabIndex = 26;
            this.btnReport.Text = "Report";
            this.btnReport.UseVisualStyleBackColor = true;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // txtClientName
            // 
            this.txtClientName.Location = new System.Drawing.Point(6, 50);
            this.txtClientName.Name = "txtClientName";
            this.txtClientName.Size = new System.Drawing.Size(88, 20);
            this.txtClientName.TabIndex = 25;
            this.txtClientName.TextChanged += new System.EventHandler(this.txtClientName_TextChanged);
            // 
            // btnNewClient
            // 
            this.btnNewClient.Enabled = false;
            this.btnNewClient.Location = new System.Drawing.Point(6, 76);
            this.btnNewClient.Name = "btnNewClient";
            this.btnNewClient.Size = new System.Drawing.Size(88, 23);
            this.btnNewClient.TabIndex = 24;
            this.btnNewClient.Text = "Add";
            this.btnNewClient.UseVisualStyleBackColor = true;
            this.btnNewClient.Click += new System.EventHandler(this.btnNewClient_Click);
            // 
            // btnWorkingVersion
            // 
            this.btnWorkingVersion.Location = new System.Drawing.Point(13, 307);
            this.btnWorkingVersion.Name = "btnWorkingVersion";
            this.btnWorkingVersion.Size = new System.Drawing.Size(165, 23);
            this.btnWorkingVersion.TabIndex = 30;
            this.btnWorkingVersion.Text = "Create Working Version";
            this.btnWorkingVersion.UseVisualStyleBackColor = true;
            this.btnWorkingVersion.Click += new System.EventHandler(this.bntWorkingVersion_Click);
            // 
            // gbRuleSets
            // 
            this.gbRuleSets.Controls.Add(this.grpScoreGroup);
            this.gbRuleSets.Controls.Add(this.groupBox1);
            this.gbRuleSets.Controls.Add(this.btnRemove);
            this.gbRuleSets.Controls.Add(this.btnAdd);
            this.gbRuleSets.Controls.Add(this.grpClientVars);
            this.gbRuleSets.Controls.Add(this.btnCustom);
            this.gbRuleSets.Controls.Add(this.grpScores);
            this.gbRuleSets.Controls.Add(this.grpDefVars);
            this.gbRuleSets.Location = new System.Drawing.Point(212, 21);
            this.gbRuleSets.Name = "gbRuleSets";
            this.gbRuleSets.Size = new System.Drawing.Size(907, 490);
            this.gbRuleSets.TabIndex = 25;
            this.gbRuleSets.TabStop = false;
            this.gbRuleSets.Text = "RuleGroup";
            // 
            // grpScoreGroup
            // 
            this.grpScoreGroup.AccessibleDescription = "";
            this.grpScoreGroup.Controls.Add(this.btnExportRuleset2);
            this.grpScoreGroup.Controls.Add(this.btnEditRuleset2);
            this.grpScoreGroup.Controls.Add(this.lbScoreGroups);
            this.grpScoreGroup.Location = new System.Drawing.Point(12, 288);
            this.grpScoreGroup.Name = "grpScoreGroup";
            this.grpScoreGroup.Size = new System.Drawing.Size(218, 190);
            this.grpScoreGroup.TabIndex = 41;
            this.grpScoreGroup.TabStop = false;
            this.grpScoreGroup.Text = "Default Score Group";
            // 
            // btnExportRuleset2
            // 
            this.btnExportRuleset2.Location = new System.Drawing.Point(163, 159);
            this.btnExportRuleset2.Name = "btnExportRuleset2";
            this.btnExportRuleset2.Size = new System.Drawing.Size(49, 23);
            this.btnExportRuleset2.TabIndex = 37;
            this.btnExportRuleset2.Text = "Export";
            this.btnExportRuleset2.UseVisualStyleBackColor = true;
            this.btnExportRuleset2.Click += new System.EventHandler(this.btnExportRuleset2_Click);
            // 
            // btnEditRuleset2
            // 
            this.btnEditRuleset2.Location = new System.Drawing.Point(6, 159);
            this.btnEditRuleset2.Name = "btnEditRuleset2";
            this.btnEditRuleset2.Size = new System.Drawing.Size(66, 23);
            this.btnEditRuleset2.TabIndex = 36;
            this.btnEditRuleset2.Text = "Edit";
            this.btnEditRuleset2.UseVisualStyleBackColor = true;
            this.btnEditRuleset2.Click += new System.EventHandler(this.btnEdit2_Click);
            // 
            // lbScoreGroups
            // 
            this.lbScoreGroups.FormattingEnabled = true;
            this.lbScoreGroups.Location = new System.Drawing.Point(6, 19);
            this.lbScoreGroups.Name = "lbScoreGroups";
            this.lbScoreGroups.Size = new System.Drawing.Size(206, 134);
            this.lbScoreGroups.Sorted = true;
            this.lbScoreGroups.TabIndex = 34;
            this.lbScoreGroups.SelectedIndexChanged += new System.EventHandler(this.lbScoreGroups_SelectedIndexChanged);
            this.lbScoreGroups.DoubleClick += new System.EventHandler(this.btnEdit2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRuleSetImport);
            this.groupBox1.Controls.Add(this.btnExportRuleset);
            this.groupBox1.Controls.Add(this.btnEditRuleset);
            this.groupBox1.Controls.Add(this.btnNewRuleset);
            this.groupBox1.Controls.Add(this.lbRuleSets);
            this.groupBox1.Location = new System.Drawing.Point(12, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(218, 242);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "RuleSet Library";
            // 
            // btnRuleSetImport
            // 
            this.btnRuleSetImport.Location = new System.Drawing.Point(163, 210);
            this.btnRuleSetImport.Name = "btnRuleSetImport";
            this.btnRuleSetImport.Size = new System.Drawing.Size(49, 23);
            this.btnRuleSetImport.TabIndex = 36;
            this.btnRuleSetImport.Text = "Import";
            this.btnRuleSetImport.UseVisualStyleBackColor = true;
            this.btnRuleSetImport.Click += new System.EventHandler(this.btnRuleSetImport_Click);
            // 
            // btnExportRuleset
            // 
            this.btnExportRuleset.Location = new System.Drawing.Point(108, 210);
            this.btnExportRuleset.Name = "btnExportRuleset";
            this.btnExportRuleset.Size = new System.Drawing.Size(49, 23);
            this.btnExportRuleset.TabIndex = 35;
            this.btnExportRuleset.Text = "Export";
            this.btnExportRuleset.UseVisualStyleBackColor = true;
            this.btnExportRuleset.Click += new System.EventHandler(this.btnExportRuleset_Click);
            // 
            // btnEditRuleset
            // 
            this.btnEditRuleset.Location = new System.Drawing.Point(52, 210);
            this.btnEditRuleset.Name = "btnEditRuleset";
            this.btnEditRuleset.Size = new System.Drawing.Size(39, 23);
            this.btnEditRuleset.TabIndex = 34;
            this.btnEditRuleset.Text = "Edit";
            this.btnEditRuleset.UseVisualStyleBackColor = true;
            this.btnEditRuleset.Click += new System.EventHandler(this.BtnEditRuleset_Click);
            // 
            // btnNewRuleset
            // 
            this.btnNewRuleset.Location = new System.Drawing.Point(6, 210);
            this.btnNewRuleset.Name = "btnNewRuleset";
            this.btnNewRuleset.Size = new System.Drawing.Size(40, 23);
            this.btnNewRuleset.TabIndex = 33;
            this.btnNewRuleset.Text = "New";
            this.btnNewRuleset.UseVisualStyleBackColor = true;
            this.btnNewRuleset.Click += new System.EventHandler(this.btnNewRuleset_Click);
            // 
            // lbRuleSets
            // 
            this.lbRuleSets.FormattingEnabled = true;
            this.lbRuleSets.Location = new System.Drawing.Point(6, 19);
            this.lbRuleSets.Name = "lbRuleSets";
            this.lbRuleSets.Size = new System.Drawing.Size(206, 186);
            this.lbRuleSets.Sorted = true;
            this.lbRuleSets.TabIndex = 32;
            this.lbRuleSets.SelectedIndexChanged += new System.EventHandler(this.lbRuleSets_SelectedIndexChanged);
            this.lbRuleSets.DoubleClick += new System.EventHandler(this.BtnEditRuleset_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(158, 262);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(66, 23);
            this.btnRemove.TabIndex = 37;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(88, 262);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(66, 23);
            this.btnAdd.TabIndex = 36;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grpClientVars
            // 
            this.grpClientVars.Controls.Add(this.pnlCVarButtons);
            this.grpClientVars.Controls.Add(this.pnlOverride);
            this.grpClientVars.Location = new System.Drawing.Point(247, 288);
            this.grpClientVars.Name = "grpClientVars";
            this.grpClientVars.Size = new System.Drawing.Size(251, 190);
            this.grpClientVars.TabIndex = 31;
            this.grpClientVars.TabStop = false;
            this.grpClientVars.Text = "Client Variables";
            // 
            // pnlCVarButtons
            // 
            this.pnlCVarButtons.Controls.Add(this.btnClientEdit);
            this.pnlCVarButtons.Controls.Add(this.btnClientDelete);
            this.pnlCVarButtons.Controls.Add(this.btnClientNew);
            this.pnlCVarButtons.Location = new System.Drawing.Point(8, 154);
            this.pnlCVarButtons.Name = "pnlCVarButtons";
            this.pnlCVarButtons.Size = new System.Drawing.Size(231, 31);
            this.pnlCVarButtons.TabIndex = 30;
            // 
            // btnClientEdit
            // 
            this.btnClientEdit.Location = new System.Drawing.Point(24, 3);
            this.btnClientEdit.Name = "btnClientEdit";
            this.btnClientEdit.Size = new System.Drawing.Size(57, 23);
            this.btnClientEdit.TabIndex = 29;
            this.btnClientEdit.Text = "Edit";
            this.btnClientEdit.UseVisualStyleBackColor = true;
            this.btnClientEdit.Click += new System.EventHandler(this.btnClientEdit_Click);
            // 
            // btnClientDelete
            // 
            this.btnClientDelete.Location = new System.Drawing.Point(150, 3);
            this.btnClientDelete.Name = "btnClientDelete";
            this.btnClientDelete.Size = new System.Drawing.Size(57, 23);
            this.btnClientDelete.TabIndex = 25;
            this.btnClientDelete.Text = "Delete";
            this.btnClientDelete.UseVisualStyleBackColor = true;
            this.btnClientDelete.Click += new System.EventHandler(this.btnClientDelete_Click);
            // 
            // btnClientNew
            // 
            this.btnClientNew.Location = new System.Drawing.Point(87, 3);
            this.btnClientNew.Name = "btnClientNew";
            this.btnClientNew.Size = new System.Drawing.Size(57, 23);
            this.btnClientNew.TabIndex = 28;
            this.btnClientNew.Text = "New";
            this.btnClientNew.UseVisualStyleBackColor = true;
            this.btnClientNew.Click += new System.EventHandler(this.btnClientNew_Click);
            // 
            // pnlOverride
            // 
            this.pnlOverride.Controls.Add(this.lbClientVar);
            this.pnlOverride.Location = new System.Drawing.Point(8, 19);
            this.pnlOverride.Name = "pnlOverride";
            this.pnlOverride.Size = new System.Drawing.Size(231, 134);
            this.pnlOverride.TabIndex = 31;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnClose.Location = new System.Drawing.Point(943, 517);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(176, 23);
            this.btnClose.TabIndex = 26;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRelease
            // 
            this.btnRelease.Location = new System.Drawing.Point(12, 517);
            this.btnRelease.Name = "btnRelease";
            this.btnRelease.Size = new System.Drawing.Size(194, 23);
            this.btnRelease.TabIndex = 27;
            this.btnRelease.Text = "Release All Changes to Live";
            this.btnRelease.UseVisualStyleBackColor = true;
            this.btnRelease.Click += new System.EventHandler(this.btnRelease_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xml";
            this.saveFileDialog1.Filter = "XML Files|*.xml";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "xml";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "XML Files|*.xml";
            // 
            // btnViewRules
            // 
            this.btnViewRules.Location = new System.Drawing.Point(224, 518);
            this.btnViewRules.Name = "btnViewRules";
            this.btnViewRules.Size = new System.Drawing.Size(122, 23);
            this.btnViewRules.TabIndex = 28;
            this.btnViewRules.Text = "View Rules";
            this.btnViewRules.UseVisualStyleBackColor = true;
            this.btnViewRules.Click += new System.EventHandler(this.btnViewRules_Click);
            // 
            // OrganisationRuleSets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1128, 549);
            this.Controls.Add(this.btnViewRules);
            this.Controls.Add(this.btnRelease);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.gbRuleSets);
            this.Controls.Add(this.groupBox5);
            this.Name = "OrganisationRuleSets";
            this.Text = "Risk Engine Configuration";
            this.grpScores.ResumeLayout(false);
            this.grpScores.PerformLayout();
            this.grpElse.ResumeLayout(false);
            this.grpElse.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nElseScore)).EndInit();
            this.grpThen.ResumeLayout(false);
            this.grpThen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nThenScore)).EndInit();
            this.panel1.ResumeLayout(false);
            this.grpDefVars.ResumeLayout(false);
            this.pnlVarButtons.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.pnlClient.ResumeLayout(false);
            this.pnlClient.PerformLayout();
            this.gbRuleSets.ResumeLayout(false);
            this.grpScoreGroup.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.grpClientVars.ResumeLayout(false);
            this.pnlCVarButtons.ResumeLayout(false);
            this.pnlOverride.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cboClient;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpScores;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbRules;
        private System.Windows.Forms.GroupBox grpDefVars;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox gbRuleSets;
        private System.Windows.Forms.Button btnDefNew;
        private System.Windows.Forms.Button btnDefDelete;
        private System.Windows.Forms.Button btnCustom;
        private System.Windows.Forms.ListBox lbClientVar;
        private System.Windows.Forms.ListBox lbDefVar;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnNewClient;
        private System.Windows.Forms.Button btnWorkingVersion;
        private System.Windows.Forms.Panel pnlOverride;
        private System.Windows.Forms.Panel pnlVarButtons;
        private System.Windows.Forms.GroupBox grpClientVars;
        private System.Windows.Forms.Button btnDefEdit;
        private System.Windows.Forms.Panel pnlCVarButtons;
        private System.Windows.Forms.Button btnClientEdit;
        private System.Windows.Forms.Button btnClientDelete;
        private System.Windows.Forms.Button btnClientNew;
        private System.Windows.Forms.Label lblRule;
        private System.Windows.Forms.GroupBox grpElse;
        private System.Windows.Forms.TextBox txtElseError;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtElseHigh;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtElseMedium;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nElseScore;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtElseLow;
        private System.Windows.Forms.GroupBox grpThen;
        private System.Windows.Forms.TextBox txtThenError;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtThenHigh;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtThenMedium;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown nThenScore;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtThenLow;
        private System.Windows.Forms.Button btnCreateScores;
        private System.Windows.Forms.ListBox lbScoreGroups;
        private System.Windows.Forms.ListBox lbRuleSets;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grpScoreGroup;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtClientName;
        private System.Windows.Forms.Panel pnlClient;
        private System.Windows.Forms.Button btnExportRuleset;
        private System.Windows.Forms.Button btnEditRuleset;
        private System.Windows.Forms.Button btnNewRuleset;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Button btnEditRuleset2;
        private System.Windows.Forms.Button btnRelease;
        private System.Windows.Forms.Label lblExists;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnRuleSetImport;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnExportRuleset2;
        private System.Windows.Forms.Button btnViewRules;
    }
}