﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using RiskEngine.Model;

namespace RiskEngine.Editor
{
    internal partial class RuleSetSelector : Form
    {
        #region Variables and constructor

        internal RuleSetSelector()
        {
            InitializeComponent();

            RuleSetDataCollection = new List<RuleSetData>();
            SelectedRuleSetDataCollection = new List<RuleSetData>();
        }

        #endregion

        #region Properties

        internal string Instructions { get; set; }
        internal bool SelectAll { get; set; }
        internal List<RuleSetData> RuleSetDataCollection { get; private set; }
        internal List<RuleSetData> SelectedRuleSetDataCollection { get; private set; }

        #endregion

        #region Form load
        
        private void RuleSetSelectorForm_Load(object sender, EventArgs e)
        {
            instructionsTextBox.Text = Instructions;
            RuleSetDataCollection.Sort();

            ruleSetsListBox.DataSource = RuleSetDataCollection;
            if (SelectAll)
            {
                foreach (object item in RuleSetDataCollection)
                {
                    ruleSetsListBox.SelectedItems.Add(item);
                }
            }
        }

        #endregion

        #region Event handlers

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            SelectedRuleSetDataCollection.Clear();

            foreach (object item in ruleSetsListBox.SelectedItems)
            {
                SelectedRuleSetDataCollection.Add(item as RuleSetData);
            }

            string duplicateRuleSetName;

            if (this.ValidateUniqueness(out duplicateRuleSetName))
            {
                this.Close();
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(string.Format(CultureInfo.InvariantCulture, "Multiple RuleSets selected with the same Name: '{0}'.", duplicateRuleSetName), "Export Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ValidateUniqueness(out string duplicateRuleSetName)
        {
            foreach (RuleSetData data1 in SelectedRuleSetDataCollection)
            {
                foreach (RuleSetData data2 in SelectedRuleSetDataCollection)
                {
                    if (data1 != data2 && String.CompareOrdinal(data1.Name, data2.Name) == 0)
                    {
                        duplicateRuleSetName = data1.Name;
                        return false;
                    }
                }
            }
            duplicateRuleSetName = null;
            return true;
        }

        #endregion
    }
}

