﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RiskEngine.Editor
{
    public partial class VarEdit : Form
    {
        public VarEdit()
        {
            InitializeComponent();

            txtName.Enabled = true;
        }

        public VarEdit(string name, string value)
        {
            InitializeComponent();

            VarName = name;
            VarValue = value;

            txtName.Enabled = false;
        }

        public string VarName
        {
            get
            {
                return txtName.Text.Trim();
            }

            set
            {
                txtName.Text = value.Trim();
            }
        }

        public string VarValue
        {
            get
            {
                return txtValue.Text.Trim();
            }

            set
            {
                txtValue.Text = value.Trim();
            }
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            btnOK.Enabled = txtName.TextLength > 0 && txtValue.TextLength > 0;
        }
    }
}
