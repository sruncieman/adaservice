﻿//----------------------------------------------------------------
// Copyright (c) Microsoft Corporation.  All rights reserved.
//----------------------------------------------------------------

using System;
using System.Windows.Forms;
using System.Workflow.ComponentModel.Compiler;

namespace RiskEngine.Editor
{
    internal partial class ValidationErrorsForm : Form
    {

        #region Constructor and variables
        
        internal ValidationErrorsForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Form load

        private void ValidationErrorsForm_Load(object sender, EventArgs e)
        {
            if (!PromptForContinue) //just showing the errors, so change the representation in the UI
            {
                continueButton.Hide();
                cancelButton.Text = "OK";
            }
        }

        internal void SetValidationErrors(ValidationErrorCollection errors)
        {

                foreach(ValidationError error in errors)
                {
                    validationErrorsBox.Items.Add(error);
                }
        }

        #endregion

        #region Properties

        internal bool PromptForContinue { get; set; }
        internal string ErrorText { get; set; }
        internal bool ContinueWithChange { get; private set; }

        #endregion

        #region Event handlers

        private void continueButton_Click(object sender, EventArgs e)
        {
            ContinueWithChange = true;
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
