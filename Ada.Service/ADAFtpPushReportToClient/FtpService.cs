﻿using System;
using System.Diagnostics;
using WinSCP;

namespace ADAFtpPushReportToClient
{
    public interface IFtpService
    {
        void SendFileToKeoghs(string fileName);
        void GetFilesFromKeoghs();
        void SendFilesToClient();
    }

    /// <summary>
    /// Wrapper helper around Winscp
    /// </summary>
    public class FtpService : IFtpService
    {
        private readonly Config _config;
        private readonly SessionOptions _sessionOptions;
        private readonly TransferOptions _transferOptions;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="config"><see cref="Config"/></param>
        public FtpService(Config config)
        {
            if (config == null) throw new ArgumentNullException("config");
            _config = config;

            _sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = _config.KeoghsHostName,
                UserName = _config.KeoghsUserName,
                Password = _config.KeoghsPassword,

                SshHostKeyFingerprint = _config.KeoghsSshHostKeyFingerprint,
            };

            _transferOptions = new TransferOptions
            {
                TransferMode = TransferMode.Binary,
                FileMask = _config.GetFileMask,
                ResumeSupport = {State = TransferResumeSupportState.Off}
            };
        }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="config"><see cref="Config"/></param>
        /// <param name="sessionOptions"><see cref="SessionOptions"/></param>
        /// <param name="transferOptions"><see cref="TransferOptions"/></param>
        public FtpService(Config config, SessionOptions sessionOptions, TransferOptions transferOptions)
        {
            if (config == null) throw new ArgumentNullException("config");
            if (sessionOptions == null) throw new ArgumentNullException("sessionOptions");
            if (transferOptions == null) throw new ArgumentNullException("transferOptions");

            _sessionOptions = sessionOptions;
            _transferOptions = transferOptions;
            _config = config;
        }

        /// <summary>
        /// Send a file from filesystem via FTP
        /// </summary>
        /// <param name="fileNamePath">path and filename of file to upload</param>
        public void SendFileToKeoghs(string fileNamePath)
        {
            if (string.IsNullOrEmpty(fileNamePath))
                throw new ArgumentException("Value cannot be null or empty.", "fileNamePath");

            using (var session = new Session())
            {
                // Connect            
                session.SessionLogPath = _config.FtpLogFile;
                session.Open(_sessionOptions);

                var transferResult = session.PutFiles(fileNamePath, _config.KeoghsFtpPath, false, _transferOptions);

                Debug.Assert(transferResult != null, "transferResult != null");
                Debug.Assert(transferResult.Transfers != null, "transferResult.Transfers != null");
                // Throw on any error                
                transferResult.Check();

                // Print results                
                foreach (TransferEventArgs transfer in transferResult.Transfers)
                {
                    Console.WriteLine("Upload of {0} succeeded", transfer.FileName);
                }
            }
        }

        /// <summary>
        /// Downloads all files from Keoghs FTP
        /// </summary>
        public void GetFilesFromKeoghs()
        {
            using (var session = new Session())
            {                
                // must set session log path before opening session else throws session already open InvalidOperationException
                session.SessionLogPath = _config.FtpLogFile;
                // Connect    
                session.Open(_sessionOptions);

                // download files but don't delete as will delete directory too!
                var transferResult = session.GetFiles(_config.KeoghsFtpPath, _config.MonitorFolderNetworkAddress, false,
                    _transferOptions);

                Debug.Assert(transferResult != null, "transferResult != null");
                Debug.Assert(transferResult.Transfers != null, "transferResult.Transfers != null");
                // Throw on any error                
                transferResult.Check();

                // Print results                
                foreach (TransferEventArgs transfer in transferResult.Transfers)
                {
                    session.RemoveFiles(session.EscapeFileMask(transfer.FileName));
                    Console.WriteLine("Retrieval of {0} succeeded, removed from server", transfer.FileName);
                }

                session.Close();                
            }            
        }

        /// <summary>
        /// Send files from folder to client
        /// </summary>
        public void SendFilesToClient()
        {
            using (var session = new Session())
            {
                // must set session log path before opening session else throws session already open InvalidOperationException
                session.SessionLogPath = _config.ClientFtpLogFile;
                // Connect   
                session.Open(_sessionOptions);          
                
                session.PutFiles(_config.MonitorFolderNetworkAddress + "\\*", _config.FtpPath, false, _transferOptions);

                session.Close();
            } 
        }
    }
}
