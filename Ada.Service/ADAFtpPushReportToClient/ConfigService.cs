﻿using System;
using System.Configuration;
using System.Diagnostics;

namespace ADAFtpPushReportToClient
{      
    /// <summary>
    /// Class to manage app.config settings
    /// </summary>
    public static class ConfigService
    {   
        /// <summary>
        /// Sets the config based on Client Name
        /// </summary>
        /// <param name="clientName">name of client to get config for</param>
        /// <returns><see cref="Config"/>populated with shared and client specific config</returns>
        public static Config Get(string clientName)
        {            
            if (string.IsNullOrEmpty(clientName))
                throw new ArgumentException("Value cannot be null or empty.", "clientName");

            switch (clientName.ToUpper())
            {
                case "DIRECTLINE":
                    return DlgConfig();
                default:
                    return null;
    
            }
        }

        private static Config DlgConfig()
        {
            var config = SharedConfig();

            Debug.Assert(config != null, "config != null");

            config.ClientName = "DirectLine";
            config.ClientSshHostKeyFingerprint = ConfigurationManager.AppSettings["DlgSshHostKeyFingerprint"];
            config.MonitorFolderNetworkAddress = ConfigurationManager.AppSettings["DlgMonitorFolderNetworkAddress"];
            config.ArchiveFolderNetworkAddress = ConfigurationManager.AppSettings["DlgArchiveFolderNetworkAddress"];
            config.ArchiveFolderDeletePeriodDays = Convert.ToInt32(ConfigurationManager.AppSettings["ArchiveFolderDeletePeriodDays"]);                
            config.FtpPath = ConfigurationManager.AppSettings["DlgFtpPathTo"];
            config.SshPrivateKeyPath = ConfigurationManager.AppSettings["SshPrivateKeyPath"];
            config.SshPrivateKeyPassphrase = ConfigurationManager.AppSettings["SshPrivateKeyPassphrase"];
            config.GetFileMask = ConfigurationManager.AppSettings["DlgGetFileMask"];
            config.ClientFtpLogFile = ConfigurationManager.AppSettings["FtpLogFile"];
            config.HostName = ConfigurationManager.AppSettings["DlgHostName"];
            config.UserName = ConfigurationManager.AppSettings["DlgUserName"];

            return config;
 	        
        }

        private static Config SharedConfig()
        {
            return new Config
            {
                KeoghsSMTP = ConfigurationManager.AppSettings["SMTP"],
                KeoghsSuccessRecipients = ConfigurationManager.AppSettings["SuccessRecipients"],
                KeoghsErrorRecipients = ConfigurationManager.AppSettings["ErrorRecipients"],
                KeoghsHostName = ConfigurationManager.AppSettings["KeoghsHostName"],                
                KeoghsFtpPath = ConfigurationManager.AppSettings["KeoghsFtpPath"],
                KeoghsUserName = ConfigurationManager.AppSettings["KeoghsUserName"],
                KeoghsPassword = ConfigurationManager.AppSettings["KeoghsPassword"],
                KeoghsSshHostKeyFingerprint = ConfigurationManager.AppSettings["KeoghsSshHostKeyFingerprint"],
                FtpLogFile = ConfigurationManager.AppSettings["FtpLogFile"],
                ShowErrors = Convert.ToBoolean(ConfigurationManager.AppSettings["ShowErrors"]),
                SendEmails = Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmails"]),
            };

        }
    }

    /// <summary>
    /// Config Entity
    /// </summary>
    public class Config
    {
        // Client Specific
        public string ClientName { get; set; }
        public string MonitorFolderNetworkAddress { get; set; }
        public string ArchiveFolderNetworkAddress { get; set; }
        public int ArchiveFolderDeletePeriodDays { get; set; }
        public string SshHostKeyFingerprint { get; set; }
        public string ClientSshHostKeyFingerprint { get; set; }
        public string ClientFtpLogFile { get; set; }
        public string SshPrivateKeyPath { get; set; }
        public string SshPrivateKeyPassphrase { get; set; }
        public string FtpPath { get; set; }
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        // Keoghs Specific
        public string KeoghsSshHostKeyFingerprint { get; set; }
        public bool SendEmails { get; set; }
        public string KeoghsSuccessRecipients { get; set; }
        public string KeoghsErrorRecipients { get; set; }
        // ReSharper disable once InconsistentNaming
        public string KeoghsSMTP { get; set; }        
        public string KeoghsFtpPath { get; set; }
        public string KeoghsHostName { get; set; }
        public string KeoghsUserName { get; set; }
        public string KeoghsPassword { get; set; }        
        public string GetFileMask { get; set; }
        public string FtpLogFile { get; set; }
        public bool ShowErrors { get; set; }        
    }    
}
