﻿using System;

namespace ADAFtpPushReportToClient
{
    class Program
    {
        /// <summary>
        /// Pull files from POC to Keoghs FTP or Keoghs FTP to another FTP
        /// </summary>
        /// <param name="args">
        /// args[0] expected to be ClientName e.g. DirectLine
        /// args[1] expected to be POCTOKEOGHSFTP or KEOGHSFTPTOCLIENTFTP
        /// </param>
        static void Main(string[] args)
        {
            // need 2 params passing in
            if (args != null && args.Length == 2)
            {
                var reportService = Factory.Create(args[0]);
                if (reportService == null) throw new ArgumentException("Cannot create Report Service from arg0: [" + args[0] + "]");

                if (args[1].Equals("POCTOKEOGHSFTP", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportService.ProcessFilesDirToKeoghsFTP();
                    reportService.TidyUp();
                }
                else if (args[1].Equals("KEOGHSFTPTOCLIENTFTP", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportService.ProcessFilesFTPToFTP();
                }
                else if (args[1].Equals("KEOGHSFTPTOCLIENTFTP1", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportService.ProcessFilesFTPToFTPStage1();
                }
                else if (args[1].Equals("KEOGHSFTPTOCLIENTFTP2", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportService.ProcessFilesFTPToFTPStage2();
                }
                else
                {
                    throw new ArgumentException("Unknown arg1: [" + args[1] + "]");
                }                
            }
            else
            {
                throw new ArgumentNullException("args");
            }
        }
    }
}
