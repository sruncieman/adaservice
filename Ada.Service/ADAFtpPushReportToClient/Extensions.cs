﻿using System;
using System.Text;

namespace ADAFtpPushReportToClient
{
    /// <summary>
    /// Shared Extension Methods
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Converts Exception to string, recusively
        /// </summary>
        /// <param name="ex"><see cref="Exception"/></param>
        /// <returns>String containing Exception</returns>
        public static string Output(this Exception ex)
        {
            if (ex == null) return string.Empty;

            var res = new StringBuilder();
            res.AppendFormat("Exception of type '{0}': {1}", ex.GetType().Name, ex.Message);
            res.AppendLine();

            if (!string.IsNullOrEmpty(ex.StackTrace))
            {
                res.AppendLine(ex.StackTrace);
            }

            if (ex.InnerException != null)
            {
                res.AppendLine(ex.InnerException.Output());
            }

            return res.ToString();
        }

    }
}