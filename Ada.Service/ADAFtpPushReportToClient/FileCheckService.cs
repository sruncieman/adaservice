﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Abstractions;
using System.Linq;

namespace ADAFtpPushReportToClient
{
    public interface IFileCheckService
    {
        string[] GetFileNamesInFolder(string path);
        bool DeleteFilesInFolderOlderThanXDays(string path, int days);
        bool MoveFile(string fromFileNamePath, string toFileNamePath);
    }

    /// <summary>
    /// File helper class
    /// </summary>
    public class FileCheckService : IFileCheckService
    {
        private readonly IFileSystem _fileSystem;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="fileSystem"><see cref="IFileSystem"/></param>
        public FileCheckService(IFileSystem fileSystem)
        {
            _fileSystem = fileSystem;
        }

        /// <summary>
        /// ctor
        /// </summary>
        public FileCheckService() : this (new FileSystem())
        {
            
        }
        
        /// <summary>
        /// Gets array of filenames
        /// </summary>
        /// <param name="path">Full path to folder</param>
        /// <returns>array of filenames with paths to process</returns>
        public string[] GetFileNamesInFolder(string path)
        {
            if (path == null) throw new ArgumentNullException("path");
            
            Debug.Assert(_fileSystem != null, "_fileSystem != null");
            Debug.Assert(_fileSystem.Directory != null, "_fileSystem.Directory != null");

            var files = _fileSystem.Directory.GetFiles(path);

            // If we find none, return
            if (files == null || !files.Any()) return null;

            // If ANY files in that list have been modified in the last "settleTime" minutes return.  The directory is not stable, files may still being
            // written.  Return now and wait for the next check
            return files.Any(x => x != null && _fileSystem.File.GetCreationTime(x).AddMinutes(1) > DateTime.Now) ? null : files;
        }

        /// <summary>
        /// delete all files in specified folder older than x days
        /// </summary>
        /// <param name="path">path to folder</param>
        /// <param name="days">number of days</param>
        /// <returns>true if successful</returns>
        public bool DeleteFilesInFolderOlderThanXDays(string path, int days)
        {
            if (path == null) throw new ArgumentNullException("path");
            if (days < 0) throw new ArgumentOutOfRangeException("days");
            
            if (days == 0) return true; // must be turned off

            Debug.Assert(_fileSystem != null, "_fileSystem != null");
            Debug.Assert(_fileSystem.Directory != null, "_fileSystem.Directory != null");
            Debug.Assert(_fileSystem.File != null, "_fileSystem.File != null");

            var files = _fileSystem.Directory.GetFiles(path);

            // If we find none, return
            if (files == null || !files.Any()) return true;
            
            var filesToDelete = files.Where(x => x != null && _fileSystem.File.GetCreationTime(x).AddDays(days) < DateTime.Now);            

            foreach (var fileToDelete in filesToDelete)
            {                
                _fileSystem.File.Delete(fileToDelete);
            }

            return true;
        }

        /// <summary>
        /// Move file to another folder
        /// </summary>
        /// <param name="fromFileNamePath">source file with path</param>
        /// <param name="toPath">destination path</param>
        /// <returns></returns>
        public bool MoveFile(string fromFileNamePath, string toPath)
        {
            if (fromFileNamePath == null) throw new ArgumentNullException("fromFileNamePath");
            if (toPath == null) throw new ArgumentNullException("toPath");

            Debug.Assert(_fileSystem != null, "_fileSystem != null");            
            Debug.Assert(_fileSystem.File != null, "_fileSystem.File != null");
            Debug.Assert(_fileSystem.Directory != null, "_fileSystem.Directory != null");

            if (!_fileSystem.File.Exists(fromFileNamePath)) throw new ArgumentException("fromFileNamePath not found");            
            if (!_fileSystem.Directory.Exists(toPath)) throw new ArgumentException("toPath not found");

            var toFileNamePath = toPath + "\\" + _fileSystem.Path.GetFileName(fromFileNamePath);

            if (_fileSystem.File.Exists(toFileNamePath)) // Check if it already exists
            {
                var path = _fileSystem.Path.GetDirectoryName(toFileNamePath);
                var filename = _fileSystem.Path.GetFileNameWithoutExtension(toFileNamePath);

                var rand = Path.GetRandomFileName().Substring(0,6);
                var ext = _fileSystem.Path.GetExtension(toFileNamePath);

                toFileNamePath = string.Format("{0}\\{1}{2}{3}", path, filename, rand, ext);
            }            

            _fileSystem.File.Move(fromFileNamePath, toFileNamePath);

            return true;
        }
        
    }
}
