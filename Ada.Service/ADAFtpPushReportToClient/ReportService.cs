﻿using System;
using System.Diagnostics;
using System.Net.Mail;
using WinSCP;

namespace ADAFtpPushReportToClient
{
    /// <summary>
    /// Report orchestration class
    /// </summary>
    public class ReportService
    {
        private readonly IFileCheckService _fileService;
        private readonly IFtpService _ftpService;
        private readonly ISmtpService _smtpService;        
        private readonly Config _config;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="fileService"><see cref="IFileCheckService"/></param>
        /// <param name="ftpService"><see cref="IFtpService"/></param>
        /// <param name="smtpService"><see cref="SmtpService"/></param>
        /// <param name="config"><see cref="Config"/></param>
        public ReportService(IFileCheckService fileService, IFtpService ftpService, ISmtpService smtpService, Config config)
        {
            if (fileService == null) throw new ArgumentNullException("fileService");
            if (ftpService == null) throw new ArgumentNullException("ftpService");
            if (smtpService == null) throw new ArgumentNullException("smtpService");
            if (config == null) throw new ArgumentNullException("config");

            _fileService = fileService;
            _ftpService = ftpService;
            _smtpService = smtpService;
            _config = config;
        }
        
        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// Pulls files from Poc to Keoghs FTP
        /// </summary>
        public void ProcessFilesDirToKeoghsFTP()
        {
            Debug.Assert(_ftpService != null, "_ftpService != null");

            try
            {
                var files = _fileService.GetFileNamesInFolder(_config.MonitorFolderNetworkAddress);

                if (files == null || files.Length <= 0) return;

                foreach (var fileNameAndPath in files)
                {                    
                    _ftpService.SendFileToKeoghs(fileNameAndPath);

                    _fileService.MoveFile(fileNameAndPath, _config.ArchiveFolderNetworkAddress);
                }

                SendEmail(true, null);                
            }
            catch (Exception ex)
            {
                SendEmail(false, ex);                    
            }
           
        }

        /// <summary>
        /// Delete old files
        /// </summary>
        public void TidyUp()
        {
            Debug.Assert(_fileService != null, "_fileService != null");
            try
            {                
                _fileService.DeleteFilesInFolderOlderThanXDays(_config.ArchiveFolderNetworkAddress, _config.ArchiveFolderDeletePeriodDays);
            }
            catch (Exception ex)
            {
                SendEmail(false, ex);                    
            }
        }

        /// <summary>
        /// Send notification emails
        /// </summary>
        /// <param name="result"><see cref="bool"/>changes method based on if result success or not</param>
        /// <param name="ex"><see cref="Exception"/>Optional Exception data if result not successful</param>
        public void SendEmail(bool result, Exception ex)
        {
            if (_config.SendEmails)
            {
                Debug.Assert(_config.KeoghsSuccessRecipients != null, "_config.SuccessRecipients != null");
                Debug.Assert(_config.KeoghsErrorRecipients != null, "_config.ErrorRecipients != null");

                Debug.Assert(_smtpService != null, "_smtpService != null");


                var mail = result ? new MailMessage("ADAClients@keoghs.co.uk", _config.KeoghsSuccessRecipients) : new MailMessage("ADAClients@keoghs.co.uk", _config.KeoghsErrorRecipients);

                if (result)
                {
                    mail.Subject = string.Format("{0} FTP Report Success.", _config.ClientName);
                    mail.Body = "FTP Report sync was successful." + Environment.NewLine + Environment.NewLine;
                }
                else
                {
                    mail.Subject = string.Format("{0} FTP Report Failure.", _config.ClientName);
                    mail.Body = "FTP Report sync was unsuccessful." + Environment.NewLine;
                    if (ex != null) mail.Body += ex.Output();
                }

                _smtpService.Send(mail);
            }
        }


        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// Pull files from Keoghs FTP to temp folder
        /// </summary>
        public void ProcessFilesFTPToFTPStage1()
        {
            Debug.Assert(_ftpService != null, "_ftpService != null");

            try
            {
                // Get files from Keoghs and stick in folder
                _ftpService.GetFilesFromKeoghs();               
            }
            catch (Exception ex)
            {
                if (_config.ShowErrors)
                {
                    throw;
                }
                SendEmail(false, ex);
            }
        }

        // ReSharper disable once InconsistentNaming
        // Push files from TempFolder to Client FTP
        public void ProcessFilesFTPToFTPStage2()
        {
            Debug.Assert(_ftpService != null, "_ftpService != null");

            try
            {
                var sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = _config.HostName,
                    UserName = _config.UserName,
                    SshHostKeyFingerprint = _config.ClientSshHostKeyFingerprint,

                    SshPrivateKeyPath = _config.SshPrivateKeyPath,
                    SshPrivateKeyPassphrase = _config.SshPrivateKeyPassphrase
                };

                var transferOptions = new TransferOptions
                {
                    TransferMode = TransferMode.Binary,
                    FileMask = _config.GetFileMask,
                    PreserveTimestamp = false,
                };

                transferOptions.ResumeSupport.State = TransferResumeSupportState.Off;

                var clientFtpService = new FtpService(_config, sessionOptions, transferOptions);

                clientFtpService.SendFilesToClient();

                var files = _fileService.GetFileNamesInFolder(_config.MonitorFolderNetworkAddress);

                if (files == null || files.Length <= 0) return;

                foreach (var fileNameAndPath in files)
                {
                    _fileService.MoveFile(fileNameAndPath, _config.ArchiveFolderNetworkAddress);
                }

                SendEmail(true, null);
            }
            catch (Exception ex)
            {
                if (_config.ShowErrors)
                {
                    throw;
                }
                SendEmail(false, ex);
            }
        }


        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// Pull files from Keoghs FTP to temp folder, push files from temp folder to Client FTP
        /// </summary>
        public void ProcessFilesFTPToFTP()
        {
            ProcessFilesFTPToFTPStage1();
            ProcessFilesFTPToFTPStage2();
        }
    }
}
