﻿using System;

namespace ADAFtpPushReportToClient
{
    /// <summary>
    /// Factory to create Report Service
    /// </summary>
    public static class Factory
    {
        /// <summary>
        /// Create a Report Service for a specified client
        /// </summary>
        /// <param name="clientName">Name of Client</param>
        /// <returns><see cref="ReportService"/></returns>
        public static ReportService Create(string clientName)
        {
            if (string.IsNullOrEmpty(clientName))
                throw new ArgumentException("Value cannot be null or empty.", "clientName");

            clientName = clientName.Replace("-", "");

            // Get details from configService

            var config = ConfigService.Get(clientName);

            if (config == null)
            {
                throw new ArgumentException("ClientName not recognised, cannot load config");
            }

            var fileService = new FileCheckService();
            var ftpService = new FtpService(config);
            var smtpService = new SmtpService(config);

            var svc = new ReportService(fileService, ftpService, smtpService, config);
            

            return svc;
        }        
    }
}
