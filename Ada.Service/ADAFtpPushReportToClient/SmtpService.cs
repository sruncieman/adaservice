﻿using System;
using System.Diagnostics;
using System.Net.Mail;

namespace ADAFtpPushReportToClient
{
    public interface ISmtpService
    {
        void Send(MailMessage mailMessage);
    }

    /// <summary>
    /// Smtp Service helper
    /// </summary>
    public class SmtpService : ISmtpService
    {
        private readonly SmtpClient _smtpService;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="config"><see cref="Config"/></param>
        public SmtpService(Config config)
        {
            if (config == null) throw new ArgumentNullException("config");
            Debug.Assert(config.KeoghsSMTP != null, "_config.SMTP != null");

            _smtpService = new SmtpClient
            {
                Port = 25,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = config.KeoghsSMTP
            };
        }

        /// <summary>
        /// Sends email message
        /// </summary>
        /// <param name="mailMessage"><see cref="MailMessage"/></param>
        public void Send(MailMessage mailMessage)
        {
            if (mailMessage == null) throw new ArgumentNullException("mailMessage");
            Debug.Assert(_smtpService != null, "_smtpService != null");

            _smtpService.Send(mailMessage);
        }
    }
}
