﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.ReportingService.SQLReportService;
using System.IO;


namespace MDA.ReportingService
{
    public class ReportServices
    {
        public byte[] RunReport(string reportId, string reportRdlPath)
        {
            ReportExecutionService rs = new ReportExecutionService();

            rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
            rs.Url = ConfigurationManager.AppSettings["ReportingServicePath"];

            // Render arguments
            byte[] result = null;
            string format = "pdf";
            //string historyID = null;
            string devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";

            // Prepare report parameter.
            ParameterValue[] parameters = new ParameterValue[1];
            parameters[0] = new ParameterValue();
            parameters[0].Name = "RiskClaim_Id";
            parameters[0].Value = reportId;
           
            string encoding;
            string mimeType;
            string extension;
            Warning[] warnings = null;

            string[] streamIDs = null;

            ExecutionInfo execInfo = new ExecutionInfo();
            ExecutionHeader execHeader = new ExecutionHeader();

            rs.ExecutionHeaderValue = execHeader;
            execInfo = rs.LoadReport("/ADALevelOneReport/V1/RPT_ADALevelOneReport", null);

            rs.SetExecutionParameters(parameters, "en-us");
            String SessionId = rs.ExecutionHeaderValue.ExecutionID;

            //MDA.DataService.DataServices.Incident_AgeRangeCount()
            
            result = rs.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamIDs);

            return result;
        }

        public byte[] RunBatchReport(string riskBatchId, int amber, int red, string reportRdlPath, string reportFormat)
        {
            ReportExecutionService rs = new ReportExecutionService();

            rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
            rs.Url = ConfigurationManager.AppSettings["ReportingServicePath"];

            // Render arguments
            byte[] result = null;
            //string format = format;
            //string historyID = null;
            string devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";

            // Prepare report parameter.
            ParameterValue[] parameters = new ParameterValue[3];
            parameters[0] = new ParameterValue();
            parameters[0].Name = "AmberClaim";
            parameters[0].Value = amber.ToString();

            parameters[1] = new ParameterValue();
            parameters[1].Name = "BatchID";
            parameters[1].Value = riskBatchId;

            parameters[2] = new ParameterValue();
            parameters[2].Name = "RedClaim";
            parameters[2].Value = red.ToString();

            string encoding;
            string mimeType;
            string extension;
            Warning[] warnings = null;

            string[] streamIDs = null;
            
            ExecutionInfo execInfo = new ExecutionInfo();
            ExecutionHeader execHeader = new ExecutionHeader();

            rs.ExecutionHeaderValue = execHeader;
            execInfo = rs.LoadReport("/ADABatchReport/V1/RPT_ADABatchReport", null);

            rs.SetExecutionParameters(parameters, "en-us");
            String SessionId = rs.ExecutionHeaderValue.ExecutionID;

            result = rs.Render(reportFormat, devInfo, out extension, out encoding, out mimeType, out warnings, out streamIDs);

            return result;
        }
    }
}
