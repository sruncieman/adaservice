declare @redClaim int = 400
declare @amberClaim int = 250
declare @greenClaim int = 75
declare @batchId int = 1

SELECT *,
(CASE WHEN a.RAG_Status = 'RED' THEN a.Reserve END) AS Red_Reserve
,(CASE WHEN a.RAG_Status = 'RED' THEN a.PaymentsToDate END) AS Red_Payments
,(CASE WHEN a.RAG_Status = 'AMBER' THEN a.Reserve END) AS Amber_Reserve
,(CASE WHEN a.RAG_Status = 'AMBER' THEN a.PaymentsToDate END) AS Amber_Payments
 
 FROM (
		SELECT  rb.[Id] as BatchRefId
			  ,rc.Id as ClaimId
			  ,rc.BaseRiskClaim_Id
			  ,rc.SourceReference
			  ,rb.[BatchReference]
			  ,rc.[CreatedDate]
			  ,rb.[CreatedBy]
			  ,i.Reserve
			  ,i.PaymentsToDate
			  ,(CASE WHEN rc.ClaimStatus = 0 THEN 'Bad'  WHEN rc.ClaimStatus = 1 THEN 'Created'  WHEN rc.ClaimStatus = 2 THEN 'Loaded' WHEN rc.ClaimStatus = 3 THEN 'Scored' END) AS [Claim Status]
			  ,(CASE WHEN rcr.TotalScore >= @redClaim  THEN 'RED' WHEN rcr.TotalScore < @redClaim  and rcr.TotalScore >= @amberClaim then 'AMBER' ELSE 'GREEN' END) AS RAG_Status
			  ,(select '1') as [Received]
			  ,(CASE WHEN rc.ClaimStatus = 3 THEN 1 ELSE NULL END) as Processed
		   
		  FROM [MDA].[dbo].[RiskBatch] rb
		  inner JOIN [MDA].[dbo].RiskClaim rc on rb.Id = rc.RiskBatch_Id
		  inner join [MDA].[dbo].Incident i on rc.Incident_Id = i.Id
		  inner join [MDA].dbo.RiskClaimRun rcr on rcr.RiskClaim_Id = rc.Id
		  where rb.Id = @batchId
  ) a
  order by a.SourceReference, a.CreatedDate
  SELECT *,
(CASE WHEN a.RiskRating = 'RED' THEN a.Reserve END) AS Red_Reserve
,(CASE WHEN a.RiskRating = 'RED' THEN a.Payment END) AS Red_Payments
,(CASE WHEN a.RiskRating = 'AMBER' THEN a.Reserve END) AS Amber_Reserve
,(CASE WHEN a.RiskRating = 'AMBER' THEN a.Payment END) AS Amber_Payments

from(
 select 
  rc.Id as ClaimId
 ,rc.BaseRiskClaim_Id
 ,rc.ClientClaimRefNumber as [Claim Number]
 ,rc.[CreatedDate]
 ,rb.ClientBatchReference as [Client Batch Reference] 
 ,i.IncidentDate as [Incident Date]
 ,rcr.TotalKeyAttractorCount
 ,i.Reserve as Reserve
 ,i.PaymentsToDate as Payment
 ,rc.CreatedDate as [Upload Date/Time]
,(select 1) as [Received]
 ,(CASE WHEN rc.ClaimStatus = 3 THEN 1 ELSE NULL END) as Processed
 ,(CASE WHEN rcr.TotalScore >= @redClaim  THEN 'RED' WHEN rcr.TotalScore < @redClaim  and rcr.TotalScore >= @amberClaim then 'AMBER' ELSE 'GREEN' END) AS RiskRating
 ,rc.LatestVersion
 ,ff.*
 FROM [MDA].[dbo].[RiskBatch] rb
 inner JOIN [MDA].[dbo].RiskClaim rc on rb.Id = rc.RiskBatch_Id
 inner join [MDA].[dbo].Incident i on rc.Incident_Id = i.Id
 inner join [MDA].dbo.RiskClaimRun rcr on rcr.RiskClaim_Id = rc.Id
 --left join #TempTable tt on tt.ID = rc.Id
 left join[MDA].dbo.fn_GetMsgs(@batchId) ff 
 on ff.Id = rc.Id

 where rb.Id = @batchId
)a
