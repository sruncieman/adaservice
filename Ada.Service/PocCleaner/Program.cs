﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PocCleaner
{
    public class Program
    {
        static void Main(string[] args)
        {
            string dirPath = ConfigurationManager.AppSettings["TracePath"];
            int retentionPeriod = Convert.ToInt32(ConfigurationManager.AppSettings["ArchiveRetentionPeriod"]);
            DateTime retentionDate = DateTime.Now.AddDays(-retentionPeriod);

            try
            {  
                string[] filePaths = Directory.GetFiles(@dirPath,"*.*", SearchOption.AllDirectories);

                string[] directoryPaths = GetDirectories(dirPath);

                DeleteFiles(retentionDate, filePaths);
                
                int count = 0;
                
                while (GetDirectories(dirPath).Any() && count<100)
                {
                    DeleteEmptyDirectories(directoryPaths);
                    count++;
                    Console.WriteLine(count);
                }
            }

            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
        }

        private static string[] GetDirectories(string dirPath)
        {
            string[] directoryPaths = Directory.GetDirectories(@dirPath, "*.*", SearchOption.AllDirectories);
            return directoryPaths;
        }

        private static void DeleteFiles(DateTime retentionDate, string[] filePaths)
        {
            foreach (var filePath in filePaths)
            {
                var fileCreated = File.GetCreationTime(filePath);

                if ((fileCreated < retentionDate) && Path.GetFileNameWithoutExtension(filePath) != "ADA-Trace")
                {
                    File.SetAttributes(filePath, FileAttributes.Normal);
                    File.Delete(filePath);
                }
            }
        }

        private static void DeleteEmptyDirectories(string[] directoryPaths)
        {
            foreach (var directoryPath in directoryPaths)
            {
                if (Directory.Exists(directoryPath))
                {
                    if (IsDirectoryEmpty(directoryPath))
                        Directory.Delete(directoryPath);
                }
            }
        }

        public static bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }

    }
}
