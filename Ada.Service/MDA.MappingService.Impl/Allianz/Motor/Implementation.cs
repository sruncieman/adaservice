﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.MappingService.Interface;
using MDA.Common;
using MDA.Common.Server;
using System.Data.OleDb;

namespace MDA.MappingService.Allianz.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        private string tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];

        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking, Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn, out ProcessingResults processingResults)
        {
            string clientFolder;

            GetFilePath(out clientFolder, ctx.RiskClientId);

            processingResults = new ProcessingResults("Mapping");

            try
            {
                new Mapper().RetrieveMotorClaimBatch(ctx, fs, clientFolder, statusTracking, ProcessClaimFn, processingResults);
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping Allianz file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void GetFilePath(out string clientFolder, int clientId)
        {
            clientFolder = tempFolder + "\\" + clientId;

            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
        }

        private string ConvertExcelToCsv(string excelFilePath, int worksheetNumber = 1)
        {
            if (!File.Exists(excelFilePath)) throw new FileNotFoundException(excelFilePath);
            //if (File.Exists(csvOutputFile)) throw new ArgumentException("File exists: " + csvOutputFile);

            // connection string
            var cnnStr = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;IMEX=1;HDR=NO\"", excelFilePath);
            var cnn = new OleDbConnection(cnnStr);

            // get schema, then data
            var dt = new DataTable();
            try
            {
                cnn.Open();
                var schemaTable = cnn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (schemaTable.Rows.Count < worksheetNumber) throw new ArgumentException("The worksheet number provided cannot be found in the spreadsheet");
                string worksheet = schemaTable.Rows[worksheetNumber - 1]["table_name"].ToString().Replace("'", "");
                string sql = String.Format("select * from [{0}]", worksheet);
                var da = new OleDbDataAdapter(sql, cnn);
                da.Fill(dt);
            }
            catch (Exception e)
            {
                // ???
                throw e;
            }
            finally
            {
                // free resources
                cnn.Close();
            }

            string date;
            date = DateTime.Now.ToString("dd/MM/yy HH:mm:ss");
            date = date.Replace("/", "");
            date = date.Replace(":", "");
            date = date.Replace(" ", "");

            string csvOutputFile = tempFolder + "\\" + date + ".csv";

            // write out CSV data
            using (var wtr = new StreamWriter(csvOutputFile))
            {
                foreach (DataRow row in dt.Rows)
                {
                    bool firstLine = true;
                    foreach (DataColumn col in dt.Columns)
                    {
                        if (!firstLine) { wtr.Write(","); } else { firstLine = false; }
                        var data = row[col.ColumnName].ToString().Replace("\"", "\"\"");
                        wtr.Write(String.Format("\"{0}\"", data));
                    }
                    wtr.WriteLine();
                }
            }

            return csvOutputFile;
        }

        public override void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard, int settleTime, int batchSize, string postOp, string archiveFolder, Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {
            // Create structure to hold our new batch of claims
            List<object> batch = new List<object>();

            int batchCount = 0;
            string firstFileName = null;

            // Get a list of the files in the folderPath filtered by the wildCard. Search the top level only (don't go into child directories)
            string[] files = Directory.GetFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

            // If we find none, return
            if (files.Count() == 0) return;

            // If ANY files in that list have been modified in the last "settleTime" minutes return.  The directory is not stable, files may still being
            // written.  Return now and wait for the next check
            if (files.Where(x => File.GetCreationTime(x).AddMinutes(settleTime) > DateTime.Now).Any())
                return;

            // If we get here we have files to process
            foreach (var file in files)
            {
                // Create a filename to use for this batch.  We will just use the first file name in this case
                if (firstFileName == null)
                    firstFileName = Path.GetFileName(file);

                if (firstFileName.Contains("Erroneous"))
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                    return;
                }

                //ConvertExcelToCsv(file, @"D:\CSV\New.csv", 1);

                // Add this new claim to the batch
                batch.Add(ConvertExcelToCsv(file, 1));

                #region If Batch full process it

                if (batch.Count >= batchSize)
                {
                    batchCount++;

                    string fileName = firstFileName;
                    string mimeType = "csv";

                    string clientName = "";
                    string date;

                    switch (ctx.RiskClientId)
                    {
                        case 9:
                            clientName = "ALLI";
                            break;
                        case 54:
                            clientName = "LIBE";
                            break;
                    }


                    date = DateTime.Now.ToString("dd/MM/yy HH:mm:ss");
                    date = date.Replace("/", "");
                    date = date.Replace(":", "");
                    date = date.Replace(" ", "");

                    string batchRef = clientName + date;
                    string fileToStream = string.Empty;

                    if(clientName=="ALLI")
                        fileToStream = file;
                    else
                        fileToStream = batch[0].ToString();


                    using (FileStream fileStream = File.OpenRead(fileToStream))
                    {
                        MemoryStream stream = new MemoryStream();
                        stream.SetLength(fileStream.Length);
                        fileStream.Read(stream.GetBuffer(), 0, (int)fileStream.Length);

                        var csvFileNameArray = batch[0].ToString().Split('\\');
                        var csvFileName = csvFileNameArray[3];

                        #region Process the batch (call the callback function)
                        processClientBatch(ctx, stream, (clientName == "ALLI" ? fileName : csvFileName), mimeType, "Motor", batchRef);
                        #endregion
                    }


                    // Setup for a new batch and go round the loop again
                    batch.Clear();
                    firstFileName = null;
                }




                #endregion

                #region Process all the options we invented for our postOp functionality

                // We are passed the string we entered into config.  Now we have to code it!
                // In this case we have implemented RENAME, DELETE, ARCHIVE
                // ARCHIVE checks and (if needed) creates the destination folder. It also checks if the file already exists
                // in the destination folder (which would break the MOVE) and (if needed) adds a random string to make it unique
                switch (postOp.ToUpper())
                {
                    case "RENAME":
                        File.Move(file, file + ".old");
                        break;

                    case "DELETE":
                        File.Delete(file);
                        break;

                    case "ARCHIVE":
                        if (!Directory.Exists(archiveFolder))
                            Directory.CreateDirectory(archiveFolder);   // Create destination if needed

                        string fname = archiveFolder + "\\" + Path.GetFileName(file);  // build dest path

                        if (File.Exists(fname))  // Check if it already exists
                        {
                            fname += "." + Path.GetRandomFileName();  // Adds a random string to end of filename
                        }

                        File.Move(file, fname);
                        break;
                }
                #endregion

            }

        }
    }
}
