﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.Allianz.Model
{
    [IgnoreFirst(8)]
    [DelimitedRecord(",")]
    public class LibertyClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PortalApplicationId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AcceptedDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String SeqNbr;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String LineOfBusiness;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Branch;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank10;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Team;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank12;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String IncidentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank15;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredVehicleRegistration;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank17;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolderFirstName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolderLastName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank20;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank21;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriverFirstName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriverLastName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriverAddress;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriverPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantInvolvement;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantVehicleRegistion;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantFirstName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantLastName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantAddress;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantPostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantPhoneNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantEmail;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantDoB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NINumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantSolicitor;

    }

    [IgnoreFirst(7)]
    [DelimitedRecord(",")]
    public class AllianzClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PortalApplicationId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AcceptedDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank6 ;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String SeqNbr;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String LineOfBusiness;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Branch;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank10;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Team;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String IncidentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredVehicleRegistration;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolderFirstName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PolicyHolderLastName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank20;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Blank21;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriverFirstName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriverLastName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriverAddress;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String InsuredDriverPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantInvolvement;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantVehicleRegistion;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantFirstName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantLastName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantAddress;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantPostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantPhoneNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantEmail;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantDoB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NINumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantSolicitor;

    }
}
