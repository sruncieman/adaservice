﻿using MDA.Common;
using MDA.MappingService.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Admiral.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        private string _tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];
        public void ConvertToClaimBatch(Common.Server.CurrentContext ctx, System.IO.Stream fs, object statusTracking, Func<Common.Server.CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn, out Common.ProcessingResults processingResults)
        {
            string clientFolder;

            GetFilePath(out clientFolder, ctx.RiskClientId);

            processingResults = new ProcessingResults("Mapping");

            var defaultDataAddressForClient = new MDA.RiskService.RiskServices(ctx).GetRiskDefaultDataAddressForClient(ctx.RiskClientId);
            var defaultDataPeopleForClient = new MDA.RiskService.RiskServices(ctx).GetRiskDefaultDataPeopleForClient(ctx.RiskClientId);

            try
            {
                MDA.MappingService.Admiral.Motor.Mapper mapper = new MDA.MappingService.Admiral.Motor.Mapper(fs, clientFolder, ctx, ProcessClaimFn, statusTracking);

                mapper.AssignFiles();
                mapper.InitialiseFileHelperEngines();
                mapper.PopulateFileHelperEngines();
                mapper.RetrieveDistinctClaims();
                mapper.Translate();
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping Admiral file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void GetFilePath(out string clientFolder, int clientId)
        {
            clientFolder = _tempFolder + "\\" + clientId;

            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
        }
    
    }
}
