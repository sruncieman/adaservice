﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.Admiral.Motor.Model
{
    public class CustomDateConverter : ConverterBase
    {
        public override object StringToField(string value)
        {
            DateTime dt;
            value = value.TrimEnd();
            if (DateTime.TryParseExact(value, "dd/MM/yyyy", null, DateTimeStyles.None, out dt))
                return dt;

            return null;
        }
    }
}
