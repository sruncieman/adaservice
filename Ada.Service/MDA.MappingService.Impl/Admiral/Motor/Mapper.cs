﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Common.Server;
using MDA.MappingService.Admiral.Motor.Model;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace MDA.MappingService.Admiral.Motor
{
    public class Mapper : IMapper
    {
        private Stream _fs;
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        private object statusTracking;
        private static bool _boolDebug = true;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        public bool BoolDebug { get; set; }
        public string ClaimFile { get; set; }
        public string FilePath { get; set; }
        public string FolderPath { get; set; }

        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private StreamReader _claimDataFileStreamReader;
        private List<string> _uniqueClaimNumberList;

        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking)
        {
            this._fs = fs;
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _uniqueClaimNumberList = new List<string>();
        }

        public void AssignFiles()
        {
            if (_fs == null)
            {
                FolderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Admiral\Data\";
                ClaimFile = FolderPath + @"\Admiral - Keogh Case Examples.csv";
            }
            else
            {
                try
                {
                    _claimDataFileStreamReader = new StreamReader(_fs);
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in creating csv file: " + ex);
                }
            }
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {
                if (_fs == null)
                {
                    _coreClaimData = _coreClaimDataEngine.ReadFile(ClaimFile) as ClaimData[];
                }
                else
                {
                    _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
                }
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            throw new NotImplementedException();
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers
                _uniqueClaimNumberList = _coreClaimData.Where(x => x.ClaimNum != "").Select(x => x.ClaimNum).Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }
        }

        public void Translate()
        {
            int _skipValue = (ConfigurationManager.AppSettings["AdmiralSkipNum"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["AdmiralSkipNum"]) : 0);
            int _takeValue = (ConfigurationManager.AppSettings["AdmiralTakeNum"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["AdmiralTakeNum"]) : 500000);

            foreach (var claim in _uniqueClaimNumberList.Skip(_skipValue).Take(_takeValue))
            {
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _coreClaimData.Where(x => x.ClaimNum == claim).FirstOrDefault();

                if (uniqueClaim != null)
                {
                    #region Claim

                    try
                    {
                        if (!string.IsNullOrEmpty(uniqueClaim.ClaimNum))
                            motorClaim.ClaimNumber = uniqueClaim.ClaimNum;

                        DateTime incidentDate = Convert.ToDateTime(uniqueClaim.AccidentDate);
                        motorClaim.IncidentDate = incidentDate;
                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion Claim

                    #region ClaimInfo

                    try
                    {
                        if (!string.IsNullOrEmpty(uniqueClaim.AmountClaimed))
                            motorClaim.ExtraClaimInfo.Reserve = Convert.ToDecimal(uniqueClaim.AmountClaimed);

                        if (!string.IsNullOrEmpty(uniqueClaim.AmountPaid))
                            motorClaim.ExtraClaimInfo.PaymentsToDate = Convert.ToDecimal(uniqueClaim.AmountPaid);
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion ClaimInfo

                    #region Policy

                    motorClaim.Policy.Insurer = "Admiral";

                    #endregion Policy

                    #region Insured Vehicle

                    try
                    {
                        PipelineVehicle insuredVehicle = new PipelineVehicle();

                        if (!string.IsNullOrEmpty(uniqueClaim.PH_Reg))
                        {
                            insuredVehicle.VehicleRegistration = uniqueClaim.PH_Reg;
                        }
                        insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                        PipelinePerson insuredPerson = new PipelinePerson();
                        insuredPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        insuredPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        string[] names = uniqueClaim.PH_Name.Split(' ');
                        insuredPerson.FirstName = names[0];
                        if (names.Count() == 2)
                        {
                            insuredPerson.LastName = names[1];
                        }
                        if (names.Count() == 3)
                        {
                            insuredPerson.MiddleName = names[1];
                            insuredPerson.LastName = names[2];
                        }

                        PipelineAddress insuredAddress = new PipelineAddress();
                        if (!string.IsNullOrEmpty(uniqueClaim.PH_Address1))
                            insuredAddress.Street = uniqueClaim.PH_Address1;

                        if (!string.IsNullOrEmpty(uniqueClaim.PH_Address2))
                            insuredAddress.Locality = uniqueClaim.PH_Address2;

                        if (!string.IsNullOrEmpty(uniqueClaim.PH_Address3))
                            insuredAddress.Town = uniqueClaim.PH_Address3;

                        if (!string.IsNullOrEmpty(uniqueClaim.PH_Postcode))
                            insuredAddress.PostCode = uniqueClaim.PH_Postcode;

                        insuredPerson.Addresses.Add(insuredAddress);
                        insuredVehicle.People.Add(insuredPerson);

                        motorClaim.Vehicles.Add(insuredVehicle);
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping insured vehicle information: " + ex);
                    }

                    #endregion Insured Vehicle

                    #region TP Vehicle
                    try
                    {
                        PipelineVehicle tpVehicle = new PipelineVehicle();

                        if (!string.IsNullOrEmpty(uniqueClaim.TP_Reg))
                        {
                            tpVehicle.VehicleRegistration = uniqueClaim.TP_Reg;
                        }
                        tpVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                      


                        PipelinePerson tpPerson = new PipelinePerson();
                        tpPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                        tpPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;


                        PipelineOrganisation chOrg = new PipelineOrganisation();
                        chOrg.OrganisationType_Id = (int)OrganisationType.AccidentManagement;
                        chOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.AccidentManagement;

                        if (uniqueClaim.CompanyName.ToUpper() == "A HIRE COMPANY NOT LISTED HERE")
                            chOrg.OrganisationName = uniqueClaim.Company;
                        else
                            chOrg.OrganisationName = uniqueClaim.CompanyName;

                        PipelineOrganisationVehicle hireVehicle = new PipelineOrganisationVehicle();
                        hireVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyHireVehicle;
                        if (!string.IsNullOrEmpty(uniqueClaim.VehicleReg))
                            hireVehicle.VehicleRegistration = uniqueClaim.VehicleReg;
                        
                        chOrg.Vehicles.Add(hireVehicle);

                        tpPerson.Organisations.Add(chOrg);


                        if (!string.IsNullOrEmpty(uniqueClaim.Title))
                            tpPerson.Salutation_Id = (int)SalutationHelper.GetSalutation(uniqueClaim.Title);

                        if (!string.IsNullOrEmpty(uniqueClaim.TP_Forename))
                            tpPerson.FirstName = uniqueClaim.TP_Forename;

                        if (!string.IsNullOrEmpty(uniqueClaim.TP_Surname))
                            tpPerson.LastName = uniqueClaim.TP_Surname;

                        PipelineAddress tpAddress = new PipelineAddress();
                        if (!string.IsNullOrEmpty(uniqueClaim.TP_Address1))
                            tpAddress.Street = uniqueClaim.TP_Address1;

                        if (!string.IsNullOrEmpty(uniqueClaim.TP_Address2))
                            tpAddress.Locality = uniqueClaim.TP_Address2;

                        if (!string.IsNullOrEmpty(uniqueClaim.TP_Address3))
                            tpAddress.Town = uniqueClaim.TP_Address3;

                        if (!string.IsNullOrEmpty(uniqueClaim.TP_Postcode))
                            tpAddress.PostCode = uniqueClaim.TP_Postcode;

                        tpPerson.Addresses.Add(tpAddress);
                        tpVehicle.People.Add(tpPerson);

                        motorClaim.Vehicles.Add(tpVehicle);


                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping tp vehicle information: " + ex);
                    }


                    #endregion

                    //if (v.Entity_Type.Contains("INV"))
                    //{
                    //    vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                    //}

                    //if (v.Entity_Type.Contains("TPV"))
                    //{
                    //    vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                    //}
                }

                if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;
            }
        }
    }
}