﻿using System;
using System.Globalization;
using FileHelpers;

namespace MDA.MappingService.Impl.Mulsanne.Motor.Model
{
    public class CustomDateConvertor : ConverterBase
    {
        public override object StringToField(string value)
        {
            DateTime dt;

            value = value.Replace("\"", "");

            if (DateTime.TryParseExact(value, "dd/MM/yyyy", null, DateTimeStyles.None, out dt))
                return dt;
                     

            if (DateTime.TryParseExact(value, "dd/MM/yyyy hh:mm:ss", null, DateTimeStyles.None, out dt))
                return dt;

            // Mulsanne Mapping error returning: 1899-12-30 on T.PARTY_CLMNT_DOB field
            if (!DateTime.TryParseExact(value, "yyyy-mm-dd", null, DateTimeStyles.None, out dt)) return null;
            if (dt.Year == 1899 && dt.Month == 12 & dt.Day == 30)
            {
                return null;
            }

            return dt;
        }
    }
}
