﻿using System;
using FileHelpers;
// ReSharper disable InconsistentNaming

namespace MDA.MappingService.Impl.Mulsanne.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord("|")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNo;

        //Not mapped
        [FieldValueDiscarded]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantId;
        
        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? LossDate;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? NotifiedDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IncidentType;

        // Not mapped
        [FieldValueDiscarded]
        [FieldTrim(FileHelpers.TrimMode.Both, '"')]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IncidentDescription;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public decimal? CurrentReserve;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public decimal? PaidToDate;
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Status;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Insurer;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyId;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? RiskDate;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? Expiry;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Title;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_NameSur;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_NameFore;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PH_DOB;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_phoneday;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_address1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_Address2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PH_P_Code;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRIMARY_CLMNT_VEH_Reg;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRIMARY_CLMNT_VEH_Make;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRIMARY_CLMNT_VEH_Model;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(SalutationConvertor))]
        public string PRIMARY_CLMNT_TITLE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRIMARY_CLMNT_Forename;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRIMARY_CLMNT_Surname;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]        
        public DateTime? PRIMARY_CLMNT_DOB;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRIMARY_CLMNT_Phone;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRIMARY_CLMNT_Email;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRIMARY_CLMNT_ADD_1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRIMARY_CLMNT_ADD_2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRIMARY_CLMNT_TOWN;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRIMARY_CLMNT_PCODE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string T_PARTY_CLMNT__REG;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)] 
        public string T_PARTY_VehicleMake;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string T_PARTY_VehicleModel;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string T_PARTY_CLMNT_TITLE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string T_PARTY_CLMNT_NameFore;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string T_PARTY_CLMNT_NameSur;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? T_PARTY_CLMNT_DOB;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IsPrimary_Claimant;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimant;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Credit_Hire_1_SP;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Credit_Hire_1_ADD1_SP;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Credit_Hire_1_ADD2_SP;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Credit_Hire_1_PCode_SP;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Credit_Hire_2_SP;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Credit_Hire_1_IP;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Clmnt_Solicitor_IP;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Clmnt_Sol_Add1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Clmnt_Sol_Add2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Clmnt_Sol_Town;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Clmnt_Sol_Pcode;
    }
}
