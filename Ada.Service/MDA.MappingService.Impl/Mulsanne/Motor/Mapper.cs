﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using MDA.MappingService.Impl.Mulsanne.Motor.Model;

namespace MDA.MappingService.Impl.Mulsanne.Motor
{
    public class Mapper : IMapper
        {
        private readonly Stream _fs;
        private StreamReader _claimDataFileStreamReader;
        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private readonly Func<CurrentContext, IPipelineClaim, object, int> _processClaimFn;
        public string ClientFolder { get; set; }
        public string ClaimFile { get; set; }
        public CurrentContext Ctx { get; set; }
        private readonly object _statusTracking;
        private List<string> _uniqueClaimNumberList;
        private List<string> _claimsToIgnore = new List<string>();


        public Mapper(Stream fs, string clientFolder, CurrentContext ctx,
            Func<CurrentContext, IPipelineClaim, object, int> processClaimFn, object statusTracking)
        {
            _fs = fs;
            ClientFolder = clientFolder;
            Ctx = ctx;
            _processClaimFn = processClaimFn;
            _statusTracking = statusTracking;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            _uniqueClaimNumberList = new List<string>();            
        }

        public void AssignFiles()
        {
            _claimDataFileStreamReader = new StreamReader(_fs);
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {
                _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            throw new NotImplementedException();
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers

                DateTime skipDate;

                var skipdateValue = ConfigurationManager.AppSettings["MulsanneSkipDate"];
                
                if (!string.IsNullOrEmpty(skipdateValue))
                {
                    if (DateTime.TryParseExact(skipdateValue, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out skipDate))
                    {
                        _uniqueClaimNumberList = _coreClaimData.Where(x => x.ClaimNo != "" && 
                            x.LossDate.HasValue && 
                            x.LossDate.Value >= skipDate)
                            .Select(x => x.ClaimNo).AsParallel().Distinct().ToList();
                        return;
                    }
                }

                #region IgnoreList                
                // list of claim numbers from the pilot to ignore in live                
                _claimsToIgnore.Add("2016070664");
                _claimsToIgnore.Add("2015070617");
                _claimsToIgnore.Add("2016070668");
                _claimsToIgnore.Add("2016070727");
                _claimsToIgnore.Add("2016070725");
                _claimsToIgnore.Add("2015070569");
                _claimsToIgnore.Add("2016070442");
                _claimsToIgnore.Add("2015070344");
                _claimsToIgnore.Add("2015070208");
                _claimsToIgnore.Add("2016070516");
                _claimsToIgnore.Add("2016070174");
                _claimsToIgnore.Add("2015070042");
                _claimsToIgnore.Add("2016069997");
                _claimsToIgnore.Add("2015069735");
                _claimsToIgnore.Add("2016069657");
                _claimsToIgnore.Add("2016070015");
                _claimsToIgnore.Add("2016070226");
                _claimsToIgnore.Add("2016069508");
                _claimsToIgnore.Add("2015069456");
                _claimsToIgnore.Add("2015069205");
                _claimsToIgnore.Add("2016070060");
                _claimsToIgnore.Add("2015069154");
                _claimsToIgnore.Add("2015069041");
                _claimsToIgnore.Add("2015068803");
                _claimsToIgnore.Add("2015068601");
                _claimsToIgnore.Add("2016068585");
                _claimsToIgnore.Add("2015068564");
                _claimsToIgnore.Add("2015068555");
                _claimsToIgnore.Add("2015068673");
                _claimsToIgnore.Add("2015067954");
                _claimsToIgnore.Add("2015068208");
                _claimsToIgnore.Add("2016067736");
                _claimsToIgnore.Add("2015063906");
                _claimsToIgnore.Add("2015064586");
                _claimsToIgnore.Add("2015064498");
                _claimsToIgnore.Add("2015063443");
                _claimsToIgnore.Add("2015063973");
                _claimsToIgnore.Add("2015063231");
                _claimsToIgnore.Add("2015063252");
                _claimsToIgnore.Add("2014062250");
                _claimsToIgnore.Add("2015062200");
                _claimsToIgnore.Add("2015061947");
                _claimsToIgnore.Add("2015061709");
                _claimsToIgnore.Add("2015061427");
                _claimsToIgnore.Add("2014061396");
                _claimsToIgnore.Add("2014062608");
                _claimsToIgnore.Add("2015060571");
                _claimsToIgnore.Add("2015059994");
                _claimsToIgnore.Add("2015059711");
                _claimsToIgnore.Add("2015059324");
                _claimsToIgnore.Add("2015058731");
                _claimsToIgnore.Add("2015057960");
                _claimsToIgnore.Add("2015056921");
                _claimsToIgnore.Add("2015050783");
                _claimsToIgnore.Add("2014040119");
                _claimsToIgnore.Add("2016070824");
                _claimsToIgnore.Add("2016070993");
                _claimsToIgnore.Add("2016068143");
                _claimsToIgnore.Add("2015066991");
                _claimsToIgnore.Add("2015066552");
                _claimsToIgnore.Add("2016065861");
                _claimsToIgnore.Add("2014046095");
                _claimsToIgnore.Add("2014046095");
                _claimsToIgnore.Add("2016070226");
                _claimsToIgnore.Add("2015066162");
                _claimsToIgnore.Add("2015070617");
                _claimsToIgnore.Add("2016067940");
                _claimsToIgnore.Add("2016070668");
                _claimsToIgnore.Add("2015067931");
                _claimsToIgnore.Add("2015066552");
                _claimsToIgnore.Add("2015068564");
                _claimsToIgnore.Add("2015069154");
                _claimsToIgnore.Add("2016070993");
                _claimsToIgnore.Add("2016070727");
                _claimsToIgnore.Add("2016069997");
                _claimsToIgnore.Add("2016069822");
                _claimsToIgnore.Add("2016071406");
                _claimsToIgnore.Add("2015063252");
                _claimsToIgnore.Add("2015059324");
                _claimsToIgnore.Add("2016071710");
                _claimsToIgnore.Add("2016071611");
                _claimsToIgnore.Add("2016071763");
                _claimsToIgnore.Add("2016070849");
                _claimsToIgnore.Add("2016070802");
                _claimsToIgnore.Add("2016071184");
                _claimsToIgnore.Add("2016069589");
                _claimsToIgnore.Add("2015068530");
                _claimsToIgnore.Add("2015071160");
                _claimsToIgnore.Add("2015067764");
                _claimsToIgnore.Add("2015066073");
                _claimsToIgnore.Add("2015071510");
                _claimsToIgnore.Add("2015054304");
                _claimsToIgnore.Add("2014053611");
                _claimsToIgnore.Add("2014051020");
                _claimsToIgnore.Add("2014035225");
                _claimsToIgnore.Add("2016072144");
                #endregion                
                _uniqueClaimNumberList = _coreClaimData.Where(x => x.ClaimNo != ""
                    && !(_claimsToIgnore.Contains(x.ClaimNo))
                    ).Select(x => x.ClaimNo).AsParallel().Distinct().ToList();

            }
            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }
        }

        public void Translate()
        {
            var skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["MulsanneSkipNum"]);
            var takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["MulsanneTakeNum"]);

            foreach (var claim in _uniqueClaimNumberList.Skip(skipValue).Take(takeValue))
            {
                var motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _coreClaimData.FirstOrDefault(x => x.ClaimNo == claim);

                if (uniqueClaim != null)
                {
                    #region Claim

                    try
                    {
                        if (IsValid(uniqueClaim.ClaimNo))
                            motorClaim.ClaimNumber = uniqueClaim.ClaimNo;

                        if (uniqueClaim.LossDate != null)
                            motorClaim.IncidentDate = uniqueClaim.LossDate.Value;

                        motorClaim.ClaimType_Id = (int) ClaimType.Motor;
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim: " + ex);
                    }

                    #endregion Claim

                    #region ClaimInfo
                    // Get all records in import linked to this Claim Number
                    var claimInfo = _coreClaimData
                        .Where(c => c.ClaimNo == claim)
                        .ToList();

                    try
                    {
                        if (IsValid(uniqueClaim.Status))
                        {
                            switch (uniqueClaim.Status.ToUpper())
                            {
                                case "O":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int) ClaimStatus.Open;
                                    break;

                                case "R":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int) ClaimStatus.Settled;
                                    break; 
                                default:
                                    throw new InvalidOperationException(
                                        string.Format("Invalid status: [{0}] on claimId [{1}]", uniqueClaim.Status,
                                            uniqueClaim.ClaimNo));
                            }
                        }

                        if (uniqueClaim.NotifiedDate != null)
                            motorClaim.ExtraClaimInfo.ClaimNotificationDate = uniqueClaim.NotifiedDate;



                        if (IsValid(uniqueClaim.IncidentType))
                            motorClaim.ExtraClaimInfo.IncidentCircumstances = uniqueClaim.IncidentType;

                        // todo: Incident Description not yet mapped!
                        motorClaim.ExtraClaimInfo.Reserve = PopulateReserve(claimInfo);

                        motorClaim.ExtraClaimInfo.PaymentsToDate = PopulatePaymentsToDate(claimInfo);
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion ClaimInfo

                    #region Policy

                    try
                    {
                        motorClaim.Policy.Insurer = "Mulsanne";                        

                        if (IsValid(uniqueClaim.PolicyId))
                            motorClaim.Policy.PolicyNumber = uniqueClaim.PolicyId;

                        if (uniqueClaim.RiskDate != null)
                            motorClaim.Policy.PolicyStartDate = uniqueClaim.RiskDate;

                        if (uniqueClaim.Expiry != null)
                            motorClaim.Policy.PolicyEndDate = uniqueClaim.Expiry;  

                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping policy: " + ex);
                    }

                    #endregion Policy

                    #region Vehicle(s) And Person(s) and Organisation(s)

                    try
                    {
                        var insuredVehicle = GenerateInsuredVehicle(claimInfo);

                        motorClaim.Vehicles.Add(insuredVehicle);

                        var thirdPartyVehicles = CreateThirdPartyVehicles(claimInfo);

                        motorClaim.Vehicles.AddRange(thirdPartyVehicles);
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping vehicle information: " + ex);
                    }

                    #endregion

                }

                if (_processClaimFn(Ctx, motorClaim, _statusTracking) == -1) return;
            }

            
        }


        /// <summary>
        /// Look in Primary Claimant and Claimant Columns for InsuredVehicle Persons who aren't policyholder
        /// </summary>
        /// <param name="claimInfo"></param>
        /// <param name="policyHolder"></param>
        /// <returns></returns>
        private List<PipelinePerson> GenerateInsuredPersons(IEnumerable<ClaimData> claimInfo, PipelinePerson policyHolder)
        {
            var personList = new List<PipelinePerson>();

            var claimData = claimInfo.Where
                (
                    x => x.IsPrimary_Claimant == "1"
                ).ToList();



            foreach (var data in claimData)
            {

                if (data.PRIMARY_CLMNT_Forename.Equals(data.PH_NameFore, StringComparison.InvariantCultureIgnoreCase) &&
                    data.PRIMARY_CLMNT_Surname.Equals(data.PH_NameSur, StringComparison.InvariantCultureIgnoreCase))
                {

                }
                else
                {

                    var primaryClaimant = new PipelinePerson
                    {
                        I2Pe_LinkData =
                        {
                            PartyType_Id = (int)PartyType.Insured,
                            SubPartyType_Id = (int)SubPartyType.Unknown
                        },
                        Pe2Po_LinkData =
                        {
                            PolicyLinkType_Id = (int)PolicyLinkType.Insured,
                        },


                    };

                    var claimant = new PipelinePerson
                    {
                        I2Pe_LinkData =
                        {
                            PartyType_Id = (int)PartyType.Insured,
                            SubPartyType_Id = (int)SubPartyType.Unknown
                        },
                        Pe2Po_LinkData =
                        {
                            PolicyLinkType_Id = (int)PolicyLinkType.Insured,
                        }
                    };

                    if ((IsValid(data.PRIMARY_CLMNT_Surname) && (IsValid(data.PRIMARY_CLMNT_Forename))))
                    {

                        if (IsValid(data.PRIMARY_CLMNT_TITLE))
                            primaryClaimant.Salutation_Id = (int)SalutationConvertor.ConvertTo(data.PRIMARY_CLMNT_TITLE);

                        if (IsValid(data.PRIMARY_CLMNT_Forename))
                            primaryClaimant.FirstName = data.PRIMARY_CLMNT_Forename;

                        if (IsValid(data.PRIMARY_CLMNT_Surname))
                            primaryClaimant.LastName = data.PRIMARY_CLMNT_Surname;

                        if (data.PRIMARY_CLMNT_DOB.HasValue)
                            primaryClaimant.DateOfBirth = data.PRIMARY_CLMNT_DOB;

                        if (IsValid(data.PRIMARY_CLMNT_Phone))
                        {
                            primaryClaimant.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.PRIMARY_CLMNT_Phone, TelephoneType_Id = (int)TelephoneType.Unknown });
                        }

                        var personAddress = TranslateClaimantAddress(data);
                        if (personAddress != null)
                        {
                            primaryClaimant.Addresses.Add(personAddress);
                        }

                        var organisations = TranslateOrganisations(data);

                        if (organisations != null && organisations.Any())
                        {
                            primaryClaimant.Organisations.AddRange(organisations);
                        }

                        personList.Add(primaryClaimant);
                    }


                    if ((IsValid(data.T_PARTY_CLMNT_NameFore) && (IsValid(data.T_PARTY_CLMNT_NameSur))))
                    {
                        if (!(data.PRIMARY_CLMNT_Surname.Equals(data.T_PARTY_CLMNT_NameSur, StringComparison.InvariantCultureIgnoreCase) &&
                           data.PRIMARY_CLMNT_Forename.Equals(data.T_PARTY_CLMNT_NameFore, StringComparison.InvariantCultureIgnoreCase)))
                        {


                            if (!(data.T_PARTY_CLMNT_NameFore.Equals(policyHolder.FirstName, StringComparison.InvariantCultureIgnoreCase) &&
                                  data.T_PARTY_CLMNT_NameSur.Equals(policyHolder.LastName, StringComparison.InvariantCultureIgnoreCase)))
                            {

                                if (IsValid(data.T_PARTY_CLMNT_TITLE))
                                    claimant.Salutation_Id = (int)SalutationConvertor.ConvertTo(data.T_PARTY_CLMNT_TITLE);

                                if (IsValid(data.T_PARTY_CLMNT_NameFore))
                                    claimant.FirstName = data.T_PARTY_CLMNT_NameFore;

                                if (IsValid(data.T_PARTY_CLMNT_NameSur))
                                    claimant.LastName = data.T_PARTY_CLMNT_NameSur;

                                if (data.T_PARTY_CLMNT_DOB.HasValue)
                                    claimant.DateOfBirth = data.T_PARTY_CLMNT_DOB;

                                var personAddress = TranslateClaimantAddress(data);
                                if (personAddress != null)
                                {
                                    claimant.Addresses.Add(personAddress);
                                }

                                var organisations = TranslateOrganisations(data);

                                if (organisations != null && organisations.Any())
                                {
                                    claimant.Organisations.AddRange(organisations);
                                }

                                personList.Add(claimant);
                            }
                        }

                    }
                }
            }

            return personList;
        }

        private List<PipelinePerson> GenerateThirdPartyPersons(IEnumerable<ClaimData> claimInfo)
        {
            var personList = new List<PipelinePerson>();

            foreach (var data in claimInfo)
            {
                var claimant = new PipelinePerson
                {
                    I2Pe_LinkData =
                    {
                        PartyType_Id = (int)PartyType.Claimant,
                        SubPartyType_Id = (int)SubPartyType.Unknown
                    },                    
                };               


                if ((IsValid(data.T_PARTY_CLMNT_NameFore) && (IsValid(data.T_PARTY_CLMNT_NameSur))))
                {
                    if (!(data.PRIMARY_CLMNT_Surname.Equals(data.T_PARTY_CLMNT_NameSur, StringComparison.InvariantCultureIgnoreCase) &&
                       data.PRIMARY_CLMNT_Forename.Equals(data.T_PARTY_CLMNT_NameFore, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        if (!(data.T_PARTY_CLMNT_NameFore.Equals(data.PH_NameFore, StringComparison.InvariantCultureIgnoreCase) &&
                              data.T_PARTY_CLMNT_NameSur.Equals(data.PH_NameSur, StringComparison.InvariantCultureIgnoreCase)))
                        {

                            if (IsValid(data.T_PARTY_CLMNT_TITLE))
                                claimant.Salutation_Id = (int)SalutationConvertor.ConvertTo(data.T_PARTY_CLMNT_TITLE);

                            if (IsValid(data.T_PARTY_CLMNT_NameFore))
                                claimant.FirstName = data.T_PARTY_CLMNT_NameFore;

                            if (IsValid(data.T_PARTY_CLMNT_NameSur))
                                claimant.LastName = data.T_PARTY_CLMNT_NameSur;

                            if (data.T_PARTY_CLMNT_DOB.HasValue)
                                claimant.DateOfBirth = data.T_PARTY_CLMNT_DOB;

                            var personAddress = TranslateClaimantAddress(data);
                            if (personAddress != null)
                            {
                                claimant.Addresses.Add(personAddress);
                            }

                            var organisations = TranslateOrganisations(data);

                            if (organisations != null && organisations.Any())
                            {
                                claimant.Organisations.AddRange(organisations);
                            }

                            personList.Add(claimant);
                        }
                    }

                }
            }

            return personList;
        }
       
        private PipelinePerson GeneratePolicyHolderPerson(IEnumerable<ClaimData> claimInfo)
        {
            var person = new PipelinePerson
            {
                I2Pe_LinkData =
                {
                    PartyType_Id = (int)PartyType.Insured,
                    SubPartyType_Id = (int)SubPartyType.Unknown
                },
                Pe2Po_LinkData =
                {
                    PolicyLinkType_Id = (int)PolicyLinkType.Policyholder,
                }
            };

            var data = claimInfo.FirstOrDefault(x => x.IsPrimary_Claimant == "1");

            if (data != null)
            {
                if (IsValid(data.PH_Title))
                    person.Salutation_Id = (int)SalutationConvertor.ConvertTo(data.PH_Title);

                if (IsValid(data.PH_NameFore))
                    person.FirstName = data.PH_NameFore;

                if (IsValid(data.PH_NameSur))
                    person.LastName = data.PH_NameSur;

                if (data.PH_DOB.HasValue)
                    person.DateOfBirth = data.PH_DOB;



                // Get Phone
                if (IsValid(data.PH_phoneday))
                {
                    person.Telephones.Add(new PipelineTelephone {ClientSuppliedNumber = data.PH_phoneday, TelephoneType_Id = (int)TelephoneType.Unknown});                
                }                

                // Get Address

                var personAddress = TranslatePHAddress(data);

                // Get Organisations
                var organisations = TranslateOrganisations(data);

                if (organisations != null && organisations.Any())
                {
                    person.Organisations.AddRange(organisations);
                }

                // If Policy Holder = Primary Claimant then get the Tel and Email from here
                if (data.PH_NameFore.Equals(data.PRIMARY_CLMNT_Forename, StringComparison.InvariantCultureIgnoreCase) &&
                    data.PH_NameSur.Equals(data.PRIMARY_CLMNT_Surname, StringComparison.InvariantCultureIgnoreCase))
                {
                    // Have I already added tel?

                    if (data.PH_phoneday != data.PRIMARY_CLMNT_Phone && IsValid(data.PRIMARY_CLMNT_Phone))
                    {
                        person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.PRIMARY_CLMNT_Phone, TelephoneType_Id = (int)TelephoneType.Unknown });
                    }

                    if (IsValid(data.PRIMARY_CLMNT_Email))
                    {
                        person.EmailAddresses.Add(new PipelineEmail { EmailAddress = data.PRIMARY_CLMNT_Email });
                    }

                    if (personAddress == null)
                    {
                        personAddress = TranslateClaimantAddress(data);
                    }
                }

                if (personAddress != null)
                {
                    person.Addresses.Add(personAddress);
                }

            }

            return person;
        }

        private List<PipelineOrganisation> TranslateOrganisations(ClaimData data)
        {
            var orgs = new List<PipelineOrganisation>();

            if (IsValid(data.Credit_Hire_1_SP))
            {
                var org = new PipelineOrganisation
                {
                    OrganisationName = data.Credit_Hire_1_SP,
                    OrganisationType_Id = (int) OrganisationType.CreditHire,
                    P2O_LinkData = {Person2OrganisationLinkType_Id = (int) Person2OrganisationLinkType.Hire}
                };

                if (IsValid(data.Credit_Hire_1_ADD1_SP) && IsValid(data.Credit_Hire_1_PCode_SP))
                {
                    var address = TranslateAddress(data.Credit_Hire_1_ADD1_SP, data.Credit_Hire_1_ADD2_SP, null, data.Credit_Hire_1_PCode_SP);
                    if (address != null) {org.Addresses.Add(address);}
                }

                orgs.Add(org);
            }

            if (IsValid(data.Credit_Hire_2_SP))
            {
                var org = new PipelineOrganisation
                {
                    OrganisationName = data.Credit_Hire_2_SP,
                    OrganisationType_Id = (int)OrganisationType.CreditHire,
                    P2O_LinkData = { Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire }
                };               
                orgs.Add(org);
            }

            if (IsValid(data.Credit_Hire_1_IP))
            {
                var org = new PipelineOrganisation
                {
                    OrganisationName = data.Credit_Hire_1_IP,
                    OrganisationType_Id = (int)OrganisationType.CreditHire,
                    P2O_LinkData = { Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire }
                };
                orgs.Add(org);
            }

            if (IsValid(data.Clmnt_Solicitor_IP))
            {
                var org = new PipelineOrganisation
                {
                    OrganisationName = data.Clmnt_Solicitor_IP,
                    OrganisationType_Id = (int)OrganisationType.Solicitor,
                    P2O_LinkData = { Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor }
                };

                if (IsValid(data.Clmnt_Sol_Add1) && IsValid(data.Clmnt_Sol_Pcode))
                {
                    var address = TranslateAddress(data.Clmnt_Sol_Add1, data.Clmnt_Sol_Add2, null, data.Clmnt_Sol_Pcode);
                    if (address != null) { org.Addresses.Add(address); }
                }

                orgs.Add(org);
            }



            return orgs;
        }


        private PipelineAddress TranslateAddress(string address1, string address2, string town, string postcode)
        {
            try
            {
                if (((IsValid(address1)) || IsValid(address2)) && IsValid(postcode))
                {


                    var address = new PipelineAddress
                    {                        
                        Street = DefaultValue(address1),
                        Locality = DefaultValue(address2), 
                        Town = DefaultValue(town),
                        PostCode = DefaultValue(postcode)
                    };

                    return address;
                }            
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in mapping address information: " + ex);
            }

            return null;
        }



        // ReSharper disable once InconsistentNaming
        private PipelineAddress TranslatePHAddress(ClaimData claimInfo)
        {
            return TranslateAddress(DefaultValue(claimInfo.PH_address1), 
                                    DefaultValue(claimInfo.PH_Address2), 
                                    null, 
                                    DefaultValue(claimInfo.PH_P_Code));
        }

        private PipelineAddress TranslateClaimantAddress(ClaimData claimInfo)
        {
            return TranslateAddress(DefaultValue(claimInfo.PRIMARY_CLMNT_ADD_1), 
                                    DefaultValue(claimInfo.PRIMARY_CLMNT_ADD_2),  
                                    DefaultValue(claimInfo.PRIMARY_CLMNT_TOWN), 
                                    DefaultValue(claimInfo.PRIMARY_CLMNT_PCODE));           
        }        

        private List<PipelineVehicle> CreateThirdPartyVehicles(IEnumerable<ClaimData> claimInfo)
        {
            var vehicles = new List<PipelineVehicle> ();

            var data = claimInfo.Where(x => x.IsPrimary_Claimant == "0").OrderBy(x => x.T_PARTY_CLMNT__REG).ToList();

            PipelineVehicle thirdPartyVehicle = null;

            foreach (var vehicleData in data)
            {
                // first time thirdPartyVehicle = null
                if (thirdPartyVehicle == null || (!vehicleData.T_PARTY_CLMNT__REG.Equals(thirdPartyVehicle.VehicleRegistration, StringComparison.InvariantCultureIgnoreCase)))
                {
                    thirdPartyVehicle = MapToPipelineThirdPartyVehicle(vehicleData);
                    thirdPartyVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle; 
                    thirdPartyVehicle.People = GenerateThirdPartyPersons(data);

                }
                else
                {
                    thirdPartyVehicle.People = GenerateThirdPartyPersons(data);
                    // only add vehicle if not already added!
                    if (vehicles.All(x => x.VehicleRegistration != thirdPartyVehicle.VehicleRegistration))
                    {
                        vehicles.Add(thirdPartyVehicle);        
                    }
                }

            }

            // only add vehicle if not already added!
            if (thirdPartyVehicle != null && vehicles.All(x => x.VehicleRegistration != thirdPartyVehicle.VehicleRegistration))
            {
                vehicles.Add(thirdPartyVehicle);
            }

            return vehicles;
        }

        private PipelineVehicle MapToPipelineThirdPartyVehicle(ClaimData claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var insuredVehicle = new PipelineVehicle
            {
                VehicleMake = DefaultValue(claimInfo.T_PARTY_VehicleMake),
                VehicleModel = DefaultValue(claimInfo.T_PARTY_VehicleModel),
                VehicleRegistration = DefaultValue(claimInfo.T_PARTY_CLMNT__REG)
            };           

            return insuredVehicle;
        }

        private PipelineVehicle MapToPipelineInsuredVehicle(ClaimData claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var insuredVehicle = new PipelineVehicle
            {
                VehicleMake = DefaultValue(claimInfo.PRIMARY_CLMNT_VEH_Reg),
                VehicleModel = DefaultValue(claimInfo.PRIMARY_CLMNT_VEH_Model),
                VehicleRegistration = DefaultValue(claimInfo.PRIMARY_CLMNT_VEH_Reg)
            };

            return insuredVehicle;
        }

        private PipelineVehicle GenerateInsuredVehicle(IEnumerable<ClaimData> claimInfo)
        {
            if (claimInfo == null) {  throw new ArgumentNullException("claimInfo");}

            var claimDatas = claimInfo as IList<ClaimData> ?? claimInfo.ToList();
            var data = claimDatas.FirstOrDefault(x=>x.IsPrimary_Claimant == "1");

            if (data == null) return new PipelineVehicle();

            var insuredVehicle = MapToPipelineInsuredVehicle(data);
            insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

            var policyHolder = GeneratePolicyHolderPerson(claimDatas);

            var insuredPersons = GenerateInsuredPersons(claimDatas, policyHolder);

            if (insuredPersons == null || (!insuredPersons.Any()))
            {
                policyHolder.V2Pe_LinkData.VehicleLinkType_Id = (int)VehicleLinkType.Insured;
            }
            else
            {
                insuredVehicle.People.AddRange(insuredPersons);
            }

            insuredVehicle.People.Add(policyHolder);

            


            return insuredVehicle;
        }

        /// <summary>
        /// Add all reserve values together for this claim
        /// </summary>
        /// <param name="claimInfo"><see cref="IEnumerable{claimInfo}"/>List of entries with same claim number</param>
        /// <returns><see cref="decimal"/>Sum of all reserve values</returns>
        private decimal? PopulateReserve(IEnumerable<ClaimData> claimInfo)
        {
            return claimInfo.Where(claimData => claimData.CurrentReserve.HasValue).Sum(claimData => claimData.CurrentReserve.Value);
        }

        /// <summary>
        /// Add all paymentsToDate values together for this claim
        /// </summary>
        /// <param name="claimInfo"><see cref="IEnumerable{claimInfo}"/>List of entries with same claim number</param>
        /// <returns><see cref="decimal"/>Sum of all PaymentsToDate</returns>
        private decimal? PopulatePaymentsToDate(IEnumerable<ClaimData> claimInfo)
        {
            return claimInfo.Where(claimData => claimData.PaidToDate.HasValue).Sum(claimData => claimData.PaidToDate.Value);
        }

        /// <summary>
        /// Detect if "NULL" is in string and ignore, value is null or value is empty
        /// </summary>
        /// <param name="value"><see cref="String"/> or null</param>
        /// <returns><see cref="bool"/>true if string has valid value</returns>
        public static bool IsValid(string value)
        {
            return !string.IsNullOrEmpty(value) && 
                   !value.Equals("NULL", StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// If input string value is null, return null
        /// </summary>
        /// <param name="value">input string</param>
        /// <returns>null if input value empty or null, else returns value</returns>
        public static string DefaultValue(string value)
        {
            return IsValid(value) ? value : null;
        }
    }
}
