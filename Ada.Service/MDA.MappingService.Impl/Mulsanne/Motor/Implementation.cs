﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.MappingService.Interface;
using MDA.Common;
using MDA.Common.Server;
using System.Data.OleDb;
using FileHelpers;
using Excel;

namespace MDA.MappingService.Impl.Mulsanne.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        private readonly string _tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];

        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking,
            Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> processClaim,
            out ProcessingResults processingResults)
        {
            string clientFolder;

            GetFilePath(out clientFolder, ctx.RiskClientId);

            processingResults = new ProcessingResults("Mapping");

            try
            {
                var mapper = new Mapper(fs, clientFolder, ctx, processClaim, statusTracking);

                mapper.AssignFiles();
                mapper.InitialiseFileHelperEngines();
                mapper.PopulateFileHelperEngines();
                mapper.RetrieveDistinctClaims();
                mapper.Translate();
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping Mulsanne file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void GetFilePath(out string clientFolder, int clientId)
        {
            clientFolder = _tempFolder + "\\" + clientId;

            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
        }


        private string ConvertExcelToCsv(string excelFilePath, int worksheetNumber = 1)
        {
            if (!File.Exists(excelFilePath)) throw new FileNotFoundException(excelFilePath);
            //if (File.Exists(csvOutputFile)) throw new ArgumentException("File exists: " + csvOutputFile);

            // connection string
            var cnnStr = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;IMEX=1;HDR=NO\"", excelFilePath);
            var cnn = new OleDbConnection(cnnStr);

            // get schema, then data
            var dt = new DataTable();
            try
            {
                cnn.Open();
                var schemaTable = cnn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (schemaTable.Rows.Count < worksheetNumber) throw new ArgumentException("The worksheet number provided cannot be found in the spreadsheet");
                string worksheet = schemaTable.Rows[worksheetNumber - 1]["table_name"].ToString().Replace("'", "");
                string sql = String.Format("select * from [{0}]", worksheet);
                var da = new OleDbDataAdapter(sql, cnn);
                da.Fill(dt);
            }
            catch (Exception e)
            {
                // ???
                throw e;
            }
            finally
            {
                // free resources
                cnn.Close();
            }

            string date;
            date = DateTime.Now.ToString("dd/MM/yy HH:mm:ss");
            date = date.Replace("/", "");
            date = date.Replace(":", "");
            date = date.Replace(" ", "");

            string csvOutputFile = _tempFolder + "\\" + date + ".csv";

            // write out CSV data
            using (var wtr = new StreamWriter(csvOutputFile))
            {
                foreach (DataRow row in dt.Rows)
                {
                    bool firstLine = true;
                    foreach (DataColumn col in dt.Columns)
                    {
                        if (!firstLine) { wtr.Write(","); } else { firstLine = false; }
                        var data = row[col.ColumnName].ToString().Replace("\"", "\"\"");
                        wtr.Write(String.Format("\"{0}\"", data));
                    }
                    wtr.WriteLine();
                }
            }

            return csvOutputFile;
        }

        private string ConvertXLSXtoPipe(string excelFilePath, int clientId)
        {
            if (!File.Exists(excelFilePath)) throw new FileNotFoundException(excelFilePath);

            FileStream stream = File.Open(excelFilePath, FileMode.Open, FileAccess.Read);

            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
            //IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            // DataSet - Create column names from first row
            excelReader.IsFirstRowAsColumnNames = false;
            DataSet result = excelReader.AsDataSet();
            DataTable dt = result.Tables[0];

            string date;
            date = DateTime.Now.ToString("dd/MM/yy HH:mm:ss");
            date = date.Replace("/", "");
            date = date.Replace(":", "");
            date = date.Replace(" ", "");

            string csvOutputFile = _tempFolder + "\\" + clientId + "\\" + date + ".txt";

            // write out Txt data
            using (var wtr = new StreamWriter(csvOutputFile))
            {
                foreach (DataRow row in dt.Rows)
                {
                    bool firstLine = true;
                    foreach (DataColumn col in dt.Columns)
                    {
                        if (!firstLine) { wtr.Write("|"); } else { firstLine = false; }
                        var data = row[col.ColumnName].ToString();
                        wtr.Write(String.Format("\"{0}\"", data));
                    }
                    wtr.WriteLine();
                }
            }

            return csvOutputFile;

        }
        
        public override void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard,
            int settleTime, int batchSize, string postOp, string archiveFolder,
            Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {
            // Create structure to hold our new batch of claims
            var batch = new List<object>();
            
            string firstFileName = null;

            // Get a list of the files in the folderPath filtered by the wildCard. Search the top level only (don't go into child directories)
            var files = Directory.GetFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

            // If we find none, return
            if (!files.Any()) return;

            // If ANY files in that list have been modified in the last "settleTime" minutes return.  The directory is not stable, files may still being
            // written.  Return now and wait for the next check
            if (files.Any(x => File.GetCreationTime(x).AddMinutes(settleTime) > DateTime.Now))
                return;

            // If we get here we have files to process
            foreach (var file in files)
            {
                // Create a filename to use for this batch.  We will just use the first file name in this case
                if (firstFileName == null)
                    firstFileName = Path.GetFileName(file);

                // Add this new claim to the batch
                //batch.Add(file);
                //batch.Add(ConvertExcelToCsv(file, 1));
                //batch.Add(ConvertXLSXtoPipe(file, ctx.RiskClientId));

                var fileName = ConvertXLSXtoPipe(file, ctx.RiskClientId);
                batch.Add(fileName);

                #region If Batch full process it

                if (batch.Count >= batchSize)
                {
                    //var fileName = firstFileName;
                    const string mimeType = "txt";

                    const string clientName = "MULSANNE";


                    var date = DateTime.Now.ToString("dd/MM/yy HH:mm:ss");
                    date = date.Replace("/", "");
                    date = date.Replace(":", "");
                    date = date.Replace(" ", "");

                    var batchRef = clientName + date;
                    string fileToStream = string.Empty;

                    fileToStream = batch[0].ToString();

                    using (var fileStream = File.OpenRead(fileToStream))
                    {
                        var stream = new MemoryStream();
                        stream.SetLength(fileStream.Length);
                        fileStream.Read(stream.GetBuffer(), 0, (int) fileStream.Length);

                        #region Process the batch (call the callback function)

                        processClientBatch(ctx, stream, fileName, mimeType, "Motor", batchRef);

                        #endregion
                    }

                    // Setup for a new batch and go round the loop again
                    batch.Clear();
                    firstFileName = null;
                }

                #endregion

                #region Process all the options we invented for our postOp functionality

                // We are passed the string we entered into config.  Now we have to code it!
                // In this case we have implemented RENAME, DELETE, ARCHIVE
                // ARCHIVE checks and (if needed) creates the destination folder. It also checks if the file already exists
                // in the destination folder (which would break the MOVE) and (if needed) adds a random string to make it unique
                switch (postOp.ToUpper())
                {
                    case "RENAME":
                        File.Move(file, file + ".old");
                        break;
                    case "DELETE":
                        File.Delete(file);
                        break;
                    case "ARCHIVE":
                        if (!Directory.Exists(archiveFolder))
                            Directory.CreateDirectory(archiveFolder); // Create destination if needed

                        var fname = archiveFolder + "\\" + Path.GetFileName(file); // build dest path

                        if (File.Exists(fname)) // Check if it already exists
                        {
                            fname += "." + Path.GetRandomFileName(); // Adds a random string to end of filename
                        }

                        File.Move(file, fname);
                        break;
                }

                #endregion

            }

        }
    }
}
