﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.DLGNew.Motor.Model;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Ionic.Zip;
using MDA.MappingService.Helpers;

namespace MDA.MappingService.DLGNew.Motor
{
    public class Mapper : IMapper
    {

        private readonly Stream _fs;
        private FileHelperEngine _coreClaimDataEngine1;
        private FileHelperEngine _coreClaimDataEngine2;
        private ClaimData1[] dlg1;  
        private ClaimData2[] dlg2;
        
        private readonly Func<CurrentContext, IPipelineClaim, object, int> _processClaimFn;
        public string ClientFolder { get; set; }
        public List<string> FileNames;
        public string DLGFile1;
        public string DLGFile2;
        public CurrentContext Ctx { get; set; }
        private readonly object _statusTracking;
        private List<string> _uniqueClaimNumberList;
        private List<string> DefaultAddressesForClient { get; set; }
        private List<string> DefaultPeopleForClient { get; set; }
        private List<string> DefaultOrganisationsForClient { get; set; }

        public Mapper(Stream fs,
                        string clientFolder,
                        CurrentContext ctx,
                        Func<CurrentContext, IPipelineClaim, object, int> processClaimFn,
                        object statusTracking,
                        List<string> defaultAddressesForClient,
                        List<string> defaultPeopleForClient,
                        List<string> defaultOrganisationsForClient)
        {
            _fs = fs;
            ClientFolder = clientFolder;
            Ctx = ctx;
            _processClaimFn = processClaimFn;
            _statusTracking = statusTracking;
            DefaultAddressesForClient = defaultAddressesForClient;
            DefaultPeopleForClient = defaultPeopleForClient;
            DefaultOrganisationsForClient = defaultOrganisationsForClient;

            _coreClaimDataEngine1 = new FileHelperEngine(typeof(ClaimData1));
            _coreClaimDataEngine2 = new FileHelperEngine(typeof(ClaimData2));
            _uniqueClaimNumberList = new List<string>();
            FileNames = new List<string>();

            #region Extract Files From Zip

            if (_fs != null)
            {
                try
                {

                    using (var zip1 = ZipFile.Read(_fs))
                    {

                        if (zip1 == null) { throw new Exception("Error reading Zip file"); }                        
                        
                        foreach (var e in zip1)
                        {
                            var extractTmpPath = string.Concat(ClientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);

                            var unpackDirectory = ClientFolder;
                            var password = ConfigurationManager.AppSettings["DLGFilePassword"];
                            
                            // Could have passwoord
                            var extractSuccess = TryFileExtractor(e, password, unpackDirectory);
                            // else could have password with space
                            if (!extractSuccess)
                            {
                                extractSuccess = TryFileExtractor(e, password + " ", unpackDirectory);
                            }
                            // else could have no password!
                            if (!extractSuccess)
                            {
                                extractSuccess = TryFileExtractor(e, string.Empty, unpackDirectory);
                                if (!extractSuccess)
                                {
                                    throw new BadPasswordException("Tried password, password + space, no password but still errored");
                                }
                            }

                            FileNames.Add("\\" + e.FileName);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                    }

                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in extraction of files: " + ex);
                }
            }

            #endregion
      
        }

        /// <summary>
        /// Tries to extract files, returns false if bad password
        /// </summary>
        /// <param name="zipFileEntry"><see cref="ZipEntry"/>ZipEntry</param>
        /// <param name="password"><see cref="string"/>string of password (optional)</param>
        /// <param name="unpackDirectory"><see cref="string"/>string path of folder to extract to</param>
        /// <returns>true if successful</returns>
        private static bool TryFileExtractor(ZipEntry zipFileEntry, string password, string unpackDirectory)
        {
            if (zipFileEntry == null) throw new ArgumentNullException("zipFileEntry");

            if (string.IsNullOrEmpty(unpackDirectory))
                throw new ArgumentException("Value cannot be null or empty.", "unpackDirectory");

            try
            {
                if (!string.IsNullOrEmpty(password))
                {
                    zipFileEntry.Password = password;              
                }
                zipFileEntry.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

            }
            catch (BadPasswordException)
            {
                return false;
            }
            return true;
        }

        public void AssignFiles()
        {
                        
            DLGFile1 = ClientFolder + FileNames.FirstOrDefault(x => x.Contains("DLG_datawash_1")).ToString();
            DLGFile2 = ClientFolder + FileNames.FirstOrDefault(x => x.Contains("DLG_datawash_2")).ToString();

        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine1 = new FileHelperEngine(typeof(ClaimData1));
                _coreClaimDataEngine2 = new FileHelperEngine(typeof(ClaimData2));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {
                dlg1 = _coreClaimDataEngine1.ReadFile(DLGFile1) as ClaimData1[];
                dlg2 = _coreClaimDataEngine2.ReadFile(DLGFile2) as ClaimData2[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            throw new NotImplementedException();
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers from dlg1 file
            
                // 90 Days Back
                var minDate = (from d in dlg1.AsParallel() select d.REPORTED_DT).Max().Value.AddDays(Convert.ToInt32(ConfigurationManager.AppSettings["DLGDaysToGoBack"]));

                var query = (from c in dlg1.AsParallel()
                            where c.CLAIM_NUMBER != "" &&
                                    c.EXPOSURE_CLOSE_DT == "" &&
                                    c.REPORTED_DT.HasValue && 
                                    c.EXPOSURE_TYPE_DESC == "Bodily Injury" &&
                                    c.REPORTED_DT.Value.Date >= minDate
                            select c.CLAIM_NUMBER);

                // A test to see the difference in claim retention
                if(ConfigurationManager.AppSettings["DLGIncludeOtherTypes"] != null && ConfigurationManager.AppSettings["DLGIncludeOtherTypes"].ToString() == "true")
                {
                    query = (from c in dlg1.AsParallel()
                             where c.CLAIM_NUMBER != "" &&
                                     c.EXPOSURE_CLOSE_DT == "" &&
                                     c.REPORTED_DT.HasValue &&
                                     (
                                        c.EXPOSURE_TYPE_DESC == "Bodily Injury" &&
                                        c.EXPOSURE_TYPE_DESC == "Hire" ||
                                        c.EXPOSURE_TYPE_DESC == "Vehicle"
                                     ) &&
                                     c.REPORTED_DT.Value.Date >= minDate
                             select c.CLAIM_NUMBER);
                }
                
                _uniqueClaimNumberList = query.Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }
        }

        public void Translate()
        {
            foreach (var claim in _uniqueClaimNumberList)
            {
                var motorClaim = new PipelineMotorClaim();

                var uniqueClaim1 = dlg1.AsParallel().FirstOrDefault(x => x.CLAIM_NUMBER == claim);
                var uniqueClaim2 = dlg2.AsParallel().FirstOrDefault(x => x.CLAIM_NUMBER == uniqueClaim1.CLAIM_NUMBER);

                if (uniqueClaim1 != null)
                {

                    #region Claim

                    try
                    {

                        //Check that there is a value in claim number and map it to ClaimNumber
                        if (!string.IsNullOrEmpty(uniqueClaim1.CLAIM_NUMBER))
                            motorClaim.ClaimNumber = uniqueClaim1.CLAIM_NUMBER;

                        //Check that there is a value in loss date and map it to IncidentDate
                        if (uniqueClaim1.LOSS_DT != null)
                            motorClaim.IncidentDate = Convert.ToDateTime(uniqueClaim1.LOSS_DT);

                        motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion Claim

                    #region ClaimInfo

                    var claimInfo1 = dlg1.AsParallel().Where(c => c.CLAIM_NUMBER == claim).ToList();
                    var claimInfo2 = dlg2.AsParallel().Where(c => c.CLAIM_NUMBER == claim).ToList();
                    
                    try
                    {
                        //If there is a value in EXPOSURE_CLOSE_DT then the claim is closed, else take the value from EXPOSURE_STATE_DESC.  If both EXPOSURE_CLOSE_DT and EXPOSURE_STATE_DESC are null, we assume the claim is open.
                        if (uniqueClaim1.EXPOSURE_CLOSE_DT != "")
                        {
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(uniqueClaim1.EXPOSURE_STATE_DESC))
                            {
                                //Map the values from EXPOSURE_STATE_DESC to our claim status.
                                switch (uniqueClaim1.EXPOSURE_STATE_DESC.ToUpper())
                                {
                                    case "OPEN":
                                    case "DRAFT":
                                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                        break;

                                    case "CLOSED":
                                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                        break;
                                }
                            }
                            else
                            {
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                            }
                        }
                        
                        if (uniqueClaim1.REPORTED_DT != null)
                            motorClaim.ExtraClaimInfo.ClaimNotificationDate = uniqueClaim1.REPORTED_DT;


                        var totalReserve = 0;
                        var i = 0;
                        foreach (var r in claimInfo1)
                        {
                            var recordReserve = claimInfo1[i].RESERVE_AMT;
                            var number = 0;

                            var result = Int32.TryParse(recordReserve, out number);
                            if (result)
                            {
                                totalReserve += number;
                            }

                            recordReserve = "";
                            number = 0;
                            i++;                  
                            
                        }
                        if (totalReserve != 0)
                            motorClaim.ExtraClaimInfo.Reserve = totalReserve;

                        var s = new StringBuilder();
                        if (!string.IsNullOrEmpty(uniqueClaim1.LOSS_CAUSE_DESC))
                            s.Append(uniqueClaim1.LOSS_CAUSE_DESC + " | ");
                        if (uniqueClaim2 != null)
                        {
                            if (!string.IsNullOrEmpty(uniqueClaim2.JOURNEY_PURPOSE_DESC))
                                s.Append(uniqueClaim2.JOURNEY_PURPOSE_DESC + " | ");
                            if (!string.IsNullOrEmpty(uniqueClaim2.COLLISION_SPEED))
                                s.Append(uniqueClaim2.COLLISION_SPEED + " | ");
                            if (!string.IsNullOrEmpty(uniqueClaim2.COLLISION_POINT1))
                                s.Append(uniqueClaim2.COLLISION_POINT1 + " | ");
                            if (!string.IsNullOrEmpty(uniqueClaim2.COLLISION_POINT2))
                                s.Append(uniqueClaim2.COLLISION_POINT2);
                        }
                        if (s != null)
                        { 
                            motorClaim.ExtraClaimInfo.IncidentCircumstances = s.ToString();
                        }                     

                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion ClaimInfo

                    #region Policy

                    try
                    {
                        motorClaim.Policy.Insurer = "DLG";

                        if (!string.IsNullOrEmpty(uniqueClaim1.POLICY_NUMBER))
                            motorClaim.Policy.PolicyNumber = uniqueClaim1.POLICY_NUMBER;

                        if (uniqueClaim1.POLICY_ORIGINAL_EFFECTIVE_DT != null)
                            motorClaim.Policy.PolicyStartDate = uniqueClaim1.POLICY_ORIGINAL_EFFECTIVE_DT;

                        if (uniqueClaim1.POLICY_EXPIRATION_DT != null)
                            motorClaim.Policy.PolicyEndDate = uniqueClaim1.POLICY_EXPIRATION_DT;

                        if (uniqueClaim1.BRAND != null)
                            motorClaim.Policy.InsurerTradingName = uniqueClaim1.BRAND;

                        if (uniqueClaim1.COVER_TYPE_DESC != null)
                        {
                            SetPolicyCoverType(motorClaim, uniqueClaim1.COVER_TYPE_DESC);
                        }

                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping policy: " + ex);
                    }


                    #endregion Policy

                    #region Vehicle(s) & Person(s)
                    
                    List<PipelineVehicle> vehicles;

                    try
                    {
                        if (uniqueClaim2 != null)
                        {
                            vehicles = GenerateVehicles(claimInfo2, claimInfo1);
                            motorClaim.Vehicles.AddRange(vehicles);
                        }
                         
                        var blankVehicle = new PipelineVehicle();
                        blankVehicle = GenerateBlankVehicle(claimInfo1);
                        motorClaim.Vehicles.Add(blankVehicle);
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping vehicle/person information: " + ex);
                    }

                    #endregion
                }

                if (_processClaimFn(Ctx, motorClaim, _statusTracking) == -1) return;
            }
        }

        private PipelineVehicle GenerateBlankVehicle(List<ClaimData1> claimInfo1)
        {
            if (claimInfo1 == null) { throw new ArgumentNullException("claimInfo"); }

            var blankVehicle = new PipelineVehicle();
            blankVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;

            foreach (var p in claimInfo1)
            {
                if (p.FIRST_NAME != null && p.LAST_NAME != null)
                {
                    var person = new PipelinePerson();
                    person = GeneratePerson(p);

                    if (person != null)
                        blankVehicle.People.Add(person);
                }
            }

            if (blankVehicle.People.Count(x => x.I2Pe_LinkData.PartyType_Id == 3 && x.I2Pe_LinkData.SubPartyType_Id == 1) == 0)
            {
                var blankPerson = new PipelinePerson();
                blankPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                blankPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                blankVehicle.People.Add(blankPerson);
            }
            
            return blankVehicle;
        }

        private PipelinePerson GeneratePerson(ClaimData1 p)
        {
            if (p == null) { throw new ArgumentNullException("claimInfo"); }
            
            var person = new PipelinePerson();
            var role = p.ROLE;
            SetPartyType(person, role);

            if (person.I2Pe_LinkData.PartyType_Id == 0)
                return null;

            if (IsValid(p.FIRST_NAME))
                person.FirstName = p.FIRST_NAME.ToUpper();

            if (IsValid(p.MIDDLE_NAME))
                person.MiddleName = p.MIDDLE_NAME.ToUpper();

            if (IsValid(p.LAST_NAME))
                person.LastName = p.LAST_NAME.ToUpper();

            if (p.DATE_OF_BIRTH.HasValue)
                person.DateOfBirth = p.DATE_OF_BIRTH;

            if (person.I2Pe_LinkData.PartyType_Id == 3)
            {
                if (IsValid(p.OCCUPATION_TXT))
                    person.Occupation = p.OCCUPATION_TXT.ToUpper();

                if (EmailHelper.IsValid(p.EMAIL_ADDRESS_LN_1))
                    person.EmailAddresses.Add(new PipelineEmail { EmailAddress = EmailHelper.ReturnValueOrNull(p.EMAIL_ADDRESS_LN_1) });
            }

            if (IsValid(p.HOME_PHONE))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = p.HOME_PHONE, TelephoneType_Id = (int)TelephoneType.Landline }); //Type=1

            if (IsValid(p.WORK_PHONE))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = p.WORK_PHONE, TelephoneType_Id = (int)TelephoneType.Unknown }); //Type=0

            if (IsValid(p.CELL_PHONE))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = p.CELL_PHONE, TelephoneType_Id = (int)TelephoneType.Mobile });  //Type=2

            var personAddress = createPersonAddress(p);
            if (personAddress != null)
            {
                person.Addresses.Add(personAddress);
                personAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.CurrentAddress;
            }

            return person;

        }

        private PipelineOrganisation GenerateOrganisation(ClaimData1 claimData)
        {
            PipelineOrganisation pipelineOrg = new PipelineOrganisation();
            switch (claimData.ROLE)
            {
                case "Car Hire Supplier":
                    pipelineOrg.OrganisationType_Id = (int)OrganisationType.CreditHire;
                    pipelineOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire;
                    break;
                case "Third Party Solicitor":
                case "Claimant's Solicitor":
                    pipelineOrg.OrganisationType_Id = (int)OrganisationType.Solicitor;
                    pipelineOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor;
                    break;
                case "Accident Management Company":
                case "Accident Repair Centre":
                    pipelineOrg.OrganisationType_Id = (int)OrganisationType.AccidentManagement;
                    pipelineOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.AccidentManagement;
                    break;
                case "Physiotherapist":
                    pipelineOrg.OrganisationType_Id = (int)OrganisationType.MedicalExaminer;
                    pipelineOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.MedicalExaminer;
                    break;
                case "Vehicle Recovery":
                    pipelineOrg.OrganisationType_Id = (int)OrganisationType.Recovery;
                    pipelineOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Recovery;
                    break;
                case "Engineer":
                    pipelineOrg.OrganisationType_Id = (int)OrganisationType.VehicleEngineer;
                    pipelineOrg.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Engineer;
                    break;
                default:
                    pipelineOrg = null;
                    break;
            }

            if (pipelineOrg != null)
            {
                pipelineOrg.OrganisationName = claimData.COMPANY_NAME;

                try
                {
                    pipelineOrg.Addresses = new List<PipelineAddress>();
                    PipelineAddress orgAddress = new PipelineAddress();
                    orgAddress.Building = claimData.ADDRESSLINE1;
                    orgAddress.Street = claimData.ADDRESSLINE2;
                    orgAddress.Town = claimData.ADDRESSLINE3;
                    orgAddress.PostCode = claimData.POSTCODE;
                    orgAddress.PO2A_LinkData.AddressLinkType_Id = (int)AddressLinkType.TradingAddress;
                    pipelineOrg.Addresses.Add(orgAddress);

                    pipelineOrg.Telephones = new List<PipelineTelephone>();
                    if (TelephoneHelper.IsValid(claimData.WORK_PHONE))
                    {
                        PipelineTelephone orgTelephone = new PipelineTelephone();
                        orgTelephone.TelephoneType_Id = (int)TelephoneType.Landline;
                        orgTelephone.ClientSuppliedNumber = claimData.WORK_PHONE;
                        pipelineOrg.Telephones.Add(orgTelephone);
                    }
                } catch(Exception ex)
                {
                    throw new CustomException(string.Format("Error working with company {0} address", claimData.COMPANY_NAME), ex);
                }
            }

            return pipelineOrg;
        }

        private void SetPartyType(PipelinePerson person, string role)
        {
            try
            {
                switch (role.ToUpper())
                {
                    case "INSURED":
                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                        break;
                    case "COVERED PARTY":
                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Policyholder;
                        break;
                    case "PASSENGER":
                    case "CLAIMANT - EXPOSURE":
                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                        break;
                    case "WITNESS":
                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;
                        break;
                    case "DRIVER":
                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        break;
                    default:
                        person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in mapping Party Type" + ex);
            }      

        }

        // Check to see if we have any vehicles in the DLG2 file. If we do, create them and create the people underneath. 
        private List<PipelineVehicle> GenerateVehicles(List<ClaimData2> claimInfo2, List<ClaimData1> claimInfo1)
        {
            if (claimInfo2 == null) { throw new ArgumentNullException("claimInfo"); }

            var vehicles = new List<PipelineVehicle>();

            foreach (var v in claimInfo2)
            {
                var vehicle = new PipelineVehicle();
                vehicle = MapToPipelineVehicle(v);
                vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.Unknown;

                var person = new PipelinePerson();
                person = GenerateVehiclePerson(v, claimInfo1);
                person.V2Pe_LinkData.VehicleLinkType_Id = (int)VehicleLinkType.Driver;
                vehicle.People.Add(person);

                vehicles.Add(vehicle);
            }

            return vehicles;
        }

        private PipelinePerson GenerateVehiclePerson(ClaimData2 v, List<ClaimData1> claimInfo1)
        {
            if (v == null) { throw new ArgumentNullException("claimInfo"); }
            var person = new PipelinePerson();

            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;

            if (IsValid(v.LAST_NAME))
                person.LastName = v.LAST_NAME.ToUpper();

            if (IsValid(v.FIRST_NAME))
                person.FirstName = v.FIRST_NAME.ToUpper();

            if (IsValid(v.MIDDLE_NAME))
                person.MiddleName = v.MIDDLE_NAME.ToUpper();

            if (v.BIRTH_DATE.HasValue)
                person.DateOfBirth = v.BIRTH_DATE;

            if (NiNumberHelper.IsValid(v.NATIONAL_INSURANCE_NO))
                person.NINumbers.Add(new PipelineNINumber { NINumber1 = NiNumberHelper.ReturnValueOrNull(v.NATIONAL_INSURANCE_NO) });
            
            if (IsValid(v.HOME_PHONE_NUMBER_TXT))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = v.HOME_PHONE_NUMBER_TXT, TelephoneType_Id = (int)TelephoneType.Landline }); //Type=1
            
            if (IsValid(v.WORK_PHONE_NUMBER_TXT))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = v.WORK_PHONE_NUMBER_TXT, TelephoneType_Id = (int)TelephoneType.Unknown }); //Type=0
            
            if (IsValid(v.CELL_PHONE_NUMBER_TXT))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = v.CELL_PHONE_NUMBER_TXT, TelephoneType_Id = (int)TelephoneType.Mobile });  //Type=2
            
            if (IsValid(v.OCCUPATION_TXT))
                person.Occupation = v.OCCUPATION_TXT.ToUpper();

            if (EmailHelper.IsValid(v.EMAIL_ADDRESS_LN_1))
                person.EmailAddresses.Add(new PipelineEmail { EmailAddress = EmailHelper.ReturnValueOrNull(v.EMAIL_ADDRESS_LN_1) });

            person.Organisations = new List<PipelineOrganisation>();
            foreach (var o in claimInfo1)
            {
                var org = GenerateOrganisation(o);
                if (org != null)
                    person.Organisations.Add(org);
            }

            return person;
        }
        
        private void SetPolicyCoverType(PipelineMotorClaim claim, string p)
        {
            switch (p.ToUpper())
            {
                case "COMPREHENSIVE":
                    claim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.Comprehensive;
                    break;

                case "THIRD PARTY ONLY":
                    claim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.ThirdPartyOnly;
                    break;

                case "THIRD PARTY, FIRE & THEFT":
                    claim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.ThirdPartyFireAndTheft;
                    break;
            }
        }
        
        private PipelineVehicle MapToPipelineVehicle(ClaimData2 claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var vehicle = new PipelineVehicle
            {
                VehicleMake = DefaultValue(claimInfo.MAKE.ToUpper()),
                VehicleModel = DefaultValue(claimInfo.MODEL.ToUpper()) + " " + DefaultValue(claimInfo.MARQUE.ToUpper()),
                VehicleRegistration = DefaultValue(claimInfo.REGNO.ToUpper())
            };

            return vehicle;
        }
                   
        /// If input string value is null, return null, else return value
        public static string DefaultValue(string value)
        {
            return IsValid(value) ? value : null;
        }

        /// Detect if "NULL" is in string and ignore, value is null or value is empty.  true if string has valid value
        public static bool IsValid(string value)
        {
            return !string.IsNullOrEmpty(value) &&
                   !value.Equals("NULL", StringComparison.InvariantCultureIgnoreCase);
        }
        
        // Create the addresses for this person
        private PipelineAddress createPersonAddress(ClaimData1 data)
        {
            return translateAddress(DefaultValue(data.ADDRESSLINE1),
                                    DefaultValue(data.ADDRESSLINE2),
                                    DefaultValue(data.ADDRESSLINE3),
                                    DefaultValue(data.CITY),
                                    DefaultValue(data.POSTCODE));
        }

        // Map the address fields to our template
        private PipelineAddress translateAddress(string address1, string address2, string address3, string city, string postcode)
        {
            try
            {
                var address = new PipelineAddress
                {
                    BuildingNumber = DefaultValue(address1),
                    Street = DefaultValue(address2),
                    Locality = DefaultValue(address3),
                    Town = DefaultValue(city),
                    PostCode = DefaultValue(postcode)
                };

                return address;
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in mapping address information: " + ex);
            }
        }
        
    }
}
