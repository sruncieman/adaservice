﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.DLGNew.Motor.Model
{
    class CustomDateConverter2 : ConverterBase
    {

        public override object StringToField(string value)
        {
            
            if (value.Length > 1)
            {
                DateTime dt;
                string concatDt = string.Empty;

                string dd = value.Substring(0, 2);
                string mm = value.Substring(2, 3);
                string yyyy = value.Substring(5, 4);
                concatDt = dd + "/" + mm + "/" + yyyy;
                
                if (DateTime.TryParseExact(concatDt, "dd/MMM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
                    return dt;
            }

            return null;

            
        }
    }
}
