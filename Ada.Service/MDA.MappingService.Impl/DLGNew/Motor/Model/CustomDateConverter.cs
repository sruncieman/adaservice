﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.DLGNew.Motor.Model
{
    class CustomDateConverter : ConverterBase
    {
        
        public override object StringToField(string value)
        {
            DateTime dt;

            if (DateTime.TryParseExact(value, "dd-MMM-yy", null, DateTimeStyles.None, out dt))
                return dt; 

            if (DateTime.TryParseExact(value, "ddMMMyyyy", null, DateTimeStyles.None, out dt))
                return dt;

            //if (DateTime.TryParseExact(value, "ddMMMYYYY:HH:MM:SS", null, DateTimeStyles.None, out dt))
            //    return dt;

            else
            {
                return null;
            }

        }

    }
}
