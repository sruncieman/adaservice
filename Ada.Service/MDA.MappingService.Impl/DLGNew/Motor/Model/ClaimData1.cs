﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using FileHelpers;
using System;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Pipeline.Model;

namespace MDA.MappingService.DLGNew.Motor.Model
{
    [DelimitedRecord(",")]
    [IgnoreFirst(1)]
    class ClaimData1
    {

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CLAIM_NUMBER;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CLAIM_ID;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? CONTACT_CREATE_DT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ROLE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COMPANY_NAME;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FIRST_NAME;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MIDDLE_NAME;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LAST_NAME;

        [FieldConverter(typeof(CustomDateConverter2))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DATE_OF_BIRTH;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ADDRESSLINE1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ADDRESSLINE2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ADDRESSLINE3;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CITY;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string POSTCODE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HOME_PHONE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string WORK_PHONE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CELL_PHONE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EMAIL_ADDRESS_LN_1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string OCCUPATION_TXT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string POLICY_NUMBER;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LINE_OF_BUSINESS;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRODUCT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string BRAND;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COVER_TYPE_DESC;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COVERAGE_SUBTYPE_DESC;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? POLICY_ORIGINAL_EFFECTIVE_DT;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? POLICY_EFFECTIVE_DT;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? POLICY_EXPIRATION_DT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LOSS_TYPE_DESC;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? LOSS_DT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LOSS_TIME;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? REPORTED_DT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string REPORTED_BY_TYPE_DESC;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HOW_REPORTED_DESC;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LOSS_CAUSE_DESC;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DM_EXPOSURE_ID;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EXPOSURE_CREATE_DT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EXPOSURE_TYPE_DESC;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EXPOSURE_STATE_DESC;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EXPOSURE_CLOSE_DT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EXPOSURE_CLOSED_OUTCOME_DESC;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string RESERVE_AMT;

    
    }
}
