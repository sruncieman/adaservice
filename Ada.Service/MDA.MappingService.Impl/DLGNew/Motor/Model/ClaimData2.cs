﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using FileHelpers;
using System;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Pipeline.Model;

namespace MDA.MappingService.DLGNew.Motor.Model
{
    [DelimitedRecord(",")]
    [IgnoreFirst(1)]
    class ClaimData2
    {

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CLAIM_NUMBER;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DM_DRIVER_SK;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FIRST_NAME;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MIDDLE_NAME;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LAST_NAME;

        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? BIRTH_DATE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LICENCE_HELD_FOR_YEARS;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LICENSE_TYPE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LICENSE_ISSUER_CNTRY;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NATIONAL_INSURANCE_NO;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HOME_PHONE_NUMBER_TXT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string WORK_PHONE_NUMBER_TXT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CELL_PHONE_NUMBER_TXT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EMAIL_ADDRESS_LN_1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string OCCUPATION_TXT;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FAULT_VEHICLE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MAKE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MARQUE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MODEL;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string REGNO;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string YEAR;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SEATS_NUM;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CURRENT_VEHICLE_ADD_PHONE;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string NUM_OF_OCCUPANTS;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string JOURNEY_PURPOSE_DESC;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COLLISION_SPEED;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COLLISION_POINT1;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COLLISION_POINT2;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string INSURED_CLAIMING_DAMAGE_IND;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AIR_BAGS_DEPLOYED_IND;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string INCIDENT_REP_TO_POLICE;


    }
}
