﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.Aviva.Motor
{
    public class CustomStringConverter : ConverterBase
    {
        public override object StringToField(string value)
        {

            if (!string.IsNullOrEmpty(value))
            {
                value = value.ToUpper();
                value = value.Replace(" ", "");
            }

            return value;

        }
    }
}
