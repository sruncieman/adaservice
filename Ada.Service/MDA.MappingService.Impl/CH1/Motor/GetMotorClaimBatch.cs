﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FileHelpers;
using Ionic.Zip;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.MappingService.CH1.Motor.Model;
using MDA.Common.Server;
using MDA.Common;

namespace MDA.MappingService.CH1.Motor
{
    public class GetMotorClaimBatch
    {
        private static bool boolDebug = true;
        private Stopwatch stopwatch;
        private static bool isQudos;
        private static bool isAntilo;

        private string CheckFileExists(string rootpath, string[] filenames)
        {
            foreach (var file in filenames)
            {
                if (file.ToUpper().Contains("QUDOS"))
                    isQudos = true;

                if (file.ToUpper().Contains("ANTILO"))
                    isAntilo = true;

                if (File.Exists(Path.Combine(rootpath, file)))
                    return Path.Combine(rootpath, file);
            }
            return null;
        }

        public void RetrieveMotorClaimBatch(CurrentContext ctx, Stream filem, string clientFolder, bool deleteExtractedFiles, object statusTracking,
                        Func<CurrentContext, MDA.Pipeline.Model.PipelineMotorClaim, object, int> ProcessClaimFn,
                        ProcessingResults processingResults)
        {
            #region Initialise variables
            DateTime minDate = new DateTime(2007, 1, 1);
            string clientFile;
            string faultPartyFile;
            string referrerFile;
            string clientSolicitorFile;
            string clientEngineerFile;
            string clientRepairerFile;
            string TPInsurerFile;
            string TPBrokerFile;
            string TPSolicitorFile;
            string witnessFile1;
            string witnessFile2;
            string witnessFile3;
            bool OrganisationInsuredFound = false;



            List<Referrer> referrersList = new List<Referrer>();
            List<ClientSolicitor> clientSolicitorsList = new List<ClientSolicitor>();
            List<ClientEngineer> clientEngineersList = new List<ClientEngineer>();
            List<ClientRepairer> clientRepairersList = new List<ClientRepairer>();
            List<TPInsurer> tpInsurersList = new List<TPInsurer>();
            List<TPBroker> tpBrokersList = new List<TPBroker>();
            List<TPSolicitor> tpSolicitorsList = new List<TPSolicitor>();
            List<Client> filteredClients = new List<Client>();
            List<Witness> filteredWitnesses = new List<Witness>();
            List<Witness> witnessCollection = new List<Witness>();
            string filePath = null;

            Client[] clients;
            TPSolicitor[] tpSolicitors;
            TPBroker[] tpBrokers;
            FaultParty[] faultParties;
            Referrer[] referrers;
            ClientSolicitor[] clientSolicitors;
            ClientEngineer[] clientEngineers;
            ClientRepairer[] clientRepairers;
            TPInsurer[] tpInsurers;
            Witness[] witness1;
            Witness[] witness2;
            Witness[] witness3;
            #endregion

            #region Extract Files from Zip

            if (filem != null)
            {
                try
                {
                    boolDebug = false;

                    using (ZipFile zip1 = ZipFile.Read(filem)) //_filePath))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            var extractTmpPath = string.Concat(clientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);


                            string unpackDirectory = clientFolder;
                            e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in extraction of files: " + ex);
                }

            }

            #endregion

            if (boolDebug)
            {
                stopwatch = new Stopwatch();
                Console.WriteLine("Start");
                stopwatch.Start();
            }

            #region Files
            if (boolDebug)
            {

                clientFile = @"C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource\KEO Client.csv";
                faultPartyFile = @"C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource\KEO Third Party.csv";
                referrerFile = @"C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource\KEO Source.csv";
                clientSolicitorFile =
                    @"C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource\KEO Client Solicitor.csv";
                clientEngineerFile =
                    @"C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource\KEO Engineer.csv";
                clientRepairerFile =
                    @"C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource\KEO Repairer.csv";
                TPInsurerFile = @"C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource\KEO Third Party Insurer.csv";
                TPBrokerFile = @"C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource\KEO Third Party Broker.csv";
                TPSolicitorFile = @"C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource\KEO Third Party Solicitor.csv";
                witnessFile1 = @"C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource\KEO Witness1.csv";
                witnessFile2 = @"C:\Projects\ADA\MDASolution\MDA.Mapping.CH1\Resource\KEO_Witness2.csv";
                witnessFile3 = @"C:\Dev\Projects\MDA\MDASolution\MDA.Mapping.CH1\Resource\KEO_Witness3.csv";
            }
            else
            {
                try
                {
                    clientFile = CheckFileExists(clientFolder, new string[] { "KEO Client.csv", "KEO Client Qudos.csv", "KEO Client Antilo.csv" });
                    faultPartyFile = CheckFileExists(clientFolder, new string[] { "KEO Third Party.csv", "KEO Third Party Qudos.csv", "KEO Third Party Antilo.csv" });
                    referrerFile = CheckFileExists(clientFolder, new string[] { "KEO Source.csv", "KEO Source Qudos.csv", "KEO Source Antilo.csv" });
                    clientSolicitorFile = CheckFileExists(clientFolder, new string[] { "KEO Client Solicitor.csv", "", "KEO Client Solicitor Antilo.csv" });
                    clientEngineerFile = CheckFileExists(clientFolder, new string[] { "KEO Engineer.csv", "KEO Engineer Qudos.csv", "KEO Engineer Antilo.csv" });
                    clientRepairerFile = CheckFileExists(clientFolder, new string[] { "KEO Repairer.csv", "KEO Repairer Qudos.csv", "KEO Repairer Antilo.csv" });
                    TPInsurerFile = CheckFileExists(clientFolder, new string[] { "KEO Third Party Insurer.csv", "", "KEO Third Party Insurer Antilo.csv" });
                    TPBrokerFile = CheckFileExists(clientFolder, new string[] { "KEO Third Party Broker.csv", "", "" });
                    TPSolicitorFile = CheckFileExists(clientFolder, new string[] { "KEO Third Party Solicitor.csv", "KEO Third Party Solicitor Qudos.csv", "KEO Third Party Solicitor Antilo.csv" });
                    witnessFile1 = CheckFileExists(clientFolder, new string[] { "KEO Witness1.csv", "KEO Witness1 Qudos.csv", "KEO Witness1 Antilo.csv" });
                    witnessFile2 = CheckFileExists(clientFolder, new string[] { "KEO Witness2.csv", "", "" });
                    witnessFile3 = CheckFileExists(clientFolder, new string[] { "KEO Witness3.csv", "", "" });
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in assigning extracted files: " + ex);
                }
            }





            #endregion

            #region FileHelper Engines
            try
            {
                FileHelperEngine clientEngine = new FileHelperEngine(typeof(Client));
                FileHelperEngine faultPartyEngine = new FileHelperEngine(typeof(FaultParty));
                FileHelperEngine referrerEngine = new FileHelperEngine(typeof(Referrer));
                FileHelperEngine clientSolicitorEngine = new FileHelperEngine(typeof(ClientSolicitor));
                FileHelperEngine clientEngineerEngine = new FileHelperEngine(typeof(ClientEngineer));
                FileHelperEngine clientRepairerEngine = new FileHelperEngine(typeof(ClientRepairer));
                FileHelperEngine TPInsurerEngine = new FileHelperEngine(typeof(TPInsurer));
                FileHelperEngine TPBrokerEngine = new FileHelperEngine(typeof(TPBroker));
                FileHelperEngine TPSolicitorEngine = new FileHelperEngine(typeof(TPSolicitor));
                FileHelperEngine witnessEngine = new FileHelperEngine(typeof(Witness));

            #endregion

                #region Populate FileHelper Engines with data from files
                clients = (clientFile != null) ? clientEngine.ReadFile(clientFile) as Client[] : null;
                faultParties = (faultPartyFile != null) ? faultPartyEngine.ReadFile(faultPartyFile) as FaultParty[] : null;
                referrers = (referrerFile != null) ? referrerEngine.ReadFile(referrerFile) as Referrer[] : null;
                clientSolicitors = (clientSolicitorFile != null) ? clientSolicitorEngine.ReadFile(clientSolicitorFile) as ClientSolicitor[] : null;
                clientEngineers = (clientEngineerFile != null) ? clientEngineerEngine.ReadFile(clientEngineerFile) as ClientEngineer[] : null;
                clientRepairers = (clientRepairerFile != null) ? clientRepairerEngine.ReadFile(clientRepairerFile) as ClientRepairer[] : null;
                tpInsurers = (TPInsurerFile != null) ? TPInsurerEngine.ReadFile(TPInsurerFile) as TPInsurer[] : null;
                tpBrokers = (TPBrokerFile != null) ? TPBrokerEngine.ReadFile(TPBrokerFile) as TPBroker[] : null;
                tpSolicitors = (TPSolicitorFile != null) ? TPSolicitorEngine.ReadFile(TPSolicitorFile) as TPSolicitor[] : null;
                witness1 = (witnessFile1 != null) ? witnessEngine.ReadFile(witnessFile1) as Witness[] : null;
                witness2 = (witnessFile2 != null) ? witnessEngine.ReadFile(witnessFile2) as Witness[] : null;
                witness3 = (witnessFile3 != null) ? witnessEngine.ReadFile(witnessFile3) as Witness[] : null;
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in initialising FileHelper Engines: " + ex);
            }
                #endregion

            //DeleteExtractedFiles(clientFolder);

            // Remove extracted zip files
            DeleteExtractedZipFiles(clientFolder, true);
            #region Distinct Lookup Table Records

            try
            {
                // Get Distinct TP Solicitor records
                if (tpSolicitors != null)
                    tpSolicitorsList = tpSolicitors
                        .GroupBy(i => i.TPSolicitorId)
                        .Select(g => g.First())
                        .ToList();

                // Get Distinct TP Broker records
                if (tpBrokers != null)
                    tpBrokersList = tpBrokers
                        .GroupBy(i => i.TPBrokerId)
                        .Select(g => g.First())
                        .ToList();

                // Get Distinct TP Insurer records
                if (tpInsurers != null)
                    tpInsurersList = tpInsurers
                        .GroupBy(i => i.TPInsurerId)
                        .Select(g => g.First())
                        .ToList();

                // Get Distinct client repairer records
                if (clientRepairers != null)
                    clientRepairersList = clientRepairers
                        .GroupBy(i => i.ClientRepairerId)
                        .Select(g => g.First())
                        .ToList();

                // Get Disintct client engineer records
                if (clientEngineers != null)
                    clientEngineersList = clientEngineers
                        .GroupBy(i => i.ClientEngineerId)
                        .Select(g => g.First())
                        .ToList();

                // Get Distinct client solicitor records
                if (clientSolicitors != null)
                    clientSolicitorsList = clientSolicitors
                        .GroupBy(i => i.ClientSolicitorId)
                        .Select(g => g.First())
                        .ToList();

                // Get Distinct referrer records
                if (referrers != null)
                    referrersList = referrers
                        .GroupBy(i => i.RefCompId)
                        .Select(g => g.First())
                        .ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct lookup table values: " + ex);
            }

            #endregion

            #region Filter Records

            try
            {
                // Get client records with incident date < 01012007  and which don't include '-2' in case number from Client file

                if (clients != null)
                    foreach (Client client in clients)
                    {
                        if (client.AccDate > minDate && !client.CaseNumber.Contains("-2"))
                        {
                            filteredClients.Add(client);
                        }
                    }

                if (witness1 != null)
                    foreach (var witness in witness1)
                    {
                        if (!witness.WitnessCaseId.Contains("-2"))
                        {
                            filteredWitnesses.Add(witness);
                        }
                    }

                if (witness2 != null)
                    foreach (var witness in witness2)
                    {
                        if (!witness.WitnessCaseId.Contains("-2"))
                        {
                            filteredWitnesses.Add(witness);
                        }
                    }

                if (witness3 != null)
                    foreach (var witness in witness3)
                    {
                        if (!witness.WitnessCaseId.Contains("-2"))
                        {
                            filteredWitnesses.Add(witness);
                        }
                    }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in filtering records: " + ex);
            }

            #endregion

            #region Populate Client class List with data

            try
            {
                foreach (var filteredClient in filteredClients)
                {
                    //witnessCollection.Clear();

                    if (faultParties != null)
                    {
                        var faultParty = faultParties.Where(x => x.DAClaimCaseNo == filteredClient.CaseNumber);

                        if (faultParty != null)
                        {
                            filteredClient.lstThirdParties = new List<FaultParty>();

                            foreach (var party in faultParty)
                            {
                                filteredClient.lstThirdParties.Add(party);
                            }
                        }
                    }


                    if (filteredWitnesses != null)
                    {
                        var witnesses = filteredWitnesses.Where(x => x.WitnessCaseId == filteredClient.CaseNumber);

                        if (witnesses != null)
                        {
                            foreach (Witness witness in witnesses)
                            {
                                witnessCollection.Add(witness);
                            }
                        }
                    }

                    if (filteredClient.ReferrerId > 0)
                    {
                        var referrer = referrersList.FirstOrDefault(x => x.RefCompId == filteredClient.ReferrerId);
                        if (referrer != null)
                        {
                            filteredClient.RefCompName = referrer.RefCompName;
                            filteredClient.RefContactName = referrer.RefContactName;
                            filteredClient.RefAdd1 = referrer.RefAdd1;
                            filteredClient.RefAdd2 = referrer.RefAdd2;
                            filteredClient.RefAdd3 = referrer.RefAdd3;
                            filteredClient.RefAdd4 = referrer.RefAdd4;
                            filteredClient.RefAdd5 = referrer.RefAdd5;
                            filteredClient.RefEmail = referrer.RefEmail;
                            filteredClient.RefMobile = referrer.RefMobile;
                            filteredClient.RefTel = referrer.RefTel;
                        }
                    }

                    if (filteredClient.RepairerId > 0)
                    {
                        var clientRepairer =
                            clientRepairersList.FirstOrDefault(x => x.ClientRepairerId == filteredClient.RepairerId);
                        if (clientRepairer != null)
                        {
                            filteredClient.ClientRepairerFullName = clientRepairer.ClientRepairerFullName;
                            filteredClient.ClientRepairerAddr1 = clientRepairer.ClientRepairerAddr1;
                            filteredClient.ClientRepairerAddr2 = clientRepairer.ClientRepairerAddr2;
                            filteredClient.ClientRepairerAddr3 = clientRepairer.ClientRepairerAddr3;
                            filteredClient.ClientRepairerAddr4 = clientRepairer.ClientRepairerAddr4;
                            filteredClient.ClientRepairerAddr5 = clientRepairer.ClientRepairerAddr5;
                            filteredClient.ClientRepairerAddr6 = clientRepairer.ClientRepairerAddr6;
                            filteredClient.ClientRepairerPostcode = clientRepairer.ClientRepairerPostcode;
                            filteredClient.ClientRepairerTel = clientRepairer.ClientRepairerTel;
                            filteredClient.ClientRepairerFax = clientRepairer.ClientRepairerFax;
                        }
                    }

                    if (filteredClient.EngineerId > 0)
                    {
                        var clientEngineer =
                            clientEngineersList.FirstOrDefault(x => x.ClientEngineerId == filteredClient.EngineerId);
                        if (clientEngineer != null)
                        {
                            filteredClient.ClientEngineerName = clientEngineer.ClientEngineerName;
                            filteredClient.ClientEngineerAddr1 = clientEngineer.ClientEngineerAddr1;
                            filteredClient.ClientEngineerAddr2 = clientEngineer.ClientEngineerAddr2;
                            filteredClient.ClientEngineerAddr3 = clientEngineer.ClientEngineerAddr3;
                            filteredClient.ClientEngineerAddr4 = clientEngineer.ClientEngineerAddr4;
                            filteredClient.ClientEngineerAddr5 = clientEngineer.ClientEngineerAddr5;
                            filteredClient.ClientEngineerAddr6 = clientEngineer.ClientEngineerAddr6;
                            filteredClient.ClientEngineerPostcode = clientEngineer.ClientEngineerPostcode;
                            filteredClient.ClientEngineerTelephone = clientEngineer.ClientEngineerTelephone;
                            filteredClient.ClientEngineerFax = clientEngineer.ClientEngineerFax;
                        }

                    }
                    if (filteredClient.SolicitorId > 0)
                    {
                        var clientSolicitor =
                            clientSolicitorsList.FirstOrDefault(x => x.ClientSolicitorId == filteredClient.SolicitorId);
                        if (clientSolicitor != null)
                        {
                            filteredClient.ClientSolicitorFullName = clientSolicitor.ClientSolicitorFullName;
                            filteredClient.ClientSolicitorAddr1 = clientSolicitor.ClientSolicitorAddr1;
                            filteredClient.ClientSolicitorAddr2 = clientSolicitor.ClientSolicitorAddr2;
                            filteredClient.ClientSolicitorAddr3 = clientSolicitor.ClientSolicitorAddr3;
                            filteredClient.ClientSolicitorAddr4 = clientSolicitor.ClientSolicitorAddr4;
                            filteredClient.ClientSolicitorAddr5 = clientSolicitor.ClientSolicitorAddr5;
                            filteredClient.ClientSolicitorAddr6 = clientSolicitor.ClientSolicitorAddr6;
                            filteredClient.ClientSolicitorTel = clientSolicitor.ClientSolicitorTel;
                            filteredClient.ClientSolicitorFax = clientSolicitor.ClientSolicitorFax;
                        }
                    }

                    filteredClient.InsurerName = (isQudos) ? "Qudos" : "Enterprise";
                    if (isAntilo)
                    {
                        filteredClient.InsurerName = "Antilo";
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in populating client class with lookup data: " + ex);
            }

            #endregion

            //PipelineClaimBatch cb = new PipelineClaimBatch();

            #region Translate to XML

            foreach (var record in filteredClients)
            {
                #region GeneralClaimData
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();
                try
                {
                    OrganisationInsuredFound = false;

                    motorClaim.ClaimNumber = record.CaseNumber;
                    motorClaim.ClaimType_Id = (int)MDA.Common.Enum.ClaimType.Motor;
                    if (record.AccDate != null) motorClaim.IncidentDate = (DateTime)record.AccDate;

                    #region Incident Time
                    //TimeSpan time = new TimeSpan();
                    //if (record.AccTime != null)
                    //{
                    //    string checkTime = record.AccTime;
                    //    if (checkTime.Length > 3 && checkTime.Length < 6 && checkTime.Substring(0,1) != "." && Regex.Matches(checkTime,@"[a-zA-Z]").Count < 1)
                    //    {


                    //        int hours = 0;
                    //        int mins = 0;

                    //        if (checkTime.Contains(".") || checkTime.Contains(":"))
                    //        {

                    //            string[] getTime = null;


                    //            if (checkTime.Contains("."))
                    //                getTime = checkTime.Split('.');

                    //            if (checkTime.Contains(":"))
                    //                getTime = checkTime.Split(':');


                    //            if (getTime.Count() < 2)
                    //            {
                    //                if (getTime[0].Length == 1)
                    //                {
                    //                    hours = Convert.ToInt32(getTime[0].ToString().PadLeft(1, '0'));
                    //                }
                    //                else
                    //                    hours = Convert.ToInt32(checkTime.Substring(0, 2));

                    //                if (getTime[1].Length == 1)
                    //                {
                    //                    mins = Convert.ToInt32(getTime[0].ToString().PadLeft(1, '0'));
                    //                }
                    //                else if (getTime[0].Length == 2)
                    //                    mins = Convert.ToInt32(checkTime.Substring(3, 2));
                    //                else
                    //                {
                    //                    mins = Convert.ToInt32(checkTime.Substring(2, 2));
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if (Regex.Matches(checkTime, @"[a-zA-Z]").Count < 1)

                    //            hours = Convert.ToInt32(checkTime.Substring(0, 2));
                    //            mins = Convert.ToInt32(checkTime.Substring(2, 2));
                    //        }

                    //        time = new TimeSpan(hours, mins, 0);
                    //    }
                    //    record.AccDate = Convert.ToDateTime(record.AccDate).Add(time);
                    //    motorClaim.IncidentDate = (DateTime) record.AccDate;
                    //}
                    #endregion
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general claim data: " + ex);
                }

                #endregion

                #region ClaimInfo
                try
                {
                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                    motorClaim.ExtraClaimInfo.IncidentLocation = record.Location;
                    motorClaim.ExtraClaimInfo.IncidentCircumstances = record.AccidentDesc;
                    if (!string.IsNullOrEmpty(record.PoliceRef))
                    {
                        motorClaim.ExtraClaimInfo.PoliceAttended = true;
                        motorClaim.ExtraClaimInfo.PoliceReference = record.PoliceRef;
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general claim info: " + ex);
                }

                #endregion

                #region Policy

                try
                {
                    motorClaim.Policy.Insurer = (isQudos) ? "Qudos" : "Enterprise";
                    if (isAntilo)
                    {
                        motorClaim.Policy.Insurer = "Antilo";
                    }
                    //motorClaim.Policy.PolicyNumber = record.InsPol;
                    motorClaim.Policy.PolicyType_Id = (int)PolicyType.Unknown;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating policy: " + ex);
                }

                #endregion

                #region Organisation

                try
                {
                    #region Client Organisation Claim Level

                    if (string.IsNullOrEmpty(record.PHTitle) && string.IsNullOrEmpty(record.PHForename) &&
                        string.IsNullOrEmpty(record.PHInitials) && !string.IsNullOrEmpty(record.PHLastName))
                    {
                        OrganisationInsuredFound = true;

                        PipelineOrganisation clientOrg = new PipelineOrganisation();
                        PipelineAddress clientAddress = new PipelineAddress();

                        clientAddress.AddressType_Id = (int)AddressLinkType.TradingAddress;

                        clientOrg.OrganisationName = record.PHLastName;
                        if (!string.IsNullOrEmpty(record.Telephone))
                            clientOrg.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                        //clientOrg.Telephone1 = record.Telephone;

                        if (!string.IsNullOrEmpty(record.Mobile))
                            clientOrg.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.Mobile, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                        //clientOrg.Telephone2 = record.Mobile;

                        clientOrg.OrganisationType_Id = (int)OrganisationType.PolicyHolder;

                        if (!string.IsNullOrEmpty(record.Address1))
                            clientAddress.SubBuilding = record.Address1;

                        if (!string.IsNullOrEmpty(record.Address2))
                            clientAddress.Building = record.Address2;

                        if (!string.IsNullOrEmpty(record.Address3))
                            clientAddress.BuildingNumber = record.Address3;

                        if (!string.IsNullOrEmpty(record.Address4))
                            clientAddress.Street = record.Address4;

                        if (!string.IsNullOrEmpty(record.Address5))
                            clientAddress.Locality = record.Address5;

                        if (!string.IsNullOrEmpty(record.Postcode))
                            clientAddress.PostCode = record.Postcode;

                        if (clientOrg.OrganisationName != null)
                        {
                            if (clientAddress.Street != null || clientAddress.SubBuilding != null || clientAddress.Building != null || clientAddress.BuildingNumber != null || clientAddress.Locality != null || clientAddress.PostCode != null)
                                clientOrg.Addresses.Add(clientAddress);

                            motorClaim.Organisations.Add(clientOrg);
                        }
                    }

                    #endregion

                    #region FaultParty Organisation Claim Level
                    if (record.lstThirdParties != null)
                    {
                        foreach (var thirdParty in record.lstThirdParties)
                        {
                            if (string.IsNullOrEmpty(thirdParty.FPTitle) && string.IsNullOrEmpty(thirdParty.FPFirstName) &&
                                string.IsNullOrEmpty(thirdParty.FPInitals) && !string.IsNullOrEmpty(thirdParty.FPLastName))
                            {

                                thirdParty.boolClaimLevelOrgFound = true;

                                PipelineOrganisation faultPartyOrg = new PipelineOrganisation();
                                PipelineAddress faultPartyAddress = new PipelineAddress();

                                faultPartyAddress.AddressType_Id = (int)AddressLinkType.TradingAddress;

                                faultPartyOrg.OrganisationName = thirdParty.FPLastName;
                                if (!string.IsNullOrEmpty(thirdParty.FPTel))
                                    faultPartyOrg.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = thirdParty.FPTel, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                //faultPartyOrg.Telephone1 = thirdParty.FPTel;

                                if (!string.IsNullOrEmpty(thirdParty.FPMobile))
                                    faultPartyOrg.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = thirdParty.FPMobile, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                                //faultPartyOrg.Telephone2 = thirdParty.FPMobile;

                                faultPartyOrg.OrganisationType_Id = (int)OrganisationType.Unknown;

                                if (!string.IsNullOrEmpty(thirdParty.FPAddress1))
                                    faultPartyAddress.SubBuilding = thirdParty.FPAddress1;

                                if (!string.IsNullOrEmpty(thirdParty.FPAddress2))
                                    faultPartyAddress.Building = thirdParty.FPAddress2;

                                if (!string.IsNullOrEmpty(thirdParty.FPAddress3))
                                    faultPartyAddress.BuildingNumber = thirdParty.FPAddress3;

                                if (!string.IsNullOrEmpty(thirdParty.FPAddress4))
                                    faultPartyAddress.Street = thirdParty.FPAddress4;

                                if (!string.IsNullOrEmpty(thirdParty.FPPostCode))
                                    faultPartyAddress.PostCode = thirdParty.FPPostCode;

                                if (faultPartyOrg.OrganisationName != null)
                                {
                                    if (faultPartyAddress.Street != null || faultPartyAddress.SubBuilding != null || faultPartyAddress.Building != null || faultPartyAddress.BuildingNumber != null || faultPartyAddress.Locality != null || faultPartyAddress.PostCode != null)
                                        faultPartyOrg.Addresses.Add(faultPartyAddress);

                                    motorClaim.Organisations.Add(faultPartyOrg);
                                }
                            }
                        }
                    }

                    #endregion Claim Level
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating organisation: " + ex);
                }


                #endregion

                #region Insured Vehicle

                try
                {

                    PipelineVehicle insuredVehicle = new PipelineVehicle();
                    PipelinePerson insuredVPerson = new PipelinePerson();
                    PipelineAddress insuredVPAddress = new PipelineAddress();
                    PipelineOrganisation insuredVPOrganisationReferrer = new PipelineOrganisation();
                    PipelineAddress insuredVPOrgAddressReferrer = new PipelineAddress();
                    PipelineOrganisation insuredVPOrganisationEngineer = new PipelineOrganisation();
                    PipelineAddress insuredVPOrgAddressEngineer = new PipelineAddress();
                    PipelineOrganisation insuredVPOrganisationSolicitor = new PipelineOrganisation();
                    PipelineAddress insuredVPOrgAddressSolicitor = new PipelineAddress();
                    PipelineOrganisation insuredVPOrganisationRepairer = new PipelineOrganisation();
                    PipelineAddress insuredVPOrgAddressRepairer = new PipelineAddress();
                    PipelineOrganisation insuredVPOrganisationBroker = new PipelineOrganisation();
                    PipelineAddress insuredVPOrgAddressBroker = new PipelineAddress();

                    insuredVPOrgAddressReferrer.AddressType_Id = (int)AddressLinkType.TradingAddress;
                    insuredVPOrgAddressEngineer.AddressType_Id = (int)AddressLinkType.TradingAddress;
                    insuredVPOrgAddressSolicitor.AddressType_Id = (int)AddressLinkType.TradingAddress;
                    insuredVPOrgAddressRepairer.AddressType_Id = (int)AddressLinkType.TradingAddress;
                    insuredVPOrgAddressBroker.AddressType_Id = (int)AddressLinkType.TradingAddress;

                    #region Insured Vehicle Details

                    if (!string.IsNullOrEmpty(record.Make))
                        insuredVehicle.VehicleMake = record.Make;

                    if (!string.IsNullOrEmpty(record.Model))
                        insuredVehicle.VehicleModel = record.Model;

                    if (!string.IsNullOrEmpty(record.Reg))
                        insuredVehicle.VehicleRegistration = record.Reg.ToUpper();

                    insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                    if (!string.IsNullOrEmpty(record.Damage))
                        insuredVehicle.I2V_LinkData.DamageDescription = record.Damage;

                    #endregion

                    #region Insured Vehicle Person

                    if (!OrganisationInsuredFound)
                    {
                        //if (!string.IsNullOrEmpty(record.PHForename) && !string.IsNullOrEmpty(record.PHLastName))
                        //{
                        insuredVPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        insuredVPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                        //}

                        if (!string.IsNullOrEmpty(record.PHTitle))
                        {
                            switch (record.PHTitle)
                            {
                                case "Mr":
                                case "Me":
                                case "Mr.":
                                    insuredVPerson.Salutation_Id = (int)Salutation.Mr;
                                    insuredVPerson.Gender_Id = (int)Gender.Male;
                                    break;
                                case "Mrs":
                                    insuredVPerson.Salutation_Id = (int)Salutation.Mrs;
                                    insuredVPerson.Gender_Id = (int)Gender.Female;
                                    break;
                                case "Ms":
                                    insuredVPerson.Salutation_Id = (int)Salutation.Ms;
                                    insuredVPerson.Gender_Id = (int)Gender.Female;
                                    break;
                                case "Miss":
                                    insuredVPerson.Salutation_Id = (int)Salutation.Miss;
                                    insuredVPerson.Gender_Id = (int)Gender.Female;
                                    break;
                                case "Master":
                                    insuredVPerson.Salutation_Id = (int)Salutation.Master;
                                    break;
                                case "Dr":
                                case "Doctor":
                                    insuredVPerson.Salutation_Id = (int)Salutation.Dr;
                                    break;
                                case "Professor":
                                case "Prof":
                                    insuredVPerson.Salutation_Id = (int)Salutation.Professor;
                                    break;
                                case "Reverend":
                                case "Rev":
                                    insuredVPerson.Salutation_Id = (int)Salutation.Reverend;
                                    break;
                                case "Sir":
                                    insuredVPerson.Salutation_Id = (int)Salutation.Sir;
                                    insuredVPerson.Gender_Id = (int)Gender.Male;
                                    break;
                                case "Lord":
                                    insuredVPerson.Salutation_Id = (int)Salutation.Lord;
                                    insuredVPerson.Gender_Id = (int)Gender.Male;
                                    break;
                                case "Baroness":
                                    insuredVPerson.Salutation_Id = (int)Salutation.Baroness;
                                    insuredVPerson.Gender_Id = (int)Gender.Female;
                                    break;
                                case "Right Honourable":
                                    insuredVPerson.Salutation_Id = (int)Salutation.RightHonourable;
                                    break;
                                default:
                                    insuredVPerson.Salutation_Id = (int)Salutation.Unknown;
                                    break;
                            }
                        }

                        if (!string.IsNullOrEmpty(record.PHForename))
                        {
                            if (record.PHForename.Contains(" "))
                            {
                                string[] names = record.PHForename.Split(' ');
                                insuredVPerson.FirstName = names[0];
                                insuredVPerson.MiddleName = names[1];
                            }
                            else
                            {
                                insuredVPerson.FirstName = record.PHForename;
                            }
                        }

                        if (!string.IsNullOrEmpty(record.PHLastName))
                            insuredVPerson.LastName = record.PHLastName;

                        if (record.DOB != null)
                            insuredVPerson.DateOfBirth = record.DOB;

                        if (!string.IsNullOrEmpty(record.Telephone))
                            insuredVPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.Telephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                        //insuredVPerson.LandlineTelephone = record.Telephone;

                        if (!string.IsNullOrEmpty(record.Mobile))
                            insuredVPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.Mobile, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                        //insuredVPerson.MobileTelephone = record.Mobile;
                    }

                    #endregion

                    #region Insured Vehicle Person Address

                    if (!string.IsNullOrEmpty(record.Address1))
                        insuredVPAddress.SubBuilding = record.Address1;

                    if (!string.IsNullOrEmpty(record.Address2))
                        insuredVPAddress.Building = record.Address2;

                    if (!string.IsNullOrEmpty(record.Address3))
                        insuredVPAddress.BuildingNumber = record.Address3;

                    if (!string.IsNullOrEmpty(record.Address4))
                        insuredVPAddress.Street = record.Address4;

                    if (!string.IsNullOrEmpty(record.Address5))
                        insuredVPAddress.Locality = record.Address5;

                    if (!string.IsNullOrEmpty(record.Postcode))
                        insuredVPAddress.PostCode = record.Postcode;

                    #endregion

                    #region Insured Vehicle Person Organisation and Address

                    #region Referrer

                    if (record.ReferrerId > 0)
                    {
                        insuredVPOrganisationReferrer.P2O_LinkData.Person2OrganisationLinkType_Id =
                            (int)Person2OrganisationLinkType.ReferralSource;
                        insuredVPOrganisationReferrer.OrganisationType_Id = (int)OrganisationType.Unknown;
                        if (!string.IsNullOrEmpty(record.RefCompName))
                            insuredVPOrganisationReferrer.OrganisationName = record.RefCompName;

                        if (!string.IsNullOrEmpty(record.RefTel))
                            insuredVPOrganisationReferrer.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.RefTel, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                        //insuredVPOrganisationReferrer.Telephone1 = record.RefTel;

                        if (!string.IsNullOrEmpty(record.RefMobile))
                            insuredVPOrganisationReferrer.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.RefMobile, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                        //insuredVPOrganisationReferrer.Telephone2 = record.RefMobile;

                        if (!string.IsNullOrEmpty(record.RefAdd1))
                            insuredVPOrgAddressReferrer.SubBuilding = record.RefAdd1;

                        if (!string.IsNullOrEmpty(record.RefAdd2))
                            insuredVPOrgAddressReferrer.Building = record.RefAdd2;

                        if (!string.IsNullOrEmpty(record.RefAdd3))
                            insuredVPOrgAddressReferrer.BuildingNumber = record.RefAdd3;

                        if (!string.IsNullOrEmpty(record.RefAdd4))
                            insuredVPOrgAddressReferrer.Street = record.RefAdd4;

                        if (!string.IsNullOrEmpty(record.RefAdd5))
                            insuredVPOrgAddressReferrer.Locality = record.RefAdd5;

                        if (!string.IsNullOrEmpty(record.RefAdd6))
                            insuredVPOrgAddressReferrer.Town = record.RefAdd6;
                    }

                    #endregion

                    #region Engineer

                    if (record.EngineerId > 0)
                    {
                        insuredVPOrganisationEngineer.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Engineer;
                        insuredVPOrganisationEngineer.OrganisationType_Id = (int)OrganisationType.VehicleEngineer;

                        if (!string.IsNullOrEmpty(record.ClientEngineerName))
                            insuredVPOrganisationEngineer.OrganisationName = record.ClientEngineerName;

                        if (!string.IsNullOrEmpty(record.ClientEngineerTelephone))
                            insuredVPOrganisationEngineer.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.ClientEngineerTelephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                        //insuredVPOrganisationEngineer.Telephone1 = record.ClientEngineerTelephone;

                        if (!string.IsNullOrEmpty(record.ClientEngineerFax))
                            insuredVPOrganisationEngineer.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.ClientEngineerFax, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.FAX });
                        //insuredVPOrganisationEngineer.Telephone2 = record.ClientEngineerFax;

                        if (!string.IsNullOrEmpty(record.ClientEngineerAddr1))
                            insuredVPOrgAddressEngineer.SubBuilding = record.ClientEngineerAddr1;

                        if (!string.IsNullOrEmpty(record.ClientEngineerAddr2))
                            insuredVPOrgAddressEngineer.Building = record.ClientEngineerAddr2;

                        if (!string.IsNullOrEmpty(record.ClientEngineerAddr3))
                            insuredVPOrgAddressEngineer.BuildingNumber = record.ClientEngineerAddr3;

                        if (!string.IsNullOrEmpty(record.ClientEngineerAddr4))
                            insuredVPOrgAddressEngineer.Street = record.ClientEngineerAddr4;

                        if (!string.IsNullOrEmpty(record.ClientEngineerAddr5))
                            insuredVPOrgAddressEngineer.Locality = record.ClientEngineerAddr5;

                        if (!string.IsNullOrEmpty(record.ClientEngineerAddr6))
                            insuredVPOrgAddressEngineer.Town = record.ClientEngineerAddr6;

                        if (!string.IsNullOrEmpty(record.ClientEngineerPostcode))
                            insuredVPOrgAddressEngineer.PostCode = record.ClientEngineerPostcode;
                    }

                    #endregion

                    #region Solicitor

                    if (record.SolicitorId > 0)
                    {
                        insuredVPOrganisationSolicitor.P2O_LinkData.Person2OrganisationLinkType_Id =
                            (int)Person2OrganisationLinkType.Solicitor;
                        insuredVPOrganisationSolicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;

                        if (!string.IsNullOrEmpty(record.ClientSolicitorFullName))
                            insuredVPOrganisationSolicitor.OrganisationName = record.ClientSolicitorFullName;

                        if (!string.IsNullOrEmpty(record.ClientSolicitorTel))
                            insuredVPOrganisationSolicitor.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.ClientSolicitorTel, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                        //insuredVPOrganisationSolicitor.Telephone1 = record.ClientSolicitorTel;

                        if (!string.IsNullOrEmpty(record.ClientSolicitorFax))
                            insuredVPOrganisationSolicitor.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.ClientSolicitorFax, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.FAX });
                        //insuredVPOrganisationSolicitor.Telephone2 = record.ClientSolicitorFax;

                        if (!string.IsNullOrEmpty(record.ClientSolicitorAddr1))
                            insuredVPOrgAddressSolicitor.SubBuilding = record.ClientSolicitorAddr1;

                        if (!string.IsNullOrEmpty(record.ClientSolicitorAddr2))
                            insuredVPOrgAddressSolicitor.Building = record.ClientSolicitorAddr2;

                        if (!string.IsNullOrEmpty(record.ClientSolicitorAddr3))
                            insuredVPOrgAddressSolicitor.BuildingNumber = record.ClientSolicitorAddr3;

                        if (!string.IsNullOrEmpty(record.ClientSolicitorAddr4))
                            insuredVPOrgAddressSolicitor.Street = record.ClientSolicitorAddr4;

                        if (!string.IsNullOrEmpty(record.ClientSolicitorAddr5))
                            insuredVPOrgAddressSolicitor.Locality = record.ClientSolicitorAddr5;

                        if (!string.IsNullOrEmpty(record.ClientSolicitorAddr6))
                            insuredVPOrgAddressSolicitor.Town = record.ClientSolicitorAddr6;
                    }

                    #endregion

                    #region Repairer

                    if (record.RepairerId > 0)
                    {
                        insuredVPOrganisationRepairer.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Repairer;
                        insuredVPOrganisationRepairer.OrganisationType_Id = (int)OrganisationType.Repairer;

                        if (!string.IsNullOrEmpty(record.ClientRepairerFullName))
                            insuredVPOrganisationRepairer.OrganisationName = record.ClientRepairerFullName;

                        if (!string.IsNullOrEmpty(record.ClientRepairerAddr1))
                            insuredVPOrgAddressRepairer.SubBuilding = record.ClientRepairerAddr1;

                        if (!string.IsNullOrEmpty(record.ClientRepairerAddr2))
                            insuredVPOrgAddressRepairer.Building = record.ClientRepairerAddr2;

                        if (!string.IsNullOrEmpty(record.ClientRepairerAddr3))
                            insuredVPOrgAddressRepairer.BuildingNumber = record.ClientRepairerAddr3;

                        if (!string.IsNullOrEmpty(record.ClientRepairerAddr4))
                            insuredVPOrgAddressRepairer.Street = record.ClientRepairerAddr4;

                        if (!string.IsNullOrEmpty(record.ClientRepairerAddr5))
                            insuredVPOrgAddressRepairer.Locality = record.ClientRepairerAddr5;

                        if (!string.IsNullOrEmpty(record.ClientRepairerAddr6))
                            insuredVPOrgAddressRepairer.Town = record.ClientRepairerAddr6;

                        if (!string.IsNullOrEmpty(record.ClientRepairerPostcode))
                            insuredVPOrgAddressRepairer.PostCode = record.ClientRepairerPostcode;

                        if (!string.IsNullOrEmpty(record.ClientRepairerTel))
                            insuredVPOrganisationRepairer.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.ClientRepairerTel, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                        //insuredVPOrganisationRepairer.Telephone1 = record.ClientRepairerTel;

                        if (!string.IsNullOrEmpty(record.ClientRepairerFax))
                            insuredVPOrganisationRepairer.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = record.ClientRepairerFax, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.FAX });
                        //insuredVPOrganisationRepairer.Telephone2 = record.ClientRepairerFax;
                    }

                    #endregion


                    #endregion

                #endregion

                    if (insuredVPerson != null)
                    {
                        //if (insuredVPAddress.Street != null && !OrganisationInsuredFound)
                        if (insuredVPAddress.Street != null || insuredVPAddress.SubBuilding != null || insuredVPAddress.Building != null || insuredVPAddress.BuildingNumber != null || insuredVPAddress.Locality != null || insuredVPAddress.Town != null || insuredVPAddress.PostCode != null && !OrganisationInsuredFound)
                            insuredVPerson.Addresses.Add(insuredVPAddress);

                        if (insuredVPOrganisationReferrer.OrganisationName != null)
                            insuredVPerson.Organisations.Add(insuredVPOrganisationReferrer);

                        if (insuredVPOrgAddressReferrer.Street != null || insuredVPOrgAddressReferrer.SubBuilding != null || insuredVPOrgAddressReferrer.Building != null || insuredVPOrgAddressReferrer.BuildingNumber != null || insuredVPOrgAddressReferrer.Locality != null || insuredVPOrgAddressReferrer.Town != null || insuredVPOrgAddressReferrer.PostCode != null)
                            insuredVPOrganisationReferrer.Addresses.Add(insuredVPOrgAddressReferrer);

                        if (insuredVPOrganisationEngineer.OrganisationName != null)
                            insuredVPerson.Organisations.Add(insuredVPOrganisationEngineer);

                        if (insuredVPOrgAddressEngineer.Street != null || insuredVPOrgAddressEngineer.SubBuilding != null || insuredVPOrgAddressEngineer.Building != null || insuredVPOrgAddressEngineer.BuildingNumber != null || insuredVPOrgAddressEngineer.Locality != null || insuredVPOrgAddressEngineer.Town != null || insuredVPOrgAddressEngineer.PostCode != null)
                            insuredVPOrganisationEngineer.Addresses.Add(insuredVPOrgAddressEngineer);

                        if (insuredVPOrganisationSolicitor.OrganisationName != null)
                            insuredVPerson.Organisations.Add(insuredVPOrganisationSolicitor);

                        if (insuredVPOrgAddressSolicitor.Street != null || insuredVPOrgAddressSolicitor.SubBuilding != null || insuredVPOrgAddressSolicitor.Building != null || insuredVPOrgAddressSolicitor.BuildingNumber != null || insuredVPOrgAddressSolicitor.Locality != null || insuredVPOrgAddressSolicitor.Town != null || insuredVPOrgAddressSolicitor.PostCode != null)
                            insuredVPOrganisationSolicitor.Addresses.Add(insuredVPOrgAddressSolicitor);

                        if (insuredVPOrganisationRepairer.OrganisationName != null)
                            insuredVPerson.Organisations.Add(insuredVPOrganisationRepairer);

                        if (insuredVPOrgAddressRepairer.Street != null || insuredVPOrgAddressRepairer.SubBuilding != null || insuredVPOrgAddressRepairer.Building != null || insuredVPOrgAddressRepairer.BuildingNumber != null || insuredVPOrgAddressRepairer.Locality != null || insuredVPOrgAddressRepairer.Town != null || insuredVPOrgAddressRepairer.PostCode != null)
                            insuredVPOrganisationRepairer.Addresses.Add(insuredVPOrgAddressRepairer);

                        if (insuredVPOrganisationBroker.OrganisationName != null)
                            insuredVPerson.Organisations.Add(insuredVPOrganisationBroker);

                        if (insuredVPOrgAddressBroker.Street != null || insuredVPOrgAddressBroker.SubBuilding != null || insuredVPOrgAddressBroker.Building != null || insuredVPOrgAddressBroker.BuildingNumber != null || insuredVPOrgAddressBroker.Locality != null || insuredVPOrgAddressBroker.Town != null || insuredVPOrgAddressBroker.PostCode != null)
                            insuredVPOrganisationBroker.Addresses.Add(insuredVPOrgAddressBroker);

                        if (OrganisationInsuredFound && insuredVPerson.Organisations.Any())
                        {
                            // blank out person details
                            insuredVPerson.BankAccounts.Clear(); // = null;
                            insuredVPerson.DateOfBirth = null;
                            insuredVPerson.DrivingLicenseNumbers.Clear(); // = null;
                            insuredVPerson.EmailAddresses.Clear(); // = null;
                            insuredVPerson.FirstName = null;
                            insuredVPerson.Gender_Id = (int)Gender.Unknown;
                            insuredVPerson.Telephones.Clear(); // = null;
                            insuredVPerson.LastName = null;
                            insuredVPerson.MiddleName = null;
                            //insuredVPerson.MobileTelephone = null;
                            insuredVPerson.NINumbers.Clear(); // = null;
                            insuredVPerson.Nationality = null;
                            insuredVPerson.Occupation = null;
                            insuredVPerson.Operation = (int)Operation.Normal;
                            //insuredVPerson.OtherTelephone = null;
                            //insuredVPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                            insuredVPerson.PassportNumbers.Clear(); // = null;
                            insuredVPerson.PaymentCards.Clear(); // = null;
                            insuredVPerson.Salutation_Id = (int)Salutation.Unknown;
                            //insuredVPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                            //insuredVPerson.WorkTelephone = null;

                            insuredVehicle.People.Add(insuredVPerson);
                        }

                        if (!OrganisationInsuredFound)
                        {
                            if ((!string.IsNullOrEmpty(insuredVPerson.FirstName) && !string.IsNullOrEmpty(insuredVPerson.LastName)) || insuredVPerson.Organisations.Any())
                                insuredVehicle.People.Add(insuredVPerson);
                        }

                    }

                    //if (insuredVPAddress.Street != null || insuredVPerson.Organisations.Any())
                    if (insuredVehicle.VehicleRegistration != null || (insuredVPerson.FirstName != null && insuredVPerson.LastName != null) || insuredVPerson.Organisations.Any())
                        motorClaim.Vehicles.Add(insuredVehicle);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating Insured Vehicle: " + ex);
                }

                #region TP Vehicle

                try
                {

                    if (record.lstThirdParties != null)
                    {
                        foreach (var thirdParty in record.lstThirdParties)
                        {
                            PipelineVehicle TPVehicle = new PipelineVehicle();
                            PipelinePerson TPVPerson = new PipelinePerson();
                            PipelineAddress TPVPAddress = new PipelineAddress();
                            PipelineOrganisation TPVPOrganisationSolicitor = new PipelineOrganisation();
                            PipelineAddress TPVPOrgAddressSolicitor = new PipelineAddress();
                            PipelineOrganisation TPVPOrganisationBroker = new PipelineOrganisation();
                            PipelineAddress TPVPOrgAddressBroker = new PipelineAddress();
                            PipelineOrganisation TPVPOrganisationInsurer = new PipelineOrganisation();
                            PipelineAddress TPVPOrgAddressInsurer = new PipelineAddress();

                            #region TP Vehicle Details

                            if (!string.IsNullOrEmpty(thirdParty.FPVehicleMake))
                                TPVehicle.VehicleMake = thirdParty.FPVehicleMake;

                            if (!string.IsNullOrEmpty(thirdParty.FPVehicleModel))
                                TPVehicle.VehicleModel = thirdParty.FPVehicleModel;

                            if (!string.IsNullOrEmpty(thirdParty.FPVehicleReg))
                                TPVehicle.VehicleRegistration = thirdParty.FPVehicleReg.ToUpper();

                            TPVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;

                            #endregion

                            #region TP Vehicle Person

                            //if (!OrganisationTPFound)
                            //{
                            if (!string.IsNullOrEmpty(thirdParty.FPFirstName) &&
                                !string.IsNullOrEmpty(thirdParty.FPLastName))
                            {
                                TPVPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;

                                if (record.lstThirdParties.Count == 1)
                                    TPVPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                                if (record.lstThirdParties.Count > 1)
                                    TPVPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                            }

                            if (!string.IsNullOrEmpty(thirdParty.FPTitle))
                            {
                                switch (thirdParty.FPTitle)
                                {
                                    case "Mr":
                                    case "Me":
                                    case "Mr.":
                                        TPVPerson.Salutation_Id = (int)Salutation.Mr;
                                        TPVPerson.Gender_Id = (int)Gender.Male;
                                        break;
                                    case "Mrs":
                                        TPVPerson.Salutation_Id = (int)Salutation.Mrs;
                                        TPVPerson.Gender_Id = (int)Gender.Female;
                                        break;
                                    case "Ms":
                                        TPVPerson.Salutation_Id = (int)Salutation.Ms;
                                        TPVPerson.Gender_Id = (int)Gender.Female;
                                        break;
                                    case "Miss":
                                        TPVPerson.Salutation_Id = (int)Salutation.Miss;
                                        TPVPerson.Gender_Id = (int)Gender.Female;
                                        break;
                                    case "Master":
                                        TPVPerson.Salutation_Id = (int)Salutation.Master;
                                        break;
                                    case "Dr":
                                    case "Doctor":
                                        TPVPerson.Salutation_Id = (int)Salutation.Dr;
                                        break;
                                    case "Professor":
                                    case "Prof":
                                        TPVPerson.Salutation_Id = (int)Salutation.Professor;
                                        break;
                                    case "Reverend":
                                    case "Rev":
                                        TPVPerson.Salutation_Id = (int)Salutation.Reverend;
                                        break;
                                    case "Sir":
                                        TPVPerson.Salutation_Id = (int)Salutation.Sir;
                                        TPVPerson.Gender_Id = (int)Gender.Male;
                                        break;
                                    case "Lord":
                                        TPVPerson.Salutation_Id = (int)Salutation.Lord;
                                        TPVPerson.Gender_Id = (int)Gender.Male;
                                        break;
                                    case "Baroness":
                                        TPVPerson.Salutation_Id = (int)Salutation.Baroness;
                                        TPVPerson.Gender_Id = (int)Gender.Female;
                                        break;
                                    case "Right Honourable":
                                        TPVPerson.Salutation_Id = (int)Salutation.RightHonourable;
                                        break;
                                    default:
                                        TPVPerson.Salutation_Id = (int)Salutation.Unknown;
                                        break;
                                }
                            }

                            if (!string.IsNullOrEmpty(thirdParty.FPFirstName))
                            {
                                if (thirdParty.FPFirstName.Contains(" "))
                                {
                                    string[] names = thirdParty.FPFirstName.Split(' ');
                                    TPVPerson.FirstName = names[0];
                                    TPVPerson.MiddleName = names[1];
                                }
                                else
                                {
                                    TPVPerson.FirstName = thirdParty.FPFirstName;
                                }
                            }

                            if (!string.IsNullOrEmpty(thirdParty.FPLastName))
                                TPVPerson.LastName = thirdParty.FPLastName;

                            if (!string.IsNullOrEmpty(thirdParty.FPTel))
                                TPVPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = thirdParty.FPTel, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                            //TPVPerson.LandlineTelephone = thirdParty.FPTel;

                            if (!string.IsNullOrEmpty(thirdParty.FPMobile))
                                TPVPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = thirdParty.FPMobile, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                            //TPVPerson.MobileTelephone = thirdParty.FPMobile;

                            //}

                            #endregion

                            #region TP Vehicle Person Address

                            if (!string.IsNullOrEmpty(thirdParty.FPAddress1))
                                TPVPAddress.SubBuilding = thirdParty.FPAddress1;

                            if (!string.IsNullOrEmpty(thirdParty.FPAddress2))
                                TPVPAddress.Building = thirdParty.FPAddress2;

                            if (!string.IsNullOrEmpty(thirdParty.FPAddress3))
                                TPVPAddress.BuildingNumber = thirdParty.FPAddress3;

                            if (!string.IsNullOrEmpty(thirdParty.FPAddress4))
                                TPVPAddress.Street = thirdParty.FPAddress4;

                            if (!string.IsNullOrEmpty(thirdParty.FPPostCode))
                                TPVPAddress.PostCode = thirdParty.FPPostCode;


                            #endregion

                            #region TP Vehicle Person Organisation and Address

                            #region TP Solicitor

                            if (thirdParty.FPSolicitor > 0)
                            {
                                var fpSolicitorDetails = tpSolicitors.FirstOrDefault(x => x.TPSolicitorId == thirdParty.FPSolicitor);

                                if (fpSolicitorDetails != null)
                                {

                                    TPVPOrganisationSolicitor.P2O_LinkData.Person2OrganisationLinkType_Id =
                                        (int)Person2OrganisationLinkType.Solicitor;
                                    TPVPOrganisationSolicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;

                                    if (!string.IsNullOrEmpty(fpSolicitorDetails.TPSolicitorFullName))
                                        TPVPOrganisationSolicitor.OrganisationName = fpSolicitorDetails.TPSolicitorFullName;

                                    if (!string.IsNullOrEmpty(fpSolicitorDetails.TPSolicitorTel))
                                        TPVPOrganisationSolicitor.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = fpSolicitorDetails.TPSolicitorTel, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                    //TPVPOrganisationSolicitor.Telephone1 = fpSolicitorDetails.TPSolicitorTel;

                                    if (!string.IsNullOrEmpty(fpSolicitorDetails.TPSolicitorFax))
                                        TPVPOrganisationSolicitor.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = fpSolicitorDetails.TPSolicitorFax, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.FAX });
                                    //TPVPOrganisationSolicitor.Telephone2 = fpSolicitorDetails.TPSolicitorFax;

                                    if (!string.IsNullOrEmpty(fpSolicitorDetails.TPSolicitorAddr1))
                                        TPVPOrgAddressSolicitor.SubBuilding = fpSolicitorDetails.TPSolicitorAddr1;

                                    if (!string.IsNullOrEmpty(fpSolicitorDetails.TPSolicitorAddr2))
                                        TPVPOrgAddressSolicitor.Building = fpSolicitorDetails.TPSolicitorAddr2;

                                    if (!string.IsNullOrEmpty(fpSolicitorDetails.TPSolicitorAddr3))
                                        TPVPOrgAddressSolicitor.BuildingNumber = fpSolicitorDetails.TPSolicitorAddr3;

                                    if (!string.IsNullOrEmpty(fpSolicitorDetails.TPSolicitorAddr4))
                                        TPVPOrgAddressSolicitor.Street = fpSolicitorDetails.TPSolicitorAddr4;

                                    if (!string.IsNullOrEmpty(fpSolicitorDetails.TPSolicitorAddr5))
                                        TPVPOrgAddressSolicitor.Locality = fpSolicitorDetails.TPSolicitorAddr5;

                                    if (!string.IsNullOrEmpty(fpSolicitorDetails.TPSolicitorAddr6))
                                        TPVPOrgAddressSolicitor.Town = fpSolicitorDetails.TPSolicitorAddr6;

                                    if (!string.IsNullOrEmpty(fpSolicitorDetails.TPSolicitorPostcode))
                                        TPVPOrgAddressSolicitor.PostCode = fpSolicitorDetails.TPSolicitorPostcode;
                                }

                            }

                            #endregion

                            #region TP Broker

                            if (thirdParty.FPBroker > 0)
                            {
                                var fpBrokerDetails = tpBrokers.FirstOrDefault(x => x.TPBrokerId == thirdParty.FPBroker);

                                if (fpBrokerDetails != null)
                                {

                                    TPVPOrganisationBroker.P2O_LinkData.Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Broker;
                                    TPVPOrganisationBroker.OrganisationType_Id = (int)OrganisationType.Broker;

                                    if (!string.IsNullOrEmpty(fpBrokerDetails.TPBrokerFullName))
                                        TPVPOrganisationBroker.OrganisationName = fpBrokerDetails.TPBrokerFullName;

                                    if (!string.IsNullOrEmpty(fpBrokerDetails.TPBrokerTel))
                                        TPVPOrganisationBroker.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = fpBrokerDetails.TPBrokerTel, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                    //TPVPOrganisationBroker.Telephone1 = fpBrokerDetails.TPBrokerTel;

                                    if (!string.IsNullOrEmpty(fpBrokerDetails.TPBrokerFax))
                                        TPVPOrganisationBroker.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = fpBrokerDetails.TPBrokerFax, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.FAX });
                                    //TPVPOrganisationBroker.Telephone2 = fpBrokerDetails.TPBrokerFax;

                                    if (!string.IsNullOrEmpty(fpBrokerDetails.TPBrokerAddr1))
                                        TPVPOrgAddressBroker.SubBuilding = fpBrokerDetails.TPBrokerAddr1;

                                    if (!string.IsNullOrEmpty(fpBrokerDetails.TPBrokerAddr2))
                                        TPVPOrgAddressBroker.Building = fpBrokerDetails.TPBrokerAddr2;

                                    if (!string.IsNullOrEmpty(fpBrokerDetails.TPBrokerAddr3))
                                        TPVPOrgAddressBroker.BuildingNumber = fpBrokerDetails.TPBrokerAddr3;

                                    if (!string.IsNullOrEmpty(fpBrokerDetails.TPBrokerAddr4))
                                        TPVPOrgAddressBroker.Street = fpBrokerDetails.TPBrokerAddr4;

                                    if (!string.IsNullOrEmpty(fpBrokerDetails.TPBrokerAddr5))
                                        TPVPOrgAddressBroker.Locality = fpBrokerDetails.TPBrokerAddr5;

                                    if (!string.IsNullOrEmpty(fpBrokerDetails.TPBrokerAddr6))
                                        TPVPOrgAddressBroker.Town = fpBrokerDetails.TPBrokerAddr6;

                                    if (!string.IsNullOrEmpty(fpBrokerDetails.TPBrokerPostcode))
                                        TPVPOrgAddressBroker.PostCode = fpBrokerDetails.TPBrokerPostcode;
                                }
                            }

                            #endregion

                            #region TP Insurer

                            if (thirdParty.FPInsuranceId > 0)
                            {
                                if (tpInsurers != null)
                                {

                                    var fpInsurerDetails = tpInsurers.FirstOrDefault(x => x.TPInsurerId == thirdParty.FPInsuranceId);

                                    if (fpInsurerDetails != null)
                                    {
                                        TPVPOrganisationInsurer.P2O_LinkData.Person2OrganisationLinkType_Id =
                                            (int)Person2OrganisationLinkType.Unknown;
                                        TPVPOrganisationInsurer.OrganisationType_Id = (int)OrganisationType.Insurer;

                                        if (!string.IsNullOrEmpty(fpInsurerDetails.TPInsurerFullName))
                                            TPVPOrganisationInsurer.OrganisationName = fpInsurerDetails.TPInsurerFullName;

                                        if (!string.IsNullOrEmpty(fpInsurerDetails.TPInsurerTel))
                                            TPVPOrganisationInsurer.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = fpInsurerDetails.TPInsurerTel, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                                        //TPVPOrganisationInsurer.Telephone1 = fpInsurerDetails.TPInsurerTel;

                                        if (!string.IsNullOrEmpty(fpInsurerDetails.TPInsurerFax))
                                            TPVPOrganisationInsurer.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = fpInsurerDetails.TPInsurerFax, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.FAX });
                                        //TPVPOrganisationInsurer.Telephone2 = fpInsurerDetails.TPInsurerFax;

                                        if (!string.IsNullOrEmpty(fpInsurerDetails.TPInsurerAddr1))
                                            TPVPOrgAddressInsurer.SubBuilding = fpInsurerDetails.TPInsurerAddr1;

                                        if (!string.IsNullOrEmpty(fpInsurerDetails.TPInsurerAddr2))
                                            TPVPOrgAddressInsurer.Building = fpInsurerDetails.TPInsurerAddr2;

                                        if (!string.IsNullOrEmpty(fpInsurerDetails.TPInsurerAddr3))
                                            TPVPOrgAddressInsurer.BuildingNumber = fpInsurerDetails.TPInsurerAddr3;

                                        if (!string.IsNullOrEmpty(fpInsurerDetails.TPInsurerAddr4))
                                            TPVPOrgAddressInsurer.Street = fpInsurerDetails.TPInsurerAddr4;

                                        if (!string.IsNullOrEmpty(fpInsurerDetails.TPInsurerAddr5))
                                            TPVPOrgAddressInsurer.Locality = fpInsurerDetails.TPInsurerAddr5;

                                        if (!string.IsNullOrEmpty(fpInsurerDetails.TPInsurerAddr6))
                                            TPVPOrgAddressInsurer.Town = fpInsurerDetails.TPInsurerAddr6;

                                        if (!string.IsNullOrEmpty(fpInsurerDetails.TPInsurerPostcode))
                                            TPVPOrgAddressInsurer.PostCode = fpInsurerDetails.TPInsurerPostcode;
                                    }
                                }
                            }

                            #endregion

                            #endregion

                #endregion

                            if (TPVPerson != null)
                            {
                                if ((TPVPAddress.Street != null || TPVPAddress.SubBuilding != null || TPVPAddress.Building != null || TPVPAddress.BuildingNumber != null || TPVPAddress.Locality != null || TPVPAddress.Town != null || TPVPAddress.PostCode != null) && !thirdParty.boolClaimLevelOrgFound)
                                    TPVPerson.Addresses.Add(TPVPAddress);

                                if (TPVPOrganisationSolicitor.OrganisationName != null)
                                    TPVPerson.Organisations.Add(TPVPOrganisationSolicitor);

                                if (TPVPOrgAddressSolicitor.Street != null || TPVPOrgAddressSolicitor.SubBuilding != null || TPVPOrgAddressSolicitor.Building != null || TPVPOrgAddressSolicitor.BuildingNumber != null || TPVPOrgAddressSolicitor.Locality != null || TPVPOrgAddressSolicitor.Town != null || TPVPOrgAddressSolicitor.PostCode != null)
                                    TPVPOrganisationSolicitor.Addresses.Add(TPVPOrgAddressSolicitor);

                                if (TPVPOrganisationBroker.OrganisationName != null)
                                    TPVPerson.Organisations.Add(TPVPOrganisationBroker);

                                if (TPVPOrgAddressBroker.Street != null || TPVPOrgAddressBroker.SubBuilding != null || TPVPOrgAddressBroker.Building != null || TPVPOrgAddressBroker.BuildingNumber != null || TPVPOrgAddressBroker.Locality != null || TPVPOrgAddressBroker.Town != null || TPVPOrgAddressBroker.PostCode != null)
                                    TPVPOrganisationBroker.Addresses.Add(TPVPOrgAddressBroker);

                                if (TPVPOrganisationInsurer.OrganisationName != null)
                                    TPVPerson.Organisations.Add(TPVPOrganisationInsurer);

                                if (TPVPOrgAddressInsurer.Street != null || TPVPOrgAddressInsurer.SubBuilding != null || TPVPOrgAddressInsurer.Building != null || TPVPOrgAddressInsurer.BuildingNumber != null || TPVPOrgAddressInsurer.Locality != null || TPVPOrgAddressInsurer.Town != null || TPVPOrgAddressInsurer.PostCode != null)
                                    TPVPOrganisationInsurer.Addresses.Add(TPVPOrgAddressInsurer);

                                if (thirdParty.boolClaimLevelOrgFound && TPVPerson.Organisations.Any() && (TPVPerson.FirstName == null && TPVPerson.Salutation_Id == (int)Salutation.Unknown))
                                {
                                    // blank out person details
                                    TPVPerson.BankAccounts.Clear(); // = null;
                                    TPVPerson.DateOfBirth = null;
                                    TPVPerson.DrivingLicenseNumbers.Clear(); // = null;
                                    TPVPerson.EmailAddresses.Clear(); // = null;
                                    TPVPerson.FirstName = null;
                                    TPVPerson.Gender_Id = (int)Gender.Unknown;
                                    TPVPerson.Telephones.Clear(); // = null;
                                    TPVPerson.LastName = null;
                                    TPVPerson.MiddleName = null;
                                    //TPVPerson.MobileTelephone = null;
                                    TPVPerson.NINumbers.Clear(); // = null;
                                    TPVPerson.Nationality = null;
                                    TPVPerson.Occupation = null;
                                    TPVPerson.Operation = (int)Operation.Normal;
                                    //TPVPerson.OtherTelephone = null;
                                    TPVPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                                    TPVPerson.PassportNumbers.Clear(); // = null;
                                    TPVPerson.PaymentCards.Clear(); // = null;
                                    TPVPerson.Salutation_Id = (int)Salutation.Unknown;
                                    TPVPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    ///TPVPerson.WorkTelephone = null;

                                    TPVehicle.People.Add(TPVPerson);
                                }

                                if (thirdParty.boolClaimLevelOrgFound && TPVPerson.Organisations.Any() &&
                                    (TPVPerson.FirstName != null && TPVPerson.LastName != null))
                                {
                                    if (TPVPAddress.Street != null || TPVPAddress.SubBuilding != null || TPVPAddress.Building != null || TPVPAddress.BuildingNumber != null || TPVPAddress.Locality != null || TPVPAddress.Town != null || TPVPAddress.PostCode != null)
                                    {
                                        TPVPerson.Addresses.Add(TPVPAddress);
                                    }

                                    TPVehicle.People.Add(TPVPerson);
                                }

                                if (!thirdParty.boolClaimLevelOrgFound)
                                {
                                    if ((!string.IsNullOrEmpty(TPVPerson.FirstName) && !string.IsNullOrEmpty(TPVPerson.LastName)) || TPVPerson.Organisations.Any())
                                        TPVehicle.People.Add(TPVPerson);
                                }
                            }

                            if (TPVehicle.VehicleRegistration != null ||
                                (TPVPerson.FirstName != null && TPVPerson.LastName != null) || TPVPerson.Organisations.Any())
                                motorClaim.Vehicles.Add(TPVehicle);
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating TP Vehicle: " + ex);
                }

                #region Witness Vehicle

                try
                {
                    PipelineVehicle witnessVehicle = new PipelineVehicle();
                    witnessVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.NoVehicleInvolved;

                    if (witnessCollection.Any())
                    {
                        foreach (var witness in witnessCollection.Where(x => x.WitnessCaseId == motorClaim.ClaimNumber))
                        {
                            #region Witness Person

                            PipelinePerson witnessPerson = new PipelinePerson();
                            PipelineAddress witnessAddress = new PipelineAddress();

                            if (!string.IsNullOrEmpty(witness.WitnessTitle))
                            {
                                switch (witness.WitnessTitle)
                                {
                                    case "Mr":
                                    case "Me":
                                    case "Mr.":
                                        witnessPerson.Salutation_Id = (int)Salutation.Mr;
                                        witnessPerson.Gender_Id = (int)Gender.Male;
                                        break;
                                    case "Mrs":
                                        witnessPerson.Salutation_Id = (int)Salutation.Mrs;
                                        witnessPerson.Gender_Id = (int)Gender.Female;
                                        break;
                                    case "Ms":
                                        witnessPerson.Salutation_Id = (int)Salutation.Ms;
                                        witnessPerson.Gender_Id = (int)Gender.Female;
                                        break;
                                    case "Miss":
                                        witnessPerson.Salutation_Id = (int)Salutation.Miss;
                                        witnessPerson.Gender_Id = (int)Gender.Female;
                                        break;
                                    case "Master":
                                        witnessPerson.Salutation_Id = (int)Salutation.Master;
                                        break;
                                    case "Dr":
                                    case "Doctor":
                                        witnessPerson.Salutation_Id = (int)Salutation.Dr;
                                        break;
                                    case "Professor":
                                    case "Prof":
                                        witnessPerson.Salutation_Id = (int)Salutation.Professor;
                                        break;
                                    case "Reverend":
                                    case "Rev":
                                        witnessPerson.Salutation_Id = (int)Salutation.Reverend;
                                        break;
                                    case "Sir":
                                        witnessPerson.Salutation_Id = (int)Salutation.Sir;
                                        witnessPerson.Gender_Id = (int)Gender.Male;
                                        break;
                                    case "Lord":
                                        witnessPerson.Salutation_Id = (int)Salutation.Lord;
                                        witnessPerson.Gender_Id = (int)Gender.Male;
                                        break;
                                    case "Baroness":
                                        witnessPerson.Salutation_Id = (int)Salutation.Baroness;
                                        witnessPerson.Gender_Id = (int)Gender.Female;
                                        break;
                                    case "Right Honourable":
                                        witnessPerson.Salutation_Id = (int)Salutation.RightHonourable;
                                        break;
                                    default:
                                        witnessPerson.Salutation_Id = (int)Salutation.Unknown;
                                        break;
                                }
                            }

                            if (!string.IsNullOrEmpty(witness.WitnessForename))
                            {
                                witnessPerson.FirstName = witness.WitnessForename;
                                witnessPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;
                                witnessPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Witness;
                            }

                            if (!string.IsNullOrEmpty(witness.WitnessLastName))
                                witnessPerson.LastName = witness.WitnessLastName;

                            if (!string.IsNullOrEmpty(witness.WitnessTel))
                                witnessPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = witness.WitnessTel, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });
                            //witnessPerson.LandlineTelephone = witness.WitnessTel;

                            if (!string.IsNullOrEmpty(witness.WitnessMobile))
                                witnessPerson.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = witness.WitnessMobile, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });
                            //witnessPerson.MobileTelephone = witness.WitnessMobile;

                            #endregion

                            #region Witness Address

                            if (!string.IsNullOrEmpty(witness.WitnessAddr1))
                                witnessAddress.SubBuilding = witness.WitnessAddr1;

                            if (!string.IsNullOrEmpty(witness.WitnessAddr2))
                                witnessAddress.Building = witness.WitnessAddr2;

                            if (!string.IsNullOrEmpty(witness.WitnessAddr3))
                                witnessAddress.BuildingNumber = witness.WitnessAddr3;

                            if (!string.IsNullOrEmpty(witness.WitnessAddr4))
                                witnessAddress.Street = witness.WitnessAddr4;

                            if (!string.IsNullOrEmpty(witness.WitnessPostCode))
                                witnessAddress.PostCode = witness.WitnessPostCode;

                            #endregion

                            if (witnessPerson != null)
                            {
                                if (witnessAddress.Street != null || witnessAddress.SubBuilding != null || witnessAddress.Building != null || witnessAddress.BuildingNumber != null || witnessAddress.PostCode != null)
                                    witnessPerson.Addresses.Add(witnessAddress);

                                if (witnessPerson.LastName != null)
                                    witnessVehicle.People.Add(witnessPerson);
                            }
                        }
                    }

                    if (witnessVehicle.People.Any())
                        motorClaim.Vehicles.Add(witnessVehicle);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating witness: " + ex);
                }

                #endregion

                #region Insured Vehicle and Driver Check

                bool insuredVehicleCheck = false;
                bool insuredDriverCheck = false;

                foreach (var vehicle in motorClaim.Vehicles)
                {
                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                    {
                        insuredVehicleCheck = true;

                        foreach (var person in vehicle.People)
                        {
                            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                            {
                                insuredDriverCheck = true;
                            }
                        }
                    }
                }

                if (insuredVehicleCheck == false)
                {
                    PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                    defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                    }

                    motorClaim.Vehicles.Add(defaultInsuredVehicle);
                }
                else
                {
                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        foreach (var vehicle in motorClaim.Vehicles)
                        {
                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                            {

                                vehicle.People.Add(defaultInsuredDriver);

                            }

                        }

                    }
                }

                #endregion

                //cb.Claims.Add(motorClaim);

                if (ProcessClaimFn(ctx, motorClaim, statusTracking) == -1) return;
            }

            //int counta = cb.Claims.Count();


            #endregion

            if (boolDebug)
            {
                stopwatch.Stop();
                GetTimeSpan(stopwatch);
            }

            //return cb;
        }



        private void DeleteExtractedZipFiles(string clientFolder, bool deleteExtractedFiles)
        {
            if (deleteExtractedFiles)
            {
                try
                {
                    string[] filePaths = Directory.GetFiles(clientFolder);
                    foreach (string file in filePaths)
                    {
                        if (Path.GetExtension(file).ToUpper() != ".ZIP")
                        {
                            File.SetAttributes(file, FileAttributes.Normal);
                            File.Delete(file);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in deleting extracted files: " + ex);
                }
            }
        }

        private static void DeleteExtractedFiles(string clientFolder)
        {
            try
            {
                string[] filePaths = Directory.GetFiles(clientFolder);
                foreach (var file in filePaths)
                {
                    if (Path.GetExtension(file).ToUpper() != ".ZIP")
                        File.Delete(file);
                }
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in deleting extracted files: " + ex);
            }
        }

        public static void GetTimeSpan(Stopwatch stopwatch)
        {
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopwatch.Elapsed;

            // Format and display the TimeSpan value. 
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                               ts.Hours, ts.Minutes, ts.Seconds,
                                               ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
            Console.WriteLine("Finish");

            if (boolDebug)
                Console.ReadLine();
        }

    }
}
