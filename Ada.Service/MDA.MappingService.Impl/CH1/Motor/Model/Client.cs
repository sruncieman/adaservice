﻿using System;
using System.Collections.Generic;
using FileHelpers;

namespace MDA.MappingService.CH1.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class Client
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CaseNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PHTitle;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PHForename;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PHInitials;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PHLastName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address1;
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DOB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address5;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Telephone;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Mobile;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Make;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Model;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Reg;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Damage;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DAInsurerIndex;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DAInsurerReference;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int SolicitorId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int EngineerId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int RepairerId;
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? AccDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccTime;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Location;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentDesc;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PoliceRef;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String OfficerName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PoliceTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int ReferrerId;

        [FieldIgnored]
        public String InsurerName;
        //[FieldQuoted('"', QuoteMode.OptionalForBoth)]
        //public String InsPol;
        [FieldIgnored]
        public String ClientSolicitorFullName;
        [FieldIgnored]
        public String ClientSolicitorAddr1;
        [FieldIgnored]
        public String ClientSolicitorAddr2;
        [FieldIgnored]
        public String ClientSolicitorAddr3;
        [FieldIgnored]
        public String ClientSolicitorAddr4;
        [FieldIgnored]
        public String ClientSolicitorAddr5;
        [FieldIgnored]
        public String ClientSolicitorAddr6;
        [FieldIgnored]
        public String ClientSolicitorTel;
        [FieldIgnored]
        public String ClientSolicitorFax;
        [FieldIgnored]
        public string ClientEngineerName;
        [FieldIgnored]
        public string ClientEngineerAddr1;
        [FieldIgnored]
        public string ClientEngineerAddr2;
        [FieldIgnored]
        public string ClientEngineerAddr3;
        [FieldIgnored]
        public string ClientEngineerAddr4;
        [FieldIgnored]
        public string ClientEngineerAddr5;
        [FieldIgnored]
        public string ClientEngineerAddr6;
        [FieldIgnored]
        public string ClientEngineerPostcode;
        [FieldIgnored]
        public string ClientEngineerTelephone;
        [FieldIgnored]
        public string ClientEngineerFax;
        [FieldIgnored]
        public String ClientRepairerFullName;
        [FieldIgnored]
        public String ClientRepairerAddr1;
        [FieldIgnored]
        public String ClientRepairerAddr2;
        [FieldIgnored]
        public String ClientRepairerAddr3;
        [FieldIgnored]
        public String ClientRepairerAddr4;
        [FieldIgnored]
        public String ClientRepairerAddr5;
        [FieldIgnored]
        public String ClientRepairerAddr6;
        [FieldIgnored]
        public String ClientRepairerPostcode;
        [FieldIgnored]
        public String ClientRepairerTel;
        [FieldIgnored]
        public String ClientRepairerFax;
        [FieldIgnored]
        public String RefContactName;
        [FieldIgnored]
        public String RefCompName;
        [FieldIgnored]
        public String RefEmail;
        [FieldIgnored]
        public String RefAdd1;
        [FieldIgnored]
        public String RefAdd2;
        [FieldIgnored]
        public String RefAdd3;
        [FieldIgnored]
        public String RefAdd4;
        [FieldIgnored]
        public String RefAdd5;
        [FieldIgnored]
        public String RefAdd6;
        [FieldIgnored]
        public String RefPostcode;
        [FieldIgnored]
        public String RefTel;
        [FieldIgnored]
        public String RefMobile;
        [FieldIgnored]
        public String WitnessTitle;
        [FieldIgnored]
        public String WitnessForename;
        [FieldIgnored]
        public String WitnessInitials;
        [FieldIgnored]
        public String WitnessLastName;
        [FieldIgnored]
        public String WitnessAddr1;
        [FieldIgnored]
        public String WitnessAddr2;
        [FieldIgnored]
        public String WitnessAddr3;
        [FieldIgnored]
        public String WitnessAddr4;
        [FieldIgnored]
        public String WitnessPostcode;
        [FieldIgnored]
        public String WitnessTel;
        [FieldIgnored]
        public String WitnessMobile;
        [FieldIgnored]
        public String MiddleName;
        //[FieldIgnored]
        //public string FPTitle;
        //[FieldIgnored]
        //public string FPFirstName;
        //[FieldIgnored]
        //public string FPInitals;
        //[FieldIgnored]
        //public string FPLastName;
        //[FieldIgnored]
        //public string FPAddress1;
        //[FieldIgnored]
        //public string FPAddress2;
        //[FieldIgnored]
        //public string FPAddress3;
        //[FieldIgnored]
        //public string FPAddress4;
        //[FieldIgnored]
        //public string FPPostcode;
        //[FieldIgnored]
        //public string FPTel;
        //[FieldIgnored]
        //public string FPMobile;
        //[FieldIgnored]
        //public string FPVehicleMake;
        //[FieldIgnored]
        //public string FPVehicleModel;
        //[FieldIgnored]
        //public string FPVehicleReg;
        //[FieldIgnored]
        //public int FPInsuranceId;
        //[FieldIgnored]
        //public String TPInsurerFullName;
        //[FieldIgnored]
        //public String TPInsurerAddr1;
        //[FieldIgnored]
        //public String TPInsurerAddr2;
        //[FieldIgnored]
        //public String TPInsurerAddr3;
        //[FieldIgnored]
        //public String TPInsurerAddr4;
        //[FieldIgnored]
        //public String TPInsurerAddr5;
        //[FieldIgnored]
        //public String TPInsurerAddr6;
        //[FieldIgnored]
        //public String TPInsurerPostcode;
        //[FieldIgnored]
        //public String TPInsurerTel;
        //[FieldIgnored]
        //public String TPInsurerFax;
        //[FieldIgnored]
        //public string FPPolNo;
        //[FieldIgnored]
        //public int FPBroker;
        //[FieldIgnored]
        //public String TPBrokerFullName;
        //[FieldIgnored]
        //public String TPBrokerAddr1;
        //[FieldIgnored]
        //public String TPBrokerAddr2;
        //[FieldIgnored]
        //public String TPBrokerAddr3;
        //[FieldIgnored]
        //public String TPBrokerAddr4;
        //[FieldIgnored]
        //public String TPBrokerAddr5;
        //[FieldIgnored]
        //public String TPBrokerAddr6;
        //[FieldIgnored]
        //public String TPBrokerPostcode;
        //[FieldIgnored]
        //public String TPBrokerTel;
        //[FieldIgnored]
        //public String TPBrokerFax;
        //[FieldIgnored]
        //public int FPSolicitor;
        //[FieldIgnored]
        //public String TPSolicitorFullName;
        //[FieldIgnored]
        //public String TPSolicitorAddr1;
        //[FieldIgnored]
        //public String TPSolicitorAddr2;
        //[FieldIgnored]
        //public String TPSolicitorAddr3;
        //[FieldIgnored]
        //public String TPSolicitorAddr4;
        //[FieldIgnored]
        //public String TPSolicitorAddr5;
        //[FieldIgnored]
        //public String TPSolicitorAddr6;
        //[FieldIgnored]
        //public String TPSolicitorPostcode;
        //[FieldIgnored]
        //public String TPSolicitorTel;
        //[FieldIgnored]
        //public String TPSolicitorFax;
        //[FieldIgnored]
        //public DateTime? FPAccDate;
        //[FieldIgnored]
        //public string FPAccTime;
        //[FieldIgnored]
        //public string FPAccLocation;
        [FieldIgnored]
        public string FPClaimCodeDesc;
        //[FieldIgnored]
        //public string FPAccNotes;
        //[FieldIgnored]
        //public string FPPoliceRef;
        //[FieldIgnored]
        //public string FPPoliceName;
        //[FieldIgnored]
        //public string FPPoliceTel;
        [FieldIgnored]
        public List<FaultParty> lstThirdParties;

    }
}
