﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CH1.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class Referrer
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int RefCompId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RefContactName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RefCompName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RefEmail;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RefAdd1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RefAdd2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RefAdd3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RefAdd4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RefAdd5;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RefTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RefMobile;
    }
}
