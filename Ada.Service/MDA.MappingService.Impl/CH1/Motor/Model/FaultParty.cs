﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CH1.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class FaultParty
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPCaseNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPTitle;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPFirstName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPInitals;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPLastName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPAddress1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPAddress2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPAddress3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPAddress4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPPostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPMobile;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPVehicleMake;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPVehicleModel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPVehicleReg;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int FPInsuranceId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPPolNo;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int FPBroker;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int FPSolicitor;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPEngineerNotes;
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? FPAccDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPAccTime;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPAccLocation;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPAccNotes;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPPoliceRef;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPPoliceName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FPPoliceTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DAClaimCaseNo;
        [FieldIgnored]
        public bool boolClaimLevelOrgFound;
    }
}
