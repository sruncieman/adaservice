﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CH1.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class TPInsurer
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int TPInsurerId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPInsurerFullName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPInsurerAddr1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPInsurerAddr2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPInsurerAddr3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPInsurerAddr4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPInsurerAddr5;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPInsurerAddr6;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPInsurerPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPInsurerTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPInsurerFax;

    }
}
