﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CH1.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class TPSolicitor
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int TPSolicitorId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPSolicitorFullName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPSolicitorAddr1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPSolicitorAddr2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPSolicitorAddr3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPSolicitorAddr4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPSolicitorAddr5;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPSolicitorAddr6;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPSolicitorPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPSolicitorTel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TPSolicitorFax;
    }
}
