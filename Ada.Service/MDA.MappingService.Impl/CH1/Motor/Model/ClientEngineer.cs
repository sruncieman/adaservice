﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.CH1.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClientEngineer
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldNullValue(typeof(int), "0")]
        public int ClientEngineerId;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClientEngineerName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClientEngineerAddr1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClientEngineerAddr2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClientEngineerAddr3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClientEngineerAddr4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClientEngineerAddr5;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClientEngineerAddr6;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClientEngineerPostcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClientEngineerTelephone;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClientEngineerFax;




    }
}
