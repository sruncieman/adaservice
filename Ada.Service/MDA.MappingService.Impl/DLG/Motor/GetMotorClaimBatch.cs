﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FileHelpers;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.Common.Helpers;
using MDA.MappingService.DLG.Model;
using MDA.Common.Server;
using MDA.Common;
using Ionic.Zip;

namespace MDA.MappingService.DLG
{
    public class GetMotorClaimBatch
    {
        private static bool boolDebug = true;
        private Stopwatch stopwatch;
        private static string folderPath;
        private static string excludedOrganisations;



        public void RetrieveMotorClaimBatch(CurrentContext ctx, Stream filem, string clientFolder, bool deleteExtractedFiles, object statusTracking,
                        Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn,
                         ProcessingResults processingResults)
        {
            #region Initialise variables
            DateTime minDate = new DateTime(2007, 1, 1);
            string DLG1File;
            string DLG2File;
            string DLG3File;
            string DLG4File;
            string DLG5File;
            string filePath = null;

            DLG1[] dlg1;
            DLG2[] dlg2;
            DLG3[] dlg3;
            DLG4[] dlg4;
            DLG5[] dlg5;

            excludedOrganisations = "DLG Legal Services Limited, DLG Legal Services, DLG Legal, Motor Legal Expenses Claims Department";
            string[] excludedOrganisationsArray = excludedOrganisations.Split(',').Select(s => s.Trim()).ToArray();
            #endregion

            #region Extract Files from Zip

            if (filem != null)
            {
                try
                {
                    boolDebug = false;

                    using (ZipFile zip1 = ZipFile.Read(filem)) //_filePath))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            var extractTmpPath = string.Concat(clientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);


                            string unpackDirectory = clientFolder;
                            e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

                            if (ctx.ShowProgress != null)
                                ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                    }

                    //File.Delete(filePath);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in extraction of files: " + ex);
                }
            }

            #endregion

            if (boolDebug)
            {
                stopwatch = new Stopwatch();
                Console.WriteLine("Start");
                stopwatch.Start();
            }

            #region Files
            if (boolDebug)
            {
                folderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\DLG\Resource\";
                DLG1File = folderPath + @"\DLG1.csv";
                DLG2File = folderPath + @"\DLG2.csv";
                DLG3File = folderPath + @"\DLG_Party.csv";
                DLG4File = folderPath + @"\DLG_Vehicle.csv";
                DLG5File = folderPath + @"\DLG_Extras.csv";
            }
            else
            {
                try
                {
                    DLG1File = clientFolder + @"\DLG1.csv";
                    DLG2File = clientFolder + @"\DLG2.csv";
                    DLG3File = clientFolder + @"\DLG_Party.csv";
                    DLG4File = clientFolder + @"\DLG_Vehicle.csv";
                    DLG5File = clientFolder + @"\DLG_Extras.csv";
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in assigning extracted files: " + ex);
                }
            }

            DeleteExtractedFiles(clientFolder, deleteExtractedFiles);
            #endregion

            #region FileHelper Engines
            try
            {

                FileHelperEngine DLG1Engine = new FileHelperEngine(typeof(DLG1));
                FileHelperEngine DLG2Engine = new FileHelperEngine(typeof(DLG2));
                FileHelperEngine DLG_PartyEngine = new FileHelperEngine(typeof(DLG3));
                FileHelperEngine DLG_VehicleEngine = new FileHelperEngine(typeof(DLG4));
                FileHelperEngine DLG_Extras = new FileHelperEngine(typeof(DLG5));


            #endregion

                #region Populate FileHelper Engines with data from files

                dlg1 = DLG1Engine.ReadFile(DLG1File) as DLG1[];
                dlg2 = DLG2Engine.ReadFile(DLG2File) as DLG2[];
                dlg3 = DLG_PartyEngine.ReadFile(DLG3File) as DLG3[];
                dlg4 = DLG_VehicleEngine.ReadFile(DLG4File) as DLG4[];
                dlg5 = DLG_Extras.ReadFile(DLG5File) as DLG5[];

            }

            catch (Exception ex)
            {
                throw new CustomException("Error in initialising FileHelper Engines: " + ex);
            }

                #endregion


            #region Distinct Lookup Table Records

            try
            {
                // Get Distinct Claim Numbers from DLG1 File
                //if (dlg1 != null)
                //    lstDLG1 = dlg1
                //        .GroupBy(i => i.Claim_Number)
                //        .Select(g => g.First())
                //        .ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct lookup table values: " + ex);
            }

            #endregion


            #region Translate to XML


            foreach (var record in dlg1)
            {
                var vehiclesList = dlg4.Where(x => x.Claim_Number == record.Claim_Number).ToList();
                var extraInfoList = dlg2.Where(x => x.Claim_Number == record.Claim_Number).ToList();


                foreach (var vehicle in vehiclesList)
                {
                    record.Vehicles.Add(vehicle);
                }

                foreach (var extra in extraInfoList)
                {
                    record.ExtraInfo.Add(extra);
                }


                var vehiclesDriverIds = record.Vehicles.Select(x => x.DM_Driver_SK).Distinct().ToList();
                var vehiclesClaimIds = record.Vehicles.Select(x => x.Claim_Id).Distinct().ToList();


                var organisations = (from d3 in dlg3
                                     where !vehiclesDriverIds.Contains(d3.Contact_Id) && d3.Company_Name != "" && vehiclesClaimIds.Contains(d3.Claim_Id) && !excludedOrganisationsArray.Contains(d3.Company_Name)
                                     select d3).Distinct().ToList();

                var v2PDetails = (from d3 in dlg3
                                  where vehiclesDriverIds.Contains(d3.Contact_Id) && vehiclesClaimIds.Contains(d3.Claim_Id)
                                  select d3).ToList();




                var v2PContactIds = v2PDetails.Select(x => x.Contact_Id).ToList();

                var peopleWithNoVehicle = (from d3 in dlg3
                                           where vehiclesClaimIds.Contains(d3.Claim_Id) && !v2PContactIds.Contains(d3.Contact_Id) && d3.Company_Name == "" && d3.Description.ToUpper() != "INSURERS VEHICLE ENGINEER"
                                           select d3).Distinct().ToList();

                var engineerAsOrganisation = (from d3 in dlg3
                                              where vehiclesClaimIds.Contains(d3.Claim_Id) && !v2PContactIds.Contains(d3.Contact_Id) && d3.Company_Name == "" && d3.Description.ToUpper() == "INSURERS VEHICLE ENGINEER"
                                              select d3).Distinct().ToList();

                #region GeneralClaimData
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                try
                {

                    motorClaim.ClaimNumber = Convert.ToString(record.Claim_Number);
                    motorClaim.IncidentDate = Convert.ToDateTime(record.LossDate2);
                    motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general claim data: " + ex);
                }

                #endregion

                #region ClaimInfo
                try
                {
                    if (!string.IsNullOrEmpty(record.ClaimStateDesc))
                    {
                        switch (record.ClaimStateDesc.ToUpper())
                        {
                            case "OPEN":
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                break;
                            case "CLOSED":
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                break;
                        }
                    }


                    motorClaim.ExtraClaimInfo.ClaimNotificationDate = Convert.ToDateTime(record.ReportedDate2);

                    if (!string.IsNullOrEmpty(record.MoCaseOpName))
                        motorClaim.ExtraClaimInfo.ClaimCode = record.MoCaseOpName;

                    if (!string.IsNullOrEmpty(record.LossCauseDesc))
                        motorClaim.ExtraClaimInfo.IncidentCircumstances = record.LossCauseDesc;

                    //Where a claim has an organisation included with the role description of ‘Police Station’ the ADA field within the Claim Info entity ‘Police Attended?’ shall be set to true. In addition the ADA field ‘Police Force’ will be populated with the ‘Company Name’ field data from the DLG3 file.
                    if (organisations != null)
                    {
                        foreach (var item in organisations)
                        {
                            if (item.Description == "Police Station")
                            {

                                motorClaim.ExtraClaimInfo.PoliceAttended = true;
                                if (!string.IsNullOrEmpty(item.Company_Name))
                                    motorClaim.ExtraClaimInfo.PoliceForce = item.Company_Name;

                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general claim info: " + ex);
                }

                #endregion

                #region Policy

                try
                {
                    motorClaim.Policy.Insurer = "DLG";
                    if (!string.IsNullOrEmpty(record.Policy_Number))
                        motorClaim.Policy.PolicyNumber = record.Policy_Number;

                    motorClaim.Policy.PolicyStartDate = Convert.ToDateTime(record.PolicyEffectiveDt);
                    motorClaim.Policy.PolicyEndDate = Convert.ToDateTime(record.PolicyExpirationDt);

                    switch (record.Business.ToUpper())
                    {
                        case "PERSONAL":
                            motorClaim.Policy.PolicyType_Id = (int)PolicyType.PersonalMotor;
                            break;
                        case "COMMERCIAL":
                            motorClaim.Policy.PolicyType_Id = (int)PolicyType.CommercialMotor;
                            break;
                        default:
                            motorClaim.Policy.PolicyType_Id = (int)PolicyType.Unknown;
                            break;
                    }

                    if (!string.IsNullOrEmpty(record.Brand))
                        motorClaim.Policy.InsurerTradingName = record.Brand;

                    if (!string.IsNullOrEmpty(record.BrokerName))
                        motorClaim.Policy.Broker = record.BrokerName;

                    switch (record.CoverTypeDesc.ToUpper())
                    {
                        case "COMPREHENSIVE":
                            motorClaim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.Comprehensive;
                            break;
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating policy: " + ex);
                }

                #endregion

                #region Claim Organisations

                foreach (var orgItem in organisations)
                {
                    AddOrganisation(dlg5, motorClaim, orgItem, null);
                }

                foreach (var orgItem in engineerAsOrganisation)
                {
                    AddOrganisation(dlg5, motorClaim, orgItem, "Insurers Vehicle Engineer");
                }

                #endregion

                #region Vehicle

                try
                {

                    foreach (var vItem in record.Vehicles)
                    {

                        //var tester = (from v2P in v2PDetails where v2P.Description.ToUpper() == "INSURERS VEHICLE ENGINEER" select v2P).ToList();

                        //if (tester.Count > 0)
                        //{
                        //    string t = "s";
                        //}


                        var personDetails = (from v2P in v2PDetails
                                             where v2P.Contact_Id == vItem.DM_Driver_SK
                                             select v2P).FirstOrDefault();

                        var extraDetails = (from v2P in v2PDetails
                                            join r in record.ExtraInfo on v2P.Contact_Id equals r.DM_Contact_Id
                                            where
                                               v2P.Contact_Id == vItem.DM_Driver_SK && r.CRU_Claimant_First_Name != ""
                                            select r).FirstOrDefault();

                        DLG5 extras = new DLG5();

                        if (personDetails != null)
                        {

                            extras = (from e in dlg5
                                      where e.DM_CONTACT_ID == personDetails.Contact_Id
                                      select e).FirstOrDefault();

                        }

                        PipelineVehicle insuredVehicle = new PipelineVehicle();
                        PipelinePerson insuredPerson = new PipelinePerson();
                        PipelineAddress insuredPAddress = new PipelineAddress();

                        PipelineVehicle TPVehicle = new PipelineVehicle();
                        PipelinePerson TPPerson = new PipelinePerson();
                        PipelineAddress TPPAddress = new PipelineAddress();

                        if (personDetails != null)
                        {

                            if (personDetails.Description == "Insured")
                            {
                                #region Insured Vehicle Details

                                if (!string.IsNullOrEmpty(vItem.RegNo))
                                    insuredVehicle.VehicleRegistration = vItem.RegNo;

                                if (!string.IsNullOrEmpty(vItem.Make))
                                    insuredVehicle.VehicleMake = vItem.Make;

                                if (!string.IsNullOrEmpty(vItem.Model))
                                    insuredVehicle.VehicleModel = vItem.Model;

                                if (!string.IsNullOrEmpty(vItem.CC))
                                    insuredVehicle.EngineCapacity = vItem.CC;

                                insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id =
                                    (int)Incident2VehicleLinkType.InsuredVehicle;

                                #endregion

                                #region Insured Vehicle Person Details

                                insuredPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                insuredPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                                if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Title))
                                    insuredPerson.Salutation_Id =
                                        (int)SalutationHelper.GetSalutation(extraDetails.CRU_Claimant_Title);

                                if (!string.IsNullOrEmpty(personDetails.First_Name))
                                    insuredPerson.FirstName = personDetails.First_Name;
                                else
                                {
                                    if (extraDetails != null &&
                                        !string.IsNullOrEmpty(extraDetails.CRU_Claimant_First_Name))
                                        insuredPerson.FirstName = extraDetails.CRU_Claimant_First_Name;
                                }

                                if (!string.IsNullOrEmpty(personDetails.Middle_Name))
                                    insuredPerson.MiddleName = personDetails.Middle_Name;

                                if (!string.IsNullOrEmpty(personDetails.Last_Name))
                                    insuredPerson.LastName = personDetails.Last_Name;
                                else
                                {
                                    if (extraDetails != null &&
                                        !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Last_Name))
                                        insuredPerson.LastName = extraDetails.CRU_Claimant_Last_Name;
                                }

                                if (personDetails.DOB != null)
                                    insuredPerson.DateOfBirth = personDetails.DOB;
                                else
                                {
                                    if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Birth_Date))
                                        insuredPerson.DateOfBirth = Convert.ToDateTime(extraDetails.CRU_CLAIMANT_BIRTH_DT);
                                }

                                if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Gender))
                                    insuredPerson.Gender_Id =
                                        (int)GenderHelper.Salutation2Gender(extraDetails.CRU_Claimant_Gender);

                                if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_NI_Number))
                                    insuredPerson.NINumbers.Add(new PipelineNINumber() { NINumber1 = extraDetails.CRU_Claimant_NI_Number });

                                if (!string.IsNullOrEmpty(personDetails.HomeTelephone))
                                    insuredPerson.Telephones.Add(new PipelineTelephone()
                                    {
                                        ClientSuppliedNumber = personDetails.HomeTelephone,
                                        TelephoneType_Id =
                                            (int)MDA.Common.Enum.TelephoneType.Landline
                                    });

                                if (!string.IsNullOrEmpty(personDetails.Mobile))
                                    insuredPerson.Telephones.Add(new PipelineTelephone()
                                    {
                                        ClientSuppliedNumber = personDetails.Mobile,
                                        TelephoneType_Id =
                                            (int)MDA.Common.Enum.TelephoneType.Mobile
                                    });

                                if (!string.IsNullOrEmpty(personDetails.WorkTelephone))
                                    insuredPerson.Telephones.Add(new PipelineTelephone()
                                    {
                                        ClientSuppliedNumber = personDetails.WorkTelephone,
                                        TelephoneType_Id =
                                            (int)MDA.Common.Enum.TelephoneType.Unknown
                                    });

                                if (extras != null)
                                {

                                    if (!string.IsNullOrEmpty(extras.EMAIL_ADDRESS_LN_1))
                                    {
                                        PipelineEmail email1 = new PipelineEmail();

                                        email1.EmailAddress = extras.EMAIL_ADDRESS_LN_1;

                                        insuredPerson.EmailAddresses.Add(email1);
                                    }

                                    if (!string.IsNullOrEmpty(extras.EMAIL_ADDRESS_LN_2) && extras.EMAIL_ADDRESS_LN_2 != extras.EMAIL_ADDRESS_LN_1)
                                    {
                                        PipelineEmail email2 = new PipelineEmail();

                                        email2.EmailAddress = extras.EMAIL_ADDRESS_LN_2;

                                        insuredPerson.EmailAddresses.Add(email2);
                                    }

                                    if (extras.OCCUPATION_TXT != null)
                                        insuredPerson.Occupation = extras.OCCUPATION_TXT;

                                }
                                #endregion

                                #region Insured Vehicle Person Address

                                if (!string.IsNullOrEmpty(personDetails.Address1))
                                    insuredPAddress.BuildingNumber = personDetails.Address1;

                                if (!string.IsNullOrEmpty(personDetails.Address2))
                                    insuredPAddress.Street = personDetails.Address2;

                                if (!string.IsNullOrEmpty(personDetails.Address3))
                                    insuredPAddress.Locality = personDetails.Address3;

                                if (!string.IsNullOrEmpty(personDetails.City))
                                    insuredPAddress.Town = personDetails.City;

                                if (!string.IsNullOrEmpty(personDetails.Postcode))
                                    insuredPAddress.PostCode = personDetails.Postcode;

                                #endregion

                                insuredPerson.Addresses.Add(insuredPAddress);
                                insuredVehicle.People.Add(insuredPerson);

                                motorClaim.Vehicles.Add(insuredVehicle);
                            }

                            if (personDetails.Description != "Insured" && personDetails.Description != "Witnesss")
                            {

                                #region TP Vehicle Details

                                if (!string.IsNullOrEmpty(vItem.RegNo))
                                    TPVehicle.VehicleRegistration = vItem.RegNo;

                                if (!string.IsNullOrEmpty(vItem.Make))
                                    TPVehicle.VehicleMake = vItem.Make;

                                if (!string.IsNullOrEmpty(vItem.Model))
                                    TPVehicle.VehicleModel = vItem.Model;

                                if (!string.IsNullOrEmpty(vItem.CC))
                                    TPVehicle.EngineCapacity = vItem.CC;

                                TPVehicle.I2V_LinkData.Incident2VehicleLinkType_Id =
                                    (int)Incident2VehicleLinkType.ThirdPartyVehicle;

                                #endregion

                                #region TP Vehicle Person Details

                                TPPerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                TPPerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                                if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Title))
                                    TPPerson.Salutation_Id =
                                        (int)SalutationHelper.GetSalutation(extraDetails.CRU_Claimant_Title);

                                if (!string.IsNullOrEmpty(personDetails.First_Name))
                                    TPPerson.FirstName = personDetails.First_Name;
                                else
                                {
                                    if (extraDetails != null &&
                                        !string.IsNullOrEmpty(extraDetails.CRU_Claimant_First_Name))
                                        TPPerson.FirstName = extraDetails.CRU_Claimant_First_Name;
                                }

                                if (!string.IsNullOrEmpty(personDetails.Middle_Name))
                                    TPPerson.MiddleName = personDetails.Middle_Name;

                                if (!string.IsNullOrEmpty(personDetails.Last_Name))
                                    TPPerson.LastName = personDetails.Last_Name;
                                else
                                {
                                    if (extraDetails != null &&
                                        !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Last_Name))
                                        TPPerson.LastName = extraDetails.CRU_Claimant_Last_Name;
                                }

                                if (personDetails.DOB != null)
                                    TPPerson.DateOfBirth = personDetails.DOB;
                                else
                                {
                                    if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Birth_Date))
                                        insuredPerson.DateOfBirth = Convert.ToDateTime(extraDetails.CRU_CLAIMANT_BIRTH_DT);
                                }

                                if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Gender))
                                    TPPerson.Gender_Id =
                                        (int)GenderHelper.Salutation2Gender(extraDetails.CRU_Claimant_Gender);

                                if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_NI_Number))
                                    TPPerson.NINumbers.Add(new PipelineNINumber() { NINumber1 = extraDetails.CRU_Claimant_NI_Number });

                                if (!string.IsNullOrEmpty(personDetails.HomeTelephone))
                                    TPPerson.Telephones.Add(new PipelineTelephone()
                                    {
                                        ClientSuppliedNumber = personDetails.HomeTelephone,
                                        TelephoneType_Id =
                                            (int)MDA.Common.Enum.TelephoneType.Landline
                                    });

                                if (!string.IsNullOrEmpty(personDetails.Mobile))
                                    TPPerson.Telephones.Add(new PipelineTelephone()
                                    {
                                        ClientSuppliedNumber = personDetails.Mobile,
                                        TelephoneType_Id =
                                            (int)MDA.Common.Enum.TelephoneType.Mobile
                                    });

                                if (!string.IsNullOrEmpty(personDetails.WorkTelephone))
                                    TPPerson.Telephones.Add(new PipelineTelephone()
                                    {
                                        ClientSuppliedNumber = personDetails.WorkTelephone,
                                        TelephoneType_Id =
                                            (int)MDA.Common.Enum.TelephoneType.Unknown
                                    });

                                if (extras != null)
                                {

                                    if (!string.IsNullOrEmpty(extras.EMAIL_ADDRESS_LN_1))
                                    {
                                        PipelineEmail email1 = new PipelineEmail();

                                        email1.EmailAddress = extras.EMAIL_ADDRESS_LN_1;

                                        TPPerson.EmailAddresses.Add(email1);
                                    }

                                    if (!string.IsNullOrEmpty(extras.EMAIL_ADDRESS_LN_2) && extras.EMAIL_ADDRESS_LN_2 != extras.EMAIL_ADDRESS_LN_1)
                                    {
                                        PipelineEmail email2 = new PipelineEmail();

                                        email2.EmailAddress = extras.EMAIL_ADDRESS_LN_2;

                                        TPPerson.EmailAddresses.Add(email2);
                                    }

                                    if (extras.OCCUPATION_TXT != null)
                                        TPPerson.Occupation = extras.OCCUPATION_TXT;
                                }
                                #endregion

                                #region TP Vehicle Person Address

                                if (!string.IsNullOrEmpty(personDetails.Address1))
                                    TPPAddress.BuildingNumber = personDetails.Address1;

                                if (!string.IsNullOrEmpty(personDetails.Address2))
                                    TPPAddress.Street = personDetails.Address2;

                                if (!string.IsNullOrEmpty(personDetails.Address3))
                                    TPPAddress.Locality = personDetails.Address3;

                                if (!string.IsNullOrEmpty(personDetails.City))
                                    TPPAddress.Town = personDetails.City;

                                if (!string.IsNullOrEmpty(personDetails.Postcode))
                                    TPPAddress.PostCode = personDetails.Postcode;

                                #endregion

                                TPPerson.Addresses.Add(TPPAddress);
                                TPVehicle.People.Add(TPPerson);

                                motorClaim.Vehicles.Add(TPVehicle);
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating vehicle: " + ex);
                }

                #endregion

                #region No Vehicle Involved

                try
                {
                    PipelineVehicle noneVehicle = new PipelineVehicle();
                    foreach (var vItem in peopleWithNoVehicle)
                    {
                        var extraDetails = (from r in record.ExtraInfo
                                            where r.DM_Contact_Id == vItem.Contact_Id && r.Claim_Number == record.Claim_Number && r.CRU_Claimant_First_Name != ""
                                            select r).FirstOrDefault();

                        var extras = (from e in dlg5
                                      where e.DM_CONTACT_ID == vItem.Contact_Id
                                      select e).FirstOrDefault();

                        PipelinePerson noneVehiclePerson = new PipelinePerson();
                        PipelineAddress noneVehicleAddress = new PipelineAddress();

                        noneVehicle.I2V_LinkData.Incident2VehicleLinkType_Id =
                            (int)Incident2VehicleLinkType.NoVehicleInvolved;

                        #region No Vehicle Involved Person Details

                        if (!string.IsNullOrEmpty(vItem.Description))
                        {
                            switch (vItem.Description.ToUpper())
                            {

                                case "CLAIMANT":
                                case "CLAIMANT - EXPOSURE":
                                case "CLAIMANT - DEPENDENT":
                                    noneVehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                                    noneVehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    break;
                                case "DEFENDANT":
                                case "INJURED PARTY":
                                case "INSURED":
                                    noneVehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                                    noneVehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    break;
                                case "DRIVER":
                                case "IMPACT DRIVER":
                                    noneVehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                                    noneVehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                    break;
                                case "PASSENGER":
                                    noneVehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                                    noneVehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                                    break;
                                case "REGISTERED KEEPER":
                                    noneVehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;
                                    noneVehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    break;
                                case "UNINSURED DRIVER":
                                    noneVehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                                    noneVehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                                    break;
                                case "WITNESS":
                                    noneVehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;
                                    noneVehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Witness;
                                    break;
                                default:
                                    noneVehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Unknown;
                                    noneVehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                                    break;
                            }
                        }

                        if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Title))
                            noneVehiclePerson.Salutation_Id =
                                (int)SalutationHelper.GetSalutation(extraDetails.CRU_Claimant_Title);

                        if (!string.IsNullOrEmpty(vItem.First_Name))
                            noneVehiclePerson.FirstName = vItem.First_Name;
                        else
                        {
                            if (extraDetails != null &&
                                !string.IsNullOrEmpty(extraDetails.CRU_Claimant_First_Name))
                                noneVehiclePerson.FirstName = extraDetails.CRU_Claimant_First_Name;
                        }

                        if (!string.IsNullOrEmpty(vItem.Middle_Name))
                            noneVehiclePerson.MiddleName = vItem.Middle_Name;

                        if (!string.IsNullOrEmpty(vItem.Last_Name))
                            noneVehiclePerson.LastName = vItem.Last_Name;
                        else
                        {
                            if (extraDetails != null &&
                                !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Last_Name))
                                noneVehiclePerson.LastName = extraDetails.CRU_Claimant_Last_Name;
                        }

                        if (vItem.DOB != null)
                            noneVehiclePerson.DateOfBirth = vItem.DOB;
                        else
                        {
                            if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Birth_Date))
                                noneVehiclePerson.DateOfBirth = Convert.ToDateTime(extraDetails.CRU_CLAIMANT_BIRTH_DT);
                        }

                        if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_Gender))
                            noneVehiclePerson.Gender_Id =
                                (int)GenderHelper.Salutation2Gender(extraDetails.CRU_Claimant_Gender);

                        if (extraDetails != null && !string.IsNullOrEmpty(extraDetails.CRU_Claimant_NI_Number))
                            noneVehiclePerson.NINumbers.Add(new PipelineNINumber() { NINumber1 = extraDetails.CRU_Claimant_NI_Number });

                        if (!string.IsNullOrEmpty(vItem.HomeTelephone))
                            noneVehiclePerson.Telephones.Add(new PipelineTelephone()
                            {
                                ClientSuppliedNumber = vItem.HomeTelephone,
                                TelephoneType_Id =
                                    (int)MDA.Common.Enum.TelephoneType.Landline
                            });

                        if (!string.IsNullOrEmpty(vItem.Mobile))
                            noneVehiclePerson.Telephones.Add(new PipelineTelephone()
                            {
                                ClientSuppliedNumber = vItem.Mobile,
                                TelephoneType_Id =
                                    (int)MDA.Common.Enum.TelephoneType.Mobile
                            });

                        if (!string.IsNullOrEmpty(vItem.WorkTelephone))
                            noneVehiclePerson.Telephones.Add(new PipelineTelephone()
                            {
                                ClientSuppliedNumber = vItem.WorkTelephone,
                                TelephoneType_Id =
                                    (int)MDA.Common.Enum.TelephoneType.Unknown
                            });

                        if (extras != null)
                        {

                            if (!string.IsNullOrEmpty(extras.EMAIL_ADDRESS_LN_1))
                            {
                                PipelineEmail email1 = new PipelineEmail();

                                email1.EmailAddress = extras.EMAIL_ADDRESS_LN_1;

                                noneVehiclePerson.EmailAddresses.Add(email1);
                            }

                            if (!string.IsNullOrEmpty(extras.EMAIL_ADDRESS_LN_2) && extras.EMAIL_ADDRESS_LN_2 != extras.EMAIL_ADDRESS_LN_1)
                            {
                                PipelineEmail email2 = new PipelineEmail();

                                email2.EmailAddress = extras.EMAIL_ADDRESS_LN_2;

                                noneVehiclePerson.EmailAddresses.Add(email2);
                            }

                            if (extras.OCCUPATION_TXT != null)
                                noneVehiclePerson.Occupation = extras.OCCUPATION_TXT;
                        }
                        #endregion

                        #region Insured None Vehicle Person Address

                        if (!string.IsNullOrEmpty(vItem.Address1))
                            noneVehicleAddress.BuildingNumber = vItem.Address1;

                        if (!string.IsNullOrEmpty(vItem.Address2))
                            noneVehicleAddress.Street = vItem.Address2;

                        if (!string.IsNullOrEmpty(vItem.Address3))
                            noneVehicleAddress.Locality = vItem.Address3;

                        if (!string.IsNullOrEmpty(vItem.City))
                            noneVehicleAddress.Town = vItem.City;

                        if (!string.IsNullOrEmpty(vItem.Postcode))
                            noneVehicleAddress.PostCode = vItem.Postcode;

                        #endregion

                        noneVehiclePerson.Addresses.Add(noneVehicleAddress);
                        noneVehicle.People.Add(noneVehiclePerson);
                    }

                    if (noneVehicle.People.Any())
                        motorClaim.Vehicles.Add(noneVehicle);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating no vehicle involved: " + ex);
                }

                #endregion

                #region Insured Vehicle and Driver Check

                bool insuredVehicleCheck = false;
                bool insuredDriverCheck = false;

                foreach (var vehicle in motorClaim.Vehicles)
                {
                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                    {
                        insuredVehicleCheck = true;

                        foreach (var person in vehicle.People)
                        {
                            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                            {
                                insuredDriverCheck = true;
                            }
                        }
                    }
                }

                if (insuredVehicleCheck == false)
                {
                    PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                    defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                    }

                    motorClaim.Vehicles.Add(defaultInsuredVehicle);
                }
                else
                {
                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        foreach (var vehicle in motorClaim.Vehicles)
                        {
                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                            {

                                vehicle.People.Add(defaultInsuredDriver);

                            }

                        }

                    }
                }

                #endregion

            #endregion

                if (ProcessClaimFn(ctx, motorClaim, statusTracking) == -1) return;
            }

            if (boolDebug)
            {
                stopwatch.Stop();
                GetTimeSpan(stopwatch);
            }
        }

        private static void AddOrganisation(DLG5[] dlg5, PipelineMotorClaim motorClaim, DLG3 orgItem, string organisationType)
        {
            var organisation = new PipelineOrganisation();
            var organisationAddress = new PipelineAddress();

            if (organisationType == "Insurers Vehicle Engineer")
                organisation.OrganisationType_Id = (int)OrganisationType.VehicleEngineer;

            var extras = (from e in dlg5
                          where e.DM_CONTACT_ID == orgItem.Contact_Id
                          select e).FirstOrDefault();

            if (organisationType == "Insurers Vehicle Engineer")
            {
                organisation.OrganisationName = orgItem.First_Name + " " + orgItem.Last_Name;
            }
            else
            {
                if (!string.IsNullOrEmpty(orgItem.Company_Name))
                    organisation.OrganisationName = orgItem.Company_Name;
            }



            if (extras != null)
            {

                if (!string.IsNullOrEmpty(extras.UK_COMPANY_REG_NUM))
                    organisation.RegisteredNumber = extras.UK_COMPANY_REG_NUM;

                if (!string.IsNullOrEmpty(extras.EMAIL_ADDRESS_LN_1))
                {
                    PipelineEmail email1 = new PipelineEmail();

                    email1.EmailAddress = extras.EMAIL_ADDRESS_LN_1;

                    organisation.EmailAddresses.Add(email1);
                }

                if (!string.IsNullOrEmpty(extras.EMAIL_ADDRESS_LN_2) && extras.EMAIL_ADDRESS_LN_2 != extras.EMAIL_ADDRESS_LN_1)
                {
                    PipelineEmail email2 = new PipelineEmail();

                    email2.EmailAddress = extras.EMAIL_ADDRESS_LN_2;

                    organisation.EmailAddresses.Add(email2);
                }

            }

            if (!string.IsNullOrEmpty(orgItem.Description))
            {
                switch (orgItem.Description.ToUpper())
                {
                    case "ACCIDENT MANAGEMENT COMPANY":
                        organisation.OrganisationType_Id = (int)OrganisationType.AccidentManagement;
                        break;
                    case "ACCIDENT REPAIR CENTRE":
                    case "GLASS SUPPLIER":
                        organisation.OrganisationType_Id = (int)OrganisationType.Repairer;
                        break;
                    case "ASSESSOR":
                    case "ASSESSOR UTILIZED AT NOTICE OF CLAIM":
                    case "AUDIO SUPPLIER":
                    case "ENGINEER":
                    case "INSURERS VEHICLE ENGINEER":
                        organisation.OrganisationType_Id = (int)OrganisationType.VehicleEngineer;
                        break;
                    case "CAR HIRE SUPPLIER":
                    case "REPLACEMENT VEHICLE":
                        organisation.OrganisationType_Id = (int)OrganisationType.CreditHire;
                        break;
                    case "CV NOMINATED SOLICITOR":
                    case "CLAIMANT'S SOLICITOR":
                    case "CLAIMANT REPRESENTATIVE":
                    case "FORMER PRIMARY CLAIMANT SOLICITORS":
                    case "FORMER PRIMARY DEFENDANT SOLICITORS":
                    case "PRIMARY CLAIMANT SOLICITORS":
                    case "PRIMARY CLAIMANT'S SOLICITORS":
                    case "PRIMARY DEFENDANT SOLICITOR":
                    case "PRIMARY DEFENDANT SOLICITORS":
                    case "SECONDARY CLAIMANT SOLICITORS":
                    case "SECONDARY CLAIMANT'S SOLICITORS":
                    case "SECONDARY DEFENDANT SOLICITORS":
                    case "SOLICITOR":
                    case "THIRD PARTY SOLICITOR":
                        organisation.OrganisationType_Id = (int)OrganisationType.Solicitor;
                        break;

                    case "INSURANCE BROKER":
                        organisation.OrganisationType_Id = (int)OrganisationType.Broker;
                        break;

                    case "DOCTOR":
                    case "MEDICAL SPECIALIST":
                    case "OCCUPATION THERAPIST":
                    case "PHYSIOTHERAPIST":
                    case "HOSPITAL - MEDICAL CENTRE":
                        organisation.OrganisationType_Id = (int)OrganisationType.MedicalExaminer;
                        break;
                    case "THIRD PARTY INSURER":
                        organisation.OrganisationType_Id = (int)OrganisationType.Insurer;
                        break;
                    case "VEHICLE RECOVERY":
                        organisation.OrganisationType_Id = (int)OrganisationType.Recovery;
                        break;
                    default:
                        organisation.OrganisationType_Id = (int)OrganisationType.Unknown;
                        break;
                }
            }

            if (!string.IsNullOrEmpty(orgItem.WorkTelephone))
                organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = orgItem.WorkTelephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Unknown });

            if (!string.IsNullOrEmpty(orgItem.HomeTelephone))
                organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = orgItem.HomeTelephone, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });

            if (!string.IsNullOrEmpty(orgItem.Mobile))
                organisation.Telephones.Add(new PipelineTelephone() { ClientSuppliedNumber = orgItem.Mobile, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Mobile });

            #region Org Address

            organisationAddress.AddressType_Id = (int)AddressLinkType.TradingAddress;

            if (!string.IsNullOrEmpty(orgItem.Address1))
                organisationAddress.BuildingNumber = orgItem.Address1;

            if (!string.IsNullOrEmpty(orgItem.Address2))
                organisationAddress.Street = orgItem.Address2;

            if (!string.IsNullOrEmpty(orgItem.Address3))
                organisationAddress.Locality = orgItem.Address3;

            if (!string.IsNullOrEmpty(orgItem.City))
                organisationAddress.Town = orgItem.City;

            if (!string.IsNullOrEmpty(orgItem.Postcode))
                organisationAddress.PostCode = orgItem.Postcode;

            organisation.Addresses.Add(organisationAddress);

            motorClaim.Organisations.Add(organisation);

            #endregion
        }

        private void GetTimeSpan(Stopwatch stopwatch)
        {
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopwatch.Elapsed;

            // Format and display the TimeSpan value. 
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                               ts.Hours, ts.Minutes, ts.Seconds,
                                               ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
            Console.WriteLine("Finish");

            if (boolDebug)
                Console.ReadLine();
        }

        private static void DeleteExtractedFiles(string clientFolder, bool deleteExtractedFiles)
        {
            if (deleteExtractedFiles)
            {
                try
                {
                    string[] filePaths = Directory.GetFiles(clientFolder);
                    foreach (string file in filePaths)
                    {
                        File.Delete(file);
                    }
                }
                catch (Exception ex)
                {
                    throw new CustomException("Error in deleting extracted files: " + ex);
                }
            }
        }


    }
}
