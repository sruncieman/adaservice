﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.DLG.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class DLG2
    {


        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? Claim_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? Claim_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? Policy_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Policy_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? DM_Claim_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? DM_Exposue_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DM_Claimant_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DM_Contact_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? DM_Incident_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Claimant_Type_Desc;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_Claimant_Birth_Date;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_Claimant_First_Name;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_Claimant_Gender;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_Claimant_Last_Name;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_Claimant_NI_Number;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_Claimant_Title;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_Hospital_Attended_TL;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_REP_Address1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_REP_Address2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_REP_Address3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_REP_Name;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_REP_Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Exposure_Close_Date;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Exposure_Closed_Outcome_Desc;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Exposure_State_Desc;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Exposure_Type_Desc;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Loss_Party_Type_Desc;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String New_CRU_REP_Addr1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String New_CRU_REP_Addr2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String New_CRU_REP_Addr3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String New_CRU_REP_Name;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String New_CRU_REP_Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Road_Accident_Desc;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Vehicle_Group_Desc;
        [FieldConverter(typeof(CustomDateConverter2))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? Exposure_Close_Dt;
        [FieldConverter(typeof(CustomDateConverter2))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? Exposure_Assignment_Dt;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Reserve_Amount;
        [FieldConverter(typeof(CustomDateConverter2))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? CRU_CLAIMANT_BIRTH_DT;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CRU_HOSPITAL_ATTENDED;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String EXPOSURE_STATE_DES;
    }
}
