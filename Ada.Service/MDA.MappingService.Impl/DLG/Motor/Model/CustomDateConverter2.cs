﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using FileHelpers;

namespace MDA.MappingService.DLG.Model
{
    public class CustomDateConverter2 : ConverterBase
    {
        public override object StringToField(string value)
        {
            if (value.Length > 1)
            {
                DateTime dt;
                string concatDt = string.Empty;

                string dd = value.Substring(0, 2);
                string mm = ConvertMonth(value.Substring(2, 3));
                string yy = value.Substring(7, 2);
                concatDt = dd + "/" + mm + "/20" + yy;


                if (DateTime.TryParseExact(concatDt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
                    return dt;
            }
            return null;
        }

        private string ConvertMonth(string strMonth)
        {
            string strMonth2Digits = string.Empty;

            switch (strMonth)
            {
                case "JAN":
                    strMonth2Digits = "01";
                    break;
                case "FEB":
                    strMonth2Digits = "02";
                    break;
                case "MAR":
                    strMonth2Digits = "03";
                    break;
                case "APR":
                    strMonth2Digits = "04";
                    break;
                case "MAY":
                    strMonth2Digits = "05";
                    break;
                case "JUN":
                    strMonth2Digits = "06";
                    break;
                case "JUL":
                    strMonth2Digits = "07";
                    break;
                case "AUG":
                    strMonth2Digits = "08";
                    break;
                case "SEP":
                    strMonth2Digits = "09";
                    break;
                case "OCT":
                    strMonth2Digits = "10";
                    break;
                case "NOV":
                    strMonth2Digits = "11";
                    break;
                case "DEC":
                    strMonth2Digits = "12";
                    break;
            }

            return strMonth2Digits;
        }

    }
}
