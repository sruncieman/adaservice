﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using FileHelpers;

namespace MDA.MappingService.DLG.Model
{
    public class CustomDateConverter1 : ConverterBase
    {
        public override object StringToField(string value)
        {
            DateTime dt;

            string dd = value.Substring(0, 2);
            string mm = ConvertMonth(value.Substring(2, 3));
            string yyyy = value.Substring(5, 4);
            string concatDt = dd + "/" + mm + "/" + yyyy;

            if (DateTime.TryParseExact(concatDt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
                return dt;

            return null;
        }

        private string ConvertMonth(string strMonth)
        {
            string strMonth2Digits = string.Empty;

            switch (strMonth)
            {
                case "JAN":
                    strMonth2Digits = "01";
                    break;
                case "FEB":
                    strMonth2Digits = "02";
                    break;
                case "MAR":
                    strMonth2Digits = "03";
                    break;
                case "APR":
                    strMonth2Digits = "04";
                    break;
                case "MAY":
                    strMonth2Digits = "05";
                    break;
                case "JUN":
                    strMonth2Digits = "06";
                    break;
                case "JUL":
                    strMonth2Digits = "07";
                    break;
                case "AUG":
                    strMonth2Digits = "08";
                    break;
                case "SEP":
                    strMonth2Digits = "09";
                    break;
                case "OCT":
                    strMonth2Digits = "10";
                    break;
                case "NOV":
                    strMonth2Digits = "11";
                    break;
                case "DEC":
                    strMonth2Digits = "12";
                    break;
            }

            return strMonth2Digits;
        }

    }
}
