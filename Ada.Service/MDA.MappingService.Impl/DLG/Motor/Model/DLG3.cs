﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.DLG.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class DLG3
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? Claim_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Contact_Id;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Company_Name;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Last_Name;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String First_Name;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Middle_Name;
        [FieldConverter(typeof(CustomDateConverter1))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DOB;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String City;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Postcode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String HomeTelephone;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String WorkTelephone;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Mobile;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? Role;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Description;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MiddleName;
    }
}
