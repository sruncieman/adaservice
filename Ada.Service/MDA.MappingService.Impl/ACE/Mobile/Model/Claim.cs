﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.ACE.Mobile.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    public sealed class Claim
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Title;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CustomerForname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CustomerSurname;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public int? ClaimHistoryCounter;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredHandsetMake;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredHandsetModel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredHandsetIMEI;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public decimal? HandsetPrice;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string UpdatedProductMake;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string UpdatedProductModel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string UpdatedProductIMEI;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CustomerAddressLine1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CustomerAddressLine2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CustomerAddressLine3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CustomerAddressLine4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string S6PostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CustomerEmail;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AlternateEmail;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MobileNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Incident_Type;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CauseOfLoss;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IncidentLocation;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DeviceFault;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? IncidentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? ClaimNotificationDate;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PoliceNotifiedDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CrimeRefNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PoliceStation;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string BypassFraud;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ReplacementPhoneMake;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ReplacementPhoneModel;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ReplacementPhoneIMEI;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ReplacementPhoneGrade;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DeliveryAddressLine1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DeliveryAddressLine2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DeliveryAddressLine3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DeliveryAddressLine4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string DeliveryPostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PickUpAddress1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PickUpADDRESS2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string pICKuPAddress3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PickUpAddress4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PickUpCity;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PickUpPostCode;
        //[FieldQuoted('"', QuoteMode.OptionalForBoth)]
        //public string PickTime;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string JiffyBagAddress1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string JiffyBagAddress2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string JiffyBagAddress3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string JiffyBagAddress4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string JiffyBagCity;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string JiffyBagPostCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TotalClaimCost;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public decimal? TotalLessExcessCost;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimStatus;
    }
}
