﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDA.MappingService.ACE.Mobile.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    [IgnoreEmptyLines]
    public sealed class Policy
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string POLICY;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? START_DATE;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public decimal? GROSS_PREMIUM;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FIRST_NAME;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SURNAME;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DATE_OF_BIRTH;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string COMPANY_NAME;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ADDRESS_LINE_1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ADDRESS_LINE_2;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ADDRESS_LINE_3;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ADDRESS_LINE_4;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string POST_CODE;
        [FieldConverter(typeof(CustomDateConverter))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? CANCEL_DATE;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PRODUCT_INSURED;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MOBILE_NUMBER;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IMEI;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string CUSTOMER_TYPE;
    }
}
