﻿using System;
using FileHelpers;
// ReSharper disable InconsistentNaming

namespace MDA.MappingService.Impl.Broadspire.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClaimantDriverClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SystemNumber;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? IncidentDate;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateIncidentNotifiedToCrawford;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IncidentDescription;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public decimal? TotalIncidentReserve;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public decimal? TotalIncidentPaid;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string IncidentStatus;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Programme;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyNumber;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PolicyStartDate;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? PolicyExpiryDate;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PolicyHolderNameOrganisation;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredEmailAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string InsuredPostCode;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehicleRegIndex;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehicleMake;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string VehicleModel;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ClaimantType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Title;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LastName;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateOfBirth;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SocialSecurityNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TelephoneNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MobileNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EmailAddress;
       
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address;
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PostCode;
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string HireCompanyName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TypeOfHire;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateOfHireStarted;

        [FieldConverter(typeof(CustomDateConvertor))]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public DateTime? DateOfHireEnded;
    }
}
