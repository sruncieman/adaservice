﻿using System;
using FileHelpers;
// ReSharper disable InconsistentNaming

namespace MDA.MappingService.Impl.Broadspire.Motor.Model
{
    [IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class HireSolicitorsClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string SystemNumber;
        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Programme;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PartyType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Claimants;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PartyName;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PartyIsOrganisation;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Reference;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string TelephoneNumber;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string EmailAddress;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Postcode;
    }
}
