﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Ionic.Zip;
using MDA.MappingService.Impl.Broadspire.Motor.Model;

namespace MDA.MappingService.Impl.Broadspire.Motor
{
    public class Mapper : IMapper
        {
        private readonly Stream _fs;
        private StreamReader _claimDataFileStreamReader;
        private FileHelperEngine _ownDriverClaimDataEngine;
        private FileHelperEngine _claimantDriverClaimDataEngine;
        private FileHelperEngine _claimantOtherClaimDataEngine;
        private FileHelperEngine _hireSolicitorsClaimEngine;


        private List<string> DefaultAddressesForClient { get; set; }
        private List<string> DefaultPeopleForClient { get; set; }
        private List<string> DefaultOrganisationsForClient { get; set; }
        
        private OwnDriverClaimData[] _ownDriverClaimData;
        private ClaimantDriverClaimData[] _claimantDriverClaimData;
        private ClaimantOtherClaimData[] _claimantOtherClaimData;
        private HireSolicitorsClaimData[] _hireSolicitorsClaimData;

        private string OwnDriverFilePath { get; set; }
        private string ClaimantDriverFilePath { get; set; }
        private string ClaimantOtherFilePath { get; set; }
        private string HireSolicitorsFilePath { get; set; }


        private readonly Func<CurrentContext, IPipelineClaim, object, int> _processClaimFn;
        public string ClientFolder { get; set; }
        public string ClaimFile { get; set; }
        public CurrentContext Ctx { get; set; }
        private readonly object _statusTracking;
        private List<string> _uniqueClaimNumberList;
        public List<string> FileNames;

        public Mapper(Stream fs, string clientFolder, CurrentContext ctx,
            Func<CurrentContext, IPipelineClaim, object, int> processClaimFn, object statusTracking,
                        List<string> defaultAddressesForClient,
                        List<string> defaultPeopleForClient,
                        List<string> defaultOrganisationsForClient)
        
        {
            _fs = fs;
            ClientFolder = clientFolder;
            Ctx = ctx;
            _processClaimFn = processClaimFn;
            _statusTracking = statusTracking;
            
            _uniqueClaimNumberList = new List<string>();

            FileNames = new List<string>();
            DefaultAddressesForClient = defaultAddressesForClient;
            DefaultPeopleForClient = defaultPeopleForClient;
            DefaultOrganisationsForClient = defaultOrganisationsForClient;
            if (_fs != null)
            {
                try
                {

                    using (var zip1 = ZipFile.Read(_fs))
                    {

                        if (zip1 == null) { throw new Exception("Error reading Zip file"); }

                        foreach (var e in zip1)
                        {
                            var extractTmpPath = string.Concat(ClientFolder + @"\" + e.FileName, ".tmp");

                            //check if the temp file already exists 
                            if (File.Exists(extractTmpPath))
                                File.Delete(extractTmpPath);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, "Unpacking Zip : " + e.FileName, false);

                            var unpackDirectory = ClientFolder;

                            // Could have passwoord
                            var extractSuccess = TryFileExtractor(e, string.Empty, unpackDirectory);
                            if (!extractSuccess)
                            {
                                throw new ZipException();
                            }

                            FileNames.Add("\\" + e.FileName);

                            if (Ctx.ShowProgress != null)
                                Ctx.ShowProgress(ProgressSeverity.Info, " : Done", true);
                        }
                    }

                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in extraction of files: " + ex);
                }
            }
        }

        /// <summary>
        /// Tries to extract files, returns false if bad password
        /// </summary>
        /// <param name="zipFileEntry"><see cref="ZipEntry"/>ZipEntry</param>
        /// <param name="password"><see cref="string"/>string of password (optional)</param>
        /// <param name="unpackDirectory"><see cref="string"/>string path of folder to extract to</param>
        /// <returns>true if successful</returns>
        private static bool TryFileExtractor(ZipEntry zipFileEntry, string password, string unpackDirectory)
        {
            if (zipFileEntry == null) throw new ArgumentNullException("zipFileEntry");

            if (string.IsNullOrEmpty(unpackDirectory))
                throw new ArgumentException("Value cannot be null or empty.", "unpackDirectory");

            try
            {
                if (!string.IsNullOrEmpty(password))
                {
                    zipFileEntry.Password = password;
                }

                zipFileEntry.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

            }
            catch (BadPasswordException)
            {
                return false;
            }
            return true;
        }

        public void AssignFiles()
        {
            OwnDriverFilePath = ClientFolder + FileNames.First(x => x.Contains("owndriver"));
            ClaimantDriverFilePath = ClientFolder + FileNames.First(x => x.Contains("claimantdriver"));
            ClaimantOtherFilePath = ClientFolder + FileNames.First(x => x.Contains("claimantother"));
            HireSolicitorsFilePath = ClientFolder + FileNames.First(x => x.Contains("hiresolicitorsdetails"));           
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _ownDriverClaimDataEngine = new FileHelperEngine(typeof(OwnDriverClaimData));
                _claimantDriverClaimDataEngine = new FileHelperEngine(typeof(ClaimantDriverClaimData));
                _claimantOtherClaimDataEngine = new FileHelperEngine(typeof(ClaimantOtherClaimData));
                _hireSolicitorsClaimEngine = new FileHelperEngine(typeof(HireSolicitorsClaimData));


            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {
                _ownDriverClaimData = _ownDriverClaimDataEngine.ReadFile(OwnDriverFilePath) as OwnDriverClaimData[];
                _claimantDriverClaimData = _claimantDriverClaimDataEngine.ReadFile(ClaimantDriverFilePath) as ClaimantDriverClaimData[];
                _claimantOtherClaimData = _claimantOtherClaimDataEngine.ReadFile(ClaimantOtherFilePath) as ClaimantOtherClaimData[];
                _hireSolicitorsClaimData = _hireSolicitorsClaimEngine.ReadFile(HireSolicitorsFilePath) as HireSolicitorsClaimData[];
                
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

       
        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            throw new NotImplementedException();
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers

                DateTime skipDate;

                var skipdateValue = ConfigurationManager.AppSettings["BroadspireSkipDate"];
                
                if (!string.IsNullOrEmpty(skipdateValue))
                {
                    if (DateTime.TryParseExact(skipdateValue, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out skipDate))
                    {
                        _uniqueClaimNumberList = _ownDriverClaimData.Where(x => x.SystemNumber != "" && x.IncidentDate.Value >= skipDate).Select(x => x.SystemNumber).AsParallel().Distinct().ToList();
                        return;
                    }
                }

                _uniqueClaimNumberList = _ownDriverClaimData.Where(x => x.SystemNumber != "").Select(x => x.SystemNumber).AsParallel().Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }
        }

        public void Translate()
        {
            var _skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["BroadspireSkipNum"]);
            var _takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["BroadspireTakeNum"]);

            if (_skipValue >= 0 && _takeValue > 0)
            {
                _uniqueClaimNumberList = _uniqueClaimNumberList.Skip(_skipValue).Take(_takeValue).ToList();
            }

            foreach (var claim in _uniqueClaimNumberList)
            {
                var motorClaim = new PipelineMotorClaim();

                var uniqueClaim = _ownDriverClaimData.FirstOrDefault(x => x.SystemNumber == claim);

                var linkedClaimantDriver = _claimantDriverClaimData.Where(x => x.SystemNumber == claim).ToList();
                var linkedClaimantOther = _claimantOtherClaimData.Where(x => x.SystemNumber == claim).ToList();
                var linkedHireSolicitor = _hireSolicitorsClaimData.Where(x => x.SystemNumber == claim).ToList();


                if (uniqueClaim != null)
                {
                    #region Claim

                    try
                    {
                        if (IsValid(uniqueClaim.SystemNumber))
                            motorClaim.ClaimNumber = uniqueClaim.SystemNumber;

                        if (uniqueClaim.IncidentDate!= null)
                            motorClaim.IncidentDate = uniqueClaim.IncidentDate.Value;                        

                        motorClaim.ClaimType_Id = (int) ClaimType.Motor;
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim: " + ex);
                    }

                    #endregion Claim

                    #region ClaimInfo
                    // Get all records in import linked to this Claim Number
                    var claimInfo = _ownDriverClaimData
                        .Where(c => c.SystemNumber == claim)
                        .ToList();

                    try
                    {
                        if (IsValid(uniqueClaim.IncidentStatus))
                        {
                            switch (uniqueClaim.IncidentStatus.ToUpper())
                            {
                                case "OPEN":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int) ClaimStatus.Open;
                                    break;
                                case "CLOSED":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                    break;
                                case "RE-OPENED":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                                    break;
                                case "INCIDENT ONLY":
                                    motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int) ClaimStatus.Open;
                                    break; 
                                default:
                                    throw new InvalidOperationException(
                                        string.Format("Invalid status: [{0}] on claimId [{1}]", uniqueClaim.IncidentStatus,
                                            uniqueClaim.SystemNumber));
                            }
                        }

                        if (uniqueClaim.DateIncidentNotifiedToCrawford != null)
                            motorClaim.ExtraClaimInfo.ClaimNotificationDate = uniqueClaim.DateIncidentNotifiedToCrawford;



                        if (IsValid(uniqueClaim.IncidentDescription))
                            motorClaim.ExtraClaimInfo.IncidentCircumstances = uniqueClaim.IncidentDescription;

                        
                        motorClaim.ExtraClaimInfo.Reserve = uniqueClaim.TotalIncidentReserve;

                        motorClaim.ExtraClaimInfo.PaymentsToDate = uniqueClaim.TotalIncidentPaid;
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping claim information: " + ex);
                    }

                    #endregion ClaimInfo

                    #region Policy

                    try
                    {
                        motorClaim.Policy.Insurer = uniqueClaim.Programme;                        

                        if (IsValid(uniqueClaim.PolicyNumber))
                            motorClaim.Policy.PolicyNumber = uniqueClaim.PolicyNumber;

                        if (uniqueClaim.PolicyStartDate.HasValue)
                            motorClaim.Policy.PolicyStartDate = uniqueClaim.PolicyStartDate;

                        if (uniqueClaim.PolicyExpiryDate.HasValue)
                            motorClaim.Policy.PolicyEndDate = uniqueClaim.PolicyExpiryDate;  

                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping policy: " + ex);
                    }

                    #endregion Policy

                    #region Vehicle(s) And Person(s) and Organisation(s)

                    try
                    {
                        // todo: find organisations that are policy holder

                        var insuredOthers = linkedClaimantOther.Where(w => w.VehicleRegIndex == uniqueClaim.VehicleRegIndex).ToList();

                        var insuredVehicle = GenerateInsuredVehicle(uniqueClaim, insuredOthers);

                        motorClaim.Vehicles.Add(insuredVehicle);

                        var thirdPartyVehicles = CreateThirdPartyVehicles(linkedClaimantDriver, linkedClaimantOther);

                        motorClaim.Vehicles.AddRange(thirdPartyVehicles);

                        // todo: find organisations linked to people
                    }
                    catch (Exception ex)
                    {
                        throw new CustomException("Error in mapping vehicle information: " + ex);
                    }

                    #endregion

                }

                if (_processClaimFn(Ctx, motorClaim, _statusTracking) == -1) return;
            }

            
        }

        private List<PipelinePerson> GenerateThirdPartyPersons(List<ClaimantOtherClaimData> data)
        {

            if (data == null)
            {
                return null;
            }

            var results = new List<PipelinePerson>();

            foreach (var claimData in data)
            {
                var person = new PipelinePerson
                {
                    I2Pe_LinkData =
                    {
                        PartyType_Id = (int)PartyType.Claimant,
                    }
                };


                switch (claimData.ClaimantType)
                {
                    case "Own Passenger":
                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                        person.V2Pe_LinkData.VehicleLinkType_Id = (int)VehicleLinkType.Passenger;
                        break;
                    default:
                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                        person.V2Pe_LinkData.VehicleLinkType_Id = (int)VehicleLinkType.Unknown;
                        break;
                }



                if (IsValid(claimData.Title))
                    person.Salutation_Id = (int)SalutationConvertor.ConvertTo(claimData.Title);

                if (IsValid(claimData.FirstName))
                    person.FirstName = claimData.FirstName;

                if (IsValid(claimData.LastName))
                    person.LastName = claimData.LastName;

                if (claimData.DateOfBirth.HasValue)
                    person.DateOfBirth = claimData.DateOfBirth;

                if (IsValid(claimData.SocialSecurityNumber))
                {
                    person.NINumbers = new List<PipelineNINumber> { new PipelineNINumber { NINumber1 = claimData.SocialSecurityNumber } };
                }

                if (IsValid(claimData.TelephoneNumber))
                {
                    if (IsMobile(claimData.TelephoneNumber))
                    {
                        person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claimData.TelephoneNumber, TelephoneType_Id = (int)TelephoneType.Mobile });
                    }
                    else
                    {
                        person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claimData.TelephoneNumber, TelephoneType_Id = (int)TelephoneType.Landline });
                    }
                }

                if (IsValid(claimData.MobileNumber) && claimData.MobileNumber != claimData.TelephoneNumber)
                {
                    person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claimData.MobileNumber, TelephoneType_Id = (int)TelephoneType.Mobile });
                }

                if (IsValid(claimData.EmailAddress))
                {
                    person.EmailAddresses = new List<PipelineEmail> { new PipelineEmail { EmailAddress = claimData.EmailAddress } };
                }

                if (IsValid(claimData.Address))
                {
                    var address = new PipelineAddress { Street = claimData.Address, PostCode = claimData.PostCode };
                    if (!_AddressMatch(address))
                    person.Addresses = new List<PipelineAddress> { address };
                }

                if (!_PersonMatch(person))
                {
                    results.Add(person);
                }
            }

            return results;
        }

        private PipelinePerson GenerateThirdPartyDriver(ClaimantDriverClaimData data)
        {
            var person = new PipelinePerson
            {
                I2Pe_LinkData =
                {
                    PartyType_Id = (int)PartyType.ThirdParty,
                    SubPartyType_Id = (int)SubPartyType.Driver,
                },
                V2Pe_LinkData = {
                    VehicleLinkType_Id = (int)VehicleLinkType.Driver
                }
            };


            if (IsValid(data.Title))
                person.Salutation_Id = (int)SalutationConvertor.ConvertTo(data.Title);

            if (IsValid(data.FirstName))
                person.FirstName = data.FirstName;

            if (IsValid(data.LastName))
                person.LastName = data.LastName;

            if (data.DateOfBirth.HasValue)
                person.DateOfBirth = data.DateOfBirth;

            if (IsValid(data.SocialSecurityNumber))
            {
                person.NINumbers = new List<PipelineNINumber> { new PipelineNINumber { NINumber1 = data.SocialSecurityNumber } };
            }

            if (IsValid(data.TelephoneNumber))
            {
                if (IsMobile(data.TelephoneNumber))
                {
                    person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.TelephoneNumber, TelephoneType_Id = (int)TelephoneType.Mobile });
                }
                else
                {
                    person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.TelephoneNumber, TelephoneType_Id = (int)TelephoneType.Landline });
                }
            }

            if (IsValid(data.MobileNumber) && data.MobileNumber != data.TelephoneNumber)
            {
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.MobileNumber, TelephoneType_Id = (int)TelephoneType.Mobile });
            }

            if (IsValid(data.EmailAddress))
            {
                person.EmailAddresses = new List<PipelineEmail> { new PipelineEmail { EmailAddress = data.EmailAddress } };
            }

            if (IsValid(data.Address))
            {
                var address = new PipelineAddress { Street = data.Address, PostCode = data.PostCode };
                if (!_AddressMatch(address))
                    person.Addresses = new List<PipelineAddress> { address };
            }

            return person;

        }
        private PipelinePerson GenerateInsuredDriver(OwnDriverClaimData data)
        {
            var person = new PipelinePerson
            {
                I2Pe_LinkData =
                {
                    PartyType_Id = (int)PartyType.Insured,
                    SubPartyType_Id = (int)SubPartyType.Driver,                
                },
                V2Pe_LinkData =
                {
                    VehicleLinkType_Id = (int)VehicleLinkType.Driver
                }
            };

            // Found policy holder
            if (data.PolicyHolderNameOrganisation.Equals(data.FirstName + " " + data.LastName))
            {
                person.Pe2Po_LinkData.PolicyLinkType_Id = (int)PolicyLinkType.Policyholder;
            };
            
            if (IsValid(data.Title))
                person.Salutation_Id = (int)SalutationConvertor.ConvertTo(data.Title);

            if (IsValid(data.FirstName))
                person.FirstName = data.FirstName;

            if (IsValid(data.LastName))
                person.LastName = data.LastName;

            if (data.DateOfBirth.HasValue)
                person.DateOfBirth = data.DateOfBirth;

            if (IsValid(data.SocialSecurityNumber))
            {
                person.NINumbers = new List<PipelineNINumber> {new PipelineNINumber {NINumber1 = data.SocialSecurityNumber }};
            }

            if (IsValid(data.TelephoneNumber))
            {
                if (IsMobile(data.TelephoneNumber))
                {
                    person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.TelephoneNumber, TelephoneType_Id = (int)TelephoneType.Mobile });    
                }
                else
                {
                    person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.TelephoneNumber, TelephoneType_Id = (int)TelephoneType.Landline });        
                }                    
            }

            if (IsValid(data.MobileNumber) && data.MobileNumber != data.TelephoneNumber)
            {
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = data.MobileNumber, TelephoneType_Id = (int)TelephoneType.Mobile });        
            }  
              
            if (IsValid(data.EmailAddress))
            {
                person.EmailAddresses = new List<PipelineEmail> {new PipelineEmail {EmailAddress = data.EmailAddress}};
            }

            if (IsValid(data.Address))
            {
                var address = new PipelineAddress { Street = data.Address, PostCode = data.PostCode };
                if (!_AddressMatch(address))
                    person.Addresses = new List<PipelineAddress> { address };
            }
            return person;
         
        }

        private List<PipelinePerson> GenerateInsuredPersons(List<ClaimantOtherClaimData> data)
        {
            if (data == null)
            {
                return null;
            }

            var results = new List<PipelinePerson>();

            foreach (var claimData in data)
            {
                var person = new PipelinePerson
                {
                    I2Pe_LinkData =
                    {
                        PartyType_Id = (int)PartyType.Insured,                        
                    }
                };
                
                switch (claimData.ClaimantType)
                {
                    case "Own Passenger":
                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                        person.V2Pe_LinkData.VehicleLinkType_Id = (int)VehicleLinkType.Passenger;
                        break;
                    default:
                        person.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;
                        person.V2Pe_LinkData.VehicleLinkType_Id = (int)VehicleLinkType.Unknown;
                        break;
                }



                if (IsValid(claimData.Title))
                    person.Salutation_Id = (int)SalutationConvertor.ConvertTo(claimData.Title);

                if (IsValid(claimData.FirstName))
                    person.FirstName = claimData.FirstName;

                if (IsValid(claimData.LastName))
                    person.LastName = claimData.LastName;

                if (claimData.DateOfBirth.HasValue)
                    person.DateOfBirth = claimData.DateOfBirth;

                if (IsValid(claimData.SocialSecurityNumber))
                {
                    person.NINumbers = new List<PipelineNINumber> { new PipelineNINumber { NINumber1 = claimData.SocialSecurityNumber } };
                }

                if (IsValid(claimData.TelephoneNumber))
                {
                    if (IsMobile(claimData.TelephoneNumber))
                    {
                        person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claimData.TelephoneNumber, TelephoneType_Id = (int)TelephoneType.Mobile });
                    }
                    else
                    {
                        person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claimData.TelephoneNumber, TelephoneType_Id = (int)TelephoneType.Landline });
                    }
                }

                if (IsValid(claimData.MobileNumber) && claimData.MobileNumber != claimData.TelephoneNumber)
                {
                    person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claimData.MobileNumber, TelephoneType_Id = (int)TelephoneType.Mobile });
                }

                if (IsValid(claimData.EmailAddress))
                {
                    person.EmailAddresses = new List<PipelineEmail> { new PipelineEmail { EmailAddress = claimData.EmailAddress } };
                }

                if (IsValid(claimData.Address))
                {
                    var address = new PipelineAddress { Street = claimData.Address, PostCode = claimData.PostCode };
                    if (!_AddressMatch(address))
                        person.Addresses = new List<PipelineAddress> { address };
                }

                results.Add(person);
            }

            return results;
        }

        private bool IsMobile(string telNo)
        {
            if (string.IsNullOrEmpty(telNo)) return false;

            if (telNo.StartsWith("07"))
            {
                return true;
            }

            return false;
        }

        private List<PipelineOrganisation> TranslateOrganisations(OwnDriverClaimData data)
        {
            var orgs = new List<PipelineOrganisation>();

            //if (IsValid(data.Credit_Hire_1_SP))
            //{
            //    var org = new PipelineOrganisation
            //    {
            //        OrganisationName = data.Credit_Hire_1_SP,
            //        OrganisationType_Id = (int) OrganisationType.CreditHire,
            //        P2O_LinkData = {Person2OrganisationLinkType_Id = (int) Person2OrganisationLinkType.Hire}
            //    };

            //    if (IsValid(data.Credit_Hire_1_ADD1_SP) && IsValid(data.Credit_Hire_1_PCode_SP))
            //    {
            //        var address = TranslateAddress(data.Credit_Hire_1_ADD1_SP, data.Credit_Hire_1_ADD2_SP, null, data.Credit_Hire_1_PCode_SP);
            //        if (address != null) {org.Addresses.Add(address);}
            //    }

            //    orgs.Add(org);
            //}

            //if (IsValid(data.Credit_Hire_2_SP))
            //{
            //    var org = new PipelineOrganisation
            //    {
            //        OrganisationName = data.Credit_Hire_2_SP,
            //        OrganisationType_Id = (int)OrganisationType.CreditHire,
            //        P2O_LinkData = { Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire }
            //    };               
            //    orgs.Add(org);
            //}

            //if (IsValid(data.Credit_Hire_1_IP))
            //{
            //    var org = new PipelineOrganisation
            //    {
            //        OrganisationName = data.Credit_Hire_1_IP,
            //        OrganisationType_Id = (int)OrganisationType.CreditHire,
            //        P2O_LinkData = { Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire }
            //    };
            //    orgs.Add(org);
            //}

            //if (IsValid(data.Clmnt_Solicitor_IP))
            //{
            //    var org = new PipelineOrganisation
            //    {
            //        OrganisationName = data.Clmnt_Solicitor_IP,
            //        OrganisationType_Id = (int)OrganisationType.Solicitor,
            //        P2O_LinkData = { Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Solicitor }
            //    };

            //    if (IsValid(data.Clmnt_Sol_Add1) && IsValid(data.Clmnt_Sol_Pcode))
            //    {
            //        var address = TranslateAddress(data.Clmnt_Sol_Add1, data.Clmnt_Sol_Add2, null, data.Clmnt_Sol_Pcode);
            //        if (address != null) { org.Addresses.Add(address); }
            //    }

            //    orgs.Add(org);
            //}



            return orgs;
        }


        private List<PipelineVehicle> CreateThirdPartyVehicles(List<ClaimantDriverClaimData> _claimantDriverClaimData, List<ClaimantOtherClaimData> _claimantOtherClaimData)
        {
            var vehicles = new List<PipelineVehicle> ();

            // add drivers

            PipelineVehicle thirdPartyVehicle = null;

            bool addNewVehicle = false;
            foreach (var driverData in _claimantDriverClaimData.OrderBy(o => o.VehicleRegIndex))
            {
                addNewVehicle = false;

                if (thirdPartyVehicle == null || thirdPartyVehicle.VehicleRegistration != driverData.VehicleRegIndex)
                {
                    // create new car
                    thirdPartyVehicle = MapToPipelineThirdPartyVehicle(driverData);
                    thirdPartyVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                    thirdPartyVehicle.People = new List<PipelinePerson>();

                    addNewVehicle = true;
                }
                
                // add drivers to car
                var thirdPartyDriver = GenerateThirdPartyDriver(driverData);

                if (!_PersonMatch(thirdPartyDriver))
                {
                    thirdPartyVehicle.People.Add(thirdPartyDriver);
                }
                
                var otherPeopleToAdd = _claimantOtherClaimData.Where(w=>w.VehicleRegIndex == driverData.VehicleRegIndex).ToList();

                var otherPeople = GenerateThirdPartyPersons(otherPeopleToAdd);

                if (otherPeople != null && otherPeople.Count > 0)
                {
                    thirdPartyVehicle.People.AddRange(otherPeople);
                }


                if (addNewVehicle)
                {
                    vehicles.Add(thirdPartyVehicle);
                }

            }

            return vehicles;
        }

            

        private PipelineVehicle MapToPipelineThirdPartyVehicle(ClaimantDriverClaimData claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var insuredVehicle = new PipelineVehicle
            {
                VehicleMake = DefaultValue(claimInfo.VehicleMake),
                VehicleModel = DefaultValue(claimInfo.VehicleModel),
                VehicleRegistration = DefaultValue(claimInfo.VehicleRegIndex)
            };           

            return insuredVehicle;
        }

        private PipelineVehicle MapToPipelineInsuredVehicle(OwnDriverClaimData claimInfo)
        {
            if (claimInfo == null) { throw new ArgumentNullException("claimInfo"); }

            var insuredVehicle = new PipelineVehicle
            {
                VehicleMake = DefaultValue(claimInfo.VehicleMake),
                VehicleModel = DefaultValue(claimInfo.VehicleModel),
                VehicleRegistration = DefaultValue(claimInfo.VehicleRegIndex)
            };

            return insuredVehicle;
        }

        private PipelineVehicle GenerateInsuredVehicle(OwnDriverClaimData claimInfo, List<ClaimantOtherClaimData> _claimantOtherClaimData)
        {
            if (claimInfo == null) {  throw new ArgumentNullException("claimInfo");}

            var insuredVehicle = MapToPipelineInsuredVehicle(claimInfo);
            insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

            var insuredPerson = GenerateInsuredDriver(claimInfo);

            if (!_PersonMatch(insuredPerson))
            {
                insuredVehicle.People.Add(insuredPerson);
            }
            var otherInsuredPeople = GenerateInsuredPersons(_claimantOtherClaimData);
            if (otherInsuredPeople != null)
            {
                foreach (var person in otherInsuredPeople)
                {
                    if (!_PersonMatch(person))
                    {
                        insuredVehicle.People.Add(person);    
                    }                    
                }
            }

            return insuredVehicle;
        }

       

        /// <summary>
        /// Detect if "NULL" is in string and ignore, value is null or value is empty
        /// </summary>
        /// <param name="value"><see cref="String"/> or null</param>
        /// <returns><see cref="bool"/>true if string has valid value</returns>
        public static bool IsValid(string value)
        {
            return !string.IsNullOrWhiteSpace(value) && 
                   !value.Equals("NULL", StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// If input string value is null, return null
        /// </summary>
        /// <param name="value">input string</param>
        /// <returns>null if input value empty or null, else returns value</returns>
        public static string DefaultValue(string value)
        {
            return IsValid(value) ? value : null;
        }

        public List<PipelineAddress> DefaultAddresses(List<string> defaultAddressesForClient)
        {
            var defaultAddresses = new List<PipelineAddress>();

            if (defaultAddressesForClient != null)
                foreach (var str in defaultAddressesForClient)
                {
                    if (!string.IsNullOrEmpty(str))
                    {
                        var address = new PipelineAddress();

                        var match = Regex.Matches(str, @"\[(.*?)\]");

                        //SB=[],BN=[],BLD=[],ST=[],LOC=[],TWN=[],CNTY=[],PC=[]

                        address.SubBuilding = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.BuildingNumber = match[1].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.Building = match[2].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.Street = match[3].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.Locality = match[4].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.Town = match[5].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.County = match[6].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                        address.PostCode = match[7].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                        defaultAddresses.Add(address);
                    }
                }

            return defaultAddresses;
        }

        private bool _AddressMatch(PipelineAddress address)
        {

            if (address == null) return false;

            var defaultAddresses = DefaultAddresses(DefaultAddressesForClient);

            if (defaultAddresses != null)
                foreach (var adr in defaultAddresses)
                {
                    var match = false;

                    if (adr != null)
                    {
                        if (!string.IsNullOrEmpty(adr.Street) && !string.IsNullOrEmpty(address.Street))
                        {
                            if (address.Street.Equals(adr.Street, StringComparison.InvariantCultureIgnoreCase))
                            {
                                match = true;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        if (!string.IsNullOrEmpty(adr.Town) && !string.IsNullOrEmpty(address.Town))
                        {
                            if (address.Town.Equals(adr.Town, StringComparison.InvariantCultureIgnoreCase))
                            {
                                match = true;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        if (!string.IsNullOrEmpty(adr.County) && !string.IsNullOrEmpty(address.County))
                        {
                            if (address.County.Equals(adr.County, StringComparison.InvariantCultureIgnoreCase))
                            {
                                match = true;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        if (!string.IsNullOrEmpty(adr.PostCode) && !string.IsNullOrEmpty(address.PostCode))
                        {
                            if (address.PostCode.Equals(adr.PostCode, StringComparison.InvariantCultureIgnoreCase))
                            {
                                match = true;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        return match;
                    }
                }
            return false;
        }

        public List<PipelinePerson> DefaultPeople(List<string> defaultPeopleForClient)
        {
            var defaultPeople = new List<PipelinePerson>();
            if (defaultPeopleForClient == null) return defaultPeople;


            foreach (var str in defaultPeopleForClient)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    PipelinePerson person = new PipelinePerson();

                    MatchCollection match = Regex.Matches(str, @"\[(.*?)\]");

                    //FN=[],LN=[]

                    person.FirstName = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    person.LastName = match[1].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultPeople.Add(person);
                }
            }

            return defaultPeople;
        }

        public List<PipelineOrganisation> DefaultOrganisation(List<string> defaultOrganisationsForClient)
        {
            var defaultOrganisations = new List<PipelineOrganisation>();
            if (defaultOrganisationsForClient == null) return defaultOrganisations;

            foreach (var orgName in defaultOrganisationsForClient)
            {
                if (!string.IsNullOrEmpty(orgName))
                {
                    PipelineOrganisation organisation = new PipelineOrganisation();

                    MatchCollection match = Regex.Matches(orgName, @"\[(.*?)\]");

                    //ON=[]

                    organisation.OrganisationName = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultOrganisations.Add(organisation);
                }
            }

            return defaultOrganisations;
        }

        private bool _PersonMatch(PipelinePerson person)
        {
            if (person == null) return false;

            var people = DefaultPeople(DefaultPeopleForClient);

            if (people != null)
                foreach (var p in people)
                {
                    if (p != null)
                    {
                        if (person.LastName != null &&
                           (person.FirstName != null &&
                           (person.FirstName.Equals(p.FirstName, StringComparison.InvariantCultureIgnoreCase) &&
                            person.LastName.Equals(p.LastName, StringComparison.InvariantCultureIgnoreCase))))
                        {
                            return true;
                        }
                    }
                }

            return false;
        }

        private bool _OrganisationMatch(PipelineOrganisation organisation)
        {
            if (organisation == null) return false;
            var defaultOrganisations = DefaultOrganisation(DefaultOrganisationsForClient);

            if (defaultOrganisations != null)
                foreach (var o in defaultOrganisations)
                {
                    if (o != null &&
                       (organisation.OrganisationName != null &&
                        organisation.OrganisationName.Equals(o.OrganisationName, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        return true;
                    }
                }

            return false;
        }
    }
}
