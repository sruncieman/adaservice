﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using MDA.Common.Enum;
using MDA.Pipeline.Model;
using MDA.MappingService.Interface;
using MDA.Common;
using MDA.Common.Server;
using System.Data.OleDb;
using System.Text;
using FileHelpers;
using Excel;
using Ionic.Zip;
using OfficeOpenXml;

namespace MDA.MappingService.Impl.Broadspire.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        private readonly string _tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];

        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking,
            Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> processClaim,
            out ProcessingResults processingResults)
        {
            string clientFolder;

            GetFilePath(out clientFolder, ctx.RiskClientId);

            processingResults = new ProcessingResults("Mapping");

            try
            {
                var defaultDataAddressForClient = new MDA.RiskService.RiskServices(ctx).GetRiskDefaultDataAddressForClient(ctx.RiskClientId);
                var defaultDataPeopleForClient = new MDA.RiskService.RiskServices(ctx).GetRiskDefaultDataPeopleForClient(ctx.RiskClientId);
                var defaultDataOrganisationsForClient = new MDA.RiskService.RiskServices(ctx).GetRiskDefaultDataOrganisationsForClient(ctx.RiskClientId);

                var mapper = new Mapper(fs,
                                        clientFolder,
                                        ctx,
                                        processClaim,
                                        statusTracking,
                                        defaultDataAddressForClient,
                                        defaultDataPeopleForClient,
                                        defaultDataOrganisationsForClient);


                mapper.AssignFiles();
                mapper.InitialiseFileHelperEngines();
                mapper.PopulateFileHelperEngines();
                mapper.RetrieveDistinctClaims();
                mapper.Translate();
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping Broadspire file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void GetFilePath(out string clientFolder, int clientId)
        {
            clientFolder = _tempFolder + "\\" + clientId;

            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
        }

        private static string MakeFileNameSafe(string filename)
        {
            if (filename == null) throw new ArgumentNullException("filename");

            filename = Path.GetInvalidFileNameChars().Aggregate(filename, (current, c) => current.Replace(c.ToString(), ""));

            if (string.IsNullOrEmpty(filename))
            {
                throw new Exception("Bad file name");
            }

            filename = filename.Replace(" ", "").Replace("-", "");

            return filename.ToLowerInvariant();
        }

        private static void DeleteTempfiles(List<string> filesToZip)
        {
            if (filesToZip == null) throw new ArgumentNullException("filesToZip");
            if (filesToZip.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.", "filesToZip");

            foreach (var filename in filesToZip)
            {
                if (filename != null && File.Exists(filename))
                {
                    File.Delete(filename);
                }

            }
        }

        private static string GetZip(List<string> filesToZip, string zipFileName)
        {
            if (filesToZip == null) throw new ArgumentNullException("filesToZip");
            if (filesToZip.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.", "filesToZip");

            var workingdirectorypath = Path.GetDirectoryName(filesToZip.First());

            using (var zip = new ZipFile())
            {
                foreach (var file in filesToZip)
                {
                    zip.AddFile(file, "");
                }

                zip.Save(workingdirectorypath + "\\" + zipFileName);
            }

            return workingdirectorypath + "\\" + zipFileName;
        }

        public static string ConvertXlsxToCsvAndZip(string xlsxFilePath, string password, List<string> worksheetNames, string zipFileName)
        {
            if (xlsxFilePath == null) throw new ArgumentNullException("xlsxFilePath");
            if (worksheetNames == null) throw new ArgumentNullException("worksheetNames");

            var workingdirectorypath = Path.GetDirectoryName(xlsxFilePath);

            var filesToZip = new List<string>();

            var result = string.Empty;

            using (var file = new FileStream(xlsxFilePath, FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite))
            {
                using (var ep = new ExcelPackage(file, password))
                {
                    var book = ep.Workbook;

                    foreach (var wsName in worksheetNames)
                    {
                        if (book != null && book.Worksheets != null)
                        {
                            var workSheet = book.Worksheets[wsName];

                            var worksheetFilename = MakeFileNameSafe(wsName) + ".csv";

                            if (workSheet != null)
                            {
                                var start = workSheet.Dimension.Start;
                                var end = workSheet.Dimension.End;

                                var sb = new StringBuilder();

                                for (var row = start.Row; row <= end.Row; row++)
                                { // Row by row...
                                    for (var col = start.Column; col <= end.Column; col++)
                                    {
                                        var cell = workSheet.Cells[row, col].Text;

                                        sb.AppendFormat("{0}", Fix(cell));

                                        sb.Append(col < end.Column ? "," : "\r\n");
                                    }
                                }

                                filesToZip.Add(workingdirectorypath + "\\" + worksheetFilename);

                                File.WriteAllText(workingdirectorypath + "\\" + worksheetFilename, sb.ToString());

                            }



                        }
                    }

                    // go and zip up files
                    result = GetZip(filesToZip, zipFileName);

                    // delete temp files
                    DeleteTempfiles(filesToZip);
                }
            }

            return result;
        }

        private static string Fix(string cell)
        {
            if (string.IsNullOrWhiteSpace(cell))
            {
                return "\" \"";
            }

            cell = cell.Replace("\n", " ");
            cell = cell.Replace("\r", " ");
            cell = cell.Replace("\"", " ");            

            cell = "\"" + cell + "\"";

            return cell;

        }
       
        public override void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard,
            int settleTime, int batchSize, string postOp, string archiveFolder,
            Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {
            // Create structure to hold our new batch of claims
            var batch = new List<object>();
            
            string firstFileName = null;

            // Get a list of the files in the folderPath filtered by the wildCard. Search the top level only (don't go into child directories)
            var files = Directory.GetFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

            // If we find none, return
            if (!files.Any()) return;

            // If ANY files in that list have been modified in the last "settleTime" minutes return.  The directory is not stable, files may still being
            // written.  Return now and wait for the next check
            if (files.Any(x => File.GetCreationTime(x).AddMinutes(settleTime) > DateTime.Now))
                return;

            // If we get here we have files to process
            foreach (var file in files)
            {
                // Create a filename to use for this batch.  We will just use the first file name in this case
                if (firstFileName == null)
                    firstFileName = Path.GetFileName(file);

                // Add this new claim to the batch
                batch.Add(file);
                var zipFileName = string.Empty;

                #region If Batch full process it

                if (batch.Count >= batchSize)
                {                    
                    const string mimeType = "zip";

                    const string clientName = "Broadspire";

                    var fileName = Path.GetFileName(file);

                    var batchRef = clientName + "-" + fileName.Substring(11, 8);
                    var zipFileNameToCreate = batchRef + ".zip";

                    var password = ConfigurationManager.AppSettings["BroadspirePassword"];
                    var sheets = ConfigurationManager.AppSettings["BroadspireWorksheets"];
                    
                    var worksheetNames = sheets.Split('|').ToList();

                    zipFileName = ConvertXlsxToCsvAndZip(file, password, worksheetNames, zipFileNameToCreate);

                    if (string.IsNullOrEmpty(zipFileName))
                    {
                        throw new Exception("Unable to zip xlsx");
                    }

                    using (var fileStream = File.OpenRead(zipFileName))
                    {
                        var stream = new MemoryStream();
                        stream.SetLength(fileStream.Length);
                        fileStream.Read(stream.GetBuffer(), 0, (int) fileStream.Length);

                        #region Process the batch (call the callback function)

                        processClientBatch(ctx, stream, Path.GetFileName(zipFileName), mimeType, "Motor", batchRef);

                        #endregion
                    }

                    // Setup for a new batch and go round the loop again
                    batch.Clear();
                    firstFileName = null;
                }

                #endregion

                #region Process all the options we invented for our postOp functionality

                // We are passed the string we entered into config.  Now we have to code it!
                // In this case we have implemented RENAME, DELETE, ARCHIVE
                // ARCHIVE checks and (if needed) creates the destination folder. It also checks if the file already exists
                // in the destination folder (which would break the MOVE) and (if needed) adds a random string to make it unique
                switch (postOp.ToUpper())
                {
                    case "RENAME":
                        File.Move(zipFileName, zipFileName + ".old");
                        File.Move(file, file + ".old");
                        break;
                    case "DELETE":
                        File.Delete(zipFileName);
                        File.Delete(file);
                        break;
                    case "ARCHIVE":
                        if (!Directory.Exists(archiveFolder))
                            Directory.CreateDirectory(archiveFolder); // Create destination if needed

                        var fname = archiveFolder + "\\" + Path.GetFileName(zipFileName); // build dest path
                        var fname2 = archiveFolder + "\\" + Path.GetFileName(file); // build dest path

                        if (File.Exists(fname)) // Check if it already exists
                        {
                            fname += "." + Path.GetRandomFileName(); // Adds a random string to end of filename
                        }

                        if (File.Exists(fname2)) // Check if it already exists
                        {
                            fname2 += "." + Path.GetRandomFileName(); // Adds a random string to end of filename
                        }

                        File.Move(zipFileName, fname);
                        File.Move(file, fname2);
                        
                        break;
                }

                #endregion

            }

        }
    }
}
