﻿using FileHelpers;
using MDA.Common.Enum;
using MDA.Common.Helpers;
using MDA.Common.Server;
using MDA.MappingService.Excel.Motor.Model;
using MDA.Pipeline.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using MDA.MappingService.Interface;
using MDA.Common;


namespace MDA.MappingService.Excel.Motor
{
    public class Mapper : IMapper
    {
        private Stream _fs;
        private List<ClaimData> _lstClaim;
        private Func<CurrentContext, Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn;
        private object statusTracking;
        private static bool _boolDebug = true;
        public string ClientFolder { get; set; }
        public CurrentContext Ctx { get; set; }
        public bool BoolDebug { get; set; }
        public string FilePath { get; set; }
        public string FolderPath { get; set; }
        public string ClaimFile { get; set; }
       
        public FileHelperEngine ClaimEngine { get; set; }

        private string _coreClaimDataFile;
        private string _vehicleClaimDataFile;
        private bool _debug;
        private FileHelperEngine _coreClaimDataEngine;
        private ClaimData[] _coreClaimData;
        private List<string> _uniqueClaimNumberList;
        private string _claimCode;
      
        private IEnumerable<ClaimData> _claimItemRows;
        private string _filePath = null;
        private bool _deleteExtractedFiles;
        private int _claimCount = 0;
        private string _clientFolder;
        private StreamReader _claimDataFileStreamReader;


        public Mapper(Stream fs, string clientFolder, CurrentContext ctx, Func<CurrentContext, IPipelineClaim, object, int> ProcessClaimFn, object statusTracking)
        {
            this._fs = fs;
            this.ClientFolder = clientFolder;
            this.Ctx = ctx;
            this.ProcessClaimFn = ProcessClaimFn;
            this.statusTracking = statusTracking;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
          
            _lstClaim = new List<ClaimData>();
        }

        public void AssignFiles()
        {

            if (_fs == null)
            {
                FolderPath = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Dataforce\Input Template";
                ClaimFile = FolderPath + @"\Test File.csv";
            }
            else
            {
                try
                {
                    _claimDataFileStreamReader = new StreamReader(_fs);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in creating csv file: " + ex);
                }
            }
        }

        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }

        public void PopulateFileHelperEngines()
        {
            try
            {

                if (_fs == null)
                {
                    _coreClaimData = _coreClaimDataEngine.ReadFile(ClaimFile) as ClaimData[];
                }
                else
                {
                    _coreClaimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
                }


            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }

        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            try
            {
                if (_debug)
                {
                    Console.WriteLine("Start");
                }

                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "Populate File Helper Engine (core) : ", false);
                }

                _coreClaimData = _coreClaimDataEngine.ReadFile(_coreClaimDataFile) as ClaimData[];

            }

            catch (Exception ex)
            {
                if (ctx.ShowProgress != null)
                {
                    ctx.ShowProgress(ProgressSeverity.Info, "PopulateFileHelpersEngines Error : " + ex.Message, true);
                }

                throw new CustomException("Error in initialising filehelper engine: " + ex);
            }
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                // Get Distinct Claim Numbers 
                if (_coreClaimData != null)
                    _lstClaim = _coreClaimData
                        .GroupBy(i => i.ClaimNumber)
                        .Select(g => g.First())
                        .ToList();
            }

            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }

        }

        public void Translate()
        {
            //int _skipValue = Convert.ToInt32(ConfigurationManager.AppSettings["LVSkipNum"]);
            //int _takeValue = Convert.ToInt32(ConfigurationManager.AppSettings["LVTakeNum"]);


            foreach (var claim in _lstClaim) //.Skip(_skipValue).Take(_takeValue))
            {
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                #region GeneralClaimData
                try
                {
                    motorClaim.ClaimNumber = claim.ClaimNumber;
                    motorClaim.IncidentDate = Convert.ToDateTime(claim.IncidentDate);
                    motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general claim data: " + ex);
                }
                #endregion

                #region ClaimInfo
                try
                {
                    //if (!string.IsNullOrEmpty(claim.ClaimStatus))
                    //{
                    switch (claim.ClaimStatus.ToUpper())
                    {
                        case "OPEN":
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                            break;
                        case "SETTLED":
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                            break;
                        case "WITHDRAWN":
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Withdrawn;
                            break;
                        case "REOPENED":
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Reopened;
                            break;
                        case "DELETE":
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Delete;
                            break;
                        default:
                            motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                            break;
                        //}

                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating claim info data: " + ex);
                }
                #endregion
         
                #region Policy
                try
                {
                    if (!string.IsNullOrEmpty(claim.Insurer))
                        motorClaim.Policy.Insurer = claim.Insurer;

                    if (!string.IsNullOrEmpty(claim.PolicyNumber))
                        motorClaim.Policy.PolicyNumber = claim.PolicyNumber;

                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating policy data: " + ex);
                }

                #endregion

                #region Insured Vehicle
                PipelineVehicle insuredVehicle = new PipelineVehicle();
                insuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                if (!string.IsNullOrEmpty(claim.InsuredVehicle_Registration))
                    insuredVehicle.VehicleRegistration = claim.InsuredVehicle_Registration;

                if (!string.IsNullOrEmpty(claim.InsuredVehicle_Make))
                    insuredVehicle.VehicleMake = claim.InsuredVehicle_Make;

                if (!string.IsNullOrEmpty(claim.InsuredVehicle_Model))
                    insuredVehicle.VehicleModel = claim.InsuredVehicle_Model;

                #region PolicyHolder
                PipelinePerson policyholder = new PipelinePerson();
                policyholder.I2Pe_LinkData.PartyType_Id = (int)PartyType.Policyholder;
                policyholder.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                if (!string.IsNullOrEmpty(claim.PolicyHolder_Email))
                    policyholder.EmailAddresses.Add(new PipelineEmail { EmailAddress = claim.PolicyHolder_Email });
              
                if (!string.IsNullOrEmpty(claim.PolicyHolder_Title))
                    policyholder.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.PolicyHolder_Title);

                if (!string.IsNullOrEmpty(claim.PolicyHolder_Forename))
                    policyholder.FirstName = claim.PolicyHolder_Forename;

                if (!string.IsNullOrEmpty(claim.PolicyHolder_Surname))
                    policyholder.LastName = claim.PolicyHolder_Surname;

                policyholder.DateOfBirth = claim.PolicyHolder_DOB;


                if (!string.IsNullOrEmpty(claim.PolicyHolder_Gender))
                {

                    policyholder.Gender_Id = (int)GenderHelper.Salutation2Gender(claim.PolicyHolder_Title);

                    if (policyholder.Gender_Id == 0)
                    {

                        switch (claim.PolicyHolder_Gender.ToUpper())
                        {
                            case "FEMALE":
                                policyholder.Gender_Id = 2;
                                break;
                            case "MALE":
                                policyholder.Gender_Id = 1;
                                break;
                            default:
                                policyholder.Gender_Id = 0;
                                break;
                        }
                    }
                }
                else
                {
                    policyholder.Gender_Id = (int)GenderHelper.Salutation2Gender(claim.PolicyHolder_Title);
                }


                if (!string.IsNullOrEmpty(claim.PolicyHolder_Gender))
                    policyholder.Gender_Id = (int)GenderHelper.Salutation2Gender(claim.PolicyHolder_Title);

                if (!string.IsNullOrEmpty(claim.PolicyHolder_NI))
                    policyholder.NINumbers.Add(new PipelineNINumber { NINumber1 = claim.PolicyHolder_NI });

                if (!string.IsNullOrEmpty(claim.PolicyHolder_ContactNumber))
                    policyholder.Telephones.Add(new PipelineTelephone
                    {
                        ClientSuppliedNumber = claim.PolicyHolder_ContactNumber,
                        TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline
                    });

                #region PolicyHolder Address
                PipelineAddress policyHolderAddress = new PipelineAddress();

                if (!string.IsNullOrEmpty(claim.PolicyHolder_AddressLine1))
                    policyHolderAddress.BuildingNumber = claim.PolicyHolder_AddressLine1;

                if (!string.IsNullOrEmpty(claim.PolicyHolder_AddressLine2))
                    policyHolderAddress.Street = claim.PolicyHolder_AddressLine2;

                if (!string.IsNullOrEmpty(claim.PolicyHolder_AddressLine3))
                    policyHolderAddress.Locality = claim.PolicyHolder_AddressLine3;

                if (!string.IsNullOrEmpty(claim.PolicyHolder_AddressLine4))
                    policyHolderAddress.Town = claim.PolicyHolder_AddressLine4;

                if (!string.IsNullOrEmpty(claim.PolicyHolder_Postcode))
                    policyHolderAddress.PostCode = claim.PolicyHolder_Postcode;

                policyholder.Addresses.Add(policyHolderAddress);


                #endregion

                if (policyholder != null)
                    insuredVehicle.People.Add(policyholder);

                

                #endregion

                #region InsuredDriver

                PipelinePerson insuredDriver = new PipelinePerson();
                insuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                insuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                insuredDriver.EmailAddresses.Add(new PipelineEmail { EmailAddress = claim.InsuredDriver_EmailAddress });

                if (!string.IsNullOrEmpty(claim.InsuredDriver_Title))
                    insuredDriver.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.InsuredDriver_Title);

                if (!string.IsNullOrEmpty(claim.InsuredDriver_Forename))
                    insuredDriver.FirstName = claim.InsuredDriver_Forename;

                if (!string.IsNullOrEmpty(claim.InsuredDriver_Surname))
                    insuredDriver.LastName = claim.InsuredDriver_Surname;

                insuredDriver.DateOfBirth = claim.InsuredDriver_DOB;

                if (!string.IsNullOrEmpty(claim.InsuredDriver_Gender))
                    insuredDriver.Gender_Id = (int)GenderHelper.Salutation2Gender(claim.InsuredDriver_Title);

                if (!string.IsNullOrEmpty(claim.InsuredDriver_Gender))
                {

                    insuredDriver.Gender_Id = (int)GenderHelper.Salutation2Gender(claim.InsuredDriver_Title);

                    if (insuredDriver.Gender_Id == 0)
                    {

                        switch (claim.InsuredDriver_Gender.ToUpper())
                        {
                            case "FEMALE":
                                insuredDriver.Gender_Id = 2;
                                break;
                            case "MALE":
                                insuredDriver.Gender_Id = 1;
                                break;
                            default:
                                insuredDriver.Gender_Id = 0;
                                break;
                        }
                    }
                }
                else
                {
                    insuredDriver.Gender_Id = (int)GenderHelper.Salutation2Gender(claim.InsuredDriver_Title);
                }


                if (!string.IsNullOrEmpty(claim.InsuredDriver_NI))
                    insuredDriver.NINumbers.Add(new PipelineNINumber { NINumber1 = claim.InsuredDriver_NI });

                if (!string.IsNullOrEmpty(claim.InsuredDriver_ContactNumber))
                    insuredDriver.Telephones.Add(new PipelineTelephone
                    {
                        ClientSuppliedNumber = claim.InsuredDriver_ContactNumber,
                        TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline
                    });

                #region InsuredDriver Address
                PipelineAddress insuredDriverAddress = new PipelineAddress();

                if (!string.IsNullOrEmpty(claim.PolicyHolder_AddressLine1))
                    insuredDriverAddress.BuildingNumber = claim.PolicyHolder_AddressLine1;

                if (!string.IsNullOrEmpty(claim.PolicyHolder_AddressLine2))
                    insuredDriverAddress.Street = claim.PolicyHolder_AddressLine2;

                if (!string.IsNullOrEmpty(claim.PolicyHolder_AddressLine3))
                    insuredDriverAddress.Locality = claim.PolicyHolder_AddressLine3;

                if (!string.IsNullOrEmpty(claim.PolicyHolder_AddressLine4))
                    insuredDriverAddress.Town = claim.PolicyHolder_AddressLine4;

                if (!string.IsNullOrEmpty(claim.PolicyHolder_Postcode))
                    insuredDriverAddress.PostCode = claim.PolicyHolder_Postcode;

                insuredDriver.Addresses.Add(insuredDriverAddress);

                if (insuredDriver != null)
                    insuredVehicle.People.Add(insuredDriver);

                motorClaim.Vehicles.Add(insuredVehicle);

                #endregion


                #endregion

                #endregion

                #region Third Party Vehicle
                 
                PipelineVehicle thirdPartyVehicle = new PipelineVehicle();

                thirdPartyVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyVehicle;

                if (!string.IsNullOrEmpty(claim.ClaimantVehicle_Registration))
                    thirdPartyVehicle.VehicleRegistration = claim.ClaimantVehicle_Registration;

                if (!string.IsNullOrEmpty(claim.ClaimantVehicle_Make))
                    thirdPartyVehicle.VehicleMake = claim.ClaimantVehicle_Make;

                if (!string.IsNullOrEmpty(claim.ClaimantVehicle_Model))
                    thirdPartyVehicle.VehicleModel = claim.ClaimantVehicle_Model;

                #region TP Claimant
                PipelinePerson claimant = new PipelinePerson();
                claimant.I2Pe_LinkData.PartyType_Id = (int)PartyType.Claimant;
                claimant.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Unknown;

                if (!string.IsNullOrEmpty(claim.PolicyHolder_Title))
                    claimant.Salutation_Id = (int)SalutationHelper.GetSalutation(claim.Claimant_Title);

                if (!string.IsNullOrEmpty(claim.Claimant_Forename))
                    claimant.FirstName = claim.Claimant_Forename;

                if (!string.IsNullOrEmpty(claim.Claimant_Surname))
                    claimant.LastName = claim.Claimant_Surname;

                claimant.DateOfBirth = claim.Claimant_DOB;

                if (!string.IsNullOrEmpty(claim.Claimant_Gender))
                {

                    claimant.Gender_Id = (int)GenderHelper.Salutation2Gender(claim.Claimant_Title);

                    if (claimant.Gender_Id == 0)
                    {

                        switch (claim.Claimant_Gender.ToUpper())
                        {
                            case "FEMALE":
                                claimant.Gender_Id = 2;
                                break;
                            case "MALE":
                                claimant.Gender_Id = 1;
                                break;
                            default:
                                claimant.Gender_Id = 0;
                                break;
                        }
                    }
                }
                else
                {
                    claimant.Gender_Id = (int)GenderHelper.Salutation2Gender(claim.Claimant_Title);
                }
                    

                if (!string.IsNullOrEmpty(claim.Claimant_NI))
                    claimant.NINumbers.Add(new PipelineNINumber { NINumber1 = claim.Claimant_NI });

                if (!string.IsNullOrEmpty(claim.Claimant_ContactNumber))
                    claimant.Telephones.Add(new PipelineTelephone
                    {
                        ClientSuppliedNumber = claim.Claimant_ContactNumber,
                        TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline
                    });

                #region claimant Address
                PipelineAddress ClaimantAddress = new PipelineAddress();

                if (!string.IsNullOrEmpty(claim.Claimant_Address1))
                    ClaimantAddress.BuildingNumber = claim.Claimant_Address1;

                if (!string.IsNullOrEmpty(claim.Claimant_Address2))
                    ClaimantAddress.Street = claim.Claimant_Address2;

                if (!string.IsNullOrEmpty(claim.Claimant_Address3))
                    ClaimantAddress.Locality = claim.Claimant_Address3;

                if (!string.IsNullOrEmpty(claim.Claimant_Address4))
                    ClaimantAddress.Town = claim.Claimant_Address4;

                if (!string.IsNullOrEmpty(claim.Claimant_Postcode))
                    ClaimantAddress.PostCode = claim.Claimant_Postcode;

                claimant.Addresses.Add(ClaimantAddress);

                #endregion

                #region Claimant Solicitor

                PipelineOrganisation claimantSolicitor = new PipelineOrganisation();
                claimantSolicitor.OrganisationType_Id = (int)OrganisationType.Solicitor;

                if (!string.IsNullOrEmpty(claim.ClaimantSolicitors_OrganisationName))
                    claimantSolicitor.OrganisationName = claim.ClaimantSolicitors_OrganisationName;

                if (!string.IsNullOrEmpty(claim.ClaimantSolicitors_ContactNumber))
                    claimantSolicitor.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claim.ClaimantSolicitors_ContactNumber, TelephoneType_Id = (int)MDA.Common.Enum.TelephoneType.Landline });

                if (!string.IsNullOrEmpty(claim.ClaimantSolicitors_EmailAddress))
                    claimantSolicitor.EmailAddresses.Add(new PipelineEmail { EmailAddress = claim.ClaimantSolicitors_EmailAddress });

                #region Claimant Solicitor Address
                PipelineAddress claimantSolicitorAddress = new PipelineAddress();
                if (!string.IsNullOrEmpty(claim.ClaimantSolicitors_AddressLine1))
                    claimantSolicitorAddress.BuildingNumber = claim.ClaimantSolicitors_AddressLine1;

                if (!string.IsNullOrEmpty(claim.ClaimantSolicitors_AddressLine2))
                    claimantSolicitorAddress.Street = claim.ClaimantSolicitors_AddressLine2;

                if (!string.IsNullOrEmpty(claim.ClaimantSolicitors_AddressLine3))
                    claimantSolicitorAddress.Locality = claim.ClaimantSolicitors_AddressLine3;

                if (!string.IsNullOrEmpty(claim.ClaimantSolicitors_AddressLine4))
                    claimantSolicitorAddress.Town = claim.ClaimantSolicitors_AddressLine4;

                if (!string.IsNullOrEmpty(claim.ClaimantSolicitors_Postcode))
                    claimantSolicitorAddress.PostCode = claim.ClaimantSolicitors_Postcode;

                claimantSolicitor.Addresses.Add(claimantSolicitorAddress);

                #endregion

                #endregion Claimant Solicitor

                #region CH Organisation
                PipelineOrganisation CHO_Organisation = new PipelineOrganisation();
                CHO_Organisation.OrganisationType_Id = (int)OrganisationType.CreditHire;

                if (!string.IsNullOrEmpty(claim.CHO_OrganisationName))
                    CHO_Organisation.OrganisationName = claim.CHO_OrganisationName;

                if (!string.IsNullOrEmpty(claim.CHO_ContactNumber))
                    CHO_Organisation.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claim.CHO_ContactNumber, TelephoneType_Id = (int)TelephoneType.Landline });

                if (!string.IsNullOrEmpty(claim.CHO_Email))
                    CHO_Organisation.EmailAddresses.Add(new PipelineEmail { EmailAddress = claim.CHO_Email });


                #region CHO Address

                PipelineAddress CHO_Address = new PipelineAddress();
                if (!string.IsNullOrEmpty(claim.CHO_Address1))
                    CHO_Address.BuildingNumber = claim.CHO_Address1;

                if (!string.IsNullOrEmpty(claim.CHO_Address2))
                    CHO_Address.Street = claim.CHO_Address2;

                if (!string.IsNullOrEmpty(claim.CHO_Address3))
                    CHO_Address.Locality = claim.CHO_Address3;

                if (!string.IsNullOrEmpty(claim.CHO_Address4))
                    CHO_Address.Town = claim.CHO_Address4;

                if (!string.IsNullOrEmpty(claim.CHO_Postcode))
                    CHO_Address.PostCode = claim.CHO_Postcode;

                #endregion
                CHO_Organisation.Addresses.Add(CHO_Address);



                #endregion

                #region CHO Hire Vehicle
                PipelineOrganisationVehicle CHO_HireVehicle = new PipelineOrganisationVehicle();

                CHO_HireVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.ThirdPartyHireVehicle;


                if (!string.IsNullOrEmpty(claim.ClaimantHire_Registration))
                    CHO_HireVehicle.VehicleRegistration = claim.ClaimantHire_Registration;

                if (!string.IsNullOrEmpty(claim.ClaimantHire_Make))
                    CHO_HireVehicle.VehicleMake = claim.ClaimantHire_Make;

                if (!string.IsNullOrEmpty(claim.ClaimantHire_Model))
                    CHO_HireVehicle.VehicleModel = claim.ClaimantHire_Model;


                CHO_HireVehicle.V2O_LinkData.HireStartDate = claim.ClaimantHire_StartDate;
                CHO_HireVehicle.V2O_LinkData.HireEndDate = claim.ClaimantHire_EndDate;

                if (CHO_HireVehicle != null)
                    CHO_Organisation.Vehicles.Add(CHO_HireVehicle);

                if (CHO_Organisation != null)
                    claimant.Organisations.Add(CHO_Organisation);


                #endregion 

                if (claimantSolicitor != null)
                    claimant.Organisations.Add(claimantSolicitor);



                if (claimant != null)
                    thirdPartyVehicle.People.Add(claimant);

                motorClaim.Vehicles.Add(thirdPartyVehicle);

                #endregion

                #endregion

                if (ProcessClaimFn(Ctx, motorClaim, statusTracking) == -1) return;

                }
            }
        }
    }

