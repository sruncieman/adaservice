﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using MDA.Common;
using MDA.Common.Enum;

using MDA.Common.Server;
using MDA.MappingService.Liberty.Motor.Model;
using MDA.Pipeline.Model;
using ClaimType = MDA.Common.Enum.ClaimType;
using PartyType = MDA.Common.Enum.PartyType;
using System.Text.RegularExpressions;


namespace MDA.MappingService.Liberty.Motor
{
    class ProcessMotorClaimBatch
    {

        public string File { get; set; }
        public bool Debug { get; set; }

        public List<string> defaultAddressesForClient { get; set; }

        public List<IncidentXsdIncident> incidents { get; set; }

        public void ProcessClientBatch(CurrentContext ctx, Stream filem, string clientFolder,
            bool deleteExtractedFiles,
            object statusTracking,
            Func<CurrentContext, MDA.Pipeline.Model.PipelineMotorClaim, object, int> ProcessClaimFn,
            ProcessingResults processingResults, List<string> defaultAddressesForClient)
        {
            if (filem == null)
            {
                Debug = true;
                string path = @"\\ukboldat05\projectvf\Projects\12.089 Intel\Analysis\Onboarding\Liberty\Resource\";
                incidents = new List<IncidentXsdIncident>();
                foreach (var file in Directory.GetFiles(path, "LSM*", SearchOption.AllDirectories))
                {

                    //File = @"D:\Dev\Projects\MDA\MDASolution\MDA.Mapping.Liberty\Resource\LSM155215.xml";

                    XmlSerializer serializer = new XmlSerializer(typeof(IncidentXsd));
                    StreamReader reader = new StreamReader(file);

                    var i = (IncidentXsd)serializer.Deserialize(reader);

                    incidents.Add(i.Incident);

                    reader.Close();

                }
            }
            else
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<IncidentXsdIncident>));

                    var i = (List<IncidentXsdIncident>)serializer.Deserialize(filem);

                    incidents = i;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in creating xml file: " + ex);
                }
            }

            this.defaultAddressesForClient = defaultAddressesForClient;

            #region Translate to pipelineMapping

            foreach (var incident in incidents)
            {
                PipelineMotorClaim motorClaim = new PipelineMotorClaim();

                #region GeneralClaimData

                try
                {

                    if (!string.IsNullOrEmpty(incident.Reference))
                        motorClaim.ClaimNumber = incident.Reference;

                    motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                    motorClaim.IncidentDate = Convert.ToDateTime(incident.dtIncident);
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general claim data: " + ex);
                }

                #endregion

                #region ClaimInfo

                try
                {
                    if (!string.IsNullOrEmpty(incident.Status.ToString()))
                    {
                        switch (incident.Status.ToString().ToUpper())
                        {
                            case "OPEN":
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;
                                break;
                            case "CLOSED":
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                                break;
                        }
                    }
                    else
                    {
                        motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Unknown;
                    }

                    if (!string.IsNullOrEmpty(incident.CauseCode.ToString()))
                    {
                        //if (motorClaim.MotorClaimInfo.IncidentCircumstances != null)
                        //{
                        //    string tempIncidentCirc = motorClaim.MotorClaimInfo.IncidentCircumstances;

                        //    motorClaim.MotorClaimInfo.IncidentCircumstances =
                        //        ConvertToString(incident.CauseCode) + " " + tempIncidentCirc;
                        //}
                        //else
                        motorClaim.ExtraClaimInfo.IncidentCircumstances = ConvertToString(incident.CauseCode);

                    }

                    foreach (var claim in incident.Claims)
                    {
                        if (!string.IsNullOrEmpty(claim.Type))
                        {
                            if (motorClaim.ExtraClaimInfo.ClaimCode != null)
                                motorClaim.ExtraClaimInfo.ClaimCode += "; " + claim.Type;
                            else
                                motorClaim.ExtraClaimInfo.ClaimCode = claim.Type;
                        }

                        string tempCosts = String.Empty;
                        if (claim.Costs != null)
                        {

                            foreach (var cost in claim.Costs)
                            {
                                if (!tempCosts.Contains(cost.Type))
                                    motorClaim.ExtraClaimInfo.ClaimCode += ", " + cost.Type;

                                tempCosts += cost.Type;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(incident.Location.FreeText))
                        motorClaim.ExtraClaimInfo.IncidentLocation = incident.Location.FreeText;

                    if (!string.IsNullOrEmpty(incident.WhatHappened))
                    {
                        if (motorClaim.ExtraClaimInfo.IncidentCircumstances != null)
                        {
                            motorClaim.ExtraClaimInfo.IncidentCircumstances += ". " + incident.WhatHappened;
                        }
                        else
                            motorClaim.ExtraClaimInfo.IncidentCircumstances = incident.WhatHappened;
                    }

                    if (incident.Police != null)
                    {
                        motorClaim.ExtraClaimInfo.PoliceAttended = true;

                        if (incident.Police.PoliceStation != null)
                            motorClaim.ExtraClaimInfo.PoliceForce = incident.Police.PoliceStation;

                        if (incident.Police.PoliceCrimeRef != null)
                            motorClaim.ExtraClaimInfo.PoliceForce = incident.Police.PoliceStation;
                    }

                    motorClaim.ExtraClaimInfo.ClaimNotificationDate = incident.Notification.dtNotified;
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating claim info data: " + ex);
                }

                #endregion

                #region Policy

                try
                {
                    var incidentXsdIncidentParty = incident.Party.FirstOrDefault();
                    if (incidentXsdIncidentParty != null)
                    {
                        var policySummaryInsurerId = incidentXsdIncidentParty.PolicySummary.idInsurer;
                        var policySummaryClientId = incidentXsdIncidentParty.PolicySummary.idClient;
                        var policySummaryBrokerId = incidentXsdIncidentParty.PolicySummary.idBroker;

                        if (policySummaryInsurerId != null)
                        {
                            var policyInsurerCompany = incident.Companies.FirstOrDefault(x => x.GUID == policySummaryInsurerId);
                            if (policyInsurerCompany != null)
                                motorClaim.Policy.Insurer = policyInsurerCompany.Name;

                        }

                        if (!string.IsNullOrEmpty(incidentXsdIncidentParty.PolicySummary.PolicyNo))
                            motorClaim.Policy.PolicyNumber = incidentXsdIncidentParty.PolicySummary.PolicyNo;

                        motorClaim.Policy.PolicyStartDate = incidentXsdIncidentParty.PolicySummary.dtStart;
                        motorClaim.Policy.PolicyEndDate = incidentXsdIncidentParty.PolicySummary.dtEnd;

                        if (policySummaryClientId != null)
                        {
                            var policyTradingCompany =
                                incident.Companies.FirstOrDefault(x => x.GUID == policySummaryClientId);
                            if (policyTradingCompany != null)
                                motorClaim.Policy.InsurerTradingName = policyTradingCompany.Name;
                        }

                        if (policySummaryBrokerId != null)
                        {
                            var policyBrokerCompany = incident.Companies.FirstOrDefault(x => x.GUID == policySummaryBrokerId);
                            if (policyBrokerCompany != null)
                                motorClaim.Policy.Broker = policyBrokerCompany.Name;
                        }

                        if (!string.IsNullOrEmpty(incidentXsdIncidentParty.PolicySummary.CoverType.ToString()))
                        {
                            switch (incidentXsdIncidentParty.PolicySummary.CoverType.ToString().ToUpper())
                            {
                                case "COMPREHENSIVE":
                                    motorClaim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.Comprehensive;
                                    break;
                                case "TPFT - THIRD PARTY FIRE AND THEFT":
                                case "FTO - FIRE AND THEFT ONLY":
                                    motorClaim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.ThirdPartyFireAndTheft;
                                    break;
                                case "TPO - THIRD PARTY ONLY":
                                    motorClaim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.ThirdPartyOnly;
                                    break;
                                default:
                                    motorClaim.Policy.PolicyCoverType_Id = (int)PolicyCoverType.Unknown;
                                    break;
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw new CustomException("Error in translating general policy data: " + ex);
                }

                #endregion

                #region Vehicles


                var vehicleCollection = incident.Party.Where(x => x.Vehicle != null);

                if (vehicleCollection != null)
                {
                    var uniqueVehicleRegs = vehicleCollection.Select(x => x.Vehicle.RegNo).Distinct();

                    foreach (var vehicleReg in uniqueVehicleRegs)
                    {

                        var vehicleInfo = vehicleCollection.Where(x => x.Vehicle.RegNo == vehicleReg).FirstOrDefault();

                        List<string> driverIds = new List<string>();
                        List<string> partyIds = new List<string>();

                        PipelineVehicle vehicle = new PipelineVehicle();

                        #region Populate Vehicle Info

                        try
                        {
                            switch (vehicleInfo.Type.ToString().ToUpper())
                            {
                                case "INSURED":
                                    vehicle.I2V_LinkData.Incident2VehicleLinkType_Id =
                                        (int)Incident2VehicleLinkType.InsuredVehicle;
                                    break;
                                case "THIRDPARTY":
                                    vehicle.I2V_LinkData.Incident2VehicleLinkType_Id =
                                        (int)Incident2VehicleLinkType.ThirdPartyVehicle;
                                    break;
                                case "WITNESS":
                                    vehicle.I2V_LinkData.Incident2VehicleLinkType_Id =
                                        (int)Incident2VehicleLinkType.NoVehicleInvolved;
                                    break;
                                default:
                                    vehicle.I2V_LinkData.Incident2VehicleLinkType_Id =
                                        (int)Incident2VehicleLinkType.Unknown;
                                    break;
                            }

                            if (vehicleInfo.Type.ToString() != "Witness" && vehicleInfo.Vehicle != null)
                            {
                                if (!string.IsNullOrEmpty(vehicleInfo.Vehicle.RegNo))
                                    vehicle.VehicleRegistration = vehicleInfo.Vehicle.RegNo;

                                if (!string.IsNullOrEmpty(vehicleInfo.Vehicle.Make))
                                    vehicle.VehicleMake = vehicleInfo.Vehicle.Make;

                                if (!string.IsNullOrEmpty(vehicleInfo.Vehicle.Model))
                                    vehicle.VehicleModel = vehicleInfo.Vehicle.Model;

                                if (!string.IsNullOrEmpty(vehicleInfo.Vehicle.Color))
                                {
                                    switch (vehicleInfo.Vehicle.Color.ToUpper())
                                    {
                                        case "BEIGE":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Beige;
                                            break;
                                        case "BLACK":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Black;
                                            break;
                                        case "BLUE":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Blue;
                                            break;
                                        case "BRONZE":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Bronze;
                                            break;
                                        case "BROWN":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Brown;
                                            break;
                                        case "CREAM":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Cream;
                                            break;
                                        case "GOLD":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Gold;
                                            break;
                                        case "GRAPHITE":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Graphite;
                                            break;
                                        case "GREEN":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Green;
                                            break;
                                        case "GREY":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Grey;
                                            break;
                                        case "LILAC":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Lilac;
                                            break;
                                        case "MAROON":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Maroon;
                                            break;
                                        case "MAUVE":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Mauve;
                                            break;
                                        case "ORANGE":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Orange;
                                            break;
                                        case "PINK":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Pink;
                                            break;
                                        case "PURPLE":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Purple;
                                            break;
                                        case "RED":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Purple;
                                            break;
                                        case "SILVER":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Silver;
                                            break;
                                        case "TURQUOISE":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Turquoise;
                                            break;
                                        case "WHITE":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.White;
                                            break;
                                        case "YELLOW":
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Yellow;
                                            break;
                                        default:
                                            vehicle.VehicleColour_Id = (int)VehicleColour.Unknown;
                                            break;
                                    }
                                }

                                if (!string.IsNullOrEmpty(vehicleInfo.Vehicle.VIN))
                                    vehicle.VIN = vehicleInfo.Vehicle.VIN;

                                if (!string.IsNullOrEmpty(vehicleInfo.Vehicle.AreaOfDamage))
                                    vehicle.DamageDescription = vehicleInfo.Vehicle.AreaOfDamage;
                            }
                        }

                        catch (Exception ex)
                        {
                            throw new CustomException("Error in translating vehicle data: " + ex);
                        }

                        #endregion

                        var collectionParties = vehicleCollection.Where(x => x.Vehicle.RegNo == vehicleReg);

                        #region People

                        foreach (var incidentXsdIncidentParty in collectionParties)
                        {

                            if (!partyIds.Contains(incidentXsdIncidentParty.idContact))
                            {

                                ContactType vehicleOwner = new ContactType();

                                PipelinePerson vehicleOwnerPerson = new PipelinePerson();
                                PipelineAddress vehicleOwnerAddress = new PipelineAddress();

                                var partyType = incidentXsdIncidentParty.Type;

                                string vehicleOwnerId = string.Empty;

                                vehicleOwnerId = incidentXsdIncidentParty.idContact;

                                vehicleOwner = incident.Contacts.FirstOrDefault(x => x.GUID == vehicleOwnerId);


                                if (vehicleOwner != null)
                                {
                                    if (vehicleOwner.GUID != incidentXsdIncidentParty.Vehicle.Driver.idContact && !partyIds.Contains(incidentXsdIncidentParty.Vehicle.Driver.idContact))
                                    {

                                        ContactType VehicleDriver = new ContactType();
                                        PipelinePerson vehicleDriverPerson = new PipelinePerson();
                                        PipelineAddress vehicleDriverAddress = new PipelineAddress();

                                        string vehicledriverId = string.Empty;

                                        vehicledriverId = incidentXsdIncidentParty.Vehicle.Driver.idContact;

                                        VehicleDriver = incident.Contacts.FirstOrDefault(x => x.GUID == vehicledriverId);

                                        AddVehiclePersonAndAddress(VehicleDriver, vehicleDriverPerson, vehicleDriverAddress, partyType.ToString(), "Driver");

                                        if (!driverIds.Contains(incidentXsdIncidentParty.Vehicle.Driver.idContact))
                                        {
                                            bool ignoreAddressFlag = _AddressMatch(vehicleDriverAddress);

                                            if (!ignoreAddressFlag)
                                                vehicleDriverPerson.Addresses.Add(vehicleDriverAddress);

                                            vehicle.People.Add(vehicleDriverPerson);

                                            driverIds.Add(incidentXsdIncidentParty.Vehicle.Driver.idContact);
                                        }

                                    }
                                }

                                if (driverIds.Count() == 0)
                                {
                                    AddVehiclePersonAndAddress(vehicleOwner, vehicleOwnerPerson, vehicleOwnerAddress, partyType.ToString(), "Driver");
                                    driverIds.Add(incidentXsdIncidentParty.Vehicle.Driver.idContact);
                                }
                                else if (partyIds.Count() == 0 && driverIds.Count() == 1)
                                {
                                    AddVehiclePersonAndAddress(vehicleOwner, vehicleOwnerPerson, vehicleOwnerAddress, partyType.ToString(), "Owner");
                                }
                                else if (partyIds.Count() > 0 && driverIds.Count() > 0)
                                {
                                    AddVehiclePersonAndAddress(vehicleOwner, vehicleOwnerPerson, vehicleOwnerAddress, partyType.ToString(), "Passenger");
                                }

                                bool ignoreVehicleOwnerAddressFlag = _AddressMatch(vehicleOwnerAddress);

                                if (!ignoreVehicleOwnerAddressFlag)
                                    vehicleOwnerPerson.Addresses.Add(vehicleOwnerAddress);

                                vehicle.People.Add(vehicleOwnerPerson);

                                partyIds.Add(incidentXsdIncidentParty.idContact);


                            }

                        }

                        #endregion

                        motorClaim.Vehicles.Add(vehicle);
                    }

                    #region Witnesses
                    var witnessCollection = incident.Party.Where(x => x.PartyRef == "Witness");

                    if (witnessCollection != null)
                    {

                        PipelineVehicle noVehicleInvolved = new PipelineVehicle();

                        foreach (var incidentXsdIncidentParty in witnessCollection)
                        {

                            ContactType witness = new ContactType();
                            PipelinePerson witnessPerson = new PipelinePerson();
                            PipelineAddress witnessAddress = new PipelineAddress();

                            var partyType = incidentXsdIncidentParty.Type;

                            string witnessId = string.Empty;

                            witnessId = incidentXsdIncidentParty.idContact;

                            witness = incident.Contacts.FirstOrDefault(x => x.GUID == witnessId);

                            if (witness != null)
                            {

                                AddVehiclePersonAndAddress(witness, witnessPerson, witnessAddress, partyType.ToString(), "Witness");

                                bool ignoreAddressFlag = _AddressMatch(witnessAddress);

                                if (!ignoreAddressFlag)
                                    witnessPerson.Addresses.Add(witnessAddress);

                                noVehicleInvolved.People.Add(witnessPerson);

                            }

                        }

                        if (noVehicleInvolved.People.Count > 0)
                        {

                            motorClaim.Vehicles.Add(noVehicleInvolved);
                        }

                    }
                    #endregion

                    #region All contacts that arent attached to a Party
                    var ids = incident.Contacts.Select(x => x.id).Except(incident.Party.Select(x => x.idContact));

                    if (ids != null)
                    {

                        PipelineVehicle noVehicleInvolved = new PipelineVehicle();

                        foreach (var i in ids)
                        {

                            ContactType contact = new ContactType();
                            PipelinePerson contactPerson = new PipelinePerson();
                            PipelineAddress contactAddress = new PipelineAddress();

                            string contactId = string.Empty;

                            contactId = i;

                            contact = incident.Contacts.FirstOrDefault(x => x.GUID == contactId);

                            if (contact != null)
                            {

                                AddVehiclePersonAndAddress(contact, contactPerson, contactAddress, "", "");

                                bool ignoreAddressFlag = _AddressMatch(contactAddress);

                                if (!ignoreAddressFlag)
                                    contactPerson.Addresses.Add(contactAddress);

                                noVehicleInvolved.People.Add(contactPerson);

                            }

                        }

                        motorClaim.Vehicles.Add(noVehicleInvolved);

                    }
                    #endregion
                }

                #endregion

                #region Organisation

                var incidentParty = incident.Party.FirstOrDefault();

                if (incidentParty != null && incidentParty.Type.ToString() == "Insured")
                {
                    var policySummaryPolicyHolderId = incidentParty.PolicySummary.idHolder;
                    var policySummaryBrokerId = incidentParty.PolicySummary.idBroker;

                    if (policySummaryPolicyHolderId != null)
                    {
                        PipelineOrganisation organisation = new PipelineOrganisation();
                        PipelineAddress address = new PipelineAddress();

                        var policyHolderCompany = incident.Companies.FirstOrDefault(x => x.GUID == policySummaryPolicyHolderId);

                        if (policyHolderCompany != null)
                        {

                            organisation.OrganisationType_Id = (int)OrganisationType.PolicyHolder;


                            if (!string.IsNullOrEmpty(policyHolderCompany.Name))
                                organisation.OrganisationName = policyHolderCompany.Name;

                            if (!string.IsNullOrEmpty(policyHolderCompany.Address.Tel1))
                            {
                                organisation.Telephones.Add(new PipelineTelephone()
                                {
                                    ClientSuppliedNumber = policyHolderCompany.Address.Tel1,
                                    TelephoneType_Id = (int)TelephoneType.Unknown
                                });
                            }

                            if (!string.IsNullOrEmpty(policyHolderCompany.Address.Tel2))
                            {
                                organisation.Telephones.Add(new PipelineTelephone()
                                {
                                    ClientSuppliedNumber = policyHolderCompany.Address.Tel2,
                                    TelephoneType_Id = (int)TelephoneType.Unknown
                                });
                            }

                            #region OrganisationAddress

                            AddOrganisationAddress(policyHolderCompany, address, organisation);

                            #endregion
                        }

                        if (organisation != null)
                            motorClaim.Organisations.Add(organisation);
                    }

                    if (policySummaryBrokerId != null)
                    {
                        PipelineOrganisation organisation = new PipelineOrganisation();
                        PipelineAddress address = new PipelineAddress();

                        var brokerCompany = incident.Companies.FirstOrDefault(x => x.GUID == policySummaryBrokerId);

                        if (brokerCompany != null)
                        {
                            organisation.OrganisationType_Id = (int)OrganisationType.Broker;


                            if (!string.IsNullOrEmpty(brokerCompany.Name))
                                organisation.OrganisationName = brokerCompany.Name;


                            #region OrganisationAddress

                            AddOrganisationAddress(brokerCompany, address, organisation);

                            #endregion
                        }

                        if (organisation != null)
                            motorClaim.Organisations.Add(organisation);
                    }

                }

                #endregion

                #region Insured Vehicle and Driver Check

                bool insuredVehicleCheck = false;
                bool insuredDriverCheck = false;

                foreach (var vehicle in motorClaim.Vehicles)
                {
                    if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                    {
                        insuredVehicleCheck = true;

                        foreach (var person in vehicle.People)
                        {
                            if (person.I2Pe_LinkData.PartyType_Id == (int)PartyType.Insured && person.I2Pe_LinkData.SubPartyType_Id == (int)SubPartyType.Driver)
                            {
                                insuredDriverCheck = true;
                            }
                        }
                    }
                }

                if (insuredVehicleCheck == false)
                {
                    PipelineVehicle defaultInsuredVehicle = new PipelineVehicle();
                    defaultInsuredVehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;

                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        defaultInsuredVehicle.People.Add(defaultInsuredDriver);

                    }

                    motorClaim.Vehicles.Add(defaultInsuredVehicle);
                }
                else
                {
                    if (insuredDriverCheck == false)
                    {
                        PipelinePerson defaultInsuredDriver = new PipelinePerson();
                        defaultInsuredDriver.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                        defaultInsuredDriver.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;

                        foreach (var vehicle in motorClaim.Vehicles)
                        {
                            if (vehicle.I2V_LinkData.Incident2VehicleLinkType_Id == (int)Incident2VehicleLinkType.InsuredVehicle)
                            {

                                vehicle.People.Add(defaultInsuredDriver);

                            }

                        }

                    }
                }

                #endregion

                if (ProcessClaimFn(ctx, motorClaim, statusTracking) == -1)
                    return;

            #endregion
            }
        }

        private static void AddOrganisationAddress(IncidentXsdIncidentCompanies policyHolderCompany, PipelineAddress address,
            PipelineOrganisation organisation)
        {

            try
            {

                address.AddressType_Id = (int)AddressLinkType.TradingAddress;

                if (!string.IsNullOrEmpty(policyHolderCompany.Address.Address))
                    address.Street = policyHolderCompany.Address.Address;

                if (!string.IsNullOrEmpty(policyHolderCompany.Address.Town))
                    address.Town = policyHolderCompany.Address.Town;

                if (!string.IsNullOrEmpty(policyHolderCompany.Address.County))
                    address.County = policyHolderCompany.Address.County;

                if (!string.IsNullOrEmpty(policyHolderCompany.Address.PostCode))
                    address.PostCode = policyHolderCompany.Address.PostCode;

                if (address != null)
                    organisation.Addresses.Add(address);

                    
            }

            catch (Exception ex)
            {
                throw new CustomException(
                    "Error in translating organisation address data: " + ex);
            }
        }

        private static void AddVehiclePersonAndAddress(ContactType personContact,
            PipelinePerson vehiclePerson, PipelineAddress vehicleAddress, string partyType, string personType)
        {

            #region VehiclePerson

            try
            {

                if (partyType == "Insured" && personType == "Owner")
                    vehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;

                if (partyType == "Insured" && personType == "Driver")
                {
                    vehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    vehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                }

                if (partyType == "Insured" && personType == "Passenger")
                {
                    vehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
                    vehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                }

                if (partyType == "ThirdParty" && personType == "Owner")
                    vehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.VehicleOwner;

                if (partyType == "ThirdParty" && personType == "Driver")
                {
                    vehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    vehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Driver;
                }

                if (partyType == "ThirdParty" && personType == "Passenger")
                {
                    vehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;
                    vehiclePerson.I2Pe_LinkData.SubPartyType_Id = (int)SubPartyType.Passenger;
                }

                if (partyType == "Witness")
                    vehiclePerson.I2Pe_LinkData.PartyType_Id = (int)PartyType.Witness;

                if (personContact != null)
                {

                    if (!string.IsNullOrEmpty(personContact.FirstName))
                        vehiclePerson.FirstName = personContact.FirstName;
                    if (!string.IsNullOrEmpty(personContact.LastName))
                        vehiclePerson.LastName = personContact.LastName;

                    if (personContact.DOB != null && personContact.DOB.ToString() != "01/01/1900 00:00:00")
                        vehiclePerson.DateOfBirth = personContact.DOB;

                    if (!string.IsNullOrEmpty(personContact.NI))
                        vehiclePerson.NINumbers.Add(new PipelineNINumber()
                        {
                            NINumber1 =
                                personContact.NI
                        });

                    if (!string.IsNullOrEmpty(personContact.Tel1))
                        vehiclePerson.Telephones.Add(new PipelineTelephone()
                        {
                            TelephoneType_Id =
                                (int)
                                TelephoneType.Landline,
                            ClientSuppliedNumber =
                                personContact
                                .Tel1
                        });

                    if (!string.IsNullOrEmpty(personContact.Tel2))
                        vehiclePerson.Telephones.Add(new PipelineTelephone()
                        {
                            TelephoneType_Id =
                                (int)
                                TelephoneType.Mobile,
                            ClientSuppliedNumber =
                                personContact
                                .Tel2
                        });

                    if (!String.IsNullOrEmpty(personContact.Title.ToString()))
                    {
                        vehiclePerson.Salutation_Id =
                            (int)
                                MDA.Common.Helpers.SalutationHelper.GetSalutation(
                                    personContact.Title.ToString());

                        switch (personContact.Title.ToString().ToUpper())
                        {
                            case "MR":
                            case "LORD":
                            case "SIR":
                                vehiclePerson.Gender_Id = (int)Gender.Male;
                                break;
                            case "MRS":
                            case "MISS":
                            case "LADY":
                            case "MS":
                                vehiclePerson.Gender_Id = (int)Gender.Female;
                                break;
                            default:
                                vehiclePerson.Gender_Id = (int)Gender.Unknown;
                                break;
                        }
                    }

                    if (!string.IsNullOrEmpty(personContact.DrivingLicenceNo))
                        vehiclePerson.DrivingLicenseNumbers.Add(new PipelineDrivingLicense() { DriverNumber = personContact.DrivingLicenceNo });

                    if (!string.IsNullOrEmpty(personContact.JobTitle))
                        vehiclePerson.Occupation = personContact.JobTitle;

                    if (!string.IsNullOrEmpty(personContact.EMail))
                        vehiclePerson.EmailAddresses.Add(new PipelineEmail()
                        {
                            EmailAddress
                                =
                                personContact
                                .EMail
                        });
                }

            }
            catch (Exception ex)
            {
                throw new CustomException(
                    "Error in translating vehicle person data: " + ex);
            }

            #endregion

            #region VehicleAddress

            try
            {
                if (personContact != null)
                {
                    if (personContact.Address != null)
                    {

                        if (!string.IsNullOrEmpty(personContact.Address.Address))
                            vehicleAddress.Street = personContact.Address.Address;

                        if (!string.IsNullOrEmpty(personContact.Address.Town))
                            vehicleAddress.Town = personContact.Address.Town;

                        if (!string.IsNullOrEmpty(personContact.Address.County))
                            vehicleAddress.County = personContact.Address.County;

                        if (!string.IsNullOrEmpty(personContact.Address.PostCode))
                            vehicleAddress.PostCode = personContact.Address.PostCode;
                    }
                }
            }

            catch (Exception ex)
            {
                throw new CustomException(
                    "Error in translating vehicle person address data: " + ex);
            }

            #endregion

        }


        public static string ConvertToString(Enum e)
        {
            // Get the Type of the enum
            Type t = e.GetType();

            // Get the FieldInfo for the member field with the enums name
            FieldInfo info = t.GetField(e.ToString("G"));
            // Check to see if the XmlEnumAttribute is defined on this field
            if (!info.IsDefined(typeof(XmlEnumAttribute), false))
            {
                // If no XmlEnumAttribute then return the string version of the enum.
                return e.ToString("G");
            }
            else
            {
                // Get the XmlEnumAttribute
                object[] o = info.GetCustomAttributes(typeof(XmlEnumAttribute), false);
                XmlEnumAttribute att = (XmlEnumAttribute)o[0];
                return att.Name;
            }
        }

        public List<PipelineAddress> defaultAddresses(List<string> defaultAddressesForClient)
        {
            List<PipelineAddress> defaultAddresses = new List<PipelineAddress>();

            foreach (var str in defaultAddressesForClient)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    PipelineAddress address = new PipelineAddress();

                    MatchCollection match = Regex.Matches(str, @"\[(.*?)\]");

                    //SB=[],BN=[],BLD=[],ST=[],LOC=[],TWN=[],CNTY=[],PC=[]

                    address.SubBuilding = match[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.BuildingNumber = match[1].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Building = match[2].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Street = match[3].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Locality = match[4].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.Town = match[5].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.County = match[6].ToString().Replace("[", string.Empty).Replace("]", string.Empty);
                    address.PostCode = match[7].ToString().Replace("[", string.Empty).Replace("]", string.Empty);

                    defaultAddresses.Add(address);
                }
            }

            return defaultAddresses;
        }

        private bool _AddressMatch(PipelineAddress address)
        {
            var dadr = defaultAddresses(defaultAddressesForClient);

            foreach (var adr in dadr)
            {
                bool match = false;

                if (!string.IsNullOrEmpty(adr.Street) && !string.IsNullOrEmpty(address.Street))
                {
                    if (address.Street.ToUpper() == adr.Street.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.Town) && !string.IsNullOrEmpty(address.Town))
                {
                    if (address.Town.ToUpper() == adr.Town.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.County) && !string.IsNullOrEmpty(address.County))
                {
                    if (address.County.ToUpper() == adr.County.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!string.IsNullOrEmpty(adr.PostCode) && !string.IsNullOrEmpty(address.PostCode))
                {
                    if (address.PostCode.ToUpper() == adr.PostCode.ToUpper())
                    {
                        match = true;
                    }
                    else
                    {
                        continue;
                    }
                }

                return match;
            }

            return false;
        }
    }
}
