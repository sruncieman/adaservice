﻿using FileHelpers;
using MDA.Common;
using MDA.Common.Enum;
using MDA.Common.Server;
using MDA.MappingService.Impl.Indemnis.Motor.Model;
using MDA.MappingService.Interface;
using MDA.Pipeline.Model;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Ionic.Zip;
using MDA.MappingService.Helpers;

namespace MDA.MappingService.Impl.Indemnis.Motor
{
    public class Mapper : IMapper
    {

        private readonly Stream _fs;
        private FileHelperEngine _coreClaimDataEngine;
        private StreamReader _claimDataFileStreamReader;
        // private FileHelperEngine _creditHireDataEngine;
        private ClaimData[] claimData;

        private readonly Func<CurrentContext, IPipelineClaim, object, int> _processClaimFn;
        public string ClientFolder { get; set; }
        public List<string> FileNames;
        public string File1;
        // public string File2;
        public CurrentContext Ctx { get; set; }
        private readonly object _statusTracking;
        private List<string> _uniqueClaimNumberList;
        private List<string> DefaultAddressesForClient { get; set; }
        private List<string> DefaultPeopleForClient { get; set; }
        private List<string> DefaultOrganisationsForClient { get; set; }

        public Mapper(Stream fs,
                        string clientFolder,
                        CurrentContext ctx,
                        Func<CurrentContext, IPipelineClaim, object, int> processClaimFn,
                        object statusTracking,
                        List<string> defaultAddressesForClient,
                        List<string> defaultPeopleForClient,
                        List<string> defaultOrganisationsForClient)
        {
            _fs = fs;
            ClientFolder = clientFolder;
            Ctx = ctx;
            _processClaimFn = processClaimFn;
            _statusTracking = statusTracking;
            DefaultAddressesForClient = defaultAddressesForClient;
            DefaultPeopleForClient = defaultPeopleForClient;
            DefaultOrganisationsForClient = defaultOrganisationsForClient;

            _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
            // _creditHireDataEngine = new FileHelperEngine(typeof(CreditHireData));
            _uniqueClaimNumberList = new List<string>();
        }

        public void RetrieveDistinctClaims()
        {
            try
            {
                var query = (from c in claimData
                             where c.ClaimNumber != ""
                             select c.ClaimNumber);

                _uniqueClaimNumberList = query.Distinct().ToList();

            }
            catch (Exception ex)
            {
                throw new CustomException("Error in retreiving distinct claims: " + ex);
            }
        }

        /// <summary>
        /// Tries to extract files, returns false if bad password
        /// </summary>
        /// <param name="zipFileEntry"><see cref="ZipEntry"/>ZipEntry</param>
        /// <param name="password"><see cref="string"/>string of password (optional)</param>
        /// <param name="unpackDirectory"><see cref="string"/>string path of folder to extract to</param>
        /// <returns>true if successful</returns>
        private static bool TryFileExtractor(ZipEntry zipFileEntry, string password, string unpackDirectory)
        {
            if (zipFileEntry == null) throw new ArgumentNullException("zipFileEntry");

            if (string.IsNullOrEmpty(unpackDirectory))
                throw new ArgumentException("Value cannot be null or empty.", "unpackDirectory");

            try
            {
                if (!string.IsNullOrEmpty(password))
                    zipFileEntry.Password = password;

                zipFileEntry.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);

            }
            catch (BadPasswordException)
            {
                return false;
            }
            return true;
        }
        public void AssignFiles()
        {
            _claimDataFileStreamReader = new StreamReader(_fs);
        }
        public void InitialiseFileHelperEngines()
        {
            try
            {
                _coreClaimDataEngine = new FileHelperEngine(typeof(ClaimData));
                // _creditHireDataEngine = new FileHelperEngine(typeof(CreditHireData));
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in initialising file helper engines: " + ex);
            }
        }
        public void PopulateFileHelperEngines()
        {
            try
            {
                claimData = _coreClaimDataEngine.ReadStream(_claimDataFileStreamReader) as ClaimData[];
            }
            catch (Exception ex)
            {
                throw new CustomException("Error in populating file helper engines: " + ex);
            }
        }
        public void PopulateFileHelpersEngines(CurrentContext ctx, string clientFolder)
        {
            throw new NotImplementedException();
        }
        public void Translate()
        {
            try
            {
                foreach (var claimId in _uniqueClaimNumberList)
                {
                     var claim = claimData.FirstOrDefault(c => c.ClaimNumber == claimId);
                     var additionalInfo = claimData.Where(c => c.ClaimNumber == claimId && c != claim).ToList();

                    if (claim == null)
                        continue;

                    var motorClaim = new PipelineMotorClaim();
                    {
                        #region Claim

                        try
                        {

                            //Check that there is a value in claim number and map it to ClaimNumber
                            if (IsValid(claim.ClaimNumber))
                                motorClaim.ClaimNumber = claim.ClaimNumber;

                            motorClaim.Policy = new PipelinePolicy();

                            //Check that there is a value in loss date and map it to IncidentDate
                            if (claim.IncidentDate != null)
                                motorClaim.IncidentDate = Convert.ToDateTime(claim.IncidentDate);

                            motorClaim.ClaimType_Id = (int)ClaimType.Motor;
                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping claim information: " + ex);
                        }

                        #endregion Claim

                        #region ClaimInfo
                        try
                        {

                            if (claim.ClaimStatus == "Settled")
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Settled;
                            else
                                motorClaim.ExtraClaimInfo.ClaimStatus_Id = (int)ClaimStatus.Open;

                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping claim information: " + ex);
                        }

                        #endregion ClaimInfo

                        #region Policy

                        try
                        {
                            motorClaim.Policy = new PipelinePolicy();
                            motorClaim.Policy.Insurer = DefaultValue(claim.Insurer);

                            if (!string.IsNullOrEmpty(claim.PolicyNo))
                                motorClaim.Policy.PolicyNumber = DefaultValue(claim.PolicyNo);

                            motorClaim.Vehicles = new List<PipelineVehicle>();

                        }
                        catch (Exception ex)
                        {
                            throw new CustomException("Error in mapping policy: " + ex);
                        }
                        #endregion Policy

                        #region Insured
                        var vehicle = new PipelineVehicle();
                        vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)Incident2VehicleLinkType.InsuredVehicle;
                        vehicle.VehicleRegistration = DefaultValue(claim.Insured_VEH_Reg);
                        vehicle.VehicleMake = DefaultValue(claim.Insured_VEH_Make);
                        vehicle.VehicleModel = DefaultValue(claim.Insured_VEH_Model);
                        vehicle.People = new List<PipelinePerson>();
                        var insuredPerson = this.Generate_Insured_Person(claim);
                        if (insuredPerson != null)
                            vehicle.People.Add(insuredPerson);

                        var policyHolderPerson = this.Generate_PH_Person(claim);
                        if (policyHolderPerson != null)
                            vehicle.People.Add(policyHolderPerson);
                    
                        motorClaim.Vehicles.Add(vehicle);
                        #endregion

                        try
                        {
                            #region Vehicle(s) & Person(s)
                            try
                            {
                                var veh = ManageVehicles(claim, additionalInfo);
                                if(veh != null && veh.Any())
                                    motorClaim.Vehicles.AddRange(veh);
                            }
                            catch (Exception ex)
                            {
                                throw new CustomException("Error in mapping vehicle/person information: " + ex);
                            }

                            #endregion
                        }
                        catch (Exception ex) { 

                        }
                    }
                    try
                    {
                        if (_processClaimFn(Ctx, motorClaim, _statusTracking) == -1)
                            return;
                    }
                    catch (Exception ex)
                    {
                        // continue
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public List<PipelineVehicle> ManageVehicles(ClaimData claimData, List<ClaimData> additionalData)
        {
            List<PipelineVehicle> vehicles = new List<PipelineVehicle>();
            var veh = ManageVehicleType(claimData);
            if (veh != null)
                vehicles.Add(veh);

            foreach(ClaimData additionalDatum in additionalData)
            {
                veh = ManageVehicleType(additionalDatum);
                if (veh != null)
                {
                    var existingCar = vehicles.FirstOrDefault(v => v.VehicleRegistration == veh.VehicleRegistration);
                    if (existingCar != null)
                    {

                        var existingPerson = existingCar.People.Any(evp => evp.FirstName == veh.People.FirstOrDefault().FirstName && evp.LastName == veh.People.FirstOrDefault().LastName);
                        if (!existingPerson)
                            existingCar.People.AddRange(veh.People);

                    } else
                        vehicles.Add(veh);
                }
            }

            return vehicles;
        }

        public PipelineVehicle ManageVehicleType(ClaimData claimData)
        {
            if (claimData.Insured_VEH_Reg == claimData.Claimant_VEH_Reg && IsValid(claimData.Claimant_VEH_Reg))
                return ManageVehicle(Incident2VehicleLinkType.InsuredVehicle, claimData);
            else
            {
                if (IsValid(claimData.Claimant_VEH_Reg))
                    return ManageVehicle(Incident2VehicleLinkType.ThirdPartyVehicle, claimData);
            }

            return null;
        }
        public PipelineVehicle ManageVehicle(Incident2VehicleLinkType incident2VehicleLinkType, ClaimData claim)
        {
            var vehicle = new PipelineVehicle();
            vehicle.I2V_LinkData.Incident2VehicleLinkType_Id = (int)incident2VehicleLinkType;
            vehicle.VehicleRegistration = DefaultValue(claim.Claimant_VEH_Reg);
            vehicle.VehicleMake = DefaultValue(claim.Claimant_VEH_Make);
            vehicle.VehicleModel = DefaultValue(claim.Claimant_VEH_Model);

            var per = this.ManagePerson(incident2VehicleLinkType, claim);
            if(per != null)
                vehicle.People = new List<PipelinePerson> { per };

            return vehicle;
        }

        public PipelinePerson ManagePerson(Incident2VehicleLinkType incident2VehicleLinkType, ClaimData claim)
        {
            var person = new PipelinePerson();

            if (incident2VehicleLinkType == Incident2VehicleLinkType.InsuredVehicle)
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;

            if (incident2VehicleLinkType == Incident2VehicleLinkType.ThirdPartyVehicle)
                person.I2Pe_LinkData.PartyType_Id = (int)PartyType.ThirdParty;

            if(IsValid(claim.Claimant_Salutation))
                person.Salutation_Id = (int)SalutationConvertor.ConvertTo(claim.Claimant_Salutation);

            if (IsValid(claim.Claimant_Surname))
                person.LastName = DefaultValue(claim.Claimant_Surname);

            if (IsValid(claim.Claimant_Forename))
                person.FirstName = DefaultValue(claim.Claimant_Forename);

            if (claim.Claimant_DateOfBirth != null)
                person.DateOfBirth = claim.Claimant_DateOfBirth;

            if (claim.Claimant_Gender == "M")
                person.Gender_Id = (int)Gender.Male;
            else if (claim.Claimant_Gender == "F")
                person.Gender_Id = (int)Gender.Female;
            else
                person.Gender_Id = (int)Gender.Unknown;

            if (NiNumberHelper.IsValid(claim.Claimant_NiNumber))
                person.NINumbers.Add(new PipelineNINumber { NINumber1 = NiNumberHelper.ReturnValueOrNull(claim.Claimant_NiNumber).ToUpper() });

            if (IsValid(claim.Claimant_ContactNumber))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = DefaultValue(claim.Claimant_ContactNumber), TelephoneType_Id = (int)TelephoneType.Unknown }); //Type=1

            if (EmailHelper.IsValid(claim.Claimant_EmailAddress))
                person.EmailAddresses.Add(new PipelineEmail { EmailAddress = EmailHelper.ReturnValueOrNull(claim.Claimant_EmailAddress) });

            person.Addresses = new List<PipelineAddress>
            {
                new PipelineAddress
                {
                    Building = DefaultValue(claim.Claimant_Addr1),
                    Street = DefaultValue(claim.Claimant_Addr2),
                    Locality = DefaultValue(claim.Claimant_Addr3),
                    Town = DefaultValue(claim.Claimant_Addr4),
                    PostCode = DefaultValue(claim.Claimant_PostCode),
                }
            };

            return person;
        }
        private PipelinePerson Generate_PH_Person(ClaimData claim)
        {
            if (!IsValid(claim.PH_Surname) && !IsValid(claim.PH_Forename) && claim.PH_DateOfBirth == null)
                return null;

            var person = new PipelinePerson();

            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Policyholder;
            person.Pe2Po_LinkData.PolicyLinkType_Id = (int)PolicyLinkType.Insured;

            if (IsValid(claim.PH_Surname))
                person.LastName = DefaultValue(claim.PH_Surname);

            if (IsValid(claim.PH_Forename))
                person.FirstName = DefaultValue(claim.PH_Forename);

            if (IsValid(claim.PH_Salutation))
                person.Salutation_Id = (int)SalutationConvertor.ConvertTo(claim.PH_Salutation);

            if (claim.PH_Gender == "M")
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Male;
            else if (claim.PH_Gender == "F")
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Female;
            else
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Unknown;

            if (claim.PH_DateOfBirth != null)
                person.DateOfBirth = claim.PH_DateOfBirth;

            if (NiNumberHelper.IsValid(claim.PH_NiNumber))
                person.NINumbers.Add(new PipelineNINumber { NINumber1 = NiNumberHelper.ReturnValueOrNull(claim.PH_NiNumber) });

            if (IsValid(claim.PH_ContactNumber))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claim.PH_ContactNumber, TelephoneType_Id = (int)TelephoneType.Unknown }); //Type=1

            if (IsValid(claim.PH_EmailAddress))
                person.EmailAddresses.Add(new PipelineEmail { EmailAddress = claim.PH_EmailAddress });

            if (!string.IsNullOrEmpty(claim.CreditHire_ORG_Name))
            {
                person.Organisations = new List<PipelineOrganisation>();
                var org = new PipelineOrganisation
                {
                    OrganisationName = claim.CreditHire_ORG_Name,
                    OrganisationType_Id = (int)OrganisationType.CreditHire,
                    P2O_LinkData = { Person2OrganisationLinkType_Id = (int)Person2OrganisationLinkType.Hire }
                };
                person.Organisations.Add(org);
            }

            person.Addresses = new List<PipelineAddress>();
            person.Addresses.Add(new PipelineAddress
            {
                Building = DefaultValue(claim.PH_Addr1),
                Street = DefaultValue(claim.PH_Addr2),
                Locality = DefaultValue(claim.PH_Addr3),
                Town = DefaultValue(claim.PH_Addr4),
                PostCode = DefaultValue(claim.PH_PostCode)
            });

            return person;
        }
        private PipelinePerson Generate_Insured_Person(ClaimData claim)
        {
            if (!IsValid(claim.Insured_Surname) && !IsValid(claim.Insured_Forename) && claim.Insured_DateOfBirth == null)
                return null;

            var person = new PipelinePerson();

            person.I2Pe_LinkData.PartyType_Id = (int)PartyType.Insured;
            person.Pe2Po_LinkData.PolicyLinkType_Id = (int)PolicyLinkType.Insured;

            if (IsValid(claim.Insured_Surname))
                person.LastName = DefaultValue(claim.Insured_Surname);

            if (IsValid(claim.Insured_Salutation))
                person.Salutation_Id = (int)SalutationConvertor.ConvertTo(claim.Insured_Salutation);

            if (claim.Insured_Gender == "M")
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Male;
            else if (claim.Insured_Gender == "F")
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Female;
            else
                person.Gender_Id = (int)MDA.Common.Enum.Gender.Unknown;

            if (IsValid(claim.Insured_Forename))
                person.FirstName = DefaultValue(claim.Insured_Forename);

            if (claim.Insured_DateOfBirth.HasValue)
                person.DateOfBirth = claim.Insured_DateOfBirth;

            if (NiNumberHelper.IsValid(claim.Insured_NiNumber))
                person.NINumbers.Add(new PipelineNINumber { NINumber1 = NiNumberHelper.ReturnValueOrNull(claim.Insured_NiNumber) });

            if (IsValid(claim.Insured_ContactNumber))
                person.Telephones.Add(new PipelineTelephone { ClientSuppliedNumber = claim.Insured_ContactNumber, TelephoneType_Id = (int)TelephoneType.Unknown }); //Type=1

            if (EmailHelper.IsValid(claim.Insured_EmailAddress))
                person.EmailAddresses.Add(new PipelineEmail { EmailAddress = EmailHelper.ReturnValueOrNull(claim.Insured_EmailAddress) });

            person.Addresses = new List<PipelineAddress>();
            person.Addresses.Add(new PipelineAddress
            {
                Building = DefaultValue(claim.Insured_Addr1),
                Street = DefaultValue(claim.Insured_Addr2),
                Locality = DefaultValue(claim.Insured_Addr3),
                Town = DefaultValue(claim.Insured_Addr4),
                PostCode = DefaultValue(claim.Insured_PostCode),
            });

            return person;
        }
        
        /// If input string value is null, return null, else return value
        public static string DefaultValue(string value)
        {
            return IsValid(value) ? value : null;
        }
        public static bool IsValid(string value)
        {
            return !string.IsNullOrEmpty(value) && !value.Equals("NULL", StringComparison.InvariantCultureIgnoreCase) && value != "Not Recorded" && value != "-";
        }
    }
}
