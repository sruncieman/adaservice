﻿using System;
using FileHelpers;

namespace MDA.MappingService.Impl.Indemnis.Motor.Model
{
    public class MoneyConvertor : ConverterBase
    {
        public override object StringToField(string from)
        {
            return Convert.ToDecimal(Decimal.Parse(from) / 100);
        }

        public override string FieldToString(object fieldValue)
        {
            return ((decimal)fieldValue).ToString("#.##").Replace(".", "");
        }

    }
}
