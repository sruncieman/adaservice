﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.Sixt.Motor.Model
{
    //[IgnoreFirst(1)]
    //[IgnoreLast(1)]
    [DelimitedRecord(",")]
    public class ClaimantData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantName;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Address1;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String City;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PostalCode;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantEmailID;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantPhoneNumber;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Sex;	
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Age;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String BirthDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientDefineCode61;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentDate;
    }
}
