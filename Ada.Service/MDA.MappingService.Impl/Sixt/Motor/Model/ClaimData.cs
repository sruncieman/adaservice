﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace MDA.MappingService.Sixt.Motor.Model
{
    //[IgnoreFirst(1)]
    [DelimitedRecord(",")]
    public class ClaimData
    {
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimNumber;	
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String CoverageCode;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentNum;	
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimantName;	
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Sex;	
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? AccidentDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        [FieldConverter(typeof(CustomDateConverter))]
        public DateTime? ReportedDate;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String SetupDate;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String Adjuster;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimPeriodRange;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClientReference;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String ClaimDescription;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentTypeCodeDescription;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String AccidentSourceCodeDescription	;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String BINatureOfLossCodeDescription;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PDNatureOfLossCodeDescription;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String PartOfBodyCodeDescription;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DriverFullName;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String DriverAge;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleTypeCategoryDesc;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String GBDRef;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String MOJPortalStage;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TotalIncurred;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String NetPaid;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TotalPaid;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String RemainingReserves;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String TotalRecoveries;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String StatusCode;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String VehicleID;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String LitigatedFlag;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String G01TPSolicitorsPaid;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String SettlementPaid;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String G14Hire;        
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String G15CRUPaid;
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public String OwnSolicitorsPaid;

    }
}
