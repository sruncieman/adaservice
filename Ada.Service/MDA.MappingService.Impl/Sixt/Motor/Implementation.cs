﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MDA.MappingService.Interface;
using System.Configuration;
using MDA.Common.Server;
using System.IO;
using MDA.Common;

namespace MDA.MappingService.Sixt.Motor
{
    public class FileMapping : MappingServiceBase, IMappingService
    {
        private string tempFolder = ConfigurationManager.AppSettings["UploadFilePath"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx">Current Context containing DB connection to use</param>
        /// <param name="fs">FileStream containing the file to be loaded</param>
        /// <param name="statusTracking"></param>
        /// <param name="processClaim">CallBack function to call for each PipelineClaim created</param>
        /// <param name="ShowProgress">Show Progress callback</param>
        /// <param name="processingResults">Structure to hold ALL processing results</param>
        public void ConvertToClaimBatch(CurrentContext ctx, Stream fs, object statusTracking,
                        Func<CurrentContext, MDA.Pipeline.Model.IPipelineClaim, object, int> ProcessClaimFn,
                        out ProcessingResults processingResults)
        {
            string clientFolder;

            GetFilePath(out clientFolder, ctx.RiskClientId);

            processingResults = new ProcessingResults("Mapping");

            try
            {
                MDA.MappingService.Sixt.Motor.Mapper mapper = new MDA.MappingService.Sixt.Motor.Mapper(fs, clientFolder, ctx, ProcessClaimFn, statusTracking);

                mapper.AttemptToExtractFilesFromZip();
                mapper.AssignFiles();
                mapper.InitialiseFileHelperEngines();
                mapper.PopulateFileHelperEngines();
                mapper.RetrieveDistinctClaims();
                mapper.Translate();
            }

            catch (Exception ex)
            {
                while (ex != null)
                {
                    processingResults.Message.AddError("Error occurred mapping Sixt file. " + ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void GetFilePath(out string clientFolder, int clientId)
        {
            clientFolder = tempFolder + "\\" + clientId;

            if (!Directory.Exists(clientFolder))
                Directory.CreateDirectory(clientFolder);
        }

        /// <summary>
        /// To implement this method you must
        /// + Use the folderPath and wildCard to check if there are any files to process. You will need to rename files AFTER processing.
        /// + Read the files(s) and process it/them into a stream or multiple streams. A STREAM holds a BATCH
        /// + Set the filename, mimeType, batchRef and Stream variables to some meaningful value
        /// + Make the Call to the processClientBatch() method ONCE PER STREAM
        /// + Lastly rename the files you processed to ensure you don't pick them up again
        /// </summary>
        /// <param name="ctx">Current Context</param>
        /// <param name="folderPath">Folder to check</param>
        /// <param name="wildCard">Filename wildcard filter</param>
        /// <param name="settleTime">Time to wait for file to settle</param>
        /// <param name="batchSize">MAximum  number of claim to put in batch</param>
        /// <param name="postOp">What do do with file after its read</param>
        /// <param name="archiveFolder">Destination folder if archiving files</param>
        /// <param name="processClientBatch">Callback to process batch</param>
        public override void ProcessMonitoredFolder(CurrentContext ctx, string folderPath, string wildCard, int settleTime, int batchSize, string postOp, string archiveFolder, Func<CurrentContext, Stream, string, string, string, string, int> processClientBatch)
        {
            // Create structure to hold our new batch of claims
            List<object> batch = new List<object>();

            int batchCount = 0;
            string firstFileName = null;

            // Get a list of the files in the folderPath filtered by the wildCard. Search the top level only (don't go into child directories)
            string[] files = Directory.GetFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

            // If we find none, return
            if (files.Count() == 0) return;

            // If ANY files in that list have been modified in the last "settleTime" minutes return.  The directory is not stable, files may still being
            // written.  Return now and wait for the next check
            if (files.Where(x => File.GetCreationTime(x).AddMinutes(settleTime) > DateTime.Now).Any())
                return;

            // If we get here we have files to process
            foreach (var file in files)
            {
                // Create a filename to use for this batch.  We will just use the first file name in this case
                if (firstFileName == null)
                    firstFileName = Path.GetFileName(file);

                if (firstFileName.Contains("Erroneous"))
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                    return;
                }

                // Add this new claim to the batch
                batch.Add(file);

                #region If Batch full process it

                if (batch.Count >= batchSize)
                {
                    batchCount++;

                    string fileName = firstFileName;
                    string mimeType = "csv";

                    string clientName = "";
                    string date;

                    switch (ctx.RiskClientId)
                    {
                        case 45:
                            clientName = "EXCL";
                            break;
                    }


                    date = DateTime.Now.ToString("dd/MM/yy HH:mm:ss");
                    date = date.Replace("/", "");
                    date = date.Replace(":", "");
                    date = date.Replace(" ", "");

                    string batchRef = clientName + date;

                    using (FileStream fileStream = File.OpenRead(file))
                    {
                        MemoryStream stream = new MemoryStream();
                        stream.SetLength(fileStream.Length);
                        fileStream.Read(stream.GetBuffer(), 0, (int)fileStream.Length);

                        #region Process the batch (call the callback function)
                        processClientBatch(ctx, stream, fileName, mimeType, "Motor", batchRef);
                        #endregion
                    }


                    // Setup for a new batch and go round the loop again
                    batch.Clear();
                    firstFileName = null;
                }




                #endregion

                #region Process all the options we invented for our postOp functionality

                // We are passed the string we entered into config.  Now we have to code it!
                // In this case we have implemented RENAME, DELETE, ARCHIVE
                // ARCHIVE checks and (if needed) creates the destination folder. It also checks if the file already exists
                // in the destination folder (which would break the MOVE) and (if needed) adds a random string to make it unique
                switch (postOp.ToUpper())
                {
                    case "RENAME":
                        File.Move(file, file + ".old");
                        break;

                    case "DELETE":
                        File.Delete(file);
                        break;

                    case "ARCHIVE":
                        if (!Directory.Exists(archiveFolder))
                            Directory.CreateDirectory(archiveFolder);   // Create destination if needed

                        string fname = archiveFolder + "\\" + Path.GetFileName(file);  // build dest path

                        if (File.Exists(fname))  // Check if it already exists
                        {
                            fname += "." + Path.GetRandomFileName();  // Adds a random string to end of filename
                        }

                        File.Move(file, fname);
                        break;
                }
                #endregion

            }

        }
    }
}
